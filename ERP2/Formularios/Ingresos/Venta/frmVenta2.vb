﻿Imports ERP.Reporte
Public Class frmVenta2

    'CLASES

    Public CSistema As New CSistema
    Public CDetalleImpuesto As New CDetalleImpuesto
    Public CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoVenta
    Dim CReporte As New CReporteVentas
    Protected PlazoCliente As Integer
    Dim vCancelarAutomatico As Boolean
    Protected vDecimalOperacion As Boolean
    Dim vIDTipoProducto As Integer = 0
    Dim vTipoProducto As String = ""
    Dim vComprobanteExento As Boolean = False
    Dim vCredito24hs As Boolean = False
    Dim vBoletaContraBoleta As Boolean = False
    Dim vIDCliente As Integer = 0
    Dim CSeguridad As New CSeguridad
    Dim ReciboVigente As Boolean = False
    Dim ReciboAnulado As Boolean = False

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private NuevoIDTransaccionValue As Integer
    Public Property NuevoIDTransaccion() As Integer
        Get
            Return NuevoIDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            NuevoIDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtDetalleValue As DataTable
    Public Property dtDetalle() As DataTable
        Get
            Return dtDetalleValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private dtPreciosProductoValue As DataTable
    Public Property dtPreciosProducto() As DataTable
        Get
            Return dtPreciosProductoValue
        End Get
        Set(ByVal value As DataTable)
            dtPreciosProductoValue = value
        End Set
    End Property

    Private EsPedidoValue As Boolean
    Public Property EsPedido() As Boolean
        Get
            Return EsPedidoValue
        End Get
        Set(ByVal value As Boolean)
            EsPedidoValue = value
        End Set
    End Property

    Private SuperadoValue As Boolean
    Public Property Superado() As Boolean
        Get
            Return SuperadoValue
        End Get
        Set(ByVal value As Boolean)
            SuperadoValue = value
        End Set
    End Property

    Private ControlarDescuentosValue As Boolean
    Public Property ControlarDescuentos() As Boolean
        Get
            Return ControlarDescuentosValue
        End Get
        Set(ByVal value As Boolean)
            ControlarDescuentosValue = value
        End Set
    End Property

    Private EsMovilValue As Boolean
    Public Property EsMovil() As Boolean
        Get
            Return EsMovilValue
        End Get
        Set(ByVal value As Boolean)
            EsMovilValue = value
        End Set
    End Property

    Private orowConfiguracionesValue As DataRow
    Public Property orowConfiguraciones() As DataRow
        Get
            Return orowConfiguracionesValue
        End Get
        Set(ByVal value As DataRow)
            orowConfiguracionesValue = value
        End Set
    End Property
    Private dtFormaPagoFacturaValue As DataTable
    Public Property dtFormaPagoFactura() As DataTable
        Get
            Return dtFormaPagoFacturaValue
        End Get
        Set(ByVal value As DataTable)
            dtFormaPagoFacturaValue = value
        End Set
    End Property
    'Para Facturacion de pedidos con linea de credito excedida aprobada
    Private ExcesoLineaCreditoAutorizadoValue As Boolean = False
    Public Property ExcesoLineaCreditoAutorizado() As Boolean
        Get
            Return ExcesoLineaCreditoAutorizadoValue
        End Get
        Set(ByVal value As Boolean)
            ExcesoLineaCreditoAutorizadoValue = value
        End Set
    End Property

    'VARIABLES
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim frmHabilitarProceso As New frmHabilitarProceso
    Public dtPuntoExpedicion As DataTable

    Sub Inicializar()

        Me.AcceptButton = New Button

        'Controles
        txtProducto.Venta = True
        txtProducto.ConectarVenta()
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente Where Estado='ACTIVO' "
        txtCliente.frm = Me
        'txtCliente.txtRazonSocial.Enabled = False
        'txtCliente.txtRUC.Enabled = False

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        OcxImpuesto1.Inicializar()
        OcxImpuesto1.dg.ReadOnly = True

        'Cotizacion
        OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaVenta.txt.Text, True, False)
        OcxCotizacion1.Inicializar()

        'Propiedades
        IDTransaccion = 0
        ControlarDescuentos = True
        vNuevo = False

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion("frmVenta", "VENTAS CLIENTES", "FATCLI", CadenaConexion)
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        btnHabilitar.Visible = False

        'Foco
        txtNroComprobante.Focus()
        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

        'CONFIGURACIONES
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        If CBool(vgConfiguraciones("VentaBloquearFecha").ToString) = True Then
            txtFechaVenta.Enabled = False
        Else
            txtFechaVenta.Enabled = True
        End If

        'Esta opcion es configurable en el menu de configuracion de pedido
        If CBool(vgConfiguraciones("BloquearListaPrecio").ToString) = True Then
            cbxListaPrecio2.Enabled = False
        Else
            cbxListaPrecio2.Enabled = True
        End If

        'Esta opcion es configurable en el menu de configuracion de pedido
        'chkImprimir2.Valor = CBool(vgConfiguraciones("VentaImprimirSinRpt").ToString)

        txtFechaVenta.SetValue(VGFechaFacturacion)
        txtProducto.FechaFacturarPedido = VGFechaFacturacion
        vCancelarAutomatico = False
        OcxCotizacion1.txtCotizacion.Enabled = False
        txtNroComprobante.Enabled = True
        btnImprimirRecibo.Visible = False
        btnAnularRecibo.Visible = False
        CSeguridad.OcultarControlesDenegados(Me)
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, cbxListaPrecio2)
        CSistema.CargaControl(vControles, txtFechaVenta)
        CSistema.CargaControl(vControles, cbxCondicion)
        CSistema.CargaControl(vControles, txtVencimiento)
        CSistema.CargaControl(vControles, txtPlazo)
        CSistema.CargaControl(vControles, cbxDeposito2)
        CSistema.CargaControl(vControles, txtDetalle)
        CSistema.CargaControl(vControles, txtTipoEntrega)
        CSistema.CargaControl(vControles, OcxCotizacion1)
        'CSistema.CargaControl(vControles, txtDireccion)
        'CSistema.CargaControl(vControles, txtTelefono)

        'Detalle
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        'CSistema.CargaControl(vControles, cbxUnidad) NO FUNCIONA MOMENTANEAMENTE
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, lklDescuento)
        'CSistema.CargaControl(vControles, txtPrecioUnitario)
        'CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, lvLista)

        'Cotizacion
        CSistema.CargaControl(vControles, OcxCotizacion1)
        CSistema.CargaControl(vControles, cbxFormaPagoFactura)

        dtDetalle = CData.GetStructure("VDetalleVenta", "Select Top(0) * From VDetalleVenta")
        dtDescuento = CData.GetStructure("VDescuento", "Select Top(0) * From VDescuento")

        dtPreciosProducto = New DataTable
        dtPreciosProducto.Columns.Add("Precio")
        dtPreciosProducto.Columns.Add("Cantidad")
        dtPreciosProducto.Columns.Add("PorcentajeDescuento")
        dtPreciosProducto.Columns.Add("DescuentoUnitario")
        dtPreciosProducto.Columns.Add("TotalDescuento")
        dtPreciosProducto.Columns.Add("Total")
        dtPreciosProducto.Columns.Add("TotalSinDescuento")
        dtPreciosProducto.Columns.Add("DescuentoUnitarioDiscriminado")
        dtPreciosProducto.Columns.Add("TotalDescuentoDiscriminado")

        'Descuentos
        dtPreciosProducto.Columns.Add("TPR")
        dtPreciosProducto.Columns.Add("Tactico")
        dtPreciosProducto.Columns.Add("Express")
        dtPreciosProducto.Columns.Add("Acuerdo")
        dtPreciosProducto.Columns.Add("TacticoPlanilla")

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR UNIDAD
        cbxUnidad.Items.Add("UNIDAD")
        cbxUnidad.Items.Add("CAJA")
        cbxUnidad.DropDownStyle = ComboBoxStyle.DropDownList


        'CARGAR CONTROLES
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", " IDOperacion = " & IDOperacion), "ID", "Codigo")

        CSistema.SqlToComboBox(cbxFormaPagoFactura.cbx, "EXEC SpViewFormaPagoFactura @IDUsuario =" & vgIDUsuario)

        'Ciudad
        cbxCiudad.Conectar()

        'Sucursal
        cbxSucursal.Conectar()

        'Puntos de Expediciones
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion").Copy
        dtPuntoExpedicion = CData.FiltrarDataTable(dtPuntoExpedicion, " IDOperacion = " & IDOperacion & " And Estado='True' ")
        txtIDTimbrado.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIMBRADO", 0))
        'Si no tenemos el timbrado, solicitar
        If txtIDTimbrado.GetValue = 0 Then
            MessageBox.Show("Para empezar seleccione un timbrado para continuar.", "Timbrado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            SeleccionarTimbrado()
        End If

        ObtenerInformacionPuntoVenta()

        'Vendedor
        'cbxVendedor.Conectar()

        'Promotor
        cbxPromotor.Conectar()

        'Listas de Precio
        'CSistema.SqlToComboBox(cbxListaPrecio2.cbx, CData.GetTable("VListaPrecio"), "ID", "Descripcion")
        'Listade precio por Sucursal
        CSistema.SqlToComboBox(cbxListaPrecio2.cbx, CData.GetTable("VListaPrecio", " IDSucursal = " & vgIDSucursal & " "), "ID", "Descripcion")


        'CONFIGURACIONES
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        If CBool(vgConfiguraciones("VentaBloquearFecha").ToString) = True Then
            txtFechaVenta.Enabled = False
        Else
            txtFechaVenta.Enabled = True
        End If

        'Fecha de Venta
        txtFechaVenta.SetValue(VGFechaFacturacion)

        Try
            'Modificacion de Precio
            txtPrecioUnitario.SoloLectura = Not CBool(vgConfiguraciones("VentaModificarPrecio").ToString)
        Catch ex As Exception

        End Try
    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
    End Sub

    Sub GuardarInformacion()

        'TIMBRADO
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIMBRADO", txtIDTimbrado.GetValue)
    End Sub

    Sub Nuevo()
        If CBool(vgConfiguraciones("BloquearNroComprobante").ToString) = True Then
            txtNroComprobante.Enabled = False
        Else
            txtNroComprobante.Enabled = True
        End If

        vTipoProducto = ""
        vIDTipoProducto = 0
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar cabecera
        txtCliente.SetValue(Nothing)

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        dtDescuento.Rows.Clear()
        ListarDetalle()

        OcxImpuesto1.Reestablecer()
        OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaVenta.txt.Text, True, False)
        OcxCotizacion1.Inicializar()

        'Recargar el dtTerminalPuntoExpedicion
        CData.ResetTable("VPuntoExpedicion")
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion & " And Estado='True' ")
        ObtenerInformacionPuntoVenta()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        OcxImpuesto1.IDMoneda = OcxCotizacion1.Registro("ID")
        'OcxCotizacion1.Cargar()
        CAsiento.InicializarAsiento()

        vNuevo = True

        If cbxUnidad.Text = "" Then
            cbxUnidad.SelectedIndex = 0
        End If

        If cbxCondicion.cbx.Text = "" Then
            cbxCondicion.cbx.SelectedIndex = 1
        End If

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        ObtenerDeposito()

        'Poner el foco en el proveedor
        txtNroComprobante.txt.SelectAll()
        txtNroComprobante.txt.Focus()

        'CONFIGURACIONES
        If IsDate(VGFechaFacturacion) = False Then
            VGFechaFacturacion = VGFechaHoraSistema
        End If

        If CBool(vgConfiguraciones("VentaBloquearFecha").ToString) = True Then
            txtFechaVenta.Enabled = False
        Else
            txtFechaVenta.Enabled = True
        End If

        txtFechaVenta.SetValue(VGFechaFacturacion)
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFechaVenta.GetValue, True, False) & "' and IDMoneda = " & OcxCotizacion1.Registro("ID")) = 0 And OcxCotizacion1.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
            Exit Sub
        End If
        vComprobanteExento = CType(CSistema.ExecuteScalar("Select Exento from vTipoComprobante where ID = " & cbxTipoComprobante.GetValue & " "), Boolean)
    End Sub

    Sub Cancelar()
        txtNroComprobante.Enabled = True
        vNuevo = False

        btnHabilitar.Visible = False

        Dim e As New KeyEventArgs(Keys.End)
        txtNroComprobante_TeclaPrecionada(New Object, e)

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'txtNroComprobante.txt.ReadOnly = False
        txtNroComprobante.txt.Focus()

    End Sub

    Function HabilitarLimiteCredito() As Boolean

        HabilitarLimiteCredito = False
        'Dim Limite As Boolean = False
        'If ExcesoLineaCreditoAutorizado = True Then
        '    Return True
        '    Exit Function
        'End If

        ''Controlar que el documento sea credito
        'If cbxCondicion.cbx.Text = "CONTADO" Then
        '    Limite = True
        'End If

        ''Si no tiene limite. salir
        'If txtCliente.Registro("LimiteCredito") = 0 Then
        '    Return True
        'End If

        ''Controlar que el total no supere el saldo
        'Dim RestoCredito As Decimal = CType(CSistema.ExecuteScalar("Select Deuda from vCliente where ID = " & txtCliente.Registro("ID") & " "), Decimal)
        ''Dim ImporteActual As Decimal = OcxImpuesto1.txtTotal.ObtenerValor
        'Dim ImporteActual As Decimal = CSistema.FormatoMoneda((OcxImpuesto1.txtTotal.txt.Text * OcxCotizacion1.Registro("Cotizacion")), False)
        'Dim LimiteCredito As Decimal = txtCliente.Registro("LimiteCredito")
        'Dim Atraso As Integer = CSistema.ExecuteScalar("Select Atraso from VCliente where id =" & txtCliente.Registro("ID") & " ")
        'Dim AtrasoTipoCliente As Integer = CSistema.ExecuteScalar("Select ToleranciaDiasCobranzaPostVencimiento from TipoCliente where ID = (Select IDTipoCliente from cliente where id = " & txtCliente.Registro("ID") & " )")
        'Dim ChequeRechazado As Integer = CSistema.ExecuteScalar("select count(*) from ChequeCliente where rechazado = 1 and saldoacuenta > 0 and idcliente=" & vIDCliente)

        'RestoCredito = RestoCredito + ImporteActual

        ''Mientras no sea negativo el resto
        'If Limite = False Then
        '    If RestoCredito <> 0 Then
        '        If RestoCredito <= LimiteCredito Then
        '            'Return True
        '            Limite = True
        '        End If
        '    End If
        'End If

        'If EsPedido Then
        '    If Atraso < AtrasoTipoCliente Then
        '        If Limite Then
        '            Return True
        '        End If
        '    End If
        'Else
        '    If Limite Then
        '        Return True
        '    End If
        'End If

        '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        'ATENCION! NO ESTA CONFIGURADO EL BLOQUEO DE FACTURA DOBLE CONTADO
        '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        'Dim frm As New frmHabilitarLimitedeCredito
        'frm.NombreFrm = "frmVenta"
        'frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        'frm.WindowState = FormWindowState.Normal
        'frm.StartPosition = FormStartPosition.CenterScreen
        'frm.ShowDialog(Me)

        'If frm.Aprobado = False Then
        '    Exit Function
        'End If

        'HabilitarLimiteCredito = frm.Aprobado

        HabilitarLimiteCredito = False


    End Function

    Function HabilitarLimiteDescuento() As Boolean

        If ControlarDescuentos = False Then
            Return True
        End If

        HabilitarLimiteDescuento = False

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Exec SpControlarActividad @IDTransaccion=" & IDTransaccion)
        Dim Mensaje As String = ""

        If dt Is Nothing Then
            Return True
        End If

        If dt.Rows.Count = 0 Then
            Return True
        End If

        Dim Resultado As DataRow = dt.Rows(0)

        If Resultado("Procesado") = True Then
            Return True
        End If

        Dim frmHabilitar As New frmHabilitarProceso
        frmHabilitar.IDOperacion = IDOperacion
        frmHabilitar.Formulario = Me.Name
        frmHabilitar.Funcion = "HabilitarDescuentoTactico"
        frmHabilitar.Mensaje = Mensaje
        FGMostrarFormulario(Me, frmHabilitar, "Actividades", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frmHabilitar.Aprobado = False Then
            Exit Function
        End If

        HabilitarLimiteDescuento = frmHabilitar.Aprobado


    End Function

    Function ModificarDescuento() As Boolean

        If txtProducto.Seleccionado = False Then
            MessageBox.Show("Seleccione un producto primeramente para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If

        ModificarDescuento = False

        'Validar
        Dim frmHabilitar As New frmHabilitarProceso
        frmHabilitar.IDOperacion = IDOperacion
        frmHabilitar.Mensaje = "Se requiere un usuario con privilegios para habilitar la modificacion de descuentos. Favor ingrese sus credenciales."
        frmHabilitar.Formulario = "frmVenta"
        frmHabilitar.IDOperacion = IDOperacion
        frmHabilitar.Funcion = "ModificarDescuento"

        FGMostrarFormulario(Me, frmHabilitar, "Habilitar", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)


        If frmHabilitar.Aprobado = False Then
            Exit Function
        End If

        Dim frm As New frmAplicarDescuentoTactico
        frm.dt = txtProducto.Precios.Copy
        frm.IDProducto = txtProducto.Registro("ID")
        frm.Precio = txtPrecioUnitario.ObtenerValor
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        FGMostrarFormulario(Me, frm, "Descuento", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterParent, True, False)

        If frm.Procesado = False Then
            Exit Function
        End If

        txtProducto.Precios = frm.dt
        ModificarDescuento = frm.Procesado

        txtCantidad.Focus()

    End Function

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Seleccion de Punto de Expedicion
        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el punto de venta!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Punto de Expedicion
        If txtIDTimbrado.GetValue = 0 Then
            Dim mensaje As String = "Seleccione correctamente el punto de venta!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If OcxCotizacion1.Registro("ID") <> 1 And OcxCotizacion1.Registro("Cotizacion") = 1 Then
            Dim mensaje As String = "La cotizacion no es correcta!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If


        'Validar que el numero de comprobante este dentro del rango del timbrado
        If CSistema.ExecuteScalar("select count(*) from PuntoExpedicion where ID = " & txtIDTimbrado.GetValue & " and " & CInt(txtNroComprobante.txt.Text) & " between NumeracionDesde and NumeracionHasta") = 0 Then
            Dim mensaje As String = "El numero de comprobante no se encuentra en el rango del Timbrado!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Bloque para que no se pueda facturar sin pedido
        If CBool(vgConfiguraciones("BloquearVentaSinPedido").ToString) = True Then
            If (EsPedido = False) And (CSistema.ExecuteScalar("Select dbo.FAprobarVentaSinPedido(" & vgIDSucursal & ", " & cbxFormaPagoFactura.GetValue & ")") = False) Then
                Dim mensaje As String = "La venta no esta asociada a un pedido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If
        End If

        'Avisar que fecha de facturacion es menor a hoy
        If CDate(txtFechaVenta.txt.Text) < Today Then
            If MessageBox.Show("Fecha de Venta menor a hoy! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                If txtFechaVenta.Enabled = True Then
                    txtFechaVenta.Focus()
                    Exit Function
                Else
                    Cancelar()
                    Exit Function
                End If
            End If
        End If

        'Verificamos la fecha del timbrado
        If (CSistema.FormatoFechaBaseDatos(txtVencimientoTimbrado.GetValue, True, False)) < (CSistema.FormatoFechaBaseDatos(txtFechaVenta.GetValue, True, False)) Then
            Dim mensaje As String = "La fecha del timbrado ya ha vencido. Seleccione otro numero de timbrado!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comentar para Lety AQUAY
        'SC: 15/12/2021 Para Validar
        Dim PuntoExpedicionRow As DataRow = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion).Select("ID=" & txtIDTimbrado.GetValue)(0)
        Dim NumeroComprobante As String
        NumeroComprobante = CType(CSistema.ExecuteScalar("Select IsNull((Select NroComprobante FROM vVenta Where IDSucursal = " & PuntoExpedicionRow("IDSucursal").ToString & " and ReferenciaPunto= " & PuntoExpedicionRow("ReferenciaPunto") & " And Anulado='False' And NroComprobante=" & txtNroComprobante.GetValue & " and IDPuntoExpedicion= " & PuntoExpedicionRow("ID").ToString & "),0)"), Integer)

        'Verificamos que el Comprobante, no se encuentre ya existente
        If NumeroComprobante = txtNroComprobante.GetValue Then
            Dim mensaje As String = "El numero de comprobante ya existe!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtNroComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asignamos entero al nro de comprobante
        txtNroComprobante.txt.Text = CInt(txtNroComprobante.txt.Text)

        If CInt(txtNroComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtCliente.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Control de Fechas.
        If cbxCondicion.cbx.SelectedIndex = 1 Then
            If txtVencimiento.GetValue < txtFechaVenta.GetValue Then
                Dim mensaje As String = "La fecha de vencimiento no puede ser menor al de la emision!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito2.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Existencia en Detalle
        If dtDetalle.Rows.Count = 0 Then
            Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Total
        If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If
        '17/01/2019 - A partir de ahora se controlara en el panel de facturacion de pedidos
        'Habilitar limite de credito
        'If HabilitarLimiteCredito() = False Then
        '    Exit Function
        'End If

        ''Habilitar limite de actividades
        'If HabilitarLimiteDescuento() = False Then
        '    Exit Function
        'End If

        'Validar el Asiento
        If CAsiento.ObtenerSaldo <> 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        ValidarDocumento = True

    End Function

    'Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

    '    tsslEstado.Text = ""
    '    ctrError.Clear()

    '    If ValidarDocumento(Operacion) = False Then
    '        Exit Sub
    '    End If

    '    'Exit Sub

    '    Dim param(-1) As SqlClient.SqlParameter
    '    Dim IDTransaccion As Integer
    '    Dim IndiceOperacion As Integer

    '    'SetSQLParameter, ayuda a generar y configurar los parametros.
    '    'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

    '    If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
    '        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
    '    End If

    '    'Punto de Expedicion
    '    'Dim PuntoExpedicionRow As DataRow = CData.GetTable("VTerminalPuntoExpedicion", " IDTerminal = " & vgIDTerminal).Select("IDPuntoExpedicion=" & txtIDTimbrado.GetValue)(0)
    '    Dim PuntoExpedicionRow As DataRow = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion).Select("ID=" & txtIDTimbrado.GetValue)(0)

    '    CSistema.SetSQLParameter(param, "@IDPuntoExpedicion", txtIDTimbrado.GetValue, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDTipoComprobante", PuntoExpedicionRow("IDTipoComprobante").ToString, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@NroComprobante", txtNroComprobante.GetValue, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@Comprobante", PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@Direccion", txtDireccion.txt.Text, ParameterDirection.Input, 100)
    '    CSistema.SetSQLParameter(param, "@Telefono", txtTelefono.txt.Text, ParameterDirection.Input, 50)
    '    CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFechaVenta.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDSucursalOperacion", PuntoExpedicionRow("IDSucursal").ToString, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito2.cbx.SelectedValue, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDVendedor", cbxVendedor.cbx, ParameterDirection.Input)

    '    'Si es por sucursal
    '    If txtCliente.SucursalSeleccionada = True Then
    '        CSistema.SetSQLParameter(param, "@IDSucursalCliente", txtCliente.Sucursal("ID").ToString, ParameterDirection.Input)
    '        CSistema.SetSQLParameter(param, "@EsVentaSucursal", "True", ParameterDirection.Input)
    '    End If

    '    'Credito
    '    If cbxCondicion.cbx.SelectedIndex = 0 Then
    '        CSistema.SetSQLParameter(param, "@Credito", "False".ToString, ParameterDirection.Input)
    '    Else
    '        CSistema.SetSQLParameter(param, "@Credito", "True".ToString, ParameterDirection.Input)
    '        CSistema.SetSQLParameter(param, "@FechaVencimiento", txtVencimiento.GetValue, ParameterDirection.Input)
    '    End If

    '    'Otros
    '    CSistema.SetSQLParameter(param, "@IDListaPrecio", cbxListaPrecio2.cbx, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@Descuento", txtDescuento.txt.Text, ParameterDirection.Input)

    '    'Moneda
    '    CSistema.SetSQLParameter(param, "@IDMoneda", OcxCotizacion1.Registro("ID"), ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoNumeroBaseDatos(OcxCotizacion1.Registro("Cotizacion"), True), ParameterDirection.Input)

    '    CSistema.SetSQLParameter(param, "@Observacion", txtObservacionProducto.txt.Text.Trim, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@NroComprobanteRemision", txtRemision.txt.Text.Trim, ParameterDirection.Input)

    '    'Totales
    '    CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total"), vDecimalOperacion), ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalImpuesto"), vDecimalOperacion), ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado"), vDecimalOperacion), ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@TotalDescuento", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDescuento"), vDecimalOperacion), ParameterDirection.Input)

    '    CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
    '    'Capturamos el index de la Operacion para un posible proceso posterior
    '    IndiceOperacion = param.GetLength(0) - 1

    '    'Transaccion
    '    CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
    '    CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

    '    'Informacion de Salida
    '    CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
    '    CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
    '    CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

    '    Dim MensajeRetorno As String = ""

    '    'Insertar Registro
    '    If CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion) = False Then
    '        'tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
    '        'ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
    '        'ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

    '        ''Eliminar el Registro si es que se registro
    '        'If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Venta Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
    '        '    param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
    '        '    CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion)
    '        'End If

    '        'Exit Sub

    '    End If

    '    If IDTransaccion > 0 Then

    '        'Insertamos el Detalle
    '        If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

    '            'Eliminar el Registro si es que se registro
    '            'If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Compra Where IDTransaccion=1) Is Null Then 'False' Else 'True' End)")) = True Then
    '            '    param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
    '            '    CSistema.ExecuteStoreProcedure(param, "SpVenta", False, False, MensajeRetorno, IDTransaccion)
    '            'End If

    '            'Exit Sub

    '        End If

    '        'Si es nuevo
    '        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

    '            'Cargamos el DetalleImpuesto
    '            CDetalleImpuesto.Decimales = vDecimalOperacion
    '            CDetalleImpuesto.Guardar(IDTransaccion)

    '            'Cargamos el asiento
    '            CAsiento.IDTransaccion = IDTransaccion
    '            GenerarAsiento()
    '            CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

    '            'Imprimimos
    '            'If chkImprimir2.Valor = False Then
    '            '    Imprimir()
    '            'Else
    '            '   Imprimir2()
    '            'End If
    '            Imprimir2()
    '        End If

    '    End If

    '    EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

    '    CargarOperacion(IDTransaccion)

    '    txtNroComprobante.SoloLectura = False
    '    vTipoProducto = ""
    '    vIDTipoProducto = 0

    'End Sub

    Sub Guardar2()

        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "INICIANDO GUARDAR " & Date.Now.ToString() & vbCrLf)
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "frmVenta/guardar2/validarDocumento 975" & Date.Now.ToString() & vbCrLf)
        If ValidarDocumento(ERP.CSistema.NUMOperacionesRegistro.INS) = False Then
            Exit Sub
        End If
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "frmVenta/guardar2/validarDocumento 975" & Date.Now.ToString() & vbCrLf)

        Dim SQL As String = ""

        'Nueva Transaccion
        'Sin controlar la caja, se procesara en la generacion de asientos
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "frmVenta/guardar2/CSistema.NuevaTransaccion 985" & Date.Now.ToString() & vbCrLf)
        NuevoIDTransaccion = CSistema.NuevaTransaccion(IDOperacion, 0, 0, cbxSucursal.GetValue, cbxDeposito2.GetValue, 0, CadenaConexion, False)
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "frmVenta/guardar2/CSistema.NuevaTransaccion 985" & Date.Now.ToString() & vbCrLf)

        SQL = SQL & InsertVenta() & vbCrLf
        SQL = SQL & InsertDetalle() & vbCrLf
        SQL = SQL & InsertDescuento() & vbCrLf
        CDetalleImpuesto.Decimales = vDecimalOperacion
        SQL = SQL & CDetalleImpuesto.Insert(NuevoIDTransaccion) & vbCrLf

        'Guardar el Asiento
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "frmVenta/guardar2/CAsiento.InsertAsiento 996" & Date.Now.ToString() & vbCrLf)
        If CAsiento.dtDetalleAsiento.Rows.Count > 0 Then
            SQL = SQL & CAsiento.InsertAsiento(NuevoIDTransaccion) & vbCrLf
            SQL = SQL & CAsiento.InsertDetalle(NuevoIDTransaccion) & vbCrLf
        End If
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "frmVenta/guardar2/CAsiento.InsertAsiento 996" & Date.Now.ToString() & vbCrLf)

        'Guardamos
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "frmVenta/guardar2/CSistema.ExecuteNonQuery 1004" & Date.Now.ToString() & vbCrLf)
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", SQL & Date.Now.ToString() & vbCrLf)
        Dim RowCount As Integer = CSistema.ExecuteNonQuery(SQL, CadenaConexion, 600)
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "frmVenta/guardar2/CSistema.ExecuteNonQuery 1004" & Date.Now.ToString() & vbCrLf)
        If RowCount = 0 Then
            CSistema.MostrarError("Error de sistema!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If


        'Procesamos

        Dim dtRespuesta As DataTable
        If IsNumeric(lblIDTransaccionPedido.Text) Then
            If CInt(lblIDTransaccionPedido.Text) > 0 Then

                dtRespuesta = CSistema.ExecuteToDataTable("Execute SpVentaProcesarVR @IDTransaccion = " & NuevoIDTransaccion & ", @IDTransaccionPedido = " & CInt(lblIDTransaccionPedido.Text) & ", @EsPedido = '" & EsPedido.ToString & "' , @EsMovil = '" & EsMovil.ToString & "' ", CadenaConexion)

            Else

                dtRespuesta = CSistema.ExecuteToDataTable("Execute SpVentaProcesarVR @IDTransaccion = " & NuevoIDTransaccion & ", @EsPedido = '" & EsPedido.ToString & "' , @EsMovil = '" & EsMovil.ToString & "' ", CadenaConexion)

            End If
        Else

            dtRespuesta = CSistema.ExecuteToDataTable("Execute SpVentaProcesarVR @IDTransaccion = " & NuevoIDTransaccion & ", @EsPedido = '" & EsPedido.ToString & "' , @EsMovil = '" & EsMovil.ToString & "' ", CadenaConexion)

        End If


        If dtRespuesta Is Nothing Then
            CSistema.MostrarError("Error de sistema!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        If dtRespuesta.Rows.Count = 0 Then
            CSistema.MostrarError("Error de sistema!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim oRow As DataRow = dtRespuesta.Rows(0)

        If oRow("Procesado") = True Then

            IDTransaccion = NuevoIDTransaccion

            'Guardar log si se habilitaron algunos procesos
            frmHabilitarProceso.Operacion = "HAB"
            frmHabilitarProceso.Tabla = "VENTAS"
            frmHabilitarProceso.IDUsuario = vgIDUsuario
            frmHabilitarProceso.Comprobante = ObtenerNroComprobante()
            frmHabilitarProceso.IDTerminal = vgIDTerminal
            frmHabilitarProceso.IDTransaccion = IDTransaccion

            frmHabilitarProceso.Guardar()


            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            vCredito24hs = False
            vBoletaContraBoleta = False

            CargarOperacion(NuevoIDTransaccion)

            txtNroComprobante.SoloLectura = False

            'Primero Imprimir el recibo
            If (vCredito24hs = True) And (vBoletaContraBoleta = False) Then
                ImprimirRecibo(True)
            End If

            If (vBoletaContraBoleta = True) Then
                ImprimirReciboBoletaContraBoleta(True)
            End If


            'If chkImprimir2.Valor = False Then
            '    Imprimir()
            'Else
            'Comentado 13/01/2024 FA 
            'Imprimir2()
            'End If

            If EsPedido = True Then
                Me.Close()
            End If
            vCredito24hs = False
            vBoletaContraBoleta = False
        Else
            CSistema.MostrarError(oRow("Mensaje"), ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

    End Sub

    Function InsertVenta() As String

        InsertVenta = ""

        Dim param As New DataTable

        Dim PuntoExpedicionRow As DataRow = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion).Select("ID=" & txtIDTimbrado.GetValue)(0)

        CSistema.SetSQLParameter(param, "IDTransaccion", NuevoIDTransaccion)

        CSistema.SetSQLParameter(param, "IDPuntoExpedicion", txtIDTimbrado.GetValue)
        CSistema.SetSQLParameter(param, "IDTipoComprobante", PuntoExpedicionRow("IDTipoComprobante").ToString)
        CSistema.SetSQLParameter(param, "NroComprobante", txtNroComprobante.GetValue)
        CSistema.SetSQLParameter(param, "Comprobante", PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue)
        CSistema.SetSQLParameter(param, "IDCliente", txtCliente.Registro("ID").ToString)
        'Clientes ERSA
        CSistema.SetSQLParameter(param, "RazonSocial", txtCliente.txtRazonSocial.txt.Text)
        CSistema.SetSQLParameter(param, "RUC", txtCliente.txtRUC.txt.Text)

        CSistema.SetSQLParameter(param, "Direccion", txtDireccion.txt.Text)
        CSistema.SetSQLParameter(param, "Telefono", txtTelefono.txt.Text)
        CSistema.SetSQLParameter(param, "FechaEmision", CSistema.FormatoFechaBaseDatos(txtFechaVenta.GetValue, True, False))
        CSistema.SetSQLParameter(param, "IDSucursal", PuntoExpedicionRow("IDSucursal").ToString)
        CSistema.SetSQLParameter(param, "IDDeposito", cbxDeposito2.GetValue)
        CSistema.SetSQLParameter(param, "IDVendedor", cbxVendedor.GetValue)

        'Si es por sucursal
        'If txtCliente.SucursalSeleccionada = True Then
        '    CSistema.SetSQLParameter(param, "IDSucursalCliente", txtCliente.Sucursal("ID").ToString)
        '    CSistema.SetSQLParameter(param, "EsVentaSucursal", "True")
        'End If
        Try
            If Not txtCliente.Sucursal Is Nothing Then
                If txtCliente.Sucursal("ID").ToString <> "" Then
                    CSistema.SetSQLParameter(param, "IDSucursalCliente", txtCliente.Sucursal("ID").ToString)
                    CSistema.SetSQLParameter(param, "EsVentaSucursal", "True")
                End If
            End If
        Catch ex As Exception

        End Try


        'Credito
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            CSistema.SetSQLParameter(param, "Credito", "False")
        Else
            CSistema.SetSQLParameter(param, "Credito", "True")
            CSistema.SetSQLParameter(param, "FechaVencimiento", CSistema.FormatoFechaBaseDatos(txtVencimiento.GetValue, True, False))
        End If

        'CSistema.SetSQLParameter(param, "Cancelado", "False")

        'Otros
        CSistema.SetSQLParameter(param, "IDListaPrecio", cbxListaPrecio2.GetValue)
        CSistema.SetSQLParameter(param, "Descuento", txtDescuento.txt.Text)

        'Moneda
        CSistema.SetSQLParameter(param, "IDMoneda", OcxCotizacion1.Registro("ID"))

        CSistema.SetSQLParameter(param, "Cotizacion", CSistema.FormatoNumeroBaseDatos(OcxCotizacion1.Registro("Cotizacion"), True))


        CSistema.SetSQLParameter(param, "Observacion", txtDetalle.txt.Text.Trim)
        CSistema.SetSQLParameter(param, "NroComprobanteRemision", txtTipoEntrega.txt.Text.Trim)

        'Totales
        CSistema.SetSQLParameter(param, "Total", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total").ToString, vDecimalOperacion))
        CSistema.SetSQLParameter(param, "TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalImpuesto").ToString, vDecimalOperacion))
        CSistema.SetSQLParameter(param, "TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado").ToString, vDecimalOperacion))
        CSistema.SetSQLParameter(param, "TotalDescuento", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDescuento"), vDecimalOperacion))


        If vCancelarAutomatico = False Then
            CSistema.SetSQLParameter(param, "Saldo", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total"), vDecimalOperacion))
            CSistema.SetSQLParameter(param, "Cancelado", "False")
        Else
            CSistema.SetSQLParameter(param, "Saldo", 0)
            CSistema.SetSQLParameter(param, "Cancelado", "True")
        End If

        'Procesado
        CSistema.SetSQLParameter(param, "Procesado", "False")

        'Forma Pago Factura
        CSistema.SetSQLParameter(param, "IdFormaPagoFactura", cbxFormaPagoFactura.GetValue)

        InsertVenta = CSistema.InsertSQL(param, "Venta")

    End Function

    Function InsertDetalle() As String

        InsertDetalle = ""

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "IDTransaccion", NuevoIDTransaccion)
            CSistema.SetSQLParameter(param, "IDProducto", oRow("IDProducto").ToString)
            CSistema.SetSQLParameter(param, "ID", oRow("ID").ToString)
            CSistema.SetSQLParameter(param, "IDDeposito", oRow("IDDeposito").ToString)
            CSistema.SetSQLParameter(param, "Observacion", oRow("Observacion").ToString)
            CSistema.SetSQLParameter(param, "IDImpuesto", oRow("IDImpuesto").ToString)
            CSistema.SetSQLParameter(param, "Cantidad", CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True))
            CSistema.SetSQLParameter(param, "PrecioUnitario", CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString, vDecimalOperacion))

            CSistema.SetSQLParameter(param, "Total", CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, vDecimalOperacion))
            CSistema.SetSQLParameter(param, "TotalImpuesto", CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, vDecimalOperacion))
            CSistema.SetSQLParameter(param, "TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, vDecimalOperacion))

            'Descuentos
            CSistema.SetSQLParameter(param, "PorcentajeDescuento", CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento"), True))
            CSistema.SetSQLParameter(param, "DescuentoUnitario", CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString, vDecimalOperacion))
            CSistema.SetSQLParameter(param, "DescuentoUnitarioDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitarioDiscriminado").ToString, vDecimalOperacion))
            CSistema.SetSQLParameter(param, "TotalDescuento", CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString, vDecimalOperacion))
            CSistema.SetSQLParameter(param, "TotalDescuentoDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuentoDiscriminado").ToString, vDecimalOperacion))

            'Costo
            CSistema.SetSQLParameter(param, "CostoUnitario", CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString, vDecimalOperacion))
            CSistema.SetSQLParameter(param, "TotalCosto", CSistema.FormatoMonedaBaseDatos(oRow("TotalCosto").ToString, vDecimalOperacion))
            CSistema.SetSQLParameter(param, "TotalCostoImpuesto", CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString, vDecimalOperacion))
            CSistema.SetSQLParameter(param, "TotalCostoDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoDiscriminado").ToString, vDecimalOperacion))

            CSistema.SetSQLParameter(param, "Caja", CSistema.FormatoNumeroBaseDatos(oRow("Caja").ToString))
            CSistema.SetSQLParameter(param, "CantidadCaja", CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString))

            InsertDetalle = InsertDetalle & CSistema.InsertSQL(param, "DetalleVenta") & vbCrLf

        Next

    End Function

    Function InsertDescuento() As String

        InsertDescuento = ""
        Dim i As Integer
        For Each oRow As DataRow In dtDescuento.Rows
            Dim param As New DataTable
            CSistema.SetSQLParameter(param, "IDTransaccion", NuevoIDTransaccion)
            CSistema.SetSQLParameter(param, "IDProducto", oRow("IDProducto"))
            CSistema.SetSQLParameter(param, "ID", oRow("ID"))
            CSistema.SetSQLParameter(param, "IDDescuento", i)
            CSistema.SetSQLParameter(param, "IDDeposito", oRow("IDDeposito"))
            CSistema.SetSQLParameter(param, "IDTipoDescuento", oRow("IDTipoDescuento"))
            CSistema.SetSQLParameter(param, "IDActividad", oRow("IDActividad").ToString)
            CSistema.SetSQLParameter(param, "Descuento", CSistema.FormatoMonedaBaseDatos(oRow("Descuento"), vDecimalOperacion))
            CSistema.SetSQLParameter(param, "Porcentaje", CSistema.FormatoNumeroBaseDatos(oRow("Porcentaje"), True))
            CSistema.SetSQLParameter(param, "DescuentoDiscriminado", CSistema.FormatoNumeroBaseDatos(oRow("DescuentoDiscriminado"), vDecimalOperacion))

            InsertDescuento = InsertDescuento & CSistema.InsertSQL(param, "Descuento") & vbCrLf

            i = i + 1

        Next

    End Function

    Function InsertarDetalle2(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle2 = False

        Dim SQL As String = ""

        'Insertar Detalle
        For Each oRow As DataRow In dtDetalle.Rows
            SQL = SQL & "" & vbCrLf
        Next

        'Insertar Descuentos
        For Each oRow As DataRow In dtDescuento.Rows
            SQL = SQL & "" & vbCrLf
        Next

        'Insertar Impuestos
        For Each oRow As DataRow In CDetalleImpuesto.dt.Rows
            SQL = SQL & "" & vbCrLf
        Next

    End Function

    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", oRow("IDDeposito").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@Cantidad", oRow("Cantidad").ToString, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@PrecioUnitario", oRow("PrecioUnitario").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@PrecioUnitario", CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString, True), ParameterDirection.Input)

            'CSistema.SetSQLParameter(param, "@Total", oRow("Total").ToString, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@TotalImpuesto", oRow("TotalImpuesto").ToString, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@TotalDiscriminado", oRow("TotalDiscriminado").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoNumeroBaseDatos(oRow("Total").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, True), ParameterDirection.Input)

            'Descuentos
            'CSistema.SetSQLParameter(param, "@DescuentoUnitario", oRow("DescuentoUnitario").ToString, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@PorcentajeDescuento", oRow("PorcentajeDescuento").ToString, ParameterDirection.Input)
            'CSistema.SetSQLParameter(param, "@TotalDescuento", oRow("TotalDescuento").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@DescuentoUnitario", CSistema.FormatoNumeroBaseDatos(oRow("DescuentoUnitario").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@PorcentajeDescuento", CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDescuento", CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Caja", oRow("Caja").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@CantidadCaja", CSistema.FormatoMonedaBaseDatos(oRow("CantidadCaja").ToString, False), ParameterDirection.Input)

            'CSistema.SetSQLParameter(param, "@CantidadCaja", oRow("CantidadCaja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleVenta", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Return False
            End If

        Next

    End Function

    Sub CargarProducto()

        'Validar
        If ValidarProducto() = False Then
            Exit Sub
        End If

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False
        Dim UnidadConvertir As Decimal = 0

        If cbxUnidad.Text = "UNIDAD" Then
            CantidadProducto = txtCantidad.ObtenerValor
            EsCaja = False
        Else
            CantidadCaja = txtCantidad.ObtenerValor

            If IsNumeric(txtProducto.Registro("UnidadPorCaja").ToString) = False Then
                If CDec(txtProducto.Registro("UnidadPorCaja").ToString) <= 0 Then
                    CantidadProducto = CantidadCaja
                Else
                    CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
                End If
            Else
                CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
            End If

            EsCaja = True
        End If

        ' Obtener unidad de medida a Convertir
        If IsNumeric(txtProducto.Registro("UnidadConvertir").ToString) = False Then
            UnidadConvertir = 1
        Else
            UnidadConvertir = txtProducto.Registro("UnidadConvertir").ToString
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()
        Dim PrecioUnitario As Decimal = 0
        Dim Total As Decimal = 0

        If vIDTipoProducto <> 0 Then
            If vIDTipoProducto <> txtProducto.Registro("IDTipoProducto").ToString Then
                MessageBox.Show("Agregue solo " & vTipoProducto, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        End If
        'Controlar que no se venta producto con costo Cero
        If txtProducto.Registro("Costo").ToString = 0 And txtProducto.Registro("ControlarExistencia") = True Then
            MessageBox.Show("El producto no tiene costo asignado.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'Obtener Valores
        If txtProducto.Registro("ControlarExistencia") = True Then
            PrecioUnitario = CDec(txtProducto.Registro("Precio").ToString)
        Else
            PrecioUnitario = txtPrecioUnitario.ObtenerValor
        End If

        Total = PrecioUnitario * CantidadProducto

        'Productos
        dRow("IDTransaccion") = NuevoIDTransaccion
        dRow("IDProducto") = txtProducto.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("Producto") = txtProducto.Registro("Descripcion").ToString
        If txtObservacionProducto.txt.Text.Trim.Length = 0 Then
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
        Else
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString & " - " & txtObservacionProducto.txt.Text
        End If
        dRow("Referencia") = txtProducto.Registro("Ref").ToString
        dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString
        dRow("Observacion") = txtObservacionProducto.txt.Text

        'Deposito
        dRow("IDDeposito") = cbxDeposito2.cbx.SelectedValue
        dRow("Deposito") = cbxDeposito2.cbx.Text

        'Descuento
        Dim Precios As DataRow = GetPrecios(txtProducto.Registro("IDImpuesto").ToString)

        'Cargar descuentos
        If CargarDescuento(dRow("IDProducto"), dRow("ID"), dRow("IDDeposito"), cbxSucursal.GetValue, Precios, txtProducto.Registro("IDImpuesto").ToString) = False Then
            Exit Sub
        End If

        dRow("PorcentajeDescuento") = Precios("PorcentajeDescuento")
        dRow("DescuentoUnitario") = Precios("DescuentoUnitario")
        dRow("DescuentoUnitarioDiscriminado") = Precios("DescuentoUnitarioDiscriminado")
        dRow("TotalDescuento") = Precios("TotalDescuento")
        dRow("TotalDescuentoDiscriminado") = Precios("TotalDescuentoDiscriminado")

        'Cantidad y Precios
        dRow("UnidadMedidas") = txtProducto.Registro("UnidadMedida")
        dRow("Cantidad") = CantidadProducto
        dRow("UnidadMedidaConvertir") = txtProducto.Registro("UnidadMedidaConvertir")
        dRow("UnidadConvertir") = UnidadConvertir

        If txtProducto.Registro("ControlarExistencia") = True Then
            dRow("PrecioUnitario") = Precios("Precio")
            dRow("Pre. Uni.") = Precios("Precio")
            dRow("Total") = Precios("TotalSinDescuento")
        Else
            dRow("PrecioUnitario") = PrecioUnitario
            dRow("Pre. Uni.") = PrecioUnitario
            dRow("Total") = Total
        End If

        'Impuestos
        dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
        dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString
        dRow("Ref Imp") = txtProducto.Registro("Ref Imp").ToString
        dRow("Exento") = txtProducto.Registro("Exento")
        dRow("TotalImpuesto") = 0
        dRow("TotalDiscriminado") = 0
        dRow("TotalExentoInmueble") = 0
        If CBool(txtProducto.Registro("Inmueble")) = False Then
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), True, False, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))
        Else
            dRow("TotalExentoInmueble") = CDec(dRow("Total").ToString) * 0.3
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString) - CDec(dRow("TotalExentoInmueble").ToString), True, False, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))
        End If

        'Costo

        Dim vCotizacionMonedaCosto As Decimal
        'CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")

        If OcxCotizacion1.Registro("ID") = txtProducto.Registro("IDMoneda").ToString Then
            dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString
        ElseIf (OcxCotizacion1.Registro("ID") = 1) And (txtProducto.Registro("IDMoneda").ToString <> 1) Then 'FACT GS - Costo otros
            vCotizacionMonedaCosto = CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")
            dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString * vCotizacionMonedaCosto
        ElseIf (OcxCotizacion1.Registro("ID") <> 1) And (txtProducto.Registro("IDMoneda").ToString <> 1) And (OcxCotizacion1.Registro("ID") <> txtProducto.Registro("IDMoneda").ToString) Then 'FACT US - Costo otros
            vCotizacionMonedaCosto = CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")
            dRow("CostoUnitario") = (txtProducto.Registro("Costo").ToString * vCotizacionMonedaCosto) / OcxCotizacion1.Registro("Cotizacion")
        ElseIf (OcxCotizacion1.Registro("ID") <> 1) And (txtProducto.Registro("IDMoneda").ToString = 1) Then 'FACT US - Costo GS
            dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString / OcxCotizacion1.Registro("Cotizacion")
        End If


        'dRow("TotalCosto") = CantidadProducto * txtProducto.Registro("Costo").ToString
        'dRow("TotalCostoImpuesto") = 0
        'dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
        'CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), True, False, 0, dRow("TotalCostoImpuesto"))

        dRow("TotalCosto") = CantidadProducto * dRow("CostoUnitario")
        dRow("TotalCostoImpuesto") = 0
        dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), True, False, 0, dRow("TotalCostoImpuesto"))

        'Totales
        dRow("TotalBruto") = 0
        dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
        dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
        dRow("TotalNeto") = 0
        dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + Precios("TotalDescuentoDiscriminado")

        'Bloqueo de Tipo de Producto ERSA
        vIDTipoProducto = txtProducto.Registro("IDTipoProducto").ToString
        vTipoProducto = txtProducto.Registro("TipoProducto").ToString

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalle()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtImporte.txt.Text = "0"
        txtPrecioUnitario.txt.Text = "0"
        txtProducto.txt.txt.Clear()
        txtDescuentoProducto.SetValue(0)

        'Retorno de Foco
        txtProducto.txt.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub
    Sub CargarProductoFacturaExportacion()

        'Validar
        If ValidarProducto() = False Then
            Exit Sub
        End If

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False
        Dim UnidadConvertir As Decimal = 0
        Dim vIDImpuesto As Integer = 3

        If cbxUnidad.Text = "UNIDAD" Then
            CantidadProducto = txtCantidad.ObtenerValor
            EsCaja = False
        Else
            CantidadCaja = txtCantidad.ObtenerValor

            If IsNumeric(txtProducto.Registro("UnidadPorCaja").ToString) = False Then
                If CDec(txtProducto.Registro("UnidadPorCaja").ToString) <= 0 Then
                    CantidadProducto = CantidadCaja
                Else
                    CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
                End If
            Else
                CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
            End If

            EsCaja = True
        End If

        ' Obtener unidad de medida a Convertir
        If IsNumeric(txtProducto.Registro("UnidadConvertir").ToString) = False Then
            UnidadConvertir = 1
        Else
            UnidadConvertir = txtProducto.Registro("UnidadConvertir").ToString
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()
        Dim PrecioUnitario As Decimal = 0
        Dim Total As Decimal = 0

        If vIDTipoProducto <> 0 Then
            If vIDTipoProducto <> txtProducto.Registro("IDTipoProducto").ToString Then
                MessageBox.Show("Agregue solo " & vTipoProducto, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        End If

        'Obtener Valores
        If txtProducto.Registro("ControlarExistencia") = True Then
            PrecioUnitario = CDec(txtProducto.Registro("Precio").ToString)
        Else
            PrecioUnitario = txtPrecioUnitario.ObtenerValor
        End If

        Total = PrecioUnitario * CantidadProducto

        'Productos
        dRow("IDTransaccion") = NuevoIDTransaccion
        dRow("IDProducto") = txtProducto.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("Producto") = txtProducto.Registro("Descripcion").ToString
        If txtObservacionProducto.txt.Text.Trim.Length = 0 Then
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
        Else
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString & " - " & txtObservacionProducto.txt.Text
        End If
        dRow("Referencia") = txtProducto.Registro("Ref").ToString
        dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString
        dRow("Observacion") = txtObservacionProducto.txt.Text

        'Deposito
        dRow("IDDeposito") = cbxDeposito2.cbx.SelectedValue
        dRow("Deposito") = cbxDeposito2.cbx.Text

        'Descuento
        Dim Precios As DataRow = GetPrecios(vIDImpuesto)

        'Cargar descuentos
        If CargarDescuento(dRow("IDProducto"), dRow("ID"), dRow("IDDeposito"), cbxSucursal.GetValue, Precios, vIDImpuesto) = False Then
            Exit Sub
        End If

        dRow("PorcentajeDescuento") = Precios("PorcentajeDescuento")
        dRow("DescuentoUnitario") = Precios("DescuentoUnitario")
        dRow("DescuentoUnitarioDiscriminado") = Precios("DescuentoUnitarioDiscriminado")
        dRow("TotalDescuento") = Precios("TotalDescuento")
        dRow("TotalDescuentoDiscriminado") = Precios("TotalDescuentoDiscriminado")

        'Cantidad y Precios
        dRow("UnidadMedidas") = txtProducto.Registro("UnidadMedida")
        dRow("Cantidad") = CantidadProducto
        dRow("UnidadMedidaConvertir") = txtProducto.Registro("UnidadMedidaConvertir")
        dRow("UnidadConvertir") = UnidadConvertir

        If txtProducto.Registro("ControlarExistencia") = True Then
            dRow("PrecioUnitario") = Precios("Precio")
            dRow("Pre. Uni.") = Precios("Precio")
            dRow("Total") = Precios("TotalSinDescuento")
        Else
            dRow("PrecioUnitario") = PrecioUnitario
            dRow("Pre. Uni.") = PrecioUnitario
            dRow("Total") = Total
        End If

        'Impuestos
        dRow("IDImpuesto") = vIDImpuesto
        dRow("Impuesto") = "Exento"
        dRow("Ref Imp") = "EX"
        dRow("Exento") = True
        dRow("TotalImpuesto") = 0
        dRow("TotalDiscriminado") = 0
        CSistema.CalcularIVA(vIDImpuesto, CDec(dRow("Total").ToString), True, False, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

        'Costo

        Dim vCotizacionMonedaCosto As Decimal
        'CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")

        If OcxCotizacion1.Registro("ID") = txtProducto.Registro("IDMoneda").ToString Then
            dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString
        ElseIf (OcxCotizacion1.Registro("ID") = 1) And (txtProducto.Registro("IDMoneda").ToString <> 1) Then 'FACT GS - Costo otros
            vCotizacionMonedaCosto = CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")
            dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString * vCotizacionMonedaCosto
        ElseIf (OcxCotizacion1.Registro("ID") <> 1) And (txtProducto.Registro("IDMoneda").ToString <> 1) And (OcxCotizacion1.Registro("ID") <> txtProducto.Registro("IDMoneda").ToString) Then 'FACT US - Costo otros
            vCotizacionMonedaCosto = CSistema.ExecuteScalar("Select isnull(cotizacion,0) from vCotizacion where ID = (Select max(ID) from Cotizacion where IDMoneda = " & txtProducto.Registro("IDMoneda").ToString & ")")
            dRow("CostoUnitario") = (txtProducto.Registro("Costo").ToString * vCotizacionMonedaCosto) / OcxCotizacion1.Registro("Cotizacion")
        ElseIf (OcxCotizacion1.Registro("ID") <> 1) And (txtProducto.Registro("IDMoneda").ToString = 1) Then 'FACT US - Costo GS
            dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString / OcxCotizacion1.Registro("Cotizacion")
        End If


        'dRow("TotalCosto") = CantidadProducto * txtProducto.Registro("Costo").ToString
        'dRow("TotalCostoImpuesto") = 0
        'dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
        'CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), True, False, 0, dRow("TotalCostoImpuesto"))

        dRow("TotalCosto") = CantidadProducto * dRow("CostoUnitario")
        dRow("TotalCostoImpuesto") = 0
        dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
        CSistema.CalcularIVA(vIDImpuesto, CDec(dRow("TotalCosto").ToString), True, False, 0, dRow("TotalCostoImpuesto"))

        'Totales
        dRow("TotalBruto") = 0
        dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
        dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
        dRow("TotalNeto") = 0
        dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + Precios("TotalDescuentoDiscriminado")

        'Bloqueo de Tipo de Producto ERSA
        vIDTipoProducto = txtProducto.Registro("IDTipoProducto").ToString
        vTipoProducto = txtProducto.Registro("TipoProducto").ToString

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalle()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtImporte.txt.Text = "0"
        txtPrecioUnitario.txt.Text = "0"
        txtProducto.txt.txt.Clear()
        txtDescuentoProducto.SetValue(0)

        'Retorno de Foco
        txtProducto.txt.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Function ValidarProducto() As Boolean

        ValidarProducto = False

        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito2.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If


        If cbxDeposito2.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Descuento acumulado
        Dim DescuentoAcumulado As Decimal = CSistema.dtSumColumn(txtProducto.Precios, "Porcentaje")
        If DescuentoAcumulado > 100 Then
            Dim mensaje As String = "El Porcentaje de descuento total no puede ser mayor a 100%!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If CDec(txtImporte.ObtenerValor) <= 0 Then
            Dim mensaje As String = "   El importe no es correcto!"
            ctrError.SetError(txtImporte, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If


        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False
        Dim UnidadConvertir As Decimal = 0

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        If cbxUnidad.Text = "UNIDAD" Then
            CantidadProducto = txtCantidad.ObtenerValor
            EsCaja = False
        Else
            CantidadCaja = txtCantidad.ObtenerValor

            If IsNumeric(txtProducto.Registro("UnidadPorCaja").ToString) = False Then
                If CDec(txtProducto.Registro("UnidadPorCaja").ToString) <= 0 Then
                    CantidadProducto = CantidadCaja
                Else
                    CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
                End If
            Else
                CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
            End If

            EsCaja = True
        End If

        ' Obtener unidad de medida a Convertir
        If IsNumeric(txtProducto.Registro("UnidadConvertir").ToString) = False Then
            UnidadConvertir = 1
        Else
            UnidadConvertir = txtProducto.Registro("UnidadConvertir").ToString
        End If

        'Verificar si se se exige el control de existencia en deposito
        If vgConfiguraciones("VentaPermitirCantidadNegativa") Is Nothing Then
            vgConfiguraciones("VentaPermitirCantidadNegativa") = True
        End If

        If vgConfiguraciones("VentaAdvertirCantidadNegativa") Is Nothing Then
            vgConfiguraciones("VentaAdvertirCantidadNegativa") = True
        End If

        'Obtenemos la cantidad que existe en el deposito el producto seleccionado
        Dim ExistenciaActual As Decimal = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & txtProducto.Registro("ID").ToString & ", " & cbxDeposito2.cbx.SelectedValue & ")")
        Dim CantidadTemp As Decimal = CantidadProducto

        'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
        For Each oRow As DataRow In dtDetalle.Rows
            If oRow("IDProducto").ToString = txtProducto.Registro("ID").ToString Then
                CantidadTemp = CantidadTemp + CDec(oRow("Cantidad").ToString)
            End If
        Next

        'If txtProducto.Registro("ControlarExistencia") = True Then
        'If CantidadTemp > ExistenciaActual Then
        'Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
        'ctrError.SetError(txtCantidad, mensaje)
        'ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
        'tsslEstado.Text = mensaje
        'Exit Function
        'End If
        'End If

        If txtProducto.Registro("ControlarExistencia") = True Then
            If CantidadTemp > ExistenciaActual Then
                If vgConfiguraciones("VentaPermitirCantidadNegativa") = False Then
                    Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                Else
                    If vgConfiguraciones("VentaAdvertirCantidadNegativa") = True Then
                        If MessageBox.Show("Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s) Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If


        'Limite de lineas en el detalle
        Dim MaximoLineas As String = vgConfiguraciones("PedidoCantidadProducto").ToString
        If IsNumeric(MaximoLineas) = False Then
            MaximoLineas = 17
        End If

        If lvLista.Items.Count >= CInt(MaximoLineas) Then
            MessageBox.Show("Se ha llegado al limite de lineas del detalle del comprobante cierre esta operacion.!", "Limite de lineas.!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Function
        End If



        Try
            Dim Costo As Decimal = txtProducto.Registro("Costo")
            Dim Precio As Decimal = txtProducto.Registro("Precio")
            Dim Margen As Decimal = Precio - Costo

            'Si debe controlar el margen bajo el costo
            If vgConfiguraciones("VentaControlarBajoElCosto") = True Then

                If Precio < Costo Then
                    MessageBox.Show("El precio no puede ser menor al costo!", "Atencion!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    txtPrecioUnitario.Focus()
                    Exit Function
                End If

            End If

            'Validar Margen y el precio se puede modificar
            If vgConfiguraciones("VentaModificarPrecio") = True Then

                'Si debe controlar el margen minimo
                If vgConfiguraciones("VentaControlarBajoElMargen") = False Then
                    Exit Try
                End If

                Dim MargenMinimo As Decimal = txtProducto.Registro("MargenMinimo")
                Dim MargenSobreCosto As Decimal = 0
                Dim MargenSobreVenta As Decimal = 0

                Dim Utilidad As Decimal = Precio - Costo

                'Solo si el margen es positivo
                If MargenMinimo <= 0 Then
                    Exit Try
                End If

                'Si el costo es 0, entonces el margen es superado, salir del control
                If Costo = 0 Then
                    Exit Try
                End If

                Margen = Utilidad / Precio

                MargenSobreCosto = (Utilidad / Costo) * 100
                MargenSobreVenta = (Utilidad / Precio) * 100

                Margen = Margen * 100

                'Comparacion
                If Margen < MargenMinimo Then
                    MessageBox.Show("El precio no supera el margen minimo de venta establecido! Aumente el precio.", "Margen!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    txtPrecioUnitario.Focus()
                    Exit Function
                End If

            End If

        Catch ex As Exception

        End Try


        ValidarProducto = True

    End Function

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If lvLista.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Eliminamos los descuentos
        EliminarDescuentos()

        For Each item As ListViewItem In lvLista.SelectedItems
            dtDetalle.Rows.RemoveAt(item.Index)
            Exit For
        Next
        If dtDetalle.Rows.Count = 0 Then
            vTipoProducto = 0
        End If
        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows

            If Not oRow.RowState = DataRowState.Deleted Then

                'Renumerar descuentos
                For Each dRow As DataRow In dtDescuento.Select(" IDProducto=" & oRow("IDProducto") & " And ID=" & oRow("ID") & " ")
                    dRow("ID") = Index
                Next

                oRow("ID") = Index
                Index = Index + 1
            End If

        Next

        ListarDetalle()

    End Sub

    Sub EliminarDescuentos()

        For Each item As ListViewItem In lvLista.SelectedItems
            Dim IDProducto As Integer = item.Text
            Dim ID As Integer = item.Index

            For i As Integer = dtDescuento.Rows.Count - 1 To 0 Step -1
                Dim oRow As DataRow = dtDescuento.Rows(i)
                If oRow("IDProducto") = IDProducto And oRow("ID") = ID Then
                    dtDescuento.Rows.RemoveAt(i)
                End If
            Next

        Next

    End Sub

    Sub ModificarDescuentoDetalle()

        'Validar
        If vgConfiguraciones("VentaModificarDescuento") = False Then
            MessageBox.Show("El sistema no esta configurado para cambiar el descuento!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If


        Dim frm As New frmModificarDetalleDescuentoVenta

        frm.dt = CData.FiltrarDataTable(dtDetalle, "ID=" & lvLista.SelectedItems(0).Index)
        Dim oRow As DataRow = frm.dt.Rows(0)
        frm.dtDescuento = CData.FiltrarDataTable(dtDescuento, " IDProducto=" & oRow("IDProducto") & " And ID=" & oRow("ID") & " ")

        FGMostrarFormulario(Me, frm, "Modificar Descuentos del Producto", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        If frm.Superado = True Then
            Superado = frm.Superado
            btnHabilitar.Visible = True
            btnGuardar.Enabled = False
        End If

        For Each oRowActualizar As DataRow In frm.dt.Rows
            For Each oRowDetalle As DataRow In dtDetalle.Select("ID=" & lvLista.SelectedItems(0).Index)
                For c As Integer = 0 To dtDetalle.Columns.Count - 1
                    oRowDetalle(c) = oRowActualizar(c)
                Next
            Next
        Next

        For Each oRowActualizar As DataRow In frm.dtDescuento.Rows
            For Each oRowDetalle As DataRow In dtDescuento.Select(" IDProducto=" & oRow("IDProducto") & " And ID=" & oRow("ID") & " ")
                For c As Integer = 0 To dtDescuento.Columns.Count - 1
                    oRowDetalle(c) = oRowActualizar(c)
                Next
            Next
        Next

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        lvLista.Items.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvLista.Items.Add(oRow("IDProducto").ToString)

            Dim Descripcion As String


            If chkImprimirSoloObservacion.Valor = True Then
                Descripcion = oRow("Observacion").ToString.Trim
            Else
                Descripcion = oRow("Descripcion").ToString.Trim

            End If
            item.SubItems.Add(oRow("Referencia").ToString)
            item.SubItems.Add(Descripcion)
            item.SubItems.Add(oRow("Ref Imp").ToString)


            If chkConversion.Valor = True Then
                Dim Cantidad As Decimal = oRow("Cantidad").ToString
                Cantidad = Cantidad * oRow("UnidadConvertir")
                Debug.Print(Cantidad.ToString)
                Debug.Print(CSistema.FormatoNumero(Cantidad.ToString, True))
                item.SubItems.Add(CSistema.FormatoNumero(Cantidad, True))
                item.SubItems.Add(oRow("UnidadMedidaConvertir").ToString)
            Else
                'item.SubItems.Add(CSistema.FormatoMoneda(oRow("Cantidad").ToString, vDecimalOperacion))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Cantidad").ToString, True))
                item.SubItems.Add(oRow("UnidadMedidas").ToString)
            End If

            item.SubItems.Add(CSistema.FormatoMoneda(oRow("PrecioUnitario").ToString, True))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("DescuentoUnitario").ToString, True))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("PorcentajeDescuento").ToString, True))
            If CBool(oRow("Exento").ToString) = True Then
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("total").ToString, vDecimalOperacion))
                item.SubItems.Add(CSistema.FormatoMoneda(0, vDecimalOperacion))
            Else
                item.SubItems.Add(CSistema.FormatoMoneda(0, vDecimalOperacion))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString, vDecimalOperacion))
            End If

        Next

        'Bloqueamos la cabecera si corresponde
        If dtDetalle.Rows.Count > 0 Then
            cbxDeposito2.SoloLectura = True
            txtCliente.SoloLectura = True
            OcxCotizacion1.SoloLectura = True
        Else
            cbxDeposito2.SoloLectura = False
            txtCliente.SoloLectura = False
            OcxCotizacion1.SoloLectura = False
        End If


        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, True, vDecimalOperacion)
        CalcularTotales()

    End Sub

    Function CargarDescuento(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal IDDeposito As Integer, ByVal IDSucursal As Integer, ByVal rowDescuento As DataRow, ByVal IDImpuesto As Integer) As Boolean

        CargarDescuento = False

        For i As Integer = 0 To rowDescuento.Table.Columns.Count - 1
            Debug.Print(rowDescuento.Table.Columns(i).ColumnName & ": " & rowDescuento(i))
        Next

        For i = 0 To 3

            Dim IDTipoDescuento As Integer = -2
            Dim TipoDescuento As String = ""
            Dim Descuento As Decimal = 0
            Dim Porcentaje As Decimal = 0

            Select Case i
                Case 0
                    IDTipoDescuento = 1
                    TipoDescuento = "TPR"
                    Descuento = CSistema.CalcularPorcentaje(rowDescuento("Precio"), rowDescuento(TipoDescuento), True, False)
                    Porcentaje = rowDescuento(TipoDescuento)
                Case 1
                    IDTipoDescuento = i
                    TipoDescuento = "Tactico"
                    Descuento = CSistema.CalcularPorcentaje(rowDescuento("Precio"), rowDescuento(TipoDescuento), True, False)
                    Porcentaje = rowDescuento(TipoDescuento)
                Case 2
                    IDTipoDescuento = i
                    TipoDescuento = "Express"
                    Descuento = CSistema.CalcularPorcentaje(rowDescuento("Precio"), rowDescuento(TipoDescuento), True, False)
                    Porcentaje = rowDescuento(TipoDescuento)
                Case 3
                    IDTipoDescuento = i
                    TipoDescuento = "Acuerdo"
                    Descuento = CSistema.CalcularPorcentaje(rowDescuento("Precio"), rowDescuento(TipoDescuento), True, False)
                    Porcentaje = rowDescuento(TipoDescuento)
            End Select

            'Descuento 
            Dim DescuentoDiscriminado As Decimal = 0
            Dim TotalDescuentoDiscriminado As Decimal = 0
            CSistema.CalcularIVA(IDImpuesto, Descuento, True, False, DescuentoDiscriminado)
            TotalDescuentoDiscriminado = txtCantidad.ObtenerValor * DescuentoDiscriminado

            'Si no es tactico, agregamos normalmente
            Select Case IDTipoDescuento

                'Tactico o Acuerdo
                Case 1, 3

                    'Verificar primero si se tiene asignado en la ficha del producto
                    'Descuentos TACTICOS y ACURDOS
                    If Porcentaje = 0 Then
                        GoTo siguiente
                    End If

                    'Si no es descuento tactico
                    Dim dtActividad As DataTable = CData.GetTable("VDetalleActividad").Copy
                    dtActividad = CData.FiltrarDataTable(dtActividad, " IDProducto=" & IDProducto).Copy()

                    'Si es que hay un solo tactico, le ponemos todo
                    If dtActividad.Rows.Count = 1 Then
                        Dim rowTactico As DataRow = dtDescuento.NewRow
                        rowTactico("IDTransaccion") = 0
                        rowTactico("IDProducto") = IDProducto
                        rowTactico("ID") = ID
                        rowTactico("IDDeposito") = IDDeposito
                        rowTactico("IDTipoDescuento") = IDTipoDescuento
                        rowTactico("Tipo") = TipoDescuento
                        rowTactico("Descuento") = Descuento
                        rowTactico("Porcentaje") = Porcentaje
                        rowTactico("IDActividad") = dtActividad.Rows(0)("IDActividad")
                        rowTactico("Actividad") = dtActividad.Rows(0)("Codigo")
                        rowTactico("Cantidad") = txtCantidad.ObtenerValor
                        rowTactico("Total") = txtCantidad.ObtenerValor * Descuento
                        rowTactico("DescuentoDiscriminado") = DescuentoDiscriminado
                        rowTactico("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

                        If Descuento > 0 Then
                            dtDescuento.Rows.Add(rowTactico)
                        End If


                    End If


                    If dtActividad.Rows.Count > 1 Then

                        'Llamamos al sistema de descuentos
                        Dim frm As New frmConfigurarDescuentosDeActividades
                        frm.dtActividad = dtActividad
                        frm.TipoDescuento = TipoDescuento
                        frm.rowDescuento = rowDescuento
                        frm.IDProducto = txtProducto.Registro("ID")
                        frm.IDSucursal = cbxSucursal.GetValue
                        frm.IDDeposito = cbxDeposito2.GetValue

                        frm.ShowDialog(Me)
                        If frm.Seleccionado = False Then
                            Return False
                        End If

                        For Each oRow As DataRow In frm.dtActividad.Select(" Sel = 'True' ")

                            Dim rowTactico As DataRow = dtDescuento.NewRow
                            rowTactico("IDTransaccion") = 0
                            rowTactico("IDProducto") = IDProducto
                            rowTactico("ID") = ID
                            rowTactico("IDDeposito") = IDDeposito
                            rowTactico("IDTipoDescuento") = IDTipoDescuento
                            rowTactico("Tipo") = TipoDescuento

                            Descuento = CSistema.CalcularPorcentaje(rowDescuento("Precio"), oRow("DescuentoMaximo"), True, False)
                            Porcentaje = oRow("DescuentoMaximo")

                            'Calcular Descuento Unitario
                            CSistema.CalcularIVA(IDImpuesto, Descuento, True, False, DescuentoDiscriminado)
                            TotalDescuentoDiscriminado = txtCantidad.ObtenerValor * DescuentoDiscriminado

                            rowTactico("Descuento") = Descuento
                            rowTactico("Porcentaje") = Porcentaje
                            rowTactico("Actividad") = oRow("Mecanica")
                            rowTactico("IDActividad") = oRow("IDActividad")
                            rowTactico("Cantidad") = txtCantidad.ObtenerValor
                            rowTactico("Total") = txtCantidad.ObtenerValor * Descuento
                            rowTactico("DescuentoDiscriminado") = DescuentoDiscriminado
                            rowTactico("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

                            If Descuento > 0 Then
                                dtDescuento.Rows.Add(rowTactico)
                            End If

                        Next

                    End If

                Case Else

                    Dim NewRow As DataRow = dtDescuento.NewRow

                    NewRow("IDTransaccion") = 0
                    NewRow("IDProducto") = IDProducto
                    NewRow("ID") = ID
                    NewRow("IDDeposito") = IDDeposito
                    NewRow("IDTipoDescuento") = IDTipoDescuento
                    NewRow("Tipo") = TipoDescuento
                    NewRow("Descuento") = Descuento
                    NewRow("Porcentaje") = Porcentaje
                    NewRow("Cantidad") = txtCantidad.ObtenerValor
                    NewRow("Total") = txtCantidad.ObtenerValor * Descuento
                    NewRow("DescuentoDiscriminado") = DescuentoDiscriminado
                    NewRow("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

                    If Descuento > 0 Then
                        dtDescuento.Rows.Add(NewRow)
                    End If

            End Select


siguiente:

        Next

        'Actualizar Precio
        rowDescuento("PorcentajeDescuento") = 0
        For Each oRowTemp As DataRow In dtDescuento.Select("ID=" & ID & " And IDProducto=" & IDProducto)
            rowDescuento("PorcentajeDescuento") = rowDescuento("PorcentajeDescuento") + oRowTemp("Porcentaje")
        Next


        Return True

    End Function

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub CalcularImporte(ByVal Decimales As Boolean, ByVal Redondear As Boolean, ByVal PorTotal As Boolean)

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Cantidad As Decimal
        Dim PrecioUnitario As Decimal
        Dim Importe As Decimal

        Cantidad = txtCantidad.ObtenerValor

        'Validar
        If Cantidad = 0 And txtCantidad.txt.Focused = False Then
            'Dim mensaje As String = "No se puede calcular los montos si la cantidad es 0!"
            'ctrError.SetError(lvLista, mensaje)
            'ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            'tsslEstado.Text = mensaje
            Exit Sub
        End If

        If PorTotal = True Then
            PrecioUnitario = txtImporte.ObtenerValor / Cantidad
        Else
            Dim Descuento As Decimal = CSistema.CalcularPorcentaje(txtPrecioUnitario.ObtenerValor, txtDescuentoProducto.ObtenerValor, True, True)
            Dim SubTotal As Decimal = 0
            SubTotal = txtPrecioUnitario.ObtenerValor - Descuento
            Importe = SubTotal * Cantidad
        End If

        If Decimales = False Then
            PrecioUnitario = CDec(Math.Round(PrecioUnitario, 0))
            Importe = CDec(Math.Round(Importe, 0))
        Else
            'If Redondear = True Then
            PrecioUnitario = CDec(Math.Round(PrecioUnitario, 4))
            Importe = CDec(Math.Round(Importe, 4))
            'End If
        End If

        If PorTotal = True Then
            txtPrecioUnitario.txt.Text = PrecioUnitario.ToString
        Else
            txtImporte.txt.Text = Importe.ToString
        End If

    End Sub

    Function ObtenerNroComprobante() As String

        Dim dt As DataTable = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion)

        If dt Is Nothing AndAlso dt.Rows.Count = 0 Then
            Return "0"
        End If

        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Return "0"
        End If

        If dt.Select("ID=" & txtIDTimbrado.GetValue).Count = 0 Then
            Return "0"
        End If

        Dim PuntoExpedicionRow As DataRow = dt.Select("ID=" & txtIDTimbrado.GetValue)(0)
        ObtenerNroComprobante = PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue

    End Function

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "Venta: " & cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante() & "  -  " & txtCliente.txtRazonSocial.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VVenta Where Comprobante='" & ObtenerNroComprobante() & "' And IDPuntoExpedicion=" & txtIDTimbrado.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtCliente.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(txtCliente, mensaje)
                ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "Venta: " & cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante() & "  -  " & txtCliente.txtRazonSocial.Text

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento
            frm.CAsiento.Bloquear = CAsiento.Bloquear

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()


        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFechaVenta.GetValue
        oRow("IDMoneda") = OcxCotizacion1.Registro("ID")
        oRow("Cotizacion") = OcxCotizacion1.Registro("Cotizacion")
        'oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue.ToString
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = ObtenerNroComprobante()
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante()
        oRow("Detalle") = txtDetalle.txt.Text
        oRow("Bloquear") = CAsiento.Bloquear
        oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        If CAsiento.Bloquear = False Then
            CAsiento.dtDetalleAsiento.Rows.Clear()
        End If

        CAsiento.IDCliente = txtCliente.Registro("ID").ToString

        If cbxCondicion.cbx.SelectedIndex = 0 Then
            CAsiento.Contado = True
        End If

        If cbxCondicion.cbx.SelectedIndex = 1 Then
            CAsiento.Credito = True
        End If

        CAsiento.Generar()

    End Sub

    Sub Eliminar()

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        Dim frm As New frmConsultaVenta
        frm.SoloConsulta = False
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.Size = New Size(frmPrincipal2.ClientSize.Width - 75, frmPrincipal2.ClientSize.Height - 75)
        frm.StartPosition = FormStartPosition.CenterScreen
        FGMostrarFormulario(Me, frm, "Seleccion de Comprobante", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()
        cbxVendedor.SoloLectura = True

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            Dim vComprobante As String = ObtenerNroComprobante()
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VVenta Where Comprobante = '" & vComprobante & "' And IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " And ReferenciaSucursal='" & txtReferenciaSucursal.GetValue & "' And ReferenciaPunto='" & txtReferenciaTerminal.GetValue & "'), 0 )")

            'Si no se encuentra, buscar el comprobante en otro timbrado 
            If IDTransaccion = 0 Then
                Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Select V.IDTransaccion, V.IDCiudad, V.Ciudad, V.IDPuntoExpedicion, V.PE, V.Comprobante, V.IDSucursal, V.Sucursal, V.Timbrado, V.IDTipoComprobante, V.[Cod.], V.ReferenciaSucursal, V.ReferenciaPunto, PE.TalonarioActual, PE.Vencimiento, PE.Saldo From VVenta V Join VPuntoExpedicion PE On V.IDPuntoExpedicion=PE.ID Where V.Comprobante='" & vComprobante & "' ")

                'No existe el comprobante
                Select Case dttemp.Rows.Count

                    Case 0 'No existe el nro de comprobante
                        IDTransaccion = 0
                    Case 1 'Se econtro 1 registro, seleccinar directamente el timbrado y extraer el IDTransaccion
                        MessageBox.Show("El nro de comprobante esta en un timbrado diferente al actual! El sistema cambiara automaticamente para visualizar el registro, pero ud. debera tener en cuenta que al querer generar un nuevo comprobante se hara en el ultimo timbrado seleccinado.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                        IDTransaccion = dttemp.Rows(0)("IDTransaccion").ToString
                        txtIDTimbrado.SetValue(dttemp.Rows(0)("IDPuntoExpedicion").ToString)
                        ObtenerInformacionPuntoVenta()
                    Case Is > 1 'Sacar un formulario si hay varios registros con el mismo nro de comprobante para seleccionar el timbrado deseado
                        MessageBox.Show("El nro de comprobante esta duplicado probablemente corresponde a varios timbrados. Seleccione correctamente el timbrado y continue.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                        IDTransaccion = 0
                End Select

            End If

        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VVenta Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleVenta Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy
        dtDescuento = CSistema.ExecuteToDataTable("Select * From VDescuento Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        'Punto de Expedicion
        txtTimbrado.SetValue(oRow("Timbrado").ToString)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.txt.Text = oRow("TipoComprobante").ToString
        txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
        txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
        txtNroComprobante.txt.Text = oRow("NroComprobante").ToString

        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtDireccion.txt.Text = oRow("Direccion").ToString
        txtTelefono.txt.Text = oRow("Telefono").ToString
        txtFechaVenta.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxCondicion.cbx.Text = oRow("Condicion").ToString
        vIDCliente = oRow("IDCliente").ToString

        'Razon Social y RUC de clientes varios
        txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        txtCliente.txtRUC.txt.Text = oRow("RUC").ToString


        'Condicion
        If CBool(oRow("Credito").ToString) = False Then
            txtVencimiento.txt.Clear()
            txtPlazo.txt.Clear()
            cbxCondicion.cbx.SelectedIndex = 0
        Else
            txtVencimiento.SetValue(CDate(oRow("FechaVencimiento").ToString))
            'calcular plazo
            'cbxCondicion.cbx.SelectedIndex = 1
        End If


        cbxDeposito2.Texto = oRow("Deposito").ToString
        If EsPedido = False Then
            cbxVendedor.Texto = oRow("Vendedor").ToString
        End If
        txtDetalle.txt.Text = oRow("Observacion").ToString
        cbxListaPrecio2.Texto = oRow("Lista de Precio").ToString
        txtDescuento.txt.Text = oRow("Descuento").ToString

        'Moneda
        OcxCotizacion1.cbxMoneda.txt.Text = oRow("Moneda")
        OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")
        OcxCotizacion1.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        OcxCotizacion1.cbxMoneda.txt.Text = oRow("Moneda")
        OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")

        cbxPromotor.txt.Text = oRow("Promotor").ToString
        txtTipoEntrega.txt.Text = oRow("TipoEntrega").ToString

        cbxFormaPagoFactura.txt.Text = oRow("Forma.Pago").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        'Habilitar y desHabilitar boton de impresion de recibos
        btnImprimirRecibo.Visible = False
        btnAnularRecibo.Visible = False
        If (oRow("DiasCredito").ToString = 1 Or oRow("FormaPago").ToString = "CH.DIFERIDO") Then
            vCredito24hs = True
            ManejarBotonesRecibo()
        End If
        If (CBool(oRow("Credito").ToString) = True) And (oRow("DiasCredito").ToString > 1) And (oRow("FormaPago").ToString <> "CH.DIFERIDO") Then
            vCredito24hs = False
            ManejarBotonesRecibo()
        End If

        If (CBool(oRow("Boleta").ToString) = True) And (CBool(oRow("Credito").ToString) = True) Then
            vBoletaContraBoleta = True
        End If


        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            btnImprimir.Enabled = False
            btnAsiento.Enabled = False
        Else
            flpAnuladoPor.Visible = False
        End If


        'Cargamos el detalle
        ListarDetalle()
        If dtDetalle.Rows.Count = 0 Then
            CDetalleImpuesto.EstablecerImpuestosDetalle(CSistema.ExecuteToDataTable("Select * From VDetalleImpuesto Where IDTransaccion=" & IDTransaccion))
        End If

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub ManejarBotonesRecibo()
        If CType(CSistema.ExecuteScalar("select count(*) from vReciboVenta where IDTransaccionVenta = " & IDTransaccion & "  and Anulado = 'False'"), Integer) = 0 Then
            btnImprimirRecibo.Visible = True
            btnAnularRecibo.Visible = False
            btnImprimirRecibo.Text = "Imprimir Recibo"
            ReciboVigente = False
            ReciboAnulado = True
        End If
        'Si tiene recibo - Poner Anular en boton
        If CType(CSistema.ExecuteScalar("select count(*) from vReciboVenta where IDTransaccionVenta = " & IDTransaccion & " and anulado = 'False' "), Integer) > 0 Then
            btnImprimirRecibo.Visible = False
            'btnImprimirRecibo.Visible = True
            btnImprimirRecibo.Text = "Imprimir Copia"
            btnAnularRecibo.Visible = True
            ReciboVigente = True
            ReciboAnulado = False
        End If
    End Sub

    Sub ImprimirRecibo(ByVal Original As Boolean)
        ''Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para Imprimir!"
            ctrError.SetError(btnImprimirRecibo, mensaje)
            ctrError.SetIconAlignment(btnImprimirRecibo, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Dim param(-1) As SqlClient.SqlParameter

        ''Datos
        'CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

        ''Transaccion
        'CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        ''Informacion de Salida
        'CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        'CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        ''Anular
        'Dim MensajeRetorno As String = ""

        'If CSistema.ExecuteStoreProcedure(param, "SpReciboVentaProcesar", False, False, MensajeRetorno) = False Then
        '    tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
        '    ctrError.SetError(btnAnularRecibo, "Atencion: " & MensajeRetorno)
        '    ctrError.SetIconAlignment(btnAnularRecibo, ErrorIconAlignment.TopRight)

        '    Exit Sub

        'Else
        '    tsslEstado.Text = MensajeRetorno
        'End If

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        If Original Then
            If ProcesarRecibo() = False Then
                Exit Sub
            End If
        End If

        Dim frm As New frmImprimirReciboDirecto
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()

        Dim CMovimiento As New Reporte.CReporteReciboVenta
        CMovimiento.ReciboVenta(IDTransaccion, frm.Impresora, Original)
        CargarOperacion(IDTransaccion)
    End Sub

    Sub ImprimirReciboBoletaContraBoleta(ByVal Original As Boolean)

        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para Imprimir!"
            ctrError.SetError(btnImprimirRecibo, mensaje)
            ctrError.SetIconAlignment(btnImprimirRecibo, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If Original Then
            If ProcesarRecibo() = False Then
                Exit Sub
            End If
        End If

        Dim frm As New frmImprimirReciboDirecto
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog()

        Dim CMovimiento As New Reporte.CReporteReciboVenta
        CMovimiento.ReciboVentaBoletaContraBoleta(IDTransaccion, vIDCliente, frm.Impresora, Original)
        CargarOperacion(IDTransaccion)
    End Sub

    Function ProcesarRecibo() As Boolean
        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para Imprimir!"
            ctrError.SetError(btnImprimirRecibo, mensaje)
            ctrError.SetIconAlignment(btnImprimirRecibo, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Return False
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpReciboVentaProcesar", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnularRecibo, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnularRecibo, ErrorIconAlignment.TopRight)

            Return False

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        Return True

    End Function

    Sub AnularRecibo()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnularRecibo, mensaje)
            ctrError.SetIconAlignment(btnAnularRecibo, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpReciboVentaProcesar", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnularRecibo, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnularRecibo, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        CargarOperacion(IDTransaccion)
    End Sub

    Sub CalcularPlazo(Optional ByVal Plazo As Integer = -1)

        Dim Fecha As Date = Date.Now
        Dim dias As Integer = 0

        If Plazo = -1 Then
            dias = CInt(DateDiff(DateInterval.Day, txtFechaVenta.GetValue, txtVencimiento.GetValue))
            If dias > PlazoCliente Then
                MessageBox.Show("El Plazo es mayor al asignado al Cliente!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtVencimiento.SetValue(DateAdd(DateInterval.Day, CDec(txtPlazo.ObtenerValor), txtFechaVenta.GetValue))
                txtPlazo.txt.Text = PlazoCliente
                txtPlazo.Focus()
                Exit Sub
            End If
            txtPlazo.SetValue(dias)
        Else
            If Plazo > PlazoCliente Then
                MessageBox.Show("El Plazo es mayor al asignado al Cliente!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtVencimiento.SetValue(DateAdd(DateInterval.Day, CDec(PlazoCliente), txtFechaVenta.GetValue))
                txtPlazo.txt.Text = PlazoCliente
                txtPlazo.Focus()
                Exit Sub
            End If
            txtVencimiento.SetValue(DateAdd(DateInterval.Day, CDec(txtPlazo.ObtenerValor), txtFechaVenta.GetValue))
        End If

    End Sub

    Sub ListarSucursalesClientes()

    End Sub

    Sub ObtenerInformacionPuntoVenta()

        txtTalonario.txt.Text = "0"
        txtVencimientoTimbrado.txt.Text = ""
        txtRestoTimbrado.txt.Text = "0"
        cbxTipoComprobante.cbx.Text = ""
        txtReferenciaSucursal.txt.Text = "000"
        txtReferenciaTerminal.txt.Text = "000"
        txtNroComprobante.txt.Text = "000"
        cbxCiudad.cbx.Text = ""
        cbxSucursal.cbx.Text = ""
        txtProducto.IDSucursal = vgIDSucursal

        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Exit Sub
        End If

        If dtPuntoExpedicion Is Nothing Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtPuntoExpedicion.Select(" ID=" & txtIDTimbrado.GetValue)
            txtTimbrado.txt.Text = oRow("Timbrado").ToString
            txtTalonario.txt.Text = oRow("TalonarioActual").ToString
            txtVencimientoTimbrado.SetValue(oRow("Vencimiento").ToString)
            txtRestoTimbrado.txt.Text = oRow("Saldo").ToString
            cbxTipoComprobante.SelectedValue(oRow("IDTipoComprobante").ToString)
            txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
            txtReferenciaTerminal.txt.Text = oRow("ReferenciaPunto").ToString
            txtNroComprobante.txt.Text = oRow("ProximoNumero").ToString
            cbxCiudad.SelectedValue(oRow("IDCiudad").ToString)
            cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)

            ' cbxDeposito.cbx.Text = 
            txtProducto.IDSucursal = oRow("IDSucursal").ToString
        Next

        Debug.Print(cbxTipoComprobante.cbx.Text)
        Debug.Print(cbxTipoComprobante.txt.Text)
        Debug.Print(cbxTipoComprobante.Texto)
        Debug.Print(txtFechaVenta.GetValueString)

    End Sub

    Sub ObtenerDeposito()

        If cbxSucursal.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If

        Dim dttemp As DataTable = CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursal.cbx.SelectedValue).Copy

        CSistema.SqlToComboBox(cbxDeposito2.cbx, dttemp)

        Dim DepositoRow As DataRow = CData.GetRow("IDSucursal=" & cbxSucursal.cbx.SelectedValue, "VDeposito")

        cbxDeposito2.cbx.Text = DepositoRow("Deposito").ToString
        cbxDeposito2.cbx.Text = vgDeposito


    End Sub

    Sub ObtenerInformacionClientes()

        Dim oRow As DataRow = txtCliente.Registro
        Dim oRowSucursal As DataRow = txtCliente.Sucursal

        'Datos Comunes
        txtDescuento.txt.Text = oRow("Descuento").ToString
        txtCreditoSaldo.txt.Text = CType(CSistema.ExecuteScalar("Select Deuda from vCliente where ID = " & oRow("ID").ToString & " "), Decimal)
        txtPlazo.txt.Text = oRow("PlazoCredito").ToString

        PlazoCliente = oRow("PlazoCredito").ToString
        Dim Plazo As Integer = oRow("PlazoCredito").ToString
        txtVencimiento.SetValue(DateAdd(DateInterval.Day, Plazo, txtFechaVenta.GetValue))

        cbxPromotor.txt.Text = oRow("Promotor")
        txtProducto.IDCliente = oRow("ID").ToString

        'Gestion Judicial
        Dim vJudicial As Boolean = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & oRow("ID").ToString, "VCliente")("Judicial").ToString)
        If vJudicial = True Then
            MessageBox.Show("El cliente se encuentra en Estado Judicial!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Inicializar()
        End If

        ''Configuraciones
        'OcxCotizacion1.Registro("ID") = oRow("IDMoneda")

        If txtCliente.SucursalSeleccionada = False Then
            cbxListaPrecio2.cbx.Text = oRow("ListaPrecio").ToString
            txtDireccion.txt.Text = oRow("Direccion").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString
            cbxVendedor.cbx.Text = oRow("Vendedor").ToString
            cbxCondicion.cbx.Text = oRow("CONDICION").ToString
            txtProducto.IDClienteSucursal = 0
            txtProducto.IDListaPrecio = oRow("IDListaPrecio").ToString
            txtPlazo.txt.Text = oRow("PlazoCredito").ToString
            PlazoCliente = oRow("PlazoCredito").ToString
            Plazo = oRow("PlazoCredito").ToString
            txtVencimiento.SetValue(DateAdd(DateInterval.Day, Plazo, txtFechaVenta.GetValue))
        Else
            cbxListaPrecio2.cbx.Text = oRowSucursal("ListaPrecio").ToString
            txtDireccion.txt.Text = oRowSucursal("Direccion").ToString
            txtTelefono.txt.Text = oRowSucursal("Telefono").ToString
            cbxVendedor.cbx.Text = oRowSucursal("Vendedor").ToString
            cbxCondicion.cbx.Text = oRowSucursal("CONDICION").ToString
            txtProducto.IDClienteSucursal = oRowSucursal("ID").ToString
            txtProducto.IDListaPrecio = oRowSucursal("IDListaPrecio").ToString
            txtPlazo.txt.Text = oRowSucursal("PlazoCredito").ToString
            PlazoCliente = oRowSucursal("PlazoCredito").ToString
            Plazo = oRowSucursal("PlazoCredito").ToString
            txtVencimiento.SetValue(DateAdd(DateInterval.Day, Plazo, txtFechaVenta.GetValue))
        End If

        If cbxCondicion.cbx.Text = "CONTADO" Then
            cbxCondicion.cbx.Enabled = False
        Else
            cbxCondicion.cbx.Enabled = True
        End If



        txtProducto.Consulta = "Select Top(100) ID, Ref, Descripcion, 'Codigo de Barra'=CodigoBarra,  'Tipo de Producto'=TipoProducto, 'Precio'=(Select " & vgOwnerDBFunction & ".FPrecioProducto(ID, " & oRow("ID").ToString & ", " & oRow("IDSucursal").ToString & ")), Impuesto, 'Existencia'=(Select dbo.FExistenciaProducto(ID, " & cbxDeposito2.cbx.SelectedValue & ")), 'Uni x Cja'=UnidadPorCaja, 'Uni Med.'=UnidadMedida From VProducto Where ControlarExistencia='True' And Estado='True' "
        txtProducto.ColumnasNumericas = {"Precio", "Existencia", "Uni x Cja"}

    End Sub

    Sub Imprimir(Optional ByVal ImprimirDirecto As Boolean = False)

        If ImprimirDirecto = False Then
            If CBool(vgConfiguraciones("VentaImprimirDocumento").ToString) = False Then
                Exit Sub
            End If
        End If

        Dim Total As Double = OcxImpuesto1.txtTotal.ObtenerValor
        Dim NumeroALetra As String

        Dim Where As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & IDTransaccion

        Dim Path As String = vgImpresionFacturaPath

        If chkConversion.Valor = True Then
            Path = vgImpresionFacturaConversionPath
        End If

#Disable Warning BC42104 ' Se usa la variable antes de que se le haya asignado un valor
        CReporte.ImprimirFactura(frm, Where, Path, vgImpresionFacturaImpresora, NumeroALetra, CSistema.FormatoMoneda(Total, vDecimalOperacion))
#Enable Warning BC42104 ' Se usa la variable antes de que se le haya asignado un valor


    End Sub
    Sub Imprimir2(Optional ByVal ImprimirDirecto As Boolean = False)

        'If ImprimirDirecto = False Then
        '    If CBool(vgConfiguraciones("VentaImprimirDocumento").ToString) = False Then
        '        Exit Sub
        '    End If
        'End If

        Dim CImpresion As New CImpresion
        CImpresion.IDTransaccion = IDTransaccion
        'CImpresion.IDFormularioImpresion = CSistema.RetornarValorString(CData.GetRow("Descripcion='FACTURA'", "FormularioImpresion")("IDFormularioImpresion").ToString)
        CImpresion.IDFormularioImpresion = CSistema.ExecuteScalar("Select Top(1) IDFormularioImpresion from vFormularioImpresionTerminal where Impresion = 'Factura' and IDTerminal = " & vgIDTerminal)
        If CImpresion.IDFormularioImpresion = 0 Then
            MessageBox.Show("Debe configurar una caida de impresion para Facturacion", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
        CImpresion.Impresora = vgImpresionFacturaImpresora
        If CImpresion.Impresora = "" Then
            MessageBox.Show("Debe configurar una impresora para Facturacion", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        CImpresion.Test = False
        CImpresion.Inicializar()
        CImpresion.Imprimir()

    End Sub

    Sub SeleccionarProducto()

        Dim Precio As DataRow = GetPrecios(txtProducto.Registro("IDImpuesto").ToString)

        'Si el producto no tiene precio en la lista de precio, salir
        'No controlar el precio
        'If Precio("Precio") = 0 Then
        '    MessageBox.Show("El producto no tiene precio!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    txtProducto.txt.SelectAll()
        '    Exit Sub
        'End If

        'Si el Porcentaje en el producto es 0, entonces el cliente no tiene descuento tactico
        If Precio("Tactico") = 0 Then
            Precio("TacticoPlanilla") = 0
        Else

            'Validar que el descuento que esta en el producto no supere el de la planilla
            If CDec(Precio("Tactico")) > CDec(Precio("TacticoPlanilla")) Then
                Dim frmHabilitar As New frmHabilitarProceso
                frmHabilitar.Mensaje = "El porcentaje de descuento Tactico (" & Precio("Tactico") & "%) Es mayor al de la planilla del mes: (" & Precio("TacticoPlanilla") & "%) "
                frmHabilitar.IDOperacion = IDOperacion
                frmHabilitar.Funcion = "HabilitarDescuentoTactico"
                frmHabilitar.Formulario = Me.Name
                frmHabilitar.ShowDialog(Me)

                If frmHabilitar.Aprobado = False Then
                    Exit Sub
                End If

            End If

        End If

        'Habilitar PrecioUnitario si el producto seleccionado es tipo servicio
        If txtProducto.Registro("ControlarExistencia") = False Then
            txtPrecioUnitario.SoloLectura = False
        Else
            txtPrecioUnitario.SoloLectura = True
        End If

        txtCantidad.Focus()

    End Sub

    Sub LimpiarControles()

        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtImporte.txt.Text = "0"
        txtPrecioUnitario.txt.Text = "0"
        txtProducto.txt.txt.Clear()
        txtDescuentoProducto.SetValue(0)

    End Sub

    Function GetPrecios(ByVal IDImpuesto As Integer) As DataRow

        'Porcentaje de Descuento
        Dim Precio As Decimal = 0
        Dim Cantidad As Decimal = 0
        Dim Descuento As Decimal = 0

        'Tipos de Descuentos
        Dim TPR As Decimal = 0
        Dim Express As Decimal = 0
        Dim Acuerdo As Decimal = 0
        Dim Tactico As Decimal = 0
        Dim TacticoPlanilla = 0

        Dim TotalDescuento As Decimal = 0
        Dim oRow As DataRow = dtPreciosProducto.NewRow

        'Descuentos
        TPR = txtProducto.ObtenerPorcentajeDescuento(ocxTXTProducto.ENUMTipoDescuento.TPR)
        Tactico = txtProducto.ObtenerPorcentajeDescuento(ocxTXTProducto.ENUMTipoDescuento.TACTICO)
        Express = txtProducto.ObtenerPorcentajeDescuento(ocxTXTProducto.ENUMTipoDescuento.EXPRESS)
        Acuerdo = txtProducto.ObtenerPorcentajeDescuento(ocxTXTProducto.ENUMTipoDescuento.ACUERDO)

        'Verificar si existe en la planilla de tacticos
        Dim ExistePlanillaTactico As Boolean = False
        Try
            ExistePlanillaTactico = CSistema.ObtenerDescuentoTacticoPlanillaExiste(txtProducto.Registro("ID"), cbxSucursal.GetValue)
        Catch ex As Exception

        End Try


        If ExistePlanillaTactico = False Then
            Tactico = 0
            Acuerdo = 0
        End If

        TotalDescuento = TPR + Express + Acuerdo + Tactico

        If IsNumeric(txtProducto.Registro("Precio")) Then
            Precio = CSistema.FormatoMoneda(txtProducto.Registro("Precio"), vDecimalOperacion)
        End If

        Cantidad = txtCantidad.ObtenerValor

        oRow("Precio") = Precio
        oRow("Cantidad") = Cantidad

        Descuento = CSistema.CalcularPorcentaje(Precio, TotalDescuento, True, False)

        oRow("PorcentajeDescuento") = TotalDescuento
        oRow("DescuentoUnitario") = Descuento
        oRow("TotalDescuento") = Descuento * Cantidad
        oRow("Total") = Precio * Cantidad
        oRow("TotalSinDescuento") = (Precio - Descuento) * Cantidad
        oRow("DescuentoUnitarioDiscriminado") = 0
        CSistema.CalcularIVA(IDImpuesto, Descuento, True, False, oRow("DescuentoUnitarioDiscriminado"))
        oRow("TotalDescuentoDiscriminado") = oRow("DescuentoUnitarioDiscriminado") * Cantidad

        'Descuentos
        oRow("TPR") = TPR
        oRow("Tactico") = Tactico
        oRow("Express") = Express
        oRow("Acuerdo") = Acuerdo

        'Descuento tactico en Planilla
        TacticoPlanilla = 0

        Try
            TacticoPlanilla = CSistema.ObtenerDescuentoTacticoPlanilla(txtProducto.Registro("ID"), cbxSucursal.GetValue, txtFechaVenta.GetValue)
        Catch ex As Exception

        End Try

        oRow("TacticoPlanilla") = TacticoPlanilla

        txtDescuentoProducto.txt.Text = TotalDescuento
        txtPrecioUnitario.txt.Text = Precio
        CalcularImporte(vDecimalOperacion, False, False)

        Return oRow

    End Function

    Sub VerDescuentos()

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim frm As New Form
        Dim lv As New ListView

        frm.Controls.Add(lv)
        lv.Dock = DockStyle.Fill
        lv.View = View.Details
        lv.HeaderStyle = ColumnHeaderStyle.Nonclickable
        lv.GridLines = True
        lv.Columns.Add("Tipo", 75)
        lv.Columns.Add("%", 50, HorizontalAlignment.Right)
        lv.Columns.Add("Desc.", 50, HorizontalAlignment.Right)
        lv.Columns.Add("Actividad", 200, HorizontalAlignment.Left)

        lv.FullRowSelect = True

        Dim IDProducto As Integer = lvLista.SelectedItems(0).Text
        Dim ID As Integer = lvLista.SelectedItems(0).Index
        Dim Total As Decimal = 0
        Dim TotalPorcentaje As Decimal = 0

        For Each oRow As DataRow In dtDescuento.Select("IDProducto=" & IDProducto & " And ID=" & ID & " ")

            If oRow("Porcentaje") > 0 Then
                Dim item As New ListViewItem(oRow("Tipo").ToString)
                item.SubItems.Add(oRow("Porcentaje").ToString)
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Descuento").ToString, True))
                item.SubItems.Add(oRow("Actividad").ToString)

                Total = Total + CDec(oRow("Descuento"))
                TotalPorcentaje = TotalPorcentaje + CDec(oRow("Porcentaje"))

                lv.Items.Add(item)
            End If

        Next

        Dim itemTotal As New ListViewItem("Total")
        itemTotal.SubItems.Add(TotalPorcentaje)
        itemTotal.SubItems.Add(CSistema.FormatoMoneda(Total, True))
        itemTotal.BackColor = Color.LightGray
        itemTotal.Font = New Font(itemTotal.Font.FontFamily.ToString, itemTotal.Font.SizeInPoints, FontStyle.Bold)

        lv.Items.Add(itemTotal)

        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Size = New Size(400, 200)
        frm.Text = lvLista.SelectedItems(0).SubItems(1).Text
        frm.ShowDialog(Me)

    End Sub

    Sub VerInformacionProducto()

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim frm As New frmPropiedad
        frm.TituloVentana = "Propiedades del Producto"
        frm.Titulo = lvLista.SelectedItems(0).SubItems(1).Text
        frm.Consulta = "Select ID, Descripcion, CodigoBarra, Ref, 'Existencia'=dbo.FExistenciaProducto(ID, " & cbxDeposito2.cbx.SelectedValue & "), TipoProducto, Linea, Marca, Presentacion, Categoria, Proveedor, Division, Procedencia, UnidadMedida,Peso,UnidadPorCaja,Impuesto, CostoPromedio From VProducto Where ID=" & lvLista.SelectedItems(0).Text
        frm.ShowDialog()

    End Sub

    Sub ManejarTecla(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo Then
            If e.KeyCode = vgKeyNuevoRegistro Then
                Nuevo()
            End If
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text
            ID = CInt(ID) + 1
            txtNroComprobante.SetValue(ID)
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtNroComprobante.SetValue(ID)
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroComprobante), 1) From VVenta Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyConsultar Then

            'Si no esta en el cliente
            If txtCliente.IsFocus = True Then
                Exit Sub
            End If

            'Si esta en el producto
            If txtProducto.Focus = True Then
                Dim IDCliente As String
                If txtCliente.Registro Is Nothing Then
                    Exit Sub
                End If

                IDCliente = txtCliente.Registro("ID").ToString

                Exit Sub
            End If

            Buscar()
        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(NroComprobante), 1) From VVenta Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub SeleccionarTimbrado()

        Dim frm As New frmSeleccionarTimbrado
        If IsNumeric(txtIDTimbrado.GetValue) = True Then
            frm.ID = txtIDTimbrado.GetValue
        End If
        frm.IDOperacion = IDOperacion
        frm.IDSucursal = vgIDSucursal

        FGMostrarFormulario(Me, frm, "Timbrados", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        txtIDTimbrado.SetValue(frm.ID)
        ObtenerInformacionPuntoVenta()
        vComprobanteExento = CType(CSistema.ExecuteScalar("Select Exento from vTipoComprobante where ID = " & cbxTipoComprobante.GetValue & " "), Boolean)
    End Sub

    Sub HabilitarDescuento()

        frmHabilitarProceso.Mensaje = "Ingrese las credenciales correspondientes."
        frmHabilitarProceso.Text = "Habilitar Proceso"
        frmHabilitarProceso.WindowState = FormWindowState.Normal
        frmHabilitarProceso.StartPosition = FormStartPosition.CenterScreen

        frmHabilitarProceso.IDOperacion = IDOperacion

        FGMostrarFormulario(Me, frmHabilitarProceso, "Credenciales", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frmHabilitarProceso.Aprobado = True Then
            btnHabilitar.Visible = False
            btnGuardar.Enabled = True
            Superado = False
        End If

    End Sub

    Sub EstablecerCondicionVenta()

        If vNuevo = True Then
            If cbxCondicion.cbx.SelectedIndex = 0 Then
                txtVencimiento.SoloLectura = True
                txtPlazo.SoloLectura = True
            Else
                txtVencimiento.SoloLectura = False
                txtPlazo.SoloLectura = False
            End If
        Else
            txtVencimiento.SoloLectura = True
            txtPlazo.SoloLectura = True
        End If

    End Sub

    Public Sub EstablecerVariablesGlobales(Optional _vgIDUsuario As Integer = 0, Optional _vgIDSucursal As Integer = 0, Optional _vgIDDeposito As Integer = 0, Optional _vgIDTerminal As Integer = 0, Optional _vgIDPerfil As Integer = 0)

        If _vgIDUsuario <> 0 Then
            vgIDUsuario = _vgIDUsuario
        End If

        If _vgIDSucursal <> 0 Then
            vgIDSucursal = _vgIDSucursal
        End If

        If _vgIDDeposito <> 0 Then
            vgIDDeposito = _vgIDDeposito
        End If

        If _vgIDTerminal <> 0 Then
            vgIDTerminal = _vgIDTerminal
        End If

        If _vgIDPerfil <> 0 Then
            vgIDPerfil = _vgIDPerfil
        End If

    End Sub

    Sub RecuperarCancelarAutomatico(vIDFormaPago As Integer)
        'vCancelarAutomatico = CSistema.RetornarValorBoolean(CData.GetRow("ID=" & vIDFormaPago, "vFormaPagoFactura")("CancelarAutomatico").ToString)
        vCancelarAutomatico = CBool(CSistema.ExecuteScalar("Select CancelarAutomatico from FormaPagoFactura where ID = " & vIDFormaPago))
        vCancelarAutomatico = vCancelarAutomatico
    End Sub

    Sub MostrarDecimales(ByVal IDMoneda As Integer)

        'If dgw.Columns.Count = 0 Then
        '    Exit Sub
        'End If

        If IDMoneda = 1 Then
            txtPrecioUnitario.Decimales = False
            'Format(lvLista.Columns("Cantidad"), "##,##")
            'dgw.Columns("Interes").DefaultCellStyle.Format = "N0"
            'dgw.Columns("Impuesto").DefaultCellStyle.Format = "N0"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N0"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N0"
        Else
            txtPrecioUnitario.Decimales = True
            'Format(lvLista.Columns("Cantidad"), "##,##.####")
            'dgw.Columns("Amortizacion").DefaultCellStyle.Format = "N2"
            'dgw.Columns("Interes").DefaultCellStyle.Format = "N2"
            'dgw.Columns("Impuesto").DefaultCellStyle.Format = "N2"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N2"
            'dgw.Columns("ImporteCuota").DefaultCellStyle.Format = "N2"
        End If

    End Sub


    Private Sub frmVenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        'Inicializar() <- Anulamos porque no se puede heredar si esta en funcionamiento
    End Sub

    Private Sub frmVenta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        LiberarMemoria()
    End Sub

    Private Sub frmVenta_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Then

            'Solo ejecutar fuera de ciertos controles
            If txtCantidad.txt.Focused = True Then
                'txtPrecioUnitario.Focus()
                Exit Sub
            End If

            If lvLista.Focused = True Then
                Exit Sub
            End If

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        'Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        Guardar2()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
        LimpiarControles()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click

        Me.Close()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If

    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtCliente_ItemMalSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.ItemMalSeleccionado

        'Limpiar los otros controles
        txtDireccion.txt.Clear()
        txtTelefono.txt.Clear()
        cbxListaPrecio2.txt.Clear()

        'Datos Comunes
        cbxListaPrecio2.cbx.Text = ""
        txtDescuento.txt.Text = "0"
        txtCreditoSaldo.txt.Text = "0"
        cbxPromotor.cbx.Text = ""
        txtProducto.IDCliente = Nothing

    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        ObtenerInformacionClientes()
        If vNuevo = True Then
            If txtCliente.ClienteVario = True Then
                txtDireccion.SoloLectura = False
                txtTelefono.SoloLectura = False
            Else
                txtDireccion.SoloLectura = True
                txtTelefono.SoloLectura = True
            End If

            If cbxListaPrecio2.SoloLectura = True Then
                If CBool(vgConfiguraciones("VentaBloquearFecha").ToString) = True Then
                    cbxCondicion.cbx.Focus()
                Else
                    txtFechaVenta.txt.Focus()
                End If
            Else
                CSistema.SelectNextControl(Me, Keys.Enter)
            End If
            'frmCliente.RecalcularDeuda(txtCliente.txtID)
        End If

    End Sub

    Private Sub cbxCondicion_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxCondicion.KeyUp
        EstablecerCondicionVenta()
    End Sub

    Private Sub cbxCondicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCondicion.PropertyChanged
        EstablecerCondicionVenta()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If CSistema.ExecuteScalar("select count(*) from Cotizacion where cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFechaVenta.GetValue, True, False) & "' and IDMoneda = " & OcxCotizacion1.Registro("ID")) = 0 And OcxCotizacion1.Registro("ID") > 1 Then
            MessageBox.Show("No se ha fijado cotizacion para la moneda seleccionada.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Close()
            Exit Sub
        End If

        txtProducto.Cotizacion = OcxCotizacion1.Registro("Cotizacion")
        If txtProducto.Seleccionado = True Then
            SeleccionarProducto()
        End If
        If dtDetalle.Rows.Count = 0 And txtProducto.txt.Texto = "" Then
            OcxCotizacion1.SoloLectura = False
        ElseIf dtDetalle.Rows.Count = 0 And txtProducto.txt.Texto <> "" Then
            OcxCotizacion1.SoloLectura = True
        End If
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada

        If txtCantidad.txt.Focused = False Then
            Exit Sub
        End If

        'Validar cantidad
        If txtCantidad.ObtenerValor = 0 Then
            Exit Sub
        End If

        CalcularImporte(vDecimalOperacion, True, False)

        If e.KeyCode = Keys.Enter Then

            If txtPrecioUnitario.SoloLectura = False Then
                txtPrecioUnitario.Focus()
                Exit Sub
            End If

            If vComprobanteExento = False Then
                CargarProducto()
            Else
                CargarProductoFacturaExportacion()
            End If

        End If

    End Sub

    Private Sub txtCostoUnitario_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPrecioUnitario.TeclaPrecionada

        CalcularImporte(vDecimalOperacion, True, False)

        If e.KeyCode = Keys.Enter Then

            Try

                'Si esta habilitado para modificar
                If vgConfiguraciones("VentaModificarPrecio") Then

                    'Si se modifico el precio
                    If txtProducto.Registro("Precio") <> txtPrecioUnitario.ObtenerValor Then
                        txtProducto.Registro("Precio") = txtPrecioUnitario.ObtenerValor
                    End If

                End If

            Catch ex As Exception

            End Try

            If vComprobanteExento = False Then
                CargarProducto()
            Else
                CargarProductoFacturaExportacion()
            End If

        End If

    End Sub

    Private Sub txtVencimiento_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtVencimiento.Leave
        CalcularPlazo()
    End Sub

    Private Sub txtPlazo_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPlazo.Leave
        CalcularPlazo(CInt(txtPlazo.ObtenerValor))
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub VerDescuentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDescuentosToolStripMenuItem.Click
        VerDescuentos()
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        EliminarProducto()
    End Sub

    Private Sub InformacionDelProductoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InformacionDelProductoToolStripMenuItem.Click
        VerInformacionProducto()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        'If chkImprimir2.Valor = False Then
        '    Imprimir(True)
        'Else
        Imprimir2()
        'End If
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(dtDetalle)
    End Sub

    Private Sub chkConversion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkConversion.PropertyChanged
        ListarDetalle()
    End Sub

    Private Sub lklTimbrado_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklTimbrado.LinkClicked
        SeleccionarTimbrado()
    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporte.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If vComprobanteExento = False Then
                CargarProducto()
            Else
                CargarProductoFacturaExportacion()
            End If
        End If
    End Sub

    Private Sub lklDescuento_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklDescuento.LinkClicked
        ModificarDescuento()
    End Sub

    Private Sub cbxDeposito2_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxDeposito2.PropertyChanged
        txtProducto.IDDeposito = cbxDeposito2.GetValue
    End Sub

    Private Sub ModificarDescToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarDescToolStripMenuItem.Click
        ModificarDescuentoDetalle()
    End Sub

    Private Sub btnHabilitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHabilitar.Click
        HabilitarDescuento()
    End Sub

    Private Sub cbxVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxVendedor.PropertyChanged
        Debug.Print(cbxVendedor.cbx.SelectedValue & " - " & Date.Now.ToShortTimeString)
    End Sub

    Private Sub cbxCondicion_Leave(sender As System.Object, e As System.EventArgs) Handles cbxCondicion.Leave
        EstablecerCondicionVenta()
    End Sub

    Private Sub cbxFormaPagoFactura_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxFormaPagoFactura.PropertyChanged
        'vCancelarAutomatico = ("Select CancelarAutomatico From FormaPagoFactura Where ID=" & cbxFormaPagoFactura.GetValue)
        RecuperarCancelarAutomatico(cbxFormaPagoFactura.GetValue)
    End Sub

    Private Sub cbxListaPrecio2_Leave(sender As System.Object, e As System.EventArgs) Handles cbxListaPrecio2.Leave
        txtProducto.IDListaPrecio = cbxListaPrecio2.GetValue
    End Sub

    Private Sub cbxFormaPagoFactura_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxFormaPagoFactura.TeclaPrecionada
        RecuperarCancelarAutomatico(cbxFormaPagoFactura.GetValue)
    End Sub

    Private Sub OcxCotizacion1_CambioMoneda() Handles OcxCotizacion1.CambioMoneda
        OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaVenta.txt.Text, True, False)
        OcxCotizacion1.Recargar()
        OcxImpuesto1.SetIDMoneda(OcxCotizacion1.Registro("ID"))
        MostrarDecimales(OcxCotizacion1.Registro("ID"))
        vDecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & OcxCotizacion1.Registro("ID"), "vMoneda")("Decimales").ToString)
        txtProducto.IDMoneda = OcxCotizacion1.Registro("ID")
        txtProducto.Cotizacion = OcxCotizacion1.Registro("Cotizacion")
        txtImporte.Decimales = vDecimalOperacion
        txtPrecioUnitario.Decimales = vDecimalOperacion
    End Sub

    Private Sub txtFechaVenta_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtFechaVenta.TeclaPrecionada
        OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaVenta.txt.Text, True, False)
        OcxCotizacion1.Recargar()
        OcxCotizacion1.Actualizar()

        txtProducto.FechaFacturarPedido = txtFechaVenta.GetValue
        CalcularPlazo()

    End Sub

    Private Sub btnImprimirRecibo_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimirRecibo.Click


        If vBoletaContraBoleta = False Then
            ImprimirRecibo(ReciboAnulado)
        Else
            ImprimirReciboBoletaContraBoleta(ReciboAnulado)
        End If
    End Sub

    Private Sub btnAnularRecibo_Click(sender As System.Object, e As System.EventArgs) Handles btnAnularRecibo.Click
        AnularRecibo()
    End Sub

    Private Sub btnAsiento_VisibleChanged(sender As Object, e As EventArgs) Handles btnAsiento.VisibleChanged
        Dim a As String = sender.ToString
    End Sub

    Private Sub chkImprimirSoloObservacion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkImprimirSoloObservacion.PropertyChanged
        ListarDetalle()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmVenta_Activate()
        Me.Refresh()
    End Sub

    Private Sub txtCliente_Load(sender As Object, e As EventArgs) Handles txtCliente.Load

    End Sub

    Private Sub gbxDetalle_Enter(sender As Object, e As EventArgs) Handles gbxDetalle.Enter

    End Sub
End Class

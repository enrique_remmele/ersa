﻿Public Class frmAgregarComprobantes
    'CLASES
    Public CData As New CData

    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()
        'Puntos de Expediciones
        cbxPuntoExpedicion.Conectar(CData.GetTable("VTerminalPuntoExpedicion"), " IDTerminal = " & vgIDTerminal)

        'Ciudades
        cbxCiudad.Conectar("", "Descripcion")

        'Sucursal
        cbxSucursal.Conectar()
    End Sub

    Private Sub frmAgregarComprobantes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmAgregarComprobantes_Activate()
        Me.Refresh()
    End Sub
End Class
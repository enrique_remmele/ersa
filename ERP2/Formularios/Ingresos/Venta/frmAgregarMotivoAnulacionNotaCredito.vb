﻿Public Class frmAgregarMotivoAnulacionNotaCredito
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDMotivoValue As Integer
    Public Property IDMotivo() As Integer
        Get
            Return IDMotivoValue
        End Get
        Set(ByVal value As Integer)
            IDMotivoValue = value
        End Set
    End Property

    Public Property Procesado As Boolean
    Public Property Mantener As Boolean

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicializar()

        'CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion from MotivoAnulacionNotaCredito where estado = 1")
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, Descripcion,MantenerPedidoVigente from MotivoAnulacionNotaCredito where estado = 1")
        cbxMotivo.DataSource = dt
        cbxMotivo.ValueMember = "ID"
        cbxMotivo.DisplayMember = "Descripcion"

        Procesado = False

    End Sub

    Private Sub frmAgregarMotivoAlulacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        IDMotivo = cbxMotivo.SelectedValue
        Mantener = cbxMotivo.Items(cbxMotivo.SelectedIndex)("MantenerPedidoVigente")
        Close()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmAgregarMotivoAnulacionNotaCredito_Activate()
        Me.Refresh()
    End Sub
End Class
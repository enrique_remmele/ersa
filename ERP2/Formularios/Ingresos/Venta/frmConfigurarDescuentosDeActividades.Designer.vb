﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfigurarDescuentosDeActividades
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colIDActividad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDProducto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.colActividad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colAsignado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescuentoMaximo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPorcentajeUtilizado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescuentoPorcentaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDescuento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colExcedente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.lblTotalDescuento = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.txtProducto = New ERP.ocxTXTString()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.txtTotalDescuento = New ERP.ocxTXTNumeric()
        Me.txtPrecio = New ERP.ocxTXTNumeric()
        Me.lblPrecio = New System.Windows.Forms.Label()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDActividad, Me.colID, Me.colIDProducto, Me.colSel, Me.colActividad, Me.colAsignado, Me.colDescuentoMaximo, Me.colPorcentajeUtilizado, Me.colSaldo, Me.colDescuentoPorcentaje, Me.colDescuento, Me.colExcedente})
        Me.dgw.Location = New System.Drawing.Point(8, 45)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(758, 182)
        Me.dgw.TabIndex = 8
        '
        'colIDActividad
        '
        Me.colIDActividad.Frozen = True
        Me.colIDActividad.HeaderText = "IDActividad"
        Me.colIDActividad.Name = "colIDActividad"
        Me.colIDActividad.ReadOnly = True
        Me.colIDActividad.Visible = False
        '
        'colID
        '
        Me.colID.Frozen = True
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Visible = False
        '
        'colIDProducto
        '
        Me.colIDProducto.Frozen = True
        Me.colIDProducto.HeaderText = "IDProducto"
        Me.colIDProducto.Name = "colIDProducto"
        Me.colIDProducto.ReadOnly = True
        Me.colIDProducto.Visible = False
        '
        'colSel
        '
        Me.colSel.Frozen = True
        Me.colSel.HeaderText = "Sel"
        Me.colSel.Name = "colSel"
        Me.colSel.Width = 30
        '
        'colActividad
        '
        Me.colActividad.Frozen = True
        Me.colActividad.HeaderText = "Codigo"
        Me.colActividad.Name = "colActividad"
        Me.colActividad.ReadOnly = True
        Me.colActividad.Width = 150
        '
        'colAsignado
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = "0"
        Me.colAsignado.DefaultCellStyle = DataGridViewCellStyle2
        Me.colAsignado.Frozen = True
        Me.colAsignado.HeaderText = "Asignado"
        Me.colAsignado.Name = "colAsignado"
        Me.colAsignado.ReadOnly = True
        Me.colAsignado.Width = 95
        '
        'colDescuentoMaximo
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = "0"
        Me.colDescuentoMaximo.DefaultCellStyle = DataGridViewCellStyle3
        Me.colDescuentoMaximo.HeaderText = "Max."
        Me.colDescuentoMaximo.Name = "colDescuentoMaximo"
        Me.colDescuentoMaximo.ReadOnly = True
        Me.colDescuentoMaximo.Width = 40
        '
        'colPorcentajeUtilizado
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = "0"
        Me.colPorcentajeUtilizado.DefaultCellStyle = DataGridViewCellStyle4
        Me.colPorcentajeUtilizado.HeaderText = "Utilizado"
        Me.colPorcentajeUtilizado.Name = "colPorcentajeUtilizado"
        Me.colPorcentajeUtilizado.ReadOnly = True
        Me.colPorcentajeUtilizado.Width = 95
        '
        'colSaldo
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = "0"
        Me.colSaldo.DefaultCellStyle = DataGridViewCellStyle5
        Me.colSaldo.HeaderText = "Saldo"
        Me.colSaldo.Name = "colSaldo"
        Me.colSaldo.ReadOnly = True
        Me.colSaldo.Width = 95
        '
        'colDescuentoPorcentaje
        '
        Me.colDescuentoPorcentaje.HeaderText = "% Desc."
        Me.colDescuentoPorcentaje.Name = "colDescuentoPorcentaje"
        Me.colDescuentoPorcentaje.Width = 75
        '
        'colDescuento
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = "0"
        Me.colDescuento.DefaultCellStyle = DataGridViewCellStyle6
        Me.colDescuento.HeaderText = "Desc."
        Me.colDescuento.Name = "colDescuento"
        Me.colDescuento.ReadOnly = True
        Me.colDescuento.Width = 75
        '
        'colExcedente
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N0"
        DataGridViewCellStyle7.NullValue = "0"
        Me.colExcedente.DefaultCellStyle = DataGridViewCellStyle7
        Me.colExcedente.HeaderText = "Excedente"
        Me.colExcedente.Name = "colExcedente"
        Me.colExcedente.ReadOnly = True
        Me.colExcedente.Width = 95
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Location = New System.Drawing.Point(12, 22)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(53, 13)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        '
        'lblTotalDescuento
        '
        Me.lblTotalDescuento.AutoSize = True
        Me.lblTotalDescuento.Location = New System.Drawing.Point(540, 22)
        Me.lblTotalDescuento.Name = "lblTotalDescuento"
        Me.lblTotalDescuento.Size = New System.Drawing.Size(45, 13)
        Me.lblTotalDescuento.TabIndex = 4
        Me.lblTotalDescuento.Text = "Total %:"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(629, 22)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Text = "Total:"
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(567, 238)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldo.TabIndex = 9
        Me.lblSaldo.Text = "Saldo:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 287)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(774, 22)
        Me.StatusStrip1.TabIndex = 14
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(610, 261)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 12
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(691, 261)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 13
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(719, 233)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 11
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(610, 233)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(103, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 10
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'txtProducto
        '
        Me.txtProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProducto.Color = System.Drawing.Color.Empty
        Me.txtProducto.Indicaciones = Nothing
        Me.txtProducto.Location = New System.Drawing.Point(65, 18)
        Me.txtProducto.Multilinea = False
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(326, 21)
        Me.txtProducto.SoloLectura = True
        Me.txtProducto.TabIndex = 1
        Me.txtProducto.TabStop = False
        Me.txtProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtProducto.Texto = ""
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Indicaciones = Nothing
        Me.txtTotal.Location = New System.Drawing.Point(663, 17)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(103, 22)
        Me.txtTotal.SoloLectura = True
        Me.txtTotal.TabIndex = 7
        Me.txtTotal.TabStop = False
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'txtTotalDescuento
        '
        Me.txtTotalDescuento.Color = System.Drawing.Color.Empty
        Me.txtTotalDescuento.Decimales = True
        Me.txtTotalDescuento.Indicaciones = Nothing
        Me.txtTotalDescuento.Location = New System.Drawing.Point(585, 17)
        Me.txtTotalDescuento.Name = "txtTotalDescuento"
        Me.txtTotalDescuento.Size = New System.Drawing.Size(44, 22)
        Me.txtTotalDescuento.SoloLectura = True
        Me.txtTotalDescuento.TabIndex = 5
        Me.txtTotalDescuento.TabStop = False
        Me.txtTotalDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDescuento.Texto = "0"
        '
        'txtPrecio
        '
        Me.txtPrecio.Color = System.Drawing.Color.Empty
        Me.txtPrecio.Decimales = True
        Me.txtPrecio.Indicaciones = Nothing
        Me.txtPrecio.Location = New System.Drawing.Point(437, 17)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(103, 22)
        Me.txtPrecio.SoloLectura = True
        Me.txtPrecio.TabIndex = 3
        Me.txtPrecio.TabStop = False
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecio.Texto = "0"
        '
        'lblPrecio
        '
        Me.lblPrecio.AutoSize = True
        Me.lblPrecio.Location = New System.Drawing.Point(397, 22)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(40, 13)
        Me.lblPrecio.TabIndex = 2
        Me.lblPrecio.Text = "Precio:"
        '
        'frmConfigurarDescuentosDeActividades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(774, 309)
        Me.Controls.Add(Me.txtPrecio)
        Me.Controls.Add(Me.lblPrecio)
        Me.Controls.Add(Me.txtCantidad)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtSaldo)
        Me.Controls.Add(Me.lblSaldo)
        Me.Controls.Add(Me.txtProducto)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.txtTotalDescuento)
        Me.Controls.Add(Me.lblTotalDescuento)
        Me.Controls.Add(Me.lblProducto)
        Me.Controls.Add(Me.dgw)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmConfigurarDescuentosDeActividades"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione los descuentos para cada actividad"
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents lblProducto As System.Windows.Forms.Label
    Friend WithEvents lblTotalDescuento As System.Windows.Forms.Label
    Friend WithEvents txtTotalDescuento As ERP.ocxTXTNumeric
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents txtProducto As ERP.ocxTXTString
    Friend WithEvents colIDActividad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIDProducto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colActividad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAsignado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescuentoMaximo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPorcentajeUtilizado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSaldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescuentoPorcentaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDescuento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colExcedente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents txtPrecio As ERP.ocxTXTNumeric
    Friend WithEvents lblPrecio As System.Windows.Forms.Label
End Class

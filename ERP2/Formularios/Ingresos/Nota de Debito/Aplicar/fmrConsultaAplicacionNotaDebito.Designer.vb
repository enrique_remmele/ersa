﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaAplicacionNotaDebito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnSeleccionar = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidadCobranza = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.lblNroComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtOperacion = New ERP.ocxTXTNumeric()
        Me.tlpInforme = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblComprobantes = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblCantidadComprobantes = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.txtCantidadComprobantes = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidadCobranza = New ERP.ocxTXTNumeric()
        Me.lvOperacion = New System.Windows.Forms.ListView()
        Me.lvComprobantes = New System.Windows.Forms.ListView()
        Me.flpTotal = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalComprobantes = New ERP.ocxTXTNumeric()
        Me.lblTotalComprobantes = New System.Windows.Forms.Label()
        Me.tlpSeleccionar = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalCobranza = New ERP.ocxTXTNumeric()
        Me.lblTotalCobranza = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkCliente = New System.Windows.Forms.CheckBox()
        Me.cbxCliente = New System.Windows.Forms.ComboBox()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.chkTipoComprobante = New System.Windows.Forms.CheckBox()
        Me.cbxTipoComprobante = New System.Windows.Forms.ComboBox()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.tlpGeneral = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.tlpInforme.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.flpTotal.SuspendLayout()
        Me.tlpSeleccionar.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.tlpGeneral.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSeleccionar
        '
        Me.btnSeleccionar.Location = New System.Drawing.Point(3, 3)
        Me.btnSeleccionar.Name = "btnSeleccionar"
        Me.btnSeleccionar.Size = New System.Drawing.Size(75, 20)
        Me.btnSeleccionar.TabIndex = 0
        Me.btnSeleccionar.Text = "Seleccionar"
        Me.btnSeleccionar.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCantidadCobranza)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(367, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(60, 20)
        Me.Panel2.TabIndex = 0
        '
        'lblCantidadCobranza
        '
        Me.lblCantidadCobranza.AutoSize = True
        Me.lblCantidadCobranza.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadCobranza.Name = "lblCantidadCobranza"
        Me.lblCantidadCobranza.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadCobranza.TabIndex = 0
        Me.lblCantidadCobranza.Text = "Cantidad:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtNroComprobante)
        Me.Panel3.Controls.Add(Me.lblNroComprobante)
        Me.Panel3.Controls.Add(Me.lblOperacion)
        Me.Panel3.Controls.Add(Me.txtOperacion)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(293, 20)
        Me.Panel3.TabIndex = 0
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(247, 2)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(117, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 3
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroComprobante.Texto = ""
        '
        'lblNroComprobante
        '
        Me.lblNroComprobante.AutoSize = True
        Me.lblNroComprobante.Location = New System.Drawing.Point(137, 6)
        Me.lblNroComprobante.Name = "lblNroComprobante"
        Me.lblNroComprobante.Size = New System.Drawing.Size(111, 13)
        Me.lblNroComprobante.TabIndex = 2
        Me.lblNroComprobante.Text = "Nro. de Comprobante:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(3, 6)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtOperacion
        '
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Decimales = True
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(68, 1)
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(68, 22)
        Me.txtOperacion.SoloLectura = False
        Me.txtOperacion.TabIndex = 1
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOperacion.Texto = "0"
        '
        'tlpInforme
        '
        Me.tlpInforme.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlpInforme.ColumnCount = 1
        Me.tlpInforme.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpInforme.Controls.Add(Me.TableLayoutPanel4, 0, 3)
        Me.tlpInforme.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.tlpInforme.Controls.Add(Me.lvOperacion, 0, 1)
        Me.tlpInforme.Controls.Add(Me.lvComprobantes, 0, 4)
        Me.tlpInforme.Controls.Add(Me.flpTotal, 0, 5)
        Me.tlpInforme.Controls.Add(Me.tlpSeleccionar, 0, 2)
        Me.tlpInforme.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpInforme.Location = New System.Drawing.Point(3, 3)
        Me.tlpInforme.Name = "tlpInforme"
        Me.tlpInforme.RowCount = 6
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpInforme.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.tlpInforme.Size = New System.Drawing.Size(522, 319)
        Me.tlpInforme.TabIndex = 0
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel1, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(4, 163)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(514, 26)
        Me.TableLayoutPanel4.TabIndex = 4
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblComprobantes)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(293, 20)
        Me.Panel5.TabIndex = 0
        '
        'lblComprobantes
        '
        Me.lblComprobantes.AutoSize = True
        Me.lblComprobantes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComprobantes.Location = New System.Drawing.Point(3, 8)
        Me.lblComprobantes.Name = "lblComprobantes"
        Me.lblComprobantes.Size = New System.Drawing.Size(117, 13)
        Me.lblComprobantes.TabIndex = 0
        Me.lblComprobantes.Text = "Lista de comprobantes "
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblCantidadComprobantes)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(367, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(60, 20)
        Me.Panel1.TabIndex = 1
        '
        'lblCantidadComprobantes
        '
        Me.lblCantidadComprobantes.AutoSize = True
        Me.lblCantidadComprobantes.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadComprobantes.Name = "lblCantidadComprobantes"
        Me.lblCantidadComprobantes.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadComprobantes.TabIndex = 0
        Me.lblCantidadComprobantes.Text = "Cantidad:"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.txtCantidadComprobantes)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(433, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(78, 20)
        Me.Panel6.TabIndex = 2
        '
        'txtCantidadComprobantes
        '
        Me.txtCantidadComprobantes.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobantes.Decimales = True
        Me.txtCantidadComprobantes.Indicaciones = Nothing
        Me.txtCantidadComprobantes.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadComprobantes.Name = "txtCantidadComprobantes"
        Me.txtCantidadComprobantes.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadComprobantes.SoloLectura = False
        Me.txtCantidadComprobantes.TabIndex = 0
        Me.txtCantidadComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobantes.Texto = "0"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.01285!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.98715!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 3, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(4, 4)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(514, 26)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidadCobranza)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(433, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(78, 20)
        Me.Panel4.TabIndex = 2
        '
        'txtCantidadCobranza
        '
        Me.txtCantidadCobranza.Color = System.Drawing.Color.Empty
        Me.txtCantidadCobranza.Decimales = True
        Me.txtCantidadCobranza.Indicaciones = Nothing
        Me.txtCantidadCobranza.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadCobranza.Name = "txtCantidadCobranza"
        Me.txtCantidadCobranza.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadCobranza.SoloLectura = False
        Me.txtCantidadCobranza.TabIndex = 0
        Me.txtCantidadCobranza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCobranza.Texto = "0"
        '
        'lvOperacion
        '
        Me.lvOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOperacion.Location = New System.Drawing.Point(4, 37)
        Me.lvOperacion.Name = "lvOperacion"
        Me.lvOperacion.Size = New System.Drawing.Size(514, 86)
        Me.lvOperacion.TabIndex = 0
        Me.lvOperacion.UseCompatibleStateImageBehavior = False
        '
        'lvComprobantes
        '
        Me.lvComprobantes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvComprobantes.Location = New System.Drawing.Point(4, 196)
        Me.lvComprobantes.Name = "lvComprobantes"
        Me.lvComprobantes.Size = New System.Drawing.Size(514, 86)
        Me.lvComprobantes.TabIndex = 5
        Me.lvComprobantes.UseCompatibleStateImageBehavior = False
        '
        'flpTotal
        '
        Me.flpTotal.Controls.Add(Me.txtTotalComprobantes)
        Me.flpTotal.Controls.Add(Me.lblTotalComprobantes)
        Me.flpTotal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpTotal.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.flpTotal.Location = New System.Drawing.Point(4, 289)
        Me.flpTotal.Name = "flpTotal"
        Me.flpTotal.Size = New System.Drawing.Size(514, 26)
        Me.flpTotal.TabIndex = 6
        '
        'txtTotalComprobantes
        '
        Me.txtTotalComprobantes.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobantes.Decimales = True
        Me.txtTotalComprobantes.Indicaciones = Nothing
        Me.txtTotalComprobantes.Location = New System.Drawing.Point(408, 3)
        Me.txtTotalComprobantes.Name = "txtTotalComprobantes"
        Me.txtTotalComprobantes.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalComprobantes.SoloLectura = False
        Me.txtTotalComprobantes.TabIndex = 1
        Me.txtTotalComprobantes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobantes.Texto = "0"
        '
        'lblTotalComprobantes
        '
        Me.lblTotalComprobantes.AutoSize = True
        Me.lblTotalComprobantes.Location = New System.Drawing.Point(368, 0)
        Me.lblTotalComprobantes.Name = "lblTotalComprobantes"
        Me.lblTotalComprobantes.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalComprobantes.TabIndex = 0
        Me.lblTotalComprobantes.Text = "Total:"
        '
        'tlpSeleccionar
        '
        Me.tlpSeleccionar.ColumnCount = 2
        Me.tlpSeleccionar.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.tlpSeleccionar.Controls.Add(Me.btnSeleccionar, 0, 0)
        Me.tlpSeleccionar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpSeleccionar.Location = New System.Drawing.Point(4, 130)
        Me.tlpSeleccionar.Name = "tlpSeleccionar"
        Me.tlpSeleccionar.RowCount = 1
        Me.tlpSeleccionar.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpSeleccionar.Size = New System.Drawing.Size(514, 26)
        Me.tlpSeleccionar.TabIndex = 9
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalCobranza)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalCobranza)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(260, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(251, 20)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'txtTotalCobranza
        '
        Me.txtTotalCobranza.Color = System.Drawing.Color.Empty
        Me.txtTotalCobranza.Decimales = True
        Me.txtTotalCobranza.Indicaciones = Nothing
        Me.txtTotalCobranza.Location = New System.Drawing.Point(145, 3)
        Me.txtTotalCobranza.Name = "txtTotalCobranza"
        Me.txtTotalCobranza.Size = New System.Drawing.Size(103, 22)
        Me.txtTotalCobranza.SoloLectura = False
        Me.txtTotalCobranza.TabIndex = 0
        Me.txtTotalCobranza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCobranza.Texto = "0"
        '
        'lblTotalCobranza
        '
        Me.lblTotalCobranza.AutoSize = True
        Me.lblTotalCobranza.Location = New System.Drawing.Point(105, 0)
        Me.lblTotalCobranza.Name = "lblTotalCobranza"
        Me.lblTotalCobranza.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalCobranza.TabIndex = 0
        Me.lblTotalCobranza.Text = "Total:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(3, 16)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(225, 300)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkCliente)
        Me.TabPage1.Controls.Add(Me.cbxCliente)
        Me.TabPage1.Controls.Add(Me.chkSucursal)
        Me.TabPage1.Controls.Add(Me.cbxSucursal)
        Me.TabPage1.Controls.Add(Me.btn4)
        Me.TabPage1.Controls.Add(Me.chkFecha)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Controls.Add(Me.chkTipoComprobante)
        Me.TabPage1.Controls.Add(Me.cbxTipoComprobante)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(217, 274)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'chkCliente
        '
        Me.chkCliente.AutoSize = True
        Me.chkCliente.Location = New System.Drawing.Point(7, 4)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(61, 17)
        Me.chkCliente.TabIndex = 10
        Me.chkCliente.Text = "Cliente:"
        Me.chkCliente.UseVisualStyleBackColor = True
        '
        'cbxCliente
        '
        Me.cbxCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormattingEnabled = True
        Me.cbxCliente.Location = New System.Drawing.Point(7, 20)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.Size = New System.Drawing.Size(203, 21)
        Me.cbxCliente.TabIndex = 11
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(6, 93)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(70, 17)
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 110)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxSucursal.TabIndex = 5
        '
        'btn4
        '
        Me.btn4.Location = New System.Drawing.Point(6, 248)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(203, 24)
        Me.btn4.TabIndex = 9
        Me.btn4.Text = "Emitir informe"
        Me.btn4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn4.UseVisualStyleBackColor = True
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(6, 181)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(59, 17)
        Me.chkFecha.TabIndex = 6
        Me.chkFecha.Text = "Fecha:"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'dtpHasta
        '
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Location = New System.Drawing.Point(6, 224)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(203, 20)
        Me.dtpHasta.TabIndex = 8
        '
        'dtpDesde
        '
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Location = New System.Drawing.Point(6, 198)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesde.TabIndex = 7
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.AutoSize = True
        Me.chkTipoComprobante.Location = New System.Drawing.Point(6, 49)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(131, 17)
        Me.chkTipoComprobante.TabIndex = 2
        Me.chkTipoComprobante.Text = "Tipo de Comprobante:"
        Me.chkTipoComprobante.UseVisualStyleBackColor = True
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormattingEnabled = True
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(6, 66)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(203, 21)
        Me.cbxTipoComprobante.TabIndex = 3
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.TabControl1)
        Me.gbxFiltro.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxFiltro.Location = New System.Drawing.Point(531, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(231, 319)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        '
        'tlpGeneral
        '
        Me.tlpGeneral.ColumnCount = 2
        Me.tlpGeneral.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpGeneral.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 237.0!))
        Me.tlpGeneral.Controls.Add(Me.gbxFiltro, 1, 0)
        Me.tlpGeneral.Controls.Add(Me.tlpInforme, 0, 0)
        Me.tlpGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tlpGeneral.Name = "tlpGeneral"
        Me.tlpGeneral.RowCount = 1
        Me.tlpGeneral.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpGeneral.Size = New System.Drawing.Size(765, 325)
        Me.tlpGeneral.TabIndex = 3
        '
        'frmConsultaAplicacionNotaDebito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(765, 325)
        Me.Controls.Add(Me.tlpGeneral)
        Me.Name = "frmConsultaAplicacionNotaDebito"
        Me.Text = "fmrConsultaAplicacionNotaDebito"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.tlpInforme.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.flpTotal.ResumeLayout(False)
        Me.flpTotal.PerformLayout()
        Me.tlpSeleccionar.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.tlpGeneral.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSeleccionar As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadCobranza As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents lblNroComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtOperacion As ERP.ocxTXTNumeric
    Friend WithEvents tlpInforme As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lblComprobantes As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadComprobantes As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txtCantidadCobranza As ERP.ocxTXTNumeric
    Friend WithEvents lvOperacion As System.Windows.Forms.ListView
    Friend WithEvents lvComprobantes As System.Windows.Forms.ListView
    Friend WithEvents flpTotal As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalComprobantes As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalComprobantes As System.Windows.Forms.Label
    Friend WithEvents tlpSeleccionar As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalCobranza As ERP.ocxTXTNumeric
    Friend WithEvents lblTotalCobranza As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents tlpGeneral As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents chkCliente As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCliente As System.Windows.Forms.ComboBox
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkTipoComprobante As System.Windows.Forms.CheckBox
    Friend WithEvents cbxTipoComprobante As System.Windows.Forms.ComboBox
End Class

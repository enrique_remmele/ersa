﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAplicacionNotaDebitoCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtTotalVenta = New ERP.ocxTXTNumeric()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblTotalVenta = New System.Windows.Forms.Label()
        Me.lklEliminarFactura = New System.Windows.Forms.LinkLabel()
        Me.btnAgregarFactura = New System.Windows.Forms.Button()
        Me.txtSaldoTotal = New ERP.ocxTXTNumeric()
        Me.lvVentas = New System.Windows.Forms.ListView()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.lblSaldoTotal = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.gbxComprobantes = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtDebitoNotaCredito = New ERP.ocxTXTNumeric()
        Me.txtSaldoNotaCredito = New ERP.ocxTXTNumeric()
        Me.txtTotalNotaCredito = New ERP.ocxTXTNumeric()
        Me.txtMonedaNotaCredito = New ERP.ocxTXTString()
        Me.txtFechaNotaCredito = New ERP.ocxTXTString()
        Me.bntAgregarNotacredito = New System.Windows.Forms.Button()
        Me.lblDebito = New System.Windows.Forms.Label()
        Me.LblSaldo = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtObservacionNotaCredito = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblMonedaNotaCredito = New System.Windows.Forms.Label()
        Me.txtCotizacionNC = New ERP.ocxTXTNumeric()
        Me.lblFechaNotaCredito = New System.Windows.Forms.Label()
        Me.txtComprobanteNotaCredito = New ERP.ocxTXTString()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxComprobantes.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.SuspendLayout()
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.DisplayIndex = 5
        Me.ColumnHeader6.Text = "Descontado"
        Me.ColumnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader6.Width = 75
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.DisplayIndex = 3
        Me.ColumnHeader8.Text = "Total"
        Me.ColumnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader8.Width = 75
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(262, 441)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(226, 20)
        Me.flpAnuladoPor.TabIndex = 22
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.DisplayIndex = 4
        Me.ColumnHeader5.Text = "Cobrado"
        Me.ColumnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader5.Width = 75
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.DisplayIndex = 2
        Me.ColumnHeader4.Text = "Coti."
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader4.Width = 45
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.DisplayIndex = 6
        Me.ColumnHeader7.Text = "Saldo"
        Me.ColumnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader7.Width = 75
        '
        'txtTotalVenta
        '
        Me.txtTotalVenta.Color = System.Drawing.Color.Empty
        Me.txtTotalVenta.Decimales = True
        Me.txtTotalVenta.Indicaciones = Nothing
        Me.txtTotalVenta.Location = New System.Drawing.Point(529, 165)
        Me.txtTotalVenta.Name = "txtTotalVenta"
        Me.txtTotalVenta.Size = New System.Drawing.Size(142, 22)
        Me.txtTotalVenta.SoloLectura = True
        Me.txtTotalVenta.TabIndex = 5
        Me.txtTotalVenta.TabStop = False
        Me.txtTotalVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalVenta.Texto = "0"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(4, 441)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(252, 20)
        Me.flpRegistradoPor.TabIndex = 19
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.DisplayIndex = 7
        Me.ColumnHeader9.Text = "Importe"
        Me.ColumnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader9.Width = 75
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.DisplayIndex = 1
        Me.ColumnHeader3.Text = "Moneda"
        Me.ColumnHeader3.Width = 52
        '
        'lblTotalVenta
        '
        Me.lblTotalVenta.AutoSize = True
        Me.lblTotalVenta.Location = New System.Drawing.Point(492, 170)
        Me.lblTotalVenta.Name = "lblTotalVenta"
        Me.lblTotalVenta.Size = New System.Drawing.Size(34, 13)
        Me.lblTotalVenta.TabIndex = 4
        Me.lblTotalVenta.Text = "Total:"
        '
        'lklEliminarFactura
        '
        Me.lklEliminarFactura.AutoSize = True
        Me.lklEliminarFactura.Location = New System.Drawing.Point(6, 165)
        Me.lklEliminarFactura.Name = "lklEliminarFactura"
        Me.lklEliminarFactura.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarFactura.TabIndex = 3
        Me.lklEliminarFactura.TabStop = True
        Me.lklEliminarFactura.Text = "Eliminar comprobante"
        '
        'btnAgregarFactura
        '
        Me.btnAgregarFactura.Location = New System.Drawing.Point(6, 14)
        Me.btnAgregarFactura.Name = "btnAgregarFactura"
        Me.btnAgregarFactura.Size = New System.Drawing.Size(153, 21)
        Me.btnAgregarFactura.TabIndex = 1
        Me.btnAgregarFactura.Text = "Agregar Comprobantes"
        Me.btnAgregarFactura.UseVisualStyleBackColor = True
        '
        'txtSaldoTotal
        '
        Me.txtSaldoTotal.Color = System.Drawing.Color.Empty
        Me.txtSaldoTotal.Decimales = True
        Me.txtSaldoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoTotal.Indicaciones = Nothing
        Me.txtSaldoTotal.Location = New System.Drawing.Point(533, 439)
        Me.txtSaldoTotal.Name = "txtSaldoTotal"
        Me.txtSaldoTotal.Size = New System.Drawing.Size(142, 22)
        Me.txtSaldoTotal.SoloLectura = True
        Me.txtSaldoTotal.TabIndex = 21
        Me.txtSaldoTotal.TabStop = False
        Me.txtSaldoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoTotal.Texto = "0"
        '
        'lvVentas
        '
        Me.lvVentas.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.ColumnHeader1, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader8, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader9})
        Me.lvVentas.FullRowSelect = True
        Me.lvVentas.GridLines = True
        Me.lvVentas.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvVentas.HideSelection = False
        Me.lvVentas.Location = New System.Drawing.Point(5, 41)
        Me.lvVentas.MultiSelect = False
        Me.lvVentas.Name = "lvVentas"
        Me.lvVentas.Size = New System.Drawing.Size(666, 118)
        Me.lvVentas.TabIndex = 2
        Me.lvVentas.UseCompatibleStateImageBehavior = False
        Me.lvVentas.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.DisplayIndex = 8
        Me.ColumnHeader2.Text = "IDTransaccion"
        Me.ColumnHeader2.Width = 0
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.DisplayIndex = 0
        Me.ColumnHeader1.Text = "Comprobante"
        Me.ColumnHeader1.Width = 110
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(604, 478)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 30
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'lblSaldoTotal
        '
        Me.lblSaldoTotal.AutoSize = True
        Me.lblSaldoTotal.Location = New System.Drawing.Point(493, 443)
        Me.lblSaldoTotal.Name = "lblSaldoTotal"
        Me.lblSaldoTotal.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldoTotal.TabIndex = 20
        Me.lblSaldoTotal.Text = "Saldo:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 506)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(696, 22)
        Me.StatusStrip1.TabIndex = 31
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(21, 478)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 23
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(102, 478)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 24
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(280, 478)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 26
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(183, 478)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(75, 23)
        Me.btnBusquedaAvanzada.TabIndex = 25
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(523, 478)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 29
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(442, 478)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 28
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(361, 478)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 27
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 3, 15, 8, 54, 7, 484)
        Me.txtFecha.Location = New System.Drawing.Point(599, 11)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 9
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(560, 14)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(367, 10)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(434, 10)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(126, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(5, 74)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 15
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(198, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc.:"
        '
        'gbxComprobantes
        '
        Me.gbxComprobantes.Controls.Add(Me.txtTotalVenta)
        Me.gbxComprobantes.Controls.Add(Me.lblTotalVenta)
        Me.gbxComprobantes.Controls.Add(Me.lklEliminarFactura)
        Me.gbxComprobantes.Controls.Add(Me.btnAgregarFactura)
        Me.gbxComprobantes.Controls.Add(Me.lvVentas)
        Me.gbxComprobantes.Location = New System.Drawing.Point(4, 235)
        Me.gbxComprobantes.Name = "gbxComprobantes"
        Me.gbxComprobantes.Size = New System.Drawing.Size(684, 198)
        Me.gbxComprobantes.TabIndex = 18
        Me.gbxComprobantes.TabStop = False
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 117
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(76, 37)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(365, 23)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 11
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(5, 43)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 10
        Me.lblCliente.Text = "Cliente:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(76, 70)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(599, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 16
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(495, 41)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(74, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 13
        Me.cbxMoneda.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(449, 45)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 12
        Me.lblMoneda.Text = "Moneda:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(583, 41)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(92, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 14
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(229, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(98, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(326, 14)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(40, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDebitoNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtSaldoNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtTotalNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtMonedaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtFechaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.bntAgregarNotacredito)
        Me.GroupBox1.Controls.Add(Me.lblDebito)
        Me.GroupBox1.Controls.Add(Me.LblSaldo)
        Me.GroupBox1.Controls.Add(Me.lblTotal)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtObservacionNotaCredito)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblMonedaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtCotizacionNC)
        Me.GroupBox1.Controls.Add(Me.lblFechaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtComprobanteNotaCredito)
        Me.GroupBox1.Location = New System.Drawing.Point(4, 115)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(684, 114)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Nota Credito a Aplicar"
        '
        'txtDebitoNotaCredito
        '
        Me.txtDebitoNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtDebitoNotaCredito.Decimales = False
        Me.txtDebitoNotaCredito.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtDebitoNotaCredito.Location = New System.Drawing.Point(565, 84)
        Me.txtDebitoNotaCredito.Name = "txtDebitoNotaCredito"
        Me.txtDebitoNotaCredito.Size = New System.Drawing.Size(108, 21)
        Me.txtDebitoNotaCredito.SoloLectura = False
        Me.txtDebitoNotaCredito.TabIndex = 16
        Me.txtDebitoNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDebitoNotaCredito.Texto = "0"
        '
        'txtSaldoNotaCredito
        '
        Me.txtSaldoNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtSaldoNotaCredito.Decimales = False
        Me.txtSaldoNotaCredito.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtSaldoNotaCredito.Location = New System.Drawing.Point(286, 84)
        Me.txtSaldoNotaCredito.Name = "txtSaldoNotaCredito"
        Me.txtSaldoNotaCredito.Size = New System.Drawing.Size(108, 21)
        Me.txtSaldoNotaCredito.SoloLectura = False
        Me.txtSaldoNotaCredito.TabIndex = 14
        Me.txtSaldoNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoNotaCredito.Texto = "0"
        '
        'txtTotalNotaCredito
        '
        Me.txtTotalNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtTotalNotaCredito.Decimales = False
        Me.txtTotalNotaCredito.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalNotaCredito.Location = New System.Drawing.Point(75, 84)
        Me.txtTotalNotaCredito.Name = "txtTotalNotaCredito"
        Me.txtTotalNotaCredito.Size = New System.Drawing.Size(108, 21)
        Me.txtTotalNotaCredito.SoloLectura = False
        Me.txtTotalNotaCredito.TabIndex = 12
        Me.txtTotalNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalNotaCredito.Texto = "0"
        '
        'txtMonedaNotaCredito
        '
        Me.txtMonedaNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonedaNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtMonedaNotaCredito.Indicaciones = Nothing
        Me.txtMonedaNotaCredito.Location = New System.Drawing.Point(520, 30)
        Me.txtMonedaNotaCredito.Multilinea = False
        Me.txtMonedaNotaCredito.Name = "txtMonedaNotaCredito"
        Me.txtMonedaNotaCredito.Size = New System.Drawing.Size(67, 21)
        Me.txtMonedaNotaCredito.SoloLectura = False
        Me.txtMonedaNotaCredito.TabIndex = 7
        Me.txtMonedaNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMonedaNotaCredito.Texto = ""
        '
        'txtFechaNotaCredito
        '
        Me.txtFechaNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFechaNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtFechaNotaCredito.Indicaciones = Nothing
        Me.txtFechaNotaCredito.Location = New System.Drawing.Point(394, 30)
        Me.txtFechaNotaCredito.Multilinea = False
        Me.txtFechaNotaCredito.Name = "txtFechaNotaCredito"
        Me.txtFechaNotaCredito.Size = New System.Drawing.Size(77, 21)
        Me.txtFechaNotaCredito.SoloLectura = False
        Me.txtFechaNotaCredito.TabIndex = 5
        Me.txtFechaNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFechaNotaCredito.Texto = ""
        '
        'bntAgregarNotacredito
        '
        Me.bntAgregarNotacredito.Location = New System.Drawing.Point(264, 30)
        Me.bntAgregarNotacredito.Name = "bntAgregarNotacredito"
        Me.bntAgregarNotacredito.Size = New System.Drawing.Size(90, 21)
        Me.bntAgregarNotacredito.TabIndex = 3
        Me.bntAgregarNotacredito.Text = "Seleccionar"
        Me.bntAgregarNotacredito.UseVisualStyleBackColor = True
        '
        'lblDebito
        '
        Me.lblDebito.AutoSize = True
        Me.lblDebito.Location = New System.Drawing.Point(518, 88)
        Me.lblDebito.Name = "lblDebito"
        Me.lblDebito.Size = New System.Drawing.Size(41, 13)
        Me.lblDebito.TabIndex = 15
        Me.lblDebito.Text = "Debito:"
        '
        'LblSaldo
        '
        Me.LblSaldo.AutoSize = True
        Me.LblSaldo.Location = New System.Drawing.Point(243, 88)
        Me.LblSaldo.Name = "LblSaldo"
        Me.LblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.LblSaldo.TabIndex = 13
        Me.LblSaldo.Text = "Saldo:"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(5, 88)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 11
        Me.lblTotal.Text = "Total:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Comprobante:"
        '
        'txtObservacionNotaCredito
        '
        Me.txtObservacionNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtObservacionNotaCredito.Indicaciones = Nothing
        Me.txtObservacionNotaCredito.Location = New System.Drawing.Point(76, 57)
        Me.txtObservacionNotaCredito.Multilinea = False
        Me.txtObservacionNotaCredito.Name = "txtObservacionNotaCredito"
        Me.txtObservacionNotaCredito.Size = New System.Drawing.Size(595, 21)
        Me.txtObservacionNotaCredito.SoloLectura = False
        Me.txtObservacionNotaCredito.TabIndex = 10
        Me.txtObservacionNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionNotaCredito.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Observacion:"
        '
        'lblMonedaNotaCredito
        '
        Me.lblMonedaNotaCredito.AutoSize = True
        Me.lblMonedaNotaCredito.Location = New System.Drawing.Point(471, 34)
        Me.lblMonedaNotaCredito.Name = "lblMonedaNotaCredito"
        Me.lblMonedaNotaCredito.Size = New System.Drawing.Size(49, 13)
        Me.lblMonedaNotaCredito.TabIndex = 6
        Me.lblMonedaNotaCredito.Text = "Moneda:"
        '
        'txtCotizacionNC
        '
        Me.txtCotizacionNC.Color = System.Drawing.Color.Empty
        Me.txtCotizacionNC.Decimales = False
        Me.txtCotizacionNC.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacionNC.Location = New System.Drawing.Point(587, 30)
        Me.txtCotizacionNC.Name = "txtCotizacionNC"
        Me.txtCotizacionNC.Size = New System.Drawing.Size(84, 21)
        Me.txtCotizacionNC.SoloLectura = False
        Me.txtCotizacionNC.TabIndex = 8
        Me.txtCotizacionNC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacionNC.Texto = "0"
        '
        'lblFechaNotaCredito
        '
        Me.lblFechaNotaCredito.AutoSize = True
        Me.lblFechaNotaCredito.Location = New System.Drawing.Point(354, 34)
        Me.lblFechaNotaCredito.Name = "lblFechaNotaCredito"
        Me.lblFechaNotaCredito.Size = New System.Drawing.Size(40, 13)
        Me.lblFechaNotaCredito.TabIndex = 4
        Me.lblFechaNotaCredito.Text = "Fecha:"
        '
        'txtComprobanteNotaCredito
        '
        Me.txtComprobanteNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtComprobanteNotaCredito.Indicaciones = Nothing
        Me.txtComprobanteNotaCredito.Location = New System.Drawing.Point(76, 30)
        Me.txtComprobanteNotaCredito.Multilinea = False
        Me.txtComprobanteNotaCredito.Name = "txtComprobanteNotaCredito"
        Me.txtComprobanteNotaCredito.Size = New System.Drawing.Size(182, 21)
        Me.txtComprobanteNotaCredito.SoloLectura = False
        Me.txtComprobanteNotaCredito.TabIndex = 2
        Me.txtComprobanteNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteNotaCredito.Texto = ""
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(4, 3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(684, 106)
        Me.gbxCabecera.TabIndex = 16
        Me.gbxCabecera.TabStop = False
        '
        'frmAplicacionNotaDebitoCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(696, 528)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.txtSaldoTotal)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.lblSaldoTotal)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxComprobantes)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Name = "frmAplicacionNotaDebitoCliente"
        Me.Text = "frmAplicacionNotaDebitoCliente"
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxComprobantes.ResumeLayout(False)
        Me.gbxComprobantes.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtTotalVenta As ERP.ocxTXTNumeric
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblTotalVenta As System.Windows.Forms.Label
    Friend WithEvents lklEliminarFactura As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAgregarFactura As System.Windows.Forms.Button
    Friend WithEvents txtSaldoTotal As ERP.ocxTXTNumeric
    Friend WithEvents lvVentas As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents lblSaldoTotal As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents gbxComprobantes As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDebitoNotaCredito As ERP.ocxTXTNumeric
    Friend WithEvents txtSaldoNotaCredito As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalNotaCredito As ERP.ocxTXTNumeric
    Friend WithEvents txtMonedaNotaCredito As ERP.ocxTXTString
    Friend WithEvents txtFechaNotaCredito As ERP.ocxTXTString
    Friend WithEvents bntAgregarNotacredito As System.Windows.Forms.Button
    Friend WithEvents lblDebito As System.Windows.Forms.Label
    Friend WithEvents LblSaldo As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtObservacionNotaCredito As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblMonedaNotaCredito As System.Windows.Forms.Label
    Friend WithEvents txtCotizacionNC As ERP.ocxTXTNumeric
    Friend WithEvents lblFechaNotaCredito As System.Windows.Forms.Label
    Friend WithEvents txtComprobanteNotaCredito As ERP.ocxTXTString
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
End Class

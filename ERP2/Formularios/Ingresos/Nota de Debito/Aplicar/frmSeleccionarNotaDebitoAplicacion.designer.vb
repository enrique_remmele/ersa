﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionarNotaDebitoAplicacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ColSaldo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.LvListar = New System.Windows.Forms.ListView()
        Me.ColFecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColMovimiento = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColOperacion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColComprobante = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColObservacion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColTotal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColTotalImpuesto = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BtnCancelar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ColSaldo
        '
        Me.ColSaldo.DisplayIndex = 6
        Me.ColSaldo.Text = "Saldo"
        Me.ColSaldo.Width = 72
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.LvListar, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.49061!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.50939!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(739, 218)
        Me.TableLayoutPanel1.TabIndex = 4
        '
        'LvListar
        '
        Me.LvListar.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColFecha, Me.ColMovimiento, Me.ColOperacion, Me.ColComprobante, Me.ColObservacion, Me.ColTotal, Me.ColTotalImpuesto, Me.ColSaldo})
        Me.LvListar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LvListar.FullRowSelect = True
        Me.LvListar.GridLines = True
        Me.LvListar.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.LvListar.HideSelection = False
        Me.LvListar.Location = New System.Drawing.Point(3, 3)
        Me.LvListar.MultiSelect = False
        Me.LvListar.Name = "LvListar"
        Me.LvListar.Size = New System.Drawing.Size(733, 184)
        Me.LvListar.TabIndex = 1
        Me.LvListar.UseCompatibleStateImageBehavior = False
        Me.LvListar.View = System.Windows.Forms.View.Details
        '
        'ColFecha
        '
        Me.ColFecha.Text = "Fecha"
        '
        'ColMovimiento
        '
        Me.ColMovimiento.Text = "Mov"
        '
        'ColOperacion
        '
        Me.ColOperacion.Text = "Operacion"
        Me.ColOperacion.Width = 90
        '
        'ColComprobante
        '
        Me.ColComprobante.Text = "Comprobante"
        Me.ColComprobante.Width = 87
        '
        'ColObservacion
        '
        Me.ColObservacion.DisplayIndex = 7
        Me.ColObservacion.Text = "Observacion"
        Me.ColObservacion.Width = 108
        '
        'ColTotal
        '
        Me.ColTotal.DisplayIndex = 4
        Me.ColTotal.Text = "Total"
        '
        'ColTotalImpuesto
        '
        Me.ColTotalImpuesto.DisplayIndex = 5
        Me.ColTotalImpuesto.Text = "TotalImpuesto"
        Me.ColTotalImpuesto.Width = 79
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 193)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(733, 22)
        Me.Panel1.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.BtnCancelar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel2.Location = New System.Drawing.Point(533, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(200, 22)
        Me.Panel2.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(43, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'BtnCancelar
        '
        Me.BtnCancelar.Location = New System.Drawing.Point(125, 0)
        Me.BtnCancelar.Name = "BtnCancelar"
        Me.BtnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.BtnCancelar.TabIndex = 0
        Me.BtnCancelar.Text = "&Cancelar"
        Me.BtnCancelar.UseVisualStyleBackColor = True
        '
        'frmSeleccionarNotaDebito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(739, 218)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmSeleccionarNotaDebito"
        Me.Text = "frmSeleccionarNotaDebito"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ColSaldo As System.Windows.Forms.ColumnHeader
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LvListar As System.Windows.Forms.ListView
    Friend WithEvents ColFecha As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColMovimiento As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColOperacion As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColComprobante As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColObservacion As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColTotal As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColTotalImpuesto As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents BtnCancelar As System.Windows.Forms.Button
End Class

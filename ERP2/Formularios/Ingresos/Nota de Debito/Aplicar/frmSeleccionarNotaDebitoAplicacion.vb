﻿Public Class frmSeleccionarNotaDebitoAplicacion

    Dim CSistema As New CSistema

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property


    Sub Inicializar()

        'Listar lvListar
        Listar()

    End Sub
    Sub Listar()

        LvListar.Items.Clear()

        CSistema.dtToLv(LvListar, dt)

        CSistema.FormatoMoneda(LvListar, 4)
        CSistema.FormatoMoneda(LvListar, 5)
        CSistema.FormatoMoneda(LvListar, 6)
        If dt.Rows.Count > 0 Then
            LvListar.Columns(7).Width = 0
            LvListar.Columns(8).Width = 0
        End If
        ' LvListar.Columns(7).Width = 0
        ' LvListar.Columns(8).Width = 0

    End Sub

    Sub seleccionar()

        If LvListar.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        'For Each item As ListViewItem In LvListar.SelectedItems
        '    IDTransaccion = item.SubItems(7).Text()
        'Next

        IDTransaccion = LvListar.SelectedItems(0).SubItems(7).Text
        Seleccionado = True

        Me.Close()

    End Sub

    Private Sub frmSeleccionarNotaCreditoAplicacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub BtnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        seleccionar()
    End Sub

    Private Sub LvListar_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles LvListar.KeyUp
        If e.KeyCode = Keys.Enter Then
            seleccionar()
        End If
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarNotaDebitoAplicacion_Activate()
        Me.Refresh()
    End Sub
End Class

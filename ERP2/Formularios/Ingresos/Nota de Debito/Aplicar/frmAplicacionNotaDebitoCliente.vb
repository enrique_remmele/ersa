﻿Public Class frmAplicacionNotaDebitoCliente
    
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsientoContableCobranzaCredito As New CAsientoContableCobranzaCredito

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDTransaccionNotaCreditoValue As Integer
    Public Property IDTransaccionNotaCredito() As Integer
        Get
            Return IDTransaccionNotaCreditoValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionNotaCreditoValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim vControles() As Control
    Dim dtCargarNotaDebito As New DataTable
    Dim dtVentasPendientes As New DataTable
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        txtCliente.Conectar()

        'Otros

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(frmAplicacionNotaCreditoCliente.Name, "APLICACION NOTA DE DEBITO", "AND")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsientoContableCobranzaCredito.Inicializar()

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False


        'Botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, bntAgregarNotacredito)
        CSistema.CargaControl(vControles, txtComprobanteNotaCredito)
        CSistema.CargaControl(vControles, txtFechaNotaCredito)
        CSistema.CargaControl(vControles, txtMonedaNotaCredito)
        CSistema.CargaControl(vControles, txtCotizacionNC)
        CSistema.CargaControl(vControles, txtObservacionNotaCredito)
        CSistema.CargaControl(vControles, txtTotalNotaCredito)
        CSistema.CargaControl(vControles, txtSaldoNotaCredito)
        CSistema.CargaControl(vControles, txtDebitoNotaCredito)

        CSistema.CargaControl(vControles, btnAgregarFactura)
        CSistema.CargaControl(vControles, lklEliminarFactura)



        'CARGAR ESTRUCTURA DEL DETALLE 


        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList



        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")



    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)



    End Sub

    Sub Nuevo()

        'Configurar botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        'Limpiar detalle
        '
        ' ListarVentas()



        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        CAsientoContableCobranzaCredito.Limpiar()

        ' vNuevo = True

        'Cabecera
        txtFecha.txt.Text = ""
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtCliente.Clear()

        'Limpiar Facturas
        lvVentas.Items.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From NotaDebitoAplicacion),1)"), Integer)
        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "NotaDebitoAplicacion", "NroComprobante", cbxSucursal.cbx.SelectedValue)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Bloquear Nota Credito Aplicar
        txtComprobanteNotaCredito.txt.ReadOnly = True
        txtFechaNotaCredito.txt.ReadOnly = True
        txtMonedaNotaCredito.txt.ReadOnly = True
        txtCotizacionNC.txt.ReadOnly = True
        txtObservacionNotaCredito.txt.ReadOnly = True
        txtTotalNotaCredito.txt.ReadOnly = True
        txtSaldoNotaCredito.txt.ReadOnly = True
        'txtDebitoNotaCredito.txt.ReadOnly = True
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()

        'Limpiar  Aplicar Nota de Credito
        txtComprobanteNotaCredito.txt.Text = ""
        txtFechaNotaCredito.txt.Text = ""
        txtMonedaNotaCredito.txt.Text = ""
        txtComprobanteNotaCredito.txt.Text = ""
        txtObservacionNotaCredito.txt.Text = ""
        txtTotalNotaCredito.txt.Text = ""
        txtSaldoNotaCredito.txt.Text = ""
        txtDebitoNotaCredito.txt.Text = ""

        txtCotizacion.txt.Text = 1

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", txtFecha.GetValue.ToShortDateString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCotizacion.txt.Text), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtDebitoNotaCredito.txt.Text), ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoAplicacion", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From NotaDebitoAplicacion Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoAplicacion", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
            CargarOperacion(IDTransaccion)

            txtID.SoloLectura = False
        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar Nota Credito Aplicada
            Procesar = InsertarDetalle(IDTransaccion)


        End If


        '
        Try

            ReDim param(-1)

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar
            If CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoAplicacionProcesar", False, False, MensajeRetorno) = False Then

            End If

            ''Cargamos el Asiento
            ''Cargamos el asiento
            'CAsientoContableCobranzaCredito.CAsiento.IDTransaccion = IDTransaccion
            'GenerarAsiento()

            'CAsientoContableCobranzaCredito.CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        Catch ex As Exception

        End Try


        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False


        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'El monto de saldo tiene que ser 0
        If txtSaldoTotal.txt.Text <> 0 Then
            Dim mensaje As String = "Los montos de la nota Debito y factura no coinciden!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Total de nota credito no puede ser > a saldo
        If txtSaldoNotaCredito.txt.Text < txtDebitoNotaCredito.txt.Text Then
            Dim mensaje As String = "Debito no puede ser mayor al saldo!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asignamos entero al nro de comprobante
        txtComprobante.txt.Text = CInt(txtComprobante.txt.Text)

        If CInt(txtComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtCliente.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Moneda
        If IsNumeric(cbxMoneda.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el tipo de moneda!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If



        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ValidarDocumento = True

    End Function

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoAplicacion", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From NotaDebitoAplicacion Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoAplicacion", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
        CargarOperacion(IDTransaccion)



    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)


        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""



        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From NotaDebitoAplicacion Where Numero=" & txtID.ObtenerValor & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtComprobante, mensaje)
            ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)



        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VNotaDebitoAplicacion Where IDTransaccion=" & IDTransaccion)


        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtComprobante, mensaje)
            ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        txtCliente.SetValue(oRow("IDCliente").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValueFromString(oRow("Fecha").ToString)
        txtCotizacion.txt.Text = oRow("Cotizacion")
        txtObservacion.txt.Text = oRow("Observacion").ToString
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos Nota de Credito y Facturas
        'Seleccionar desde NotaCreditoVentaAplicada IDTransaccionNOtaCredito
        dtCargarNotaDebito = CSistema.ExecuteToDataTable("Select NCVA.IDTransaccionNotaDebitoAplicacion, N .IDTransaccion, N .Fec , N .NroComprobante , N .Moneda, N.Cotizacion ,N.Observacion ,N.Total ,N.Saldo ,NCVA.Cobrado, NCVA.Acreditado, NCVA.SAldo, NCVA.Importe, 'Sel'='True', 'Totales'=NCA.Total From NotaDebitoVentaAplicada NCVA Join NotaDebitoAplicacion NCA On NCVA.IDTransaccionNotaDebitoAplicacion=NCA.IDTransaccion Join VNotaCredito N On NCVA.IDTransaccionNotaDebito =N.IDTransaccion Where NCVA.IDTransaccionNotaDebitoAplicacion=" & IDTransaccion).Copy
        If dtCargarNotaDebito IsNot Nothing Then
            If dtCargarNotaDebito.Rows.Count > 0 Then
                IDTransaccionNotaCredito = dtCargarNotaDebito.Rows(0)("IDTransaccion")
            End If
        End If
        'dtVentas
        dtVentasPendientes = CSistema.ExecuteToDataTable("Select V.IDTransaccion, V.Comprobante, V.Moneda, V.Cotizacion, V.Total, NCVA.Cobrado, NCVA.Acreditado, NCVA.SAldo, NCVA.Importe, 'Sel'='True' From NotaDebitoVentaAplicada NCVA Join VVenta V On NCVA.IDTransaccionVenta=V.IDTransaccion Where NCVA.IDTransaccionNotaDebitoAplicacion=" & IDTransaccion).Copy
        CargarNotaCreditoAplicar2()
        ListarVentas()

        CalcularTotales()

        'Inicializamos el Asiento
        'CAsiento.Limpiar()



    End Sub

    Sub Cancelar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        'Otros
        Dim frm As New frmConsultaAplicacionNotaDebito
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)
    End Sub

    Sub ObtenerNotaCredito()

        If txtCliente.Seleccionado = False Then
            dtCargarNotaDebito.Rows.Clear()
            Exit Sub
        End If

        dtCargarNotaDebito = CSistema.ExecuteToDataTable("Select  Fec, NroComprobante, Observacion, Moneda, Cotizacion, Total, Saldo, IDTransaccion,'Sel'='True'   from VNotaDebito where Aplicar = 'False' and Saldo >0 and IDCliente=" & txtCliente.Registro("ID").ToString).Copy
        dtVentasPendientes = CSistema.ExecuteToDataTable("Select *, 'Sel'='True' From VVentasPendientes Where IDCliente=" & txtCliente.Registro("ID").ToString).Copy

        ' ListarVentas()
    End Sub

    Sub CargarNotaCredito()
        Dim frm As New frmSeleccionarNotaDebitoAplicacion
        frm.Text = " Seleccionar Nota de Debito "
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtCargarNotaDebito
        frm.ShowDialog(Me)
        If frm.Seleccionado = True Then
            IDTransaccionNotaCredito = frm.IDTransaccion
            CargarNotaCreditoAplicar()
        End If
        If txtSaldoTotal.ObtenerValor > 0 Then
            CalcularTotales()

        End If
        txtDebitoNotaCredito.txt.Focus()
    End Sub

    Sub CargarVenta()

        Dim frm As New FrmSeleccionarNotaCreditoAplicarFactura
        frm.Text = "Seleccionar Factura"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtVentasPendientes
        frm.Inicializar()
        frm.ShowDialog(Me)
        dtVentasPendientes = frm.dt

        ListarVentas()

    End Sub

    Sub CargarNotaCreditoAplicar()
        For Each oRow As DataRow In dtCargarNotaDebito.Rows()
            If oRow("Sel") = True Then
                If oRow("IDTransaccion").ToString = IDTransaccionNotaCredito Then
                    txtFechaNotaCredito.txt.Text = oRow("Fec").ToString
                    txtComprobanteNotaCredito.txt.Text = oRow("NroComprobante").ToString
                    txtMonedaNotaCredito.txt.Text = oRow("Moneda").ToString
                    txtCotizacionNC.txt.Text = oRow("Cotizacion").ToString
                    txtObservacionNotaCredito.txt.Text = oRow("Observacion").ToString
                    txtTotalNotaCredito.SetValue(oRow("Total").ToString)
                    txtSaldoNotaCredito.SetValue(oRow("Saldo").ToString)
                    txtDebitoNotaCredito.SetValue(oRow("Saldo").ToString)
                    Exit Sub
                End If
            End If
        Next

    End Sub

    Sub CargarNotaCreditoAplicar2()
        For Each oRow As DataRow In dtCargarNotaDebito.Rows()
            If oRow("Sel") = True Then
                If oRow("IDTransaccion").ToString = IDTransaccionNotaCredito Then
                    txtFechaNotaCredito.txt.Text = oRow("Fec").ToString
                    txtComprobanteNotaCredito.txt.Text = oRow("NroComprobante").ToString
                    txtMonedaNotaCredito.txt.Text = oRow("Moneda").ToString
                    txtCotizacionNC.txt.Text = oRow("Cotizacion").ToString
                    txtObservacion.txt.Text = oRow("Observacion").ToString
                    txtTotalNotaCredito.SetValue(oRow("Total").ToString)
                    txtSaldoNotaCredito.SetValue(oRow("Saldo").ToString)
                    txtDebitoNotaCredito.SetValue(oRow("Totales").ToString)
                    Exit Sub
                End If
            End If
        Next

    End Sub

    Sub ListarVentas()



        lvVentas.Items.Clear()

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dtVentasPendientes.Rows
            If oRow("Sel") = True Then
                Dim item As ListViewItem = New ListViewItem(oRow("IDTransaccion").ToString)
                item.SubItems.Add(oRow("Comprobante").ToString)
                item.SubItems.Add(oRow("Moneda").ToString)
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Cotizacion").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Cobrado").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Acreditado").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Saldo").ToString))
                item.SubItems.Add(CSistema.FormatoMoneda(oRow("Importe").ToString))

                Total = Total + CDec(oRow("Importe").ToString)

                lvVentas.Items.Add(item)
            End If
        Next

        txtTotalVenta.SetValue(Total)
        CalcularTotales()

    End Sub

    Sub EliminarVenta()

        For Each item As ListViewItem In lvVentas.SelectedItems

            Dim IDtransaccion As String = item.SubItems(0).Text

            'Buscamos en Facturas Pendientes
            For Each oRow As DataRow In dtVentasPendientes.Select(" IDTransaccion = " & IDtransaccion & "")


                oRow("Sel") = False
                Exit For

            Next
        Next

        ListarVentas()

    End Sub

    Sub CalcularTotales()

        Dim TotalVentas As Decimal
        Dim TotalDebito As Decimal
        Dim SaldoTotal As Decimal

        TotalVentas = txtTotalVenta.ObtenerValor
        TotalDebito = txtDebitoNotaCredito.ObtenerValor

        'Calcular
        SaldoTotal = TotalVentas - TotalDebito
        If txtDebitoNotaCredito.txt.Text = 0 Then
            txtSaldoTotal.txt.Text = 0
        Else
            txtSaldoTotal.SetValue(SaldoTotal)
        End If



    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean
        InsertarDetalle = True
        For Each oRow As DataRow In dtVentasPendientes.Select(" Sel = 'True' ")
            Dim sql As String
            sql = "Insert Into NotaDebitoVentaAplicada (IDTransaccionNotaDebitoAplicacion, IDTransaccionNotaDebito, IDTransaccionVenta, Importe, Cobrado, Acreditado, Saldo) Values (" & IDTransaccion & "," & IDTransaccionNotaCredito & "," & oRow("IDTransaccion").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Acreditado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString) & ") "
            If CSistema.ExecuteNonQuery(sql) = 0 Then
                Return False
            End If
        Next
    End Function

    Private Sub frmAplicacionNotaCredito_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmAplicacionNotaCredito_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            If txtCliente.Focused = True Then
                Exit Sub
            End If

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmAplicacionNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado

        ObtenerNotaCredito()
        cbxMoneda.cbx.Focus()

    End Sub

    Private Sub bntAgregarNotacredito_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntAgregarNotacredito.Click
        CargarNotaCredito()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()

    End Sub

    Private Sub btnAgregarFactura_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregarFactura.Click
        CargarVenta()
    End Sub

    Private Sub lklEliminarFactura_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarFactura.LinkClicked
        EliminarVenta()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VNotaCreditoAplicacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VNotaCreditoAplicacion Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub txtDebitoNotaCredito_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDebitoNotaCredito.Leave
        CalcularTotales()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
    '10-06-2021 - SC - Actualiza datos
    Sub frmAplicacionNotaDebitoCliente_Activate()
        Me.Refresh()
    End Sub
End Class


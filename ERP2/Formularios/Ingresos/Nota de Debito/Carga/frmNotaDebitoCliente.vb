﻿Imports ERP.Reporte
Public Class frmNotaDebitoCliente

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CAsiento As New CAsientoNotaDebito
    Dim CReporte As New CReporteNotaDebito
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtVentas As New DataTable
    Dim dtDetalleVenta As New DataTable
    Dim dtNotaDebitoVenta As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dtPuntoExpedicion As DataTable
    Dim vHabilitar As Boolean
    Dim vConComprobantes As Boolean
    Dim vObservacionDevolucion As String
    Dim vObservacionDescuento As String

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        txtProducto.Conectar()
        txtCliente.Conectar()
        OcxImpuesto1.Inicializar()
        OcxImpuesto1.dg.ReadOnly = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "NOTA DEBITO", "NDE")
        vNuevo = True
        vHabilitar = True
        vConComprobantes = True

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        gbxCabecera.BringToFront()
        pnlDevolucion.Dock = DockStyle.Fill
        pnlDescuento.Dock = DockStyle.Fill

        'Variables
        vObservacionDescuento = "DEBITO"
        vObservacionDevolucion = "REPOSICION DE MERCADERIAS"

        'Funciones
        CargarInformacion()
        'ObtenerInformacionPuntoVenta()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        'CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        'Foco
        txtNroComprobante.Focus()
        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        MoverRegistro(New KeyEventArgs(Keys.End))

        txtNroComprobante.txt.SelectAll()
        CargarOperacion()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Combrobante
        'CSistema.CargaControl(vControles, cbxPuntoExpedicion)

        'Cabecera
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCotizacion)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, rdDebito)
        CSistema.CargaControl(vControles, rdbReposicion)
        CSistema.CargaControl(vControles, chkAplicar)

        'Detalle
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, txtCostoUnitario)
        CSistema.CargaControl(vControles, txtImporte)
        CSistema.CargaControl(vControles, lvLista)

        'Decuento
        CSistema.CargaControl(vControles, lklAgregarDetalleDescuento)
        CSistema.CargaControl(vControles, lklEliminarDetalleDescuento)
        CSistema.CargaControl(vControles, lklAgregarComprobanteDescuento)
        CSistema.CargaControl(vControles, lklEliminarComprobanteDescuento)
        CSistema.CargaControl(vControles, lvListaDescuento)
        CSistema.CargaControl(vControles, dgw)

        'Detalles
        dtDetalle = CData.GetStructure("VDetalleNotaDebito", "Select Top(0) *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0  From VDetalleNotaDebito")
        dtDetalleVenta = CData.GetStructure("VDetalleVenta", "Select TOP(0) * , 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetalleVenta ORDER BY")
        dtDescuento = CData.GetStructure("VDescuento", "Select Top(0) * From VDescuento")

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR CONTROLES
        'Ciudad
        '  CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")


        'Puntos de Expediciones
        dtPuntoExpedicion = CData.GetTable("VPuntoExpedicion").Copy
        dtPuntoExpedicion = CData.FiltrarDataTable(dtPuntoExpedicion, " IDOperacion = " & IDOperacion & " And Estado='True' ")
        txtIDTimbrado.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIMBRADO", 0))
        ObtenerInformacionPuntoVenta()

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        ''Ciudades
        'cbxCiudad.Conectar("", "Descripcion")

        ''Sucursal
        'cbxSucursal.Conectar()

        'Depositos
        cbxDeposito.Conectar()
        cbxDeposito.cbx.Text = vgDeposito

        'Monedas
        cbxMoneda.Conectar()
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        ''Tipo de Comprobante
        'cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")
        'JGr 20140816
        'Tipo de Comprobante
        Try
            CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select TOP 1 ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion & " And Codigo = '" & "NDE" & "'")
            cbxTipoComprobante.cbx.SelectedIndex = 0

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al cargar tipo de comprobante", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        'Impuestos
        OcxImpuesto1.EstablecerSoloLectura()

        'Radio Button
        rdbReposicion.Checked = True


        'checK


    End Sub

    Sub GuardarInformacion()

       
        'TIMBRADO
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIMBRADO", txtIDTimbrado.GetValue)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As CSistema.NUMHabilitacionBotonesRegistros)

        'Configurar botones
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnula, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnEliminar)

    End Sub

    Sub Nuevo()

        'Configurar botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        ''Limpiar detalle
        dtDetalle.Rows.Clear()
        dtDescuento.Rows.Clear()
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        OcxImpuesto1.CargarValores(dtDetalle)
        dtNotaDebitoVenta.Rows.Clear()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True
        CAsiento.Inicializar()

        'CheckBox
        chkAplicar.Valor = False
        rdbReposicion.Checked = True
        txtObservacion.SetValue(vObservacionDevolucion)

        'Cabecera
        txtFecha.txt.Text = ""
        cbxComprobante.cbx.Text = ""
        cbxTipoComprobante.cbx.Text = ""
        txtCliente.Clear()

        txtCostoUnitario.SoloLectura = True
        txtImporte.SoloLectura = True
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        chkAplicar.Valor = False
        rdDebito.Enabled = True
        rdbReposicion.Enabled = True
        chkAplicar.Enabled = True

        'Limpiar Detalle
        lvLista.Items.Clear()
        cbxComprobante.cbx.Text = " "

        'Obtener registro nuevo
        ObtenerInformacionPuntoVenta()

        'Poner el foco en el proveedor
        txtNroComprobante.txt.SelectAll()
        txtNroComprobante.txt.Focus()

        'limpiar Detalle Descuento
        lvListaDescuento.Items.Clear()
        dgw.Rows.Clear()
        txtTotalDescuento.txt.Text = ""
        txtSaldo.txt.Text = ""
        txtDescontados.txt.Text = ""
        txtCantidadCobrado.txt.Text = ""
        cbxComprobante.SoloLectura = False
        txtObservacion.SoloLectura = False
        dgw.ReadOnly = False
        txtObservacionProducto.SoloLectura = False
        txtProducto.txt.Text = ""

        txtCotizacion.txt.Text = 1
    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Seleccion de Punto de Expedicion

        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el punto de venta!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If


        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtNroComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asignamos entero al nro de comprobante
        txtNroComprobante.txt.Text = CInt(txtNroComprobante.txt.Text)

        If CInt(txtNroComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Cliente
        If txtCliente.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If txtCliente.Registro Is Nothing Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
            Dim mensaje As String = "Seleccione correctamente el cliente!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Moneda
        If IsNumeric(cbxMoneda.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el tipo de moneda!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Existencia en Detalle
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If dtDetalle.Rows.Count = 0 Then
                Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Total
            If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
                Dim mensaje As String = "El importe del documento no es valido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Validar el Asiento
            If CAsiento.ObtenerSaldo <> 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If

        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ValidarDocumento = True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        GenerarAsiento()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        'Punto de Expedicion
        'Dim PuntoExpedicionRow As DataRow = CData.GetTable("VTerminalPuntoExpedicion", " IDTerminal = " & vgIDTerminal).Select("IDPuntoExpedicion=" & txtIDTimbrado.GetValue)(0)
        Dim PuntoExpedicionRow As DataRow = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion).Select("ID=" & txtIDTimbrado.GetValue)(0)

        CSistema.SetSQLParameter(param, "@IDPuntoExpedicion", txtIDTimbrado.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", PuntoExpedicionRow("IDTipoComprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtNroComprobante.GetValue, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Comprobante", PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", PuntoExpedicionRow("IDSucursal").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Aplicar", chkAplicar.Valor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Debito", rdDebito.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Reposicion", rdbReposicion.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ConComprobantes", vConComprobantes, ParameterDirection.Input)

        'Saldo
        If rdbReposicion.Checked = True Then
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total")), ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(txtSaldo.ObtenerValor), ParameterDirection.Input)
        End If

        'Otros


        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx, ParameterDirection.Input)
        If txtCotizacion.ObtenerValor = 0 Then
            CSistema.SetSQLParameter(param, "@Cotizacion", "1".ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.ObtenerValor, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalImpuesto")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDescuento", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDescuento")), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaDebito", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From Venta Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaDebito", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
            CargarOperacion(IDTransaccion)
        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar 
            Procesar = InsertarDetalle(IDTransaccion)

            'Cargamos el DetalleImpuesto
            CDetalleImpuesto.Guardar(IDTransaccion)

        End If

        If Procesar = True Then

            Try

                ReDim param(-1)

                CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

                'Informacion de Salida
                CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
                CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

                'Aplicar
                If CSistema.ExecuteStoreProcedure(param, "SpNotaDebitoProcesar", False, False, MensajeRetorno) = False Then

                End If


            Catch ex As Exception

            End Try

        End If


        'Si es nuevo
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

            'Cargamos el asiento
            'Guardar el Asiento
            If CAsiento.dtDetalleAsiento.Rows.Count > 0 Then
                CAsiento.IDTransaccion = IDTransaccion
                CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
            End If

            'Imprimimos
            'Imprimir()

        End If


        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)
        CargarOperacion(IDTransaccion)

        txtNroComprobante.SoloLectura = False

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "Nota de Debito: " & cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante() & "  -  " & txtCliente.txtRazonSocial.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vNotaDebito Where Comprobante='" & ObtenerNroComprobante() & "' And IDPuntoExpedicion=" & txtIDTimbrado.GetValue & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtCliente.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(txtCliente, mensaje)
                ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = "Venta: " & cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante() & "  -  " & txtCliente.txtRazonSocial.Text

            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento
            frm.CAsiento.Bloquear = CAsiento.Bloquear

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                VisualizarAsiento()
            End If

        End If

    End Sub

    Sub GenerarAsiento()


        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = cbxMoneda.GetValue
        oRow("Cotizacion") = txtCotizacion.ObtenerValor
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = ObtenerNroComprobante()
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & ObtenerNroComprobante()
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Bloquear") = CAsiento.Bloquear
        oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        If CAsiento.Bloquear = False Then
            CAsiento.dtDetalleAsiento.Rows.Clear()
        End If

        CAsiento.IDCliente = txtCliente.Registro("ID").ToString

        OcxImpuesto1.Generar()
        CAsiento.dtDetalleAsiento.Rows.Clear()
        CAsiento.dtImpuesto = OcxImpuesto1.dtImpuesto.Copy
        CAsiento.dtDetalleProductos = dtDetalle
        CAsiento.dtDescuento = dtDescuento
        CAsiento.IDCliente = txtCliente.Registro("ID").ToString
        CAsiento.Generar()

    End Sub

    Sub CargarDescuento(ByVal Venta As DataRow)

        'Variables
        Dim vIDTransaccionVenta As Integer = Venta("IDTransaccion")
        Dim vIDProducto As Integer = Venta("ID")
        Dim vID As Integer = 0

        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Select * From VDescuento Where IDTransaccion=" & vIDTransaccionVenta & " And IDProducto=" & vIDProducto & " And ID=" & vID & " ").Copy

        If dttemp Is Nothing Then
            Exit Sub
        End If

        'Agregar
        For Each oRow As DataRow In dttemp.Rows
            Dim NewRow As DataRow = dtDescuento.NewRow
            NewRow.ItemArray = oRow.ItemArray

            If NewRow("Descuento") = 0 Then
                GoTo siguiente
            End If

            'Descuento 
            Dim Descuento As Decimal = NewRow("Descuento")
            Dim DescuentoDiscriminado As Decimal = 0
            Dim TotalDescuentoDiscriminado As Decimal = 0

            NewRow("Cantidad") = txtCantidad.ObtenerValor
            NewRow("Total") = txtCantidad.ObtenerValor * Descuento

            CSistema.CalcularIVA(Venta("IDImpuesto"), Descuento, False, True, DescuentoDiscriminado)
            NewRow("DescuentoDiscriminado") = DescuentoDiscriminado

            TotalDescuentoDiscriminado = NewRow("Cantidad") * DescuentoDiscriminado
            NewRow("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

            dtDescuento.Rows.Add(NewRow.ItemArray)

siguiente:

        Next

    End Sub

    Sub EliminarDescuentos()

        For Each item As ListViewItem In lvLista.SelectedItems
            Dim IDProducto As Integer = item.Text
            Dim ID As Integer = item.Index

            For i As Integer = dtDescuento.Rows.Count - 1 To 0 Step -1
                Dim oRow As DataRow = dtDescuento.Rows(i)
                If oRow("IDProducto") = IDProducto And oRow("ID") = ID Then
                    dtDescuento.Rows.RemoveAt(i)
                End If
            Next

        Next

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaDebito", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From NotaDebito Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaDebito", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)



    End Sub

    Sub Eliminar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        'Consultar
        If MessageBox.Show("Desea eliminar el comprobante?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
            Exit Sub
        End If

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpNotaDebito", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From NotaDebito Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpNotaDebito", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

        txtNroComprobante.Focus()
        MoverRegistro(New KeyEventArgs(Keys.End))

    End Sub

    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        'Otros
        Dim frm As New frmConsultaNotaDebitoCliente
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        Seleccionardescuento()
        Dim i As Integer = 0
        If rdbReposicion.Checked = True Then
            For Each oRow As DataRow In dtDetalle.Rows

                Dim sql As String = ""
                Dim sql2 As String = ""

                If chkAplicar.Valor = True Then

                    sql = "Insert Into DetalleNotaDebito (IDTransaccion, IDProducto, ID, IDTransaccionVenta, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & ", NULL," & oRow("IDDeposito").ToString & ",'" & oRow("Observacion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCosto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & " ,'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & " )"

                    If vConComprobantes = True Then
                        sql2 = "Insert Into NotaDebitoVenta (IDTransaccionNotaDebito, IDTransaccionVentaGenerada, ID, Importe, Cobrado, Acreditado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccionVenta").ToString & ", " & i & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString) & ")"
                    End If

                Else
                    sql = "Insert Into DetalleNotaDebito (IDTransaccion, IDProducto, ID, IDTransaccionVenta, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDTransaccionVenta").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Observacion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCosto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & ",'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & " )"
                    sql2 = "Insert Into NotaDebitoVenta (IDTransaccionNotaDebito, IDTransaccionVentaGenerada, ID, Importe, Cobrado, Acreditado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccionVenta").ToString & ", " & i & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString) & ")"
                End If

                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If

                If rdbReposicion.Checked = True Then
                    If vConComprobantes = True Then
                        If CSistema.ExecuteNonQuery(sql2) = 0 Then
                            Return False
                        End If
                    End If
                End If

                i = i + 1

            Next
        End If

        If rdDebito.Checked = True Then

            If chkAplicar.Valor = False Then

                For Each oRow As DataRow In dtNotaDebitoVenta.Select(" Sel = 'True' ")
                    Dim sql As String
                    sql = "Insert Into NotaDebitoVenta (IDTransaccionNotaDebito, IDTransaccionVentaGenerada, ID, Importe, Cobrado, Acreditado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccion").ToString & ", " & i & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString) & ") "

                    If CSistema.ExecuteNonQuery(sql) = 0 Then
                        Return False
                    End If

                    i = i + 1

                Next

                For Each oRow As DataRow In dtDetalle.Rows
                    Dim sql2 As String = ""
                    sql2 = "Insert Into DetalleNotaDebito (IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Observacion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString) & "," & oRow("TotalCosto").ToString & "," & oRow("TotalDescuento").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & ",'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & " )"
                    If CSistema.ExecuteNonQuery(sql2) = 0 Then
                        Return False
                    End If
                Next
            Else
                For Each oRow As DataRow In dtNotaDebitoVenta.Select(" Sel = 'True' ")
                    Dim sql As String
                    sql = "Insert Into NotaDebitoVenta (IDTransaccionNotaDebito, IDTransaccionVentaGenerada, ID, Importe, Cobrado, Acreditado, Saldo) Values (" & IDTransaccion & "," & oRow("IDTransaccion").ToString & ", " & i & ", " & CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Cobrado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Descontado").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString) & ") "
                    If CSistema.ExecuteNonQuery(sql) = 0 Then
                        Return False
                    End If

                    i = i + 1

                Next

                For Each oRow As DataRow In dtDetalle.Rows
                    Dim sql2 As String = ""
                    sql2 = "Insert Into DetalleNotaDebito (IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, CostoUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalCostoImpuesto, TotalCosto, TotalDescuento, TotalDiscriminado, TotalCostoDiscriminado, Caja, CantidadCaja) Values(" & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & "," & oRow("IDDeposito").ToString & ",'" & oRow("Descripcion").ToString & "'," & oRow("IDImpuesto").ToString & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("CostoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("DescuentoUnitario").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PorcentajeDescuento").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString) & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalCostoImpuesto").ToString) & "," & oRow("TotalCosto").ToString & "," & oRow("TotalDescuento").ToString & "," & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & ", " & CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString) & ",'" & oRow("Caja").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("CantidadCaja").ToString) & " )"
                    If CSistema.ExecuteNonQuery(sql2) = 0 Then
                        Return False
                    End If
                Next
            End If

        End If

    End Function

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        cbxComprobante.SoloLectura = True
        txtObservacion.SoloLectura = True
        txtObservacionProducto.SoloLectura = True

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtNroComprobante.txt.Focus()
        txtNroComprobante.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VNotaDebito Where Comprobante = '" & ObtenerNroComprobante() & "' And IDTipoComprobante=" & cbxTipoComprobante.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VNotaDebito Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select *, 'Cobrado'=0, 'Descontado'=0, 'Saldo'=0 From VDetalleNotaDebito Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtNroComprobante, mensaje)
            ctrError.SetIconAlignment(txtNroComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxDeposito.txt.Text = oRow("Deposito").ToString
        cbxMoneda.txt.Text = oRow("Moneda").ToString
        txtCotizacion.txt.Text = oRow("Cotizacion").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        chkAplicar.Valor = oRow("Aplicar").ToString
        rdDebito.Checked = oRow("Debito").ToString
        rdbReposicion.Checked = oRow("Reposicion").ToString
        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        ListarDetalleDescuento()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()
        dgw.ReadOnly = True
        CargarComprobanteDescuentofacturados(IDTransaccion)

        calcularTotalDescuento()

        txtSaldo.txt.Text = ""

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub Cancelar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR, btnNuevo, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

        txtNroComprobante.txt.ReadOnly = False
        txtNroComprobante.txt.Focus()
        cbxComprobante.SoloLectura = True
        txtObservacionProducto.SoloLectura = True
        'txtProducto.txt.sololectura = True

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroComprobante), 1) From VNotaDebito Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

        txtNroComprobante.txt.Text = ID
        txtNroComprobante.txt.SelectAll()
        CargarOperacion()

    End Sub

    Sub CalcularImporte(ByVal Decimales As Boolean, ByVal Redondear As Boolean, ByVal PorTotal As Boolean)

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Cantidad As Decimal
        Dim PrecioUnitario As Decimal
        Dim Importe As Decimal

        Cantidad = txtCantidad.ObtenerValor

        'Validar
        If Cantidad = 0 Then
            Dim mensaje As String = "No se puede calcular los montos si la cantidad es 0!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If PorTotal = True Then
            PrecioUnitario = txtImporte.ObtenerValor / Cantidad
        Else
            Importe = txtCostoUnitario.ObtenerValor * Cantidad
        End If

        If Decimales = False Then
            PrecioUnitario = CInt(Math.Round(PrecioUnitario, 0))
            Importe = CLng(Math.Round(Importe, 0))
        Else
            If Redondear = True Then
                PrecioUnitario = CInt(Math.Round(PrecioUnitario, 0))
                Importe = CInt(Math.Round(Importe, 0))
            End If
        End If

        If PorTotal = True Then
            txtCostoUnitario.txt.Text = PrecioUnitario.ToString
        Else
            txtImporte.txt.Text = Importe.ToString
        End If

    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        lvLista.Items.Clear()

        'Variables
        Dim Total As Decimal = 0
        Dim Saldo As Decimal = 0
        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvLista.Items.Add(oRow("IDProducto").ToString)

            Dim Descripcion As String
            If oRow("Observacion").ToString.Trim.Length > 0 Then
                Descripcion = oRow("Descripcion").ToString.Trim & " - " & oRow("Observacion").ToString.Trim
            Else
                Descripcion = oRow("Descripcion").ToString.Trim

            End If

            item.SubItems.Add(oRow("Descripcion").ToString)
            item.SubItems.Add(oRow("Comprobante").ToString)
            item.SubItems.Add(oRow("Ref Imp").ToString)
            item.SubItems.Add(CSistema.FormatoNumero(oRow("Cantidad").ToString))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("PrecioUnitario").ToString))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))


        Next

        Bloquear()

    End Sub

    Sub CargarProducto()

        If vConComprobantes = True Then
            CargarProductoConComprobante()
        Else
            CargarProductoSinComprobante()
        End If

    End Sub

    Sub CargarProductoConComprobante()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxDeposito.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim CantidadProducto As Decimal = txtCantidad.ObtenerValor
        Dim CantidadCaja As Integer = CantidadProducto / txtProducto.Registro("UnidadPorCaja")

        'Obtenemos la cantidad que existe en el comprobante
        Dim CantidadComprobante As Decimal = txtProducto.Registro("SaldoCantidad").ToString

        'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
        For Each oRow As DataRow In dtDetalle.Rows

            If cbxComprobante.GetValue = oRow("IDtransaccionVenta") And txtProducto.Registro("ID") = oRow("IDProducto") Then
                CSistema.MostrarError("No puede ingresar el mismo producto 2 veces", ctrError, txtProducto, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If

        Next

        'No se controla la cantidad en nota de debito
        'If CantidadProducto > CantidadComprobante Then
        '    CSistema.MostrarError("La cantidad no puede ser mayor al del comprobante (" & CSistema.FormatoNumero(CantidadComprobante.ToString) & ")", ctrError, txtCantidad, tsslEstado, ErrorIconAlignment.MiddleRight)
        '    Exit Sub
        'End If

        'Cargamos el registro en el detalle
        dtDetalle.Columns("IDTransaccionVenta").AllowDBNull = True

        Dim dRow As DataRow = dtDetalle.NewRow()
        Dim VentaRow As DataRow = dtDetalleVenta.Select("IDTransaccion= " & cbxComprobante.GetValue & " And ID = " & txtProducto.Registro("ID") & " ")(0)

        Try

            'Obtener Valores
            'Productos
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("IDTransaccionVenta") = cbxComprobante.GetValue

            If txtObservacionProducto.txt.Text.Trim.Length = 0 Then
                dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
            Else
                dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString & " - " & txtObservacionProducto.txt.Text
            End If

            dRow("CodigoBarra") = VentaRow("CodigoBarra").ToString
            dRow("Observacion") = txtObservacionProducto.txt.Text

            'Deposito
            dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
            dRow("Deposito") = cbxDeposito.cbx.Text

            'Cantidad y Precios
            dRow("Cantidad") = CantidadProducto

            Dim PrecioUnitario As Decimal
            PrecioUnitario = VentaRow("PrecioUnitario") - VentaRow("DescuentoUnitario")

            dRow("PrecioUnitario") = PrecioUnitario
            dRow("Pre. Uni.") = PrecioUnitario
            dRow("Total") = PrecioUnitario * CantidadProducto

            'Impuestos
            dRow("IDImpuesto") = VentaRow("IDImpuesto").ToString
            dRow("Impuesto") = VentaRow("Impuesto").ToString
            dRow("Ref Imp") = VentaRow("Ref Imp").ToString
            dRow("Exento") = VentaRow("Exento").ToString
            dRow("TotalImpuesto") = 0
            dRow("TotalDiscriminado") = 0
            CSistema.CalcularIVA(dRow("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

            'Costo
            dRow("CostoUnitario") = VentaRow("Costo").ToString
            dRow("TotalCosto") = CantidadProducto * VentaRow("Costo").ToString
            dRow("TotalCostoImpuesto") = 0
            dRow("TotalCostoDiscriminado") = dRow("TotalCosto")
            CSistema.CalcularIVA(VentaRow("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), False, True, 0, dRow("TotalCostoImpuesto"))

            'Descuentos
            dRow("PorcentajeDescuento") = VentaRow("PorcentajeDescuento")
            dRow("DescuentoUnitario") = VentaRow("DescuentoUnitario")
            dRow("DescuentoUnitarioDiscriminado") = VentaRow("DescuentoUnitarioDiscriminado")
            CSistema.CalcularIVA(VentaRow("IDImpuesto"), VentaRow("DescuentoUnitario"), False, True, dRow("DescuentoUnitarioDiscriminado"))

            dRow("TotalDescuento") = VentaRow("DescuentoUnitario") * CantidadProducto
            dRow("TotalDescuentoDiscriminado") = dRow("DescuentoUnitarioDiscriminado") * CantidadProducto

            'Totales
            dRow("TotalBruto") = dRow("Total")
            dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
            dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
            dRow("TotalNeto") = 0
            dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + dRow("TotalDescuentoDiscriminado")

            'Venta
            dRow("IDTransaccionVenta") = cbxComprobante.GetValue
            dRow("Comprobante") = cbxComprobante.cbx.Text

            Try
                dRow("Cobrado") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Cobrado")
                dRow("Descontado") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Descontado")
                dRow("Saldo") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Saldo")
            Catch ex As Exception

            End Try

            'Cantidad Caja
            dRow("Caja") = 0
            dRow("CantidadCaja") = CantidadCaja

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

            'Cargar Descuento
            CargarDescuento(VentaRow)

            ListarDetalle()
            CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
            CalcularTotales()

            'Inicializamos los valores
            txtObservacionProducto.txt.Clear()
            txtCantidad.txt.Text = "0"
            txtImporte.txt.Text = "0"
            txtCostoUnitario.txt.Text = "0"
            txtProducto.txt.Clear()

            'Retorno de Foco
            txtProducto.txt.SelectAll()
            txtProducto.txt.Focus()

            ctrError.Clear()
            tsslEstado.Text = ""

        Catch ex As Exception

        End Try


    End Sub

    Sub CargarProductoSinComprobante()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxDeposito.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el deposito!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim CantidadProducto As Decimal = txtCantidad.ObtenerValor
        Dim CantidadCaja As Integer = CantidadProducto / txtProducto.Registro("UnidadPorCaja")




        If vHabilitar = True Then

            'Obtenemos la cantidad que existe en el comprobante
            Dim CantidadComprobante As Decimal = txtProducto.Registro("SaldoCantidad").ToString

            'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
            For Each oRow As DataRow In dtDetalle.Rows

                If cbxComprobante.GetValue = oRow("IDtransaccionVenta") And txtProducto.Registro("ID") = oRow("IDProducto") Then
                    CSistema.MostrarError("No puede ingresar el mismo producto 2 veces", ctrError, txtProducto, tsslEstado, ErrorIconAlignment.MiddleRight)
                    Exit Sub
                End If

            Next

            If CantidadProducto > CantidadComprobante Then
                CSistema.MostrarError("La cantidad no puede ser mayor al del comprobante (" & CSistema.FormatoNumero(CantidadComprobante.ToString) & ")", ctrError, txtCantidad, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If


        End If


        'Cargamos el registro en el detalle
        dtDetalle.Columns("IDTransaccionVenta").AllowDBNull = True

        Dim dRow As DataRow = dtDetalle.NewRow()

        'Obtener Valores
        'Productos
        dRow("IDProducto") = txtProducto.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("IDTransaccionVenta") = cbxComprobante.GetValue

        If txtObservacionProducto.txt.Text.Trim.Length = 0 Then
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
        Else
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString & " - " & txtObservacionProducto.txt.Text
        End If

        dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString
        dRow("Observacion") = txtObservacionProducto.txt.Text

        'Deposito
        dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
        dRow("Deposito") = cbxDeposito.cbx.Text

        'Cantidad y Precios
        dRow("Cantidad") = CantidadProducto

        Dim PrecioUnitario As Decimal

        If vHabilitar = True Then
            PrecioUnitario = CDec(txtProducto.Registro("PrecioUnitario").ToString)
        Else
            PrecioUnitario = txtCostoUnitario.txt.Text
        End If

        dRow("PrecioUnitario") = PrecioUnitario
        dRow("Pre. Uni.") = PrecioUnitario
        dRow("Total") = PrecioUnitario * CantidadProducto

        'Impuestos
        dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
        dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString
        dRow("Ref Imp") = txtProducto.Registro("Ref Imp").ToString
        dRow("TotalImpuesto") = 0
        dRow("TotalDiscriminado") = 0
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

        'Costo
        dRow("CostoUnitario") = txtProducto.Registro("Costo").ToString
        dRow("TotalCosto") = CantidadProducto * txtProducto.Registro("Costo").ToString
        dRow("TotalCostoImpuesto") = 0
        dRow("TotalCostoDiscriminado") = 0
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalCosto").ToString), False, True, dRow("TotalCostoDiscriminado"), dRow("TotalCostoImpuesto"))

        'Descuento
        dRow("TotalDescuento") = 0
        dRow("DescuentoUnitario") = 0
        dRow("PorcentajeDescuento") = 0
        dRow("TotalDescuentoDiscriminado") = 0

        'Totales
        dRow("TotalBruto") = 0
        dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
        dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
        dRow("TotalNeto") = 0
        dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + dRow("TotalDescuentoDiscriminado")

        'Venta
        dRow("IDTransaccionVenta") = cbxComprobante.GetValue
        dRow("Comprobante") = cbxComprobante.cbx.Text

        Try
            dRow("Cobrado") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Cobrado")
            dRow("Descontado") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Descontado")
            dRow("Saldo") = dtVentas.Select(" IDTransaccion = " & cbxComprobante.GetValue)(0)("Saldo")
        Catch ex As Exception

        End Try

        'Cantidad Caja
        dRow("Caja") = 0
        dRow("CantidadCaja") = CantidadCaja

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtImporte.txt.Text = "0"
        txtCostoUnitario.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub CargarDescuentos()

        'Validar
        'Cliente

        Dim frm As New frmNotaCreditoCargarDescuento
        FGMostrarFormulario(Me, frm, "Cargar detalle", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()

        'Obtener Valores
        'Productos
        dRow("IDProducto") = frm.Registro("ID").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("Descripcion") = frm.Registro("Descripcion").ToString
        dRow("CodigoBarra") = frm.Registro("CodigoBarra").ToString
        dRow("Observacion") = frm.Observacion

        'Deposito
        dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
        dRow("Deposito") = cbxDeposito.cbx.Text

        'Cantidad y Precios
        dRow("Total") = frm.Importe

        'Impuestos
        dRow("IDImpuesto") = frm.Registro("IDImpuesto").ToString
        dRow("Impuesto") = frm.Registro("Impuesto").ToString
        dRow("Ref Imp") = frm.Registro("Ref Imp").ToString
        dRow("TotalImpuesto") = 0
        dRow("TotalDiscriminado") = 0
        CSistema.CalcularIVA(dRow("IDImpuesto"), CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

        'Costo
        dRow("CostoUnitario") = 0
        dRow("TotalCosto") = 0
        dRow("TotalCostoImpuesto") = 0
        dRow("TotalCostoDiscriminado") = 0

        'Descuento
        dRow("TotalDescuento") = 0
        dRow("DescuentoUnitario") = 0
        dRow("PorcentajeDescuento") = 0
        dRow("TotalDescuentoDiscriminado") = 0

        'Totales
        dRow("TotalBruto") = 0
        dRow("TotalSinDescuento") = dRow("Total") - dRow("TotalDescuento")
        dRow("TotalSinImpuesto") = dRow("Total") - dRow("TotalImpuesto")
        dRow("TotalNeto") = 0
        dRow("TotalNetoConDescuentoNeto") = dRow("TotalDiscriminado") + dRow("TotalDescuentoDiscriminado")

        'Cantidad Caja
        dRow("Caja") = False
        dRow("CantidadCaja") = 1

        'Agregamos al detalle
        dtDetalle.Rows.Add(dRow)

        ListarDetalleDescuento()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub ListarDetalleDescuento()

        'Limpiamos todo el detalle
        lvListaDescuento.Items.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvListaDescuento.Items.Add(oRow("IDProducto").ToString)

            Dim Descripcion As String
            If oRow("Observacion").ToString.Trim.Length > 0 Then
                Descripcion = oRow("Descripcion").ToString.Trim & " - " & oRow("Observacion").ToString.Trim
            Else
                Descripcion = oRow("Descripcion").ToString.Trim
            End If

            item.SubItems.Add(Descripcion)
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Total").ToString))
            Total = Total + CDec(oRow("Total").ToString)

        Next

        txtTotalDescuento.txt.Text = Total

        If vNuevo Then
            calcularTotalDescuento()
        End If

        Bloquear()


    End Sub

    Sub EliminarProductoDevolucion()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If lvLista.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Eliminamos los descuentos
        EliminarDescuentos()

        dtDetalle.Rows(lvLista.SelectedIndices(0)).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            If Not oRow.RowState = DataRowState.Deleted Then

                'Renumerar descuentos
                For Each dRow As DataRow In dtDescuento.Select(" IDProducto=" & oRow("IDProducto") & " And ID=" & oRow("ID") & " ")
                    dRow("ID") = Index
                Next

                oRow("ID") = Index
                Index = Index + 1
            End If
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()
        Bloquear()

    End Sub

    Sub EliminarProductoDescuento()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If lvListaDescuento.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvListaDescuento, mensaje)
            ctrError.SetIconAlignment(lvListaDescuento, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        dtDetalle.Rows(lvListaDescuento.SelectedIndices(0)).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalleDescuento()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)
        CalcularTotales()

        Bloquear()

    End Sub

    Sub EliminarComprobanteDescuento()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(lvListaDescuento, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgw.SelectedRows
            Dim vIDTransaccion As Integer

            vIDTransaccion = oRow.Cells("colIDTransaccion").Value
            For Each dtRow As DataRow In dtNotaDebitoVenta.Select(" IDTransaccion = " & vIDTransaccion)
                dtRow("Sel") = False

            Next
        Next

        ListarComprobanteDescuento()


    End Sub

    Sub CargarComprobante()

        If txtCliente.Seleccionado = False Then
            Exit Sub
        End If

        CargarComprobanteDevolucion()
        ObtenerComprobanteDescuento()


    End Sub

    Sub CargarComprobanteDevolucion()

        ctrError.Clear()
        tsslEstado.Text = ""

        If vNuevo = False Then
            If txtCliente.Seleccionado = True Then
                Exit Sub
            End If
        End If

        If txtCliente.Seleccionado = False Then
            CSistema.MostrarError("Seleccione el cliente", ctrError, chkAplicar, tsslEstado)
            Exit Sub
        End If

        If chkAplicar.Valor = True Then
            dtVentas = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante, Cobrado, Descontado, Saldo From Venta Where DATEDIFF(DD, FechaEmision, GetDate()) <= 365 and IDCliente =" & txtCliente.Registro("ID").ToString).Copy
            dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaDebito V Where V.Dias <= 365 And V.IDCliente =" & txtCliente.Registro("ID").ToString).Copy
        Else
            dtVentas = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante, Cobrado, Descontado, Saldo From Venta Where Cancelado = 'False' and IDCliente =" & txtCliente.Registro("ID").ToString).Copy
            dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaDebito V Where V.Cancelado='False' And V.IDCliente =" & txtCliente.Registro("ID").ToString).Copy
        End If

        CSistema.SqlToComboBox(cbxComprobante.cbx, dtVentas, "IDTransaccion", "Comprobante")

    End Sub

    Sub ObtenerComprobanteDescuento()

        If txtCliente.Registro Is Nothing Then
            Exit Sub
        End If

        dtNotaDebitoVenta = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Cliente, Total, Cobrado, Descontado, Saldo, Condicion,Fec,[Fec. Venc.], 'Sel'='False', 'Importe'=0, 'Cancelar'='False' From VVenta where Cancelado = 'False' And Saldo>0 and IDCliente =" & txtCliente.Registro("ID").ToString).Copy

    End Sub

    Sub CargarComprobanteDescuento()

        Dim frm As New frmNotaCreditoCargarDocumentoDescuento
        frm.dt = dtNotaDebitoVenta
        FGMostrarFormulario(Me, frm, "Comprobantes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        dtNotaDebitoVenta = frm.dt
        ListarComprobanteDescuento()

    End Sub

    Sub ListarComprobanteDescuento()

        'Limpiar la grilla
        dgw.Rows.Clear()

        Dim dt As DataTable = dtNotaDebitoVenta.Copy
        For Each oRow As DataRow In dt.Select("Sel='True'")

            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cod.").ToString
            Registro(4) = oRow("Condicion").ToString
            Registro(5) = oRow("Fec. Venc.").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(7) = CSistema.FormatoMoneda(oRow("Cobrado").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("Descontado").ToString)
            Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(11) = oRow("Cancelar").ToString

            'Sumar el total de la deuda
            dgw.Rows.Add(Registro)

        Next

        calcularTotalDescuento()

    End Sub

    Sub CargarComprobanteDescuentofacturados(ByVal IDTransaccion As Integer)

        If txtCliente.Registro Is Nothing Then
            Exit Sub
        End If


        dtNotaDebitoVenta = CSistema.ExecuteToDataTable("Select IDTransaccion, Comprobante,[Cod.], Cliente, Total, NCV.Cobrado, NCV.Acreditado, NCV.Saldo, NCV.Importe, Condicion, Fec, [Fec. Venc.], 'Sel'='True', 'Cancelar'='False' From VVenta V Join NotaDebitoVenta NCV On V.IDTransaccion=NCV.IDTransaccionVentaGenerada  where IDTransaccionNotaDebito = " & IDTransaccion & " And IDCliente =" & txtCliente.Registro("ID").ToString).Copy

        'Limpiar la grilla
        dgw.Rows.Clear()
        'Dim TotalSaldo As Decimal = 0

        For Each oRow As DataRow In dtNotaDebitoVenta.Rows

            Dim Registro(11) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cod.").ToString
            Registro(4) = oRow("Condicion").ToString
            Registro(5) = oRow("Fec. Venc.").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(7) = CSistema.FormatoMoneda(oRow("Cobrado").ToString)
            Registro(8) = CSistema.FormatoMoneda(oRow("Acreditado").ToString)
            Registro(9) = CSistema.FormatoMoneda(oRow("Saldo").ToString)
            Registro(10) = CSistema.FormatoMoneda(oRow("Importe").ToString)
            Registro(11) = oRow("Cancelar").ToString

            'Sumar el total de la deuda

            'TotalSaldo = CDec(oRow("Saldo").ToString) - txtTotalDescuento.txt.Text
            dgw.Rows.Add(Registro)

        Next

        ' txtDescuento.txt.ReadOnly = True
        'dgw.ReadOnly = True




    End Sub

    Function ObtenerNroComprobante() As String

        Dim dt As DataTable = CData.GetTable("VPuntoExpedicion", " IDOperacion = " & IDOperacion)

        If dt Is Nothing AndAlso dt.Rows.Count = 0 Then
            Return "0"
        End If

        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Return "0"
        End If

        If dt.Select("ID=" & txtIDTimbrado.GetValue).Count = 0 Then
            Return "0"
        End If

        Dim PuntoExpedicionRow As DataRow = dt.Select("ID=" & txtIDTimbrado.GetValue)(0)
        ObtenerNroComprobante = PuntoExpedicionRow("ReferenciaSucursal").ToString & "-" & PuntoExpedicionRow("ReferenciaPunto").ToString & "-" & txtNroComprobante.GetValue


    End Function

    Sub SeleccionarTimbrado()

        Dim frm As New frmSeleccionarTimbrado
        frm.ID = txtIDTimbrado.GetValue
        frm.IDOperacion = IDOperacion
        frm.IDSucursal = vgIDSucursal

        FGMostrarFormulario(Me, frm, "Timbrados", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Exit Sub
        End If

        txtIDTimbrado.SetValue(frm.ID)
        ObtenerInformacionPuntoVenta()

    End Sub

    Sub ObtenerInformacionPuntoVenta()

        txtTalonario.txt.Text = "0"
        txtVencimientoTimbrado.txt.Text = ""
        txtRestoTimbrado.txt.Text = "0"
        cbxTipoComprobante.cbx.Text = ""
        txtReferenciaSucursal.txt.Text = "000"
        txtReferenciaTerminal.txt.Text = "000"
        txtNroComprobante.txt.Text = "000"
        cbxCiudad.cbx.Text = ""
        cbxSucursal.cbx.Text = ""
        txtProducto.IDSucursal = vgIDSucursal
        'cbxCiudad.cbx.Enabled = True
        cbxSucursal.SoloLectura = False
        If IsNumeric(txtIDTimbrado.GetValue) = False Then
            Exit Sub
        End If

        If dtPuntoExpedicion Is Nothing Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtPuntoExpedicion.Select(" ID=" & txtIDTimbrado.GetValue)
            txtTimbrado.txt.Text = oRow("Timbrado").ToString
            txtTalonario.txt.Text = oRow("TalonarioActual").ToString
            txtVencimientoTimbrado.txt.Text = oRow("Vencimiento").ToString
            txtRestoTimbrado.txt.Text = oRow("Saldo").ToString
            cbxTipoComprobante.SelectedValue(oRow("IDTipoComprobante").ToString)
            txtReferenciaSucursal.txt.Text = oRow("ReferenciaSucursal").ToString
            txtReferenciaTerminal.txt.Text = oRow("ReferenciaPunto").ToString
            txtNroComprobante.txt.Text = oRow("ProximoNumero").ToString
            cbxCiudad.SelectedValue(oRow("IDCiudad").ToString)
            cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)

            'cbxCiudad.cbx.Text = oRow("Ciudad").ToString
            'cbxSucursal.cbx.Text = oRow("Suc").ToString
            ' cbxDeposito.cbx.Text = 
            txtProducto.IDSucursal = oRow("IDSucursal").ToString

            cbxCiudad.SoloLectura = True
            cbxSucursal.SoloLectura = True
        Next



    End Sub

    Sub calcularTotalDescuento()

        Dim TotalSaldo As Decimal = 0
        Dim Total As Decimal = 0
        Dim TotalComprobantes As Integer = 0

        For Each oRow As DataGridViewRow In dgw.Rows

            If oRow.Cells(1).Value = True Then
                Total = Total + oRow.Cells(10).Value
                TotalComprobantes = TotalComprobantes + 1
            End If

        Next
        TotalSaldo = txtTotalDescuento.ObtenerValor - Total
        txtDescontados.SetValue(Total)
        txtCantidadCobrado.SetValue(TotalComprobantes)
        txtSaldo.SetValue(TotalSaldo)


    End Sub

    Sub Aplica()

        If vNuevo = False Then
            Exit Sub
        End If

        If chkAplicar.Valor = True Then


            If MessageBox.Show("Desea aplicar con comprobantes?", "Aplicar", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                HabilitarConComprobante(True)
                vHabilitar = True
                ObtenerComprobanteDescuento()
                vConComprobantes = True
            Else
                HabilitarConComprobante(False)
                vHabilitar = False
                vConComprobantes = False
                If rdDebito.Checked = True Then
                    dgw.Rows.Clear()
                    'chkAplicar.Valor = False
                End If
            End If
        Else
            HabilitarConComprobante(True)
        End If


    End Sub

    Sub HabilitarConComprobante(ByRef Comprobante As Boolean)

        If Comprobante = True Then
            cbxComprobante.cbx.Text = ""
            cbxComprobante.SoloLectura = False
            CargarComprobanteDevolucion()
            txtCostoUnitario.SoloLectura = True
            txtImporte.SoloLectura = True

            'Descuento
            lklAgregarComprobanteDescuento.Enabled = True
            lklEliminarComprobanteDescuento.Enabled = True

        Else
            cbxComprobante.cbx.Text = ""
            cbxComprobante.SoloLectura = True
            txtProducto.Conectar()
            txtCostoUnitario.SoloLectura = False
            txtImporte.SoloLectura = False

            'Descuento
            lklAgregarComprobanteDescuento.Enabled = False
            lklEliminarComprobanteDescuento.Enabled = False

        End If

    End Sub

    Sub Seleccionardescuento()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dtNotaDebitoVenta.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells("colSel").Value
                oRow1("Importe") = CSistema.FormatoMonedaBaseDatos(oRow.Cells("colImporte").Value)
                oRow1("Cancelar") = oRow.Cells("colcancelar").Value
            Next

        Next



    End Sub

    Sub MoverRegistro(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtNroComprobante.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(NroComprobante), 1) From VNotaDebito Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(NroComprobante), 1) From VNotaDebito Where IDPuntoExpedicion=" & txtIDTimbrado.GetValue & " "), Integer)

            txtNroComprobante.txt.Text = ID
            txtNroComprobante.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub Bloquear()

        If dtDetalle.Rows.Count > 0 Then
            rdDebito.Enabled = False
            rdbReposicion.Enabled = False
            chkAplicar.Enabled = False
            cbxDeposito.SoloLectura = True
            txtCliente.SoloLectura = True
        Else
            rdDebito.Enabled = True
            rdbReposicion.Enabled = True
            chkAplicar.Enabled = True
            cbxDeposito.SoloLectura = False
            txtCliente.SoloLectura = False
        End If


        For i As Integer = 0 To dgw.Rows.Count - 1
            If dgw.Rows(i).Cells("colSel").Value = True Then
                rdDebito.Enabled = False
                rdbReposicion.Enabled = False
                chkAplicar.Enabled = False
                Exit For
            End If
        Next



    End Sub

    Sub Imprimir()

        Dim Total As Double = OcxImpuesto1.txtTotal.ObtenerValor
        Dim NumeroALetra As String = CSistema.NumeroALetra(OcxImpuesto1.txtTotal.ObtenerValor)

        Dim Where As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & IDTransaccion

        Dim Path As String = vgImpresionNotaCreditoPath


        CReporte.ImpresionNotaDebito(frm, Where, vgImpresionNotaCreditoPath, vgImpresionFacturaImpresora, NumeroALetra, CSistema.FormatoMoneda(Total))

    End Sub

    Private Sub frmNotaDebitoCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmNotaDebitoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            If txtCantidad.txt.Focused = True Then
                Exit Sub
            End If

            If txtImporte.txt.Focused = True Then
                Exit Sub
            End If

            If dgw.Focused = True Then
                Exit Sub
            End If

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmNotaDebitoCliente_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If txtCantidad.txt.Focused = False Then
            Exit Sub
        End If

        CalcularImporte(False, True, False)

        If e.KeyCode = Keys.Enter Then
            If vHabilitar = True Then
                CargarProducto()
            Else
                txtCostoUnitario.txt.Focus()
            End If

        End If
    End Sub

    Private Sub txtCostoUnitario_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCostoUnitario.TeclaPrecionada

        'If txtCostoUnitario.txt.Focused = False Then
        '    Exit Sub
        'End If

        CalcularImporte(False, True, False)

        If e.KeyCode = Keys.Enter Then
            If vHabilitar = True Then
                CargarProducto()
            Else
                txtImporte.txt.Focus()
            End If

        End If
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then

            If vConComprobantes = True Then
                txtCostoUnitario.txt.Text = txtProducto.Registro("PrecioUnitario").ToString
            Else
                txtCostoUnitario.txt.Text = txtProducto.Registro("Precio").ToString
            End If
            CalcularImporte(False, True, False)
            txtObservacionProducto.Focus()

        End If

    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado

        If vNuevo = True Then
            CargarComprobante()
            txtFecha.txt.Focus()
        End If

    End Sub

    Private Sub cbxComprobante_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxComprobante.Leave
        Try
            If chkAplicar.Valor = True Then
                dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaDebito V Where IDTransaccion=" & cbxComprobante.GetValue & " ").Copy
            Else
                dtDetalleVenta = CSistema.ExecuteToDataTable("Select * From VDetalleVentaParaNotaDebito V Where V.Cancelado='False' And V.IDCliente =" & txtCliente.Registro("ID").ToString).Copy
            End If

            Dim dttemp As DataTable = CData.FiltrarDataTable(dtDetalleVenta, " IDTransaccion = " & cbxComprobante.GetValue)
            txtProducto.Conectar(dttemp)
        Catch ex As Exception
            MessageBox.Show("Verificar cliente. Si el problema continua consulte con el Administrador del Sistema.")
        End Try


    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProductoDevolucion()
        End If

    End Sub

    Private Sub cbxPuntoExpedicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ObtenerInformacionPuntoVenta()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        MoverRegistro(e)
    End Sub

    Private Sub chkAplicar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkAplicar.PropertyChanged
        Aplica()
    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporte.TeclaPrecionada
        If txtImporte.txt.Focused = False Then
            Exit Sub
        End If

        CalcularImporte(False, True, True)


        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If

    End Sub

    Private Sub rdbDescuentos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdDebito.CheckedChanged
        If rdDebito.Checked = True Then
            ObtenerComprobanteDescuento()
            pnlDevolucion.Visible = False
            pnlDescuento.Visible = True

            If vNuevo = True Then
                If txtObservacion.GetValue = vObservacionDescuento Or txtObservacion.GetValue = vObservacionDevolucion Then
                    txtObservacion.SetValue(vObservacionDescuento)
                End If
            End If


        End If
    End Sub

    Private Sub lvListaDescuento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvListaDescuento.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProductoDescuento()
        End If
    End Sub

    Private Sub rdbDevolucion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbReposicion.CheckedChanged

        If rdbReposicion.Checked = True Then
            pnlDevolucion.Visible = True
            pnlDescuento.Visible = False

            If vNuevo = True Then
                If txtObservacion.GetValue = vObservacionDescuento Or txtObservacion.GetValue = vObservacionDevolucion Then
                    txtObservacion.SetValue(vObservacionDevolucion)
                End If
            End If

        End If

    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub cbxCiudad_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

        cbxSucursal.cbx.Text = ""
        cbxDeposito.cbx.Text = ""

        If cbxCiudad.Validar() = False Then
            Exit Sub
        End If

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " IDCiudad = " & cbxCiudad.GetValue), "ID", "Descripcion")
        cbxSucursal.SelectedValue(vgIDSucursal)

    End Sub

    Private Sub cbxSucursal_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged

        cbxDeposito.cbx.Text = ""

        If cbxSucursal.Validar() = False Then
            Exit Sub
        End If

        'Deposito
        CSistema.SqlToComboBox(cbxDeposito.cbx, CData.GetTable("VDeposito", " IDSucursal = " & cbxSucursal.GetValue), "ID", "Deposito")
        cbxDeposito.SelectedValue(vgIDDeposito)

    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Close()

    End Sub

    Private Sub lklAgregarDescuento_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarDetalleDescuento.LinkClicked
        CargarDescuentos()
    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklAgregarComprobanteDescuento.LinkClicked
        CargarComprobanteDescuento()
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarDetalleDescuento.LinkClicked
        EliminarProductoDescuento()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarComprobanteDescuento.LinkClicked
        EliminarComprobanteDescuento()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarComprobanteDescuento()
        End If
    End Sub

    Private Sub VerDetalleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDetalleToolStripMenuItem.Click

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim Filtro As String
        Filtro = " ID=" & lvLista.SelectedItems(0).Index & " And IDProducto=" & lvLista.SelectedItems(0).Text & " "

        Dim dttemp As DataTable = dtDetalle.Copy
        dttemp = CData.FiltrarDataTable(dttemp, Filtro)
        CSistema.MostrarPropiedades(Me, "Detalle", "", "", dttemp)

    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(dtDetalle)
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub lklTimbrado_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklTimbrado.LinkClicked
        SeleccionarTimbrado()
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        Eliminar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub

    '10-06-2021 - SC - Actualiza datos
    Sub frmNotaDebitoCliente_Activate()
        Me.Refresh()
    End Sub

End Class

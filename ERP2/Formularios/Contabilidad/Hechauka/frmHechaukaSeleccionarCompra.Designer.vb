﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHechaukaSeleccionarCompra
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bntCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.lnkSeleccionar = New System.Windows.Forms.LinkLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SeleccionarTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitarTodaSeleccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.txtCantidadSeleccionado = New ERP.ocxTXTNumeric()
        Me.txtSeleccionado = New ERP.ocxTXTNumeric()
        Me.lblSeleccionado = New System.Windows.Forms.Label()
        Me.ColSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodigoTipoRegistro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodigoTipoDeIdentificacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ruc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodigoTipoComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumeroTimbrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumeroDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoCompra10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoCompra5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoCompraNoGrabada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoTotalDelComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodigoCondicionCompra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OperacionEnMonedaExtranjera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImputaAlIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImputaAlIRE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImputaAlIRPRSP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoImputa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumeroDelComprobanteDeVentaAsociado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TimbradoDelComprobanteDeVentaAsociado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bntCancelar
        '
        Me.bntCancelar.Location = New System.Drawing.Point(879, 395)
        Me.bntCancelar.Name = "bntCancelar"
        Me.bntCancelar.Size = New System.Drawing.Size(75, 23)
        Me.bntCancelar.TabIndex = 19
        Me.bntCancelar.Text = "&Cancelar"
        Me.bntCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(788, 395)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(85, 23)
        Me.btnAceptar.TabIndex = 18
        Me.btnAceptar.Tag = ""
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'lnkSeleccionar
        '
        Me.lnkSeleccionar.AutoSize = True
        Me.lnkSeleccionar.Location = New System.Drawing.Point(45, 398)
        Me.lnkSeleccionar.Name = "lnkSeleccionar"
        Me.lnkSeleccionar.Size = New System.Drawing.Size(87, 13)
        Me.lnkSeleccionar.TabIndex = 17
        Me.lnkSeleccionar.TabStop = True
        Me.lnkSeleccionar.Text = "Seleccionar todo"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SeleccionarTodoToolStripMenuItem, Me.QuitarTodaSeleccionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(187, 48)
        '
        'SeleccionarTodoToolStripMenuItem
        '
        Me.SeleccionarTodoToolStripMenuItem.Name = "SeleccionarTodoToolStripMenuItem"
        Me.SeleccionarTodoToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SeleccionarTodoToolStripMenuItem.Text = "Seleccionar todo"
        '
        'QuitarTodaSeleccionToolStripMenuItem
        '
        Me.QuitarTodaSeleccionToolStripMenuItem.Name = "QuitarTodaSeleccionToolStripMenuItem"
        Me.QuitarTodaSeleccionToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.QuitarTodaSeleccionToolStripMenuItem.Text = "Quitar toda seleccion"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 428)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(980, 22)
        Me.StatusStrip1.TabIndex = 21
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'lblComprobante
        '
        Me.lblComprobante.Location = New System.Drawing.Point(39, 25)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(106, 23)
        Me.lblComprobante.TabIndex = 23
        Me.lblComprobante.Text = "Nro. Comprobante:"
        Me.lblComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(151, 28)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(220, 20)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 22
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColSel, Me.IDTransaccion, Me.CodigoTipoRegistro, Me.CodigoTipoDeIdentificacion, Me.Ruc, Me.NombreProveedor, Me.CodigoTipoComprobante, Me.FechaDocumento, Me.NumeroTimbrado, Me.NumeroDocumento, Me.MontoCompra10, Me.MontoCompra5, Me.MontoCompraNoGrabada, Me.MontoTotalDelComprobante, Me.CodigoCondicionCompra, Me.OperacionEnMonedaExtranjera, Me.ImputaAlIVA, Me.ImputaAlIRE, Me.ImputaAlIRPRSP, Me.NoImputa, Me.NumeroDelComprobanteDeVentaAsociado, Me.TimbradoDelComprobanteDeVentaAsociado})
        Me.dgw.Location = New System.Drawing.Point(12, 62)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(956, 316)
        Me.dgw.TabIndex = 24
        '
        'txtCantidadSeleccionado
        '
        Me.txtCantidadSeleccionado.Color = System.Drawing.Color.Empty
        Me.txtCantidadSeleccionado.Decimales = True
        Me.txtCantidadSeleccionado.Indicaciones = Nothing
        Me.txtCantidadSeleccionado.Location = New System.Drawing.Point(706, 396)
        Me.txtCantidadSeleccionado.Name = "txtCantidadSeleccionado"
        Me.txtCantidadSeleccionado.Size = New System.Drawing.Size(44, 22)
        Me.txtCantidadSeleccionado.SoloLectura = False
        Me.txtCantidadSeleccionado.TabIndex = 27
        Me.txtCantidadSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadSeleccionado.Texto = "0"
        '
        'txtSeleccionado
        '
        Me.txtSeleccionado.Color = System.Drawing.Color.Empty
        Me.txtSeleccionado.Decimales = True
        Me.txtSeleccionado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSeleccionado.Indicaciones = Nothing
        Me.txtSeleccionado.Location = New System.Drawing.Point(555, 396)
        Me.txtSeleccionado.Name = "txtSeleccionado"
        Me.txtSeleccionado.Size = New System.Drawing.Size(145, 22)
        Me.txtSeleccionado.SoloLectura = True
        Me.txtSeleccionado.TabIndex = 26
        Me.txtSeleccionado.TabStop = False
        Me.txtSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSeleccionado.Texto = "0"
        '
        'lblSeleccionado
        '
        Me.lblSeleccionado.Location = New System.Drawing.Point(478, 393)
        Me.lblSeleccionado.Name = "lblSeleccionado"
        Me.lblSeleccionado.Size = New System.Drawing.Size(81, 22)
        Me.lblSeleccionado.TabIndex = 25
        Me.lblSeleccionado.Text = "Seleccionado:"
        Me.lblSeleccionado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ColSel
        '
        Me.ColSel.HeaderText = "Seleccionado"
        Me.ColSel.Name = "ColSel"
        Me.ColSel.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ColSel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'IDTransaccion
        '
        Me.IDTransaccion.HeaderText = "IDTransaccion"
        Me.IDTransaccion.Name = "IDTransaccion"
        '
        'CodigoTipoRegistro
        '
        Me.CodigoTipoRegistro.HeaderText = "CodigoTipoRegistro"
        Me.CodigoTipoRegistro.Name = "CodigoTipoRegistro"
        '
        'CodigoTipoDeIdentificacion
        '
        Me.CodigoTipoDeIdentificacion.HeaderText = "CodigoTipoDeIdentificacion"
        Me.CodigoTipoDeIdentificacion.Name = "CodigoTipoDeIdentificacion"
        '
        'Ruc
        '
        Me.Ruc.HeaderText = "Ruc"
        Me.Ruc.Name = "Ruc"
        '
        'NombreProveedor
        '
        Me.NombreProveedor.HeaderText = "NombreProveedor"
        Me.NombreProveedor.Name = "NombreProveedor"
        '
        'CodigoTipoComprobante
        '
        Me.CodigoTipoComprobante.HeaderText = "CodigoTipoComprobante"
        Me.CodigoTipoComprobante.Name = "CodigoTipoComprobante"
        '
        'FechaDocumento
        '
        Me.FechaDocumento.HeaderText = "FechaDocumento"
        Me.FechaDocumento.Name = "FechaDocumento"
        '
        'NumeroTimbrado
        '
        Me.NumeroTimbrado.HeaderText = "NumeroTimbrado"
        Me.NumeroTimbrado.Name = "NumeroTimbrado"
        '
        'NumeroDocumento
        '
        Me.NumeroDocumento.HeaderText = "NumeroDocumento"
        Me.NumeroDocumento.Name = "NumeroDocumento"
        '
        'MontoCompra10
        '
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.MontoCompra10.DefaultCellStyle = DataGridViewCellStyle2
        Me.MontoCompra10.HeaderText = "MontoCompra10%"
        Me.MontoCompra10.Name = "MontoCompra10"
        '
        'MontoCompra5
        '
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.MontoCompra5.DefaultCellStyle = DataGridViewCellStyle3
        Me.MontoCompra5.HeaderText = "MontoCompra5%"
        Me.MontoCompra5.Name = "MontoCompra5"
        '
        'MontoCompraNoGrabada
        '
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.MontoCompraNoGrabada.DefaultCellStyle = DataGridViewCellStyle4
        Me.MontoCompraNoGrabada.HeaderText = "MontoCompraNoGrabada"
        Me.MontoCompraNoGrabada.Name = "MontoCompraNoGrabada"
        '
        'MontoTotalDelComprobante
        '
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.MontoTotalDelComprobante.DefaultCellStyle = DataGridViewCellStyle5
        Me.MontoTotalDelComprobante.HeaderText = "MontoTotalDelComprobante"
        Me.MontoTotalDelComprobante.Name = "MontoTotalDelComprobante"
        '
        'CodigoCondicionCompra
        '
        Me.CodigoCondicionCompra.HeaderText = "CodigoCondicionCompra"
        Me.CodigoCondicionCompra.Name = "CodigoCondicionCompra"
        '
        'OperacionEnMonedaExtranjera
        '
        Me.OperacionEnMonedaExtranjera.HeaderText = "OperacionEnMonedaExtranjera"
        Me.OperacionEnMonedaExtranjera.Name = "OperacionEnMonedaExtranjera"
        '
        'ImputaAlIVA
        '
        Me.ImputaAlIVA.HeaderText = "ImputaAlIVA"
        Me.ImputaAlIVA.Name = "ImputaAlIVA"
        '
        'ImputaAlIRE
        '
        Me.ImputaAlIRE.HeaderText = "ImputaAlIRE"
        Me.ImputaAlIRE.Name = "ImputaAlIRE"
        '
        'ImputaAlIRPRSP
        '
        Me.ImputaAlIRPRSP.HeaderText = "ImputaAl-IRP-RSP"
        Me.ImputaAlIRPRSP.Name = "ImputaAlIRPRSP"
        '
        'NoImputa
        '
        Me.NoImputa.HeaderText = "NoImputa"
        Me.NoImputa.Name = "NoImputa"
        '
        'NumeroDelComprobanteDeVentaAsociado
        '
        Me.NumeroDelComprobanteDeVentaAsociado.HeaderText = "NumeroDelComprobanteDeVentaAsociado"
        Me.NumeroDelComprobanteDeVentaAsociado.Name = "NumeroDelComprobanteDeVentaAsociado"
        '
        'TimbradoDelComprobanteDeVentaAsociado
        '
        Me.TimbradoDelComprobanteDeVentaAsociado.HeaderText = "TimbradoDelComprobanteDeVentaAsociado"
        Me.TimbradoDelComprobanteDeVentaAsociado.Name = "TimbradoDelComprobanteDeVentaAsociado"
        '
        'frmHechaukaSeleccionarCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(980, 450)
        Me.Controls.Add(Me.txtCantidadSeleccionado)
        Me.Controls.Add(Me.txtSeleccionado)
        Me.Controls.Add(Me.lblSeleccionado)
        Me.Controls.Add(Me.dgw)
        Me.Controls.Add(Me.lblComprobante)
        Me.Controls.Add(Me.txtComprobante)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.bntCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.lnkSeleccionar)
        Me.Name = "frmHechaukaSeleccionarCompra"
        Me.Tag = "frmHechaukaSeleccionarCompra"
        Me.Text = "frmHechaukaSeleccionarCompra"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents bntCancelar As Button
    Friend WithEvents btnAceptar As Button
    Friend WithEvents lnkSeleccionar As LinkLabel
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents SeleccionarTodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuitarTodaSeleccionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents lblComprobante As Label
    Friend WithEvents txtComprobante As ocxTXTString
    Friend WithEvents dgw As DataGridView
    Friend WithEvents txtCantidadSeleccionado As ocxTXTNumeric
    Friend WithEvents txtSeleccionado As ocxTXTNumeric
    Friend WithEvents lblSeleccionado As Label
    Friend WithEvents ColSel As DataGridViewCheckBoxColumn
    Friend WithEvents IDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents CodigoTipoRegistro As DataGridViewTextBoxColumn
    Friend WithEvents CodigoTipoDeIdentificacion As DataGridViewTextBoxColumn
    Friend WithEvents Ruc As DataGridViewTextBoxColumn
    Friend WithEvents NombreProveedor As DataGridViewTextBoxColumn
    Friend WithEvents CodigoTipoComprobante As DataGridViewTextBoxColumn
    Friend WithEvents FechaDocumento As DataGridViewTextBoxColumn
    Friend WithEvents NumeroTimbrado As DataGridViewTextBoxColumn
    Friend WithEvents NumeroDocumento As DataGridViewTextBoxColumn
    Friend WithEvents MontoCompra10 As DataGridViewTextBoxColumn
    Friend WithEvents MontoCompra5 As DataGridViewTextBoxColumn
    Friend WithEvents MontoCompraNoGrabada As DataGridViewTextBoxColumn
    Friend WithEvents MontoTotalDelComprobante As DataGridViewTextBoxColumn
    Friend WithEvents CodigoCondicionCompra As DataGridViewTextBoxColumn
    Friend WithEvents OperacionEnMonedaExtranjera As DataGridViewTextBoxColumn
    Friend WithEvents ImputaAlIVA As DataGridViewTextBoxColumn
    Friend WithEvents ImputaAlIRE As DataGridViewTextBoxColumn
    Friend WithEvents ImputaAlIRPRSP As DataGridViewTextBoxColumn
    Friend WithEvents NoImputa As DataGridViewTextBoxColumn
    Friend WithEvents NumeroDelComprobanteDeVentaAsociado As DataGridViewTextBoxColumn
    Friend WithEvents TimbradoDelComprobanteDeVentaAsociado As DataGridViewTextBoxColumn
End Class

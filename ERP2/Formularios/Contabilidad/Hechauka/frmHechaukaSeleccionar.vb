﻿Public Class frmHechaukaSeleccionar
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim SelTodo As Boolean = False
    Public Property dtSeleccionado As DataTable
    Public Property dtAux As DataTable

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Public Property Decimales As Boolean

    'VARIABLES
    Dim vCargado As Boolean
    Dim SeleccionRegistro As Boolean
    'Private dt As DataTable

    'FUNCIONES
    Sub Inicializar()

        vCargado = False

        dtAux = dt.Copy
        dtAux.Clear()

        'Funciones
        Listar(False)
        Cargar()

        'Foco
        dgw.Focus()

    End Sub

    'Sub Listar()

    '    Dim vdt As DataTable
    '    Dim Decimales As Boolean = True

    '    vCargado = False
    '    dgw.DataSource = Nothing
    '    vdt = dt
    '    vdt.Columns("Seleccionado").SetOrdinal(0)

    '    CSistema.dtToGrid(dgw, vdt)
    '    'Dim ColumnasVisibles() As String = {"Seleccionado", "TipoRegistro", "CodigoTipoDeIdentificacion", "Ruc", "CI", "RUCCliente", "NombreCliente", "CodigoTipoComprobante", "FechaDocumento", "NumeroTimbrado", "NumeroDocumento", "MontoVenta10%", "MontoVenta5%", "MontoVentaNoGrabada", "MontoTotalDelIngreso", "CondicionVenta", "OperacionEnMonedaExtranjera", "ImputaAlIVA", "ImputaAlIRE", "ImputaAl-IRP-RSP", "NumeroDelComprobanteDeVentaAsociado", "TimbradoDelComprobanteDeVentaAsociado"}
    '    Dim ColumnasVisibles() As String = {"Seleccionado", "TipoRegistro", "CodigoTipoDeIdentificacion", "Ruc", "NombreCliente", "CodigoTipoComprobante", "FechaDocumento", "NumeroTimbrado", "NumeroDocumento", "MontoVenta10%", "MontoVenta5%", "MontoVentaNoGrabada", "MontoTotalDelIngreso", "CondicionVenta", "OperacionEnMonedaExtranjera", "ImputaAlIVA", "ImputaAlIRE", "ImputaAl-IRP-RSP", "NumeroDelComprobanteDeVentaAsociado", "TimbradoDelComprobanteDeVentaAsociado"}
    '    Dim ColumnasNumericas() As String = {"MontoVenta10%", "MontoVenta5%", "MontoVentaNoGrabada", "MontoTotalDelIngreso"}
    '    Dim CantidadDecimal As Integer = 0

    '    If Decimales = True Then
    '        CantidadDecimal = 2
    '    End If

    '    'Formato
    '    For Each c As DataGridViewColumn In dgw.Columns

    '        'Habilitar solo el campo de seleccion
    '        If c.Name <> "Seleccionado" Then
    '            c.ReadOnly = True
    '        End If

    '        'Ocultar los que no son visibles
    '        If ColumnasVisibles.Contains(c.Name) = False Then
    '            c.Visible = False
    '        End If

    '        'Formatos numericos
    '        If ColumnasNumericas.Contains(c.Name) = True Then
    '            c.DefaultCellStyle.Format = "N" & CantidadDecimal
    '            c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    '        End If
    '        'AutoSize
    '        Select Case c.Name
    '            Case "Seleccionado"
    '                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
    '            Case "MontoVenta10%", "MontoVenta5%", "MontoVentaNoGrabada", "MontoTotalDelIngreso"
    '                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
    '            Case "NumeroDocumento"
    '                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
    '            Case Else
    '                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader
    '        End Select

    '    Next

    '    'Habilitar celdas de los que ya estan seleccionados
    '    For Each oRow As DataGridViewRow In dgw.Rows
    '        If oRow.Cells("Seleccionado").Value = True Then
    '            oRow.Cells("MontoVenta10%").ReadOnly = False
    '            oRow.Cells("MontoVenta5%").ReadOnly = False
    '            oRow.Cells("MontoVentaNoGrabada").ReadOnly = False
    '            oRow.Cells("MontoTotalDelIngreso").ReadOnly = False
    '        End If
    '    Next
    '    'Nombres de Columnas
    '    dgw.Columns("Seleccionado").HeaderText = "Sel"

    '    dgw.SelectionMode = DataGridViewSelectionMode.CellSelect


    'End Sub

    Private Sub txtComprobante_TeclaPrecionada(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            Listar(True)
        End If
    End Sub

    Sub Listar(ByVal PorFactura As Boolean)

        'dbs
        Dim Where As String = ""

        If PorFactura = True Then
            Where = "NumeroDocumento ='" & txtComprobante.GetValue & "'"
        Else
            Where = ""
        End If
        'fin dbs

        'Limpiar la grilla
        dgw.Rows.Clear()

        vCargado = False

        For Each oRow As DataRow In dt.Select(Where)
            Dim Registro(21) As String

            Registro(0) = oRow("Seleccionado").ToString
            Registro(1) = oRow("Idtransaccion").ToString
            Registro(2) = oRow("TipoRegistro").ToString
            Registro(3) = oRow("CodigoTipoDeIdentificacion").ToString
            Registro(4) = oRow("Ruc").ToString
            Registro(5) = oRow("NombreCliente").ToString
            Registro(6) = oRow("CodigoTipoComprobante").ToString
            Registro(7) = Replace((oRow("FechaDocumento").ToString), "/", "-")
            Registro(8) = oRow("NumeroTimbrado").ToString
            Registro(9) = oRow("NumeroDocumento").ToString
            Registro(10) = CDec(oRow("MontoVenta10%").ToString)
            Registro(11) = CDec(oRow("MontoVenta5%").ToString)
            Registro(12) = CDec(oRow("MontoVentaNoGrabada").ToString)
            Registro(13) = CDec(oRow("MontoTotalDelIngreso").ToString)
            Registro(14) = oRow("CondicionVenta").ToString
            Registro(15) = oRow("OperacionEnMonedaExtranjera").ToString
            Registro(16) = oRow("InputaAlIVA").ToString
            Registro(17) = oRow("ImputaAlIRE").ToString
            Registro(18) = oRow("ImputaAl-IRP-RSP").ToString
            Registro(19) = oRow("NumeroDelComprobanteDeVentaAsociado").ToString
            Registro(20) = oRow("TimbradoDelComprobanteDeVentaAsociado").ToString
            dgw.Rows.Add(Registro)

        Next

    End Sub

    Sub Seleccionar()

        If dtAux.Rows.Count < 1 Then
            dtSeleccionado = dt.Copy
            dtSeleccionado.Clear()

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Cells(0).Value = True Then
                    'Nuevo
                    Dim NewRow As DataRow = dtSeleccionado.NewRow
                    NewRow("Seleccionado") = oRow.Cells(0).Value
                    NewRow("Idtransaccion") = oRow.Cells(1).Value
                    NewRow("TipoRegistro") = oRow.Cells(2).Value
                    NewRow("CodigoTipoDeIdentificacion") = oRow.Cells(3).Value
                    NewRow("Ruc") = oRow.Cells(4).Value
                    NewRow("NombreCliente") = oRow.Cells(5).Value
                    NewRow("CodigoTipoComprobante") = oRow.Cells(6).Value
                    NewRow("FechaDocumento") = Replace((oRow.Cells(7).Value), "/", "-")
                    NewRow("NumeroTimbrado") = oRow.Cells(8).Value
                    NewRow("NumeroDocumento") = oRow.Cells(9).Value
                    NewRow("MontoVenta10%") = CDec(oRow.Cells(10).Value)
                    NewRow("MontoVenta5%") = CDec(oRow.Cells(11).Value)
                    NewRow("MontoVentaNoGrabada") = CDec(oRow.Cells(12).Value)
                    NewRow("MontoTotalDelIngreso") = CDec(oRow.Cells(13).Value)
                    NewRow("CondicionVenta") = oRow.Cells(14).Value
                    NewRow("OperacionEnMonedaExtranjera") = oRow.Cells(15).Value
                    NewRow("InputaAlIVA") = oRow.Cells(16).Value
                    NewRow("ImputaAlIRE") = oRow.Cells(17).Value
                    NewRow("ImputaAl-IRP-RSP") = oRow.Cells(18).Value
                    NewRow("NumeroDelComprobanteDeVentaAsociado") = oRow.Cells(19).Value
                    NewRow("TimbradoDelComprobanteDeVentaAsociado") = oRow.Cells(20).Value
                    dtSeleccionado.Rows.Add(NewRow)

                End If

            Next

            dt.Clear()
            dt = dtSeleccionado

        Else
            dt.Clear()
            dt = dtAux
        End If


        Me.Close()

    End Sub

    Sub Cargar()

        PintarCelda()
        CalcularTotales()
    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = 0
        Dim contador As Integer = 0

        If dtAux.Rows.Count < 1 Then
            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Cells(0).Value = True Then

                    Total = Total + CDec(oRow.Cells(13).Value)
                    contador = contador + 1
                    'Marca registro Marcado
                    SeleccionRegistro = True
                End If

            Next

            txtCantidadSeleccionado.SetValue(contador)
            txtSeleccionado.SetValue(Total)
        Else
            txtCantidadSeleccionado.SetValue(dtAux.Rows.Count)
            txtSeleccionado.SetValue(CSistema.dtSumColumn(dtAux, "MontoTotalDelIngreso"))
        End If
        txtComprobante.Clear()

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

    End Sub

    Sub SeleccionarTodo()
        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(0).Value = True
        Next
        SelTodo = True
        lnkSeleccionar.Text = "Quitar Todo"
        PintarCelda()
        CalcularTotales()

    End Sub

    Sub QuitarTodo()
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(0).Value = False
        Next
        SelTodo = False
        lnkSeleccionar.Text = "Seleccionar Todo"
        PintarCelda()
        CalcularTotales()
    End Sub

    Private Sub frmHechaukaSeleccionar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        vCargado = True
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns(0).Index Then

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(0).Value = False Then
                        oRow.Cells(0).Value = True

                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise


                    Else
                        oRow.Cells(0).ReadOnly = True
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            For Each oRow As DataGridViewRow In dgw.Rows
                Dim IDTransaccion As Integer = oRow.Cells(1).Value
                Dim Filtro As String = " IDTransaccion=" & IDTransaccion

                If dtAux.Select(Filtro).Count < 1 Then
                    If oRow.Cells(0).Value = True Then
                        Dim NewRow As DataRow = dtAux.NewRow
                        NewRow("Seleccionado") = oRow.Cells(0).Value
                        NewRow("Idtransaccion") = oRow.Cells(1).Value
                        NewRow("TipoRegistro") = oRow.Cells(2).Value
                        NewRow("CodigoTipoDeIdentificacion") = oRow.Cells(3).Value
                        NewRow("Ruc") = oRow.Cells(4).Value
                        NewRow("NombreCliente") = oRow.Cells(5).Value
                        NewRow("CodigoTipoComprobante") = oRow.Cells(6).Value
                        NewRow("FechaDocumento") = Replace((oRow.Cells(7).Value), "/", "-")
                        NewRow("NumeroTimbrado") = oRow.Cells(8).Value
                        NewRow("NumeroDocumento") = oRow.Cells(9).Value
                        NewRow("MontoVenta10%") = CDec(oRow.Cells(10).Value)
                        NewRow("MontoVenta5%") = CDec(oRow.Cells(11).Value)
                        NewRow("MontoVentaNoGrabada") = CDec(oRow.Cells(12).Value)
                        NewRow("MontoTotalDelIngreso") = CDec(oRow.Cells(13).Value)
                        NewRow("CondicionVenta") = oRow.Cells(14).Value
                        NewRow("OperacionEnMonedaExtranjera") = oRow.Cells(15).Value
                        NewRow("InputaAlIVA") = oRow.Cells(16).Value
                        NewRow("ImputaAlIRE") = oRow.Cells(17).Value
                        NewRow("ImputaAl-IRP-RSP") = oRow.Cells(18).Value
                        NewRow("NumeroDelComprobanteDeVentaAsociado") = oRow.Cells(19).Value
                        NewRow("TimbradoDelComprobanteDeVentaAsociado") = oRow.Cells(20).Value
                        dtAux.Rows.Add(NewRow)
                        'Next
                    End If
                End If
            Next

            dgw.Update()
            CalcularTotales()
        End If

    End Sub

    'Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

    '    If dgw.Columns(e.ColumnIndex).Name = "Importe" Then

    '        If IsNumeric(dgw.CurrentCell.Value) = False Then

    '            Dim mensaje As String = "El importe debe ser valido!"
    '            ctrError.SetError(dgw, mensaje)
    '            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
    '            tsslEstado.Text = mensaje

    '            dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Saldo").Value

    '        Else
    '            dgw.CurrentCell.Value = dgw.CurrentRow.Cells("Importe").Value
    '            dgw.CurrentRow.Cells("RetencionIVA").Value = 0
    '            dgw.CurrentRow.Cells("IVA").Value = 0
    '            Dim mensaje As String = "Debe Ingresar un importe para el IVA y la Retencion!"
    '            ctrError.SetError(dgw, mensaje)
    '            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
    '            tsslEstado.Text = mensaje
    '            MessageBox.Show("Debe Ingresar el Importe de Retencion", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        End If


    '    End If

    '    si se modifica la Retencion
    '    If dgw.Columns(e.ColumnIndex).Name = "RetencionIVA" Then

    '        If IsNumeric(dgw.CurrentCell.Value) = False Then

    '            Dim mensaje As String = "El importe de Retencion IVA debe ser valido!"
    '            ctrError.SetError(dgw, mensaje)
    '            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
    '            tsslEstado.Text = mensaje

    '            dgw.CurrentCell.Value = 0

    '        Else
    '            ctrError.SetError(dgw, "")
    '            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
    '            tsslEstado.Text = ""
    '        End If


    '    End If

    '    si se modifica el IVA
    '    If dgw.Columns(e.ColumnIndex).Name = "IVA" Then

    '        If IsNumeric(dgw.CurrentCell.Value) = False Then

    '            Dim mensaje As String = "El importe del IVA debe ser valido!"
    '            ctrError.SetError(dgw, mensaje)
    '            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
    '            tsslEstado.Text = mensaje

    '            dgw.CurrentCell.Value = 0

    '        Else
    '            ctrError.SetError(dgw, "")
    '            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopRight)
    '            tsslEstado.Text = ""
    '        End If

    '    End If

    'End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(0).Value = False Then
                        oRow.Cells(0).Value = True
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells(0).ReadOnly = True
                        oRow.Cells(0).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next


            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(0).Value = False Then
                        oRow.Cells(0).Value = True

                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                    Else
                        oRow.Cells(0).ReadOnly = True

                        oRow.Cells(0).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For


                End If

            Next

            dgw.Update()

            e.SuppressKeyPress = True

        End If

    End Sub

    'Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

    '    If vCargado = False Then
    '        Exit Sub
    '    End If

    '    Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)

    '    If dgw.Rows(e.RowIndex).Cells("Seleccionado").Value = False Then
    '        dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
    '    End If

    '    dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f1

    'End Sub

    'Private Sub dgw_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowLeave

    '    If vCargado = False Then
    '        Exit Sub
    '    End If

    '    Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

    '    dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f2

    '    If dgw.Rows(e.RowIndex).Cells("Seleccionado").Value = False Then
    '        dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
    '    Else
    '        dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleTurquoise
    '    End If

    'End Sub

    Private Sub dgw_RowLeave(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowLeave

        If vCargado = False Then
            Exit Sub
        End If

        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f2

        If dgw.Rows(e.RowIndex).Cells(0).Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
        Else
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.PaleTurquoise
        End If

    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        If vCargado = False Then
            Exit Sub
        End If

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)

        If dgw.Rows(e.RowIndex).Cells(0).Value = False Then
            dgw.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
        End If

        dgw.Rows(e.RowIndex).DefaultCellStyle.Font = f1

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp

        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntCancelar.Click
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSeleccionar.LinkClicked
        If SelTodo = False Then
            SeleccionarTodo()
        Else
            QuitarTodo()
        End If
    End Sub

    '22/02/2022 - SC - Actualiza datos
    Sub frmHechaukaSeleccionar_Activate()
        Me.Refresh()
    End Sub
End Class
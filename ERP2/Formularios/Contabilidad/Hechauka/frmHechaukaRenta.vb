﻿Public Class frmHechaukaRenta

    'CLASES
    Public CSistema As New CSistema

    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Meses
        Dim Mes() As String = {"ENERO", "FREBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
        cbxMes.Items.AddRange(Mes)
        cbxMes.DropDownStyle = ComboBoxStyle.DropDownList
        cbxMes.SelectedIndex = Date.Now.Month - 1

        'Año
        NumericUpDown1.Minimum = Date.Now.Year - 10
        NumericUpDown1.Maximum = Date.Now.Year
        NumericUpDown1.Value = Date.Now.Year

        'Tipo de Reporte
        cbxTipoReporte.Items.Add("ORIGINAL")
        cbxTipoReporte.Items.Add("RECTIFICATIVA")
        cbxTipoReporte.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoReporte.SelectedIndex = 0




    End Sub

    Sub Procesar1()

        Dim dt1 As DataTable = CSistema.ExecuteToDataTable("Execute SpHechaukaRetencionEncabezado @Año=" & NumericUpDown1.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & "," & "@TipoReporte=" & cbxTipoReporte.SelectedIndex + 1 & "")
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpHechaukaRetencionDetalle  @Año=" & NumericUpDown1.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & " ")

        DatatableToExcel1(dt1)
        DatatableToExcel2(dt)


    End Sub

    Private Sub DatatableToExcel1(ByVal dtTemp As DataTable)
        Try
            Dim _excel As Excel.Application
            Dim wBook As Excel.Workbook
            Dim wSheet As Excel.Worksheet

            _excel = CType(CreateObject("Excel.application"), Excel.Application)
            wBook = CType(_excel.Workbooks.Add, Excel.Workbook)
            wSheet = CType(wBook.Worksheets(1), Excel.Worksheet)

            Dim dt As System.Data.DataTable = dtTemp
            Dim dc As System.Data.DataColumn
            Dim dr As System.Data.DataRow
            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0

            For Each dc In dt.Columns
                colIndex = colIndex + 1
                _excel.Cells(1, colIndex) = dc.ColumnName
            Next

            For Each dr In dt.Rows
                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In dt.Columns
                    colIndex = colIndex + 1
                    _excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                Next
            Next

            wSheet.Columns.AutoFit()

            Dim strFileName As String = "C:\Documents and Settings\All Users\Escritorio\HechaukaRetencionEncabezado.xlsx"
            If System.IO.File.Exists(strFileName) Then
                System.IO.File.Delete(strFileName)
            End If

            Dim savefile As New SaveFileDialog
            savefile.AddExtension = True
            savefile.Filter = "Archivos de texto|*.xlsx"
            savefile.FileName = "HechaukaCompra"
            savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

            If savefile.ShowDialog = DialogResult.OK Then
                wBook.SaveAs(strFileName)
            End If
            wBook.Close()
            _excel.Quit()


        Catch ex As Exception

            Try
                CSistema.dtToOO(dtTemp)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Try
    End Sub

    Private Sub DatatableToExcel2(ByVal dtTemp As DataTable)
        Try
            Dim _excel As Excel.Application
            Dim wBook As Excel.Workbook
            Dim wSheet As Excel.Worksheet

            _excel = CType(CreateObject("Excel.application"), Excel.Application)
            wBook = CType(_excel.Workbooks.Add, Excel.Workbook)
            wSheet = CType(wBook.Worksheets(1), Excel.Worksheet)

            Dim dt As System.Data.DataTable = dtTemp
            Dim dc As System.Data.DataColumn
            Dim dr As System.Data.DataRow
            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0

            For Each dc In dt.Columns
                colIndex = colIndex + 1
                _excel.Cells(1, colIndex) = dc.ColumnName
            Next

            For Each dr In dt.Rows
                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In dt.Columns
                    colIndex = colIndex + 1
                    _excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                Next
            Next

            wSheet.Columns.AutoFit()

            Dim strFileName As String = "C:\Documents and Settings\All Users\Escritorio\HechaukaRetencionDetalle.xlsx"
            If System.IO.File.Exists(strFileName) Then
                System.IO.File.Delete(strFileName)
            End If

            Dim savefile As New SaveFileDialog
            savefile.AddExtension = True
            savefile.Filter = "Archivos de texto|*.xlsx"
            savefile.FileName = "HechaukaCompra"
            savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

            If savefile.ShowDialog = DialogResult.OK Then
                wBook.SaveAs(strFileName)
            End If

            MsgBox("Registro Guardado.!")
            Me.Close()

        Catch ex As Exception

            Try
                CSistema.dtToOO(dtTemp)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Try
    End Sub

    Sub Procesar()

        'CABECERA
        Dim fic As String = VGCarpetaTemporal & "\HechaukaRetencion.txt"

        Try
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpHechaukaRetencionEncabezado @Año=" & NumericUpDown1.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & "," & "@TipoReporte=" & cbxTipoReporte.SelectedIndex + 1 & "")



            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        'DETALLE
        Try
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpHechaukaRetencionDetalle  @Año=" & NumericUpDown1.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & " ")


            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        MsgBox("Registro Guardado.!")

        Dim savefile As New SaveFileDialog
        savefile.AddExtension = True
        savefile.Filter = "Archivos de texto|*.txt"
        savefile.FileName = "HechaukaRetenciones"
        savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        If savefile.ShowDialog = DialogResult.OK Then
            FileCopy(fic, savefile.FileName)
        End If

    End Sub

    Private Sub frmHechaukaLibrodeComprasEncabezado_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmHechaukaLibrodeComprasEncabezado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnExportarExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        Procesar1()
    End Sub
End Class





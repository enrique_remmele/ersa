﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHechaukaSeleccionar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.lnkSeleccionar = New System.Windows.Forms.LinkLabel()
        Me.bntCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SeleccionarTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuitarTodaSeleccionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtCantidadSeleccionado = New ERP.ocxTXTNumeric()
        Me.txtSeleccionado = New ERP.ocxTXTNumeric()
        Me.lblSeleccionado = New System.Windows.Forms.Label()
        Me.ColSel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoRegistro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodigoTipoDeIdentificacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ruc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodigoTipoComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumeroTimbrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumeroDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoVenta10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoVenta5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoVentaNoGrabada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoTotalDelIngreso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CondicionVenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OperacionEnMonedaExtranjera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InputaAlIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImputaAlIRE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImputaAlIRPRSP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumeroDelComprobanteDeVentaAsociado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TimbradoDelComprobanteDeVentaAsociado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColSel, Me.IDTransaccion, Me.TipoRegistro, Me.CodigoTipoDeIdentificacion, Me.Ruc, Me.NombreCliente, Me.CodigoTipoComprobante, Me.FechaDocumento, Me.NumeroTimbrado, Me.NumeroDocumento, Me.MontoVenta10, Me.MontoVenta5, Me.MontoVentaNoGrabada, Me.MontoTotalDelIngreso, Me.CondicionVenta, Me.OperacionEnMonedaExtranjera, Me.InputaAlIVA, Me.ImputaAlIRE, Me.ImputaAlIRPRSP, Me.NumeroDelComprobanteDeVentaAsociado, Me.TimbradoDelComprobanteDeVentaAsociado})
        Me.dgw.Location = New System.Drawing.Point(8, 67)
        Me.dgw.Name = "dgw"
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(965, 316)
        Me.dgw.TabIndex = 1
        '
        'lnkSeleccionar
        '
        Me.lnkSeleccionar.AutoSize = True
        Me.lnkSeleccionar.Location = New System.Drawing.Point(37, 406)
        Me.lnkSeleccionar.Name = "lnkSeleccionar"
        Me.lnkSeleccionar.Size = New System.Drawing.Size(87, 13)
        Me.lnkSeleccionar.TabIndex = 2
        Me.lnkSeleccionar.TabStop = True
        Me.lnkSeleccionar.Text = "Seleccionar todo"
        '
        'bntCancelar
        '
        Me.bntCancelar.Location = New System.Drawing.Point(884, 397)
        Me.bntCancelar.Name = "bntCancelar"
        Me.bntCancelar.Size = New System.Drawing.Size(75, 23)
        Me.bntCancelar.TabIndex = 15
        Me.bntCancelar.Text = "&Cancelar"
        Me.bntCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(793, 397)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(85, 23)
        Me.btnAceptar.TabIndex = 14
        Me.btnAceptar.Tag = ""
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SeleccionarTodoToolStripMenuItem, Me.QuitarTodaSeleccionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(187, 48)
        '
        'SeleccionarTodoToolStripMenuItem
        '
        Me.SeleccionarTodoToolStripMenuItem.Name = "SeleccionarTodoToolStripMenuItem"
        Me.SeleccionarTodoToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.SeleccionarTodoToolStripMenuItem.Text = "Seleccionar todo"
        '
        'QuitarTodaSeleccionToolStripMenuItem
        '
        Me.QuitarTodaSeleccionToolStripMenuItem.Name = "QuitarTodaSeleccionToolStripMenuItem"
        Me.QuitarTodaSeleccionToolStripMenuItem.Size = New System.Drawing.Size(186, 22)
        Me.QuitarTodaSeleccionToolStripMenuItem.Text = "Quitar toda seleccion"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 436)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(985, 22)
        Me.StatusStrip1.TabIndex = 17
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'lblComprobante
        '
        Me.lblComprobante.Location = New System.Drawing.Point(12, 24)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(106, 23)
        Me.lblComprobante.TabIndex = 20
        Me.lblComprobante.Text = "Nro. Comprobante:"
        Me.lblComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(124, 27)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(220, 20)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 19
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtCantidadSeleccionado
        '
        Me.txtCantidadSeleccionado.Color = System.Drawing.Color.Empty
        Me.txtCantidadSeleccionado.Decimales = True
        Me.txtCantidadSeleccionado.Indicaciones = Nothing
        Me.txtCantidadSeleccionado.Location = New System.Drawing.Point(673, 398)
        Me.txtCantidadSeleccionado.Name = "txtCantidadSeleccionado"
        Me.txtCantidadSeleccionado.Size = New System.Drawing.Size(46, 22)
        Me.txtCantidadSeleccionado.SoloLectura = False
        Me.txtCantidadSeleccionado.TabIndex = 23
        Me.txtCantidadSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadSeleccionado.Texto = "0"
        '
        'txtSeleccionado
        '
        Me.txtSeleccionado.Color = System.Drawing.Color.Empty
        Me.txtSeleccionado.Decimales = True
        Me.txtSeleccionado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSeleccionado.Indicaciones = Nothing
        Me.txtSeleccionado.Location = New System.Drawing.Point(527, 398)
        Me.txtSeleccionado.Name = "txtSeleccionado"
        Me.txtSeleccionado.Size = New System.Drawing.Size(138, 22)
        Me.txtSeleccionado.SoloLectura = True
        Me.txtSeleccionado.TabIndex = 22
        Me.txtSeleccionado.TabStop = False
        Me.txtSeleccionado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSeleccionado.Texto = "0"
        '
        'lblSeleccionado
        '
        Me.lblSeleccionado.Location = New System.Drawing.Point(450, 395)
        Me.lblSeleccionado.Name = "lblSeleccionado"
        Me.lblSeleccionado.Size = New System.Drawing.Size(81, 22)
        Me.lblSeleccionado.TabIndex = 21
        Me.lblSeleccionado.Text = "Seleccionado:"
        Me.lblSeleccionado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ColSel
        '
        Me.ColSel.HeaderText = "Seleccionado"
        Me.ColSel.Name = "ColSel"
        Me.ColSel.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ColSel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'IDTransaccion
        '
        Me.IDTransaccion.HeaderText = "IDTransaccion"
        Me.IDTransaccion.Name = "IDTransaccion"
        '
        'TipoRegistro
        '
        Me.TipoRegistro.HeaderText = "TipoRegistro"
        Me.TipoRegistro.Name = "TipoRegistro"
        '
        'CodigoTipoDeIdentificacion
        '
        Me.CodigoTipoDeIdentificacion.HeaderText = "CodigoTipoDeIdentificacion"
        Me.CodigoTipoDeIdentificacion.Name = "CodigoTipoDeIdentificacion"
        '
        'Ruc
        '
        Me.Ruc.HeaderText = "Ruc"
        Me.Ruc.Name = "Ruc"
        '
        'NombreCliente
        '
        Me.NombreCliente.HeaderText = "NombreCliente"
        Me.NombreCliente.Name = "NombreCliente"
        '
        'CodigoTipoComprobante
        '
        Me.CodigoTipoComprobante.HeaderText = "CodigoTipoComprobante"
        Me.CodigoTipoComprobante.Name = "CodigoTipoComprobante"
        '
        'FechaDocumento
        '
        Me.FechaDocumento.HeaderText = "FechaDocumento"
        Me.FechaDocumento.Name = "FechaDocumento"
        '
        'NumeroTimbrado
        '
        Me.NumeroTimbrado.HeaderText = "NumeroTimbrado"
        Me.NumeroTimbrado.Name = "NumeroTimbrado"
        '
        'NumeroDocumento
        '
        Me.NumeroDocumento.HeaderText = "NumeroDocumento"
        Me.NumeroDocumento.Name = "NumeroDocumento"
        '
        'MontoVenta10
        '
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.MontoVenta10.DefaultCellStyle = DataGridViewCellStyle2
        Me.MontoVenta10.HeaderText = "MontoVenta10%"
        Me.MontoVenta10.Name = "MontoVenta10"
        '
        'MontoVenta5
        '
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.MontoVenta5.DefaultCellStyle = DataGridViewCellStyle3
        Me.MontoVenta5.HeaderText = "MontoVenta5%"
        Me.MontoVenta5.Name = "MontoVenta5"
        '
        'MontoVentaNoGrabada
        '
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.MontoVentaNoGrabada.DefaultCellStyle = DataGridViewCellStyle4
        Me.MontoVentaNoGrabada.HeaderText = "MontoVentaNoGrabada"
        Me.MontoVentaNoGrabada.Name = "MontoVentaNoGrabada"
        '
        'MontoTotalDelIngreso
        '
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.MontoTotalDelIngreso.DefaultCellStyle = DataGridViewCellStyle5
        Me.MontoTotalDelIngreso.HeaderText = "MontoTotalDelIngreso"
        Me.MontoTotalDelIngreso.Name = "MontoTotalDelIngreso"
        '
        'CondicionVenta
        '
        Me.CondicionVenta.HeaderText = "CondicionVenta"
        Me.CondicionVenta.Name = "CondicionVenta"
        '
        'OperacionEnMonedaExtranjera
        '
        Me.OperacionEnMonedaExtranjera.HeaderText = "OperacionEnMonedaExtranjera"
        Me.OperacionEnMonedaExtranjera.Name = "OperacionEnMonedaExtranjera"
        '
        'InputaAlIVA
        '
        Me.InputaAlIVA.HeaderText = "InputaAlIVA"
        Me.InputaAlIVA.Name = "InputaAlIVA"
        '
        'ImputaAlIRE
        '
        Me.ImputaAlIRE.HeaderText = "ImputaAlIRE"
        Me.ImputaAlIRE.Name = "ImputaAlIRE"
        '
        'ImputaAlIRPRSP
        '
        Me.ImputaAlIRPRSP.HeaderText = "ImputaAl-IRP-RSP"
        Me.ImputaAlIRPRSP.Name = "ImputaAlIRPRSP"
        '
        'NumeroDelComprobanteDeVentaAsociado
        '
        Me.NumeroDelComprobanteDeVentaAsociado.HeaderText = "NumeroDelComprobanteDeVentaAsociado"
        Me.NumeroDelComprobanteDeVentaAsociado.Name = "NumeroDelComprobanteDeVentaAsociado"
        '
        'TimbradoDelComprobanteDeVentaAsociado
        '
        Me.TimbradoDelComprobanteDeVentaAsociado.HeaderText = "TimbradoDelComprobanteDeVentaAsociado"
        Me.TimbradoDelComprobanteDeVentaAsociado.Name = "TimbradoDelComprobanteDeVentaAsociado"
        '
        'frmHechaukaSeleccionar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(985, 458)
        Me.Controls.Add(Me.txtCantidadSeleccionado)
        Me.Controls.Add(Me.txtSeleccionado)
        Me.Controls.Add(Me.lblSeleccionado)
        Me.Controls.Add(Me.lblComprobante)
        Me.Controls.Add(Me.txtComprobante)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.bntCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.lnkSeleccionar)
        Me.Controls.Add(Me.dgw)
        Me.Name = "frmHechaukaSeleccionar"
        Me.Tag = "frmHechaukaSeleccionar"
        Me.Text = "frmHechaukaSeleccionar"
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgw As DataGridView
    Friend WithEvents lnkSeleccionar As LinkLabel
    Friend WithEvents bntCancelar As Button
    Friend WithEvents btnAceptar As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents SeleccionarTodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuitarTodaSeleccionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents lblComprobante As Label
    Friend WithEvents txtComprobante As ocxTXTString
    Friend WithEvents txtCantidadSeleccionado As ocxTXTNumeric
    Friend WithEvents txtSeleccionado As ocxTXTNumeric
    Friend WithEvents lblSeleccionado As Label
    Friend WithEvents ColSel As DataGridViewCheckBoxColumn
    Friend WithEvents IDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents TipoRegistro As DataGridViewTextBoxColumn
    Friend WithEvents CodigoTipoDeIdentificacion As DataGridViewTextBoxColumn
    Friend WithEvents Ruc As DataGridViewTextBoxColumn
    Friend WithEvents NombreCliente As DataGridViewTextBoxColumn
    Friend WithEvents CodigoTipoComprobante As DataGridViewTextBoxColumn
    Friend WithEvents FechaDocumento As DataGridViewTextBoxColumn
    Friend WithEvents NumeroTimbrado As DataGridViewTextBoxColumn
    Friend WithEvents NumeroDocumento As DataGridViewTextBoxColumn
    Friend WithEvents MontoVenta10 As DataGridViewTextBoxColumn
    Friend WithEvents MontoVenta5 As DataGridViewTextBoxColumn
    Friend WithEvents MontoVentaNoGrabada As DataGridViewTextBoxColumn
    Friend WithEvents MontoTotalDelIngreso As DataGridViewTextBoxColumn
    Friend WithEvents CondicionVenta As DataGridViewTextBoxColumn
    Friend WithEvents OperacionEnMonedaExtranjera As DataGridViewTextBoxColumn
    Friend WithEvents InputaAlIVA As DataGridViewTextBoxColumn
    Friend WithEvents ImputaAlIRE As DataGridViewTextBoxColumn
    Friend WithEvents ImputaAlIRPRSP As DataGridViewTextBoxColumn
    Friend WithEvents NumeroDelComprobanteDeVentaAsociado As DataGridViewTextBoxColumn
    Friend WithEvents TimbradoDelComprobanteDeVentaAsociado As DataGridViewTextBoxColumn
End Class

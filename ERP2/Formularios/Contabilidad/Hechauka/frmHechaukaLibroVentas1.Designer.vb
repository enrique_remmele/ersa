﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmHechaukaLibroVentas1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHechaukaLibroVentas1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cbxExportador = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cbxInforme = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtRucCliente = New ERP.ocxTXTString()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.lblrucnoValido = New System.Windows.Forms.Label()
        Me.nudAño = New System.Windows.Forms.NumericUpDown()
        Me.lblMes = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxTipoReporte = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.gbxResultado = New System.Windows.Forms.GroupBox()
        Me.lklVerDetalle = New System.Windows.Forms.LinkLabel()
        Me.txtDiferencia = New ERP.ocxTXTNumeric()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTotalCabecera = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTotalDetalle = New ERP.ocxTXTNumeric()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.lblRegistros = New System.Windows.Forms.Label()
        Me.btnExportarExcel = New System.Windows.Forms.Button()
        Me.btnExportarTXT = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxResultado.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbxExportador)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cbxInforme)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtRucCliente)
        Me.GroupBox1.Controls.Add(Me.cbxMes)
        Me.GroupBox1.Controls.Add(Me.lblrucnoValido)
        Me.GroupBox1.Controls.Add(Me.nudAño)
        Me.GroupBox1.Controls.Add(Me.lblMes)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxTipoReporte)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(317, 135)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Opciones"
        '
        'cbxExportador
        '
        Me.cbxExportador.FormattingEnabled = True
        Me.cbxExportador.Location = New System.Drawing.Point(221, 76)
        Me.cbxExportador.Name = "cbxExportador"
        Me.cbxExportador.Size = New System.Drawing.Size(86, 21)
        Me.cbxExportador.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(194, 80)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Exp.:"
        '
        'cbxInforme
        '
        Me.cbxInforme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxInforme.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxInforme.FormattingEnabled = True
        Me.cbxInforme.Items.AddRange(New Object() {"VENTAS", "COMPRAS", "RETENCIONES"})
        Me.cbxInforme.Location = New System.Drawing.Point(90, 25)
        Me.cbxInforme.Name = "cbxInforme"
        Me.cbxInforme.Size = New System.Drawing.Size(217, 24)
        Me.cbxInforme.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Informe:"
        '
        'txtRucCliente
        '
        Me.txtRucCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRucCliente.Color = System.Drawing.Color.Empty
        Me.txtRucCliente.Indicaciones = Nothing
        Me.txtRucCliente.Location = New System.Drawing.Point(148, 103)
        Me.txtRucCliente.Multilinea = False
        Me.txtRucCliente.Name = "txtRucCliente"
        Me.txtRucCliente.Size = New System.Drawing.Size(159, 21)
        Me.txtRucCliente.SoloLectura = False
        Me.txtRucCliente.TabIndex = 10
        Me.txtRucCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRucCliente.Texto = ""
        '
        'cbxMes
        '
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Location = New System.Drawing.Point(90, 53)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(142, 21)
        Me.cbxMes.TabIndex = 3
        '
        'lblrucnoValido
        '
        Me.lblrucnoValido.AutoSize = True
        Me.lblrucnoValido.Location = New System.Drawing.Point(3, 107)
        Me.lblrucnoValido.Name = "lblrucnoValido"
        Me.lblrucnoValido.Size = New System.Drawing.Size(146, 13)
        Me.lblrucnoValido.TabIndex = 9
        Me.lblrucnoValido.Text = "RUC no Valido remplazar por:"
        '
        'nudAño
        '
        Me.nudAño.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAño.Location = New System.Drawing.Point(234, 52)
        Me.nudAño.Maximum = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudAño.Name = "nudAño"
        Me.nudAño.Size = New System.Drawing.Size(73, 22)
        Me.nudAño.TabIndex = 4
        Me.nudAño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMes
        '
        Me.lblMes.AutoSize = True
        Me.lblMes.Location = New System.Drawing.Point(3, 56)
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Size = New System.Drawing.Size(54, 13)
        Me.lblMes.TabIndex = 2
        Me.lblMes.Text = "Mes/Año:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Tipo de Reporte:"
        '
        'cbxTipoReporte
        '
        Me.cbxTipoReporte.FormattingEnabled = True
        Me.cbxTipoReporte.Location = New System.Drawing.Point(90, 76)
        Me.cbxTipoReporte.Name = "cbxTipoReporte"
        Me.cbxTipoReporte.Size = New System.Drawing.Size(104, 21)
        Me.cbxTipoReporte.TabIndex = 6
        '
        'Button1
        '
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.Location = New System.Drawing.Point(12, 153)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(317, 31)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Generar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'gbxResultado
        '
        Me.gbxResultado.Controls.Add(Me.lklVerDetalle)
        Me.gbxResultado.Controls.Add(Me.txtDiferencia)
        Me.gbxResultado.Controls.Add(Me.Label4)
        Me.gbxResultado.Controls.Add(Me.txtTotalCabecera)
        Me.gbxResultado.Controls.Add(Me.Label3)
        Me.gbxResultado.Controls.Add(Me.txtTotalDetalle)
        Me.gbxResultado.Controls.Add(Me.txtCantidadDetalle)
        Me.gbxResultado.Controls.Add(Me.lblRegistros)
        Me.gbxResultado.Location = New System.Drawing.Point(12, 190)
        Me.gbxResultado.Name = "gbxResultado"
        Me.gbxResultado.Size = New System.Drawing.Size(317, 112)
        Me.gbxResultado.TabIndex = 3
        Me.gbxResultado.TabStop = False
        Me.gbxResultado.Text = "Resultado"
        '
        'lklVerDetalle
        '
        Me.lklVerDetalle.AutoSize = True
        Me.lklVerDetalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklVerDetalle.Location = New System.Drawing.Point(159, 16)
        Me.lklVerDetalle.Name = "lklVerDetalle"
        Me.lklVerDetalle.Size = New System.Drawing.Size(152, 13)
        Me.lklVerDetalle.TabIndex = 0
        Me.lklVerDetalle.TabStop = True
        Me.lklVerDetalle.Text = "Ver el detalle de registros"
        Me.lklVerDetalle.Visible = False
        '
        'txtDiferencia
        '
        Me.txtDiferencia.Color = System.Drawing.Color.Empty
        Me.txtDiferencia.Decimales = True
        Me.txtDiferencia.Indicaciones = Nothing
        Me.txtDiferencia.Location = New System.Drawing.Point(113, 78)
        Me.txtDiferencia.Name = "txtDiferencia"
        Me.txtDiferencia.Size = New System.Drawing.Size(138, 21)
        Me.txtDiferencia.SoloLectura = True
        Me.txtDiferencia.TabIndex = 7
        Me.txtDiferencia.TabStop = False
        Me.txtDiferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferencia.Texto = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Diferencia:"
        '
        'txtTotalCabecera
        '
        Me.txtTotalCabecera.Color = System.Drawing.Color.Empty
        Me.txtTotalCabecera.Decimales = True
        Me.txtTotalCabecera.Indicaciones = Nothing
        Me.txtTotalCabecera.Location = New System.Drawing.Point(113, 36)
        Me.txtTotalCabecera.Name = "txtTotalCabecera"
        Me.txtTotalCabecera.Size = New System.Drawing.Size(138, 21)
        Me.txtTotalCabecera.SoloLectura = True
        Me.txtTotalCabecera.TabIndex = 2
        Me.txtTotalCabecera.TabStop = False
        Me.txtTotalCabecera.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCabecera.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Total en Cabecera:"
        '
        'txtTotalDetalle
        '
        Me.txtTotalDetalle.Color = System.Drawing.Color.Empty
        Me.txtTotalDetalle.Decimales = True
        Me.txtTotalDetalle.Indicaciones = Nothing
        Me.txtTotalDetalle.Location = New System.Drawing.Point(113, 57)
        Me.txtTotalDetalle.Name = "txtTotalDetalle"
        Me.txtTotalDetalle.Size = New System.Drawing.Size(138, 21)
        Me.txtTotalDetalle.SoloLectura = True
        Me.txtTotalDetalle.TabIndex = 4
        Me.txtTotalDetalle.TabStop = False
        Me.txtTotalDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDetalle.Texto = "0"
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(257, 57)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(45, 21)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 5
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'lblRegistros
        '
        Me.lblRegistros.AutoSize = True
        Me.lblRegistros.Location = New System.Drawing.Point(9, 61)
        Me.lblRegistros.Name = "lblRegistros"
        Me.lblRegistros.Size = New System.Drawing.Size(96, 13)
        Me.lblRegistros.TabIndex = 3
        Me.lblRegistros.Text = "Total en el Detalle:"
        '
        'btnExportarExcel
        '
        Me.btnExportarExcel.BackgroundImage = CType(resources.GetObject("btnExportarExcel.BackgroundImage"), System.Drawing.Image)
        Me.btnExportarExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnExportarExcel.Enabled = False
        Me.btnExportarExcel.Location = New System.Drawing.Point(252, 305)
        Me.btnExportarExcel.Name = "btnExportarExcel"
        Me.btnExportarExcel.Size = New System.Drawing.Size(32, 32)
        Me.btnExportarExcel.TabIndex = 7
        Me.btnExportarExcel.UseVisualStyleBackColor = True
        '
        'btnExportarTXT
        '
        Me.btnExportarTXT.BackgroundImage = CType(resources.GetObject("btnExportarTXT.BackgroundImage"), System.Drawing.Image)
        Me.btnExportarTXT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnExportarTXT.Enabled = False
        Me.btnExportarTXT.Location = New System.Drawing.Point(290, 305)
        Me.btnExportarTXT.Name = "btnExportarTXT"
        Me.btnExportarTXT.Size = New System.Drawing.Size(32, 32)
        Me.btnExportarTXT.TabIndex = 8
        Me.btnExportarTXT.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Location = New System.Drawing.Point(14, 305)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(232, 32)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "EXPORTAR DATOS"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 348)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(342, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'frmHechaukaLibroVentas1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(342, 370)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnExportarExcel)
        Me.Controls.Add(Me.btnExportarTXT)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.gbxResultado)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmHechaukaLibroVentas1"
        Me.Tag = "frmHechaukaLibroVentas1"
        Me.Text = "frmHechaukaLibroVentas1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxResultado.ResumeLayout(False)
        Me.gbxResultado.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cbxExportador As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cbxInforme As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtRucCliente As ocxTXTString
    Friend WithEvents cbxMes As ComboBox
    Friend WithEvents lblrucnoValido As Label
    Friend WithEvents nudAño As NumericUpDown
    Friend WithEvents lblMes As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxTipoReporte As ComboBox
    Friend WithEvents Button1 As Button
    Friend WithEvents gbxResultado As GroupBox
    Friend WithEvents lklVerDetalle As LinkLabel
    Friend WithEvents txtDiferencia As ocxTXTNumeric
    Friend WithEvents Label4 As Label
    Friend WithEvents txtTotalCabecera As ocxTXTNumeric
    Friend WithEvents Label3 As Label
    Friend WithEvents txtTotalDetalle As ocxTXTNumeric
    Friend WithEvents txtCantidadDetalle As ocxTXTNumeric
    Friend WithEvents lblRegistros As Label
    Friend WithEvents btnExportarExcel As Button
    Friend WithEvents btnExportarTXT As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHechaukaLibrodeCompra
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHechaukaLibrodeCompra))
        Me.lblMes = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.cbxTipoReporte = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxExportador = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtRucProveedor = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.gbxResultado = New System.Windows.Forms.GroupBox()
        Me.lklVerDetalle = New System.Windows.Forms.LinkLabel()
        Me.txtDiferencia = New ERP.ocxTXTNumeric()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTotalCabecera = New ERP.ocxTXTNumeric()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtTotalDetalle = New ERP.ocxTXTNumeric()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.lblRegistros = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnExportarTXT = New System.Windows.Forms.Button()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.gbxResultado.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblMes
        '
        Me.lblMes.AutoSize = True
        Me.lblMes.Location = New System.Drawing.Point(7, 26)
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Size = New System.Drawing.Size(54, 13)
        Me.lblMes.TabIndex = 0
        Me.lblMes.Text = "Mes/Año:"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.NumericUpDown1.Location = New System.Drawing.Point(235, 21)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {0, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(73, 22)
        Me.NumericUpDown1.TabIndex = 3
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cbxMes
        '
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Location = New System.Drawing.Point(93, 22)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(142, 21)
        Me.cbxMes.TabIndex = 1
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 324)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(340, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'cbxTipoReporte
        '
        Me.cbxTipoReporte.FormattingEnabled = True
        Me.cbxTipoReporte.Location = New System.Drawing.Point(93, 45)
        Me.cbxTipoReporte.Name = "cbxTipoReporte"
        Me.cbxTipoReporte.Size = New System.Drawing.Size(103, 21)
        Me.cbxTipoReporte.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "T. de Reporte:"
        '
        'cbxExportador
        '
        Me.cbxExportador.FormattingEnabled = True
        Me.cbxExportador.Location = New System.Drawing.Point(257, 45)
        Me.cbxExportador.Name = "cbxExportador"
        Me.cbxExportador.Size = New System.Drawing.Size(51, 21)
        Me.cbxExportador.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(195, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Exportador:"
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(12, 126)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(317, 31)
        Me.btnProcesar.TabIndex = 8
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtRucProveedor)
        Me.GroupBox1.Controls.Add(Me.cbxMes)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox1.Controls.Add(Me.lblMes)
        Me.GroupBox1.Controls.Add(Me.cbxTipoReporte)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxExportador)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(317, 106)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Opciones"
        '
        'txtRucProveedor
        '
        Me.txtRucProveedor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRucProveedor.Color = System.Drawing.Color.Empty
        Me.txtRucProveedor.Indicaciones = Nothing
        Me.txtRucProveedor.Location = New System.Drawing.Point(218, 73)
        Me.txtRucProveedor.Multilinea = False
        Me.txtRucProveedor.Name = "txtRucProveedor"
        Me.txtRucProveedor.Size = New System.Drawing.Size(90, 20)
        Me.txtRucProveedor.SoloLectura = False
        Me.txtRucProveedor.TabIndex = 26
        Me.txtRucProveedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRucProveedor.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(204, 13)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "RUC no Valido Proveedor Remplazar Por:"
        '
        'gbxResultado
        '
        Me.gbxResultado.Controls.Add(Me.lklVerDetalle)
        Me.gbxResultado.Controls.Add(Me.txtDiferencia)
        Me.gbxResultado.Controls.Add(Me.Label4)
        Me.gbxResultado.Controls.Add(Me.txtTotalCabecera)
        Me.gbxResultado.Controls.Add(Me.Label5)
        Me.gbxResultado.Controls.Add(Me.txtTotalDetalle)
        Me.gbxResultado.Controls.Add(Me.txtCantidadDetalle)
        Me.gbxResultado.Controls.Add(Me.lblRegistros)
        Me.gbxResultado.Location = New System.Drawing.Point(12, 163)
        Me.gbxResultado.Name = "gbxResultado"
        Me.gbxResultado.Size = New System.Drawing.Size(317, 112)
        Me.gbxResultado.TabIndex = 25
        Me.gbxResultado.TabStop = False
        Me.gbxResultado.Text = "Resultado"
        '
        'lklVerDetalle
        '
        Me.lklVerDetalle.AutoSize = True
        Me.lklVerDetalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lklVerDetalle.Location = New System.Drawing.Point(159, 16)
        Me.lklVerDetalle.Name = "lklVerDetalle"
        Me.lklVerDetalle.Size = New System.Drawing.Size(152, 13)
        Me.lklVerDetalle.TabIndex = 0
        Me.lklVerDetalle.TabStop = True
        Me.lklVerDetalle.Text = "Ver el detalle de registros"
        '
        'txtDiferencia
        '
        Me.txtDiferencia.Color = System.Drawing.Color.Empty
        Me.txtDiferencia.Decimales = True
        Me.txtDiferencia.Indicaciones = Nothing
        Me.txtDiferencia.Location = New System.Drawing.Point(113, 78)
        Me.txtDiferencia.Name = "txtDiferencia"
        Me.txtDiferencia.Size = New System.Drawing.Size(138, 21)
        Me.txtDiferencia.SoloLectura = True
        Me.txtDiferencia.TabIndex = 7
        Me.txtDiferencia.TabStop = False
        Me.txtDiferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferencia.Texto = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Diferencia:"
        '
        'txtTotalCabecera
        '
        Me.txtTotalCabecera.Color = System.Drawing.Color.Empty
        Me.txtTotalCabecera.Decimales = True
        Me.txtTotalCabecera.Indicaciones = Nothing
        Me.txtTotalCabecera.Location = New System.Drawing.Point(113, 36)
        Me.txtTotalCabecera.Name = "txtTotalCabecera"
        Me.txtTotalCabecera.Size = New System.Drawing.Size(138, 21)
        Me.txtTotalCabecera.SoloLectura = True
        Me.txtTotalCabecera.TabIndex = 2
        Me.txtTotalCabecera.TabStop = False
        Me.txtTotalCabecera.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCabecera.Texto = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Total en Cabecera:"
        '
        'txtTotalDetalle
        '
        Me.txtTotalDetalle.Color = System.Drawing.Color.Empty
        Me.txtTotalDetalle.Decimales = True
        Me.txtTotalDetalle.Indicaciones = Nothing
        Me.txtTotalDetalle.Location = New System.Drawing.Point(113, 57)
        Me.txtTotalDetalle.Name = "txtTotalDetalle"
        Me.txtTotalDetalle.Size = New System.Drawing.Size(138, 21)
        Me.txtTotalDetalle.SoloLectura = True
        Me.txtTotalDetalle.TabIndex = 4
        Me.txtTotalDetalle.TabStop = False
        Me.txtTotalDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDetalle.Texto = "0"
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(257, 57)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(45, 21)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 5
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'lblRegistros
        '
        Me.lblRegistros.AutoSize = True
        Me.lblRegistros.Location = New System.Drawing.Point(9, 61)
        Me.lblRegistros.Name = "lblRegistros"
        Me.lblRegistros.Size = New System.Drawing.Size(96, 13)
        Me.lblRegistros.TabIndex = 3
        Me.lblRegistros.Text = "Total en el Detalle:"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label6.Location = New System.Drawing.Point(12, 278)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(232, 32)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "EXPORTAR DATOS"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Button1
        '
        Me.Button1.BackgroundImage = CType(resources.GetObject("Button1.BackgroundImage"), System.Drawing.Image)
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(250, 279)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(32, 32)
        Me.Button1.TabIndex = 27
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnExportarTXT
        '
        Me.btnExportarTXT.BackgroundImage = CType(resources.GetObject("btnExportarTXT.BackgroundImage"), System.Drawing.Image)
        Me.btnExportarTXT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnExportarTXT.Enabled = False
        Me.btnExportarTXT.Location = New System.Drawing.Point(288, 279)
        Me.btnExportarTXT.Name = "btnExportarTXT"
        Me.btnExportarTXT.Size = New System.Drawing.Size(32, 32)
        Me.btnExportarTXT.TabIndex = 28
        Me.btnExportarTXT.UseVisualStyleBackColor = True
        '
        'frmHechaukaLibrodeCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(340, 346)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnExportarTXT)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.gbxResultado)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnProcesar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmHechaukaLibrodeCompra"
        Me.Text = "frmHechaukaLibrodeCompras"
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbxResultado.ResumeLayout(False)
        Me.gbxResultado.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblMes As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbxMes As System.Windows.Forms.ComboBox
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoReporte As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxExportador As System.Windows.Forms.ComboBox
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRucProveedor As ERP.ocxTXTString
    Friend WithEvents gbxResultado As System.Windows.Forms.GroupBox
    Friend WithEvents lklVerDetalle As System.Windows.Forms.LinkLabel
    Friend WithEvents txtDiferencia As ERP.ocxTXTNumeric
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtTotalCabecera As ERP.ocxTXTNumeric
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTotalDetalle As ERP.ocxTXTNumeric
    Friend WithEvents txtCantidadDetalle As ERP.ocxTXTNumeric
    Friend WithEvents lblRegistros As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnExportarTXT As System.Windows.Forms.Button
End Class

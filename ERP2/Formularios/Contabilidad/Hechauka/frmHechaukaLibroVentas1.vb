﻿Public Class frmHechaukaLibroVentas1
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim vdtCabecera As DataTable
    Dim vdtDetalle As DataTable
    Dim vdtDetalleOriginal As DataTable
    Dim vdtRUCNoValidos As DataTable

    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Meses
        Dim Mes() As String = {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
        cbxMes.Items.AddRange(Mes)
        cbxMes.DropDownStyle = ComboBoxStyle.DropDownList
        cbxMes.SelectedIndex = Date.Now.Month - 1

        'Año
        nudAño.Minimum = Date.Now.Year - 10
        nudAño.Maximum = Date.Now.Year
        nudAño.Value = Date.Now.Year

        'Tipo de Reporte
        cbxTipoReporte.Items.Add("ORIGINAL")
        cbxTipoReporte.Items.Add("RECTIFICATIVA")
        cbxTipoReporte.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoReporte.SelectedIndex = 0

        'Exportador
        cbxExportador.Items.Add("SI")
        cbxExportador.Items.Add("NO")
        cbxExportador.SelectedIndex = 1
        cbxExportador.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Sub ExportarExcel()
        'SC:21/02/2022 - Nuevo
        Dim Mes As String
        If cbxMes.SelectedIndex >= 10 Then
            Mes = cbxMes.SelectedIndex + 1
        Else
            Mes = "0" & cbxMes.SelectedIndex + 1
        End If
        'hasta aqui SC - Nuevo

        'Dim Nombre As String = "HechaukaVenta " & nudAño.Value & " " & cbxMes.Text & ".xls"
        Dim Nombre As String = "80008448_REG_ " & Mes & nudAño.Value & " " & ".CSV"
        Dim NombreRUCNoValido As String = "HechaukaVentaRUCNoValidos " & nudAño.Value & " " & cbxMes.Text & ".xls"
        Dim cantdetalle As Integer
        Dim ds As New DataSet
        'ds.Tables.Add(vdtCabecera.Copy)
        ds.Tables.Add(vdtDetalle.Copy)
        cantdetalle = vdtDetalle.Rows.Count

        CSistema.dtToExcel3(ds, Nombre, cantdetalle)

        If vdtRUCNoValidos Is Nothing Then
            Exit Sub
        End If

        If vdtRUCNoValidos.Rows.Count > 0 Then
            Dim dsRUCInvalido As New DataSet
            dsRUCInvalido.Tables.Add(vdtRUCNoValidos.Copy)
            CSistema.dtToExcel(dsRUCInvalido, NombreRUCNoValido)
        End If

    End Sub

    Private Sub DatatableToExcel1(ByVal dtTemp As DataTable)

        Try

            Dim _excel As Excel.Application
            Dim wBook As Excel.Workbook
            Dim wSheet As Excel.Worksheet

            _excel = CType(CreateObject("Excel.application"), Excel.Application)
            wBook = CType(_excel.Workbooks.Add, Excel.Workbook)
            wSheet = CType(wBook.Worksheets(1), Excel.Worksheet)

            Dim dt As System.Data.DataTable = dtTemp
            Dim dc As System.Data.DataColumn
            Dim dr As System.Data.DataRow
            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0

            For Each dc In dt.Columns
                colIndex = colIndex + 1
                _excel.Cells(1, colIndex) = dc.ColumnName
            Next

            For Each dr In dt.Rows
                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In dt.Columns
                    colIndex = colIndex + 1
                    _excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                Next
            Next

            wSheet.Columns.AutoFit()

            Dim strFileName As String = "C:\Documents and Settings\All Users\Escritorio\HechaukaVentaEncabezado.xlsx"
            If System.IO.File.Exists(strFileName) Then
                System.IO.File.Delete(strFileName)
            End If

            Dim savefile As New SaveFileDialog
            savefile.AddExtension = True
            savefile.Filter = "Archivos de texto|*.xlsx"
            savefile.FileName = "HechaukaVenta" & cbxMes.Text
            savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

            If savefile.ShowDialog = DialogResult.OK Then
                wBook.SaveAs(strFileName)
            End If
            wBook.Close()
            _excel.Quit()

        Catch ex As Exception

            Try
                CSistema.dtToOO(dtTemp)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Try

    End Sub

    Private Sub DatatableToExcel2(ByVal dtTemp As DataTable)
        Try
            Dim _excel As Excel.Application
            Dim wBook As Excel.Workbook
            Dim wSheet As Excel.Worksheet

            _excel = CType(CreateObject("Excel.application"), Excel.Application)
            wBook = CType(_excel.Workbooks.Add, Excel.Workbook)
            wSheet = CType(wBook.Worksheets(1), Excel.Worksheet)

            Dim dt As System.Data.DataTable = dtTemp
            Dim dc As System.Data.DataColumn
            Dim dr As System.Data.DataRow
            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0

            For Each dc In dt.Columns
                colIndex = colIndex + 1
                _excel.Cells(1, colIndex) = dc.ColumnName
            Next

            For Each dr In dt.Rows
                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In dt.Columns
                    colIndex = colIndex + 1
                    _excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                Next
            Next

            wSheet.Columns.AutoFit()

            Dim strFileName As String = "C:\Documents and Settings\All Users\Escritorio\HechaukaVentaDetalle.xlsx"
            If System.IO.File.Exists(strFileName) Then
                System.IO.File.Delete(strFileName)
            End If

            Dim savefile As New SaveFileDialog
            savefile.AddExtension = True
            savefile.Filter = "Archivos de texto|*.xlsx"
            savefile.FileName = "HechaukaVenta" & cbxMes.Text
            savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

            If savefile.ShowDialog = DialogResult.OK Then
                wBook.SaveAs(strFileName)
            End If

            wBook.Close()
            _excel.Quit()
            MsgBox("Registro Guardado.!")
            Me.Close()
        Catch ex As Exception

            Try
                CSistema.dtToOO(dtTemp)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Try
    End Sub

    Sub Generar()

        If cbxInforme.SelectedIndex < 0 Then
            MessageBox.Show("Seleccione un tipo de reporte.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim TotalCabecera As Decimal
        Dim TotalDetalle As Decimal
        Dim TotalDiferencia As Decimal

        'Tipo de Informe
        Select Case cbxInforme.SelectedIndex
            Case 0
                HechaukaVenta()

                TotalCabecera = vdtCabecera.Rows(0)("MontoTotal")
                TotalDetalle = CSistema.dtSumColumn(vdtDetalle, "MontoTotalDelIngreso")
                TotalDiferencia = TotalCabecera - TotalDetalle

                'Cambiar el total en la cabecera
                vdtCabecera.Rows(0)("MontoTotal") = TotalDetalle

                'Cambiar la candidad del detalle
                vdtCabecera.Rows(0)("CantidadRegistros") = vdtDetalle.Rows.Count

            Case 1

                HechaukaCompra()

                TotalCabecera = vdtCabecera.Rows(0)("MontoTotal")
                TotalDetalle = CSistema.dtSumColumn(vdtDetalle, "MontoCompra10%") + CSistema.dtSumColumn(vdtDetalle, "MontoCompra5%") + CSistema.dtSumColumn(vdtDetalle, "MontoCompraNoGrabada") + CSistema.dtSumColumn(vdtDetalle, "IVACredito10%") + CSistema.dtSumColumn(vdtDetalle, "IVACredito5%")
                txtDiferencia.SetValue(TotalCabecera - TotalDetalle)
                TotalDiferencia = TotalCabecera - TotalDetalle

                'Cambiar el total en la cabecera
                vdtCabecera.Rows(0)("MontoTotal") = TotalDetalle

                'Cambiar la candidad del detalle
                vdtCabecera.Rows(0)("CantidadRegistros") = vdtDetalle.Rows.Count

            Case 2

                HechaukaRetencion()

                TotalCabecera = vdtCabecera.Rows(0)("MontoTotal")
                TotalDetalle = CSistema.dtSumColumn(vdtDetalle, "ImporteRetenidoIVA")
                txtDiferencia.SetValue(TotalCabecera - TotalDetalle)
                TotalDiferencia = TotalCabecera - TotalDetalle

                'Cambiar el total en la cabecera
                vdtCabecera.Rows(0)("MontoTotal") = TotalDetalle

                'Cambiar la candidad del detalle
                vdtCabecera.Rows(0)("CantidadRegistros") = vdtDetalle.Rows.Count

        End Select

        'Resumen
        txtTotalCabecera.SetValue(TotalCabecera)
        txtTotalDetalle.SetValue(TotalDetalle)
        txtCantidadDetalle.SetValue(vdtDetalle.Rows.Count)
        txtDiferencia.SetValue(TotalDiferencia)



        'Habilitar los botones
        btnExportarExcel.Enabled = True
        btnExportarTXT.Enabled = True

    End Sub

    Sub HechaukaVenta()

        'Generar las tablas
        GenerarCabeceraVenta()
        GenerarDetalleVenta()

        Dim CI As New DataColumn
        Dim RUCCliente As New DataColumn
        vdtDetalle.Columns.Remove("CI") 'SelCI
        vdtDetalle.Columns.Remove("RUCCliente") 'SelRUC

        '21/02/2022- SC- Nuevo
        Dim frm As New frmHechaukaSeleccionar
        frm.Text = "Selección de Comprobantes de Venta a Presentar"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        frm.dt = vdtDetalle

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccion de Comprobantes de Venta para Presentar", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        vdtDetalle = frm.dt

        Dim ColSeleccionado As New DataColumn
        Dim Idtransaccion As New DataColumn

        vdtDetalle.Columns.Remove("Seleccionado") ''Seleccionado
        vdtDetalle.Columns.Remove("Idtransaccion") 'IDTransaccion


    End Sub

    Sub HechaukaCompra()

        GenerarCabeceraCompra()
        GenerarDetalleCompra()

        '21/02/2022- SC- Nuevo
        Dim frm As New frmHechaukaSeleccionarCompra
        frm.Text = "Selección de Comprobantes de Compra a Presentar"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        frm.dt = vdtDetalle

        frm.Inicializar()
        FGMostrarFormulario(Me, frm, "Seleccion de Comprobantes de Compra para Presentar", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        vdtDetalle = frm.dt

        Dim ColSeleccionado As New DataColumn
        Dim Idtransaccion As New DataColumn

        vdtDetalle.Columns.Remove("Idtransaccion") 'IDTransaccion
        vdtDetalle.Columns.Remove("Seleccionado") 'RUCCliente

    End Sub

    Sub HechaukaRetencion()

        GenerarCabeceraRetencion()
        GenerarDetalleRetencion()

    End Sub

    Sub GenerarCabeceraVenta()

        vdtCabecera = CSistema.ExecuteToDataTable("Execute SpHechaukaVentaEncabezado @Año=" & nudAño.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & "," & "@TipoReporte=" & cbxTipoReporte.SelectedIndex + 1 & "", "", 1000)

    End Sub

    Sub GenerarDetalleVenta()

        vdtDetalle = Nothing
        'vdtDetalle = CSistema.ExecuteToDataTable("Execute spHechaukaVentaDetalle @Año=" & nudAño.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & " ", "", 1000)
        vdtDetalle = CSistema.ExecuteToDataTable("Execute spHechaukaVentaDetalle1 @Año=" & nudAño.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & " ", "", 1000)


        Dim Comprobante As String
        Dim Original As String
        Dim Adicional As String
        Dim Cant As Integer = 0
        Dim Cant1 As Integer = 0
        Dim Diferencia As Integer = 7

        If vdtDetalle Is Nothing Then
            MessageBox.Show("No se genero el detalle!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If vdtDetalle.Rows.Count = 0 Then
            MessageBox.Show("No se genero el detalle!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Agregar una Columna al datable
        'Cedula
        'Dim ColSelCi As New DataColumn
        'ColSelCi.DataType = System.Type.GetType("System.Boolean")
        'ColSelCi.DefaultValue = 0
        'ColSelCi.ColumnName = "SelCi"
        'vdtDetalle.Columns.Add(ColSelCi)

        'RUC
        'Dim ColSelRuc As New DataColumn
        'ColSelRuc.DataType = System.Type.GetType("System.Boolean")
        'ColSelRuc.DefaultValue = 0
        'ColSelRuc.ColumnName = "SelRuc"
        'vdtDetalle.Columns.Add(ColSelRuc)

        'Seleccionado Comprobante- 21/02/2022: SC - Nuevo
        Dim ColSeleccionado As New DataColumn
        ColSeleccionado.DataType = System.Type.GetType("System.Boolean")
        ColSeleccionado.DefaultValue = 0
        ColSeleccionado.ColumnName = "Seleccionado"
        vdtDetalle.Columns.Add(ColSeleccionado)

        'SC: 18/02/2022
        Dim i As Integer = 0

        For Each oRow As DataRow In vdtDetalle.Rows
            Comprobante = oRow("NumeroDocumento")
            Original = Comprobante.Substring(0, 8) '001-001- hasta aca extrae segun el ejemplo
            Adicional = Comprobante.Substring(8) 'serian los ultimos numeros posterior al segundo guion
            Cant = Adicional.Length 'extra la longitud de los ultimos digitos extraidos en la linea anterior
            Cant1 = Diferencia - Cant ' calcula la cantidad de caracteres que faltan para rellenar con ceros
            Comprobante = Original

            For i = 1 To Cant1
                Comprobante = Comprobante & "0"
            Next
            Comprobante = Comprobante & Adicional
            oRow("NumeroDocumento") = Comprobante
            oRow("Seleccionado") = False
        Next
        'SC: 18/02/2022

        Dim i2 As Integer = 0
        For Each oRow As DataRow In vdtDetalle.Rows
            If oRow("NumeroDelComprobanteDeVentaAsociado") <> "" Then
                Comprobante = oRow("NumeroDelComprobanteDeVentaAsociado")
                Original = Comprobante.Substring(0, 8) '001-001- hasta aca extrae segun el ejemplo
                Adicional = Comprobante.Substring(8) 'serian los ultimos numeros posterior al segundo guion
                Cant = Adicional.Length 'extra la longitud de los ultimos digitos extraidos en la linea anterior
                Cant1 = Diferencia - Cant ' calcula la cantidad de caracteres que faltan para rellenar con ceros
                Comprobante = Original

                For i2 = 1 To Cant1
                    Comprobante = Comprobante & "0"
                Next
                Comprobante = Comprobante & Adicional
                oRow("NumeroDelComprobanteDeVentaAsociado") = Comprobante
            End If
        Next

        'Verificamos si es que se haya definido el RUC generico
        If txtRucCliente.txt.Text = "" Then
            Exit Sub
        End If

        'Validar
        Dim Ruc As String = txtRucCliente.txt.Text
        Dim Dv As String = ""

        If txtRucCliente.GetValue.Contains("-") Then
            Ruc = txtRucCliente.txt.Text.Split("-")(0).Trim()
            Dv = txtRucCliente.txt.Text.Split("-")(1).Trim()
        End If

        'Si es numero de cedula  lo remplazamos por el RUC 
        For Each oRow As DataRow In vdtDetalle.Rows
            If CSistema.ValidarDigitoVerificador(oRow("Ruc"), False, True) = False Or oRow("CI") = True Then
                'oRow("RucProveedor") = Ruc
                'oRow("DVProveedor") = Dv
                'oRow("SelCi") = True

                'SC:18/02/2021
                'Ruc = oRow("RucCliente")
                oRow("Ruc") = Ruc
                oRow("SelCi") = True
            End If
        Next


        'Agregamos al dt en una sola  fila las CI no validos
        Dim NewRow As DataRow = vdtDetalle.NewRow

        'SC:18/02/2022
        'NewRow("TipoDocumento") = "0"
        NewRow("MontoVenta10%") = 0

        'SC:18/02/2022
        'NewRow("IVADebito10%") = 0

        NewRow("MontoVenta5%") = 0
        'SC:18/02/2022
        'NewRow("IVADebito5%") = 0
        NewRow("MontoVentaNoGrabada") = 0

        'SC:18/02/2022
        'NewRow("MontodelIngreso") = 0
        NewRow("NumeroTimbrado") = "0"
        NewRow("NumeroDocumento") = "0"
        NewRow("CondicionVenta") = "1"

        For Each oRow As DataRow In vdtDetalle.Select("SelCi='True'")
            NewRow("MontoVenta10%") = NewRow("MontoVenta10%") + oRow("MontoVenta10%")
            NewRow("MontoVenta5%") = NewRow("MontoVenta5%") + oRow("MontoVenta5%")
            NewRow("MontoVentaNoGrabada") = NewRow("MontoVentaNoGrabada") + oRow("MontoVentaNoGrabada")

            'SC: 09-09-2021 para toma aqui el valor de IVADebito 5 y 10%
            'NewRow("IVADebito10%") = NewRow("IVADebito10%") + oRow("IVADebito10%")
            'NewRow("IVADebito5%") = NewRow("IVADebito5%") + oRow("IVADebito5%")
        Next

        NewRow("SelCi") = False
        'SC:21/02/2022
        'NewRow("TipoRegistro") = 2

        NewRow("TipoRegistro") = 1
        'SC:18/02/2022
        NewRow("RUCCliente") = Ruc
        NewRow("RUC") = Ruc

        'NewRow("RUCProveedor") = Ruc
        'NewRow("DVProveedor") = Dv
        NewRow("NombreCliente") = "Importes Consolidados"

        NewRow("FechaDocumento") = CSistema.FormatDobleDigito(Date.DaysInMonth(nudAño.Value, cbxMes.SelectedIndex + 1)) & "/" & CSistema.FormatDobleDigito(cbxMes.SelectedIndex + 1) & "/" & nudAño.Value

        'SC: 09-09-2021 se comenta y se toma el valor de vdtDetalle
        'Calcular el IVA
        'NewRow("IVADebito10%") = NewRow("MontoVenta10%") * 0.1
        'NewRow("IVADebito5%") = NewRow("MontoVenta5%") * 0.05

        'Volvemos a sumar el total
        'NewRow("MontodelIngreso") = NewRow("MontoVenta10%") + NewRow("MontoVenta5%") + NewRow("IVADebito10%") + NewRow("IVADebito5%") + NewRow("MontoVentaNoGrabada")
        NewRow("MontoTotalDelIngreso") = NewRow("MontoVenta10%") + NewRow("MontoVenta5%") + NewRow("MontoVentaNoGrabada")

        'Generamos los RUC Invalidos
        GenerarRUCNoValidos()

        'Agregamos el ruc generico
        vdtDetalle.Rows.Add(NewRow)

        'Quitamos los RUC NO validos, porque ya estan agrupados en el detalle
        Dim CData As New CData
        vdtDetalle = CData.FiltrarDataTable(vdtDetalle, "SelCI='False'").Copy

        'Enviar a una dt para seleccionar
    End Sub

    Sub GenerarCabeceraCompra()

        vdtCabecera = CSistema.ExecuteToDataTable("Execute SpHechaukaCompraEncabezado @Año=" & nudAño.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & "," & "@TipoReporte=" & cbxTipoReporte.SelectedIndex + 1 & "," & "@Exportador=" & cbxExportador.Text & " ", "", 1000)

    End Sub

    Sub GenerarDetalleCompra()

        vdtDetalle = Nothing
        'vdtDetalle = CSistema.ExecuteToDataTable("Execute spHechaukaCompraDetalle @Año=" & nudAño.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & "," & "@Exportador=" & cbxExportador.Text & "  ", "", 1000)
        vdtDetalle = CSistema.ExecuteToDataTable("Execute spHechaukaCompraDetalle1 @Año=" & nudAño.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & "," & "@Exportador=" & cbxExportador.Text & "  ", "", 1000)

        Dim Comprobante As String
        Dim Original As String
        Dim Adicional As String
        Dim Cant As Integer = 0
        Dim Cant1 As Integer = 0
        Dim Diferencia As Integer = 7

        If vdtDetalle Is Nothing Then
            MessageBox.Show("No se genero el detalle!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If vdtDetalle.Rows.Count = 0 Then
            MessageBox.Show("No se genero el detalle!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Seleccionado Comprobante- 21/02/2022: SC - Nuevo
        Dim ColSeleccionado As New DataColumn
        ColSeleccionado.DataType = System.Type.GetType("System.Boolean")
        ColSeleccionado.DefaultValue = 0
        ColSeleccionado.ColumnName = "Seleccionado"
        vdtDetalle.Columns.Add(ColSeleccionado)

        'SC: 18/02/2022
        Dim i As Integer = 0

        For Each oRow As DataRow In vdtDetalle.Rows

            Comprobante = oRow("NumeroDocumento")
            If (Comprobante.Length) >= 9 Then
                Original = Comprobante.Substring(0, 8) '001-001- hasta aca extrae segun el ejemplo
                Adicional = Comprobante.Substring(8) 'serian los ultimos numeros posterior al segundo guion
                Cant = Adicional.Length 'extra la longitud de los ultimos digitos extraidos en la linea anterior
                Cant1 = Diferencia - Cant ' calcula la cantidad de caracteres que faltan para rellenar con ceros
                Comprobante = Original

                For i = 1 To Cant1
                    Comprobante = Comprobante & "0"
                Next
                Comprobante = Comprobante & Adicional
                oRow("NumeroDocumento") = Comprobante
                oRow("Seleccionado") = False
            End If
        Next
        'SC: 18/02/2022
        Dim i2 As Integer = 0
        For Each oRow As DataRow In vdtDetalle.Rows
            If oRow("NumeroDelComprobanteDeVentaAsociado") <> "" Then
                Comprobante = oRow("NumeroDelComprobanteDeVentaAsociado")
                Original = Comprobante.Substring(0, 8) '001-001- hasta aca extrae segun el ejemplo
                Adicional = Comprobante.Substring(8) 'serian los ultimos numeros posterior al segundo guion
                Cant = Adicional.Length 'extra la longitud de los ultimos digitos extraidos en la linea anterior
                Cant1 = Diferencia - Cant ' calcula la cantidad de caracteres que faltan para rellenar con ceros
                Comprobante = Original

                For i2 = 1 To Cant1
                    Comprobante = Comprobante & "0"
                Next
                Comprobante = Comprobante & Adicional
                oRow("NumeroDelComprobanteDeVentaAsociado") = Comprobante
            End If
        Next

        'Verificamos si es que se haya definido el RUC generico
        If txtRucCliente.txt.Text = "" Then
            Exit Sub
        End If

        'Validar
        Dim Ruc As String = txtRucCliente.txt.Text
        Dim Dv As String = ""

        If txtRucCliente.GetValue.Contains("-") Then
            Ruc = txtRucCliente.txt.Text.Split("-")(0).Trim()
            Dv = txtRucCliente.txt.Text.Split("-")(1).Trim()
        End If

        For Each oRow As DataRow In vdtDetalle.Rows
            If IsNumeric(oRow("RucProveedor")) = False Then
                oRow("RucProveedor") = Ruc
                ' oRow("DVProveedor") = Dv
                Continue For
            End If

            If CSistema.ValidarDigitoVerificador(oRow("RucProveedor") & "-" & oRow("DVProveedor"), False) = False Then
                oRow("RucProveedor") = Ruc
                'oRow("DVProveedor") = Dv
            End If

        Next


    End Sub

    Sub GenerarCabeceraRetencion()

        vdtCabecera = CSistema.ExecuteToDataTable("Execute SpHechaukaRetencionEncabezado @Año=" & nudAño.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & "," & "@TipoReporte=" & cbxTipoReporte.SelectedIndex + 1 & " ", "", 1000)

    End Sub

    Sub GenerarDetalleRetencion()

        vdtDetalle = Nothing
        vdtDetalle = CSistema.ExecuteToDataTable("Execute spHechaukaRetencionDetalle @Año=" & nudAño.Value & "," & "@Mes= " & cbxMes.SelectedIndex + 1 & " ", "", 1000)

        If vdtDetalle Is Nothing Then
            MessageBox.Show("No se genero el detalle!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If vdtDetalle.Rows.Count = 0 Then
            MessageBox.Show("No se genero el detalle!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Verificamos si es que se haya definido el RUC generico
        If txtRucCliente.txt.Text = "" Then
            Exit Sub
        End If

        'Validar
        Dim Ruc As String = txtRucCliente.txt.Text
        Dim Dv As String = ""

        If txtRucCliente.GetValue.Contains("-") Then
            Ruc = txtRucCliente.txt.Text.Split("-")(0).Trim()
            Dv = txtRucCliente.txt.Text.Split("-")(1).Trim()
        End If

        For Each oRow As DataRow In vdtDetalle.Rows
            If IsNumeric(oRow("RucProveedor")) = False Then
                oRow("RucProveedor") = Ruc
                oRow("DVProveedor") = Dv
                Continue For
            End If

            If CSistema.ValidarDigitoVerificador(oRow("RucProveedor") & "-" & oRow("DVProveedor"), False) = False Then
                oRow("RucProveedor") = Ruc
                oRow("DVProveedor") = Dv
            End If

        Next


    End Sub

    Sub GenerarRUCNoValidos()

        ''Cargamos los Ruc no validos
        Dim dt1 As DataTable
        dt1 = vdtDetalle.Copy

        'Eliminamos el ultimo, que es el RUC generico
        Dim i As Integer = dt1.Rows.Count - 1
        'Solo si existe
        If dt1.Rows(i)("RUC").ToString = txtRucCliente.GetValue.Trim Then
            dt1.Rows(i).Delete()
        End If

        'Verificamos que el RUC sea correcto
        For Each oRow As DataRow In dt1.Rows
            If CSistema.ValidarDigitoVerificador(oRow("Ruc").ToString, False, True) = False Then
                oRow("SelRuc") = True
            End If
        Next

        'Filtrar la tabala Ruc no validos
        vdtRUCNoValidos = CSistema.FiltrarDataTable(dt1, "SelRuc=" & True)

    End Sub

    Sub ExportarTXT()

        Dim NombreArchivo As String = nudAño.Value & " Hechauka" & cbxInforme.Text & " " & cbxMes.Text & ".txt"
        Dim NombreArchivoRUCNoValido As String = nudAño.Value & " Hechauka" & cbxInforme.Text & " NoValido" & cbxMes.Text & ".txt"

        Dim file As String = VGCarpetaTemporal & "\" & NombreArchivo
        Dim fileRUCNoValido As String = VGCarpetaTemporal & "\" & NombreArchivoRUCNoValido

        If IO.Directory.Exists(VGCarpetaTemporal) = False Then

            'Eliminar
            IO.Directory.CreateDirectory(VGCarpetaTemporal)

        End If

        'Eliminamos si existe
        Try
            If IO.File.Exists(file) = True Then IO.File.Delete(file)
            If IO.File.Exists(fileRUCNoValido) = True Then IO.File.Delete(fileRUCNoValido)
        Catch ex As Exception

        End Try

        'CABECERA
        Dim sw As New System.IO.StreamWriter(file, False)
        Dim cadena As String = ""
        For Each oRow As DataRow In vdtCabecera.Rows
            cadena = ""
            For c As Integer = 0 To vdtCabecera.Columns.Count - 1
                If IsNumeric(oRow(c).ToString) Then
                    If oRow(c).ToString.Contains(",") Then
                        Dim vvalor As String = oRow(c).ToString
                        vvalor = vvalor.Split(",")(0)
                        cadena = cadena & vvalor & vbTab
                    Else
                        cadena = cadena & oRow(c).ToString & vbTab
                    End If
                Else
                    cadena = cadena & oRow(c).ToString & vbTab
                End If
            Next
            sw.WriteLine(cadena)
        Next

        'DETALLE
        cadena = ""
        Dim ColumnasNoValidas(-1) As String

        Select Case cbxInforme.SelectedIndex
            Case 0
                ReDim ColumnasNoValidas(3)
                ColumnasNoValidas(0) = "Ruc"
                ColumnasNoValidas(1) = "SelCi"
                ColumnasNoValidas(2) = "SelRuc"
                ColumnasNoValidas(3) = "CI"
            Case 1
                ReDim ColumnasNoValidas(3)
        End Select

        For Each oRow As DataRow In vdtDetalle.Rows
            cadena = ""
            For c As Integer = 0 To vdtDetalle.Columns.Count - 1

                'Ocultamos las columnas
                If ColumnasNoValidas.Contains(vdtDetalle.Columns(c).ColumnName) = True Then
                    'If vdtDetalle.Columns(c).ColumnName = "Ruc" Or vdtDetalle.Columns(c).ColumnName = "SelCi" Or vdtDetalle.Columns(c).ColumnName = "SelRUC" Or vdtDetalle.Columns(c).ColumnName = "CI" Then
                    Continue For
                Else
                    If IsNumeric(oRow(c).ToString) Then
                        If oRow(c).ToString.Contains(",") Then
                            Dim vvalor As String = oRow(c).ToString
                            vvalor = vvalor.Split(",")(0)
                            cadena = cadena & vvalor & vbTab
                        Else
                            cadena = cadena & oRow(c).ToString & vbTab
                        End If
                    Else
                        cadena = cadena & oRow(c).ToString & vbTab
                    End If

                    'End If
                End If


            Next
            sw.WriteLine(cadena)
        Next

        sw.Close()

        'RUC NO VALIDOS
        If Not vdtRUCNoValidos Is Nothing Then
            If vdtRUCNoValidos.Rows.Count > 0 Then

                sw = New System.IO.StreamWriter(fileRUCNoValido, False)
                cadena = ""

                For Each oRow As DataRow In vdtRUCNoValidos.Rows
                    cadena = ""
                    For c As Integer = 0 To vdtRUCNoValidos.Columns.Count - 1
                        cadena = cadena & oRow(c).ToString & vbTab
                    Next
                    sw.WriteLine(cadena)
                Next

                sw.Close()

            End If
        End If

        'CABECERA
        'Convertir de mayuscula a minuscula solo la primera letra en mayuscula y los demas minuscula
        Dim Mes As String = cbxMes.Text
        Mes = Mes.Substring(0, 1).ToUpper() & Mes.Substring(1, Mes.Length - 1).ToLower

        Dim SaveFolder As New FolderBrowserDialog
        SaveFolder.Description = "Guardar"
        SaveFolder.ShowNewFolderButton = True

        If SaveFolder.ShowDialog = DialogResult.OK Then

            FileCopy(file, SaveFolder.SelectedPath & "\" & NombreArchivo)

            'Generar los RUC no validos, solo si tienen registros
            If Not vdtRUCNoValidos Is Nothing Then
                If vdtRUCNoValidos.Rows.Count > 0 Then
                    FileCopy(fileRUCNoValido, SaveFolder.SelectedPath & "\" & NombreArchivoRUCNoValido)
                End If
            End If

        End If

    End Sub

    Private Sub frmHechaukaLibroVentas_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RUC GENERICO", txtRucCliente.GetValue)
    End Sub

    Private Sub frmHechaukaLibrodeComprasEncabezado_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmHechaukaLibrodeComprasEncabezado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportarTXT.Click
        ExportarTXT()
    End Sub

    Private Sub btnExportarExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        ExportarExcel()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Generar()
    End Sub

    Private Sub lklVerDetalle_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklVerDetalle.LinkClicked

        Dim frm As New Form
        frm.WindowState = FormWindowState.Maximized

        Dim dgv As New DataGridView
        Dim rightClick As New ContextMenuStrip()
        Dim item As New ToolStripMenuItem("Exportar a Excel")

        dgv.Dock = DockStyle.Fill
        dgv.ContextMenuStrip = rightClick

        rightClick.Items.AddRange(New System.Windows.Forms.ToolStripItem() {item})
        AddHandler item.Click, AddressOf FGToolStripMenuItem_Click

        dgv.DataSource = vdtDetalle

        frm.Controls.Add(dgv)

        FGMostrarFormulario(Me, frm, "Detalle de Registros", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

    End Sub

    Private Sub cbxInforme_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxInforme.SelectedIndexChanged
        'Ruc no validos por tipo de informe
        txtRucCliente.txt.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name & " " & cbxInforme.Text, "RUC GENERICO", "")
    End Sub

    Private Sub txtRucCliente_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRucCliente.Leave
        'Ruc no validos por tipo de informe
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name & " " & cbxInforme.Text, "RUC GENERICO", txtRucCliente.txt.Text)
    End Sub

End Class
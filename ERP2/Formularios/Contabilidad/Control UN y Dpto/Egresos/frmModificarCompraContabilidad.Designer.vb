﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmModificarCompraContabilidad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.chkfe = New System.Windows.Forms.CheckBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(11, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(129, 13)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "Departamento Solicitante:"
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(146, 35)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(263, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 36
        Me.cbxDepartamento.Texto = ""
        '
        'chkfe
        '
        Me.chkfe.AutoSize = True
        Me.chkfe.Location = New System.Drawing.Point(95, 12)
        Me.chkfe.Name = "chkfe"
        Me.chkfe.Size = New System.Drawing.Size(182, 17)
        Me.chkfe.TabIndex = 35
        Me.chkfe.Text = "Es Fact Electrónica / Fact Virtual"
        Me.chkfe.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(334, 62)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 34
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(246, 62)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 33
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'frmModificarCompraContabilidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(428, 100)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cbxDepartamento)
        Me.Controls.Add(Me.chkfe)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnGuardar)
        Me.Name = "frmModificarCompraContabilidad"
        Me.Text = "frmModificarCompraContabilidad"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label4 As Label
    Friend WithEvents cbxDepartamento As ocxCBX
    Friend WithEvents chkfe As CheckBox
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnGuardar As Button
End Class

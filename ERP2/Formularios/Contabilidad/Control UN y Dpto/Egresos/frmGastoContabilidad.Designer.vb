﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGastoContabilidad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblFormaPago = New System.Windows.Forms.Label()
        Me.lblTipoIVA = New System.Windows.Forms.Label()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbxVale = New System.Windows.Forms.GroupBox()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.lvVale = New System.Windows.Forms.ListView()
        Me.colIDTransaccion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colFecha = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colMotivo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colNombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colTotal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colSaldo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colImporte = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txtTotalComprobante = New ERP.ocxTXTNumeric()
        Me.lklAgregarVale = New System.Windows.Forms.LinkLabel()
        Me.lklEliminarVale = New System.Windows.Forms.LinkLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ChkFE = New System.Windows.Forms.CheckBox()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnCuota = New System.Windows.Forms.Button()
        Me.lblPlazo = New System.Windows.Forms.Label()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblDistribucion = New System.Windows.Forms.Label()
        Me.chkGastoMultiple = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnVerProducto = New System.Windows.Forms.Button()
        Me.pnlDistribucion = New System.Windows.Forms.Panel()
        Me.cbxCamion = New ERP.ocxCBX()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.chkAcuerdoAsociado = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtVtoTimbrado = New ERP.ocxTXTDate()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxDepartamentoEmpresa = New ERP.ocxCBX()
        Me.lblDepartamento = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtVencimiento = New ERP.ocxTXTDate()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.lblCondicion = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxCotizacion()
        Me.lblDepartamentoSolicitante = New System.Windows.Forms.Label()
        Me.txtPlazo = New ERP.ocxTXTNumeric()
        Me.chkCuota = New ERP.ocxCHK()
        Me.chkEfectivo = New ERP.ocxCHK()
        Me.chkCheque = New ERP.ocxCHK()
        Me.OcxImpuesto1 = New ERP.ocxImpuesto()
        Me.chkIncluirLibro = New ERP.ocxCHK()
        Me.cbxTipoIVA = New ERP.ocxCBX()
        Me.cbxSeccion = New ERP.ocxCBX()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.flpRegistradoPor.SuspendLayout()
        Me.gbxVale.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDistribucion.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblFormaPago
        '
        Me.lblFormaPago.AutoSize = True
        Me.lblFormaPago.Location = New System.Drawing.Point(174, 259)
        Me.lblFormaPago.Name = "lblFormaPago"
        Me.lblFormaPago.Size = New System.Drawing.Size(82, 13)
        Me.lblFormaPago.TabIndex = 26
        Me.lblFormaPago.Text = "Forma de Pago:"
        '
        'lblTipoIVA
        '
        Me.lblTipoIVA.AutoSize = True
        Me.lblTipoIVA.Location = New System.Drawing.Point(29, 259)
        Me.lblTipoIVA.Name = "lblTipoIVA"
        Me.lblTipoIVA.Size = New System.Drawing.Size(55, 13)
        Me.lblTipoIVA.TabIndex = 24
        Me.lblTipoIVA.Text = "T. de Imp:"
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(339, 427)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(471, 20)
        Me.flpRegistradoPor.TabIndex = 32
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'gbxVale
        '
        Me.gbxVale.Controls.Add(Me.txtCantidadComprobante)
        Me.gbxVale.Controls.Add(Me.lvVale)
        Me.gbxVale.Controls.Add(Me.txtTotalComprobante)
        Me.gbxVale.Controls.Add(Me.lklAgregarVale)
        Me.gbxVale.Controls.Add(Me.lklEliminarVale)
        Me.gbxVale.Location = New System.Drawing.Point(27, 293)
        Me.gbxVale.Name = "gbxVale"
        Me.gbxVale.Size = New System.Drawing.Size(305, 156)
        Me.gbxVale.TabIndex = 31
        Me.gbxVale.TabStop = False
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(251, 132)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 4
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'lvVale
        '
        Me.lvVale.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colIDTransaccion, Me.colFecha, Me.colMotivo, Me.colNombre, Me.colTotal, Me.colSaldo, Me.colImporte})
        Me.lvVale.FullRowSelect = True
        Me.lvVale.GridLines = True
        Me.lvVale.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvVale.HideSelection = False
        Me.lvVale.Location = New System.Drawing.Point(0, 35)
        Me.lvVale.MultiSelect = False
        Me.lvVale.Name = "lvVale"
        Me.lvVale.Size = New System.Drawing.Size(296, 92)
        Me.lvVale.TabIndex = 2
        Me.lvVale.UseCompatibleStateImageBehavior = False
        Me.lvVale.View = System.Windows.Forms.View.Details
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.Text = "IDTransaccion"
        Me.colIDTransaccion.Width = 0
        '
        'colFecha
        '
        Me.colFecha.Text = "Fecha"
        Me.colFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colFecha.Width = 53
        '
        'colMotivo
        '
        Me.colMotivo.Text = "Motivo"
        Me.colMotivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colMotivo.Width = 0
        '
        'colNombre
        '
        Me.colNombre.Text = "Nombre"
        Me.colNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colNombre.Width = 74
        '
        'colTotal
        '
        Me.colTotal.Text = "Total"
        Me.colTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colTotal.Width = 55
        '
        'colSaldo
        '
        Me.colSaldo.Text = "Saldo"
        Me.colSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colSaldo.Width = 55
        '
        'colImporte
        '
        Me.colImporte.Text = "Importe"
        Me.colImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colImporte.Width = 55
        '
        'txtTotalComprobante
        '
        Me.txtTotalComprobante.Color = System.Drawing.Color.Empty
        Me.txtTotalComprobante.Decimales = True
        Me.txtTotalComprobante.Indicaciones = Nothing
        Me.txtTotalComprobante.Location = New System.Drawing.Point(159, 133)
        Me.txtTotalComprobante.Name = "txtTotalComprobante"
        Me.txtTotalComprobante.Size = New System.Drawing.Size(86, 22)
        Me.txtTotalComprobante.SoloLectura = True
        Me.txtTotalComprobante.TabIndex = 3
        Me.txtTotalComprobante.TabStop = False
        Me.txtTotalComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalComprobante.Texto = "0"
        '
        'lklAgregarVale
        '
        Me.lklAgregarVale.AutoSize = True
        Me.lklAgregarVale.Location = New System.Drawing.Point(6, 16)
        Me.lklAgregarVale.Name = "lklAgregarVale"
        Me.lklAgregarVale.Size = New System.Drawing.Size(109, 13)
        Me.lklAgregarVale.TabIndex = 0
        Me.lklAgregarVale.TabStop = True
        Me.lklAgregarVale.Text = "Agregar comprobante"
        '
        'lklEliminarVale
        '
        Me.lklEliminarVale.AutoSize = True
        Me.lklEliminarVale.Location = New System.Drawing.Point(118, 16)
        Me.lklEliminarVale.Name = "lklEliminarVale"
        Me.lklEliminarVale.Size = New System.Drawing.Size(108, 13)
        Me.lklEliminarVale.TabIndex = 1
        Me.lklEliminarVale.TabStop = True
        Me.lklEliminarVale.Text = "Eliminar comprobante"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 498)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(815, 22)
        Me.StatusStrip1.TabIndex = 45
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(313, 471)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 34
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(100, 471)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 38
        Me.btnBusquedaAvanzada.Text = "&Búsqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(719, 471)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 43
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(637, 471)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 44
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Enabled = False
        Me.btnAnular.Location = New System.Drawing.Point(19, 471)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 37
        Me.btnAnular.Text = "E&liminar"
        Me.btnAnular.UseVisualStyleBackColor = True
        Me.btnAnular.Visible = False
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(556, 471)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 35
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Enabled = False
        Me.btnNuevo.Location = New System.Drawing.Point(475, 471)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 42
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        Me.btnNuevo.Visible = False
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'ChkFE
        '
        Me.ChkFE.AutoSize = True
        Me.ChkFE.Location = New System.Drawing.Point(32, 238)
        Me.ChkFE.Name = "ChkFE"
        Me.ChkFE.Size = New System.Drawing.Size(182, 17)
        Me.ChkFE.TabIndex = 23
        Me.ChkFE.Text = "Es Fact Electrónica / Fact Virtual"
        Me.ChkFE.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(394, 471)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 41
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnCuota
        '
        Me.btnCuota.Enabled = False
        Me.btnCuota.Location = New System.Drawing.Point(232, 471)
        Me.btnCuota.Name = "btnCuota"
        Me.btnCuota.Size = New System.Drawing.Size(75, 23)
        Me.btnCuota.TabIndex = 40
        Me.btnCuota.Text = "C&uota"
        Me.btnCuota.UseVisualStyleBackColor = True
        '
        'lblPlazo
        '
        Me.lblPlazo.AutoSize = True
        Me.lblPlazo.Location = New System.Drawing.Point(704, 44)
        Me.lblPlazo.Name = "lblPlazo"
        Me.lblPlazo.Size = New System.Drawing.Size(36, 13)
        Me.lblPlazo.TabIndex = 36
        Me.lblPlazo.Text = "Plazo:"
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(344, 201)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 35
        Me.lblDeposito.Text = "Deposito:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(381, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Vto. Timbrado:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(240, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Timbrado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(37, 71)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Grupo:"
        '
        'lblDistribucion
        '
        Me.lblDistribucion.AutoSize = True
        Me.lblDistribucion.Location = New System.Drawing.Point(3, 7)
        Me.lblDistribucion.Name = "lblDistribucion"
        Me.lblDistribucion.Size = New System.Drawing.Size(87, 13)
        Me.lblDistribucion.TabIndex = 0
        Me.lblDistribucion.Text = "Camion / Chofer:"
        '
        'chkGastoMultiple
        '
        Me.chkGastoMultiple.AutoSize = True
        Me.chkGastoMultiple.Enabled = False
        Me.chkGastoMultiple.Location = New System.Drawing.Point(74, 93)
        Me.chkGastoMultiple.Name = "chkGastoMultiple"
        Me.chkGastoMultiple.Size = New System.Drawing.Size(208, 17)
        Me.chkGastoMultiple.TabIndex = 22
        Me.chkGastoMultiple.Text = "Asignacion de Unidad y Departamento"
        Me.chkGastoMultiple.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Unidad de Negocio:"
        '
        'btnVerProducto
        '
        Me.btnVerProducto.Location = New System.Drawing.Point(204, 192)
        Me.btnVerProducto.Name = "btnVerProducto"
        Me.btnVerProducto.Size = New System.Drawing.Size(85, 23)
        Me.btnVerProducto.TabIndex = 34
        Me.btnVerProducto.Text = "&Ver Producto"
        Me.btnVerProducto.UseVisualStyleBackColor = True
        Me.btnVerProducto.Visible = False
        '
        'pnlDistribucion
        '
        Me.pnlDistribucion.Controls.Add(Me.lblDistribucion)
        Me.pnlDistribucion.Controls.Add(Me.cbxCamion)
        Me.pnlDistribucion.Controls.Add(Me.cbxChofer)
        Me.pnlDistribucion.Location = New System.Drawing.Point(568, 194)
        Me.pnlDistribucion.Name = "pnlDistribucion"
        Me.pnlDistribucion.Size = New System.Drawing.Size(211, 29)
        Me.pnlDistribucion.TabIndex = 37
        '
        'cbxCamion
        '
        Me.cbxCamion.CampoWhere = Nothing
        Me.cbxCamion.CargarUnaSolaVez = False
        Me.cbxCamion.DataDisplayMember = "Descripcion"
        Me.cbxCamion.DataFilter = Nothing
        Me.cbxCamion.DataOrderBy = "Descripcion"
        Me.cbxCamion.DataSource = "VCamion"
        Me.cbxCamion.DataValueMember = "ID"
        Me.cbxCamion.dtSeleccionado = Nothing
        Me.cbxCamion.FormABM = Nothing
        Me.cbxCamion.Indicaciones = Nothing
        Me.cbxCamion.Location = New System.Drawing.Point(90, 3)
        Me.cbxCamion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCamion.Name = "cbxCamion"
        Me.cbxCamion.SeleccionMultiple = False
        Me.cbxCamion.SeleccionObligatoria = False
        Me.cbxCamion.Size = New System.Drawing.Size(201, 21)
        Me.cbxCamion.SoloLectura = False
        Me.cbxCamion.TabIndex = 1
        Me.cbxCamion.Texto = ""
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = Nothing
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "VChofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.dtSeleccionado = Nothing
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(295, 3)
        Me.cbxChofer.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionMultiple = False
        Me.cbxChofer.SeleccionObligatoria = False
        Me.cbxChofer.Size = New System.Drawing.Size(213, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 2
        Me.cbxChofer.Texto = ""
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.cbxSeccion)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.chkGastoMultiple)
        Me.gbxCabecera.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxCabecera.Controls.Add(Me.Label4)
        Me.gbxCabecera.Controls.Add(Me.btnVerProducto)
        Me.gbxCabecera.Controls.Add(Me.chkAcuerdoAsociado)
        Me.gbxCabecera.Controls.Add(Me.pnlDistribucion)
        Me.gbxCabecera.Controls.Add(Me.cbxDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblDeposito)
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.txtVtoTimbrado)
        Me.gbxCabecera.Controls.Add(Me.Label2)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.txtPlazo)
        Me.gbxCabecera.Controls.Add(Me.txtTimbrado)
        Me.gbxCabecera.Controls.Add(Me.cbxGrupo)
        Me.gbxCabecera.Controls.Add(Me.Label1)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.cbxDepartamentoEmpresa)
        Me.gbxCabecera.Controls.Add(Me.lblDepartamento)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtVencimiento)
        Me.gbxCabecera.Controls.Add(Me.lblPlazo)
        Me.gbxCabecera.Controls.Add(Me.lblVencimiento)
        Me.gbxCabecera.Controls.Add(Me.cbxCondicion)
        Me.gbxCabecera.Controls.Add(Me.lblCondicion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblProveedor)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.lblDepartamentoSolicitante)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 2)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(799, 230)
        Me.gbxCabecera.TabIndex = 22
        Me.gbxCabecera.TabStop = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = Nothing
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = Nothing
        Me.cbxUnidadNegocio.DataValueMember = Nothing
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(20, 137)
        Me.cbxUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(244, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 24
        Me.cbxUnidadNegocio.Texto = ""
        '
        'chkAcuerdoAsociado
        '
        Me.chkAcuerdoAsociado.BackColor = System.Drawing.SystemColors.Control
        Me.chkAcuerdoAsociado.Color = System.Drawing.SystemColors.Control
        Me.chkAcuerdoAsociado.Location = New System.Drawing.Point(92, 195)
        Me.chkAcuerdoAsociado.Name = "chkAcuerdoAsociado"
        Me.chkAcuerdoAsociado.Size = New System.Drawing.Size(113, 20)
        Me.chkAcuerdoAsociado.SoloLectura = False
        Me.chkAcuerdoAsociado.TabIndex = 33
        Me.chkAcuerdoAsociado.Texto = "Acuerdo Asociado"
        Me.chkAcuerdoAsociado.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "Deposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = Nothing
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(402, 197)
        Me.cbxDeposito.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(161, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 36
        Me.cbxDeposito.Texto = ""
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(295, 10)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(502, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 4
        '
        'txtVtoTimbrado
        '
        Me.txtVtoTimbrado.AñoFecha = 0
        Me.txtVtoTimbrado.Color = System.Drawing.Color.Empty
        Me.txtVtoTimbrado.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtVtoTimbrado.Location = New System.Drawing.Point(458, 67)
        Me.txtVtoTimbrado.MesFecha = 0
        Me.txtVtoTimbrado.Name = "txtVtoTimbrado"
        Me.txtVtoTimbrado.PermitirNulo = False
        Me.txtVtoTimbrado.Size = New System.Drawing.Size(86, 20)
        Me.txtVtoTimbrado.SoloLectura = False
        Me.txtVtoTimbrado.TabIndex = 21
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Empty
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(295, 67)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(80, 21)
        Me.txtTimbrado.SoloLectura = False
        Me.txtTimbrado.TabIndex = 19
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = Nothing
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = Nothing
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = Nothing
        Me.cbxGrupo.DataSource = Nothing
        Me.cbxGrupo.DataValueMember = Nothing
        Me.cbxGrupo.dtSeleccionado = Nothing
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(74, 67)
        Me.cbxGrupo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionMultiple = False
        Me.cbxGrupo.SeleccionObligatoria = True
        Me.cbxGrupo.Size = New System.Drawing.Size(161, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 17
        Me.cbxGrupo.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(575, 170)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 31
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxDepartamentoEmpresa
        '
        Me.cbxDepartamentoEmpresa.CampoWhere = Nothing
        Me.cbxDepartamentoEmpresa.CargarUnaSolaVez = False
        Me.cbxDepartamentoEmpresa.DataDisplayMember = Nothing
        Me.cbxDepartamentoEmpresa.DataFilter = Nothing
        Me.cbxDepartamentoEmpresa.DataOrderBy = Nothing
        Me.cbxDepartamentoEmpresa.DataSource = Nothing
        Me.cbxDepartamentoEmpresa.DataValueMember = Nothing
        Me.cbxDepartamentoEmpresa.dtSeleccionado = Nothing
        Me.cbxDepartamentoEmpresa.FormABM = Nothing
        Me.cbxDepartamentoEmpresa.Indicaciones = Nothing
        Me.cbxDepartamentoEmpresa.Location = New System.Drawing.Point(280, 137)
        Me.cbxDepartamentoEmpresa.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepartamentoEmpresa.Name = "cbxDepartamentoEmpresa"
        Me.cbxDepartamentoEmpresa.SeleccionMultiple = False
        Me.cbxDepartamentoEmpresa.SeleccionObligatoria = False
        Me.cbxDepartamentoEmpresa.Size = New System.Drawing.Size(281, 21)
        Me.cbxDepartamentoEmpresa.SoloLectura = False
        Me.cbxDepartamentoEmpresa.TabIndex = 28
        Me.cbxDepartamentoEmpresa.Texto = ""
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(277, 113)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(145, 13)
        Me.lblDepartamento.TabIndex = 27
        Me.lblDepartamento.Text = "Departamento que Consume:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(56, 166)
        Me.txtObservacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(507, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 30
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(74, 13)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(63, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtVencimiento
        '
        Me.txtVencimiento.AñoFecha = 0
        Me.txtVencimiento.Color = System.Drawing.Color.Empty
        Me.txtVencimiento.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtVencimiento.Location = New System.Drawing.Point(605, 40)
        Me.txtVencimiento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtVencimiento.MesFecha = 0
        Me.txtVencimiento.Name = "txtVencimiento"
        Me.txtVencimiento.PermitirNulo = False
        Me.txtVencimiento.Size = New System.Drawing.Size(99, 20)
        Me.txtVencimiento.SoloLectura = False
        Me.txtVencimiento.TabIndex = 13
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(567, 44)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(38, 13)
        Me.lblVencimiento.TabIndex = 12
        Me.lblVencimiento.Text = "Venc.:"
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(435, 40)
        Me.cbxCondicion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = True
        Me.cbxCondicion.Size = New System.Drawing.Size(128, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 11
        Me.cbxCondicion.Texto = ""
        '
        'lblCondicion
        '
        Me.lblCondicion.AutoSize = True
        Me.lblCondicion.Location = New System.Drawing.Point(378, 44)
        Me.lblCondicion.Name = "lblCondicion"
        Me.lblCondicion.Size = New System.Drawing.Size(57, 13)
        Me.lblCondicion.TabIndex = 10
        Me.lblCondicion.Text = "Condición:"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 20, 10, 18, 15, 593)
        Me.txtFecha.Location = New System.Drawing.Point(294, 40)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(81, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 9
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(254, 44)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(74, 40)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(137, 40)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(97, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(235, 17)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 3
        Me.lblProveedor.Text = "Proveedor:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(137, 13)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(97, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(17, 170)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(32, 13)
        Me.lblObservacion.TabIndex = 29
        Me.lblObservacion.Text = "Obs.:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(27, 44)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(49, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Compro.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(17, 17)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.dt = Nothing
        Me.txtCotizacion.FiltroFecha = Nothing
        Me.txtCotizacion.Location = New System.Drawing.Point(620, 160)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Registro = Nothing
        Me.txtCotizacion.Saltar = False
        Me.txtCotizacion.Seleccionado = True
        Me.txtCotizacion.Size = New System.Drawing.Size(148, 32)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 32
        '
        'lblDepartamentoSolicitante
        '
        Me.lblDepartamentoSolicitante.AutoSize = True
        Me.lblDepartamentoSolicitante.Location = New System.Drawing.Point(291, 113)
        Me.lblDepartamentoSolicitante.Name = "lblDepartamentoSolicitante"
        Me.lblDepartamentoSolicitante.Size = New System.Drawing.Size(129, 13)
        Me.lblDepartamentoSolicitante.TabIndex = 38
        Me.lblDepartamentoSolicitante.Text = "Departamento Solicitante:"
        '
        'txtPlazo
        '
        Me.txtPlazo.Color = System.Drawing.Color.Empty
        Me.txtPlazo.Decimales = False
        Me.txtPlazo.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPlazo.Location = New System.Drawing.Point(740, 39)
        Me.txtPlazo.Name = "txtPlazo"
        Me.txtPlazo.Size = New System.Drawing.Size(53, 21)
        Me.txtPlazo.SoloLectura = True
        Me.txtPlazo.TabIndex = 39
        Me.txtPlazo.TabStop = False
        Me.txtPlazo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazo.Texto = "0"
        '
        'chkCuota
        '
        Me.chkCuota.BackColor = System.Drawing.SystemColors.Control
        Me.chkCuota.Color = System.Drawing.SystemColors.Control
        Me.chkCuota.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCuota.Location = New System.Drawing.Point(25, 448)
        Me.chkCuota.Name = "chkCuota"
        Me.chkCuota.Size = New System.Drawing.Size(211, 20)
        Me.chkCuota.SoloLectura = False
        Me.chkCuota.TabIndex = 33
        Me.chkCuota.Texto = "Comprobante a pagar en cuotas"
        Me.chkCuota.Valor = False
        '
        'chkEfectivo
        '
        Me.chkEfectivo.BackColor = System.Drawing.SystemColors.Control
        Me.chkEfectivo.Color = System.Drawing.SystemColors.Control
        Me.chkEfectivo.Location = New System.Drawing.Point(256, 277)
        Me.chkEfectivo.Name = "chkEfectivo"
        Me.chkEfectivo.Size = New System.Drawing.Size(81, 20)
        Me.chkEfectivo.SoloLectura = False
        Me.chkEfectivo.TabIndex = 28
        Me.chkEfectivo.Texto = "Efectivo"
        Me.chkEfectivo.Valor = False
        '
        'chkCheque
        '
        Me.chkCheque.BackColor = System.Drawing.SystemColors.Control
        Me.chkCheque.Color = System.Drawing.SystemColors.Control
        Me.chkCheque.Location = New System.Drawing.Point(256, 255)
        Me.chkCheque.Name = "chkCheque"
        Me.chkCheque.Size = New System.Drawing.Size(81, 20)
        Me.chkCheque.SoloLectura = False
        Me.chkCheque.TabIndex = 27
        Me.chkCheque.Texto = "Cheque"
        Me.chkCheque.Valor = False
        '
        'OcxImpuesto1
        '
        Me.OcxImpuesto1.Decimales = False
        Me.OcxImpuesto1.dtImpuesto = Nothing
        Me.OcxImpuesto1.IDMoneda = 0
        Me.OcxImpuesto1.Location = New System.Drawing.Point(339, 252)
        Me.OcxImpuesto1.Margin = New System.Windows.Forms.Padding(4)
        Me.OcxImpuesto1.Name = "OcxImpuesto1"
        Me.OcxImpuesto1.Size = New System.Drawing.Size(471, 171)
        Me.OcxImpuesto1.TabIndex = 30
        Me.OcxImpuesto1.TotalRetencionIVA = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'chkIncluirLibro
        '
        Me.chkIncluirLibro.BackColor = System.Drawing.SystemColors.Control
        Me.chkIncluirLibro.Color = System.Drawing.SystemColors.Control
        Me.chkIncluirLibro.Location = New System.Drawing.Point(84, 277)
        Me.chkIncluirLibro.Name = "chkIncluirLibro"
        Me.chkIncluirLibro.Size = New System.Drawing.Size(90, 20)
        Me.chkIncluirLibro.SoloLectura = False
        Me.chkIncluirLibro.TabIndex = 29
        Me.chkIncluirLibro.Texto = "Incluir en Libro"
        Me.chkIncluirLibro.Valor = False
        '
        'cbxTipoIVA
        '
        Me.cbxTipoIVA.CampoWhere = Nothing
        Me.cbxTipoIVA.CargarUnaSolaVez = False
        Me.cbxTipoIVA.DataDisplayMember = Nothing
        Me.cbxTipoIVA.DataFilter = Nothing
        Me.cbxTipoIVA.DataOrderBy = Nothing
        Me.cbxTipoIVA.DataSource = Nothing
        Me.cbxTipoIVA.DataValueMember = Nothing
        Me.cbxTipoIVA.dtSeleccionado = Nothing
        Me.cbxTipoIVA.FormABM = Nothing
        Me.cbxTipoIVA.Indicaciones = Nothing
        Me.cbxTipoIVA.Location = New System.Drawing.Point(84, 255)
        Me.cbxTipoIVA.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoIVA.Name = "cbxTipoIVA"
        Me.cbxTipoIVA.SeleccionMultiple = False
        Me.cbxTipoIVA.SeleccionObligatoria = True
        Me.cbxTipoIVA.Size = New System.Drawing.Size(90, 21)
        Me.cbxTipoIVA.SoloLectura = False
        Me.cbxTipoIVA.TabIndex = 25
        Me.cbxTipoIVA.Texto = ""
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = Nothing
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = Nothing
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = Nothing
        Me.cbxSeccion.DataSource = Nothing
        Me.cbxSeccion.DataValueMember = Nothing
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(568, 137)
        Me.cbxSeccion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(225, 21)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 41
        Me.cbxSeccion.Texto = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(565, 113)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 40
        Me.Label5.Text = "Seccion:"
        '
        'frmGastoContabilidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(815, 520)
        Me.Controls.Add(Me.lblFormaPago)
        Me.Controls.Add(Me.lblTipoIVA)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.gbxVale)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.ChkFE)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.chkCuota)
        Me.Controls.Add(Me.chkEfectivo)
        Me.Controls.Add(Me.chkCheque)
        Me.Controls.Add(Me.OcxImpuesto1)
        Me.Controls.Add(Me.chkIncluirLibro)
        Me.Controls.Add(Me.btnCuota)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.cbxTipoIVA)
        Me.Name = "frmGastoContabilidad"
        Me.Tag = "frmGastoContabilidad"
        Me.Text = "frmGastoContabilidad"
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.gbxVale.ResumeLayout(False)
        Me.gbxVale.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDistribucion.ResumeLayout(False)
        Me.pnlDistribucion.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblFormaPago As Label
    Friend WithEvents lblTipoIVA As Label
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents gbxVale As GroupBox
    Friend WithEvents txtCantidadComprobante As ocxTXTNumeric
    Friend WithEvents lvVale As ListView
    Friend WithEvents colIDTransaccion As ColumnHeader
    Friend WithEvents colFecha As ColumnHeader
    Friend WithEvents colMotivo As ColumnHeader
    Friend WithEvents colNombre As ColumnHeader
    Friend WithEvents colTotal As ColumnHeader
    Friend WithEvents colSaldo As ColumnHeader
    Friend WithEvents colImporte As ColumnHeader
    Friend WithEvents txtTotalComprobante As ocxTXTNumeric
    Friend WithEvents lklAgregarVale As LinkLabel
    Friend WithEvents lklEliminarVale As LinkLabel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents btnAsiento As Button
    Friend WithEvents btnBusquedaAvanzada As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnAnular As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents txtPlazo As ocxTXTNumeric
    Friend WithEvents ChkFE As CheckBox
    Friend WithEvents btnModificar As Button
    Friend WithEvents chkCuota As ocxCHK
    Friend WithEvents chkEfectivo As ocxCHK
    Friend WithEvents chkCheque As ocxCHK
    Friend WithEvents OcxImpuesto1 As ocxImpuesto
    Friend WithEvents chkIncluirLibro As ocxCHK
    Friend WithEvents btnCuota As Button
    Friend WithEvents lblPlazo As Label
    Friend WithEvents gbxCabecera As GroupBox
    Friend WithEvents chkGastoMultiple As CheckBox
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents Label4 As Label
    Friend WithEvents btnVerProducto As Button
    Friend WithEvents chkAcuerdoAsociado As ocxCHK
    Friend WithEvents pnlDistribucion As Panel
    Friend WithEvents lblDistribucion As Label
    Friend WithEvents cbxCamion As ocxCBX
    Friend WithEvents cbxChofer As ocxCBX
    Friend WithEvents cbxDeposito As ocxCBX
    Friend WithEvents lblDeposito As Label
    Friend WithEvents txtProveedor As ocxTXTProveedor
    Friend WithEvents txtVtoTimbrado As ocxTXTDate
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtTimbrado As ocxTXTString
    Friend WithEvents cbxGrupo As ocxCBX
    Friend WithEvents Label1 As Label
    Friend WithEvents lblMoneda As Label
    Friend WithEvents cbxDepartamentoEmpresa As ocxCBX
    Friend WithEvents lblDepartamento As Label
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents txtVencimiento As ocxTXTDate
    Friend WithEvents lblVencimiento As Label
    Friend WithEvents cbxCondicion As ocxCBX
    Friend WithEvents lblCondicion As Label
    Friend WithEvents txtFecha As ocxTXTDate
    Friend WithEvents lblFecha As Label
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents txtComprobante As ocxTXTString
    Friend WithEvents lblProveedor As Label
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents lblObservacion As Label
    Friend WithEvents lblComprobante As Label
    Friend WithEvents lblOperacion As Label
    Friend WithEvents txtCotizacion As ocxCotizacion
    Friend WithEvents lblDepartamentoSolicitante As Label
    Friend WithEvents cbxTipoIVA As ocxCBX
    Friend WithEvents cbxSeccion As ocxCBX
    Friend WithEvents Label5 As Label
End Class

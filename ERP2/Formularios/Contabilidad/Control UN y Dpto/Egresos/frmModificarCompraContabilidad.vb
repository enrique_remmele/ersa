﻿Public Class frmModificarCompraContabilidad

    Dim Csistema As New CSistema
    Dim CData As New CData
    Public Property IDOperacion As Integer
    Public Property IDTransaccion As Integer
    Public Property Departamento As String
    Public Property IDSucursal As Integer

    Sub Inicializar()
        Cargar()
    End Sub

    Sub Cargar()

        If IDSucursal = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            Csistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & IDSucursal)
            Csistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        End If

        cbxDepartamento.cbx.Text = Departamento

    End Sub

    Sub Guardar()
        Try
            Dim param(-1) As SqlClient.SqlParameter

            Csistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)

            Csistema.SetSQLParameter(param, "@Operacion", "UPD2", ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDterminal", vgIDTerminal, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDTransaccionSalida", 0, ParameterDirection.Output)

            If chkfe.Checked = True Then
                Csistema.SetSQLParameter(param, "@EsFe", chkfe.Checked, ParameterDirection.Input)
            End If

            Csistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)

            'Informacion de Salida
            Csistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            Csistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Aplicar Modificaciones
            If Csistema.ExecuteStoreProcedure(param, "SpCompra", False, False, MensajeRetorno) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            Me.Close()

        Catch ex As Exception

        End Try
    End Sub


    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub frmModificarCompraContabilidad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

End Class
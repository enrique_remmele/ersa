﻿Public Class frmModificarGastoContabilidad

    Dim Csistema As New CSistema
    Dim CData As New CData
    Private dt As Object
    Public Property IDOperacion As Integer
    Public Property IDTransaccion As Integer
    Public Property EsFE As Boolean
    Public Property UnidadNegocio As String
    Public Property Departamento As String
    Public Property GastosVarios As Boolean
    Public Property IDSucursal As Integer
    Public Property Seccion As String
    Dim vSeccion As Boolean = False

    Sub Inicializar()
        Cargar()
    End Sub

    Sub Cargar()

        chkGastosVarios.Checked = GastosVarios
        chkGastosVarios.Enabled = False
        If IDSucursal = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            Csistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & IDSucursal)
            Csistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        End If
        Csistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio", "Estado = 1"), "ID", "Descripcion")
        If GastosVarios = True Then
            cbxUnidadNegocio.cbx.Enabled = False
            cbxUnidadNegocio.cbx.Text = ""
        Else
            cbxUnidadNegocio.cbx.Enabled = True
            cbxUnidadNegocio.cbx.Text = UnidadNegocio
        End If

        cbxDepartamento.cbx.Text = Departamento
        cbxSeccion.cbx.Text = Seccion
        'Csistema.SqlToComboBox(cbxDepartamento.cbx, CData.GetTable("vDepartamentoEmpresa"), "ID", "Descripcion")

        ''ChkFE.Checked = EsFE
    End Sub

    Sub Guardar()

        Try
            Dim param(-1) As SqlClient.SqlParameter

            Csistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@Operacion", "UPD3", ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDterminal", vgIDTerminal, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            Csistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            'Se agrega cuando se carga para diferenciar Factura Electrónica
            If ChkFE.Checked = True Then
                Csistema.SetSQLParameter(param, "@EsFE", ChkFE.Checked, ParameterDirection.Input)
            End If

            If chkGastosVarios.Checked = False Then
                Csistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.cbx.SelectedValue, ParameterDirection.Input)
                Csistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)
                If vSeccion = True Then
                    Csistema.SetSQLParameter(param, "@IDSeccion", cbxSeccion.cbx.SelectedValue, ParameterDirection.Input)
                End If
            Else
                Csistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", cbxDepartamento.cbx.SelectedValue, ParameterDirection.Input)
                If vSeccion = True Then
                    Csistema.SetSQLParameter(param, "@IDSeccion", cbxSeccion.cbx.SelectedValue, ParameterDirection.Input)
                End If
            End If
            Csistema.SetSQLParameter(param, "@IDTransaccionSalida", 0, ParameterDirection.Output)

            'Informacion de Salida
            Csistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            Csistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Aplicar Modificaciones
            If Csistema.ExecuteStoreProcedure(param, "SpGasto", False, False, MensajeRetorno) = False Then
                '12-07-2021 comentado por error en tipos de datos, de igual forma ejecuta el proceso
                'MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            Me.Close()

        Catch ex As Exception

        End Try
    End Sub


    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub frmModificarCompra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamento.PropertyChanged
        vSeccion = Csistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamento.cbx.SelectedValue)
        If vSeccion = True Then
            Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamento.cbx.SelectedValue)
            Csistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
            cbxSeccion.cbx.Text = ""
            cbxSeccion.SoloLectura = False
        Else
            cbxSeccion.SoloLectura = True
            cbxSeccion.cbx.Text = ""
        End If
    End Sub
End Class
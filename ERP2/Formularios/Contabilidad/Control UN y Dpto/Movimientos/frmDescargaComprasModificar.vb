﻿Public Class frmDescargaComprasModificar
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CAsiento As New CAsientoMovimiento
    Dim CData As New CData
    Dim dtComprobantes As New DataTable
    Dim VAnulado As Boolean = False
    Dim Reprocesar As Boolean = False
    'Dim CReporte As New CReporte
    Dim Seccion As Boolean = False

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim CancelarTransferencia As Boolean = False
    Dim IDTransaccionTransferenciaEnvio As Integer
    Dim Enviar As Boolean = False
    Dim Recibir As Boolean = False
    Dim IDTransaccionEnvio As Integer = 0
    Dim Precargado As Boolean = False

    'SC: 01-09-2021 - Variables
    Dim TotalCreditoAsiento As Decimal = 0
    Dim TotalDebitoAsiento As Decimal = 0
    Dim SaldoAsiento As Decimal = 0
    Dim DesbalanceoAsiento As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Conectar()

        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "MOVIMIENTO DESCARGA DE COMPRA", "MO")
        IDTransaccion = 0
        vNuevo = False
        lklSeleccionarProductos.Enabled = False

        'SC: 01-09-2021
        DesbalanceoAsiento = False

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.IDOperacion = IDOperacion
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxTipoOperacion)
        CSistema.CargaControl(vControles, cbxMotivo)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxDepositoSalida)
        CSistema.CargaControl(vControles, cbxDepositoEntrada)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        CSistema.CargaControl(vControles, cbxUnidad)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, cbxUnidadNegocio)
        CSistema.CargaControl(vControles, cbxDepartamentoEmpresa)
        CSistema.CargaControl(vControles, cbxSeccion)

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleMovimiento").Clone

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR DATOS DE OPERACION
        dtOperaciones = CSistema.ExecuteToDataTable("Select ID, Descripcion, Activo, Entrada, Salida From TipoOperacion Where IDOperacion=" & IDOperacion)

        'CARGAR UNIDAD
        cbxUnidad.Items.Add("UNIDAD")
        cbxUnidad.Items.Add("CAJA")
        cbxUnidad.Items.Add("KILOGRAMOS")
        cbxUnidad.Items.Add("BOLSAS")
        cbxUnidad.Items.Add("LITROS")
        cbxUnidad.Items.Add("CM")
        cbxUnidad.Items.Add("METRO")
        cbxUnidad.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR CONTROLES
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From Movimiento where DescargaCompra = 'True'),1)"), Integer)

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Tipo de Operaciones
        CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion=" & IDOperacion)

        'Tipo de Comprobante
        dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dtComprobantes, "ID", "Codigo")

        'Depositos
        dtDepositos = CSistema.ExecuteToDataTable(" Select * From VDeposito Where Activo='True' Order By 2").Copy

        'Deposito de Origen
        cbxDepositoEntrada.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoEntrada.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoEntrada.cbx.DataSource = dtDepositos.Copy

        'Deposito de Destino
        cbxDepositoSalida.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoSalida.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoSalida.cbx.DataSource = dtDepositos.Copy
        'cbxDepositoSalida.cbx.DataSource = CData.FiltrarDataTable(dtDepositos.Copy, " IDSucursal = " & vgIDSucursal)

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento ")

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo Operacion
        cbxSucursal.SelectedValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "0"))

        'Tipo Operacion
        cbxTipoOperacion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO OPERACION", "")

        'Tipo Motivo
        cbxMotivo.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MOTIVO", "")

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Deposito de Origen
        cbxDepositoSalida.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO ORIGEN", "")

        'Deposito de Destino
        cbxDepositoEntrada.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO DESTINO", "")

        'Unidad
        cbxUnidad.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", "UNIDAD")

        'CONFIGURACIONES
        If CBool(vgConfiguraciones("MovimientoBloquearFecha").ToString) = True Then
            If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) = 0 Then
                txtFecha.Enabled = False
            Else
                txtFecha.Enabled = True
            End If

            txtFecha.SetValue(VGFechaHoraSistema)
        Else
            txtFecha.Enabled = True
        End If

        'If cbxTipoOperacion.Texto = "ENTRADA" Then
        '    cbxDepositoEntrada.Texto = True
        'End If

        'If cbxTipoOperacion.Texto = "SALIDA" Then
        '    cbxDepositoSalida.Texto = True
        'End If

        'Unidad de Negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio", "Estado = 1"), "ID", "Descripcion")
        cbxUnidadNegocio.cbx.Text = ""

    End Sub


    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VMovimiento Where DescargaCompra = 'True' and IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Verificar que no se transfiera al mismo tipo de deposito y con Transferir = False
        'If cbxDepositoEntrada.Enabled = True And cbxDepositoSalida.Enabled = True Then
        '    If CType(CSistema.ExecuteScalar("Select IsNull((Select IDTipoDeposito from Deposito D join TipoDeposito TD on D.IDTipoDeposito = TD.ID and D.ID = " & cbxDepositoEntrada.GetValue & "),0)"), Integer) = CType(CSistema.ExecuteScalar("Select IsNull((Select IDTipoDeposito from Deposito D join TipoDeposito TD on D.IDTipoDeposito = TD.ID and D.ID = " & cbxDepositoSalida.GetValue & "),0)"), Integer) Then
        '        If CType(CSistema.ExecuteScalar("Select IsNull((Select Transferir from Deposito D join TipoDeposito TD on D.IDTipoDeposito = TD.ID and D.ID = " & cbxDepositoEntrada.GetValue & "),'False')"), Boolean) = False Then
        '            MessageBox.Show("Atencion! No se puede transferir entre estos depositos", "Transferencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
        '            Exit Function
        '        End If
        '    End If
        'End If

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Obligar carga de observacion
        If txtObservacion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese observacion del consumo!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Obligar carga de Motivo
        If cbxMotivo.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese motivo del Movimiento!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        'Controlar que no se guarden consumos sin asignar UN/DEP/SEC
        If cbxTipoOperacion.cbx.SelectedValue = 8 Then
            If cbxUnidadNegocio.cbx.Text = "" Or cbxDepartamentoEmpresa.cbx.Text = "" Then
                If Seccion = True Then
                    Dim mensaje As String = "Selecione la Unidad de negocio, el Departamento o la Seccion del consumo!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                Else
                    Dim mensaje As String = "Seleccione la Unidad de Negocio o El Departamento del consumo!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If
            End If
        End If

        Return True

    End Function

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, New Button, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro


        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Ref").DisplayIndex = 1
        dgw.Columns("Descripcion").DisplayIndex = 2
        dgw.Columns("Cantidad").DisplayIndex = 3
        dgw.Columns("PrecioUnitario").DisplayIndex = 4
        dgw.Columns("Total").DisplayIndex = 5

        dgw.Columns("Ref").Visible = True
        dgw.Columns("Descripcion").Visible = True
        dgw.Columns("Cantidad").Visible = True
        dgw.Columns("PrecioUnitario").Visible = True
        dgw.Columns("Total").Visible = True

        dgw.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"

        dgw.Columns("Ref").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("Cantidad").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("PrecioUnitario").HeaderText = "Costo"

        Total = CSistema.dtSumColumn(dtDetalle, "Total")


        'Bloqueamos la cabecera si corresponde
        If dtDetalle.Rows.Count > 0 And vNuevo = True Then
            cbxTipoOperacion.cbx.Enabled = False
            cbxMotivo.cbx.Enabled = False
            cbxDepositoEntrada.cbx.Enabled = False
            cbxDepositoSalida.cbx.Enabled = False
        Else
            cbxTipoOperacion.cbx.Enabled = True
            cbxMotivo.cbx.Enabled = True
            cbxTipoOperacion.cbx.SelectedValue = cbxTipoOperacion.GetValue
            cbxDepositoEntrada.cbx.Enabled = True
            cbxDepositoSalida.cbx.Enabled = True
        End If

        If dtDetalle.Rows.Count > 0 Then
            lklSeleccionarProductos.Enabled = False
        Else
            If vNuevo And Recibir Then
                lklSeleccionarProductos.Enabled = True
            End If
        End If

        'Calculamos el Total
        txtTotal.txt.Text = Total


    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        lblAnulado.Visible = False

        Dim frm As New frmConsultaMovimiento
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.TipoMovimiento = "DescargaCompra"

        FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False
        lklSeleccionarProductos.Enabled = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()


        'Tipo de Comprobante
        dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dtComprobantes, "ID", "Codigo")


        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VMovimiento Where DescargaCompra = 'True' and IDSucursal=" & cbxSucursal.GetValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VMovimiento Where DescargaCompra = 'True' and IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select IDProducto, ID, Ref, 'Descripcion'=Producto, IDDepositoEntrada, IDDepositoSalida, Cantidad, Observacion, PrecioUnitario, IDImpuesto, Impuesto, Total, TotalImpuesto, TotalDiscriminado, Caja, CantidadCaja, CodigoBarra From VDetalleMovimiento Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        IDTransaccionEnvio = oRow("IDTransaccionEnvio")
        If IDTransaccionEnvio > 0 Then
            Enviar = True
            Recibir = False
        Else
            Enviar = False
            Recibir = True
        End If
        CancelarTransferencia = False
        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxTipoOperacion.cbx.Text = oRow("Operacion").ToString

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & oRow("IDTipoOperacion").ToString)

        cbxMotivo.cbx.Text = oRow("Motivo").ToString
        cbxTipoComprobante.Texto = oRow("Cod.").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxDepositoEntrada.cbx.Text = oRow("DepositoEntrada").ToString
        cbxDepositoSalida.cbx.Text = oRow("DepositoSalida").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtTotal.txt.Text = oRow("Total").ToString

        If oRow("DepositoEntrada").ToString = "---" Then
            cbxDepositoEntrada.txt.Text = ""
        End If
        If oRow("DepositoSalida").ToString = "---" Then
            cbxDepositoSalida.txt.Text = ""
        End If

        cbxUnidadNegocio.cbx.Text = oRow("UnidadNegocio").ToString
        cbxDepartamentoEmpresa.cbx.Text = oRow("Departamento").ToString
        cbxSeccion.cbx.Text = oRow("Seccion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        'lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString
        lblUsuarioRegistro.Text = oRow("Usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            VAnulado = True
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else

            flpAnuladoPor.Visible = False
            VAnulado = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "MOVIMIENTO " & cbxTipoOperacion.cbx.Text & " - " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VMovimiento Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & " Order By IDTransaccion Desc), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        Else
            MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)


        End If


    End Sub

    Sub HabilitarBotones()
        cbxUnidadNegocio.cbx.Enabled = True
        cbxDepartamentoEmpresa.cbx.Enabled = True
        cbxSeccion.cbx.Enabled = True

    End Sub

    Private Sub frmDescargaComprasModificar_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnAsiento_Click(sender As Object, e As EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(sender As Object, e As EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        HabilitarBotones()
    End Sub
End Class
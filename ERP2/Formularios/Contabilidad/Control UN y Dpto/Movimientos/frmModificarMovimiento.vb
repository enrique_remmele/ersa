﻿Public Class frmModificarMovimiento
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CAsiento As New CAsientoMovimiento
    Dim CData As New CData
    Dim dtComprobantes As New DataTable
    Dim VAnulado As Boolean = False
    Dim Reprocesar As Boolean = False
    'Dim CReporte As New CReporte
    Dim Seccion As Boolean = False

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Propiedades
        'IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "MOVIMIENTO DESCARGA DE COMPRA", "MO")
        IDTransaccion = 0
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.IDOperacion = IDOperacion
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, New Button, New Button, New Button, btnAsiento, vControles, btnModificar)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'CSistema.CargaControl(vControles, cbxTipoMovimiento)

        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxMotivo)
        CSistema.CargaControl(vControles, cbxDepositoSalida)
        CSistema.CargaControl(vControles, cbxDepositoEntrada)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtObservacion)

        CSistema.CargaControl(vControles, cbxUnidadNegocio)
        CSistema.CargaControl(vControles, cbxDepartamento)
        CSistema.CargaControl(vControles, cbxSeccion)

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleMovimiento").Clone

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'Depositos
        dtDepositos = CSistema.ExecuteToDataTable(" Select * From VDeposito Where Activo='True' Order By 2").Copy

        'Deposito de Origen
        cbxDepositoEntrada.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoEntrada.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoEntrada.cbx.DataSource = dtDepositos.Copy

        'Deposito de Destino
        cbxDepositoSalida.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoSalida.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoSalida.cbx.DataSource = dtDepositos.Copy

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Unidad de Negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio", "Estado = 1"), "ID", "Descripcion")
        cbxUnidadNegocio.cbx.Text = ""

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Tipo de Comprobante
        dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dtComprobantes, "ID", "Codigo")

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VMovimiento Where Operacion = 'SALIDA' And TipoMovimiento = '" & cbxTipoMovimiento.Text & "' and IDSucursal=" & cbxSucursal.GetValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VMovimiento Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select IDProducto, ID, Ref, 'Descripcion'=Producto, IDDepositoEntrada, IDDepositoSalida, Cantidad, Observacion, PrecioUnitario, IDImpuesto, Impuesto, Total, TotalImpuesto, TotalDiscriminado, Caja, CantidadCaja, CodigoBarra From VDetalleMovimiento Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxUnidadNegocio.cbx.Text = oRow("UnidadNegocio").ToString
        cbxDepartamento.cbx.Text = oRow("Departamento").ToString
        cbxSeccion.cbx.Text = oRow("Seccion").ToString

        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        'cbxTipoOperacion.cbx.Text = oRow("Operacion").ToString

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & oRow("IDTipoOperacion").ToString)

        cbxMotivo.cbx.Text = oRow("Motivo").ToString
        cbxTipoComprobante.Texto = oRow("Cod.").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxDepositoEntrada.cbx.Text = oRow("DepositoEntrada").ToString
        cbxDepositoSalida.cbx.Text = oRow("DepositoSalida").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtTotal.txt.Text = oRow("Total").ToString

        If oRow("DepositoEntrada").ToString = "---" Then
            cbxDepositoEntrada.txt.Text = ""
        End If
        If oRow("DepositoSalida").ToString = "---" Then
            cbxDepositoSalida.txt.Text = ""
        End If

        cbxUnidadNegocio.cbx.Text = oRow("UnidadNegocio").ToString
        cbxDepartamento.cbx.Text = oRow("Departamento").ToString
        cbxSeccion.cbx.Text = oRow("Seccion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        'lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString
        lblUsuarioRegistro.Text = oRow("Usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            VAnulado = True
            flpAnuladoPor.Visible = True
            'btnAnular.Text = "Reprocesar"
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString

            'If Enviar = False Then
            '    btnAnular.Visible = True
            'Else
            '    btnAnular.Visible = False
            'End If
        Else
            'btnAnular.Visible = True
            flpAnuladoPor.Visible = False
            'btnAnular.Text = "Anular"
            'VAnulado = False
        End If

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From VMovimiento Where Num > " & ID & " and DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), " & ID & ") From VMovimiento Where Num < " & ID & " and DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), " & ID & ") From VMovimiento Where DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From VMovimiento Where DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro


        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Ref").DisplayIndex = 1
        dgw.Columns("Descripcion").DisplayIndex = 2
        dgw.Columns("Cantidad").DisplayIndex = 3
        dgw.Columns("PrecioUnitario").DisplayIndex = 4
        dgw.Columns("Total").DisplayIndex = 5

        dgw.Columns("Ref").Visible = True
        dgw.Columns("Descripcion").Visible = True
        dgw.Columns("Cantidad").Visible = True
        dgw.Columns("PrecioUnitario").Visible = True
        dgw.Columns("Total").Visible = True

        dgw.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"

        dgw.Columns("Ref").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("Cantidad").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("PrecioUnitario").HeaderText = "Costo"

        Total = CSistema.dtSumColumn(dtDetalle, "Total")


        'Bloqueamos la cabecera si corresponde
        If dtDetalle.Rows.Count > 0 And vNuevo = True Then
            'cbxTipoOperacion.cbx.Enabled = False
            cbxMotivo.cbx.Enabled = False
            cbxDepositoEntrada.cbx.Enabled = False
            cbxDepositoSalida.cbx.Enabled = False
        Else
            'cbxTipoOperacion.cbx.Enabled = True
            cbxMotivo.cbx.Enabled = True
            'cbxTipoOperacion.cbx.SelectedValue = cbxTipoOperacion.GetValue
            cbxDepositoEntrada.cbx.Enabled = True
            cbxDepositoSalida.cbx.Enabled = True
        End If

        'Calculamos el Total
        txtTotal.txt.Text = Total


    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "MOVIMIENTO " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VMovimiento Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & " Order By IDTransaccion Desc), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else
            MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End If


    End Sub

    Sub Modificar()

        vNuevo = True
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)

        'Habilitar
        cbxUnidadNegocio.SoloLectura = False
        cbxDepartamento.SoloLectura = False
        cbxSeccion.SoloLectura = False

        If cbxSucursal.GetValue = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
            CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        End If

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        'txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VMovimiento Where DescargaCompra = 'True' and IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        'Dim IDTransaccion As Integer

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)

        End If

        'FA - 26/01/2022 PLAN DE CUENTA
        CSistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxDepartamento.GetValue, ParameterDirection.Input)

        If Seccion = True Then
            CSistema.SetSQLParameter(param, "@IDSeccion", cbxSeccion.GetValue, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", "UPD2", ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input) 'JGR 20140816
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpMovimiento", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro

            Exit Sub

        End If

        ' Ejecutar el SpAsientoMovimiento (si asiento no bloqueado)
        If CAsiento.Bloquear = False Then
            CSistema.ExecuteNonQuery(" Execute SpAsientoMovimiento " & IDTransaccion)
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion()

    End Sub

    Private Sub frmModificarMovimiento_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub txtID_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoComprobante.PropertyChanged
        If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
            lblNombreDocumento.Text = "---"
        Else
            lblNombreDocumento.Text = dtComprobantes.Select("ID = " & cbxTipoComprobante.cbx.SelectedValue)(0)("Descripcion").ToString
        End If

        If cbxTipoComprobante.cbx.SelectedValue = CInt(vgConfiguraciones("IDTipoComprobanteImportacion")) Then
            txtComprobante.txt.Mask = "DESP Nro:0000000"
        Else
            txtComprobante.txt.Mask = ""
        End If
    End Sub

    Private Sub btnAsiento_Click(sender As Object, e As EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamento.PropertyChanged
        If vNuevo = True Then
            Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamento.cbx.SelectedValue)
            If Seccion = True Then
                Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamento.cbx.SelectedValue)
                CSistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
                cbxSeccion.cbx.Text = ""
                cbxSeccion.SoloLectura = False
            Else
                cbxSeccion.SoloLectura = True
            End If
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmModificarMovimiento_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        LiberarMemoria()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD)
    End Sub
End Class
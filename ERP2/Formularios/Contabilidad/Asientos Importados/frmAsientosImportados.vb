﻿Public Class frmAsientosImportados

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidadOperacion.txt.ResetText()
        txtTotalOperacion.txt.ResetText()

        'CheckBox
        chkSucursal.Checked = False

        'ComboBox
        cbxSucursal.Enabled = False

        'RadioButton

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")


    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Consulta = "Select Numero, 'Suc'=CodigoSucursal, Fec, Operacion, Observacion, Total, IDTransaccion From VAsientoImportado "

        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        If Numero > 0 Then
            Where = " Where Numero=" & Numero
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable(Consulta & " " & Where & " Order By Fecha ")
        DataGridView1.DataSource = dt

        'Formato
        DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        DataGridView1.RowHeadersVisible = False
        DataGridView1.BackgroundColor = Color.White
        DataGridView1.StandardTab = True

        For c As Integer = 0 To DataGridView1.ColumnCount - 1
            DataGridView1.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        DataGridView1.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        DataGridView1.Columns(5).DefaultCellStyle.Format = "N0"
        DataGridView1.Columns(6).Visible = False
        DataGridView1.Columns(6).Name = "IDTransaccion"

        txtCantidadOperacion.txt.Text = DataGridView1.RowCount
        CSistema.TotalesGrid(DataGridView1, txtTotalOperacion.txt, 5, True)


    End Sub

    Sub EliminarRegistros()

        'Consulta
        If MessageBox.Show("Desea eliminar todos los registros dentro del rango de fecha selecciado?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        Dim SQL As String = " Exec SpAsientoImportado " & vbCrLf
        CSistema.ConcatenarParametro(SQL, "@Desde", CSistema.FormatoFechaDesdeHastaBaseDatos(dtpDesde.Value, True, False))
        CSistema.ConcatenarParametro(SQL, "@Hasta", CSistema.FormatoFechaDesdeHastaBaseDatos(dtpHasta.Value, False, True))

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL).Copy

        If dt Is Nothing Then
            MessageBox.Show("Se produjo un error.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("Se produjo un error.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        If oRow("Procesado") = True Then
            MessageBox.Show("Registros procesados: " & oRow("Registros"), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        Else
            MessageBox.Show(oRow("Mensaje"), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        'If lvOperacion.SelectedItems.Count = 0 Then
        '    Dim Mensaje As String = "Seleccione correctamente un registro!"
        '    ctrError.SetError(lvOperacion, Mensaje)
        '    ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
        '    Exit Sub
        'End If

        'If IsNumeric(lvOperacion.SelectedItems(0).SubItems(13).Text) = False Then
        '    Dim Mensaje As String = "Seleccione correctamente un registro!"
        '    ctrError.SetError(lvOperacion, Mensaje)
        '    ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
        '    Exit Sub
        'End If

        ''Obtener el IDTransaccion
        'IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(13).Text)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Sub Visualizar()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(DataGridView1, Mensaje)
            ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Text = "Asiento"

        Dim IDTransaccion As Integer = 0

        IDTransaccion = DataGridView1.SelectedRows(0).Cells("IDTransaccion").Value

        frm.IDTransaccion = IDTransaccion

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Sub Nuevo()

        Dim frm As New frmAgregarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Text = "Asiento"

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Private Sub frm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub dtbRegistrosPorFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtbRegistrosPorFecha.Click
        Where = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub btnVizualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVizualizar.Click
        Visualizar()
    End Sub


    Private Sub txtNumeroAsiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroAsiento.KeyUp
        If e.KeyCode = Keys.Enter Then

            If IsNumeric(txtNumeroAsiento.Text) = False Then
                Exit Sub
            End If

            Dim Numero As Integer = CInt(txtNumeroAsiento.Text)
            ListarOperaciones(Numero)

        End If

    End Sub

    Private Sub btnRegistrosNoBalanceados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Where = " Where Balanceado='False' And (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        EliminarRegistros()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        FGMostrarFormulario(Me, frmImportarAsientos, "Importar Asientos", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
    End Sub

    'FA 28/05/2021
    Sub frmAsientosImportados_Activate()
        Me.Refresh()
    End Sub

End Class
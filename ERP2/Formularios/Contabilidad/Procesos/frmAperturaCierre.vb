﻿Public Class frmAperturaCierre
    'CLASES
    Dim CSistema As New CSistema
    Dim Consulta As String

    'PROPIEDADES
    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private dtDocumentosValue As DataTable
    Public Property dtDocumentos() As DataTable
        Get
            Return dtDocumentosValue
        End Get
        Set(ByVal value As DataTable)
            dtDocumentosValue = value
        End Set
    End Property

    Dim vControles() As Control
    Dim vNuevo As Boolean

    Sub Inicializar()
        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "CIERRE Y REAPERTURA", "CO")

        Dim Mes() As String = {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
        cbxMes.Items.AddRange(Mes)
        cbxMes.DropDownStyle = ComboBoxStyle.DropDownList
        cbxMes.SelectedIndex = Date.Now.Month - 1
        cbxMes.SelectedItem = Date.Now.MaxValue

        NumericUpDown1.Minimum = Date.Now.Year - 5
        NumericUpDown1.Maximum = Date.Now.Year + 5
        NumericUpDown1.Value = Date.Now.Year
        txtFecha.Hoy()

    End Sub

    Sub ListarInventario()

        'Dim Consulta As String = "Select 'ID'=IDTransaccion, 'Suc.'=Sucursal, 'Comprobante'=[Cod.] + ' ' + Comprobante, 'Ref'=Referencia, Cliente, RUC, Fec, Condicion,'NumeroPedido'=(Select Top(1) VDL.NumeroPedido From VVentaLoteDistribucion VDL Where VDL.IDTransaccionVenta=V.IDTransaccion), [Fec. Venc.], Total, Saldo, 'Estado'=EstadoVenta, Anulado,  'Usuario'=UsuarioIdentificador, 'NroLote'=(Select Min(VDL.Numero) From VVentaLoteDistribucion VDL Where VDL.IDTransaccionVenta=V.IDTransaccion), 'Distribuidor'=(Select Top(1) VDL.Distribuidor From VVentaLoteDistribucion VDL Where VDL.IDTransaccionVenta=V.IDTransaccion) From VVenta V "
        DataGridView1.DataSource = dtDocumentos


        If DataGridView1.ColumnCount = 0 Then
            Exit Sub
        End If

        DataGridView1.Columns("Codigo").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns("Denominacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView1.Columns("Credito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        DataGridView1.Columns("Debito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight



        'Formato
        'DataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
        'DataGridView1.Columns("Anulado").Visible = False
        'DataGridView1.Columns("ID").Visible = False


        'For c As Integer = 0 To DataGridView1.ColumnCount - 1
        'DataGridView1.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        'Next

        'DataGridView1.Columns("Suc.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'DataGridView1.Columns("Fec").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'DataGridView1.Columns("NumeroPedido").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'DataGridView1.Columns("Condicion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'DataGridView1.Columns("Fec. Venc.").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        'DataGridView1.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        'DataGridView1.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        'DataGridView1.Columns("Total").DefaultCellStyle.Format = "N0"
        'DataGridView1.Columns("Saldo").DefaultCellStyle.Format = "N0"

    End Sub
    Sub Procesar()
        Dim vcredito, vdebito As Decimal
        Dim i As Integer = 1 'txtUltimoNroConciliado.ObtenerValor + 1

        For Each oRow As DataRow In dtDocumentos.Rows
            vcredito = oRow("Debito")
            vdebito = oRow("Credito")

            oRow("Debito") = vdebito
            oRow("Credito") = vcredito
        Next

        ListarInventario()
    End Sub

    Private Sub frmAperturaCierre_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        If rdbApertura.Checked = True Then
            Consulta = "Execute SpViewApertura  @Año=" & NumericUpDown1.Value & ", @Mes='" & cbxMes.SelectedIndex + 1 & "' , @Factor='1' "
        End If
        If rdbCierre.Checked = True Then
            Consulta = "Execute SpViewCierre  @Año=" & NumericUpDown1.Value & ", @Mes='" & cbxMes.SelectedIndex + 1 & "' , @Factor='1' "
        End If

        'Listar 
        dtDocumentos = CSistema.ExecuteToDataTable(Consulta)
        ListarInventario()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs)
        Cancelar()
    End Sub
    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)
        'CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, btnBusquedaAvanzada, btnAsiento, vControles)
    End Sub
    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        vNuevo = False

        'Ultimo Registro
        'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub
    Sub Guardar()

        Dim Porcentaje As Integer = 0
        Dim Indice As Integer = 0
        Dim dtAsiento As DataTable
        dtAsiento = CSistema.ExecuteToDataTable("Execute SpViewApertura  @Año=" & NumericUpDown1.Value & ", @Mes='" & cbxMes.SelectedIndex + 1 & "' , @Factor='1' ")
        'Dim ProgresoTexto As String = " 0 de " & dtAsiento.Rows.Count & " - 0%"

        'CMultihilo.ProgressBarValue(Me, pbProgreso, Porcentaje)

        For Each oRow As DataRow In dtAsiento.Rows

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer = 0
            Dim Procedimiento As String = "SpReapertura"
            CSistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccion", oRow("IDTransaccion").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Numero", oRow("Asiento").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Numero", oRow("Numero").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Asiento", oRow("Asiento").ToString, ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, Procedimiento, False, False, MensajeRetorno) = False Then
                If MessageBox.Show(MensajeRetorno & ". Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                    Exit For
                End If
            End If

        Next

        'DetenerProceso = True
        MessageBox.Show("Proceso finalizado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        'Detener()

    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

End Class
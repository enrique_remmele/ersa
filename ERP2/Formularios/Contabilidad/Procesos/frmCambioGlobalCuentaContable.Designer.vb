﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambioGlobalCuentaContable
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.lblCuentaOrigen = New System.Windows.Forms.Label()
        Me.lblCuentaNueva = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblHasta = New System.Windows.Forms.Label()
        Me.lblIncluirConciliado = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.cbxOperacion = New System.Windows.Forms.ComboBox()
        Me.gbx = New System.Windows.Forms.GroupBox()
        Me.txtCuentaOriginal = New ERP.ocxTXTCuentaContable()
        Me.txtCuentaNueva = New ERP.ocxTXTCuentaContable()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.chkIncluirConciliados = New ERP.ocxCHK()
        Me.chkOperacion = New ERP.ocxCHK()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbx.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(291, 192)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(75, 23)
        Me.btnProcesar.TabIndex = 6
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'lblCuentaOrigen
        '
        Me.lblCuentaOrigen.AutoSize = True
        Me.lblCuentaOrigen.Location = New System.Drawing.Point(6, 21)
        Me.lblCuentaOrigen.Name = "lblCuentaOrigen"
        Me.lblCuentaOrigen.Size = New System.Drawing.Size(82, 13)
        Me.lblCuentaOrigen.TabIndex = 0
        Me.lblCuentaOrigen.Text = "Cuenta Original:"
        '
        'lblCuentaNueva
        '
        Me.lblCuentaNueva.AutoSize = True
        Me.lblCuentaNueva.Location = New System.Drawing.Point(6, 54)
        Me.lblCuentaNueva.Name = "lblCuentaNueva"
        Me.lblCuentaNueva.Size = New System.Drawing.Size(79, 13)
        Me.lblCuentaNueva.TabIndex = 2
        Me.lblCuentaNueva.Text = "Cuenta Nueva:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 84)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 4
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(184, 84)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(38, 13)
        Me.lblHasta.TabIndex = 6
        Me.lblHasta.Text = "Hasta:"
        '
        'lblIncluirConciliado
        '
        Me.lblIncluirConciliado.AutoSize = True
        Me.lblIncluirConciliado.Location = New System.Drawing.Point(234, 149)
        Me.lblIncluirConciliado.Name = "lblIncluirConciliado"
        Me.lblIncluirConciliado.Size = New System.Drawing.Size(87, 13)
        Me.lblIncluirConciliado.TabIndex = 4
        Me.lblIncluirConciliado.Text = "Incl. Conciliados:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(13, 149)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 1
        Me.lblOperacion.Text = "Operacion:"
        '
        'cbxOperacion
        '
        Me.cbxOperacion.Enabled = False
        Me.cbxOperacion.FormattingEnabled = True
        Me.cbxOperacion.Location = New System.Drawing.Point(92, 145)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.Size = New System.Drawing.Size(136, 21)
        Me.cbxOperacion.TabIndex = 3
        '
        'gbx
        '
        Me.gbx.Controls.Add(Me.lblCuentaOrigen)
        Me.gbx.Controls.Add(Me.txtCuentaOriginal)
        Me.gbx.Controls.Add(Me.txtCuentaNueva)
        Me.gbx.Controls.Add(Me.lblHasta)
        Me.gbx.Controls.Add(Me.lblPeriodo)
        Me.gbx.Controls.Add(Me.txtDesde)
        Me.gbx.Controls.Add(Me.lblCuentaNueva)
        Me.gbx.Controls.Add(Me.txtHasta)
        Me.gbx.Location = New System.Drawing.Point(12, 12)
        Me.gbx.Name = "gbx"
        Me.gbx.Size = New System.Drawing.Size(480, 121)
        Me.gbx.TabIndex = 0
        Me.gbx.TabStop = False
        '
        'txtCuentaOriginal
        '
        Me.txtCuentaOriginal.AlturaMaxima = 0
        Me.txtCuentaOriginal.Consulta = Nothing
        Me.txtCuentaOriginal.ListarTodas = False
        Me.txtCuentaOriginal.Location = New System.Drawing.Point(94, 14)
        Me.txtCuentaOriginal.Name = "txtCuentaOriginal"
        Me.txtCuentaOriginal.Registro = Nothing
        Me.txtCuentaOriginal.Resolucion173 = False
        Me.txtCuentaOriginal.Seleccionado = False
        Me.txtCuentaOriginal.Size = New System.Drawing.Size(378, 27)
        Me.txtCuentaOriginal.SoloLectura = False
        Me.txtCuentaOriginal.TabIndex = 1
        Me.txtCuentaOriginal.Texto = Nothing
        '
        'txtCuentaNueva
        '
        Me.txtCuentaNueva.AlturaMaxima = 0
        Me.txtCuentaNueva.Consulta = Nothing
        Me.txtCuentaNueva.ListarTodas = False
        Me.txtCuentaNueva.Location = New System.Drawing.Point(94, 47)
        Me.txtCuentaNueva.Name = "txtCuentaNueva"
        Me.txtCuentaNueva.Registro = Nothing
        Me.txtCuentaNueva.Resolucion173 = False
        Me.txtCuentaNueva.Seleccionado = False
        Me.txtCuentaNueva.Size = New System.Drawing.Size(378, 27)
        Me.txtCuentaNueva.SoloLectura = False
        Me.txtCuentaNueva.TabIndex = 3
        Me.txtCuentaNueva.Texto = Nothing
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 11, 11, 54, 26, 812)
        Me.txtDesde.Location = New System.Drawing.Point(94, 80)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(90, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 5
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 11, 11, 54, 26, 812)
        Me.txtHasta.Location = New System.Drawing.Point(222, 80)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(90, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 7
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(391, 192)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'chkIncluirConciliados
        '
        Me.chkIncluirConciliados.Color = System.Drawing.Color.Empty
        Me.chkIncluirConciliados.Location = New System.Drawing.Point(321, 145)
        Me.chkIncluirConciliados.Name = "chkIncluirConciliados"
        Me.chkIncluirConciliados.Size = New System.Drawing.Size(20, 21)
        Me.chkIncluirConciliados.SoloLectura = False
        Me.chkIncluirConciliados.TabIndex = 5
        Me.chkIncluirConciliados.Texto = Nothing
        Me.chkIncluirConciliados.Valor = False
        '
        'chkOperacion
        '
        Me.chkOperacion.Color = System.Drawing.Color.Empty
        Me.chkOperacion.Location = New System.Drawing.Point(72, 145)
        Me.chkOperacion.Name = "chkOperacion"
        Me.chkOperacion.Size = New System.Drawing.Size(20, 21)
        Me.chkOperacion.SoloLectura = False
        Me.chkOperacion.TabIndex = 2
        Me.chkOperacion.Texto = Nothing
        Me.chkOperacion.Valor = False
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 222)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(499, 22)
        Me.StatusStrip1.TabIndex = 8
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'frmCambioGlobalCuentaContable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(499, 244)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gbx)
        Me.Controls.Add(Me.cbxOperacion)
        Me.Controls.Add(Me.btnProcesar)
        Me.Controls.Add(Me.lblIncluirConciliado)
        Me.Controls.Add(Me.lblOperacion)
        Me.Controls.Add(Me.chkIncluirConciliados)
        Me.Controls.Add(Me.chkOperacion)
        Me.Name = "frmCambioGlobalCuentaContable"
        Me.Text = "frmCambioGlobalCuentaContable"
        Me.gbx.ResumeLayout(False)
        Me.gbx.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCuentaOriginal As ERP.ocxTXTCuentaContable
    Friend WithEvents txtCuentaNueva As ERP.ocxTXTCuentaContable
    Friend WithEvents chkIncluirConciliados As ERP.ocxCHK
    Friend WithEvents chkOperacion As ERP.ocxCHK
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents lblCuentaOrigen As System.Windows.Forms.Label
    Friend WithEvents lblCuentaNueva As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents lblIncluirConciliado As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents cbxOperacion As System.Windows.Forms.ComboBox
    Friend WithEvents gbx As System.Windows.Forms.GroupBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class

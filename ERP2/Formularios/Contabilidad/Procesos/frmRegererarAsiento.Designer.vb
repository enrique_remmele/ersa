﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegererarAsiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkSoloConDiferencias = New System.Windows.Forms.CheckBox()
        Me.chkGenerarTodo = New System.Windows.Forms.CheckBox()
        Me.lblPorcentaje = New System.Windows.Forms.Label()
        Me.pbPorcentaje = New System.Windows.Forms.ProgressBar()
        Me.cbxDocumento = New System.Windows.Forms.ComboBox()
        Me.lblDocumento = New System.Windows.Forms.Label()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblHasta = New System.Windows.Forms.Label()
        Me.lblDesde = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSoloConDiferencias)
        Me.gbxFiltro.Controls.Add(Me.chkGenerarTodo)
        Me.gbxFiltro.Controls.Add(Me.lblPorcentaje)
        Me.gbxFiltro.Controls.Add(Me.pbPorcentaje)
        Me.gbxFiltro.Controls.Add(Me.cbxDocumento)
        Me.gbxFiltro.Controls.Add(Me.lblDocumento)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.lblSucursal)
        Me.gbxFiltro.Controls.Add(Me.lblHasta)
        Me.gbxFiltro.Controls.Add(Me.lblDesde)
        Me.gbxFiltro.Controls.Add(Me.txtHasta)
        Me.gbxFiltro.Controls.Add(Me.txtDesde)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(327, 186)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkSoloConDiferencias
        '
        Me.chkSoloConDiferencias.AutoSize = True
        Me.chkSoloConDiferencias.Location = New System.Drawing.Point(90, 130)
        Me.chkSoloConDiferencias.Name = "chkSoloConDiferencias"
        Me.chkSoloConDiferencias.Size = New System.Drawing.Size(164, 17)
        Me.chkSoloConDiferencias.TabIndex = 9
        Me.chkSoloConDiferencias.Text = "Solo asientos con diferencias"
        Me.chkSoloConDiferencias.UseVisualStyleBackColor = True
        '
        'chkGenerarTodo
        '
        Me.chkGenerarTodo.AutoSize = True
        Me.chkGenerarTodo.Location = New System.Drawing.Point(90, 107)
        Me.chkGenerarTodo.Name = "chkGenerarTodo"
        Me.chkGenerarTodo.Size = New System.Drawing.Size(162, 17)
        Me.chkGenerarTodo.TabIndex = 8
        Me.chkGenerarTodo.Text = "Generar todas las sucursales"
        Me.chkGenerarTodo.UseVisualStyleBackColor = True
        '
        'lblPorcentaje
        '
        Me.lblPorcentaje.AutoSize = True
        Me.lblPorcentaje.BackColor = System.Drawing.Color.Transparent
        Me.lblPorcentaje.Location = New System.Drawing.Point(156, 156)
        Me.lblPorcentaje.Name = "lblPorcentaje"
        Me.lblPorcentaje.Size = New System.Drawing.Size(60, 13)
        Me.lblPorcentaje.TabIndex = 11
        Me.lblPorcentaje.Text = "0 de 0 - 0%"
        Me.lblPorcentaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pbPorcentaje
        '
        Me.pbPorcentaje.Location = New System.Drawing.Point(90, 152)
        Me.pbPorcentaje.Name = "pbPorcentaje"
        Me.pbPorcentaje.Size = New System.Drawing.Size(217, 21)
        Me.pbPorcentaje.TabIndex = 10
        '
        'cbxDocumento
        '
        Me.cbxDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxDocumento.FormattingEnabled = True
        Me.cbxDocumento.Location = New System.Drawing.Point(87, 53)
        Me.cbxDocumento.Name = "cbxDocumento"
        Me.cbxDocumento.Size = New System.Drawing.Size(220, 21)
        Me.cbxDocumento.TabIndex = 3
        '
        'lblDocumento
        '
        Me.lblDocumento.AutoSize = True
        Me.lblDocumento.Location = New System.Drawing.Point(16, 57)
        Me.lblDocumento.Name = "lblDocumento"
        Me.lblDocumento.Size = New System.Drawing.Size(65, 13)
        Me.lblDocumento.TabIndex = 2
        Me.lblDocumento.Text = "Documento:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(87, 26)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(220, 21)
        Me.cbxSucursal.TabIndex = 1
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(16, 30)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 0
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(178, 84)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(38, 13)
        Me.lblHasta.TabIndex = 6
        Me.lblHasta.Text = "Hasta:"
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(16, 84)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(41, 13)
        Me.lblDesde.TabIndex = 4
        Me.lblDesde.Text = "Desde:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 29, 13, 3, 23, 343)
        Me.txtHasta.Location = New System.Drawing.Point(216, 80)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(91, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 7
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 29, 13, 3, 8, 859)
        Me.txtDesde.Location = New System.Drawing.Point(87, 80)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(91, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 5
        '
        'btnGenerar
        '
        Me.btnGenerar.Location = New System.Drawing.Point(153, 204)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(75, 23)
        Me.btnGenerar.TabIndex = 1
        Me.btnGenerar.Text = "Generar"
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(244, 204)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(75, 23)
        Me.btnCerrar.TabIndex = 2
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'frmRegererarAsiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(356, 242)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnGenerar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmRegererarAsiento"
        Me.Text = "frmRegererarAsiento"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents cbxDocumento As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocumento As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents lblDesde As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents lblPorcentaje As System.Windows.Forms.Label
    Friend WithEvents pbPorcentaje As System.Windows.Forms.ProgressBar
    Friend WithEvents chkGenerarTodo As System.Windows.Forms.CheckBox
    Friend WithEvents chkSoloConDiferencias As System.Windows.Forms.CheckBox
End Class

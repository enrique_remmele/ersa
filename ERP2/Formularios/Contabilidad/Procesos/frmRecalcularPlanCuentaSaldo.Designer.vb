﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecalcularPlanCuentaSaldo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxParametros = New System.Windows.Forms.GroupBox()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.btnProcesarAño = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nudAño = New System.Windows.Forms.NumericUpDown()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.lblAño = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.rdbContabilidad = New System.Windows.Forms.RadioButton()
        Me.rdbAdministracion = New System.Windows.Forms.RadioButton()
        Me.gbxParametros.SuspendLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxParametros
        '
        Me.gbxParametros.Controls.Add(Me.rdbContabilidad)
        Me.gbxParametros.Controls.Add(Me.rdbAdministracion)
        Me.gbxParametros.Controls.Add(Me.btnProcesar)
        Me.gbxParametros.Controls.Add(Me.btnProcesarAño)
        Me.gbxParametros.Controls.Add(Me.Label1)
        Me.gbxParametros.Controls.Add(Me.nudAño)
        Me.gbxParametros.Controls.Add(Me.cbxMes)
        Me.gbxParametros.Controls.Add(Me.lblAño)
        Me.gbxParametros.Location = New System.Drawing.Point(12, 12)
        Me.gbxParametros.Name = "gbxParametros"
        Me.gbxParametros.Size = New System.Drawing.Size(299, 138)
        Me.gbxParametros.TabIndex = 0
        Me.gbxParametros.TabStop = False
        Me.gbxParametros.Text = "Parametros"
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(172, 94)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(121, 32)
        Me.btnProcesar.TabIndex = 7
        Me.btnProcesar.Text = "Recalcular el Mes"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'btnProcesarAño
        '
        Me.btnProcesarAño.Location = New System.Drawing.Point(20, 94)
        Me.btnProcesarAño.Name = "btnProcesarAño"
        Me.btnProcesarAño.Size = New System.Drawing.Size(110, 32)
        Me.btnProcesarAño.TabIndex = 6
        Me.btnProcesarAño.Text = "Recalcular el Año"
        Me.btnProcesarAño.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(136, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Mes:"
        '
        'nudAño
        '
        Me.nudAño.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAño.Location = New System.Drawing.Point(52, 51)
        Me.nudAño.Name = "nudAño"
        Me.nudAño.Size = New System.Drawing.Size(78, 26)
        Me.nudAño.TabIndex = 3
        '
        'cbxMes
        '
        Me.cbxMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxMes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"})
        Me.cbxMes.Location = New System.Drawing.Point(172, 50)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(121, 28)
        Me.cbxMes.TabIndex = 5
        '
        'lblAño
        '
        Me.lblAño.AutoSize = True
        Me.lblAño.Location = New System.Drawing.Point(17, 58)
        Me.lblAño.Name = "lblAño"
        Me.lblAño.Size = New System.Drawing.Size(29, 13)
        Me.lblAño.TabIndex = 2
        Me.lblAño.Text = "Año:"
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(195, 156)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(110, 23)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'rdbContabilidad
        '
        Me.rdbContabilidad.AutoSize = True
        Me.rdbContabilidad.Location = New System.Drawing.Point(151, 28)
        Me.rdbContabilidad.Name = "rdbContabilidad"
        Me.rdbContabilidad.Size = New System.Drawing.Size(83, 17)
        Me.rdbContabilidad.TabIndex = 1
        Me.rdbContabilidad.Text = "Contabilidad"
        Me.rdbContabilidad.UseVisualStyleBackColor = True
        '
        'rdbAdministracion
        '
        Me.rdbAdministracion.AutoSize = True
        Me.rdbAdministracion.Checked = True
        Me.rdbAdministracion.Location = New System.Drawing.Point(52, 28)
        Me.rdbAdministracion.Name = "rdbAdministracion"
        Me.rdbAdministracion.Size = New System.Drawing.Size(93, 17)
        Me.rdbAdministracion.TabIndex = 0
        Me.rdbAdministracion.TabStop = True
        Me.rdbAdministracion.Text = "Administracion"
        Me.rdbAdministracion.UseVisualStyleBackColor = True
        '
        'frmRecalcularPlanCuentaSaldo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(315, 199)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.gbxParametros)
        Me.Name = "frmRecalcularPlanCuentaSaldo"
        Me.Text = "frmRecalcularPlanCuentaSaldo"
        Me.gbxParametros.ResumeLayout(False)
        Me.gbxParametros.PerformLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxParametros As System.Windows.Forms.GroupBox
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents nudAño As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbxMes As System.Windows.Forms.ComboBox
    Friend WithEvents lblAño As System.Windows.Forms.Label
    Friend WithEvents btnProcesarAño As System.Windows.Forms.Button
    Friend WithEvents rdbContabilidad As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAdministracion As System.Windows.Forms.RadioButton
End Class

﻿Imports System.Threading

Public Class frmKardex

    'CLASES
    Dim CSistema As New CSistema
    Dim CMHilo As New CMHilo
    Dim CData As New CData
    Dim CReporte As New Reporte.CReporteKardex

    'PROPIEDADES
    Public Property dt As DataTable

    'VARIABLES
    Dim vTodos As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Botones
        btnCancelar.Enabled = False
        btnCargar.Enabled = True
        btnRecalcular.Enabled = False
        btnProcesar.Enabled = False

        'Controles
        lblProgreso.Text = "0 de 0 - 0%"
        'Año
        nudAño.Minimum = Date.Now.Year - 10
        nudAño.Maximum = Date.Now.Year + 10
        nudAño.Value = Date.Now.Year

        txtProducto.Conectar()

        btnRecalcular.Enabled = False
        btnProcesar.Enabled = False
        btnCancelar.Enabled = False
        btnListar.Enabled = False
        btnCargar.Enabled = True

        pb.Value = 0
        cbxMes.SelectedIndex = 0

    End Sub

    Sub Vizualizar()

        dgvLista.DataSource = Nothing

        Dim frm As New frmConsultaBaseDatos
        frm.Tipo = frmConsultaBaseDatos.ENUNTipo.Consulta
        frm.SQL = "Select IDKardex, Fecha, Indice, Producto, Operacion, Comprobante, CantidadEntrada, CostoUnitarioEntrada, TotalEntrada, CantidadSalida, CostoUnitarioSalida, TotalSalida, CantidadSaldo, CostoPromedio, TotalSaldo, CostoPromedioOperacion, 'Diferencia'=(Case When (Operacion)='VENTAS CLIENTES' Then DiferenciaTotal Else 0 End), IDProducto From VKardex Where Fecha>=convert(date, '01' + '-' + '" & cbxMes.SelectedIndex + 1 & "' + '-' + '" & nudAño.Value & "') Order By Indice"
        'frm.SQL = "Select IDKardex, Fecha, Indice, Producto, Operacion, Comprobante, CantidadEntrada, CostoUnitarioEntrada, TotalEntrada, CantidadSalida, CostoUnitarioSalida, TotalSalida, CantidadSaldo, CostoPromedio, TotalSaldo, CostoPromedioOperacion, 'Diferencia'=(Case When (Operacion)='VENTAS CLIENTES' Then DiferenciaTotal Else 0 End), IDProducto From VKardex Where Fecha Between '20140101' And '20140131' Order By Indice"
        FGMostrarFormulario(Me, frm, "Consultando la Base de Datos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        MessageBox.Show(CSistema.FormatoNumero(frm.dt.Rows.Count) & " Registro(s) cargado(s)", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

        dt = frm.dt

        btnRecalcular.Enabled = True
        btnProcesar.Enabled = True
        btnCancelar.Enabled = True
        btnListar.Enabled = True
        btnCargar.Enabled = False

        'Cargar totales
        ListarTodos()

    End Sub

    Sub Listar()

        If txtProducto.Seleccionado = False Then
            MessageBox.Show("Seleccione un producto!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        vTodos = False

        Dim vdt As DataTable = CData.FiltrarDataTable(dt, "IDProducto=" & txtProducto.Registro("ID").ToString)
        CData.OrderDataTable(vdt, "Fecha, Indice")

        CSistema.dtToGrid(dgvLista, vdt)

        'Formato
        'dgvLista.Columns("IDKardex").Visible = False
        'dgvLista.Columns("Indice").Visible = False
        dgvLista.Columns("IDProducto").Visible = False
        dgvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        'textos de Cabecera
        dgvLista.Columns("CantidadEntrada").HeaderText = "Entrada"
        dgvLista.Columns("CostoUnitarioEntrada").HeaderText = "Costo E."
        dgvLista.Columns("TotalEntrada").HeaderText = "Total"
        dgvLista.Columns("CantidadSalida").HeaderText = "Salida"
        dgvLista.Columns("CostoUnitarioSalida").HeaderText = "Costo S."
        dgvLista.Columns("TotalSalida").HeaderText = "Total"
        dgvLista.Columns("CantidadSaldo").HeaderText = "Saldo"
        dgvLista.Columns("CostoPromedio").HeaderText = "Costo Promedio"
        dgvLista.Columns("TotalSaldo").HeaderText = "Total"
        dgvLista.Columns("CostoPromedioOperacion").HeaderText = "Costo Operacion"

        Dim ColumnasNumericas() As String = {"CantidadEntrada", "CostoUnitarioEntrada", "TotalEntrada", "CantidadSalida", "CostoUnitarioSalida", "TotalSalida", "CantidadSaldo", "CostoPromedio", "TotalSaldo", "CostoPromedioOperacion", "Diferencia"}

        For Each col As String In ColumnasNumericas
            dgvLista.Columns(col).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLista.Columns(col).DefaultCellStyle.Format = "N2"
        Next

        txtCantidad.Text = CSistema.FormatoNumero(dgvLista.RowCount.ToString, False)

        Dim Diferencia As Decimal = CSistema.gridSumColumn(dgvLista, "Diferencia").ToString
        txtTotalDiferencia.Text = CSistema.FormatoNumero(Diferencia, True)


        Dim DiferenciaPromedio As Decimal = 0
        Dim SumaCostoPromedio As Decimal = CSistema.gridSumColumn(dgvLista, "CostoPromedio")
        Dim SumaCostoOperacion As Decimal = CSistema.gridSumColumn(dgvLista, "CostoPromedioOperacion")
        DiferenciaPromedio = SumaCostoPromedio - SumaCostoOperacion

        If dgvLista.RowCount > 0 Then
            DiferenciaPromedio = DiferenciaPromedio / dgvLista.RowCount
        End If

        txtDiferenciaPromedio.Text = CSistema.FormatoNumero(DiferenciaPromedio, True)

    End Sub

    Sub ListarTodos()

        vTodos = True

        Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select Referencia, Producto, 'Total'=Sum(DiferenciaTotal), IDProducto From VKardex Where fecha>=convert(date, '01' + '-' + '" & cbxMes.SelectedIndex + 1 & "' + '-' + '" & nudAño.Value & "') And Operacion = 'VENTAS CLIENTES' Group By Referencia, Producto, IDProducto Order By 3 Desc")
        'Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select Referencia, Producto, 'Total'=Sum(DiferenciaTotal), IDProducto From VKardex Where fecha between '20140101' And '20140131' And Operacion = 'VENTAS CLIENTES' Group By Referencia, Producto, IDProducto Order By 3 Desc")

        CSistema.dtToGrid(dgvLista, vdt)

        'Formato
        dgvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvLista.Columns("IDProducto").Visible = False

        Dim ColumnasNumericas() As String = {"Total"}

        For Each col As String In ColumnasNumericas
            dgvLista.Columns(col).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvLista.Columns(col).DefaultCellStyle.Format = "N2"
        Next

        txtCantidad.Text = CSistema.FormatoNumero(dgvLista.RowCount.ToString, False)

        Dim Diferencia As Decimal = CSistema.gridSumColumn(dgvLista, "Total").ToString
        txtTotalDiferencia.Text = CSistema.FormatoNumero(Diferencia, True)

    End Sub

    Sub Recargar()

        'Advertencia
        If MessageBox.Show("Este proceso eliminara y volvera a recargar en el kardex los registros desde el mes/año seleccionado! Desea continuar?", "Atencion!!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        Dim frm As New frmConsultaBaseDatos
        frm.Tipo = frmConsultaBaseDatos.ENUNTipo.Ejecutar
        frm.SQL = "Exec SpCargarKardex"
        FGMostrarFormulario(Me, frm, "Consultando la Base de Datos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            MessageBox.Show("No se pudo terminar la operacion!", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        MessageBox.Show("Registros procesados!", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Sub Recalcular()

        'Advertencia
        If MessageBox.Show("Este proceso recalculara los valores de los registros en el Kardex! Desea continuar?", "Atencion!!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        Dim frm As New frmConsultaBaseDatos
        frm.Tipo = frmConsultaBaseDatos.ENUNTipo.Ejecutar
        frm.SQL = "Exec SpRecalcularKardex"
        FGMostrarFormulario(Me, frm, "Consultando la Base de Datos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            MessageBox.Show("No se pudo terminar la operacion!", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        MessageBox.Show("Registros procesados!", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Listar()

    End Sub

    Sub Procesar()

        'Advertencia
        If MessageBox.Show("Esto reprocesará todos los costos  de los registros en las operaciones afectadas! Desea continuar?", "Atencion!!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        Dim frm As New frmConsultaBaseDatos
        frm.Tipo = frmConsultaBaseDatos.ENUNTipo.Ejecutar
        frm.SQL = "Exec SpProcesarKardex"
        FGMostrarFormulario(Me, frm, "Consultando la Base de Datos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            MessageBox.Show("No se pudo terminar la operacion!", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        MessageBox.Show("Registros procesados!", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Listar()

    End Sub

    Sub Cancelar()

        dt = Nothing

        dgvLista.DataSource = Nothing

        btnRecalcular.Enabled = False
        btnProcesar.Enabled = False
        btnCancelar.Enabled = False
        btnListar.Enabled = False
        btnCargar.Enabled = True

    End Sub

    Sub informe()

        CReporte.ListadoKardex(dgvLista.DataSource, txtProducto.Registro("Descripcion").ToString, cbxMes.Text & " " & nudAño.Value)


    End Sub

    Private Sub frmKardex_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnRecalcular_Click(sender As System.Object, e As System.EventArgs) Handles btnRecalcular.Click
        Recalcular()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnVisualizar_Click(sender As System.Object, e As System.EventArgs) Handles btnCargar.Click
        Vizualizar()
    End Sub

    Private Sub lklRecargar_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklRecargar.LinkClicked
        Recargar()
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(dgvLista.DataSource)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        informe()
    End Sub

    Private Sub btnMostrarTodos_Click(sender As System.Object, e As System.EventArgs) Handles btnMostrarTodos.Click
        ListarTodos()
    End Sub

    Private Sub dgvLista_CellMouseDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvLista.CellMouseDoubleClick

        If vTodos = False Then
            Exit Sub
        End If

        Dim Codigo As String = dgvLista.SelectedRows(0).Cells("IDProducto").Value
        txtProducto.SetValue(Codigo)
        Listar()
    End Sub

End Class
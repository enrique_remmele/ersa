﻿Imports System.Threading

Public Class frmRenumeracionAsiento

    'CLASES
    Dim CSistema As New CSistema
    Dim CMultihilo As New CMHilo

    'PROPIEDADES
    Private Property Tabla As String = "VAsientoCG"
    Private Property Proceso As Thread
    Private Property DetenerProceso As Boolean = False

    ' VARIABLES
    Dim dtAsiento As New DataTable
    Dim Max As Integer

    Sub Inicializa()

        'Form 
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Button
        btnCancelar.Enabled = False

        'Fechas JGR 20140813
        dtpDesde.Value = New DateTime(Date.Today.Year, 1, 1)
        dtpHasta.Value = New DateTime(Date.Today.Year, 12, 31)

    End Sub

    Sub Cargar()

        Dim SQL As String = ""
        Dim SQL2 As String = ""

        If Tabla = "VAsiento" Then
            SQL = "Select IDTransaccion, Numero as Asiento, IDSucursal, Suc, Fecha, Comprobante, NroComprobante, Detalle, Total, Numero From " & Tabla & " where Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' Order By Fecha, Numero"
            SQL2 = "Select IsNull((Select Max(Numero) From " & Tabla & " Where Fecha < '" & CSistema.FormatoFechaBaseDatos(dtpDesde.Value, True, False) & "' And Year(Fecha)=" & dtpDesde.Value.Year & "), 0) "
        Else
            SQL = "Select IDTransaccionOrigen, NumeroAsiento as Asiento, IDSucursal, Suc, Fecha, Comprobante, NroComprobante, Detalle, Total, Numero From " & Tabla & " where Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' Order By Fecha, Numero"
            SQL2 = "Select IsNull((Select Max(NumeroAsiento) From " & Tabla & " Where Fecha < '" & CSistema.FormatoFechaBaseDatos(dtpDesde.Value, True, False) & "' And Year(Fecha)=" & dtpDesde.Value.Year & "), 0) "
        End If

        dtAsiento = CSistema.ExecuteToDataTable(SQL)
        ListarAsientos()

        'Ultimo Nro de Asiento del mismo año
        Dim UltimoNumero As Decimal = CSistema.ExecuteScalar(SQL2)

        txtUltimoNroConciliado.SetValue(UltimoNumero)
        txtEmpezarCon.SetValue(UltimoNumero + 1)

        pbProgreso.Value = 0
        lblProgreso.Text = "0 de 0 - 0%"

    End Sub

    Sub ListarAsientos()

        dgw.DataSource = Nothing
        dgw.DataSource = dtAsiento

        'Tamaño de Columanas
        For i As Integer = 0 To dgw.ColumnCount - 1
            dgw.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        If dgw.Columns.Count = 0 Then
            Exit Sub
        End If

        dgw.Columns("Detalle").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns(0).Visible = False
        dgw.Columns(2).Visible = False
        dgw.Columns("Numero").Visible = False

        'Formato
        'dgw.Columns(5).DefaultCellStyle.Format = "N0"

        'Alineacion
        dgw.Columns("Suc").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"

        'JGR 20140813 Comenté las sgtes lineas
        'Ultimo Nro Conciliado
        'Max = CSistema.ExecuteScalar(" Select ISNULL (MAX (Numero),0) from VAsiento Where Conciliado='True'")
        'txtUltimoNroConciliado.txt.Text = Max

        txtCantidadRegistro.txt.Text = dgw.Rows.Count

    End Sub

    Sub Iniciar()

        'Bloquear controles
        btnGuardar.Enabled = False
        btnListar.Enabled = False
        btnProcesar.Enabled = False
        btnCancelar.Enabled = True
        dtpDesde.Enabled = False
        dtpHasta.Enabled = False
        rdbAdministracion.Enabled = False
        rdbContabilidad.Enabled = False
        txtEmpezarCon.Enabled = False

        DetenerProceso = False
        Proceso = New Thread(AddressOf Atualizar)
        Proceso.Start()

    End Sub

    Sub Detener()

        'Consulta
        If MessageBox.Show("Desea cancelar el proceso?", "Detener", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        'DesBloquear controles
        btnGuardar.Enabled = True
        btnListar.Enabled = True
        btnProcesar.Enabled = True
        btnCancelar.Enabled = False
        dtpDesde.Enabled = True
        dtpHasta.Enabled = True
        rdbAdministracion.Enabled = True
        rdbContabilidad.Enabled = True
        txtEmpezarCon.Enabled = True

        DetenerProceso = True
        Proceso.Abort()

        pbProgreso.Value = 0
        lblProgreso.Text = "0 de 0 - 0%"

    End Sub

    Sub Procesar()

        pbProgreso.Value = 0
        lblProgreso.Text = "0 de 0 - 0%"

        Dim i As Integer = txtEmpezarCon.ObtenerValor

        For Each oRow As DataRow In dtAsiento.Rows
            oRow("Asiento") = i
            i = i + 1
        Next

        ListarAsientos()

    End Sub

    Sub Atualizar()

        Dim Porcentaje As Integer = 0
        Dim Indice As Integer = 0
        Dim ProgresoTexto As String = " 0 de " & dtAsiento.Rows.Count & " - 0%"

        CMultihilo.ProgressBarValue(Me, pbProgreso, Porcentaje)

        For Each oRow As DataRow In dtAsiento.Rows

            If DetenerProceso = True Then
                Exit For
            End If

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer = 0
            Dim Procedimiento As String = "SpRenumerarAsiento"

            If Tabla = "VAsientoCG" Then
                Procedimiento = Procedimiento & "CG"
            End If

            CSistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)

            If Tabla = "VAsiento" Then
                CSistema.SetSQLParameter(param, "@IDTransaccion", oRow("IDTransaccion").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Numero", oRow("Asiento").ToString, ParameterDirection.Input)
            Else
                CSistema.SetSQLParameter(param, "@Numero", oRow("Numero").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@Asiento", oRow("Asiento").ToString, ParameterDirection.Input)
            End If

            IndiceOperacion = param.GetLength(0) - 1

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, Procedimiento, False, False, MensajeRetorno) = False Then
                If MessageBox.Show(MensajeRetorno & ". Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                    Exit For
                End If
            End If

seguir:

            Indice = Indice + 1
            Porcentaje = CDec(Indice / dtAsiento.Rows.Count) * 100
            ProgresoTexto = Indice & " de " & dtAsiento.Rows.Count & " - " & Porcentaje & "%"
            CMultihilo.LabelText(Me, lblProgreso, ProgresoTexto)
            CMultihilo.ProgressBarValue(Me, pbProgreso, Porcentaje)

        Next

        DetenerProceso = True
        MessageBox.Show("Proceso finalizado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        Detener()

    End Sub

    Private Sub frmRenumeracionAsiento_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmRenumeracion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializa()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Cargar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Iniciar()
    End Sub

    Private Sub rdbAdministracion_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbAdministracion.CheckedChanged
        If rdbAdministracion.Checked = True Then
            Tabla = "VAsiento"
        End If
    End Sub

    Private Sub rdbContabilidad_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbContabilidad.CheckedChanged
        If rdbContabilidad.Checked = True Then
            Tabla = "VAsientoCG"
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Detener()
    End Sub

End Class
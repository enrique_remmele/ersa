﻿Public Class frmRecalcularPlanCuentaSaldo

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Año
        nudAño.Minimum = Date.Now.Year - 10
        nudAño.Maximum = Date.Now.Year + 10
        nudAño.Value = Date.Now.Year

        'Mes
        cbxMes.SelectedIndex = Date.Now.Month - 1

        nudAño.Focus()

    End Sub

    Sub Procesar()

        Dim Tabla As String = "SpRecalcularPlanCuentaSaldo"

        If rdbContabilidad.Checked = True Then
            Tabla = "SpRecalcularPlanCuentaSaldoCG"
        End If

        Dim SQL As String = "Exec " & Tabla & " @Año='" & nudAño.Value & "', @Mes='" & cbxMes.SelectedIndex + 1 & "' "
        Dim dttemp As DataTable = CSistema.ExecuteToDataTable(SQL, "", 1000)

        If dttemp Is Nothing Then
            MessageBox.Show("Error en la consulta...", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If dttemp.Rows.Count = 0 Then
            MessageBox.Show("Error en la consulta...", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Dim Resultado As DataRow = dttemp.Rows(0)

        If Resultado("Procesado") = False Then
            MessageBox.Show(Resultado("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim Registros As Integer = Resultado("Registros")

        MessageBox.Show("Registros procesados: (" & CSistema.FormatoNumero(Registros) & ") ", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)


    End Sub

    Sub ProcesarAño()

        If MessageBox.Show("Este proceso puede durar varios minutos! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        Dim Procesados As String = ""
        Dim Tabla As String = "SpRecalcularPlanCuentaSaldo"

        If rdbContabilidad.Checked = True Then
            Tabla = "SpRecalcularPlanCuentaSaldoCG"
        End If

        For i As Integer = 1 To 12

            Dim SQL As String = "Exec " & Tabla & " @Año='" & nudAño.Value & "', @Mes='" & i & "' "
            Dim dttemp As DataTable = CSistema.ExecuteToDataTable(SQL, "", 1000)

            If dttemp Is Nothing Then
                MessageBox.Show("Error en la consulta...", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            If dttemp.Rows.Count = 0 Then
                MessageBox.Show("Error en la consulta...", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            Dim Resultado As DataRow = dttemp.Rows(0)

            If Resultado("Procesado") = False Then
                MessageBox.Show(Resultado("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            Dim Registros As Integer = Resultado("Registros")

            cbxMes.SelectedIndex = i - 1

            Procesados = Procesados & "" & cbxMes.Text & ": (" & CSistema.FormatoNumero(Registros) & ") " & vbCrLf

        Next
        

        MessageBox.Show(Procesados, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub frmRecalcularPlanCuentaSaldo_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmRecalcularPlanCuentaSaldo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnProcesarAño_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesarAño.Click
        ProcesarAño()
    End Sub

End Class
﻿Imports System.Threading

Public Class frmRegererarAsiento

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim tProceso As Thread
    Dim tContador As Thread
    Dim vContando As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        '
        chkGenerarTodo.Checked = False

        'Form
        CheckForIllegalCrossThreadCalls = False
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Cargar informacion
        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal, CData.GetTable("VSucursal"))
        cbxSucursal.SelectedValue = vgIDSucursal

        'Documentos
        cbxDocumento.Items.Add("VENTA")
        cbxDocumento.Items.Add("NOTA DE CREDITO")
        cbxDocumento.Items.Add("NOTA DE DEBITO")
        cbxDocumento.Items.Add("COBRANZA")
        cbxDocumento.Items.Add("MOVIMIENTO")
        cbxDocumento.Items.Add("TICKET DE BASCULA")
        cbxDocumento.Items.Add("COMPRA MERCADERIAS")
        cbxDocumento.Items.Add("DEPOSITO BANCARIO")
        cbxDocumento.Items.Add("ORDEN DE PAGO")
        cbxDocumento.Items.Add("ENTREGA DE CHEQUE")
        cbxDocumento.Items.Add("VENCIMIENTO DE CHEQUE")

    End Sub

    Sub Procesar()

        tProceso = New Thread(AddressOf Generar)
        tProceso.Start()

    End Sub

    Sub ProcesarContador()

        vContando = True
        tContador = New Thread(AddressOf Contar)
        tContador.Start()

    End Sub

    Sub Contar()

        Thread.Sleep(2000)

        While vContando

            Try
                Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Top(1) * From GeneracionAsientoNotaCredito")

                If dt Is Nothing Then
                    Exit Sub
                End If

                If dt.Rows.Count = 0 Then
                    Exit Sub
                End If

                Dim oRow As DataRow = dt.Rows(0)

                Dim Cantidad As Integer = oRow("Cantidad")
                Dim RegistrosProcesados As Integer = oRow("RegistrosProcesados")
                Dim Porcentaje As Integer = (RegistrosProcesados / Cantidad) * 100

                lblPorcentaje.Text = RegistrosProcesados & " de " & Cantidad & " - " & Porcentaje & "%"
                pbPorcentaje.Value = Porcentaje

            Catch ex As Exception

            End Try

            Thread.Sleep(2000)

        End While

    End Sub

    Sub Generar()

        'Validar
        If MessageBox.Show("Esto podria tardar varios minutos! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        btnCerrar.Enabled = False
        btnGenerar.Enabled = False

        If StoreProcedure() = "" Then
            MessageBox.Show("Seleccione un documento para continuar!", "Asiento", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        If chkGenerarTodo.Checked = False Then

            ProcesarContador()

            Dim SQL As String = " Exec " & StoreProcedure()
            CSistema.ConcatenarParametro(SQL, "@IDSucursal", cbxSucursal.SelectedValue)
            CSistema.ConcatenarParametro(SQL, "@Todos", chkGenerarTodo.Checked.ToString)
            CSistema.ConcatenarParametro(SQL, "@SoloConDiferencias", chkSoloConDiferencias.Checked.ToString)
            CSistema.ConcatenarParametro(SQL, "@Desde", CSistema.FormatoFechaDesdeHastaBaseDatos(txtDesde.GetValue, True, False))
            CSistema.ConcatenarParametro(SQL, "@Hasta", CSistema.FormatoFechaDesdeHastaBaseDatos(txtHasta.GetValue, False, True))


            Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL, "", 1000)

            Dim SQL1 As String = "Insert into aregeneracionasiento (Comando,IDUsuario,IDTerminal,Fecha) Values ('" & StoreProcedure() & "'," & vgIDUsuario & "," & vgIDTerminal & ",'" & CSistema.FormatoFechaBaseDatos(VGFechaHoraSistema, True, True) & "')"
            CSistema.ExecuteNonQuery(SQL1)

            If Resultado Is Nothing Then
                Exit Sub
            End If

            If Resultado.Rows.Count = 0 Then
                Exit Sub
            End If

            vContando = False
            MessageBox.Show(Resultado.Rows(0)("Mensaje"), "Asiento", MessageBoxButtons.OK, MessageBoxIcon.Information)

            lblPorcentaje.Text = "0 de 0 - 0%"
            pbPorcentaje.Value = 0

            btnCerrar.Enabled = True
            btnGenerar.Enabled = True

        Else

            For Each oRow As DataRow In CData.GetTable("VSucursal").Rows

                cbxSucursal.SelectedValue = oRow("ID")

                ProcesarContador()

                Dim SQL As String = " Exec " & StoreProcedure()
                CSistema.ConcatenarParametro(SQL, "@IDSucursal", oRow("ID"))
                CSistema.ConcatenarParametro(SQL, "@Todos", chkGenerarTodo.Checked.ToString)
                CSistema.ConcatenarParametro(SQL, "@SoloConDiferencias", chkSoloConDiferencias.Checked.ToString)
                CSistema.ConcatenarParametro(SQL, "@Desde", CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False))
                CSistema.ConcatenarParametro(SQL, "@Hasta", CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False))

                Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL, "", 10000)

                If Resultado Is Nothing Then
                    Exit Sub
                End If

                If Resultado.Rows.Count = 0 Then
                    Exit Sub
                End If

                vContando = False
                
                lblPorcentaje.Text = "0 de 0 - 0%"
                pbPorcentaje.Value = 0

                btnCerrar.Enabled = True
                btnGenerar.Enabled = True

            Next

        End If



    End Sub

    Function StoreProcedure() As String

        StoreProcedure = ""

        Select Case cbxDocumento.SelectedIndex
            Case 0
                StoreProcedure = "SpRegenerarAsientoVenta"
            Case 1
                StoreProcedure = "SpRegenerarAsientoNotaCredito"
            Case 2
                StoreProcedure = "SpRegenerarAsientoNotaDebito"
            Case 3
                StoreProcedure = "SpRegenerarAsientoCobranza"
            Case 4
                StoreProcedure = "SpRegenerarAsientoMovimiento"
            Case 5
                StoreProcedure = "SpRegenerarAsientoTicketBascula"
            Case 6
                StoreProcedure = "SpRegenerarAsientoCompra"
            Case 7
                StoreProcedure = "SpRegenerarAsientoDepositoBancario"
            Case 8
                StoreProcedure = "SpRegenerarAsientoOrdenPago"
            Case 9
                StoreProcedure = "SpRegenerarAsientoEntregaCheque"
            Case 10
                StoreProcedure = "SpRegenerarAsientoOrdenPagoVencimientoCheque"
        End Select

    End Function

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        Procesar()
    End Sub

    Private Sub frmRegererarAsiento_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmRegererarAsiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

   
    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub chkGenerarTodo_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkGenerarTodo.CheckedChanged
        cbxSucursal.Enabled = Not chkGenerarTodo.Checked
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class
﻿Public Class frmCambioGlobalCuentaContable

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtCuentaNueva.Conectar()
        txtCuentaOriginal.Conectar()

        'Funciones
        CargarDatos()

        'Foco
        txtCuentaOriginal.txtCodigo.Focus()
        txtCuentaOriginal.txtCodigo.SelectAll()

    End Sub

    Sub CargarDatos()

        'Operaciones
        CSistema.SqlToComboBox(cbxOperacion, CData.GetTable("VOperacion").Copy, "ID", "Descripcion")

    End Sub

    Sub Procesar()

        Dim SQL As String = "Exec SpCambioGlobalCodigoCuentaContable "
        CSistema.ConcatenarParametro(SQL, "@Desde", txtDesde.GetValueString)
        CSistema.ConcatenarParametro(SQL, "@Hasta", txtHasta.GetValueString)
        CSistema.ConcatenarParametro(SQL, "@CodigoOriginal", txtCuentaOriginal.Registro("Codigo").ToString)
        CSistema.ConcatenarParametro(SQL, "@CodigoNuevo", txtCuentaNueva.Registro("Codigo").ToString)
        CSistema.ConcatenarParametro(SQL, "@IncluirConciliado", chkIncluirConciliados.Valor.ToString)

        If chkOperacion.Valor = True Then
            CSistema.ConcatenarParametro(SQL, "@IDOperacion", cbxOperacion.SelectedValue)
        End If

        Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL)

        If Resultado Is Nothing AndAlso Resultado.Rows.Count = 0 Then
            CSistema.MostrarError("Error en el sistema! No se puede procesar.", ctrError, btnProcesar, tsslEstado)
            Exit Sub
        End If

        Dim Registro As DataRow = Resultado.Rows(0)

        If Registro("Procesado") = False Then
            CSistema.MostrarError(Registro("Mensaje").ToString, ctrError, btnProcesar, tsslEstado)
            Exit Sub
        End If

        MessageBox.Show(Registro("Mensaje").ToString & " - Cantidad de registros procesados: " & Registro("Registros").ToString, "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub frmCambioGlobalCuentaContable_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
       CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmCambioGlobalCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub chkOperacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkOperacion.PropertyChanged
        cbxOperacion.Enabled = value
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRenumeracionAsiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblProgreso = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblEmpezarCon = New System.Windows.Forms.Label()
        Me.lblBase = New System.Windows.Forms.Label()
        Me.rdbContabilidad = New System.Windows.Forms.RadioButton()
        Me.rdbAdministracion = New System.Windows.Forms.RadioButton()
        Me.lblCantidadRegistro = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaHasta = New System.Windows.Forms.Label()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.pbProgreso = New System.Windows.Forms.ProgressBar()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtEmpezarCon = New ERP.ocxTXTNumeric()
        Me.txtCantidadRegistro = New ERP.ocxTXTNumeric()
        Me.txtUltimoNroConciliado = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblProgreso, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.pbProgreso, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(666, 458)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lblProgreso
        '
        Me.lblProgreso.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblProgreso.Location = New System.Drawing.Point(3, 441)
        Me.lblProgreso.Name = "lblProgreso"
        Me.lblProgreso.Size = New System.Drawing.Size(660, 17)
        Me.lblProgreso.TabIndex = 5
        Me.lblProgreso.Text = "0 de 0 - 0%"
        Me.lblProgreso.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.txtEmpezarCon)
        Me.Panel1.Controls.Add(Me.lblEmpezarCon)
        Me.Panel1.Controls.Add(Me.lblBase)
        Me.Panel1.Controls.Add(Me.rdbContabilidad)
        Me.Panel1.Controls.Add(Me.rdbAdministracion)
        Me.Panel1.Controls.Add(Me.txtCantidadRegistro)
        Me.Panel1.Controls.Add(Me.lblCantidadRegistro)
        Me.Panel1.Controls.Add(Me.txtUltimoNroConciliado)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btnProcesar)
        Me.Panel1.Controls.Add(Me.btnListar)
        Me.Panel1.Controls.Add(Me.dtpDesde)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.dtpHasta)
        Me.Panel1.Controls.Add(Me.lblFechaHasta)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(660, 71)
        Me.Panel1.TabIndex = 0
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(580, 37)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 16
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblEmpezarCon
        '
        Me.lblEmpezarCon.AutoSize = True
        Me.lblEmpezarCon.Location = New System.Drawing.Point(130, 42)
        Me.lblEmpezarCon.Name = "lblEmpezarCon"
        Me.lblEmpezarCon.Size = New System.Drawing.Size(72, 13)
        Me.lblEmpezarCon.TabIndex = 10
        Me.lblEmpezarCon.Text = "Empezar con:"
        '
        'lblBase
        '
        Me.lblBase.AutoSize = True
        Me.lblBase.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBase.Location = New System.Drawing.Point(6, 14)
        Me.lblBase.Name = "lblBase"
        Me.lblBase.Size = New System.Drawing.Size(39, 13)
        Me.lblBase.TabIndex = 0
        Me.lblBase.Text = "Base:"
        '
        'rdbContabilidad
        '
        Me.rdbContabilidad.AutoSize = True
        Me.rdbContabilidad.Checked = True
        Me.rdbContabilidad.Location = New System.Drawing.Point(143, 12)
        Me.rdbContabilidad.Name = "rdbContabilidad"
        Me.rdbContabilidad.Size = New System.Drawing.Size(83, 17)
        Me.rdbContabilidad.TabIndex = 2
        Me.rdbContabilidad.TabStop = True
        Me.rdbContabilidad.Text = "Contabilidad"
        Me.rdbContabilidad.UseVisualStyleBackColor = True
        '
        'rdbAdministracion
        '
        Me.rdbAdministracion.AutoSize = True
        Me.rdbAdministracion.Location = New System.Drawing.Point(50, 12)
        Me.rdbAdministracion.Name = "rdbAdministracion"
        Me.rdbAdministracion.Size = New System.Drawing.Size(93, 17)
        Me.rdbAdministracion.TabIndex = 1
        Me.rdbAdministracion.Text = "Administracion"
        Me.rdbAdministracion.UseVisualStyleBackColor = True
        '
        'lblCantidadRegistro
        '
        Me.lblCantidadRegistro.AutoSize = True
        Me.lblCantidadRegistro.Location = New System.Drawing.Point(259, 42)
        Me.lblCantidadRegistro.Name = "lblCantidadRegistro"
        Me.lblCantidadRegistro.Size = New System.Drawing.Size(109, 13)
        Me.lblCantidadRegistro.TabIndex = 12
        Me.lblCantidadRegistro.Text = "Cantidad de registros:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(499, 37)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 15
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Ultimo numero:"
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(418, 37)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(75, 23)
        Me.btnProcesar.TabIndex = 14
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(516, 7)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(139, 23)
        Me.btnListar.TabIndex = 7
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'dtpDesde
        '
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(275, 10)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(93, 20)
        Me.dtpDesde.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(231, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Desde:"
        '
        'dtpHasta
        '
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(417, 8)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(93, 20)
        Me.dtpHasta.TabIndex = 6
        '
        'lblFechaHasta
        '
        Me.lblFechaHasta.AutoSize = True
        Me.lblFechaHasta.Location = New System.Drawing.Point(374, 12)
        Me.lblFechaHasta.Name = "lblFechaHasta"
        Me.lblFechaHasta.Size = New System.Drawing.Size(38, 13)
        Me.lblFechaHasta.TabIndex = 5
        Me.lblFechaHasta.Text = "Hasta:"
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 80)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.Size = New System.Drawing.Size(660, 321)
        Me.dgw.TabIndex = 1
        Me.dgw.Tag = "frmRemuneracion"
        '
        'pbProgreso
        '
        Me.pbProgreso.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbProgreso.Location = New System.Drawing.Point(3, 407)
        Me.pbProgreso.Name = "pbProgreso"
        Me.pbProgreso.Size = New System.Drawing.Size(660, 31)
        Me.pbProgreso.TabIndex = 3
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'txtEmpezarCon
        '
        Me.txtEmpezarCon.Color = System.Drawing.Color.Empty
        Me.txtEmpezarCon.Decimales = True
        Me.txtEmpezarCon.Indicaciones = Nothing
        Me.txtEmpezarCon.Location = New System.Drawing.Point(202, 38)
        Me.txtEmpezarCon.Name = "txtEmpezarCon"
        Me.txtEmpezarCon.Size = New System.Drawing.Size(44, 20)
        Me.txtEmpezarCon.SoloLectura = False
        Me.txtEmpezarCon.TabIndex = 11
        Me.txtEmpezarCon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEmpezarCon.Texto = "0"
        '
        'txtCantidadRegistro
        '
        Me.txtCantidadRegistro.Color = System.Drawing.Color.Empty
        Me.txtCantidadRegistro.Decimales = True
        Me.txtCantidadRegistro.Indicaciones = Nothing
        Me.txtCantidadRegistro.Location = New System.Drawing.Point(368, 38)
        Me.txtCantidadRegistro.Name = "txtCantidadRegistro"
        Me.txtCantidadRegistro.Size = New System.Drawing.Size(44, 20)
        Me.txtCantidadRegistro.SoloLectura = True
        Me.txtCantidadRegistro.TabIndex = 13
        Me.txtCantidadRegistro.TabStop = False
        Me.txtCantidadRegistro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadRegistro.Texto = "0"
        '
        'txtUltimoNroConciliado
        '
        Me.txtUltimoNroConciliado.Color = System.Drawing.Color.Empty
        Me.txtUltimoNroConciliado.Decimales = True
        Me.txtUltimoNroConciliado.Indicaciones = Nothing
        Me.txtUltimoNroConciliado.Location = New System.Drawing.Point(86, 38)
        Me.txtUltimoNroConciliado.Name = "txtUltimoNroConciliado"
        Me.txtUltimoNroConciliado.Size = New System.Drawing.Size(44, 20)
        Me.txtUltimoNroConciliado.SoloLectura = True
        Me.txtUltimoNroConciliado.TabIndex = 9
        Me.txtUltimoNroConciliado.TabStop = False
        Me.txtUltimoNroConciliado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtUltimoNroConciliado.Texto = "0"
        '
        'frmRenumeracionAsiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(666, 458)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmRenumeracionAsiento"
        Me.Text = " "
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFechaHasta As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents txtUltimoNroConciliado As ERP.ocxTXTNumeric
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtCantidadRegistro As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidadRegistro As System.Windows.Forms.Label
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblBase As System.Windows.Forms.Label
    Friend WithEvents rdbContabilidad As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAdministracion As System.Windows.Forms.RadioButton
    Friend WithEvents pbProgreso As System.Windows.Forms.ProgressBar
    Friend WithEvents lblProgreso As System.Windows.Forms.Label
    Friend WithEvents txtEmpezarCon As ERP.ocxTXTNumeric
    Friend WithEvents lblEmpezarCon As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class

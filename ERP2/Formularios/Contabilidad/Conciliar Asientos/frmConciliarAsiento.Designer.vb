﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConciliarAsiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExportarAPlanillaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnVerDetalle = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnConciliado = New System.Windows.Forms.Button()
        Me.btnEnviarError = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkSinConciliar = New System.Windows.Forms.CheckBox()
        Me.chkOperacion = New System.Windows.Forms.CheckBox()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.lblNumero = New System.Windows.Forms.Label()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkFecha2 = New System.Windows.Forms.CheckBox()
        Me.txtHasta2 = New System.Windows.Forms.DateTimePicker()
        Me.txtDesde2 = New System.Windows.Forms.DateTimePicker()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbFechaDocumento2 = New System.Windows.Forms.RadioButton()
        Me.rdbFechaRegistro2 = New System.Windows.Forms.RadioButton()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbFechaDocumento = New System.Windows.Forms.RadioButton()
        Me.rdbFechaRegistro = New System.Windows.Forms.RadioButton()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvDetalleAsiento = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblTotalDebito = New System.Windows.Forms.Label()
        Me.lblTotalCredito = New System.Windows.Forms.Label()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.cbxOperacion = New ERP.ocxCBX()
        Me.txtComprobanteOperacion = New ERP.ocxTXTString()
        Me.cbxSucursalOperacion = New ERP.ocxCBX()
        Me.txtNumeroOperacion = New ERP.ocxTXTString()
        Me.txtTotalSinConciliar = New ERP.ocxTXTNumeric()
        Me.txtTotalConciliado = New ERP.ocxTXTNumeric()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.txtTotalDebito = New ERP.ocxTXTNumeric()
        Me.txtTotalCredito = New ERP.ocxTXTNumeric()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvDetalleAsiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportarAPlanillaToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(169, 26)
        '
        'ExportarAPlanillaToolStripMenuItem
        '
        Me.ExportarAPlanillaToolStripMenuItem.Name = "ExportarAPlanillaToolStripMenuItem"
        Me.ExportarAPlanillaToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ExportarAPlanillaToolStripMenuItem.Text = "Exportar a Planilla"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(159, 8)
        Me.Label6.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Total Conciliado:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.Controls.Add(Me.btnGuardar)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtTotalSinConciliar)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtTotalConciliado)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label6)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtTotal)
        Me.FlowLayoutPanel4.Controls.Add(Me.Label5)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(355, 383)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(652, 31)
        Me.FlowLayoutPanel4.TabIndex = 2
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(574, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 8
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(362, 8)
        Me.Label7.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Total Sin Conciliar:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 8)
        Me.Label5.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Total:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(84, 3)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 1
        Me.btnAsiento.Text = "Ver Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.btnVerDetalle)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnAsiento)
        Me.FlowLayoutPanel3.Controls.Add(Me.Panel3)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnConciliado)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnEnviarError)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 383)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(346, 31)
        Me.FlowLayoutPanel3.TabIndex = 1
        '
        'btnVerDetalle
        '
        Me.btnVerDetalle.Location = New System.Drawing.Point(3, 3)
        Me.btnVerDetalle.Name = "btnVerDetalle"
        Me.btnVerDetalle.Size = New System.Drawing.Size(75, 23)
        Me.btnVerDetalle.TabIndex = 0
        Me.btnVerDetalle.Text = "Ver Detalle"
        Me.btnVerDetalle.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Location = New System.Drawing.Point(167, 3)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(5, 3, 5, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1, 23)
        Me.Panel3.TabIndex = 4
        '
        'btnConciliado
        '
        Me.btnConciliado.Location = New System.Drawing.Point(176, 3)
        Me.btnConciliado.Name = "btnConciliado"
        Me.btnConciliado.Size = New System.Drawing.Size(75, 23)
        Me.btnConciliado.TabIndex = 3
        Me.btnConciliado.Text = "Desconciliar"
        Me.btnConciliado.UseVisualStyleBackColor = True
        '
        'btnEnviarError
        '
        Me.btnEnviarError.Location = New System.Drawing.Point(257, 3)
        Me.btnEnviarError.Name = "btnEnviarError"
        Me.btnEnviarError.Size = New System.Drawing.Size(75, 23)
        Me.btnEnviarError.TabIndex = 2
        Me.btnEnviarError.Text = "Enviar Error"
        Me.btnEnviarError.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'Panel2
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel2, 2)
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1004, 374)
        Me.Panel2.TabIndex = 1
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle20
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle21
        Me.DataGridView1.Size = New System.Drawing.Size(1004, 374)
        Me.DataGridView1.TabIndex = 5
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 15)
        '
        'StatusStrip1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.StatusStrip1, 2)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 585)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1010, 20)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 658.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.StatusStrip1, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TabControl1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 168.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1254, 605)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'TabControl1
        '
        Me.TabControl1.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(1013, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TableLayoutPanel1.SetRowSpan(Me.TabControl1, 4)
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(238, 599)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkSinConciliar)
        Me.TabPage1.Controls.Add(Me.chkOperacion)
        Me.TabPage1.Controls.Add(Me.cbxOperacion)
        Me.TabPage1.Controls.Add(Me.lblComprobante)
        Me.TabPage1.Controls.Add(Me.txtComprobanteOperacion)
        Me.TabPage1.Controls.Add(Me.chkSucursal)
        Me.TabPage1.Controls.Add(Me.cbxSucursalOperacion)
        Me.TabPage1.Controls.Add(Me.lblNumero)
        Me.TabPage1.Controls.Add(Me.btnExportar)
        Me.TabPage1.Controls.Add(Me.txtNumeroOperacion)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.chkFecha2)
        Me.TabPage1.Controls.Add(Me.txtHasta2)
        Me.TabPage1.Controls.Add(Me.txtDesde2)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel7)
        Me.TabPage1.Controls.Add(Me.dtpHasta)
        Me.TabPage1.Controls.Add(Me.dtpDesde)
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel5)
        Me.TabPage1.Controls.Add(Me.lblFecha)
        Me.TabPage1.Controls.Add(Me.btnListar)
        Me.TabPage1.Location = New System.Drawing.Point(4, 4)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(230, 573)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'chkSinConciliar
        '
        Me.chkSinConciliar.AutoSize = True
        Me.chkSinConciliar.Location = New System.Drawing.Point(4, 294)
        Me.chkSinConciliar.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkSinConciliar.Name = "chkSinConciliar"
        Me.chkSinConciliar.Size = New System.Drawing.Size(87, 17)
        Me.chkSinConciliar.TabIndex = 40
        Me.chkSinConciliar.Text = "Sin Conciliar:"
        Me.chkSinConciliar.UseVisualStyleBackColor = True
        '
        'chkOperacion
        '
        Me.chkOperacion.AutoSize = True
        Me.chkOperacion.Checked = True
        Me.chkOperacion.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOperacion.Enabled = False
        Me.chkOperacion.Location = New System.Drawing.Point(3, 156)
        Me.chkOperacion.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkOperacion.Name = "chkOperacion"
        Me.chkOperacion.Size = New System.Drawing.Size(78, 17)
        Me.chkOperacion.TabIndex = 38
        Me.chkOperacion.Text = "Operacion:"
        Me.chkOperacion.UseVisualStyleBackColor = True
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(8, 240)
        Me.lblComprobante.Margin = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 36
        Me.lblComprobante.Text = "Comprobante:"
        Me.lblComprobante.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(3, 211)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(3, 6, 3, 3)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(51, 17)
        Me.chkSucursal.TabIndex = 34
        Me.chkSucursal.Text = "Suc.:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'lblNumero
        '
        Me.lblNumero.AutoSize = True
        Me.lblNumero.Location = New System.Drawing.Point(122, 210)
        Me.lblNumero.Margin = New System.Windows.Forms.Padding(0, 6, 0, 0)
        Me.lblNumero.Name = "lblNumero"
        Me.lblNumero.Size = New System.Drawing.Size(56, 13)
        Me.lblNumero.TabIndex = 4
        Me.lblNumero.Text = "Nro. Ope.:"
        Me.lblNumero.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(125, 326)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(100, 30)
        Me.btnExportar.TabIndex = 33
        Me.btnExportar.Text = "Exportar"
        Me.btnExportar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(58, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "Hasta:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(58, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Desde:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(61, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Hasta:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(61, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Desde:"
        '
        'chkFecha2
        '
        Me.chkFecha2.AutoSize = True
        Me.chkFecha2.Location = New System.Drawing.Point(4, 84)
        Me.chkFecha2.Name = "chkFecha2"
        Me.chkFecha2.Size = New System.Drawing.Size(56, 17)
        Me.chkFecha2.TabIndex = 28
        Me.chkFecha2.Text = "2 Fec:"
        Me.chkFecha2.UseVisualStyleBackColor = True
        '
        'txtHasta2
        '
        Me.txtHasta2.Enabled = False
        Me.txtHasta2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHasta2.Location = New System.Drawing.Point(121, 128)
        Me.txtHasta2.Name = "txtHasta2"
        Me.txtHasta2.Size = New System.Drawing.Size(103, 20)
        Me.txtHasta2.TabIndex = 27
        '
        'txtDesde2
        '
        Me.txtDesde2.Enabled = False
        Me.txtDesde2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesde2.Location = New System.Drawing.Point(121, 107)
        Me.txtDesde2.Name = "txtDesde2"
        Me.txtDesde2.Size = New System.Drawing.Size(103, 20)
        Me.txtDesde2.TabIndex = 26
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel7.Controls.Add(Me.rdbFechaDocumento2)
        Me.FlowLayoutPanel7.Controls.Add(Me.rdbFechaRegistro2)
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(60, 82)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(117, 20)
        Me.FlowLayoutPanel7.TabIndex = 25
        '
        'rdbFechaDocumento2
        '
        Me.rdbFechaDocumento2.AutoSize = True
        Me.rdbFechaDocumento2.Checked = True
        Me.rdbFechaDocumento2.Enabled = False
        Me.rdbFechaDocumento2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaDocumento2.Location = New System.Drawing.Point(2, 1)
        Me.rdbFechaDocumento2.Margin = New System.Windows.Forms.Padding(2, 1, 3, 3)
        Me.rdbFechaDocumento2.Name = "rdbFechaDocumento2"
        Me.rdbFechaDocumento2.Size = New System.Drawing.Size(56, 16)
        Me.rdbFechaDocumento2.TabIndex = 0
        Me.rdbFechaDocumento2.TabStop = True
        Me.rdbFechaDocumento2.Text = "Docum."
        Me.rdbFechaDocumento2.UseVisualStyleBackColor = True
        '
        'rdbFechaRegistro2
        '
        Me.rdbFechaRegistro2.AutoSize = True
        Me.rdbFechaRegistro2.Enabled = False
        Me.rdbFechaRegistro2.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaRegistro2.Location = New System.Drawing.Point(62, 1)
        Me.rdbFechaRegistro2.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbFechaRegistro2.Name = "rdbFechaRegistro2"
        Me.rdbFechaRegistro2.Size = New System.Drawing.Size(43, 16)
        Me.rdbFechaRegistro2.TabIndex = 1
        Me.rdbFechaRegistro2.Text = "Reg."
        Me.rdbFechaRegistro2.UseVisualStyleBackColor = True
        '
        'dtpHasta
        '
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(120, 53)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(105, 20)
        Me.dtpHasta.TabIndex = 3
        '
        'dtpDesde
        '
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(120, 30)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(105, 20)
        Me.dtpDesde.TabIndex = 2
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel5.Controls.Add(Me.rdbFechaDocumento)
        Me.FlowLayoutPanel5.Controls.Add(Me.rdbFechaRegistro)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(61, 5)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(117, 20)
        Me.FlowLayoutPanel5.TabIndex = 1
        '
        'rdbFechaDocumento
        '
        Me.rdbFechaDocumento.AutoSize = True
        Me.rdbFechaDocumento.Checked = True
        Me.rdbFechaDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaDocumento.Location = New System.Drawing.Point(2, 1)
        Me.rdbFechaDocumento.Margin = New System.Windows.Forms.Padding(2, 1, 3, 3)
        Me.rdbFechaDocumento.Name = "rdbFechaDocumento"
        Me.rdbFechaDocumento.Size = New System.Drawing.Size(56, 16)
        Me.rdbFechaDocumento.TabIndex = 0
        Me.rdbFechaDocumento.TabStop = True
        Me.rdbFechaDocumento.Text = "Docum."
        Me.rdbFechaDocumento.UseVisualStyleBackColor = True
        '
        'rdbFechaRegistro
        '
        Me.rdbFechaRegistro.AutoSize = True
        Me.rdbFechaRegistro.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbFechaRegistro.Location = New System.Drawing.Point(62, 1)
        Me.rdbFechaRegistro.Margin = New System.Windows.Forms.Padding(1, 1, 3, 3)
        Me.rdbFechaRegistro.Name = "rdbFechaRegistro"
        Me.rdbFechaRegistro.Size = New System.Drawing.Size(43, 16)
        Me.rdbFechaRegistro.TabIndex = 1
        Me.rdbFechaRegistro.Text = "Reg."
        Me.rdbFechaRegistro.UseVisualStyleBackColor = True
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(6, 8)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(55, 13)
        Me.lblFecha.TabIndex = 0
        Me.lblFecha.Text = "Fecha de:"
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(7, 326)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(112, 30)
        Me.btnListar.TabIndex = 4
        Me.btnListar.Text = "Listar"
        Me.btnListar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.dgvDetalleAsiento)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 420)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1004, 162)
        Me.Panel1.TabIndex = 6
        '
        'dgvDetalleAsiento
        '
        Me.dgvDetalleAsiento.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleAsiento.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.dgvDetalleAsiento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalleAsiento.DefaultCellStyle = DataGridViewCellStyle23
        Me.dgvDetalleAsiento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDetalleAsiento.Location = New System.Drawing.Point(0, 0)
        Me.dgvDetalleAsiento.Name = "dgvDetalleAsiento"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleAsiento.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.dgvDetalleAsiento.Size = New System.Drawing.Size(1004, 131)
        Me.dgvDetalleAsiento.TabIndex = 4
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 286.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.FlowLayoutPanel2, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.FlowLayoutPanel6, 1, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 131)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(1004, 31)
        Me.TableLayoutPanel4.TabIndex = 3
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadDetalle)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(280, 25)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(3, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(52, 25)
        Me.lblCantidadDetalle.TabIndex = 0
        Me.lblCantidadDetalle.Text = "Cantidad:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.Controls.Add(Me.lblTotalDebito)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtTotalDebito)
        Me.FlowLayoutPanel6.Controls.Add(Me.lblTotalCredito)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtTotalCredito)
        Me.FlowLayoutPanel6.Controls.Add(Me.lblSaldo)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtSaldo)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(289, 3)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(712, 25)
        Me.FlowLayoutPanel6.TabIndex = 0
        '
        'lblTotalDebito
        '
        Me.lblTotalDebito.Location = New System.Drawing.Point(3, 0)
        Me.lblTotalDebito.Name = "lblTotalDebito"
        Me.lblTotalDebito.Size = New System.Drawing.Size(50, 25)
        Me.lblTotalDebito.TabIndex = 0
        Me.lblTotalDebito.Text = "Debito:"
        Me.lblTotalDebito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalCredito
        '
        Me.lblTotalCredito.Location = New System.Drawing.Point(160, 0)
        Me.lblTotalCredito.Name = "lblTotalCredito"
        Me.lblTotalCredito.Size = New System.Drawing.Size(57, 25)
        Me.lblTotalCredito.TabIndex = 5
        Me.lblTotalCredito.Text = "Credito:"
        Me.lblTotalCredito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSaldo
        '
        Me.lblSaldo.Location = New System.Drawing.Point(324, 0)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(57, 25)
        Me.lblSaldo.TabIndex = 7
        Me.lblSaldo.Text = "Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbxOperacion
        '
        Me.cbxOperacion.CampoWhere = "Tipo"
        Me.cbxOperacion.CargarUnaSolaVez = False
        Me.cbxOperacion.DataDisplayMember = "Tipo"
        Me.cbxOperacion.DataFilter = Nothing
        Me.cbxOperacion.DataOrderBy = Nothing
        Me.cbxOperacion.DataSource = ""
        Me.cbxOperacion.DataValueMember = "Tipo"
        Me.cbxOperacion.dtSeleccionado = Nothing
        Me.cbxOperacion.FormABM = Nothing
        Me.cbxOperacion.Indicaciones = Nothing
        Me.cbxOperacion.Location = New System.Drawing.Point(9, 179)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.SeleccionMultiple = False
        Me.cbxOperacion.SeleccionObligatoria = False
        Me.cbxOperacion.Size = New System.Drawing.Size(218, 23)
        Me.cbxOperacion.SoloLectura = False
        Me.cbxOperacion.TabIndex = 39
        Me.cbxOperacion.Texto = ""
        '
        'txtComprobanteOperacion
        '
        Me.txtComprobanteOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteOperacion.Color = System.Drawing.Color.Empty
        Me.txtComprobanteOperacion.Enabled = False
        Me.txtComprobanteOperacion.Indicaciones = Nothing
        Me.txtComprobanteOperacion.Location = New System.Drawing.Point(84, 235)
        Me.txtComprobanteOperacion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtComprobanteOperacion.Multilinea = False
        Me.txtComprobanteOperacion.Name = "txtComprobanteOperacion"
        Me.txtComprobanteOperacion.Size = New System.Drawing.Size(140, 20)
        Me.txtComprobanteOperacion.SoloLectura = False
        Me.txtComprobanteOperacion.TabIndex = 37
        Me.txtComprobanteOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteOperacion.Texto = ""
        '
        'cbxSucursalOperacion
        '
        Me.cbxSucursalOperacion.CampoWhere = "IDSucursal"
        Me.cbxSucursalOperacion.CargarUnaSolaVez = False
        Me.cbxSucursalOperacion.DataDisplayMember = "Codigo"
        Me.cbxSucursalOperacion.DataFilter = Nothing
        Me.cbxSucursalOperacion.DataOrderBy = Nothing
        Me.cbxSucursalOperacion.DataSource = "Sucursal"
        Me.cbxSucursalOperacion.DataValueMember = "ID"
        Me.cbxSucursalOperacion.dtSeleccionado = Nothing
        Me.cbxSucursalOperacion.Enabled = False
        Me.cbxSucursalOperacion.FormABM = Nothing
        Me.cbxSucursalOperacion.Indicaciones = Nothing
        Me.cbxSucursalOperacion.Location = New System.Drawing.Point(60, 208)
        Me.cbxSucursalOperacion.Name = "cbxSucursalOperacion"
        Me.cbxSucursalOperacion.SeleccionMultiple = False
        Me.cbxSucursalOperacion.SeleccionObligatoria = False
        Me.cbxSucursalOperacion.Size = New System.Drawing.Size(54, 23)
        Me.cbxSucursalOperacion.SoloLectura = False
        Me.cbxSucursalOperacion.TabIndex = 35
        Me.cbxSucursalOperacion.Texto = ""
        '
        'txtNumeroOperacion
        '
        Me.txtNumeroOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroOperacion.Color = System.Drawing.Color.Empty
        Me.txtNumeroOperacion.Enabled = False
        Me.txtNumeroOperacion.Indicaciones = Nothing
        Me.txtNumeroOperacion.Location = New System.Drawing.Point(181, 208)
        Me.txtNumeroOperacion.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtNumeroOperacion.Multilinea = False
        Me.txtNumeroOperacion.Name = "txtNumeroOperacion"
        Me.txtNumeroOperacion.Size = New System.Drawing.Size(43, 20)
        Me.txtNumeroOperacion.SoloLectura = False
        Me.txtNumeroOperacion.TabIndex = 5
        Me.txtNumeroOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNumeroOperacion.Texto = ""
        '
        'txtTotalSinConciliar
        '
        Me.txtTotalSinConciliar.Color = System.Drawing.Color.Empty
        Me.txtTotalSinConciliar.Decimales = True
        Me.txtTotalSinConciliar.Indicaciones = Nothing
        Me.txtTotalSinConciliar.Location = New System.Drawing.Point(463, 5)
        Me.txtTotalSinConciliar.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtTotalSinConciliar.Name = "txtTotalSinConciliar"
        Me.txtTotalSinConciliar.Size = New System.Drawing.Size(105, 22)
        Me.txtTotalSinConciliar.SoloLectura = False
        Me.txtTotalSinConciliar.TabIndex = 7
        Me.txtTotalSinConciliar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSinConciliar.Texto = "0"
        '
        'txtTotalConciliado
        '
        Me.txtTotalConciliado.Color = System.Drawing.Color.Empty
        Me.txtTotalConciliado.Decimales = True
        Me.txtTotalConciliado.Indicaciones = Nothing
        Me.txtTotalConciliado.Location = New System.Drawing.Point(251, 5)
        Me.txtTotalConciliado.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtTotalConciliado.Name = "txtTotalConciliado"
        Me.txtTotalConciliado.Size = New System.Drawing.Size(105, 22)
        Me.txtTotalConciliado.SoloLectura = False
        Me.txtTotalConciliado.TabIndex = 5
        Me.txtTotalConciliado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalConciliado.Texto = "0"
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Indicaciones = Nothing
        Me.txtTotal.Location = New System.Drawing.Point(48, 5)
        Me.txtTotal.Margin = New System.Windows.Forms.Padding(3, 5, 3, 3)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(105, 22)
        Me.txtTotal.SoloLectura = False
        Me.txtTotal.TabIndex = 3
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(61, 3)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(48, 22)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 4
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'txtTotalDebito
        '
        Me.txtTotalDebito.Color = System.Drawing.Color.Empty
        Me.txtTotalDebito.Decimales = True
        Me.txtTotalDebito.Indicaciones = Nothing
        Me.txtTotalDebito.Location = New System.Drawing.Point(59, 3)
        Me.txtTotalDebito.Name = "txtTotalDebito"
        Me.txtTotalDebito.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalDebito.SoloLectura = True
        Me.txtTotalDebito.TabIndex = 4
        Me.txtTotalDebito.TabStop = False
        Me.txtTotalDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebito.Texto = "0"
        '
        'txtTotalCredito
        '
        Me.txtTotalCredito.Color = System.Drawing.Color.Empty
        Me.txtTotalCredito.Decimales = True
        Me.txtTotalCredito.Indicaciones = Nothing
        Me.txtTotalCredito.Location = New System.Drawing.Point(223, 3)
        Me.txtTotalCredito.Name = "txtTotalCredito"
        Me.txtTotalCredito.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalCredito.SoloLectura = True
        Me.txtTotalCredito.TabIndex = 6
        Me.txtTotalCredito.TabStop = False
        Me.txtTotalCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCredito.Texto = "0"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(387, 3)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(95, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 8
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'frmConciliarAsiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1254, 605)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConciliarAsiento"
        Me.Text = "frmConciliarAsiento"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.FlowLayoutPanel7.PerformLayout()
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvDetalleAsiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ExportarAPlanillaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents btnVerDetalle As Button
    Friend WithEvents btnAsiento As Button
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents txtTotalConciliado As ocxTXTNumeric
    Friend WithEvents Label6 As Label
    Friend WithEvents txtTotal As ocxTXTNumeric
    Friend WithEvents Label5 As Label
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents Panel1 As Panel
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents lblCantidadDetalle As Label
    Friend WithEvents txtCantidadDetalle As ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents lblTotalDebito As Label
    Friend WithEvents txtTotalDebito As ocxTXTNumeric
    Friend WithEvents lblTotalCredito As Label
    Friend WithEvents txtTotalCredito As ocxTXTNumeric
    Friend WithEvents lblSaldo As Label
    Friend WithEvents txtSaldo As ocxTXTNumeric
    Friend WithEvents dgvDetalleAsiento As DataGridView
    Friend WithEvents txtTotalSinConciliar As ocxTXTNumeric
    Friend WithEvents Label7 As Label
    Friend WithEvents btnGuardar As Button
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents chkOperacion As CheckBox
    Friend WithEvents cbxOperacion As ocxCBX
    Friend WithEvents lblComprobante As Label
    Friend WithEvents txtComprobanteOperacion As ocxTXTString
    Friend WithEvents chkSucursal As CheckBox
    Friend WithEvents cbxSucursalOperacion As ocxCBX
    Friend WithEvents lblNumero As Label
    Friend WithEvents btnExportar As Button
    Friend WithEvents txtNumeroOperacion As ocxTXTString
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents chkFecha2 As CheckBox
    Friend WithEvents txtHasta2 As DateTimePicker
    Friend WithEvents txtDesde2 As DateTimePicker
    Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
    Friend WithEvents rdbFechaDocumento2 As RadioButton
    Friend WithEvents rdbFechaRegistro2 As RadioButton
    Friend WithEvents dtpHasta As DateTimePicker
    Friend WithEvents dtpDesde As DateTimePicker
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents rdbFechaDocumento As RadioButton
    Friend WithEvents rdbFechaRegistro As RadioButton
    Friend WithEvents lblFecha As Label
    Friend WithEvents btnListar As Button
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents btnEnviarError As Button
    Friend WithEvents btnConciliado As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents chkSinConciliar As CheckBox
End Class

﻿Public Class frmConciliarAsiento

    Dim CSistema As New CSistema
    Dim dt As DataTable
    Dim IDTransaccion As Integer

    Sub Inicializar()


        'Listar()
        cbxOperacion.cbx.Items.Add("VENTA")
        'cbxOperacion.cbx.Items.Add("NOTA DEBITO CLIENTE")
        cbxOperacion.cbx.Items.Add("NOTA CREDITO CLIENTE")
        cbxOperacion.cbx.Items.Add("COBRANZA CONTADO")
        cbxOperacion.cbx.Items.Add("COBRANZA CREDITO")

        cbxOperacion.cbx.Items.Add("COMPRA")
        cbxOperacion.cbx.Items.Add("GASTO")
        cbxOperacion.cbx.Items.Add("GASTO FONDO FIJO")
        'cbxOperacion.cbx.Items.Add("NOTA DEBITO PROVEEDOR")
        cbxOperacion.cbx.Items.Add("NOTA CREDITO PROVEEDOR")
        cbxOperacion.cbx.Items.Add("DEPOSITO BANCARIO")
        cbxOperacion.cbx.Items.Add("DESCUENTO DE CHEQUE DIFERIDO")
        cbxOperacion.cbx.Items.Add("DEBITO/CREDITO BANCARIO")
        cbxOperacion.cbx.Items.Add("ORDEN DE PAGO")
        cbxOperacion.cbx.Items.Add("VENCIMIENTO DE CHEQUE EMITIDO")
        cbxOperacion.cbx.Items.Add("MOVIMIENTO")
        'cbxOperacion.cbx.Items.Add("AJUSTE")
        'cbxOperacion.cbx.Items.Add("* ASIENTOS IMPORTADOS")
        cbxOperacion.cbx.Items.Add("ASIENTOS MANUALES")
        cbxOperacion.cbx.Items.Add("AJUSTE DE INVENTARIO")
        cbxOperacion.cbx.Items.Add("COMPROBANTES DE LIBRO IVA")
        CSistema.SqlToComboBox(cbxSucursalOperacion, "Select ID, Codigo from Sucursal")
        CalcularTotales()

    End Sub

    Sub Listar()
        Dim Consulta As String = "Select *,'YaConciliado'=Conciliado From VLibroDiarioAgrupado "
        Dim Where As String = " Where 0=0 "
        Dim OrderBy As String = " Order By Operacion "


        Dim TipoFecha As String = ""

        If rdbFechaDocumento.Checked Then TipoFecha = "Fecha"
        If rdbFechaRegistro.Checked Then TipoFecha = "FechaTransaccion"

        If dtpDesde.Text = dtpHasta.Text Then
            Where = Where & " And cast(" & TipoFecha & " as date)= cast('" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' as date)"
        Else
            Where = Where & " And cast(" & TipoFecha & " as date) Between cast('" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "'  as date) And cast('" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' as date) "
        End If

        If chkSinConciliar.Checked = True Then
            Where = Where & "and Conciliado = 'False'"
        End If

        If chkFecha2.Checked Then

            If rdbFechaDocumento2.Checked Then TipoFecha = "Fecha"
            If rdbFechaRegistro2.Checked Then TipoFecha = "FechaTransaccion"
            If txtDesde2.Text = txtHasta2.Text Then
                Where = Where & " And cast(" & TipoFecha & " as date)= cast('" & CSistema.FormatoFechaBaseDatos(txtDesde2, True, False) & "' as date)"
            Else
                Where = Where & " And cast(" & TipoFecha & " as date) Between cast('" & CSistema.FormatoFechaBaseDatos(txtDesde2, True, False) & "' as date) And cast('" & CSistema.FormatoFechaBaseDatos(txtHasta2, True, False) & "' as date)"
            End If

        End If

        If cbxOperacion.Enabled And cbxOperacion.cbx.Text <> "" Then
            Where = Where & " and Tipo = '" & cbxOperacion.cbx.Text & "' "
        End If
        If chkSucursal.Checked Then
            Where = Where & " and IDSucursal = " & cbxSucursalOperacion.cbx.SelectedValue
        End If

        If txtNumeroOperacion.txt.Text <> "" Then
            Where = Where & " and Operacion like '%" & txtNumeroOperacion.txt.Text & "%' "
        End If

        If txtComprobanteOperacion.txt.Text <> "" Then
            Where = Where & " and NroComprobante like '%" & txtComprobanteOperacion.txt.Text & "%' "
        End If

        dt = CSistema.ExecuteToDataTable(Consulta & Where & OrderBy, "", 1000)

        CSistema.dtToGrid(DataGridView1, dt)

        CSistema.DataGridColumnasVisibles(DataGridView1, {"Conciliado", "FechaConciliado", "UsuarioConciliacion", "Sucursal", "Tipo", "Operacion", "Fecha", "NroComprobante", "Observacion", "Importe", "FechaTransaccion", "Usuario"})
        CSistema.DataGridColumnasNumericas(DataGridView1, {"Importe"})
        DataGridView1.Columns("FechaConciliado").Width = 10


        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = 0
        Dim TotalConciliado As Decimal = 0
        Dim TotalSinConciliar As Decimal = 0

        Total = CSistema.gridSumColumn(DataGridView1, "Importe")
        TotalConciliado = CSistema.gridSumColumn(DataGridView1, "Importe", "Conciliado", "True")
        TotalSinConciliar = CSistema.gridSumColumn(DataGridView1, "Importe", "Conciliado", "False")

        txtTotal.txt.Text = CSistema.FormatoMoneda(Total, False)
        txtTotalConciliado.txt.Text = CSistema.FormatoMoneda(TotalConciliado, False)
        txtTotalSinConciliar.txt.Text = CSistema.FormatoMoneda(TotalSinConciliar, False)


    End Sub

    Sub Guardar()

        Dim IDTransaccionGuardar As Integer
        Dim conciliado As Boolean

        Dim dv As DataView
        dv = New DataView(DataGridView1.DataSource, "Conciliado = 'True' ", "Conciliado Desc", DataViewRowState.CurrentRows)

        Dim gridfiltrado As New DataGridView

        gridfiltrado.DataSource = dv
        CSistema.dtToGrid(gridfiltrado, dv.ToTable())
        Dim dtfiltrado As DataTable = gridfiltrado.DataSource

        'For Each item As DataGridViewRow In gridfiltrado.Rows
        For i = 0 To dtfiltrado.Rows.Count - 1


            conciliado = dtfiltrado.Rows(i)("Conciliado")
            If conciliado = False Then
                GoTo Seguir
            End If
            IDTransaccionGuardar = dtfiltrado.Rows(i)("IDTransaccion")

            If dtfiltrado.Rows(i)("YaConciliado") = True Then
                GoTo Seguir
            End If

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer = 0

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccionGuardar, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Conciliado", conciliado, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Transaccion
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpConciliarAsiento", False, True, MensajeRetorno) = False Then
                CSistema.MostrarError("Atencion: " & MensajeRetorno, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)

            End If
Seguir:

        Next

        Listar()

    End Sub

    Sub Desconciliar()

        Dim Transaccion As Integer
        Dim Conciliado As Boolean

        If DataGridView1.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione correctamente un registro!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        Transaccion = CInt(DataGridView1.CurrentRow.Cells("IDTransaccion").Value)
        Conciliado = CBool(DataGridView1.CurrentRow.Cells("Conciliado").Value)

        If Conciliado = False Then
            MessageBox.Show("El asiento no esta conciliado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        Conciliado = "False"
        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        'Entrada
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Conciliado", conciliado, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", "DESCONCILIAR", ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpConciliarAsiento", False, True, MensajeRetorno) = False Then
            CSistema.MostrarError("Atencion: " & MensajeRetorno, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)

            Exit Sub
        End If

        Listar()


    End Sub

    'Listar Detalle
    Sub ListarDetalle()

        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If
        ctrError.Clear()

        dgvDetalleAsiento.DataSource = Nothing

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(DataGridView1, Mensaje)
            ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(DataGridView1.CurrentRow.Cells("IDTransaccion").Value)
        Dim Consulta As String = "Select Codigo, Descripcion, Debito, Credito, Observacion, Moneda From VDetalleAsiento Where IDTransaccion=" & IDTransaccion & " Order By Orden"
        Dim dt As DataTable = CSistema.ExecuteToDataTable(Consulta)
        dgvDetalleAsiento.DataSource = dt

        'Formato
        dgvDetalleAsiento.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvDetalleAsiento.RowHeadersVisible = False
        dgvDetalleAsiento.BackgroundColor = Color.White

        dgvDetalleAsiento.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgvDetalleAsiento.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvDetalleAsiento.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgvDetalleAsiento.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgvDetalleAsiento.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvDetalleAsiento.Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        dgvDetalleAsiento.Columns(2).DefaultCellStyle.Format = "N0"
        dgvDetalleAsiento.Columns(3).DefaultCellStyle.Format = "N0"

        txtCantidadDetalle.txt.Text = dgvDetalleAsiento.Rows.Count
        txtTotalDebito.txt.Text = CSistema.dtSumColumn(dt, "Debito")
        txtTotalCredito.txt.Text = CSistema.dtSumColumn(dt, "Credito")

        'CSistema.TotalesGrid(dgvDetalleAsiento, txtTotalCredito.txt, 3)
        'CSistema.TotalesGrid(dgvDetalleAsiento, txtTotalDebito.txt, 2)

        txtSaldo.SetValue(CStr(txtTotalCredito.ObtenerValor - txtTotalDebito.ObtenerValor))

    End Sub

    Sub VerDetalle()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnVerDetalle, mensaje)
            ctrError.SetIconAlignment(btnVerDetalle, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDTransaccion As Integer
        IDTransaccion = DataGridView1.SelectedRows(0).Cells("IDTransaccion").Value

        Dim IDSucursalOperacion As Integer
        IDSucursalOperacion = DataGridView1.SelectedRows(0).Cells("IDSucursal").Value

        'If DataGridView1.SelectedRows(0).Cells("Tipo").Value = "COMPRA" Then
        '    Dim frmCompra As New frmCompra
        '    frmCompra.IDTransaccion = IDTransaccion
        '    frmCompra.ShowDialog()
        'ElseIf DataGridView1.SelectedRows(0).Cells("Tipo").Value = "GASTO" Then
        '    Dim frmCompra As New frmGastos
        '    frmCompra.IDTransaccion = IDTransaccion
        '    frmCompra.ShowDialog()
        'ElseIf DataGridView1.SelectedRows(0).Cells("Tipo").Value = "GASTO FONDO FIJO" Then
        '    Dim frmCompra As New frmGastos
        '    frmCompra.CajaChica = True
        '    frmCompra.IDTransaccion = IDTransaccion
        '    frmCompra.ShowDialog()
        'End If


        Select Case DataGridView1.SelectedRows(0).Cells("Tipo").Value
            Case "COMPRA"
                Dim frmCompra As New frmCompraContabilidad
                frmCompra.IDTransaccion = IDTransaccion
                frmCompra.ShowDialog()
            Case "GASTO"
                Dim frmCompra As New frmGastoContabilidad
                'frmCompra.IDSucursal = IDSucursalOperacion
                frmCompra.IDTransaccion = IDTransaccion
                frmCompra.ShowDialog()
            Case "GASTO FONDO FIJO"
                Dim frmCompra As New frmGastoContabilidad
                ' frmCompra.IDSucursal = IDSucursalOperacion
                frmCompra.CajaChica = True
                frmCompra.IDTransaccion = IDTransaccion
                frmCompra.ShowDialog()

            Case "COBRANZA CONTADO"
                Dim frmCompra As New frmCobranzaContado
                frmCompra.IDTransaccion = IDTransaccion
                frmCompra.Show()
                frmCompra.cbxSucursal.SelectedValue(IDSucursalOperacion)
                frmCompra.CargarOperacion(IDTransaccion)

            Case "COBRANZA CREDITO"
                Dim frmCompra As New frmCobranzaCredito
                frmCompra.IDTransaccion = IDTransaccion
                frmCompra.Show()
                frmCompra.cbxSucursal.SelectedValue(IDSucursalOperacion)
                frmCompra.CargarOperacion(IDTransaccion)

            Case "DEBITO/CREDITO BANCARIO"
                Dim frmCompra As New frmDebitoCreditoBancario
                frmCompra.IDTransaccion = IDTransaccion
                frmCompra.Show()
                frmCompra.Inicializar()
                frmCompra.cbxSucursal.SelectedValue(IDSucursalOperacion)
                frmCompra.CargarOperacion(IDTransaccion)
        End Select

    End Sub

    Sub VerAsiento()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            ctrError.SetError(btnAsiento, mensaje)
            ctrError.SetIconAlignment(btnAsiento, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim Tipo As String
        Dim Comprobante As String
        Dim IDTransaccion As Integer

        IDTransaccion = DataGridView1.SelectedRows(0).Cells("IDTransaccion").Value
        Comprobante = DataGridView1.SelectedRows(0).Cells("NroComprobante").Value
        Tipo = DataGridView1.SelectedRows(0).Cells("Tipo").Value

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.Text = Tipo & ": " & Comprobante
        frm.IDTransaccion = IDTransaccion

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Sub EnviarError()

        If DataGridView1.SelectedRows.Count = 0 Then
            Exit Sub
        End If
        ctrError.Clear()

        dgvDetalleAsiento.DataSource = Nothing

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(DataGridView1, Mensaje)
            ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(DataGridView1.CurrentRow.Cells("IDTransaccion").Value)


        'Dim CorreoUsuarioRegistro As String = CSistema.ExecuteScalar("Select U.mail from Usuario U join Transaccion T on U.ID = T.IDUsuario where T.ID = " & IDTransaccion)
        Dim rowUsuarioRegistro As DataRow = CSistema.ExecuteToDataTable("Select u.* from Usuario U join Transaccion T on U.ID = T.IDUsuario where T.ID = " & IDTransaccion)(0)

        Dim frm As New frmEnviarCorreo
        frm.Destinatario = rowUsuarioRegistro("Email")
        frm.CC = CSistema.ExecuteScalar("select dbo.FCorreoCCConciliacionContable(" & rowUsuarioRegistro("id") & ")")
        frm.ShowDialog()


    End Sub

    Private Sub frmConciliarAsiento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        CSistema.dtToExcel2(dt, "Comprobantes")
    End Sub

    Private Sub chkSucursal_CheckedChanged(sender As Object, e As EventArgs) Handles chkSucursal.CheckedChanged
        cbxSucursalOperacion.Enabled = chkSucursal.Checked
        txtNumeroOperacion.Enabled = chkSucursal.Checked
        txtComprobanteOperacion.Enabled = chkSucursal.Checked
    End Sub
    Private Sub btnVerDetalle_Click(sender As System.Object, e As System.EventArgs) Handles btnVerDetalle.Click
        VerDetalle()
    End Sub

    Private Sub btnAsiento_Click(sender As System.Object, e As System.EventArgs) Handles btnAsiento.Click
        VerAsiento()
    End Sub
    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        ListarDetalle()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub chkFecha2_CheckedChanged(sender As Object, e As EventArgs) Handles chkFecha2.CheckedChanged
        rdbFechaDocumento2.Enabled = chkFecha2.Checked
        rdbFechaRegistro2.Enabled = chkFecha2.Checked
        txtDesde2.Enabled = chkFecha2.Checked
        txtHasta2.Enabled = chkFecha2.Checked
    End Sub

    Private Sub btnEnviarError_Click(sender As Object, e As EventArgs) Handles btnEnviarError.Click
        EnviarError()
    End Sub

    Private Sub btnConciliado_Click(sender As Object, e As EventArgs) Handles btnConciliado.Click
        Desconciliar()
    End Sub

    'Private Sub DataGridView1_SelectionChanged_1(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
    '    ListarDetalle()
    'End Sub

    'FA 28/05/2021
    Sub frmConciliarAsiento_Activate()
        Me.Refresh()
    End Sub

End Class
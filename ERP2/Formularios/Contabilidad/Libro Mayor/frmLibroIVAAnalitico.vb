﻿Imports ERP.Reporte

Public Class frmLibroIVAAnalitico

    'CLASES
    Dim CSistema As New CSistema
    Dim CReporte As New CReporteContabilidad

    'PROPIEDADES
    Private dtCuentaValue As DataTable
    Public Property dtCuenta() As DataTable
        Get
            Return dtCuentaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaValue = value
        End Set
    End Property

    Private dtAsientoValue As DataTable
    Public Property dtAsiento() As DataTable
        Get
            Return dtAsientoValue
        End Get
        Set(ByVal value As DataTable)
            dtAsientoValue = value
        End Set
    End Property

    Private TituloValue As String
    Public Property Titulo() As String
        Get
            Return TituloValue
        End Get
        Set(ByVal value As String)
            TituloValue = value
        End Set
    End Property

    Private FechaDesdeValue As String
    Public Property FechaDesde() As String
        Get
            Return FechaDesdeValue
        End Get
        Set(ByVal value As String)
            FechaDesdeValue = value
        End Set
    End Property

    Private FechaHastaValue As String
    Public Property FechaHasta() As String
        Get
            Return FechaHastaValue
        End Get
        Set(ByVal value As String)
            FechaHastaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private SucursalValue As String
    Public Property Sucursal() As String
        Get
            Return SucursalValue
        End Get
        Set(ByVal value As String)
            SucursalValue = value
        End Set
    End Property

    Private IDTipoComprobanteValue As Integer
    Public Property IDTipoComprobante() As Integer
        Get
            Return IDTipoComprobanteValue
        End Get
        Set(ByVal value As Integer)
            IDTipoComprobanteValue = value
        End Set
    End Property

    Private TipoComprobanteValue As String
    Public Property TipoComprobante() As String
        Get
            Return TipoComprobanteValue
        End Get
        Set(ByVal value As String)
            TipoComprobanteValue = value
        End Set
    End Property

    Private TipoCuentaValue As Integer
    Public Property TipoCuenta() As Integer
        Get
            Return TipoCuentaValue
        End Get
        Set(ByVal value As Integer)
            TipoCuentaValue = value
        End Set
    End Property

    Private ResumidoValue As Boolean
    Public Property Resumido() As Boolean
        Get
            Return ResumidoValue
        End Get
        Set(ByVal value As Boolean)
            ResumidoValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Titulo
        lblTitulo.Text = Titulo

        'Funciones
        ListarCuentas()

        'Foco
        dgvCuenta.Focus()
        chkVistaMoneda.Valor = True
    End Sub

    Sub ListarCuentas()

        'Listar
        If dtCuenta Is Nothing Then
            Exit Sub
        End If

        CSistema.dtToGrid(dgvCuenta, dtCuenta)

        'Formato
        dgvCuenta.SelectionMode = DataGridViewSelectionMode.CellSelect

        dgvCuenta.Columns(0).Visible = False
        dgvCuenta.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvCuenta.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvCuenta.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvCuenta.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvCuenta.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgvCuenta.Columns(3).DefaultCellStyle.Format = "N0"
        dgvCuenta.Columns(4).DefaultCellStyle.Format = "N0"
        dgvCuenta.Columns(5).DefaultCellStyle.Format = "N0"
        dgvCuenta.Columns(6).DefaultCellStyle.Format = "N0"

    End Sub

    Sub ListarAsientos()

        If dgvCuenta.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim SQL As String = "Select IDTransaccion, Fecha, Asiento, 'Suc'=CodSucursal, Operacion, Comprobante, Debito, Credito, 'Saldo'=Convert(money,0), Concepto, TipoCuenta, cast(FechaTransaccion as date)  as FechaTransaccion,Usuario From VLibroMayor " & Where() & " Order By Asiento"
        dtAsiento = CSistema.ExecuteToDataTable(SQL)

        CalcularSaldo()

        CSistema.dtToGrid(dgvAsiento, dtAsiento)
        dgvAsiento.Columns("IDTransaccion").Visible = False
        'Formato
        dgvAsiento.Columns("TipoCuenta").Visible = False
        dgvAsiento.Columns(8).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgvAsiento.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvAsiento.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvAsiento.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgvAsiento.Columns(5).DefaultCellStyle.Format = "N0"
        dgvAsiento.Columns(6).DefaultCellStyle.Format = "N0"
        dgvAsiento.Columns(7).DefaultCellStyle.Format = "N0"
        dgvAsiento.Columns(10).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvAsiento.Columns(11).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub CalcularSaldo()

        Dim Primero As Boolean = True
        Dim index As Integer = 0

        For Each oRow As DataRow In dtAsiento.Rows
            Dim Tipo As String = oRow("TipoCuenta")
            Dim Debe As Decimal = oRow("Debito")
            Dim Haber As Decimal = oRow("Credito")
            Dim Saldo As Decimal = 0

            If Primero = True Then
                If IsNumeric(dgvCuenta.Rows(dgvCuenta.SelectedCells(0).RowIndex).Cells("Saldo Anterior").Value) = False Then
                    Saldo = 0
                Else
                    Saldo = dgvCuenta.Rows(dgvCuenta.SelectedCells(0).RowIndex).Cells("Saldo Anterior").Value
                End If

                Primero = False
            Else
                Saldo = dtAsiento.Rows(index - 1)("Saldo")
            End If

            If Tipo = "DEBE" Then
                Saldo = (Saldo + Debe) - Haber
            End If

            If Tipo = "HABER" Then
                Saldo = (Saldo + Haber) - Debe
            End If

            oRow("Saldo") = Saldo

            index = index + 1

        Next

    End Sub

    Function Where() As String

        Where = ""

        Dim Codigo As String = dgvCuenta.Rows(dgvCuenta.SelectedCells(0).RowIndex).Cells(1).Value

        'Codigo
        Where = " Where Codigo = '" & Codigo & "' "

        'Fechas
        Where = Where & " And Fecha Between '" & FechaDesde & "' and '" & FechaHasta & "' "

        'Sucursal
        If IDSucursal > 0 Then
            Where = Where & " And  IDSucursal=" & IDSucursal & " "
        End If

        'Tipo de Comprobante
        If IDTipoComprobante > 0 Then
            Where = Where & " And IDTipoComprobante=" & IDTipoComprobante & " "
        End If

        'Tipo de Cuenta
        Select Case TipoCuenta
            Case 1
                Where = Where & " And Debito>0 And Credito=0 "
            Case 2
                Where = Where & " And Debito=0 And Credito>0 "
        End Select

    End Function

    Sub VerLibro()

        Try

            Dim frm As New frmReporte
            Dim Titulo As String = "LIBRO MAYOR"
            Dim SubTitulo As String = "Del " & FechaDesde & " al " & FechaHasta

            If IDSucursal > 0 Then
                SubTitulo = SubTitulo & " - " & Sucursal
            End If

            If IDTipoComprobante > 0 Then
                SubTitulo = SubTitulo & " - " & TipoComprobante
            End If
            If chkVistaMoneda.Valor = False Then
                CReporte.ImprimirLibroMayor(frm, Where(False), WhereDetalle, Titulo, SubTitulo, vgUsuarioIdentificador, Resumido)
            Else
                CReporte.ImprimirLibroMayorDetalles(frm, Where(False), WhereDetalle, Titulo, SubTitulo, vgUsuarioIdentificador, False, True, False, False, False, chkVistaMoneda.Valor)
            End If
        Catch ex As Exception

        End Try

    End Sub

    Function Where(ByVal Pantalla As Boolean) As String

        Dim Codigo As String = dgvCuenta.Rows(dgvCuenta.SelectedCells(0).RowIndex).Cells("Codigo").Value
        Where = Codigo & "," & Codigo & ",'" & FechaDesde & "','" & FechaHasta & "','" & Pantalla & "'"

    End Function

    Function WhereDetalle() As String

        WhereDetalle = ""

        If IDSucursal > 0 Then
            WhereDetalle = "IDSucursal = " & IDSucursal
        End If

        If IDTipoComprobante > 0 Then

            If WhereDetalle = "" Then
                WhereDetalle = "IDTipoComprobante= " & IDTipoComprobante
            Else
                WhereDetalle = WhereDetalle & " And IDTipoComprobante = " & IDTipoComprobante
            End If

        End If

    End Function

    Sub VerAsiento()

        Dim frm As New frmVisualizarAsiento
        Dim Numero As Integer = dgvAsiento.SelectedRows(0).Cells("Asiento").Value
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Text = "Asiento"

        Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From Asiento Where Numero='" & Numero & "'), 0 )")
        frm.IDTransaccion = IDTransaccion

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Private Sub frmLibroIVAAnalitico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgvCuenta_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCuenta.SelectionChanged
        ListarAsientos()
    End Sub

    Private Sub btnLibro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLibro.Click
        VerLibro()
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VerAsiento()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        If Not dtAsiento Is Nothing Then
            If dtAsiento.Rows.Count > 0 Then
                CSistema.dtToExcel2(dtAsiento, "Libro Mayor")
            End If
        End If

    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaAsientoContable
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.btnRegistrosGenerales = New System.Windows.Forms.Button()
        Me.lblTotalDebito = New System.Windows.Forms.Label()
        Me.txtComprobante = New System.Windows.Forms.TextBox()
        Me.chkComprobante = New System.Windows.Forms.CheckBox()
        Me.lblTotalOperacion = New System.Windows.Forms.Label()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalOperacion = New ERP.ocxTXTNumeric()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnVizualizar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.cbxComprobante = New System.Windows.Forms.ComboBox()
        Me.dtbRegistrosPorFecha = New System.Windows.Forms.Button()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtCantidadOperacion = New ERP.ocxTXTNumeric()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblNumero = New System.Windows.Forms.Label()
        Me.txtNumeroAsiento = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnRegistrosNoBalanceados = New System.Windows.Forms.Button()
        Me.btnRegistrosDelMes = New System.Windows.Forms.Button()
        Me.btnRegistrosDelDia = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblCantidadOperacion = New System.Windows.Forms.Label()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalDebito = New ERP.ocxTXTNumeric()
        Me.lblTotalCredito = New System.Windows.Forms.Label()
        Me.txtTotalCredito = New ERP.ocxTXTNumeric()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 36)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(203, 21)
        Me.cbxSucursal.TabIndex = 1
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(6, 13)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(70, 17)
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'btnRegistrosGenerales
        '
        Me.btnRegistrosGenerales.Location = New System.Drawing.Point(6, 171)
        Me.btnRegistrosGenerales.Name = "btnRegistrosGenerales"
        Me.btnRegistrosGenerales.Size = New System.Drawing.Size(203, 23)
        Me.btnRegistrosGenerales.TabIndex = 11
        Me.btnRegistrosGenerales.Text = "Registro General"
        Me.btnRegistrosGenerales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistrosGenerales.UseVisualStyleBackColor = True
        '
        'lblTotalDebito
        '
        Me.lblTotalDebito.Location = New System.Drawing.Point(3, 0)
        Me.lblTotalDebito.Name = "lblTotalDebito"
        Me.lblTotalDebito.Size = New System.Drawing.Size(50, 25)
        Me.lblTotalDebito.TabIndex = 0
        Me.lblTotalDebito.Text = "Debito:"
        Me.lblTotalDebito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtComprobante
        '
        Me.txtComprobante.Location = New System.Drawing.Point(99, 86)
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(110, 20)
        Me.txtComprobante.TabIndex = 6
        '
        'chkComprobante
        '
        Me.chkComprobante.AutoSize = True
        Me.chkComprobante.Location = New System.Drawing.Point(6, 63)
        Me.chkComprobante.Name = "chkComprobante"
        Me.chkComprobante.Size = New System.Drawing.Size(92, 17)
        Me.chkComprobante.TabIndex = 4
        Me.chkComprobante.Text = "Comprobante:"
        Me.chkComprobante.UseVisualStyleBackColor = True
        '
        'lblTotalOperacion
        '
        Me.lblTotalOperacion.Location = New System.Drawing.Point(194, 0)
        Me.lblTotalOperacion.Name = "lblTotalOperacion"
        Me.lblTotalOperacion.Size = New System.Drawing.Size(34, 23)
        Me.lblTotalOperacion.TabIndex = 0
        Me.lblTotalOperacion.Text = "Total:"
        Me.lblTotalOperacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.FlowLayoutPanel3, 1, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.FlowLayoutPanel4, 0, 0)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(3, 245)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(676, 34)
        Me.TableLayoutPanel5.TabIndex = 2
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtTotalOperacion)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblTotalOperacion)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(341, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(332, 28)
        Me.FlowLayoutPanel3.TabIndex = 0
        '
        'txtTotalOperacion
        '
        Me.txtTotalOperacion.Color = System.Drawing.Color.Empty
        Me.txtTotalOperacion.Decimales = True
        Me.txtTotalOperacion.Indicaciones = Nothing
        Me.txtTotalOperacion.Location = New System.Drawing.Point(234, 3)
        Me.txtTotalOperacion.Name = "txtTotalOperacion"
        Me.txtTotalOperacion.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalOperacion.SoloLectura = True
        Me.txtTotalOperacion.TabIndex = 4
        Me.txtTotalOperacion.TabStop = False
        Me.txtTotalOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalOperacion.Texto = "0"
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.btnNuevo)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnVizualizar)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnModificar)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnEliminar)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(332, 28)
        Me.FlowLayoutPanel4.TabIndex = 1
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(3, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnVizualizar
        '
        Me.btnVizualizar.Location = New System.Drawing.Point(84, 3)
        Me.btnVizualizar.Name = "btnVizualizar"
        Me.btnVizualizar.Size = New System.Drawing.Size(75, 23)
        Me.btnVizualizar.TabIndex = 1
        Me.btnVizualizar.Text = "Visualizar"
        Me.btnVizualizar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(165, 3)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 2
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'cbxComprobante
        '
        Me.cbxComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxComprobante.Enabled = False
        Me.cbxComprobante.FormattingEnabled = True
        Me.cbxComprobante.Location = New System.Drawing.Point(6, 86)
        Me.cbxComprobante.Name = "cbxComprobante"
        Me.cbxComprobante.Size = New System.Drawing.Size(92, 21)
        Me.cbxComprobante.TabIndex = 5
        '
        'dtbRegistrosPorFecha
        '
        Me.dtbRegistrosPorFecha.Location = New System.Drawing.Point(6, 281)
        Me.dtbRegistrosPorFecha.Name = "dtbRegistrosPorFecha"
        Me.dtbRegistrosPorFecha.Size = New System.Drawing.Size(203, 23)
        Me.dtbRegistrosPorFecha.TabIndex = 15
        Me.dtbRegistrosPorFecha.Text = "Registros por Fecha"
        Me.dtbRegistrosPorFecha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.dtbRegistrosPorFecha.UseVisualStyleBackColor = True
        '
        'dtpHasta
        '
        Me.dtpHasta.Location = New System.Drawing.Point(6, 250)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(203, 20)
        Me.dtpHasta.TabIndex = 14
        '
        'dtpDesde
        '
        Me.dtpDesde.Location = New System.Drawing.Point(6, 224)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(203, 20)
        Me.dtpDesde.TabIndex = 13
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(6, 208)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(145, 13)
        Me.lblFecha.TabIndex = 12
        Me.lblFecha.Text = "Fecha del Comprobante:"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtCantidadOperacion)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(617, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(56, 24)
        Me.Panel4.TabIndex = 2
        '
        'txtCantidadOperacion
        '
        Me.txtCantidadOperacion.Color = System.Drawing.Color.Empty
        Me.txtCantidadOperacion.Decimales = True
        Me.txtCantidadOperacion.Indicaciones = Nothing
        Me.txtCantidadOperacion.Location = New System.Drawing.Point(3, 1)
        Me.txtCantidadOperacion.Name = "txtCantidadOperacion"
        Me.txtCantidadOperacion.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadOperacion.SoloLectura = True
        Me.txtCantidadOperacion.TabIndex = 4
        Me.txtCantidadOperacion.TabStop = False
        Me.txtCantidadOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadOperacion.Texto = "0"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblNumero)
        Me.Panel3.Controls.Add(Me.txtNumeroAsiento)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(528, 24)
        Me.Panel3.TabIndex = 0
        '
        'lblNumero
        '
        Me.lblNumero.AutoSize = True
        Me.lblNumero.Location = New System.Drawing.Point(3, 5)
        Me.lblNumero.Name = "lblNumero"
        Me.lblNumero.Size = New System.Drawing.Size(100, 13)
        Me.lblNumero.TabIndex = 8
        Me.lblNumero.Text = "Numero de Asiento:"
        '
        'txtNumeroAsiento
        '
        Me.txtNumeroAsiento.Location = New System.Drawing.Point(109, 1)
        Me.txtNumeroAsiento.Name = "txtNumeroAsiento"
        Me.txtNumeroAsiento.Size = New System.Drawing.Size(110, 20)
        Me.txtNumeroAsiento.TabIndex = 7
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnRegistrosNoBalanceados)
        Me.GroupBox1.Controls.Add(Me.chkSucursal)
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.btnRegistrosGenerales)
        Me.GroupBox1.Controls.Add(Me.txtComprobante)
        Me.GroupBox1.Controls.Add(Me.chkComprobante)
        Me.GroupBox1.Controls.Add(Me.cbxComprobante)
        Me.GroupBox1.Controls.Add(Me.dtbRegistrosPorFecha)
        Me.GroupBox1.Controls.Add(Me.dtpHasta)
        Me.GroupBox1.Controls.Add(Me.dtpDesde)
        Me.GroupBox1.Controls.Add(Me.lblFecha)
        Me.GroupBox1.Controls.Add(Me.btnRegistrosDelMes)
        Me.GroupBox1.Controls.Add(Me.btnRegistrosDelDia)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(691, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(213, 525)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'btnRegistrosNoBalanceados
        '
        Me.btnRegistrosNoBalanceados.Location = New System.Drawing.Point(6, 352)
        Me.btnRegistrosNoBalanceados.Name = "btnRegistrosNoBalanceados"
        Me.btnRegistrosNoBalanceados.Size = New System.Drawing.Size(203, 23)
        Me.btnRegistrosNoBalanceados.TabIndex = 16
        Me.btnRegistrosNoBalanceados.Text = "Registros no Balanceados"
        Me.btnRegistrosNoBalanceados.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistrosNoBalanceados.UseVisualStyleBackColor = True
        '
        'btnRegistrosDelMes
        '
        Me.btnRegistrosDelMes.Location = New System.Drawing.Point(6, 142)
        Me.btnRegistrosDelMes.Name = "btnRegistrosDelMes"
        Me.btnRegistrosDelMes.Size = New System.Drawing.Size(203, 23)
        Me.btnRegistrosDelMes.TabIndex = 10
        Me.btnRegistrosDelMes.Text = "Registros del Mes"
        Me.btnRegistrosDelMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistrosDelMes.UseVisualStyleBackColor = True
        '
        'btnRegistrosDelDia
        '
        Me.btnRegistrosDelDia.Location = New System.Drawing.Point(6, 113)
        Me.btnRegistrosDelDia.Name = "btnRegistrosDelDia"
        Me.btnRegistrosDelDia.Size = New System.Drawing.Size(203, 23)
        Me.btnRegistrosDelDia.TabIndex = 9
        Me.btnRegistrosDelDia.Text = "Registros del Dia"
        Me.btnRegistrosDelDia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistrosDelDia.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 219.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(907, 531)
        Me.TableLayoutPanel1.TabIndex = 3
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel5, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.DataGridView1, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.DataGridView2, 0, 3)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(682, 525)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 4
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.27047!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.729528!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Panel2, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Panel4, 3, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(676, 30)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblCantidadOperacion)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(551, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(60, 24)
        Me.Panel2.TabIndex = 1
        '
        'lblCantidadOperacion
        '
        Me.lblCantidadOperacion.AutoSize = True
        Me.lblCantidadOperacion.Location = New System.Drawing.Point(3, 8)
        Me.lblCantidadOperacion.Name = "lblCantidadOperacion"
        Me.lblCantidadOperacion.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadOperacion.TabIndex = 0
        Me.lblCantidadOperacion.Text = "Cantidad:"
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 153.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.FlowLayoutPanel2, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 491)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(676, 31)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lblCantidadDetalle)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtCantidadDetalle)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(147, 25)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(3, 0)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(52, 25)
        Me.lblCantidadDetalle.TabIndex = 0
        Me.lblCantidadDetalle.Text = "Cantidad:"
        Me.lblCantidadDetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(61, 3)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(48, 22)
        Me.txtCantidadDetalle.SoloLectura = True
        Me.txtCantidadDetalle.TabIndex = 4
        Me.txtCantidadDetalle.TabStop = False
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalDebito)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalDebito)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblTotalCredito)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtTotalCredito)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblSaldo)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtSaldo)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(156, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(517, 25)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'txtTotalDebito
        '
        Me.txtTotalDebito.Color = System.Drawing.Color.Empty
        Me.txtTotalDebito.Decimales = True
        Me.txtTotalDebito.Indicaciones = Nothing
        Me.txtTotalDebito.Location = New System.Drawing.Point(59, 3)
        Me.txtTotalDebito.Name = "txtTotalDebito"
        Me.txtTotalDebito.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalDebito.SoloLectura = True
        Me.txtTotalDebito.TabIndex = 4
        Me.txtTotalDebito.TabStop = False
        Me.txtTotalDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebito.Texto = "0"
        '
        'lblTotalCredito
        '
        Me.lblTotalCredito.Location = New System.Drawing.Point(160, 0)
        Me.lblTotalCredito.Name = "lblTotalCredito"
        Me.lblTotalCredito.Size = New System.Drawing.Size(57, 25)
        Me.lblTotalCredito.TabIndex = 5
        Me.lblTotalCredito.Text = "Credito:"
        Me.lblTotalCredito.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalCredito
        '
        Me.txtTotalCredito.Color = System.Drawing.Color.Empty
        Me.txtTotalCredito.Decimales = True
        Me.txtTotalCredito.Indicaciones = Nothing
        Me.txtTotalCredito.Location = New System.Drawing.Point(223, 3)
        Me.txtTotalCredito.Name = "txtTotalCredito"
        Me.txtTotalCredito.Size = New System.Drawing.Size(95, 22)
        Me.txtTotalCredito.SoloLectura = True
        Me.txtTotalCredito.TabIndex = 6
        Me.txtTotalCredito.TabStop = False
        Me.txtTotalCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCredito.Texto = "0"
        '
        'lblSaldo
        '
        Me.lblSaldo.Location = New System.Drawing.Point(324, 0)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(57, 25)
        Me.lblSaldo.TabIndex = 7
        Me.lblSaldo.Text = "Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(387, 3)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(95, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 8
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(3, 39)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridView1.Size = New System.Drawing.Size(676, 200)
        Me.DataGridView1.StandardTab = True
        Me.DataGridView1.TabIndex = 3
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView2.DefaultCellStyle = DataGridViewCellStyle17
        Me.DataGridView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView2.Location = New System.Drawing.Point(3, 285)
        Me.DataGridView2.MultiSelect = False
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.Size = New System.Drawing.Size(676, 200)
        Me.DataGridView2.StandardTab = True
        Me.DataGridView2.TabIndex = 4
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(246, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'frmConsultaAsientoContable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(907, 531)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConsultaAsientoContable"
        Me.Text = "frmConsultaAsientoContable"
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents txtTotalDebito As ERP.ocxTXTNumeric
    Friend WithEvents btnRegistrosGenerales As System.Windows.Forms.Button
    Friend WithEvents lblTotalDebito As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As System.Windows.Forms.TextBox
    Friend WithEvents chkComprobante As System.Windows.Forms.CheckBox
    Friend WithEvents lblTotalOperacion As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtTotalOperacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxComprobante As System.Windows.Forms.ComboBox
    Friend WithEvents txtCantidadOperacion As ERP.ocxTXTNumeric
    Friend WithEvents dtbRegistrosPorFecha As System.Windows.Forms.Button
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRegistrosDelMes As System.Windows.Forms.Button
    Friend WithEvents btnRegistrosDelDia As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadOperacion As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblNumero As System.Windows.Forms.Label
    Friend WithEvents txtNumeroAsiento As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
    Friend WithEvents txtCantidadDetalle As ERP.ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblTotalCredito As System.Windows.Forms.Label
    Friend WithEvents txtTotalCredito As ERP.ocxTXTNumeric
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnVizualizar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents btnRegistrosNoBalanceados As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
End Class

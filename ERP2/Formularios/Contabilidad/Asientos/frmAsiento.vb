﻿Public Class frmAsiento

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CCaja As New CCaja

    'EVENTOS

    'PROPIEDADES
    Private CAsientoValue As New CAsiento
    Public Property CAsiento() As CAsiento
        Get
            Return CAsientoValue
        End Get
        Set(ByVal value As CAsiento)
            CAsientoValue = value
        End Set
    End Property

    Private oRowCabeceraValue As DataRow
    Public Property oRowCabecera() As DataRow
        Get
            Return oRowCabeceraValue
        End Get
        Set(ByVal value As DataRow)
            oRowCabeceraValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private IDTransaccionOperacionValue As Integer
    Public Property IDTransaccionOperacion() As Integer
        Get
            Return IDTransaccionOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionOperacionValue = value
        End Set
    End Property

    Private VolverAGenerarValue As Boolean
    Public Property VolverAGenerar() As Boolean
        Get
            Return VolverAGenerarValue
        End Get
        Set(ByVal value As Boolean)
            VolverAGenerarValue = value
        End Set
    End Property

    Private ActualizandoValue As Boolean
    Public Property Actualizando() As Boolean
        Get
            Return ActualizandoValue
        End Get
        Set(ByVal value As Boolean)
            ActualizandoValue = value
        End Set
    End Property

    Private FiltrarValue As Boolean
    Public Property Filtrar() As Boolean
        Get
            Return FiltrarValue
        End Get
        Set(ByVal value As Boolean)
            FiltrarValue = value
        End Set
    End Property

    Private whereFiltrovalue As String
    Public Property whereFiltro() As String
        Get
            Return whereFiltrovalue
        End Get
        Set(ByVal value As String)
            whereFiltrovalue = value
        End Set
    End Property

    Public Property GastoMultiple As Boolean
    Public Property IDUnidadNegocio As Integer
    Public Property IDCentroCosto As Integer
    Public Property IDDepartamento As Integer
    Public Property Seccion As Boolean

    'VARIABLES
    Dim CodigoSeleccionado As String

    'FUNCIONES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        FGEstiloFormulario(Me)

        'Controles
        If Filtrar = True Then
            ocxCuenta.whereFiltro = whereFiltrovalue
        Else
            ocxCuenta.whereFiltro = ""
        End If
        ocxCuenta.Conectar()
        chkBloquear.Checked = CAsiento.Bloquear

        'Funciones
        CargarInformacion()
        ListarDetalle()

        'Foco
        ocxCuenta.Focus()
        ocxCuenta.txtDescripcion.Focus()

    End Sub

    Sub CargarInformacion()

        'Detalle Asiento
        'CAsiento.dtDetalleAsiento = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleAsiento").Clone

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.GetTable("VCiudadSucursal").Copy, "IDCiudad", "CodigoCiudad")

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal").Copy, "ID", "Codigo")
        CSistema.SqlToComboBox(cbxSucursalDetalle.cbx, CData.GetTable("VSucursal").Copy, "ID", "Codigo")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, CData.GetTable("VMoneda").Copy, "ID", "Referencia")

        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        ''Unidad de negocio
        'CSistema.SqlToComboBox(cbxUnidadNegocioDetalle.cbx, CData.GetTable("VUnidadNegocio", "Estado='True'").Copy, "ID", "Descripcion")
        'cbxUnidadNegocioDetalle.cbx.SelectedItem = 0

        'Centro de Costos
        'CSistema.SqlToComboBox(cbxCentroCosto.cbx, CData.GetTable("VCentroCosto").Copy, "ID", "Descripcion")

        'FA 22/11/2022 - Plan de cuenta
        'CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio").Copy, "ID", "Descripcion")
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, "Select ID, Descripcion From UnidadNegocio Where Estado = 1 order by 2")
        If cbxSucursal.GetValue = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
            CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        End If
        'CSistema.SqlToComboBox(cbxCentroCosto.cbx, CData.GetTable("VCentroCosto", "ID=" & IDCentroCosto), "ID", "Descripcion")
        'CSistema.SqlToComboBox(cbxCentroCosto.cbx, CData.GetTable("vUnidadNegocio", "ID=" & IDUnidadNegocio), "ID", "Descripcion")
        'CSistema.SqlToComboBox(cbxCentroCosto.cbx, CData.GetTable("DepartamentoEmpresa", "ID=" & IDDepartamento), "ID", "Descripcion")


        'FA 22/11/2022 - Plan de cuenta
        CSistema.SqlToComboBox(cbxUnidadNegocioDetalle1.cbx, CData.GetTable("vUnidadNegocio").Copy, "ID", "Descripcion")
        'CSistema.SqlToComboBox(cbxCentroCostoDetalle1.cbx, CData.GetTable("VCentroCosto").Copy, "ID", "Descripcion")
        'CSistema.SqlToComboBox(cbxDepartamentoDetalle.cbx, CData.GetTable("DepartamentoEmpresa").Copy, "ID", "Departamento")
        If cbxSucursal.GetValue = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            CSistema.SqlToComboBox(cbxDepartamentoDetalle.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamentoDetalle.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
            CSistema.SqlToComboBox(cbxDepartamentoDetalle.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamentoDetalle.cbx.Text = ""
        End If

        'Detalle
        dgv.Columns.Add("colCodigo", "Codigo")
        dgv.Columns.Add("colDescripcion", "Descripcion")
        dgv.Columns.Add("colObservacion", "Observacion")
        dgv.Columns.Add("colDebito", "Debito")
        dgv.Columns.Add("colCredito", "Credito")

        dgv.Columns("colCodigo").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colDescripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colObservacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("colDebito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colCredito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        dgv.Columns("colDebito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("colCredito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        'Operacion
        cbxTipo.cbx.Items.Add("DEBITO")
        cbxTipo.cbx.Items.Add("CREDITO")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Cargar la Cabecera
        cbxCiudad.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDCiudad").ToString
        cbxSucursal.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDSucursal").ToString
        cbxSucursalDetalle.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDSucursal").ToString
        CAsiento.dtAsiento.Rows(0)("Numero") = CInt(CSistema.ExecuteScalar("Select IsNull(Max(Numero)+1,1) From Asiento"))
        txtID.txt.Text = CAsiento.dtAsiento.Rows(0)("Numero").ToString
        txtFecha.SetValue(CAsiento.dtAsiento.Rows(0)("Fecha").ToString)
        cbxMoneda.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDMoneda").ToString
        txtCotizacion.txt.Text = CAsiento.dtAsiento.Rows(0)("Cotizacion").ToString
        txtComprobante.txt.Text = CAsiento.dtAsiento.Rows(0)("Comprobante").ToString
        txtDetalle.txt.Text = CAsiento.dtAsiento.Rows(0)("Detalle").ToString
        CCaja.IDSucursal = CAsiento.dtAsiento.Rows(0)("IDSucursal").ToString
        'FA 22/11/2022 - Plan de cuenta
        chkGastosVarios.chk.Checked = CAsiento.dtAsiento.Rows(0)("GastosMultiples").ToString
        chkGastosVarios.SoloLectura = True
        If CAsiento.dtAsiento.Rows(0)("IDUnidadNegocio").ToString = "" Then
            cbxUnidadNegocio.cbx.Text = ""
        Else
            cbxUnidadNegocio.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDUnidadNegocio").ToString
        End If

        If chkGastosVarios.chk.Checked = True Then
            lblDepatamentoSolicitante.Visible = True
            Label6.Visible = False
        Else
            lblDepatamentoSolicitante.Visible = False
            Label6.Visible = True
        End If
        'If CAsiento.dtAsiento.Rows(0)("IDCentroCosto").ToString = "" Then
        '    cbxCentroCosto.cbx.Text = ""
        'Else
        '    cbxCentroCosto.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDCentroCosto").ToString
        'End If

        If CAsiento.dtAsiento.Rows(0)("IDDepartamento").ToString = "" Then
            cbxDepartamento.cbx.Text = ""
        Else
            cbxDepartamento.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDDepartamento").ToString
        End If

        If CAsiento.dtAsiento.Rows(0)("IDSeccion").ToString = "" Then
            cbxSeccion.cbx.Text = ""
        Else
            cbxSeccion.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDSeccion").ToString
        End If

        If chkGastosVarios.chk.Checked = True Then
            cbxUnidadNegocioDetalle1.SoloLectura = False
            'cbxCentroCostoDetalle1.SoloLectura = False
            cbxDepartamentoDetalle.SoloLectura = False
            cbxSeccionDetalle.SoloLectura = False
        End If
        'FA 22/11/2022 - Plan de cuenta
        'cbxUnidadNegocio.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDUnidadNegocio").ToString
        'cbxCentroCosto.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDCentroCosto").ToString
        'cbxDepartamento.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDDepartamento").ToString

        CCaja.ObtenerUltimoNumero()
        CargarInformacionCaja()

        'Bloquear controles
        cbxCiudad.Locked = False
        cbxSucursal.Locked = False
        txtID.SoloLectura = True
        txtFecha.SoloLectura = True
        cbxMoneda.Locked = False
        txtCotizacion.SoloLectura = True
        txtComprobante.SoloLectura = True
        txtDetalle.SoloLectura = True

        cbxUnidadNegocioDetalle1.cbx.Text = ""
        'cbxCentroCostoDetalle1.cbx.Text = ""
        cbxDepartamentoDetalle.cbx.Text = ""

    End Sub

    Sub CargarInformacionCaja()

        lblInformacionCaja.Text = ""

        If CCaja.Numero = 0 Then
            Exit Sub
        End If

        txtNroCaja.SetValue(CCaja.Numero)
        lblInformacionCaja.Text = CCaja.Estado

    End Sub

    Sub CargarItem()

        'Validar
        'Cuenta
        If ocxCuenta.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente la cuenta!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Importe
        If IsNumeric(txtImporteLocal.ObtenerValor) = False Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtImporteLocal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If cbxTipo.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'FA 01/12/2022 - Plan de cuenta
        If chkGastosVarios.chk.Checked = True Then
            If cbxUnidadNegocioDetalle1.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar una Unidad de Negocio", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If

            'If cbxCentroCostoDetalle1.cbx.Text = "" Then
            '    CSistema.MostrarError("Debe de seleccionar un Centro de Costo", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            '    Exit Sub
            'End If

            If cbxDepartamentoDetalle.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar un Departamento", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = CAsiento.dtDetalleAsiento.NewRow()

        If Actualizando = True Then

            Dim Observacion As String = dgv.SelectedRows(0).Cells("colObservacion").Value

            If Observacion = "" Then
                Observacion = txtDetalle.GetValue
            End If

            For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Select("Codigo='" & CodigoSeleccionado & "' And Observacion='" & Observacion & "' ")
                dRow = oRow
            Next
        Else
            dRow = CAsiento.dtDetalleAsiento.NewRow()
        End If

        dRow("IDTransaccion") = 0
        dRow("IDCuentaContable") = ocxCuenta.Registro("ID").ToString
        dRow("Cuenta") = ocxCuenta.Registro("Cuenta").ToString
        dRow("Codigo") = ocxCuenta.Registro("Codigo").ToString
        dRow("Descripcion") = ocxCuenta.Registro("Descripcion").ToString

        If txtDescripcion.GetValue = "" Then
            dRow("Observacion") = txtDetalle.GetValue
        Else
            dRow("Observacion") = txtDescripcion.GetValue
        End If

        dRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
        dRow("Orden") = CAsiento.dtDetalleAsiento.Rows.Count + 10

        Dim Importe As Decimal = CDec(txtImporteLocal.ObtenerValor)

        'Verificar la moneda
        If CAsiento.dtAsiento.Rows(0)("IDMoneda") <> 1 Then
            Importe = Importe * CAsiento.dtAsiento.Rows(0)("Cotizacion")
        End If

        dRow("Importe") = Importe

        If cbxTipo.cbx.Text = "CREDITO" Then
            dRow("Credito") = Importe
            dRow("Debito") = 0
        End If

        If cbxTipo.cbx.Text = "DEBITO" Then
            dRow("Credito") = 0
            dRow("Debito") = Importe
        End If

        dRow("Importe") = Importe

        If txtDescripcion.GetValue = "" Then
            dRow("Observacion") = txtDetalle.GetValue
        Else
            dRow("Observacion") = txtDescripcion.GetValue
        End If

        dRow("IDSucursal") = cbxSucursalDetalle.GetValue
        dRow("TipoComprobante") = txtTipoComprobante.GetValue
        dRow("NroComprobante") = txtNroComprobante.GetValue

        'FA 05/12/2022 - Plan de cuenta
        If chkGastosVarios.chk.Checked = True Then
            dRow("IDUnidadNegocioDetalle") = cbxUnidadNegocioDetalle1.GetValue
            dRow("UnidadNegocioDetalle") = cbxUnidadNegocioDetalle1.cbx.Text
            'dRow("IDCentroCostoDetalle") = cbxCentroCostoDetalle1.GetValue
            'dRow("CentroCostoDetalle") = cbxCentroCostoDetalle1.cbx.Text
            dRow("IDDepartamentoEmpresa") = cbxDepartamentoDetalle.GetValue
            dRow("Departamento") = cbxDepartamentoDetalle.cbx.Text
            dRow("IDSeccionDetalle") = cbxSeccionDetalle.GetValue
            dRow("Seccion") = cbxSeccionDetalle.cbx.Text
        Else
            dRow("IDCentroCosto") = ocxCuenta.IDCentroCosto
            dRow("IDCentroCostoDetalle") = ocxCuenta.IDCentroCosto
            'dRow("CentroCostoDetalle") = ocxCuenta.IDCentroCosto
        End If

        dRow("Fijo") = False


        If Actualizando = False Then
            CAsiento.dtDetalleAsiento.Rows.Add(dRow)
        End If

        'Listar Detalle
        ListarDetalle()

        'Caluclar Totales
        CalcularTotales()

        'Inicializar controles
        ocxCuenta.Clear()
        txtImporteLocal.txt.Clear()
        txtDescripcion.txt.Clear()
        Actualizando = False

        cbxUnidadNegocioDetalle1.cbx.Text = ""
        cbxDepartamentoDetalle.cbx.Text = ""

        ocxCuenta.Focus()

        PosicionarAutomaticamente()

        'Preguntar para salir
        If txtSaldo.ObtenerValor = "0" And dgv.Rows.Count > 0 Then
            If MessageBox.Show("Cerrar el asiento?", "Asiento", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                Procesar()
            End If
        End If

    End Sub

    Sub ListarDetalle()


        'Limpiamos todo el detalle
        dgv.Rows.Clear()

        'Volver a Ordenar
        CData.OrderDataTable(CAsiento.dtDetalleAsiento, "Orden")
        CAsiento.AgruparDetalle()

        'Cargamos registro por registro
        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows

            Dim Registro(4) As String
            Registro(0) = oRow("Codigo").ToString
            Registro(1) = oRow("Descripcion").ToString
            Registro(2) = oRow("Observacion").ToString
            Registro(3) = CSistema.FormatoNumero(oRow("Debito").ToString)
            Registro(4) = CSistema.FormatoMoneda(oRow("Credito").ToString)

            dgv.Rows.Add(Registro)

        Next

        CalcularTotales()


    End Sub

    Public Sub CalcularTotales()

        CSistema.TotalesGrid(dgv, txtTotalDebitoLocal.txt, 3)
        CSistema.TotalesGrid(dgv, txtTotalCreditoLocal.txt, 4)

        'Hayar el saldo
        Dim Saldo As Decimal = 0
        Saldo = CDec(txtTotalCreditoLocal.ObtenerValor) - CDec(txtTotalDebitoLocal.ObtenerValor)

        txtSaldo.txt.Text = Saldo


    End Sub

    Sub EliminarItem()

        'Validar
        If dgv.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgv, mensaje)
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
            If oRow("Codigo") = dgv.SelectedRows(0).Cells(0).Value Then

                Dim Observacion As String = dgv.SelectedRows(0).Cells("colObservacion").Value

                If Observacion = "" Then
                    Observacion = oRow("Observacion")
                End If

                If Observacion = oRow("Observacion") Then
                    CAsiento.dtDetalleAsiento.Rows.Remove(oRow)
                    Exit For
                End If

            End If
        Next

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()
        CalcularTotales()

    End Sub

    Sub SeleccionarItem()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim Observacion As String = dgv.SelectedRows(0).Cells("colObservacion").Value

        If Observacion = "" Then
            Observacion = txtDetalle.GetValue
        End If

        For Each orow As DataRow In CAsiento.dtDetalleAsiento.Select(" Codigo = '" & CodigoSeleccionado & "' And Observacion = '" & Observacion & "' ")

            ocxCuenta.SeleccionarRegistro(orow("Codigo"))

            If orow("Credito") > 0 Then
                cbxTipo.cbx.SelectedIndex = 1
                txtImporteLocal.SetValue(orow("Credito"))
            End If

            If orow("Debito") > 0 Then
                cbxTipo.cbx.SelectedIndex = 0
                txtImporteLocal.SetValue(orow("Debito"))
            End If

            txtDescripcion.SetValue(orow("Observacion").ToString)
            txtTipoComprobante.SetValue(orow("TipoComprobante").ToString)
            txtNroComprobante.SetValue(orow("NroComprobante").ToString)

            cbxSucursalDetalle.cbx.Text = orow("CodSucursal").ToString

        Next

        Actualizando = True

        ocxCuenta.txtCodigo.Focus()

    End Sub

    Sub Procesar()

        'Actualizar Cabecera
        Try

            'Caja
            If CCaja.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente la caja correspondiente!"
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'If CCaja.Habilitado = False Then
            '    Dim mensaje As String = "La caja no esta habilitada! Seleccione otro numero de caja."
            '    MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    tsslEstado.Text = mensaje
            '    Exit Sub
            'End If

            CAsiento.Bloquear = chkBloquear.Checked
            'CAsiento.IDCentroCosto = cbxCentroCosto.GetValue
            CAsiento.NroCaja = txtNroCaja.ObtenerValor
            CAsiento.CajaHabilitada = CCaja.Habilitado

        Catch ex As Exception

        End Try

        Me.Close()

    End Sub

    Sub Salir()

        Me.Close()

    End Sub

    Sub Aceptar()


    End Sub

    Sub VerDetalle()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim frm As New frmPropiedad
        frm.TituloVentana = "Propiedades de la Cuenta"
        frm.Titulo = dgv.SelectedRows(0).Cells(1).Value
        frm.dt = CAsiento.dtDetalleAsiento
        frm.ShowDialog()

    End Sub

    Sub PosicionarAutomaticamente()

        Dim Debito As Decimal = txtTotalDebitoLocal.ObtenerValor
        Dim Credito As Decimal = txtTotalCreditoLocal.ObtenerValor

        If Credito > Debito Then
            cbxTipo.cbx.SelectedIndex = 0
        Else
            cbxTipo.cbx.SelectedIndex = 1
        End If

    End Sub

    Sub CalcularImporte()

    End Sub

    Sub AperturaCaja()

        Dim frm As New frmAperturaCaja
        FGMostrarFormulario(Me, frm, "Apertura", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        CCaja.ObtenerUltimoNumero()
        CargarInformacionCaja()

    End Sub

    Private Sub frmAsiento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAsiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub ocxCuenta_ItemSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxCuenta.ItemSeleccionado
        If ocxCuenta.Seleccionado = True Then
            PosicionarAutomaticamente()
            cbxTipo.cbx.Focus()
            'cbxUnidadNegocioDetalle.cbx.SelectedValue = ocxCuenta.Registro("IDUnidadNegocio")
            'cbxCentroCosto.cbx.SelectedValue = ocxCuenta.Registro("IDCentroCosto")
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Procesar()
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub chkVolverGenerar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVolverGenerar.CheckedChanged

        If chkVolverGenerar.Checked = True Then
            If MessageBox.Show("Reestablecer el asiento?", "Asiento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                VolverAGenerar = True
                Me.Close()
            Else
                chkVolverGenerar.Checked = False
            End If
        End If

    End Sub

    Private Sub chkBloquear_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBloquear.CheckedChanged
        CAsiento.Bloquear = chkBloquear.Checked
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        CargarItem()
    End Sub

    Private Sub VerDescuentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDescuentosToolStripMenuItem.Click
        VerDetalle()
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(CAsiento.dtDetalleAsiento)
    End Sub

    Private Sub txtDescripcion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            If txtDescripcion.GetValue.Length <= 1 Then
                txtDescripcion.SetValue(txtDetalle.GetValue)
                CSistema.SelectNextControl(Me, Keys.Enter)
            End If
        End If
    End Sub

    Private Sub txtTipoComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtTipoComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            txtTipoComprobante.SetValue(CAsiento.dtAsiento.Rows(0)("TipoComprobante").ToString)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            txtNroComprobante.SetValue(CAsiento.dtAsiento.Rows(0)("NroComprobante").ToString)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If
    End Sub

    Private Sub txtImporteLocal_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteLocal.TeclaPrecionada

        If e.KeyCode = Keys.Add Then

            Dim Importe As Decimal = CAsiento.dtAsiento.Rows(0)("Total")

            txtImporteLocal.SetValue(Importe)

            CSistema.SelectNextControl(Me, Keys.Enter)

        End If


        If e.KeyCode = Keys.Multiply Then

            Dim Saldo As Decimal = txtSaldo.ObtenerValor / CAsiento.dtAsiento.Rows(0)("Cotizacion")
            If Saldo < 0 Then
                Saldo = Saldo * -1
            End If
            txtImporteLocal.SetValue(Saldo)
            CSistema.SelectNextControl(Me, Keys.Enter)

        End If

    End Sub

    Private Sub dgv_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarItem()
        End If

        If e.KeyCode = Keys.F2 Then
            SeleccionarItem()
        End If

    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Salir()
    End Sub

    Private Sub txtNroCaja_Leave(sender As System.Object, e As System.EventArgs) Handles txtNroCaja.Leave
        CCaja.IDSucursal = cbxSucursal.GetValue
        CCaja.Obtener(txtNroCaja.ObtenerValor)
        CAsiento.CajaHabilitada = CCaja.Habilitado
        CargarInformacionCaja()
    End Sub

    Private Sub txtNroCaja_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroCaja.TeclaPrecionada

        If e.KeyCode = Keys.Add Then
            AperturaCaja()
        End If

        If e.KeyCode = Keys.End Then
            CCaja.ObtenerUltimoNumero()
            CargarInformacionCaja()
        End If

    End Sub

    Private Sub dgv_SelectionChanged(sender As Object, e As System.EventArgs) Handles dgv.SelectionChanged

        ocxCuenta.Clear()
        txtImporteLocal.SetValue(0)
        txtDescripcion.SetValue("")
        txtTipoComprobante.SetValue("")
        txtNroComprobante.SetValue("")
        cbxSucursal.cbx.Text = ""

        Actualizando = False

        CodigoSeleccionado = dgv.CurrentRow.Cells("colCodigo").Value

    End Sub

    Private Sub cbxUnidadNegocio_PropertyChanged(sender As Object, e As System.EventArgs)

    End Sub

    'FA 28/05/2021
    Sub frmAsiento_Activate()
        Me.Refresh()
    End Sub

    Private Sub cbxDepartamentoDetalle_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamentoDetalle.PropertyChanged
        Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamentoDetalle.cbx.SelectedValue)
        If Seccion = True Then
            Dim dtSeccionDetalle As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamentoDetalle.cbx.SelectedValue)
            CSistema.SqlToComboBox(cbxSeccionDetalle.cbx, dtSeccionDetalle, "ID", "Descripcion")
            cbxSeccionDetalle.cbx.Text = ""
            cbxSeccionDetalle.SoloLectura = False
        Else
            cbxSeccionDetalle.SoloLectura = True
        End If
    End Sub
End Class
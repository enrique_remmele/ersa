﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAgregarAsiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.lblDptoSolicitante = New System.Windows.Forms.Label()
        Me.chkGastosVarios = New ERP.ocxCHK()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.lblDptoConsume = New System.Windows.Forms.Label()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxOperacion = New ERP.ocxCBX()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion2 = New System.Windows.Forms.Label()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.chkBloquear = New System.Windows.Forms.CheckBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.txtTotalDebitoLocal = New ERP.ocxTXTNumeric()
        Me.txtTotalCreditoLocal = New ERP.ocxTXTNumeric()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.VerDescuentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbxDepartamentoDetalle = New ERP.ocxCBX()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbxUnidadNegocioDetalle1 = New ERP.ocxCBX()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ocxCuenta = New ERP.ocxTXTCuentaContable()
        Me.cbxSucursalDetalle = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTipoComprobante = New ERP.ocxTXTString()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtImporteLocal = New ERP.ocxTXTNumeric()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.lblImporteLocal = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxSeccion = New ERP.ocxCBX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxSeccionDetalle = New ERP.ocxCBX()
        Me.gbxCabecera.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(220, 13)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(531, 13)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 7
        Me.lblMoneda.Text = "Moneda:"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.cbxSeccion)
        Me.gbxCabecera.Controls.Add(Me.lblDptoSolicitante)
        Me.gbxCabecera.Controls.Add(Me.chkGastosVarios)
        Me.gbxCabecera.Controls.Add(Me.cbxDepartamento)
        Me.gbxCabecera.Controls.Add(Me.lblDptoConsume)
        Me.gbxCabecera.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxOperacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion2)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.Location = New System.Drawing.Point(3, 3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(766, 165)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'lblDptoSolicitante
        '
        Me.lblDptoSolicitante.AutoSize = True
        Me.lblDptoSolicitante.Location = New System.Drawing.Point(254, 92)
        Me.lblDptoSolicitante.Name = "lblDptoSolicitante"
        Me.lblDptoSolicitante.Size = New System.Drawing.Size(129, 13)
        Me.lblDptoSolicitante.TabIndex = 23
        Me.lblDptoSolicitante.Text = "Departamento Solicitante:"
        '
        'chkGastosVarios
        '
        Me.chkGastosVarios.BackColor = System.Drawing.Color.Transparent
        Me.chkGastosVarios.Color = System.Drawing.Color.Empty
        Me.chkGastosVarios.Location = New System.Drawing.Point(82, 64)
        Me.chkGastosVarios.Name = "chkGastosVarios"
        Me.chkGastosVarios.Size = New System.Drawing.Size(206, 20)
        Me.chkGastosVarios.SoloLectura = False
        Me.chkGastosVarios.TabIndex = 14
        Me.chkGastosVarios.Texto = "Asignacion de Unidad y Departamento"
        Me.chkGastosVarios.Valor = False
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(250, 109)
        Me.cbxDepartamento.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(248, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 20
        Me.cbxDepartamento.Texto = ""
        '
        'lblDptoConsume
        '
        Me.lblDptoConsume.AutoSize = True
        Me.lblDptoConsume.Location = New System.Drawing.Point(251, 92)
        Me.lblDptoConsume.Name = "lblDptoConsume"
        Me.lblDptoConsume.Size = New System.Drawing.Size(145, 13)
        Me.lblDptoConsume.TabIndex = 19
        Me.lblDptoConsume.Text = "Departamento que Consume:"
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = Nothing
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = Nothing
        Me.cbxUnidadNegocio.DataValueMember = Nothing
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(7, 109)
        Me.cbxUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(235, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 16
        Me.cbxUnidadNegocio.Texto = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 92)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(102, 13)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Unidad de Negocio:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(253, 36)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(96, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 12
        Me.cbxTipoComprobante.Texto = ""
        '
        'cbxOperacion
        '
        Me.cbxOperacion.CampoWhere = Nothing
        Me.cbxOperacion.CargarUnaSolaVez = False
        Me.cbxOperacion.DataDisplayMember = Nothing
        Me.cbxOperacion.DataFilter = Nothing
        Me.cbxOperacion.DataOrderBy = Nothing
        Me.cbxOperacion.DataSource = Nothing
        Me.cbxOperacion.DataValueMember = Nothing
        Me.cbxOperacion.dtSeleccionado = Nothing
        Me.cbxOperacion.FormABM = Nothing
        Me.cbxOperacion.Indicaciones = Nothing
        Me.cbxOperacion.Location = New System.Drawing.Point(82, 36)
        Me.cbxOperacion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.SeleccionMultiple = False
        Me.cbxOperacion.SeleccionObligatoria = True
        Me.cbxOperacion.Size = New System.Drawing.Size(165, 21)
        Me.cbxOperacion.SoloLectura = False
        Me.cbxOperacion.TabIndex = 11
        Me.cbxOperacion.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(271, 9)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(94, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(586, 9)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(87, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 8
        Me.cbxMoneda.Texto = ""
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(673, 9)
        Me.txtCotizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(83, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 9
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 18, 12, 8, 47, 578)
        Me.txtFecha.Location = New System.Drawing.Point(426, 9)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(83, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 6
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(386, 13)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 5
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(82, 9)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(145, 9)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion2
        '
        Me.lblOperacion2.AutoSize = True
        Me.lblOperacion2.Location = New System.Drawing.Point(18, 13)
        Me.lblOperacion2.Name = "lblOperacion2"
        Me.lblOperacion2.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion2.TabIndex = 0
        Me.lblOperacion2.Text = "Operacion:"
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(34, 141)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 21
        Me.lblDetalle.Text = "Detalle:"
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(82, 137)
        Me.txtDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(674, 21)
        Me.txtDetalle.SoloLectura = False
        Me.txtDetalle.TabIndex = 22
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(4, 40)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 10
        Me.lblComprobante.Text = "Comprobante:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(355, 36)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(128, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 13
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 667)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(772, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxCabecera, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 171.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 179.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(772, 689)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.chkBloquear)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnAceptar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 629)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(766, 38)
        Me.Panel2.TabIndex = 3
        '
        'chkBloquear
        '
        Me.chkBloquear.AutoSize = True
        Me.chkBloquear.Location = New System.Drawing.Point(16, 18)
        Me.chkBloquear.Name = "chkBloquear"
        Me.chkBloquear.Size = New System.Drawing.Size(68, 17)
        Me.chkBloquear.TabIndex = 0
        Me.chkBloquear.Text = "Bloquear"
        Me.chkBloquear.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(681, 12)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(600, 12)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtSaldo, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtTotalDebitoLocal, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtTotalCreditoLocal, 3, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 415)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(766, 29)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblSaldo)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(460, 23)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(420, 0)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldo.TabIndex = 0
        Me.lblSaldo.Text = "Saldo:"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(470, 4)
        Me.txtSaldo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(90, 19)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 1
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'txtTotalDebitoLocal
        '
        Me.txtTotalDebitoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalDebitoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalDebitoLocal.Decimales = True
        Me.txtTotalDebitoLocal.Indicaciones = Nothing
        Me.txtTotalDebitoLocal.Location = New System.Drawing.Point(570, 4)
        Me.txtTotalDebitoLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalDebitoLocal.Name = "txtTotalDebitoLocal"
        Me.txtTotalDebitoLocal.Size = New System.Drawing.Size(90, 19)
        Me.txtTotalDebitoLocal.SoloLectura = True
        Me.txtTotalDebitoLocal.TabIndex = 2
        Me.txtTotalDebitoLocal.TabStop = False
        Me.txtTotalDebitoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebitoLocal.Texto = "0"
        '
        'txtTotalCreditoLocal
        '
        Me.txtTotalCreditoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalCreditoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalCreditoLocal.Decimales = True
        Me.txtTotalCreditoLocal.Indicaciones = Nothing
        Me.txtTotalCreditoLocal.Location = New System.Drawing.Point(670, 4)
        Me.txtTotalCreditoLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalCreditoLocal.Name = "txtTotalCreditoLocal"
        Me.txtTotalCreditoLocal.Size = New System.Drawing.Size(90, 19)
        Me.txtTotalCreditoLocal.SoloLectura = True
        Me.txtTotalCreditoLocal.TabIndex = 3
        Me.txtTotalCreditoLocal.TabStop = False
        Me.txtTotalCreditoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCreditoLocal.Texto = "0"
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 174)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(766, 235)
        Me.dgv.StandardTab = True
        Me.dgv.TabIndex = 1
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerDescuentosToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(158, 48)
        '
        'VerDescuentosToolStripMenuItem
        '
        Me.VerDescuentosToolStripMenuItem.Name = "VerDescuentosToolStripMenuItem"
        Me.VerDescuentosToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.VerDescuentosToolStripMenuItem.Text = "Ver detalle"
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 450)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(766, 173)
        Me.Panel3.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.cbxSeccionDetalle)
        Me.Panel1.Controls.Add(Me.cbxDepartamentoDetalle)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.cbxUnidadNegocioDetalle1)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.ocxCuenta)
        Me.Panel1.Controls.Add(Me.cbxSucursalDetalle)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtNroComprobante)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtTipoComprobante)
        Me.Panel1.Controls.Add(Me.lblCuenta)
        Me.Panel1.Controls.Add(Me.btnAgregar)
        Me.Panel1.Controls.Add(Me.lblTipo)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Controls.Add(Me.txtImporteLocal)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.cbxTipo)
        Me.Panel1.Controls.Add(Me.lblImporteLocal)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(766, 173)
        Me.Panel1.TabIndex = 0
        '
        'cbxDepartamentoDetalle
        '
        Me.cbxDepartamentoDetalle.CampoWhere = Nothing
        Me.cbxDepartamentoDetalle.CargarUnaSolaVez = False
        Me.cbxDepartamentoDetalle.DataDisplayMember = Nothing
        Me.cbxDepartamentoDetalle.DataFilter = Nothing
        Me.cbxDepartamentoDetalle.DataOrderBy = Nothing
        Me.cbxDepartamentoDetalle.DataSource = Nothing
        Me.cbxDepartamentoDetalle.DataValueMember = Nothing
        Me.cbxDepartamentoDetalle.dtSeleccionado = Nothing
        Me.cbxDepartamentoDetalle.FormABM = Nothing
        Me.cbxDepartamentoDetalle.Indicaciones = Nothing
        Me.cbxDepartamentoDetalle.Location = New System.Drawing.Point(251, 111)
        Me.cbxDepartamentoDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepartamentoDetalle.Name = "cbxDepartamentoDetalle"
        Me.cbxDepartamentoDetalle.SeleccionMultiple = False
        Me.cbxDepartamentoDetalle.SeleccionObligatoria = False
        Me.cbxDepartamentoDetalle.Size = New System.Drawing.Size(256, 21)
        Me.cbxDepartamentoDetalle.SoloLectura = False
        Me.cbxDepartamentoDetalle.TabIndex = 17
        Me.cbxDepartamentoDetalle.TabStop = False
        Me.cbxDepartamentoDetalle.Texto = ""
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(252, 96)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(145, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Departamento que Consume:"
        '
        'cbxUnidadNegocioDetalle1
        '
        Me.cbxUnidadNegocioDetalle1.CampoWhere = Nothing
        Me.cbxUnidadNegocioDetalle1.CargarUnaSolaVez = False
        Me.cbxUnidadNegocioDetalle1.DataDisplayMember = Nothing
        Me.cbxUnidadNegocioDetalle1.DataFilter = Nothing
        Me.cbxUnidadNegocioDetalle1.DataOrderBy = Nothing
        Me.cbxUnidadNegocioDetalle1.DataSource = Nothing
        Me.cbxUnidadNegocioDetalle1.DataValueMember = Nothing
        Me.cbxUnidadNegocioDetalle1.dtSeleccionado = Nothing
        Me.cbxUnidadNegocioDetalle1.FormABM = Nothing
        Me.cbxUnidadNegocioDetalle1.Indicaciones = Nothing
        Me.cbxUnidadNegocioDetalle1.Location = New System.Drawing.Point(17, 111)
        Me.cbxUnidadNegocioDetalle1.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocioDetalle1.Name = "cbxUnidadNegocioDetalle1"
        Me.cbxUnidadNegocioDetalle1.SeleccionMultiple = False
        Me.cbxUnidadNegocioDetalle1.SeleccionObligatoria = False
        Me.cbxUnidadNegocioDetalle1.Size = New System.Drawing.Size(228, 21)
        Me.cbxUnidadNegocioDetalle1.SoloLectura = False
        Me.cbxUnidadNegocioDetalle1.TabIndex = 13
        Me.cbxUnidadNegocioDetalle1.TabStop = False
        Me.cbxUnidadNegocioDetalle1.Texto = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(18, 96)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(138, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Unidad de Negocio Detalle:"
        '
        'ocxCuenta
        '
        Me.ocxCuenta.AlturaMaxima = 84
        Me.ocxCuenta.Consulta = Nothing
        Me.ocxCuenta.IDCentroCosto = 0
        Me.ocxCuenta.IDUnidadNegocio = 0
        Me.ocxCuenta.ListarTodas = False
        Me.ocxCuenta.Location = New System.Drawing.Point(14, 18)
        Me.ocxCuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.ocxCuenta.Name = "ocxCuenta"
        Me.ocxCuenta.Registro = Nothing
        Me.ocxCuenta.Resolucion173 = False
        Me.ocxCuenta.Seleccionado = False
        Me.ocxCuenta.Size = New System.Drawing.Size(556, 24)
        Me.ocxCuenta.SoloLectura = False
        Me.ocxCuenta.TabIndex = 1
        Me.ocxCuenta.Texto = Nothing
        Me.ocxCuenta.whereFiltro = Nothing
        '
        'cbxSucursalDetalle
        '
        Me.cbxSucursalDetalle.CampoWhere = Nothing
        Me.cbxSucursalDetalle.CargarUnaSolaVez = False
        Me.cbxSucursalDetalle.DataDisplayMember = Nothing
        Me.cbxSucursalDetalle.DataFilter = Nothing
        Me.cbxSucursalDetalle.DataOrderBy = Nothing
        Me.cbxSucursalDetalle.DataSource = Nothing
        Me.cbxSucursalDetalle.DataValueMember = Nothing
        Me.cbxSucursalDetalle.dtSeleccionado = Nothing
        Me.cbxSucursalDetalle.FormABM = Nothing
        Me.cbxSucursalDetalle.Indicaciones = Nothing
        Me.cbxSucursalDetalle.Location = New System.Drawing.Point(587, 65)
        Me.cbxSucursalDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursalDetalle.Name = "cbxSucursalDetalle"
        Me.cbxSucursalDetalle.SeleccionMultiple = False
        Me.cbxSucursalDetalle.SeleccionObligatoria = True
        Me.cbxSucursalDetalle.Size = New System.Drawing.Size(171, 21)
        Me.cbxSucursalDetalle.SoloLectura = False
        Me.cbxSucursalDetalle.TabIndex = 10
        Me.cbxSucursalDetalle.TabStop = False
        Me.cbxSucursalDetalle.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(588, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Sucursal:"
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(497, 65)
        Me.txtNroComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(88, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 9
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroComprobante.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(432, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Comprobante:"
        '
        'txtTipoComprobante
        '
        Me.txtTipoComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoComprobante.Color = System.Drawing.Color.Empty
        Me.txtTipoComprobante.Indicaciones = Nothing
        Me.txtTipoComprobante.Location = New System.Drawing.Point(429, 65)
        Me.txtTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTipoComprobante.Multilinea = False
        Me.txtTipoComprobante.Name = "txtTipoComprobante"
        Me.txtTipoComprobante.Size = New System.Drawing.Size(69, 21)
        Me.txtTipoComprobante.SoloLectura = False
        Me.txtTipoComprobante.TabIndex = 8
        Me.txtTipoComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoComprobante.Texto = ""
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(18, 5)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 0
        Me.lblCuenta.Text = "Cuenta:"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(664, 139)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(94, 23)
        Me.btnAgregar.TabIndex = 18
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(578, 5)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 2
        Me.lblTipo.Text = "Tipo:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(18, 50)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 5
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'txtImporteLocal
        '
        Me.txtImporteLocal.Color = System.Drawing.Color.Empty
        Me.txtImporteLocal.Decimales = True
        Me.txtImporteLocal.Indicaciones = Nothing
        Me.txtImporteLocal.Location = New System.Drawing.Point(668, 20)
        Me.txtImporteLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtImporteLocal.Name = "txtImporteLocal"
        Me.txtImporteLocal.Size = New System.Drawing.Size(90, 22)
        Me.txtImporteLocal.SoloLectura = False
        Me.txtImporteLocal.TabIndex = 4
        Me.txtImporteLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteLocal.Texto = "0"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(15, 65)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(409, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 6
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.dtSeleccionado = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(578, 20)
        Me.cbxTipo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionMultiple = False
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(90, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'lblImporteLocal
        '
        Me.lblImporteLocal.AutoSize = True
        Me.lblImporteLocal.Location = New System.Drawing.Point(713, 5)
        Me.lblImporteLocal.Name = "lblImporteLocal"
        Me.lblImporteLocal.Size = New System.Drawing.Size(45, 13)
        Me.lblImporteLocal.TabIndex = 4
        Me.lblImporteLocal.Text = "Importe:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(503, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Seccion"
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = Nothing
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = Nothing
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = Nothing
        Me.cbxSeccion.DataSource = Nothing
        Me.cbxSeccion.DataValueMember = Nothing
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(506, 109)
        Me.cbxSeccion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(248, 21)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 24
        Me.cbxSeccion.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(513, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Seccion"
        '
        'cbxSeccionDetalle
        '
        Me.cbxSeccionDetalle.CampoWhere = Nothing
        Me.cbxSeccionDetalle.CargarUnaSolaVez = False
        Me.cbxSeccionDetalle.DataDisplayMember = Nothing
        Me.cbxSeccionDetalle.DataFilter = Nothing
        Me.cbxSeccionDetalle.DataOrderBy = Nothing
        Me.cbxSeccionDetalle.DataSource = Nothing
        Me.cbxSeccionDetalle.DataValueMember = Nothing
        Me.cbxSeccionDetalle.dtSeleccionado = Nothing
        Me.cbxSeccionDetalle.FormABM = Nothing
        Me.cbxSeccionDetalle.Indicaciones = Nothing
        Me.cbxSeccionDetalle.Location = New System.Drawing.Point(516, 113)
        Me.cbxSeccionDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSeccionDetalle.Name = "cbxSeccionDetalle"
        Me.cbxSeccionDetalle.SeleccionMultiple = False
        Me.cbxSeccionDetalle.SeleccionObligatoria = False
        Me.cbxSeccionDetalle.Size = New System.Drawing.Size(238, 21)
        Me.cbxSeccionDetalle.SoloLectura = False
        Me.cbxSeccionDetalle.TabIndex = 26
        Me.cbxSeccionDetalle.Texto = ""
        '
        'frmAgregarAsiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(772, 689)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmAgregarAsiento"
        Me.Text = "frmAgregarAsiento"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblOperacion2 As System.Windows.Forms.Label
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents txtDetalle As ERP.ocxTXTString
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents ocxCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents lblImporteLocal As System.Windows.Forms.Label
    Friend WithEvents txtTotalCreditoLocal As ERP.ocxTXTNumeric
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents txtTotalDebitoLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtImporteLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents cbxOperacion As ERP.ocxCBX
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents cbxSucursalDetalle As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTipoComprobante As ERP.ocxTXTString
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents VerDescuentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportarAExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkBloquear As System.Windows.Forms.CheckBox
    Friend WithEvents cbxDepartamento As ocxCBX
    Friend WithEvents lblDptoConsume As Label
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents Label5 As Label
    Friend WithEvents chkGastosVarios As ocxCHK
    Friend WithEvents cbxUnidadNegocioDetalle1 As ocxCBX
    Friend WithEvents Label7 As Label
    Friend WithEvents cbxDepartamentoDetalle As ocxCBX
    Friend WithEvents Label9 As Label
    Friend WithEvents lblDptoSolicitante As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents cbxSeccion As ocxCBX
    Friend WithEvents Label4 As Label
    Friend WithEvents cbxSeccionDetalle As ocxCBX
End Class

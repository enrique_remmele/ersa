﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAsiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.lblDepatamentoSolicitante = New System.Windows.Forms.Label()
        Me.chkGastosVarios = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblInformacionCaja = New System.Windows.Forms.Label()
        Me.txtNroCaja = New ERP.ocxTXTNumeric()
        Me.lblNroCaja = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion2 = New System.Windows.Forms.Label()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cbxSeccionDetalle = New ERP.ocxCBX()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbxUnidadNegocioDetalle1 = New ERP.ocxCBX()
        Me.cbxDepartamentoDetalle = New ERP.ocxCBX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.ocxCuenta = New ERP.ocxTXTCuentaContable()
        Me.cbxSucursalDetalle = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTipoComprobante = New ERP.ocxTXTString()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.lblImporteLocal = New System.Windows.Forms.Label()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.txtImporteLocal = New ERP.ocxTXTNumeric()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.VerDescuentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtTotalCreditoLocal = New ERP.ocxTXTNumeric()
        Me.txtTotalDebitoLocal = New ERP.ocxTXTNumeric()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.chkVolverGenerar = New System.Windows.Forms.CheckBox()
        Me.chkBloquear = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbxSeccion = New ERP.ocxCBX()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(213, 45)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 13
        Me.lblComprobante.Text = "Comprobante:"
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(34, 148)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 22
        Me.lblDetalle.Text = "Detalle:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 636)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(783, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.Label8)
        Me.gbxCabecera.Controls.Add(Me.cbxSeccion)
        Me.gbxCabecera.Controls.Add(Me.lblDepatamentoSolicitante)
        Me.gbxCabecera.Controls.Add(Me.chkGastosVarios)
        Me.gbxCabecera.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxCabecera.Controls.Add(Me.cbxDepartamento)
        Me.gbxCabecera.Controls.Add(Me.Label6)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.lblInformacionCaja)
        Me.gbxCabecera.Controls.Add(Me.txtNroCaja)
        Me.gbxCabecera.Controls.Add(Me.lblNroCaja)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion2)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.Location = New System.Drawing.Point(3, 3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(777, 174)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'lblDepatamentoSolicitante
        '
        Me.lblDepatamentoSolicitante.AutoSize = True
        Me.lblDepatamentoSolicitante.Location = New System.Drawing.Point(246, 98)
        Me.lblDepatamentoSolicitante.Name = "lblDepatamentoSolicitante"
        Me.lblDepatamentoSolicitante.Size = New System.Drawing.Size(129, 13)
        Me.lblDepatamentoSolicitante.TabIndex = 24
        Me.lblDepatamentoSolicitante.Text = "Departamento Solicitante:"
        '
        'chkGastosVarios
        '
        Me.chkGastosVarios.BackColor = System.Drawing.Color.Transparent
        Me.chkGastosVarios.Color = System.Drawing.Color.Empty
        Me.chkGastosVarios.Location = New System.Drawing.Point(82, 69)
        Me.chkGastosVarios.Name = "chkGastosVarios"
        Me.chkGastosVarios.Size = New System.Drawing.Size(204, 17)
        Me.chkGastosVarios.SoloLectura = False
        Me.chkGastosVarios.TabIndex = 15
        Me.chkGastosVarios.Texto = "Asignacion de Unidad y Departamento"
        Me.chkGastosVarios.Valor = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = Nothing
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = Nothing
        Me.cbxUnidadNegocio.DataValueMember = Nothing
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(10, 115)
        Me.cbxUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(231, 21)
        Me.cbxUnidadNegocio.SoloLectura = True
        Me.cbxUnidadNegocio.TabIndex = 17
        Me.cbxUnidadNegocio.TabStop = False
        Me.cbxUnidadNegocio.Texto = ""
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(249, 115)
        Me.cbxDepartamento.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(273, 21)
        Me.cbxDepartamento.SoloLectura = True
        Me.cbxDepartamento.TabIndex = 21
        Me.cbxDepartamento.TabStop = False
        Me.cbxDepartamento.Texto = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(246, 98)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 13)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Departamento:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 98)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(102, 13)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "Unidad de Negocio:"
        '
        'lblInformacionCaja
        '
        Me.lblInformacionCaja.AutoSize = True
        Me.lblInformacionCaja.ForeColor = System.Drawing.Color.DimGray
        Me.lblInformacionCaja.Location = New System.Drawing.Point(154, 16)
        Me.lblInformacionCaja.Name = "lblInformacionCaja"
        Me.lblInformacionCaja.Size = New System.Drawing.Size(53, 13)
        Me.lblInformacionCaja.TabIndex = 2
        Me.lblInformacionCaja.Tag = "No tocar"
        Me.lblInformacionCaja.Text = "__/__/__"
        '
        'txtNroCaja
        '
        Me.txtNroCaja.Color = System.Drawing.Color.Empty
        Me.txtNroCaja.Decimales = False
        Me.txtNroCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroCaja.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtNroCaja.Location = New System.Drawing.Point(82, 11)
        Me.txtNroCaja.Margin = New System.Windows.Forms.Padding(6, 5, 6, 5)
        Me.txtNroCaja.Name = "txtNroCaja"
        Me.txtNroCaja.Size = New System.Drawing.Size(63, 22)
        Me.txtNroCaja.SoloLectura = False
        Me.txtNroCaja.TabIndex = 1
        Me.txtNroCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroCaja.Texto = "0"
        '
        'lblNroCaja
        '
        Me.lblNroCaja.AutoSize = True
        Me.lblNroCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNroCaja.Location = New System.Drawing.Point(6, 14)
        Me.lblNroCaja.Name = "lblNroCaja"
        Me.lblNroCaja.Size = New System.Drawing.Size(71, 15)
        Me.lblNroCaja.TabIndex = 0
        Me.lblNroCaja.Text = "Nro. Caja:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(274, 11)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(111, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(223, 15)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(600, 11)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(87, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 8
        Me.cbxMoneda.TabStop = False
        Me.cbxMoneda.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(551, 15)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 7
        Me.lblMoneda.Text = "Moneda:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(687, 11)
        Me.txtCotizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(83, 21)
        Me.txtCotizacion.SoloLectura = True
        Me.txtCotizacion.TabIndex = 9
        Me.txtCotizacion.TabStop = False
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 18, 14, 32, 41, 843)
        Me.txtFecha.Location = New System.Drawing.Point(451, 11)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(83, 20)
        Me.txtFecha.SoloLectura = True
        Me.txtFecha.TabIndex = 6
        Me.txtFecha.TabStop = False
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(411, 15)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 5
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(82, 41)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 11
        Me.cbxCiudad.TabStop = False
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(145, 41)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 12
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion2
        '
        Me.lblOperacion2.AutoSize = True
        Me.lblOperacion2.Location = New System.Drawing.Point(18, 45)
        Me.lblOperacion2.Name = "lblOperacion2"
        Me.lblOperacion2.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion2.TabIndex = 10
        Me.lblOperacion2.Text = "Operacion:"
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(82, 144)
        Me.txtDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(688, 21)
        Me.txtDetalle.SoloLectura = False
        Me.txtDetalle.TabIndex = 23
        Me.txtDetalle.TabStop = False
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(288, 41)
        Me.txtComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(482, 21)
        Me.txtComprobante.SoloLectura = True
        Me.txtComprobante.TabIndex = 14
        Me.txtComprobante.TabStop = False
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.gbxCabecera, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(783, 636)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.cbxSeccionDetalle)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.cbxUnidadNegocioDetalle1)
        Me.Panel3.Controls.Add(Me.cbxDepartamentoDetalle)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.btnAgregar)
        Me.Panel3.Controls.Add(Me.ocxCuenta)
        Me.Panel3.Controls.Add(Me.cbxSucursalDetalle)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.txtNroComprobante)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.txtTipoComprobante)
        Me.Panel3.Controls.Add(Me.lblDescripcion)
        Me.Panel3.Controls.Add(Me.txtDescripcion)
        Me.Panel3.Controls.Add(Me.lblCuenta)
        Me.Panel3.Controls.Add(Me.lblImporteLocal)
        Me.Panel3.Controls.Add(Me.cbxTipo)
        Me.Panel3.Controls.Add(Me.txtImporteLocal)
        Me.Panel3.Controls.Add(Me.lblTipo)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 417)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(777, 172)
        Me.Panel3.TabIndex = 3
        '
        'cbxSeccionDetalle
        '
        Me.cbxSeccionDetalle.CampoWhere = Nothing
        Me.cbxSeccionDetalle.CargarUnaSolaVez = False
        Me.cbxSeccionDetalle.DataDisplayMember = Nothing
        Me.cbxSeccionDetalle.DataFilter = Nothing
        Me.cbxSeccionDetalle.DataOrderBy = Nothing
        Me.cbxSeccionDetalle.DataSource = Nothing
        Me.cbxSeccionDetalle.DataValueMember = Nothing
        Me.cbxSeccionDetalle.dtSeleccionado = Nothing
        Me.cbxSeccionDetalle.FormABM = Nothing
        Me.cbxSeccionDetalle.Indicaciones = Nothing
        Me.cbxSeccionDetalle.Location = New System.Drawing.Point(569, 117)
        Me.cbxSeccionDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSeccionDetalle.Name = "cbxSeccionDetalle"
        Me.cbxSeccionDetalle.SeleccionMultiple = False
        Me.cbxSeccionDetalle.SeleccionObligatoria = False
        Me.cbxSeccionDetalle.Size = New System.Drawing.Size(203, 21)
        Me.cbxSeccionDetalle.SoloLectura = True
        Me.cbxSeccionDetalle.TabIndex = 21
        Me.cbxSeccionDetalle.TabStop = False
        Me.cbxSeccionDetalle.Texto = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(566, 98)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Seccion Detalle:"
        '
        'cbxUnidadNegocioDetalle1
        '
        Me.cbxUnidadNegocioDetalle1.CampoWhere = Nothing
        Me.cbxUnidadNegocioDetalle1.CargarUnaSolaVez = False
        Me.cbxUnidadNegocioDetalle1.DataDisplayMember = Nothing
        Me.cbxUnidadNegocioDetalle1.DataFilter = Nothing
        Me.cbxUnidadNegocioDetalle1.DataOrderBy = Nothing
        Me.cbxUnidadNegocioDetalle1.DataSource = Nothing
        Me.cbxUnidadNegocioDetalle1.DataValueMember = Nothing
        Me.cbxUnidadNegocioDetalle1.dtSeleccionado = Nothing
        Me.cbxUnidadNegocioDetalle1.FormABM = Nothing
        Me.cbxUnidadNegocioDetalle1.Indicaciones = Nothing
        Me.cbxUnidadNegocioDetalle1.Location = New System.Drawing.Point(18, 117)
        Me.cbxUnidadNegocioDetalle1.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocioDetalle1.Name = "cbxUnidadNegocioDetalle1"
        Me.cbxUnidadNegocioDetalle1.SeleccionMultiple = False
        Me.cbxUnidadNegocioDetalle1.SeleccionObligatoria = False
        Me.cbxUnidadNegocioDetalle1.Size = New System.Drawing.Size(242, 21)
        Me.cbxUnidadNegocioDetalle1.SoloLectura = True
        Me.cbxUnidadNegocioDetalle1.TabIndex = 14
        Me.cbxUnidadNegocioDetalle1.TabStop = False
        Me.cbxUnidadNegocioDetalle1.Texto = ""
        '
        'cbxDepartamentoDetalle
        '
        Me.cbxDepartamentoDetalle.CampoWhere = Nothing
        Me.cbxDepartamentoDetalle.CargarUnaSolaVez = False
        Me.cbxDepartamentoDetalle.DataDisplayMember = Nothing
        Me.cbxDepartamentoDetalle.DataFilter = Nothing
        Me.cbxDepartamentoDetalle.DataOrderBy = Nothing
        Me.cbxDepartamentoDetalle.DataSource = Nothing
        Me.cbxDepartamentoDetalle.DataValueMember = Nothing
        Me.cbxDepartamentoDetalle.dtSeleccionado = Nothing
        Me.cbxDepartamentoDetalle.FormABM = Nothing
        Me.cbxDepartamentoDetalle.Indicaciones = Nothing
        Me.cbxDepartamentoDetalle.Location = New System.Drawing.Point(268, 117)
        Me.cbxDepartamentoDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepartamentoDetalle.Name = "cbxDepartamentoDetalle"
        Me.cbxDepartamentoDetalle.SeleccionMultiple = False
        Me.cbxDepartamentoDetalle.SeleccionObligatoria = False
        Me.cbxDepartamentoDetalle.Size = New System.Drawing.Size(291, 21)
        Me.cbxDepartamentoDetalle.SoloLectura = True
        Me.cbxDepartamentoDetalle.TabIndex = 18
        Me.cbxDepartamentoDetalle.TabStop = False
        Me.cbxDepartamentoDetalle.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(271, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(145, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Departamento que Consume:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(21, 98)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Unidad de Negocio Detalle:"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(674, 142)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(94, 23)
        Me.btnAgregar.TabIndex = 19
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'ocxCuenta
        '
        Me.ocxCuenta.AlturaMaxima = 84
        Me.ocxCuenta.Consulta = Nothing
        Me.ocxCuenta.IDCentroCosto = 0
        Me.ocxCuenta.IDUnidadNegocio = 0
        Me.ocxCuenta.ListarTodas = False
        Me.ocxCuenta.Location = New System.Drawing.Point(17, 24)
        Me.ocxCuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.ocxCuenta.Name = "ocxCuenta"
        Me.ocxCuenta.Registro = Nothing
        Me.ocxCuenta.Resolucion173 = False
        Me.ocxCuenta.Seleccionado = False
        Me.ocxCuenta.Size = New System.Drawing.Size(561, 24)
        Me.ocxCuenta.SoloLectura = False
        Me.ocxCuenta.TabIndex = 1
        Me.ocxCuenta.Texto = Nothing
        Me.ocxCuenta.whereFiltro = Nothing
        '
        'cbxSucursalDetalle
        '
        Me.cbxSucursalDetalle.CampoWhere = Nothing
        Me.cbxSucursalDetalle.CargarUnaSolaVez = False
        Me.cbxSucursalDetalle.DataDisplayMember = Nothing
        Me.cbxSucursalDetalle.DataFilter = Nothing
        Me.cbxSucursalDetalle.DataOrderBy = Nothing
        Me.cbxSucursalDetalle.DataSource = Nothing
        Me.cbxSucursalDetalle.DataValueMember = Nothing
        Me.cbxSucursalDetalle.dtSeleccionado = Nothing
        Me.cbxSucursalDetalle.FormABM = Nothing
        Me.cbxSucursalDetalle.Indicaciones = Nothing
        Me.cbxSucursalDetalle.Location = New System.Drawing.Point(595, 73)
        Me.cbxSucursalDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursalDetalle.Name = "cbxSucursalDetalle"
        Me.cbxSucursalDetalle.SeleccionMultiple = False
        Me.cbxSucursalDetalle.SeleccionObligatoria = True
        Me.cbxSucursalDetalle.Size = New System.Drawing.Size(173, 21)
        Me.cbxSucursalDetalle.SoloLectura = False
        Me.cbxSucursalDetalle.TabIndex = 12
        Me.cbxSucursalDetalle.TabStop = False
        Me.cbxSucursalDetalle.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(598, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Sucursal:"
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(507, 73)
        Me.txtNroComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(88, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 10
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroComprobante.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(442, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Comprobante:"
        '
        'txtTipoComprobante
        '
        Me.txtTipoComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoComprobante.Color = System.Drawing.Color.Empty
        Me.txtTipoComprobante.Indicaciones = Nothing
        Me.txtTipoComprobante.Location = New System.Drawing.Point(439, 73)
        Me.txtTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTipoComprobante.Multilinea = False
        Me.txtTipoComprobante.Name = "txtTipoComprobante"
        Me.txtTipoComprobante.Size = New System.Drawing.Size(69, 21)
        Me.txtTipoComprobante.SoloLectura = False
        Me.txtTipoComprobante.TabIndex = 9
        Me.txtTipoComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoComprobante.Texto = ""
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(21, 57)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 6
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(18, 73)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(413, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 7
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(30, 10)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 0
        Me.lblCuenta.Text = "Cuenta:"
        '
        'lblImporteLocal
        '
        Me.lblImporteLocal.AutoSize = True
        Me.lblImporteLocal.Location = New System.Drawing.Point(721, 10)
        Me.lblImporteLocal.Name = "lblImporteLocal"
        Me.lblImporteLocal.Size = New System.Drawing.Size(45, 13)
        Me.lblImporteLocal.TabIndex = 4
        Me.lblImporteLocal.Text = "Importe:"
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.dtSeleccionado = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(586, 26)
        Me.cbxTipo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionMultiple = False
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(90, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'txtImporteLocal
        '
        Me.txtImporteLocal.Color = System.Drawing.Color.Empty
        Me.txtImporteLocal.Decimales = True
        Me.txtImporteLocal.Indicaciones = Nothing
        Me.txtImporteLocal.Location = New System.Drawing.Point(676, 26)
        Me.txtImporteLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtImporteLocal.Name = "txtImporteLocal"
        Me.txtImporteLocal.Size = New System.Drawing.Size(90, 22)
        Me.txtImporteLocal.SoloLectura = False
        Me.txtImporteLocal.TabIndex = 5
        Me.txtImporteLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteLocal.Texto = "0"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(586, 10)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 2
        Me.lblTipo.Text = "Tipo:"
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.ContextMenuStrip = Me.ContextMenuStrip1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 183)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv.RowHeadersVisible = False
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(777, 189)
        Me.dgv.StandardTab = True
        Me.dgv.TabIndex = 1
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerDescuentosToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(158, 48)
        '
        'VerDescuentosToolStripMenuItem
        '
        Me.VerDescuentosToolStripMenuItem.Name = "VerDescuentosToolStripMenuItem"
        Me.VerDescuentosToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.VerDescuentosToolStripMenuItem.Text = "Ver detalle"
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.txtTotalCreditoLocal, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtTotalDebitoLocal, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtSaldo, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 378)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(777, 33)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'txtTotalCreditoLocal
        '
        Me.txtTotalCreditoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalCreditoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalCreditoLocal.Decimales = True
        Me.txtTotalCreditoLocal.Indicaciones = Nothing
        Me.txtTotalCreditoLocal.Location = New System.Drawing.Point(681, 4)
        Me.txtTotalCreditoLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalCreditoLocal.Name = "txtTotalCreditoLocal"
        Me.txtTotalCreditoLocal.Size = New System.Drawing.Size(90, 18)
        Me.txtTotalCreditoLocal.SoloLectura = True
        Me.txtTotalCreditoLocal.TabIndex = 3
        Me.txtTotalCreditoLocal.TabStop = False
        Me.txtTotalCreditoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCreditoLocal.Texto = "0"
        '
        'txtTotalDebitoLocal
        '
        Me.txtTotalDebitoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalDebitoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalDebitoLocal.Decimales = True
        Me.txtTotalDebitoLocal.Indicaciones = Nothing
        Me.txtTotalDebitoLocal.Location = New System.Drawing.Point(581, 4)
        Me.txtTotalDebitoLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalDebitoLocal.Name = "txtTotalDebitoLocal"
        Me.txtTotalDebitoLocal.Size = New System.Drawing.Size(90, 18)
        Me.txtTotalDebitoLocal.SoloLectura = True
        Me.txtTotalDebitoLocal.TabIndex = 2
        Me.txtTotalDebitoLocal.TabStop = False
        Me.txtTotalDebitoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebitoLocal.Texto = "0"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(481, 4)
        Me.txtSaldo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(90, 18)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 1
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblSaldo)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(471, 27)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'lblSaldo
        '
        Me.lblSaldo.Location = New System.Drawing.Point(431, 0)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 22)
        Me.lblSaldo.TabIndex = 0
        Me.lblSaldo.Text = "Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnAceptar)
        Me.Panel1.Controls.Add(Me.chkVolverGenerar)
        Me.Panel1.Controls.Add(Me.chkBloquear)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 595)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(777, 38)
        Me.Panel1.TabIndex = 4
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(687, 12)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(606, 12)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'chkVolverGenerar
        '
        Me.chkVolverGenerar.AutoSize = True
        Me.chkVolverGenerar.Location = New System.Drawing.Point(22, 18)
        Me.chkVolverGenerar.Name = "chkVolverGenerar"
        Me.chkVolverGenerar.Size = New System.Drawing.Size(104, 17)
        Me.chkVolverGenerar.TabIndex = 0
        Me.chkVolverGenerar.Text = "Volver a generar"
        Me.chkVolverGenerar.UseVisualStyleBackColor = True
        '
        'chkBloquear
        '
        Me.chkBloquear.AutoSize = True
        Me.chkBloquear.Location = New System.Drawing.Point(132, 18)
        Me.chkBloquear.Name = "chkBloquear"
        Me.chkBloquear.Size = New System.Drawing.Size(68, 17)
        Me.chkBloquear.TabIndex = 1
        Me.chkBloquear.Text = "Bloquear"
        Me.chkBloquear.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(527, 98)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "Seccion:"
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = Nothing
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = Nothing
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = Nothing
        Me.cbxSeccion.DataSource = Nothing
        Me.cbxSeccion.DataValueMember = Nothing
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(530, 115)
        Me.cbxSeccion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(240, 21)
        Me.cbxSeccion.SoloLectura = True
        Me.cbxSeccion.TabIndex = 25
        Me.cbxSeccion.TabStop = False
        Me.cbxSeccion.Texto = ""
        '
        'frmAsiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(783, 658)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmAsiento"
        Me.Text = "frmAsiento"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents txtDetalle As ERP.ocxTXTString
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblOperacion2 As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ocxCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents lblImporteLocal As System.Windows.Forms.Label
    Friend WithEvents txtTotalCreditoLocal As ERP.ocxTXTNumeric
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents txtTotalDebitoLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtImporteLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents chkVolverGenerar As System.Windows.Forms.CheckBox
    Friend WithEvents chkBloquear As System.Windows.Forms.CheckBox
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents VerDescuentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportarAExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents cbxSucursalDetalle As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTipoComprobante As ERP.ocxTXTString
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblInformacionCaja As System.Windows.Forms.Label
    Friend WithEvents txtNroCaja As ERP.ocxTXTNumeric
    Friend WithEvents lblNroCaja As System.Windows.Forms.Label
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents cbxDepartamento As ocxCBX
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents cbxUnidadNegocioDetalle1 As ocxCBX
    Friend WithEvents cbxDepartamentoDetalle As ocxCBX
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents chkGastosVarios As ocxCHK
    Friend WithEvents lblDepatamentoSolicitante As Label
    Friend WithEvents cbxSeccionDetalle As ocxCBX
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents cbxSeccion As ocxCBX
End Class

﻿Public Class frmModificarAsiento

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CCaja As New CCaja

    'EVENTOS

    'PROPIEDADES
    Private CAsientoValue As New CAsiento
    Public Property CAsiento() As CAsiento
        Get
            Return CAsientoValue
        End Get
        Set(ByVal value As CAsiento)
            CAsientoValue = value
        End Set
    End Property

    Private oRowCabeceraValue As DataRow
    Public Property oRowCabecera() As DataRow
        Get
            Return oRowCabeceraValue
        End Get
        Set(ByVal value As DataRow)
            oRowCabeceraValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private IDTransaccionOperacionValue As Integer
    Public Property IDTransaccionOperacion() As Integer
        Get
            Return IDTransaccionOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionOperacionValue = value
        End Set
    End Property

    Private VolverAGenerarValue As Boolean
    Public Property VolverAGenerar() As Boolean
        Get
            Return VolverAGenerarValue
        End Get
        Set(ByVal value As Boolean)
            VolverAGenerarValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private ActualizandoValue As Boolean
    Public Property Actualizando() As Boolean
        Get
            Return ActualizandoValue
        End Get
        Set(ByVal value As Boolean)
            ActualizandoValue = value
        End Set
    End Property

    Private FiltrarValue As Boolean
    Public Property Filtrar() As Boolean
        Get
            Return FiltrarValue
        End Get
        Set(ByVal value As Boolean)
            FiltrarValue = value
        End Set
    End Property

    Private whereFiltrovalue As String
    Public Property whereFiltro() As String
        Get
            Return whereFiltrovalue
        End Get
        Set(ByVal value As String)
            whereFiltrovalue = value
        End Set
    End Property

    'VARIABLES
    Dim CodigoSeleccionado As String
    Dim IDDetalleSeleccionado As String
    Dim IDUnidadNegocio As Integer
    Dim IDCentroCosto As Integer
    Dim Seccion As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        FGEstiloFormulario(Me)

        'Controles
        If Filtrar = True Then
            ocxCuenta.whereFiltro = whereFiltrovalue
        Else
            ocxCuenta.whereFiltro = ""
        End If
        ocxCuenta.Conectar()

        'Funciones
        CargarInformacion()

        ListarDetalle()
        'CAsiento.ListarDetalle(dgv)

        'Foco
        ocxCuenta.Focus()
        ocxCuenta.txtDescripcion.Focus()

    End Sub

    Sub CargarInformacion()

        'Detalle Asiento
        'CAsiento.dtDetalleAsiento = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleAsiento").Clone

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.GetTable("VCiudadSucursal").Copy, "IDCiudad", "CodigoCiudad")

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal").Copy, "ID", "Codigo")
        CSistema.SqlToComboBox(cbxSucursalDetalle.cbx, CData.GetTable("VSucursal").Copy, "ID", "Codigo")

        ''Unidad de negocio
        'CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("VUnidadNegocio").Copy, "IDUnidadNegocio", "Descripcion")
        'cbxUnidadNegocio.cbx.SelectedItem = 0

        'Unidad de negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, "Select ID, Descripcion From UnidadNegocio Where Estado = 1 order by 2")
        cbxUnidadNegocio.cbx.SelectedItem = 0
        cbxUnidadNegocio.cbx.Text = ""

        'Unidad de negocio
        CSistema.SqlToComboBox(cbxUnidadNegocioDetalle1.cbx, "Select ID, Descripcion From UnidadNegocio Where Estado = 1 order by 2")
        cbxUnidadNegocioDetalle1.cbx.SelectedItem = 0
        cbxUnidadNegocioDetalle1.cbx.Text = ""

        If cbxSucursal.GetValue = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
            CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamento.cbx.Text = ""
        End If

        If cbxSucursal.GetValue = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            CSistema.SqlToComboBox(cbxDepartamentoDetalle.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamentoDetalle.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
            CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamentoDetalle.cbx.Text = ""
        End If

        ''Centro de Costo
        'CSistema.SqlToComboBox(cbxCentroCostoDetalle.cbx, CData.GetTable("VCentroCosto", "IDUnidadNegocio=" & CStr(cbxUnidadNegocio.cbx.SelectedValue) & " And Estado='True'").Copy, "ID", "Descripcion")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, CData.GetTable("VMoneda").Copy, "ID", "Referencia")

        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Centro de Costos
        CSistema.SqlToComboBox(cbxCentroCosto.cbx, CData.GetTable("VCentroCosto").Copy, "ID", "Descripcion")

        'Detalle
        dgv.Columns.Add("colCodigo", "Codigo")
        dgv.Columns.Add("colDescripcion", "Descripcion")
        dgv.Columns.Add("colObservacion", "Observacion")
        'FA 15/02/2023
        dgv.Columns.Add("colDetalle", "Detalle")

        dgv.Columns.Add("colDebito", "Debito")
        dgv.Columns.Add("colCredito", "Credito")

        dgv.Columns.Add("colID", "ID")

        dgv.Columns.Add("colIDUnidadNegocio", "IDUnidadNegocio")
        dgv.Columns.Add("colIDCentroCosto", "IDCentroCosto")



        dgv.Columns("colCodigo").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        'dgv.Columns("colDescripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colDescripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("colObservacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("colDebito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colCredito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        dgv.Columns("colDebito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("colCredito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgv.Columns("ColIDUnidadNegocio").Visible = False
        dgv.Columns("ColIDCentroCosto").Visible = False

        dgv.Columns("colID").Visible = False

        'Operacion
        cbxTipo.cbx.Items.Add("DEBITO")
        cbxTipo.cbx.Items.Add("CREDITO")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        If CAsiento Is Nothing Then
            Exit Sub
        End If

        If CAsiento.dtAsiento.Rows.Count = 0 Then
            Exit Sub
        End If

        'Cargar la Cabecera
        cbxCiudad.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDCiudad").ToString
        cbxSucursal.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDSucursal").ToString
        txtID.SetValue(CAsiento.dtAsiento.Rows(0)("Numero").ToString)
        txtFecha.SetValue(CAsiento.dtAsiento.Rows(0)("Fecha").ToString)
        cbxMoneda.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDMoneda").ToString
        txtCotizacion.txt.Text = CAsiento.dtAsiento.Rows(0)("Cotizacion").ToString
        txtTipoComprobanteCabecera.txt.Text = CAsiento.dtAsiento.Rows(0)("TipoComprobante").ToString
        txtNroComprobanteCabecera.txt.Text = CAsiento.dtAsiento.Rows(0)("NroComprobante").ToString
        txtDetalle.txt.Text = CAsiento.dtAsiento.Rows(0)("Detalle").ToString
        CCaja.Numero = CAsiento.dtAsiento.Rows(0)("NroCaja")

        'Plan de cuenta 13/02/2023
        chkGastosMultiples.chk.Checked = CAsiento.dtAsiento.Rows(0)("GastosMultiples").ToString
        chkGastosMultiples.Enabled = False
        If CAsiento.dtAsiento.Rows(0)("IDOperacion").ToString = 54 Then
            txtDepartamento.Visible = False
            cbxDepartamento.Visible = True
            txtUnidadNegocio.Visible = False
            cbxUnidadNegocio.Visible = True
            txtSeccion.Visible = False
            cbxSeccion.Visible = True
            If cbxSucursal.GetValue = 1 Then
                Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal in (1,4)")
                CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
                'cbxDepartamento.cbx.Text = ""
            Else
                Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
                CSistema.SqlToComboBox(cbxDepartamentoDetalle.cbx, dtDepartamentos, "ID", "Descripcion")
                'cbxDepartamentoDetalle.cbx.Text = ""
            End If
        End If

        'txtDepartamento.txt.Text = CAsiento.dtAsiento.Rows(0)("Departamento").ToString
        If chkGastosMultiples.chk.Checked = False Then
            cbxUnidadNegocioDetalle1.SoloLectura = True
            cbxDepartamentoDetalle.SoloLectura = True
            cbxSeccionDetalle.SoloLectura = True
            cbxUnidadNegocio.cbx.Text = CAsiento.dtAsiento.Rows(0)("UnidadNegocio").ToString
            cbxDepartamento.cbx.Text = CAsiento.dtAsiento.Rows(0)("Departamento").ToString
        Else
            cbxUnidadNegocioDetalle1.SoloLectura = False
            cbxDepartamentoDetalle.SoloLectura = False
            cbxSeccionDetalle.SoloLectura = False
        End If

        CargarInformacionCaja()

        'Bloquear controles
        cbxCiudad.Locked = False
        cbxSucursal.Locked = False
        txtID.SoloLectura = True
        cbxMoneda.SoloLectura = True
        txtCotizacion.SoloLectura = True

    End Sub

    Sub CargarInformacionCaja()

        lblInformacionCaja.Text = ""

        If CCaja.Numero = 0 Then
            Exit Sub
        End If

        txtNroCaja.SetValue(CCaja.Numero)
        lblInformacionCaja.Text = CCaja.Estado

    End Sub

    Sub CargarItem()

        'Validar
        'Cuenta
        If ocxCuenta.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente la cuenta!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Importe
        If IsNumeric(txtImporteLocal.ObtenerValor) = False Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtImporteLocal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If cbxTipo.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'FA 01/12/2022 - Plan de cuenta
        If chkGastosMultiples.chk.Checked = True Then
            If cbxUnidadNegocioDetalle1.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar una Unidad de Negocio", ctrError, btnAgregar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If

            'If cbxCentroCostoDetalle1.cbx.Text = "" Then
            '    CSistema.MostrarError("Debe de seleccionar un Centro de Costo", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            '    Exit Sub
            'End If

            If cbxDepartamentoDetalle.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar un Departamento", ctrError, btnAgregar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = CAsiento.dtDetalleAsiento.NewRow

        If Actualizando = True Then

            Dim Observacion As String = dgv.SelectedRows(0).Cells("colObservacion").Value

            If Observacion = "" Then
                Observacion = txtDetalle.GetValue
            End If

            For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Select("Codigo='" & CodigoSeleccionado & "' And Observacion='" & Observacion & "' ")
                dRow = oRow
            Next
        Else
            dRow = CAsiento.dtDetalleAsiento.NewRow()
        End If

        dRow("IDTransaccion") = 0
        dRow("IDCuentaContable") = ocxCuenta.Registro("ID").ToString
        dRow("Cuenta") = ocxCuenta.Registro("Cuenta").ToString
        dRow("Codigo") = ocxCuenta.Registro("Codigo").ToString
        dRow("Descripcion") = ocxCuenta.Registro("Descripcion").ToString
        dRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
        dRow("Orden") = CAsiento.dtDetalleAsiento.Rows.Count + 10

        Dim Importe As Decimal = CDec(txtImporteLocal.ObtenerValor)

        'Verificar la moneda
        If CAsiento.dtAsiento.Rows(0)("IDMoneda") <> 1 Then
            Importe = Importe * CAsiento.dtAsiento.Rows(0)("Cotizacion")
        End If

        If cbxTipo.cbx.Text = "CREDITO" Then
            dRow("Credito") = Importe
            dRow("Debito") = 0
        End If

        If cbxTipo.cbx.Text = "DEBITO" Then
            dRow("Credito") = 0
            dRow("Debito") = Importe
        End If

        dRow("Importe") = Importe
        If txtDescripcion.GetValue = "" Then
            dRow("Observacion") = txtDetalle.GetValue
        Else
            dRow("Observacion") = txtDescripcion.GetValue
        End If

        dRow("IDSucursal") = cbxSucursalDetalle.GetValue
        dRow("TipoComprobante") = txtTipoComprobante.GetValue
        dRow("NroComprobante") = txtNroComprobante.GetValue

        'FA 30/11/2022 - Plan de cuenta
        If chkGastosMultiples.chk.Checked = True Then
            dRow("IDUnidadNegocioDetalle") = cbxUnidadNegocioDetalle1.GetValue
            dRow("UnidadNegocioDetalle") = cbxUnidadNegocioDetalle1.cbx.Text
            dRow("IDCentroCostoDetalle") = ocxCuenta.IDCentroCosto
            'dRow("CentroCostoDetalle") = cbxCentroCostoDetalle1.cbx.Text
            dRow("IDDepartamentoEmpresa") = cbxDepartamentoDetalle.GetValue
            dRow("Departamento") = cbxDepartamentoDetalle.cbx.Text
            dRow("Detalle") = cbxUnidadNegocioDetalle1.cbx.Text & " - " & cbxDepartamentoDetalle.cbx.Text & " - " & cbxSeccionDetalle.cbx.Text
            dRow("IDSeccionDetalle") = cbxSeccionDetalle.GetValue
            dRow("Seccion") = cbxSeccionDetalle.cbx.Text
        Else
            dRow("IDCentroCosto") = ocxCuenta.IDCentroCosto
            dRow("IDCentroCostoDetalle") = ocxCuenta.IDCentroCosto
            dRow("IDDepartamentoEmpresa") = cbxDepartamento.GetValue
            dRow("Departamento") = cbxDepartamento.cbx.Text
            dRow("Detalle") = cbxUnidadNegocio.cbx.Text & " - " & cbxDepartamento.cbx.Text & " - " & cbxSeccion.cbx.Text
            dRow("IDSeccionDetalle") = cbxSeccion.GetValue
            dRow("Seccion") = cbxSeccion.cbx.Text
        End If

        'dRow("IDUnidadNegocio") = cbxUnidadNegocio.GetValue
        'dRow("IDCentroCosto") = cbxCentroCostoDetalle.GetValue

        If Actualizando = False Then
            CAsiento.dtDetalleAsiento.Rows.Add(dRow)
        End If

        'Listar Detalle
        ListarDetalle()
        'CAsiento.ListarDetalle(dgv)

        'Caluclar Totales
        CalcularTotales()

        PosicionarAutomaticamente()

        'Preguntar para salir
        If txtSaldo.ObtenerValor = "0" And dgv.Rows.Count > 0 Then
            If MessageBox.Show("Cerrar el asiento?", "Asiento", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                Guardar()
            End If
        End If

        'Inicializar controles
        ocxCuenta.txtDescripcion.Clear()
        txtImporteLocal.txt.Clear()
        ocxCuenta.Focus()
        Actualizando = False

        cbxUnidadNegocioDetalle1.cbx.Text = ""
        cbxDepartamentoDetalle.cbx.Text = ""

    End Sub

    Sub ListarDetalle()


        'Limpiamos todo el detalle
        dgv.Rows.Clear()

        'Volver a Ordenar
        CData.OrderDataTable(CAsiento.dtDetalleAsiento, "Orden")
        'FA 14/03/2023
        'CAsiento.AgruparDetalle()

        'Cargamos registro por registro
        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows

            Dim Registro(6) As String
            Registro(0) = oRow("Codigo").ToString
            Registro(1) = oRow("Descripcion").ToString

            'Solo mostrar si la observacion es distinta al del asiento
            Dim Observacion As String = ""
            'If oRow("Observacion").ToString <> txtDetalle.GetValue Then
            Observacion = oRow("Observacion").ToString
            'End If

            Registro(2) = Observacion
            Registro(3) = oRow("Detalle").ToString
            Registro(4) = CSistema.FormatoNumero(oRow("Debito").ToString)
            Registro(5) = CSistema.FormatoMoneda(oRow("Credito").ToString)
            Registro(6) = oRow("Orden").ToString
            'Registro(5) = oRow("IDUnidadNegocio")
            'Registro(6) = oRow("IDCentroCosto")

            dgv.Rows.Add(Registro)

        Next

        CalcularTotales()


    End Sub

    Public Sub CalcularTotales()

        'CSistema.TotalesGrid(dgv, txtTotalDebitoLocal.txt, 3)
        'CSistema.TotalesGrid(dgv, txtTotalCreditoLocal.txt, 4)

        CSistema.TotalesGrid(dgv, txtTotalDebitoLocal.txt, 4)
        CSistema.TotalesGrid(dgv, txtTotalCreditoLocal.txt, 5)

        'Hayar el saldo
        Dim Saldo As Decimal = 0
        Saldo = CDec(txtTotalCreditoLocal.ObtenerValor) - CDec(txtTotalDebitoLocal.ObtenerValor)

        txtSaldo.txt.Text = Saldo


    End Sub

    Sub EliminarItem()

        'Validar
        If dgv.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgv, mensaje)
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
        '    If oRow("Codigo") = dgv.SelectedRows(0).Cells("colCodigo").Value And oRow("ID") = IDDetalleSeleccionado Then
        '        'If oRow("Codigo") = dgv.CurrentRow.Cells("Codigo").Value And oRow("ID") = IDDetalleSeleccionado Then
        '        Dim Observacion As String = dgv.SelectedRows(0).Cells("colObservacion").Value

        '        If Observacion = "" Then
        '            Observacion = oRow("Observacion")
        '        End If

        '        'If Observacion = oRow("Observacion") Then
        '        'If oRow("Codigo") = dgv.SelectedRows(0).Cells("colCodigo").Value And oRow("ID") = IDDetalleSeleccionado Then
        '        CAsiento.dtDetalleAsiento.Rows.Remove(oRow)
        '        Exit For
        '        'End If

        '    End If
        'Next

        CAsiento.dtDetalleAsiento.Rows.RemoveAt(dgv.SelectedRows(0).Index)


        ''Volver a enumerar los ID
        'Dim Index As Integer = 1
        'For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
        '    oRow("ID") = Index
        '    Index = Index + 1
        'Next

        ListarDetalle()
        CalcularTotales()

    End Sub

    Sub SeleccionarItem()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim Observacion As String = dgv.SelectedRows(0).Cells("colObservacion").Value

        If Observacion = "" Then
            Observacion = txtDetalle.GetValue
        End If

        'For Each orow As DataRow In CAsiento.dtDetalleAsiento.Select(" Codigo = '" & CodigoSeleccionado & "' And Orden= " & IDUnidadNegocio & " And IDCentroCosto= " & IDCentroCosto) ' & " And Observacion = '" & Observacion & "'")
        For Each orow As DataRow In CAsiento.dtDetalleAsiento.Select(" Codigo = '" & CodigoSeleccionado & "' And Orden= " & dgv.SelectedRows(0).Cells("colID").Value)

            ocxCuenta.SeleccionarRegistro(orow("Codigo"))

            If orow("Credito") > 0 Then
                cbxTipo.cbx.SelectedIndex = 1
                txtImporteLocal.SetValue(orow("Credito"))
            End If

            If orow("Debito") > 0 Then
                cbxTipo.cbx.SelectedIndex = 0
                txtImporteLocal.SetValue(orow("Debito"))
            End If

            txtDescripcion.SetValue(orow("Observacion").ToString)
            txtTipoComprobante.SetValue(orow("TipoComprobante").ToString)
            txtNroComprobante.SetValue(orow("NroComprobante").ToString)

            cbxSucursalDetalle.cbx.Text = orow("CodSucursal").ToString

            'cbxUnidadNegocio.cbx.SelectedValue = orow("IDUnidadNegocio")
            'cbxCentroCosto.cbx.SelectedValue = orow("IDCentroCosto")

        Next

        Actualizando = True

        ocxCuenta.txtCodigo.Focus()

    End Sub

    Sub Procesar()

        'Validar
        'Detalle
        If CAsiento.dtAsiento.Rows.Count = 0 Then
            Dim mensaje As String = "Ingrese el detalle!"
            ctrError.SetError(btnAgregar, mensaje)
            ctrError.SetIconAlignment(btnAgregar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Montos
        Dim Debitos As Decimal = txtTotalDebitoLocal.ObtenerValor
        Dim Creditos As Decimal = txtTotalCreditoLocal.ObtenerValor
        Dim Saldo As Decimal = Creditos - Debitos

        If Saldo <> 0 Then
            Dim mensaje As String = "Ingrese el detalle!"
            ctrError.SetError(btnAgregar, mensaje)
            ctrError.SetIconAlignment(btnAgregar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Total

    End Sub

    Sub Salir()

        Procesado = False
        Me.Close()

    End Sub

    Sub Guardar()

        'Validar
        'Si el saldo es incorrecto
        If txtSaldo.ObtenerValor <> 0 Then
            CSistema.MostrarError("Los importes no concuerdan!", ctrError, btnGuardar, tsslEstado)
            Exit Sub
        End If

        'Si el importe es incorrecto
        If txtTotalCreditoLocal.ObtenerValor = 0 Then
            CSistema.MostrarError("El asiento no puede ser 0!", ctrError, btnGuardar, tsslEstado)
            Exit Sub
        End If

        'Caja
        'If CCaja.Seleccionado = False Then
        '    Dim mensaje As String = "Seleccione correctamente la caja correspondiente!"
        '    MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'End If

        'If CCaja.Habilitado = False Then
        '    Dim mensaje As String = "La caja no esta habilitada! Seleccione otro numero de caja."
        '    MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'End If

        'Actualizar la Cabecera
        CAsiento.dtAsiento(0)("Detalle") = txtDetalle.GetValue
        'CAsiento.dtAsiento(0)("Comprobante") = txtTipoComprobanteCabecera.GetValue & " " & txtNroComprobanteCabecera.GetValue
        'CAsiento.dtAsiento(0)("NroComprobante") = txtNroComprobanteCabecera.GetValue
        CAsiento.dtAsiento(0)("IDCentroCosto") = cbxCentroCosto.GetValue
        CAsiento.NroCaja = txtNroCaja.ObtenerValor
        CAsiento.IDCentroCosto = cbxCentroCosto.GetValue
        CAsiento.Bloquear = chkBloquear.Checked.ToString

        CAsiento.dtAsiento(0)("IDUnidadNegocio") = cbxUnidadNegocio.GetValue
        CAsiento.dtAsiento(0)("IDDepartamento") = cbxDepartamento.GetValue
        CAsiento.dtAsiento(0)("IDSeccion") = cbxSeccion.GetValue
        If CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.UPD) = True Then
            Procesado = True
            Me.Close()
        End If

    End Sub

    Sub PosicionarAutomaticamente()

        Dim Debito As Decimal = txtTotalDebitoLocal.ObtenerValor
        Dim Credito As Decimal = txtTotalCreditoLocal.ObtenerValor

        If Credito > Debito Then
            cbxTipo.cbx.SelectedIndex = 0
        Else
            cbxTipo.cbx.SelectedIndex = 1
        End If

    End Sub

    Sub AperturaCaja()

        Dim frm As New frmAperturaCaja
        FGMostrarFormulario(Me, frm, "Apertura", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        CCaja.ObtenerUltimoNumero()
        CargarInformacionCaja()

    End Sub

    Private Sub frmAsiento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If dgv.Focused = True Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAsiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub ocxCuenta_ItemSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxCuenta.ItemSeleccionado
        If ocxCuenta.Seleccionado = True Then

            PosicionarAutomaticamente()
            cbxTipo.cbx.Focus()

            'cbxUnidadNegocio.cbx.SelectedValue = ocxCuenta.IDUnidadNegocio
            'cbxCentroCostoDetalle.cbx.SelectedValue = ocxCuenta.IDCentroCosto

        End If
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            EliminarItem()
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        CargarItem()
    End Sub

    Private Sub VerDescuentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDescuentosToolStripMenuItem.Click

    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(CAsiento.dtDetalleAsiento)
    End Sub

    Private Sub txtDescripcion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            If txtDescripcion.GetValue.Length <= 1 Then
                txtDescripcion.SetValue(txtDetalle.GetValue)
                CSistema.SelectNextControl(Me, Keys.Enter)
            End If
        End If
    End Sub

    Private Sub txtTipoComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtTipoComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            txtTipoComprobante.SetValue(CAsiento.dtAsiento.Rows(0)("TipoComprobante").ToString)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            txtNroComprobante.SetValue(CAsiento.dtAsiento.Rows(0)("NroComprobante").ToString)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If
    End Sub

    Private Sub txtImporteLocal_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteLocal.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            txtImporteLocal.SetValue(CAsiento.dtAsiento.Rows(0)("Total").ToString)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If


        If e.KeyCode = Keys.Multiply Then
            Dim Saldo As Decimal = txtSaldo.ObtenerValor
            If Saldo < 0 Then
                Saldo = Saldo * -1
            End If
            txtImporteLocal.SetValue(Saldo)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If

    End Sub

    Private Sub dgv_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp

        If e.KeyCode = Keys.Delete Then
            EliminarItem()
        End If

        If e.KeyCode = Keys.F2 Then
            SeleccionarItem()
        End If

    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Salir()
    End Sub

    Private Sub dgv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown
        If e.KeyCode = Keys.Enter Then

            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgv_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged

        ocxCuenta.Clear()
        txtImporteLocal.SetValue(0)
        txtDescripcion.SetValue("")
        txtTipoComprobante.SetValue("")
        txtNroComprobante.SetValue("")
        cbxSucursal.cbx.Text = ""

        Actualizando = False
        CodigoSeleccionado = dgv.CurrentRow.Cells("colCodigo").Value
        IDDetalleSeleccionado = dgv.CurrentRow.Cells("colID").Value
        IDUnidadNegocio = dgv.CurrentRow.Cells("ColIDUnidadNegocio").Value
        IDCentroCosto = dgv.CurrentRow.Cells("ColIDCentroCosto").Value

    End Sub

    Private Sub txtNroCaja_Leave(sender As System.Object, e As System.EventArgs) Handles txtNroCaja.Leave
        CCaja.IDSucursal = cbxSucursal.GetValue
        CCaja.Obtener(txtNroCaja.ObtenerValor)
        CargarInformacionCaja()
    End Sub

    Private Sub txtNroCaja_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroCaja.TeclaPrecionada

        If e.KeyCode = Keys.Add Then
            AperturaCaja()
        End If

        If e.KeyCode = Keys.End Then
            CCaja.ObtenerUltimoNumero()
            CargarInformacionCaja()
        End If

    End Sub

    Private Sub cbxUnidadNegocio_PropertyChanged(sender As Object, e As System.EventArgs)
        'Dim IDUnidadNegocio As String = CStr(cbxUnidadNegocio.cbx.SelectedValue)
        'If IDUnidadNegocio Is Nothing Then
        '    IDUnidadNegocio = 0
        'End If

        'CSistema.SqlToComboBox(cbxCentroCostoDetalle.cbx, CData.GetTable("VCentroCosto", "IDUnidadNegocio=" & CStr(IDUnidadNegocio) & " And Estado='True'").Copy, "ID", "Descripcion")
        'If ocxCuenta.IDUnidadNegocio > 0 Then
        '    cbxCentroCostoDetalle.cbx.SelectedValue = ocxCuenta.IDUnidadNegocio
        'End If
    End Sub

    'FA 28/05/2021
    Sub frmModificarAsiento_Activate()
        Me.Refresh()
    End Sub

    Private Sub cbxDepartamentoDetalle_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamentoDetalle.PropertyChanged
        Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamentoDetalle.cbx.SelectedValue)
        If Seccion = True Then
            Dim dtSeccionDetalle As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamentoDetalle.cbx.SelectedValue)
            CSistema.SqlToComboBox(cbxSeccionDetalle.cbx, dtSeccionDetalle, "ID", "Descripcion")
            cbxSeccionDetalle.cbx.Text = ""
            cbxSeccionDetalle.SoloLectura = False
        Else
            cbxSeccionDetalle.SoloLectura = True
        End If
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamento.PropertyChanged
        Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamento.cbx.SelectedValue)
        If Seccion = True Then
            Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamento.cbx.SelectedValue)
            CSistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
            cbxSeccion.SoloLectura = False
            cbxSeccion.cbx.Text = ""
        Else
            cbxSeccion.SoloLectura = True
            cbxSeccion.cbx.Text = ""
        End If
    End Sub
End Class
﻿Public Class frmAgregarAsiento

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS

    'PROPIEDADES
    Private CAsientoValue As New CAsiento
    Public Property CAsiento() As CAsiento
        Get
            Return CAsientoValue
        End Get
        Set(ByVal value As CAsiento)
            CAsientoValue = value
        End Set
    End Property

    Private oRowCabeceraValue As DataRow
    Public Property oRowCabecera() As DataRow
        Get
            Return oRowCabeceraValue
        End Get
        Set(ByVal value As DataRow)
            oRowCabeceraValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private IDTransaccionOperacionValue As Integer
    Public Property IDTransaccionOperacion() As Integer
        Get
            Return IDTransaccionOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionOperacionValue = value
        End Set
    End Property

    Private VolverAGenerarValue As Boolean
    Public Property VolverAGenerar() As Boolean
        Get
            Return VolverAGenerarValue
        End Get
        Set(ByVal value As Boolean)
            VolverAGenerarValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Public Property IDTransaccionGuardado As Integer = 0
    Public Property IDUnidadNegocio As Integer
    Public Property IDCentroCosto As Integer
    Public Property IDDepartamento As Integer
    Public Property Seccion As Boolean

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        FGEstiloFormulario(Me)

        'Controles
        ocxCuenta.Conectar()
        CAsiento.Inicializar()

        'Funciones
        CargarInformacion()

        'Foco
        txtID.txt.Focus()

    End Sub

    Sub CargarInformacion()

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.GetTable("VCiudadSucursal").Copy, "IDCiudad", "CodigoCiudad")

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal").Copy, "ID", "Codigo")
        CSistema.SqlToComboBox(cbxSucursalDetalle.cbx, CData.GetTable("VSucursal").Copy, "ID", "Codigo")

        '30/11/2022 FA - Plan de cuenta
        'Unidad de negocio
        'CSistema.SqlToComboBox(cbxUnidadNegocioDetalle.cbx, CData.GetTable("VUnidadNegocio").Copy, "ID", "Descripcion")
        'CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, "Select ID, Descripcion From VUnidadNegocio Order By 2")
        'cbxUnidadNegocioDetalle.cbx.SelectedValue = 0

        'Centro de Costo
        'CSistema.SqlToComboBox(cbxCentroCostoDetalle.cbx, CData.GetTable("VCentroCosto", "IDUnidadNegocio=" & CStr(cbxUnidadNegocio.cbx.SelectedValue) & " And Estado='True'").Copy, "ID", "Descripcion")
        ''CSistema.SqlToComboBox(cbxCentroCostoDetalle.cbx, "Select ID, Descripcion From VCentroCosto Where IDUnidadNegocio=" & cbxUnidadNegocio.cbx.SelectedValue & "Order By 2")
        'cbxCentroCosto.cbx.SelectedValue = 0

        'Operacion
        CSistema.SqlToComboBox(cbxOperacion, "Select ID, Descripcion from Operacion where FormName is null or ID = 36")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, CData.GetTable("VMoneda").Copy, "ID", "Referencia")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        txtCotizacion.txt.Text = 1
        'Centro de Costos
        'CSistema.SqlToComboBox(cbxCentroCosto.cbx, CData.GetTable("VCentroCosto").Copy, "ID", "Descripcion")

        'Unidad de Negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, "Select ID, Descripcion From UnidadNegocio Where Estado = 1 order by 2")

        'Centro de Costo
        'CSistema.SqlToComboBox(cbxCentroCosto.cbx, "Select ID, Descripcion From CentroCosto Where Estado = 1 order by 2")

        'Departamento
        'CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Departamento From DepartamentoEmpresa order by 2")
        Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'") 'and IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
        'cbxDepartamento.cbx.Text = ""
        'Unidad de Negocio Detalle
        CSistema.SqlToComboBox(cbxUnidadNegocioDetalle1.cbx, "Select ID, Descripcion From UnidadNegocio Where Estado = 1 order by 2")

        'FA 23/03/2023
        'Centro de Costo Detalle
        'CSistema.SqlToComboBox(cbxCentroCostoDetalle1.cbx, "Select ID, Descripcion From CentroCosto Where Estado = 1 order by 2")

        'Departamento Detalle
        'CSistema.SqlToComboBox(cbxDepartamentoDetalle.cbx, "Select ID, Departamento From DepartamentoEmpresa order by 2")
        Dim dtDepartamentosDetalle As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'") 'and IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDepartamentoDetalle.cbx, dtDepartamentosDetalle, "ID", "Descripcion")
        'cbxDepartamentoDetalle.cbx.Text = ""

        cbxUnidadNegocio.cbx.Text = ""
        'cbxCentroCosto.cbx.Text = ""
        cbxDepartamento.cbx.Text = ""
        cbxUnidadNegocioDetalle1.cbx.Text = ""
        'cbxCentroCostoDetalle1.cbx.Text = ""
        cbxDepartamentoDetalle.cbx.Text = ""

        cbxUnidadNegocioDetalle1.SoloLectura = True
        'cbxCentroCostoDetalle1.SoloLectura = True
        cbxDepartamentoDetalle.SoloLectura = True
        cbxSeccionDetalle.SoloLectura = True
        lblDptoSolicitante.Visible = False

        'Detalle
        dgv.Columns.Add("colCodigo", "Codigo")
        dgv.Columns.Add("colDescripcion", "Descripcion")
        dgv.Columns.Add("colDebito", "Debito")
        dgv.Columns.Add("colCredito", "Credito")
        dgv.Columns.Add("colID", "ID")

        dgv.Columns("colCodigo").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colDescripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("colDebito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colCredito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        dgv.Columns("colDebito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("colCredito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("colID").Visible = False
        'Operacion
        cbxTipo.cbx.Items.Add("DEBITO")
        cbxTipo.cbx.Items.Add("CREDITO")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        txtID.SetValue(CSistema.ExecuteScalar("Select IsNull(Max(Numero)+1,1) From Asiento"))

    End Sub

    Sub CargarItem()

        'Validar
        'Cuenta
        If ocxCuenta.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente la cuenta!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Importe
        If IsNumeric(txtImporteLocal.ObtenerValor) = False Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtImporteLocal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If cbxTipo.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'FA 01/12/2022 - Plan de cuenta
        If chkGastosVarios.chk.Checked = True Then
            If cbxUnidadNegocioDetalle1.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar una Unidad de Negocio", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If

            'If cbxCentroCostoDetalle1.cbx.Text = "" Then
            '    CSistema.MostrarError("Debe de seleccionar un Centro de Costo", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            '    Exit Sub
            'End If

            If cbxDepartamentoDetalle.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar un Departamento", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = CAsiento.dtDetalleAsiento.NewRow()
        dRow("IDTransaccion") = 0
        dRow("IDCuentaContable") = ocxCuenta.Registro("ID").ToString
        dRow("Cuenta") = ocxCuenta.Registro("Cuenta").ToString
        dRow("Codigo") = ocxCuenta.Registro("Codigo").ToString
        dRow("Descripcion") = ocxCuenta.Registro("Descripcion").ToString

        If txtDescripcion.GetValue = "" Then
            dRow("Observacion") = txtDetalle.GetValue
        Else
            dRow("Observacion") = txtDescripcion.GetValue
        End If

        dRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
        dRow("Orden") = CAsiento.dtDetalleAsiento.Rows.Count + 10

        Dim Importe As Decimal = CDec(txtImporteLocal.ObtenerValor)

        'Verificar la moneda
        If CAsiento.dtAsiento.Rows.Count > 0 Then
            If CAsiento.dtAsiento.Rows(0)("IDMoneda") <> 1 Then
                Importe = Importe * CAsiento.dtAsiento.Rows(0)("Cotizacion")
            End If
        Else
            If CSistema.FormatoNumero(txtCotizacion.txt.Text, 0) = 0 Then
                txtCotizacion.txt.Text = 1
                Importe = Importe * CSistema.FormatoNumero(txtCotizacion.txt.Text, 0)
            ElseIf CSistema.FormatoNumero(txtCotizacion.txt.Text, 0) > 0 Then
                Importe = Importe * CSistema.FormatoNumero(txtCotizacion.txt.Text, 0)
            End If
        End If

        If cbxTipo.cbx.Text = "CREDITO" Then
            dRow("Credito") = Importe
            dRow("Debito") = 0
        End If

        If cbxTipo.cbx.Text = "DEBITO" Then
            dRow("Credito") = 0
            dRow("Debito") = Importe
        End If

        dRow("Importe") = Importe
        If txtDescripcion.GetValue = "" Then
            dRow("Observacion") = txtDetalle.GetValue
        Else
            dRow("Observacion") = txtDescripcion.GetValue
        End If

        dRow("IDSucursal") = cbxSucursalDetalle.GetValue
        dRow("TipoComprobante") = txtTipoComprobante.GetValue
        dRow("NroComprobante") = txtNroComprobante.GetValue

        'FA 30/11/2022 - Plan de cuenta
        If chkGastosVarios.chk.Checked = True Then
            dRow("IDUnidadNegocioDetalle") = cbxUnidadNegocioDetalle1.GetValue
            dRow("UnidadNegocioDetalle") = cbxUnidadNegocioDetalle1.cbx.Text
            dRow("IDCentroCostoDetalle") = ocxCuenta.IDCentroCosto
            'dRow("CentroCostoDetalle") = cbxCentroCostoDetalle1.cbx.Text
            dRow("IDDepartamentoEmpresa") = cbxDepartamentoDetalle.GetValue
            dRow("Departamento") = cbxDepartamentoDetalle.cbx.Text
            dRow("IDSeccionDetalle") = cbxSeccionDetalle.GetValue
            dRow("Seccion") = cbxSeccionDetalle.cbx.Text
        Else
            dRow("IDCentroCosto") = ocxCuenta.IDCentroCosto
            dRow("IDCentroCostoDetalle") = ocxCuenta.IDCentroCosto
        End If

        dRow("Fijo") = False

        'dRow("IDCentroCosto") = ocxCuenta.Registro("IDCentroCosto")

        CAsiento.dtDetalleAsiento.Rows.Add(dRow)

        'Listar Detalle
        ListarDetalle()

        'Caluclar Totales
        CalcularTotales()

        'Inicializar controles
        ocxCuenta.Clear()
        txtImporteLocal.txt.Clear()
        txtDescripcion.txt.Clear()

        cbxUnidadNegocioDetalle1.cbx.Text = ""
        cbxDepartamentoDetalle.cbx.Text = ""

        ocxCuenta.Focus()

        PosicionarAutomaticamente()

        'Preguntar para salir
        If txtSaldo.ObtenerValor = "0" And dgv.Rows.Count > 0 Then
            If MessageBox.Show("Cerrar el asiento?", "Asiento", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                Guardar()
            End If
        End If

    End Sub

    Sub ListarDetalle()


        'Limpiamos todo el detalle
        dgv.Rows.Clear()

        'Volver a Ordenar
        CData.OrderDataTable(CAsiento.dtDetalleAsiento, "Orden")
        CAsiento.AgruparDetalle()

        'Cargamos registro por registro
        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows

            Dim Registro(5) As String
            Registro(0) = oRow("Codigo").ToString
            Registro(1) = oRow("Descripcion").ToString & "-" & oRow("UnidadNegocioDetalle") & "-" & oRow("CentroCostoDetalle") & "-" & oRow("Departamento")
            Registro(2) = CSistema.FormatoNumero(oRow("Debito").ToString)
            Registro(3) = CSistema.FormatoMoneda(oRow("Credito").ToString)
            Registro(4) = oRow("ID")

            dgv.Rows.Add(Registro)

        Next

        CalcularTotales()


    End Sub

    Public Sub CalcularTotales()

        CSistema.TotalesGrid(dgv, txtTotalDebitoLocal.txt, 2)
        CSistema.TotalesGrid(dgv, txtTotalCreditoLocal.txt, 3)

        'Hayar el saldo
        Dim Saldo As Decimal = 0
        Saldo = CDec(txtTotalCreditoLocal.ObtenerValor) - CDec(txtTotalDebitoLocal.ObtenerValor)

        txtSaldo.txt.Text = Saldo


    End Sub

    Sub EliminarItem()

        'Validar
        If dgv.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgv, mensaje)
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
        '    If oRow("Codigo") = dgv.SelectedRows(0).Cells(0).Value Then
        '        CAsiento.dtDetalleAsiento.Rows.Remove(oRow)
        '        Exit For
        '    End If
        'Next

        Dim IDLinea As Integer = CInt(dgv.SelectedRows(0).Cells(4).Value)
        Dim dtAux As DataTable = CAsiento.dtDetalleAsiento.Clone()

        For Each Row As DataRow In CAsiento.dtDetalleAsiento.Rows
            If Row("ID") = IDLinea Then
                GoTo Seguir
            End If
            Dim NewRow As DataRow = dtAux.NewRow()
            For I = 0 To CAsiento.dtDetalleAsiento.Columns.Count - 1
                NewRow(I) = Row(I)
            Next
            dtAux.Rows.Add(NewRow)
Seguir:

        Next

        'dtDetalle.Rows(dgw.SelectedRows(0).Index).Delete()
        CAsiento.dtDetalleAsiento.Clear()
        CAsiento.dtDetalleAsiento = dtAux.Copy()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()
        CalcularTotales()

    End Sub

    Sub Procesar()

        'Validar
        'Detalle
        If CAsiento.dtAsiento.Rows.Count = 0 Then
            Dim mensaje As String = "Ingrese el detalle!"
            ctrError.SetError(btnAceptar, mensaje)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Montos
        Dim Debitos As Decimal = txtTotalDebitoLocal.ObtenerValor
        Dim Creditos As Decimal = txtTotalCreditoLocal.ObtenerValor
        Dim Saldo As Decimal = Creditos - Debitos

        If Saldo <> 0 Then
            Dim mensaje As String = "Ingrese el detalle!"
            ctrError.SetError(btnAceptar, mensaje)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Total

    End Sub

    Sub Salir()

    End Sub

    Sub Guardar()


        'Validar
        'Si el saldo es incorrecto
        If txtSaldo.ObtenerValor <> 0 Then
            CSistema.MostrarError("Los importes no concuerdan!", ctrError, btnAceptar, tsslEstado)
            Exit Sub
        End If

        'Si el importe es incorrecto
        If txtTotalCreditoLocal.ObtenerValor = 0 Then
            CSistema.MostrarError("El asiento no puede ser 0!", ctrError, btnAceptar, tsslEstado)
            Exit Sub
        End If
        If chkGastosVarios.chk.Checked = False Then
            If cbxUnidadNegocio.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar una Unidad de Negocio", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If

            'If cbxCentroCosto.cbx.Text = "" Then
            '    CSistema.MostrarError("Debe de seleccionar un Centro de Costo", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            '    Exit Sub
            'End If

            If cbxDepartamento.cbx.Text = "" Then
                CSistema.MostrarError("Debe de seleccionar un Departamento", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Sub
            End If

            'If cbxSeccion.cbx.Text = "" Then
            '    CSistema.MostrarError("Debe de seleccionar una Seccion", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            '    Exit Sub
            'End If

        End If

        If txtDetalle.txt.Text = "" Then
            CSistema.MostrarError("Ingrese una observacion al asiento", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Cargar la Transaccion
        Dim IDTransaccion As Integer = 0

        If IDTransaccionOperacion > 0 Then
            IDTransaccion = IDTransaccionOperacion
        Else
            IDTransaccion = GuardarTransaccion()
        End If

        If IDTransaccion = 0 Then
            CSistema.MostrarError("No se puede generar el asiento! Ocurrio un error al tratar de registrar la transaccion", ctrError, btnAceptar, tsslEstado)
            Exit Sub
        End If

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & " " & txtComprobante.GetValue

        'Cargar la Cabecera
        Dim NewRow As DataRow = CAsiento.dtAsiento.NewRow

        NewRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        NewRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        NewRow("Fecha") = txtFecha.GetValue
        NewRow("IDMoneda") = cbxMoneda.GetValue
        NewRow("Cotizacion") = txtCotizacion.ObtenerValor
        NewRow("IDTipoComprobante") = cbxTipoComprobante.GetValue
        NewRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        NewRow("NroComprobante") = txtComprobante.GetValue
        NewRow("Comprobante") = Comprobante
        NewRow("Total") = txtTotalCreditoLocal.ObtenerValor
        NewRow("Detalle") = txtDetalle.GetValue
        NewRow("Bloquear") = chkBloquear.Checked.ToString
        'FA 18/11/2022 - Plan de cuenta 
        NewRow("GastosMultiples") = chkGastosVarios.chk.Checked.ToString
        If chkGastosVarios.chk.Checked = True Then
            NewRow("IDUnidadNegocio") = 0
            NewRow("IDCentroCosto") = 0
            NewRow("IDDepartamento") = cbxDepartamento.GetValue
            NewRow("IDSeccion") = cbxSeccion.GetValue
        Else
            NewRow("IDUnidadNegocio") = cbxUnidadNegocio.GetValue
            'NewRow("IDCentroCosto") = cbxCentroCosto.GetValue
            NewRow("IDDepartamento") = cbxDepartamento.GetValue
            NewRow("IDSeccion") = cbxSeccion.GetValue
        End If


        CAsiento.dtAsiento.Rows.Add(NewRow)
        CAsiento.IDTransaccion = IDTransaccion
        'CAsiento.IDCentroCosto = cbxCentroCosto.GetValue
        CAsiento.NroCaja = 1
        CAsiento.Bloquear = chkBloquear.Checked.ToString
        IDTransaccionGuardado = IDTransaccion
        If CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS) = True Then

            Procesado = True
            Me.Close()
        End If

    End Sub

    Function GuardarTransaccion() As Integer

        GuardarTransaccion = 0
        Dim SQL As String = ""
        SQL = SQL & "Declare @Mensaje varchar(50)" & vbCrLf
        SQL = SQL & "Declare @Procesado bit" & vbCrLf
        SQL = SQL & "Declare @IDTransaccionSalida int" & vbCrLf
        SQL = SQL & "EXEC SpTransaccion " & vbCrLf
        SQL = SQL & "@IDUsuario = " & vgIDUsuario & ", " & vbCrLf
        SQL = SQL & "@IDSucursal = " & vgIDSucursal & ", " & vbCrLf
        SQL = SQL & "@IDDeposito = " & vgIDDeposito & ", " & vbCrLf
        SQL = SQL & "@IDTerminal = " & vgIDTerminal & ", " & vbCrLf
        SQL = SQL & "@IDOperacion = " & cbxOperacion.GetValue & ", " & vbCrLf
        SQL = SQL & "@Mensaje = @Mensaje OUTPUT, " & vbCrLf
        SQL = SQL & "@Procesado = @Procesado OUTPUT, " & vbCrLf
        SQL = SQL & "@IDTransaccion = @IDTransaccionSalida OUTPUT" & vbCrLf

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        If dt Is Nothing Then
            Return 0
        End If

        If dt.Rows.Count = 0 Then
            Return 0
        End If

        Dim orow As DataRow = dt.Rows(0)

        Return orow("IDTransaccion")

    End Function

    Sub PosicionarAutomaticamente()

        Dim Debito As Decimal = txtTotalDebitoLocal.ObtenerValor
        Dim Credito As Decimal = txtTotalCreditoLocal.ObtenerValor

        If Credito > Debito Then
            cbxTipo.cbx.SelectedIndex = 0
        Else
            cbxTipo.cbx.SelectedIndex = 1
        End If

    End Sub

    Sub VerDetalle()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim frm As New frmPropiedad
        frm.TituloVentana = "Propiedades de la Cuenta"
        frm.Titulo = dgv.SelectedRows(0).Cells(1).Value
        frm.dt = CAsiento.dtDetalleAsiento
        frm.ShowDialog()

    End Sub

    Sub ListarTipoComprobante()

        cbxTipoComprobante.cbx.DataSource = Nothing

        If IsNumeric(cbxOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If


        Dim IDOperacion As Integer = cbxOperacion.GetValue
        Dim dt As DataTable = CData.GetTable("VTipoComprobante", " IDOperacion=" & IDOperacion).Copy

        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dt, "ID", "Codigo")

    End Sub

    Private Sub VerDescuentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDescuentosToolStripMenuItem.Click
        VerDetalle()
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(CAsiento.dtDetalleAsiento)
    End Sub

    Private Sub frmAsiento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAsiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub ocxCuenta_ItemSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxCuenta.ItemSeleccionado
        If ocxCuenta.Seleccionado = True Then
            'FA 30/11/2022 - Plan de cuenta
            'cbxUnidadNegocioDetalle.cbx.SelectedValue = ocxCuenta.IDUnidadNegocio
            'cbxCentroCostoDetalle.cbx.SelectedValue = ocxCuenta.IDCentroCosto

            PosicionarAutomaticamente()
            cbxTipo.cbx.Focus()

        End If
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        CargarItem()
    End Sub

    Private Sub cbxOperacion_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxOperacion.Leave
        ListarTipoComprobante()
    End Sub

    Private Sub cbxOperacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOperacion.PropertyChanged
        ListarTipoComprobante()
    End Sub

    Private Sub gbxCabecera_Enter(sender As System.Object, e As System.EventArgs) Handles gbxCabecera.Enter

    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub dgv_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarItem()
        End If
    End Sub

    Private Sub txtDescripcion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            If txtDescripcion.GetValue.Length <= 1 Then
                txtDescripcion.SetValue(txtDetalle.GetValue)
                CSistema.SelectNextControl(Me, Keys.Enter)
            End If
        End If
    End Sub

    Private Sub txtTipoComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtTipoComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            txtTipoComprobante.SetValue(cbxTipoComprobante.cbx.Text)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            txtNroComprobante.SetValue(txtComprobante.GetValue)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If
    End Sub

    Private Sub txtImporteLocal_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteLocal.TeclaPrecionada

        If e.KeyCode = Keys.Multiply Then
            Dim Saldo As Decimal = txtSaldo.ObtenerValor
            If Saldo < 0 Then
                Saldo = Saldo * -1
            End If
            txtImporteLocal.SetValue(Saldo)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If

    End Sub

    Private Sub txtDescripcion_Leave(sender As System.Object, e As System.EventArgs) Handles txtDescripcion.Leave
        If txtDescripcion.GetValue = "" Then
            txtDescripcion.SetValue(txtDetalle.GetValue)
        End If
    End Sub

    Private Sub cbxUnidadNegocio_PropertyChanged(sender As Object, e As System.EventArgs)
        'FA 30/11/2022 - Plan de cuenta
        'Dim IDUnidadNegocio As String = CStr(cbxUnidadNegocioDetalle.cbx.SelectedValue)
        'If IDUnidadNegocio Is Nothing Then
        '    IDUnidadNegocio = 0
        'End If

        'If ocxCuenta.IDUnidadNegocio > 0 Then
        '    cbxCentroCostoDetalle.cbx.SelectedValue = ocxCuenta.IDUnidadNegocio
        'End If

    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    'FA 28/05/2021
    Sub frmAgregarAsiento_Activate()
        Me.Refresh()
    End Sub

    Private Sub OcxCHK1_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkGastosVarios.PropertyChanged
        If chkGastosVarios.chk.Checked = True Then
            cbxUnidadNegocio.SoloLectura = True
            'cbxCentroCosto.SoloLectura = True
            'cbxDepartamento.SoloLectura = True
            cbxUnidadNegocioDetalle1.SoloLectura = False
            'cbxCentroCostoDetalle1.SoloLectura = False
            cbxDepartamentoDetalle.SoloLectura = False
            lblDptoSolicitante.Visible = True
            lblDptoConsume.Visible = False
            cbxSeccion.SoloLectura = False
            cbxSeccionDetalle.SoloLectura = False
        Else
            cbxUnidadNegocio.SoloLectura = False
            'cbxCentroCosto.SoloLectura = False
            cbxDepartamento.SoloLectura = False
            cbxUnidadNegocioDetalle1.SoloLectura = True
            'cbxCentroCostoDetalle1.SoloLectura = True
            cbxDepartamentoDetalle.SoloLectura = True
            lblDptoSolicitante.Visible = False
            lblDptoConsume.Visible = True
            cbxSeccion.SoloLectura = False
            cbxSeccionDetalle.SoloLectura = True
        End If
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As Object, e As EventArgs) Handles cbxSucursal.PropertyChanged
        Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
        CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamento.PropertyChanged
        Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamento.cbx.SelectedValue)
        If Seccion = True Then
            Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamento.cbx.SelectedValue)
            CSistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
            cbxSeccion.cbx.Text = ""
            cbxSeccion.SoloLectura = False
        Else
            cbxSeccion.SoloLectura = True
        End If
    End Sub

    Private Sub cbxDepartamentoDetalle_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamentoDetalle.PropertyChanged
        Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamentoDetalle.cbx.SelectedValue)
        If Seccion = True Then
            Dim dtSeccionDetalle As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamentoDetalle.cbx.SelectedValue)
            CSistema.SqlToComboBox(cbxSeccionDetalle.cbx, dtSeccionDetalle, "ID", "Descripcion")
            cbxSeccionDetalle.cbx.Text = ""
            cbxSeccionDetalle.SoloLectura = False
        Else
            cbxSeccionDetalle.SoloLectura = True
        End If
    End Sub
End Class
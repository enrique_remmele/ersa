﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarAsiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.cbxSeccionDetalle = New ERP.ocxCBX()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbxDepartamentoDetalle = New ERP.ocxCBX()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbxUnidadNegocioDetalle1 = New ERP.ocxCBX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.ocxCuenta = New ERP.ocxTXTCuentaContable()
        Me.cbxSucursalDetalle = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNroComprobante = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTipoComprobante = New ERP.ocxTXTString()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtImporteLocal = New ERP.ocxTXTNumeric()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.lblImporteLocal = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtSeccion = New ERP.ocxTXTString()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkGastosMultiples = New ERP.ocxCHK()
        Me.txtDepartamento = New ERP.ocxTXTString()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtUnidadNegocio = New ERP.ocxTXTString()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTipoComprobanteCabecera = New ERP.ocxTXTString()
        Me.lblInformacionCaja = New System.Windows.Forms.Label()
        Me.txtNroCaja = New ERP.ocxTXTNumeric()
        Me.lblNroCaja = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxCentroCosto = New ERP.ocxCBX()
        Me.lblCentroCosto = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion2 = New System.Windows.Forms.Label()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.txtNroComprobanteCabecera = New ERP.ocxTXTString()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalCreditoLocal = New ERP.ocxTXTNumeric()
        Me.txtTotalDebitoLocal = New ERP.ocxTXTNumeric()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkBloquear = New System.Windows.Forms.CheckBox()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.VerDescuentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cbxSeccion = New ERP.ocxCBX()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(4, 72)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 13
        Me.lblComprobante.Text = "Comprobante:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 471)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(788, 164)
        Me.Panel3.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel2.Controls.Add(Me.cbxSeccionDetalle)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.cbxDepartamentoDetalle)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.cbxUnidadNegocioDetalle1)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.btnAgregar)
        Me.Panel2.Controls.Add(Me.ocxCuenta)
        Me.Panel2.Controls.Add(Me.cbxSucursalDetalle)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txtNroComprobante)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.txtTipoComprobante)
        Me.Panel2.Controls.Add(Me.lblCuenta)
        Me.Panel2.Controls.Add(Me.lblTipo)
        Me.Panel2.Controls.Add(Me.lblDescripcion)
        Me.Panel2.Controls.Add(Me.txtImporteLocal)
        Me.Panel2.Controls.Add(Me.txtDescripcion)
        Me.Panel2.Controls.Add(Me.cbxTipo)
        Me.Panel2.Controls.Add(Me.lblImporteLocal)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(788, 164)
        Me.Panel2.TabIndex = 0
        '
        'cbxSeccionDetalle
        '
        Me.cbxSeccionDetalle.CampoWhere = Nothing
        Me.cbxSeccionDetalle.CargarUnaSolaVez = False
        Me.cbxSeccionDetalle.DataDisplayMember = Nothing
        Me.cbxSeccionDetalle.DataFilter = Nothing
        Me.cbxSeccionDetalle.DataOrderBy = Nothing
        Me.cbxSeccionDetalle.DataSource = Nothing
        Me.cbxSeccionDetalle.DataValueMember = Nothing
        Me.cbxSeccionDetalle.dtSeleccionado = Nothing
        Me.cbxSeccionDetalle.FormABM = Nothing
        Me.cbxSeccionDetalle.Indicaciones = Nothing
        Me.cbxSeccionDetalle.Location = New System.Drawing.Point(507, 107)
        Me.cbxSeccionDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSeccionDetalle.Name = "cbxSeccionDetalle"
        Me.cbxSeccionDetalle.SeleccionMultiple = False
        Me.cbxSeccionDetalle.SeleccionObligatoria = False
        Me.cbxSeccionDetalle.Size = New System.Drawing.Size(272, 21)
        Me.cbxSeccionDetalle.SoloLectura = False
        Me.cbxSeccionDetalle.TabIndex = 21
        Me.cbxSeccionDetalle.TabStop = False
        Me.cbxSeccionDetalle.Texto = ""
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(508, 92)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Seccion Detalle:"
        '
        'cbxDepartamentoDetalle
        '
        Me.cbxDepartamentoDetalle.CampoWhere = Nothing
        Me.cbxDepartamentoDetalle.CargarUnaSolaVez = False
        Me.cbxDepartamentoDetalle.DataDisplayMember = Nothing
        Me.cbxDepartamentoDetalle.DataFilter = Nothing
        Me.cbxDepartamentoDetalle.DataOrderBy = Nothing
        Me.cbxDepartamentoDetalle.DataSource = Nothing
        Me.cbxDepartamentoDetalle.DataValueMember = Nothing
        Me.cbxDepartamentoDetalle.dtSeleccionado = Nothing
        Me.cbxDepartamentoDetalle.FormABM = Nothing
        Me.cbxDepartamentoDetalle.Indicaciones = Nothing
        Me.cbxDepartamentoDetalle.Location = New System.Drawing.Point(247, 107)
        Me.cbxDepartamentoDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepartamentoDetalle.Name = "cbxDepartamentoDetalle"
        Me.cbxDepartamentoDetalle.SeleccionMultiple = False
        Me.cbxDepartamentoDetalle.SeleccionObligatoria = False
        Me.cbxDepartamentoDetalle.Size = New System.Drawing.Size(254, 21)
        Me.cbxDepartamentoDetalle.SoloLectura = False
        Me.cbxDepartamentoDetalle.TabIndex = 18
        Me.cbxDepartamentoDetalle.TabStop = False
        Me.cbxDepartamentoDetalle.Texto = ""
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(248, 92)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(145, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Departamento que Consume:"
        '
        'cbxUnidadNegocioDetalle1
        '
        Me.cbxUnidadNegocioDetalle1.CampoWhere = Nothing
        Me.cbxUnidadNegocioDetalle1.CargarUnaSolaVez = False
        Me.cbxUnidadNegocioDetalle1.DataDisplayMember = Nothing
        Me.cbxUnidadNegocioDetalle1.DataFilter = Nothing
        Me.cbxUnidadNegocioDetalle1.DataOrderBy = Nothing
        Me.cbxUnidadNegocioDetalle1.DataSource = Nothing
        Me.cbxUnidadNegocioDetalle1.DataValueMember = Nothing
        Me.cbxUnidadNegocioDetalle1.dtSeleccionado = Nothing
        Me.cbxUnidadNegocioDetalle1.FormABM = Nothing
        Me.cbxUnidadNegocioDetalle1.Indicaciones = Nothing
        Me.cbxUnidadNegocioDetalle1.Location = New System.Drawing.Point(9, 107)
        Me.cbxUnidadNegocioDetalle1.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocioDetalle1.Name = "cbxUnidadNegocioDetalle1"
        Me.cbxUnidadNegocioDetalle1.SeleccionMultiple = False
        Me.cbxUnidadNegocioDetalle1.SeleccionObligatoria = False
        Me.cbxUnidadNegocioDetalle1.Size = New System.Drawing.Size(228, 21)
        Me.cbxUnidadNegocioDetalle1.SoloLectura = False
        Me.cbxUnidadNegocioDetalle1.TabIndex = 14
        Me.cbxUnidadNegocioDetalle1.TabStop = False
        Me.cbxUnidadNegocioDetalle1.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(138, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Unidad de Negocio Detalle:"
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(682, 136)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(94, 23)
        Me.btnAgregar.TabIndex = 19
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'ocxCuenta
        '
        Me.ocxCuenta.AlturaMaxima = 84
        Me.ocxCuenta.Consulta = Nothing
        Me.ocxCuenta.IDCentroCosto = 0
        Me.ocxCuenta.IDUnidadNegocio = 0
        Me.ocxCuenta.ListarTodas = False
        Me.ocxCuenta.Location = New System.Drawing.Point(9, 19)
        Me.ocxCuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.ocxCuenta.Name = "ocxCuenta"
        Me.ocxCuenta.Registro = Nothing
        Me.ocxCuenta.Resolucion173 = False
        Me.ocxCuenta.Seleccionado = False
        Me.ocxCuenta.Size = New System.Drawing.Size(579, 24)
        Me.ocxCuenta.SoloLectura = False
        Me.ocxCuenta.TabIndex = 1
        Me.ocxCuenta.Texto = Nothing
        Me.ocxCuenta.whereFiltro = Nothing
        '
        'cbxSucursalDetalle
        '
        Me.cbxSucursalDetalle.CampoWhere = Nothing
        Me.cbxSucursalDetalle.CargarUnaSolaVez = False
        Me.cbxSucursalDetalle.DataDisplayMember = Nothing
        Me.cbxSucursalDetalle.DataFilter = Nothing
        Me.cbxSucursalDetalle.DataOrderBy = Nothing
        Me.cbxSucursalDetalle.DataSource = Nothing
        Me.cbxSucursalDetalle.DataValueMember = Nothing
        Me.cbxSucursalDetalle.dtSeleccionado = Nothing
        Me.cbxSucursalDetalle.FormABM = Nothing
        Me.cbxSucursalDetalle.Indicaciones = Nothing
        Me.cbxSucursalDetalle.Location = New System.Drawing.Point(598, 67)
        Me.cbxSucursalDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursalDetalle.Name = "cbxSucursalDetalle"
        Me.cbxSucursalDetalle.SeleccionMultiple = False
        Me.cbxSucursalDetalle.SeleccionObligatoria = True
        Me.cbxSucursalDetalle.Size = New System.Drawing.Size(181, 21)
        Me.cbxSucursalDetalle.SoloLectura = False
        Me.cbxSucursalDetalle.TabIndex = 12
        Me.cbxSucursalDetalle.TabStop = False
        Me.cbxSucursalDetalle.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(601, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Sucursal:"
        '
        'txtNroComprobante
        '
        Me.txtNroComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobante.Color = System.Drawing.Color.Empty
        Me.txtNroComprobante.Indicaciones = Nothing
        Me.txtNroComprobante.Location = New System.Drawing.Point(510, 67)
        Me.txtNroComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNroComprobante.Multilinea = False
        Me.txtNroComprobante.Name = "txtNroComprobante"
        Me.txtNroComprobante.Size = New System.Drawing.Size(88, 21)
        Me.txtNroComprobante.SoloLectura = False
        Me.txtNroComprobante.TabIndex = 9
        Me.txtNroComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroComprobante.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(445, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Comprobante:"
        '
        'txtTipoComprobante
        '
        Me.txtTipoComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoComprobante.Color = System.Drawing.Color.Empty
        Me.txtTipoComprobante.Indicaciones = Nothing
        Me.txtTipoComprobante.Location = New System.Drawing.Point(442, 67)
        Me.txtTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTipoComprobante.Multilinea = False
        Me.txtTipoComprobante.Name = "txtTipoComprobante"
        Me.txtTipoComprobante.Size = New System.Drawing.Size(69, 21)
        Me.txtTipoComprobante.SoloLectura = False
        Me.txtTipoComprobante.TabIndex = 11
        Me.txtTipoComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoComprobante.Texto = ""
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(22, 2)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 0
        Me.lblCuenta.Text = "Cuenta:"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(596, 1)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 2
        Me.lblTipo.Text = "Tipo:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(12, 51)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 6
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'txtImporteLocal
        '
        Me.txtImporteLocal.Color = System.Drawing.Color.Empty
        Me.txtImporteLocal.Decimales = True
        Me.txtImporteLocal.Indicaciones = Nothing
        Me.txtImporteLocal.Location = New System.Drawing.Point(686, 19)
        Me.txtImporteLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtImporteLocal.Name = "txtImporteLocal"
        Me.txtImporteLocal.Size = New System.Drawing.Size(90, 22)
        Me.txtImporteLocal.SoloLectura = False
        Me.txtImporteLocal.TabIndex = 5
        Me.txtImporteLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteLocal.Texto = "0"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(9, 67)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(431, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 7
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.dtSeleccionado = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(596, 20)
        Me.cbxTipo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionMultiple = False
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(90, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'lblImporteLocal
        '
        Me.lblImporteLocal.AutoSize = True
        Me.lblImporteLocal.Location = New System.Drawing.Point(739, 5)
        Me.lblImporteLocal.Name = "lblImporteLocal"
        Me.lblImporteLocal.Size = New System.Drawing.Size(45, 13)
        Me.lblImporteLocal.TabIndex = 4
        Me.lblImporteLocal.Text = "Importe:"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(623, 5)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(704, 5)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(442, 0)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldo.TabIndex = 0
        Me.lblSaldo.Text = "Saldo:"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.gbxCabecera, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 4)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 210.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 170.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(794, 677)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.Label4)
        Me.gbxCabecera.Controls.Add(Me.txtSeccion)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.chkGastosMultiples)
        Me.gbxCabecera.Controls.Add(Me.txtDepartamento)
        Me.gbxCabecera.Controls.Add(Me.Label6)
        Me.gbxCabecera.Controls.Add(Me.txtUnidadNegocio)
        Me.gbxCabecera.Controls.Add(Me.Label7)
        Me.gbxCabecera.Controls.Add(Me.txtTipoComprobanteCabecera)
        Me.gbxCabecera.Controls.Add(Me.lblInformacionCaja)
        Me.gbxCabecera.Controls.Add(Me.txtNroCaja)
        Me.gbxCabecera.Controls.Add(Me.lblNroCaja)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.cbxCentroCosto)
        Me.gbxCabecera.Controls.Add(Me.lblCentroCosto)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion2)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtNroComprobanteCabecera)
        Me.gbxCabecera.Controls.Add(Me.cbxDepartamento)
        Me.gbxCabecera.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxCabecera.Controls.Add(Me.cbxSeccion)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.Location = New System.Drawing.Point(3, 3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(788, 204)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(509, 125)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Seccion:"
        '
        'txtSeccion
        '
        Me.txtSeccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSeccion.Color = System.Drawing.Color.Empty
        Me.txtSeccion.Indicaciones = Nothing
        Me.txtSeccion.Location = New System.Drawing.Point(508, 148)
        Me.txtSeccion.Multilinea = False
        Me.txtSeccion.Name = "txtSeccion"
        Me.txtSeccion.Size = New System.Drawing.Size(273, 21)
        Me.txtSeccion.SoloLectura = True
        Me.txtSeccion.TabIndex = 28
        Me.txtSeccion.TabStop = False
        Me.txtSeccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSeccion.Texto = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(235, 125)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(129, 13)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Departamento Solicitante:"
        '
        'chkGastosMultiples
        '
        Me.chkGastosMultiples.BackColor = System.Drawing.Color.Transparent
        Me.chkGastosMultiples.Color = System.Drawing.Color.Empty
        Me.chkGastosMultiples.Location = New System.Drawing.Point(80, 95)
        Me.chkGastosMultiples.Name = "chkGastosMultiples"
        Me.chkGastosMultiples.Size = New System.Drawing.Size(205, 21)
        Me.chkGastosMultiples.SoloLectura = False
        Me.chkGastosMultiples.TabIndex = 18
        Me.chkGastosMultiples.Texto = "Asignacion de Unidad y Departamento"
        Me.chkGastosMultiples.Valor = False
        '
        'txtDepartamento
        '
        Me.txtDepartamento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartamento.Color = System.Drawing.Color.Empty
        Me.txtDepartamento.Indicaciones = Nothing
        Me.txtDepartamento.Location = New System.Drawing.Point(237, 148)
        Me.txtDepartamento.Multilinea = False
        Me.txtDepartamento.Name = "txtDepartamento"
        Me.txtDepartamento.Size = New System.Drawing.Size(268, 21)
        Me.txtDepartamento.SoloLectura = True
        Me.txtDepartamento.TabIndex = 23
        Me.txtDepartamento.TabStop = False
        Me.txtDepartamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDepartamento.Texto = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(235, 125)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(145, 13)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Departamento que Consume:"
        '
        'txtUnidadNegocio
        '
        Me.txtUnidadNegocio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.txtUnidadNegocio.Indicaciones = Nothing
        Me.txtUnidadNegocio.Location = New System.Drawing.Point(14, 148)
        Me.txtUnidadNegocio.Multilinea = False
        Me.txtUnidadNegocio.Name = "txtUnidadNegocio"
        Me.txtUnidadNegocio.Size = New System.Drawing.Size(220, 21)
        Me.txtUnidadNegocio.SoloLectura = True
        Me.txtUnidadNegocio.TabIndex = 20
        Me.txtUnidadNegocio.TabStop = False
        Me.txtUnidadNegocio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUnidadNegocio.Texto = ""
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(102, 13)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Unidad de Negocio:"
        '
        'txtTipoComprobanteCabecera
        '
        Me.txtTipoComprobanteCabecera.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoComprobanteCabecera.Color = System.Drawing.Color.Empty
        Me.txtTipoComprobanteCabecera.Indicaciones = Nothing
        Me.txtTipoComprobanteCabecera.Location = New System.Drawing.Point(80, 68)
        Me.txtTipoComprobanteCabecera.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTipoComprobanteCabecera.Multilinea = False
        Me.txtTipoComprobanteCabecera.Name = "txtTipoComprobanteCabecera"
        Me.txtTipoComprobanteCabecera.Size = New System.Drawing.Size(63, 21)
        Me.txtTipoComprobanteCabecera.SoloLectura = False
        Me.txtTipoComprobanteCabecera.TabIndex = 14
        Me.txtTipoComprobanteCabecera.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoComprobanteCabecera.Texto = ""
        '
        'lblInformacionCaja
        '
        Me.lblInformacionCaja.AutoSize = True
        Me.lblInformacionCaja.ForeColor = System.Drawing.Color.DimGray
        Me.lblInformacionCaja.Location = New System.Drawing.Point(166, 16)
        Me.lblInformacionCaja.Name = "lblInformacionCaja"
        Me.lblInformacionCaja.Size = New System.Drawing.Size(53, 13)
        Me.lblInformacionCaja.TabIndex = 2
        Me.lblInformacionCaja.Tag = "No tocar"
        Me.lblInformacionCaja.Text = "__/__/__"
        '
        'txtNroCaja
        '
        Me.txtNroCaja.Color = System.Drawing.Color.Empty
        Me.txtNroCaja.Decimales = False
        Me.txtNroCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroCaja.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtNroCaja.Location = New System.Drawing.Point(80, 11)
        Me.txtNroCaja.Margin = New System.Windows.Forms.Padding(6, 5, 6, 5)
        Me.txtNroCaja.Name = "txtNroCaja"
        Me.txtNroCaja.Size = New System.Drawing.Size(63, 22)
        Me.txtNroCaja.SoloLectura = False
        Me.txtNroCaja.TabIndex = 1
        Me.txtNroCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroCaja.Texto = "0"
        '
        'lblNroCaja
        '
        Me.lblNroCaja.AutoSize = True
        Me.lblNroCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNroCaja.Location = New System.Drawing.Point(4, 14)
        Me.lblNroCaja.Name = "lblNroCaja"
        Me.lblNroCaja.Size = New System.Drawing.Size(71, 15)
        Me.lblNroCaja.TabIndex = 0
        Me.lblNroCaja.Text = "Nro. Caja:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(253, 41)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(111, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(202, 45)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 6
        Me.lblSucursal.Text = "Sucursal:"
        '
        'cbxCentroCosto
        '
        Me.cbxCentroCosto.CampoWhere = Nothing
        Me.cbxCentroCosto.CargarUnaSolaVez = False
        Me.cbxCentroCosto.DataDisplayMember = Nothing
        Me.cbxCentroCosto.DataFilter = Nothing
        Me.cbxCentroCosto.DataOrderBy = Nothing
        Me.cbxCentroCosto.DataSource = Nothing
        Me.cbxCentroCosto.DataValueMember = Nothing
        Me.cbxCentroCosto.dtSeleccionado = Nothing
        Me.cbxCentroCosto.FormABM = Nothing
        Me.cbxCentroCosto.Indicaciones = Nothing
        Me.cbxCentroCosto.Location = New System.Drawing.Point(426, 68)
        Me.cbxCentroCosto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCentroCosto.Name = "cbxCentroCosto"
        Me.cbxCentroCosto.SeleccionMultiple = False
        Me.cbxCentroCosto.SeleccionObligatoria = False
        Me.cbxCentroCosto.Size = New System.Drawing.Size(280, 21)
        Me.cbxCentroCosto.SoloLectura = False
        Me.cbxCentroCosto.TabIndex = 17
        Me.cbxCentroCosto.Texto = ""
        Me.cbxCentroCosto.Visible = False
        '
        'lblCentroCosto
        '
        Me.lblCentroCosto.AutoSize = True
        Me.lblCentroCosto.Location = New System.Drawing.Point(370, 72)
        Me.lblCentroCosto.Name = "lblCentroCosto"
        Me.lblCentroCosto.Size = New System.Drawing.Size(50, 13)
        Me.lblCentroCosto.TabIndex = 16
        Me.lblCentroCosto.Text = "C. Costo:"
        Me.lblCentroCosto.Visible = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(536, 41)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(87, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 11
        Me.cbxMoneda.TabStop = False
        Me.cbxMoneda.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(487, 45)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 10
        Me.lblMoneda.Text = "Moneda:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(623, 41)
        Me.txtCotizacion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(83, 21)
        Me.txtCotizacion.SoloLectura = True
        Me.txtCotizacion.TabIndex = 12
        Me.txtCotizacion.TabStop = False
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 18, 14, 30, 7, 171)
        Me.txtFecha.Location = New System.Drawing.Point(404, 41)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(83, 20)
        Me.txtFecha.SoloLectura = True
        Me.txtFecha.TabIndex = 9
        Me.txtFecha.TabStop = False
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(364, 45)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(80, 41)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 4
        Me.cbxCiudad.TabStop = False
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(143, 41)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 5
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion2
        '
        Me.lblOperacion2.AutoSize = True
        Me.lblOperacion2.Location = New System.Drawing.Point(4, 45)
        Me.lblOperacion2.Name = "lblOperacion2"
        Me.lblOperacion2.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion2.TabIndex = 3
        Me.lblOperacion2.Text = "Operacion:"
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(5, 180)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 24
        Me.lblDetalle.Text = "Detalle:"
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(81, 176)
        Me.txtDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(702, 21)
        Me.txtDetalle.SoloLectura = False
        Me.txtDetalle.TabIndex = 25
        Me.txtDetalle.TabStop = False
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'txtNroComprobanteCabecera
        '
        Me.txtNroComprobanteCabecera.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroComprobanteCabecera.Color = System.Drawing.Color.Empty
        Me.txtNroComprobanteCabecera.Indicaciones = Nothing
        Me.txtNroComprobanteCabecera.Location = New System.Drawing.Point(143, 68)
        Me.txtNroComprobanteCabecera.Margin = New System.Windows.Forms.Padding(4)
        Me.txtNroComprobanteCabecera.Multilinea = False
        Me.txtNroComprobanteCabecera.Name = "txtNroComprobanteCabecera"
        Me.txtNroComprobanteCabecera.Size = New System.Drawing.Size(221, 21)
        Me.txtNroComprobanteCabecera.SoloLectura = False
        Me.txtNroComprobanteCabecera.TabIndex = 15
        Me.txtNroComprobanteCabecera.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroComprobanteCabecera.Texto = ""
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(235, 147)
        Me.cbxDepartamento.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(267, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 26
        Me.cbxDepartamento.Texto = ""
        Me.cbxDepartamento.Visible = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = Nothing
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = Nothing
        Me.cbxUnidadNegocio.DataValueMember = Nothing
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(14, 148)
        Me.cbxUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(216, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 29
        Me.cbxUnidadNegocio.Texto = ""
        Me.cbxUnidadNegocio.Visible = False
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 213)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(788, 218)
        Me.dgv.StandardTab = True
        Me.dgv.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtTotalCreditoLocal, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtTotalDebitoLocal, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtSaldo, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 437)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(788, 28)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblSaldo)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(482, 22)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'txtTotalCreditoLocal
        '
        Me.txtTotalCreditoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalCreditoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalCreditoLocal.Decimales = True
        Me.txtTotalCreditoLocal.Indicaciones = Nothing
        Me.txtTotalCreditoLocal.Location = New System.Drawing.Point(692, 4)
        Me.txtTotalCreditoLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalCreditoLocal.Name = "txtTotalCreditoLocal"
        Me.txtTotalCreditoLocal.Size = New System.Drawing.Size(90, 18)
        Me.txtTotalCreditoLocal.SoloLectura = True
        Me.txtTotalCreditoLocal.TabIndex = 3
        Me.txtTotalCreditoLocal.TabStop = False
        Me.txtTotalCreditoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCreditoLocal.Texto = "0"
        '
        'txtTotalDebitoLocal
        '
        Me.txtTotalDebitoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalDebitoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalDebitoLocal.Decimales = True
        Me.txtTotalDebitoLocal.Indicaciones = Nothing
        Me.txtTotalDebitoLocal.Location = New System.Drawing.Point(592, 4)
        Me.txtTotalDebitoLocal.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTotalDebitoLocal.Name = "txtTotalDebitoLocal"
        Me.txtTotalDebitoLocal.Size = New System.Drawing.Size(90, 18)
        Me.txtTotalDebitoLocal.SoloLectura = True
        Me.txtTotalDebitoLocal.TabIndex = 2
        Me.txtTotalDebitoLocal.TabStop = False
        Me.txtTotalDebitoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebitoLocal.Texto = "0"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(492, 4)
        Me.txtSaldo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(90, 18)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 1
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkBloquear)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 641)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(788, 33)
        Me.Panel1.TabIndex = 3
        '
        'chkBloquear
        '
        Me.chkBloquear.AutoSize = True
        Me.chkBloquear.Location = New System.Drawing.Point(9, 9)
        Me.chkBloquear.Name = "chkBloquear"
        Me.chkBloquear.Size = New System.Drawing.Size(68, 17)
        Me.chkBloquear.TabIndex = 0
        Me.chkBloquear.Text = "Bloquear"
        Me.chkBloquear.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 677)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(794, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VerDescuentosToolStripMenuItem, Me.ExportarAExcelToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(158, 48)
        '
        'VerDescuentosToolStripMenuItem
        '
        Me.VerDescuentosToolStripMenuItem.Name = "VerDescuentosToolStripMenuItem"
        Me.VerDescuentosToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.VerDescuentosToolStripMenuItem.Text = "Ver detalle"
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = Nothing
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = Nothing
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = Nothing
        Me.cbxSeccion.DataSource = Nothing
        Me.cbxSeccion.DataValueMember = Nothing
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(509, 147)
        Me.cbxSeccion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(267, 21)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 30
        Me.cbxSeccion.Texto = ""
        Me.cbxSeccion.Visible = False
        '
        'frmModificarAsiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 699)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmModificarAsiento"
        Me.Text = "frmModificarAsiento"
        Me.Panel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ocxCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents lblImporteLocal As System.Windows.Forms.Label
    Friend WithEvents txtTotalCreditoLocal As ERP.ocxTXTNumeric
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents txtTotalDebitoLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtImporteLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxCentroCosto As ERP.ocxCBX
    Friend WithEvents lblCentroCosto As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblOperacion2 As System.Windows.Forms.Label
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents txtDetalle As ERP.ocxTXTString
    Friend WithEvents txtNroComprobanteCabecera As ERP.ocxTXTString
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbxSucursalDetalle As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNroComprobante As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTipoComprobante As ERP.ocxTXTString
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents VerDescuentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportarAExcelToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblInformacionCaja As System.Windows.Forms.Label
    Friend WithEvents txtNroCaja As ERP.ocxTXTNumeric
    Friend WithEvents lblNroCaja As System.Windows.Forms.Label
    Friend WithEvents txtTipoComprobanteCabecera As ERP.ocxTXTString
    Friend WithEvents chkBloquear As System.Windows.Forms.CheckBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents Label5 As Label
    Friend WithEvents chkGastosMultiples As ocxCHK
    Friend WithEvents txtDepartamento As ocxTXTString
    Friend WithEvents Label6 As Label
    Friend WithEvents txtUnidadNegocio As ocxTXTString
    Friend WithEvents Label7 As Label
    Friend WithEvents cbxDepartamentoDetalle As ocxCBX
    Friend WithEvents Label9 As Label
    Friend WithEvents cbxUnidadNegocioDetalle1 As ocxCBX
    Friend WithEvents Label3 As Label
    Friend WithEvents cbxSeccionDetalle As ocxCBX
    Friend WithEvents Label8 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtSeccion As ocxTXTString
    Friend WithEvents cbxDepartamento As ocxCBX
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents cbxSeccion As ocxCBX
End Class

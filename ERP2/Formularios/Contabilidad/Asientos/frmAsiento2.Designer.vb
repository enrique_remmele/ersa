﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAsiento2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxCentroCosto = New ERP.ocxCBX()
        Me.lblCentroCosto = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion2 = New System.Windows.Forms.Label()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.colCuenta = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDenominacion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDebito = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colCredito = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ocxCuenta = New ERP.ocxTXTCuentaContable()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.txtTotalCreditoLocal = New ERP.ocxTXTNumeric()
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        Me.txtTotalDebitoLocal = New ERP.ocxTXTNumeric()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.txtImporteLocal = New ERP.ocxTXTNumeric()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.lblImporteLocal = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxCabecera.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxDetalle.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(261, 9)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(149, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(204, 13)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'cbxCentroCosto
        '
        Me.cbxCentroCosto.CampoWhere = Nothing
        Me.cbxCentroCosto.CargarUnaSolaVez = False
        Me.cbxCentroCosto.DataDisplayMember = Nothing
        Me.cbxCentroCosto.DataFilter = Nothing
        Me.cbxCentroCosto.DataOrderBy = Nothing
        Me.cbxCentroCosto.DataSource = Nothing
        Me.cbxCentroCosto.DataValueMember = Nothing
        Me.cbxCentroCosto.FormABM = Nothing
        Me.cbxCentroCosto.Indicaciones = Nothing
        Me.cbxCentroCosto.Location = New System.Drawing.Point(82, 90)
        Me.cbxCentroCosto.Name = "cbxCentroCosto"
        Me.cbxCentroCosto.SeleccionObligatoria = False
        Me.cbxCentroCosto.Size = New System.Drawing.Size(205, 21)
        Me.cbxCentroCosto.SoloLectura = False
        Me.cbxCentroCosto.TabIndex = 15
        Me.cbxCentroCosto.Texto = ""
        '
        'lblCentroCosto
        '
        Me.lblCentroCosto.AutoSize = True
        Me.lblCentroCosto.Location = New System.Drawing.Point(7, 94)
        Me.lblCentroCosto.Name = "lblCentroCosto"
        Me.lblCentroCosto.Size = New System.Drawing.Size(50, 13)
        Me.lblCentroCosto.TabIndex = 14
        Me.lblCentroCosto.Text = "C. Costo:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(375, 36)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(87, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 10
        Me.cbxMoneda.TabStop = False
        Me.cbxMoneda.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(328, 40)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 9
        Me.lblMoneda.Text = "Moneda:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(477, 321)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 8
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(462, 36)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(83, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 11
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.cbxCentroCosto)
        Me.gbxCabecera.Controls.Add(Me.lblCentroCosto)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion2)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Location = New System.Drawing.Point(7, 2)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(561, 119)
        Me.gbxCabecera.TabIndex = 5
        Me.gbxCabecera.TabStop = False
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(CType(0, Long))
        Me.txtFecha.Location = New System.Drawing.Point(462, 9)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(83, 20)
        Me.txtFecha.SoloLectura = True
        Me.txtFecha.TabIndex = 6
        Me.txtFecha.TabStop = False
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(416, 13)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 5
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(82, 9)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.TabStop = False
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(145, 9)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion2
        '
        Me.lblOperacion2.AutoSize = True
        Me.lblOperacion2.Location = New System.Drawing.Point(7, 13)
        Me.lblOperacion2.Name = "lblOperacion2"
        Me.lblOperacion2.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion2.TabIndex = 0
        Me.lblOperacion2.Text = "Operacion:"
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(7, 67)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 12
        Me.lblDetalle.Text = "Detalle:"
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(82, 63)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(463, 21)
        Me.txtDetalle.SoloLectura = False
        Me.txtDetalle.TabIndex = 13
        Me.txtDetalle.TabStop = False
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(7, 40)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 7
        Me.lblComprobante.Text = "Comprobante:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(82, 36)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(240, 21)
        Me.txtComprobante.SoloLectura = True
        Me.txtComprobante.TabIndex = 8
        Me.txtComprobante.TabStop = False
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'lvLista
        '
        Me.lvLista.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colCuenta, Me.colDenominacion, Me.colDebito, Me.colCredito})
        Me.lvLista.FullRowSelect = True
        Me.lvLista.GridLines = True
        Me.lvLista.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvLista.HideSelection = False
        Me.lvLista.Location = New System.Drawing.Point(7, 60)
        Me.lvLista.MultiSelect = False
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(548, 97)
        Me.lvLista.TabIndex = 6
        Me.lvLista.UseCompatibleStateImageBehavior = False
        Me.lvLista.View = System.Windows.Forms.View.Details
        '
        'colCuenta
        '
        Me.colCuenta.Text = "Cuenta"
        Me.colCuenta.Width = 106
        '
        'colDenominacion
        '
        Me.colDenominacion.Text = "Denominacion"
        Me.colDenominacion.Width = 248
        '
        'colDebito
        '
        Me.colDebito.Text = "Debito"
        Me.colDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colDebito.Width = 92
        '
        'colCredito
        '
        Me.colCredito.Text = "Credito"
        Me.colCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colCredito.Width = 90
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(396, 321)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 7
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ocxCuenta
        '
        Me.ocxCuenta.AlturaMaxima = 125
        Me.ocxCuenta.Consulta = Nothing
        Me.ocxCuenta.IDCentroCosto = 0
        Me.ocxCuenta.IDUnidadNegocio = 0
        Me.ocxCuenta.ListarTodas = False
        Me.ocxCuenta.Location = New System.Drawing.Point(7, 32)
        Me.ocxCuenta.Name = "ocxCuenta"
        Me.ocxCuenta.Registro = Nothing
        Me.ocxCuenta.Resolucion173 = False
        Me.ocxCuenta.Seleccionado = False
        Me.ocxCuenta.Size = New System.Drawing.Size(355, 24)
        Me.ocxCuenta.SoloLectura = False
        Me.ocxCuenta.TabIndex = 1
        Me.ocxCuenta.Texto = Nothing
        Me.ocxCuenta.whereFiltro = Nothing
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(7, 16)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 0
        Me.lblCuenta.Text = "Cuenta:"
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(364, 34)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(90, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 352)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(574, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'txtTotalCreditoLocal
        '
        Me.txtTotalCreditoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalCreditoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalCreditoLocal.Decimales = True
        Me.txtTotalCreditoLocal.Indicaciones = Nothing
        Me.txtTotalCreditoLocal.Location = New System.Drawing.Point(455, 159)
        Me.txtTotalCreditoLocal.Name = "txtTotalCreditoLocal"
        Me.txtTotalCreditoLocal.Size = New System.Drawing.Size(90, 22)
        Me.txtTotalCreditoLocal.SoloLectura = True
        Me.txtTotalCreditoLocal.TabIndex = 10
        Me.txtTotalCreditoLocal.TabStop = False
        Me.txtTotalCreditoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCreditoLocal.Texto = "0"
        '
        'gbxDetalle
        '
        Me.gbxDetalle.Controls.Add(Me.ocxCuenta)
        Me.gbxDetalle.Controls.Add(Me.lblCuenta)
        Me.gbxDetalle.Controls.Add(Me.lvLista)
        Me.gbxDetalle.Controls.Add(Me.cbxTipo)
        Me.gbxDetalle.Controls.Add(Me.txtTotalCreditoLocal)
        Me.gbxDetalle.Controls.Add(Me.txtTotalDebitoLocal)
        Me.gbxDetalle.Controls.Add(Me.lblSaldo)
        Me.gbxDetalle.Controls.Add(Me.lblTipo)
        Me.gbxDetalle.Controls.Add(Me.txtImporteLocal)
        Me.gbxDetalle.Controls.Add(Me.txtSaldo)
        Me.gbxDetalle.Controls.Add(Me.lblImporteLocal)
        Me.gbxDetalle.Location = New System.Drawing.Point(7, 127)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(561, 188)
        Me.gbxDetalle.TabIndex = 6
        Me.gbxDetalle.TabStop = False
        '
        'txtTotalDebitoLocal
        '
        Me.txtTotalDebitoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalDebitoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalDebitoLocal.Decimales = True
        Me.txtTotalDebitoLocal.Indicaciones = Nothing
        Me.txtTotalDebitoLocal.Location = New System.Drawing.Point(363, 159)
        Me.txtTotalDebitoLocal.Name = "txtTotalDebitoLocal"
        Me.txtTotalDebitoLocal.Size = New System.Drawing.Size(90, 22)
        Me.txtTotalDebitoLocal.SoloLectura = True
        Me.txtTotalDebitoLocal.TabIndex = 9
        Me.txtTotalDebitoLocal.TabStop = False
        Me.txtTotalDebitoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebitoLocal.Texto = "0"
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.Location = New System.Drawing.Point(154, 164)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 13)
        Me.lblSaldo.TabIndex = 7
        Me.lblSaldo.Text = "Saldo:"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(365, 16)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 2
        Me.lblTipo.Text = "Tipo:"
        '
        'txtImporteLocal
        '
        Me.txtImporteLocal.Color = System.Drawing.Color.Empty
        Me.txtImporteLocal.Decimales = True
        Me.txtImporteLocal.Indicaciones = Nothing
        Me.txtImporteLocal.Location = New System.Drawing.Point(455, 33)
        Me.txtImporteLocal.Name = "txtImporteLocal"
        Me.txtImporteLocal.Size = New System.Drawing.Size(90, 22)
        Me.txtImporteLocal.SoloLectura = False
        Me.txtImporteLocal.TabIndex = 5
        Me.txtImporteLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteLocal.Texto = "0"
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(197, 159)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(90, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 8
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'lblImporteLocal
        '
        Me.lblImporteLocal.AutoSize = True
        Me.lblImporteLocal.Location = New System.Drawing.Point(500, 16)
        Me.lblImporteLocal.Name = "lblImporteLocal"
        Me.lblImporteLocal.Size = New System.Drawing.Size(45, 13)
        Me.lblImporteLocal.TabIndex = 4
        Me.lblImporteLocal.Text = "Importe:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmAsiento2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(574, 374)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxDetalle)
        Me.Name = "frmAsiento2"
        Me.Text = "frmAsiento2"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxCentroCosto As ERP.ocxCBX
    Friend WithEvents lblCentroCosto As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblOperacion2 As System.Windows.Forms.Label
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents txtDetalle As ERP.ocxTXTString
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents colCuenta As System.Windows.Forms.ColumnHeader
    Friend WithEvents colDenominacion As System.Windows.Forms.ColumnHeader
    Friend WithEvents colDebito As System.Windows.Forms.ColumnHeader
    Friend WithEvents colCredito As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ocxCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents txtTotalCreditoLocal As ERP.ocxTXTNumeric
    Friend WithEvents gbxDetalle As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotalDebitoLocal As ERP.ocxTXTNumeric
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents txtImporteLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents lblImporteLocal As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
End Class

﻿Public Class frmConsultaAsientoContable

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'TextBox
        txtCantidadDetalle.txt.ResetText()
        txtCantidadOperacion.txt.ResetText()
        txtComprobante.ResetText()
        txtTotalDebito.txt.ResetText()
        txtTotalOperacion.txt.ResetText()

        'CheckBox
        chkComprobante.Checked = False
        chkSucursal.Checked = False

        'ComboBox
        cbxComprobante.Enabled = False
        cbxSucursal.Enabled = False

        'RadioButton

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxComprobante, "Select ID, Codigo From TipoComprobante ")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Comprobante
        chkComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", "False")
        cbxComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", "")
        txtComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", "")


    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", chkComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", cbxComprobante.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", txtComprobante.Text)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Consulta = "Select Numero, Suc, Fecha, Moneda, 'T. Comp.'=TipoComprobante, 'Comprobante'=NroComprobante, Detalle, Total, Estado, IDTransaccion From VAsiento "

        Where = Condicion

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Comprobante
        If chkComprobante.Checked = True Then

            If EstablecerCondicion(cbxComprobante, chkComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
                Exit Sub
            Else
                If Where = "" Then
                    Where = " Where (Comprobante Like '%" & txtComprobante.Text.Trim & "%') "
                Else
                    Where = Where & " And (Comprobante Like '%" & txtComprobante.Text.Trim & "%') "
                End If
            End If

        End If

        If Numero > 0 Then
            Where = " Where Numero=" & Numero
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable(Consulta & " " & Where & " Order By Fecha ")
        DataGridView1.DataSource = dt

        'Formato
        DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        DataGridView1.RowHeadersVisible = False
        DataGridView1.BackgroundColor = Color.White

        DataGridView1.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(6).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView1.Columns(7).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(8).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        DataGridView1.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        DataGridView1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        DataGridView1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        DataGridView1.Columns(7).DefaultCellStyle.Format = "N0"

        DataGridView1.Columns(9).Visible = False

        txtCantidadOperacion.txt.Text = DataGridView1.RowCount
        CSistema.TotalesGrid(DataGridView1, txtTotalOperacion.txt, 7, True)


    End Sub

    'Listar Detalle
    Sub ListarDetalle()

        ctrError.Clear()

        DataGridView2.DataSource = Nothing

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(DataGridView1, Mensaje)
            ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(DataGridView1.CurrentRow.Cells(9).Value)
        Dim Consulta As String = "Select Codigo, Descripcion, 'Debito'=Isnull(Debito,0), 'Credito'=Isnull(Credito,0), Observacion, Moneda From VDetalleAsiento Where IDTransaccion=" & IDTransaccion & " Order By Orden"
        Dim dt As DataTable = CSistema.ExecuteToDataTable(Consulta)
        DataGridView2.DataSource = dt

        'Formato
        DataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        DataGridView2.RowHeadersVisible = False
        DataGridView2.BackgroundColor = Color.White

        DataGridView2.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView2.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView2.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView2.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView2.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView2.Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        DataGridView2.Columns(2).DefaultCellStyle.Format = "N0"
        DataGridView2.Columns(3).DefaultCellStyle.Format = "N0"

        txtCantidadDetalle.txt.Text = DataGridView2.Rows.Count
        CSistema.TotalesGrid(DataGridView2, txtTotalCredito.txt, 3)
        CSistema.TotalesGrid(DataGridView2, txtTotalDebito.txt, 2)

        txtSaldo.SetValue(CStr(txtTotalCredito.ObtenerValor - txtTotalDebito.ObtenerValor))

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        'If lvOperacion.SelectedItems.Count = 0 Then
        '    Dim Mensaje As String = "Seleccione correctamente un registro!"
        '    ctrError.SetError(lvOperacion, Mensaje)
        '    ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
        '    Exit Sub
        'End If

        'If IsNumeric(lvOperacion.SelectedItems(0).SubItems(13).Text) = False Then
        '    Dim Mensaje As String = "Seleccione correctamente un registro!"
        '    ctrError.SetError(lvOperacion, Mensaje)
        '    ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
        '    Exit Sub
        'End If

        ''Obtener el IDTransaccion
        'IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(13).Text)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Sub Visualizar()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(DataGridView1, Mensaje)
            ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Text = "Asiento"

        Dim IDTransaccion As Integer = 0

        IDTransaccion = DataGridView1.SelectedRows(0).Cells(9).Value

        frm.IDTransaccion = IDTransaccion

        'Mostramos
        frm.ShowDialog(Me)

    End Sub

    Sub Modificar()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(DataGridView1, Mensaje)
            ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim frm As New frmModificarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.IDTransaccion = IDTransaccion
        frm.CAsiento.dtAsiento = CSistema.ExecuteToDataTable("Select * From VAsiento Where IDTransaccion=" & IDTransaccion)
        frm.CAsiento.dtDetalleAsiento = CSistema.ExecuteToDataTable("Select * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion)
        frm.CAsiento.IDTransaccion = IDTransaccion

        'Mostramos
        frm.ShowDialog(Me)

        If frm.Procesado = True Then
            Inicializar()
        End If

    End Sub

    Sub Nuevo()

        Dim frm As New frmAgregarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Text = "Asiento"

        'Mostramos
        frm.ShowDialog(Me)
        Dim NumeroAsiento As Integer

        If frm.Procesado Then
            If frm.IDTransaccionGuardado > 0 Then
                NumeroAsiento = CSistema.ExecuteScalar("Select Numero from asiento where idtransaccion = " & frm.IDTransaccionGuardado)
                txtNumeroAsiento.Text = NumeroAsiento
                ListarOperaciones(NumeroAsiento)
            End If
        End If



    End Sub

    Sub Eliminar()

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "DEL", ParameterDirection.Input)
        
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAsiento", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Else
            MessageBox.Show(MensajeRetorno, "Eliminar", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnRegistrosDelDia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelDia.Click
        Where = " Where (DateDiff(dd, Fecha, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub frmConsultaCompra_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub chkComprobante_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkComprobante.CheckedChanged
        If chkComprobante.Checked = True Then
            cbxComprobante.Enabled = True
            txtComprobante.Enabled = True
        Else
            cbxComprobante.Enabled = False
            txtComprobante.Enabled = False
        End If

    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub btnRegistrosDelMes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosDelMes.Click
        Where = " Where (DateDiff(MM, Fecha, GetDate())=0) "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub btnRegistrosGenerales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosGenerales.Click
        ListarOperaciones()
    End Sub

    Private Sub dtbRegistrosPorFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtbRegistrosPorFecha.Click
        Where = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        ListarDetalle()
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        ListarDetalle()
    End Sub

    Private Sub btnVizualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVizualizar.Click
        Visualizar()
    End Sub

    Private Sub txtNumeroAsiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroAsiento.KeyUp
        If e.KeyCode = Keys.Enter Then

            If IsNumeric(txtNumeroAsiento.Text) = False Then
                Exit Sub
            End If

            Dim Numero As Integer = CInt(txtNumeroAsiento.Text)
            ListarOperaciones(Numero)

        End If
        
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnRegistrosNoBalanceados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistrosNoBalanceados.Click
        Where = " Where Balanceado='False' And (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "') "
        ListarOperaciones(0, Where)
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    'FA 28/05/2021
    Sub frmConsultaAsientoContable_Activate()
        Me.Refresh()
    End Sub

End Class
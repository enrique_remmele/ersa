﻿Public Class frmAsiento2

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS

    'PROPIEDADES
    Private CAsientoValue As New CAsiento
    Public Property CAsiento() As CAsiento
        Get
            Return CAsientoValue
        End Get
        Set(ByVal value As CAsiento)
            CAsientoValue = value
        End Set
    End Property

    Private oRowCabeceraValue As DataRow
    Public Property oRowCabecera() As DataRow
        Get
            Return oRowCabeceraValue
        End Get
        Set(ByVal value As DataRow)
            oRowCabeceraValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private IDTransaccionOperacionValue As Integer
    Public Property IDTransaccionOperacion() As Integer
        Get
            Return IDTransaccionOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionOperacionValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Controles
        ocxCuenta.Conectar()

        FGEstiloFormulario(Me)

        'Funciones
        CargarInformacion()

        'Foco
        ocxCuenta.Focus()
        ocxCuenta.txtDescripcion.Focus()

    End Sub

    Sub CargarInformacion()

        'Detalle Asiento
        'CAsiento.dtDetalleAsiento = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleAsiento").Clone

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From VSucursal Order By 2")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Centro de Costos
        CSistema.SqlToComboBox(cbxCentroCosto.cbx, "Select Distinct ID, Descripcion From CentroCosto Order By 2")

        'Operacion
        cbxTipo.cbx.Items.Add("DEBITO")
        cbxTipo.cbx.Items.Add("CREDITO")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Cargar la Cabecera
        cbxCiudad.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDCiudad").ToString
        cbxSucursal.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDSucursal").ToString
        txtID.txt.Text = CInt(CSistema.ExecuteScalar("Select IsNull(Max(Numero)+1,1) From Asiento"))
        txtFecha.SetValue(CAsiento.dtAsiento.Rows(0)("Fecha").ToString)
        cbxMoneda.cbx.SelectedValue = CAsiento.dtAsiento.Rows(0)("IDMoneda").ToString
        txtCotizacion.txt.Text = CAsiento.dtAsiento.Rows(0)("Cotizacion").ToString
        txtComprobante.txt.Text = CAsiento.dtAsiento.Rows(0)("Comprobante").ToString
        txtDetalle.txt.Text = CAsiento.dtAsiento.Rows(0)("Detalle").ToString

        'Bloquear controles
        cbxCiudad.Locked = False
        cbxSucursal.Locked = False
        txtID.SoloLectura = True
        txtFecha.SoloLectura = True
        'cbxMoneda.Locked = False
        'txtCotizacion.SoloLectura = True
        txtComprobante.SoloLectura = True
        txtDetalle.SoloLectura = True

    End Sub

    Sub CargarItem()

        'Validar
        'Cuenta
        If ocxCuenta.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente la cuenta!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Importe
        If IsNumeric(txtImporteLocal.ObtenerValor) = False Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtImporteLocal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If cbxTipo.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = CAsiento.dtDetalleAsiento.NewRow()
        dRow("IDTransaccion") = 0
        dRow("IDCuentaContable") = ocxCuenta.Registro("ID").ToString
        dRow("Cuenta") = ocxCuenta.Registro("Cuenta").ToString
        dRow("Codigo") = ocxCuenta.Registro("Codigo").ToString
        dRow("Descripcion") = ocxCuenta.Registro("Descripcion").ToString
        dRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count

        Dim Importe As Decimal = CDec(txtImporteLocal.ObtenerValor)

        If cbxTipo.cbx.Text = "CREDITO" Then
            dRow("Credito") = Importe
            dRow("Debito") = 0
        End If

        If cbxTipo.cbx.Text = "DEBITO" Then
            dRow("Credito") = 0
            dRow("Debito") = Importe
        End If

        dRow("Importe") = Importe
        dRow("Observacion") = ""
        dRow("Fijo") = False

        CAsiento.dtDetalleAsiento.Rows.Add(dRow)

        'Listar Detalle
        ListarDetalle()

        'Caluclar Totales
        CalcularTotales()

        'Inicializar controles
        ocxCuenta.txtDescripcion.Clear()
        txtImporteLocal.txt.Clear()
        ocxCuenta.Focus()

    End Sub

    Sub ListarDetalle()


        'Limpiamos todo el detalle
        lvLista.Items.Clear()

        'Cargamos registro por registro
        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
            Dim item As ListViewItem = lvLista.Items.Add(oRow("Codigo").ToString)
            item.SubItems.Add(oRow("Descripcion").ToString)
            item.SubItems.Add(CSistema.FormatoNumero(oRow("Debito").ToString))
            item.SubItems.Add(CSistema.FormatoMoneda(oRow("Credito").ToString))
        Next

    End Sub

    Public Sub CalcularTotales()

        CSistema.TotalesLv(lvLista, txtTotalDebitoLocal.txt, 2)
        CSistema.TotalesLv(lvLista, txtTotalCreditoLocal.txt, 3)

        'Hayar el saldo
        Dim Saldo As Decimal = 0
        Saldo = CDec(txtTotalCreditoLocal.ObtenerValor) - CDec(txtTotalDebitoLocal.ObtenerValor)

        txtSaldo.txt.Text = Saldo

        If CAsiento.dtAsiento.Rows.Count > 0 Then
            CAsiento.dtAsiento.Rows(0)("Debito") = txtTotalDebitoLocal.ObtenerValor
            CAsiento.dtAsiento.Rows(0)("Credito") = txtTotalCreditoLocal.ObtenerValor
            If txtTotalCreditoLocal.ObtenerValor > txtTotalDebitoLocal.ObtenerValor Then
                CAsiento.dtAsiento.Rows(0)("Total") = txtTotalCreditoLocal.ObtenerValor
            Else
                CAsiento.dtAsiento.Rows(0)("Total") = txtTotalDebitoLocal.ObtenerValor
            End If

            CAsiento.dtAsiento.Rows(0)("Saldo") = Saldo
        End If

    End Sub

    Sub EliminarItem()

        'Validar
        If lvLista.SelectedItems.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        CAsiento.dtDetalleAsiento.Rows(lvLista.SelectedIndices(0)).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()
        CalcularTotales()

    End Sub

    Sub Procesar()

        'Validar
        'Detalle
        If CAsiento.dtAsiento.Rows.Count = 0 Then
            Dim mensaje As String = "Ingrese el detalle!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Montos
        Dim Debitos As Decimal = txtTotalDebitoLocal.ObtenerValor
        Dim Creditos As Decimal = txtTotalCreditoLocal.ObtenerValor
        Dim Saldo As Decimal = Creditos - Debitos

        If Saldo <> 0 Then
            Dim mensaje As String = "Ingrese el detalle!"
            ctrError.SetError(lvLista, mensaje)
            ctrError.SetIconAlignment(lvLista, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Total

    End Sub

    Sub Salir()

    End Sub

    Private Sub frmAsiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtImporteLocal_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporteLocal.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarItem()
        End If
    End Sub

    Private Sub ocxCuenta_ItemSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxCuenta.ItemSeleccionado
        If ocxCuenta.Seleccionado = True Then
            cbxTipo.cbx.Focus()
        End If
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarItem()
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.Close()
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Public Sub New(ByVal vIDOperacion As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        'CAsientoValue.IDOperacion = vIDOperacion

    End Sub

    'FA 28/05/2021
    Sub frmAsiento2_Activate()
        Me.Refresh()
    End Sub

End Class
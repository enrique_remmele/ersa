﻿Public Class frmVisualizarAsiento

    'CLASES
    Dim CSistema As New CSistema

    'PROPIADES
    Private CAsientoValue As New CAsiento
    Public Property CAsiento() As CAsiento
        Get
            Return CAsientoValue
        End Get
        Set(ByVal value As CAsiento)
            CAsientoValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private FiltrarValue As Boolean
    Public Property Filtrar() As Boolean
        Get
            Return FiltrarValue
        End Get
        Set(ByVal value As Boolean)
            FiltrarValue = value
        End Set
    End Property

    Private whereFiltrovalue As String
    Public Property whereFiltro() As String
        Get
            Return whereFiltrovalue
        End Get
        Set(ByVal value As String)
            whereFiltrovalue = value
        End Set
    End Property


    'FUNCIONES
    Sub Inicializar()

        'Apariencia
        FGEstiloFormulario(Me)

        CAsiento.CargarOperacion(IDTransaccion)

        ' Validar
        If CAsiento.dtAsiento.Rows.Count = 0 Then
            MessageBox.Show("El sistema no encontro ningun asiento asociado! Se debe volver a crear.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            btnModificar.Visible = False
            btnCrear.Visible = True
            Exit Sub
        End If

        btnModificar.Visible = True
        btnCrear.Visible = False

        'Cargar Cabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.Rows(0)
        txtCiudad.txt.Text = oRow("Ciudad").ToString
        txtSucursal.txt.Text = oRow("Sucursal").ToString
        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.txt.Text = oRow("Fecha").ToString
        txtMoneda.txt.Text = oRow("Moneda").ToString
        txtCotizacion.txt.Text = oRow("Cotizacion").ToString
        'txtCentroCosto.txt.Text = oRow("CentroCosto").ToString
        'FA 21/11/2022 - Plan de cuenta
        txtUnidadNegocio.txt.Text = oRow("UnidadNegocio").ToString
        txtDepartamento.txt.Text = oRow("Departamento").ToString
        txtSeccion.txt.Text = oRow("Seccion").ToString

        If CBool(oRow("GastosMultiples").ToString) <> 0 Then
            chkGastosMultiples.chk.Checked = True
        Else
            chkGastosMultiples.chk.Checked = False
        End If
        If chkGastosMultiples.chk.Checked = True Then
            Label4.Visible = False
            Label5.Visible = True
        Else
            Label4.Visible = True
            Label5.Visible = False
        End If
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtDetalle.txt.Text = oRow("Detalle").ToString
        chkBloquear.Checked = CBool(oRow("Bloquear").ToString)

        'Caja
        If IsNumeric(oRow("NroCaja").ToString) = True Then
            txtNroCaja.SetValue(oRow("NroCaja").ToString)
            lblInformacionCaja.Text = oRow("EstadoCaja").ToString
        Else
            txtNroCaja.SetValue(0)
            lblInformacionCaja.ResetText()
        End If

        'Cargar columnas
        DataGridView1.Columns.Add("colCodigo", "Codigo")
        DataGridView1.Columns.Add("colDescripcion", "Descripcion")
        DataGridView1.Columns.Add("colDetalle", "Detalle")
        DataGridView1.Columns.Add("col", "Comprobante")
        DataGridView1.Columns.Add("colDebito", "Debito")
        DataGridView1.Columns.Add("colCredito", "Credito")

        DataGridView1.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView1.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView1.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridView1.Columns(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        DataGridView1.Columns("colDebito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        DataGridView1.Columns("colCredito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        DataGridView1.Columns("colDebito").DefaultCellStyle.Format = "N0"
        DataGridView1.Columns("colCredito").DefaultCellStyle.Format = "N0"


        'Cagar ddetalle
        CAsiento.ListarDetalle(DataGridView1)

        CalcularTotales()

        'FA 02/12/2022 - Plan de cuenta
        chkGastosMultiples.SoloLectura = True
    End Sub

    Public Sub CalcularTotales()

        CSistema.TotalesGrid(DataGridView1, txtTotalDebitoLocal.txt, 4)
        CSistema.TotalesGrid(DataGridView1, txtTotalCreditoLocal.txt, 5)

        'Hayar el saldo
        Dim Saldo As Decimal = 0
        Saldo = CDec(txtTotalCreditoLocal.ObtenerValor) - CDec(txtTotalDebitoLocal.ObtenerValor)

        txtSaldo.txt.Text = Saldo


    End Sub

    Sub Modificar()

        Dim frm As New frmModificarAsiento
        frm.IDTransaccion = IDTransaccion
        frm.CAsiento = CAsiento
        frm.CAsiento.IDTransaccion = IDTransaccion
        frm.Filtrar = FiltrarValue
        frm.whereFiltro = whereFiltrovalue

        'Mostramos
        FGMostrarFormulario(Me, frm, "Modificar Asiento", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, False)

        If frm.Procesado = True Then
            Me.Close()
        End If

    End Sub

    Sub Crear()

        Dim frm As New frmAgregarAsiento
        frm.IDTransaccionOperacion = IDTransaccion
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Text = "Asiento"

        'Mostramos
        frm.ShowDialog(Me)

        FGMostrarFormulario(Me, frm, "Asientos Contables", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = False Then
            Exit Sub
        End If

        FGMostrarFormulario(Me, frm, "Asientos Contables", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Private Sub frmVisualizarAsiento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub frmVisualizarAsiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Me.Close()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnCrear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCrear.Click
        Crear()
    End Sub

    'FA 28/05/2021
    Sub frmVisualizarAsiento_Activate()
        Me.Refresh()
    End Sub

End Class
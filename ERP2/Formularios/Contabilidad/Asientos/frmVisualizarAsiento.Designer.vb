﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVisualizarAsiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkGastosMultiples = New ERP.ocxCHK()
        Me.txtDepartamento = New ERP.ocxTXTString()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtUnidadNegocio = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblInformacionCaja = New System.Windows.Forms.Label()
        Me.txtNroCaja = New ERP.ocxTXTNumeric()
        Me.lblNroCaja = New System.Windows.Forms.Label()
        Me.txtSucursal = New ERP.ocxTXTString()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.txtFecha = New ERP.ocxTXTString()
        Me.txtCiudad = New ERP.ocxTXTString()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion2 = New System.Windows.Forms.Label()
        Me.lblDetalle = New System.Windows.Forms.Label()
        Me.txtDetalle = New ERP.ocxTXTString()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCrear = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalCreditoLocal = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTotalDebitoLocal = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.lblSaldo = New System.Windows.Forms.Label()
        Me.chkBloquear = New System.Windows.Forms.CheckBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtSeccion = New ERP.ocxTXTString()
        Me.gbxCabecera.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(589, 15)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 9
        Me.lblMoneda.Text = "Moneda:"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.Label6)
        Me.gbxCabecera.Controls.Add(Me.txtSeccion)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.chkGastosMultiples)
        Me.gbxCabecera.Controls.Add(Me.txtDepartamento)
        Me.gbxCabecera.Controls.Add(Me.Label4)
        Me.gbxCabecera.Controls.Add(Me.txtUnidadNegocio)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.lblInformacionCaja)
        Me.gbxCabecera.Controls.Add(Me.txtNroCaja)
        Me.gbxCabecera.Controls.Add(Me.lblNroCaja)
        Me.gbxCabecera.Controls.Add(Me.txtSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.txtCiudad)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion2)
        Me.gbxCabecera.Controls.Add(Me.lblDetalle)
        Me.gbxCabecera.Controls.Add(Me.txtDetalle)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.Location = New System.Drawing.Point(3, 3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(798, 178)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(237, 98)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(129, 13)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Departamento Solicitante:"
        '
        'chkGastosMultiples
        '
        Me.chkGastosMultiples.BackColor = System.Drawing.Color.Transparent
        Me.chkGastosMultiples.Color = System.Drawing.Color.Empty
        Me.chkGastosMultiples.Location = New System.Drawing.Point(87, 68)
        Me.chkGastosMultiples.Name = "chkGastosMultiples"
        Me.chkGastosMultiples.Size = New System.Drawing.Size(205, 21)
        Me.chkGastosMultiples.SoloLectura = False
        Me.chkGastosMultiples.TabIndex = 23
        Me.chkGastosMultiples.Texto = "Asignacion de Unidad y Departamento"
        Me.chkGastosMultiples.Valor = False
        '
        'txtDepartamento
        '
        Me.txtDepartamento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartamento.Color = System.Drawing.Color.Empty
        Me.txtDepartamento.Indicaciones = Nothing
        Me.txtDepartamento.Location = New System.Drawing.Point(240, 117)
        Me.txtDepartamento.Multilinea = False
        Me.txtDepartamento.Name = "txtDepartamento"
        Me.txtDepartamento.Size = New System.Drawing.Size(292, 21)
        Me.txtDepartamento.SoloLectura = True
        Me.txtDepartamento.TabIndex = 22
        Me.txtDepartamento.TabStop = False
        Me.txtDepartamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDepartamento.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(237, 98)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(145, 13)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Departamento que Consume:"
        '
        'txtUnidadNegocio
        '
        Me.txtUnidadNegocio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.txtUnidadNegocio.Indicaciones = Nothing
        Me.txtUnidadNegocio.Location = New System.Drawing.Point(14, 117)
        Me.txtUnidadNegocio.Multilinea = False
        Me.txtUnidadNegocio.Name = "txtUnidadNegocio"
        Me.txtUnidadNegocio.Size = New System.Drawing.Size(220, 21)
        Me.txtUnidadNegocio.SoloLectura = True
        Me.txtUnidadNegocio.TabIndex = 20
        Me.txtUnidadNegocio.TabStop = False
        Me.txtUnidadNegocio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUnidadNegocio.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 13)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Unidad de Negocio:"
        '
        'lblInformacionCaja
        '
        Me.lblInformacionCaja.AutoSize = True
        Me.lblInformacionCaja.ForeColor = System.Drawing.Color.DimGray
        Me.lblInformacionCaja.Location = New System.Drawing.Point(173, 16)
        Me.lblInformacionCaja.Name = "lblInformacionCaja"
        Me.lblInformacionCaja.Size = New System.Drawing.Size(53, 13)
        Me.lblInformacionCaja.TabIndex = 18
        Me.lblInformacionCaja.Tag = "No tocar"
        Me.lblInformacionCaja.Text = "__/__/__"
        '
        'txtNroCaja
        '
        Me.txtNroCaja.Color = System.Drawing.Color.Empty
        Me.txtNroCaja.Decimales = False
        Me.txtNroCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroCaja.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtNroCaja.Location = New System.Drawing.Point(87, 11)
        Me.txtNroCaja.Margin = New System.Windows.Forms.Padding(6, 5, 6, 5)
        Me.txtNroCaja.Name = "txtNroCaja"
        Me.txtNroCaja.Size = New System.Drawing.Size(63, 22)
        Me.txtNroCaja.SoloLectura = True
        Me.txtNroCaja.TabIndex = 17
        Me.txtNroCaja.TabStop = False
        Me.txtNroCaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroCaja.Texto = "0"
        '
        'lblNroCaja
        '
        Me.lblNroCaja.AutoSize = True
        Me.lblNroCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNroCaja.Location = New System.Drawing.Point(11, 14)
        Me.lblNroCaja.Name = "lblNroCaja"
        Me.lblNroCaja.Size = New System.Drawing.Size(71, 15)
        Me.lblNroCaja.TabIndex = 16
        Me.lblNroCaja.Text = "Nro. Caja:"
        '
        'txtSucursal
        '
        Me.txtSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursal.Color = System.Drawing.Color.Empty
        Me.txtSucursal.Indicaciones = Nothing
        Me.txtSucursal.Location = New System.Drawing.Point(283, 13)
        Me.txtSucursal.Multilinea = False
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.Size = New System.Drawing.Size(161, 21)
        Me.txtSucursal.SoloLectura = True
        Me.txtSucursal.TabIndex = 4
        Me.txtSucursal.TabStop = False
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(232, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(638, 11)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(68, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 10
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Indicaciones = Nothing
        Me.txtFecha.Location = New System.Drawing.Point(500, 12)
        Me.txtFecha.Multilinea = False
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Size = New System.Drawing.Size(83, 20)
        Me.txtFecha.SoloLectura = True
        Me.txtFecha.TabIndex = 6
        Me.txtFecha.TabStop = False
        Me.txtFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFecha.Texto = ""
        '
        'txtCiudad
        '
        Me.txtCiudad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCiudad.Color = System.Drawing.Color.Empty
        Me.txtCiudad.Indicaciones = Nothing
        Me.txtCiudad.Location = New System.Drawing.Point(87, 41)
        Me.txtCiudad.Multilinea = False
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(57, 21)
        Me.txtCiudad.SoloLectura = True
        Me.txtCiudad.TabIndex = 1
        Me.txtCiudad.TabStop = False
        Me.txtCiudad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCiudad.Texto = ""
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(706, 11)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(82, 21)
        Me.txtCotizacion.SoloLectura = True
        Me.txtCotizacion.TabIndex = 11
        Me.txtCotizacion.TabStop = False
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(454, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 5
        Me.lblFecha.Text = "Fecha:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(144, 41)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 2
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion2
        '
        Me.lblOperacion2.AutoSize = True
        Me.lblOperacion2.Location = New System.Drawing.Point(23, 44)
        Me.lblOperacion2.Name = "lblOperacion2"
        Me.lblOperacion2.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion2.TabIndex = 0
        Me.lblOperacion2.Text = "Operacion:"
        '
        'lblDetalle
        '
        Me.lblDetalle.AutoSize = True
        Me.lblDetalle.Location = New System.Drawing.Point(41, 148)
        Me.lblDetalle.Name = "lblDetalle"
        Me.lblDetalle.Size = New System.Drawing.Size(43, 13)
        Me.lblDetalle.TabIndex = 12
        Me.lblDetalle.Text = "Detalle:"
        '
        'txtDetalle
        '
        Me.txtDetalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDetalle.Color = System.Drawing.Color.Empty
        Me.txtDetalle.Indicaciones = Nothing
        Me.txtDetalle.Location = New System.Drawing.Point(89, 144)
        Me.txtDetalle.Multilinea = False
        Me.txtDetalle.Name = "txtDetalle"
        Me.txtDetalle.Size = New System.Drawing.Size(701, 21)
        Me.txtDetalle.SoloLectura = True
        Me.txtDetalle.TabIndex = 13
        Me.txtDetalle.TabStop = False
        Me.txtDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDetalle.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(210, 44)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 7
        Me.lblComprobante.Text = "Comprobante:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(286, 40)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(328, 21)
        Me.txtComprobante.SoloLectura = True
        Me.txtComprobante.TabIndex = 8
        Me.txtComprobante.TabStop = False
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(263, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(3, 3)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 3
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.gbxCabecera, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.DataGridView1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 184.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(804, 574)
        Me.TableLayoutPanel1.TabIndex = 4
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.97964!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.02036!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel2, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 537)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(798, 34)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(449, 3)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(341, 27)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnModificar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCrear)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(433, 27)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'btnCrear
        '
        Me.btnCrear.Location = New System.Drawing.Point(84, 3)
        Me.btnCrear.Name = "btnCrear"
        Me.btnCrear.Size = New System.Drawing.Size(75, 23)
        Me.btnCrear.TabIndex = 4
        Me.btnCrear.Text = "&Crear"
        Me.btnCrear.UseVisualStyleBackColor = True
        Me.btnCrear.Visible = False
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtTotalCreditoLocal)
        Me.FlowLayoutPanel3.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtTotalDebitoLocal)
        Me.FlowLayoutPanel3.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtSaldo)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblSaldo)
        Me.FlowLayoutPanel3.Controls.Add(Me.chkBloquear)
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 504)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(786, 27)
        Me.FlowLayoutPanel3.TabIndex = 4
        '
        'txtTotalCreditoLocal
        '
        Me.txtTotalCreditoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalCreditoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalCreditoLocal.Decimales = True
        Me.txtTotalCreditoLocal.Indicaciones = Nothing
        Me.txtTotalCreditoLocal.Location = New System.Drawing.Point(670, 3)
        Me.txtTotalCreditoLocal.Name = "txtTotalCreditoLocal"
        Me.txtTotalCreditoLocal.Size = New System.Drawing.Size(113, 22)
        Me.txtTotalCreditoLocal.SoloLectura = True
        Me.txtTotalCreditoLocal.TabIndex = 4
        Me.txtTotalCreditoLocal.TabStop = False
        Me.txtTotalCreditoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCreditoLocal.Texto = "0"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(616, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 25)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Credito:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTotalDebitoLocal
        '
        Me.txtTotalDebitoLocal.BackColor = System.Drawing.Color.LightGray
        Me.txtTotalDebitoLocal.Color = System.Drawing.Color.Empty
        Me.txtTotalDebitoLocal.Decimales = True
        Me.txtTotalDebitoLocal.Indicaciones = Nothing
        Me.txtTotalDebitoLocal.Location = New System.Drawing.Point(497, 3)
        Me.txtTotalDebitoLocal.Name = "txtTotalDebitoLocal"
        Me.txtTotalDebitoLocal.Size = New System.Drawing.Size(113, 22)
        Me.txtTotalDebitoLocal.SoloLectura = True
        Me.txtTotalDebitoLocal.TabIndex = 3
        Me.txtTotalDebitoLocal.TabStop = False
        Me.txtTotalDebitoLocal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalDebitoLocal.Texto = "0"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(433, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 25)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Debito:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(314, 3)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(113, 22)
        Me.txtSaldo.SoloLectura = True
        Me.txtSaldo.TabIndex = 2
        Me.txtSaldo.TabStop = False
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'lblSaldo
        '
        Me.lblSaldo.Location = New System.Drawing.Point(271, 0)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(37, 25)
        Me.lblSaldo.TabIndex = 1
        Me.lblSaldo.Text = "Saldo:"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkBloquear
        '
        Me.chkBloquear.AutoSize = True
        Me.chkBloquear.Enabled = False
        Me.chkBloquear.Location = New System.Drawing.Point(197, 3)
        Me.chkBloquear.Name = "chkBloquear"
        Me.chkBloquear.Size = New System.Drawing.Size(68, 17)
        Me.chkBloquear.TabIndex = 7
        Me.chkBloquear.Text = "Bloquear"
        Me.chkBloquear.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(3, 187)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(798, 311)
        Me.DataGridView1.StandardTab = True
        Me.DataGridView1.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(535, 98)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 13)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Seccion:"
        '
        'txtSeccion
        '
        Me.txtSeccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSeccion.Color = System.Drawing.Color.Empty
        Me.txtSeccion.Indicaciones = Nothing
        Me.txtSeccion.Location = New System.Drawing.Point(538, 117)
        Me.txtSeccion.Multilinea = False
        Me.txtSeccion.Name = "txtSeccion"
        Me.txtSeccion.Size = New System.Drawing.Size(251, 21)
        Me.txtSeccion.SoloLectura = True
        Me.txtSeccion.TabIndex = 25
        Me.txtSeccion.TabStop = False
        Me.txtSeccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSeccion.Texto = ""
        '
        'frmVisualizarAsiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(804, 574)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.KeyPreview = True
        Me.Name = "frmVisualizarAsiento"
        Me.Text = "frmVisualizarAsiento"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblOperacion2 As System.Windows.Forms.Label
    Friend WithEvents lblDetalle As System.Windows.Forms.Label
    Friend WithEvents txtDetalle As ERP.ocxTXTString
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents txtFecha As ERP.ocxTXTString
    Friend WithEvents txtCiudad As ERP.ocxTXTString
    Friend WithEvents txtSucursal As ERP.ocxTXTString
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtTotalCreditoLocal As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalDebitoLocal As ERP.ocxTXTNumeric
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCrear As System.Windows.Forms.Button
    Friend WithEvents lblInformacionCaja As System.Windows.Forms.Label
    Friend WithEvents txtNroCaja As ERP.ocxTXTNumeric
    Friend WithEvents lblNroCaja As System.Windows.Forms.Label
    Friend WithEvents chkBloquear As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents txtDepartamento As ocxTXTString
    Friend WithEvents Label4 As Label
    Friend WithEvents txtUnidadNegocio As ocxTXTString
    Friend WithEvents Label3 As Label
    Friend WithEvents chkGastosMultiples As ocxCHK
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtSeccion As ocxTXTString
End Class

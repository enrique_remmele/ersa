﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuentaFijas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Facturacion", 2, 2)
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Nota de Credito", 2, 2)
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Nota de Debito", 2, 2)
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Ventas", 0, 1, New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2, TreeNode3})
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Entrada por Compras", 2, 2)
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Nota de Credito", 2, 2)
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Nota de Debito", 2, 2)
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Compras", 0, 1, New System.Windows.Forms.TreeNode() {TreeNode5, TreeNode6, TreeNode7})
        Dim TreeNode9 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Movimientos", 2, 2)
        Dim TreeNode10 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Ajuste", 2, 2)
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Descarga", 2, 2)
        Dim TreeNode12 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Consumo Combustible", 2, 2)
        Dim TreeNode13 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Materia Prima Balanceado", 2, 2)
        Dim TreeNode14 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Descarga de Compras")
        Dim TreeNode15 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Ticket de Bascula")
        Dim TreeNode16 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Macheo Ticket Factura")
        Dim TreeNode17 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Stock", 0, 1, New System.Windows.Forms.TreeNode() {TreeNode9, TreeNode10, TreeNode11, TreeNode12, TreeNode13, TreeNode14, TreeNode15, TreeNode16})
        Dim TreeNode18 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Cobranzas", 2, 2)
        Dim TreeNode19 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Cheques Rechazados", 2, 2)
        Dim TreeNode20 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Descuento de Cheques", 2, 2)
        Dim TreeNode21 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Debitos y Creditos Bancarios", 2, 2)
        Dim TreeNode22 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Depositos Bancarios", 2, 2)
        Dim TreeNode23 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Orden de Pago", 2, 2)
        Dim TreeNode24 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Entrega de Cheque", 2, 2)
        Dim TreeNode25 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Gastos", 2, 2)
        Dim TreeNode26 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Vale")
        Dim TreeNode27 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Vencimiento de Cheque Dif.")
        Dim TreeNode28 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Vencimiento de Cheque Descontado")
        Dim TreeNode29 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Canje de Cheque")
        Dim TreeNode30 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Tesoreria", 0, 1, New System.Windows.Forms.TreeNode() {TreeNode18, TreeNode19, TreeNode20, TreeNode21, TreeNode22, TreeNode23, TreeNode24, TreeNode25, TreeNode26, TreeNode27, TreeNode28, TreeNode29})
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCuentaFijas))
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.tvArbol = New System.Windows.Forms.TreeView()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvArbol)
        Me.SplitContainer1.Size = New System.Drawing.Size(993, 402)
        Me.SplitContainer1.SplitterDistance = 221
        Me.SplitContainer1.TabIndex = 1
        '
        'tvArbol
        '
        Me.tvArbol.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvArbol.ImageIndex = 0
        Me.tvArbol.ImageList = Me.ImageList1
        Me.tvArbol.Location = New System.Drawing.Point(0, 0)
        Me.tvArbol.Name = "tvArbol"
        TreeNode1.ImageIndex = 2
        TreeNode1.Name = "Venta"
        TreeNode1.SelectedImageIndex = 2
        TreeNode1.Text = "Facturacion"
        TreeNode2.ImageIndex = 2
        TreeNode2.Name = "NotaCreditoVenta"
        TreeNode2.SelectedImageIndex = 2
        TreeNode2.Text = "Nota de Credito"
        TreeNode3.ImageIndex = 2
        TreeNode3.Name = "NotaDebitoVenta"
        TreeNode3.SelectedImageIndex = 2
        TreeNode3.Text = "Nota de Debito"
        TreeNode4.ImageIndex = 0
        TreeNode4.Name = "Nodo0"
        TreeNode4.SelectedImageIndex = 1
        TreeNode4.Text = "Ventas"
        TreeNode5.ImageIndex = 2
        TreeNode5.Name = "Compra"
        TreeNode5.SelectedImageIndex = 2
        TreeNode5.Text = "Entrada por Compras"
        TreeNode6.ImageIndex = 2
        TreeNode6.Name = "NotaCreditoCompra"
        TreeNode6.SelectedImageIndex = 2
        TreeNode6.Text = "Nota de Credito"
        TreeNode7.ImageIndex = 2
        TreeNode7.Name = "NotaDebitoCompra"
        TreeNode7.SelectedImageIndex = 2
        TreeNode7.Text = "Nota de Debito"
        TreeNode8.ImageIndex = 0
        TreeNode8.Name = "Nodo1"
        TreeNode8.SelectedImageIndex = 1
        TreeNode8.Text = "Compras"
        TreeNode9.ImageIndex = 2
        TreeNode9.Name = "Movimiento"
        TreeNode9.SelectedImageIndex = 2
        TreeNode9.Text = "Movimientos"
        TreeNode10.ImageIndex = 2
        TreeNode10.Name = "Ajuste"
        TreeNode10.SelectedImageIndex = 2
        TreeNode10.Text = "Ajuste"
        TreeNode11.ImageIndex = 2
        TreeNode11.Name = "Descarga"
        TreeNode11.SelectedImageIndex = 2
        TreeNode11.Text = "Descarga"
        TreeNode12.ImageIndex = 2
        TreeNode12.Name = "ConsumoCombustible"
        TreeNode12.SelectedImageIndex = 2
        TreeNode12.Text = "Consumo Combustible"
        TreeNode13.ImageIndex = 2
        TreeNode13.Name = "MateriaPrima"
        TreeNode13.SelectedImageIndex = 2
        TreeNode13.Text = "Materia Prima Balanceado"
        TreeNode14.ImageIndex = 2
        TreeNode14.Name = "DescargaCompra"
        TreeNode14.Text = "Descarga de Compras"
        TreeNode15.ImageIndex = 2
        TreeNode15.Name = "TicketBascula"
        TreeNode15.Text = "Ticket de Bascula"
        TreeNode16.ImageIndex = 2
        TreeNode16.Name = "MacheoTicketFactura"
        TreeNode16.Text = "Macheo Ticket Factura"
        TreeNode17.ImageIndex = 0
        TreeNode17.Name = "Nodo2"
        TreeNode17.SelectedImageIndex = 1
        TreeNode17.Text = "Stock"
        TreeNode18.ImageIndex = 2
        TreeNode18.Name = "Cobranzas"
        TreeNode18.SelectedImageIndex = 2
        TreeNode18.Text = "Cobranzas"
        TreeNode19.ImageIndex = 2
        TreeNode19.Name = "ChequeRechazado"
        TreeNode19.SelectedImageIndex = 2
        TreeNode19.Text = "Cheques Rechazados"
        TreeNode20.ImageIndex = 2
        TreeNode20.Name = "DescuentoCheque"
        TreeNode20.SelectedImageIndex = 2
        TreeNode20.Text = "Descuento de Cheques"
        TreeNode21.ImageIndex = 2
        TreeNode21.Name = "DebitoCreditoBancario"
        TreeNode21.SelectedImageIndex = 2
        TreeNode21.Text = "Debitos y Creditos Bancarios"
        TreeNode22.ImageIndex = 2
        TreeNode22.Name = "DepositoBancario"
        TreeNode22.SelectedImageIndex = 2
        TreeNode22.Text = "Depositos Bancarios"
        TreeNode23.ImageIndex = 2
        TreeNode23.Name = "OrdenPago"
        TreeNode23.SelectedImageIndex = 2
        TreeNode23.Text = "Orden de Pago"
        TreeNode24.ImageIndex = 2
        TreeNode24.Name = "EntregaChequeOP"
        TreeNode24.SelectedImageIndex = 2
        TreeNode24.Text = "Entrega de Cheque"
        TreeNode25.ImageIndex = 2
        TreeNode25.Name = "Gasto"
        TreeNode25.SelectedImageIndex = 2
        TreeNode25.Text = "Gastos"
        TreeNode26.ImageIndex = 2
        TreeNode26.Name = "Vale"
        TreeNode26.SelectedImageKey = "form.png"
        TreeNode26.Text = "Vale"
        TreeNode27.ImageIndex = 2
        TreeNode27.Name = "VencimientoChequeOP"
        TreeNode27.SelectedImageKey = "form.png"
        TreeNode27.Text = "Vencimiento de Cheque Dif."
        TreeNode28.ImageIndex = 2
        TreeNode28.Name = "VencimientoChequeDescontado"
        TreeNode28.SelectedImageKey = "form.png"
        TreeNode28.Text = "Vencimiento de Cheque Descontado"
        TreeNode29.ImageIndex = 2
        TreeNode29.Name = "CanjeCheque"
        TreeNode29.Text = "Canje de Cheque"
        TreeNode30.ImageIndex = 0
        TreeNode30.Name = "Nodo3"
        TreeNode30.SelectedImageIndex = 1
        TreeNode30.Text = "Tesoreria"
        Me.tvArbol.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode4, TreeNode8, TreeNode17, TreeNode30})
        Me.tvArbol.SelectedImageIndex = 0
        Me.tvArbol.Size = New System.Drawing.Size(217, 398)
        Me.tvArbol.TabIndex = 0
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "icon.folder.close.gif")
        Me.ImageList1.Images.SetKeyName(1, "icon.folder.open.gif")
        Me.ImageList1.Images.SetKeyName(2, "form.png")
        '
        'frmCuentaFijas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(993, 402)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "frmCuentaFijas"
        Me.Text = "frmCuentaFijas"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tvArbol As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
End Class

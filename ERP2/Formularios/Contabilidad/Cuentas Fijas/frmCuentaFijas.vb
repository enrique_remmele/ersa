﻿Public Class frmCuentaFijas

    'Ventas
    Dim WithEvents ocxCuentaFijaVenta As New ocxCuentaFijaVenta2
    Dim WithEvents ocxCuentaFijaNotaCredito As New ocxCuentaFijaNotaCredito
    Dim WithEvents ocxCuentaFijaNotaDebito As New ocxCuentaFijaNotaDebito

    'Compras
    Dim WithEvents ocxCuentaFijaCompra As New ocxCuentaFijaCompra
    Dim WithEvents ocxCuentaFijaNotaCreditoProveedor As New ocxCuentaFijaNotaCreditoProveedor
    Dim WithEvents ocxCuentaFijaNotaDebitoProveedor As New ocxCuentaFijaNotaDebitoProveedor

    'Movimientos
    Dim WithEvents ocxCuentaFijaMovimiento As New ocxCuentaFijaMovimiento
    Dim WithEvents ocxCuentaFijaAjuste As New ocxCuentaFijaAjuste
    Dim WithEvents ocxCuentaFijaDescargaStock As New ocxCuentaFijaDescargaStock
    Dim WithEvents ocxCuentaFijaConsumoCombustible As New ocxConsumoCombustible
    Dim WithEvents ocxCuentaFijaMateriaPrima As New ocxMateriaPrima
    Dim WithEvents ocxCuentaFijaDescargaCompra As New ocxDescargaCompra
    Dim WithEvents ocxCuentaFijaTicketBascula As New ocxCuentaFijaTicketBascula
    Dim WithEvents ocxCuentaFijaMacheoTicketFactura As New ocxCuentaFijaMacheoTicketFactura
    'SC: 05/10/2021 CF para Movimiento Unisal
    'Dim WithEvents ocxCuentaFijaMovimientoUnisal As New ocxCuentaFijaMovimientoUnisal

    'Tesoreria
    Dim WithEvents ocxCuentaFijaCobranza As New ocxCuentaFijaCobranza
    Dim WithEvents ocxCuentaFijaGasto As New ocxCuentaFijaGasto
    Dim WithEvents ocxCuentaFijaOrdenPago As New ocxCuentaFijaOrdenPago2
    Dim WithEvents ocxCuentaFijaEntregaChequeOP As New ocxCuentaFijaEntregaChequeOP
    Dim WithEvents ocxCuentaFijaChequeRechazado As New ocxCuentaFijaChequeRechazado
    Dim WithEvents ocxCuentaFijaDepositoBancario As New ocxCuentaFijaDepositoBancario
    Dim WithEvents ocxCuentaFijaDebitoCreditoBancario As New ocxCuentaFijaDebitoCreditoBancario
    Dim WithEvents ocxCuentaFijaDescuentoCheque As New ocxCuentaFijaDescuentoCheque
    Dim WithEvents ocxCuentaFijaVencimientoChequeDescontado As New ocxCuentaFijaVencimientoChequeDescontado
    Dim WithEvents ocxCuentaFijaVale As New ocxCuentaFijaVale
    Dim WithEvents ocxCuentaFijaVencimientoChequeDiferido As New ocxCuentaFijaVencimientoCheque
    Dim WithEvents ocxCuentaFijaCanjeCheque As New ocxCuentaFijaCanjeCheque
    'Dim WithEvents ocxCuentaFijaAplicacionAnticipoProveedores As New ocxCuentaFijaAplicacionAnticipoProveedores

    Sub Inicializar()

        'Cargar los controles
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaVenta)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaCobranza)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaGasto)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaOrdenPago)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaEntregaChequeOP)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaNotaCredito)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaNotaDebito)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaCompra)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaNotaCreditoProveedor)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaNotaDebitoProveedor)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaMovimiento)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaAjuste)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaDescargaStock)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaConsumoCombustible)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaMateriaPrima)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaDescargaCompra)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaChequeRechazado)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaDepositoBancario)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaDebitoCreditoBancario)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaDescuentoCheque)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaVale)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaVencimientoChequeDiferido)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaTicketBascula)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaMacheoTicketFactura)
        'SC: 05/10/2021 CF para Movimiento Unisal
        'SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaMovimientoUnisal)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaVencimientoChequeDescontado)
        SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaCanjeCheque)
        'SplitContainer1.Panel2.Controls.Add(ocxCuentaFijaAplicacionAnticipoProveedores)

        OcultarTodo()

    End Sub

    Sub OcultarTodo()

        'Ocultamos todos los controles del contenedor menos el del parametro
        For Each ctr As Control In SplitContainer1.Panel2.Controls
            ctr.Visible = False
        Next

    End Sub

    Sub CargarControl(ByVal ocx As Control)

        'Ocultamos todos los controles del contenedor menos el del parametro
        For Each ctr As Control In SplitContainer1.Panel2.Controls
            If ctr.Name = ocx.Name Then
                ctr.Visible = True
            Else
                ctr.Visible = False
            End If

            ctr.Dock = DockStyle.Fill

        Next


    End Sub

    Private Sub tvArbol_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvArbol.AfterSelect

        OcultarTodo()

        Dim Objeto As String = tvArbol.SelectedNode.Name

        Select Case Objeto

            'Ventas
            Case "Venta"
                CargarControl(Me.ocxCuentaFijaVenta)
                Me.ocxCuentaFijaVenta.Inicializar()
            Case "NotaCreditoVenta"
                CargarControl(Me.ocxCuentaFijaNotaCredito)
            Case "NotaDebitoVenta"
                CargarControl(Me.ocxCuentaFijaNotaDebito)

                'Compras
            Case "Compra"
                CargarControl(Me.ocxCuentaFijaCompra)
            Case "NotaCreditoCompra"
                CargarControl(Me.ocxCuentaFijaNotaCreditoProveedor)
            Case "NotaDebitoCompra"
                CargarControl(Me.ocxCuentaFijaNotaDebitoProveedor)

                'Stock
            Case "Movimiento"
                CargarControl(Me.ocxCuentaFijaMovimiento)
                'Ajuste
            Case "Ajuste"
                CargarControl(Me.ocxCuentaFijaAjuste)
                'Descarga Stock'
            Case "Descarga"
                CargarControl(Me.ocxCuentaFijaDescargaStock)
            Case "ConsumoCombustible"
                CargarControl(Me.ocxCuentaFijaConsumoCombustible)
            Case "MateriaPrima"
                CargarControl(Me.ocxCuentaFijaMateriaPrima)
            Case "DescargaCompra"
                CargarControl(Me.ocxCuentaFijaDescargaCompra)
                'Tesoreria
            Case "Cobranzas"
                CargarControl(ocxCuentaFijaCobranza)
                ocxCuentaFijaCobranza.Inicializar()
            Case "TicketBascula"
                CargarControl(ocxCuentaFijaTicketBascula)
                ocxCuentaFijaTicketBascula.Inicializar()
            Case "MacheoTicketFactura"
                CargarControl(Me.ocxCuentaFijaMacheoTicketFactura)
                'SC: 05/10/2021 CF para Movimiento Unisal
                'Case "MovimientoUnisal"
            '    CargarControl(Me.ocxCuentaFijaMovimientoUnisal)

            Case "OrdenPago"
                CargarControl(Me.ocxCuentaFijaOrdenPago)
            Case "EntregaChequeOP"
                CargarControl(Me.ocxCuentaFijaEntregaChequeOP)
            Case "VencimientoChequeOP"
                CargarControl(Me.ocxCuentaFijaVencimientoChequeDiferido)
            Case "Gasto"
                CargarControl(Me.ocxCuentaFijaGasto)
            Case "ChequeRechazado"
                CargarControl(Me.ocxCuentaFijaChequeRechazado)
            Case "DepositoBancario"
                CargarControl(Me.ocxCuentaFijaDepositoBancario)
            Case "DebitoCreditoBancario"
                CargarControl(Me.ocxCuentaFijaDebitoCreditoBancario)
            Case "DescuentoCheque"
                CargarControl(Me.ocxCuentaFijaDescuentoCheque)
            Case "Vale"
                CargarControl(Me.ocxCuentaFijaVale)
            Case "VencimientoChequeDescontado"
                CargarControl(Me.ocxCuentaFijaVencimientoChequeDescontado)
            Case "CanjeCheque"
                CargarControl(Me.ocxCuentaFijaCanjeCheque)
                'Case "AplicacionAnticipoProveedores"
                '   CargarControl(Me.ocxCuentaFijaAplicacionAnticipoProveedores)

        End Select

    End Sub

    Private Sub frmCuentaFijas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub


End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComprobanteLibroIVAModificar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxOperacion = New ERP.ocxCBX()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtVtoTimbrado = New ERP.ocxTXTDate()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTimbrado = New ERP.ocxTXTString()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtCotizacion = New ERP.ocxCotizacion()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTipoIVA = New System.Windows.Forms.Label()
        Me.lblretencion = New System.Windows.Forms.Label()
        Me.txtTotalRetencion = New ERP.ocxTXTNumeric()
        Me.cbxTipoIVA = New ERP.ocxCBX()
        Me.OcxImpuesto1 = New ERP.ocxImpuesto()
        Me.ChkFE = New System.Windows.Forms.CheckBox()
        Me.gbxCabecera.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.txtVtoTimbrado)
        Me.gbxCabecera.Controls.Add(Me.Label2)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.txtTimbrado)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.cbxCondicion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblProveedor)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Location = New System.Drawing.Point(2, -3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(788, 133)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(75, 100)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(703, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 21
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(496, 12)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(86, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 6
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxOperacion
        '
        Me.cbxOperacion.CampoWhere = Nothing
        Me.cbxOperacion.CargarUnaSolaVez = False
        Me.cbxOperacion.DataDisplayMember = Nothing
        Me.cbxOperacion.DataFilter = Nothing
        Me.cbxOperacion.DataOrderBy = Nothing
        Me.cbxOperacion.DataSource = Nothing
        Me.cbxOperacion.DataValueMember = Nothing
        Me.cbxOperacion.dtSeleccionado = Nothing
        Me.cbxOperacion.FormABM = Nothing
        Me.cbxOperacion.Indicaciones = Nothing
        Me.cbxOperacion.Location = New System.Drawing.Point(209, 12)
        Me.cbxOperacion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.SeleccionMultiple = False
        Me.cbxOperacion.SeleccionObligatoria = True
        Me.cbxOperacion.Size = New System.Drawing.Size(165, 21)
        Me.cbxOperacion.SoloLectura = False
        Me.cbxOperacion.TabIndex = 3
        Me.cbxOperacion.Texto = ""
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(75, 39)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(507, 24)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 11
        '
        'txtVtoTimbrado
        '
        Me.txtVtoTimbrado.AñoFecha = 0
        Me.txtVtoTimbrado.Color = System.Drawing.Color.Empty
        Me.txtVtoTimbrado.Fecha = New Date(2013, 5, 21, 14, 6, 47, 947)
        Me.txtVtoTimbrado.Location = New System.Drawing.Point(471, 72)
        Me.txtVtoTimbrado.MesFecha = 0
        Me.txtVtoTimbrado.Name = "txtVtoTimbrado"
        Me.txtVtoTimbrado.PermitirNulo = False
        Me.txtVtoTimbrado.Size = New System.Drawing.Size(74, 20)
        Me.txtVtoTimbrado.SoloLectura = False
        Me.txtVtoTimbrado.TabIndex = 17
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(395, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Vto. Timbrado:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(197, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Timbrado:"
        '
        'txtTimbrado
        '
        Me.txtTimbrado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTimbrado.Color = System.Drawing.Color.Empty
        Me.txtTimbrado.Indicaciones = Nothing
        Me.txtTimbrado.Location = New System.Drawing.Point(251, 72)
        Me.txtTimbrado.Multilinea = False
        Me.txtTimbrado.Name = "txtTimbrado"
        Me.txtTimbrado.Size = New System.Drawing.Size(138, 21)
        Me.txtTimbrado.SoloLectura = False
        Me.txtTimbrado.TabIndex = 15
        Me.txtTimbrado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTimbrado.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(545, 76)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 18
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(75, 72)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(122, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 13
        Me.cbxSucursal.Texto = ""
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(589, 12)
        Me.cbxCondicion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = True
        Me.cbxCondicion.Size = New System.Drawing.Size(73, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 7
        Me.cbxCondicion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 21, 14, 6, 47, 994)
        Me.txtFecha.Location = New System.Drawing.Point(702, 12)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 9
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(662, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 8
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(426, 12)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(63, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 5
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(36, 46)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(33, 13)
        Me.lblProveedor.TabIndex = 10
        Me.lblProveedor.Text = "RUC:"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(79, 12)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(146, 12)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(26, 104)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(43, 13)
        Me.lblObservacion.TabIndex = 20
        Me.lblObservacion.Text = "Detalle:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(18, 75)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 12
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(374, 16)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(52, 13)
        Me.lblComprobante.TabIndex = 4
        Me.lblComprobante.Text = "Comprob:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(10, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operación:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.dt = Nothing
        Me.txtCotizacion.FiltroFecha = Nothing
        Me.txtCotizacion.Location = New System.Drawing.Point(598, 67)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Registro = Nothing
        Me.txtCotizacion.Saltar = False
        Me.txtCotizacion.Seleccionado = True
        Me.txtCotizacion.Size = New System.Drawing.Size(183, 31)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 19
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 80
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(75, 39)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(507, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 11
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(334, 299)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 4
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(703, 299)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(415, 299)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 5
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 329)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(807, 22)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblTipoIVA
        '
        Me.lblTipoIVA.AutoSize = True
        Me.lblTipoIVA.Location = New System.Drawing.Point(22, 162)
        Me.lblTipoIVA.Name = "lblTipoIVA"
        Me.lblTipoIVA.Size = New System.Drawing.Size(55, 13)
        Me.lblTipoIVA.TabIndex = 1
        Me.lblTipoIVA.Text = "T. de Imp:"
        '
        'lblretencion
        '
        Me.lblretencion.AutoSize = True
        Me.lblretencion.Location = New System.Drawing.Point(12, 190)
        Me.lblretencion.Name = "lblretencion"
        Me.lblretencion.Size = New System.Drawing.Size(59, 13)
        Me.lblretencion.TabIndex = 8
        Me.lblretencion.Text = "Retencion:"
        '
        'txtTotalRetencion
        '
        Me.txtTotalRetencion.Color = System.Drawing.Color.Empty
        Me.txtTotalRetencion.Decimales = True
        Me.txtTotalRetencion.Indicaciones = Nothing
        Me.txtTotalRetencion.Location = New System.Drawing.Point(77, 186)
        Me.txtTotalRetencion.Name = "txtTotalRetencion"
        Me.txtTotalRetencion.Size = New System.Drawing.Size(122, 20)
        Me.txtTotalRetencion.SoloLectura = True
        Me.txtTotalRetencion.TabIndex = 9
        Me.txtTotalRetencion.TabStop = False
        Me.txtTotalRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetencion.Texto = "0"
        '
        'cbxTipoIVA
        '
        Me.cbxTipoIVA.CampoWhere = Nothing
        Me.cbxTipoIVA.CargarUnaSolaVez = False
        Me.cbxTipoIVA.DataDisplayMember = Nothing
        Me.cbxTipoIVA.DataFilter = Nothing
        Me.cbxTipoIVA.DataOrderBy = Nothing
        Me.cbxTipoIVA.DataSource = Nothing
        Me.cbxTipoIVA.DataValueMember = Nothing
        Me.cbxTipoIVA.dtSeleccionado = Nothing
        Me.cbxTipoIVA.FormABM = Nothing
        Me.cbxTipoIVA.Indicaciones = Nothing
        Me.cbxTipoIVA.Location = New System.Drawing.Point(77, 158)
        Me.cbxTipoIVA.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoIVA.Name = "cbxTipoIVA"
        Me.cbxTipoIVA.SeleccionMultiple = False
        Me.cbxTipoIVA.SeleccionObligatoria = True
        Me.cbxTipoIVA.Size = New System.Drawing.Size(122, 21)
        Me.cbxTipoIVA.SoloLectura = False
        Me.cbxTipoIVA.TabIndex = 2
        Me.cbxTipoIVA.Texto = ""
        '
        'OcxImpuesto1
        '
        Me.OcxImpuesto1.Decimales = False
        Me.OcxImpuesto1.dtImpuesto = Nothing
        Me.OcxImpuesto1.IDMoneda = 0
        Me.OcxImpuesto1.Location = New System.Drawing.Point(334, 137)
        Me.OcxImpuesto1.Margin = New System.Windows.Forms.Padding(4)
        Me.OcxImpuesto1.Name = "OcxImpuesto1"
        Me.OcxImpuesto1.Size = New System.Drawing.Size(456, 155)
        Me.OcxImpuesto1.TabIndex = 3
        Me.OcxImpuesto1.TotalRetencionIVA = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'ChkFE
        '
        Me.ChkFE.AutoSize = True
        Me.ChkFE.Location = New System.Drawing.Point(17, 136)
        Me.ChkFE.Name = "ChkFE"
        Me.ChkFE.Size = New System.Drawing.Size(182, 17)
        Me.ChkFE.TabIndex = 24
        Me.ChkFE.Text = "Es Fact Electrónica / Fact Virtual"
        Me.ChkFE.UseVisualStyleBackColor = True
        '
        'frmComprobanteLibroIVAModificar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(807, 351)
        Me.Controls.Add(Me.ChkFE)
        Me.Controls.Add(Me.txtTotalRetencion)
        Me.Controls.Add(Me.lblretencion)
        Me.Controls.Add(Me.cbxTipoIVA)
        Me.Controls.Add(Me.lblTipoIVA)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.OcxImpuesto1)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxCabecera)
        Me.KeyPreview = True
        Me.Name = "frmComprobanteLibroIVAModificar"
        Me.Text = "frmComprobanteLibroIVA"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents OcxImpuesto1 As ERP.ocxImpuesto
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cbxTipoIVA As ERP.ocxCBX
    Friend WithEvents lblTipoIVA As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents cbxOperacion As ERP.ocxCBX
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents txtVtoTimbrado As ERP.ocxTXTDate
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTimbrado As ERP.ocxTXTString
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxCotizacion
    Friend WithEvents txtTotalRetencion As ERP.ocxTXTNumeric
    Friend WithEvents lblretencion As System.Windows.Forms.Label
    Friend WithEvents ChkFE As CheckBox
End Class

﻿Public Class frmComprobanteLibroIVA
    ' CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Public CAsiento As New CAsiento
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTransaccionARendirValue As Integer
    Public Property IDTransaccionARendir() As Integer
        Get
            Return IDTransaccionARendirValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionARendirValue = value
        End Set
    End Property

    Private CajaChicaValue As Boolean
    Public Property CajaChica As Boolean
        Get
            Return CajaChicaValue
        End Get
        Set(ByVal value As Boolean)
            CajaChicaValue = value
        End Set
    End Property

    Private TipoValue As String
    Public Property Tipo As String
        Get
            Return TipoValue
        End Get
        Set(ByVal value As String)
            TipoValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim vControles() As Control
    Public vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProveedor.Conectar()
        txtCliente.Conectar()

        'Otros
        OcxImpuesto1.Inicializar()
        CDetalleImpuesto.Inicializar()
        txtCotizacion.Inicializar()

        'Propiedades
        'IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "COMPROBANTELIBROIVA", "CLI")

        CAsiento.Inicializar()
        'Funciones
        CargarInformacion()


        vNuevo = True


    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'TIPO IVA
        cbxTipoIVA.cbx.Items.Add("DIRECTO")
        cbxTipoIVA.cbx.Items.Add("INDISTINTO")
        cbxTipoIVA.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoIVA.cbx.SelectedIndex = 0

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxOperacion.cbx, "Select ID, Descripcion From Operacion Order By Descripcion")

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Condicion
        cbxCondicion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CONDICION", "CONTADO")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)


        'Moneda
        txtCotizacion.cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")

        'Impuestos
        OcxImpuesto1.EstablecerSoloLectura()

        'Ultimo Registro
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero+1) From ComprobanteLibroIVA Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

        txtID.txt.ReadOnly = True

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, New Button, New Button, New Button, New Button, btnAsiento, vControles)

    End Sub


    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Condicion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CONDICION", cbxCondicion.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Moneda
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", txtCotizacion.cbxMoneda.cbx.Text)


    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Validar
        'Tipo Comprobante
        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            CSistema.MostrarError("Ingrese un número de comprobante!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Total
        If CDec(OcxImpuesto1.txtTotal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe del documento no es válido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Timbrado
        'If txtTimbrado.txt.Text.Trim.Length = 0 Then
        '    Dim mensaje As String = "Ingrese un número de timbrado!"
        '    ctrError.SetError(btnGuardar, mensaje)
        '    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Function
        'End If



        If CData.GetTable("VTipoComprobante", " ID=" & cbxTipoComprobante.GetValue)(0)("ComprobanteTimbrado") = True Then
            If txtTimbrado.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Ingrese un número de timbrado!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If CSistema.ControlarTimbrado(txtTimbrado.txt.Text) = False Then
                Exit Function
            End If

            If CSistema.ControlarNroDocumento(txtComprobante.txt.Text) = False Then
                Exit Function
            End If

        End If

        'Validar el Asiento
        If CAsiento.ObtenerSaldo <> 0 Then
            CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Function
        End If

        'If CAsiento.ObtenerTotal = 0 Then
        '    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        'Si va a eliminar
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        'Dim IDTransaccion As Integer
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroTimbrado", txtTimbrado.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaVencimientoTimbrado", CSistema.FormatoFechaBaseDatos(txtVtoTimbrado.txt.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Detalle", txtObservacion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacionComprobante", cbxOperacion.cbx.SelectedValue, ParameterDirection.Input)
        'Se agrega cuando se carga para diferenciar Factura Electrónica
        If ChkFE.Checked = True Then
            CSistema.SetSQLParameter(param, "@EsFE", ChkFE.Checked, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        IndiceOperacion = param.GetLength(0) - 1

        'Moneda
        CSistema.SetSQLParameter(param, "@IDMoneda", txtCotizacion.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", txtCotizacion.GetValue, ParameterDirection.Input)

        'Credito
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            CSistema.SetSQLParameter(param, "@Credito", "False".ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Credito", "True".ToString, ParameterDirection.Input)
        End If

        If cbxTipoIVA.cbx.Text = "DIRECTO" Then
            CSistema.SetSQLParameter(param, "@Directo", "TRUE", ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@Directo", "FALSE", ParameterDirection.Input)
        End If

        'Totales
        'Totales
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colTotal")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colImpuesto")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.gridSumColumn(OcxImpuesto1.dg, "colDiscriminado")), ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpComprobanteLibroIVA", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From ComprobanteLibroIVA Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpCompra", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        'Si es nuevo
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

            'Cargamos el DetalleImpuesto
            OcxImpuesto1.Generar(IDTransaccion)
            CDetalleImpuesto.dt = OcxImpuesto1.dtImpuesto
            CDetalleImpuesto.Guardar(IDTransaccion)

            'Cargamos el asiento
            CAsiento.IDTransaccion = IDTransaccion
            CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

        End If

        Dim Procesar As Boolean = True

        Me.Close()
    End Sub

    Sub CalcularTotales()

        OcxImpuesto1.CargarValores(CDetalleImpuesto.dt)

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "ComprobanteLibroIVA: " & txtID.txt.Text & "  -  " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.Text & "  -  " & txtProveedor.txtRazonSocial.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From ComprobanteLibroIVA Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        Else

            'Validar
            If cbxCiudad.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la ciudad de operacion!"
                ctrError.SetError(cbxCiudad, mensaje)
                ctrError.SetIconAlignment(cbxCiudad, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtProveedor.Seleccionado = False And txtCliente.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el proveedor o cliente!"
                ctrError.SetError(txtProveedor, mensaje)
                ctrError.SetIconAlignment(txtProveedor, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "ComprobanteLibroIVA: " & txtID.txt.Text & "  -  " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.Text & "  -  " & txtProveedor.txtRazonSocial.Text

            'EstablecerCabecera
            Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

            oRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
            oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
            oRow("Fecha") = txtFecha.GetValue
            oRow("IDMoneda") = txtCotizacion.Registro("ID")
            oRow("Cotizacion") = txtCotizacion.GetValue
            oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
            oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
            oRow("NroComprobante") = txtComprobante.GetValue
            oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.GetValue
            oRow("Detalle") = txtObservacion.GetValue
            oRow("Total") = OcxImpuesto1.txtTotal.ObtenerValor


            oRow("GastosMultiples") = False
            CAsiento.dtAsiento.Rows.Clear()
            CAsiento.dtAsiento.Rows.Add(oRow)

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
        End If


    End Sub

    Sub CargarOperacion()

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        ''Obtenemos el IDTransaccion
        'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From ComprobanteLibroIVA Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )")


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VComprobanteLibroIVA Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString

        If oRow("IDProveedor").ToString = 0 Or oRow("IDProveedor") Is DBNull.Value Then
            txtProveedor.Visible = False
            txtCliente.SetValue(oRow("IDCliente").ToString)
        Else
            txtCliente.Visible = False
            txtProveedor.SetValue(oRow("IDProveedor").ToString)
        End If

        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        cbxOperacion.cbx.Text = oRow("Operacion").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        txtFecha.SetValue(oRow("Fecha"))
        txtTimbrado.txt.Text = oRow("NroTimbrado").ToString
        txtVtoTimbrado.SetValue(oRow("FechaVencimientoTimbrado"))


        'Cotizacion
        txtCotizacion.SetValue(oRow("Moneda").ToString, oRow("Cotizacion").ToString)
        txtObservacion.txt.Text = oRow("Detalle").ToString

        'Cargamos el detalle
        OcxImpuesto1.CargarValores(IDTransaccion)

    End Sub

    Private Sub frmComprobanteLibroIVA_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
      CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmComprobanteLibroIVA_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmComprobanteLibroIVA_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub txtProveedor_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtProveedor.KeyUp
        If e.KeyCode = Keys.Escape Then
            txtProveedor.LimpiarSeleccion()
            txtProveedor.OcultarLista()
        End If
    End Sub

    Private Sub txtProveedor_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.Leave
        txtProveedor.OcultarLista()
    End Sub

    Private Sub txtProveedor_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProveedor.ItemSeleccionado
        cbxSucursal.cbx.Focus()
    End Sub

    Private Sub txtCliente_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.Leave
        txtCliente.OcultarLista()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        cbxSucursal.cbx.Focus()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub


    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub


    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged



        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If



    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub cbxOperacion_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxOperacion.PropertyChanged

        cbxTipoComprobante.cbx.DataSource = Nothing

        If IsNumeric(cbxOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxOperacion.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID,Codigo  From VTipoComprobante Where IDOperacion=" & cbxOperacion.cbx.SelectedValue)
        Tipo = CSistema.ExecuteScalar("Select Tipo From VTipoComprobante Where IDOperacion=" & cbxOperacion.cbx.SelectedValue)

        txtProveedor.Visible = False
        txtCliente.Visible = False

        If Tipo = "CLIENTE" Then
            txtProveedor.Clear()
            txtProveedor.Visible = False
            txtCliente.Visible = True
        End If

        If Tipo = "PROVEEDOR" Then
            txtCliente.Clear()
            txtProveedor.Visible = True
            txtCliente.Visible = False
        End If

    End Sub

    Private Sub OcxImpuesto1_ImporteCambiado() Handles OcxImpuesto1.ImporteCambiado
        txtTotalRetencion.SetValue(OcxImpuesto1.TotalRetencionIVA)
    End Sub

    'FA 28/05/2021
    Sub frmComprobanteLibroIVA_Activate()
        Me.Refresh()
    End Sub

    Private Sub txtCotizacion_CambioMoneda() Handles txtCotizacion.CambioMoneda
        txtCotizacion.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
        txtCotizacion.Recargar()
        OcxImpuesto1.SetIDMoneda(txtCotizacion.Registro("ID"))
    End Sub
End Class
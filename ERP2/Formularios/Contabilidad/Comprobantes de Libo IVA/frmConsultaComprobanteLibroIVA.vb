﻿Public Class frmConsultaComprobanteLibroIVA

    'CLASES
    Dim CSistema As New CSistema
    Dim vControles() As Control

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private CiudadValue As String
    Public Property Ciudad() As String
        Get
            Return CiudadValue
        End Get
        Set(ByVal value As String)
            CiudadValue = value
        End Set
    End Property

   

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'Variable
    Dim dtComprobanteLibroIVA As New DataTable

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        IDOperacion = CSistema.ObtenerIDOperacion(frmComprobanteLibroIVA.Name, "COMPROBANTELIBROIVA", "CLI")

        'Listar los comprobantes del mes
        txtDesde.txt.Text = "01" & CSistema.FormatDobleDigito(Date.Now.Month) & Date.Now.Year
        txtHasta.SetValue(Date.Now)
        Listar()

        'Foco
        dgw.Focus()

    End Sub

    Sub Listar()

        Dim where As String = " Where 1 = 1 "
        If chkFechaDocumento.Checked Then
            If IsDate(txtDesde.txt.Text) And IsDate(txtHasta.txt.Text) Then
                where = where & " And Fecha between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
            Else
                MessageBox.Show("La fecha no es correcta!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txtDesde.Focus()
                Exit Sub
            End If
        End If

        If chkFechaCarga.Checked Then
            If IsDate(txtDesdeCarga.txt.Text) And IsDate(txtHastaCarga.txt.Text) Then
                where = where & " And FechaTransaccion between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
            Else
                MessageBox.Show("La fecha no es correcta!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                txtDesdeCarga.Focus()
                Exit Sub
            End If
        End If
        If chkUsuario.Checked Then
            where = where & "And IDUsuarioTransaccion = " & cbxUsuario.cbx.SelectedValue
        End If


        Dim dtResumenComprobaneLibroIVA = CSistema.ExecuteToDataTable("Select 'Tipo Comprobante'= TipoComprobante, Moneda, 'Total'=Sum(Total)  From VComprobanteLibroIVA " & where & " and IDSucursal=" & cbxSucursal.cbx.SelectedValue & "Group by TipoComprobante, Moneda").Copy
        'Cargar
        dtComprobanteLibroIVA = CSistema.ExecuteToDataTable("Select IDTransaccion, 'Suc'=CodigoSucursal, Numero, 'T.Comp'= TipoComprobante, NroComprobante, Fec, Condicion, RUC, RazonSocial, Total, Moneda  From VComprobanteLibroIVA " & where & " and IDSucursal=" & cbxSucursal.cbx.SelectedValue).Copy

        'Limpiar la grilla
        ' dgw.Rows.Clear()

        CSistema.dtToGrid(dgw, dtComprobanteLibroIVA)
        CSistema.dtToGrid(dgwResumen, dtResumenComprobaneLibroIVA)


        CSistema.DataGridColumnasNumericas(dgwResumen, {"Total"}, True)


        'For Each oRow As DataRow In dtComprobanteLibroIVA.Rows

        '    Dim oRow1(14) As String
        '    oRow1(0) = oRow("IDTransaccion").ToString
        '    oRow1(1) = oRow("CodigoSucursal").ToString
        '    oRow1(2) = oRow("Numero").ToString
        '    oRow1(3) = oRow("TipoComprobante").ToString
        '    oRow1(4) = oRow("NroComprobante").ToString()
        '    oRow1(5) = oRow("Fec").ToString
        '    oRow1(6) = oRow("Condicion").ToString
        '    oRow1(7) = oRow("RUC").ToString
        '    oRow1(8) = oRow("RazonSocial").ToString
        '    oRow1(13) = CSistema.FormatoMoneda(oRow("Total").ToString)
        '    oRow1(14) = oRow("Moneda").ToString

        '    dgw.Rows.Add(oRow1)

        'Next

        'Formato
        dgw.Columns("IDTransaccion").Visible = False
        dgw.Columns("RazonSocial").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"
        dgw.Columns("Suc").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Numero").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("T.Comp").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Fec").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Condicion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        ctrError.Clear()
        tsslEstado.Text = ""

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub Procesar()

        tsslEstado.Text = ""
        ctrError.Clear()

        Seleccionar()

        For Each oRow As DataRow In dtComprobanteLibroIVA.Rows

            Dim IDTransaccion As Integer = oRow("IDTransaccion").ToString

            Dim param(-1) As SqlClient.SqlParameter
            Dim IndiceOperacion As Integer = 0

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesABM.UPD.ToString, ParameterDirection.Input)
            IndiceOperacion = param.GetLength(0) - 1

            'Transaccion
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)


            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpActualizarEfectivo", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnModificar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnModificar, ErrorIconAlignment.TopRight)

                Exit Sub

            End If

        Next

    End Sub

    Sub Seleccionar()

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells("colIDTransaccion").Value

            For Each oRow1 As DataRow In dtComprobanteLibroIVA.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells("colSeleccionar").Value
            Next

        Next

    End Sub

    Sub Cargar()

        For Each oRow As DataRow In dtComprobanteLibroIVA.Rows

            Dim oRow1(5) As String
            oRow1(0) = oRow("IDTransaccion").ToString
            oRow1(1) = oRow("NroComprobante").ToString()
            oRow1(2) = oRow("Fecha").ToString
            oRow1(3) = oRow("RazonSocial").ToString
            oRow1(4) = CSistema.FormatoMoneda(oRow("Total").ToString)

            dgw.Rows.Add(oRow1)

        Next

        PintarCelda()

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

        If dgw.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, New Button, New Button, btnEliminar, New Button, New Button, btnModificar, vControles)

    End Sub

    Sub Eliminar()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnEliminar, mensaje)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.DEL.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Eliminar
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpComprobanteLibroIVA", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        Listar()

    End Sub

    Private Sub dgw_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellClick
        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        IDTransaccion = Convert.ToString(dgw.CurrentRow.Cells(0).Value)
        Ciudad = dgw.CurrentRow.Cells(1).Value

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells("colImporte")
        End If

    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()

    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmConsultaComprobanteLibroIVA_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmHabilitarParaPagoEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim frm As New frmComprobanteLibroIVAModificar
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.IDTransaccion = IDTransaccion
        frm.ShowDialog(Me)

        Listar()

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If


    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click

        'listar
        Listar()

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click

        FGMostrarFormulario(Me, New frmComprobanteLibroIVA, "Comprobantes para Libro IVA", Windows.Forms.FormBorderStyle.FixedDialog, FormStartPosition.CenterScreen, True, False)
        Listar()

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Dim frm As New frmComprobanteLibroIVAModificar
        frm.IDTransaccion = IDTransaccion
        FGMostrarFormulario(Me, frm, "Comprobantes para Libro IVA", Windows.Forms.FormBorderStyle.FixedDialog, FormStartPosition.CenterScreen, True, False)
        Listar()
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles chkFechaDocumento.CheckedChanged
        txtDesde.Enabled = chkFechaDocumento.Checked
        txtHasta.Enabled = chkFechaDocumento.Checked
    End Sub

    Private Sub chkFechaCarga_CheckedChanged(sender As Object, e As EventArgs) Handles chkFechaCarga.CheckedChanged
        txtDesdeCarga.Enabled = chkFechaCarga.Checked
        txtHastaCarga.Enabled = chkFechaCarga.Checked

        If chkFechaCarga.Checked = False Then
            chkFechaDocumento.Checked = True
            chkFechaDocumento.Enabled = False
        Else
            chkFechaDocumento.Enabled = True
        End If

    End Sub

    Private Sub chkUsuario_CheckedChanged(sender As Object, e As EventArgs) Handles chkUsuario.CheckedChanged
        cbxUsuario.Enabled = chkUsuario.Checked
    End Sub

    'FA 28/05/2021
    Sub frmConsultaComprobanteLibroIVA_Activate()
        Me.Refresh()
    End Sub

End Class

﻿
Public Class frmExportarPlanCuentaSaldo

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        CargarInformacion()

        txtCuentaContable.Conectar()


    End Sub

    Sub CargarInformacion()

        'Mes
        cbxMes.SelectedIndex = Date.Now.Month - 1

        'Año
        nudAño.Minimum = Date.Now.Year - 10
        nudAño.Maximum = Date.Now.Year + 10
        nudAño.Value = Date.Now.Year


    End Sub

    Sub Listar()
        Dim SQL As String = "Select * from vplancuentasaldo "
        Dim Where As String = "Where Año = " & nudAño.Value & " And M = '" & cbxMes.Text & "' "
        Dim dt As New DataTable
        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next
        If Not chkIncluirSinSaldo.chk.Checked Then
            Where = Where & " And Saldo <> 0 "
        End If

        If txtCuentaContable.Enabled Then
            Where = Where & " and codigo = '" & txtCuentaContable.txtCodigo.Texto & "' "
        End If

        dt = CSistema.ExecuteToDataTable(SQL & Where)
        CSistema.dtToExcel2(dt, "PlanCuentaSaldo")
        Me.Close()


    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub OcxCHK1_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles OcxCHK1.PropertyChanged
        cbxUnidadNegocio.Enabled = value
    End Sub

    Private Sub chkCentroCosto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCentroCosto.PropertyChanged
        cbxCentroCosto.Enabled = value
    End Sub

    Private Sub chkCuentaContable_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCuentaContable.PropertyChanged
        txtCuentaContable.Enabled = value
    End Sub

    Private Sub frmExportarPlanCuentaSaldo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    'FA 28/05/2021
    Sub frmExportarPlanCuentaSaldo_Activate()
        Me.Refresh()
    End Sub

End Class
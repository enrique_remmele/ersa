﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExportarPlanCuentaSaldo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.nudAño = New System.Windows.Forms.NumericUpDown()
        Me.lblMes = New System.Windows.Forms.Label()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.chkIncluirSinSaldo = New ERP.ocxCHK()
        Me.txtCuentaContable = New ERP.ocxTXTCuentaContable()
        Me.chkCuentaContable = New ERP.ocxCHK()
        Me.cbxCentroCosto = New ERP.ocxCBX()
        Me.chkCentroCosto = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.OcxCHK1 = New ERP.ocxCHK()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'nudAño
        '
        Me.nudAño.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAño.Location = New System.Drawing.Point(9, 34)
        Me.nudAño.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.nudAño.Name = "nudAño"
        Me.nudAño.Size = New System.Drawing.Size(58, 20)
        Me.nudAño.TabIndex = 4
        Me.nudAño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudAño.ThousandsSeparator = True
        Me.nudAño.Value = New Decimal(New Integer() {2013, 0, 0, 0})
        '
        'lblMes
        '
        Me.lblMes.AutoSize = True
        Me.lblMes.Location = New System.Drawing.Point(6, 15)
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Size = New System.Drawing.Size(54, 13)
        Me.lblMes.TabIndex = 3
        Me.lblMes.Text = "Año/Mes:"
        '
        'cbxMes
        '
        Me.cbxMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"})
        Me.cbxMes.Location = New System.Drawing.Point(73, 34)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(125, 21)
        Me.cbxMes.TabIndex = 5
        '
        'chkIncluirSinSaldo
        '
        Me.chkIncluirSinSaldo.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirSinSaldo.Color = System.Drawing.Color.Empty
        Me.chkIncluirSinSaldo.Location = New System.Drawing.Point(9, 66)
        Me.chkIncluirSinSaldo.Name = "chkIncluirSinSaldo"
        Me.chkIncluirSinSaldo.Size = New System.Drawing.Size(152, 21)
        Me.chkIncluirSinSaldo.SoloLectura = False
        Me.chkIncluirSinSaldo.TabIndex = 12
        Me.chkIncluirSinSaldo.Texto = "Incluir cuentas sin saldos"
        Me.chkIncluirSinSaldo.Valor = False
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.AlturaMaxima = 0
        Me.txtCuentaContable.Consulta = Nothing
        Me.txtCuentaContable.Enabled = False
        Me.txtCuentaContable.IDCentroCosto = 0
        Me.txtCuentaContable.IDUnidadNegocio = 0
        Me.txtCuentaContable.ListarTodas = False
        Me.txtCuentaContable.Location = New System.Drawing.Point(129, 76)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Registro = Nothing
        Me.txtCuentaContable.Resolucion173 = False
        Me.txtCuentaContable.Seleccionado = False
        Me.txtCuentaContable.Size = New System.Drawing.Size(304, 59)
        Me.txtCuentaContable.SoloLectura = False
        Me.txtCuentaContable.TabIndex = 14
        Me.txtCuentaContable.Texto = Nothing
        Me.txtCuentaContable.whereFiltro = Nothing
        '
        'chkCuentaContable
        '
        Me.chkCuentaContable.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaContable.Color = System.Drawing.Color.Empty
        Me.chkCuentaContable.Location = New System.Drawing.Point(5, 76)
        Me.chkCuentaContable.Name = "chkCuentaContable"
        Me.chkCuentaContable.Size = New System.Drawing.Size(115, 19)
        Me.chkCuentaContable.SoloLectura = False
        Me.chkCuentaContable.TabIndex = 13
        Me.chkCuentaContable.Texto = "Cuenta Contable:"
        Me.chkCuentaContable.Valor = False
        '
        'cbxCentroCosto
        '
        Me.cbxCentroCosto.CampoWhere = "IDCentroCosto"
        Me.cbxCentroCosto.CargarUnaSolaVez = False
        Me.cbxCentroCosto.DataDisplayMember = "Descripcion"
        Me.cbxCentroCosto.DataFilter = Nothing
        Me.cbxCentroCosto.DataOrderBy = Nothing
        Me.cbxCentroCosto.DataSource = "VCentroCosto"
        Me.cbxCentroCosto.DataValueMember = "ID"
        Me.cbxCentroCosto.dtSeleccionado = Nothing
        Me.cbxCentroCosto.Enabled = False
        Me.cbxCentroCosto.FormABM = Nothing
        Me.cbxCentroCosto.Indicaciones = Nothing
        Me.cbxCentroCosto.Location = New System.Drawing.Point(129, 49)
        Me.cbxCentroCosto.Name = "cbxCentroCosto"
        Me.cbxCentroCosto.SeleccionMultiple = False
        Me.cbxCentroCosto.SeleccionObligatoria = False
        Me.cbxCentroCosto.Size = New System.Drawing.Size(301, 21)
        Me.cbxCentroCosto.SoloLectura = False
        Me.cbxCentroCosto.TabIndex = 16
        Me.cbxCentroCosto.Texto = ""
        '
        'chkCentroCosto
        '
        Me.chkCentroCosto.BackColor = System.Drawing.Color.Transparent
        Me.chkCentroCosto.Color = System.Drawing.Color.Empty
        Me.chkCentroCosto.Location = New System.Drawing.Point(5, 46)
        Me.chkCentroCosto.Name = "chkCentroCosto"
        Me.chkCentroCosto.Size = New System.Drawing.Size(118, 19)
        Me.chkCentroCosto.SoloLectura = False
        Me.chkCentroCosto.TabIndex = 15
        Me.chkCentroCosto.Texto = "Centro de Costo:"
        Me.chkCentroCosto.Valor = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = "IDUnidadNegocio"
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = "Descripcion"
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = "VUnidadNegocio"
        Me.cbxUnidadNegocio.DataValueMember = "ID"
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.Enabled = False
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(129, 19)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(301, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 18
        Me.cbxUnidadNegocio.Texto = ""
        '
        'OcxCHK1
        '
        Me.OcxCHK1.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK1.Color = System.Drawing.Color.Empty
        Me.OcxCHK1.Location = New System.Drawing.Point(5, 19)
        Me.OcxCHK1.Name = "OcxCHK1"
        Me.OcxCHK1.Size = New System.Drawing.Size(118, 19)
        Me.OcxCHK1.SoloLectura = False
        Me.OcxCHK1.TabIndex = 17
        Me.OcxCHK1.Texto = "Unidad de Negocio:"
        Me.OcxCHK1.Valor = False
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(460, 179)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(215, 23)
        Me.btnListar.TabIndex = 19
        Me.btnListar.Text = "Exportar a Excel"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.chkCuentaContable)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaContable)
        Me.gbxFiltro.Controls.Add(Me.OcxCHK1)
        Me.gbxFiltro.Controls.Add(Me.chkCentroCosto)
        Me.gbxFiltro.Controls.Add(Me.cbxCentroCosto)
        Me.gbxFiltro.Location = New System.Drawing.Point(9, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(445, 146)
        Me.gbxFiltro.TabIndex = 20
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblMes)
        Me.GroupBox2.Controls.Add(Me.cbxMes)
        Me.GroupBox2.Controls.Add(Me.nudAño)
        Me.GroupBox2.Controls.Add(Me.chkIncluirSinSaldo)
        Me.GroupBox2.Location = New System.Drawing.Point(460, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(215, 146)
        Me.GroupBox2.TabIndex = 21
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'frmExportarPlanCuentaSaldo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(696, 222)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnListar)
        Me.Name = "frmExportarPlanCuentaSaldo"
        Me.Text = "frmExportarPlanCuentaSaldo"
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents nudAño As NumericUpDown
    Friend WithEvents lblMes As Label
    Friend WithEvents cbxMes As ComboBox
    Friend WithEvents chkIncluirSinSaldo As ocxCHK
    Friend WithEvents txtCuentaContable As ocxTXTCuentaContable
    Friend WithEvents chkCuentaContable As ocxCHK
    Friend WithEvents cbxCentroCosto As ocxCBX
    Friend WithEvents chkCentroCosto As ocxCHK
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents OcxCHK1 As ocxCHK
    Friend WithEvents btnListar As Button
    Friend WithEvents gbxFiltro As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
End Class

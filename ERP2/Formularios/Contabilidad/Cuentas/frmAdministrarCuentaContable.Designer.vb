﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdministrarCuentaContable
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtPlanCuenta = New System.Windows.Forms.TextBox()
        Me.lblPlanCuenta = New System.Windows.Forms.Label()
        Me.lblPadre = New System.Windows.Forms.Label()
        Me.lblCategoria = New System.Windows.Forms.Label()
        Me.lblAlias = New System.Windows.Forms.Label()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.lblID = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblResolucion173 = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.txtTipo = New System.Windows.Forms.TextBox()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxPadre = New ERP.ocxCBX()
        Me.OcxTXTString5 = New ERP.ocxTXTString()
        Me.OcxTXTString4 = New ERP.ocxTXTString()
        Me.txtCategoria = New ERP.ocxTXTNumeric()
        Me.txtAlias = New ERP.ocxTXTString()
        Me.txtCodigo = New ERP.ocxTXTString()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxCentroCosto = New ERP.ocxCBX()
        Me.chkUnidadNegocio = New ERP.ocxCHK()
        Me.chkCentroCosto = New ERP.ocxCHK()
        Me.lblCuentaContableRG49 = New System.Windows.Forms.Label()
        Me.OcxTXTRG49 = New ERP.ocxTXTString()
        Me.OcxTXTDescripcion = New ERP.ocxTXTString()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtPlanCuenta
        '
        Me.txtPlanCuenta.BackColor = System.Drawing.Color.White
        Me.txtPlanCuenta.Location = New System.Drawing.Point(227, 6)
        Me.txtPlanCuenta.Name = "txtPlanCuenta"
        Me.txtPlanCuenta.ReadOnly = True
        Me.txtPlanCuenta.Size = New System.Drawing.Size(142, 20)
        Me.txtPlanCuenta.TabIndex = 3
        Me.txtPlanCuenta.TabStop = False
        '
        'lblPlanCuenta
        '
        Me.lblPlanCuenta.AutoSize = True
        Me.lblPlanCuenta.Location = New System.Drawing.Point(138, 9)
        Me.lblPlanCuenta.Name = "lblPlanCuenta"
        Me.lblPlanCuenta.Size = New System.Drawing.Size(83, 13)
        Me.lblPlanCuenta.TabIndex = 2
        Me.lblPlanCuenta.Text = "Plan de Cuenta:"
        '
        'lblPadre
        '
        Me.lblPadre.AutoSize = True
        Me.lblPadre.Location = New System.Drawing.Point(8, 35)
        Me.lblPadre.Name = "lblPadre"
        Me.lblPadre.Size = New System.Drawing.Size(38, 13)
        Me.lblPadre.TabIndex = 4
        Me.lblPadre.Text = "Padre:"
        '
        'lblCategoria
        '
        Me.lblCategoria.AutoSize = True
        Me.lblCategoria.Location = New System.Drawing.Point(8, 118)
        Me.lblCategoria.Name = "lblCategoria"
        Me.lblCategoria.Size = New System.Drawing.Size(55, 13)
        Me.lblCategoria.TabIndex = 12
        Me.lblCategoria.Text = "Categoria:"
        '
        'lblAlias
        '
        Me.lblAlias.AutoSize = True
        Me.lblAlias.Location = New System.Drawing.Point(218, 63)
        Me.lblAlias.Name = "lblAlias"
        Me.lblAlias.Size = New System.Drawing.Size(32, 13)
        Me.lblAlias.TabIndex = 8
        Me.lblAlias.Text = "Alias:"
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(8, 62)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCodigo.TabIndex = 6
        Me.lblCodigo.Text = "Codigo:"
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(136, 212)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 18
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Checked = True
        Me.rdbActivo.Location = New System.Drawing.Point(75, 212)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 17
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(8, 214)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 16
        Me.lblEstado.Text = "Estado:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(8, 89)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 10
        Me.lblDescripcion.Text = "Descripción:"
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        Me.txtID.Location = New System.Drawing.Point(75, 6)
        Me.txtID.Name = "txtID"
        Me.txtID.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(61, 20)
        Me.txtID.TabIndex = 1
        Me.txtID.TabStop = False
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(8, 10)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(209, 381)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 26
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(290, 381)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 27
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(128, 381)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 25
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lblResolucion173
        '
        Me.lblResolucion173.AutoSize = True
        Me.lblResolucion173.Location = New System.Drawing.Point(8, 331)
        Me.lblResolucion173.Name = "lblResolucion173"
        Me.lblResolucion173.Size = New System.Drawing.Size(98, 13)
        Me.lblResolucion173.TabIndex = 22
        Me.lblResolucion173.Text = "Resolucion 173/04"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 415)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(378, 22)
        Me.StatusStrip1.TabIndex = 28
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(124, 118)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 14
        Me.lblTipo.Text = "Tipo:"
        '
        'txtTipo
        '
        Me.txtTipo.BackColor = System.Drawing.Color.White
        Me.txtTipo.Location = New System.Drawing.Point(161, 114)
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.ReadOnly = True
        Me.txtTipo.Size = New System.Drawing.Size(208, 20)
        Me.txtTipo.TabIndex = 15
        Me.txtTipo.TabStop = False
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(8, 304)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 19
        Me.lblSucursal.Text = "Sucursal:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(126, 301)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(243, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 21
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(108, 305)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(12, 15)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 20
        Me.chkSucursal.Texto = ""
        Me.chkSucursal.Valor = False
        '
        'cbxPadre
        '
        Me.cbxPadre.CampoWhere = Nothing
        Me.cbxPadre.CargarUnaSolaVez = False
        Me.cbxPadre.DataDisplayMember = Nothing
        Me.cbxPadre.DataFilter = Nothing
        Me.cbxPadre.DataOrderBy = Nothing
        Me.cbxPadre.DataSource = Nothing
        Me.cbxPadre.DataValueMember = Nothing
        Me.cbxPadre.dtSeleccionado = Nothing
        Me.cbxPadre.FormABM = Nothing
        Me.cbxPadre.Indicaciones = Nothing
        Me.cbxPadre.Location = New System.Drawing.Point(75, 32)
        Me.cbxPadre.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxPadre.Name = "cbxPadre"
        Me.cbxPadre.SeleccionMultiple = False
        Me.cbxPadre.SeleccionObligatoria = False
        Me.cbxPadre.Size = New System.Drawing.Size(294, 21)
        Me.cbxPadre.SoloLectura = False
        Me.cbxPadre.TabIndex = 26
        Me.cbxPadre.Texto = ""
        '
        'OcxTXTString5
        '
        Me.OcxTXTString5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString5.Color = System.Drawing.Color.Empty
        Me.OcxTXTString5.Indicaciones = Nothing
        Me.OcxTXTString5.Location = New System.Drawing.Point(11, 347)
        Me.OcxTXTString5.Margin = New System.Windows.Forms.Padding(4)
        Me.OcxTXTString5.Multilinea = False
        Me.OcxTXTString5.Name = "OcxTXTString5"
        Me.OcxTXTString5.Size = New System.Drawing.Size(75, 21)
        Me.OcxTXTString5.SoloLectura = False
        Me.OcxTXTString5.TabIndex = 23
        Me.OcxTXTString5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.OcxTXTString5.Texto = ""
        '
        'OcxTXTString4
        '
        Me.OcxTXTString4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTString4.Color = System.Drawing.Color.Empty
        Me.OcxTXTString4.Indicaciones = Nothing
        Me.OcxTXTString4.Location = New System.Drawing.Point(86, 347)
        Me.OcxTXTString4.Margin = New System.Windows.Forms.Padding(4)
        Me.OcxTXTString4.Multilinea = False
        Me.OcxTXTString4.Name = "OcxTXTString4"
        Me.OcxTXTString4.Size = New System.Drawing.Size(283, 21)
        Me.OcxTXTString4.SoloLectura = False
        Me.OcxTXTString4.TabIndex = 24
        Me.OcxTXTString4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.OcxTXTString4.Texto = ""
        '
        'txtCategoria
        '
        Me.txtCategoria.Color = System.Drawing.Color.Empty
        Me.txtCategoria.Decimales = True
        Me.txtCategoria.Indicaciones = Nothing
        Me.txtCategoria.Location = New System.Drawing.Point(75, 113)
        Me.txtCategoria.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCategoria.Name = "txtCategoria"
        Me.txtCategoria.Size = New System.Drawing.Size(43, 22)
        Me.txtCategoria.SoloLectura = False
        Me.txtCategoria.TabIndex = 13
        Me.txtCategoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCategoria.Texto = "0"
        '
        'txtAlias
        '
        Me.txtAlias.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtAlias.Color = System.Drawing.Color.Empty
        Me.txtAlias.Indicaciones = Nothing
        Me.txtAlias.Location = New System.Drawing.Point(250, 59)
        Me.txtAlias.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAlias.Multilinea = False
        Me.txtAlias.Name = "txtAlias"
        Me.txtAlias.Size = New System.Drawing.Size(119, 21)
        Me.txtAlias.SoloLectura = False
        Me.txtAlias.TabIndex = 9
        Me.txtAlias.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAlias.Texto = ""
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCodigo.Color = System.Drawing.Color.Empty
        Me.txtCodigo.Indicaciones = Nothing
        Me.txtCodigo.Location = New System.Drawing.Point(75, 59)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigo.Multilinea = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(137, 21)
        Me.txtCodigo.SoloLectura = False
        Me.txtCodigo.TabIndex = 7
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigo.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(75, 86)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(294, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 11
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 149)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 13)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Unidad de Negocio:"
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = ""
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = ""
        Me.cbxUnidadNegocio.DataFilter = ""
        Me.cbxUnidadNegocio.DataOrderBy = ""
        Me.cbxUnidadNegocio.DataSource = ""
        Me.cbxUnidadNegocio.DataValueMember = ""
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(126, 147)
        Me.cbxUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(242, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 29
        Me.cbxUnidadNegocio.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 181)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Centro de Costo:"
        '
        'cbxCentroCosto
        '
        Me.cbxCentroCosto.CampoWhere = ""
        Me.cbxCentroCosto.CargarUnaSolaVez = False
        Me.cbxCentroCosto.DataDisplayMember = ""
        Me.cbxCentroCosto.DataFilter = ""
        Me.cbxCentroCosto.DataOrderBy = ""
        Me.cbxCentroCosto.DataSource = ""
        Me.cbxCentroCosto.DataValueMember = ""
        Me.cbxCentroCosto.dtSeleccionado = Nothing
        Me.cbxCentroCosto.FormABM = Nothing
        Me.cbxCentroCosto.Indicaciones = Nothing
        Me.cbxCentroCosto.Location = New System.Drawing.Point(126, 180)
        Me.cbxCentroCosto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCentroCosto.Name = "cbxCentroCosto"
        Me.cbxCentroCosto.SeleccionMultiple = False
        Me.cbxCentroCosto.SeleccionObligatoria = False
        Me.cbxCentroCosto.Size = New System.Drawing.Size(242, 21)
        Me.cbxCentroCosto.SoloLectura = False
        Me.cbxCentroCosto.TabIndex = 31
        Me.cbxCentroCosto.Texto = ""
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(108, 150)
        Me.chkUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(12, 15)
        Me.chkUnidadNegocio.SoloLectura = False
        Me.chkUnidadNegocio.TabIndex = 33
        Me.chkUnidadNegocio.Texto = ""
        Me.chkUnidadNegocio.Valor = True
        '
        'chkCentroCosto
        '
        Me.chkCentroCosto.BackColor = System.Drawing.Color.Transparent
        Me.chkCentroCosto.Color = System.Drawing.Color.Empty
        Me.chkCentroCosto.Location = New System.Drawing.Point(108, 183)
        Me.chkCentroCosto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCentroCosto.Name = "chkCentroCosto"
        Me.chkCentroCosto.Size = New System.Drawing.Size(12, 15)
        Me.chkCentroCosto.SoloLectura = False
        Me.chkCentroCosto.TabIndex = 34
        Me.chkCentroCosto.Texto = ""
        Me.chkCentroCosto.Valor = True
        '
        'lblCuentaContableRG49
        '
        Me.lblCuentaContableRG49.AutoSize = True
        Me.lblCuentaContableRG49.Enabled = False
        Me.lblCuentaContableRG49.Location = New System.Drawing.Point(8, 241)
        Me.lblCuentaContableRG49.Name = "lblCuentaContableRG49"
        Me.lblCuentaContableRG49.Size = New System.Drawing.Size(75, 13)
        Me.lblCuentaContableRG49.TabIndex = 53
        Me.lblCuentaContableRG49.Text = "Cuenta RG49:"
        '
        'OcxTXTRG49
        '
        Me.OcxTXTRG49.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTRG49.Color = System.Drawing.Color.Empty
        Me.OcxTXTRG49.Enabled = False
        Me.OcxTXTRG49.Indicaciones = Nothing
        Me.OcxTXTRG49.Location = New System.Drawing.Point(11, 268)
        Me.OcxTXTRG49.Margin = New System.Windows.Forms.Padding(4)
        Me.OcxTXTRG49.Multilinea = False
        Me.OcxTXTRG49.Name = "OcxTXTRG49"
        Me.OcxTXTRG49.Size = New System.Drawing.Size(75, 21)
        Me.OcxTXTRG49.SoloLectura = False
        Me.OcxTXTRG49.TabIndex = 54
        Me.OcxTXTRG49.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.OcxTXTRG49.Texto = ""
        '
        'OcxTXTDescripcion
        '
        Me.OcxTXTDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.OcxTXTDescripcion.Color = System.Drawing.Color.Empty
        Me.OcxTXTDescripcion.Enabled = False
        Me.OcxTXTDescripcion.Indicaciones = Nothing
        Me.OcxTXTDescripcion.Location = New System.Drawing.Point(86, 268)
        Me.OcxTXTDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.OcxTXTDescripcion.Multilinea = False
        Me.OcxTXTDescripcion.Name = "OcxTXTDescripcion"
        Me.OcxTXTDescripcion.Size = New System.Drawing.Size(283, 21)
        Me.OcxTXTDescripcion.SoloLectura = False
        Me.OcxTXTDescripcion.TabIndex = 55
        Me.OcxTXTDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.OcxTXTDescripcion.Texto = ""
        '
        'frmAdministrarCuentaContable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(378, 437)
        Me.Controls.Add(Me.OcxTXTRG49)
        Me.Controls.Add(Me.OcxTXTDescripcion)
        Me.Controls.Add(Me.lblCuentaContableRG49)
        Me.Controls.Add(Me.chkCentroCosto)
        Me.Controls.Add(Me.chkUnidadNegocio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxCentroCosto)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxUnidadNegocio)
        Me.Controls.Add(Me.cbxSucursal)
        Me.Controls.Add(Me.chkSucursal)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.cbxPadre)
        Me.Controls.Add(Me.txtTipo)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.OcxTXTString5)
        Me.Controls.Add(Me.OcxTXTString4)
        Me.Controls.Add(Me.lblResolucion173)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.txtPlanCuenta)
        Me.Controls.Add(Me.lblPlanCuenta)
        Me.Controls.Add(Me.lblPadre)
        Me.Controls.Add(Me.txtCategoria)
        Me.Controls.Add(Me.lblCategoria)
        Me.Controls.Add(Me.txtAlias)
        Me.Controls.Add(Me.lblAlias)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblCodigo)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "frmAdministrarCuentaContable"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "---------------------------------------------------------------------------------" &
    "--------------------------------------------------------------------------------" &
    "-"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPlanCuenta As System.Windows.Forms.TextBox
    Friend WithEvents lblPlanCuenta As System.Windows.Forms.Label
    Friend WithEvents lblPadre As System.Windows.Forms.Label
    Friend WithEvents txtCategoria As ERP.ocxTXTNumeric
    Friend WithEvents lblCategoria As System.Windows.Forms.Label
    Friend WithEvents txtAlias As ERP.ocxTXTString
    Friend WithEvents lblAlias As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As ERP.ocxTXTString
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents OcxTXTString4 As ERP.ocxTXTString
    Friend WithEvents lblResolucion173 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTString5 As ERP.ocxTXTString
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtTipo As System.Windows.Forms.TextBox
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents cbxPadre As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxUnidadNegocio As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxCentroCosto As ERP.ocxCBX
    Friend WithEvents chkCentroCosto As ERP.ocxCHK
    Friend WithEvents chkUnidadNegocio As ERP.ocxCHK
    Friend WithEvents OcxTXTRG49 As ocxTXTString
    Friend WithEvents OcxTXTDescripcion As ocxTXTString
    Friend WithEvents lblCuentaContableRG49 As Label
End Class

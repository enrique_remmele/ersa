﻿Public Class frmCambiarPlanCuenta

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones
        ListarPlanCuenta()

        'Foco
        dgv.Focus()

    End Sub

    Sub ListarPlanCuenta()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select ID, Descripcion, Tipo From VPlanCuenta Where Estado = 'True' ")
        CSistema.dtToGrid(dgv, dt)

        'Formato
        dgv.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub Aceptar()

        'Validar
        If dgv.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione correctamente un registro!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Consultar
        If MessageBox.Show("Este proceso puede modificar drasticamente el funcionamiento del sistema! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If

        Dim IDPlanCuenta As Integer = dgv.SelectedRows(0).Cells(0).Value


        Dim SQL As String = ""
        SQL = SQL & "Update PlanCuenta Set Titular='False' " & vbCrLf
        SQL = SQL & "Update PlanCuenta Set Titular='True' Where ID=" & IDPlanCuenta & "  " & vbCrLf

        'Error
        If CSistema.ExecuteNonQuery(SQL) < 0 Then

        End If

        ListarPlanCuenta()

    End Sub

    Private Sub frmCambiarPlanCuenta_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmCambiarPlanCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgv_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown
        If e.KeyCode = Keys.Enter Then
            'tu codigo
            e.SuppressKeyPress = True
        End If
    End Sub

    'FA 28/05/2021
    Sub frmCambiarPlanCuenta_Activate()
        Me.Refresh()
    End Sub

End Class
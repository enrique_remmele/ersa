﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuentaContable
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCuentaContable))
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.gbxPlanCuenta = New System.Windows.Forms.GroupBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.rdbResolucion173 = New System.Windows.Forms.RadioButton()
        Me.rdbEmpresa = New System.Windows.Forms.RadioButton()
        Me.cbxPlanCuenta = New ERP.ocxCBX()
        Me.tvCuentas = New System.Windows.Forms.TreeView()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExpandirNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExpandirTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ContraerNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContraerTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.gbxInfo = New System.Windows.Forms.GroupBox()
        Me.txtCuentaRG49 = New ERP.ocxTXTString()
        Me.lblCuentaContableRG49 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtCentroCosto = New ERP.ocxTXTString()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtUnidadNegocio = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.lblPlanCuenta2 = New System.Windows.Forms.Label()
        Me.txtEstado = New ERP.ocxTXTString()
        Me.lblID = New System.Windows.Forms.Label()
        Me.txtTipo = New ERP.ocxTXTString()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txtPlanCuenta = New System.Windows.Forms.TextBox()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.txtCodigo = New ERP.ocxTXTString()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.lblAlias = New System.Windows.Forms.Label()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.txtAlias = New ERP.ocxTXTString()
        Me.txtPadre = New ERP.ocxTXTString()
        Me.lblCategoria1 = New System.Windows.Forms.Label()
        Me.lblPadre = New System.Windows.Forms.Label()
        Me.txtCategoria = New ERP.ocxTXTNumeric()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.gbxPlanCuenta.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.gbxInfo.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TableLayoutPanel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbxInfo)
        Me.SplitContainer1.Panel2.Controls.Add(Me.StatusStrip1)
        Me.SplitContainer1.Size = New System.Drawing.Size(968, 470)
        Me.SplitContainer1.SplitterDistance = 520
        Me.SplitContainer1.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.gbxPlanCuenta, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tvCuentas, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(520, 470)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'gbxPlanCuenta
        '
        Me.gbxPlanCuenta.Controls.Add(Me.LinkLabel1)
        Me.gbxPlanCuenta.Controls.Add(Me.rdbResolucion173)
        Me.gbxPlanCuenta.Controls.Add(Me.rdbEmpresa)
        Me.gbxPlanCuenta.Controls.Add(Me.cbxPlanCuenta)
        Me.gbxPlanCuenta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxPlanCuenta.Location = New System.Drawing.Point(3, 3)
        Me.gbxPlanCuenta.Name = "gbxPlanCuenta"
        Me.gbxPlanCuenta.Size = New System.Drawing.Size(514, 32)
        Me.gbxPlanCuenta.TabIndex = 0
        Me.gbxPlanCuenta.TabStop = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(6, 11)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(88, 13)
        Me.LinkLabel1.TabIndex = 4
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Plan de Cuentas:"
        '
        'rdbResolucion173
        '
        Me.rdbResolucion173.AutoSize = True
        Me.rdbResolucion173.Location = New System.Drawing.Point(365, 9)
        Me.rdbResolucion173.Name = "rdbResolucion173"
        Me.rdbResolucion173.Size = New System.Drawing.Size(116, 17)
        Me.rdbResolucion173.TabIndex = 3
        Me.rdbResolucion173.Text = "Resolucion 173/04"
        Me.rdbResolucion173.UseVisualStyleBackColor = True
        '
        'rdbEmpresa
        '
        Me.rdbEmpresa.AutoSize = True
        Me.rdbEmpresa.Checked = True
        Me.rdbEmpresa.Location = New System.Drawing.Point(293, 9)
        Me.rdbEmpresa.Name = "rdbEmpresa"
        Me.rdbEmpresa.Size = New System.Drawing.Size(66, 17)
        Me.rdbEmpresa.TabIndex = 2
        Me.rdbEmpresa.TabStop = True
        Me.rdbEmpresa.Text = "Empresa"
        Me.rdbEmpresa.UseVisualStyleBackColor = True
        '
        'cbxPlanCuenta
        '
        Me.cbxPlanCuenta.CampoWhere = Nothing
        Me.cbxPlanCuenta.CargarUnaSolaVez = False
        Me.cbxPlanCuenta.DataDisplayMember = Nothing
        Me.cbxPlanCuenta.DataFilter = Nothing
        Me.cbxPlanCuenta.DataOrderBy = Nothing
        Me.cbxPlanCuenta.DataSource = Nothing
        Me.cbxPlanCuenta.DataValueMember = Nothing
        Me.cbxPlanCuenta.dtSeleccionado = Nothing
        Me.cbxPlanCuenta.FormABM = Nothing
        Me.cbxPlanCuenta.Indicaciones = Nothing
        Me.cbxPlanCuenta.Location = New System.Drawing.Point(98, 7)
        Me.cbxPlanCuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxPlanCuenta.Name = "cbxPlanCuenta"
        Me.cbxPlanCuenta.SeleccionMultiple = False
        Me.cbxPlanCuenta.SeleccionObligatoria = False
        Me.cbxPlanCuenta.Size = New System.Drawing.Size(189, 21)
        Me.cbxPlanCuenta.SoloLectura = False
        Me.cbxPlanCuenta.TabIndex = 1
        Me.cbxPlanCuenta.Texto = ""
        '
        'tvCuentas
        '
        Me.tvCuentas.AllowDrop = True
        Me.tvCuentas.ContextMenuStrip = Me.ContextMenuStrip1
        Me.tvCuentas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvCuentas.FullRowSelect = True
        Me.tvCuentas.HideSelection = False
        Me.tvCuentas.ImageIndex = 0
        Me.tvCuentas.ImageList = Me.ImageList101
        Me.tvCuentas.Location = New System.Drawing.Point(3, 41)
        Me.tvCuentas.Name = "tvCuentas"
        Me.tvCuentas.SelectedImageIndex = 0
        Me.tvCuentas.Size = New System.Drawing.Size(514, 426)
        Me.tvCuentas.TabIndex = 1
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem, Me.EditarToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.ToolStripSeparator2, Me.ExpandirNodoToolStripMenuItem, Me.ExpandirTodoToolStripMenuItem, Me.ToolStripSeparator3, Me.ContraerNodoToolStripMenuItem, Me.ContraerTodoToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(152, 170)
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.NuevoToolStripMenuItem.Text = "Nuevo"
        '
        'EditarToolStripMenuItem
        '
        Me.EditarToolStripMenuItem.Name = "EditarToolStripMenuItem"
        Me.EditarToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.EditarToolStripMenuItem.Text = "Editar"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(148, 6)
        '
        'ExpandirNodoToolStripMenuItem
        '
        Me.ExpandirNodoToolStripMenuItem.Name = "ExpandirNodoToolStripMenuItem"
        Me.ExpandirNodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExpandirNodoToolStripMenuItem.Text = "Expandir nodo"
        '
        'ExpandirTodoToolStripMenuItem
        '
        Me.ExpandirTodoToolStripMenuItem.Name = "ExpandirTodoToolStripMenuItem"
        Me.ExpandirTodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExpandirTodoToolStripMenuItem.Text = "Expandir todo"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(148, 6)
        '
        'ContraerNodoToolStripMenuItem
        '
        Me.ContraerNodoToolStripMenuItem.Name = "ContraerNodoToolStripMenuItem"
        Me.ContraerNodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ContraerNodoToolStripMenuItem.Text = "Contraer nodo"
        '
        'ContraerTodoToolStripMenuItem
        '
        Me.ContraerTodoToolStripMenuItem.Name = "ContraerTodoToolStripMenuItem"
        Me.ContraerTodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ContraerTodoToolStripMenuItem.Text = "Contraer todo"
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "icon.folder.close.gif")
        Me.ImageList101.Images.SetKeyName(1, "icon.folder.open.gif")
        Me.ImageList101.Images.SetKeyName(2, "form_specific_completed.gif")
        '
        'gbxInfo
        '
        Me.gbxInfo.Controls.Add(Me.txtCuentaRG49)
        Me.gbxInfo.Controls.Add(Me.lblCuentaContableRG49)
        Me.gbxInfo.Controls.Add(Me.Button1)
        Me.gbxInfo.Controls.Add(Me.txtCentroCosto)
        Me.gbxInfo.Controls.Add(Me.Label2)
        Me.gbxInfo.Controls.Add(Me.txtUnidadNegocio)
        Me.gbxInfo.Controls.Add(Me.Label1)
        Me.gbxInfo.Controls.Add(Me.btnImprimir)
        Me.gbxInfo.Controls.Add(Me.lblPlanCuenta2)
        Me.gbxInfo.Controls.Add(Me.txtEstado)
        Me.gbxInfo.Controls.Add(Me.lblID)
        Me.gbxInfo.Controls.Add(Me.txtTipo)
        Me.gbxInfo.Controls.Add(Me.txtID)
        Me.gbxInfo.Controls.Add(Me.lblDescripcion)
        Me.gbxInfo.Controls.Add(Me.lblEstado)
        Me.gbxInfo.Controls.Add(Me.txtPlanCuenta)
        Me.gbxInfo.Controls.Add(Me.lblCodigo)
        Me.gbxInfo.Controls.Add(Me.lblTipo)
        Me.gbxInfo.Controls.Add(Me.txtDescripcion)
        Me.gbxInfo.Controls.Add(Me.btnEliminar)
        Me.gbxInfo.Controls.Add(Me.txtCodigo)
        Me.gbxInfo.Controls.Add(Me.btnEditar)
        Me.gbxInfo.Controls.Add(Me.lblAlias)
        Me.gbxInfo.Controls.Add(Me.btnNuevo)
        Me.gbxInfo.Controls.Add(Me.txtAlias)
        Me.gbxInfo.Controls.Add(Me.txtPadre)
        Me.gbxInfo.Controls.Add(Me.lblCategoria1)
        Me.gbxInfo.Controls.Add(Me.lblPadre)
        Me.gbxInfo.Controls.Add(Me.txtCategoria)
        Me.gbxInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbxInfo.Name = "gbxInfo"
        Me.gbxInfo.Size = New System.Drawing.Size(444, 448)
        Me.gbxInfo.TabIndex = 26
        Me.gbxInfo.TabStop = False
        '
        'txtCuentaRG49
        '
        Me.txtCuentaRG49.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCuentaRG49.Color = System.Drawing.Color.Empty
        Me.txtCuentaRG49.Enabled = False
        Me.txtCuentaRG49.Indicaciones = Nothing
        Me.txtCuentaRG49.Location = New System.Drawing.Point(90, 321)
        Me.txtCuentaRG49.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCuentaRG49.Multilinea = False
        Me.txtCuentaRG49.Name = "txtCuentaRG49"
        Me.txtCuentaRG49.Size = New System.Drawing.Size(237, 21)
        Me.txtCuentaRG49.SoloLectura = True
        Me.txtCuentaRG49.TabIndex = 70
        Me.txtCuentaRG49.TabStop = False
        Me.txtCuentaRG49.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCuentaRG49.Texto = ""
        '
        'lblCuentaContableRG49
        '
        Me.lblCuentaContableRG49.AutoSize = True
        Me.lblCuentaContableRG49.Enabled = False
        Me.lblCuentaContableRG49.Location = New System.Drawing.Point(6, 325)
        Me.lblCuentaContableRG49.Name = "lblCuentaContableRG49"
        Me.lblCuentaContableRG49.Size = New System.Drawing.Size(75, 13)
        Me.lblCuentaContableRG49.TabIndex = 69
        Me.lblCuentaContableRG49.Text = "Cuenta RG49:"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(90, 414)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(157, 23)
        Me.Button1.TabIndex = 31
        Me.Button1.Text = "&Exportar Saldos"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtCentroCosto
        '
        Me.txtCentroCosto.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCentroCosto.Color = System.Drawing.Color.Empty
        Me.txtCentroCosto.Indicaciones = Nothing
        Me.txtCentroCosto.Location = New System.Drawing.Point(90, 258)
        Me.txtCentroCosto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCentroCosto.Multilinea = False
        Me.txtCentroCosto.Name = "txtCentroCosto"
        Me.txtCentroCosto.Size = New System.Drawing.Size(236, 21)
        Me.txtCentroCosto.SoloLectura = True
        Me.txtCentroCosto.TabIndex = 30
        Me.txtCentroCosto.TabStop = False
        Me.txtCentroCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCentroCosto.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 262)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Centro de Costo:"
        '
        'txtUnidadNegocio
        '
        Me.txtUnidadNegocio.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.txtUnidadNegocio.Indicaciones = Nothing
        Me.txtUnidadNegocio.Location = New System.Drawing.Point(90, 229)
        Me.txtUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtUnidadNegocio.Multilinea = False
        Me.txtUnidadNegocio.Name = "txtUnidadNegocio"
        Me.txtUnidadNegocio.Size = New System.Drawing.Size(236, 21)
        Me.txtUnidadNegocio.SoloLectura = True
        Me.txtUnidadNegocio.TabIndex = 28
        Me.txtUnidadNegocio.TabStop = False
        Me.txtUnidadNegocio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUnidadNegocio.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 233)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Unidad Negocio:"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(251, 414)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 26
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'lblPlanCuenta2
        '
        Me.lblPlanCuenta2.AutoSize = True
        Me.lblPlanCuenta2.Location = New System.Drawing.Point(6, 16)
        Me.lblPlanCuenta2.Name = "lblPlanCuenta2"
        Me.lblPlanCuenta2.Size = New System.Drawing.Size(83, 13)
        Me.lblPlanCuenta2.TabIndex = 23
        Me.lblPlanCuenta2.Text = "Plan de Cuenta:"
        '
        'txtEstado
        '
        Me.txtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtEstado.Color = System.Drawing.Color.Empty
        Me.txtEstado.Indicaciones = Nothing
        Me.txtEstado.Location = New System.Drawing.Point(90, 288)
        Me.txtEstado.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEstado.Multilinea = False
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(107, 21)
        Me.txtEstado.SoloLectura = True
        Me.txtEstado.TabIndex = 25
        Me.txtEstado.TabStop = False
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEstado.Texto = ""
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(6, 42)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 2
        Me.lblID.Text = "ID:"
        '
        'txtTipo
        '
        Me.txtTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtTipo.Color = System.Drawing.Color.Empty
        Me.txtTipo.Indicaciones = Nothing
        Me.txtTipo.Location = New System.Drawing.Point(90, 200)
        Me.txtTipo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTipo.Multilinea = False
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.Size = New System.Drawing.Size(107, 21)
        Me.txtTipo.SoloLectura = True
        Me.txtTipo.TabIndex = 24
        Me.txtTipo.TabStop = False
        Me.txtTipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipo.Texto = ""
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        Me.txtID.Location = New System.Drawing.Point(90, 38)
        Me.txtID.Name = "txtID"
        Me.txtID.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(43, 20)
        Me.txtID.TabIndex = 3
        Me.txtID.Text = "0"
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(6, 95)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 6
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(6, 292)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 16
        Me.lblEstado.Text = "Estado:"
        '
        'txtPlanCuenta
        '
        Me.txtPlanCuenta.BackColor = System.Drawing.Color.White
        Me.txtPlanCuenta.Location = New System.Drawing.Point(90, 12)
        Me.txtPlanCuenta.Name = "txtPlanCuenta"
        Me.txtPlanCuenta.ReadOnly = True
        Me.txtPlanCuenta.Size = New System.Drawing.Size(237, 20)
        Me.txtPlanCuenta.TabIndex = 1
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(6, 68)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCodigo.TabIndex = 4
        Me.lblCodigo.Text = "Codigo:"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(6, 203)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 14
        Me.lblTipo.Text = "Tipo:"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(90, 91)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(237, 21)
        Me.txtDescripcion.SoloLectura = True
        Me.txtDescripcion.TabIndex = 7
        Me.txtDescripcion.TabStop = False
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(252, 369)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 21
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Color = System.Drawing.Color.Empty
        Me.txtCodigo.Indicaciones = Nothing
        Me.txtCodigo.Location = New System.Drawing.Point(90, 64)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigo.Multilinea = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(107, 21)
        Me.txtCodigo.SoloLectura = True
        Me.txtCodigo.TabIndex = 5
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigo.Texto = ""
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(171, 369)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 20
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'lblAlias
        '
        Me.lblAlias.AutoSize = True
        Me.lblAlias.Location = New System.Drawing.Point(6, 122)
        Me.lblAlias.Name = "lblAlias"
        Me.lblAlias.Size = New System.Drawing.Size(32, 13)
        Me.lblAlias.TabIndex = 8
        Me.lblAlias.Text = "Alias:"
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(90, 369)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 19
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'txtAlias
        '
        Me.txtAlias.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtAlias.Color = System.Drawing.Color.Empty
        Me.txtAlias.Indicaciones = Nothing
        Me.txtAlias.Location = New System.Drawing.Point(90, 118)
        Me.txtAlias.Margin = New System.Windows.Forms.Padding(4)
        Me.txtAlias.Multilinea = False
        Me.txtAlias.Name = "txtAlias"
        Me.txtAlias.Size = New System.Drawing.Size(107, 21)
        Me.txtAlias.SoloLectura = True
        Me.txtAlias.TabIndex = 9
        Me.txtAlias.TabStop = False
        Me.txtAlias.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAlias.Texto = ""
        '
        'txtPadre
        '
        Me.txtPadre.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPadre.Color = System.Drawing.Color.Empty
        Me.txtPadre.Indicaciones = Nothing
        Me.txtPadre.Location = New System.Drawing.Point(90, 173)
        Me.txtPadre.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPadre.Multilinea = False
        Me.txtPadre.Name = "txtPadre"
        Me.txtPadre.Size = New System.Drawing.Size(237, 21)
        Me.txtPadre.SoloLectura = True
        Me.txtPadre.TabIndex = 13
        Me.txtPadre.TabStop = False
        Me.txtPadre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPadre.Texto = ""
        '
        'lblCategoria1
        '
        Me.lblCategoria1.AutoSize = True
        Me.lblCategoria1.Location = New System.Drawing.Point(6, 150)
        Me.lblCategoria1.Name = "lblCategoria1"
        Me.lblCategoria1.Size = New System.Drawing.Size(55, 13)
        Me.lblCategoria1.TabIndex = 10
        Me.lblCategoria1.Text = "Categoria:"
        '
        'lblPadre
        '
        Me.lblPadre.AutoSize = True
        Me.lblPadre.Location = New System.Drawing.Point(6, 177)
        Me.lblPadre.Name = "lblPadre"
        Me.lblPadre.Size = New System.Drawing.Size(38, 13)
        Me.lblPadre.TabIndex = 12
        Me.lblPadre.Text = "Padre:"
        '
        'txtCategoria
        '
        Me.txtCategoria.Color = System.Drawing.Color.Empty
        Me.txtCategoria.Decimales = True
        Me.txtCategoria.Indicaciones = Nothing
        Me.txtCategoria.Location = New System.Drawing.Point(90, 145)
        Me.txtCategoria.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCategoria.Name = "txtCategoria"
        Me.txtCategoria.Size = New System.Drawing.Size(43, 22)
        Me.txtCategoria.SoloLectura = True
        Me.txtCategoria.TabIndex = 11
        Me.txtCategoria.TabStop = False
        Me.txtCategoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCategoria.Texto = "0"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 448)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(444, 22)
        Me.StatusStrip1.TabIndex = 22
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'frmCuentaContable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(968, 470)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "frmCuentaContable"
        Me.Text = "frmCuentaContable"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.gbxPlanCuenta.ResumeLayout(False)
        Me.gbxPlanCuenta.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.gbxInfo.ResumeLayout(False)
        Me.gbxInfo.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents gbxPlanCuenta As System.Windows.Forms.GroupBox
    Friend WithEvents tvCuentas As System.Windows.Forms.TreeView
    Friend WithEvents txtAlias As ERP.ocxTXTString
    Friend WithEvents lblAlias As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As ERP.ocxTXTString
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblCodigo As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtPadre As ERP.ocxTXTString
    Friend WithEvents lblPadre As System.Windows.Forms.Label
    Friend WithEvents txtCategoria As ERP.ocxTXTNumeric
    Friend WithEvents lblCategoria1 As System.Windows.Forms.Label
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents txtPlanCuenta As System.Windows.Forms.TextBox
    Friend WithEvents ImageList101 As System.Windows.Forms.ImageList
    Friend WithEvents rdbResolucion173 As System.Windows.Forms.RadioButton
    Friend WithEvents rdbEmpresa As System.Windows.Forms.RadioButton
    Friend WithEvents cbxPlanCuenta As ERP.ocxCBX
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblPlanCuenta2 As System.Windows.Forms.Label
    Friend WithEvents txtEstado As ERP.ocxTXTString
    Friend WithEvents txtTipo As ERP.ocxTXTString
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents gbxInfo As System.Windows.Forms.GroupBox
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents NuevoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExpandirNodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExpandirTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ContraerNodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContraerTodoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents txtCentroCosto As ERP.ocxTXTString
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUnidadNegocio As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As Button
    Friend WithEvents txtCuentaRG49 As ocxTXTString
    Friend WithEvents lblCuentaContableRG49 As Label
End Class

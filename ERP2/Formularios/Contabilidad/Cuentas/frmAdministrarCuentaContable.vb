﻿Public Class frmAdministrarCuentaContable

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CCTemp As Integer
    Dim CodigoTemp As String

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private PadreValue As String
    Public Property Padre() As String
        Get
            Return PadreValue
        End Get
        Set(ByVal value As String)
            PadreValue = value
        End Set
    End Property

    Private CodigoPadreValue As String
    Public Property CodigoPadre() As String
        Get
            Return CodigoPadreValue
        End Get
        Set(ByVal value As String)
            CodigoPadreValue = value
        End Set
    End Property

    Private IDPadreValue As Integer
    Public Property IDPadre() As Integer
        Get
            Return IDPadreValue
        End Get
        Set(ByVal value As Integer)
            IDPadreValue = value
        End Set
    End Property

    Private PlanCuentaValue As String
    Public Property PlanCuenta() As String
        Get
            Return PlanCuentaValue
        End Get
        Set(ByVal value As String)
            PlanCuentaValue = value
        End Set
    End Property

    Private IDPlanCuentaValue As Integer
    Public Property IDPlanCuenta() As Integer
        Get
            Return IDPlanCuentaValue
        End Get
        Set(ByVal value As Integer)
            IDPlanCuentaValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private CategoriaValue As Integer
    Public Property Categoria() As Integer
        Get
            Return CategoriaValue
        End Get
        Set(ByVal value As Integer)
            CategoriaValue = value
        End Set
    End Property

    Private TipoValue As String
    Public Property Tipo() As String
        Get
            Return TipoValue
        End Get
        Set(ByVal value As String)
            TipoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim oRow As DataRow
    Dim vTabla As String = "VCuentaContable"

    'FUNCIONES
    Sub Inicializar()

        'Form
        If Nuevo = True Then
            Me.Text = "Agregar una Cuenta"
        Else
            Me.Text = "Modificar la cuenta"
        End If

        'RadioButton
        rdbActivo.Checked = True

        'Funciones
        CargarInformacion()


    End Sub

    Sub CargarInformacion()

        'Detalle
        txtCategoria.txt.Text = Categoria
        txtTipo.Text = Tipo

        'Sucursales
        cbxSucursal.Conectar()

        'Unidad de negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("VUnidadNegocio", "Estado='True'").Copy, "ID", "Descripcion")
        cbxUnidadNegocio.cbx.SelectedItem = 0

        'Centro de Costo
        CSistema.SqlToComboBox(cbxCentroCosto.cbx, CData.GetTable("VCentroCosto", "Estado='True'").Copy, "ID", "Descripcion")
        cbxUnidadNegocio.cbx.SelectedItem = 0

        'Padres
        Dim dtTemp As DataTable = CData.GetTable(vTabla, "IDPlanCuenta=" & IDPlanCuenta & " ").Copy
        CSistema.OrderDataTable(dtTemp, "Cuenta")
        CSistema.SqlToComboBox(cbxPadre.cbx, dtTemp, "ID", "Cuenta")

        If Nuevo = True Then

            ID = CInt(CSistema.ExecuteScalar("Select ISNULL(MAX(ID)+1, 1) From CuentaContable"))
            txtID.Text = ID
            txtPlanCuenta.Text = PlanCuenta
            cbxPadre.cbx.Text = Padre
            txtCodigo.txt.Text = CodigoPadre
            txtCodigo.txt.Focus()
            txtCodigo.txt.SelectionStart = txtCodigo.txt.Text.Length
            chkSucursal.Valor = False
            cbxSucursal.Enabled = False


        Else

            Try
                'oRow = CSistema.ExecuteToDataTable("Select * From VCuentaContable Where ID=" & ID & " ").Rows(0)
                oRow = CData.GetTable(vTabla, "ID=" & ID & " ").Rows(0)
                txtID.Text = ID.ToString
                txtPlanCuenta.Text = oRow("PlanCuenta").ToString
                cbxPadre.SelectedValue(oRow("IDPadre").ToString)

                txtCodigo.txt.Text = oRow("Codigo").ToString
                txtAlias.txt.Text = oRow("Alias").ToString
                txtDescripcion.txt.Text = oRow("Descripcion").ToString

                txtCategoria.txt.Text = oRow("Categoria").ToString
                txtTipo.Text = oRow("Tipo").ToString

                If CBool(oRow("Activo").ToString) = True Then
                    rdbActivo.Checked = True
                Else
                    rdbDesactivado.Checked = True
                End If

                IDPadre = oRow("IDPadre").ToString
                IDPlanCuenta = oRow("IDPlanCuenta").ToString

                'Sucursal
                chkSucursal.Valor = oRow("Sucursal").ToString
                If chkSucursal.Valor = True Then
                    cbxSucursal.Enabled = True
                    cbxSucursal.cbx.Text = oRow("Suc").ToString
                Else
                    cbxSucursal.Enabled = False
                    cbxSucursal.cbx.Text = "---"
                End If

                'Unidad de negocio
                If IsDBNull(oRow("IDUnidadNegocio")) = False Then
                    cbxUnidadNegocio.cbx.SelectedValue = oRow("IDUnidadNegocio")
                Else
                    cbxUnidadNegocio.cbx.Text = ""
                End If

                'Centro de Costo
                If IsDBNull(oRow("IDCentroCosto")) = False Then
                    cbxCentroCosto.cbx.SelectedValue = oRow("IDCentroCosto")
                Else
                    cbxCentroCosto.cbx.Text = ""
                End If

                OcxTXTRG49.txt.Text = oRow("CodigoRG49").ToString
                OcxTXTDescripcion.txt.Text = oRow("DescripcionRG49").ToString

            Catch ex As Exception
                Dim mensaje As String = ex.Message
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End Try

        End If

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        'Codigo
        If txtCodigo.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Introduzca un codigo valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Introduzca una descripcion valida!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion del Padre
        'If cbxPadre.Validar("Seleccione correctamente la cuenta PADRE", ctrError, btnGuardar, tsslEstado) = False Then
        '    Exit Sub
        'End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro y sus dependientes. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@ID", txtID.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPlanCuenta", IDPlanCuenta, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Codigo", txtCodigo.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Alias", txtAlias.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Categoria", txtCategoria.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPadre", cbxPadre.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)

        'Sucursal
        If chkSucursal.Valor = True Then
            CSistema.SetSQLParameter(param, "@Sucursal", True, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.cbx, ParameterDirection.Input)
        End If

        'Unidad Negocio
        If chkUnidadNegocio.Valor = True Then
            CSistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.cbx, ParameterDirection.Input)
        End If

        'Centro Costo
        If chkCentroCosto.Valor = True Then
            CSistema.SetSQLParameter(param, "@IDCentroCosto", cbxCentroCosto.cbx, ParameterDirection.Input)
        End If

        'Agrega los datos en la tabla que vincula los ID's Resolucion 49
        ''CSistema.SetSQLParameter(param, "@IDCuentaContableRG49", CCTemp, ParameterDirection.Input)
        ''If CodigoTemp <> 0 Then
        ''    CSistema.SetSQLParameter(param, "@CodigoRG49", CodigoTemp, ParameterDirection.Input)
        ''    CSistema.SetSQLParameter(param, "@DescripcionRG49", OcxTXTDescripcion.txt.Text, ParameterDirection.Input)
        ''End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro

        If CSistema.ExecuteStoreProcedure(param, "SpCuentaContable", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Procesado = False
            Exit Sub
        End If

        'Actualizar DataTable
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Then
            CData.Actualizar(ID, vTabla)
        Else
            CData.Insertar(ID, vTabla)
        End If

        Procesado = True

        Me.Close()

    End Sub

    Private Sub frmAdministrarCuentaContable_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
       CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAdministrarCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Dim Opcion As CSistema.NUMOperacionesABM
        If Nuevo = True Then
            Opcion = ERP.CSistema.NUMOperacionesABM.INS
        Else
            Opcion = ERP.CSistema.NUMOperacionesABM.UPD
        End If

        Guardar(Opcion)

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Guardar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub cbxUnidadNegocio_PropertyChanged(sender As Object, e As System.EventArgs) Handles cbxUnidadNegocio.PropertyChanged
        ' cbxCentroCosto.cbx.Text = ""
        ' CSistema.SqlToComboBox(cbxCentroCosto.cbx, CData.GetTable("VCentroCosto", "IDUnidadNegocio=" & CStr(cbxUnidadNegocio.cbx.SelectedValue) & " And Estado='True'").Copy, "ID", "Descripcion")
    End Sub

    Private Sub chkUnidadNegocio_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkUnidadNegocio.PropertyChanged
        cbxUnidadNegocio.Enabled = chkUnidadNegocio.Valor
    End Sub

    Private Sub chkCentroCosto_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkCentroCosto.PropertyChanged
        cbxCentroCosto.Enabled = chkCentroCosto.Valor
    End Sub

    'FA 28/05/2021
    Sub frmAdministrarCuentaContable_Activate()
        Me.Refresh()
    End Sub

    Private Sub OcxTXTRG49_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles OcxTXTRG49.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            BuscarCuenta()
        End If

    End Sub

    Sub BuscarCuenta()
        Dim CC As DataTable
        'Dim CCTemp As Integer

        If OcxTXTRG49.txt.Text = "" Then
            tsslEstado.Text = "Escriba una cuenta"
            OcxTXTRG49.txt.Clear()
            OcxTXTDescripcion.txt.Text = ""
            Exit Sub
        End If

        CC = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From VCuentaContableRG49 where codigo =" & OcxTXTRG49.txt.Text)
        tsslEstado.Text = ""


        If CC.Rows.Count = 0 Then
            tsslEstado.Text = "La cuenta No existe"
            OcxTXTRG49.txt.Clear()
            OcxTXTDescripcion.txt.Text = ""
            Exit Sub
        Else
            Dim orow1 As DataRow = CC.Rows(0)
            OcxTXTDescripcion.txt.Text = orow1("Descripcion")
            CCTemp = orow1("ID")
            CodigoTemp = orow1("Codigo")
        End If



    End Sub

End Class
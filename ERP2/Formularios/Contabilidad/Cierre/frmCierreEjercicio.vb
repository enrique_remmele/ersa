﻿Public Class frmCierreEjercicio
    'CLASES
    Dim CSistema As New CSistema

    'SC:16/02/2022
    Dim Consulta As String
    Dim Where As String

    Sub Inicializar()
        NumericUpDown1.Minimum = Date.Now.Year - 5
        NumericUpDown1.Maximum = Date.Now.Year + 5
        NumericUpDown1.Value = Date.Now.Year
    End Sub

    Sub Procesar()

        Dim IDTransaccion As Integer = 0
        Dim frm As New frmCierreCuentas
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Text = "Cierre de Cuentas"
        frm.Año = NumericUpDown1.Value

        'Mostramos
        frm.ShowDialog(Me)


        tsslEstado.Text = ""
        ctrError.Clear()



        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        CSistema.SetSQLParameter(param, "@Operacion", "CIERREDELEJERCICCIO", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Año", NumericUpDown1.Value, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""
        If CSistema.ExecuteStoreProcedure(param, "SpCierreContable", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnProcesar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.TopRight)

        Else
            MessageBox.Show("Registro guardado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End If



    End Sub

    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Consulta = "Select Numero, Suc, Fecha, Moneda, 'T. Comp.'=TipoComprobante, 'Comprobante'=NroComprobante, Detalle, Total, Estado, IDTransaccion From VAsiento "

        Where = Condicion

        If Numero > 0 Then
            Where = " Where Numero=" & Numero
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable(Consulta & " " & Where & " Order By Fecha ")

    End Sub


    Private Sub frmCierredelEjercicio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(sender As System.Object, e As System.EventArgs) Handles btnProcesar.Click
        'Procesar()
        'Validar que todo el cierre del Año seleccionado este Conciliado
        Dim Año As Integer = NumericUpDown1.Value
        Dim AsientoSinConciliar As Integer = CSistema.ExecuteScalar("Select IsNull((select count(*) from vasiento where year(fecha) =" & Año & " and conciliado = 'False' and idoperacion in (1,2,31,29,15,57,29,61,62,63,64)), 0 )")

        'Si el saldo es incorrecto
        If AsientoSinConciliar > 0 Then
            MsgBox("Los Asientos NO están totalmente conciliados para ejecutar el cierre!")
            Exit Sub
        End If

        Dim Fecha As Date = "31/12/" & NumericUpDown1.Value
        Dim Conciliado As Boolean = CBool(CSistema.ExecuteScalar("Select dbo.FValidarCierreContable('" & CSistema.FormatoFechaBaseDatos(Fecha, True, False) & "')"))
        If Conciliado = True Then
            Procesar()
            RegenerarAsientos()
        Else
            MsgBox("Ya existe un Cierre")
            Exit Sub
        End If
    End Sub

    Sub RegenerarAsientos()

        If MessageBox.Show("Se Recalcularan Saldos, este proceso puede durar varios minutos! Desea continuar? ", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
            Exit Sub
        End If
        MessageBox.Show("Aguarde mensaje de Finalizacion de Proceso...", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Dim Tabla As String = "SpRecalcularPlanCuentaSaldo"

        For i As Integer = 1 To 12

            Dim SQL As String = "Exec " & Tabla & " @Año='" & NumericUpDown1.Value & "', @Mes='" & i & "' "
            Dim dttemp As DataTable = CSistema.ExecuteToDataTable(SQL, "", 1000)

            If dttemp Is Nothing Then
                MessageBox.Show("Error en la consulta...", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            If dttemp.Rows.Count = 0 Then
                MessageBox.Show("Error en la consulta...", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            Dim Resultado As DataRow = dttemp.Rows(0)

            If Resultado("Procesado") = False Then
                MessageBox.Show(Resultado("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            Dim Registros As Integer = Resultado("Registros")

        Next

        MessageBox.Show("PROCESO FINALIZADO: Se Recalcularon Saldos del Año de Cierre", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Sub frmCierreEjercicio_Activate()
        Me.Refresh()
    End Sub

End Class
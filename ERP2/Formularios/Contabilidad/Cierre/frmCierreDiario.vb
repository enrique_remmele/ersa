﻿Public Class frmCierreDiario

    'CLASES
    Dim CSistema As New CSistema

    Sub Conciliar()
        tsslEstado.Text = ""
        ctrError.Clear()



        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        CSistema.SetSQLParameter(param, "@Operacion", "CIERREDIARIO", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(dtpHasta, True, False), ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""
        If CSistema.ExecuteStoreProcedure(param, "SpCierreContable", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnProcesar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.TopRight)

        Else
            MessageBox.Show("Registro gurdado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End If

    End Sub

    Sub Desconciliar()

        tsslEstado.Text = ""
        ctrError.Clear()

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        CSistema.SetSQLParameter(param, "@Operacion", "DESCONCILIACIONDIARIO", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(dtpHasta, True, False), ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1


        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""
        If CSistema.ExecuteStoreProcedure(param, "SpCierreContable", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnDesconciliar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnDesconciliar, ErrorIconAlignment.TopRight)

        Else
            MessageBox.Show("Registro gurdado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End If

    End Sub


    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Conciliar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDesconciliar.Click
        Desconciliar()
    End Sub

    'FA 28/05/2021
    Sub frmCierreDiario_Activate()
        Me.Refresh()
    End Sub

End Class
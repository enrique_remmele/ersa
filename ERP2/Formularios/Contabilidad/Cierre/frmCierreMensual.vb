﻿Public Class frmCierreMensual
    'CLASES
    Dim CSistema As New CSistema

    Sub Inicializar()
        Dim Mes() As String = {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"}
        cbxMes.Items.AddRange(Mes)
        cbxMes.DropDownStyle = ComboBoxStyle.DropDownList
        cbxMes.SelectedIndex = Date.Now.Month - 1

        NumericUpDown1.Minimum = Date.Now.Year - 5
        NumericUpDown1.Maximum = Date.Now.Year + 5
        NumericUpDown1.Value = Date.Now.Year

    End Sub

    Sub Procesar()
        tsslEstado.Text = ""
        ctrError.Clear()



        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        CSistema.SetSQLParameter(param, "@Operacion", "CIERREMENSUAL", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Mes", cbxMes.SelectedIndex + 1, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Año", NumericUpDown1.Value, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""
        If CSistema.ExecuteStoreProcedure(param, "SpCierreContable", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnProcesar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnProcesar, ErrorIconAlignment.TopRight)

        Else
            MessageBox.Show("Registro guardado", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        End If




    End Sub

    Private Sub frmCierreMensual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    'FA 28/05/2021
    Sub frmCierreMensual_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmCierreCuentas
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Public Property Año As Integer
    'EVENTOS

    'VARIABLES
    Dim CodigoSeleccionado As String
    Dim IDUnidadNegocio As Integer
    Dim IDCentroCosto As Integer

    'PROPIEDADES
    Private CAsientoValue As New CAsiento
    Public Property CAsiento() As CAsiento
        Get
            Return CAsientoValue
        End Get
        Set(ByVal value As CAsiento)
            CAsientoValue = value
        End Set
    End Property

    Private ActualizandoValue As Boolean
    Public Property Actualizando() As Boolean
        Get
            Return ActualizandoValue
        End Get
        Set(ByVal value As Boolean)
            ActualizandoValue = value
        End Set
    End Property

    Private oRowCabeceraValue As DataRow
    Public Property oRowCabecera() As DataRow
        Get
            Return oRowCabeceraValue
        End Get
        Set(ByVal value As DataRow)
            oRowCabeceraValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private IDTransaccionOperacionValue As Integer
    Public Property IDTransaccionOperacion() As Integer
        Get
            Return IDTransaccionOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionOperacionValue = value
        End Set
    End Property

    Private VolverAGenerarValue As Boolean
    Public Property VolverAGenerar() As Boolean
        Get
            Return VolverAGenerarValue
        End Get
        Set(ByVal value As Boolean)
            VolverAGenerarValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Dim dtDetalleCierre As DataTable
    Dim dtCierre As DataTable

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        FGEstiloFormulario(Me)

        'Controles
        ocxCuenta.Conectar()
        CAsiento.Inicializar()

        'Funciones
        CargarInformacion()
        CargarItem()
        'Foco
        txtID.txt.Focus()
        txtCotizacion.Texto = "1"
        txtComprobante.Texto = txtID.ObtenerValor
        txtFecha.txt.Text = "31/12/" & Año.ToString

    End Sub

    Sub CargarInformacion()

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, CData.GetTable("VCiudadSucursal").Copy, "IDCiudad", "CodigoCiudad")

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal").Copy, "ID", "Codigo")
        CSistema.SqlToComboBox(cbxSucursalDetalle.cbx, CData.GetTable("VSucursal").Copy, "ID", "Codigo")

        'Unidad de negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("VUnidadNegocio").Copy, "ID", "Descripcion")
        cbxUnidadNegocio.cbx.SelectedValue = 0

        'Operacion
        CSistema.SqlToComboBox(cbxOperacion.cbx, CData.GetTable("Operacion").Copy, "ID", "Descripcion")
        cbxOperacion.cbx.SelectedValue = 55

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("TipoComprobante").Copy, "ID", "Descripcion")
        cbxTipoComprobante.cbx.SelectedValue = 138

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, CData.GetTable("VMoneda").Copy, "ID", "Referencia")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Detalle
        dgv.Columns.Add("colCodigo", "Codigo")
        dgv.Columns.Add("colDescripcion", "Descripcion")
        dgv.Columns.Add("colDebito", "Debito")
        dgv.Columns.Add("colCredito", "Credito")

        dgv.Columns("colCodigo").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colDescripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("colDebito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgv.Columns("colCredito").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells

        dgv.Columns("colDebito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("colCredito").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        'Operacion
        cbxTipo.cbx.Items.Add("DEBITO")
        cbxTipo.cbx.Items.Add("CREDITO")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        txtID.SetValue(CSistema.ExecuteScalar("Select IsNull(Max(Numero)+1,1) From Asiento"))

        'dtDetalleCierre = CSistema.ExecuteToDataTable("Select ID, Codigo, Denominacion, Debito = '0', Credito = '0', Saldo From VPlanCuentaSaldo Where Año=" & Año & " And Mes = 12 order by Codigo")
        dtDetalleCierre = CSistema.ExecuteToDataTable("Select ID, Codigo, IDCuentaContable, Denominacion, Debito = '0', Credito = '0', Saldo = sum(Saldo) From VPlanCuentaSaldoEjercicio Where Año=" & Año & "group by ID, Codigo, IDCuentaContable, Denominacion order by ID, Codigo, IDCuentaContable, Denominacion")
        dtDetalleCierre.Columns.Add("Fijo")

        For Each oRow As DataRow In dtDetalleCierre.Select("Codigo like '1%' or Codigo like '2%' or Codigo like '3%' or Codigo like '4%' or Codigo like '5%'")
            Dim Iniciales As String
            Iniciales = oRow("Codigo")
            Select Case Iniciales.Substring(0, 1).ToString
                Case "1"
                    If Iniciales.Substring(0, 1) = "1" And oRow("Saldo") > 0 Then
                        oRow("Credito") = oRow("Saldo")
                    Else
                        oRow("Debito") = oRow("Saldo") * -1
                    End If
                Case "2"
                    If Iniciales.Substring(0, 1) = "2" And oRow("Saldo") > 0 Then
                        oRow("Debito") = oRow("Saldo")
                    Else
                        oRow("Credito") = oRow("Saldo") * -1
                    End If
                Case "3"
                    If Iniciales.Substring(0, 1) = "3" And oRow("Saldo") > 0 Then
                        oRow("Debito") = oRow("Saldo")
                    Else
                        oRow("Credito") = oRow("Saldo") * -1
                    End If
                Case "4"
                    If Iniciales.Substring(0, 1) = "4" And oRow("Saldo") > 0 Then
                        oRow("Debito") = oRow("Saldo")
                    Else
                        oRow("Credito") = oRow("Saldo") * -1
                    End If
                Case "5"
                    If Iniciales.Substring(0, 1) = "5" And oRow("Saldo") > 0 Then
                        oRow("Credito") = oRow("Saldo")
                    Else
                        oRow("Debito") = oRow("Saldo") * -1
                    End If
            End Select
        Next

        ListarDetalleCierre(dgv)

    End Sub

    Public Sub CalcularTotales()

        CSistema.TotalesGrid(dgv, txtTotalDebitoLocal.txt, 2)
        CSistema.TotalesGrid(dgv, txtTotalCreditoLocal.txt, 3)

        'Hayar el saldo
        Dim Saldo As Decimal = 0
        Saldo = CDec(txtTotalCreditoLocal.ObtenerValor) - CDec(txtTotalDebitoLocal.ObtenerValor)

        txtSaldo.txt.Text = Saldo


    End Sub

    Sub EliminarItem()

        'Validar
        If dgv.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgv, mensaje)
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        For Each oRow As DataRow In dtDetalleCierre.Rows
            If oRow("Codigo") = dgv.SelectedRows(0).Cells(0).Value Then
                dtDetalleCierre.Rows.Remove(oRow)
                Exit For
            End If
        Next

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalleCierre.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalleCierre(dgv)
        CalcularTotales()

    End Sub

    Sub Procesar()

        'Validar
        'Detalle
        If dtDetalleCierre.Rows.Count = 0 Then
            Dim mensaje As String = "Ingrese el detalle!"
            ctrError.SetError(btnAceptar, mensaje)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Montos
        Dim Debitos As Decimal = txtTotalDebitoLocal.ObtenerValor
        Dim Creditos As Decimal = txtTotalCreditoLocal.ObtenerValor
        Dim Saldo As Decimal = Creditos - Debitos

        If Saldo <> 0 Then
            Dim mensaje As String = "Ingrese el detalle!"
            ctrError.SetError(btnAceptar, mensaje)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Total

    End Sub

    Sub Salir()
        Me.Close()
    End Sub

    Sub Guardar()

        'Validar
        'Si el saldo es incorrecto
        If txtSaldo.ObtenerValor <> 0 Then
            CSistema.MostrarError("Los importes no concuerdan!", ctrError, btnAceptar, tsslEstado)
            Exit Sub
        End If

        'Si el importe es incorrecto
        If txtTotalCreditoLocal.ObtenerValor = 0 Then
            CSistema.MostrarError("El asiento no puede ser 0!", ctrError, btnAceptar, tsslEstado)
            Exit Sub
        End If

        'Cargar la Transaccion
        ' Dim IDTransaccion As Integer = 0

        If IDTransaccionOperacion > 0 Then
            IDTransaccion = IDTransaccionOperacion
        Else
            IDTransaccion = GuardarTransaccion()
        End If

        If IDTransaccion = 0 Then
            CSistema.MostrarError("No se puede generar el asiento! Ocurrio un error al tratar de registrar la transaccion", ctrError, btnAceptar, tsslEstado)
            Exit Sub
        End If

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & " " & txtComprobante.GetValue

        'Cargar la Cabecera
        Dim NewRow As DataRow = CAsiento.dtAsiento.NewRow

        NewRow("IDCiudad") = cbxCiudad.cbx.SelectedValue
        NewRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        NewRow("Fecha") = txtFecha.GetValue
        NewRow("IDMoneda") = cbxMoneda.GetValue
        NewRow("Cotizacion") = txtCotizacion.ObtenerValor
        NewRow("IDTipoComprobante") = cbxTipoComprobante.GetValue
        NewRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        NewRow("NroComprobante") = txtID.ObtenerValor
        NewRow("Comprobante") = Comprobante
        NewRow("Total") = txtTotalCreditoLocal.ObtenerValor
        NewRow("Detalle") = txtDetalle.GetValue
        NewRow("IDCentroCosto") = cbxCentroCosto.GetValue
        NewRow("Bloquear") = chkBloquear.Checked.ToString
        'NewRow("Debito") = txtTotalDebitoLocal.ObtenerValor
        'NewRow("Credito") = txtTotalCreditoLocal.ObtenerValor

        CAsiento.dtAsiento.Rows.Add(NewRow)
        CAsiento.IDTransaccion = IDTransaccion
        CAsiento.IDCentroCosto = cbxCentroCosto.GetValue
        CAsiento.NroCaja = 1
        CAsiento.Bloquear = chkBloquear.Checked.ToString

        If CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS) = True Then
            Procesado = True
            IDTransaccion = CAsiento.IDTransaccion
            Me.Close()
        End If

        'Insertar en SPCierre - SC - 15/03/2022
        InsertarCierre(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS)

        InsertarDetalleCierre(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS)

        InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS)

    End Sub

    Sub InsertarCierre(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro)
        'Insertar en SPCierre - SC - 15/03/2022

        Dim NroASiento As String

        NroASiento = CType(CSistema.ExecuteScalar("Select IsNull((Select Numero From Asiento Where IDTransaccion=" & IDTransaccion & "),1)"), Integer)


        Dim param(-1) As SqlClient.SqlParameter
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroAsiento", NroASiento, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", "CIERRE", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaAsiento", CSistema.FormatoFechaBaseDatos(txtFecha.ToString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDetalle.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotalCreditoLocal.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Debito", CSistema.FormatoMonedaBaseDatos(txtTotalDebitoLocal.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Credito", CSistema.FormatoMonedaBaseDatos(txtTotalCreditoLocal.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Año", Año, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Insertar el detalle
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpCierre", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAceptar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro
        End If

    End Sub

    Function InsertarDetalleCierre(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        'InsertarDetalle = True

        dtDetalleCierre.DefaultView.Sort = "Codigo"

        For Each oRow As DataRow In dtDetalleCierre.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCuentaContable", oRow("IDCuentaContable").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Codigo", oRow("Codigo").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Credito", CSistema.FormatoMonedaBaseDatos(oRow("Credito").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Debito", CSistema.FormatoMonedaBaseDatos(oRow("Debito").ToString, True), ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleCierre", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnAceptar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If

        Next

    End Function

    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        dtDetalleCierre.DefaultView.Sort = "Codigo"

        For Each oRow As DataRow In dtDetalleCierre.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCuentaContable", oRow("IDCuentaContable").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Credito", CSistema.FormatoMonedaBaseDatos(oRow("Credito").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Debito", CSistema.FormatoMonedaBaseDatos(oRow("Debito").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Saldo").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", txtDetalle.Texto, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TIpoComprobante", cbxTipoComprobante.GetValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroComprobante", txtID.Texto, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCentroCosto", "0", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDUnidadNegocio", "1", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleAsiento", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnAceptar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnAceptar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If

        Next

    End Function

    Function GuardarTransaccion() As Integer

        GuardarTransaccion = 0
        Dim SQL As String = ""
        SQL = SQL & "Declare @Mensaje varchar(50)" & vbCrLf
        SQL = SQL & "Declare @Procesado bit" & vbCrLf
        SQL = SQL & "Declare @IDTransaccionSalida int" & vbCrLf
        SQL = SQL & "EXEC SpTransaccion " & vbCrLf
        SQL = SQL & "@IDUsuario = " & vgIDUsuario & ", " & vbCrLf
        SQL = SQL & "@IDSucursal = " & vgIDSucursal & ", " & vbCrLf
        SQL = SQL & "@IDDeposito = " & vgIDDeposito & ", " & vbCrLf
        SQL = SQL & "@IDTerminal = " & vgIDTerminal & ", " & vbCrLf
        SQL = SQL & "@IDOperacion = " & cbxOperacion.GetValue & ", " & vbCrLf
        SQL = SQL & "@Mensaje = @Mensaje OUTPUT, " & vbCrLf
        SQL = SQL & "@Procesado = @Procesado OUTPUT, " & vbCrLf
        SQL = SQL & "@IDTransaccion = @IDTransaccionSalida OUTPUT" & vbCrLf

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)

        If dt Is Nothing Then
            Return 0
        End If

        If dt.Rows.Count = 0 Then
            Return 0
        End If

        Dim orow As DataRow = dt.Rows(0)

        Return orow("IDTransaccion")

    End Function

    Sub PosicionarAutomaticamente()

        Dim Debito As Decimal = txtTotalDebitoLocal.ObtenerValor
        Dim Credito As Decimal = txtTotalCreditoLocal.ObtenerValor

        If Credito > Debito Then
            cbxTipo.cbx.SelectedIndex = 0
        Else
            cbxTipo.cbx.SelectedIndex = 1
        End If

    End Sub

    Sub VerDetalle()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim frm As New frmPropiedad
        frm.TituloVentana = "Propiedades de la Cuenta"
        frm.Titulo = dgv.SelectedRows(0).Cells(1).Value
        frm.dt = dtDetalleCierre
        frm.ShowDialog()

    End Sub

    Sub ListarTipoComprobante()

        cbxTipoComprobante.cbx.DataSource = Nothing

        If IsNumeric(cbxOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        Dim IDOperacion As Integer = cbxOperacion.GetValue
        Dim dt As DataTable = CData.GetTable("VTipoComprobante", " IDOperacion=" & IDOperacion).Copy

        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dt, "ID", "Codigo")

    End Sub

    Sub AgregarItem()

        'Validar
        'Cuenta
        If ocxCuenta.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente la cuenta!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Importe
        If IsNumeric(txtImporteLocal.ObtenerValor) = False Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtImporteLocal.ObtenerValor) <= 0 Then
            Dim mensaje As String = "El importe no es correcto!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If cbxTipo.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo!"
            ctrError.SetError(txtImporteLocal, mensaje)
            ctrError.SetIconAlignment(txtImporteLocal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalleCierre.NewRow()
        'dRow("IDTransaccion") = 0
        'dRow("IDCuentaContable") = ocxCuenta.Registro("ID").ToString
        'dRow("Cuenta") = ocxCuenta.Registro("Cuenta").ToString
        dRow("Codigo") = ocxCuenta.Registro("Codigo").ToString
        dRow("IDCuentaContable") = ocxCuenta.Registro("IDCuentaContable").ToString
        dRow("Denominacion") = ocxCuenta.Registro("Descripcion").ToString

        'If txtDescripcion.GetValue = "" Then
        '    dRow("Observacion") = txtDetalle.GetValue
        'Else
        '    dRow("Observacion") = txtDescripcion.GetValue
        'End If

        dRow("ID") = dtDetalleCierre.Rows.Count
        'dRow("Orden") = dtDetalleCierre.Rows.Count + 10

        Dim Importe As Decimal = CDec(txtImporteLocal.ObtenerValor)

        'Verificar la moneda
        'If dtDetalleCierre.Rows.Count > 0 Then
        '    If dtDetalleCierre.Rows(0)("IDMoneda") <> 1 Then
        '        Importe = Importe * dtDetalleCierre.Rows(0)("Cotizacion")
        '    End If
        'Else
        '    If CSistema.FormatoNumero(txtCotizacion.txt.Text, 0) = 0 Then
        '        txtCotizacion.txt.Text = 1
        '        Importe = Importe * CSistema.FormatoNumero(txtCotizacion.txt.Text, 0)
        '    ElseIf CSistema.FormatoNumero(txtCotizacion.txt.Text, 0) > 0 Then
        '        Importe = Importe * CSistema.FormatoNumero(txtCotizacion.txt.Text, 0)
        '    End If
        'End If

        If cbxTipo.cbx.Text = "CREDITO" Then
            dRow("Credito") = Importe
            dRow("Debito") = 0
        End If

        If cbxTipo.cbx.Text = "DEBITO" Then
            dRow("Credito") = 0
            dRow("Debito") = Importe
        End If

        dRow("Saldo") = Importe

        'dRow("Importe") = Importe
        'If txtDescripcion.GetValue = "" Then
        '    dRow("Observacion") = txtDetalle.GetValue
        'Else
        '    dRow("Observacion") = txtDescripcion.GetValue
        'End If

        'dRow("IDSucursal") = cbxSucursalDetalle.GetValue
        'dRow("TipoComprobante") = txtTipoComprobante.GetValue
        'dRow("NroComprobante") = txtNroComprobante.GetValue

        'dRow("IDUnidadNegocio") = cbxUnidadNegocio.GetValue
        'dRow("IDCentroCosto") = cbxCentroCostoDetalle.GetValue

        'dRow("Fijo") = False


        dtDetalleCierre.Rows.Add(dRow)

        'Listar Detalle
        ListarDetalleCierre(dgv)

        'Caluclar Totales
        CalcularTotales()

        'Inicializar controles
        ocxCuenta.Clear()
        txtImporteLocal.txt.Clear()
        txtDescripcion.txt.Clear()

        ocxCuenta.Focus()

        PosicionarAutomaticamente()

        'Preguntar para salir
        If txtSaldo.ObtenerValor = "0" And dgv.Rows.Count > 0 Then
            If MessageBox.Show("Cerrar el asiento?", "Asiento", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                Guardar()
                Apertura()
            End If
        End If

    End Sub

    Sub CargarItem()

        For Each i As DataRow In dtDetalleCierre.Rows

            Dim oRow As DataRow = dtDetalleCierre.NewRow()
            Dim Registro(3) As String
            Registro(0) = i("Codigo").ToString
            Registro(1) = i("Denominacion").ToString
            Registro(2) = CSistema.FormatoNumero(i("Debito").ToString)
            Registro(3) = CSistema.FormatoMoneda(i("Credito").ToString)

            Try
                dgv.Rows.Add(Registro)
            Catch ex As Exception

            End Try

        Next
        ListarDetalleCierre(dgv)
        'Caluclar Totales
        CalcularTotales()

        ocxCuenta.Focus()

        PosicionarAutomaticamente()

    End Sub

    'Sub ListarDetalle()


    '    'Limpiamos todo el detalle
    '    dgv.Rows.Clear()

    '    'Volver a Ordenar
    '    CData.OrderDataTable(dtDetalleCierre, "Orden")
    '    CAsiento.AgruparDetalle()

    '    'Cargamos registro por registro
    '    For Each oRow As DataRow In dtDetalleCierre.Rows

    '        Dim Registro(3) As String
    '        Registro(0) = oRow("Codigo").ToString
    '        Registro(1) = oRow("Descripcion").ToString
    '        Registro(2) = CSistema.FormatoNumero(oRow("Debito").ToString)
    '        Registro(3) = CSistema.FormatoMoneda(oRow("Credito").ToString)

    '        dgv.Rows.Add(Registro)

    '    Next

    '    CalcularTotales()


    'End Sub

    Sub ListarDetalleCierre(ByVal dgv As DataGridView)

        If dtDetalleCierre Is Nothing Then
            Exit Sub
        End If

        'Limpiamos todo el detalle
        dgv.Rows.Clear()

        'Calcular Moneda
        ' CalcularMoneda()

        'Volver a Ordenar
        'AgruparDetalleCierre()

        For Each i As DataRow In dtDetalleCierre.Rows

            Dim Registro(3) As String
            Registro(0) = i("Codigo").ToString
            Registro(1) = i("Denominacion").ToString
            Registro(2) = CSistema.FormatoNumero(i("Debito").ToString)
            Registro(3) = CSistema.FormatoMoneda(i("Credito").ToString)

            Try
                dgv.Rows.Add(Registro)
            Catch ex As Exception

            End Try
        Next

    End Sub
    Sub SeleccionarItem()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        For Each orow As DataRow In dtDetalleCierre.Select(" Codigo = '" & CodigoSeleccionado & "'") 'And IDCentroCosto= " & IDCentroCosto) ' & " And Observacion = '" & Observacion & "'")

            ocxCuenta.SeleccionarRegistro(orow("Codigo"))

            If orow("Credito") > 0 Then
                cbxTipo.cbx.SelectedIndex = 1
                txtImporteLocal.SetValue(orow("Credito"))
            End If

            If orow("Debito") > 0 Then
                cbxTipo.cbx.SelectedIndex = 0
                txtImporteLocal.SetValue(orow("Debito"))
            End If

        Next

        Actualizando = True

        ocxCuenta.txtCodigo.Focus()

    End Sub

    Sub Apertura()
        Dim IDTransaccion As Integer = 0
        Dim frm As New frmAperturaCuentas
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        frm.Text = "Apertura de Cuentas"
        frm.Año = Año

        'Mostramos
        frm.ShowDialog(Me)
    End Sub

    Private Sub VerDescuentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VerDescuentosToolStripMenuItem.Click
        VerDetalle()
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(dtDetalleCierre)
    End Sub

    Private Sub frmAsiento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAsiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub ocxCuenta_ItemSeleccionado(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxCuenta.ItemSeleccionado
        If ocxCuenta.Seleccionado = True Then

            cbxUnidadNegocio.cbx.SelectedValue = ocxCuenta.IDUnidadNegocio
            cbxCentroCostoDetalle.cbx.SelectedValue = ocxCuenta.IDCentroCosto

            PosicionarAutomaticamente()
            cbxTipo.cbx.Focus()

        End If
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        AgregarItem()
    End Sub

    Private Sub cbxOperacion_Leave(ByVal sender As Object, ByVal e As System.EventArgs)
        ListarTipoComprobante()
    End Sub

    Private Sub cbxOperacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOperacion.PropertyChanged
        ListarTipoComprobante()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
        Apertura()
    End Sub

    Private Sub dgv_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarItem()
        End If

        'If e.KeyCode = Keys.F2 Then
        '    SeleccionarItem()
        'End If
    End Sub

    Private Sub dgv_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged

        ocxCuenta.Clear()
        txtImporteLocal.SetValue(0)
        txtDescripcion.SetValue("")
        txtTipoComprobante.SetValue("")
        txtNroComprobante.SetValue("")
        cbxSucursal.cbx.Text = ""

        Actualizando = False

        CodigoSeleccionado = dgv.CurrentRow.Cells("colCodigo").Value

        'IDUnidadNegocio = dgv.CurrentRow.Cells("ColIDUnidadNegocio").Value
        'IDCentroCosto = dgv.CurrentRow.Cells("ColIDCentroCosto").Value

    End Sub

    Private Sub txtDescripcion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.TeclaPrecionada
        If e.KeyCode = Keys.Add Then
            If txtDescripcion.GetValue.Length <= 1 Then
                txtDescripcion.SetValue(txtDetalle.GetValue)
                CSistema.SelectNextControl(Me, Keys.Enter)
            End If
        End If
    End Sub

    Private Sub txtTipoComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtTipoComprobante.TeclaPrecionada
        'If e.KeyCode = Keys.Add Then
        txtTipoComprobante.SetValue(cbxTipoComprobante.cbx.Text)
        CSistema.SelectNextControl(Me, Keys.Enter)
        txtNroComprobante.Focus()
        'End If
    End Sub

    Private Sub txtNroComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtNroComprobante.TeclaPrecionada
        'If e.KeyCode = Keys.Add Then
        txtNroComprobante.SetValue(txtComprobante.GetValue)
        CSistema.SelectNextControl(Me, Keys.Enter)
        'End If
    End Sub

    Private Sub txtImporteLocal_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteLocal.TeclaPrecionada

        If e.KeyCode = Keys.Multiply Then
            Dim Saldo As Decimal = txtSaldo.ObtenerValor
            If Saldo < 0 Then
                Saldo = Saldo * -1
            End If
            txtImporteLocal.SetValue(Saldo)
            CSistema.SelectNextControl(Me, Keys.Enter)
        End If

    End Sub

    Private Sub txtDescripcion_Leave(sender As System.Object, e As System.EventArgs) Handles txtDescripcion.Leave
        If txtDescripcion.GetValue = "" Then
            txtDescripcion.SetValue(txtDetalle.GetValue)
        End If
    End Sub

    Private Sub cbxUnidadNegocio_PropertyChanged(sender As Object, e As System.EventArgs) Handles cbxUnidadNegocio.PropertyChanged
        Dim IDUnidadNegocio As String = CStr(cbxUnidadNegocio.cbx.SelectedValue)
        If IDUnidadNegocio Is Nothing Then
            IDUnidadNegocio = 0
        End If

        CSistema.SqlToComboBox(cbxCentroCostoDetalle.cbx, CData.GetTable("VCentroCosto", "IDUnidadNegocio=" & CStr(IDUnidadNegocio) & " And Estado='True'").Copy, "ID", "Descripcion")
        If ocxCuenta.IDUnidadNegocio > 0 Then
            cbxCentroCostoDetalle.cbx.SelectedValue = ocxCuenta.IDUnidadNegocio
        End If


    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub


End Class
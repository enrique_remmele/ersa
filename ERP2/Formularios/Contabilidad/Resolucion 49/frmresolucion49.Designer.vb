﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmresolucion49
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmresolucion49))
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NuevoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExpandirNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExpandirTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ContraerNodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContraerTodoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblPadre = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.lblCategoria1 = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.tvCuentas = New System.Windows.Forms.TreeView()
        Me.txtResolucion49 = New System.Windows.Forms.TextBox()
        Me.lbl49 = New System.Windows.Forms.Label()
        Me.cbxResolucion49 = New ERP.ocxCBX()
        Me.txtEstado = New ERP.ocxTXTString()
        Me.txtPadre = New ERP.ocxTXTString()
        Me.txtCategoria = New ERP.ocxTXTNumeric()
        Me.txtTipo = New ERP.ocxTXTString()
        Me.txtCodigo = New ERP.ocxTXTString()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblCuentaContable = New System.Windows.Forms.Label()
        Me.txtCuentaContable = New ERP.ocxTXTString()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoToolStripMenuItem, Me.EditarToolStripMenuItem, Me.EliminarToolStripMenuItem, Me.ToolStripSeparator2, Me.ExpandirNodoToolStripMenuItem, Me.ExpandirTodoToolStripMenuItem, Me.ToolStripSeparator3, Me.ContraerNodoToolStripMenuItem, Me.ContraerTodoToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(152, 170)
        '
        'NuevoToolStripMenuItem
        '
        Me.NuevoToolStripMenuItem.Name = "NuevoToolStripMenuItem"
        Me.NuevoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.NuevoToolStripMenuItem.Text = "Nuevo"
        '
        'EditarToolStripMenuItem
        '
        Me.EditarToolStripMenuItem.Name = "EditarToolStripMenuItem"
        Me.EditarToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.EditarToolStripMenuItem.Text = "Editar"
        '
        'EliminarToolStripMenuItem
        '
        Me.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem"
        Me.EliminarToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.EliminarToolStripMenuItem.Text = "Eliminar"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(148, 6)
        '
        'ExpandirNodoToolStripMenuItem
        '
        Me.ExpandirNodoToolStripMenuItem.Name = "ExpandirNodoToolStripMenuItem"
        Me.ExpandirNodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExpandirNodoToolStripMenuItem.Text = "Expandir nodo"
        '
        'ExpandirTodoToolStripMenuItem
        '
        Me.ExpandirTodoToolStripMenuItem.Name = "ExpandirTodoToolStripMenuItem"
        Me.ExpandirTodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ExpandirTodoToolStripMenuItem.Text = "Expandir todo"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(148, 6)
        '
        'ContraerNodoToolStripMenuItem
        '
        Me.ContraerNodoToolStripMenuItem.Name = "ContraerNodoToolStripMenuItem"
        Me.ContraerNodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ContraerNodoToolStripMenuItem.Text = "Contraer nodo"
        '
        'ContraerTodoToolStripMenuItem
        '
        Me.ContraerTodoToolStripMenuItem.Name = "ContraerTodoToolStripMenuItem"
        Me.ContraerTodoToolStripMenuItem.Size = New System.Drawing.Size(151, 22)
        Me.ContraerTodoToolStripMenuItem.Text = "Contraer todo"
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "icon.folder.close.gif")
        Me.ImageList101.Images.SetKeyName(1, "icon.folder.open.gif")
        Me.ImageList101.Images.SetKeyName(2, "form_specific_completed.gif")
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 326)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(786, 22)
        Me.StatusStrip1.TabIndex = 44
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(608, 278)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 62
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(527, 278)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 61
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(446, 278)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 60
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(443, 45)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 46
        Me.lblID.Text = "ID:"
        '
        'lblPadre
        '
        Me.lblPadre.AutoSize = True
        Me.lblPadre.Location = New System.Drawing.Point(443, 151)
        Me.lblPadre.Name = "lblPadre"
        Me.lblPadre.Size = New System.Drawing.Size(38, 13)
        Me.lblPadre.TabIndex = 54
        Me.lblPadre.Text = "Padre:"
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        Me.txtID.Location = New System.Drawing.Point(532, 41)
        Me.txtID.Name = "txtID"
        Me.txtID.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(43, 20)
        Me.txtID.TabIndex = 47
        Me.txtID.Text = "0"
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCategoria1
        '
        Me.lblCategoria1.AutoSize = True
        Me.lblCategoria1.Location = New System.Drawing.Point(443, 123)
        Me.lblCategoria1.Name = "lblCategoria1"
        Me.lblCategoria1.Size = New System.Drawing.Size(55, 13)
        Me.lblCategoria1.TabIndex = 52
        Me.lblCategoria1.Text = "Categoria:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(443, 96)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 50
        Me.lblDescripcion.Text = "Descripción:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(443, 207)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 57
        Me.lblEstado.Text = "Estado:"
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(443, 71)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCodigo.TabIndex = 48
        Me.lblCodigo.Text = "Codigo:"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(443, 178)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 56
        Me.lblTipo.Text = "Tipo:"
        '
        'tvCuentas
        '
        Me.tvCuentas.AllowDrop = True
        Me.tvCuentas.ContextMenuStrip = Me.ContextMenuStrip1
        Me.tvCuentas.FullRowSelect = True
        Me.tvCuentas.HideSelection = False
        Me.tvCuentas.ImageIndex = 0
        Me.tvCuentas.ImageList = Me.ImageList101
        Me.tvCuentas.Location = New System.Drawing.Point(12, 53)
        Me.tvCuentas.Name = "tvCuentas"
        Me.tvCuentas.SelectedImageIndex = 0
        Me.tvCuentas.Size = New System.Drawing.Size(419, 270)
        Me.tvCuentas.TabIndex = 63
        '
        'txtResolucion49
        '
        Me.txtResolucion49.BackColor = System.Drawing.Color.White
        Me.txtResolucion49.Location = New System.Drawing.Point(532, 15)
        Me.txtResolucion49.Name = "txtResolucion49"
        Me.txtResolucion49.ReadOnly = True
        Me.txtResolucion49.Size = New System.Drawing.Size(237, 20)
        Me.txtResolucion49.TabIndex = 65
        '
        'lbl49
        '
        Me.lbl49.AutoSize = True
        Me.lbl49.Location = New System.Drawing.Point(443, 18)
        Me.lbl49.Name = "lbl49"
        Me.lbl49.Size = New System.Drawing.Size(41, 13)
        Me.lbl49.TabIndex = 66
        Me.lbl49.Text = "RG 49:"
        '
        'cbxResolucion49
        '
        Me.cbxResolucion49.CampoWhere = Nothing
        Me.cbxResolucion49.CargarUnaSolaVez = False
        Me.cbxResolucion49.DataDisplayMember = Nothing
        Me.cbxResolucion49.DataFilter = Nothing
        Me.cbxResolucion49.DataOrderBy = Nothing
        Me.cbxResolucion49.DataSource = Nothing
        Me.cbxResolucion49.DataValueMember = Nothing
        Me.cbxResolucion49.dtSeleccionado = Nothing
        Me.cbxResolucion49.FormABM = Nothing
        Me.cbxResolucion49.Indicaciones = Nothing
        Me.cbxResolucion49.Location = New System.Drawing.Point(181, 13)
        Me.cbxResolucion49.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxResolucion49.Name = "cbxResolucion49"
        Me.cbxResolucion49.SeleccionMultiple = False
        Me.cbxResolucion49.SeleccionObligatoria = False
        Me.cbxResolucion49.Size = New System.Drawing.Size(189, 21)
        Me.cbxResolucion49.SoloLectura = False
        Me.cbxResolucion49.TabIndex = 64
        Me.cbxResolucion49.Texto = ""
        '
        'txtEstado
        '
        Me.txtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtEstado.Color = System.Drawing.Color.Empty
        Me.txtEstado.Indicaciones = Nothing
        Me.txtEstado.Location = New System.Drawing.Point(532, 207)
        Me.txtEstado.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEstado.Multilinea = False
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(107, 21)
        Me.txtEstado.SoloLectura = True
        Me.txtEstado.TabIndex = 59
        Me.txtEstado.TabStop = False
        Me.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEstado.Texto = ""
        '
        'txtPadre
        '
        Me.txtPadre.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPadre.Color = System.Drawing.Color.Empty
        Me.txtPadre.Indicaciones = Nothing
        Me.txtPadre.Location = New System.Drawing.Point(532, 151)
        Me.txtPadre.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPadre.Multilinea = False
        Me.txtPadre.Name = "txtPadre"
        Me.txtPadre.Size = New System.Drawing.Size(237, 21)
        Me.txtPadre.SoloLectura = True
        Me.txtPadre.TabIndex = 55
        Me.txtPadre.TabStop = False
        Me.txtPadre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPadre.Texto = ""
        '
        'txtCategoria
        '
        Me.txtCategoria.Color = System.Drawing.Color.Empty
        Me.txtCategoria.Decimales = True
        Me.txtCategoria.Indicaciones = Nothing
        Me.txtCategoria.Location = New System.Drawing.Point(532, 123)
        Me.txtCategoria.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCategoria.Name = "txtCategoria"
        Me.txtCategoria.Size = New System.Drawing.Size(43, 22)
        Me.txtCategoria.SoloLectura = True
        Me.txtCategoria.TabIndex = 53
        Me.txtCategoria.TabStop = False
        Me.txtCategoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCategoria.Texto = "0"
        '
        'txtTipo
        '
        Me.txtTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtTipo.Color = System.Drawing.Color.Empty
        Me.txtTipo.Indicaciones = Nothing
        Me.txtTipo.Location = New System.Drawing.Point(532, 178)
        Me.txtTipo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtTipo.Multilinea = False
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.Size = New System.Drawing.Size(107, 21)
        Me.txtTipo.SoloLectura = True
        Me.txtTipo.TabIndex = 58
        Me.txtTipo.TabStop = False
        Me.txtTipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipo.Texto = ""
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Color = System.Drawing.Color.Empty
        Me.txtCodigo.Indicaciones = Nothing
        Me.txtCodigo.Location = New System.Drawing.Point(532, 67)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigo.Multilinea = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(107, 21)
        Me.txtCodigo.SoloLectura = True
        Me.txtCodigo.TabIndex = 49
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigo.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(532, 96)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(237, 21)
        Me.txtDescripcion.SoloLectura = True
        Me.txtDescripcion.TabIndex = 51
        Me.txtDescripcion.TabStop = False
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblCuentaContable
        '
        Me.lblCuentaContable.AutoSize = True
        Me.lblCuentaContable.Location = New System.Drawing.Point(443, 236)
        Me.lblCuentaContable.Name = "lblCuentaContable"
        Me.lblCuentaContable.Size = New System.Drawing.Size(89, 13)
        Me.lblCuentaContable.TabIndex = 67
        Me.lblCuentaContable.Text = "Cuenta Contable:"
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCuentaContable.Color = System.Drawing.Color.Empty
        Me.txtCuentaContable.Indicaciones = Nothing
        Me.txtCuentaContable.Location = New System.Drawing.Point(532, 236)
        Me.txtCuentaContable.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCuentaContable.Multilinea = False
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Size = New System.Drawing.Size(107, 21)
        Me.txtCuentaContable.SoloLectura = True
        Me.txtCuentaContable.TabIndex = 68
        Me.txtCuentaContable.TabStop = False
        Me.txtCuentaContable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCuentaContable.Texto = ""
        '
        'frmresolucion49
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(786, 348)
        Me.Controls.Add(Me.txtCuentaContable)
        Me.Controls.Add(Me.lblCuentaContable)
        Me.Controls.Add(Me.lbl49)
        Me.Controls.Add(Me.txtResolucion49)
        Me.Controls.Add(Me.cbxResolucion49)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.txtEstado)
        Me.Controls.Add(Me.txtPadre)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.txtCategoria)
        Me.Controls.Add(Me.txtTipo)
        Me.Controls.Add(Me.lblPadre)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblCategoria1)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblCodigo)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.tvCuentas)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmresolucion49"
        Me.Tag = "frmResolucion49"
        Me.Text = "frmresolucion49"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents NuevoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EliminarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ExpandirNodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExpandirTodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ContraerNodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContraerTodoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ImageList101 As ImageList
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnEditar As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents txtEstado As ocxTXTString
    Friend WithEvents txtPadre As ocxTXTString
    Friend WithEvents lblID As Label
    Friend WithEvents txtCategoria As ocxTXTNumeric
    Friend WithEvents txtTipo As ocxTXTString
    Friend WithEvents lblPadre As Label
    Friend WithEvents txtID As TextBox
    Friend WithEvents lblCategoria1 As Label
    Friend WithEvents lblDescripcion As Label
    Friend WithEvents txtCodigo As ocxTXTString
    Friend WithEvents lblEstado As Label
    Friend WithEvents txtDescripcion As ocxTXTString
    Friend WithEvents lblCodigo As Label
    Friend WithEvents lblTipo As Label
    Friend WithEvents tvCuentas As TreeView
    Friend WithEvents cbxResolucion49 As ocxCBX
    Friend WithEvents lbl49 As Label
    Friend WithEvents txtResolucion49 As TextBox
    Friend WithEvents txtCuentaContable As ocxTXTString
    Friend WithEvents lblCuentaContable As Label
End Class

﻿Imports ERP.Reporte
Public Class frmresolucion49

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData
    Dim CReporte As New CReporteContabilidad

    'PROPIEDADES

    'EVENTOS

    'VARIABLES
    Dim dt As New DataTable
    Dim vTabla As String = "VCuentaContableRG49ABM"
    Private DragDropTreeView As Boolean
    Private NodoOrigen As TreeNode

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    'FUNCIONES
    Sub Inicializar()

        'Controles

        'Propiedades

        'Otros

        'Funciones
        CargarInformacion()
        ListarPlanCuentaResolucion49()

        'Foco

    End Sub

    Sub CargarInformacion()

        'Resolucion49
        CSistema.SqlToComboBox(cbxResolucion49.cbx, "Select ID, Descripcion From PlanCuentaRG49")
        cbxResolucion49.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Text.ToString, "RESOLUCION 49", "")
        cbxResolucion49.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Sub GuardarInformacion()

        'Plan de Cuentas
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Text.ToString, "RESOLUCION 49", cbxResolucion49.cbx.Text)

    End Sub

    Sub ListarPlanCuentaResolucion49()

        If IsNumeric(cbxResolucion49.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxResolucion49.cbx.SelectedValue = 0 Then
            Exit Sub
        End If

        'Limpiamos todo
        tvCuentas.Nodes.Clear()

        'Crear el primer nodo
        Dim Nodo1 As TreeNode = New TreeNode()
        Nodo1.Name = cbxResolucion49.cbx.SelectedValue
        Nodo1.Text = cbxResolucion49.cbx.Text & "                     "
        Nodo1.NodeFont = New Font(Me.Font.Name, Me.Font.SizeInPoints, FontStyle.Bold, GraphicsUnit.Point)

        tvCuentas.Nodes.Add(Nodo1)

        'Cargamos 
        'dt = CSistema.ExecuteToDataTable("Select * From VCuentaContableResolucion49 Where IDPlanCuentaResolucion49=" & cbxResolucion49.cbx.SelectedValue & " Order By Categoria, Codigo")
        dt = CData.GetTable(vTabla, "IDPlanCuentaRG49=" & cbxResolucion49.cbx.SelectedValue & "")
        CSistema.OrderDataTable(dt, "Categoria, Codigo")

        ListarPlanCuentaResolucion49(0, Nothing)

    End Sub

    Sub AdministrarPlanCuentas()

        Dim frm As New frmresolucion49
        FGMostrarFormulario(Me, frm, "Resolucion 49", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

    End Sub

    Sub Nuevo()

        Dim frm As New frmAdministrarResolucion49
        frm.Nuevo = True
        frm.btnEliminar.Enabled = False
        frm.ID = 0

        frm.IDPlanCuentaRG49 = tvCuentas.Nodes(0).Name
        frm.CuentaContableRG49 = tvCuentas.Nodes(0).Text

        Dim Categoria As Integer
        Dim Tipo As String
        Categoria = tvCuentas.SelectedNode.Level + 1
        Tipo = "IMPUTABLE"


        frm.Categoria = Categoria
        frm.Tipo = Tipo

        If tvCuentas.SelectedNode.Level > 0 Then

            frm.IDPadre = tvCuentas.SelectedNode.Name
            frm.Padre = tvCuentas.SelectedNode.Text
            frm.CodigoPadre = dt.Select(" ID = " & tvCuentas.SelectedNode.Name)(0)("Codigo").ToString

        Else
            frm.IDPadre = 0
            frm.Padre = "---"
        End If

        frm.ShowDialog(Me)

        If frm.Procesado = True Then
            AgregarNodo(frm.ID)
        End If

    End Sub

    Sub Editar()

        If tvCuentas.SelectedNode.Level = 0 Then
            Exit Sub
        End If

        Dim frm As New frmAdministrarResolucion49
        frm.Nuevo = False
        frm.btnEliminar.Enabled = False
        frm.ID = tvCuentas.SelectedNode.Name

        'Plan de Cuentas
        frm.IDPlanCuentaRG49 = tvCuentas.Nodes(0).Name
        frm.CuentaContableRG49 = tvCuentas.Nodes(0).Text

        'Categorias y Tipos
        Dim Categoria As Integer
        Dim Tipo As String
        Categoria = tvCuentas.SelectedNode.Level + 1
        If tvCuentas.SelectedNode.Nodes.Count > 0 Then
            Tipo = "TOTALIZADOR"
        Else
            Tipo = "IMPUTABLE"
        End If

        frm.Categoria = Categoria
        frm.Tipo = Tipo

        frm.IDPadre = tvCuentas.SelectedNode.Parent.Name
        frm.Padre = tvCuentas.SelectedNode.Parent.Text

        frm.ShowDialog(Me)

        If frm.Procesado = True Then
            ModificarNodo(tvCuentas.SelectedNode.Name)
        End If

    End Sub

    Sub Eliminar()

        If tvCuentas.SelectedNode.Level = 0 Then
            Exit Sub
        End If

        Dim frm As New frmAdministrarResolucion49
        frm.Nuevo = False
        frm.btnGuardar.Enabled = False
        frm.ID = tvCuentas.SelectedNode.Name

        'Resolucion 49
        frm.IDPlanCuentaRG49 = tvCuentas.Nodes(0).Name
        frm.CuentaContableRG49 = tvCuentas.Nodes(0).Text

        'Categorias y Tipos
        Dim Categoria As Integer
        Dim Tipo As String
        Categoria = tvCuentas.SelectedNode.Level + 1
        If tvCuentas.SelectedNode.Nodes.Count > 0 Then
            Tipo = "TOTALIZADOR"
        Else
            Tipo = "IMPUTABLE"
        End If

        frm.Categoria = Categoria
        frm.Tipo = Tipo

        frm.IDPadre = tvCuentas.SelectedNode.Parent.Name
        frm.Padre = tvCuentas.SelectedNode.Parent.Text

        frm.ShowDialog(Me)

        If frm.Procesado = True Then
            tvCuentas.Nodes.Remove(tvCuentas.SelectedNode)

            If tvCuentas.SelectedNode.Nodes.Count = 0 Then
                For Each oRow As DataRow In dt.Select("ID=" & tvCuentas.SelectedNode.Name)
                    oRow("Tipo") = "IMPUTABLE"
                    tvCuentas.SelectedNode.ImageIndex = 2
                    tvCuentas.SelectedNode.SelectedImageIndex = 2
                    SeleccionarItem(tvCuentas.SelectedNode)
                Next
            End If

        End If


    End Sub

    Sub SeleccionarItem(ByVal e As TreeNode)

        txtID.Clear()
        txtCodigo.txt.Clear()
        'txtAlias.txt.Clear()
        txtDescripcion.txt.Clear()
        txtCategoria.txt.Clear()
        txtPadre.txt.Clear()
        txtTipo.txt.Clear()
        txtEstado.txt.Clear()
        txtResolucion49.Clear()
        'txtUnidadNegocio.Clear()
        'txtCentroCosto.Clear()

        If dt.Select(" ID = " & e.Name).Length = 0 Then
            Exit Sub
        End If

        Try

            Dim oRow As DataRow = dt.Select(" ID = " & e.Name)(0)

            txtResolucion49.Text = oRow("PlanCuenta").ToString
            txtID.Text = oRow("ID").ToString
            txtCodigo.txt.Text = oRow("Codigo").ToString
            'txtAlias.txt.Text = oRow("Alias").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString
            txtCategoria.txt.Text = oRow("Categoria").ToString
            txtPadre.txt.Text = oRow("Padre").ToString
            txtTipo.txt.Text = oRow("Tipo").ToString
            'txtUnidadNegocio.txt.Text = oRow("UnidadNegocio").ToString
            'txtCentroCosto.txt.Text = oRow("CentroCosto").ToString
            txtEstado.txt.Text = oRow("Estado").ToString
            'txtCuentaContable.txt.Text = oRow("DescripcionCC").ToString

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try


    End Sub

    Sub ModificarNodo(ByVal ID As Integer)

        'Dim newRow As DataRow = CSistema.ExecuteToDataTable("Select * From VCuentaContable Where ID=" & ID & " ")(0)

        'For Each oRow As DataRow In dt.Select("ID=" & ID)
        '    For i As Integer = 0 To dt.Columns.Count - 1
        '        oRow(i) = newRow(i).ToString
        '    Next
        'Next

        Dim oRow As DataRow = CData.GetTable(vTabla, " ID = " & ID)(0)
        tvCuentas.SelectedNode.Text = oRow("Cuenta")
        SeleccionarItem(tvCuentas.SelectedNode)

        'Mover si el padre cambio
        Dim IDPadreActual As Integer = oRow("IDPadre")
        Dim IDPadreAnterior As Integer = tvCuentas.SelectedNode.Parent.Name

        If IDPadreActual = 0 Then

        Else
            If IDPadreActual <> IDPadreAnterior Then

                Dim node1 As TreeNode = tvCuentas.SelectedNode
                Dim nodeParent As TreeNode = GetIndexNode(IDPadreActual)

                'Colocar el nodo con el padre actual
                tvCuentas.SelectedNode.Remove()
                nodeParent.Nodes.Add(node1)
                tvCuentas.SelectedNode = node1

            End If

        End If



    End Sub

    Function GetIndexNode(ByVal ID As String) As TreeNode

        GetIndexNode = Nothing
        For Each node As TreeNode In tvCuentas.Nodes

            If node.Name = ID Then
                Return node
            Else
                If node.Nodes.Count > 0 Then
                    GetIndexNode = GetIndexNode(ID, node)
                End If
            End If

            If Not GetIndexNode Is Nothing Then
                Exit For
            End If

        Next

    End Function

    Function GetIndexNode(ByVal ID As String, ByVal node1 As TreeNode) As TreeNode

        GetIndexNode = Nothing
        For Each node As TreeNode In node1.Nodes
            If node.Name = ID Then
                Return node
            Else
                If node.Nodes.Count > 0 Then
                    GetIndexNode = GetIndexNode(ID, node)
                End If
            End If

            If Not GetIndexNode Is Nothing Then
                Exit For
            End If

        Next

    End Function

    Sub AgregarNodo(ByVal ID As Integer)

        'Dim newRow As DataRow = CSistema.ExecuteToDataTable("Select * From VCuentaContable Where ID=" & ID & " ")(0)
        'dt.ImportRow(newRow)

        'Dim nodo As New TreeNode
        'nodo.Name = ID
        'nodo.Text = newRow("Cuenta").ToString
        'nodo.SelectedImageIndex = 2
        'nodo.ImageIndex = 2
        'tvCuentas.SelectedNode.Nodes.Add(nodo)
        'tvCuentas.SelectedNode = nodo
        'SeleccionarItem(nodo)

        ''Poner al padre como totalizador y cambiar su icono
        'For Each oRow As DataRow In dt.Select("ID=" & tvCuentas.SelectedNode.Parent.Name)
        '    oRow("Tipo") = "TOTALIZADOR"
        'Next

        'tvCuentas.SelectedNode.Parent.ImageIndex = 0
        'tvCuentas.SelectedNode.Parent.SelectedImageIndex = 0

    End Sub

    Sub SeleccionarUltimoItemSeleccionado()
        Dim Index As Integer = tvCuentas.SelectedNode.Name
    End Sub

    Sub Listar()
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ImprimirPlanCuenta(frm, vgUsuarioIdentificador, cbxResolucion49.GetValue)

    End Sub

    Private Sub ListarPlanCuentaResolucion49(ByVal indicePadre As Integer, ByVal nodePadre As TreeNode)

        Dim dataViewHijos As DataView

        ' Crear un DataView con los Nodos que dependen del Nodo padre pasado como parámetro.
        dataViewHijos = New DataView(dt)

        dataViewHijos.RowFilter = dt.Columns("IDPadre").ColumnName + " = " + indicePadre.ToString()

        If dataViewHijos.Count = 0 Then
            If nodePadre Is Nothing Then

            Else
                nodePadre.ImageIndex = 2
                nodePadre.SelectedImageIndex = 2
            End If

        End If

        ' Agregar al TreeView los nodos Hijos que se han obtenido en el DataView.
        For Each dataRowCurrent As DataRowView In dataViewHijos

            Dim nuevoNodo As New TreeNode
            nuevoNodo.Text = dataRowCurrent("Cuenta").ToString().Trim()
            nuevoNodo.Name = dataRowCurrent("ID").ToString().Trim()
            nuevoNodo.ImageIndex = 0
            nuevoNodo.SelectedImageIndex = 0

            ' si el parámetro nodoPadre es nulo es porque es la primera llamada, son los Nodos
            ' del primer nivel que no dependen de otro nodo.
            If nodePadre Is Nothing Then
                'tvCuentas.Nodes.Add(nuevoNodo)
                tvCuentas.Nodes(0).Nodes.Add(nuevoNodo)
            Else
                ' se añade el nuevo nodo al nodo padre.
                nodePadre.Nodes.Add(nuevoNodo)
            End If

            ' Llamada recurrente al mismo método para agregar los Hijos del Nodo recién agregado.
            ListarPlanCuentaResolucion49(Int32.Parse(dataRowCurrent("ID").ToString()), nuevoNodo)

        Next dataRowCurrent

    End Sub

    'Private Sub frmCuentaContable_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
    '    GuardarInformacion()
    'End Sub

    'Private Sub frmCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Inicializar()
    'End Sub

    Private Sub btnNuevo_Click_1(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click_1(sender As Object, e As EventArgs) Handles btnEditar.Click
        Editar()
    End Sub


    Private Sub btnEliminar_Click_1(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub cbxResolucion49_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxResolucion49.PropertyChanged
        ListarPlanCuentaResolucion49()
    End Sub

    Private Sub NuevoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoToolStripMenuItem.Click
        Nuevo()
    End Sub

    Private Sub EditarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditarToolStripMenuItem.Click
        Editar()
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        Eliminar()
    End Sub

    Private Sub tvCuentas_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tvCuentas.AfterSelect
        SeleccionarItem(e.Node)
    End Sub


    'Private Sub tvCuentas_NodeMouseClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles tvCuentas.NodeMouseClick
    'If e.Button = Windows.Forms.MouseButtons.Right Then

    '    ' Referenciamos el control
    '    Dim tv As Windows.Forms.TreeView = DirectCast(sender, Windows.Forms.TreeView)

    '    ' Seleccionamos el nodo
    '    tv.SelectedNode = e.Node

    'End If
    'End Sub

    'Private Sub tvCuentas_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs)

    'Dim Left As String = e.Button.ToString
    '' pregunto si el botón que estoy pulsando es el izquierdo para poder arrastrar el nodo
    'If Left = MouseButtons.Left.ToString Then
    '    ' señalo que se está haciendo un Drag and Drop dentro del TreeView
    '    DragDropTreeView = True

    '    ' obtengo el arbol del control TreeView1
    '    Dim tree As TreeView = CType(sender, TreeView)

    '    ' recupero el nodo debajo del mouse.
    '    Dim node As TreeNode
    '    node = tree.GetNodeAt(e.X, e.Y)

    '    ' establezco el nodo del árbol seleccionado actualmente en el control TreeView
    '    tree.SelectedNode = node

    '    ' guardo los datos del origen del nodo
    '    NodoOrigen = CType(node, TreeNode)

    '    ' inicio la operación Drag and Drop con una copia clonada del nodo.
    '    If Not node Is Nothing Then
    '        tree.DoDragDrop(node.Clone(), DragDropEffects.Copy)
    '    End If
    'End If

    'End Sub

    'Private Sub tvCuentas_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs)
    'If DragDropTreeView = True Then
    '    ' determino si los datos almacenados en la instancia están asociados al formato especificado del TreeView
    '    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", False) Then
    '        ' variable que sirve para guardar el valor de un punto en coordenadas X e Y
    '        Dim pt As Point
    '        ' variable que sirve para guardar el valor del nodo de destino
    '        Dim DestinationNode As TreeNode

    '        ' uso PointToClient para calcular la ubicación del mouse sobre el control TreeView
    '        pt = CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
    '        ' uso este punto para recuperar el nodo de destino dentro del árbol del control TreeView.
    '        DestinationNode = CType(sender, TreeView).GetNodeAt(pt)
    '        ' verifico que el nodo de destino sea distinto al nodo de origen
    '        If DestinationNode.FullPath <> NodoOrigen.FullPath Then
    '            DestinationNode.Nodes.Add(CType(NodoOrigen.Clone, TreeNode))
    '            ' expando el nodo padre donde agregue el nuevo nodo. Sin esto, solo aparecería el signo +.
    '            DestinationNode.Expand()
    '            ' elimino el nodo de origen dentro del árbol
    '            Dim nodoPadre As TreeNode = DestinationNode

    '            'Guardar configuracion
    '            Dim IDPadre As Integer = nodoPadre.Name
    '            Dim ID As Integer = NodoOrigen.Name

    '            Dim SQL As String = "UPDATE CuentaContable Set IDPadre=" & IDPadre & " WHERE ID = " & ID
    '            CSistema.ExecuteScalar(SQL)

    '            'Actualizar CData
    '            CData.Actualizar(ID, vTabla)

    '            NodoOrigen.Remove()


    '        End If
    '    End If
    'End If
    'End Sub

    'Private Sub tvCuentas_DragOver(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs)
    ' Verifica si dentro del TreeView se está arrastrando
    'If DragDropTreeView Then

    '    ' deshabilita la actualización en pantalla del control TreeView 
    '    tvCuentas.BeginUpdate()

    '    ' obtengo el árbol
    '    Dim tree As TreeView = CType(sender, TreeView)

    '    ' establezco el efecto de la operación Drag and Drop
    '    e.Effect = DragDropEffects.None

    '    ' pregunto por si el formato es válido?
    '    If Not e.Data.GetData(GetType(TreeNode)) Is Nothing Then

    '        ' Obtengo el punto en la pantalla.
    '        Dim pt As New Point(e.X, e.Y)

    '        ' Convierto a un punto en el sistema de coordenadas del control TreeView
    '        pt = tree.PointToClient(pt)

    '        ' pregunto si el mouse está sobre un nodo válido
    '        Dim node As TreeNode = tree.GetNodeAt(pt)
    '        If Not node Is Nothing Then
    '            ' establezco el efecto de la operación Drag and Drop
    '            e.Effect = DragDropEffects.Copy
    '            ' establezco el nodo del árbol seleccionado actualmente en el control TreeView
    '            tree.SelectedNode = node

    '        End If

    '    End If
    '    ' habilita la actualización en pantalla del control TreeView
    '    tvCuentas.EndUpdate()
    'End If
    'End Sub

    'Private Sub tvCuentas_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
    ' señalo que no está haciendo un Drag and Drop dentro del TreeView
    'DragDropTreeView = False
    'End Sub

    'Private Sub tvCuentas_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs)
    ' le asigno la imagen con la carpeta abierta
    ' tvCuentas.SelectedImageIndex = 1 -> SE COMENTO PORQUE DA ERROR DE MEMORIA PROTEGIDA ROMPE BOLA. NO ELIMINE PARA SABER QUE NO FUNCIONA
    'End Sub

    'Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
    '    AdministrarPlanCuentas()
    'End Sub

    'Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
    '    Listar()
    'End Sub

    'Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
    '    Dim frm As New frmExportarPlanCuentaSaldo
    '    frm.ShowDialog()
    'End Sub

    'FA 28/05/2021
    Sub frmCuentaContable_Activate()
        Me.Refresh()
    End Sub

    Private Sub frmresolucion49_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmresolucion49_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAdministrarResolucion49
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtTipo = New System.Windows.Forms.TextBox()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblPadre = New System.Windows.Forms.Label()
        Me.lblCategoria = New System.Windows.Forms.Label()
        Me.lblCodigo = New System.Windows.Forms.Label()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.lblID = New System.Windows.Forms.Label()
        Me.txtResolucion49 = New System.Windows.Forms.TextBox()
        Me.lblCuentaContable = New System.Windows.Forms.Label()
        Me.txtDefinicion = New System.Windows.Forms.TextBox()
        Me.cbxPadre = New ERP.ocxCBX()
        Me.txtCategoria = New ERP.ocxTXTNumeric()
        Me.txtCodigo = New ERP.ocxTXTString()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.txtCuentaContable = New ERP.ocxTXTNumeric()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtTipo
        '
        Me.txtTipo.BackColor = System.Drawing.Color.White
        Me.txtTipo.Location = New System.Drawing.Point(174, 146)
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.ReadOnly = True
        Me.txtTipo.Size = New System.Drawing.Size(208, 20)
        Me.txtTipo.TabIndex = 39
        Me.txtTipo.TabStop = False
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(137, 150)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 38
        Me.lblTipo.Text = "Tipo:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(226, 265)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 44
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 323)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(420, 22)
        Me.StatusStrip1.TabIndex = 47
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(307, 265)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 46
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(145, 265)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 43
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lblPadre
        '
        Me.lblPadre.AutoSize = True
        Me.lblPadre.Location = New System.Drawing.Point(21, 67)
        Me.lblPadre.Name = "lblPadre"
        Me.lblPadre.Size = New System.Drawing.Size(38, 13)
        Me.lblPadre.TabIndex = 31
        Me.lblPadre.Text = "Padre:"
        '
        'lblCategoria
        '
        Me.lblCategoria.AutoSize = True
        Me.lblCategoria.Location = New System.Drawing.Point(21, 150)
        Me.lblCategoria.Name = "lblCategoria"
        Me.lblCategoria.Size = New System.Drawing.Size(55, 13)
        Me.lblCategoria.TabIndex = 36
        Me.lblCategoria.Text = "Categoria:"
        '
        'lblCodigo
        '
        Me.lblCodigo.AutoSize = True
        Me.lblCodigo.Location = New System.Drawing.Point(21, 94)
        Me.lblCodigo.Name = "lblCodigo"
        Me.lblCodigo.Size = New System.Drawing.Size(43, 13)
        Me.lblCodigo.TabIndex = 32
        Me.lblCodigo.Text = "Codigo:"
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(149, 178)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 42
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Checked = True
        Me.rdbActivo.Location = New System.Drawing.Point(88, 178)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 41
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(21, 180)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 40
        Me.lblEstado.Text = "Estado:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(21, 121)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 34
        Me.lblDescripcion.Text = "Descripción:"
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        Me.txtID.Location = New System.Drawing.Point(88, 38)
        Me.txtID.Name = "txtID"
        Me.txtID.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(61, 20)
        Me.txtID.TabIndex = 30
        Me.txtID.TabStop = False
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(21, 42)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 29
        Me.lblID.Text = "ID:"
        '
        'txtResolucion49
        '
        Me.txtResolucion49.BackColor = System.Drawing.Color.White
        Me.txtResolucion49.Location = New System.Drawing.Point(88, 12)
        Me.txtResolucion49.Name = "txtResolucion49"
        Me.txtResolucion49.ReadOnly = True
        Me.txtResolucion49.Size = New System.Drawing.Size(142, 20)
        Me.txtResolucion49.TabIndex = 48
        Me.txtResolucion49.TabStop = False
        '
        'lblCuentaContable
        '
        Me.lblCuentaContable.AutoSize = True
        Me.lblCuentaContable.Location = New System.Drawing.Point(21, 212)
        Me.lblCuentaContable.Name = "lblCuentaContable"
        Me.lblCuentaContable.Size = New System.Drawing.Size(89, 13)
        Me.lblCuentaContable.TabIndex = 49
        Me.lblCuentaContable.Text = "Cuenta Contable:"
        '
        'txtDefinicion
        '
        Me.txtDefinicion.BackColor = System.Drawing.Color.White
        Me.txtDefinicion.Location = New System.Drawing.Point(256, 212)
        Me.txtDefinicion.Name = "txtDefinicion"
        Me.txtDefinicion.ReadOnly = True
        Me.txtDefinicion.Size = New System.Drawing.Size(126, 20)
        Me.txtDefinicion.TabIndex = 51
        Me.txtDefinicion.TabStop = False
        '
        'cbxPadre
        '
        Me.cbxPadre.CampoWhere = Nothing
        Me.cbxPadre.CargarUnaSolaVez = False
        Me.cbxPadre.DataDisplayMember = Nothing
        Me.cbxPadre.DataFilter = Nothing
        Me.cbxPadre.DataOrderBy = Nothing
        Me.cbxPadre.DataSource = Nothing
        Me.cbxPadre.DataValueMember = Nothing
        Me.cbxPadre.dtSeleccionado = Nothing
        Me.cbxPadre.FormABM = Nothing
        Me.cbxPadre.Indicaciones = Nothing
        Me.cbxPadre.Location = New System.Drawing.Point(88, 64)
        Me.cbxPadre.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxPadre.Name = "cbxPadre"
        Me.cbxPadre.SeleccionMultiple = False
        Me.cbxPadre.SeleccionObligatoria = False
        Me.cbxPadre.Size = New System.Drawing.Size(294, 21)
        Me.cbxPadre.SoloLectura = False
        Me.cbxPadre.TabIndex = 45
        Me.cbxPadre.Texto = ""
        '
        'txtCategoria
        '
        Me.txtCategoria.Color = System.Drawing.Color.Empty
        Me.txtCategoria.Decimales = True
        Me.txtCategoria.Indicaciones = Nothing
        Me.txtCategoria.Location = New System.Drawing.Point(88, 145)
        Me.txtCategoria.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCategoria.Name = "txtCategoria"
        Me.txtCategoria.Size = New System.Drawing.Size(43, 22)
        Me.txtCategoria.SoloLectura = False
        Me.txtCategoria.TabIndex = 37
        Me.txtCategoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCategoria.Texto = "0"
        '
        'txtCodigo
        '
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCodigo.Color = System.Drawing.Color.Empty
        Me.txtCodigo.Indicaciones = Nothing
        Me.txtCodigo.Location = New System.Drawing.Point(88, 91)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCodigo.Multilinea = False
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(137, 21)
        Me.txtCodigo.SoloLectura = False
        Me.txtCodigo.TabIndex = 33
        Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigo.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(88, 118)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(294, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 35
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.Color = System.Drawing.Color.Empty
        Me.txtCuentaContable.Decimales = True
        Me.txtCuentaContable.Indicaciones = Nothing
        Me.txtCuentaContable.Location = New System.Drawing.Point(125, 212)
        Me.txtCuentaContable.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Size = New System.Drawing.Size(109, 22)
        Me.txtCuentaContable.SoloLectura = False
        Me.txtCuentaContable.TabIndex = 52
        Me.txtCuentaContable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuentaContable.Texto = "0"
        '
        'frmAdministrarResolucion49
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(420, 345)
        Me.Controls.Add(Me.txtCuentaContable)
        Me.Controls.Add(Me.txtDefinicion)
        Me.Controls.Add(Me.lblCuentaContable)
        Me.Controls.Add(Me.txtResolucion49)
        Me.Controls.Add(Me.cbxPadre)
        Me.Controls.Add(Me.txtTipo)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblPadre)
        Me.Controls.Add(Me.txtCategoria)
        Me.Controls.Add(Me.lblCategoria)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblCodigo)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblDescripcion)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.Name = "frmAdministrarResolucion49"
        Me.Text = "frmAdministrarResolucion49"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbxPadre As ocxCBX
    Friend WithEvents txtTipo As TextBox
    Friend WithEvents lblTipo As Label
    Friend WithEvents btnCancelar As Button
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents btnEliminar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents lblPadre As Label
    Friend WithEvents txtCategoria As ocxTXTNumeric
    Friend WithEvents lblCategoria As Label
    Friend WithEvents txtCodigo As ocxTXTString
    Friend WithEvents txtDescripcion As ocxTXTString
    Friend WithEvents lblCodigo As Label
    Friend WithEvents rdbDesactivado As RadioButton
    Friend WithEvents rdbActivo As RadioButton
    Friend WithEvents lblEstado As Label
    Friend WithEvents lblDescripcion As Label
    Friend WithEvents txtID As TextBox
    Friend WithEvents lblID As Label
    Friend WithEvents txtResolucion49 As TextBox
    Friend WithEvents lblCuentaContable As Label
    Friend WithEvents txtDefinicion As TextBox
    Friend WithEvents txtCuentaContable As ocxTXTNumeric
End Class

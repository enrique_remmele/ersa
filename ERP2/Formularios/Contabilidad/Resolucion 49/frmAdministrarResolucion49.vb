﻿Public Class frmAdministrarResolucion49

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CCTemp As Integer

    'PROPIEDADES
    Private IDValue As Integer
        Public Property ID() As Integer
            Get
                Return IDValue
            End Get
            Set(ByVal value As Integer)
                IDValue = value
            End Set
        End Property

        Private PadreValue As String
        Public Property Padre() As String
            Get
                Return PadreValue
            End Get
            Set(ByVal value As String)
                PadreValue = value
            End Set
        End Property

        Private CodigoPadreValue As String
        Public Property CodigoPadre() As String
            Get
                Return CodigoPadreValue
            End Get
            Set(ByVal value As String)
                CodigoPadreValue = value
            End Set
        End Property

        Private IDPadreValue As Integer
        Public Property IDPadre() As Integer
            Get
                Return IDPadreValue
            End Get
            Set(ByVal value As Integer)
                IDPadreValue = value
            End Set
        End Property

    Private CuentaContableRG49Value As String
    Public Property CuentaContableRG49() As String
        Get
            Return CuentaContableRG49Value
        End Get
        Set(ByVal value As String)
            CuentaContableRG49Value = value
        End Set
    End Property

    Private IDPlanCuentaRG49Value As Integer
    Public Property IDPlanCuentaRG49() As Integer
        Get
            Return IDPlanCuentaRG49Value
        End Get
        Set(ByVal value As Integer)
            IDPlanCuentaRG49Value = value
        End Set
    End Property

    Private NuevoValue As Boolean
        Public Property Nuevo() As Boolean
            Get
                Return NuevoValue
            End Get
            Set(ByVal value As Boolean)
                NuevoValue = value
            End Set
        End Property

        Private CategoriaValue As Integer
        Public Property Categoria() As Integer
            Get
                Return CategoriaValue
            End Get
            Set(ByVal value As Integer)
                CategoriaValue = value
            End Set
        End Property

        Private TipoValue As String
        Public Property Tipo() As String
            Get
                Return TipoValue
            End Get
            Set(ByVal value As String)
                TipoValue = value
            End Set
        End Property

        Private ProcesadoValue As Boolean
        Public Property Procesado() As Boolean
            Get
                Return ProcesadoValue
            End Get
            Set(ByVal value As Boolean)
                ProcesadoValue = value
            End Set
        End Property

        'EVENTOS

        'VARIABLES
        Dim oRow As DataRow
    Dim vTabla As String = "VCuentaContableRG49"

    'FUNCIONES
    Sub Inicializar()

            'Form
            If Nuevo = True Then
                Me.Text = "Agregar una Cuenta"
            Else
                Me.Text = "Modificar la cuenta"
            End If

            'RadioButton
            rdbActivo.Checked = True

            'Funciones
            CargarInformacion()


        End Sub

        Sub CargarInformacion()

            'Detalle
            txtCategoria.txt.Text = Categoria
        txtTipo.Text = Tipo
        'txtCuentaContable.Texto = ""

        'Padres
        Dim dtTemp As DataTable = CData.GetTable(vTabla, "IDPlanCuentaRG49=" & IDPlanCuentaRG49 & " ").Copy
        CSistema.OrderDataTable(dtTemp, "Cuenta")
        CSistema.SqlToComboBox(cbxPadre.cbx, dtTemp, "ID", "Cuenta")

        If Nuevo = True Then

            ID = CInt(CSistema.ExecuteScalar("Select ISNULL(MAX(ID)+1, 1) From VCuentaContableRG49"))
            txtID.Text = ID
            txtResolucion49.Text = CuentaContableRG49
            cbxPadre.cbx.Text = Padre
            txtCodigo.txt.Text = CodigoPadre
            txtCodigo.txt.Focus()
            txtCodigo.txt.SelectionStart = txtCodigo.txt.Text.Length

        Else

                Try
                    'oRow = CSistema.ExecuteToDataTable("Select * From VCuentaContable Where ID=" & ID & " ").Rows(0)
                    oRow = CData.GetTable(vTabla, "ID=" & ID & " ").Rows(0)
                    txtID.Text = ID.ToString
                txtResolucion49.Text = oRow("PlanCuenta").ToString
                cbxPadre.SelectedValue(oRow("IDPadre").ToString)

                    txtCodigo.txt.Text = oRow("Codigo").ToString
                'txtAlias.txt.Text = oRow("Alias").ToString
                txtDescripcion.txt.Text = oRow("Descripcion").ToString

                    txtCategoria.txt.Text = oRow("Categoria").ToString
                    txtTipo.Text = oRow("Tipo").ToString

                    If CBool(oRow("Activo").ToString) = True Then
                        rdbActivo.Checked = True
                    Else
                        rdbDesactivado.Checked = True
                    End If

                'txtCuentaContable.txt.Text = oRow("CodigoCC").ToString
                'txtDefinicion.Text = oRow("DescripcionC").ToString

                IDPadre = oRow("IDPadre").ToString
                'IDPlanCuentaRG49 = oRow("IDPlanCuentaRG49").ToString

            Catch ex As Exception
                    Dim mensaje As String = ex.Message
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End Try

            End If

        End Sub

        Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
            ctrError.Clear()

            'Validar
            'Codigo
            If txtCodigo.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Introduzca un codigo valido!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Descripcion
            If txtDescripcion.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Introduzca una descripcion valida!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

        'Seleccion del Padre
        'If cbxPadre.Validar("Seleccione correctamente la cuenta PADRE", ctrError, btnGuardar, tsslEstado) = False Then
        '    Exit Sub
        'End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro y sus dependientes. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@ID", txtID.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPlanCuentaRG49", IDPlanCuentaRG49, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Codigo", txtCodigo.txt.Text.Trim, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Alias", txtAlias.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Categoria", txtCategoria.ObtenerValor, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDPadre", cbxPadre.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", rdbActivo.Checked.ToString, ParameterDirection.Input)

        'Agrega los datos en la tabla que vincula los ID's
        'CSistema.SetSQLParameter(param, "@IDCuentaContable", CCTemp, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDCuentaContableRG49", txtID.Text, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Insertar Registro
            If CSistema.ExecuteStoreProcedure(param, "SpResolucion49", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Procesado = False
                Exit Sub
            End If

            'Actualizar DataTable
            If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Then
                CData.Actualizar(ID, vTabla)
            Else
                CData.Insertar(ID, vTabla)
            End If

            Procesado = True

            Me.Close()

        End Sub

        Private Sub frmAdministrarCuentaContable_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
            CSistema.SelectNextControl(Me, e.KeyCode)
        End Sub

        Private Sub frmAdministrarCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Inicializar()
        End Sub

        Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
            Dim Opcion As CSistema.NUMOperacionesABM
            If Nuevo = True Then
                Opcion = ERP.CSistema.NUMOperacionesABM.INS
            Else
                Opcion = ERP.CSistema.NUMOperacionesABM.UPD
            End If

            Guardar(Opcion)

        End Sub

        Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
            Procesado = False
            Me.Close()
        End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Guardar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    'FA 28/05/2021
    Sub frmAdministrarCuentaContable_Activate()
        Me.Refresh()
    End Sub

    Private Sub txtCuentaContable_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtCuentaContable.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            BuscarCuenta()
        End If

    End Sub

    Sub BuscarCuenta()
        Dim CC As DataTable
        'Dim CCTemp As Integer

        If txtCuentaContable.txt.Text = "" Then
            tsslEstado.Text = "Escriba una cuenta"
            txtCuentaContable.txt.Clear()
            txtDefinicion.Text = ""
            Exit Sub
        End If

        CC = CSistema.ExecuteToDataTable("Select ID, Descripcion From VCuentaContable where codigo =" & txtCuentaContable.txt.Text)
        tsslEstado.Text = ""


        If CC.Rows.Count = 0 Then
            tsslEstado.Text = "La cuenta No existe"
            txtCuentaContable.txt.Clear()
            txtDefinicion.Text = ""
            Exit Sub
        Else
            Dim orow1 As DataRow = CC.Rows(0)
            txtDefinicion.Text = orow1("Descripcion")
            CCTemp = orow1("ID")
        End If



    End Sub

End Class
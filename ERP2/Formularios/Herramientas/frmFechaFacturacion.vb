﻿Public Class frmFechaFacturacion

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim FechaTemporal As Date

    'FUNCIONES
    Sub Inicializar()


        'If (IsDate(vgConfiguraciones("VentaFechaFacturacion"))) = True Then
        '    FechaTemporal = vgConfiguraciones("VentaFechaFacturacion")
        'Else
        '    FechaTemporal = VGFechaHoraSistema
        'End If
        'txtFecha.txt.Text = FechaTemporal

        'Consultar la fecha configurada actualmente
        VGFechaFacturacion = CSistema.ExecuteScalar("Select Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal, VGCadenaConexion)
        'vgConfiguraciones("VentaFechaFacturacion") = VGFechaFacturacion

        'Guardar informacion
        If (IsDate(VGFechaFacturacion)) = True Then
            FechaTemporal = VGFechaFacturacion
        Else
            FechaTemporal = VGFechaHoraSistema
        End If

        txtFecha.txt.Text = FechaTemporal

    End Sub

    Private Sub frmFechaFacturacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        CSistema.ExecuteNonQuery(" Update Configuraciones Set VentaFechaFacturacion= '" & txtFecha.GetValueString & "' Where IDSucursal=" & vgIDSucursal & " ")
        'CSistema.ExecuteNonQuery(" If Exists(Select * From FechaFacturacion Where IDSucursal=" & vgIDSucursal & ") Begin Update FechaFacturacion Set Fecha= '" & txtFecha.GetValueString & "' Where IDSucursal= " & vgIDSucursal & " End Else Insert Into FechaFacturacion(IDSucursal, Fecha) Values(1, '" & txtFecha.GetValueString & "')")
        CSistema.ExecuteNonQuery(" If Exists(Select * From FechaFacturacion Where IDSucursal=" & vgIDSucursal & ") Begin Update FechaFacturacion Set Fecha= '" & txtFecha.GetValueString & "',  IDUsuarioUltimaModificacion = " & vgIDUsuario & ", FechaUltimaModificacion=GETDATE() Where IDSucursal= " & vgIDSucursal & " End Else Insert Into FechaFacturacion(IDSucursal, Fecha, IDUsuarioUltimaModificacion, FechaUltimaModificacion) Values(" & vgIDSucursal & ", '" & txtFecha.GetValueString & "', " & vgIDUsuario & ", GETDATE())")
        CSistema.ExecuteNonQuery("Insert Into aFechaFacturacion(IDSucursal, Fecha, IDUsuarioUltimaModificacion, FechaUltimaModificacion) Values(" & vgIDSucursal & ", '" & txtFecha.GetValueString & "', " & vgIDUsuario & ", GETDATE())")
        VGFechaFacturacion = txtFecha.GetValue
        vgConfiguraciones("VentaFechaFacturacion") = VGFechaFacturacion


        MsgBox("Registro Guardado.!!")
        Me.Close()

        'CSistema.ExecuteNonQuery(" Update  Configuraciones Set VentaFechaFacturacion= '" & txtFecha.GetValueString & "'")
        'vgConfiguraciones("VentaFechaFacturacion") = txtFecha.GetValue

        'MsgBox("Registro Guardado.!!")
        'Me.Close()

    End Sub

End Class
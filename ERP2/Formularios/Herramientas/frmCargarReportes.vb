﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmCargarReportes

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio
    Dim CMultiHilo As New CMHilo

    'VARIABLES
    Dim t As Thread
    Property procesando As Boolean
    Dim Directorios() As String

    'FUNCIONES
    Sub Inicializar()

    End Sub

    Sub CargarDirectorios(Optional path As String = "")

        If path = "" Then
            path = txtDirectorio.Text
        End If

        For Each vfile As String In Directory.GetFiles(path)

            Dim finfo As IO.FileInfo = New IO.FileInfo(vfile)

            If finfo.Extension = ".rpt" Then

                Dim indice As Integer = 0

                If Directorios Is Nothing Then
                    indice = 0
                Else
                    indice = Directorios.Length
                End If

                ReDim Preserve Directorios(indice)

                Directorios(indice) = vfile

            End If
        Next

        If Directory.GetDirectories(path).Length > 0 Then
            For Each vpath As String In Directory.GetDirectories(path)
                CargarDirectorios(vpath)
            Next
        End If

    End Sub

    Sub Iniciar()

        CMultiHilo.LabelText(Me, lblInforme, "0 de 0 - 0%")
        CMultiHilo.ProgressBarValue(Me, pb, 0)

        'Validar el directorio
        If Directory.Exists(txtDirectorio.Text.Trim) = False Then
            MessageBox.Show("Seleccione correctamente el directorio!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        procesando = True

        CargarDirectorios()

        t = New Thread(AddressOf GuardarLote)
        t.Start()

        btnIniciar.Enabled = False
        btnCancelar.Enabled = True

    End Sub

    Sub Cancelar()

        t.Abort()
        procesando = False
        CMultiHilo.LabelText(Me, lblInforme, "0 de 0 - 0%")
        CMultiHilo.ProgressBarValue(Me, pb, 0)
        ReDim Directorios(-1)

        btnIniciar.Enabled = True
        btnCancelar.Enabled = False

    End Sub

    Sub GuardarLote()

        Dim Contador As Integer = 0
        Dim Total As Integer = 0
        Dim Progreso As Integer = 0

        Total = Directorios.Length
        CMultiHilo.LabelText(Me, lblInforme, "0 de " & Total & " - 0%")

        For Each vfile As String In Directorios

            If procesando = False Then
                Exit Sub
            End If

            Dim Archivo As String = vfile
            Dim Version As Integer = 0
            Dim report As New CrystalDecisions.CrystalReports.Engine.ReportClass
            report.FileName = Archivo
           

            Dim filebyte As Byte() = Nothing

            Try
                If report.SummaryInfo.ReportComments IsNot Nothing Then
                    Version = CType(report.SummaryInfo.ReportComments, Integer)
                End If

                filebyte = System.IO.File.ReadAllBytes(Archivo)
                If (filebyte IsNot Nothing) Then
                    Dim strSql As String = "INSERT INTO Informe (nombre, archivo, version, usuario, observacion) VALUES (@nombre, @archivo, @version, @usuario, @observacion)"
                    Using sqlConn As SqlConnection = New SqlConnection(VGCadenaConexion)
                        Using sqlComm As SqlCommand = New SqlCommand(strSql, sqlConn)
                            sqlComm.Parameters.Add("@nombre", SqlDbType.VarChar, 50).Value = System.IO.Path.GetFileNameWithoutExtension(Archivo)
                            sqlComm.Parameters.Add("@archivo", SqlDbType.Binary).Value = filebyte
                            sqlComm.Parameters.Add("@version", SqlDbType.Int).Value = Version
                            sqlComm.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = "ADM"
                            sqlComm.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = ""
                            sqlConn.Open()
                            sqlComm.ExecuteNonQuery()
                            sqlComm.CommandText = "DELETE FROM Informe WHERE nombre = '" & System.IO.Path.GetFileNameWithoutExtension(Archivo) & "' AND version < " & Version
                            sqlComm.ExecuteNonQuery()

                        End Using
                    End Using

                End If
            Catch Ex As Exception

            Finally
                ' Check this again, since we need to make sure we didn't throw an exception on open. 
                If (filebyte IsNot Nothing) Then
                    filebyte = Nothing
                End If
            End Try

            'Cuenta
            Contador = Contador + 1
            Progreso = CInt((Contador / Total) * 100)
            CMultiHilo.LabelText(Me, lblInforme, Contador.ToString & " de " & Total & " - " & Progreso & "%")
            CMultiHilo.ProgressBarValue(Me, pb, Progreso)

        Next

        'Terminar
        procesando = False
        ReDim Directorios(-1)
        CMultiHilo.LabelText(Me, lblInforme, "0 de 0 - 0%")

        btnIniciar.Enabled = True
        btnCancelar.Enabled = False

    End Sub

    Private Sub btnIniciar_Click(sender As System.Object, e As System.EventArgs) Handles btnIniciar.Click
        Iniciar()
    End Sub

    Private Sub frmCargarReportes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub lklSeleccionarDirectorio_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarDirectorio.LinkClicked
        If FolderBrowserDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            Exit Sub
        End If

        txtDirectorio.Text = FolderBrowserDialog1.SelectedPath

    End Sub

End Class
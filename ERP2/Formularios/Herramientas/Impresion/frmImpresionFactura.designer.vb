﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionFormatoImpresion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtExaminarConversion = New ERP.ocxTXTString()
        Me.btnExaminarConversion = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtExaminar = New ERP.ocxTXTString()
        Me.btnExaminar = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxImpresora = New ERP.ocxCBX()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtExaminarNotaDebito = New ERP.ocxTXTString()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtExaminarNotaCredito = New ERP.ocxTXTString()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtExaminarRetencion = New ERP.ocxTXTString()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtExaminarConversion)
        Me.GroupBox1.Controls.Add(Me.btnExaminarConversion)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtExaminar)
        Me.GroupBox1.Controls.Add(Me.btnExaminar)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(409, 76)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturación"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 13)
        Me.Label3.TabIndex = 44
        Me.Label3.Text = "Patch Fact. Conversión:"
        '
        'txtExaminarConversion
        '
        Me.txtExaminarConversion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExaminarConversion.Color = System.Drawing.Color.Empty
        Me.txtExaminarConversion.Indicaciones = Nothing
        Me.txtExaminarConversion.Location = New System.Drawing.Point(130, 46)
        Me.txtExaminarConversion.Multilinea = False
        Me.txtExaminarConversion.Name = "txtExaminarConversion"
        Me.txtExaminarConversion.Size = New System.Drawing.Size(197, 21)
        Me.txtExaminarConversion.SoloLectura = True
        Me.txtExaminarConversion.TabIndex = 42
        Me.txtExaminarConversion.TabStop = False
        Me.txtExaminarConversion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtExaminarConversion.Texto = ""
        '
        'btnExaminarConversion
        '
        Me.btnExaminarConversion.Location = New System.Drawing.Point(342, 44)
        Me.btnExaminarConversion.Name = "btnExaminarConversion"
        Me.btnExaminarConversion.Size = New System.Drawing.Size(60, 23)
        Me.btnExaminarConversion.TabIndex = 43
        Me.btnExaminarConversion.Text = "E&xaminar"
        Me.btnExaminarConversion.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(49, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 41
        Me.Label1.Text = "Patch Factura:"
        '
        'txtExaminar
        '
        Me.txtExaminar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExaminar.Color = System.Drawing.Color.Empty
        Me.txtExaminar.Indicaciones = Nothing
        Me.txtExaminar.Location = New System.Drawing.Point(130, 21)
        Me.txtExaminar.Multilinea = False
        Me.txtExaminar.Name = "txtExaminar"
        Me.txtExaminar.Size = New System.Drawing.Size(197, 21)
        Me.txtExaminar.SoloLectura = True
        Me.txtExaminar.TabIndex = 37
        Me.txtExaminar.TabStop = False
        Me.txtExaminar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtExaminar.Texto = ""
        '
        'btnExaminar
        '
        Me.btnExaminar.Location = New System.Drawing.Point(342, 19)
        Me.btnExaminar.Name = "btnExaminar"
        Me.btnExaminar.Size = New System.Drawing.Size(60, 23)
        Me.btnExaminar.TabIndex = 38
        Me.btnExaminar.Text = "E&xaminar"
        Me.btnExaminar.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Impresora:"
        '
        'cbxImpresora
        '
        Me.cbxImpresora.CampoWhere = Nothing
        Me.cbxImpresora.CargarUnaSolaVez = False
        Me.cbxImpresora.DataDisplayMember = Nothing
        Me.cbxImpresora.DataFilter = Nothing
        Me.cbxImpresora.DataOrderBy = Nothing
        Me.cbxImpresora.DataSource = Nothing
        Me.cbxImpresora.DataValueMember = Nothing
        Me.cbxImpresora.FormABM = Nothing
        Me.cbxImpresora.Indicaciones = Nothing
        Me.cbxImpresora.Location = New System.Drawing.Point(65, 29)
        Me.cbxImpresora.Name = "cbxImpresora"
        Me.cbxImpresora.SeleccionObligatoria = False
        Me.cbxImpresora.Size = New System.Drawing.Size(334, 21)
        Me.cbxImpresora.SoloLectura = False
        Me.cbxImpresora.TabIndex = 39
        Me.cbxImpresora.Texto = ""
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(336, 318)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "Elegir el Formato de impresion"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.txtExaminarNotaDebito)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtExaminarNotaCredito)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 90)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(409, 76)
        Me.GroupBox2.TabIndex = 41
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Nota Credito/Debito"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 50)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 13)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Patch Nota Debito:"
        '
        'txtExaminarNotaDebito
        '
        Me.txtExaminarNotaDebito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExaminarNotaDebito.Color = System.Drawing.Color.Empty
        Me.txtExaminarNotaDebito.Indicaciones = Nothing
        Me.txtExaminarNotaDebito.Location = New System.Drawing.Point(130, 46)
        Me.txtExaminarNotaDebito.Multilinea = False
        Me.txtExaminarNotaDebito.Name = "txtExaminarNotaDebito"
        Me.txtExaminarNotaDebito.Size = New System.Drawing.Size(197, 21)
        Me.txtExaminarNotaDebito.SoloLectura = True
        Me.txtExaminarNotaDebito.TabIndex = 42
        Me.txtExaminarNotaDebito.TabStop = False
        Me.txtExaminarNotaDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtExaminarNotaDebito.Texto = ""
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(342, 44)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(60, 23)
        Me.Button1.TabIndex = 43
        Me.Button1.Text = "E&xaminar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(24, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 13)
        Me.Label5.TabIndex = 41
        Me.Label5.Text = "Patch Nota Credito:"
        '
        'txtExaminarNotaCredito
        '
        Me.txtExaminarNotaCredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExaminarNotaCredito.Color = System.Drawing.Color.Empty
        Me.txtExaminarNotaCredito.Indicaciones = Nothing
        Me.txtExaminarNotaCredito.Location = New System.Drawing.Point(130, 21)
        Me.txtExaminarNotaCredito.Multilinea = False
        Me.txtExaminarNotaCredito.Name = "txtExaminarNotaCredito"
        Me.txtExaminarNotaCredito.Size = New System.Drawing.Size(197, 21)
        Me.txtExaminarNotaCredito.SoloLectura = True
        Me.txtExaminarNotaCredito.TabIndex = 37
        Me.txtExaminarNotaCredito.TabStop = False
        Me.txtExaminarNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtExaminarNotaCredito.Texto = ""
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(342, 19)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(60, 23)
        Me.Button2.TabIndex = 38
        Me.Button2.Text = "E&xaminar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cbxImpresora)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Location = New System.Drawing.Point(4, 242)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(409, 68)
        Me.GroupBox3.TabIndex = 42
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Seleccionar Impresora"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.txtExaminarRetencion)
        Me.GroupBox4.Controls.Add(Me.Button4)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 175)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(409, 57)
        Me.GroupBox4.TabIndex = 45
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Retención"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(34, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(90, 13)
        Me.Label7.TabIndex = 41
        Me.Label7.Text = "Patch Retención:"
        '
        'txtExaminarRetencion
        '
        Me.txtExaminarRetencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtExaminarRetencion.Color = System.Drawing.Color.Empty
        Me.txtExaminarRetencion.Indicaciones = Nothing
        Me.txtExaminarRetencion.Location = New System.Drawing.Point(130, 21)
        Me.txtExaminarRetencion.Multilinea = False
        Me.txtExaminarRetencion.Name = "txtExaminarRetencion"
        Me.txtExaminarRetencion.Size = New System.Drawing.Size(197, 21)
        Me.txtExaminarRetencion.SoloLectura = True
        Me.txtExaminarRetencion.TabIndex = 37
        Me.txtExaminarRetencion.TabStop = False
        Me.txtExaminarRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtExaminarRetencion.Texto = ""
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(342, 19)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(60, 23)
        Me.Button4.TabIndex = 38
        Me.Button4.Text = "E&xaminar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'frmSeleccionFormatoImpresion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(414, 348)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmSeleccionFormatoImpresion"
        Me.Text = "frmSeleccionReporteImpresion"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtExaminar As ERP.ocxTXTString
    Friend WithEvents cbxImpresora As ERP.ocxCBX
    Friend WithEvents btnExaminar As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtExaminarConversion As ERP.ocxTXTString
    Friend WithEvents btnExaminarConversion As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtExaminarNotaDebito As ERP.ocxTXTString
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtExaminarNotaCredito As ERP.ocxTXTString
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtExaminarRetencion As ERP.ocxTXTString
    Friend WithEvents Button4 As System.Windows.Forms.Button
End Class

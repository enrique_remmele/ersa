﻿Public Class frmImpresionSeccion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTipoComprobanteValue As Integer
    Public Property IDTipoComprobante() As Integer
        Get
            Return IDTipoComprobanteValue
        End Get
        Set(ByVal value As Integer)
            IDTipoComprobanteValue = value
        End Set
    End Property

    Private IDSeccionValue As Integer
    Public Property IDSeccion() As Integer
        Get
            Return IDSeccionValue
        End Get
        Set(ByVal value As Integer)
            IDSeccionValue = value
        End Set
    End Property

    Private OperacionABMValue As ERP.CSistema.NUMOperacionesABM
    Public Property OperacionABM() As ERP.CSistema.NUMOperacionesABM
        Get
            Return OperacionABMValue
        End Get
        Set(ByVal value As ERP.CSistema.NUMOperacionesABM)
            OperacionABMValue = value
        End Set
    End Property


    'VARIABLES
    Dim dtSecciones As DataTable

    'FUNCIONES
    Sub InicializarNuevo()

        For Each oRow As DataRow In CData.GetTable("VOperacion", " ID = " & IDOperacion).Copy.Rows
            txtOperacion.txt.Text = oRow("Descripcion").ToString
        Next

        For Each oRow As DataRow In CData.GetTable("VTipoComprobante", " IDOperacion = " & IDOperacion & " And ID = " & IDTipoComprobante).Copy.Rows
            txtTipoComprobante.txt.Text = oRow("Descripcion").ToString
        Next

        txtID.SetValue(CSistema.ExecuteScalar("Select IsNull((Select MAX(ID) + 1 From ImpresionSeccion Where IDOperacion=" & IDOperacion & " And IDTipoComprobante=" & IDOperacion & "), 1)"))

        CargarDatos()

    End Sub

    Sub InicializarEditar()

        For Each oRow As DataRow In CData.GetTable("VOperacion", " ID = " & IDOperacion).Copy.Rows
            txtOperacion.txt.Text = oRow("Descripcion").ToString
        Next

        For Each oRow As DataRow In CData.GetTable("VTipoComprobante", " IDOperacion = " & IDOperacion & " And ID = " & IDTipoComprobante).Copy.Rows
            txtTipoComprobante.txt.Text = oRow("Descripcion").ToString
        Next

        cbxSeccion.SoloLectura = True

        CargarDatos()

    End Sub

    Sub CargarDatos()

        'Secciones
        dtSecciones = New DataTable
        dtSecciones.Columns.Add("ID")
        dtSecciones.Columns.Add("Descripcion")
        dtSecciones.Columns.Add("Seccion")

        Dim NewRow As DataRow
        NewRow = dtSecciones.NewRow
        NewRow("ID") = 2
        NewRow("Descripcion") = "CABECERA"
        NewRow("Seccion") = "Seccion2"
        dtSecciones.Rows.Add(NewRow)

        NewRow = dtSecciones.NewRow
        NewRow("ID") = 3
        NewRow("Descripcion") = "DETALLE"
        NewRow("Seccion") = "Seccion3"
        dtSecciones.Rows.Add(NewRow)

        NewRow = dtSecciones.NewRow
        NewRow("ID") = 4
        NewRow("Descripcion") = "PIE"
        NewRow("Seccion") = "Seccion4"
        dtSecciones.Rows.Add(NewRow)

        CSistema.SqlToComboBox(cbxSeccion.cbx, dtSecciones, "ID", "Descripcion")

        'Tablas
        Dim dtTabla As DataTable = CData.GetTable("sysobjects", " Type = 'V' ").Copy
        CData.OrderDataTable(dtTabla, "name")
        CSistema.SqlToComboBox(cbxTabla.cbx, dtTabla, "name", "name")

        Dim dtTemp As DataTable = CData.GetTable("ImpresionSeccion", " IDOperacion=" & IDOperacion & " And IDTipoComprobante=" & IDTipoComprobante & " And ID=" & IDSeccion).Copy

        For Each oRow As DataRow In dtTemp.Rows
            txtAlto.SetValue(oRow("Alto").ToString)
            txtAlto.SetValue(oRow("Alto").ToString)
            cbxSeccion.cbx.Text = oRow("Descripcion").ToString
            cbxTabla.cbx.Text = oRow("Tabla").ToString
        Next

    End Sub

    Sub Guardar()

        'Validar seleccion
        If cbxSeccion.Validar("Seleccione correctamente la seccion!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", IDTipoComprobante, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ID", cbxSeccion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", cbxSeccion.cbx.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Alto", txtAlto.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Tabla", cbxTabla.cbx.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", OperacionABM.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpImpresionSeccion", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()

            'Actualizamos la tabla
            CData.ResetTable("ImpresionSeccion")

        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub Cancelar()

    End Sub

    Sub Eliminar()

    End Sub

    Private Sub frmImpresionSeccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Guardar()
    End Sub

End Class
﻿Public Class frmImpersion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CImpresion As New CImpresion

    'EVENTOS

    'PROPIEDADES

    'VARIABLES
    Dim dtImpresion As DataTable
    Dim dtSecciones As DataTable
    Dim dtTipoComprobante As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Tablas
        dtImpresion = CData.GetTable("Impresion")
        dtSecciones = CData.GetTable("ImpresionSeccion")

    End Sub

    Sub ListarImpresion()

        'Limpiar informacion
        nudTamañoPaginaAlto.Value = 0
        nudTamañoPaginaAncho.Value = 0
        rdbHorizontal.Checked = False
        rdbVertical.Checked = False
        txtCantidad.SetValue(0)
        nudDuplicadoX.Value = 0
        nudDuplicadoY.Value = 0

        'Validar seleccion
        If cbxOperacion.Validar("Seleccione correctamente la operacion!", ctrError, btnGuardarImpresion, tsslEstado) = False Then
            Exit Sub
        End If

        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardarImpresion, tsslEstado) = False Then
            Exit Sub
        End If

        If dtImpresion Is Nothing Then
            Exit Sub
        End If

        Dim Filtro As String = " IDOperacion=" & cbxOperacion.GetValue & " And IDTipoComprobante=" & cbxTipoComprobante.GetValue

        For Each oRow As DataRow In dtImpresion.Select(Filtro)
            nudTamañoPaginaAlto.Value = oRow("PaginaAltoLargo").ToString.Split(",")(0)
            nudTamañoPaginaAncho.Value = oRow("PaginaAltoLargo").ToString.Split(",")(1)
            rdbHorizontal.Checked = oRow("Horizontal")
            rdbVertical.Checked = oRow("Vertical")
            txtCantidad.SetValue(oRow("CantidadDetalle"))
            nudDuplicadoX.Value = oRow("DuplicadoXY").ToString.Split(",")(0)
            nudDuplicadoY.Value = oRow("DuplicadoXY").ToString.Split(",")(1)
        Next

        'Listar Campos
        ListarCampos()

    End Sub

    Sub GuardarImpresion()

        'Validar seleccion
        If cbxOperacion.Validar("Seleccione correctamente la operacion!", ctrError, btnGuardarImpresion, tsslEstado) = False Then
            Exit Sub
        End If

        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardarImpresion, tsslEstado) = False Then
            Exit Sub
        End If


        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDOperacion", cbxOperacion.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Horizontal", rdbHorizontal.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Vertical", rdbVertical.Checked.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PaginaAltoLargo", SetParam(nudTamañoPaginaAlto.Value.ToString, nudTamañoPaginaAncho.Value.ToString, ","), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@DuplicadoXY", SetParam(nudDuplicadoX.Value.ToString, nudDuplicadoY.Value.ToString, ","), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CantidadDetalle", txtCantidad.ObtenerValor, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpImpresion", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()

            'Actualizamos la tabla
            CData.ResetTable("Impresion")
            dtImpresion = CData.GetTable("Impresion")

            ListarImpresion()

        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardarImpresion, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardarImpresion, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub Visualizar()

        'CImpresion.SetReport(New rptGenerico)
        'CImpresion.PaginaAlto = nudTamañoPaginaAlto.Value
        'CImpresion.PaginaAncho = nudTamañoPaginaAncho.Value
        'CImpresion.Horizontal = rdbHorizontal.Checked

        'CImpresion.Visualizar(crvVista)

    End Sub

    Sub CargarSeccion(ByVal Tipo As CSistema.NUMOperacionesABM)

        If cbxOperacion.Validar("Seleccione correctamente la operacion!", ctrError, btnGuardarImpresion, tsslEstado) = False Then
            Exit Sub
        End If

        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardarImpresion, tsslEstado) = False Then
            Exit Sub
        End If

        Dim frm As New frmImpresionSeccion

        'Obtener valores
        frm.IDOperacion = cbxOperacion.GetValue
        frm.IDTipoComprobante = cbxTipoComprobante.GetValue

        If tvCampos.SelectedNode IsNot Nothing Then
            frm.IDSeccion = tvCampos.SelectedNode.Name
        End If

        frm.OperacionABM = Tipo

        'Customizar el formulario
        Select Case Tipo
            Case ERP.CSistema.NUMOperacionesABM.INS
                frm.btnEliminar.Enabled = False
                frm.InicializarNuevo()
            Case ERP.CSistema.NUMOperacionesABM.UPD
                frm.btnEliminar.Enabled = False
                frm.InicializarEditar()
            Case ERP.CSistema.NUMOperacionesABM.DEL
                frm.btnGuardar.Enabled = False
                frm.InicializarEditar()
        End Select

        'Mostrar
        FGMostrarFormulario(Me, frm, "Secciones de Impresion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True)

        'Actualizar Campos
        ListarCampos()

    End Sub

    Sub CargarCampo(ByVal Tipo As CSistema.NUMOperacionesABM)

        If cbxOperacion.Validar("Seleccione correctamente la operacion!", ctrError, btnGuardarImpresion, tsslEstado) = False Then
            Exit Sub
        End If

        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardarImpresion, tsslEstado) = False Then
            Exit Sub
        End If

        If tvCampos.SelectedNode Is Nothing Then
            CSistema.MostrarError("Seleccione correctamente una seccion!", ctrError, tvCampos, tsslEstado, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'If tvCampos.SelectedNode.Level <> 0 Then
        '    CSistema.MostrarError("Seleccione correctamente una seccion!", ctrError, tvCampos, tsslEstado, ErrorIconAlignment.TopLeft)
        '    Exit Sub
        'End If

        Dim frm As New frmImpresionCampo

        'Obtener valores
        frm.IDOperacion = cbxOperacion.GetValue
        frm.IDTipoComprobante = cbxTipoComprobante.GetValue
        If tvCampos.SelectedNode.Level = 1 Then
            frm.IDSeccion = tvCampos.SelectedNode.Parent.Name
        Else
            frm.IDSeccion = tvCampos.SelectedNode.Name
        End If

        frm.ID = tvCampos.SelectedNode.Name
        frm.OperacionABM = Tipo

        'Customizar el formulario
        Select Case Tipo
            Case ERP.CSistema.NUMOperacionesABM.INS
                frm.btnEliminar.Enabled = False
                frm.InicializarNuevo()
            Case ERP.CSistema.NUMOperacionesABM.UPD
                frm.btnEliminar.Enabled = False
                frm.InicializarEditar()
            Case ERP.CSistema.NUMOperacionesABM.DEL
                frm.btnGuardar.Enabled = False
                frm.InicializarEditar()
        End Select

        'Mostrar
        FGMostrarFormulario(Me, frm, "Campos de Impresion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True)

        'Actualizar Campos
        ListarCampos()

    End Sub

    Sub ListarSecciones()

        tvCampos.Nodes.Clear()

        If cbxOperacion.Validar() = False Then
            Exit Sub
        End If

        If cbxTipoComprobante.Validar() = False Then
            Exit Sub
        End If

        dtSecciones = CData.GetTable("ImpresionSeccion")

        For Each oRow As DataRow In dtSecciones.Select(" IDOperacion = " & cbxOperacion.GetValue & " And IDTipoComprobante=" & cbxTipoComprobante.GetValue)
            Dim node1 As New TreeNode(oRow("Descripcion").ToString, 0, 1)
            node1.Name = oRow("ID").ToString

            tvCampos.Nodes.Add(node1)

        Next

    End Sub

    Sub ListarCampos()

        ListarSecciones()

        For Each node1 As TreeNode In tvCampos.Nodes

            Dim IDSeccion As Integer = node1.Name
            Dim IDOperacion As Integer = cbxOperacion.GetValue
            Dim IDTipoComprobante As Integer = cbxTipoComprobante.GetValue

            Dim dtTemp As DataTable = CData.GetTable("ImpresionCampo", " IDSeccion = " & IDSeccion & " And IDTipoComprobante=" & IDTipoComprobante & " And IDOperacion=" & IDOperacion & " ").Copy

            For Each oRow As DataRow In dtTemp.Rows
                Dim node2 As New TreeNode(oRow("Descripcion").ToString)
                node2.Name = oRow("ID").ToString

                Select Case oRow("TipoCampo").ToString.ToUpper
                    Case "STRING"
                        node2.ImageIndex = 0 + 2
                        node2.SelectedImageIndex = 0 + 2
                    Case "BYTE", "INT16", "INT32"
                        node2.ImageIndex = 1 + 2
                        node2.SelectedImageIndex = 1 + 2
                    Case "DECIMAL"
                        node2.ImageIndex = 1 + 2
                        node2.SelectedImageIndex = 1 + 2
                    Case "DATETIME"
                        node2.ImageIndex = 3 + 2
                        node2.SelectedImageIndex = 3 + 2
                    Case "BOOLEAN"
                        node2.ImageIndex = 4 + 2
                        node2.SelectedImageIndex = 4 + 2
                End Select

                node1.Nodes.Add(node2)

            Next

        Next

    End Sub

    Sub ComportamientoCampos()

        If tvCampos.SelectedNode Is Nothing Then

            'Seccion
            btnNuevaSeccion.Enabled = True
            btnEditarSeccion.Enabled = False
            btnEliminarSeccion.Enabled = False

            'Seccion
            btnNuevoCampo.Enabled = False
            btnEditarCampo.Enabled = False
            btnEliminarCampo.Enabled = False

            Exit Sub

        End If

        If tvCampos.SelectedNode.Level = 0 Then

            'Seccion
            btnNuevaSeccion.Enabled = True
            btnEditarSeccion.Enabled = True
            btnEliminarSeccion.Enabled = True

            'Seccion
            btnNuevoCampo.Enabled = True
            btnEditarCampo.Enabled = False
            btnEliminarCampo.Enabled = False

            Exit Sub

        End If

        If tvCampos.SelectedNode.Level = 1 Then

            'Seccion
            btnNuevaSeccion.Enabled = False
            btnEditarSeccion.Enabled = False
            btnEliminarSeccion.Enabled = False

            'Seccion
            btnNuevoCampo.Enabled = False
            btnEditarCampo.Enabled = True
            btnEliminarCampo.Enabled = True

            Exit Sub

        End If

    End Sub

    Function GetParam(ByVal value As String, ByVal delimitador As String, ByVal posicion As Integer) As String

        GetParam = ""

        Try
            GetParam = value.Split(delimitador)(posicion)

        Catch ex As Exception
            Return ""
        End Try

    End Function

    Function SetParam(ByVal value1 As String, ByVal value2 As String, ByVal delimitador As String) As String
        SetParam = value1 & delimitador & value2
    End Function

    Private Sub frmImpersion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxOperacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOperacion.PropertyChanged

        If cbxOperacion.Validar = False Then
            Exit Sub
        End If

        cbxTipoComprobante.Conectar(" IDOperacion = " & cbxOperacion.GetValue)

    End Sub

    Private Sub frmImpersion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.PropertyChanged
        ListarImpresion()
    End Sub

    Private Sub btnGuardarImpresion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardarImpresion.Click
        GuardarImpresion()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Visualizar()
    End Sub

    Private Sub tvCampos_All(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvCampos.AfterSelect
        ComportamientoCampos()
    End Sub

    Private Sub tvCampos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvCampos.Click
        ComportamientoCampos()
    End Sub

    Private Sub tvCampos_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvCampos.MouseEnter
        ComportamientoCampos()
    End Sub

    Private Sub tvCampos_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvCampos.MouseLeave
        ComportamientoCampos()
    End Sub

    Private Sub btnNuevaSeccion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevaSeccion.Click

        If tvCampos.SelectedNode Is Nothing Then
            CargarSeccion(ERP.CSistema.NUMOperacionesABM.INS)
            Exit Sub
        End If

        If tvCampos.SelectedNode.Level = 0 Then
            CargarSeccion(ERP.CSistema.NUMOperacionesABM.INS)
            Exit Sub
        End If

    End Sub

    Private Sub btnEditarSeccion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditarSeccion.Click

        If tvCampos.SelectedNode Is Nothing Then
            Exit Sub
        End If

        If tvCampos.SelectedNode.Level = 0 Then
            CargarSeccion(ERP.CSistema.NUMOperacionesABM.UPD)
            Exit Sub
        End If

    End Sub

    Private Sub btnEliminarSeccion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarSeccion.Click

        If tvCampos.SelectedNode Is Nothing Then
            Exit Sub
        End If

        If tvCampos.SelectedNode.Level = 0 Then
            CargarSeccion(ERP.CSistema.NUMOperacionesABM.DEL)
            Exit Sub
        End If

    End Sub

    Private Sub btnNuevoCampo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoCampo.Click

        If tvCampos.SelectedNode Is Nothing Then
            Exit Sub
        End If

        If tvCampos.SelectedNode.Level <> 0 Then
            Exit Sub
        End If

        CargarCampo(ERP.CSistema.NUMOperacionesABM.INS)

    End Sub

    Private Sub btnEditarCampo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditarCampo.Click

        If tvCampos.SelectedNode Is Nothing Then
            Exit Sub
        End If

        If tvCampos.SelectedNode.Level <> 1 Then
            Exit Sub
        End If

        CargarCampo(ERP.CSistema.NUMOperacionesABM.UPD)

    End Sub

    Private Sub btnEliminarCampo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarCampo.Click

        If tvCampos.SelectedNode Is Nothing Then
            Exit Sub
        End If

        If tvCampos.SelectedNode.Level <> 1 Then
            Exit Sub
        End If

        CargarCampo(ERP.CSistema.NUMOperacionesABM.UPD)

    End Sub

End Class
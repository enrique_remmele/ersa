﻿Public Class frmSeleccionFormatoImpresion

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio
    Dim CSistema As New CSistema

    'Variable
    Dim vPathRemision As String

    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones
        CargarDatos()
        CargarInformacion()
        DesplegarImpresoras()

    End Sub

    Sub CargarDatos()

        'ARCHIVO INI
        vgImpresionFacturaPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA PATH", "")
        vgImpresionFacturaConversionPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA CONVERSION PATH", "")
        vgImpresionNotaCreditoPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "NOTACREDITO PATH", "")
        vgImpresionNotaDebitoPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "NOTADEBITO PATH", "")
        vgImpresionRetencionPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "RETENCION PATH", "")
        vgImpresionFacturaImpresora = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA IMPRESORA", "")

    End Sub

    Sub CargarInformacion()

        txtExaminar.txt.Text = vgImpresionFacturaPath
        txtExaminarConversion.txt.Text = vgImpresionFacturaConversionPath
        txtExaminarNotaCredito.txt.Text = vgImpresionNotaCreditoPath
        txtExaminarNotaDebito.txt.Text = vgImpresionNotaDebitoPath
        txtExaminarRetencion.txt.Text = vgImpresionRetencionPath
        cbxImpresora.cbx.Text = vgImpresionFacturaImpresora


    End Sub

    Sub GuardarInformacion()

        'Reporte Facturacion
        CArchivoInicio.IniWrite(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA PATH", txtExaminar.txt.Text)
        vgImpresionFacturaPath = txtExaminar.txt.Text

        CArchivoInicio.IniWrite(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA CONVERSION PATH", txtExaminarConversion.txt.Text)
        vgImpresionFacturaConversionPath = txtExaminarConversion.txt.Text

        'Reporte Nota Credito/Debito
        CArchivoInicio.IniWrite(VGArchivoINI, "CONFIGURACION IMPRESORA", "NOTACREDITO PATH", txtExaminarNotaCredito.txt.Text)
        vgImpresionNotaCreditoPath = txtExaminarNotaCredito.txt.Text

        CArchivoInicio.IniWrite(VGArchivoINI, "CONFIGURACION IMPRESORA", "NOTADEBITO PATH", txtExaminarNotaDebito.txt.Text)
        vgImpresionNotaDebitoPath = txtExaminarNotaDebito.txt.Text

        'Reporte Retención
        CArchivoInicio.IniWrite(VGArchivoINI, "CONFIGURACION IMPRESORA", "RETENCION PATH", txtExaminarRetencion.txt.Text)
        vgImpresionRetencionPath = txtExaminarRetencion.txt.Text


        'Impresora
        CArchivoInicio.IniWrite(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA IMPRESORA", cbxImpresora.cbx.Text)
        vgImpresionFacturaImpresora = cbxImpresora.cbx.Text

        MsgBox("Registro Guardado.!!")

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        GuardarInformacion()
        Me.Close()
    End Sub

    Sub DesplegarImpresoras()

        cbxImpresora.cbx.Items.Clear()

        For Each strPrinter As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            cbxImpresora.cbx.Items.Add(strPrinter)
        Next

    End Sub

    Private Sub frmSeleccionFormatoImpresion_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me)
    End Sub

    Private Sub frmImpresionFactura_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnExaminarA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminar.Click
        OpenFileDialog1.Filter = "Archivos rpt|*.rpt"
        OpenFileDialog1.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        If OpenFileDialog1.ShowDialog <> DialogResult.OK Then
            Exit Sub
        End If

        txtExaminar.txt.Text = OpenFileDialog1.SafeFileName()
        vPathRemision = OpenFileDialog1.FileName


        Try
            FileCopy(vPathRemision, VGCarpetaAplicacion & "\" & OpenFileDialog1.SafeFileName)
        Catch ex As Exception

        End Try
    End Sub


    Private Sub btnExaminarConversion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminarConversion.Click
        OpenFileDialog1.Filter = "Archivos rpt|*.rpt"
        OpenFileDialog1.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        If OpenFileDialog1.ShowDialog <> DialogResult.OK Then
            Exit Sub
        End If

        txtExaminarConversion.txt.Text = OpenFileDialog1.SafeFileName()
        vPathRemision = OpenFileDialog1.FileName
        FileCopy(vPathRemision, VGCarpetaAplicacion & "\" & OpenFileDialog1.SafeFileName)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        OpenFileDialog1.Filter = "Archivos rpt|*.rpt"
        OpenFileDialog1.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        If OpenFileDialog1.ShowDialog <> DialogResult.OK Then
            Exit Sub
        End If

        txtExaminarNotaCredito.txt.Text = OpenFileDialog1.SafeFileName()
        vPathRemision = OpenFileDialog1.FileName
        FileCopy(vPathRemision, VGCarpetaAplicacion & "\" & OpenFileDialog1.SafeFileName)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        OpenFileDialog1.Filter = "Archivos rpt|*.rpt"
        OpenFileDialog1.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        If OpenFileDialog1.ShowDialog <> DialogResult.OK Then
            Exit Sub
        End If

        txtExaminarNotaDebito.txt.Text = OpenFileDialog1.SafeFileName()
        vPathRemision = OpenFileDialog1.FileName
        FileCopy(vPathRemision, VGCarpetaAplicacion & "\" & OpenFileDialog1.SafeFileName)

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        OpenFileDialog1.Filter = "Archivos rpt|*.rpt"
        OpenFileDialog1.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        If OpenFileDialog1.ShowDialog <> DialogResult.OK Then
            Exit Sub
        End If

        txtExaminarRetencion.txt.Text = OpenFileDialog1.SafeFileName()
        vPathRemision = OpenFileDialog1.FileName
        FileCopy(vPathRemision, VGCarpetaAplicacion & "\" & OpenFileDialog1.SafeFileName)

    End Sub
End Class
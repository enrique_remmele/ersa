﻿Public Class frmImpresionCampo

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTipoComprobanteValue As Integer
    Public Property IDTipoComprobante() As Integer
        Get
            Return IDTipoComprobanteValue
        End Get
        Set(ByVal value As Integer)
            IDTipoComprobanteValue = value
        End Set
    End Property

    Private IDSeccionValue As Integer
    Public Property IDSeccion() As Integer
        Get
            Return IDSeccionValue
        End Get
        Set(ByVal value As Integer)
            IDSeccionValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private OperacionABMValue As ERP.CSistema.NUMOperacionesABM
    Public Property OperacionABM() As ERP.CSistema.NUMOperacionesABM
        Get
            Return OperacionABMValue
        End Get
        Set(ByVal value As ERP.CSistema.NUMOperacionesABM)
            OperacionABMValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtCampos As DataTable

    'FUNCIONES
    Sub InicializarNuevo()

        For Each oRow As DataRow In CData.GetTable("VOperacion", " ID = " & IDOperacion).Copy.Rows
            txtOperacion.txt.Text = oRow("Descripcion").ToString
        Next

        For Each oRow As DataRow In CData.GetTable("VTipoComprobante", " ID = " & IDOperacion).Copy.Rows
            txtTipoComprobante.txt.Text = oRow("Descripcion").ToString
        Next

        For Each oRow As DataRow In CData.GetTable("ImpresionSeccion", " ID = " & IDSeccion & " And IDOperacion=" & IDOperacion & " And IDTipoComprobante=" & IDTipoComprobante).Copy.Rows
            txtSeccion.txt.Text = oRow("Descripcion").ToString
        Next

        txtID.SetValue(CSistema.ExecuteScalar("Select IsNull((Select MAX(ID) + 1 From ImpresionCampo Where IDOperacion=" & IDOperacion & " And IDTipoComprobante=" & IDTipoComprobante & " And IDSeccion=" & IDSeccion & "), 1)"))

        CargarDatos()

    End Sub

    Sub InicializarEditar()

        For Each oRow As DataRow In CData.GetTable("VOperacion", " ID = " & IDOperacion).Copy.Rows
            txtOperacion.txt.Text = oRow("Descripcion").ToString
        Next

        For Each oRow As DataRow In CData.GetTable("VTipoComprobante", " ID = " & IDOperacion).Copy.Rows
            txtTipoComprobante.txt.Text = oRow("Descripcion").ToString
        Next

        For Each oRow As DataRow In CData.GetTable("ImpresionSeccion", " ID = " & IDSeccion & " And IDOperacion=" & IDOperacion & " And IDTipoComprobante=" & IDTipoComprobante).Copy.Rows
            txtSeccion.txt.Text = oRow("Descripcion").ToString
        Next

        CargarDatos()

        For Each oRow As DataRow In CData.GetTable("ImpresionCampo", " ID = " & ID & " And IDSeccion = " & IDSeccion & " And IDOperacion=" & IDOperacion & " And IDTipoComprobante=" & IDTipoComprobante).Copy.Rows
            For Each item As ListViewItem In lvCampo.Items
                If item.Text = oRow("Descripcion").ToString Then
                    item.Selected = True
                    lvCampo.EnsureVisible(item.Index)
                    Exit For
                End If
            Next
        Next

        txtID.txt.Text = ID

    End Sub

    Sub CargarDatos()

        If lvCampo.Columns.Count = 0 Then
            lvCampo.Columns.Add("Campo")
        End If

        lvCampo.Items.Clear()

        'Listar Datos de la Tabla
        Dim Tabla As String = CData.GetTable("ImpresionSeccion", " ID = " & IDSeccion & " And IDOperacion=" & IDOperacion & " And IDTipoComprobante=" & IDTipoComprobante).Copy.Rows(0)("Tabla").ToString
        dtCampos = CData.GetTable(Tabla).Copy

        For i As Integer = 0 To dtCampos.Columns.Count - 1
            Dim item As New ListViewItem(dtCampos.Columns(i).ToString)

            Select Case dtCampos.Columns(i).DataType.Name.ToUpper
                Case "STRING"
                    item.ImageIndex = 0
                Case "BYTE", "INT16", "INT32"
                    item.ImageIndex = 1
                Case "DECIMAL"
                    item.ImageIndex = 1
                Case "DATETIME"
                    item.ImageIndex = 3
                Case "BOOLEAN"
                    item.ImageIndex = 4
            End Select

            lvCampo.Items.Add(item)
        Next

        For i = 0 To lvCampo.Columns.Count - 1
            lvCampo.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
        Next

    End Sub

    Sub Guardar()

        'Validar seleccion
        If lvCampo.Items.Count = 0 Then
            CSistema.MostrarError("Seleccione un campo correctamente!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim Descripcion, Columna, Campo, TipoCampo As String

        descripcion = lvCampo.SelectedItems(0).Text
        campo = txtSeccion.txt.Text.ToLower & txtID.ObtenerValor.ToString
        columna = txtID.ObtenerValor.ToString
        TipoCampo = "String"

        For i As Integer = 0 To dtCampos.Columns.Count - 1
            If dtCampos.Columns(i).ToString.ToUpper = Descripcion.ToUpper Then
                TipoCampo = dtCampos.Columns(i).DataType.Name
                Exit For
            End If
        Next

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", IDTipoComprobante, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSeccion", IDSeccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ID", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", descripcion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Campo", campo, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Columna", columna, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoCampo", TipoCampo, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", OperacionABM.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpImpresionCampo", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            ctrError.Clear()

            'Actualizamos la tabla
            CData.ResetTable("ImpresionCampo")

        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub Cancelar()

    End Sub

    Sub Eliminar()

    End Sub


    Private Sub frmImpresionSeccion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Guardar()
    End Sub


End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpersion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImpersion))
        Me.gbxCampos = New System.Windows.Forms.GroupBox()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.btnNuevaSeccion = New System.Windows.Forms.ToolStripButton()
        Me.btnEditarSeccion = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminarSeccion = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.btnNuevoCampo = New System.Windows.Forms.ToolStripButton()
        Me.btnEditarCampo = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminarCampo = New System.Windows.Forms.ToolStripButton()
        Me.tvCampos = New System.Windows.Forms.TreeView()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.lblCoordenadaCampo = New System.Windows.Forms.Label()
        Me.btnCancelarCampo = New System.Windows.Forms.Button()
        Me.btnGuardarCampo = New System.Windows.Forms.Button()
        Me.gbxConfiguracion = New System.Windows.Forms.GroupBox()
        Me.rdbOcultar = New System.Windows.Forms.RadioButton()
        Me.rdbMostrar = New System.Windows.Forms.RadioButton()
        Me.nudCampoX = New System.Windows.Forms.NumericUpDown()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.pbxColor = New System.Windows.Forms.PictureBox()
        Me.lklColor = New System.Windows.Forms.LinkLabel()
        Me.chkMantenerJunto = New System.Windows.Forms.CheckBox()
        Me.chkPuedeCrecer = New System.Windows.Forms.CheckBox()
        Me.nudCampoDerecho = New System.Windows.Forms.NumericUpDown()
        Me.nudCampoIzquierdo = New System.Windows.Forms.NumericUpDown()
        Me.lblMargenCampo = New System.Windows.Forms.Label()
        Me.nudCampoAlto = New System.Windows.Forms.NumericUpDown()
        Me.nudCampoAncho = New System.Windows.Forms.NumericUpDown()
        Me.lblTamañoCampo = New System.Windows.Forms.Label()
        Me.nudCampoY = New System.Windows.Forms.NumericUpDown()
        Me.cbxFormato = New ERP.ocxCBX()
        Me.lblFormato = New System.Windows.Forms.Label()
        Me.gbxImpresion = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.nudDuplicadoX = New System.Windows.Forms.NumericUpDown()
        Me.nudDuplicadoY = New System.Windows.Forms.NumericUpDown()
        Me.lblDuplicado = New System.Windows.Forms.Label()
        Me.btnGuardarImpresion = New System.Windows.Forms.Button()
        Me.lblCantidadLineas = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.nudTamañoPaginaAncho = New System.Windows.Forms.NumericUpDown()
        Me.nudTamañoPaginaAlto = New System.Windows.Forms.NumericUpDown()
        Me.lblTamañoPagina = New System.Windows.Forms.Label()
        Me.rdbVertical = New System.Windows.Forms.RadioButton()
        Me.rdbHorizontal = New System.Windows.Forms.RadioButton()
        Me.lblOrientacionPagina = New System.Windows.Forms.Label()
        Me.cbxOperacion = New ERP.ocxCBX()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.crvVista = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.reportDocument1 = New CrystalDecisions.CrystalReports.Engine.ReportDocument()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxCampos.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.gbxConfiguracion.SuspendLayout()
        CType(Me.nudCampoX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbxColor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCampoDerecho, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCampoIzquierdo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCampoAlto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCampoAncho, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudCampoY, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxImpresion.SuspendLayout()
        CType(Me.nudDuplicadoX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDuplicadoY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudTamañoPaginaAncho, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudTamañoPaginaAlto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxCampos
        '
        Me.gbxCampos.Controls.Add(Me.ToolStrip1)
        Me.gbxCampos.Controls.Add(Me.tvCampos)
        Me.gbxCampos.Location = New System.Drawing.Point(492, 18)
        Me.gbxCampos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxCampos.Name = "gbxCampos"
        Me.gbxCampos.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxCampos.Size = New System.Drawing.Size(361, 314)
        Me.gbxCampos.TabIndex = 1
        Me.gbxCampos.TabStop = False
        Me.gbxCampos.Text = "Campos"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.btnNuevaSeccion, Me.btnEditarSeccion, Me.btnEliminarSeccion, Me.ToolStripSeparator2, Me.ToolStripLabel2, Me.btnNuevoCampo, Me.btnEditarCampo, Me.btnEliminarCampo})
        Me.ToolStrip1.Location = New System.Drawing.Point(4, 19)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(353, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(63, 22)
        Me.ToolStripLabel1.Text = "Seccion:"
        '
        'btnNuevaSeccion
        '
        Me.btnNuevaSeccion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevaSeccion.Image = CType(resources.GetObject("btnNuevaSeccion.Image"), System.Drawing.Image)
        Me.btnNuevaSeccion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevaSeccion.Name = "btnNuevaSeccion"
        Me.btnNuevaSeccion.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevaSeccion.Text = "ToolStripButton1"
        Me.btnNuevaSeccion.ToolTipText = "Nueva Seccion"
        '
        'btnEditarSeccion
        '
        Me.btnEditarSeccion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEditarSeccion.Image = CType(resources.GetObject("btnEditarSeccion.Image"), System.Drawing.Image)
        Me.btnEditarSeccion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditarSeccion.Name = "btnEditarSeccion"
        Me.btnEditarSeccion.Size = New System.Drawing.Size(23, 22)
        Me.btnEditarSeccion.Text = "ToolStripButton2"
        Me.btnEditarSeccion.ToolTipText = "Editar Seccion"
        '
        'btnEliminarSeccion
        '
        Me.btnEliminarSeccion.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminarSeccion.Image = CType(resources.GetObject("btnEliminarSeccion.Image"), System.Drawing.Image)
        Me.btnEliminarSeccion.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminarSeccion.Name = "btnEliminarSeccion"
        Me.btnEliminarSeccion.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminarSeccion.Text = "Eliminar Seccion"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(60, 22)
        Me.ToolStripLabel2.Text = "Campo:"
        '
        'btnNuevoCampo
        '
        Me.btnNuevoCampo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevoCampo.Image = CType(resources.GetObject("btnNuevoCampo.Image"), System.Drawing.Image)
        Me.btnNuevoCampo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevoCampo.Name = "btnNuevoCampo"
        Me.btnNuevoCampo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevoCampo.Text = "ToolStripButton1"
        Me.btnNuevoCampo.ToolTipText = "Nuevo Campo"
        '
        'btnEditarCampo
        '
        Me.btnEditarCampo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEditarCampo.Image = CType(resources.GetObject("btnEditarCampo.Image"), System.Drawing.Image)
        Me.btnEditarCampo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditarCampo.Name = "btnEditarCampo"
        Me.btnEditarCampo.Size = New System.Drawing.Size(23, 22)
        Me.btnEditarCampo.Text = "ToolStripButton2"
        Me.btnEditarCampo.ToolTipText = "Editar Campo"
        '
        'btnEliminarCampo
        '
        Me.btnEliminarCampo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminarCampo.Image = CType(resources.GetObject("btnEliminarCampo.Image"), System.Drawing.Image)
        Me.btnEliminarCampo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminarCampo.Name = "btnEliminarCampo"
        Me.btnEliminarCampo.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminarCampo.Text = "ToolStripButton3"
        Me.btnEliminarCampo.ToolTipText = "Eliminar Campo"
        '
        'tvCampos
        '
        Me.tvCampos.ImageIndex = 0
        Me.tvCampos.ImageList = Me.ImageList101
        Me.tvCampos.Location = New System.Drawing.Point(8, 54)
        Me.tvCampos.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tvCampos.Name = "tvCampos"
        Me.tvCampos.SelectedImageIndex = 0
        Me.tvCampos.Size = New System.Drawing.Size(344, 249)
        Me.tvCampos.TabIndex = 0
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "icon.folder.close.gif")
        Me.ImageList101.Images.SetKeyName(1, "icon.folder.open.gif")
        Me.ImageList101.Images.SetKeyName(2, "texto.PNG")
        Me.ImageList101.Images.SetKeyName(3, "numero.PNG")
        Me.ImageList101.Images.SetKeyName(4, "moneda.PNG")
        Me.ImageList101.Images.SetKeyName(5, "fecha.PNG")
        Me.ImageList101.Images.SetKeyName(6, "boolean.PNG")
        '
        'lblCoordenadaCampo
        '
        Me.lblCoordenadaCampo.AutoSize = True
        Me.lblCoordenadaCampo.Location = New System.Drawing.Point(229, 75)
        Me.lblCoordenadaCampo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCoordenadaCampo.Name = "lblCoordenadaCampo"
        Me.lblCoordenadaCampo.Size = New System.Drawing.Size(123, 17)
        Me.lblCoordenadaCampo.TabIndex = 9
        Me.lblCoordenadaCampo.Text = "Coordenadas X/Y:"
        '
        'btnCancelarCampo
        '
        Me.btnCancelarCampo.Location = New System.Drawing.Point(319, 276)
        Me.btnCancelarCampo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancelarCampo.Name = "btnCancelarCampo"
        Me.btnCancelarCampo.Size = New System.Drawing.Size(100, 28)
        Me.btnCancelarCampo.TabIndex = 19
        Me.btnCancelarCampo.Text = "&Cancelar"
        Me.btnCancelarCampo.UseVisualStyleBackColor = True
        '
        'btnGuardarCampo
        '
        Me.btnGuardarCampo.Location = New System.Drawing.Point(211, 276)
        Me.btnGuardarCampo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnGuardarCampo.Name = "btnGuardarCampo"
        Me.btnGuardarCampo.Size = New System.Drawing.Size(100, 28)
        Me.btnGuardarCampo.TabIndex = 18
        Me.btnGuardarCampo.Text = "&Guardar"
        Me.btnGuardarCampo.UseVisualStyleBackColor = True
        '
        'gbxConfiguracion
        '
        Me.gbxConfiguracion.Controls.Add(Me.rdbOcultar)
        Me.gbxConfiguracion.Controls.Add(Me.rdbMostrar)
        Me.gbxConfiguracion.Controls.Add(Me.nudCampoX)
        Me.gbxConfiguracion.Controls.Add(Me.cbxTipo)
        Me.gbxConfiguracion.Controls.Add(Me.lblTipo)
        Me.gbxConfiguracion.Controls.Add(Me.pbxColor)
        Me.gbxConfiguracion.Controls.Add(Me.lklColor)
        Me.gbxConfiguracion.Controls.Add(Me.chkMantenerJunto)
        Me.gbxConfiguracion.Controls.Add(Me.chkPuedeCrecer)
        Me.gbxConfiguracion.Controls.Add(Me.nudCampoDerecho)
        Me.gbxConfiguracion.Controls.Add(Me.nudCampoIzquierdo)
        Me.gbxConfiguracion.Controls.Add(Me.lblMargenCampo)
        Me.gbxConfiguracion.Controls.Add(Me.nudCampoAlto)
        Me.gbxConfiguracion.Controls.Add(Me.nudCampoAncho)
        Me.gbxConfiguracion.Controls.Add(Me.lblTamañoCampo)
        Me.gbxConfiguracion.Controls.Add(Me.nudCampoY)
        Me.gbxConfiguracion.Controls.Add(Me.cbxFormato)
        Me.gbxConfiguracion.Controls.Add(Me.btnGuardarCampo)
        Me.gbxConfiguracion.Controls.Add(Me.lblFormato)
        Me.gbxConfiguracion.Controls.Add(Me.btnCancelarCampo)
        Me.gbxConfiguracion.Controls.Add(Me.lblCoordenadaCampo)
        Me.gbxConfiguracion.Location = New System.Drawing.Point(861, 18)
        Me.gbxConfiguracion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxConfiguracion.Name = "gbxConfiguracion"
        Me.gbxConfiguracion.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxConfiguracion.Size = New System.Drawing.Size(437, 314)
        Me.gbxConfiguracion.TabIndex = 0
        Me.gbxConfiguracion.TabStop = False
        Me.gbxConfiguracion.Text = "Configuracion"
        '
        'rdbOcultar
        '
        Me.rdbOcultar.AutoSize = True
        Me.rdbOcultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbOcultar.Location = New System.Drawing.Point(108, 33)
        Me.rdbOcultar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.rdbOcultar.Name = "rdbOcultar"
        Me.rdbOcultar.Size = New System.Drawing.Size(75, 21)
        Me.rdbOcultar.TabIndex = 1
        Me.rdbOcultar.TabStop = True
        Me.rdbOcultar.Text = "Ocultar"
        Me.rdbOcultar.UseVisualStyleBackColor = True
        '
        'rdbMostrar
        '
        Me.rdbMostrar.AutoSize = True
        Me.rdbMostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbMostrar.Location = New System.Drawing.Point(20, 33)
        Me.rdbMostrar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.rdbMostrar.Name = "rdbMostrar"
        Me.rdbMostrar.Size = New System.Drawing.Size(77, 21)
        Me.rdbMostrar.TabIndex = 0
        Me.rdbMostrar.TabStop = True
        Me.rdbMostrar.Text = "Mostrar"
        Me.rdbMostrar.UseVisualStyleBackColor = True
        '
        'nudCampoX
        '
        Me.nudCampoX.Location = New System.Drawing.Point(229, 96)
        Me.nudCampoX.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudCampoX.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudCampoX.Name = "nudCampoX"
        Me.nudCampoX.Size = New System.Drawing.Size(95, 22)
        Me.nudCampoX.TabIndex = 10
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.FormABM = "frmImpresionTipo"
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(20, 144)
        Me.cbxTipo.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(189, 26)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 5
        Me.cbxTipo.Texto = ""
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(20, 124)
        Me.lblTipo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(40, 17)
        Me.lblTipo.TabIndex = 4
        Me.lblTipo.Text = "Tipo:"
        '
        'pbxColor
        '
        Me.pbxColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pbxColor.Location = New System.Drawing.Point(20, 193)
        Me.pbxColor.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pbxColor.Name = "pbxColor"
        Me.pbxColor.Size = New System.Drawing.Size(139, 24)
        Me.pbxColor.TabIndex = 36
        Me.pbxColor.TabStop = False
        '
        'lklColor
        '
        Me.lklColor.AutoSize = True
        Me.lklColor.Location = New System.Drawing.Point(20, 174)
        Me.lklColor.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lklColor.Name = "lklColor"
        Me.lklColor.Size = New System.Drawing.Size(45, 17)
        Me.lklColor.TabIndex = 6
        Me.lklColor.TabStop = True
        Me.lklColor.Text = "Color:"
        '
        'chkMantenerJunto
        '
        Me.chkMantenerJunto.AutoSize = True
        Me.chkMantenerJunto.Location = New System.Drawing.Point(20, 247)
        Me.chkMantenerJunto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.chkMantenerJunto.Name = "chkMantenerJunto"
        Me.chkMantenerJunto.Size = New System.Drawing.Size(125, 21)
        Me.chkMantenerJunto.TabIndex = 8
        Me.chkMantenerJunto.Text = "Mantener junto"
        Me.chkMantenerJunto.UseVisualStyleBackColor = True
        '
        'chkPuedeCrecer
        '
        Me.chkPuedeCrecer.AutoSize = True
        Me.chkPuedeCrecer.Location = New System.Drawing.Point(20, 223)
        Me.chkPuedeCrecer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.chkPuedeCrecer.Name = "chkPuedeCrecer"
        Me.chkPuedeCrecer.Size = New System.Drawing.Size(115, 21)
        Me.chkPuedeCrecer.TabIndex = 7
        Me.chkPuedeCrecer.Text = "Puede crecer"
        Me.chkPuedeCrecer.UseVisualStyleBackColor = True
        '
        'nudCampoDerecho
        '
        Me.nudCampoDerecho.Location = New System.Drawing.Point(324, 193)
        Me.nudCampoDerecho.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudCampoDerecho.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudCampoDerecho.Name = "nudCampoDerecho"
        Me.nudCampoDerecho.Size = New System.Drawing.Size(95, 22)
        Me.nudCampoDerecho.TabIndex = 17
        '
        'nudCampoIzquierdo
        '
        Me.nudCampoIzquierdo.Location = New System.Drawing.Point(229, 193)
        Me.nudCampoIzquierdo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudCampoIzquierdo.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudCampoIzquierdo.Name = "nudCampoIzquierdo"
        Me.nudCampoIzquierdo.Size = New System.Drawing.Size(95, 22)
        Me.nudCampoIzquierdo.TabIndex = 16
        '
        'lblMargenCampo
        '
        Me.lblMargenCampo.AutoSize = True
        Me.lblMargenCampo.Location = New System.Drawing.Point(229, 174)
        Me.lblMargenCampo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblMargenCampo.Name = "lblMargenCampo"
        Me.lblMargenCampo.Size = New System.Drawing.Size(159, 17)
        Me.lblMargenCampo.TabIndex = 15
        Me.lblMargenCampo.Text = "Margen Izquiedo/Arriba:"
        '
        'nudCampoAlto
        '
        Me.nudCampoAlto.Location = New System.Drawing.Point(324, 144)
        Me.nudCampoAlto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudCampoAlto.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudCampoAlto.Name = "nudCampoAlto"
        Me.nudCampoAlto.Size = New System.Drawing.Size(95, 22)
        Me.nudCampoAlto.TabIndex = 14
        '
        'nudCampoAncho
        '
        Me.nudCampoAncho.Location = New System.Drawing.Point(229, 144)
        Me.nudCampoAncho.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudCampoAncho.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudCampoAncho.Name = "nudCampoAncho"
        Me.nudCampoAncho.Size = New System.Drawing.Size(95, 22)
        Me.nudCampoAncho.TabIndex = 13
        '
        'lblTamañoCampo
        '
        Me.lblTamañoCampo.AutoSize = True
        Me.lblTamañoCampo.Location = New System.Drawing.Point(229, 124)
        Me.lblTamañoCampo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTamañoCampo.Name = "lblTamañoCampo"
        Me.lblTamañoCampo.Size = New System.Drawing.Size(136, 17)
        Me.lblTamañoCampo.TabIndex = 12
        Me.lblTamañoCampo.Text = "Tamaño Ancho/Alto:"
        '
        'nudCampoY
        '
        Me.nudCampoY.Location = New System.Drawing.Point(324, 96)
        Me.nudCampoY.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudCampoY.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudCampoY.Name = "nudCampoY"
        Me.nudCampoY.Size = New System.Drawing.Size(95, 22)
        Me.nudCampoY.TabIndex = 11
        '
        'cbxFormato
        '
        Me.cbxFormato.CampoWhere = Nothing
        Me.cbxFormato.CargarUnaSolaVez = False
        Me.cbxFormato.DataDisplayMember = "Descripcion"
        Me.cbxFormato.DataFilter = Nothing
        Me.cbxFormato.DataOrderBy = Nothing
        Me.cbxFormato.DataSource = "ImpresionFormato"
        Me.cbxFormato.DataValueMember = "ID"
        Me.cbxFormato.FormABM = "frmImpresionFormato"
        Me.cbxFormato.Indicaciones = Nothing
        Me.cbxFormato.Location = New System.Drawing.Point(20, 96)
        Me.cbxFormato.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxFormato.Name = "cbxFormato"
        Me.cbxFormato.SeleccionObligatoria = False
        Me.cbxFormato.Size = New System.Drawing.Size(189, 26)
        Me.cbxFormato.SoloLectura = False
        Me.cbxFormato.TabIndex = 3
        Me.cbxFormato.Texto = ""
        '
        'lblFormato
        '
        Me.lblFormato.AutoSize = True
        Me.lblFormato.Location = New System.Drawing.Point(20, 75)
        Me.lblFormato.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFormato.Name = "lblFormato"
        Me.lblFormato.Size = New System.Drawing.Size(64, 17)
        Me.lblFormato.TabIndex = 2
        Me.lblFormato.Text = "Formato:"
        '
        'gbxImpresion
        '
        Me.gbxImpresion.Controls.Add(Me.Label3)
        Me.gbxImpresion.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxImpresion.Controls.Add(Me.Label2)
        Me.gbxImpresion.Controls.Add(Me.nudDuplicadoX)
        Me.gbxImpresion.Controls.Add(Me.nudDuplicadoY)
        Me.gbxImpresion.Controls.Add(Me.lblDuplicado)
        Me.gbxImpresion.Controls.Add(Me.btnGuardarImpresion)
        Me.gbxImpresion.Controls.Add(Me.lblCantidadLineas)
        Me.gbxImpresion.Controls.Add(Me.txtCantidad)
        Me.gbxImpresion.Controls.Add(Me.nudTamañoPaginaAncho)
        Me.gbxImpresion.Controls.Add(Me.nudTamañoPaginaAlto)
        Me.gbxImpresion.Controls.Add(Me.lblTamañoPagina)
        Me.gbxImpresion.Controls.Add(Me.rdbVertical)
        Me.gbxImpresion.Controls.Add(Me.rdbHorizontal)
        Me.gbxImpresion.Controls.Add(Me.lblOrientacionPagina)
        Me.gbxImpresion.Controls.Add(Me.cbxOperacion)
        Me.gbxImpresion.Location = New System.Drawing.Point(17, 18)
        Me.gbxImpresion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxImpresion.Name = "gbxImpresion"
        Me.gbxImpresion.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxImpresion.Size = New System.Drawing.Size(469, 314)
        Me.gbxImpresion.TabIndex = 0
        Me.gbxImpresion.TabStop = False
        Me.gbxImpresion.Text = "Documento"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(25, 85)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(149, 17)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Tipo de Comprobante:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = "VTipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.FormABM = "frmTipoComprobante"
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(29, 106)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(212, 26)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 3
        Me.cbxTipoComprobante.Texto = "REMISIONES EMITIDAS"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 26)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 17)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Operacion:"
        '
        'nudDuplicadoX
        '
        Me.nudDuplicadoX.Location = New System.Drawing.Point(249, 106)
        Me.nudDuplicadoX.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudDuplicadoX.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudDuplicadoX.Name = "nudDuplicadoX"
        Me.nudDuplicadoX.Size = New System.Drawing.Size(95, 22)
        Me.nudDuplicadoX.TabIndex = 11
        '
        'nudDuplicadoY
        '
        Me.nudDuplicadoY.Location = New System.Drawing.Point(344, 106)
        Me.nudDuplicadoY.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudDuplicadoY.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudDuplicadoY.Name = "nudDuplicadoY"
        Me.nudDuplicadoY.Size = New System.Drawing.Size(95, 22)
        Me.nudDuplicadoY.TabIndex = 12
        '
        'lblDuplicado
        '
        Me.lblDuplicado.AutoSize = True
        Me.lblDuplicado.Location = New System.Drawing.Point(249, 85)
        Me.lblDuplicado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblDuplicado.Name = "lblDuplicado"
        Me.lblDuplicado.Size = New System.Drawing.Size(190, 17)
        Me.lblDuplicado.TabIndex = 10
        Me.lblDuplicado.Text = "Coordenadas Duplicado X/Y:"
        '
        'btnGuardarImpresion
        '
        Me.btnGuardarImpresion.Location = New System.Drawing.Point(29, 276)
        Me.btnGuardarImpresion.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnGuardarImpresion.Name = "btnGuardarImpresion"
        Me.btnGuardarImpresion.Size = New System.Drawing.Size(100, 28)
        Me.btnGuardarImpresion.TabIndex = 15
        Me.btnGuardarImpresion.Text = "&Guardar"
        Me.btnGuardarImpresion.UseVisualStyleBackColor = True
        '
        'lblCantidadLineas
        '
        Me.lblCantidadLineas.AutoSize = True
        Me.lblCantidadLineas.Location = New System.Drawing.Point(249, 144)
        Me.lblCantidadLineas.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCantidadLineas.Name = "lblCantidadLineas"
        Me.lblCantidadLineas.Size = New System.Drawing.Size(217, 17)
        Me.lblCantidadLineas.TabIndex = 13
        Me.lblCantidadLineas.Text = "Cantidad de Lineas en el Detalle:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(255, 166)
        Me.txtCantidad.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(45, 27)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 14
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'nudTamañoPaginaAncho
        '
        Me.nudTamañoPaginaAncho.Location = New System.Drawing.Point(249, 47)
        Me.nudTamañoPaginaAncho.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudTamañoPaginaAncho.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudTamañoPaginaAncho.Name = "nudTamañoPaginaAncho"
        Me.nudTamañoPaginaAncho.Size = New System.Drawing.Size(95, 22)
        Me.nudTamañoPaginaAncho.TabIndex = 8
        '
        'nudTamañoPaginaAlto
        '
        Me.nudTamañoPaginaAlto.Location = New System.Drawing.Point(344, 47)
        Me.nudTamañoPaginaAlto.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.nudTamañoPaginaAlto.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudTamañoPaginaAlto.Name = "nudTamañoPaginaAlto"
        Me.nudTamañoPaginaAlto.Size = New System.Drawing.Size(95, 22)
        Me.nudTamañoPaginaAlto.TabIndex = 9
        '
        'lblTamañoPagina
        '
        Me.lblTamañoPagina.AutoSize = True
        Me.lblTamañoPagina.Location = New System.Drawing.Point(249, 26)
        Me.lblTamañoPagina.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTamañoPagina.Name = "lblTamañoPagina"
        Me.lblTamañoPagina.Size = New System.Drawing.Size(204, 17)
        Me.lblTamañoPagina.TabIndex = 7
        Me.lblTamañoPagina.Text = "Tamaño de Pagina Ancho/Alto:"
        '
        'rdbVertical
        '
        Me.rdbVertical.AutoSize = True
        Me.rdbVertical.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbVertical.Location = New System.Drawing.Point(33, 197)
        Me.rdbVertical.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.rdbVertical.Name = "rdbVertical"
        Me.rdbVertical.Size = New System.Drawing.Size(76, 21)
        Me.rdbVertical.TabIndex = 6
        Me.rdbVertical.TabStop = True
        Me.rdbVertical.Text = "Vertical"
        Me.rdbVertical.UseVisualStyleBackColor = True
        '
        'rdbHorizontal
        '
        Me.rdbHorizontal.AutoSize = True
        Me.rdbHorizontal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbHorizontal.Location = New System.Drawing.Point(33, 169)
        Me.rdbHorizontal.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.rdbHorizontal.Name = "rdbHorizontal"
        Me.rdbHorizontal.Size = New System.Drawing.Size(93, 21)
        Me.rdbHorizontal.TabIndex = 5
        Me.rdbHorizontal.TabStop = True
        Me.rdbHorizontal.Text = "Horizontal"
        Me.rdbHorizontal.UseVisualStyleBackColor = True
        '
        'lblOrientacionPagina
        '
        Me.lblOrientacionPagina.AutoSize = True
        Me.lblOrientacionPagina.Location = New System.Drawing.Point(28, 144)
        Me.lblOrientacionPagina.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOrientacionPagina.Name = "lblOrientacionPagina"
        Me.lblOrientacionPagina.Size = New System.Drawing.Size(168, 17)
        Me.lblOrientacionPagina.TabIndex = 4
        Me.lblOrientacionPagina.Text = "Orientacion de la Pagina:"
        '
        'cbxOperacion
        '
        Me.cbxOperacion.CampoWhere = Nothing
        Me.cbxOperacion.CargarUnaSolaVez = False
        Me.cbxOperacion.DataDisplayMember = "Descripcion"
        Me.cbxOperacion.DataFilter = Nothing
        Me.cbxOperacion.DataOrderBy = Nothing
        Me.cbxOperacion.DataSource = "VOperacion"
        Me.cbxOperacion.DataValueMember = "ID"
        Me.cbxOperacion.FormABM = "frmOperacion"
        Me.cbxOperacion.Indicaciones = Nothing
        Me.cbxOperacion.Location = New System.Drawing.Point(29, 46)
        Me.cbxOperacion.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.SeleccionObligatoria = False
        Me.cbxOperacion.Size = New System.Drawing.Size(212, 26)
        Me.cbxOperacion.SoloLectura = False
        Me.cbxOperacion.TabIndex = 1
        Me.cbxOperacion.Texto = "VENTAS CLIENTES"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.StatusStrip1, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel3, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.crvVista, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 411.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1312, 892)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 870)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(1312, 22)
        Me.StatusStrip1.TabIndex = 23
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.GroupBox1)
        Me.Panel3.Controls.Add(Me.gbxImpresion)
        Me.Panel3.Controls.Add(Me.gbxCampos)
        Me.Panel3.Controls.Add(Me.gbxConfiguracion)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(4, 4)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1304, 403)
        Me.Panel3.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 340)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(1281, 55)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(103, 20)
        Me.NumericUpDown1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(95, 22)
        Me.NumericUpDown1.TabIndex = 21
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.Location = New System.Drawing.Point(197, 20)
        Me.NumericUpDown2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(95, 22)
        Me.NumericUpDown2.TabIndex = 22
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 25)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 17)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Mover todo a:"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(1055, 20)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(208, 28)
        Me.Button2.TabIndex = 19
        Me.Button2.Text = "Visualizar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'crvVista
        '
        Me.crvVista.ActiveViewIndex = -1
        Me.crvVista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvVista.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvVista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvVista.Location = New System.Drawing.Point(4, 415)
        Me.crvVista.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.crvVista.Name = "crvVista"
        Me.crvVista.ShowCloseButton = False
        Me.crvVista.ShowGotoPageButton = False
        Me.crvVista.ShowGroupTreeButton = False
        Me.crvVista.ShowPageNavigateButtons = False
        Me.crvVista.ShowParameterPanelButton = False
        Me.crvVista.ShowTextSearchButton = False
        Me.crvVista.Size = New System.Drawing.Size(1304, 448)
        Me.crvVista.TabIndex = 24
        Me.crvVista.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'frmImpersion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1312, 892)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmImpersion"
        Me.Text = "frmImpersion"
        Me.gbxCampos.ResumeLayout(False)
        Me.gbxCampos.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.gbxConfiguracion.ResumeLayout(False)
        Me.gbxConfiguracion.PerformLayout()
        CType(Me.nudCampoX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbxColor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCampoDerecho, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCampoIzquierdo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCampoAlto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCampoAncho, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudCampoY, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxImpresion.ResumeLayout(False)
        Me.gbxImpresion.PerformLayout()
        CType(Me.nudDuplicadoX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDuplicadoY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudTamañoPaginaAncho, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudTamañoPaginaAlto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbxOperacion As ERP.ocxCBX
    Friend WithEvents gbxCampos As System.Windows.Forms.GroupBox
    Friend WithEvents cbxFormato As ERP.ocxCBX
    Friend WithEvents btnCancelarCampo As System.Windows.Forms.Button
    Friend WithEvents btnGuardarCampo As System.Windows.Forms.Button
    Friend WithEvents lblCoordenadaCampo As System.Windows.Forms.Label
    Friend WithEvents gbxConfiguracion As System.Windows.Forms.GroupBox
    Friend WithEvents nudCampoY As System.Windows.Forms.NumericUpDown
    Friend WithEvents gbxImpresion As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents reportDocument1 As CrystalDecisions.CrystalReports.Engine.ReportDocument
    Friend WithEvents lblTamañoCampo As System.Windows.Forms.Label
    Friend WithEvents tvCampos As System.Windows.Forms.TreeView
    Friend WithEvents nudCampoDerecho As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudCampoIzquierdo As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMargenCampo As System.Windows.Forms.Label
    Friend WithEvents nudCampoAlto As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudCampoAncho As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents pbxColor As System.Windows.Forms.PictureBox
    Friend WithEvents lklColor As System.Windows.Forms.LinkLabel
    Friend WithEvents chkMantenerJunto As System.Windows.Forms.CheckBox
    Friend WithEvents chkPuedeCrecer As System.Windows.Forms.CheckBox
    Friend WithEvents nudCampoX As System.Windows.Forms.NumericUpDown
    Friend WithEvents rdbOcultar As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMostrar As System.Windows.Forms.RadioButton
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents lblFormato As System.Windows.Forms.Label
    Friend WithEvents nudTamañoPaginaAncho As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudTamañoPaginaAlto As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblTamañoPagina As System.Windows.Forms.Label
    Friend WithEvents rdbVertical As System.Windows.Forms.RadioButton
    Friend WithEvents rdbHorizontal As System.Windows.Forms.RadioButton
    Friend WithEvents lblOrientacionPagina As System.Windows.Forms.Label
    Friend WithEvents lblCantidadLineas As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents btnGuardarImpresion As System.Windows.Forms.Button
    Friend WithEvents nudDuplicadoX As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudDuplicadoY As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDuplicado As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents crvVista As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ImageList101 As System.Windows.Forms.ImageList
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnNuevaSeccion As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEditarSeccion As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEliminarSeccion As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents btnNuevoCampo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEditarCampo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEliminarCampo As System.Windows.Forms.ToolStripButton

End Class

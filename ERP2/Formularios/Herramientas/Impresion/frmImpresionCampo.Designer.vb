﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpresionCampo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImpresionCampo))
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblTipoComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.lblSeccion = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lvCampo = New System.Windows.Forms.ListView()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.lblCampo = New System.Windows.Forms.Label()
        Me.txtSeccion = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtTipoComprobante = New ERP.ocxTXTString()
        Me.txtOperacion = New ERP.ocxTXTString()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(16, 118)
        Me.lblID.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(25, 17)
        Me.lblID.TabIndex = 6
        Me.lblID.Text = "ID:"
        '
        'lblTipoComprobante
        '
        Me.lblTipoComprobante.AutoSize = True
        Me.lblTipoComprobante.Location = New System.Drawing.Point(16, 52)
        Me.lblTipoComprobante.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTipoComprobante.Name = "lblTipoComprobante"
        Me.lblTipoComprobante.Size = New System.Drawing.Size(74, 17)
        Me.lblTipoComprobante.TabIndex = 2
        Me.lblTipoComprobante.Text = "T. Compr.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(16, 20)
        Me.lblOperacion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(78, 17)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'lblSeccion
        '
        Me.lblSeccion.AutoSize = True
        Me.lblSeccion.Location = New System.Drawing.Point(16, 85)
        Me.lblSeccion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblSeccion.Name = "lblSeccion"
        Me.lblSeccion.Size = New System.Drawing.Size(62, 17)
        Me.lblSeccion.TabIndex = 4
        Me.lblSeccion.Text = "Seccion:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 381)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(432, 25)
        Me.StatusStrip1.TabIndex = 13
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(57, 20)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(209, 330)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(100, 28)
        Me.btnCancelar.TabIndex = 11
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(317, 330)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(100, 28)
        Me.btnEliminar.TabIndex = 12
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(101, 330)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(100, 28)
        Me.btnGuardar.TabIndex = 10
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'lvCampo
        '
        Me.lvCampo.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvCampo.HideSelection = False
        Me.lvCampo.LargeImageList = Me.ImageList101
        Me.lvCampo.Location = New System.Drawing.Point(101, 148)
        Me.lvCampo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.lvCampo.MultiSelect = False
        Me.lvCampo.Name = "lvCampo"
        Me.lvCampo.Size = New System.Drawing.Size(315, 174)
        Me.lvCampo.SmallImageList = Me.ImageList101
        Me.lvCampo.TabIndex = 9
        Me.lvCampo.UseCompatibleStateImageBehavior = False
        Me.lvCampo.View = System.Windows.Forms.View.Details
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "texto.PNG")
        Me.ImageList101.Images.SetKeyName(1, "numero.PNG")
        Me.ImageList101.Images.SetKeyName(2, "moneda.PNG")
        Me.ImageList101.Images.SetKeyName(3, "fecha.PNG")
        Me.ImageList101.Images.SetKeyName(4, "boolean.PNG")
        '
        'lblCampo
        '
        Me.lblCampo.AutoSize = True
        Me.lblCampo.Location = New System.Drawing.Point(16, 148)
        Me.lblCampo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblCampo.Name = "lblCampo"
        Me.lblCampo.Size = New System.Drawing.Size(63, 17)
        Me.lblCampo.TabIndex = 8
        Me.lblCampo.Text = "Campos:"
        '
        'txtSeccion
        '
        Me.txtSeccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSeccion.Color = System.Drawing.Color.Empty
        Me.txtSeccion.Indicaciones = Nothing
        Me.txtSeccion.Location = New System.Drawing.Point(101, 80)
        Me.txtSeccion.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.txtSeccion.Multilinea = False
        Me.txtSeccion.Name = "txtSeccion"
        Me.txtSeccion.Size = New System.Drawing.Size(316, 26)
        Me.txtSeccion.SoloLectura = True
        Me.txtSeccion.TabIndex = 5
        Me.txtSeccion.TabStop = False
        Me.txtSeccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSeccion.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(101, 113)
        Me.txtID.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(55, 27)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 7
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtTipoComprobante
        '
        Me.txtTipoComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoComprobante.Color = System.Drawing.Color.Empty
        Me.txtTipoComprobante.Indicaciones = Nothing
        Me.txtTipoComprobante.Location = New System.Drawing.Point(101, 47)
        Me.txtTipoComprobante.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.txtTipoComprobante.Multilinea = False
        Me.txtTipoComprobante.Name = "txtTipoComprobante"
        Me.txtTipoComprobante.Size = New System.Drawing.Size(316, 26)
        Me.txtTipoComprobante.SoloLectura = True
        Me.txtTipoComprobante.TabIndex = 3
        Me.txtTipoComprobante.TabStop = False
        Me.txtTipoComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoComprobante.Texto = ""
        '
        'txtOperacion
        '
        Me.txtOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(101, 15)
        Me.txtOperacion.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.txtOperacion.Multilinea = False
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(316, 26)
        Me.txtOperacion.SoloLectura = True
        Me.txtOperacion.TabIndex = 1
        Me.txtOperacion.TabStop = False
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtOperacion.Texto = ""
        '
        'frmImpresionCampo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(432, 406)
        Me.Controls.Add(Me.lblCampo)
        Me.Controls.Add(Me.lvCampo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.txtSeccion)
        Me.Controls.Add(Me.lblSeccion)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.txtTipoComprobante)
        Me.Controls.Add(Me.txtOperacion)
        Me.Controls.Add(Me.lblTipoComprobante)
        Me.Controls.Add(Me.lblOperacion)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmImpresionCampo"
        Me.Text = "frmImpresionCampo"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtTipoComprobante As ERP.ocxTXTString
    Friend WithEvents txtOperacion As ERP.ocxTXTString
    Friend WithEvents lblTipoComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtSeccion As ERP.ocxTXTString
    Friend WithEvents lblSeccion As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblCampo As System.Windows.Forms.Label
    Friend WithEvents lvCampo As System.Windows.Forms.ListView
    Friend WithEvents ImageList101 As System.Windows.Forms.ImageList
End Class

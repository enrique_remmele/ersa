﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpresionSeccion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblSeccion = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.lblTipoComprobante = New System.Windows.Forms.Label()
        Me.lblAlto = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.lblTabla = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.cbxTabla = New ERP.ocxCBX()
        Me.txtTipoComprobante = New ERP.ocxTXTString()
        Me.txtOperacion = New ERP.ocxTXTString()
        Me.txtAlto = New ERP.ocxTXTNumeric()
        Me.cbxSeccion = New ERP.ocxCBX()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblSeccion
        '
        Me.lblSeccion.AutoSize = True
        Me.lblSeccion.Location = New System.Drawing.Point(12, 99)
        Me.lblSeccion.Name = "lblSeccion"
        Me.lblSeccion.Size = New System.Drawing.Size(49, 13)
        Me.lblSeccion.TabIndex = 6
        Me.lblSeccion.Text = "Seccion:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(12, 19)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'lblTipoComprobante
        '
        Me.lblTipoComprobante.AutoSize = True
        Me.lblTipoComprobante.Location = New System.Drawing.Point(12, 45)
        Me.lblTipoComprobante.Name = "lblTipoComprobante"
        Me.lblTipoComprobante.Size = New System.Drawing.Size(56, 13)
        Me.lblTipoComprobante.TabIndex = 2
        Me.lblTipoComprobante.Text = "T. Compr.:"
        '
        'lblAlto
        '
        Me.lblAlto.AutoSize = True
        Me.lblAlto.Location = New System.Drawing.Point(12, 128)
        Me.lblAlto.Name = "lblAlto"
        Me.lblAlto.Size = New System.Drawing.Size(28, 13)
        Me.lblAlto.TabIndex = 8
        Me.lblAlto.Text = "Alto:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 226)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(321, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(43, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(157, 187)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 13
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(238, 187)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 14
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(76, 187)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 12
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lblTabla
        '
        Me.lblTabla.AutoSize = True
        Me.lblTabla.Location = New System.Drawing.Point(12, 155)
        Me.lblTabla.Name = "lblTabla"
        Me.lblTabla.Size = New System.Drawing.Size(37, 13)
        Me.lblTabla.TabIndex = 10
        Me.lblTabla.Text = "Tabla:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "ID:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(76, 68)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(41, 22)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 5
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'cbxTabla
        '
        Me.cbxTabla.DataDisplayMember = ""
        Me.cbxTabla.DataFilter = ""
        Me.cbxTabla.DataSource = ""
        Me.cbxTabla.DataValueMember = ""
        Me.cbxTabla.FormABM = Nothing
        Me.cbxTabla.Indicaciones = Nothing
        Me.cbxTabla.Location = New System.Drawing.Point(76, 151)
        Me.cbxTabla.Name = "cbxTabla"
        Me.cbxTabla.SeleccionObligatoria = False
        Me.cbxTabla.Size = New System.Drawing.Size(237, 21)
        Me.cbxTabla.SoloLectura = False
        Me.cbxTabla.TabIndex = 11
        Me.cbxTabla.Texto = "VClienteSucursal"
        '
        'txtTipoComprobante
        '
        Me.txtTipoComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoComprobante.Color = System.Drawing.Color.Empty
        Me.txtTipoComprobante.Indicaciones = Nothing
        Me.txtTipoComprobante.Location = New System.Drawing.Point(76, 41)
        Me.txtTipoComprobante.Name = "txtTipoComprobante"
        Me.txtTipoComprobante.Size = New System.Drawing.Size(237, 21)
        Me.txtTipoComprobante.SoloLectura = True
        Me.txtTipoComprobante.TabIndex = 3
        Me.txtTipoComprobante.TabStop = False
        Me.txtTipoComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoComprobante.Texto = ""
        '
        'txtOperacion
        '
        Me.txtOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOperacion.Color = System.Drawing.Color.Empty
        Me.txtOperacion.Indicaciones = Nothing
        Me.txtOperacion.Location = New System.Drawing.Point(76, 15)
        Me.txtOperacion.Name = "txtOperacion"
        Me.txtOperacion.Size = New System.Drawing.Size(237, 21)
        Me.txtOperacion.SoloLectura = True
        Me.txtOperacion.TabIndex = 1
        Me.txtOperacion.TabStop = False
        Me.txtOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtOperacion.Texto = ""
        '
        'txtAlto
        '
        Me.txtAlto.Color = System.Drawing.Color.Empty
        Me.txtAlto.Decimales = True
        Me.txtAlto.Indicaciones = Nothing
        Me.txtAlto.Location = New System.Drawing.Point(76, 123)
        Me.txtAlto.Name = "txtAlto"
        Me.txtAlto.Size = New System.Drawing.Size(103, 22)
        Me.txtAlto.SoloLectura = False
        Me.txtAlto.TabIndex = 9
        Me.txtAlto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAlto.Texto = "0"
        '
        'cbxSeccion
        '
        Me.cbxSeccion.DataDisplayMember = Nothing
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataSource = Nothing
        Me.cbxSeccion.DataValueMember = Nothing
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(76, 96)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(237, 21)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 7
        Me.cbxSeccion.Texto = ""
        '
        'frmImpresionSeccion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(321, 248)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxTabla)
        Me.Controls.Add(Me.lblTabla)
        Me.Controls.Add(Me.txtTipoComprobante)
        Me.Controls.Add(Me.txtOperacion)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblAlto)
        Me.Controls.Add(Me.txtAlto)
        Me.Controls.Add(Me.lblTipoComprobante)
        Me.Controls.Add(Me.lblOperacion)
        Me.Controls.Add(Me.cbxSeccion)
        Me.Controls.Add(Me.lblSeccion)
        Me.Name = "frmImpresionSeccion"
        Me.Text = "frmImpresionSeccion"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxSeccion As ERP.ocxCBX
    Friend WithEvents lblSeccion As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents lblTipoComprobante As System.Windows.Forms.Label
    Friend WithEvents txtAlto As ERP.ocxTXTNumeric
    Friend WithEvents lblAlto As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtTipoComprobante As ERP.ocxTXTString
    Friend WithEvents txtOperacion As ERP.ocxTXTString
    Friend WithEvents cbxTabla As ERP.ocxCBX
    Friend WithEvents lblTabla As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

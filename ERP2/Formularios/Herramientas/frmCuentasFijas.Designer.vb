﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuentaFija
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.gbxLista = New System.Windows.Forms.GroupBox()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lblDebeHaber = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.chkBuscarEnProducto = New System.Windows.Forms.CheckBox()
        Me.chkBuscarEnClienteProveedor = New System.Windows.Forms.CheckBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.chkSiEsVenta = New System.Windows.Forms.CheckBox()
        Me.rdbContado = New System.Windows.Forms.RadioButton()
        Me.rdbCredito = New System.Windows.Forms.RadioButton()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxDatos = New System.Windows.Forms.GroupBox()
        Me.txtOrden = New ERP.ocxTXTNumeric()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.lblID = New System.Windows.Forms.Label()
        Me.cbxDebeHaber = New ERP.ocxCBX()
        Me.txtCuenta = New ERP.ocxTXTCuentaContable()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.cbxOperacion = New ERP.ocxCBX()
        Me.cbxTipoOperacion = New ERP.ocxCBX()
        Me.lblTipoOperacion = New System.Windows.Forms.Label()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxLista.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxDatos.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 422)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(738, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(12, 9)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'gbxLista
        '
        Me.gbxLista.Controls.Add(Me.lvLista)
        Me.gbxLista.Location = New System.Drawing.Point(12, 52)
        Me.gbxLista.Name = "gbxLista"
        Me.gbxLista.Size = New System.Drawing.Size(726, 209)
        Me.gbxLista.TabIndex = 2
        Me.gbxLista.TabStop = False
        '
        'lvLista
        '
        Me.lvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLista.Location = New System.Drawing.Point(3, 16)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(720, 190)
        Me.lvLista.TabIndex = 0
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lblDebeHaber
        '
        Me.lblDebeHaber.AutoSize = True
        Me.lblDebeHaber.Location = New System.Drawing.Point(308, 16)
        Me.lblDebeHaber.Name = "lblDebeHaber"
        Me.lblDebeHaber.Size = New System.Drawing.Size(55, 13)
        Me.lblDebeHaber.TabIndex = 4
        Me.lblDebeHaber.Text = "Deb/Hab."
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(47, 16)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(28, 13)
        Me.lblTipo.TabIndex = 2
        Me.lblTipo.Text = "Tipo"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(405, 16)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(41, 13)
        Me.lblCuenta.TabIndex = 6
        Me.lblCuenta.Text = "Cuenta"
        '
        'chkBuscarEnProducto
        '
        Me.chkBuscarEnProducto.AutoSize = True
        Me.chkBuscarEnProducto.Location = New System.Drawing.Point(9, 62)
        Me.chkBuscarEnProducto.Name = "chkBuscarEnProducto"
        Me.chkBuscarEnProducto.Size = New System.Drawing.Size(131, 17)
        Me.chkBuscarEnProducto.TabIndex = 10
        Me.chkBuscarEnProducto.Text = "Buscar en el Producto"
        Me.chkBuscarEnProducto.UseVisualStyleBackColor = True
        '
        'chkBuscarEnClienteProveedor
        '
        Me.chkBuscarEnClienteProveedor.AutoSize = True
        Me.chkBuscarEnClienteProveedor.Location = New System.Drawing.Point(9, 85)
        Me.chkBuscarEnClienteProveedor.Name = "chkBuscarEnClienteProveedor"
        Me.chkBuscarEnClienteProveedor.Size = New System.Drawing.Size(174, 17)
        Me.chkBuscarEnClienteProveedor.TabIndex = 11
        Me.chkBuscarEnClienteProveedor.Text = "Buscar en el Cliente/Proveedor"
        Me.chkBuscarEnClienteProveedor.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(394, 388)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(475, 388)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 8
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(313, 388)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 6
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(232, 388)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 5
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(151, 388)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 4
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'chkSiEsVenta
        '
        Me.chkSiEsVenta.AutoSize = True
        Me.chkSiEsVenta.Location = New System.Drawing.Point(224, 62)
        Me.chkSiEsVenta.Name = "chkSiEsVenta"
        Me.chkSiEsVenta.Size = New System.Drawing.Size(82, 17)
        Me.chkSiEsVenta.TabIndex = 12
        Me.chkSiEsVenta.Text = "Si es venta:"
        Me.chkSiEsVenta.UseVisualStyleBackColor = True
        '
        'rdbContado
        '
        Me.rdbContado.AutoSize = True
        Me.rdbContado.Location = New System.Drawing.Point(311, 62)
        Me.rdbContado.Name = "rdbContado"
        Me.rdbContado.Size = New System.Drawing.Size(65, 17)
        Me.rdbContado.TabIndex = 13
        Me.rdbContado.TabStop = True
        Me.rdbContado.Text = "Contado"
        Me.rdbContado.UseVisualStyleBackColor = True
        Me.rdbContado.Visible = False
        '
        'rdbCredito
        '
        Me.rdbCredito.AutoSize = True
        Me.rdbCredito.Location = New System.Drawing.Point(382, 62)
        Me.rdbCredito.Name = "rdbCredito"
        Me.rdbCredito.Size = New System.Drawing.Size(58, 17)
        Me.rdbCredito.TabIndex = 14
        Me.rdbCredito.TabStop = True
        Me.rdbCredito.Text = "Credito"
        Me.rdbCredito.UseVisualStyleBackColor = True
        Me.rdbCredito.Visible = False
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'gbxDatos
        '
        Me.gbxDatos.Controls.Add(Me.txtOrden)
        Me.gbxDatos.Controls.Add(Me.lblOrden)
        Me.gbxDatos.Controls.Add(Me.txtID)
        Me.gbxDatos.Controls.Add(Me.lblID)
        Me.gbxDatos.Controls.Add(Me.lblTipo)
        Me.gbxDatos.Controls.Add(Me.cbxDebeHaber)
        Me.gbxDatos.Controls.Add(Me.lblDebeHaber)
        Me.gbxDatos.Controls.Add(Me.txtCuenta)
        Me.gbxDatos.Controls.Add(Me.cbxTipo)
        Me.gbxDatos.Controls.Add(Me.rdbCredito)
        Me.gbxDatos.Controls.Add(Me.lblCuenta)
        Me.gbxDatos.Controls.Add(Me.rdbContado)
        Me.gbxDatos.Controls.Add(Me.chkBuscarEnProducto)
        Me.gbxDatos.Controls.Add(Me.chkSiEsVenta)
        Me.gbxDatos.Controls.Add(Me.chkBuscarEnClienteProveedor)
        Me.gbxDatos.Location = New System.Drawing.Point(12, 267)
        Me.gbxDatos.Name = "gbxDatos"
        Me.gbxDatos.Size = New System.Drawing.Size(726, 115)
        Me.gbxDatos.TabIndex = 3
        Me.gbxDatos.TabStop = False
        '
        'txtOrden
        '
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Decimales = True
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(685, 32)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(38, 22)
        Me.txtOrden.SoloLectura = False
        Me.txtOrden.TabIndex = 9
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOrden.Texto = "0"
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(687, 16)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(36, 13)
        Me.lblOrden.TabIndex = 8
        Me.lblOrden.Text = "Orden"
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.White
        Me.txtID.Location = New System.Drawing.Point(9, 33)
        Me.txtID.Name = "txtID"
        Me.txtID.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(41, 20)
        Me.txtID.TabIndex = 1
        Me.txtID.TabStop = False
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(9, 16)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(18, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID"
        '
        'cbxDebeHaber
        '
        Me.cbxDebeHaber.CampoWhere = Nothing
        Me.cbxDebeHaber.DataDisplayMember = Nothing
        Me.cbxDebeHaber.DataFilter = Nothing
        Me.cbxDebeHaber.DataOrderBy = Nothing
        Me.cbxDebeHaber.DataSource = Nothing
        Me.cbxDebeHaber.DataValueMember = Nothing
        Me.cbxDebeHaber.FormABM = Nothing
        Me.cbxDebeHaber.Indicaciones = Nothing
        Me.cbxDebeHaber.Location = New System.Drawing.Point(312, 33)
        Me.cbxDebeHaber.Name = "cbxDebeHaber"
        Me.cbxDebeHaber.SeleccionObligatoria = False
        Me.cbxDebeHaber.Size = New System.Drawing.Size(96, 21)
        Me.cbxDebeHaber.SoloLectura = False
        Me.cbxDebeHaber.TabIndex = 5
        Me.cbxDebeHaber.Texto = ""
        '
        'txtCuenta
        '
        Me.txtCuenta.AlturaMaxima = 80
        Me.txtCuenta.ListarTodas = False
        Me.txtCuenta.Location = New System.Drawing.Point(408, 33)
        Me.txtCuenta.SoloLectura = False
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Registro = Nothing
        Me.txtCuenta.Resolucion173 = False
        Me.txtCuenta.Seleccionado = False
        Me.txtCuenta.Size = New System.Drawing.Size(278, 21)
        Me.txtCuenta.TabIndex = 7
        Me.txtCuenta.Texto = Nothing
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(50, 33)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(262, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'cbxOperacion
        '
        Me.cbxOperacion.CampoWhere = Nothing
        Me.cbxOperacion.DataDisplayMember = Nothing
        Me.cbxOperacion.DataFilter = Nothing
        Me.cbxOperacion.DataOrderBy = Nothing
        Me.cbxOperacion.DataSource = Nothing
        Me.cbxOperacion.DataValueMember = Nothing
        Me.cbxOperacion.FormABM = Nothing
        Me.cbxOperacion.Indicaciones = Nothing
        Me.cbxOperacion.Location = New System.Drawing.Point(12, 25)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.SeleccionObligatoria = False
        Me.cbxOperacion.Size = New System.Drawing.Size(275, 21)
        Me.cbxOperacion.SoloLectura = False
        Me.cbxOperacion.TabIndex = 1
        Me.cbxOperacion.Texto = ""
        '
        'cbxTipoOperacion
        '
        Me.cbxTipoOperacion.CampoWhere = Nothing
        Me.cbxTipoOperacion.DataDisplayMember = Nothing
        Me.cbxTipoOperacion.DataFilter = Nothing
        Me.cbxTipoOperacion.DataOrderBy = Nothing
        Me.cbxTipoOperacion.DataSource = Nothing
        Me.cbxTipoOperacion.DataValueMember = Nothing
        Me.cbxTipoOperacion.FormABM = Nothing
        Me.cbxTipoOperacion.Indicaciones = Nothing
        Me.cbxTipoOperacion.Location = New System.Drawing.Point(293, 25)
        Me.cbxTipoOperacion.Name = "cbxTipoOperacion"
        Me.cbxTipoOperacion.SeleccionObligatoria = False
        Me.cbxTipoOperacion.Size = New System.Drawing.Size(232, 21)
        Me.cbxTipoOperacion.SoloLectura = False
        Me.cbxTipoOperacion.TabIndex = 11
        Me.cbxTipoOperacion.Texto = ""
        Me.cbxTipoOperacion.Visible = False
        '
        'lblTipoOperacion
        '
        Me.lblTipoOperacion.AutoSize = True
        Me.lblTipoOperacion.Location = New System.Drawing.Point(293, 9)
        Me.lblTipoOperacion.Name = "lblTipoOperacion"
        Me.lblTipoOperacion.Size = New System.Drawing.Size(31, 13)
        Me.lblTipoOperacion.TabIndex = 10
        Me.lblTipoOperacion.Text = "Tipo:"
        Me.lblTipoOperacion.Visible = False
        '
        'frmCuentasFijas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(738, 444)
        Me.Controls.Add(Me.cbxTipoOperacion)
        Me.Controls.Add(Me.lblTipoOperacion)
        Me.Controls.Add(Me.gbxDatos)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxLista)
        Me.Controls.Add(Me.cbxOperacion)
        Me.Controls.Add(Me.lblOperacion)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmCuentasFijas"
        Me.Tag = "CUENTAS FIJAS"
        Me.Text = "frmCuentasFijas"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxLista.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxDatos.ResumeLayout(False)
        Me.gbxDatos.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cbxOperacion As ERP.ocxCBX
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents gbxLista As System.Windows.Forms.GroupBox
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents lblDebeHaber As System.Windows.Forms.Label
    Friend WithEvents txtCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents chkBuscarEnProducto As System.Windows.Forms.CheckBox
    Friend WithEvents chkBuscarEnClienteProveedor As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents chkSiEsVenta As System.Windows.Forms.CheckBox
    Friend WithEvents rdbContado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents cbxDebeHaber As ERP.ocxCBX
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents gbxDatos As System.Windows.Forms.GroupBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents cbxTipoOperacion As ERP.ocxCBX
    Friend WithEvents lblTipoOperacion As System.Windows.Forms.Label
    Friend WithEvents txtOrden As ERP.ocxTXTNumeric
    Friend WithEvents lblOrden As System.Windows.Forms.Label
End Class

﻿Public Class frmAuditoriaUsuarioOperacionFueraRango

    Dim CSistema As New CSistema
    Dim dt As New DataTable

    Sub Inicializar()


        CargarInformacion()


    End Sub

    Sub CargarInformacion()

        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Nombre from Usuario Order by Nombre")
        CSistema.SqlToComboBox(cbxUsuarioAuditoria, "Select ID, Nombre from Usuario Order by Nombre")

        CSistema.SqlToComboBox(cbxOperacion, "Select ID, Descripcion from Operacion")
        txtDesde.Text = Today
        txtHasta.Text = Today
        txtDesdeAuditoria.Text = Today
        txtHastaAuditoria.Text = Today


    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1 "


        'Validar

        If chkRangoFechaHabilitacion.Checked Then

            If IsDate(txtDesde.Text) = False Then
                MessageBox.Show("Seleccione correctamente la fecha", "Atencion", MessageBoxButtons.OK)
                txtDesde.Focus()
                Exit Sub
            End If

            If IsDate(txtHasta.Text) = False Then
                MessageBox.Show("Seleccione correctamente la fecha", "Atencion", MessageBoxButtons.OK)
                txtHasta.Focus()
                Exit Sub
            End If

            Where = Where & " And Desde >= '" & CSistema.FormatoFechaBaseDatos(txtDesde.Text, True, False) & "' "
            Where = Where & " And Hasta <= '" & CSistema.FormatoFechaBaseDatos(txtHasta.Text, True, False) & "' "
        End If

        If chkUsuario.Checked Then

            If IsNumeric(cbxUsuario.SelectedValue) = False Then
                MessageBox.Show("Seleccione correctamente el Usuario ", "Atencion", MessageBoxButtons.OK)
                cbxUsuario.Focus()
                Exit Sub
            End If
            Where = Where & " And IDUsuario = " & cbxUsuario.SelectedValue
        End If

        If chkOperacion.Checked Then
            If IsNumeric(cbxOperacion.SelectedValue) = False Then
                MessageBox.Show("Seleccione correctamente la Operacion", "Atencion", MessageBoxButtons.OK)
                cbxOperacion.Focus()
                Exit Sub
            End If
            Where = Where & " And IDOperacion = " & cbxOperacion.SelectedValue
        End If

        If chkRangoAuditoria.Checked Then
            If IsDate(txtDesdeAuditoria.Text) = False Then
                MessageBox.Show("Seleccione correctamente la fecha", "Atencion", MessageBoxButtons.OK)
                txtDesdeAuditoria.Focus()
                Exit Sub
            End If

            If IsDate(txtHastaAuditoria.Text) = False Then
                MessageBox.Show("Seleccione correctamente la fecha", "Atencion", MessageBoxButtons.OK)
                txtHastaAuditoria.Focus()
                Exit Sub
            End If

            Where = Where & " And FechaAuditoria between '" & CSistema.FormatoFechaBaseDatos(txtDesdeAuditoria.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHastaAuditoria.Text, True, False) & "' "
        End If

        If chkUsuarioAuditoria.Checked Then
            If IsNumeric(cbxUsuarioAuditoria.SelectedValue) = False Then
                MessageBox.Show("Seleccione correctamente el Usuario ", "Atencion", MessageBoxButtons.OK)
                cbxUsuarioAuditoria.Focus()
                Exit Sub
            End If
            Where = Where & " And IDUsuarioAuditoria = " & cbxUsuarioAuditoria.SelectedValue
        End If


        dt = CSistema.ExecuteToDataTable("Select * from VaUsuarioOperacionFueraRangoFecha " & Where)

        CSistema.dtToGrid(dgv, dt)

        CSistema.DataGridColumnasVisibles(dgv, {"Usuario", "Operacion", "Desde", "Hasta", "OperacionAuditoria", "UsuarioAuditoria", "FechaAuditoria"})


    End Sub

    Private Sub frmAuditoriaUsuarioOperacionFueraRango_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub chkRangoFechaHabilitacion_CheckedChanged(sender As Object, e As EventArgs) Handles chkRangoFechaHabilitacion.CheckedChanged
        txtDesde.Enabled = chkRangoFechaHabilitacion.Checked
        txtHasta.Enabled = chkRangoFechaHabilitacion.Checked

    End Sub

    Private Sub chkRangoAuditoria_CheckedChanged(sender As Object, e As EventArgs) Handles chkRangoAuditoria.CheckedChanged
        txtDesdeAuditoria.Enabled = chkRangoAuditoria.Checked
        txtHastaAuditoria.Enabled = chkRangoAuditoria.Checked
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        If dt Is Nothing Then
            Exit Sub
        End If
        CSistema.dtToExcel2(dt, "Auditoria")
    End Sub

    Private Sub chkOperacion_CheckedChanged(sender As Object, e As EventArgs) Handles chkOperacion.CheckedChanged
        cbxOperacion.Enabled = chkOperacion.Checked
    End Sub

    Private Sub chkUsuario_CheckedChanged(sender As Object, e As EventArgs) Handles chkUsuario.CheckedChanged
        cbxUsuario.Enabled = chkUsuario.Checked
    End Sub

    Private Sub chkUsuarioAuditoria_CheckedChanged(sender As Object, e As EventArgs) Handles chkUsuarioAuditoria.CheckedChanged
        cbxUsuarioAuditoria.Enabled = chkUsuarioAuditoria.Checked
    End Sub
End Class
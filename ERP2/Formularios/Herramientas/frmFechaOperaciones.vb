﻿Public Class frmFechaOperaciones
    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim FechaTemporalDesde As Date
    Dim FechaTemporalHasta As Date

    'FUNCIONES
    Sub Inicializar()


        Try
            FechaTemporalDesde = CSistema.ExecuteScalar("Select Top(1) OperacionBloquearFechaDesde From Configuraciones Where IDSucursal=" & vgIDSucursal)
            FechaTemporalHasta = CSistema.ExecuteScalar("Select Top(1) OperacionBloquearFechaHasta From Configuraciones Where IDSucursal=" & vgIDSucursal)

        Catch ex As Exception

        End Try

        vgConfiguraciones("OperacionBloquearFechaDesde") = FechaTemporalDesde
        vgConfiguraciones("OperacionBloquearFechaHasta") = FechaTemporalHasta

        txtFechaDesde.txt.Text = FechaTemporalDesde
        txtFechaHasta.txt.Text = FechaTemporalHasta

    End Sub

    Private Sub frmFechaFacturacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        'CSistema.ExecuteNonQuery(" Update Configuraciones Set OperacionBloquearFechaDesde= '" & txtFechaDesde.GetValueString & "', OperacionBloquearFechaHasta= '" & txtFechaHasta.GetValueString & "' Where IDSucursal=" & vgIDSucursal & " ")
        'Se pidio en ERSA que no sea por sucursal

        CSistema.ExecuteNonQuery(" Update Configuraciones Set OperacionBloquearFechaDesde= '" & txtFechaDesde.GetValueString & "', OperacionBloquearFechaHasta= '" & txtFechaHasta.GetValueString & "'")

        VGFechaFacturacion = txtFechaDesde.GetValue
        VGFechaFacturacionHasta = txtFechaHasta.GetValue

        vgConfiguraciones("OperacionBloquearFechaDesde") = txtFechaDesde.GetValue
        vgConfiguraciones("OperacionBloquearFechaHasta") = txtFechaHasta.GetValue
        MsgBox("Registro Guardado.!!")
        Me.Close()

    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

End Class
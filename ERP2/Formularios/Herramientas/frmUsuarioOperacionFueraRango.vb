﻿Public Class frmUsuarioOperacionFueraRango
    'Clases
    Dim Csistema As New CSistema

    'Variables
    Dim dt As DataTable
    Dim IDOperacion As String = ""
    Dim IDUsuario As String = ""

    Sub Inicializar()
        CargarDatos()
        Listar()
    End Sub

    Sub CargarDatos()
        'Cargar Listbox
        'Operaciones
        Dim dtOperacion As DataTable = Csistema.ExecuteToDataTable("Select ID, Descripcion From Operacion Order By Descripcion")
        ListBox1.DisplayMember = "Descripcion"
        ListBox1.ValueMember = "ID"
        ListBox1.DataSource = dtOperacion
    End Sub

    Sub Listar()

        dgvUsuarioOperacion.Columns.Clear()

        Dim SQL As String = "Select IDUsuario, Usuario, Operacion, IDOperacion,Desde, Hasta From VUsuarioOperacionFueraRango"
        Dim Where As String = "" '"Where IDUsuario=" & cbxUsuario.GetValue
        Dim OrderBy As String = "Order By Usuario"

        SQL = SQL & " " & Where & " " & OrderBy

        dt = CSistema.ExecuteToDataTable(SQL)
        dgvUsuarioOperacion.DataSource = dt

        'Formato

        dgvUsuarioOperacion.Columns.Item(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgvUsuarioOperacion.Columns.Item(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvUsuarioOperacion.Columns.Item(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgvUsuarioOperacion.Columns.Item(3).Visible = False
        dgvUsuarioOperacion.Columns.Item(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        dgvUsuarioOperacion.Columns.Item(5).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
    End Sub

    Function ValidarProceso(ByVal Operacion As CSistema.NUMOperacionesABM) As Boolean

        ValidarProceso = False
        'Nuevo
        If Operacion = Csistema.NUMOperacionesABM.DEL Then

            'Seleccion de registro
            If ListBox1.SelectedValue Is Nothing Then
                MessageBox.Show("Seleccione un registro para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
            End If

            If cbxUsuario.Texto = "" Then
                MessageBox.Show("Seleccione un registro para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
            End If

        End If

        'Eliminar
        If Operacion = Csistema.NUMOperacionesABM.DEL Then

            'Seleccion de registro
            If dgvUsuarioOperacion.SelectedRows Is Nothing Then
                MessageBox.Show("Seleccione un registro para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
            End If

            If MessageBox.Show("Desea eliminar el registro permanentemente?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
                Return False
            End If

        End If

        ValidarProceso = True

    End Function

    Sub Nuevo()

        IDOperacion = ListBox1.SelectedValue.ToString
        IDUsuario = cbxUsuario.GetValue.ToString

        Guardar(Csistema.NUMOperacionesABM.INS)

    End Sub

    Sub Eliminar()

        Guardar(Csistema.NUMOperacionesABM.DEL)

    End Sub

    Sub ObtenerValoresGrid()
        IDOperacion = dgvUsuarioOperacion.SelectedRows(0).Cells("IDOperacion").Value
        IDUsuario = dgvUsuarioOperacion.SelectedRows(0).Cells("IDUsuario").Value
    End Sub

    Sub ObtenerValores()
        IDUsuario = cbxUsuario.GetValue.ToString
        IDOperacion = ListBox1.SelectedValue.ToString
    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar
        If ValidarProceso(Operacion) = False Then
            Exit Sub
        End If

        If Operacion = ERP.CSistema.NUMOperacionesABM.INS Then
            ObtenerValores()
        End If

        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            ObtenerValoresGrid()
        End If

        ''Establecer el scrip del Procedimiento Almacenado

        Dim SQL As String = "Exec SpUsuarioOperacionFueraRangoFecha "
        Csistema.ConcatenarParametro(SQL, "@IDUsuario", IDUsuario, False)
        Csistema.ConcatenarParametro(SQL, "@IDOperacion", IDOperacion, False)
        Csistema.ConcatenarParametro(SQL, "@Desde", Csistema.FormatoFechaBaseDatos(txtDesde.Text, True, False), False)
        Csistema.ConcatenarParametro(SQL, "@Hasta", Csistema.FormatoFechaBaseDatos(txtHasta.Text, True, False), False)
        Csistema.ConcatenarParametro(SQL, "@Operacion", Operacion.ToString, False)
        Csistema.ConcatenarParametro(SQL, "@IDTerminal", vgIDTerminal, False)
        Csistema.ConcatenarParametro(SQL, "@IDUsuarioOperacion", vgIDUsuario, False)


        'Ejecutar SP
        Dim dt As DataTable = Csistema.ExecuteToDataTable(SQL)

        'Validar, si fue exitosa la operacion
        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Resultado As DataRow = dt.Rows(0)

        If CBool(Resultado("Procesado")) = True Then
            MessageBox.Show(CStr(Resultado("Mensaje")), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Listar()
            Exit Sub
        End If

        If CBool(Resultado("Procesado")) = False Then
            MessageBox.Show(CStr(Resultado("Mensaje")), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

    End Sub

    Sub EliminarPermisos()

        If MessageBox.Show("Esta operacion eliminara TODOS LOS PERMISOS de TODOS LOS USUARIOS, Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then
            Exit Sub
        End If

        Csistema.ExecuteNonQuery("Delete from UsuarioOperacionFueraRangoFecha")

        MessageBox.Show("Permisos Eliminados", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub frmUsuarioOperacionFueraRango_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub cbxUsuario_PropertyChanged(sender As Object, e As System.EventArgs) Handles cbxUsuario.PropertyChanged
        'Listar()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub lklEliminarPermisos_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklEliminarPermisos.LinkClicked

        EliminarPermisos()
    End Sub

End Class
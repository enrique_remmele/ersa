﻿Imports System.Threading

Public Class frmImportarDatosOtros

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CMHilo As New CMHilo

    'Varables
    Dim dt As DataTable
    Dim dtError As DataTable
    Dim dtImportarDatos As DataTable
    Dim tProceso As Thread
    Dim Cancelar As Boolean = False
    Dim Registro As String

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones
        CSistema.SqlToListBox(lbxLista, CSistema.ExecuteToDataTable("Select ID, Descripcion From ImportarDatos Order By Descripcion"), "ID", "Descripcion")
        CargarInformacion()

        'Foco

    End Sub

    Sub CargarInformacion()

        txtPath.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "PATH" & lbxLista.Text, ""))

        Try
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From ImportarDatos Where ID = " & lbxLista.SelectedValue)

            For Each oRow As DataRow In dt.Rows
                txtCodigo.SetValue(oRow("ID").ToString)
                txtDescripcion.SetValue(oRow("Descripcion").ToString)
                txtScrip.SetValue(oRow("Scrip").ToString)
            Next


        Catch ex As Exception

        End Try
    End Sub

    Sub GuardarInformacion()

        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "PATH" & lbxLista.Text, txtPath.GetValue)

    End Sub

    Sub Listar()

        Dim SQL As String = CSistema.ExecuteScalar("Select Consulta From ImportarDatos Where ID=" & lbxLista.SelectedValue).ToString.Replace("''", "'")
        SQL = SQL

        'DBF
        If txtPath.GetValue.ToLower.EndsWith(".dbf") Then
            dt = CSistema.ExecuteToDataTableDBF(SQL, txtPath.GetValue)
        End If

        'EXCEL
        If txtPath.GetValue.ToLower.EndsWith(".xls") OrElse txtPath.GetValue.ToLower.EndsWith(".xlsx") Then
            dt = CSistema.ExecuteToDataTableXLS(SQL, txtPath.GetValue)
        End If

        'BLOC DE NOTAS
        If txtPath.GetValue.ToLower.EndsWith(".txt") Then
            dt = CSistema.ExecuteToDataTableTXT(txtPath.GetValue)
        End If

        If dt Is Nothing Then
            Exit Sub
        End If

        Try
            dt.Columns.Add("Mensaje")
            dt.Columns("Mensaje").SetOrdinal(0)

            dgv.DataSource = dt

            For i As Integer = 0 To dgv.ColumnCount - 1
                dgv.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            Next

            Cancelar = False

            dtError = dt.Clone
            dgError.DataSource = dtError

        Catch ex As Exception

        End Try


    End Sub

    Sub Procesar()

        Cancelar = False
        btnCancelar.Visible = True

        tProceso = New Thread(AddressOf Actualizar)
        tProceso.Start()

    End Sub

    Sub Actualizar()

        Panel1.Enabled = False
        FlowLayoutPanel1.Enabled = False
        btnCancelar.Visible = True
        Dim SQL As String = ""

        Try
            dtImportarDatos = CSistema.ExecuteToDataTable("Select * From ImportarDatos Where ID=" & lbxLista.SelectedValue)
            Dim NombreSp As String = dtImportarDatos.Rows(0)("Scrip").ToString
            Dim EsTransaccion As Boolean = dtImportarDatos.Rows(0)("EsTransaccion").ToString

            SQL = "Exec " & NombreSp & " " & vbCrLf

            Dim Progreso As Integer = 0
            pbrConteo.Value = Progreso
            txtProcesando.Text = ""

            Dim Actualizar As Boolean = rdbActualizar.Checked
            Dim NombreCampo As String = ""

            For i As Integer = 0 To dgv.RowCount - 1

                Try
                    SQL = "Exec " & NombreSp & " " & vbCrLf

                    For c As Integer = 1 To dgv.Columns.Count - 1

                        NombreCampo = dgv.Columns(c).Name

                        Dim valor As String = dgv.Rows(i).Cells(NombreCampo).Value.ToString.Trim
                        valor = valor.Replace("'", "")

                        'Valores especiales ñ
                        If valor.Contains("¥") Then
                            valor = valor.Replace("¥", "Ñ")
                        End If

                        'Si es fecha
                        If IsDate(valor) = True Then
                            If valor.Contains("/") Or valor.Contains("-") And valor.Length > 6 Then
                                'valor = CDate(valor).ToShortDateString
                                valor = CSistema.FormatoFechaBaseDatos(CDate(valor), True, False)
                                GoTo Concatenar
                            End If
                        End If

                        'Si es numerico
                        If IsNumeric(valor) = True And chkControlarFormatoNumerico.Checked Then
                            If CDec(valor) > 0 Then
                                If valor.Substring(0, 1) <> "0" Then
                                    valor = CSistema.FormatoNumeroBaseDatos(valor, True)
                                    GoTo Concatenar
                                End If
                            Else
                                valor = CSistema.FormatoNumeroBaseDatos(valor)
                                GoTo Concatenar
                            End If
                        End If

Concatenar:

                        CSistema.ConcatenarParametro(SQL, "@" & NombreCampo, valor)

                        SQL = SQL & vbCrLf

                    Next

                    'Transacciones
                    If EsTransaccion = True Then

                        Dim IDOperacion As Integer = dtImportarDatos.Rows(0)("IDOperacion").ToString

                        'Transaccion
                        CSistema.ConcatenarParametro(SQL, "@IDUsuario", vgIDUsuario)
                        CSistema.ConcatenarParametro(SQL, "@IDSucursal", vgIDSucursal)
                        CSistema.ConcatenarParametro(SQL, "@IDDeposito", vgIDDeposito)
                        CSistema.ConcatenarParametro(SQL, "@IDTerminal", vgIDTerminal)
                        CSistema.ConcatenarParametro(SQL, "@IDOperacion", IDOperacion)

                    End If

                    CSistema.ConcatenarParametro(SQL, "@Actualizar", rdbActualizar.Checked.ToString)

                    'Ejecutamos el Scrip
                    Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL)

                    If Resultado Is Nothing Then
                        If MessageBox.Show("Continuar?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                            Exit For
                        End If
                        GoTo seguir
                    End If

                    For Each oRow As DataRow In Resultado.Rows

                        dgv.Rows(i).Cells(0).Value = oRow("Mensaje").ToString
                        If oRow("Procesado") = False Then
                            dgv.Rows(i).DefaultCellStyle.BackColor = Color.LightSalmon
                            CargarError(i)
                        End If

                        'dgv.CurrentCell = dgv.Rows(i).Cells(0)
                        dgv.Rows(i).Selected = True

                    Next

seguir:
                    Progreso = (i / (dgv.RowCount - 1)) * 100

                    pbrConteo.Value = Progreso
                    txtProcesando.Text = "Procesando " & i + 1 & " de " & dgv.RowCount & " - " & pbrConteo.Value & "%"

                    If Cancelar = True Then
                        Exit For
                    End If

                Catch ex As Exception
                    Debug.Print(SQL)
                    If MessageBox.Show(ex.Message & ". Continuar?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                        Exit For
                    End If
                End Try

            Next

            MessageBox.Show("Importacion de datos terminada!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            pbrConteo.Value = 0
            txtProcesando.Text = ""

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)

        End Try

        Panel1.Enabled = True
        FlowLayoutPanel1.Enabled = True
        btnCancelar.Visible = False

    End Sub

    Sub CargarPath()

        Dim obj As New OpenFileDialog
        obj.AddExtension = False
        obj.CheckFileExists = True
        obj.CheckPathExists = True
        obj.FileName = ""
        obj.Filter = "Microsoft DBF|*.dbf|Excel Files|*.xls;*.xlsx;*.xlsm|Documento de Texto|*.txt"
        obj.Title = "Seleccione un archivo de importacion"
        obj.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop
        obj.RestoreDirectory = True

        If obj.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then
            Exit Sub
        End If

        txtPath.SetValue(obj.FileName)

        GuardarInformacion()

    End Sub

    Sub Consulta()

        Dim frm As New frmSQL
        frm.ID = lbxLista.SelectedValue
        frm.Path = txtPath.GetValue
        frm.ShowDialog(Me)

    End Sub

    Sub CargarError(ByVal Index As Integer)


        Try

            Dim NewRow As DataRow = dtError.NewRow
            For c As Integer = 0 To dgv.Columns.Count - 1
                NewRow(c) = dgv.Rows(Index).Cells(c).Value.ToString
            Next
            dtError.Rows.Add(NewRow)

            If dgError.Rows.Count = 1 Then

                For i As Integer = 0 To dgError.ColumnCount - 1
                    dgError.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                Next

            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub Nuevo()

        Dim frmImportarDatos As New frmImportarDatosEdicion
        frmImportarDatos.Nuevo = True
        FGMostrarFormulario(Me, frmImportarDatos, "Nuevo Scrip", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frmImportarDatos.Procesado = False Then
            Exit Sub
        End If

        CSistema.SqlToListBox(lbxLista, CSistema.ExecuteToDataTable("Select ID, Descripcion From ImportarDatos Order By Descripcion"), "ID", "Descripcion")

    End Sub

    Sub Editar()

        Dim frmImportarDatos As New frmImportarDatosEdicion
        frmImportarDatos.ID = lbxLista.SelectedValue
        frmImportarDatos.Nuevo = False
        FGMostrarFormulario(Me, frmImportarDatos, "Editar Scrip", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frmImportarDatos.Procesado = False Then
            Exit Sub
        End If

        CSistema.SqlToListBox(lbxLista, CSistema.ExecuteToDataTable("Select ID, Descripcion From ImportarDatos Order By Descripcion"), "ID", "Descripcion")

    End Sub

    Sub Eliminar()

        'Usuario
        If MessageBox.Show("Desea eliminar el registro?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        'Procesar
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", lbxLista.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "DEL", ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpImportarDatos", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Inicializar()

    End Sub


    Private Sub frmImportarDatos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnExaminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminar.Click
        CargarPath()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar = True
    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulta.Click
        Consulta()
    End Sub

    Private Sub lbxLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxLista.SelectedIndexChanged
        CargarInformacion()
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dgvToPlanilla(dgError, "Errores")
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        'CSistema.dtToExcel(dgv.DataSource)
        CSistema.dgvToPlanilla(dgv, "Registros")
    End Sub

    Private Sub btNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Editar()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

   
End Class
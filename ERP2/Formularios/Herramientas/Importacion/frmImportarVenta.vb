﻿Imports System.Threading

Public Class frmImportarVenta

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'Varables
    Dim dt As DataTable
    Dim dtImportarDatos As DataTable
    Dim dtEstructura As DataTable
    Dim tProceso As Thread
    Dim Cancelar As Boolean = False
    Dim ID As Integer
    Dim IDOperacion As Integer
    Dim Importar As String = "VENTA"
    Dim Scrip As String = "SpImportarVenta"
    Dim Registro As String

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Obtener valor
        ID = CSistema.ObtenerIDImportacionDatos(Importar, Scrip)
        IDOperacion = CSistema.ObtenerIDOperacion(frmVenta.Name, "VENTAS CLIENTES", "FATCLI")

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    Sub CargarInformacion()

        txtPath.SetValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "PATH", ""))

    End Sub

    Sub GuardarInformacion()

        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "PATH", txtPath.GetValue)

    End Sub

    Sub Listar()

        dt = ListarDBF()


        Try
            dt.Columns.Add("Mensaje")
            dt.Columns("Mensaje").SetOrdinal(0)

            dgv.DataSource = dt

            For i As Integer = 0 To dgv.ColumnCount - 1
                dgv.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            Next

            Cancelar = False

        Catch ex As Exception

        End Try


    End Sub

    Function ListarDBF() As DataTable

        ListarDBF = New DataTable

        Dim SQL As String = CSistema.ExecuteScalar("Select Consulta From ImportarDatos Where ID=" & ID).ToString.Replace("''", "'")
        Dim Where As String = ""
        Dim OrderBy As String = " Order By FechaEmision Desc"
        If chkFecha.Checked = True Then
            Dim Desde As String = txtDesde.GetValue.Month & "/" & txtDesde.GetValue.Day & "/" & txtDesde.GetValue.Year
            Dim Hasta As String = txtHasta.GetValue.Month & "/" & txtHasta.GetValue.Day & "/" & txtHasta.GetValue.Year
            Where = Where & " And Between (Fecha, CTOD('" & Desde & "'), CTOD('" & Hasta & "')) "
        End If

        If chkCliente.Checked = True Then
            Where = Where & " And Cliente = '" & txtCliente.GetValue & "'  "
        End If

        If chkSucursal.Checked = True Then
            Where = Where & " And Origen = '" & txtSucursal.GetValue & "'  "
        End If

        If chkTipoDocumento.Checked = True Then
            Where = Where & " And TDoc = '" & txtTipoDocumento.GetValue & "'  "
        End If

        SQL = SQL & Where & OrderBy

        ListarDBF = CSistema.ExecuteToDataTableDBF(SQL, txtPath.GetValue)

    End Function

    Sub Procesar()

        Cancelar = False
        btnCancelar.Visible = True

        tProceso = New Thread(AddressOf Actualizar)
        tProceso.Start()

    End Sub

    Sub Actualizar()

        Panel1.Enabled = False
        FlowLayoutPanel1.Enabled = False
        btnCancelar.Visible = True
        Dim SQL As String = ""

        Try
            dtImportarDatos = CSistema.ExecuteToDataTable("Select * From ImportarDatos Where ID=" & ID)
            Dim NombreSp As String = dtImportarDatos.Rows(0)("Scrip").ToString

            SQL = "Exec " & NombreSp & " " & vbCrLf

            Dim Progreso As Integer = 0
            pbrConteo.Value = Progreso
            txtProcesando.Text = ""

            Dim Actualizar As Boolean = rdbActualizar.Checked
            Dim NombreCampo As String = ""

            For i As Integer = 0 To dgv.RowCount - 1

                Try
                    SQL = "Exec " & NombreSp & " " & vbCrLf

                    For c As Integer = 1 To dgv.Columns.Count - 1

                        NombreCampo = dgv.Columns(c).Name

                        Dim valor As String = dgv.Rows(i).Cells(NombreCampo).Value.ToString.Trim
                        valor = valor.Replace("'", "")

                        'Si es fecha
                        If IsDate(valor) = True Then
                            If valor.Contains("/") Or valor.Contains("-") And valor.Length > 6 Then
                                valor = CDate(valor).ToShortDateString
                            End If
                        End If

                        'Si es numerico
                        If IsNumeric(valor) = True Then
                            If valor.Substring(0, 1) <> "0" Then
                                valor = CSistema.FormatoNumeroBaseDatos(valor)
                            End If
                        End If

                        CSistema.ConcatenarParametro(SQL, "@" & NombreCampo, valor)

                        SQL = SQL & vbCrLf

                    Next

                    'Transaccion
                    CSistema.ConcatenarParametro(SQL, "@IDUsuario", vgIDUsuario)
                    CSistema.ConcatenarParametro(SQL, "@IDSucursal", vgIDSucursal)
                    CSistema.ConcatenarParametro(SQL, "@IDDeposito", vgIDDeposito)
                    CSistema.ConcatenarParametro(SQL, "@IDTerminal", vgIDTerminal)
                    CSistema.ConcatenarParametro(SQL, "@IDOperacion", IDOperacion)

                    CSistema.ConcatenarParametro(SQL, "@Actualizar", rdbActualizar.Checked.ToString)

                    'Ejecutamos el Scrip
                    Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL)

                    If Resultado Is Nothing Then
                        If MessageBox.Show("Continuar?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                            Exit For
                        End If
                        GoTo seguir
                    End If

                    For Each oRow As DataRow In Resultado.Rows

                        If oRow("Procesado") = False Then
                            dgv.Rows(i).DefaultCellStyle.BackColor = Color.LightSalmon
                        End If

                        dgv.Rows(i).Cells(0).Value = oRow("Mensaje").ToString

                        'dgv.CurrentCell = dgv.Rows(i).Cells(0)
                        dgv.Rows(i).Selected = True

                    Next

seguir:

                    Progreso = (i / (dgv.RowCount - 1)) * 100

                    pbrConteo.Value = Progreso
                    txtProcesando.Text = "Procesando " & i + 1 & " de " & dgv.RowCount & " - " & pbrConteo.Value & "%"

                    If Cancelar = True Then
                        Exit For
                    End If

                Catch ex As Exception
                    Debug.Print(SQL)
                    If MessageBox.Show("Continuar?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                        Exit For
                    End If
                End Try

            Next

            MessageBox.Show("Importacion de datos terminada!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            pbrConteo.Value = 0
            txtProcesando.Text = ""

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)

        End Try

        Panel1.Enabled = True
        FlowLayoutPanel1.Enabled = True
        btnCancelar.Visible = False

    End Sub

    Sub CargarEstructura()

        dtEstructura = New DataTable
        dtEstructura.Columns.Add("ID")
        dtEstructura.Columns.Add("Campo")

        Dim valor As String = dtImportarDatos.Rows(0)("Estructura")
        Dim Estructura() As String = valor.Split(";")

        For i As Integer = 0 To Estructura.GetLength(0) - 1
            Dim NewRow As DataRow = dtEstructura.NewRow
            NewRow("ID") = i + 1
            NewRow("Campo") = Estructura(i)
            dtEstructura.Rows.Add(NewRow)
        Next


    End Sub

    Sub CargarPath()

        Dim obj As New OpenFileDialog
        obj.AddExtension = False
        obj.CheckFileExists = True
        obj.CheckPathExists = True
        obj.FileName = ""
        obj.Filter = "Microsoft DBF|*.dbf"
        obj.Title = "Seleccione un archivo de importacion"
        obj.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop
        obj.RestoreDirectory = True

        If obj.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then
            Exit Sub
        End If

        txtPath.SetValue(obj.FileName)

        GuardarInformacion()

    End Sub

    Sub Consulta()

        Dim frm As New frmSQL
        frm.ID = ID
        frm.Path = txtPath.GetValue
        frm.ShowDialog(Me)

    End Sub

    Private Sub frmImportarDatos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnExaminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminar.Click
        CargarPath()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar = True
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        txtDesde.SoloLectura = Not chkFecha.Checked
        txtHasta.SoloLectura = Not chkFecha.Checked
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        txtSucursal.SoloLectura = Not chkSucursal.Checked
    End Sub

    Private Sub chkTipoDocumento_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoDocumento.CheckedChanged
        txtTipoDocumento.SoloLectura = Not chkTipoDocumento.Checked
    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulta.Click
        Consulta()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCliente.CheckedChanged
        txtCliente.SoloLectura = Not chkCliente.Checked
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportarCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkCliente = New System.Windows.Forms.CheckBox()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdbActualizar = New System.Windows.Forms.RadioButton()
        Me.rdbNoHacerNada = New System.Windows.Forms.RadioButton()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.pbrConteo = New System.Windows.Forms.ProgressBar()
        Me.txtProcesando = New System.Windows.Forms.Label()
        Me.txtCliente = New ERP.ocxTXTString()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.btnConsulta = New System.Windows.Forms.Button()
        Me.txtTipoDocumento = New ERP.ocxTXTString()
        Me.chkTipoDocumento = New System.Windows.Forms.CheckBox()
        Me.txtSucursal = New ERP.ocxTXTString()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnExaminar = New System.Windows.Forms.Button()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.txtPath = New ERP.ocxTXTString()
        Me.lblArchivo = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkCliente
        '
        Me.chkCliente.AutoSize = True
        Me.chkCliente.Location = New System.Drawing.Point(11, 95)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(61, 17)
        Me.chkCliente.TabIndex = 8
        Me.chkCliente.Text = "Cliente:"
        Me.chkCliente.UseVisualStyleBackColor = True
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(327, 3)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(103, 25)
        Me.btnProcesar.TabIndex = 2
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(335, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(70, 25)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        Me.btnCancelar.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbActualizar)
        Me.GroupBox1.Controls.Add(Me.rdbNoHacerNada)
        Me.GroupBox1.Location = New System.Drawing.Point(146, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(175, 25)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'rdbActualizar
        '
        Me.rdbActualizar.AutoSize = True
        Me.rdbActualizar.Location = New System.Drawing.Point(1, 6)
        Me.rdbActualizar.Name = "rdbActualizar"
        Me.rdbActualizar.Size = New System.Drawing.Size(71, 17)
        Me.rdbActualizar.TabIndex = 1
        Me.rdbActualizar.TabStop = True
        Me.rdbActualizar.Text = "Actualizar"
        Me.rdbActualizar.UseVisualStyleBackColor = True
        '
        'rdbNoHacerNada
        '
        Me.rdbNoHacerNada.AutoSize = True
        Me.rdbNoHacerNada.Location = New System.Drawing.Point(72, 6)
        Me.rdbNoHacerNada.Name = "rdbNoHacerNada"
        Me.rdbNoHacerNada.Size = New System.Drawing.Size(96, 17)
        Me.rdbNoHacerNada.TabIndex = 1
        Me.rdbNoHacerNada.TabStop = True
        Me.rdbNoHacerNada.Text = "No hacer nada"
        Me.rdbNoHacerNada.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.66828!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.33172!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 542)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(854, 39)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnProcesar)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(418, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(433, 33)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Location = New System.Drawing.Point(25, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 28)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Si el codigo ya existe:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.btnCancelar, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.pbrConteo, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.txtProcesando, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(409, 33)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'pbrConteo
        '
        Me.pbrConteo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbrConteo.Location = New System.Drawing.Point(3, 3)
        Me.pbrConteo.MarqueeAnimationSpeed = 5
        Me.pbrConteo.Name = "pbrConteo"
        Me.pbrConteo.Size = New System.Drawing.Size(160, 27)
        Me.pbrConteo.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbrConteo.TabIndex = 0
        '
        'txtProcesando
        '
        Me.txtProcesando.BackColor = System.Drawing.Color.Transparent
        Me.txtProcesando.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtProcesando.Location = New System.Drawing.Point(169, 0)
        Me.txtProcesando.Name = "txtProcesando"
        Me.txtProcesando.Size = New System.Drawing.Size(160, 33)
        Me.txtProcesando.TabIndex = 1
        Me.txtProcesando.Text = "0 de 0"
        Me.txtProcesando.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCliente
        '
        Me.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCliente.Color = System.Drawing.Color.Empty
        Me.txtCliente.Indicaciones = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(80, 93)
        Me.txtCliente.Multilinea = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(149, 21)
        Me.txtCliente.SoloLectura = True
        Me.txtCliente.TabIndex = 9
        Me.txtCliente.TabStop = False
        Me.txtCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCliente.Texto = ""
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 135)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(854, 401)
        Me.dgv.TabIndex = 1
        '
        'btnConsulta
        '
        Me.btnConsulta.Location = New System.Drawing.Point(425, 32)
        Me.btnConsulta.Name = "btnConsulta"
        Me.btnConsulta.Size = New System.Drawing.Size(103, 21)
        Me.btnConsulta.TabIndex = 3
        Me.btnConsulta.Text = "Consulta"
        Me.btnConsulta.UseVisualStyleBackColor = True
        '
        'txtTipoDocumento
        '
        Me.txtTipoDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoDocumento.Color = System.Drawing.Color.Empty
        Me.txtTipoDocumento.Indicaciones = Nothing
        Me.txtTipoDocumento.Location = New System.Drawing.Point(301, 95)
        Me.txtTipoDocumento.Multilinea = False
        Me.txtTipoDocumento.Name = "txtTipoDocumento"
        Me.txtTipoDocumento.Size = New System.Drawing.Size(91, 21)
        Me.txtTipoDocumento.SoloLectura = True
        Me.txtTipoDocumento.TabIndex = 13
        Me.txtTipoDocumento.TabStop = False
        Me.txtTipoDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoDocumento.Texto = ""
        '
        'chkTipoDocumento
        '
        Me.chkTipoDocumento.AutoSize = True
        Me.chkTipoDocumento.Location = New System.Drawing.Point(235, 97)
        Me.chkTipoDocumento.Name = "chkTipoDocumento"
        Me.chkTipoDocumento.Size = New System.Drawing.Size(65, 17)
        Me.chkTipoDocumento.TabIndex = 12
        Me.chkTipoDocumento.Text = "T. Doc.:"
        Me.chkTipoDocumento.UseVisualStyleBackColor = True
        '
        'txtSucursal
        '
        Me.txtSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursal.Color = System.Drawing.Color.Empty
        Me.txtSucursal.Indicaciones = Nothing
        Me.txtSucursal.Location = New System.Drawing.Point(301, 68)
        Me.txtSucursal.Multilinea = False
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.Size = New System.Drawing.Size(91, 21)
        Me.txtSucursal.SoloLectura = True
        Me.txtSucursal.TabIndex = 11
        Me.txtSucursal.TabStop = False
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursal.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.AutoSize = True
        Me.chkSucursal.Location = New System.Drawing.Point(235, 70)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(70, 17)
        Me.chkSucursal.TabIndex = 10
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(149, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(10, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "-"
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(11, 71)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(59, 17)
        Me.chkFecha.TabIndex = 4
        Me.chkFecha.Text = "Fecha:"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtCliente)
        Me.Panel1.Controls.Add(Me.chkCliente)
        Me.Panel1.Controls.Add(Me.btnConsulta)
        Me.Panel1.Controls.Add(Me.txtTipoDocumento)
        Me.Panel1.Controls.Add(Me.chkTipoDocumento)
        Me.Panel1.Controls.Add(Me.txtSucursal)
        Me.Panel1.Controls.Add(Me.chkSucursal)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtHasta)
        Me.Panel1.Controls.Add(Me.chkFecha)
        Me.Panel1.Controls.Add(Me.txtDesde)
        Me.Panel1.Controls.Add(Me.btnExaminar)
        Me.Panel1.Controls.Add(Me.btnListar)
        Me.Panel1.Controls.Add(Me.txtPath)
        Me.Panel1.Controls.Add(Me.lblArchivo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(854, 126)
        Me.Panel1.TabIndex = 0
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 7, 1, 11, 18, 11, 984)
        Me.txtHasta.Location = New System.Drawing.Point(164, 69)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(65, 20)
        Me.txtHasta.SoloLectura = True
        Me.txtHasta.TabIndex = 7
        Me.txtHasta.TabStop = False
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 7, 1, 11, 18, 11, 984)
        Me.txtDesde.Location = New System.Drawing.Point(80, 69)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(65, 20)
        Me.txtDesde.SoloLectura = True
        Me.txtDesde.TabIndex = 5
        Me.txtDesde.TabStop = False
        '
        'btnExaminar
        '
        Me.btnExaminar.Location = New System.Drawing.Point(398, 32)
        Me.btnExaminar.Name = "btnExaminar"
        Me.btnExaminar.Size = New System.Drawing.Size(21, 21)
        Me.btnExaminar.TabIndex = 2
        Me.btnExaminar.Text = "..."
        Me.btnExaminar.UseVisualStyleBackColor = True
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(425, 68)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(103, 47)
        Me.btnListar.TabIndex = 14
        Me.btnListar.Text = "LISTAR"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'txtPath
        '
        Me.txtPath.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPath.Color = System.Drawing.Color.Empty
        Me.txtPath.Indicaciones = Nothing
        Me.txtPath.Location = New System.Drawing.Point(8, 32)
        Me.txtPath.Multilinea = False
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(384, 21)
        Me.txtPath.SoloLectura = True
        Me.txtPath.TabIndex = 1
        Me.txtPath.TabStop = False
        Me.txtPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPath.Texto = ""
        '
        'lblArchivo
        '
        Me.lblArchivo.AutoSize = True
        Me.lblArchivo.Location = New System.Drawing.Point(8, 16)
        Me.lblArchivo.Name = "lblArchivo"
        Me.lblArchivo.Size = New System.Drawing.Size(105, 13)
        Me.lblArchivo.TabIndex = 0
        Me.lblArchivo.Text = "Archivo de Consulta:"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel1, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 132.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(860, 584)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'frmImportarCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(860, 584)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Name = "frmImportarCliente"
        Me.Text = "frmImportarCliente"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkCliente As System.Windows.Forms.CheckBox
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbActualizar As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNoHacerNada As System.Windows.Forms.RadioButton
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pbrConteo As System.Windows.Forms.ProgressBar
    Friend WithEvents txtProcesando As System.Windows.Forms.Label
    Friend WithEvents txtCliente As ERP.ocxTXTString
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents btnConsulta As System.Windows.Forms.Button
    Friend WithEvents txtTipoDocumento As ERP.ocxTXTString
    Friend WithEvents chkTipoDocumento As System.Windows.Forms.CheckBox
    Friend WithEvents txtSucursal As ERP.ocxTXTString
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnExaminar As System.Windows.Forms.Button
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents txtPath As ERP.ocxTXTString
    Friend WithEvents lblArchivo As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
End Class

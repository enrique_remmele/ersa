﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportarDatos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lklVerScrip = New System.Windows.Forms.LinkLabel()
        Me.btnExaminar = New System.Windows.Forms.Button()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.cbxScrip = New System.Windows.Forms.ComboBox()
        Me.lblStoreProcedure = New System.Windows.Forms.Label()
        Me.lblArchivo = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtProcesando = New System.Windows.Forms.Label()
        Me.pbrConteo = New System.Windows.Forms.ProgressBar()
        Me.btnProcesar = New System.Windows.Forms.Button()
        Me.rdbActualizar = New System.Windows.Forms.RadioButton()
        Me.rdbNoHacerNada = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtPathClientes = New ERP.ocxTXTString()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 72)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(826, 424)
        Me.dgv.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lklVerScrip)
        Me.Panel1.Controls.Add(Me.btnExaminar)
        Me.Panel1.Controls.Add(Me.btnListar)
        Me.Panel1.Controls.Add(Me.cbxScrip)
        Me.Panel1.Controls.Add(Me.lblStoreProcedure)
        Me.Panel1.Controls.Add(Me.txtPathClientes)
        Me.Panel1.Controls.Add(Me.lblArchivo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(826, 63)
        Me.Panel1.TabIndex = 0
        '
        'lklVerScrip
        '
        Me.lklVerScrip.AutoSize = True
        Me.lklVerScrip.Location = New System.Drawing.Point(570, 36)
        Me.lklVerScrip.Name = "lklVerScrip"
        Me.lklVerScrip.Size = New System.Drawing.Size(74, 13)
        Me.lklVerScrip.TabIndex = 5
        Me.lklVerScrip.TabStop = True
        Me.lklVerScrip.Text = "Ver Estructura"
        '
        'btnExaminar
        '
        Me.btnExaminar.Location = New System.Drawing.Point(398, 32)
        Me.btnExaminar.Name = "btnExaminar"
        Me.btnExaminar.Size = New System.Drawing.Size(21, 21)
        Me.btnExaminar.TabIndex = 3
        Me.btnExaminar.Text = "..."
        Me.btnExaminar.UseVisualStyleBackColor = True
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(650, 28)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(103, 25)
        Me.btnListar.TabIndex = 6
        Me.btnListar.Text = "LISTAR"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'cbxScrip
        '
        Me.cbxScrip.FormattingEnabled = True
        Me.cbxScrip.Location = New System.Drawing.Point(425, 32)
        Me.cbxScrip.Name = "cbxScrip"
        Me.cbxScrip.Size = New System.Drawing.Size(139, 21)
        Me.cbxScrip.TabIndex = 4
        '
        'lblStoreProcedure
        '
        Me.lblStoreProcedure.AutoSize = True
        Me.lblStoreProcedure.Location = New System.Drawing.Point(422, 16)
        Me.lblStoreProcedure.Name = "lblStoreProcedure"
        Me.lblStoreProcedure.Size = New System.Drawing.Size(122, 13)
        Me.lblStoreProcedure.TabIndex = 2
        Me.lblStoreProcedure.Text = "Scrip de Base de Datos:"
        '
        'lblArchivo
        '
        Me.lblArchivo.AutoSize = True
        Me.lblArchivo.Location = New System.Drawing.Point(8, 16)
        Me.lblArchivo.Name = "lblArchivo"
        Me.lblArchivo.Size = New System.Drawing.Size(105, 13)
        Me.lblArchivo.TabIndex = 0
        Me.lblArchivo.Text = "Archivo de Consulta:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(323, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(70, 25)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        Me.btnCancelar.Visible = False
        '
        'txtProcesando
        '
        Me.txtProcesando.BackColor = System.Drawing.Color.Transparent
        Me.txtProcesando.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtProcesando.Location = New System.Drawing.Point(163, 0)
        Me.txtProcesando.Name = "txtProcesando"
        Me.txtProcesando.Size = New System.Drawing.Size(154, 33)
        Me.txtProcesando.TabIndex = 0
        Me.txtProcesando.Text = "0 de 0"
        Me.txtProcesando.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pbrConteo
        '
        Me.pbrConteo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbrConteo.Location = New System.Drawing.Point(3, 3)
        Me.pbrConteo.MarqueeAnimationSpeed = 5
        Me.pbrConteo.Name = "pbrConteo"
        Me.pbrConteo.Size = New System.Drawing.Size(154, 27)
        Me.pbrConteo.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbrConteo.TabIndex = 1
        '
        'btnProcesar
        '
        Me.btnProcesar.Location = New System.Drawing.Point(312, 3)
        Me.btnProcesar.Name = "btnProcesar"
        Me.btnProcesar.Size = New System.Drawing.Size(103, 25)
        Me.btnProcesar.TabIndex = 2
        Me.btnProcesar.Text = "Procesar"
        Me.btnProcesar.UseVisualStyleBackColor = True
        '
        'rdbActualizar
        '
        Me.rdbActualizar.AutoSize = True
        Me.rdbActualizar.Location = New System.Drawing.Point(1, 6)
        Me.rdbActualizar.Name = "rdbActualizar"
        Me.rdbActualizar.Size = New System.Drawing.Size(71, 17)
        Me.rdbActualizar.TabIndex = 0
        Me.rdbActualizar.TabStop = True
        Me.rdbActualizar.Text = "Actualizar"
        Me.rdbActualizar.UseVisualStyleBackColor = True
        '
        'rdbNoHacerNada
        '
        Me.rdbNoHacerNada.AutoSize = True
        Me.rdbNoHacerNada.Location = New System.Drawing.Point(72, 6)
        Me.rdbNoHacerNada.Name = "rdbNoHacerNada"
        Me.rdbNoHacerNada.Size = New System.Drawing.Size(96, 17)
        Me.rdbNoHacerNada.TabIndex = 1
        Me.rdbNoHacerNada.TabStop = True
        Me.rdbNoHacerNada.Text = "No hacer nada"
        Me.rdbNoHacerNada.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.SystemColors.Control
        Me.Label2.Location = New System.Drawing.Point(10, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 28)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Si el codigo ya existe:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbActualizar)
        Me.GroupBox1.Controls.Add(Me.rdbNoHacerNada)
        Me.GroupBox1.Location = New System.Drawing.Point(131, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(175, 25)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnProcesar)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(405, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(418, 33)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.66828!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.33172!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 502)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(826, 39)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel1, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(832, 544)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.btnCancelar, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.pbrConteo, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.txtProcesando, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(396, 33)
        Me.TableLayoutPanel3.TabIndex = 2
        '
        'txtPathClientes
        '
        Me.txtPathClientes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPathClientes.Color = System.Drawing.Color.Empty
        Me.txtPathClientes.Indicaciones = Nothing
        Me.txtPathClientes.Location = New System.Drawing.Point(8, 32)
        Me.txtPathClientes.Multilinea = False
        Me.txtPathClientes.Name = "txtPathClientes"
        Me.txtPathClientes.Size = New System.Drawing.Size(384, 21)
        Me.txtPathClientes.SoloLectura = True
        Me.txtPathClientes.TabIndex = 1
        Me.txtPathClientes.TabStop = False
        Me.txtPathClientes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPathClientes.Texto = ""
        '
        'frmImportarDatos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(832, 544)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Name = "frmImportarDatos"
        Me.Tag = "IMPORTAR DATOS"
        Me.Text = "frmImportarDatos"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtPathClientes As ERP.ocxTXTString
    Friend WithEvents lblArchivo As System.Windows.Forms.Label
    Friend WithEvents cbxScrip As System.Windows.Forms.ComboBox
    Friend WithEvents lblStoreProcedure As System.Windows.Forms.Label
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents btnExaminar As System.Windows.Forms.Button
    Friend WithEvents txtProcesando As System.Windows.Forms.Label
    Friend WithEvents pbrConteo As System.Windows.Forms.ProgressBar
    Friend WithEvents btnProcesar As System.Windows.Forms.Button
    Friend WithEvents rdbActualizar As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNoHacerNada As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lklVerScrip As System.Windows.Forms.LinkLabel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
End Class

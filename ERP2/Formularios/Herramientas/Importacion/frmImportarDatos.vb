﻿Imports System.Threading

Public Class frmImportarDatos

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'Varables
    Dim dt As DataTable
    Dim dtImportarDatos As DataTable
    Dim dtEstructura As DataTable
    Dim tProceso As Thread
    Dim Cancelar As Boolean = False
    Dim vExtencionArchivo As String

    Sub Inicializar()

        txtPathClientes.txt.CharacterCasing = CharacterCasing.Normal

        CargarDatos()

    End Sub

    Sub CargarDatos()

        dtImportarDatos = CData.GetTable("ImportarDatos").Copy
        CSistema.SqlToComboBox(cbxScrip, dtImportarDatos, "ID", "Descripcion")

    End Sub

    'Listar
    Sub Listar()

        'Verificar  la extencion
        If vExtencionArchivo.ToUpper = "TXT" Then
            dt = ListarTXT()
        Else
            dt = ListarXLS()
        End If

        dt.Columns.Add("Mensaje")
        dt.Columns("Mensaje").SetOrdinal(0)

        dgv.DataSource = dt

        For i As Integer = 0 To dgv.ColumnCount - 1
            dgv.Columns(i).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Next

        Cancelar = False

    End Sub

    Function ListarXLS() As DataTable

        ListarXLS = New DataTable

        Dim SQL As String = "SELECT * FROM [Hoja1$]"
        ListarXLS = CSistema.ExecuteToDataTableXLS(SQL, txtPathClientes.GetValue)

    End Function

    Function ListarTXT() As DataTable

        ListarTXT = New DataTable
        ListarTXT = CSistema.ExecuteToDataTableTXT(txtPathClientes.GetValue)

    End Function

    Sub Procesar()

        Cancelar = False
        btnCancelar.Visible = True

        If ControlarEstructura() = False Then
            Exit Sub
        End If

        tProceso = New Thread(AddressOf Actualizar)
        tProceso.Start()

    End Sub

    Sub CargarEstructura()

        dtEstructura = New DataTable
        dtEstructura.Columns.Add("ID")
        dtEstructura.Columns.Add("Campo")

        'Validar que se seleccione
        If IsNumeric(cbxScrip.SelectedValue) = False Then
            Exit Sub
        End If

        Dim valor As String = dtImportarDatos.Select("ID = " & cbxScrip.SelectedValue)(0)("Estructura")
        Dim Estructura() As String = valor.Split(";")

        For i As Integer = 0 To Estructura.GetLength(0) - 1
            Dim NewRow As DataRow = dtEstructura.NewRow
            NewRow("ID") = i + 1
            NewRow("Campo") = Estructura(i)
            dtEstructura.Rows.Add(NewRow)
        Next


    End Sub

    Sub VerEstructura()

        CargarEstructura()

        Dim frm As New Form
        Dim lv As New ListView
        CSistema.dtToLv(lv, dtEstructura)
        frm.Controls.Add(lv)
        lv.Dock = DockStyle.Fill
        FGMostrarFormulario(Me, frm, "Estructura", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Function ControlarEstructura() As Boolean

        CargarEstructura()


        For Each oRow As DataRow In dtEstructura.Rows
            Dim NombreArchivo As String = ""
            Dim NombreEstructura As String = oRow("Campo")
            Dim Encontrado As Boolean = False

            'Buscamos en todas las columnas de la Estructura
            For c As Integer = 0 To dgv.Columns.Count - 1
                NombreArchivo = dgv.Columns(c).Name
                If NombreEstructura.ToUpper = NombreArchivo.ToUpper Then
                    Encontrado = True
                    Exit For
                End If
            Next

            If Encontrado = False Then
                MessageBox.Show("La estructura no es correcta: (" & NombreEstructura & ") ", "Estructura", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If

        Next

        Return True

    End Function

    Sub Actualizar()

        Panel1.Enabled = False
        FlowLayoutPanel1.Enabled = False
        btnCancelar.Visible = True

        Try
            Dim NombreSp As String = dtImportarDatos.Select("ID = " & cbxScrip.SelectedValue)(0)("Scrip").ToString
            Dim SQL As String = "Exec " & NombreSp & " " & vbCrLf

            Dim Progreso As Integer = 0
            pbrConteo.Value = Progreso
            txtProcesando.Text = ""

            Dim Actualizar As Boolean = rdbActualizar.Checked
            Dim NombreCampo As String = ""

            For i As Integer = 0 To dgv.RowCount - 1

                SQL = "Exec " & NombreSp & " " & vbCrLf

                For Each orow As DataRow In dtEstructura.Rows
                    NombreCampo = orow("Campo")
                    Dim valor As String = dgv.Rows(i).Cells(NombreCampo).Value.ToString.Trim
                    valor = valor.Replace("'", "")
                    valor = FormatoFecha(valor)
                    CSistema.ConcatenarParametro(SQL, "@" & NombreCampo, valor)
                Next

                SQL = SQL & ", @Actualizar = '" & rdbActualizar.Checked.ToString & "' "

                'Ejecutamos el Scrip
                Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL)

                If Resultado Is Nothing Then
                    GoTo seguir
                End If

                For Each oRow As DataRow In Resultado.Rows

                    If oRow("Procesado") = False Then
                        dgv.Rows(i).DefaultCellStyle.BackColor = Color.Red
                        dgv.Rows(i).Cells(0).Value = oRow("Mensaje").ToString
                        dgv.CurrentCell = dgv.Rows(i).Cells(0)
                    End If
                Next

seguir:

                Progreso = (i / (dgv.RowCount - 1)) * 100

                pbrConteo.Value = Progreso
                txtProcesando.Text = "Procesando " & i + 1 & " de " & dgv.RowCount & " - " & pbrConteo.Value & "%"

                If Cancelar = True Then
                    Exit For
                End If

            Next

            MessageBox.Show("Importacion de datos terminada!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            pbrConteo.Value = 0
            txtProcesando.Text = ""

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try

        Panel1.Enabled = True
        FlowLayoutPanel1.Enabled = True
        btnCancelar.Visible = False

    End Sub

    Sub CargarPath()

        Dim obj As New OpenFileDialog
        obj.AddExtension = False
        obj.CheckFileExists = True
        obj.CheckPathExists = True
        obj.FileName = ""
        obj.Filter = "Microsoft Excel (97-2000)|*.xls|Excel 2007|*.xlsx|Archivo de Texto|*.txt"
        obj.Title = "Seleccione un archivo de importacion"
        obj.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop
        obj.RestoreDirectory = True

        If obj.ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then
            Exit Sub
        End If

        txtPathClientes.SetValue(obj.FileName)
        vExtencionArchivo = obj.SafeFileName.Split(".")(1)

    End Sub

    Function FormatoFecha(valor As String) As String

        If IsDate(valor) = False Then
            Return valor
        End If

        Dim FechaHora As Date = CDate(valor)

        FormatoFecha = CSistema.FormatoFechaBaseDatos(valor, True, False)

    End Function

    Private Sub frmImportarDatos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnExaminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExaminar.Click
        CargarPath()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

    Private Sub lklVerScrip_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklVerScrip.LinkClicked
        VerEstructura()
    End Sub

    Private Sub cbxScrip_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxScrip.SelectedIndexChanged
        CargarEstructura()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar = True
    End Sub

End Class
﻿Public Class frmSQL

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private PathValue As String
    Public Property Path() As String
        Get
            Return PathValue
        End Get
        Set(ByVal value As String)
            PathValue = value
        End Set
    End Property

    'FUNIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    Sub CargarInformacion()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From ImportarDatos Where ID=" & ID)
        For Each oRow As DataRow In dt.Rows
            txtID.SetValue(oRow("ID").ToString)
            txtDescripcion.SetValue(oRow("Descripcion").ToString)
            Scintilla2.Text = oRow("Consulta").ToString.Replace("''", "'")
        Next

    End Sub

    Sub Ejecutar()

        Dim SQL As String

        If Scintilla2.Selection.Length = 0 Then
            SQL = Scintilla2.Text
        Else
            SQL = Scintilla2.Selection.Text
        End If

        Dim dt As DataTable = Nothing

        'DBF
        If Path.ToLower.EndsWith(".dbf") Then
            dt = CSistema.ExecuteToDataTableDBF(SQL, Path)
        End If

        'EXCEL
        If Path.ToLower.EndsWith(".xls") OrElse Path.ToLower.EndsWith(".xlsx") Then
            dt = CSistema.ExecuteToDataTableXLS(SQL, Path)
        End If

        'BLOC DE NOTAS
        If Path.ToLower.EndsWith(".txt") Then
            dt = CSistema.ExecuteToDataTableTXT(Path)
        End If

        If dt Is Nothing Then
            Exit Sub
        End If

        CSistema.dtToGrid(DataGridView1, dt)

        If DataGridView1.Columns.Count > 0 Then
            For i As Integer = 0 To DataGridView1.ColumnCount - 1
                Debug.Print(i.ToString & vbTab & DataGridView1.Columns(i).HeaderText)
            Next
        End If

        OcxTXTString1.SetValue(DataGridView1.RowCount)

    End Sub

    Sub Guardar()

        Dim SQL As String = "Update ImportarDatos Set Consulta='" & Scintilla2.Text.Replace("'", "''''") & "' Where ID=" & ID

        If CSistema.ExecuteNonQuery(SQL) <= 0 Then
            MessageBox.Show("Ocurrio un error! No se actualizo.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        MessageBox.Show("Registro guardado!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)


    End Sub

    Private Sub frmSQL_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.F5 Then
            Ejecutar()
        End If

    End Sub

    Private Sub frmSQL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click
        Ejecutar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Guardar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

End Class
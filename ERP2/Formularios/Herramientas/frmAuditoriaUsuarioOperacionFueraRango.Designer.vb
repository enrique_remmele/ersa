﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAuditoriaUsuarioOperacionFueraRango
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtDesdeAuditoria = New System.Windows.Forms.DateTimePicker()
        Me.txtHastaAuditoria = New System.Windows.Forms.DateTimePicker()
        Me.cbxUsuarioAuditoria = New System.Windows.Forms.ComboBox()
        Me.chkUsuarioAuditoria = New System.Windows.Forms.CheckBox()
        Me.txtDesde = New System.Windows.Forms.DateTimePicker()
        Me.txtHasta = New System.Windows.Forms.DateTimePicker()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.cbxOperacion = New System.Windows.Forms.ComboBox()
        Me.chkOperacion = New System.Windows.Forms.CheckBox()
        Me.cbxUsuario = New System.Windows.Forms.ComboBox()
        Me.chkUsuario = New System.Windows.Forms.CheckBox()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.chkRangoAuditoria = New System.Windows.Forms.CheckBox()
        Me.chkRangoFechaHabilitacion = New System.Windows.Forms.CheckBox()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 270.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 1, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1020, 463)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.chkRangoFechaHabilitacion)
        Me.Panel2.Controls.Add(Me.chkRangoAuditoria)
        Me.Panel2.Controls.Add(Me.txtDesdeAuditoria)
        Me.Panel2.Controls.Add(Me.txtHastaAuditoria)
        Me.Panel2.Controls.Add(Me.cbxUsuarioAuditoria)
        Me.Panel2.Controls.Add(Me.chkUsuarioAuditoria)
        Me.Panel2.Controls.Add(Me.txtDesde)
        Me.Panel2.Controls.Add(Me.txtHasta)
        Me.Panel2.Controls.Add(Me.btnExportar)
        Me.Panel2.Controls.Add(Me.btnListar)
        Me.Panel2.Controls.Add(Me.cbxOperacion)
        Me.Panel2.Controls.Add(Me.chkOperacion)
        Me.Panel2.Controls.Add(Me.cbxUsuario)
        Me.Panel2.Controls.Add(Me.chkUsuario)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(264, 457)
        Me.Panel2.TabIndex = 3
        '
        'txtDesdeAuditoria
        '
        Me.txtDesdeAuditoria.Enabled = False
        Me.txtDesdeAuditoria.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesdeAuditoria.Location = New System.Drawing.Point(96, 216)
        Me.txtDesdeAuditoria.Name = "txtDesdeAuditoria"
        Me.txtDesdeAuditoria.Size = New System.Drawing.Size(78, 20)
        Me.txtDesdeAuditoria.TabIndex = 19
        '
        'txtHastaAuditoria
        '
        Me.txtHastaAuditoria.Enabled = False
        Me.txtHastaAuditoria.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHastaAuditoria.Location = New System.Drawing.Point(180, 216)
        Me.txtHastaAuditoria.Name = "txtHastaAuditoria"
        Me.txtHastaAuditoria.Size = New System.Drawing.Size(78, 20)
        Me.txtHastaAuditoria.TabIndex = 18
        '
        'cbxUsuarioAuditoria
        '
        Me.cbxUsuarioAuditoria.Enabled = False
        Me.cbxUsuarioAuditoria.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbxUsuarioAuditoria.FormattingEnabled = True
        Me.cbxUsuarioAuditoria.Location = New System.Drawing.Point(4, 189)
        Me.cbxUsuarioAuditoria.Name = "cbxUsuarioAuditoria"
        Me.cbxUsuarioAuditoria.Size = New System.Drawing.Size(249, 21)
        Me.cbxUsuarioAuditoria.TabIndex = 17
        '
        'chkUsuarioAuditoria
        '
        Me.chkUsuarioAuditoria.AutoSize = True
        Me.chkUsuarioAuditoria.Location = New System.Drawing.Point(4, 166)
        Me.chkUsuarioAuditoria.Name = "chkUsuarioAuditoria"
        Me.chkUsuarioAuditoria.Size = New System.Drawing.Size(109, 17)
        Me.chkUsuarioAuditoria.TabIndex = 16
        Me.chkUsuarioAuditoria.Text = "Usuario Auditoria:"
        Me.chkUsuarioAuditoria.UseVisualStyleBackColor = True
        '
        'txtDesde
        '
        Me.txtDesde.Enabled = False
        Me.txtDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesde.Location = New System.Drawing.Point(96, 114)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.Size = New System.Drawing.Size(78, 20)
        Me.txtDesde.TabIndex = 14
        '
        'txtHasta
        '
        Me.txtHasta.Enabled = False
        Me.txtHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHasta.Location = New System.Drawing.Point(180, 114)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.Size = New System.Drawing.Size(78, 20)
        Me.txtHasta.TabIndex = 13
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(159, 260)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(99, 23)
        Me.btnExportar.TabIndex = 5
        Me.btnExportar.Text = "Exportar a Excel"
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(4, 260)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(75, 23)
        Me.btnListar.TabIndex = 4
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'cbxOperacion
        '
        Me.cbxOperacion.Enabled = False
        Me.cbxOperacion.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbxOperacion.FormattingEnabled = True
        Me.cbxOperacion.Location = New System.Drawing.Point(4, 32)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.Size = New System.Drawing.Size(249, 21)
        Me.cbxOperacion.TabIndex = 1
        '
        'chkOperacion
        '
        Me.chkOperacion.AutoSize = True
        Me.chkOperacion.Location = New System.Drawing.Point(4, 9)
        Me.chkOperacion.Name = "chkOperacion"
        Me.chkOperacion.Size = New System.Drawing.Size(78, 17)
        Me.chkOperacion.TabIndex = 0
        Me.chkOperacion.Text = "Operacion:"
        Me.chkOperacion.UseVisualStyleBackColor = True
        '
        'cbxUsuario
        '
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbxUsuario.FormattingEnabled = True
        Me.cbxUsuario.Location = New System.Drawing.Point(4, 87)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.Size = New System.Drawing.Size(249, 21)
        Me.cbxUsuario.TabIndex = 3
        '
        'chkUsuario
        '
        Me.chkUsuario.AutoSize = True
        Me.chkUsuario.Location = New System.Drawing.Point(4, 64)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(65, 17)
        Me.chkUsuario.TabIndex = 2
        Me.chkUsuario.Text = "Usuario:"
        Me.chkUsuario.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(273, 3)
        Me.dgv.Name = "dgv"
        Me.dgv.Size = New System.Drawing.Size(744, 457)
        Me.dgv.TabIndex = 0
        '
        'chkRangoAuditoria
        '
        Me.chkRangoAuditoria.AutoSize = True
        Me.chkRangoAuditoria.Location = New System.Drawing.Point(4, 218)
        Me.chkRangoAuditoria.Name = "chkRangoAuditoria"
        Me.chkRangoAuditoria.Size = New System.Drawing.Size(86, 17)
        Me.chkRangoAuditoria.TabIndex = 21
        Me.chkRangoAuditoria.Text = "Rango Aud.:"
        Me.chkRangoAuditoria.UseVisualStyleBackColor = True
        '
        'chkRangoFechaHabilitacion
        '
        Me.chkRangoFechaHabilitacion.AutoSize = True
        Me.chkRangoFechaHabilitacion.Location = New System.Drawing.Point(4, 116)
        Me.chkRangoFechaHabilitacion.Name = "chkRangoFechaHabilitacion"
        Me.chkRangoFechaHabilitacion.Size = New System.Drawing.Size(87, 17)
        Me.chkRangoFechaHabilitacion.TabIndex = 22
        Me.chkRangoFechaHabilitacion.Text = "Rango Hab.:"
        Me.chkRangoFechaHabilitacion.UseVisualStyleBackColor = True
        '
        'frmAuditoriaUsuarioOperacionFueraRango
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 463)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmAuditoriaUsuarioOperacionFueraRango"
        Me.Text = "frmAuditoriaUsuarioOperacionFueraRango"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents dgv As DataGridView
    Friend WithEvents chkOperacion As CheckBox
    Friend WithEvents cbxOperacion As ComboBox
    Friend WithEvents cbxUsuario As ComboBox
    Friend WithEvents chkUsuario As CheckBox
    Friend WithEvents btnListar As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents txtDesde As DateTimePicker
    Friend WithEvents txtHasta As DateTimePicker
    Friend WithEvents btnExportar As Button
    Friend WithEvents txtDesdeAuditoria As DateTimePicker
    Friend WithEvents txtHastaAuditoria As DateTimePicker
    Friend WithEvents cbxUsuarioAuditoria As ComboBox
    Friend WithEvents chkUsuarioAuditoria As CheckBox
    Friend WithEvents chkRangoFechaHabilitacion As CheckBox
    Friend WithEvents chkRangoAuditoria As CheckBox
End Class

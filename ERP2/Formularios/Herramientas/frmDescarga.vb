﻿Imports System.Threading

Public Class frmDescarga

    'CLASES
    Dim WithEvents CData As New CData
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vCantidad As Integer
    Dim vProceso As Thread

    'FUNCIONES
    Sub Inicializar()

        CheckForIllegalCrossThreadCalls = False
        Me.Icon = My.Resources.SAIN
        vProceso = New Thread(AddressOf Procesar)
        vProceso.Start()

    End Sub

    Sub Procesar()

        CData.Inicializar()

        CData.CargarTablas()

        pb.Value = 100
        lblPorcentaje.Text = pb.Value & "%"
        lblInfo.Text = "Descarga completada...."
        VGFechaHoraSistema = CSistema.ExecuteScalar("select getdate()")
        Thread.Sleep(2000)

        Salir()


    End Sub

    Sub Parar()

        If vProceso.ThreadState = ThreadState.Running Then

        End If

    End Sub

    Sub Salir()
        Parar()
        Me.Close()
    End Sub

    Private Sub frmDescarga_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Parar()
    End Sub

    Private Sub frmDescarga_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()

    End Sub

    Private Sub CData_DescargandoDato(ByVal sender As Object, ByVal e As System.EventArgs) Handles CData.DescargandoDato
        lblInfo.Text = CData.ObjetoActual
        pb.Value = Math.Round((CData.Index / CData.Objetos) * 100, 0)
        lblPorcentaje.Text = pb.Value & "%"
    End Sub

    Private Sub CData_ErrorEnDescarga(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Mensaje As String) Handles CData.ErrorEnDescarga
        lblError.Text = Mensaje
    End Sub

    Private Sub CData_Inicializado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Mensaje As String) Handles CData.Inicializado
        lblInfo.Text = Mensaje
    End Sub


    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class
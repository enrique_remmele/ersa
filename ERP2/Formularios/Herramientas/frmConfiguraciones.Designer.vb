﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfiguraciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Opciones")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Compras")
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Ventas")
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Movimientos")
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Cobranza Credito")
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Gastos")
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Orden de Pago")
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Pedido")
        Dim TreeNode9 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Transacciones", New System.Windows.Forms.TreeNode() {TreeNode2, TreeNode3, TreeNode4, TreeNode5, TreeNode6, TreeNode7, TreeNode8})
        Dim TreeNode10 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Clasificacion de Productos")
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Usuarios con permisos especiales")
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.tvArbol = New System.Windows.Forms.TreeView()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TableLayoutPanel1.SetColumnSpan(Me.SplitContainer1, 2)
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 44)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvArbol)
        Me.SplitContainer1.Size = New System.Drawing.Size(692, 512)
        Me.SplitContainer1.SplitterDistance = 196
        Me.SplitContainer1.TabIndex = 0
        '
        'tvArbol
        '
        Me.tvArbol.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tvArbol.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvArbol.Location = New System.Drawing.Point(0, 0)
        Me.tvArbol.Name = "tvArbol"
        TreeNode1.Name = "Nodo0"
        TreeNode1.Text = "Opciones"
        TreeNode2.Name = "Nodo4"
        TreeNode2.Text = "Compras"
        TreeNode3.Name = "Nodo7"
        TreeNode3.Text = "Ventas"
        TreeNode4.Name = "Nodo9"
        TreeNode4.Text = "Movimientos"
        TreeNode5.Name = "Nodo0"
        TreeNode5.Text = "Cobranza Credito"
        TreeNode6.Name = "Nodo0"
        TreeNode6.Text = "Gastos"
        TreeNode7.Name = "Nodo2"
        TreeNode7.Text = "Orden de Pago"
        TreeNode8.Name = "Nodo1"
        TreeNode8.Text = "Pedido"
        TreeNode9.Name = "Nodo3"
        TreeNode9.Text = "Transacciones"
        TreeNode10.Name = "Nodo0"
        TreeNode10.Text = "Clasificacion de Productos"
        TreeNode11.Name = "Nodo0"
        TreeNode11.Text = "Usuarios con permisos especiales"
        Me.tvArbol.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode9, TreeNode10, TreeNode11})
        Me.tvArbol.Size = New System.Drawing.Size(194, 510)
        Me.tvArbol.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 506.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.SplitContainer1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cbxSucursal, 1, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.343412!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.65659!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(698, 559)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(186, 41)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Sucursal:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(196, 5)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(498, 31)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.Texto = ""
        '
        'frmConfiguraciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(698, 559)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmConfiguraciones"
        Me.Tag = "CONFIGURACIONES"
        Me.Text = "frmConfiguraciones"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tvArbol As System.Windows.Forms.TreeView
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
End Class

﻿Public Class frmSeleccionFecha

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Seleccionado = False
        Me.Close()
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        Seleccionado = True
        Me.Close()
    End Sub

    Private Sub frmSeleccionFecha_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Seleccionado = False
            Me.Close()
        End If

        If e.KeyCode = Keys.Enter Then
            Seleccionado = True
            Me.Close()
        End If

    End Sub

    Private Sub frmSeleccionFecha_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.DoubleClick
        If Me.MonthCalendar1.Focused = True Then
            Seleccionado = True
            Me.Close()
        End If
    End Sub

    Private Sub MonthCalendar1_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MonthCalendar1.KeyUp
        If e.KeyCode = Keys.Enter Then
            Seleccionado = True
            Me.Close()
        End If
    End Sub
End Class
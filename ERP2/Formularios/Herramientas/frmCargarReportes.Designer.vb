﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCargarReportes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.pb = New System.Windows.Forms.ProgressBar()
        Me.txtDirectorio = New System.Windows.Forms.TextBox()
        Me.lklSeleccionarDirectorio = New System.Windows.Forms.LinkLabel()
        Me.btnIniciar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.SuspendLayout()
        '
        'lblInforme
        '
        Me.lblInforme.Location = New System.Drawing.Point(17, 76)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(477, 13)
        Me.lblInforme.TabIndex = 3
        Me.lblInforme.Text = "0 de 0 - 0%"
        Me.lblInforme.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pb
        '
        Me.pb.Location = New System.Drawing.Point(15, 50)
        Me.pb.Name = "pb"
        Me.pb.Size = New System.Drawing.Size(479, 23)
        Me.pb.TabIndex = 2
        '
        'txtDirectorio
        '
        Me.txtDirectorio.Location = New System.Drawing.Point(134, 24)
        Me.txtDirectorio.Name = "txtDirectorio"
        Me.txtDirectorio.Size = New System.Drawing.Size(360, 20)
        Me.txtDirectorio.TabIndex = 1
        '
        'lklSeleccionarDirectorio
        '
        Me.lklSeleccionarDirectorio.AutoSize = True
        Me.lklSeleccionarDirectorio.Location = New System.Drawing.Point(12, 27)
        Me.lklSeleccionarDirectorio.Name = "lklSeleccionarDirectorio"
        Me.lklSeleccionarDirectorio.Size = New System.Drawing.Size(116, 13)
        Me.lklSeleccionarDirectorio.TabIndex = 0
        Me.lklSeleccionarDirectorio.TabStop = True
        Me.lklSeleccionarDirectorio.Text = "Directorio de Reportes:"
        '
        'btnIniciar
        '
        Me.btnIniciar.Location = New System.Drawing.Point(338, 101)
        Me.btnIniciar.Name = "btnIniciar"
        Me.btnIniciar.Size = New System.Drawing.Size(75, 23)
        Me.btnIniciar.TabIndex = 4
        Me.btnIniciar.Text = "Iniciar"
        Me.btnIniciar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Enabled = False
        Me.btnCancelar.Location = New System.Drawing.Point(419, 101)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frmCargarReportes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 138)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnIniciar)
        Me.Controls.Add(Me.lklSeleccionarDirectorio)
        Me.Controls.Add(Me.txtDirectorio)
        Me.Controls.Add(Me.pb)
        Me.Controls.Add(Me.lblInforme)
        Me.Name = "frmCargarReportes"
        Me.Text = "frmCargarReportes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents pb As System.Windows.Forms.ProgressBar
    Friend WithEvents txtDirectorio As System.Windows.Forms.TextBox
    Friend WithEvents lklSeleccionarDirectorio As System.Windows.Forms.LinkLabel
    Friend WithEvents btnIniciar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
End Class

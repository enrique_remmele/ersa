﻿Public Class frmEnviarCorreo

    Dim CSistema As New CSistema

    Public Property Destinatario As String
    Public Property CC As String

    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()
        txtDestinatario.Text = Destinatario
        txtCC.Text = CC

        txtDestinatario.Enabled = True
        txtCC.Enabled = True
        txtCC2.Enabled = False

    End Sub

    Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click

        Dim Contraseña As String = InputBox("", "Confirmar Contraseña", "")
        If Contraseña = "" Then
            MessageBox.Show("Es necesario introducir una contraseña", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        txtDestinatario.Text.Replace(",", ";")
        txtCC.Text.Replace(",", ";")
        txtCC2.Text.Replace(",", ";")

        Dim Destinatarios As String() = txtDestinatario.Text.Split(";")
        Dim CCs As String() = txtCC.Text.Split(";")
        Dim CC2 As String() = txtCC2.Text.Split(";")

        For i = 1 To Destinatarios.LongLength - 1
            If Not CSistema.validar_Mail(Destinatarios(i)) Then
                MessageBox.Show("Uno de los destinatarios es incorrecto, favor verifique las cuentas de correo introducidas", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        For i = 1 To CCs.LongLength - 1
            If Not CSistema.validar_Mail(CCs(i)) Then
                MessageBox.Show("Uno de los destinatarios CC es incorrecto, favor verifique las cuentas de correo introducidas", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        For i = 1 To CC2.LongLength - 1
            If Not CSistema.validar_Mail(CC2(i)) Then
                MessageBox.Show("Uno de los destinatarios CC es incorrecto, favor verifique las cuentas de correo introducidas", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        Dim SQL As String = "Exec SpEnviarCorreo "

        CSistema.ConcatenarParametro(SQL, "@IDUsuario", vgIDUsuario)
        CSistema.ConcatenarParametro(SQL, "@Destinatarios", txtDestinatario.Text)
        CSistema.ConcatenarParametro(SQL, "@CC", txtCC.Text & ";" & txtCC2.Text)
        CSistema.ConcatenarParametro(SQL, "@Asunco", txtAsunto.Text)
        CSistema.ConcatenarParametro(SQL, "@Cuerpo", txtCuerpo.Text)
        CSistema.ConcatenarParametro(SQL, "@Contraseña", Contraseña)
        CSistema.ConcatenarParametro(SQL, "@ID", 0)

        Dim frm As New frmLoadingForm
        frm.Operacion = "Correo"
        frm.Consulta = SQL
        frm.ShowDialog()

        Dim IDCorreo As Integer
        Dim dt As DataTable
        IDCorreo = CInt(frm.ValorRetorno)


        Threading.Thread.Sleep(1000)

        dt = CSistema.ExecuteToDataTable("SELECT * FROM msdb.dbo.sysmail_sentitems where mailitem_id = " & IDCorreo)

        If dt Is Nothing Then
            MessageBox.Show("Correo no enviado, contacte con soporte tecnico. ID de correo: " & IDCorreo, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("Correo no enviado, contacte con soporte tecnico. ID de correo: " & IDCorreo, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If MessageBox.Show("Correo enviado con exito, desea enviar otro correo?", "Correcto", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
            Me.Close()
        End If


    End Sub

    Private Sub frmEnviarCorreo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
End Class
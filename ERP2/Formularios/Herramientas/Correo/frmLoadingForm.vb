﻿Imports System.ComponentModel

Public Class frmLoadingForm
    Dim CSistema As New CSistema

    Public Property Operacion As String
    Public Property Procesado As Boolean
    Public Property ValorRetorno As Object
    Public Property dtRetorno As DataTable
    Public Property Consulta As String

    Private _worker As BackgroundWorker

    Protected Overrides Sub OnShown(ByVal e As System.EventArgs)
        MyBase.OnShown(e)

        _worker = New BackgroundWorker()
        AddHandler _worker.DoWork, AddressOf WorkerDoWork
        AddHandler _worker.RunWorkerCompleted, AddressOf WorkerCompleted

        _worker.RunWorkerAsync()
    End Sub

    ' This is executed on a worker thread and will not make the dialog unresponsive.  If you want
    ' to interact with the dialog (like changing a progress bar or label), you need to use the
    ' worker's ReportProgress() method (see documentation for details)
    Private Sub WorkerDoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        If Operacion = "Correo" Then
            EnviarCorreo()
        End If
        If Operacion = "SQLtoDataTable" Then
            SQLtoDataTable()
        End If
    End Sub

    Sub SQLtoDataTable()
        dtRetorno = CSistema.ExecuteToDataTable(Consulta)
        Threading.Thread.Sleep(1000)
    End Sub

    Sub EnviarCorreo()
        ValorRetorno = CSistema.ExecuteScalar(Consulta)
        Threading.Thread.Sleep(1000)
    End Sub

    ' This is executed on the UI thread after the work is complete.  It's a good place to either
    ' close the dialog or indicate that the initialization is complete.  It's safe to work with
    ' controls from this event.
    Private Sub WorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click

    End Sub
End Class
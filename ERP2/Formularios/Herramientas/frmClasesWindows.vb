﻿Imports System.Management

Public Class frmClasesWindows

    'CLASES
    Dim CSistema As New CSistema

    'Funciones
    Sub Inicializar()

        CargarClases()

    End Sub

    Sub CargarClases()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From ConfiguracionClases ")
        lbx.DisplayMember = "Clase"
        lbx.DataSource = dt

    End Sub

    Sub CargarPropiedades()

        Dim dt As New DataTable
        dt.Columns.Add("Propiedad")
        dt.Columns.Add("Valor")

        Try
            Dim mc As ManagementClass = New ManagementClass(lbx.Text)
            Dim moc As ManagementObjectCollection = mc.GetInstances()
            For Each mo As ManagementObject In moc
                For Each a As PropertyData In mo.Properties

                    Dim NewRow As DataRow = dt.NewRow
                    NewRow("Propiedad") = a.Name

                    If a.Value IsNot Nothing Then
                        NewRow("Valor") = a.Value.ToString
                    Else
                        NewRow("Valor") = "?"
                    End If

                    dt.Rows.Add(NewRow)

                Next

                If dt.Rows.Count > 500 Then
                    Exit For
                End If

            Next

        Catch ex As Exception

        End Try

        CSistema.dtToGrid(DataGridView1, dt)

    End Sub

    Private Sub frmClasesWindows_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lbx_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbx.SelectedIndexChanged
        CargarPropiedades()
    End Sub

End Class
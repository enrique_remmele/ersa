﻿Public Class frmFechaOperacionesAcuerdos
    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim FechaTemporalDesde As Date
    Dim FechaTemporalHasta As Date
    Dim ObsTemporal = ""

    'FUNCIONES
    Sub Inicializar()


        Try
            FechaTemporalDesde = CSistema.ExecuteScalar("Select Top(1) Desde From RangoFechaAcuerdos")
            FechaTemporalHasta = CSistema.ExecuteScalar("Select Top(1) Hasta From RangoFechaAcuerdos")
            ObsTemporal = CSistema.ExecuteScalar("Select Top(1) Observacion From RangoFechaAcuerdos")

        Catch ex As Exception

        End Try

        txtFechaDesde.SetValue(FechaTemporalDesde)
        txtFechaHasta.SetValue(FechaTemporalHasta)
        txtObservacion.SetValue(ObsTemporal)

    End Sub

    Private Sub frmFechaFacturacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        'CSistema.ExecuteNonQuery(" Update Configuraciones Set OperacionBloquearFechaDesde= '" & txtFechaDesde.GetValueString & "', OperacionBloquearFechaHasta= '" & txtFechaHasta.GetValueString & "' Where IDSucursal=" & vgIDSucursal & " ")
        'Se pidio en ERSA que no sea por sucursal
        CSistema.ExecuteNonQuery(" Update RangoFechaAcuerdos Set Desde= '" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "', Hasta= '" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "', Observacion= '" & txtObservacion.txt.Text & "'")

        'Se agrega Auditoria de Fecha de Contabilidad
        Dim Sql As String = ""
        Sql = "Insert into aFechaAcuerdos (FechaDesde, FechaHasta, Observacion, IDUsuarioModificacion, IDTerminalModificacion, FechaModificacion) values ('" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "','" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "', '" & txtObservacion.txt.Text & "'," & vgIDUsuario & " , " & vgIDTerminal & " , '" & CSistema.FormatoFechaBaseDatos(VGFechaHoraSistema, True, True) & "')"
        CSistema.ExecuteNonQuery(Sql, VGCadenaConexion)

        VGFechaFacturacion = txtFechaDesde.GetValue
        VGFechaFacturacionHasta = txtFechaHasta.GetValue

        MsgBox("Registro Guardado.!!")
        Me.Close()

    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class
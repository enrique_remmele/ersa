﻿Public Class frmApariencia

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        OcxConfiguracionApariencia1.Inicializar()

    End Sub

    Private Sub frmApariencia_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmApariencia_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

End Class
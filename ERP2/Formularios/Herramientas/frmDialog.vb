﻿Imports System.Windows.Forms

Public Class frmDialog

    'PROPIEDADES
    Private MensajeValue As String
    Public Property Mensaje() As String
        Get
            Return MensajeValue
        End Get
        Set(ByVal value As String)
            MensajeValue = value
            lblMensaje.Text = value
        End Set
    End Property

    Private TituloValue As String
    Public Property Titulo() As String
        Get
            Return TituloValue
        End Get
        Set(ByVal value As String)
            TituloValue = value
            Me.Text = value
        End Set
    End Property

    Private Sub btnSi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSi.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub btnNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.No
        Me.Close()
    End Sub

End Class

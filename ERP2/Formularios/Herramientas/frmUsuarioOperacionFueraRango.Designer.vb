﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmUsuarioOperacionFueraRango
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUsuarioOperacionFueraRango))
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.dgvUsuarioOperacion = New System.Windows.Forms.DataGridView()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.lklEliminarPermisos = New System.Windows.Forms.LinkLabel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtHasta = New System.Windows.Forms.DateTimePicker()
        Me.txtDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        CType(Me.dgvUsuarioOperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(6, 66)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(262, 95)
        Me.ListBox1.TabIndex = 1
        '
        'dgvUsuarioOperacion
        '
        Me.dgvUsuarioOperacion.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvUsuarioOperacion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvUsuarioOperacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsuarioOperacion.Location = New System.Drawing.Point(6, 16)
        Me.dgvUsuarioOperacion.MultiSelect = False
        Me.dgvUsuarioOperacion.Name = "dgvUsuarioOperacion"
        Me.dgvUsuarioOperacion.ReadOnly = True
        Me.dgvUsuarioOperacion.RowHeadersVisible = False
        Me.dgvUsuarioOperacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUsuarioOperacion.ShowRowErrors = False
        Me.dgvUsuarioOperacion.Size = New System.Drawing.Size(586, 338)
        Me.dgvUsuarioOperacion.TabIndex = 2
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(12, 202)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(93, 23)
        Me.btnNuevo.TabIndex = 5
        Me.btnNuevo.Text = "Agregar"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(175, 202)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(93, 23)
        Me.btnEliminar.TabIndex = 6
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Usuario:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Operaciones:"
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "ID"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "VUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(6, 21)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(262, 23)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 4
        Me.cbxUsuario.Texto = ""
        '
        'lklEliminarPermisos
        '
        Me.lklEliminarPermisos.AutoSize = True
        Me.lklEliminarPermisos.Location = New System.Drawing.Point(136, 346)
        Me.lklEliminarPermisos.Name = "lklEliminarPermisos"
        Me.lklEliminarPermisos.Size = New System.Drawing.Size(132, 13)
        Me.lklEliminarPermisos.TabIndex = 9
        Me.lklEliminarPermisos.TabStop = True
        Me.lklEliminarPermisos.Text = "Eliminar todos los permisos"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvUsuarioOperacion)
        Me.GroupBox1.Location = New System.Drawing.Point(294, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(598, 363)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'txtHasta
        '
        Me.txtHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHasta.Location = New System.Drawing.Point(190, 167)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.Size = New System.Drawing.Size(78, 20)
        Me.txtHasta.TabIndex = 11
        '
        'txtDesde
        '
        Me.txtDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesde.Location = New System.Drawing.Point(106, 167)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.Size = New System.Drawing.Size(78, 20)
        Me.txtDesde.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 171)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(87, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Rango de fecha:"
        '
        'frmUsuarioOperacionFueraRango
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(904, 374)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDesde)
        Me.Controls.Add(Me.txtHasta)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lklEliminarPermisos)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.cbxUsuario)
        Me.Controls.Add(Me.ListBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmUsuarioOperacionFueraRango"
        Me.Text = "Permiso Usuarios Para Operaciones Fuera de Rango de Fecha"
        CType(Me.dgvUsuarioOperacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents dgvUsuarioOperacion As System.Windows.Forms.DataGridView
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lklEliminarPermisos As LinkLabel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtHasta As DateTimePicker
    Friend WithEvents txtDesde As DateTimePicker
    Friend WithEvents Label3 As Label
End Class

﻿Public Class frmSeleccionUsuarioHabilitacion

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDUsuarioValue As Integer
    Public Property IDUsuario() As Integer
        Get
            Return IDUsuarioValue
        End Get
        Set(ByVal value As Integer)
            IDUsuarioValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private SeleccionadoValue As Integer
    Public Property Seleccionado() As Integer
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Integer)
            SeleccionadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = btnAceptar
        Me.CancelButton = btnCancelar
        Me.KeyPreview = False

        ListarUsuario()

    End Sub

    Sub ListarUsuario()

        Dim Sql As String = "Select U.ID, U.Nombre, U.Usuario From USuarioOperacion UO Join Usuario U On UO.IDUsuario=U.ID Where IDOperacion=" & IDOperacion & " Order By 2"
        CSistema.SqlToDataGrid(dgvUsuario, Sql)

        'Formato
        dgvUsuario.Columns("Nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub Seleccionar()

        'Validar
        If dgvUsuario.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione un usuario para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        IDUsuario = dgvUsuario.SelectedRows(0).Cells("ID").Value
        Seleccionado = True
        Me.Close()

    End Sub


    Private Sub frmSeleccionUsuarioHabilitacion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Seleccionar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Seleccionado = False
        Me.Close()
    End Sub

    Private Sub dgvUsuario_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvUsuario.KeyDown

        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
        End If

    End Sub

    Private Sub dgvUsuario_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgvUsuario.KeyUp
        If e.KeyCode = Keys.Enter Then
            Seleccionar()
        End If
    End Sub

    
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEnvioEmail
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblPara = New System.Windows.Forms.Label()
        Me.cbxPara = New ERP.ocxCBX()
        Me.lblAsunto = New System.Windows.Forms.Label()
        Me.txtAsunto = New ERP.ocxTXTString()
        Me.txtMensaje = New ERP.ocxTXTString()
        Me.lblMensaje = New System.Windows.Forms.Label()
        Me.btnSolicitar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblPara
        '
        Me.lblPara.AutoSize = True
        Me.lblPara.Location = New System.Drawing.Point(12, 20)
        Me.lblPara.Name = "lblPara"
        Me.lblPara.Size = New System.Drawing.Size(32, 13)
        Me.lblPara.TabIndex = 0
        Me.lblPara.Text = "Para:"
        '
        'cbxPara
        '
        Me.cbxPara.CampoWhere = Nothing
        Me.cbxPara.CargarUnaSolaVez = False
        Me.cbxPara.DataDisplayMember = Nothing
        Me.cbxPara.DataFilter = Nothing
        Me.cbxPara.DataOrderBy = Nothing
        Me.cbxPara.DataSource = Nothing
        Me.cbxPara.DataValueMember = Nothing
        Me.cbxPara.FormABM = Nothing
        Me.cbxPara.Indicaciones = Nothing
        Me.cbxPara.Location = New System.Drawing.Point(12, 36)
        Me.cbxPara.Name = "cbxPara"
        Me.cbxPara.SeleccionObligatoria = False
        Me.cbxPara.Size = New System.Drawing.Size(318, 23)
        Me.cbxPara.SoloLectura = False
        Me.cbxPara.TabIndex = 1
        Me.cbxPara.Texto = ""
        '
        'lblAsunto
        '
        Me.lblAsunto.AutoSize = True
        Me.lblAsunto.Location = New System.Drawing.Point(12, 62)
        Me.lblAsunto.Name = "lblAsunto"
        Me.lblAsunto.Size = New System.Drawing.Size(43, 13)
        Me.lblAsunto.TabIndex = 2
        Me.lblAsunto.Text = "Asunto:"
        '
        'txtAsunto
        '
        Me.txtAsunto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAsunto.Color = System.Drawing.Color.Empty
        Me.txtAsunto.Indicaciones = Nothing
        Me.txtAsunto.Location = New System.Drawing.Point(12, 78)
        Me.txtAsunto.Multilinea = False
        Me.txtAsunto.Name = "txtAsunto"
        Me.txtAsunto.Size = New System.Drawing.Size(318, 21)
        Me.txtAsunto.SoloLectura = False
        Me.txtAsunto.TabIndex = 3
        Me.txtAsunto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAsunto.Texto = ""
        '
        'txtMensaje
        '
        Me.txtMensaje.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMensaje.Color = System.Drawing.Color.Empty
        Me.txtMensaje.Indicaciones = Nothing
        Me.txtMensaje.Location = New System.Drawing.Point(12, 122)
        Me.txtMensaje.Multilinea = True
        Me.txtMensaje.Name = "txtMensaje"
        Me.txtMensaje.Size = New System.Drawing.Size(318, 167)
        Me.txtMensaje.SoloLectura = False
        Me.txtMensaje.TabIndex = 4
        Me.txtMensaje.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMensaje.Texto = ""
        '
        'lblMensaje
        '
        Me.lblMensaje.AutoSize = True
        Me.lblMensaje.Location = New System.Drawing.Point(12, 106)
        Me.lblMensaje.Name = "lblMensaje"
        Me.lblMensaje.Size = New System.Drawing.Size(50, 13)
        Me.lblMensaje.TabIndex = 5
        Me.lblMensaje.Text = "Mensaje:"
        '
        'btnSolicitar
        '
        Me.btnSolicitar.Location = New System.Drawing.Point(12, 299)
        Me.btnSolicitar.Name = "btnSolicitar"
        Me.btnSolicitar.Size = New System.Drawing.Size(210, 23)
        Me.btnSolicitar.TabIndex = 6
        Me.btnSolicitar.Text = "Solicitar"
        Me.btnSolicitar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(255, 299)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Text = "Carncelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frmEnvioEmail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(342, 335)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnSolicitar)
        Me.Controls.Add(Me.lblMensaje)
        Me.Controls.Add(Me.txtMensaje)
        Me.Controls.Add(Me.txtAsunto)
        Me.Controls.Add(Me.lblAsunto)
        Me.Controls.Add(Me.cbxPara)
        Me.Controls.Add(Me.lblPara)
        Me.Name = "frmEnvioEmail"
        Me.Text = "frmEnvioEmail"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblPara As System.Windows.Forms.Label
    Friend WithEvents cbxPara As ERP.ocxCBX
    Friend WithEvents lblAsunto As System.Windows.Forms.Label
    Friend WithEvents txtAsunto As ERP.ocxTXTString
    Friend WithEvents txtMensaje As ERP.ocxTXTString
    Friend WithEvents lblMensaje As System.Windows.Forms.Label
    Friend WithEvents btnSolicitar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
End Class

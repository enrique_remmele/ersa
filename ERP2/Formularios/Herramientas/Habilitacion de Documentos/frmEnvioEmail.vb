﻿Public Class frmEnvioEmail

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'FUNCIÖNES
    Sub Inicializar()

    End Sub

    'Enviar
    Sub Enviar()

        'Optener el email del usuario seleccionado
        Dim oRow As DataRow = CData.GetRow(" ID = " & cbxPara.GetValue, "VUsuario")

        If CSistema.EnviarEmail(oRow("Email").ToString, txtAsunto.GetValue, txtMensaje.GetValue) = False Then
            Exit Sub
        End If

        Me.Close()

    End Sub

    Private Sub frmEnvioEmail_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmEnvioEmail_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSolicitar_Click(sender As System.Object, e As System.EventArgs) Handles btnSolicitar.Click
        Enviar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

End Class
﻿Imports System.Data.SqlClient
Imports System.Threading

Public Class frmConsultaBaseDatos

    'CLASES
    Dim CSistema As New CSistema
    Dim CMultihilo As New CMHilo

    'ENUMERACIONES
    Enum ENUNTipo
        Consulta = 1
        Ejecutar = 2
        Scalar = 3
    End Enum

    'PROPIEDADES
    Public Property SQL As String
    Public Property dt As DataTable
    Public Property Procesado As Boolean
    Public Property Tipo As ENUNTipo


    'EVENTOS
    Public Event Cargando()
    Public Event Finalizado(vdt As DataTable)
    Public Event ErrorProducido(mensaje As String)

    'VARIABLES
    Dim vProceso As Thread
    Dim vProgressBar As Thread
    Dim vProcesando As Boolean
    Dim vSegundos As Integer
    Dim vMinutos As Integer
    Dim vHoras As Integer
    Dim vI As Integer = 0
    Dim vPuntos As Integer = 1

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        Procesado = False

        Timer1.Interval = 1000

        'Funciones
        Iniciar()

    End Sub

    Sub Iniciar()

        If vProcesando = True Then
            Exit Sub
        End If

        vProcesando = True

        If Tipo = ENUNTipo.Consulta Then
            vProceso = New Thread(AddressOf Consultar)
            vProceso.Start()
        End If

        If Tipo = ENUNTipo.Ejecutar Then
            vProceso = New Thread(AddressOf Ejecutar)
            vProceso.Start()
        End If

    End Sub

    Sub Cancelar()

        vProcesando = False
        vProceso.Abort()
        vProgressBar.Abort()

        Procesado = False

        RaiseEvent ErrorProducido("Se ha cancelado por el usuario")

        Me.Close()


    End Sub

    Sub Consultar()

        IniciarProgressBar()

        vSegundos = 0
        Timer1.Start()
        dt = CSistema.ExecuteToDataTable(SQL)

        If dt Is Nothing Then
            RaiseEvent ErrorProducido("Se ha producido un error durante la seleccion! intentelo nuevamente.")
            CMultihilo.LabelText(Me, lblInforme, "Se ha producido un error durante la seleccion! intentelo nuevamente.")
            GoTo terminar

        End If

        If dt.Rows.Count = 0 Then
            RaiseEvent ErrorProducido("No se encontro ningun registro")
            CMultihilo.LabelText(Me, lblInforme, "No se encontro ningun registro")
            GoTo terminar
        End If

        Procesado = True
        CMultihilo.LabelText(Me, lblInforme, "Terminado!")

terminar:

        vProcesando = False

        vProgressBar.Abort()
        RaiseEvent Finalizado(dt)
        Me.Close()

    End Sub

    Sub Ejecutar()

        IniciarProgressBar()

        CMultihilo.LabelText(Me, lblInforme, "Procesando.")

        vSegundos = 0
        Timer1.Start()
        CSistema.ExecuteNonQuery(SQL, "", 10000)

        CMultihilo.LabelText(Me, lblInforme, "Terminado!")


terminar:

        vProcesando = False
        Procesado = True
        vProgressBar.Abort()
        RaiseEvent Finalizado(dt)
        Me.Close()

    End Sub

    Sub CambiarTiempo()

        vSegundos = vSegundos + 1

        If vSegundos > 59 Then
            vMinutos = vMinutos + 1
            vSegundos = 0
        End If

        If vMinutos > 59 Then
            vHoras = vHoras + 1
            vMinutos = 0
        End If

        CMultihilo.LabelText(Me, lblTiempo, CSistema.FormatDobleDigito(vHoras.ToString) & ":" & CSistema.FormatDobleDigito(vMinutos.ToString) & ":" & CSistema.FormatDobleDigito(vSegundos.ToString))

        vPuntos = vPuntos + 1

        If vPuntos = 5 Then
            vPuntos = 1
        End If

        Dim puntos As String = ""
        For c As Integer = 0 To vPuntos
            puntos = puntos & "."
        Next

        CMultihilo.LabelText(Me, lblInforme, "Procesando" & puntos.ToString)
        
    End Sub

    Sub ProgressBar()

        For i As Integer = 1 To 10000

            CMultihilo.ProgressBarValue(Me, ProgressBar1, vI)

            If vI >= 100 Then
                vI = 0
            End If

            vI = vI + 10

            If vProcesando = False Then
                Exit For
            End If

            Thread.Sleep(100)

        Next

    End Sub

    Sub IniciarProgressBar()

        vProgressBar = New Thread(AddressOf ProgressBar)
        vProgressBar.Start()

    End Sub

    Private Sub frmConsultaBaseDatos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        CambiarTiempo()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

End Class
﻿Public Class frmConexionServidor

    'CLASES
    Dim CSistema As New CSistema

    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        OcxConexionBaseDeDatos1.Inicializar()

    End Sub

    Private Sub frmConexionServidor_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmConexionServidor_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

End Class
﻿Public Class frmCuentaFija

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS

    'PROPIEDADES

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Inicializar Controles
        CSistema.InicializaControles(gbxLista)

        'Controles
        txtCuenta.Conectar()

        'Funciones
        CargarInformacion()
        InicializarControles()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub CargarInformacion()

        'Cargar Operaciones
        CSistema.SqlToComboBox(cbxOperacion.cbx, "Select ID, Descripcion From Operacion Order By 1")

        'Tipos
        CSistema.SqlToComboBox(cbxTipo.cbx, "Select ID, Descripcion From  TipoCuentaFija Order By 1")

        'DebeHaber
        cbxDebeHaber.cbx.Items.Add("DEBE")
        cbxDebeHaber.cbx.Items.Add("HABER")
        cbxDebeHaber.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Cargar controles
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxTipo)
        CSistema.CargaControl(vControles, cbxDebeHaber)
        CSistema.CargaControl(vControles, txtCuenta)
        CSistema.CargaControl(vControles, txtOrden)
        CSistema.CargaControl(vControles, chkBuscarEnProducto)
        CSistema.CargaControl(vControles, chkBuscarEnClienteProveedor)
        CSistema.CargaControl(vControles, chkSiEsVenta)
        CSistema.CargaControl(vControles, rdbContado)
        CSistema.CargaControl(vControles, rdbCredito)

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If IsNumeric(cbxOperacion.cbx.SelectedValue) = False Then
            ctrError.SetError(lvLista, "Seleccione correctamente la operacion!")
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Si es que se selecciono el registro.
        If cbxOperacion.cbx.SelectedValue = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente la operacion!")
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Si es que se selecciono el registro.
        If lvLista.SelectedItems.Count = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim IDOperacion As Integer
        Dim ID As Integer

        IDOperacion = cbxOperacion.cbx.SelectedValue
        ID = lvLista.SelectedItems(0).SubItems(0).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select ID, TipoCuentaFija, DebeHaber, CuentaContable, Orden, BuscarEnProducto, BuscarEnClienteProveedor, EsVenta, Contado, Credito From  VCuentasFijas Where IDOperacion=" & IDOperacion & " And ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.Text = oRow("ID").ToString
            cbxTipo.cbx.Text = oRow("TipoCuentaFija").ToString
            cbxDebeHaber.Texto = oRow("DebeHaber").ToString
            txtCuenta.txtDescripcion.Text = oRow("CuentaContable").ToString
            txtOrden.txt.Text = oRow("Orden").ToString
            chkBuscarEnProducto.Checked = CBool(oRow("BuscarEnProducto").ToString)
            chkBuscarEnClienteProveedor.Checked = CBool(oRow("BuscarEnClienteProveedor").ToString)
            chkSiEsVenta.Checked = CBool(oRow("EsVenta").ToString)
            If chkSiEsVenta.Checked = True Then
                rdbContado.Checked = CBool(oRow("Contado").ToString)
                rdbCredito.Checked = CBool(oRow("Credito").ToString)
            Else
                rdbContado.Checked = False
                rdbCredito.Checked = False
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub ListarCuentasFijas(ByVal TipoOperacion As Boolean)

        If TipoOperacion = False Then
            cbxTipoOperacion.Visible = False
            lblTipoOperacion.Visible = False
            Listar()
        Else
            cbxTipoOperacion.Visible = True
            lblTipoOperacion.Visible = True

            'Si es que se selecciono el registro.
            If IsNumeric(cbxOperacion.cbx.SelectedValue) = False Then
                ctrError.SetError(lvLista, "Seleccione correctamente la operacion!")
                ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)
                Exit Sub
            End If

            'Si es que se selecciono el registro.
            If cbxOperacion.cbx.SelectedValue = 0 Then
                ctrError.SetError(lvLista, "Seleccione correctamente la operacion!")
                ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)
                Exit Sub
            End If

            CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion=" & cbxOperacion.cbx.SelectedValue)

        End If

    End Sub

    Sub Listar(Optional ByVal IDTipoOperacion As Integer = -1)


        'Si es que se selecciono el registro.
        If IsNumeric(cbxOperacion.cbx.SelectedValue) = False Then
            ctrError.SetError(lvLista, "Seleccione correctamente la operacion!")
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Si es que se selecciono el registro.
        If cbxOperacion.cbx.SelectedValue = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente la operacion!")
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        Dim ID As Integer

        If IDTipoOperacion = -1 Then

            ID = cbxOperacion.cbx.SelectedValue

            lvLista.Items.Clear()
            CSistema.SqlToLv(lvLista, "Select ID, 'Tipo'=TipoCuentaFija, DebeHaber, CuentaContable, Orden From  VCuentasFijas Where IDOperacion=" & ID & " Order By Orden")

        Else
            ID = cbxOperacion.cbx.SelectedValue
            lvLista.Items.Clear()
            CSistema.SqlToLv(lvLista, "Select ID, 'Tipo'=TipoCuentaFija, 'T. Operacion'=TipoOperacion, DebeHaber, CuentaContable, Orden From  VCuentasFijas Where IDOperacion=" & ID & " And IDTipoOperacion=" & IDTipoOperacion & " Order By Orden")
        End If



    End Sub

    Sub InicializarControles()

        txtID.Text = CInt(CSistema.ExecuteScalar("Select ISNULL((Max(ID)+1), 1) From CuentasFijas"))
        cbxTipo.cbx.Text = ""
        cbxDebeHaber.cbx.Text = ""
        txtCuenta.txtDescripcion.Clear()
        txtOrden.txt.Clear()
        chkBuscarEnProducto.Checked = False
        chkBuscarEnClienteProveedor.Checked = False
        chkSiEsVenta.Checked = False

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro y sus dependientes. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@ID", txtID.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDOperacion", cbxOperacion.cbx, ParameterDirection.Input)

        If cbxOperacion.cbx.SelectedValue = 20 Then
            CSistema.SetSQLParameter(param, "@IDTipoOperacion", cbxTipoOperacion.cbx, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDTipoCuentaFija", cbxTipo.cbx, ParameterDirection.Input)
        Dim Debe As Boolean
        Dim Haber As Boolean

        If cbxDebeHaber.cbx.Text = "DEBE" Then
            Debe = True
            Haber = False
        End If

        If cbxDebeHaber.cbx.Text = "HABER" Then
            Debe = False
            Haber = True
        End If

        CSistema.SetSQLParameter(param, "@Debe", Debe, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Haber", Haber, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCuentaContable", txtCuenta, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Orden", txtOrden.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@BuscarEnProducto", chkBuscarEnProducto.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@BuscarEnClienteProveedor", chkBuscarEnClienteProveedor.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@EsVenta", chkSiEsVenta.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Contado", rdbContado.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Credito", rdbCredito.Checked, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCuentaFija", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()

            If cbxOperacion.cbx.SelectedValue = 20 Then
                Listar(cbxTipoOperacion.cbx.SelectedValue)
            Else
                Listar()
            End If

        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If


    End Sub

    Sub ConfigurarSiEsVenta()

        If chkSiEsVenta.Checked = True Then
            rdbContado.Visible = True
            rdbCredito.Visible = True
        Else
            rdbContado.Visible = False
            rdbCredito.Visible = False
        End If

    End Sub

    Private Sub frmCuentasFijas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
        cbxTipo.cbx.Focus()

    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        cbxTipo.cbx.Focus()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        'InicializarControles()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub chkSiEsVenta_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSiEsVenta.CheckedChanged
        ConfigurarSiEsVenta()
    End Sub

    Private Sub lvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLista.SelectedIndexChanged
        ObtenerInformacion()
    End Sub

    Private Sub cbxOperacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOperacion.PropertyChanged

        'Si es que se selecciono el registro.
        If IsNumeric(cbxOperacion.cbx.SelectedValue) = False Then
            ctrError.SetError(lvLista, "Seleccione correctamente la operacion!")
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Si es que se selecciono el registro.
        If cbxOperacion.cbx.SelectedValue = 0 Then
            ctrError.SetError(lvLista, "Seleccione correctamente la operacion!")
            ctrError.SetIconAlignment(cbxOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If cbxOperacion.cbx.SelectedValue = 20 Then
            ListarCuentasFijas(True)
        Else
            ListarCuentasFijas(False)
        End If

    End Sub

    Private Sub txtCuenta_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuenta.ItemSeleccionado
        chkBuscarEnProducto.Focus()
    End Sub

    Private Sub cbxTipoOperacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoOperacion.PropertyChanged
        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        Listar(cbxTipoOperacion.cbx.SelectedValue)

    End Sub

End Class
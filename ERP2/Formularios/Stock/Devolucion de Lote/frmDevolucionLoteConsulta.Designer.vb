﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDevolucionLoteConsulta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        Me.chkDistribuidor = New System.Windows.Forms.CheckBox()
        Me.cbxDistribuidor = New System.Windows.Forms.ComboBox()
        Me.chkSucursal = New System.Windows.Forms.CheckBox()
        Me.cbxSucursal = New System.Windows.Forms.ComboBox()
        Me.chkUsuario = New System.Windows.Forms.CheckBox()
        Me.cbxUsuario = New System.Windows.Forms.ComboBox()
        Me.txtComprobante = New System.Windows.Forms.TextBox()
        Me.chkComprobante = New System.Windows.Forms.CheckBox()
        Me.cbxComprobante = New System.Windows.Forms.ComboBox()
        Me.btnEmitirInforme = New System.Windows.Forms.Button()
        Me.dtpHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtpDesde = New System.Windows.Forms.DateTimePicker()
        Me.txtCantidadDetalle = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblCantidadDetalle = New System.Windows.Forms.Label()
        Me.lblCantidadOperacion = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.lvDetalle = New System.Windows.Forms.ListView()
        Me.lvOperacion = New System.Windows.Forms.ListView()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtCantidadOperacion = New ERP.ocxTXTNumeric()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkFecha)
        Me.GroupBox1.Controls.Add(Me.chkDistribuidor)
        Me.GroupBox1.Controls.Add(Me.cbxDistribuidor)
        Me.GroupBox1.Controls.Add(Me.chkSucursal)
        Me.GroupBox1.Controls.Add(Me.cbxSucursal)
        Me.GroupBox1.Controls.Add(Me.chkUsuario)
        Me.GroupBox1.Controls.Add(Me.cbxUsuario)
        Me.GroupBox1.Controls.Add(Me.txtComprobante)
        Me.GroupBox1.Controls.Add(Me.chkComprobante)
        Me.GroupBox1.Controls.Add(Me.cbxComprobante)
        Me.GroupBox1.Controls.Add(Me.btnEmitirInforme)
        Me.GroupBox1.Controls.Add(Me.dtpHasta)
        Me.GroupBox1.Controls.Add(Me.dtpDesde)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(670, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(183, 564)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'chkFecha
        '
        Me.chkFecha.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkFecha.Location = New System.Drawing.Point(6, 228)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(171, 21)
        Me.chkFecha.TabIndex = 18
        Me.chkFecha.Text = "Fecha"
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'chkDistribuidor
        '
        Me.chkDistribuidor.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkDistribuidor.Location = New System.Drawing.Point(6, 174)
        Me.chkDistribuidor.Name = "chkDistribuidor"
        Me.chkDistribuidor.Size = New System.Drawing.Size(171, 21)
        Me.chkDistribuidor.TabIndex = 16
        Me.chkDistribuidor.Text = "Distribuidor"
        Me.chkDistribuidor.UseVisualStyleBackColor = True
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxDistribuidor.Enabled = False
        Me.cbxDistribuidor.FormattingEnabled = True
        Me.cbxDistribuidor.Location = New System.Drawing.Point(6, 201)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.Size = New System.Drawing.Size(171, 21)
        Me.cbxDistribuidor.TabIndex = 17
        '
        'chkSucursal
        '
        Me.chkSucursal.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkSucursal.Location = New System.Drawing.Point(6, 13)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(171, 21)
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Text = "Sucursal:"
        Me.chkSucursal.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormattingEnabled = True
        Me.cbxSucursal.Location = New System.Drawing.Point(6, 39)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.Size = New System.Drawing.Size(171, 21)
        Me.cbxSucursal.TabIndex = 1
        '
        'chkUsuario
        '
        Me.chkUsuario.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkUsuario.Location = New System.Drawing.Point(6, 120)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(171, 21)
        Me.chkUsuario.TabIndex = 7
        Me.chkUsuario.Text = "Usuario:"
        Me.chkUsuario.UseVisualStyleBackColor = True
        '
        'cbxUsuario
        '
        Me.cbxUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormattingEnabled = True
        Me.cbxUsuario.Location = New System.Drawing.Point(6, 147)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.Size = New System.Drawing.Size(171, 21)
        Me.cbxUsuario.TabIndex = 8
        '
        'txtComprobante
        '
        Me.txtComprobante.Enabled = False
        Me.txtComprobante.Location = New System.Drawing.Point(92, 94)
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(85, 20)
        Me.txtComprobante.TabIndex = 6
        '
        'chkComprobante
        '
        Me.chkComprobante.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkComprobante.Location = New System.Drawing.Point(6, 66)
        Me.chkComprobante.Name = "chkComprobante"
        Me.chkComprobante.Size = New System.Drawing.Size(171, 21)
        Me.chkComprobante.TabIndex = 4
        Me.chkComprobante.Text = "Comprobante:"
        Me.chkComprobante.UseVisualStyleBackColor = True
        '
        'cbxComprobante
        '
        Me.cbxComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxComprobante.Enabled = False
        Me.cbxComprobante.FormattingEnabled = True
        Me.cbxComprobante.Location = New System.Drawing.Point(6, 93)
        Me.cbxComprobante.Name = "cbxComprobante"
        Me.cbxComprobante.Size = New System.Drawing.Size(85, 21)
        Me.cbxComprobante.TabIndex = 5
        '
        'btnEmitirInforme
        '
        Me.btnEmitirInforme.Location = New System.Drawing.Point(6, 307)
        Me.btnEmitirInforme.Name = "btnEmitirInforme"
        Me.btnEmitirInforme.Size = New System.Drawing.Size(171, 33)
        Me.btnEmitirInforme.TabIndex = 15
        Me.btnEmitirInforme.Text = "Emitir Informe"
        Me.btnEmitirInforme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEmitirInforme.UseVisualStyleBackColor = True
        '
        'dtpHasta
        '
        Me.dtpHasta.Enabled = False
        Me.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHasta.Location = New System.Drawing.Point(92, 255)
        Me.dtpHasta.Name = "dtpHasta"
        Me.dtpHasta.Size = New System.Drawing.Size(85, 20)
        Me.dtpHasta.TabIndex = 14
        '
        'dtpDesde
        '
        Me.dtpDesde.Enabled = False
        Me.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDesde.Location = New System.Drawing.Point(6, 255)
        Me.dtpDesde.Name = "dtpDesde"
        Me.dtpDesde.Size = New System.Drawing.Size(85, 20)
        Me.dtpDesde.TabIndex = 13
        '
        'txtCantidadDetalle
        '
        Me.txtCantidadDetalle.Color = System.Drawing.Color.Empty
        Me.txtCantidadDetalle.Decimales = True
        Me.txtCantidadDetalle.Indicaciones = Nothing
        Me.txtCantidadDetalle.Location = New System.Drawing.Point(602, 3)
        Me.txtCantidadDetalle.Name = "txtCantidadDetalle"
        Me.txtCantidadDetalle.Size = New System.Drawing.Size(48, 22)
        Me.txtCantidadDetalle.SoloLectura = False
        Me.txtCantidadDetalle.TabIndex = 4
        Me.txtCantidadDetalle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadDetalle.Texto = "0"
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 4
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.txtCantidadDetalle, 3, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel6, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Panel5, 2, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 531)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(655, 30)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'Panel6
        '
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(3, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(245, 24)
        Me.Panel6.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblCantidadDetalle)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(505, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(91, 24)
        Me.Panel5.TabIndex = 1
        '
        'lblCantidadDetalle
        '
        Me.lblCantidadDetalle.AutoSize = True
        Me.lblCantidadDetalle.Location = New System.Drawing.Point(34, 5)
        Me.lblCantidadDetalle.Name = "lblCantidadDetalle"
        Me.lblCantidadDetalle.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadDetalle.TabIndex = 0
        Me.lblCantidadDetalle.Text = "Cantidad:"
        '
        'lblCantidadOperacion
        '
        Me.lblCantidadOperacion.AutoSize = True
        Me.lblCantidadOperacion.Location = New System.Drawing.Point(42, 4)
        Me.lblCantidadOperacion.Name = "lblCantidadOperacion"
        Me.lblCantidadOperacion.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidadOperacion.TabIndex = 0
        Me.lblCantidadOperacion.Text = "Cantidad:"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 189.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(856, 570)
        Me.TableLayoutPanel1.TabIndex = 6
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.lvDetalle, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.lvOperacion, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel5, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 4
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(661, 564)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'lvDetalle
        '
        Me.lvDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvDetalle.Location = New System.Drawing.Point(3, 285)
        Me.lvDetalle.Name = "lvDetalle"
        Me.lvDetalle.Size = New System.Drawing.Size(655, 240)
        Me.lvDetalle.TabIndex = 3
        Me.lvDetalle.UseCompatibleStateImageBehavior = False
        '
        'lvOperacion
        '
        Me.lvOperacion.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvOperacion.Location = New System.Drawing.Point(3, 3)
        Me.lvOperacion.Name = "lvOperacion"
        Me.lvOperacion.Size = New System.Drawing.Size(655, 240)
        Me.lvOperacion.TabIndex = 1
        Me.lvOperacion.UseCompatibleStateImageBehavior = False
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 4
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 49.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.txtCantidadOperacion, 3, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.Panel1, 2, 0)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(3, 249)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(655, 30)
        Me.TableLayoutPanel5.TabIndex = 2
        '
        'txtCantidadOperacion
        '
        Me.txtCantidadOperacion.Color = System.Drawing.Color.Empty
        Me.txtCantidadOperacion.Decimales = True
        Me.txtCantidadOperacion.Indicaciones = Nothing
        Me.txtCantidadOperacion.Location = New System.Drawing.Point(608, 3)
        Me.txtCantidadOperacion.Name = "txtCantidadOperacion"
        Me.txtCantidadOperacion.Size = New System.Drawing.Size(42, 22)
        Me.txtCantidadOperacion.SoloLectura = False
        Me.txtCantidadOperacion.TabIndex = 4
        Me.txtCantidadOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadOperacion.Texto = "0"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblCantidadOperacion)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(505, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(97, 24)
        Me.Panel1.TabIndex = 0
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmDevolucionLoteConsulta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(856, 570)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmDevolucionLoteConsulta"
        Me.Tag = "CONSULTA DE DEVOLUCIONES LOTE"
        Me.Text = "frmDevolucionLoteConsulta"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents chkDistribuidor As System.Windows.Forms.CheckBox
    Friend WithEvents cbxDistribuidor As System.Windows.Forms.ComboBox
    Friend WithEvents chkSucursal As System.Windows.Forms.CheckBox
    Friend WithEvents cbxSucursal As System.Windows.Forms.ComboBox
    Friend WithEvents chkUsuario As System.Windows.Forms.CheckBox
    Friend WithEvents cbxUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents txtComprobante As System.Windows.Forms.TextBox
    Friend WithEvents chkComprobante As System.Windows.Forms.CheckBox
    Friend WithEvents cbxComprobante As System.Windows.Forms.ComboBox
    Friend WithEvents btnEmitirInforme As System.Windows.Forms.Button
    Friend WithEvents dtpHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtCantidadDetalle As ERP.ocxTXTNumeric
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lblCantidadDetalle As System.Windows.Forms.Label
    Friend WithEvents lblCantidadOperacion As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lvDetalle As System.Windows.Forms.ListView
    Friend WithEvents lvOperacion As System.Windows.Forms.ListView
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtCantidadOperacion As ERP.ocxTXTNumeric
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
End Class

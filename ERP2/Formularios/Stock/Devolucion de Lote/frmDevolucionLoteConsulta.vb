﻿Public Class frmDevolucionLoteConsulta

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private SeleccionValue As Boolean
    Public Property Seleccion() As Boolean
        Get
            Return SeleccionValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'Form

        'Variables
        IDOperacion = CSistema.ObtenerIDOperacion(frmCargaMercaderia.Name, "CARGA DE MERCADERIA", "CAR")

        'TextBox
        txtCantidadDetalle.txt.ResetText()
        txtCantidadOperacion.txt.ResetText()
        txtComprobante.ResetText()

        'CheckBox
        chkComprobante.Checked = False
        chkDistribuidor.Checked = False
        chkUsuario.Checked = False
        chkSucursal.Checked = False

        'ComboBox
        cbxComprobante.Enabled = False
        cbxDistribuidor.Enabled = False
        cbxUsuario.Enabled = False
        cbxSucursal.Enabled = False

        'RadioButton

        'ListView
        lvDetalle.Items.Clear()
        lvOperacion.Items.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'Dsitribuidor
        CSistema.SqlToComboBox(cbxDistribuidor, "Select ID, Nombres From Distribuidor Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxComprobante, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Usuarios
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxUsuario, "Select ID, Usuario From Usuario Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

        'Distribuidor
        chkDistribuidor.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "DISTRIBUIDOR ACTIVO", "False")
        cbxDistribuidor.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "DISTRIBUIDOR", "")

        'Comprobante
        chkComprobante.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", "False")
        cbxComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", "")
        txtComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", "")

        'Usuario
        chkUsuario.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", "False")
        cbxUsuario.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "USUARIO", "")

        'Fecha
        chkFecha.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "FECHA ACTIVO", "False")

    End Sub

    'Gardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCRUSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Distribuidor
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "DISTRIBUIDOR ACTIVO", chkDistribuidor.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "DISTRIBUIDOR", cbxDistribuidor.Text)

        'Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE ACTIVO", chkComprobante.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "COMPROBANTE", cbxComprobante.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "NROCOMPROBANTE", txtComprobante.Text)

        'Usuario
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO ACTIVO", chkUsuario.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "USUARIO", cbxUsuario.Text)

        'Fecha
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "FECHA ACTIVO", chkFecha.Checked.ToString)

    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Operaciones
    Sub ListarOperaciones(Optional ByVal Numero As Integer = 0)

        ctrError.Clear()

        Where = ""

        Consulta = "Select Suc, Numero, 'Tipo'=TipoComprobante, Comprobante, Fec, [Fec Reparto], Distribuidor, Zona, Usuario, Estado, IDTransaccion From VCargaMercaderia "

        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la Sucursal!") = False Then
            Exit Sub
        End If

        'Distribuidor
        If EstablecerCondicion(cbxDistribuidor, chkDistribuidor, "IDDistribuidor", "Seleccione correctamente el distribuidor!") = False Then
            Exit Sub
        End If

        'Comprobante
        If chkComprobante.Checked = True Then

            If EstablecerCondicion(cbxComprobante, chkComprobante, "IDTipoComprobante", "Seleccione correctamente el tipo de comprobante!") = False Then
                Exit Sub
            End If

            If Where = "" Then
                Where = " Where (Comprobante Like '%" & txtComprobante.Text.Trim & "%') "
            Else
                Where = Where & " And (Comprobante Like '%" & txtComprobante.Text.Trim & "%') "
            End If

        End If

        'Usuario
        If EstablecerCondicion(cbxUsuario, chkUsuario, "IDUsuario", "Seleccione correctamente el usuario!") = False Then
            Exit Sub
        End If

        'Fecha
        If chkFecha.Checked = True Then

            If Where = "" Then
                Where = " Where (Fecha Between '" & CSistema.FormatoFechaDesdeHastaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaDesdeHastaBaseDatos(dtpHasta, False, True) & "') "
            Else
                Where = Where & " And (Fecha Between '" & CSistema.FormatoFechaDesdeHastaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaDesdeHastaBaseDatos(dtpHasta, False, True) & "') "
            End If

        End If

        lvOperacion.Items.Clear()
        lvDetalle.Items.Clear()

        CSistema.SqlToLv(lvOperacion, Consulta & " " & Where & " Order By Fecha")

        'Formato
        lvOperacion.Columns(lvOperacion.Columns.Count - 1).Width = 0

        'Totales
        txtCantidadOperacion.txt.Text = lvOperacion.Items.Count


    End Sub

    'Listar Detalle
    Sub ListarDetalle()

        ctrError.Clear()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(10).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Limpiar ListView
        lvDetalle.Items.Clear()

        'Obtener el IDTransaccion
        IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(10).Text)
        Dim Consulta As String = "Select ID, Producto, CodigoBarra, Ref, Cajas, Cantidad From VDetalleCargaMercaderia Where IDTransaccion=" & IDTransaccion & " Order By Producto "
        CSistema.SqlToLv(lvDetalle, Consulta)
        CSistema.FormatoNumero(lvDetalle, 4, True)
        CSistema.FormatoNumero(lvDetalle, 5, False)
        txtCantidadDetalle.txt.Text = lvDetalle.Items.Count

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        If Seleccion = False Then
            Exit Sub
        End If

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(10).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CInt(lvOperacion.SelectedItems(0).SubItems(10).Text)

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaCompra_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub lvOperacion_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        SeleccionarRegistro()
    End Sub

    Private Sub lvOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ListarDetalle()
    End Sub

    Private Sub chkComprobante_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If chkComprobante.Checked = True Then
            cbxComprobante.Enabled = True
            txtComprobante.Enabled = True
        Else
            cbxComprobante.Enabled = False
            txtComprobante.Enabled = False
        End If

    End Sub

    Private Sub chkUsuario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HabilitarControles(chkUsuario, cbxUsuario)
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub lvOperacion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            SeleccionarRegistro()
        End If

    End Sub

    Private Sub frmConsultaMovimiento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub chkDistribuidor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HabilitarControles(chkDistribuidor, cbxDistribuidor)
    End Sub

    Private Sub btnEmitirInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ListarOperaciones()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)

        If lvOperacion.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim Zona As String = lvOperacion.SelectedItems(0).SubItems(7).Text
        Dim frm As New frmVentasLoteDistribucionMapa
        Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select * From VVentaLoteDistribucion Where IDTransaccionLote=" & IDTransaccion).Copy
        Dim dtZona As DataTable = CSistema.ExecuteToDataTable("Select * From ZonaVenta Where Descripcion='" & Zona & "' ").Copy

        dtDetalle.Columns.Add("Seleccionado")
        dtDetalle.Columns("Seleccionado").SetOrdinal(0)

        For Each oRow As DataRow In dtDetalle.Rows
            oRow("Seleccionado") = True
        Next

        frm.OcxVentasLoteDistribucion1.dt = dtDetalle
        frm.OcxVentasLoteDistribucion1.Poligono = dtZona.Rows(0)("Poligono").ToString
        frm.OcxVentasLoteDistribucion1.Color = dtZona.Rows(0)("ColorPoligono").ToString
        frm.Inicializar()
        frm.WindowState = FormWindowState.Maximized
        frm.ShowDialog()
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If chkFecha.Checked = True Then
            dtpDesde.Enabled = True
            dtpHasta.Enabled = True
        Else
            dtpDesde.Enabled = False
            dtpHasta.Enabled = False
        End If
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmDevolucionLoteConsulta_Activate()
        Me.Refresh()
    End Sub
End Class
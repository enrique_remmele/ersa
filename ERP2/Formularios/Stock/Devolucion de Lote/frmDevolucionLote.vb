﻿Imports ERP.Reporte
Public Class frmDevolucionLote

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CReporte As New CReporteLotes

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtLote As New DataTable
    Dim dtDetalleLote As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Otros
        flpRegistradoPor.Visible = False

        'Controles

        'Propiedades
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "DEVOLUCION DE LOTE", "DDL")
        CargarInformacion()

        'Botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, btnBusquedaAvanzada, New Button, vControles)

        ' Cargar el Ultimo Regidtro
        ManejarTecla(New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxLote)
        CSistema.CargaControl(vControles, txtObservacion)

        'Detalle
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        CSistema.CargaControl(vControles, cbxUnidad)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, lklControlMercaderia)

        'CARGAR CONTROLES
        cbxUnidad.Items.Add("UNIDAD")
        cbxUnidad.Items.Add("CAJA")
        cbxUnidad.DropDownStyle = ComboBoxStyle.DropDownList
        cbxUnidad.SelectedIndex = 0

        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Where ID=" & vgIDSucursal & " Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion & "")

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleDevolucionLote").Clone

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Unidad
        cbxUnidad.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", "")

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Unidad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "UNIDAD", cbxUnidad.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

        'Cargamos lotes pendientes
        'dtLote = CSistema.ExecuteToDataTable("Select * From VLoteDistribucion Where dbo.FLoteValidoParaDevolucion(IDTransaccion) = 'True' And IDSucursal=" & vgIDSucursal & " ").Copy
        dtLote = CSistema.ExecuteToDataTable("Select * From VDevolucionLote Where IDSucursal=" & vgIDSucursal & " ").Copy
        CSistema.SqlToComboBox(cbxLote.cbx, dtLote, "IDTransaccion", "Comprobante")

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        'Cabecera
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From DevolucionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en el la fecha
        txtComprobante.txt.Focus()

    End Sub

    Sub Cancelar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        ManejarTecla(New KeyEventArgs(Keys.End))

    End Sub

    Sub Buscar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

        'Otros

        Dim frm As New frmDevolucionLoteConsulta
        frm.Seleccion = True
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.ShowDialog(Me)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarProducto()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        If cbxUnidad.Text = "UNIDAD" Then
            CantidadProducto = txtCantidad.ObtenerValor
            EsCaja = False
        Else
            CantidadCaja = txtCantidad.ObtenerValor
            CantidadProducto = CDec(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
            EsCaja = True
        End If

        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dtDetalle.NewRow()

        'Obtener Valores
        dRow("IDProducto") = txtProducto.Registro("ID").ToString
        dRow("Ref") = txtProducto.Registro("Ref").ToString
        dRow("ID") = dtDetalle.Rows.Count
        dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
        dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString
        dRow("Cantidad") = CantidadProducto

        Dim Cajas As Decimal
        If IsNumeric(txtProducto.Registro("UnidadPorCaja")) = False Then
            txtProducto.Registro("UnidadPorCaja") = 1
        End If

        If txtProducto.Registro("UnidadPorCaja") <= 0 Then
            txtProducto.Registro("UnidadPorCaja") = 1
        End If

        Cajas = CDec(CantidadProducto / txtProducto.Registro("UnidadPorCaja"))
        dRow("Cajas") = Cajas

        'Si existe el producto, sumamos los totales
        If dtDetalle.Select(" IDProducto = " & txtProducto.Registro("ID").ToString).Count > 0 Then
            For Each oRow As DataRow In dtDetalle.Select(" IDProducto = " & txtProducto.Registro("ID").ToString)

                oRow("Cantidad") = oRow("Cantidad") + CantidadProducto

                'Recalcular las cajas
                Cajas = CDec(oRow("Cantidad") / txtProducto.Registro("UnidadPorCaja"))
                oRow("Cajas") = Cajas

            Next
        Else
            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)
        End If

        ListarDetalle()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        lvLista.Items.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        For Each oRow As DataRow In dtDetalle.Rows

            Dim item As ListViewItem = lvLista.Items.Add(oRow("IDProducto").ToString)
            item.SubItems.Add(oRow("Ref").ToString.Trim)
            item.SubItems.Add(oRow("Descripcion").ToString.Trim)
            item.SubItems.Add(oRow("CodigoBarra").ToString.Trim)
            item.SubItems.Add(CSistema.FormatoNumero(oRow("Cajas").ToString, True))
            item.SubItems.Add(CSistema.FormatoNumero(oRow("Cantidad").ToString))
        Next

        lvLista.Columns(0).Width = 0

        'Bloqueamos la cabecera si corresponde

    End Sub

    Sub ObtenerInformacionLote()

        'Limpiar los controles
        txtDistribuidor.txt.Clear()
        txtVehiculo.txt.Clear()
        txtChofer.txt.Clear()
        txtFecha.txt.Clear()
        txtFechaReparto.txt.Clear()
        txtZona.txt.Clear()

        dtDetalleLote.Rows.Clear()
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Validar
        If IsNumeric(cbxLote.cbx.SelectedValue) = False Then
            CSistema.MostrarError("Seleccione correctamente el lote!", ctrError, cbxLote, tsslEstado, ErrorIconAlignment.BottomLeft)
            Exit Sub
        End If

        If dtLote.Select(" IDTransaccion = " & cbxLote.cbx.SelectedValue).Count = 0 Then
            Exit Sub
        End If

        'Cargar el detalle del lote
        Dim oRowLote As DataRow = dtLote.Select(" IDTransaccion = " & cbxLote.cbx.SelectedValue)(0)
        dtDetalleLote = CSistema.ExecuteToDataTable("Select * From VDetalleLoteDistribucion Where IDTransaccion=" & oRowLote("IDTransaccion").ToString & " ").Copy
        txtProducto.Conectar(dtDetalleLote)

        'Asignamos los valores a los controles
        txtDistribuidor.txt.Text = oRowLote("Distribuidor").ToString
        txtVehiculo.txt.Text = oRowLote("Camion").ToString
        txtChofer.txt.Text = oRowLote("Chofer").ToString
        txtZona.txt.Text = oRowLote("Zona").ToString
        txtFecha.txt.Text = oRowLote("Fecha").ToString
        txtFechaReparto.txt.Text = oRowLote("FechaReparto").ToString

    End Sub

    Sub ControlMercaderias()

        Dim frm As New frmDevolucionLoteControl
        frm.Comprobante = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text
        frm.dtLote = dtDetalleLote.Copy
        frm.dtCarga = dtDetalle.Copy
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.ShowDialog(Me)

    End Sub

    Sub EliminarProducto()

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        'Recorrer los seleccionados
        For Each item As ListViewItem In lvLista.SelectedItems

            Dim IDProducto As String = item.Text

            'Quitar el producto
            For Each oRow As DataRow In dtDetalle.Select(" IDProducto=" & IDProducto & " ")
                oRow.Delete()
            Next

        Next

        ListarDetalle()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Tipo Comprobante
        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Lote
            If cbxLote.Validar("Seleccione correctamente el lote!", ctrError, btnGuardar, tsslEstado) = False Then
                Exit Function
            End If

        End If

        'Cantidades
        If dtDetalle.Rows.Count = 0 Then
            CSistema.MostrarError("El registro no tiene ningun detalle!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If

        Dim frm As New frmDevolucionLoteControl
        frm.dtLote = dtDetalleLote.Copy
        frm.dtCarga = dtDetalle.Copy
        frm.Analizar()
        If frm.Valido = False Then
            CSistema.MostrarError("Las cantidades no concuerdan! Haga click en Controlar de Mercaderias para mas detalle.", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.BottomRight)
            Exit Function
        End If


        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion.ToString, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDTransaccionLote", cbxLote.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpDevolucionLote", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro si es que se registro
            If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From DevolucionLote Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                CSistema.ExecuteStoreProcedure(param, "SpDevolucionLote", False, False, MensajeRetorno, IDTransaccion)
            End If

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

                'Insertamos el Detalle
                If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

                    'Eliminar el Registro si es que se registro
                    If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From LoteDistribucion Where IDTransaccion=1) Is Null Then 'False' Else 'True' End)")) = True Then
                        param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                        CSistema.ExecuteStoreProcedure(param, "SpLoteDistribucion", False, False, MensajeRetorno, IDTransaccion)
                    End If

                    Exit Sub

                End If

            End If

        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, btnBusquedaAvanzada, New Button, vControles)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True
        Dim i As Integer = 0

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter
            i = i + 1

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CantidadCaja", CSistema.FormatoNumeroBaseDatos(oRow("Cajas").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleDevolucionLote", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
                Return False
            End If

        Next

    End Function

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From DevolucionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, btnBusquedaAvanzada, New Button, vControles)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VDevolucionLote Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleDevolucionLote Where IDTransaccion=" & IDTransaccion & " Order By Descripcion").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        cbxLote.cbx.Text = oRow("Lote").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtFechaReparto.SetValueFromString(CDate(oRow("FechaReparto").ToString))

        txtDistribuidor.txt.Text = oRow("Distribuidor").ToString
        txtVehiculo.txt.Text = oRow("Vehiculo").ToString
        txtChofer.txt.Text = oRow("Chofer").ToString
        txtZona.txt.Text = oRow("Zona").ToString

        txtObservacion.txt.Text = oRow("Observacion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        ListarDetalle()

    End Sub

    Sub ManejarTecla(ByRef e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VDevolucionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VDevolucionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub Imprimir()
        Dim Where As String
        Dim Titulo As String = "Listado de Devolucion de Lotes"

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & IDTransaccion

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        CReporte.ListadoDevolucionLote(frm, Where, Titulo, vgUsuarioIdentificador)
    End Sub

    Private Sub frmDevolucionLote_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmDevolucionLote_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmDevolucionLote_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Down Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or e.KeyCode = Keys.Up Then

            'Solo ejecutar fuera de ciertos controles
            If txtID.txt.Focused = True Then
                Exit Sub
            End If

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)


    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Private Sub cbxLote_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxLote.KeyUp
        If e.KeyCode = Keys.Enter Then
            ObtenerInformacionLote()
        End If
    End Sub

    Private Sub OcxTXTProducto1_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            cbxUnidad.Focus()
        End If
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub cbxLote_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxLote.Leave
        ObtenerInformacionLote()
    End Sub

    Private Sub lklControlMercaderia_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklControlMercaderia.LinkClicked
        ControlMercaderias()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDevolucionLote_Activate()
        Me.Refresh()
    End Sub
End Class
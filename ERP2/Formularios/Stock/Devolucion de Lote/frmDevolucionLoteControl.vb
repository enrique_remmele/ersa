﻿Public Class frmDevolucionLoteControl

    'CLASES
    Dim CSistema As New CSistema

    'VARABLES
    Dim colorOK As Color = Color.LightGreen
    Dim colorPendiente As Color = Color.LightBlue
    Dim colorBlanco As Color = Color.White
    Dim colorIncompleto As Color = Color.LightYellow
    Dim colorDesfasado As Color = Color.Red

    'PROPIEDADES
    Private dtLoteValue As DataTable
    Public Property dtLote() As DataTable
        Get
            Return dtLoteValue
        End Get
        Set(ByVal value As DataTable)
            dtLoteValue = value
        End Set
    End Property

    Private dtCargaValue As DataTable
    Public Property dtCarga() As DataTable
        Get
            Return dtCargaValue
        End Get
        Set(ByVal value As DataTable)
            dtCargaValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private ValidoValue As Boolean
    Public Property Valido() As Boolean
        Get
            Return ValidoValue
        End Get
        Set(ByVal value As Boolean)
            ValidoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.Text = "Comprobacion de Mercaderias :: " & Comprobante
        Valido = False

        'Funciones

        Analizar()
        Listar("")

    End Sub

    Sub Analizar()

        dtLote.Columns.Add("Saldo")
        dtLote.Columns.Add("Estado")

        Valido = True

        For Each oRow As DataRow In dtLote.Rows

            'Verificar si existe
            If dtCarga.Select(" IDProducto = " & oRow("IDProducto").ToString).Count = 0 Then
                oRow("Saldo") = oRow("Cantidad")
                oRow("Cantidad") = 0
                oRow("Cajas") = 0
                oRow("Estado") = "---"
            Else

                Dim CantidadLote As Decimal = oRow("Cantidad")
                Dim CantidadCarga As Decimal = dtCarga.Select(" IDProducto = " & oRow("IDProducto").ToString)(0)("Cantidad")
                Dim Cajas As Decimal = dtCarga.Select(" IDProducto = " & oRow("IDProducto").ToString)(0)("Cajas")
                Dim Saldo As Decimal = CantidadLote - CantidadCarga

                oRow("Saldo") = Saldo
                oRow("Cantidad") = CantidadCarga
                oRow("Cajas") = Cajas

                Select Case Saldo

                    Case Is < 0 'Desfasado
                        oRow("Estado") = "Desfasado"
                        Valido = False
                    Case Else
                        If Saldo <> CantidadLote Then
                            oRow("Estado") = "Devuelto"
                        Else
                            oRow("Estado") = "---"
                        End If

                End Select
            End If
        Next

    End Sub

    Sub Listar(ByVal Filtro As String)

        'Limpiar Lista
        lvLista.Items.Clear()

        Dim dtTemp As New DataTable
        Dim dtResultado As New DataTable

        dtResultado.Columns.Add("IDProducto")
        dtResultado.Columns.Add("Ref")
        dtResultado.Columns.Add("Producto")
        dtResultado.Columns.Add("CodigoBarra")
        dtResultado.Columns.Add("Cajas")
        dtResultado.Columns.Add("Cantidad")
        dtResultado.Columns.Add("Saldo")
        dtResultado.Columns.Add("Estado")

        'Filtramos si es necesario
        If Filtro <> "" Then
            dtTemp = CSistema.FiltrarDataTable(dtLote, Filtro).Copy
        Else
            dtTemp = dtLote.Copy
        End If

        'Cargamos la lista
        For Each oRow As DataRow In dtTemp.Rows
            Dim NewRow As DataRow = dtResultado.NewRow

            For i As Integer = 0 To dtResultado.Columns.Count - 1
                Dim Columna As String = dtResultado.Columns(i).ColumnName
                NewRow(Columna) = oRow(Columna)
            Next

            dtResultado.Rows.Add(NewRow)

        Next

        CSistema.dtToLv(lvLista, dtResultado)

        'Formato
        lvLista.Columns(0).Width = 0
        CSistema.FormatoNumero(lvLista, 4, True)
        CSistema.FormatoNumero(lvLista, 5, False)
        CSistema.FormatoNumero(lvLista, 6, False)

        PintarLV()

    End Sub

    Sub PintarLV()

        For Each item As ListViewItem In lvLista.Items
            Select Case item.SubItems(6).Text.ToUpper

                Case "OK"
                    item.BackColor = colorBlanco
                Case "---"
                    item.BackColor = colorBlanco
                Case "DEVUELTO"
                    item.BackColor = Me.colorPendiente
                Case "DESFASADO"
                    item.BackColor = colorDesfasado
            End Select

        Next

    End Sub

    Private Sub frmCargaMercaderiaControl_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim Tipo As String = ""

        If rdbTodos.Checked = True Then
            Tipo = ""
        End If

        If rdbDevueltos.Checked = True Then
            Tipo = " Estado = 'Devuelto' "
        End If

        If rdbDesfasados.Checked = True Then
            Tipo = " Estado = 'Desfasado' "
        End If

        Listar(Tipo)

    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmDevolucionLoteControl_Activate()
        Me.Refresh()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCargaMercaderiaControl
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rdbDesfasados = New System.Windows.Forms.RadioButton()
        Me.rdbImcompletos = New System.Windows.Forms.RadioButton()
        Me.rdbPendientes = New System.Windows.Forms.RadioButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.rdbTodos = New System.Windows.Forms.RadioButton()
        Me.lblFiltro = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lvLista, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(666, 451)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 418)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(660, 30)
        Me.Panel2.TabIndex = 2
        '
        'lvLista
        '
        Me.lvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLista.Location = New System.Drawing.Point(3, 33)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(660, 379)
        Me.lvLista.TabIndex = 0
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.rdbDesfasados)
        Me.Panel1.Controls.Add(Me.rdbImcompletos)
        Me.Panel1.Controls.Add(Me.rdbPendientes)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.rdbTodos)
        Me.Panel1.Controls.Add(Me.lblFiltro)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(660, 24)
        Me.Panel1.TabIndex = 1
        '
        'rdbDesfasados
        '
        Me.rdbDesfasados.AutoSize = True
        Me.rdbDesfasados.BackColor = System.Drawing.SystemColors.Control
        Me.rdbDesfasados.Location = New System.Drawing.Point(265, 4)
        Me.rdbDesfasados.Name = "rdbDesfasados"
        Me.rdbDesfasados.Size = New System.Drawing.Size(81, 17)
        Me.rdbDesfasados.TabIndex = 5
        Me.rdbDesfasados.TabStop = True
        Me.rdbDesfasados.Text = "Desfasados"
        Me.rdbDesfasados.UseVisualStyleBackColor = False
        '
        'rdbImcompletos
        '
        Me.rdbImcompletos.AutoSize = True
        Me.rdbImcompletos.BackColor = System.Drawing.SystemColors.Control
        Me.rdbImcompletos.Location = New System.Drawing.Point(183, 4)
        Me.rdbImcompletos.Name = "rdbImcompletos"
        Me.rdbImcompletos.Size = New System.Drawing.Size(82, 17)
        Me.rdbImcompletos.TabIndex = 4
        Me.rdbImcompletos.TabStop = True
        Me.rdbImcompletos.Text = "Incompletos"
        Me.rdbImcompletos.UseVisualStyleBackColor = False
        '
        'rdbPendientes
        '
        Me.rdbPendientes.AutoSize = True
        Me.rdbPendientes.BackColor = System.Drawing.SystemColors.Control
        Me.rdbPendientes.Location = New System.Drawing.Point(105, 4)
        Me.rdbPendientes.Name = "rdbPendientes"
        Me.rdbPendientes.Size = New System.Drawing.Size(78, 17)
        Me.rdbPendientes.TabIndex = 3
        Me.rdbPendientes.TabStop = True
        Me.rdbPendientes.Text = "Pendientes"
        Me.rdbPendientes.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(346, 1)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Listar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'rdbTodos
        '
        Me.rdbTodos.AutoSize = True
        Me.rdbTodos.Location = New System.Drawing.Point(50, 4)
        Me.rdbTodos.Name = "rdbTodos"
        Me.rdbTodos.Size = New System.Drawing.Size(55, 17)
        Me.rdbTodos.TabIndex = 1
        Me.rdbTodos.TabStop = True
        Me.rdbTodos.Text = "Todos"
        Me.rdbTodos.UseVisualStyleBackColor = True
        '
        'lblFiltro
        '
        Me.lblFiltro.AutoSize = True
        Me.lblFiltro.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFiltro.Location = New System.Drawing.Point(3, 6)
        Me.lblFiltro.Name = "lblFiltro"
        Me.lblFiltro.Size = New System.Drawing.Size(43, 13)
        Me.lblFiltro.TabIndex = 0
        Me.lblFiltro.Text = "Filtrar:"
        '
        'frmCargaMercaderiaControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(666, 451)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmCargaMercaderiaControl"
        Me.Text = "frmCargaMercaderiaControl"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents rdbDesfasados As System.Windows.Forms.RadioButton
    Friend WithEvents rdbImcompletos As System.Windows.Forms.RadioButton
    Friend WithEvents rdbPendientes As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents rdbTodos As System.Windows.Forms.RadioButton
    Friend WithEvents lblFiltro As System.Windows.Forms.Label
    Friend WithEvents lvLista As System.Windows.Forms.ListView

End Class

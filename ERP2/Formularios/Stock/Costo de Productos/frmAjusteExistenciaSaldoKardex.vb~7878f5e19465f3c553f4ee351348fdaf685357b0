﻿Public Class frmAjusteExistenciaSaldoKardex
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Conectar()

        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "AJUSTE-EXISTENCIA-KARDEX", "AEK")
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, chkProducto)
        CSistema.CargaControl(vControles, chkTipoProducto)
        CSistema.CargaControl(vControles, btnConsultar)

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleAjusteExistenciaSaldoKardex").Clone

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select Distinct ID, CodigoCiudad  From VSucursal where ID = 1 Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VAjusteExistenciaSaldoKardex Where IDSucursal = 1),1)"), Integer)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        txtObservacion.txt.Clear()
        txtCantidad.txt.ResetText()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM VAjusteExistenciaSaldoKardex Where IDSucursal = 1),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en la fecha
        If txtFecha.Enabled = True Then
            txtFecha.txt.Focus()
        Else
            txtObservacion.txt.Focus()
        End If

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VAjusteExistenciaSaldoKardex Where IDSucursal = 1),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Observacion
        If txtObservacion.GetValue.Trim.Length = 0 Then
            MessageBox.Show("Introduzca una breve observacion al registro para continuar!", "Observacion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Function
        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input) 'JGR 20140816
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAjusteExistenciaSaldoKardex", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

                'Eliminar Registro
                Exit Sub

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ExistenciaValorizada", CSistema.FormatoNumeroBaseDatos(oRow("ExistenciaValorizada"), True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ExistenciaKardexAnterior", CSistema.FormatoNumeroBaseDatos(oRow("ExistenciaKardexAnterior"), True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CantidadAjuste", CSistema.FormatoNumeroBaseDatos(oRow("CantidadAjuste"), True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleAjusteExistenciaSaldoKardex", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                'Return False

            End If

        Next

    End Function

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, New Button, New Button, New Button, vControles)

    End Sub

    Sub CargarDetale(ByVal dtProductosACargar As DataTable)

        For Each row In dtProductosACargar.Rows

            'Cargamos el registro en el detalle
            Dim dRow As DataRow = dtDetalle.NewRow()

            'Obtener Valores
            dRow("IDProducto") = row("IDProducto").ToString
            dRow("Referencia") = row("Referencia").ToString
            dRow("Producto") = row("Producto").ToString
            dRow("Costo") = row("CostoPromedio")
            dRow("ExistenciaValorizada") = row("Existencia") - row("CantidadAnulados")
            dRow("ExistenciaKardexAnterior") = row("ExistenciaKardex")
            dRow("CantidadAjuste") = row("DiferenciaEnExistencias") * -1

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

        Next
    End Sub

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtDetalle.Rows(dgw.SelectedRows(0).Index).Delete()

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        txtCantidad.SetValue(0)

        'Cargamos registro por registro
        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Referencia").DisplayIndex = 1
        dgw.Columns("Producto").DisplayIndex = 2
        dgw.Columns("ExistenciaValorizada").DisplayIndex = 3
        dgw.Columns("ExistenciaKardexAnterior").DisplayIndex = 4
        dgw.Columns("CantidadAjuste").DisplayIndex = 5
        dgw.Columns("Costo").DisplayIndex = 6

        dgw.Columns("Referencia").Visible = True
        dgw.Columns("Producto").Visible = True
        dgw.Columns("ExistenciaValorizada").Visible = True
        dgw.Columns("ExistenciaKardexAnterior").Visible = True
        dgw.Columns("CantidadAjuste").Visible = True
        dgw.Columns("Costo").Visible = True
        dgw.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgw.Columns("Costo").DefaultCellStyle.Format = "N2"
        dgw.Columns("Costo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        'Calculamos el Total
        txtCantidad.SetValue(dgw.RowCount)

    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Reestablecer costo
        Dim Respuesta As DialogResult = MessageBox.Show("Desea reestablecer el costo anterior?", "Reestablecer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)

        Select Case Respuesta
            Case Windows.Forms.DialogResult.Cancel
                Exit Sub
            Case Windows.Forms.DialogResult.Yes
                CSistema.SetSQLParameter(param, "@ReestablecerCostoAnterior", "True", ParameterDirection.Input)
        End Select
        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpAjusteExistenciaSaldoKardex", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VAjusteExistenciaSaldoKardex Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Top(1) * From VAjusteExistenciaSaldoKardex Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VDetalleAjusteExistenciaSaldoKardex Where IDTransaccion=" & IDTransaccion & " ").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtObservacion.txt.Text = oRow("Observacion").ToString


        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), 1) From VAjusteExistenciaSaldoKardex Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), 1) From VAjusteExistenciaSaldoKardex Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If


        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Sub EstablecerCondicionSP(ByRef Where As String)

        Where = " @Fecha = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' "

        If chkTipoProducto.chk.Checked Then
            Where = Where & " ,@IDTipoProducto = " & cbxTipoProducto.cbx.SelectedValue
        End If

        If chkProducto.chk.Checked Then
            Where = Where & " ,@IDProducto = " & txtProducto.Registro("ID")
        End If


    End Sub

    Sub Consultar()

        Dim Where As String = ""

        EstablecerCondicionSP(Where)

        Dim dtExistenciaValorizadaComparativoKardex As DataTable = CSistema.ExecuteToDataTable("Exec SpViewExistenciaValorizadaComparativoKardex " & Where,, 60000)
        Try
            CargarDetale(dtExistenciaValorizadaComparativoKardex)


        Catch Ex As Exception
            MessageBox.Show(Ex.Message, "Atencion", MessageBoxButtons.OK)
        Finally

        End Try


        ListarDetalle()

        'Inicializamos los valores
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""


    End Sub

    Private Sub frmAjusteExistenciaSaldoKardexStock_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAjusteExistenciaSaldoKardexStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        EliminarProducto()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnImportar_Click(sender As System.Object, e As System.EventArgs) Handles btnConsultar.Click
        Consultar()
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub
End Class
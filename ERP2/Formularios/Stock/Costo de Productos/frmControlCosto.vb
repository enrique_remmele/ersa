﻿Public Class frmControlCosto

    'Clases
    Dim CSistema As New CSistema
    Dim dt As New DataTable
    Dim IDTransaccion As Integer
    Dim Fecha As String
    Dim IDOperacion As Integer
    Dim vControles() As Control

    'Funciones
    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button
        txtProducto.Conectar()

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "CONTROL-COSTO", "CCOS")
        IDTransaccion = 0

        'Funciones
        CargarInformacion()
        CargarOperacion()
        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub CargarInformacion()
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtDesde)
        CSistema.CargaControl(vControles, txtHasta)
        CSistema.CargaControl(vControles, btnListar)
        CSistema.CargaControl(vControles, txtCosto)
        CSistema.CargaControl(vControles, btnCalcular)
        'CSistema.CargaControl(vControles, dgw)

        'CARGAR ESTRUCTURA DEL DETALLE
        dt = CSistema.ExecuteToDataTable("select Top(0) 'Sel'=Cast(0 as bit),  IDTransaccion, idproducto, producto, referencia, fecha, movimiento,operacion, tipo, [cod.], [Cliente/Proveedor], Entrada, Salida, Saldo, Costo, 'Calculado'=0, 'Diferencia'=0, EstableceCosto from VExtractoMovimientoProductoDetalleCosteado").Clone

        'CARGAR LA ULTIMA CONFIGURACION
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM aControlCosto),1)"), Integer)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vaControlCosto Where Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            MessageBox.Show("El sistema no encuentra el registro!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dt.Clear()

        Dim dtCabecera As DataTable = CSistema.ExecuteToDataTable("Select Top(1) * From vaControlCosto Where IDTransaccion=" & IDTransaccion)
        dt = CSistema.ExecuteToDataTable("Select * From vaControlCostoDetalle Where IDTransaccion=" & IDTransaccion & " ").Copy

        'Cargamos la cabecera
        If dtCabecera Is Nothing Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtCabecera.Rows(0)

        txtID.txt.Text = oRow("Numero").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        'If CBool(oRow("Anulado").ToString) = True Then
        '    flpAnuladoPor.Visible = True
        '    lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
        '    lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        'Else
        '    flpAnuladoPor.Visible = False
        'End If

        'Cargamos el detalle
        Listar("Select *, 'EstableceCosto'=0 from VaControlCostoDetalle where idtransaccion = " & IDTransaccion & "Order by Fecha ASC")

    End Sub


    Sub Listar(Optional sql As String = "")
        If sql <> "" Then
            dt = CSistema.ExecuteToDataTable(sql)
        Else

            If txtProducto.Seleccionado = False Then
                MessageBox.Show("Seleccione un producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            sql = "select 'Sel'=Cast(0 as bit),  IDTransaccion, idproducto, producto, referencia, fecha, movimiento,operacion, tipo, [cod.], [Cliente/Proveedor], Entrada, Salida, Saldo, Costo, 'Calculado'=0.000, 'Diferencia'=0.000, EstableceCosto from VExtractoMovimientoProductoDetalleCosteado where idproducto = " & txtProducto.Registro("ID").ToString & " and Fecha Between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "' order by fecha asc"

            'Solo los que tienen diferencias
            dt = CSistema.ExecuteToDataTable(sql)

            Dim i As Integer = 0
            Dim SaldoAnterior As Decimal = 0
            Dim Existencia As Decimal = CSistema.ExecuteScalar("Select 'Cantidad'=dbo.FExtractoMovimientoProducto(" & txtProducto.Registro("ID") & ",'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "','False')", "", 1000)
            For Each oRow As DataRow In dt.Rows

                If i = 0 Then
                    SaldoAnterior = Existencia
                End If

                oRow("Saldo") = (SaldoAnterior + CDec(oRow("Entrada"))) - CDec(oRow("Salida"))
                SaldoAnterior = CDec(oRow("Saldo"))

                i = 1

            Next
        End If
        '
        CSistema.dtToGrid(dgvLista, dt)

        'Formato
        If dgvLista.ColumnCount = 0 Then
            Exit Sub
        End If

        dgvLista.Columns("IDProducto").Visible = False
        dgvLista.Columns("EstableceCosto").Visible = False
        dgvLista.Columns("IDTransaccion").Visible = False


        'dgvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'dgvLista.Columns("Costo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        'dgvLista.Columns("Costo").DefaultCellStyle.Format = "N0"


        ''Totales
        'txtCantidad.SetValue(dgvLista.RowCount)
        'txtTotal.SetValue(CSistema.gridSumColumn(dgvLista, "Total"))
        'txtDifNegativa.SetValue(CSistema.gridSumColumn(dgvLista, "Total", "Dif", "<", "0"))
        'txtDifPositiva.SetValue(CSistema.gridSumColumn(dgvLista, "Total", "Dif", ">", "0"))


    End Sub

    Sub Calcular(ByVal vFecha As String)

        dt = dt.Select("Fecha >= '" & vFecha & "'").CopyToDataTable
        Dim i As Integer = 0
        Dim Costo As Decimal
        Dim CantidadAnterior As Decimal
        Dim CostoAnterior As Decimal
        For Each oRow As DataRow In dt.Rows

            If i = 0 Then
                Costo = CSistema.FormatoNumero(txtCosto.ObtenerValor, True)
                CantidadAnterior = oRow("Saldo")
            Else
                If oRow("EstableceCosto") = 1 Then
                    If oRow("Saldo") < 0 Then
                        GoTo Seguir
                    End If
                    Costo = (((oRow("Costo") * oRow("Entrada")) + ((Costo * CantidadAnterior))) / (CantidadAnterior + oRow("Entrada")))
                End If
            End If
            If Costo > 10000 Then
                Dim a As Integer
                a = 1
            End If
            CostoAnterior = Costo

            If oRow("Saldo") >= 0 Then
                CantidadAnterior = oRow("Saldo")
            End If

Seguir:
            oRow("Calculado") = Costo
            oRow("Diferencia") = oRow("Costo") - Costo


            i = 1

        Next

        CSistema.dtToGrid(dgvLista, dt)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, New Button, New Button, vControles)

    End Sub

    Sub Nuevo()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        IDTransaccion = 0
        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM aControlCosto),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

    End Sub

    Sub Actualizar()


        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(Today.Date.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpControlCosto", False, False, MensajeRetorno, IDTransaccion) = False Then

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS)

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Sub InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro)

        Dim CostoPromedio As String
        For Each oRow As DataRow In dt.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionOperacion", oRow("IDTransaccion"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CostoAnterior", CSistema.FormatoMonedaBaseDatos(oRow("Costo"), True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Costo", CSistema.FormatoMonedaBaseDatos(oRow("Calculado"), True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CantidadSaldo", CSistema.FormatoNumeroBaseDatos(oRow("Saldo"), True), ParameterDirection.Input)
            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpActualizarCostoOperacion", False, False, MensajeRetorno) = False Then
                ' MessageBox.Show("No se realizo la Actualizacion", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                '  Exit Sub
            End If
            CostoPromedio = CSistema.FormatoMonedaBaseDatos(oRow("Calculado"), True)
            CostoPromedio = CostoPromedio.Replace(",", ".")
        Next
        If chkActualizarCosto.chk.Checked Then
            CSistema.ExecuteNonQuery("Update Producto Set CostoPromedio=" & CostoPromedio & " Where ID =" & txtProducto.Registro("ID") & "")
        End If

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), 1) From VaControlCosto"), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), 1) From VaControlCosto "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Private Sub frmControlExistencia_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me)
    End Sub

    Private Sub frmControlExistencia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(dgvLista.DataSource)
    End Sub

    Private Sub dgvLista_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLista.CellClick
        Try

            If dgvLista.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            If dgvLista.Rows(e.RowIndex).Cells("EstableceCosto").Value = 0 Then
                dgvLista.ReadOnly = True
                Exit Sub
            End If

            If e.ColumnIndex = dgvLista.Columns.Item("Sel").Index Then
                dgvLista.ReadOnly = False
            Else
                dgvLista.ReadOnly = True
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvLista_CellEndEdit(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLista.CellEndEdit
        Try
            txtCosto.Texto = dgvLista.Rows(e.RowIndex).Cells("Costo").Value
            IDTransaccion = dgvLista.Rows(e.RowIndex).Cells("IDTransaccion").Value
            Fecha = dgvLista.Rows(e.RowIndex).Cells("Fecha").Value
            For Each Row As DataGridViewRow In dgvLista.Rows
                If Row.Index <> e.RowIndex Then
                    Row.Cells("Sel").Value = 0
                End If
            Next

        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub btnCalcular_Click(sender As System.Object, e As System.EventArgs) Handles btnCalcular.Click

        Calcular(Fecha)

    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Actualizar()
    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub txtID_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub txtHasta_FocoPerdido() Handles txtHasta.FocoPerdido
        If CDate(txtHasta.txt.Text) = Today Then
            chkActualizarCosto.Enabled = True
        End If
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmControlCosto_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmConsultaCostoProducto

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Public Property Seleccionado As Boolean

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim dtBanco As DataTable
    Dim dtCliente As DataTable

    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'TextBox
        txtCantidad.txt.ResetText()
        txtOperacion.txt.ResetText()

        'DateTimePicker
        dtpDesde.Value = "01-" & Date.Now.Month & "-" & Date.Now.Year
        dtpHasta.Value = Date.Now

        'Funciones
        CargarInformacion()

        'Foco

    End Sub

    Sub CargarInformacion()

        'CARGAR LA ULTIMA CONFIGURACION

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub ListarOperaciones(Optional ByVal Numero As String = "", Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Consulta = "Select IDTransaccion, Num, Fecha, Observacion, Estado, usuario From VProductoCosto "
        Where = " Where (Fecha  Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "

        If Condicion <> "" Then
            Where = Condicion
        End If

        'Solo por numero
        If IsNumeric(Numero) = True Then
            Where = " Where Numero = " & Numero & ""
        End If

        CSistema.SqlToDataGrid(dgvLista, Consulta & " " & Where & " Order By Numero")

        'Formato
        If dgvLista.ColumnCount = 0 Then
            Exit Sub
        End If

        dgvLista.Columns("IDTransaccion").Visible = False
        dgvLista.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvLista.Columns("Fecha").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        txtCantidad.txt.Text = dgvLista.Rows.Count

    End Sub

    Sub SeleccionarRegistro()

        'Validar
        If dgvLista.SelectedRows.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgvLista, Mensaje)
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(dgvLista.SelectedRows(0).Cells(0).Value) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(dgvLista, Mensaje)
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = dgvLista.SelectedRows(0).Cells("IDTransaccion").Value

        Seleccionado = True
        Me.Close()

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn2.Click
        ListarOperaciones()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarOperaciones(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dtpDesde_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpDesde.ValueChanged
        ListarOperaciones()
    End Sub

    Private Sub dtpHasta_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpHasta.ValueChanged
        ListarOperaciones()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Seleccionado = False
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        SeleccionarRegistro()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaControlCosto_Activate()
        Me.Refresh()
    End Sub
End Class
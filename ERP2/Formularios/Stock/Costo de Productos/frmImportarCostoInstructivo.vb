﻿Public Class frmImportarCostoInstructivo

    Dim CSistema As New CSistema

    Sub Inicializar()
        txtInstrucciones.Text = "En un excel, crea dos columnas: Codigo de Producto y Nuevo Costo. Carga los productos y los costos nuevos." & _
        "La columna de costos debe ser sin puntos separadores de miles. Copia las dos columnas SIN los titulos."
        img.Image = GetImage("Paso1")
    End Sub

    Sub SiguientePaso()
        Select Case lblPaso.Text
            Case "Paso 1"
                lblPaso.Text = "Paso 2"
                txtInstrucciones.Text = "Pega las columnas en un documento de texto y luego guardas."
                img.Image = GetImage("Paso2")
            Case "Paso 2"
                lblPaso.Text = "Paso 3"
                txtInstrucciones.Text = "Luego haces click en el boton importar del modulo Asignar Costo Producto." & _
                "Seleccionas el archivo de texto guardado y click en Abrir."
                img.Image = GetImage("Paso3")
            Case "Paso 3"
                lblPaso.Text = "Paso 1"
                txtInstrucciones.Text = "En un excel, crea dos columnas: Codigo de Producto y Nuevo Costo. Carga los productos y los costos nuevos." & _
                "La columna de costos debe ser sin puntos separadores de miles. Copia las dos columnas SIN los titulos."
                img.Image = GetImage("Paso1")
        End Select

    End Sub

    Private Function GetImage(codigo As String) As Image

        If (String.IsNullOrEmpty(codigo)) Then Return Nothing

        Dim value As Object
        value = CSistema.ExecuteScalar("SELECT imagen FROM RecursosGraficos WHERE Formulario = '" & Me.Name & "' and Descripcion = '" & codigo & "'")

        If (value Is Nothing) Then
            Return Nothing
        End If


        Dim foto As Byte() = DirectCast(value, Byte())

        Using ms As New IO.MemoryStream(foto)
            Return Image.FromStream(ms)
        End Using


    End Function


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        SiguientePaso()
    End Sub

    Private Sub frmImportarCostoInstructivo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmImportarCostoInstructivo_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmControlExistencia

    'Clases
    Dim CSistema As New CSistema

    'Funciones
    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        cbxSucursal.Conectar()
        cbxDeposito.Conectar()

    End Sub

    Sub Listar()

        Dim sql As String = "Select *, 'Dif'=(Calculado-Existencia), 'Total'=Costo*(Calculado-Existencia) From VProductosConDiferencias "
        Dim Where As String = " Where Calculado-Existencia<>0 "
        Dim OrderBy As String = " Order By Dif Desc "

        'Filtros
        cbxSucursal.EstablecerCondicion(where)
        cbxDeposito.EstablecerCondicion(where)

        'Solo los que tienen diferencias
        Dim dt As DataTable = CSistema.ExecuteToDataTable(sql & Where & OrderBy, "", 600)

        '
        'CSistema.SqlToDataGrid(dgvLista, sql & Where & OrderBy)
        CSistema.dtToGrid(dgvLista, dt)

        'Formato
        If dgvLista.ColumnCount = 0 Then
            Exit Sub
        End If

        dgvLista.Columns("IDProducto").Visible = False
        dgvLista.Columns("IDDeposito").Visible = False
        dgvLista.Columns("IDSucursal").Visible = False

        dgvLista.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvLista.Columns("Costo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Costo").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Existencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Existencia").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Calculado").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Calculado").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Dif").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Dif").DefaultCellStyle.Format = "N0"
        dgvLista.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Total").DefaultCellStyle.Format = "N0"

        'Totales
        txtCantidad.SetValue(dgvLista.RowCount)
        txtTotal.SetValue(CSistema.gridSumColumn(dgvLista, "Total"))
        txtDifNegativa.SetValue(CSistema.gridSumColumn(dgvLista, "Total", "Dif", "<", "0"))
        txtDifPositiva.SetValue(CSistema.gridSumColumn(dgvLista, "Total", "Dif", ">", "0"))


    End Sub

    Sub Actualizar()

        'Consultar
        If MessageBox.Show("Atencion!!! Este proceso actualizara la existencia del producto al calculado. No se podra revertir la operacion! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) <> DialogResult.Yes Then
            Exit Sub
        End If

        For Each oRow As DataGridViewRow In dgvLista.Rows

            Dim IDProducto As Integer = oRow.Cells("IDProducto").Value
            Dim IDDeposito As Integer = oRow.Cells("IDDeposito").Value
            Dim Cantidad As String = CSistema.FormatoNumeroBaseDatos(oRow.Cells("Calculado").Value, True)

            Dim sql As String = "Update ExistenciaDeposito "
            sql = sql & " Set Existencia=" & Cantidad
            sql = sql & " Where IDProducto=" & IDProducto
            sql = sql & " And IDDeposito=" & IDDeposito

            'Actualizar
            Dim Resultado As Integer = CSistema.ExecuteNonQuery(sql)

            If Resultado = 0 Then
                If MessageBox.Show("Desea continuar?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                    Exit Sub
                End If
            End If

        Next

        MessageBox.Show("Registros actualizados!", "Stock", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub frmControlExistencia_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me)
    End Sub

    Private Sub frmControlExistencia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        Actualizar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub ContextMenuStrip1_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip1.Opening

    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.dtToExcel(dgvLista.DataSource)
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmControlExistencia_Activate()
        Me.Refresh()
    End Sub
End Class
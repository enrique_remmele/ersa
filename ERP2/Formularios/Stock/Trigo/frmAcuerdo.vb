﻿Imports ERP.Reporte
Public Class frmAcuerdo
    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim dtSucursal As DataTable
    Dim vDecimalMoneda As Boolean
    Dim PrecioUnitario As Decimal
    Dim PrecioFlete As Decimal
    Dim IDMoneda As Integer
    Dim IDMonedaFlete As Integer
    Dim CReporte As New CReporteAcuerdo

    Dim vPrecioCambiado As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        CSistema.InicializaControles(Me)

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Botones
        'CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles, btnImprimir)
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        btnImprimir.Enabled = True

        'Focus
        dgvLista.Focus()
        txtFecha.Hoy()
        cbxMoneda.cbx.SelectedValue = 1
        cbxMonedaFlete.cbx.SelectedValue = 1
        'cbxMonedaContado.cbx.SelectedValue = 1
    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, New Button, New Button, vControles, btnImprimir)

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        'CSistema.CargaControl(vControles, txtID)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtNroAcuerdo)
        CSistema.CargaControl(vControles, txtZafra)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtPrecio)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, cbxTipoFlete)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, rdbActivo)
        CSistema.CargaControl(vControles, rdbDesactivado)
        CSistema.CargaControl(vControles, txtDesdeEmbarque)
        CSistema.CargaControl(vControles, txtHastaEmbarque)
        CSistema.CargaControl(vControles, cbxMonedaFlete)
        CSistema.CargaControl(vControles, txtCostoFlete)

        'JCA - 05/03/2024
        CSistema.CargaControl(vControles, txtphmin)
        CSistema.CargaControl(vControles, txtphmax)
        CSistema.CargaControl(vControles, txthmin)
        CSistema.CargaControl(vControles, txthmax)
        CSistema.CargaControl(vControles, txtgluten)
        CSistema.CargaControl(vControles, txtfn)
        CSistema.CargaControl(vControles, txtimpurezas)

        CSistema.CargaControl(vControles, cbxPrecioacordado)
        CSistema.CargaControl(vControles, cbxCondicionEntrega)
        CSistema.CargaControl(vControles, cbxFacturacion)
        CSistema.CargaControl(vControles, cbxBascula)
        CSistema.CargaControl(vControles, cbxtabladescuento)

        CSistema.CargaControl(vControles, txtproveedorint)
        CSistema.CargaControl(vControles, txtcompradorint)

        'FA - 25/05/2023
        CSistema.CargaControl(vControles, cbxMonedaCredito)
        CSistema.CargaControl(vControles, txtPrecioCredito)
        CSistema.CargaControl(vControles, txtCantidadCredito)

        'Moneda
        CSistema.SqlToComboBox(cbxMoneda, "Select ID, Referencia from Moneda Order By 1")

        CSistema.SqlToComboBox(cbxMonedaCredito, "Select ID, Referencia from Moneda Order By 1")
        'Moneda de Flete
        CSistema.SqlToComboBox(cbxMonedaFlete, "Select ID, Referencia from Moneda Order By 1")

        'Moneda
        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "
        txtProducto.Conectar()

        'Orden en forma
        cbxTipoFlete.cbx.Items.Add("INCLUIDO")
        cbxTipoFlete.cbx.Items.Add("PROPIO")
        cbxTipoFlete.cbx.Items.Add("TERCERIZADO")
        cbxTipoFlete.cbx.Text = ""
        'cbxTipoFlete.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'JCA - 05/03/2024
        'Precio acordado
        cbxPrecioacordado.cbx.Items.Add("Fijado")
        cbxPrecioacordado.cbx.Items.Add("Referencial")
        cbxPrecioacordado.cbx.Items.Add("Abierto")
        'cbxPrecioacordado.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Condicion de Entrega
        cbxCondicionEntrega.cbx.Items.Add("Puesto en Molino")
        cbxCondicionEntrega.cbx.Items.Add("A Retirar")
        'cbxCondicionEntrega.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Facturacion
        cbxFacturacion.cbx.Items.Add("Contado")
        cbxFacturacion.cbx.Items.Add("Crédito")
        cbxFacturacion.cbx.Items.Add("Otro")
        'cbxFacturacion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Bascula
        cbxBascula.cbx.Items.Add("ERSA")
        cbxBascula.cbx.Items.Add("PROVEEDOR")
        'cbxBascula.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tabla de Descuento
        cbxtabladescuento.cbx.Items.Add("Si")
        cbxtabladescuento.cbx.Items.Add("No")
        'cbxtabladescuento.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'cbxPrecioacordado.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'cbxCondicionEntrega.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'cbxFacturacion.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'cbxBascula.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'cbxtabladescuento.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Cargamos los registos en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgvLista.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If
        txtProducto.Conectar()
        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgvLista.SelectedRows(0).Cells(0).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VAcuerdo Where Numero=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("Numero").ToString
            txtFecha.SetValue(CDate(oRow("Fecha").ToString))
            txtNroAcuerdo.txt.Text = oRow("NroAcuerdo").ToString
            txtZafra.txt.Text = oRow("Zafra").ToString
            txtProveedor.SetValue(oRow("IDProveedor").ToString)
            txtIDProducto.txt.Text = oRow("IDProducto").ToString
            txtProducto.txt.Texto = oRow("Producto").ToString
            cbxMoneda.Texto = oRow("ReferenciaMoneda").ToString
            cbxMonedaFlete.Texto = oRow("ReferenciaMonedaFlete").ToString
            txtPrecio.txt.Text = oRow("Precio").ToString
            txtCantidad.txt.Text = oRow("Cantidad").ToString
            cbxTipoFlete.Texto = oRow("TipoFlete").ToString
            txtObservacion.txt.Text = oRow("Observacion").ToString
            txtDesdeEmbarque.SetValue(CDate(oRow("FechaDesdeEmbarque").ToString))
            txtHastaEmbarque.SetValue(CDate(oRow("FechaHastaEmbarque").ToString))
            lblUnidadMedida.Text = oRow("UnidadMedida").ToString
            txtCostoFlete.txt.Text = oRow("CostoFlete").ToString
            rdbActivo.Checked = Not oRow("Anulado")
            rdbDesactivado.Checked = oRow("Anulado")
            PrecioUnitario = oRow("Precio")
            IDMoneda = oRow("IDMoneda")
            PrecioFlete = oRow("CostoFlete")
            IDMonedaFlete = oRow("IDMonedaFlete")

            'JCA - 05/03/2024
            cbxPrecioacordado.Texto = oRow("Precioacordado").ToString
            cbxCondicionEntrega.Texto = oRow("CondicionEntrega").ToString
            cbxFacturacion.Texto = oRow("Facturacion").ToString
            cbxBascula.Texto = oRow("Bascula")
            cbxtabladescuento.Texto = oRow("tabladescuento").ToString

            txtphmin.txt.Text = oRow("phmin").ToString
            txtphmax.txt.Text = oRow("phmax").ToString
            txthmin.txt.Text = oRow("hmin").ToString
            txthmax.txt.Text = oRow("hmax").ToString
            txtgluten.txt.Text = oRow("gluten").ToString
            txtfn.txt.Text = oRow("fn").ToString
            txtimpurezas.txt.Text = oRow("impurezas").ToString

            txtproveedorint.txt.Text = oRow("Proveedorint").ToString
            txtcompradorint.txt.Text = oRow("Compradorint").ToString

            cbxMonedaCredito.Texto = oRow("ReferenciaMonedaCredito").ToString
            txtPrecioCredito.txt.Text = oRow("PrecioCredito").ToString
            txtCantidadCredito.txt.Text = oRow("CantidadCredito").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal Where As String = "")

        CSistema.SqlToDataGrid(dgvLista, "select Numero, cast(Fecha as date) as Fecha, cast(NroAcuerdo as int) as NroAcuerdo,ReferenciaMoneda as Moneda,Proveedor, concat(ReferenciaProducto,' - ',Producto) as Producto, Precio, Cantidad from vacuerdo " & Where & " Order By Fecha desc ")

        'Formato
        'dgvLista.Columns("IDProveedor").Visible = False
        dgvLista.Columns("Precio").DefaultCellStyle.Format = "N2"
        dgvLista.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgvLista.Columns("Precio").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLista.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight



        'txtCantidad.SetValue(dgvLista.RowCount)

    End Sub

    Sub InicializarControles()

        'TextBox
        CSistema.InicializaControles(vControles)

        'Funciones
        If vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero)+1, 1) From Acuerdo"), Integer)
        Else
            Listar()
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtNroAcuerdo.Focus()
        txtFecha.Hoy()
        txtNroAcuerdo.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(Convert(int,NroAcuerdo))+1, 1) From Acuerdo"), Integer)
        txtZafra.txt.Text = "2017"
        txtProveedor.Clear()
        txtProducto.txt.txt.Text = ""
        txtIDProducto.txt.Text = ""
        txtPrecio.txt.Text = "0"
        txtCantidad.txt.Text = "0"
        cbxTipoFlete.cbx.SelectedIndex = 0
        cbxMoneda.cbx.SelectedValue = 1
        cbxMonedaFlete.cbx.SelectedValue = 1
        txtObservacion.txt.Text = ""
        txtDesdeEmbarque.Hoy()
        txtHastaEmbarque.Hoy()

        cbxMonedaCredito.cbx.SelectedValue = 1
        txtPrecioCredito.txt.Text = "0"
        txtCantidadCredito.txt.Text = "0"
    End Sub
    Sub Nuevo()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
        cbxMoneda.Texto = "GS"
        cbxMonedaFlete.Texto = "GS"
        rdbActivo.Checked = True
        rdbDesactivado.Checked = False
    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()
        Dim vOperacion As String = Operacion.ToString


        'Validar
        If Operacion <> ERP.CSistema.NUMOperacionesABM.DEL Then

            'Sucursal
            If txtProveedor.Seleccionado = False And txtProveedor.txtReferencia.txt.Text = "" Then
                Dim mensaje As String = "Seleccione correctamente el proveedor!"
                ctrError.SetError(txtProveedor, mensaje)
                ctrError.SetIconAlignment(txtProveedor, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtProducto.Seleccionado = False And txtProducto.txt.txt.Text = "" Then
                Dim mensaje As String = "Seleccione correctamente el producto!"
                ctrError.SetError(txtProducto, mensaje)
                ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If IsNumeric(cbxMoneda.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente la Moneda!"
                ctrError.SetError(cbxMoneda, mensaje)
                ctrError.SetIconAlignment(cbxMoneda, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Referencia
            If txtNroAcuerdo.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Debe ingresar una Nro de Acuerdo valido!"
                ctrError.SetError(txtNroAcuerdo, mensaje)
                ctrError.SetIconAlignment(txtNroAcuerdo, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Escritura de la Descripcion
            If txtObservacion.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Debe ingresar una descripcion valida!"
                ctrError.SetError(txtObservacion, mensaje)
                ctrError.SetIconAlignment(txtObservacion, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If txtProveedor.Registro("Acopiador").ToString = False And txtPrecio.ObtenerValor = 0 Then
                If MessageBox.Show("Desea asignar Costo Promedio?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                    Dim vPrecio As Double = 0
                    If txtProducto.Registro("IDImpuesto").ToString = "1" Then
                        vPrecio = txtProducto.Registro("Costo").ToString * 1.1
                    ElseIf txtProducto.Registro("IDImpuesto").ToString = "2" Then
                        vPrecio = txtProducto.Registro("Costo").ToString * 1.05
                    ElseIf txtProducto.Registro("IDImpuesto").ToString = "3" Then
                        vPrecio = txtProducto.Registro("Costo").ToString
                    End If

                    If cbxMoneda.GetValue = 1 Then
                        txtPrecio.txt.Text = CSistema.FormatoMoneda(vPrecio, False)
                    Else
                        Dim Cotizacion As Integer = 0
                        Cotizacion = CType(CSistema.ExecuteScalar("Select Isnull(Cotizacion,1) From Cotizacion where IDMoneda = " & cbxMoneda.GetValue & " and cast(fecha as date) = '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "'"), Integer)
                        If Cotizacion > 1 Then
                            txtPrecio.txt.Text = CSistema.FormatoMoneda(vPrecio / Cotizacion, True)
                        End If
                    End If
                Else
                    Dim mensaje As String = "Debe ingresar un precio!"
                    ctrError.SetError(txtObservacion, mensaje)
                    ctrError.SetIconAlignment(txtObservacion, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                End If
            End If
            'Referencia
            If txtPrecio.ObtenerValor = 0 Then
                Dim mensaje As String = "El precio no puede ser Cero!"
                ctrError.SetError(txtPrecio, mensaje)
                ctrError.SetIconAlignment(txtPrecio, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Precio Acordado
            If cbxPrecioacordado.Texto = "" Then
                Dim mensaje As String = "Ingresar un Precio Acordado"
                ctrError.SetError(txtPrecio, mensaje)
                ctrError.SetIconAlignment(txtPrecio, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Condicion de Entrega
            If cbxCondicionEntrega.Texto = "" Then
                Dim mensaje As String = "Ingresar una Condicion de Entrega"
                ctrError.SetError(txtPrecio, mensaje)
                ctrError.SetIconAlignment(txtPrecio, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Facturacion
            If cbxFacturacion.Texto = "" Then
                Dim mensaje As String = "Ingresar una Opción de Facturacion"
                ctrError.SetError(txtPrecio, mensaje)
                ctrError.SetIconAlignment(txtPrecio, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            'Bascula
            If cbxBascula.Texto = "" Then
                Dim mensaje As String = "Ingresar una Opción de Bascula"
                ctrError.SetError(txtPrecio, mensaje)
                ctrError.SetIconAlignment(txtPrecio, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

        End If



        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Then
            If vPrecioCambiado Then
                If MessageBox.Show("Desea actualizar los precios a los tickets de este acuerdo?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then
                    vOperacion = "REASIGNAR"
                End If
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@Numero", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProducto", CSistema.FormatoMonedaBaseDatos(txtIDProducto.txt.Text, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroAcuerdo", txtNroAcuerdo.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaDesdeEmbarque", CSistema.FormatoFechaBaseDatos(txtDesdeEmbarque.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaHastaEmbarque", CSistema.FormatoFechaBaseDatos(txtHastaEmbarque.GetValue, True, False), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoMonedaBaseDatos(txtCantidad.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Precio", CSistema.FormatoMonedaBaseDatos(txtPrecio.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoFlete", cbxTipoFlete.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Zafra", CSistema.FormatoMonedaBaseDatos(txtZafra.txt.Text, False), ParameterDirection.Input)

        'FA - 25/05/2023 Cambio para intereses devengados
        CSistema.SetSQLParameter(param, "@CantidadCredito", CSistema.FormatoMonedaBaseDatos(txtCantidadCredito.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PrecioCredito", CSistema.FormatoMonedaBaseDatos(txtPrecioCredito.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMonedaCredito", cbxMonedaCredito.GetValue, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@IDMonedaFlete", cbxMonedaFlete.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CostoFlete", CSistema.FormatoMonedaBaseDatos(txtCostoFlete.txt.Text, True), ParameterDirection.Input)

        'JCA agregado el 06/03/2024
        CSistema.SetSQLParameter(param, "@Precioacordado", cbxPrecioacordado.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CondicionEntrega", cbxCondicionEntrega.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Facturacion", cbxFacturacion.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Bascula", cbxBascula.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@tabladescuento", cbxtabladescuento.Texto, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Phmin", txtphmin.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Phmax", txtphmax.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@hmin", txthmin.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@hmax", txthmax.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Gluten", txtgluten.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fn", txtfn.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Impurezas", txtimpurezas.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Proveedorint", txtproveedorint.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Compradorint", txtcompradorint.txt.Text, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Anulado", Not rdbActivo.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Finalizado", rdbDesactivado.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", vOperacion, ParameterDirection.Input)
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpAcuerdo", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            'Listar(" Where Numero=" & txtID.txt.Text)
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

        If vOperacion = "UPD" Then
            If vPrecioCambiado Then
                MessageBox.Show("Se actualizaron todos los datos a excepcion de los precios", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtNroAcuerdo.Focus()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        InicializarControles()
        ObtenerInformacion()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgvLista_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ObtenerInformacion()
    End Sub


    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmAcuerdo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub frmAcuerdo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub dgvLista_SelectionChanged_1(sender As System.Object, e As System.EventArgs) Handles dgvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(sender As System.Object, e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        txtIDProducto.txt.Text = txtProducto.Registro("ID").ToString
        lblUnidadMedida.Text = txtProducto.Registro("Uni. Med.")
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub Imprimir()
        Dim where As String = " where NroAcuerdo = " & txtNroAcuerdo.txt.Text
        Dim whereDetalle As String = "" ''"Lote " & txtComprobante.txt.Text & " - Sucursal " & cbxSucursal.cbx.Text & " - Fecha " & txtFecha.txt.Text & " - Vehiculo " & cbxVehiculo.cbx.Text & " - Chofer " & cbxChofer.cbx.Text
        Dim Titulo As String = "ACUERDO DE COMPRA MATERIA PRIMA"
        Dim Subtitulo As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.InformeAcuerdoProveedor(frm, where, Titulo, Subtitulo, vgUsuarioIdentificador, 0, Top, 0, txtFecha.txt.Text, 0, Subtitulo)
    End Sub

    Private Sub txtPrecio_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtPrecio.TeclaPrecionada

        If txtPrecio.txt.Text.ToString = PrecioUnitario.ToString Then
            vPrecioCambiado = False
        Else
            vPrecioCambiado = True
        End If

    End Sub

    Private Sub txtCostoFlete_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtCostoFlete.TeclaPrecionada
        If txtCostoFlete.txt.Text.ToString = PrecioFlete.ToString Then
            vPrecioCambiado = False
        Else
            vPrecioCambiado = True
        End If
    End Sub

    Private Sub cbxMonedaFlete_PropertyChanged(sender As Object, e As EventArgs) Handles cbxMonedaFlete.PropertyChanged
        If cbxMonedaFlete.cbx.SelectedValue = IDMonedaFlete Then
            vPrecioCambiado = False
        Else
            vPrecioCambiado = True
        End If
    End Sub

    Private Sub cbxMoneda_PropertyChanged(sender As Object, e As EventArgs) Handles cbxMoneda.PropertyChanged
        If cbxMoneda.cbx.SelectedValue = IDMoneda Then
            vPrecioCambiado = False
        Else
            vPrecioCambiado = True
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmAcuerdo_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Public Class frmAsignarAcuerdoTicket
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CAsiento As New CAsientoTicketBascula
    Dim CData As New CData
    Dim vDecimalOperacion As Boolean
    Dim vIDProveedor As Integer = 0
    Dim vIDProducto As Integer = 0
    Dim vIDTipoProducto As Integer = 0
    Dim vCostoFlete As Decimal = 0
    'Dim CReporte As New CRepo

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim vModificar As Boolean
    Dim dt As DataTable
    Dim dtGastoAdicional As DataTable
    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Conectar()
        txtProveedor.Conectar()
        txtProveedorGasto.Conectar()
        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
        'OcxCotizacion1.Inicializar()

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion("frmTicketBascula", "TICKET DE BASCULA", "TIBA")
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        '    CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        txtFechaTicket.Hoy()
        CargarOperacion()
        txtAcuerdo.Focus()
        BloquearCostos()
        cbxSucursal.Texto = "MOL"
    End Sub

    Sub CargarInformacion()
        ReDim vControles(-1)
        'CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, txtAcuerdo)
        'CSistema.CargaControl(vControles, txtFechaTicket)
        'CSistema.CargaControl(vControles, cbxTipoComprobante)
        'CSistema.CargaControl(vControles, txtComprobante)
        'CSistema.CargaControl(vControles, txtRemision)
        'CSistema.CargaControl(vControles, txtRecepcion)
        'CSistema.CargaControl(vControles, cbxTipoFlete)
        'CSistema.CargaControl(vControles, cbxDeposito)
        'CSistema.CargaControl(vControles, cbxCamion)
        'CSistema.CargaControl(vControles, cbxChapa)
        'CSistema.CargaControl(vControles, cbxChofer)
        'CSistema.CargaControl(vControles, txtObservacion)
        'CSistema.CargaControl(vControles, txtProducto)
        'CSistema.CargaControl(vControles, txtPesoRemision)
        'CSistema.CargaControl(vControles, txtCantidadBascula)
        'CSistema.CargaControl(vControles, txtCostoUnitario)
        'CSistema.CargaControl(vControles, txtCostoUnitarioUs)
        'CSistema.CargaControl(vControles, cbxUnidad)
        CSistema.CargaControl(vControles, cbxGastoAdicional)
        CSistema.CargaControl(vControles, cbxMonedaGasto)
        CSistema.CargaControl(vControles, txtCotizacionGastoAdicional)
        CSistema.CargaControl(vControles, txtImporteGasto)
        CSistema.CargaControl(vControles, cbxImpuesto)
        CSistema.CargaControl(vControles, txtProveedorGasto)

        'CARGAR ESTRUCTURA DEL GASTO ADICIONAL
        dtGastoAdicional = CSistema.ExecuteToDataTable("Select Top(0) * From VTicketBasculaGastoAdicional").Clone

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()


        'CARGAR UNIDAD
        'Sucursales
        'cbxUnidad.Texto = "TONELADAS"

        'Orden en forma
        cbxTipoFlete.cbx.Items.Add("INCLUIDO")
        cbxTipoFlete.cbx.Items.Add("TERCERIZADO")
        cbxTipoFlete.cbx.Items.Add("PROPIO")
        cbxTipoFlete.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Sucursales
        CSistema.SqlToComboBox(cbxDeposito.cbx, "Select ID, Deposito From VDeposito Where IDTipoDeposito = 6 Order By 2") ' Tipo de Deposito Silo

        'Moneda
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Descripcion From vMoneda Order By 1")

        'Moneda gasto adicional
        CSistema.SqlToComboBox(cbxMonedaGasto.cbx, "Select ID, Descripcion From vMoneda Order By 1")
        'Impuesto
        CSistema.SqlToComboBox(cbxImpuesto.cbx, "Select ID, Descripcion From vImpuesto Order By 1")

        'Gasto Adicional
        CSistema.SqlToComboBox(cbxGastoAdicional.cbx, "Select ID, Descripcion From GastoAdicional Order By 2")

        'Chofer
        CSistema.SqlToComboBox(cbxChofer.cbx, "Select ID, Nombres From ChoferProveedor Order By 2")
        'Chapa
        CSistema.SqlToComboBox(cbxChapa.cbx, "Select ID, Patente From CamionProveedor Order By 2")
        'camion
        CSistema.SqlToComboBox(cbxCamion.cbx, "Select ID, Descripcion From CamionProveedor Order By 2")

        'Tipo de Operaciones
        'CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion=" & IDOperacion)

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo Operacion
        cbxSucursal.SelectedValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "0"))

        'Tipo Operacion
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO OPERACION", "")

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Deposito de Origen
        cbxDeposito.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO", "")

        'Unidad
        cbxUnidad.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", "UNIDAD")

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From TicketBascula),1)"), Integer)

    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.GetValue)

        'Tipo Operacion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO OPERACION", cbxSucursal.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Deposito de Origen
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO", cbxDeposito.cbx.Text)

        'Unidad
        CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", cbxUnidad.Text)

    End Sub

    Sub Modificar()

        vModificar = True
        'CARGAR ESTRUCTURA DEL DETALLE
        'dt = CSistema.ExecuteToDataTable("Select Top(0) * From vTicketBascula").Clone
        dtGastoAdicional = CSistema.ExecuteToDataTable("Select Top(0) * From VTicketBasculaGastoAdicional").Clone
        'Configurar botones
        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)

        dtGastoAdicional.Clear()
        'LimpiarGastoAdicional()

        'dt.Rows.Clear()
        dtGastoAdicional.Rows.Clear()
        ListarDetalle()
        ListarGastoAdicional()
        'CDetalleImpuesto.EstablecerImpuestosDetalle(dt)

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        txtAcuerdo.txt.Clear()
        txtAcuerdo.Enabled = True
        txtAcuerdo.SoloLectura = False
        txtTotalGastoGS.txt.Text = "0"
        txtTotalGastoUS.txt.Text = "0"

        'ObtenerCotizacion()
        'CAsiento.Limpiar()
        'CAsiento.Inicializar()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False
        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en la fecha
        txtAcuerdo.txt.Focus()
        txtFechaTicket.Hoy()
        'ObtenerCotizacion()
    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VTicketBascula ),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()
    End Sub
    Sub ObtenerCotizacion()
        If vNuevo = True Then
            Dim vFecha As String = ""
            vFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
            If vFecha.Length = 8 Then
                txtCotizacionUs.txt.Text = CType(CSistema.ExecuteScalar("select Isnull((Select cotizacion from cotizacion where IDMoneda = 2 and cast(fecha as date) = '" & vFecha & "'),1)"), Integer)
            End If
            If txtCotizacionUs.ObtenerValor = 1 Then
                MessageBox.Show("Cargar cotizacion del dia!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Cancelar()
            End If
        End If
    End Sub
    Sub ObtenerCotizacionGasto()
        If vNuevo = True Then
            Dim vFecha As String = ""
            vFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
            If vFecha.Length = 8 Then
                txtCotizacionGastoAdicional.txt.Text = CType(CSistema.ExecuteScalar("select Isnull((Select cotizacion from cotizacion where IDMoneda = " & cbxMonedaGasto.GetValue & " and cast(fecha as date) = '" & vFecha & "'),1)"), Integer)
            End If
        End If
    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        Dim dt As DataTable = Nothing
        dt = CSistema.ExecuteToDataTable("Select * From vAcuerdo Where NroAcuerdo = '" & txtAcuerdo.txt.Text & "' and IDProducto = " & vIDProducto & " and IDProveedor = " & vIDProveedor)

        'Cargamos la cabecera
        If dt Is Nothing Or dt.Rows.Count = 0 Then
            Dim mensaje As String = "Verificar datos del acuerdo."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtAcuerdo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            dt.Clear()
            Exit Function
        End If
        dt.Clear()

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ''Validar el Asiento
        'If CAsiento.ObtenerSaldo <> 0 Then
        '    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        'If CAsiento.ObtenerTotal = 0 Then
        '    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        Return True

    End Function
    Sub MostrarDecimales(ByVal IDMoneda As Integer)
        txtCostoUnitario.Decimales = True
        txtCostoUnitarioUs.Decimales = True
        txtCantidadBascula.Decimales = True
        txtPesoRemision.Decimales = True
    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()
        If CSistema.dtSumColumn(dt, "PrecioUnitario") = 0 Then
            CargarAcuerdo()
        End If
        ' GenerarAsiento()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If


        Dim param(-1) As SqlClient.SqlParameter
        'Dim IDTransaccion As Integer

        'Se inserta primero el Gasto Adicional para el calculo del Costo al guardar al ticket
        If IDTransaccion > 0 Then
            If InsertarGastoAdicional(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then
                'Exit Sub
            End If
        End If

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCotizacionUs.txt.Text, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProducto", CSistema.dtSumColumn(dt, "IDProducto"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDImpuesto", CSistema.dtSumColumn(dt, "IDImpuesto"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PesoRemision", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "PesoRemision"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PesoBascula", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "PesoBascula"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Diferencia", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "Diferencia"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PrecioUnitario", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "PrecioUnitario"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PrecioUnitarioUS", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "PrecioUnitarioUS"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "Total"), False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalUS", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalUS"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalImpuesto"), False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuestoUS", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalImpuestoUS"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalDiscriminado"), False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminadoUS", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalDiscriminadoUS"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CostoAdicional", CSistema.FormatoMonedaBaseDatos(CSistema.FormatoMonedaBaseDatos(txtTotalGastoGS.txt.Text, False)), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CostoAdicionalUS", CSistema.FormatoMonedaBaseDatos(CSistema.FormatoMonedaBaseDatos(txtTotalGastoUS.txt.Text, False)), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoFlete", cbxTipoFlete.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDChofer", cbxChofer.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCamion", cbxCamion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroRemision", txtRemision.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroRecepcion", txtRecepcion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroAcuerdo", txtAcuerdo.txt.Text, ParameterDirection.Input)
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            CSistema.SetSQLParameter(param, "@Operacion", "ASIGNAR", ParameterDirection.Input)
        End If
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.GetValue, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "spTicketBascula", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro
            Exit Sub

        End If

        ''Se inserta primero el Gasto Adicional para el calculo del Costo al guardar al ticket
        'If IDTransaccion > 0 Then
        '    If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
        '        CAsiento.IDTransaccion = IDTransaccion
        '        CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
        '    End If

        'End If

        'Actualizar el Ultimo Costo
        ActualizarCosto()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Function InsertarGastoAdicional(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarGastoAdicional = True
        Dim ID As Integer = 1

        For Each oRow As DataRow In dtGastoAdicional.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDGastoAdicional", oRow("IDGastoAdicional").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDMoneda", cbxMonedaGasto.GetValue, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ImporteGS", CSistema.FormatoMonedaBaseDatos(oRow("ImporteGS").ToString, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ImporteUS", CSistema.FormatoMonedaBaseDatos(oRow("ImporteUS").ToString, True), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion").ToString, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CotizacionUS", CSistema.FormatoMonedaBaseDatos(oRow("CotizacionUS").ToString, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProveedor", oRow("IDProveedor").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpTicketBasculaGastoAdicional", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If
            ID = ID + 1
        Next

    End Function

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnModificar)

    End Sub

    Sub ActualizarCosto()

        For Each oRow As DataRow In dt.Rows
            For Each ProductoRow As DataRow In vgData.Tables("VProducto").Select(" ID = " & vIDProducto)
                ProductoRow("UltimoCosto") = oRow("PrecioUnitario")
            Next
        Next

    End Sub

    Sub CalcularGastoAdicional()
        If txtImporteGasto.txt.Text <> "" Then
            If cbxMonedaGasto.GetValue = 1 Then
                txtImporteGastoGS.txt.Text = txtImporteGasto.txt.Text 'GS
                txtImporteGastoUS.txt.Text = CSistema.FormatoNumero(CSistema.FormatoMonedaBaseDatos(txtImporteGasto.txt.Text, False) / txtCotizacionUs.ObtenerValor, True)
            End If
                If cbxMonedaGasto.GetValue = 2 Then
                    txtImporteGastoGS.txt.Text = txtImporteGasto.txt.Text * txtCotizacionUs.ObtenerValor
                    txtImporteGastoUS.txt.Text = CSistema.FormatoNumero((txtImporteGasto.txt.Text * txtCotizacionGastoAdicional.ObtenerValor) / txtCotizacionUs.ObtenerValor, True)
                End If
                If cbxMonedaGasto.GetValue <> 1 And cbxMonedaGasto.GetValue <> 2 Then 'No es GS ni US
                    txtImporteGastoGS.txt.Text = txtImporteGasto.txt.Text * txtCotizacionGastoAdicional.ObtenerValor
                    txtImporteGastoUS.txt.Text = CSistema.FormatoNumero((txtImporteGasto.txt.Text * txtCotizacionGastoAdicional.ObtenerValor) / txtCotizacionUs.ObtenerValor, True)
                End If
            End If
    End Sub

    Sub CargarProducto()

        'Validar


        If txtCostoUnitario.ObtenerValor = 0 Then
            Dim mensaje As String = "El Costo debe ser mayor a cero!"
            ctrError.SetError(cbxSucursal, mensaje)
            ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        ' Insertar
        'Cargamos el registro en el detalle
        'Dim dRow As DataRow = dt.NewRow()

        If dt.Select(" IDProducto=" & vIDProducto).Count > 0 Then
            Dim Rows() As DataRow = dt.Select(" IDProducto=" & vIDProducto)
            'Totales
            Rows(0)("PrecioUnitario") = txtCostoUnitario.ObtenerValor
            Rows(0)("PrecioUnitarioUS") = txtCostoUnitarioUs.ObtenerValor
            Rows(0)("Total") = Rows(0)("PesoBascula") * CDec(txtCostoUnitario.ObtenerValor)
            Rows(0)("TotalUS") = Rows(0)("PesoBascula") * CDec(txtCostoUnitarioUs.ObtenerValor)

            Rows(0)("TotalDiscriminado") = 0
            Rows(0)("TotalDiscriminadoUS") = 0
            Rows(0)("TotalImpuesto") = 0
            Rows(0)("TotalImpuestoUS") = 0


            txtImporteGasto.txt.Text = Rows(0)("PesoBascula") * vCostoFlete
            If txtImporteGasto.ObtenerValor > 0 Then
                CalcularGastoAdicional()
            End If
            
            CSistema.CalcularIVA(Rows(0)("IDImpuesto"), CDec(Rows(0)("Total").ToString), False, True, Rows(0)("TotalDiscriminado"), Rows(0)("TotalImpuesto"))
            CSistema.CalcularIVA(Rows(0)("IDImpuesto"), CDec(Rows(0)("TotalUS").ToString), True, False, Rows(0)("TotalDiscriminadoUS"), Rows(0)("TotalImpuestoUS"))
        End If

        'Agregamos al detalle
        'dt.Rows.Add(dRow)
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dt, False)

        'Inicializamos los valores
        txtCantidadBascula.txt.Text = "0"
        txtPesoRemision.txt.Text = "0"
        txtCostoUnitario.txt.Text = "0"
        txtCostoUnitarioUs.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub
    Sub CargarGastoAdicional()

        'Validar
        'Operacion
        If cbxGastoAdicional.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente el Gasto!"
            ctrError.SetError(cbxGastoAdicional, mensaje)
            ctrError.SetIconAlignment(cbxGastoAdicional, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxMonedaGasto.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente la moneda de gasto!"
            ctrError.SetError(cbxMonedaGasto, mensaje)
            ctrError.SetIconAlignment(cbxMonedaGasto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If txtProveedorGasto.txtRazonSocial.txt.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el Proveedor del Gasto!"
            ctrError.SetError(txtProveedorGasto, mensaje)
            ctrError.SetIconAlignment(txtProveedorGasto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtImporteGasto.ObtenerValor) = False Then
            Dim mensaje As String = "El Importe del gasto no es correcto!"
            ctrError.SetError(txtImporteGasto, mensaje)
            ctrError.SetIconAlignment(txtImporteGasto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtImporteGastoGS.ObtenerValor) = False Then
            Dim mensaje As String = "El Importe del gasto GS no es correcto!"
            ctrError.SetError(txtImporteGasto, mensaje)
            ctrError.SetIconAlignment(txtImporteGasto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        ' Insertar
        'Cargamos el registro en el Gasto adicional
        Dim dRow As DataRow = dtGastoAdicional.NewRow()

        'Obtener Valores
        dRow("IDGastoAdicional") = cbxGastoAdicional.GetValue
        dRow("GastoAdicional") = cbxGastoAdicional.Texto
        dRow("ID") = dt.Rows.Count
        dRow("IDMoneda") = cbxMonedaGasto.GetValue
        dRow("ReferenciaMoneda") = cbxMonedaGasto.Texto
        'Impuestos
        dRow("Importe") = txtImporteGasto.ObtenerValor
        dRow("ImporteGS") = txtImporteGastoGS.ObtenerValor
        dRow("ImporteUS") = txtImporteGastoUS.ObtenerValor

        dRow("Cotizacion") = txtCotizacionGastoAdicional.ObtenerValor
        dRow("CotizacionUS") = txtCotizacionUs.ObtenerValor
        dRow("IDImpuesto") = cbxImpuesto.GetValue
        dRow("IDProveedor") = txtProveedorGasto.Registro("ID").ToString
        dRow("Proveedor") = txtProveedorGasto.Registro("RazonSocial").ToString


        'Agregamos al detalle
        dtGastoAdicional.Rows.Add(dRow)
        ListarGastoAdicional()

        'Inicializamos los valores
        txtImporteGasto.txt.Text = "0"
        txtImporteGastoGS.txt.Text = "0"
        txtImporteGastoUS.txt.Text = "0"

        'Retorno de Foco
        cbxGastoAdicional.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dt.Rows(dgw.SelectedRows(0).Index).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dt.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dt, False)
        CargarAcuerdo()
    End Sub
    Sub EliminarGastoAdicional()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgwGastoAdicional.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgwGastoAdicional, mensaje)
            ctrError.SetIconAlignment(dgwGastoAdicional, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtGastoAdicional.Rows(dgwGastoAdicional.SelectedRows(0).Index).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtGastoAdicional.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarGastoAdicional()
    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro


        CSistema.dtToGrid(dgw, dt)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("ReferenciaProducto").DisplayIndex = 1
        dgw.Columns("Producto").DisplayIndex = 2
        dgw.Columns("PesoRemision").DisplayIndex = 3
        dgw.Columns("PesoBascula").DisplayIndex = 4
        dgw.Columns("Diferencia").DisplayIndex = 5
        dgw.Columns("PrecioUnitario").DisplayIndex = 6
        dgw.Columns("PrecioUnitarioUS").DisplayIndex = 7
        dgw.Columns("Total").DisplayIndex = 8
        dgw.Columns("TotalUS").DisplayIndex = 9

        dgw.Columns("ReferenciaProducto").Visible = True
        dgw.Columns("Producto").Visible = True
        dgw.Columns("PesoRemision").Visible = True
        dgw.Columns("PrecioUnitario").Visible = vgVerCosto
        dgw.Columns("Total").Visible = vgVerCosto
        dgw.Columns("PrecioUnitarioUS").Visible = vgVerCosto
        dgw.Columns("TotalUS").Visible = vgVerCosto
        dgw.Columns("PesoBascula").Visible = True
        dgw.Columns("Diferencia").Visible = True

        dgw.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("PesoRemision").DefaultCellStyle.Format = "N2"
        dgw.Columns("PesoBascula").DefaultCellStyle.Format = "N2"
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"
        dgw.Columns("PrecioUnitarioUS").DefaultCellStyle.Format = "N3"
        dgw.Columns("TotalUS").DefaultCellStyle.Format = "N3"
        dgw.Columns("Diferencia").DefaultCellStyle.Format = "N2"

        dgw.Columns("ReferenciaProducto").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("PesoRemision").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PesoBascula").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Diferencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitarioUS").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("TotalUS").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("PrecioUnitario").HeaderText = "Costo"
        dgw.Columns("PrecioUnitarioUS").HeaderText = "Costo US"
        dgw.Columns("PesoRemision").HeaderText = "Remision"
        dgw.Columns("PesoBascula").HeaderText = "Bascula"
        dgw.Columns("Total").HeaderText = "Total"
        dgw.Columns("TotalUS").HeaderText = "Total US"
        dgw.Columns("ReferenciaProducto").HeaderText = "Producto"
        dgw.Columns("Producto").HeaderText = "Descripcion"


        Total = CSistema.dtSumColumn(dt, "TotalGs")


        'Bloqueamos la cabecera si corresponde
        If dt.Rows.Count > 0 And vNuevo = True Then
            cbxSucursal.cbx.Enabled = False
            cbxDeposito.cbx.Enabled = False
            txtFechaTicket.Enabled = False
            txtCantidadBascula.Enabled = False
            txtPesoRemision.Enabled = False
        Else
            cbxSucursal.cbx.Enabled = True
            cbxSucursal.cbx.SelectedValue = cbxSucursal.GetValue
            cbxDeposito.cbx.Enabled = True
            txtFechaTicket.Enabled = True
            txtCantidadBascula.Enabled = True
            txtPesoRemision.Enabled = True
        End If

        'Calculamos el Total
        'txtTotal.txt.Text = Total


    End Sub
    Sub ListarGastoAdicional()
        'Variables
        Dim TotalGS As Decimal = 0
        Dim TotalUS As Decimal = 0
        'Cargamos registro por registro
        CSistema.dtToGrid(dgwGastoAdicional, dtGastoAdicional)

        For i As Integer = 0 To dgwGastoAdicional.Columns.Count - 1
            dgwGastoAdicional.Columns(i).Visible = False
        Next

        dgwGastoAdicional.Columns("GastoAdicional").DisplayIndex = 1
        dgwGastoAdicional.Columns("Proveedor").DisplayIndex = 2
        dgwGastoAdicional.Columns("ReferenciaMoneda").DisplayIndex = 3
        dgwGastoAdicional.Columns("Cotizacion").DisplayIndex = 4
        dgwGastoAdicional.Columns("Importe").DisplayIndex = 5
        dgwGastoAdicional.Columns("ImporteGs").DisplayIndex = 6
        dgwGastoAdicional.Columns("ImporteUS").DisplayIndex = 7

        dgwGastoAdicional.Columns("GastoAdicional").Visible = True
        dgwGastoAdicional.Columns("Proveedor").Visible = True
        dgwGastoAdicional.Columns("ReferenciaMoneda").Visible = True
        dgwGastoAdicional.Columns("Cotizacion").Visible = True
        dgwGastoAdicional.Columns("Importe").Visible = True
        dgwGastoAdicional.Columns("ImporteGs").Visible = True
        dgwGastoAdicional.Columns("ImporteUS").Visible = True

        dgwGastoAdicional.Columns("GastoAdicional").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        'dgwGastoAdicional.Columns("ReferenciaMoneda").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgwGastoAdicional.Columns("Cotizacion").DefaultCellStyle.Format = "N0"
        dgwGastoAdicional.Columns("CotizacionUS").DefaultCellStyle.Format = "N0"

        dgwGastoAdicional.Columns("Importe").DefaultCellStyle.Format = "N3"
        dgwGastoAdicional.Columns("ImporteGS").DefaultCellStyle.Format = "N0"
        dgwGastoAdicional.Columns("ImporteUS").DefaultCellStyle.Format = "N3"

        dgwGastoAdicional.Columns("GastoAdicional").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwGastoAdicional.Columns("ReferenciaMoneda").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgwGastoAdicional.Columns("Cotizacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgwGastoAdicional.Columns("Importe").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgwGastoAdicional.Columns("ImporteUS").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgwGastoAdicional.Columns("ImporteGS").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


        dgwGastoAdicional.Columns("GastoAdicional").HeaderText = "Gasto"
        dgwGastoAdicional.Columns("ReferenciaMoneda").HeaderText = "Moneda"
        dgwGastoAdicional.Columns("ImporteGS").HeaderText = "Importe GS"
        dgwGastoAdicional.Columns("ImporteUS").HeaderText = "Importe US"

        TotalGS = CSistema.dtSumColumn(dtGastoAdicional, "ImporteGS")
        TotalUS = CSistema.dtSumColumn(dtGastoAdicional, "ImporteUS")
        txtTotalGastoGS.txt.Text = TotalGS
        txtTotalGastoUS.txt.Text = TotalUS

    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "DESASOCIAR", ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpTicketBascula", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        CargarOperacion()
    End Sub

    Sub Imprimir()

        Dim CTicketBascula As New Reporte.CReporteTicketBascula
        CTicketBascula.TicketBascula(IDTransaccion)

    End Sub

    Sub Buscar()

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        ''Otros
        'lblAnulado.Visible = False

        'Dim frm As New frmConsultaMovimiento
        'frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        'frm.WindowState = FormWindowState.Normal
        'frm.StartPosition = FormStartPosition.CenterScreen

        'FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        'If frm.IDTransaccion > 0 Then
        '    CargarOperacion(frm.IDTransaccion)
        'End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VTicketBascula where Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'dt.Clear()

        dt = CSistema.ExecuteToDataTable("Select * From vTicketBascula Where IDTransaccion=" & IDTransaccion)
        dtGastoAdicional = CSistema.ExecuteToDataTable("Select * From VTicketBasculaGastoAdicional Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)


        txtID.txt.Text = oRow("Numero").ToString
        txtFechaTicket.SetValueFromString(CDate(oRow("Fecha").ToString))
        'cbxSucursal.Texto = oRow("ReferenciaSucursal").ToString
        cbxTipoComprobante.Texto = oRow("ReferenciaTipoComprobante").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxDeposito.cbx.Text = oRow("Deposito").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        'txtTotal.txt.Text = oRow("Total").ToString
        vIDProveedor = oRow("IDProveedor").ToString
        vIDProducto = oRow("IDProducto").ToString
        vIDTipoProducto = oRow("IDTipoProducto").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        txtAcuerdo.txt.Text = oRow("NroAcuerdo").ToString

        cbxTipoFlete.Texto = oRow("TipoFlete").ToString
        cbxChofer.Texto = oRow("Chofer").ToString
        cbxChapa.Texto = oRow("Chapa").ToString
        cbxCamion.Texto = oRow("Camion").ToString
        txtRemision.txt.Text = oRow("NroRemision").ToString
        txtRecepcion.txt.Text = oRow("NroRecepcion").ToString

        'Moneda
        'OcxCotizacion1.cbxMoneda.txt.Text = oRow("ReferenciaMoneda")
        'OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")
        'OcxCotizacion1.SetValue(oRow("ReferenciaMoneda").ToString, oRow("Cotizacion").ToString)
        'OcxCotizacion1.cbxMoneda.txt.Text = oRow("ReferenciaMoneda")
        'OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")
        cbxMoneda.Texto = oRow("Moneda").ToString
        txtCotizacionUs.txt.Text = oRow("Cotizacion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dt, False)

        If dtGastoAdicional Is Nothing = False Then
            ListarGastoAdicional()
        End If

        'Inicializamos el Asiento
        '   CAsiento.Limpiar()
        txtAcuerdo.Enabled = False
        txtAcuerdo.SoloLectura = True
    End Sub
    Sub CargarAcuerdo()
        ctrError.Clear()
        tsslEstado.Text = ""
        Dim dtAcuerdo As DataTable

        If txtAcuerdo.txt.Text = "" Then
            Dim mensaje As String = "Ingrese Nro de Comprobante del Acuerdo!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        dtAcuerdo = CSistema.ExecuteToDataTable("Select * From vAcuerdo Where NroAcuerdo = '" & txtAcuerdo.txt.Text & "' and IDProducto = " & vIDProducto & " and IDProveedor = " & vIDProveedor)

        'Cargamos la cabecera
        If dtAcuerdo Is Nothing Or dtAcuerdo.Rows.Count = 0 Then
            Dim mensaje As String = "Verificar datos del acuerdo."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtAcuerdo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dtAcuerdo.Rows(0)
        cbxTipoFlete.Texto = oRow("TipoFlete").ToString
        'Moneda
        cbxMoneda.Texto = oRow("Moneda").ToString
        If oRow("IDMoneda").ToString = 1 Then
            txtCostoUnitario.txt.Text = oRow("Precio").ToString
            txtCostoUnitarioUs.txt.Text = CSistema.FormatoMoneda3Decimales((oRow("Precio").ToString / CSistema.FormatoMonedaBaseDatos(txtCotizacionUs.txt.Text, False)), True)
        End If
        If oRow("IDMoneda").ToString <> 1 Then
            txtCostoUnitarioUs.txt.Text = CSistema.FormatoMoneda3Decimales(oRow("Precio").ToString, True)
            txtCostoUnitario.txt.Text = CSistema.FormatoMoneda3Decimales((oRow("Precio").ToString * CSistema.FormatoMonedaBaseDatos(txtCotizacionUs.txt.Text, False)), True)
        End If

        If oRow("TipoFlete").ToString = "INCLUIDO" Then
            dgwGastoAdicional.Enabled = False
            cbxGastoAdicional.SoloLectura = True
            cbxMonedaGasto.SoloLectura = True
            txtImporteGasto.SoloLectura = True
            txtCotizacionGastoAdicional.SoloLectura = True
            cbxImpuesto.SoloLectura = True
            txtProveedorGasto.SoloLectura = True
            txtProveedorGasto.Enabled = False
        Else
            dgwGastoAdicional.Enabled = True
            cbxGastoAdicional.SoloLectura = False
            cbxMonedaGasto.SoloLectura = False
            txtImporteGasto.SoloLectura = False
            txtCotizacionGastoAdicional.SoloLectura = False
            cbxImpuesto.SoloLectura = False
            txtProveedorGasto.SoloLectura = False
            txtProveedorGasto.Enabled = True
        End If
        vCostoFlete = oRow("CostoFlete").ToString
        cbxMonedaGasto.Texto = oRow("MonedaFlete").ToString

        'Cargamos el detalle
        'ListarDetalle()
        'CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos el Asiento
        'CAsiento.Limpiar()
        dtAcuerdo.Clear()
        CargarProducto()
    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        '  If vNuevo = False And vModificar = False Then

        Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vTicketBascula Where Numero=" & txtID.ObtenerValor & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        'Else

        '    If cbxSucursal.cbx.SelectedValue Is Nothing Then
        '        Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
        '        ctrError.SetError(cbxSucursal, mensaje)
        '        ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
        '        tsslEstado.Text = mensaje
        '        Exit Sub
        '    End If

        '    Dim frm As New frmAsiento
        '    frm.WindowState = FormWindowState.Normal
        '    frm.StartPosition = FormStartPosition.CenterScreen
        '    frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
        '    frm.Text = Comprobante

        '    GenerarAsiento()

        '    frm.CAsiento.dtAsiento = CAsiento.dtAsiento
        '    CAsiento.ListarDetalle(frm.dgv)
        '    frm.CalcularTotales()

        '    frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

        '    'Mostramos
        '    frm.ShowDialog(Me)

        '    'Actualizamos el asiento si es que este tuvo alguna modificacion
        '    CAsiento.dtAsiento = frm.CAsiento.dtAsiento
        '    CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
        '    CAsiento.Bloquear = frm.CAsiento.Bloquear
        '    CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
        '    CAsiento.NroCaja = frm.CAsiento.NroCaja
        '    CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

        '    If frm.VolverAGenerar = True Then
        '        CAsiento.Generado = False
        '        CAsiento.dtAsiento.Clear()
        '        CAsiento.dtDetalleAsiento.Clear()
        '        VisualizarAsiento()
        '    End If

        'End If

    End Sub

    Sub GenerarAsiento()

        'EstablecerCabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = CData.GetRow("ID=" & cbxSucursal.GetValue, "VSucursal")("IDCiudad")
        oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
        oRow("Fecha") = txtFechaTicket.GetValue

        'Dim CuentaRow As DataRow = CData.GetRow("ID=" & cbxCuenta.GetValue, dtCuentaBancaria)

        oRow("IDMoneda") = cbxMoneda.GetValue
        If cbxMoneda.GetValue = 1 Then
            oRow("Cotizacion") = 1
        Else
            oRow("Cotizacion") = txtCotizacionUs.ObtenerValor
        End If
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtProveedor.Registro("RazonSocial").ToString & "-" & cbxTipoComprobante.cbx.Text & "-" & txtComprobante.txt.Text & " --- " & txtObservacion.txt.Text
        If cbxMoneda.GetValue = 1 Then
            oRow("Total") = CSistema.dtSumColumn(dt, "Total")
        Else
            oRow("Total") = CSistema.dtSumColumn(dt, "TotalUS")
        End If
        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        CAsiento.IDTipoProducto = vIDTipoProducto
        If cbxMoneda.GetValue = 1 Then
            CAsiento.Total = CSistema.dtSumColumn(dt, "Total")
            CAsiento.TotalGastoAdicional = txtTotalGastoGS.ObtenerValor
        Else
            CAsiento.Total = CSistema.dtSumColumn(dt, "TotalUS")
            CAsiento.TotalGastoAdicional = txtTotalGastoUS.ObtenerValor
        End If
        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.Generar()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VTicketBascula Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VTicketBascula Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            'Nuevo()
        End If
    End Sub

    Sub BloquearCostos()
        txtCostoUnitario.Visible = vgVerCosto
        txtCostoUnitarioUs.Visible = vgVerCosto
        Label7.Visible = vgVerCosto
        Label9.Visible = vgVerCosto
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click, Button3.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click, Button5.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click, Button8.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click, Button7.Click
        Imprimir()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click, Button4.Click
        Anular()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click, Button6.Click
        Me.Close()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click, Button1.Click
        VisualizarAsiento()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub



    'Private Sub OcxCotizacion1_CambioMoneda()
    '    OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
    '    OcxCotizacion1.Recargar()
    '    MostrarDecimales(OcxCotizacion1.Registro("ID"))
    '    vDecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & OcxCotizacion1.Registro("ID"), "vMoneda")("Decimales").ToString)
    '    txtProducto.IDMoneda = OcxCotizacion1.Registro("ID")
    '    txtProducto.Cotizacion = OcxCotizacion1.Registro("Cotizacion")
    'End Sub

    Private Sub txtFechaTicket_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtFechaTicket.TeclaPrecionada
        'OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
        'OcxCotizacion1.Recargar()
        ObtenerCotizacion()
    End Sub


    Private Sub frmTicketBascula_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmTicketBascula_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub txtCostoUnitario_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCostoUnitario.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub txtCostoUnitarioUs_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCostoUnitarioUs.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub txtCantidadBascula_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCantidadBascula.TeclaPrecionada
        If txtCostoUnitario.txt.Text <> "" Then
            If e.KeyCode = Keys.Enter Then
                If txtCostoUnitario.SoloLectura = True Then
                    CargarProducto()
                End If
            End If
        End If
    End Sub

    Private Sub frmTicketBascula_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(sender As System.Object, e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            cbxUnidad.Focus()
        End If
    End Sub

    Private Sub txtAcuerdo_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcuerdo.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarAcuerdo()
        End If
    End Sub

    Private Sub txtFechaTicket_Validated(sender As System.Object, e As System.EventArgs) Handles txtFechaTicket.Validated
        ObtenerCotizacion()
    End Sub

    Private Sub cbxMonedaGasto_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxMonedaGasto.TeclaPrecionada
        ObtenerCotizacionGasto()
    End Sub

    Private Sub cbxMonedaGasto_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxMonedaGasto.PropertyChanged
        ObtenerCotizacionGasto()
    End Sub

    Private Sub txtImporteGasto_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtImporteGasto.TeclaPrecionada
        If txtImporteGasto.txt.Text <> "" Then
            If e.KeyCode = Keys.Enter Then
                CargarGastoAdicional()
            Else
                If cbxMonedaGasto.GetValue = 1 Then
                    txtImporteGastoGS.txt.Text = txtImporteGasto.txt.Text 'GS
                    txtImporteGastoUS.txt.Text = CSistema.FormatoNumero(CSistema.FormatoMonedaBaseDatos(txtImporteGasto.txt.Text, False) / txtCotizacionUs.ObtenerValor, True)
                End If
                If cbxMonedaGasto.GetValue = 2 Then
                    txtImporteGastoGS.txt.Text = txtImporteGasto.txt.Text * txtCotizacionUs.ObtenerValor
                    txtImporteGastoUS.txt.Text = CSistema.FormatoNumero((txtImporteGasto.txt.Text * txtCotizacionGastoAdicional.ObtenerValor) / txtCotizacionUs.ObtenerValor, True)
                End If
                If cbxMonedaGasto.GetValue <> 1 And cbxMonedaGasto.GetValue <> 2 Then 'No es GS ni US
                    txtImporteGastoGS.txt.Text = txtImporteGasto.txt.Text * txtCotizacionGastoAdicional.ObtenerValor
                    txtImporteGastoUS.txt.Text = CSistema.FormatoNumero((txtImporteGasto.txt.Text * txtCotizacionGastoAdicional.ObtenerValor) / txtCotizacionUs.ObtenerValor, True)
                End If
            End If
        End If
    End Sub

    Private Sub dgwGastoAdicional_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgwGastoAdicional.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarGastoAdicional()
        End If
    End Sub

    Private Sub cbxChapa_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxChapa.PropertyChanged
        cbxCamion.cbx.DataSource = Nothing
        If cbxChapa.Validar = False Then
            Exit Sub
        End If
        CSistema.SqlToComboBox(cbxCamion.cbx, CData.GetTable("vCamionProveedor", " ID = " & cbxChapa.GetValue), "ID", "Descripcion")
        cbxChofer.Texto = CType(CSistema.ExecuteScalar("Select top(1) Nombres from ChoferProveedor where IDCamion = " & cbxChapa.GetValue), String)
    End Sub

    Private Sub btnModificar_Click(sender As System.Object, e As System.EventArgs) Handles btnModificar.Click
        Modificar()

    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmAsignarAcuerdoTicket_Activate()
        Me.Refresh()
    End Sub
End Class
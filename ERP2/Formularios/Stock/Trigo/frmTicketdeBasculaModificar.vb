﻿Public Class frmTicketdeBasculaModificar
    Dim CSistema As New CSistema
    Public Property Remision As String
    Public Property Recepcion As String
    Public Property Observacion As String
    Public Property IDOperacion As Integer
    Public Property IDTransaccion As Integer

    Sub Inicializar()
        Cargar()
    End Sub

    Sub Cargar()
        txtRemision.txt.Text = Remision
        txtRecepcion.txt.Text = Recepcion
        txtObservacion.txt.Text = Observacion
    End Sub

    Sub Guardar()
        Try
            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroRemision", txtRemision.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroRecepcion", txtRecepcion.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDterminal", vgIDTerminal, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionSalida", 0, ParameterDirection.Output)



            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            'Aplicar Modificaciones
            If CSistema.ExecuteStoreProcedure(param, "SpTicketBascula", False, False, MensajeRetorno) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            Me.Close()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmTicketdeBasculaModificar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmTicketdeBasculaModificar_Activate()
        Me.Refresh()
    End Sub
End Class
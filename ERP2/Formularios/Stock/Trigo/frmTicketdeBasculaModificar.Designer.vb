﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTicketdeBasculaModificar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtRemision = New ERP.ocxTXTString()
        Me.txtRecepcion = New ERP.ocxTXTString()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.lblRemision = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtRemision
        '
        Me.txtRemision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemision.Color = System.Drawing.Color.Empty
        Me.txtRemision.Indicaciones = Nothing
        Me.txtRemision.Location = New System.Drawing.Point(141, 31)
        Me.txtRemision.Multilinea = False
        Me.txtRemision.Name = "txtRemision"
        Me.txtRemision.Size = New System.Drawing.Size(264, 27)
        Me.txtRemision.SoloLectura = False
        Me.txtRemision.TabIndex = 0
        Me.txtRemision.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRemision.Texto = ""
        '
        'txtRecepcion
        '
        Me.txtRecepcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRecepcion.Color = System.Drawing.Color.Empty
        Me.txtRecepcion.Indicaciones = Nothing
        Me.txtRecepcion.Location = New System.Drawing.Point(141, 66)
        Me.txtRecepcion.Multilinea = False
        Me.txtRecepcion.Name = "txtRecepcion"
        Me.txtRecepcion.Size = New System.Drawing.Size(264, 27)
        Me.txtRecepcion.SoloLectura = False
        Me.txtRecepcion.TabIndex = 1
        Me.txtRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRecepcion.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(141, 101)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(264, 27)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 2
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'lblRemision
        '
        Me.lblRemision.AutoSize = True
        Me.lblRemision.Location = New System.Drawing.Point(30, 38)
        Me.lblRemision.Name = "lblRemision"
        Me.lblRemision.Size = New System.Drawing.Size(56, 13)
        Me.lblRemision.TabIndex = 3
        Me.lblRemision.Text = "Remision :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(30, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Recepción :"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(30, 108)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(73, 13)
        Me.lblObservacion.TabIndex = 5
        Me.lblObservacion.Text = "Observación :"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(55, 163)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 6
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(285, 163)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmTicketdeBasculaModificar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(442, 212)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.lblObservacion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblRemision)
        Me.Controls.Add(Me.txtObservacion)
        Me.Controls.Add(Me.txtRecepcion)
        Me.Controls.Add(Me.txtRemision)
        Me.Name = "frmTicketdeBasculaModificar"
        Me.Text = "frmTicketdeBasculaModificar"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtRemision As ocxTXTString
    Friend WithEvents txtRecepcion As ocxTXTString
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents lblRemision As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblObservacion As Label
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnSalir As Button
End Class

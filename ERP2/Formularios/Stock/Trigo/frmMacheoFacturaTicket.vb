﻿Public Class frmMacheoFacturaTicket
    'CLASES
    Dim CSistema As New CSistema
    Dim CAsientoContableCobranzaCredito As New CAsientoContableCobranzaCredito
    Dim CArchivoInicio As New CArchivoInicio
    Dim CAsiento As New CAsientoMacheoFacturaTicket

    'VARIABLES
    Dim vControles() As Control
    Dim dtCargarFactura As New DataTable
    Dim dtCargarTicketBascula As New DataTable
    Dim dtFacturaNotaCredito As New DataTable
    Dim dtTicketsPendientes As New DataTable
    Dim vNuevo As Boolean
    Dim vIDAcuerdo As Integer = 0
    Dim vIDTransaccionticket As Integer = 0
    Dim dtTicket As DataTable
    Dim vDiferencia As Integer = 0
    Dim vIDMoneda As Integer = 1
    Dim vid As Integer = 0

    'PROPIEDADES
    Private IDTransaccionFacturaValue As Integer
    Public Property IDTransaccionFactura() As Integer
        Get
            Return IDTransaccionFacturaValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionFacturaValue = value
        End Set
    End Property

    Private IDTransaccionTicketValue As Integer
    Public Property IDTransaccionTicket() As Integer
        Get
            Return IDTransaccionTicketValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionTicketValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Controles
        'txtProveedor.Conectar()

        'Otros
        cbxCiudad.Enabled = True

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "MACHEO FACTURA TICKET", "MFT")
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        CAsientoContableCobranzaCredito.Inicializar()


        'Botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO, btnNuevo, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, btnAsiento, vControles, New Button, btnEliminar)


        'Dim ID As Integer
        'ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From vMacheoFacturaTicket Where IDsucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

        'txtID.txt.Text = ID
        txtID.txt.SelectAll()
        CargarOperacion()
        BloquearCostos()
        txtID.Focus()
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtAcuerdo)
        CSistema.CargaControl(vControles, bntAgregarFactura)
        CSistema.CargaControl(vControles, btnAgregarTicket)

        'CARGAR ESTRUCTURA DEL GASTO ADICIONAL
        dtTicketsPendientes = CSistema.ExecuteToDataTable("Select Top(0) * From vTicketBascula").Clone


        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal where ID in(4,10) Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "YAT")
        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "MOL")
        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        txtFecha.Hoy()
        'Moneda
        ObtenerCotizacion(vIDMoneda)


    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)



    End Sub

    Sub Nuevo()

        'Configurar botones
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO, btnNuevo, btnGuardar, btnCancelar, btnEliminar, New Button, btnBusquedaAvanzada, btnAsiento, vControles)
        vIDAcuerdo = 0
        'Limpiar detalle
        '
        ' ListarVentas()

        vNuevo = True

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        'CAsientoContableCobranzaCredito.Limpiar()


        'Cabecera
        txtFecha.txt.Text = ""
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()
        txtAcuerdo.txt.Clear()
        CAsiento.Limpiar()
        CAsiento.Inicializar()

        'Obtener registro nuevo

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From MacheoFacturaTicket where idSucursal = " & cbxSucursal.cbx.SelectedValue & "),1)"), Integer)

        vid = txtID.ObtenerValor

        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "MacheoFacturaTicket", "NroComprobante", cbxSucursal.cbx.SelectedValue)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en el proveedor
        cbxTipoComprobante.cbx.Focus()

        'Limpiar  Transferencia Nota de Credito
        txtComprobanteFactura.txt.Text = ""
        txtFechaFactura.txt.Text = ""
        txtComprobanteFactura.txt.Text = ""
        txtProducto.txt.Text = ""
        txtTotalFactura.txt.Text = ""
        txtTotalRemision.txt.Text = ""
        txtTotalBascula.txt.Text = ""
        txtProveedor.txt.Text = ""
        txtPrecio.txt.Text = "0"
        txtCotizacionFactura.txt.Text = "0"
        txtCantidadFactura.txt.Text = "0"
        'ListBox1.DataSource = Nothing
        DataGridView1.Rows.Clear()
        txtComprobante.Focus()
        dtTicketsPendientes.Clear()
        txtTotalGs.txt.Text = "0"
        txtTotalUs.txt.Text = "0"
        txtComprobante.txt.Text = txtID.txt.Text
    End Sub

    Sub Cancelar()
        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR, btnNuevo, btnGuardar, btnCancelar, btnEliminar, New Button, btnBusquedaAvanzada, btnAsiento, vControles)

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From vMacheoFacturaTicket Where IDsucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
        vIDAcuerdo = 0
        txtAcuerdo.txt.Text = ""
        txtProveedor.txt.Text = ""
        txtCotizacion.txt.Text = "0"
        txtPrecio.txt.Text = "0"
        txtProducto.txt.Text = ""
        txtObservacion.txt.Text = ""
        txtComprobanteFactura.txt.Text = ""
        txtFechaFactura.txt.Text = ""
        txtCotizacionFactura.txt.Text = "0"
        txtCantidadFactura.txt.Text = "0"
        txtTotalFactura.txt.Text = "0"
        dtTicketsPendientes.Clear()
        DataGridView1.Rows.Clear()
        txtID.txt.Text = ID
        txtID.txt.SelectAll()
        CargarOperacion()
        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()


        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If
        'SMC - 20052021 - Se modifica por variable al depositar número
        CSistema.SetSQLParameter(param, "@Numero", vid, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCotizacion.txt.Text), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroAcuerdo", vIDAcuerdo, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTransaccionGasto", IDTransaccionFactura, ParameterDirection.Input)
        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1
        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpMacheoFacturaTicket", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim Procesar As Boolean = True


        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, btnEliminar, New Button, btnBusquedaAvanzada, btnAsiento, vControles)
            'CargarOperacion(IDTransaccion)

            txtID.SoloLectura = False
        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            'Guardar Nota Credito Aplicada
            Procesar = InsertarTicket(IDTransaccion)

            'Cargamos el asiento (si asiento bloqueado)
            'If CAsiento.Bloquear = True Then
            If CAsiento.dtDetalleAsiento.Rows.Count > 0 Then
                CAsiento.IDTransaccion = IDTransaccion
                GenerarAsiento()
                CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
                'End If
            End If

        End If



        Try

            ReDim param(-1)
            '' SOLO PARA CARGAR EN EL KARDEX

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            'Aplicar
            If CSistema.ExecuteStoreProcedure(param, "SpMacheoProcesar", False, False, MensajeRetorno) = False Then

            End If

        Catch ex As Exception

        End Try


        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR, btnNuevo, btnGuardar, btnCancelar, btnEliminar, New Button, btnBusquedaAvanzada, btnAsiento, vControles)
        CargarOperacion()

        txtID.SoloLectura = False

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False


        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtComprobante.txt.Text) = False Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Nro de Comprobante, mayor a 0
        If IsNumeric(txtAcuerdo.txt.Text) = False Then
            Dim mensaje As String = "El Acuerdo no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Asignamos entero al nro de comprobante
        txtComprobante.txt.Text = CInt(txtComprobante.txt.Text)

        If CInt(txtComprobante.txt.Text) = 0 Then
            Dim mensaje As String = "El numero de comprobante no es correcto!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Seleccion de Moneda
        If IsNumeric(cbxMoneda.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el tipo de moneda!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If
        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If
        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        'Validar el Asiento
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
            If CAsiento.ObtenerSaldo <> 0 Then
                CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
                Exit Function
            End If
        End If

        ValidarDocumento = True

    End Function
    Sub ObtenerCotizacion(ByVal IDMoneda As Integer)
        If vNuevo = True Then
            Dim vFecha As String = ""
            vFecha = CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False)
            If vFecha.Length = 8 Then
                txtCotizacion.txt.Text = CType(CSistema.ExecuteScalar("select Isnull((Select cotizacion from cotizacion where IDMoneda = " & IDMoneda & "and cast(fecha as date) = '" & vFecha & "'),1)"), Integer)
            End If
            If txtCotizacion.ObtenerValor = 1 And vIDMoneda <> 1 Then
                MessageBox.Show("Cargar cotizacion del dia!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Cancelar()
            End If
        End If
    End Sub
    Sub CargarAcuerdo()
        ctrError.Clear()
        tsslEstado.Text = ""
        Dim dtAcuerdo As DataTable

        If txtAcuerdo.txt.Text = "" Then
            Dim mensaje As String = "Ingrese Nro de Comprobante del Acuerdo!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtAcuerdo = CSistema.ExecuteToDataTable("Select * From vAcuerdo Where NroAcuerdo = '" & txtAcuerdo.txt.Text & "'")

        'Cargamos la cabecera
        If dtAcuerdo Is Nothing Or dtAcuerdo.Rows.Count = 0 Then
            Dim mensaje As String = "El Acuerdo no existe."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtAcuerdo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dtAcuerdo.Rows(0)
        vIDAcuerdo = oRow("NroAcuerdo").ToString
        txtProveedor.Texto = oRow("Proveedor").ToString
        'Moneda
        cbxMoneda.Texto = oRow("Moneda").ToString
        txtPrecio.txt.Text = oRow("Precio").ToString
        txtProducto.txt.Text = oRow("Producto").ToString
        cbxMoneda.Texto = oRow("ReferenciaMoneda").ToString
        vIDMoneda = oRow("IDMoneda").ToString
        ObtenerCotizacion(oRow("IDMoneda").ToString)
        'txtCotizacion
        dtAcuerdo.Clear()
        SeleccionarAcuerdo()
    End Sub

    Sub SeleccionarAcuerdo()

        'Si no es nuevo, salir
        If vNuevo = False Then
            Exit Sub
        End If

        'Limpiar
        DataGridView1.Rows.Clear()

        If vIDAcuerdo = 0 Then
            dtCargarFactura.Rows.Clear()
            Exit Sub
        End If

        'Obtenemos las Facturas y Tickets de Basculas
        '!!!IMPORTANTE -
        dtCargarFactura = CSistema.ExecuteToDataTable("Select IDTransaccion, Sucursal, G.Fecha, [Cod.],Comprobante, Total, Saldo, Moneda, Cotizacion,GP.Cantidad, GP.Observacion,'Sel'='True' from VGasto G  Join GastoProducto GP on GP.IDTransaccionGasto = G.IDTransaccion Join Acuerdo A on A.NroAcuerdo = GP.NroAcuerdo where A.NroAcuerdo ='" & vIDAcuerdo & "'")
        'dtCargarTicketBascula = CSistema.ExecuteToDataTable("Select IDTransaccion, Fecha,Cotizacion,'Comprobante'=concat(ReferenciaTipoComprobante,' - ',NroComprobante), PesoRemision,PesoBascula,Diferencia,'Importe'=Total,'IDTransaccionFacturaAplicacion'=0 From VTicketBascula Where NroAcuerdo ='" & vIDAcuerdo & "'")

        'Listamos las Facturas pendientes
        'ListarTicket()

        'Foco
        cbxMoneda.cbx.Focus()

    End Sub
    Sub VerificarAcuerdo()

        'Validar
        If vIDAcuerdo = 0 Then
            MessageBox.Show("Seleccione un acuerdo para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
    End Sub
    Function CargarFactura() As Boolean

        CargarFactura = False

        Dim frm As New frmSeleccionarFacturaAmachear
        frm.Text = "Seleccionar Factura"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtCargarFactura
        frm.txtImportante.Visible = True
        FGMostrarFormulario(Me, frm, "Seleccione la Factura para la operacion", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Seleccionado = False Then
            Return False
        End If

        IDTransaccionFactura = frm.IDTransaccion

        Return True

    End Function
    Sub CargarTicketBascula()
        'Validar
        ' Insertar
        dtTicketsPendientes = CSistema.ExecuteToDataTable("Select vTicketBascula.*, 'Sel'='False' From vTicketBascula Where NroAcuerdo = '" & vIDAcuerdo & "' and IDTransaccion not in (select IDTransaccionTicket from detallemacheo ) and NroAcuerdo >0 and Anulado = 'False'")

        Dim frm As New frmSeleccionarTicketBascula
        frm.Text = "Seleccion Tickets de Bascula"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtTicketsPendientes
        frm.ShowDialog(Me)
        dtTicketsPendientes = frm.dt

        ListarTicket()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub
    Sub SeleccionarFactura()

        'Validar
        If vIDAcuerdo = 0 Then
            MessageBox.Show("Seleccione un acuerdo para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If


        If CargarFactura() = False Then
            Exit Sub
        End If

        'Cargamos la transaccion seleccionada
        CargarFacturaAmachear()
    End Sub
    Sub SeleccionarTicketBascula()

        'Validar
        If vIDAcuerdo = 0 Then
            MessageBox.Show("Seleccione un acuerdo para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If


    End Sub
    Sub CargarFacturaAmachear()

        For Each oRow As DataRow In dtCargarFactura.Rows()
            If oRow("Sel") = True Then
                If oRow("IDTransaccion").ToString = IDTransaccionFactura Then
                    txtFechaFactura.txt.Text = oRow("Fecha").ToString
                    txtComprobanteFactura.txt.Text = oRow("Comprobante").ToString
                    txtCotizacionFactura.txt.Text = oRow("Cotizacion").ToString
                    txtCantidadFactura.txt.Text = oRow("Cantidad").ToString
                    txtTotalFactura.SetValue(oRow("Total").ToString)
                    Exit For
                End If
            End If
        Next
    End Sub
    Sub ListarTicket()
        'Cargar en la Grilla
        Dim vTotalBascula As Decimal = 0.0
        Dim vTotalRemision As Decimal = 0.0
        Dim vTotal As Decimal = 0
        Dim vTotalUs As Decimal = 0.0
        DataGridView1.Rows.Clear()

        For Each oRow As DataRow In dtTicketsPendientes.Rows
            If oRow("Sel") = True Then
                Dim rIndex As Integer = DataGridView1.Rows.Add()
                DataGridView1.Item(0, rIndex).Value = oRow("IDTransaccion").ToString
                DataGridView1.Item(1, rIndex).Value = oRow("Numero").ToString
                DataGridView1.Item(2, rIndex).Value = oRow("Fecha").ToString
                DataGridView1.Item(3, rIndex).Value = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
                DataGridView1.Item(4, rIndex).Value = CSistema.FormatoMoneda(oRow("PesoRemision").ToString, True)
                DataGridView1.Item(5, rIndex).Value = CSistema.FormatoMoneda(oRow("PesoBascula").ToString, True)
                DataGridView1.Item(6, rIndex).Value = CSistema.FormatoMoneda(oRow("Total").ToString, True)
                DataGridView1.Item(7, rIndex).Value = CSistema.FormatoMoneda(oRow("TotalUS").ToString, True)


                vTotalBascula = vTotalBascula + oRow("PesoBascula").ToString
                vTotalRemision = vTotalRemision + oRow("PesoRemision").ToString
                vTotal = vTotal + oRow("Total").ToString
                vTotalUs = vTotalUs + oRow("TotalUS").ToString
            End If
        Next
        txtTotalBascula.txt.Text = vTotalBascula
        txtTotalRemision.txt.Text = vTotalRemision
        txtTotalGs.txt.Text = vTotal
        txtTotalUs.txt.Text = vTotalUs
        txtDiferenciaCantidad.txt.Text = txtCantidadFactura.ObtenerValor - vTotalBascula
        txtDiferenciaImporte.txt.Text = (txtTotalFactura.ObtenerValor * txtCotizacionFactura.ObtenerValor) - vTotal
        'Formato
        DataGridView1.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        DataGridView1.Columns(6).Visible = vgVerCosto
        DataGridView1.Columns(7).Visible = vgVerCosto

    End Sub

    Function InsertarTicket(ByRef IDTransaccion As Integer) As Boolean
        InsertarTicket = True
        Dim id As Integer = 0

        For Each oRow As DataRow In dtTicketsPendientes.Rows
            Dim sql As String = ""
            If oRow("Sel") = True Then
                sql = "Insert Into DetalleMacheo (IDTransaccionMacheo,ID, IDTransaccionTicket,Anulado) Values (" & IDTransaccion & "," & id & "," & oRow("IDTransaccion").ToString & ",'False')"
                If CSistema.ExecuteNonQuery(sql) = 0 Then
                    Return False
                End If
                id = id + 1
            End If
        Next

        'Insertamos el asiento
        CSistema.ExecuteNonQuery("exec SpAsientoMacheoTicketFactura @IDTransaccion= " & IDTransaccion)

    End Function

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vMacheoFacturaTicket Where Numero=" & txtID.ObtenerValor & "And IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")
            'SMC - 20052021 - Se modifica porque al eliminar si la transaccion es 0, arroja mensaje de error, debe traer el ultimo de la base.
            'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select max(IDTransaccion) From vMacheoFacturaTicket Where IDSucursal = " & cbxSucursal.cbx.SelectedValue & "), 0 )")

        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtComprobante, mensaje)
            ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA, btnNuevo, btnGuardar, btnCancelar, btnEliminar, New Button, btnBusquedaAvanzada, btnAsiento, vControles)



        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From vMacheoFacturaTicket Where IDTransaccion=" & IDTransaccion)


        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtComprobante, mensaje)
            ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        cbxCiudad.txt.Text = oRow("Sucursal").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxMoneda.txt.Text = oRow("Moneda").ToString
        'txtCliente.SetValue(oRow("IDCliente").ToString)
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtFecha.SetValueFromString(oRow("Fecha").ToString)
        txtCotizacion.txt.Text = CSistema.FormatoMoneda(oRow("Cotizacion"))
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtAcuerdo.txt.Text = oRow("NroAcuerdo").ToString
        txtProveedor.txt.Text = oRow("Proveedor").ToString
        txtPrecio.txt.Text = CSistema.FormatoMoneda(oRow("Precio"), True, 3)
        txtProducto.txt.Text = oRow("Producto").ToString

        'Factura
        txtComprobanteFactura.txt.Text = oRow("ComprobanteGasto").ToString
        txtFechaFactura.txt.Text = oRow("FechaGasto").ToString
        txtCotizacionFactura.txt.Text = CSistema.FormatoMoneda(oRow("CotizacionGasto"))
        txtCantidadFactura.txt.Text = CSistema.FormatoMoneda(oRow("CantidadGasto"), True)
        txtTotalFactura.txt.Text = CSistema.FormatoMoneda(oRow("TotalGasto"), True)

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        'Cargar detalle de Facturas y Tickets guardados
        If dtCargarFactura IsNot Nothing Then
            If dtCargarFactura.Rows.Count > 0 Then
                IDTransaccionFactura = dtCargarFactura.Rows(0)("IDTransaccion")
            End If
        End If


        dtTicketsPendientes = CSistema.ExecuteToDataTable("select vDetalleMacheo.*,'Sel'='True' from vDetalleMacheo where IDTransaccionMacheo=" & IDTransaccion).Copy
        ListarTicket()

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "MACHEO " & cbxTipoComprobante.cbx.Text & " - " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From vMacheoFacturaTicket Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & " Order By IDTransaccion Desc), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
                ctrError.SetError(cbxTipoComprobante, mensaje)
                ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "MACHEO " & cbxTipoComprobante.cbx.Text & " - " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
            GenerarAsiento()

            frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            CAsiento.ListarDetalle(frm.dgv)
            frm.CalcularTotales()

            frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            'Actualizamos el asiento si es que este tuvo alguna modificacion
            CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            CAsiento.Bloquear = frm.CAsiento.Bloquear
            CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            CAsiento.NroCaja = frm.CAsiento.NroCaja
            CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            If frm.VolverAGenerar = True Then
                CAsiento.Generado = False
                CAsiento.dtAsiento.Clear()
                CAsiento.dtDetalleAsiento.Clear()
                CAsiento.dtDetalleProductos.Clear()
                VisualizarAsiento()
            End If

        End If


    End Sub
    Sub GenerarAsiento()

        'Establecer Cabecera
        Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

        oRow("IDCiudad") = cbxCiudad.GetValue
        oRow("IDSucursal") = cbxSucursal.GetValue
        oRow("Fecha") = txtFecha.GetValue
        oRow("IDMoneda") = cbxMoneda.GetValue
        oRow("Cotizacion") = txtCotizacion.txt.Text
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("NroComprobante") = txtComprobante.txt.Text
        oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
        oRow("Detalle") = txtObservacion.txt.Text
        oRow("Total") = txtTotalFactura.ObtenerValor

        CAsiento.dtAsiento.Rows.Clear()
        CAsiento.dtAsiento.Rows.Add(oRow)

        'CAsiento.dtDetalleProductos = dtDetalle
        CAsiento.IDSucursal = cbxSucursal.GetValue
        CAsiento.IDMoneda = cbxMoneda.GetValue
        CAsiento.IDTipoOperacion = cbxTipoComprobante.GetValue

        'CAsiento.Generar()

    End Sub

    Sub EliminarTicket()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(DataGridView1, mensaje)
            ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        For Each item As DataGridViewRow In DataGridView1.SelectedRows

            Dim Comprobante As String = item.Cells(0).Value

            For Each oRow As DataRow In dtTicketsPendientes.Select("IDTransaccion='" & Comprobante & "'")
                oRow("Sel") = False
            Next
            Exit For

        Next

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtTicketsPendientes.Rows
            'oRow("ID") = Index
            Index = Index + 1
        Next

        ListarTicket()
    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnEliminar, mensaje)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpMacheoFacturaTicket", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnEliminar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnEliminar, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Private Sub BloquearCostos()
        Label7.Visible = vgVerCosto
        txtPrecio.Visible = vgVerCosto
        txtTotalGs.Visible = vgVerCosto
        txtTotalUs.Visible = vgVerCosto
    End Sub


    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        cbxSucursal.cbx.DataSource = Nothing
        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

        Cancelar()

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From vMacheoFacturaTicket Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From vMacheoFacturaTicket Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub DataGridView1_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellEndEdit
        If DataGridView1.Columns(e.ColumnIndex).Name = "colImporte" Then

            If IsNumeric(DataGridView1.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(DataGridView1, mensaje)
                ctrError.SetIconAlignment(DataGridView1, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                DataGridView1.CurrentCell.Value = DataGridView1.CurrentRow.Cells("colImporte").Value

            Else
                DataGridView1.CurrentCell.Value = CSistema.FormatoMoneda(DataGridView1.CurrentRow.Cells("colImporte").Value)
            End If

            For Each oRow As DataGridViewRow In DataGridView1.Rows
                Dim IDTransaccion As Integer = oRow.Cells("colIDTransaccion").Value
                For Each oRow1 As DataRow In dtFacturaNotaCredito.Select("IDTransaccionVenta  =" & IDTransaccion)
                    oRow1("Importe") = oRow.Cells("colImporte").Value
                Next
            Next
            txtTotalBascula.txt.Text = CSistema.dtSumColumn(dtFacturaNotaCredito, "PesoBascula")
            txtTotalRemision.txt.Text = CSistema.dtSumColumn(dtFacturaNotaCredito, "PesoRemision")

        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        'Buscar()
    End Sub

    Private Sub txtAcuerdo_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcuerdo.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarAcuerdo()
        End If
    End Sub

    Private Sub txtFecha_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtFecha.TeclaPrecionada
        ObtenerCotizacion(vIDMoneda)
    End Sub

    Private Sub bntAgregarFactura_Click(sender As System.Object, e As System.EventArgs) Handles bntAgregarFactura.Click
        SeleccionarFactura()
    End Sub

    Private Sub btnAgregar_Click(sender As System.Object, e As System.EventArgs)
        CargarTicketBascula()
    End Sub


    Private Sub DataGridView1_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarTicket()
        End If
    End Sub

    Private Sub btnAsiento_Click(sender As System.Object, e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub btnAgregarTicket_Click(sender As System.Object, e As System.EventArgs) Handles btnAgregarTicket.Click
        CargarTicketBascula()
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub

    Private Sub frmMacheoFacturaTicket_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmMacheoFacturaTicket_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmMacheoFacturaTicket_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmMacheoFacturaTicket_Activate()
        Me.Refresh()
    End Sub
End Class
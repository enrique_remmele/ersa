﻿Public Class frmSeleccionarTicketBascula
    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event VentasSeleccionadas(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEDADES
    Private NroAcuerdoValue As Integer
    Public Property NroAcuerdo() As Integer

        Get
            Return NroAcuerdoValue
        End Get
        Set(ByVal value As Integer)
            NroAcuerdoValue = value
        End Set

    End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private Decimalesvalue As Boolean
    Public Property decimales() As Boolean
        Get
            Return Decimalesvalue
        End Get
        Set(ByVal value As Boolean)
            Decimalesvalue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()
        'Funciones
        ListarVentas(False)
        CargarTicket()
        'Foco
        dgw.Focus()


    End Sub

    Sub ListarVentas(ByVal PorFactura As Boolean)

        'Limpiar la grilla
        dgw.Rows.Clear()
        Dim TotalDeuda As Decimal = 0

        For Each oRow As DataRow In dt.Select("")

            'For Each oRow As DataRow In dt.Rows
            Dim Registro(14) As String
            Registro(0) = oRow("IDTransaccion").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Numero").ToString
            Registro(3) = oRow("Fecha").ToString
            Registro(4) = oRow("Moneda").ToString
            Registro(5) = CSistema.FormatoMoneda(oRow("Cotizacion").ToString, False)
            Registro(6) = CSistema.FormatoMoneda(oRow("PesoRemision").ToString, True)
            Registro(7) = CSistema.FormatoMoneda(oRow("PesoBascula").ToString, True)
            Registro(8) = CSistema.FormatoMoneda(oRow("Diferencia").ToString, True)
            Registro(9) = CSistema.FormatoMoneda(oRow("PrecioUnitario").ToString, False)
            Registro(10) = CSistema.FormatoMoneda(oRow("PrecioUnitarioUS").ToString, True)
            Registro(11) = CSistema.FormatoMoneda(oRow("Total").ToString, False)
            Registro(12) = CSistema.FormatoMoneda(oRow("TotalUS").ToString, False)
            Registro(13) = CSistema.FormatoMoneda(oRow("CostoAdicional").ToString, False)
            Registro(14) = CSistema.FormatoMoneda(oRow("CostoAdicionalUS").ToString, True)

            dgw.Rows.Add(Registro)

        Next
        dgw.Columns(9).Visible = vgVerCosto
        dgw.Columns(10).Visible = vgVerCosto
        dgw.Columns(11).Visible = vgVerCosto
        dgw.Columns(12).Visible = vgVerCosto
    End Sub

    Sub SeleccionarTicket()

        'Validar nuevamente que los importes sean mayor a 0
        For Each oRow As DataGridViewRow In dgw.Rows
            'If oRow.Cells("colImporte").Value <= 0 Then
            '    MessageBox.Show("El importe no puede ser 0", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    Exit Sub
            'End If

        Next

        For Each oRow As DataGridViewRow In dgw.Rows

            Dim IDTransaccion As Integer = oRow.Cells(0).Value

            For Each oRow1 As DataRow In dt.Select("IDTransaccion=" & IDTransaccion)
                oRow1("Sel") = oRow.Cells(1).Value
            Next

        Next

        Me.Close()

    End Sub

    Sub CargarTicket()

        For Each oRow As DataRow In dt.Rows
            For Each oRow1 As DataGridViewRow In dgw.Rows
                If oRow("IDTransaccion") = oRow1.Cells(0).Value Then
                    oRow1.Cells(1).Value = CBool(oRow("Sel").ToString)
                End If
            Next
        Next

        PintarCelda()

    End Sub

    Sub PintarCelda()

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgw.Rows
            If Row.Cells(1).Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

        dgw.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub


    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(11).ReadOnly = False
                        oRow.Cells(13).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise

                        'dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(10)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(11).ReadOnly = True
                        oRow.Cells(13).ReadOnly = True
                        oRow.Cells(11).Value = oRow.Cells(9).Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next
            dgw.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells(11).Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            Button1.Focus()
        End If

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then
            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex
            For Each oRow As DataGridViewRow In dgw.Rows
                If oRow.Index = RowIndex Then
                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(11).ReadOnly = False
                        oRow.Cells(13).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        'dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(11)
                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(11).ReadOnly = True
                        oRow.Cells(13).ReadOnly = True
                        oRow.Cells(11).Value = oRow.Cells(9).Value
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If
                    Exit For
                End If
            Next
            dgw.Update()

        End If
    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeleccionarTodoToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = True
        Next

        PintarCelda()

    End Sub

    Private Sub QuitarTodaSeleccionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitarTodaSeleccionToolStripMenuItem.Click
        For Each oRow As DataGridViewRow In dgw.Rows
            oRow.Cells(1).Value = False
        Next

        PintarCelda()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        SeleccionarTicket()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub dgw_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellLeave

    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        For Each fila As DataGridViewRow In dgw.Rows
            fila.Cells("colSel").Value = True
        Next
    End Sub

    Private Sub frmSeleccionarTicketBascula_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmSeleccionarTicketBascula_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAcuerdo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAcuerdo))
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtcompradorint = New ERP.ocxTXTString()
        Me.txtproveedorint = New ERP.ocxTXTString()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.lblintervinientes = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtimpurezas = New ERP.ocxTXTNumeric()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.cbxtabladescuento = New ERP.ocxCBX()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtfn = New ERP.ocxTXTNumeric()
        Me.txtgluten = New ERP.ocxTXTNumeric()
        Me.txthmax = New ERP.ocxTXTNumeric()
        Me.txtphmax = New ERP.ocxTXTNumeric()
        Me.txthmin = New ERP.ocxTXTNumeric()
        Me.txtphmin = New ERP.ocxTXTNumeric()
        Me.lblPrecioacordado = New System.Windows.Forms.Label()
        Me.cbxPrecioacordado = New ERP.ocxCBX()
        Me.lblPromedio = New System.Windows.Forms.Label()
        Me.lblBascula = New System.Windows.Forms.Label()
        Me.cbxBascula = New ERP.ocxCBX()
        Me.lblFacturacion = New System.Windows.Forms.Label()
        Me.cbxFacturacion = New ERP.ocxCBX()
        Me.lblCondicionEntrega = New System.Windows.Forms.Label()
        Me.cbxCondicionEntrega = New ERP.ocxCBX()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtCantidadCredito = New ERP.ocxTXTNumeric()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtPrecioCredito = New ERP.ocxTXTNumeric()
        Me.cbxMonedaCredito = New ERP.ocxCBX()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCostoFlete = New ERP.ocxTXTNumeric()
        Me.cbxMonedaFlete = New ERP.ocxCBX()
        Me.lblUnidadMedida = New System.Windows.Forms.Label()
        Me.txtHastaEmbarque = New ERP.ocxTXTDate()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtDesdeEmbarque = New ERP.ocxTXTDate()
        Me.txtIDProducto = New ERP.ocxTXTNumeric()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbxTipoFlete = New ERP.ocxCBX()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtZafra = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPrecio = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblVencimiento = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtNroAcuerdo = New ERP.ocxTXTString()
        Me.txtProducto = New ERP.ocxTXTProducto()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Agregar.png")
        Me.ImageList1.Images.SetKeyName(1, "Editar.png")
        Me.ImageList1.Images.SetKeyName(2, "Eliminar.png")
        Me.ImageList1.Images.SetKeyName(3, "Guardar.png")
        Me.ImageList1.Images.SetKeyName(4, "Cancelar.png")
        Me.ImageList1.Images.SetKeyName(5, "Actualizar.png")
        Me.ImageList1.Images.SetKeyName(6, "Buscar.png")
        Me.ImageList1.Images.SetKeyName(7, "Cargar.png")
        Me.ImageList1.Images.SetKeyName(8, "Imprimir.png")
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 505)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1060, 22)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnSalir)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnGuardar)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(494, 476)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(563, 48)
        Me.FlowLayoutPanel2.TabIndex = 4
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Location = New System.Drawing.Point(485, 3)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.ImageIndex = 4
        Me.btnCancelar.ImageList = Me.ImageList1
        Me.btnCancelar.Location = New System.Drawing.Point(389, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(90, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.ImageIndex = 3
        Me.btnGuardar.ImageList = Me.ImageList1
        Me.btnGuardar.Location = New System.Drawing.Point(293, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(90, 23)
        Me.btnGuardar.TabIndex = 0
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnNuevo)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEditar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEliminar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnImprimir)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 476)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(485, 48)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'btnNuevo
        '
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.ImageIndex = 0
        Me.btnNuevo.ImageList = Me.ImageList1
        Me.btnNuevo.Location = New System.Drawing.Point(3, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(82, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.ImageIndex = 1
        Me.btnEditar.ImageList = Me.ImageList1
        Me.btnEditar.Location = New System.Drawing.Point(91, 3)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 1
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.ImageIndex = 2
        Me.btnEliminar.ImageList = Me.ImageList1
        Me.btnEliminar.Location = New System.Drawing.Point(172, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(83, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.ImageKey = "Imprimir.png"
        Me.btnImprimir.ImageList = Me.ImageList1
        Me.btnImprimir.Location = New System.Drawing.Point(261, 3)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(87, 23)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 11)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(485, 459)
        Me.dgvLista.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 569.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 1, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1060, 527)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtcompradorint)
        Me.GroupBox1.Controls.Add(Me.txtproveedorint)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.lblintervinientes)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.txtimpurezas)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.cbxtabladescuento)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.txtfn)
        Me.GroupBox1.Controls.Add(Me.txtgluten)
        Me.GroupBox1.Controls.Add(Me.txthmax)
        Me.GroupBox1.Controls.Add(Me.txtphmax)
        Me.GroupBox1.Controls.Add(Me.txthmin)
        Me.GroupBox1.Controls.Add(Me.txtphmin)
        Me.GroupBox1.Controls.Add(Me.lblPrecioacordado)
        Me.GroupBox1.Controls.Add(Me.cbxPrecioacordado)
        Me.GroupBox1.Controls.Add(Me.lblPromedio)
        Me.GroupBox1.Controls.Add(Me.lblBascula)
        Me.GroupBox1.Controls.Add(Me.cbxBascula)
        Me.GroupBox1.Controls.Add(Me.lblFacturacion)
        Me.GroupBox1.Controls.Add(Me.cbxFacturacion)
        Me.GroupBox1.Controls.Add(Me.lblCondicionEntrega)
        Me.GroupBox1.Controls.Add(Me.cbxCondicionEntrega)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtCantidadCredito)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.txtPrecioCredito)
        Me.GroupBox1.Controls.Add(Me.cbxMonedaCredito)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtCostoFlete)
        Me.GroupBox1.Controls.Add(Me.cbxMonedaFlete)
        Me.GroupBox1.Controls.Add(Me.lblUnidadMedida)
        Me.GroupBox1.Controls.Add(Me.txtHastaEmbarque)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtProveedor)
        Me.GroupBox1.Controls.Add(Me.txtDesdeEmbarque)
        Me.GroupBox1.Controls.Add(Me.txtIDProducto)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.cbxTipoFlete)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtCantidad)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtZafra)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtPrecio)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.lblEstado)
        Me.GroupBox1.Controls.Add(Me.rdbActivo)
        Me.GroupBox1.Controls.Add(Me.rdbDesactivado)
        Me.GroupBox1.Controls.Add(Me.lblID)
        Me.GroupBox1.Controls.Add(Me.lblVencimiento)
        Me.GroupBox1.Controls.Add(Me.txtID)
        Me.GroupBox1.Controls.Add(Me.txtFecha)
        Me.GroupBox1.Controls.Add(Me.cbxMoneda)
        Me.GroupBox1.Controls.Add(Me.lblDescripcion)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.txtNroAcuerdo)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(494, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.TableLayoutPanel1.SetRowSpan(Me.GroupBox1, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(563, 467)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos"
        '
        'txtcompradorint
        '
        Me.txtcompradorint.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompradorint.Color = System.Drawing.Color.Empty
        Me.txtcompradorint.Indicaciones = Nothing
        Me.txtcompradorint.Location = New System.Drawing.Point(363, 385)
        Me.txtcompradorint.Multilinea = False
        Me.txtcompradorint.Name = "txtcompradorint"
        Me.txtcompradorint.Size = New System.Drawing.Size(183, 21)
        Me.txtcompradorint.SoloLectura = False
        Me.txtcompradorint.TabIndex = 28
        Me.txtcompradorint.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtcompradorint.Texto = ""
        '
        'txtproveedorint
        '
        Me.txtproveedorint.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtproveedorint.Color = System.Drawing.Color.Empty
        Me.txtproveedorint.Indicaciones = Nothing
        Me.txtproveedorint.Location = New System.Drawing.Point(80, 385)
        Me.txtproveedorint.Multilinea = False
        Me.txtproveedorint.Name = "txtproveedorint"
        Me.txtproveedorint.Size = New System.Drawing.Size(183, 21)
        Me.txtproveedorint.SoloLectura = False
        Me.txtproveedorint.TabIndex = 27
        Me.txtproveedorint.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtproveedorint.Texto = ""
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(298, 385)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(58, 13)
        Me.Label27.TabIndex = 83
        Me.Label27.Text = "Comprador"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(11, 385)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(56, 13)
        Me.Label28.TabIndex = 82
        Me.Label28.Text = "Proveedor"
        '
        'lblintervinientes
        '
        Me.lblintervinientes.AutoSize = True
        Me.lblintervinientes.Location = New System.Drawing.Point(182, 368)
        Me.lblintervinientes.Name = "lblintervinientes"
        Me.lblintervinientes.Size = New System.Drawing.Size(117, 13)
        Me.lblintervinientes.TabIndex = 81
        Me.lblintervinientes.Text = "Personas Intervinientes"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(11, 349)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(55, 13)
        Me.Label26.TabIndex = 80
        Me.Label26.Text = "Impurezas"
        '
        'txtimpurezas
        '
        Me.txtimpurezas.Color = System.Drawing.Color.Empty
        Me.txtimpurezas.Decimales = True
        Me.txtimpurezas.Indicaciones = Nothing
        Me.txtimpurezas.Location = New System.Drawing.Point(80, 346)
        Me.txtimpurezas.Name = "txtimpurezas"
        Me.txtimpurezas.Size = New System.Drawing.Size(76, 22)
        Me.txtimpurezas.SoloLectura = False
        Me.txtimpurezas.TabIndex = 22
        Me.txtimpurezas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtimpurezas.Texto = "0"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(357, 329)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(102, 13)
        Me.Label25.TabIndex = 77
        Me.Label25.Text = "Tabla de descuento"
        '
        'cbxtabladescuento
        '
        Me.cbxtabladescuento.CampoWhere = Nothing
        Me.cbxtabladescuento.CargarUnaSolaVez = False
        Me.cbxtabladescuento.DataDisplayMember = Nothing
        Me.cbxtabladescuento.DataFilter = Nothing
        Me.cbxtabladescuento.DataOrderBy = Nothing
        Me.cbxtabladescuento.DataSource = Nothing
        Me.cbxtabladescuento.DataValueMember = Nothing
        Me.cbxtabladescuento.dtSeleccionado = Nothing
        Me.cbxtabladescuento.FormABM = Nothing
        Me.cbxtabladescuento.Indicaciones = Nothing
        Me.cbxtabladescuento.Location = New System.Drawing.Point(466, 329)
        Me.cbxtabladescuento.Name = "cbxtabladescuento"
        Me.cbxtabladescuento.SeleccionMultiple = False
        Me.cbxtabladescuento.SeleccionObligatoria = False
        Me.cbxtabladescuento.Size = New System.Drawing.Size(57, 21)
        Me.cbxtabladescuento.SoloLectura = False
        Me.cbxtabladescuento.TabIndex = 26
        Me.cbxtabladescuento.Texto = "SI"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(176, 323)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(21, 13)
        Me.Label24.TabIndex = 76
        Me.Label24.Text = "FN"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(11, 323)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(38, 13)
        Me.Label23.TabIndex = 75
        Me.Label23.Text = "Gluten"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(161, 300)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(42, 13)
        Me.Label21.TabIndex = 74
        Me.Label21.Text = "H° Máx"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(11, 300)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(39, 13)
        Me.Label22.TabIndex = 73
        Me.Label22.Text = "H° Min"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(161, 272)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(45, 13)
        Me.Label20.TabIndex = 72
        Me.Label20.Text = "PH Máx"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(11, 272)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 13)
        Me.Label19.TabIndex = 71
        Me.Label19.Text = "PH Min"
        '
        'txtfn
        '
        Me.txtfn.Color = System.Drawing.Color.Empty
        Me.txtfn.Decimales = True
        Me.txtfn.Indicaciones = Nothing
        Me.txtfn.Location = New System.Drawing.Point(209, 320)
        Me.txtfn.Name = "txtfn"
        Me.txtfn.Size = New System.Drawing.Size(76, 22)
        Me.txtfn.SoloLectura = False
        Me.txtfn.TabIndex = 21
        Me.txtfn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtfn.Texto = "0"
        '
        'txtgluten
        '
        Me.txtgluten.Color = System.Drawing.Color.Empty
        Me.txtgluten.Decimales = True
        Me.txtgluten.Indicaciones = Nothing
        Me.txtgluten.Location = New System.Drawing.Point(80, 320)
        Me.txtgluten.Name = "txtgluten"
        Me.txtgluten.Size = New System.Drawing.Size(76, 22)
        Me.txtgluten.SoloLectura = False
        Me.txtgluten.TabIndex = 20
        Me.txtgluten.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtgluten.Texto = "0"
        '
        'txthmax
        '
        Me.txthmax.Color = System.Drawing.Color.Empty
        Me.txthmax.Decimales = True
        Me.txthmax.Indicaciones = Nothing
        Me.txthmax.Location = New System.Drawing.Point(209, 296)
        Me.txthmax.Name = "txthmax"
        Me.txthmax.Size = New System.Drawing.Size(76, 22)
        Me.txthmax.SoloLectura = False
        Me.txthmax.TabIndex = 19
        Me.txthmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txthmax.Texto = "0"
        '
        'txtphmax
        '
        Me.txtphmax.Color = System.Drawing.Color.Empty
        Me.txtphmax.Decimales = True
        Me.txtphmax.Indicaciones = Nothing
        Me.txtphmax.Location = New System.Drawing.Point(209, 272)
        Me.txtphmax.Name = "txtphmax"
        Me.txtphmax.Size = New System.Drawing.Size(76, 22)
        Me.txtphmax.SoloLectura = False
        Me.txtphmax.TabIndex = 17
        Me.txtphmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtphmax.Texto = "0"
        '
        'txthmin
        '
        Me.txthmin.Color = System.Drawing.Color.Empty
        Me.txthmin.Decimales = True
        Me.txthmin.Indicaciones = Nothing
        Me.txthmin.Location = New System.Drawing.Point(80, 296)
        Me.txthmin.Name = "txthmin"
        Me.txthmin.Size = New System.Drawing.Size(76, 22)
        Me.txthmin.SoloLectura = False
        Me.txthmin.TabIndex = 18
        Me.txthmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txthmin.Texto = "0"
        '
        'txtphmin
        '
        Me.txtphmin.Color = System.Drawing.Color.Empty
        Me.txtphmin.Decimales = True
        Me.txtphmin.Indicaciones = Nothing
        Me.txtphmin.Location = New System.Drawing.Point(80, 272)
        Me.txtphmin.Name = "txtphmin"
        Me.txtphmin.Size = New System.Drawing.Size(76, 22)
        Me.txtphmin.SoloLectura = False
        Me.txtphmin.TabIndex = 16
        Me.txtphmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtphmin.Texto = "0"
        '
        'lblPrecioacordado
        '
        Me.lblPrecioacordado.AutoSize = True
        Me.lblPrecioacordado.Location = New System.Drawing.Point(428, 54)
        Me.lblPrecioacordado.Name = "lblPrecioacordado"
        Me.lblPrecioacordado.Size = New System.Drawing.Size(86, 13)
        Me.lblPrecioacordado.TabIndex = 63
        Me.lblPrecioacordado.Text = "Precio Acordado"
        '
        'cbxPrecioacordado
        '
        Me.cbxPrecioacordado.CampoWhere = Nothing
        Me.cbxPrecioacordado.CargarUnaSolaVez = False
        Me.cbxPrecioacordado.DataDisplayMember = Nothing
        Me.cbxPrecioacordado.DataFilter = Nothing
        Me.cbxPrecioacordado.DataOrderBy = Nothing
        Me.cbxPrecioacordado.DataSource = Nothing
        Me.cbxPrecioacordado.DataValueMember = Nothing
        Me.cbxPrecioacordado.dtSeleccionado = Nothing
        Me.cbxPrecioacordado.FormABM = Nothing
        Me.cbxPrecioacordado.Indicaciones = Nothing
        Me.cbxPrecioacordado.Location = New System.Drawing.Point(428, 70)
        Me.cbxPrecioacordado.Name = "cbxPrecioacordado"
        Me.cbxPrecioacordado.SeleccionMultiple = False
        Me.cbxPrecioacordado.SeleccionObligatoria = False
        Me.cbxPrecioacordado.Size = New System.Drawing.Size(123, 21)
        Me.cbxPrecioacordado.SoloLectura = False
        Me.cbxPrecioacordado.TabIndex = 6
        Me.cbxPrecioacordado.Texto = "Fijado"
        '
        'lblPromedio
        '
        Me.lblPromedio.AutoSize = True
        Me.lblPromedio.Location = New System.Drawing.Point(39, 249)
        Me.lblPromedio.Name = "lblPromedio"
        Me.lblPromedio.Size = New System.Drawing.Size(237, 13)
        Me.lblPromedio.TabIndex = 62
        Me.lblPromedio.Text = "Promedios Negociados (Agregar rango Min/Max)"
        '
        'lblBascula
        '
        Me.lblBascula.AutoSize = True
        Me.lblBascula.Location = New System.Drawing.Point(428, 196)
        Me.lblBascula.Name = "lblBascula"
        Me.lblBascula.Size = New System.Drawing.Size(45, 13)
        Me.lblBascula.TabIndex = 60
        Me.lblBascula.Text = "Bascula"
        '
        'cbxBascula
        '
        Me.cbxBascula.CampoWhere = Nothing
        Me.cbxBascula.CargarUnaSolaVez = False
        Me.cbxBascula.DataDisplayMember = Nothing
        Me.cbxBascula.DataFilter = Nothing
        Me.cbxBascula.DataOrderBy = Nothing
        Me.cbxBascula.DataSource = Nothing
        Me.cbxBascula.DataValueMember = Nothing
        Me.cbxBascula.dtSeleccionado = Nothing
        Me.cbxBascula.FormABM = Nothing
        Me.cbxBascula.Indicaciones = Nothing
        Me.cbxBascula.Location = New System.Drawing.Point(428, 211)
        Me.cbxBascula.Name = "cbxBascula"
        Me.cbxBascula.SeleccionMultiple = False
        Me.cbxBascula.SeleccionObligatoria = False
        Me.cbxBascula.Size = New System.Drawing.Size(123, 21)
        Me.cbxBascula.SoloLectura = False
        Me.cbxBascula.TabIndex = 9
        Me.cbxBascula.Texto = "ERSA"
        '
        'lblFacturacion
        '
        Me.lblFacturacion.AutoSize = True
        Me.lblFacturacion.Location = New System.Drawing.Point(428, 154)
        Me.lblFacturacion.Name = "lblFacturacion"
        Me.lblFacturacion.Size = New System.Drawing.Size(63, 13)
        Me.lblFacturacion.TabIndex = 58
        Me.lblFacturacion.Text = "Facturacion"
        '
        'cbxFacturacion
        '
        Me.cbxFacturacion.CampoWhere = Nothing
        Me.cbxFacturacion.CargarUnaSolaVez = False
        Me.cbxFacturacion.DataDisplayMember = Nothing
        Me.cbxFacturacion.DataFilter = Nothing
        Me.cbxFacturacion.DataOrderBy = Nothing
        Me.cbxFacturacion.DataSource = Nothing
        Me.cbxFacturacion.DataValueMember = Nothing
        Me.cbxFacturacion.dtSeleccionado = Nothing
        Me.cbxFacturacion.FormABM = Nothing
        Me.cbxFacturacion.Indicaciones = Nothing
        Me.cbxFacturacion.Location = New System.Drawing.Point(428, 168)
        Me.cbxFacturacion.Name = "cbxFacturacion"
        Me.cbxFacturacion.SeleccionMultiple = False
        Me.cbxFacturacion.SeleccionObligatoria = False
        Me.cbxFacturacion.Size = New System.Drawing.Size(123, 21)
        Me.cbxFacturacion.SoloLectura = False
        Me.cbxFacturacion.TabIndex = 8
        Me.cbxFacturacion.Texto = "Contado"
        '
        'lblCondicionEntrega
        '
        Me.lblCondicionEntrega.AutoSize = True
        Me.lblCondicionEntrega.Location = New System.Drawing.Point(428, 106)
        Me.lblCondicionEntrega.Name = "lblCondicionEntrega"
        Me.lblCondicionEntrega.Size = New System.Drawing.Size(109, 13)
        Me.lblCondicionEntrega.TabIndex = 56
        Me.lblCondicionEntrega.Text = "Condicion de Entrega"
        '
        'cbxCondicionEntrega
        '
        Me.cbxCondicionEntrega.CampoWhere = Nothing
        Me.cbxCondicionEntrega.CargarUnaSolaVez = False
        Me.cbxCondicionEntrega.DataDisplayMember = Nothing
        Me.cbxCondicionEntrega.DataFilter = Nothing
        Me.cbxCondicionEntrega.DataOrderBy = Nothing
        Me.cbxCondicionEntrega.DataSource = Nothing
        Me.cbxCondicionEntrega.DataValueMember = Nothing
        Me.cbxCondicionEntrega.dtSeleccionado = Nothing
        Me.cbxCondicionEntrega.FormABM = Nothing
        Me.cbxCondicionEntrega.Indicaciones = Nothing
        Me.cbxCondicionEntrega.Location = New System.Drawing.Point(428, 121)
        Me.cbxCondicionEntrega.Name = "cbxCondicionEntrega"
        Me.cbxCondicionEntrega.SeleccionMultiple = False
        Me.cbxCondicionEntrega.SeleccionObligatoria = False
        Me.cbxCondicionEntrega.Size = New System.Drawing.Size(123, 21)
        Me.cbxCondicionEntrega.SoloLectura = False
        Me.cbxCondicionEntrega.TabIndex = 7
        Me.cbxCondicionEntrega.Texto = "Puesto en Molino"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(11, 220)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(73, 13)
        Me.Label18.TabIndex = 55
        Me.Label18.Text = "Precio Credito"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(13, 177)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 13)
        Me.Label17.TabIndex = 54
        Me.Label17.Text = "Precio Acuerdo"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(297, 205)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(10, 13)
        Me.Label13.TabIndex = 53
        Me.Label13.Text = "-"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(242, 204)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(49, 13)
        Me.Label14.TabIndex = 51
        Me.Label14.Text = "Cantidad"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(163, 204)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(37, 13)
        Me.Label15.TabIndex = 49
        Me.Label15.Text = "Precio"
        '
        'txtCantidadCredito
        '
        Me.txtCantidadCredito.Color = System.Drawing.Color.Empty
        Me.txtCantidadCredito.Decimales = True
        Me.txtCantidadCredito.Indicaciones = Nothing
        Me.txtCantidadCredito.Location = New System.Drawing.Point(245, 220)
        Me.txtCantidadCredito.Name = "txtCantidadCredito"
        Me.txtCantidadCredito.Size = New System.Drawing.Size(73, 22)
        Me.txtCantidadCredito.SoloLectura = False
        Me.txtCantidadCredito.TabIndex = 15
        Me.txtCantidadCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadCredito.Texto = "0"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(93, 204)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(46, 13)
        Me.Label16.TabIndex = 47
        Me.Label16.Text = "Moneda"
        '
        'txtPrecioCredito
        '
        Me.txtPrecioCredito.Color = System.Drawing.Color.Empty
        Me.txtPrecioCredito.Decimales = True
        Me.txtPrecioCredito.Indicaciones = Nothing
        Me.txtPrecioCredito.Location = New System.Drawing.Point(166, 220)
        Me.txtPrecioCredito.Name = "txtPrecioCredito"
        Me.txtPrecioCredito.Size = New System.Drawing.Size(76, 22)
        Me.txtPrecioCredito.SoloLectura = False
        Me.txtPrecioCredito.TabIndex = 14
        Me.txtPrecioCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioCredito.Texto = "0"
        '
        'cbxMonedaCredito
        '
        Me.cbxMonedaCredito.CampoWhere = Nothing
        Me.cbxMonedaCredito.CargarUnaSolaVez = False
        Me.cbxMonedaCredito.DataDisplayMember = Nothing
        Me.cbxMonedaCredito.DataFilter = Nothing
        Me.cbxMonedaCredito.DataOrderBy = Nothing
        Me.cbxMonedaCredito.DataSource = Nothing
        Me.cbxMonedaCredito.DataValueMember = Nothing
        Me.cbxMonedaCredito.dtSeleccionado = Nothing
        Me.cbxMonedaCredito.FormABM = Nothing
        Me.cbxMonedaCredito.Indicaciones = Nothing
        Me.cbxMonedaCredito.Location = New System.Drawing.Point(95, 219)
        Me.cbxMonedaCredito.Name = "cbxMonedaCredito"
        Me.cbxMonedaCredito.SeleccionMultiple = False
        Me.cbxMonedaCredito.SeleccionObligatoria = False
        Me.cbxMonedaCredito.Size = New System.Drawing.Size(65, 21)
        Me.cbxMonedaCredito.SoloLectura = False
        Me.cbxMonedaCredito.TabIndex = 13
        Me.cbxMonedaCredito.Texto = "GS"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(314, 305)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(78, 13)
        Me.Label12.TabIndex = 46
        Me.Label12.Text = "Costo de Flete:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(463, 281)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 13)
        Me.Label10.TabIndex = 44
        Me.Label10.Text = "Precio"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(393, 281)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(46, 13)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "Moneda"
        '
        'txtCostoFlete
        '
        Me.txtCostoFlete.Color = System.Drawing.Color.Empty
        Me.txtCostoFlete.Decimales = True
        Me.txtCostoFlete.Indicaciones = Nothing
        Me.txtCostoFlete.Location = New System.Drawing.Point(466, 296)
        Me.txtCostoFlete.Name = "txtCostoFlete"
        Me.txtCostoFlete.Size = New System.Drawing.Size(76, 22)
        Me.txtCostoFlete.SoloLectura = False
        Me.txtCostoFlete.TabIndex = 25
        Me.txtCostoFlete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCostoFlete.Texto = "0"
        '
        'cbxMonedaFlete
        '
        Me.cbxMonedaFlete.CampoWhere = Nothing
        Me.cbxMonedaFlete.CargarUnaSolaVez = False
        Me.cbxMonedaFlete.DataDisplayMember = Nothing
        Me.cbxMonedaFlete.DataFilter = Nothing
        Me.cbxMonedaFlete.DataOrderBy = Nothing
        Me.cbxMonedaFlete.DataSource = Nothing
        Me.cbxMonedaFlete.DataValueMember = Nothing
        Me.cbxMonedaFlete.dtSeleccionado = Nothing
        Me.cbxMonedaFlete.FormABM = Nothing
        Me.cbxMonedaFlete.Indicaciones = Nothing
        Me.cbxMonedaFlete.Location = New System.Drawing.Point(395, 297)
        Me.cbxMonedaFlete.Name = "cbxMonedaFlete"
        Me.cbxMonedaFlete.SeleccionMultiple = False
        Me.cbxMonedaFlete.SeleccionObligatoria = False
        Me.cbxMonedaFlete.Size = New System.Drawing.Size(65, 21)
        Me.cbxMonedaFlete.SoloLectura = False
        Me.cbxMonedaFlete.TabIndex = 24
        Me.cbxMonedaFlete.Texto = "GS"
        '
        'lblUnidadMedida
        '
        Me.lblUnidadMedida.AutoSize = True
        Me.lblUnidadMedida.Location = New System.Drawing.Point(296, 163)
        Me.lblUnidadMedida.Name = "lblUnidadMedida"
        Me.lblUnidadMedida.Size = New System.Drawing.Size(10, 13)
        Me.lblUnidadMedida.TabIndex = 41
        Me.lblUnidadMedida.Text = "-"
        '
        'txtHastaEmbarque
        '
        Me.txtHastaEmbarque.AñoFecha = 0
        Me.txtHastaEmbarque.Color = System.Drawing.Color.Empty
        Me.txtHastaEmbarque.Fecha = New Date(2013, 2, 6, 9, 33, 15, 531)
        Me.txtHastaEmbarque.Location = New System.Drawing.Point(225, 439)
        Me.txtHastaEmbarque.MesFecha = 0
        Me.txtHastaEmbarque.Name = "txtHastaEmbarque"
        Me.txtHastaEmbarque.PermitirNulo = False
        Me.txtHastaEmbarque.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaEmbarque.SoloLectura = False
        Me.txtHastaEmbarque.TabIndex = 31
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(11, 442)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Periodo de embarque:"
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(9, 61)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(382, 53)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 4
        '
        'txtDesdeEmbarque
        '
        Me.txtDesdeEmbarque.AñoFecha = 0
        Me.txtDesdeEmbarque.Color = System.Drawing.Color.Empty
        Me.txtDesdeEmbarque.Fecha = New Date(2013, 2, 6, 9, 33, 15, 531)
        Me.txtDesdeEmbarque.Location = New System.Drawing.Point(138, 439)
        Me.txtDesdeEmbarque.MesFecha = 0
        Me.txtDesdeEmbarque.Name = "txtDesdeEmbarque"
        Me.txtDesdeEmbarque.PermitirNulo = False
        Me.txtDesdeEmbarque.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeEmbarque.SoloLectura = False
        Me.txtDesdeEmbarque.TabIndex = 30
        '
        'txtIDProducto
        '
        Me.txtIDProducto.Color = System.Drawing.Color.Empty
        Me.txtIDProducto.Decimales = True
        Me.txtIDProducto.Indicaciones = Nothing
        Me.txtIDProducto.Location = New System.Drawing.Point(11, 133)
        Me.txtIDProducto.Name = "txtIDProducto"
        Me.txtIDProducto.Size = New System.Drawing.Size(63, 22)
        Me.txtIDProducto.SoloLectura = True
        Me.txtIDProducto.TabIndex = 39
        Me.txtIDProducto.TabStop = False
        Me.txtIDProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIDProducto.Texto = "0"
        Me.txtIDProducto.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(241, 162)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Cantidad"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(162, 162)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Precio"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(314, 257)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(33, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Flete:"
        '
        'cbxTipoFlete
        '
        Me.cbxTipoFlete.CampoWhere = Nothing
        Me.cbxTipoFlete.CargarUnaSolaVez = False
        Me.cbxTipoFlete.DataDisplayMember = Nothing
        Me.cbxTipoFlete.DataFilter = Nothing
        Me.cbxTipoFlete.DataOrderBy = Nothing
        Me.cbxTipoFlete.DataSource = Nothing
        Me.cbxTipoFlete.DataValueMember = Nothing
        Me.cbxTipoFlete.dtSeleccionado = Nothing
        Me.cbxTipoFlete.FormABM = Nothing
        Me.cbxTipoFlete.Indicaciones = Nothing
        Me.cbxTipoFlete.Location = New System.Drawing.Point(390, 256)
        Me.cbxTipoFlete.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoFlete.Name = "cbxTipoFlete"
        Me.cbxTipoFlete.SeleccionMultiple = False
        Me.cbxTipoFlete.SeleccionObligatoria = False
        Me.cbxTipoFlete.Size = New System.Drawing.Size(161, 21)
        Me.cbxTipoFlete.SoloLectura = False
        Me.cbxTipoFlete.TabIndex = 23
        Me.cbxTipoFlete.Texto = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(13, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Producto:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = Nothing
        Me.txtCantidad.Location = New System.Drawing.Point(244, 178)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(73, 22)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 12
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(92, 162)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Moneda"
        '
        'txtZafra
        '
        Me.txtZafra.Color = System.Drawing.Color.Empty
        Me.txtZafra.Decimales = True
        Me.txtZafra.Indicaciones = Nothing
        Me.txtZafra.Location = New System.Drawing.Point(358, 17)
        Me.txtZafra.Name = "txtZafra"
        Me.txtZafra.Size = New System.Drawing.Size(63, 22)
        Me.txtZafra.SoloLectura = False
        Me.txtZafra.TabIndex = 2
        Me.txtZafra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtZafra.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(314, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Zafra:"
        '
        'txtPrecio
        '
        Me.txtPrecio.Color = System.Drawing.Color.Empty
        Me.txtPrecio.Decimales = True
        Me.txtPrecio.Indicaciones = Nothing
        Me.txtPrecio.Location = New System.Drawing.Point(165, 178)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(76, 22)
        Me.txtPrecio.SoloLectura = False
        Me.txtPrecio.TabIndex = 11
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecio.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(142, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nro Acuerdo:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Proveedor:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(340, 443)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 22
        Me.lblEstado.Text = "Estado:"
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(400, 441)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 32
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(461, 441)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 33
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(13, 20)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'lblVencimiento
        '
        Me.lblVencimiento.AutoSize = True
        Me.lblVencimiento.Location = New System.Drawing.Point(428, 20)
        Me.lblVencimiento.Name = "lblVencimiento"
        Me.lblVencimiento.Size = New System.Drawing.Size(40, 13)
        Me.lblVencimiento.TabIndex = 2
        Me.lblVencimiento.Text = "Fecha:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(40, 15)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(88, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 0
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 2, 6, 9, 33, 15, 531)
        Me.txtFecha.Location = New System.Drawing.Point(472, 17)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 3
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(94, 177)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(65, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 10
        Me.cbxMoneda.Texto = "GS"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(11, 416)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(70, 13)
        Me.lblDescripcion.TabIndex = 20
        Me.lblDescripcion.Text = "Observacion:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(92, 412)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(454, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 29
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtNroAcuerdo
        '
        Me.txtNroAcuerdo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroAcuerdo.Color = System.Drawing.Color.Empty
        Me.txtNroAcuerdo.Indicaciones = Nothing
        Me.txtNroAcuerdo.Location = New System.Drawing.Point(218, 17)
        Me.txtNroAcuerdo.Multilinea = False
        Me.txtNroAcuerdo.Name = "txtNroAcuerdo"
        Me.txtNroAcuerdo.Size = New System.Drawing.Size(90, 21)
        Me.txtNroAcuerdo.SoloLectura = False
        Me.txtNroAcuerdo.TabIndex = 1
        Me.txtNroAcuerdo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNroAcuerdo.Texto = ""
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(91, 112)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(296, 47)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 5
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'frmAcuerdo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1060, 527)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmAcuerdo"
        Me.Text = "frmAcuerdo"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblVencimiento As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtNroAcuerdo As ERP.ocxTXTString
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtPrecio As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtZafra As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoFlete As ERP.ocxCBX
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtIDProducto As ERP.ocxTXTNumeric
    Friend WithEvents txtHastaEmbarque As ERP.ocxTXTDate
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDesdeEmbarque As ERP.ocxTXTDate
    Friend WithEvents lblUnidadMedida As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCostoFlete As ERP.ocxTXTNumeric
    Friend WithEvents cbxMonedaFlete As ERP.ocxCBX
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents txtCantidadCredito As ocxTXTNumeric
    Friend WithEvents Label16 As Label
    Friend WithEvents txtPrecioCredito As ocxTXTNumeric
    Friend WithEvents cbxMonedaCredito As ocxCBX
    Friend WithEvents lblBascula As Label
    Friend WithEvents cbxBascula As ocxCBX
    Friend WithEvents lblFacturacion As Label
    Friend WithEvents cbxFacturacion As ocxCBX
    Friend WithEvents lblCondicionEntrega As Label
    Friend WithEvents cbxCondicionEntrega As ocxCBX
    Friend WithEvents lblPrecioacordado As Label
    Friend WithEvents cbxPrecioacordado As ocxCBX
    Friend WithEvents lblPromedio As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents txtfn As ocxTXTNumeric
    Friend WithEvents txtgluten As ocxTXTNumeric
    Friend WithEvents txthmax As ocxTXTNumeric
    Friend WithEvents txtphmax As ocxTXTNumeric
    Friend WithEvents txthmin As ocxTXTNumeric
    Friend WithEvents txtphmin As ocxTXTNumeric
    Friend WithEvents Label25 As Label
    Friend WithEvents cbxtabladescuento As ocxCBX
    Friend WithEvents Label26 As Label
    Friend WithEvents txtimpurezas As ocxTXTNumeric
    Friend WithEvents txtcompradorint As ocxTXTString
    Friend WithEvents txtproveedorint As ocxTXTString
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents lblintervinientes As Label
End Class

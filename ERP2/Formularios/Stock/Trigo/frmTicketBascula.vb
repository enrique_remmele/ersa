﻿Public Class frmTicketBascula
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    '   Dim CAsiento As New CAsientoTicketBascula
    Dim CData As New CData
    Dim vDecimalOperacion As Boolean
    'Dim CReporte As New CRepo

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim dt As DataTable
    Dim dtGastoAdicional As DataTable
    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Conectar()
        txtProveedor.Conectar()
        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
        'OcxCotizacion1.Inicializar()

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "TICKET DE BASCULA", "TIBA")
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        CargarInformacion()

        'Clases
        '    CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        txtFechaTicket.Hoy()
        CargarOperacion()
        txtAcuerdo.Focus()
        BloquearCostos()
    End Sub

    Sub CargarInformacion()
        ReDim vControles(-1)
        'CSistema.CargaControl(vControles, cbxSucursal)
        'CSistema.CargaControl(vControles, txtAcuerdo)
        CSistema.CargaControl(vControles, txtFechaTicket)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtRemision)
        CSistema.CargaControl(vControles, txtRecepcion)
        'CSistema.CargaControl(vControles, cbxTipoFlete)
        CSistema.CargaControl(vControles, cbxDeposito)
        'CSistema.CargaControl(vControles, cbxCamion)
        CSistema.CargaControl(vControles, cbxChapa)
        CSistema.CargaControl(vControles, cbxChofer)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtProveedor)
        CSistema.CargaControl(vControles, txtPesoRemision)
        CSistema.CargaControl(vControles, txtCantidadBascula)
        'CSistema.CargaControl(vControles, txtCostoUnitario)
        'CSistema.CargaControl(vControles, txtCostoUnitarioUs)
        'CSistema.CargaControl(vControles, cbxUnidad)


        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()


        'CARGAR UNIDAD
        'Sucursales
        'cbxUnidad.Texto = "TONELADAS"

        'Orden en forma
        cbxTipoFlete.cbx.Items.Add("INCLUIDO")
        cbxTipoFlete.cbx.Items.Add("TERCERIZADO")
        cbxTipoFlete.cbx.Items.Add("PROPIO")
        cbxTipoFlete.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Sucursales
        CSistema.SqlToComboBox(cbxDeposito.cbx, "Select ID, Deposito From VDeposito Order By 2")

        'Moneda
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Descripcion From vMoneda Order By 1")

        'Chofer
        CSistema.SqlToComboBox(cbxChofer.cbx, "Select ID, concat(Nombres,'- ',nrodocumento)  From ChoferProveedor Order By 2")
        'Chapa
        CSistema.SqlToComboBox(cbxChapa.cbx, "Select ID, Patente From CamionProveedor Order By 2")
        'camion
        CSistema.SqlToComboBox(cbxCamion.cbx, "Select ID, Descripcion From CamionProveedor Order By 2")

        'Tipo de Operaciones
        'CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion=" & IDOperacion)

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo Operacion
        cbxSucursal.SelectedValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "0"))

        'Tipo Operacion
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO OPERACION", "")

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Deposito de Origen
        cbxDeposito.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO", "")

        'Unidad
        cbxUnidad.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", "UNIDAD")

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From TicketBascula),1)"), Integer)

    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.GetValue)

        'Tipo Operacion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO OPERACION", cbxSucursal.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Deposito de Origen
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO", cbxDeposito.cbx.Text)

        'Unidad
        CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", cbxUnidad.Text)

    End Sub

    Sub Nuevo()
        'CARGAR ESTRUCTURA DEL DETALLE
        dt = CSistema.ExecuteToDataTable("Select Top(0) * From vTicketBascula").Clone
        dtGastoAdicional = CSistema.ExecuteToDataTable("Select Top(0) * From VTicketBasculaGastoAdicional").Clone
        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        'Tipo de Comprobante
        'CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where Estado = 1 and IDOperacion=" & IDOperacion)
        'Limpiar detalle

        dtGastoAdicional.Clear()
        'LimpiarGastoAdicional()

        dt.Rows.Clear()
        dtGastoAdicional.Rows.Clear()
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dt)

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        'cbxSucursal.cbx.Text = ""
        cbxTipoComprobante.cbx.Text = ""
        txtComprobante.txt.Clear()
        cbxDeposito.cbx.Text = ""
        cbxDeposito.Texto = ""
        txtProveedor.LimpiarSeleccion()
        txtObservacion.txt.Clear()
        'txtTotal.txt.ResetText()
        txtAcuerdo.txt.Clear()
        txtCotizacionUs.txt.Text = "0"
        txtRemision.txt.Text = ""
        txtRecepcion.txt.Text = ""

        ObtenerCotizacion()
        'CAsiento.Limpiar()
        'CAsiento.Inicializar()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM vTicketBascula ),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en la fecha
        txtAcuerdo.txt.Focus()
        txtFechaTicket.Hoy()
        ObtenerCotizacion()
        'Sucursales
        CSistema.SqlToComboBox(cbxDeposito.cbx, "Select ID, Deposito From VDeposito Where IDTipoDeposito = 6 Order By 2") ' Tipo de Deposito Silo
    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VTicketBascula ),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub
    Sub ObtenerCotizacion()
        If vNuevo = True Then
            Dim vFecha As String = ""
            vFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
            If vFecha.Length = 8 Then
                txtCotizacionUs.txt.Text = CType(CSistema.ExecuteScalar("select Isnull((Select cotizacion from cotizacion where IDMoneda = 2 and cast(fecha as date) = '" & vFecha & "'),1)"), Integer)
            End If
            If txtCotizacionUs.ObtenerValor = 1 Then
                MessageBox.Show("Cargar cotizacion del dia!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Cancelar()
            End If
        End If
    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ''Validar el Asiento
        'If CAsiento.ObtenerSaldo <> 0 Then
        '    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        'If CAsiento.ObtenerTotal = 0 Then
        '    CSistema.MostrarError("El asiento no es correcto!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
        '    Exit Function
        'End If

        Return True

    End Function
    Sub MostrarDecimales(ByVal IDMoneda As Integer)
        txtCostoUnitario.Decimales = False
        txtCostoUnitarioUs.Decimales = True
        txtCantidadBascula.Decimales = True
        txtPesoRemision.Decimales = True
    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()
        '    GenerarAsiento()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(txtCotizacionUs.txt.Text, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProducto", CSistema.dtSumColumn(dt, "IDProducto"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDImpuesto", CSistema.dtSumColumn(dt, "IDImpuesto"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PesoRemision", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "PesoRemision"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PesoBascula", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "PesoBascula"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Diferencia", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "Diferencia"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PrecioUnitario", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "PrecioUnitario"), False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PrecioUnitarioUS", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "PrecioUnitarioUS"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "Total"), False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalUS", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalUS"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalImpuesto"), False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalImpuestoUS", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalImpuestoUS"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalDiscriminado"), False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminadoUS", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dt, "TotalDiscriminadoUS"), True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoFlete", cbxTipoFlete.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDChofer", cbxChofer.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCamion", cbxCamion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroRemision", txtRemision.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroRecepcion", txtRecepcion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroAcuerdo", txtAcuerdo.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.GetValue, ParameterDirection.Input)
        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida

        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "spTicketBascula", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro

            Exit Sub

        End If

        If IDTransaccion > 0 Then
            'Cargamos el DetalleImpuesto
            CDetalleImpuesto.Guardar(IDTransaccion)

            ''Si es nuevo
            'If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
            '    'Cargamos el asiento
            '    CAsiento.IDTransaccion = IDTransaccion
            '    CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
            'End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub
    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

    End Sub

    Sub CargarProducto()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If cbxSucursal.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(cbxSucursal, mensaje)
            ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxSucursal.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(cbxSucursal, mensaje)
            ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Cantidad
        If IsNumeric(txtPesoRemision.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad en remision no es correcta!"
            ctrError.SetError(txtPesoRemision, mensaje)
            ctrError.SetIconAlignment(txtPesoRemision, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If IsNumeric(txtCantidadBascula.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad en bascula no es correcta!"
            ctrError.SetError(txtCantidadBascula, mensaje)
            ctrError.SetIconAlignment(txtCantidadBascula, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        If CDec(txtPesoRemision.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad en remision no es correcta!"
            ctrError.SetError(txtPesoRemision, mensaje)
            ctrError.SetIconAlignment(txtPesoRemision, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If
        If CDec(txtCantidadBascula.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad en bascula no es correcta!"
            ctrError.SetError(txtCantidadBascula, mensaje)
            ctrError.SetIconAlignment(txtCantidadBascula, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        ' Insertar
        'Cargamos el registro en el detalle
        Dim dRow As DataRow = dt.NewRow()

        'Obtener Valores
        dRow("IDProducto") = txtProducto.Registro("ID").ToString
        dRow("ReferenciaProducto") = txtProducto.Registro("Ref").ToString
        'dRow("ID") = dt.Rows.Count
        dRow("Producto") = txtProducto.Registro("Descripcion").ToString

        If Not cbxDeposito.cbx.SelectedValue Is Nothing Then
            dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue
        End If

        dRow("PesoBascula") = txtCantidadBascula.ObtenerValor
        dRow("PesoRemision") = txtPesoRemision.ObtenerValor
        dRow("Diferencia") = txtCantidadBascula.ObtenerValor - txtPesoRemision.ObtenerValor

        'Impuestos
        dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
        dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString

        'Totales
        dRow("PrecioUnitario") = txtCostoUnitario.ObtenerValor
        dRow("PrecioUnitarioUS") = txtCostoUnitarioUs.ObtenerValor

        dRow("Total") = txtCantidadBascula.ObtenerValor * CDec(txtCostoUnitario.ObtenerValor)
        dRow("TotalUS") = txtCantidadBascula.ObtenerValor * CDec(txtCostoUnitarioUs.ObtenerValor)

        dRow("TotalImpuesto") = 0
        dRow("TotalImpuestoUS") = 0
        dRow("TotalDiscriminado") = 0
        dRow("TotalDiscriminadoUS") = 0
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))
        CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("TotalUS").ToString), True, True, dRow("TotalDiscriminadoUS"), dRow("TotalImpuestoUS"))

        'Agregamos al detalle
        dt.Rows.Add(dRow)
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dt, False)

        'Inicializamos los valores
        txtCantidadBascula.txt.Text = "0"
        txtPesoRemision.txt.Text = "0"
        txtCostoUnitario.txt.Text = "0"
        txtCostoUnitarioUs.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub
    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dt.Rows(dgw.SelectedRows(0).Index).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dt.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dt, False)
        CargarAcuerdo()
    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro


        CSistema.dtToGrid(dgw, dt)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("ReferenciaProducto").DisplayIndex = 1
        dgw.Columns("Producto").DisplayIndex = 2
        dgw.Columns("PesoRemision").DisplayIndex = 3
        dgw.Columns("PesoBascula").DisplayIndex = 4
        dgw.Columns("Diferencia").DisplayIndex = 5
        dgw.Columns("PrecioUnitario").DisplayIndex = 6
        dgw.Columns("PrecioUnitarioUS").DisplayIndex = 7
        dgw.Columns("Total").DisplayIndex = 8
        dgw.Columns("TotalUS").DisplayIndex = 9

        dgw.Columns("ReferenciaProducto").Visible = True
        dgw.Columns("Producto").Visible = True
        dgw.Columns("PesoRemision").Visible = True
        dgw.Columns("PrecioUnitario").Visible = vgVerCosto
        dgw.Columns("Total").Visible = vgVerCosto
        dgw.Columns("PrecioUnitarioUS").Visible = vgVerCosto
        dgw.Columns("TotalUS").Visible = vgVerCosto
        dgw.Columns("PesoBascula").Visible = True
        dgw.Columns("Diferencia").Visible = True

        dgw.Columns("Producto").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("PesoRemision").DefaultCellStyle.Format = "N2"
        dgw.Columns("PesoBascula").DefaultCellStyle.Format = "N2"
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"
        dgw.Columns("PrecioUnitarioUS").DefaultCellStyle.Format = "N3"
        dgw.Columns("TotalUS").DefaultCellStyle.Format = "N3"

        dgw.Columns("ReferenciaProducto").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("PesoRemision").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PesoBascula").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Diferencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitarioUS").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("TotalUS").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("PrecioUnitario").HeaderText = "Costo"
        dgw.Columns("PrecioUnitarioUS").HeaderText = "Costo US"
        dgw.Columns("PesoRemision").HeaderText = "Remision"
        dgw.Columns("PesoBascula").HeaderText = "Bascula"
        dgw.Columns("Total").HeaderText = "Total"
        dgw.Columns("TotalUS").HeaderText = "Total US"
        dgw.Columns("ReferenciaProducto").HeaderText = "Producto"
        dgw.Columns("Producto").HeaderText = "Descripcion"


        Total = CSistema.dtSumColumn(dt, "TotalGs")


        'Bloqueamos la cabecera si corresponde
        If dt.Rows.Count > 0 And vNuevo = True Then
            cbxSucursal.cbx.Enabled = False
            cbxDeposito.cbx.Enabled = False
            txtFechaTicket.Enabled = False
            txtCantidadBascula.Enabled = False
            txtPesoRemision.Enabled = False
        Else
            cbxSucursal.cbx.Enabled = True
            cbxSucursal.cbx.SelectedValue = cbxSucursal.GetValue
            cbxDeposito.cbx.Enabled = True
            txtFechaTicket.Enabled = True
            txtCantidadBascula.Enabled = True
            txtPesoRemision.Enabled = True
        End If

        If dt.Rows.Count > 0 Then
            txtProducto.Enabled = False
            txtPesoRemision.Enabled = False
            txtCantidadBascula.Enabled = False
        Else
            txtProducto.Enabled = True
            txtPesoRemision.Enabled = True
            txtCantidadBascula.Enabled = True
        End If


    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpTicketBascula", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Sub Imprimir()

        Dim CTicketBascula As New Reporte.CReporteTicketBascula
        CTicketBascula.TicketBascula(IDTransaccion)

    End Sub

    Sub Buscar()

        'EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        ''Otros
        'lblAnulado.Visible = False

        'Dim frm As New frmConsultaMovimiento
        'frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        'frm.WindowState = FormWindowState.Normal
        'frm.StartPosition = FormStartPosition.CenterScreen

        'FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        'If frm.IDTransaccion > 0 Then
        '    CargarOperacion(frm.IDTransaccion)
        'End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VTicketBascula where Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'dt.Clear()

        dt = CSistema.ExecuteToDataTable("Select * From vTicketBascula Where IDTransaccion=" & IDTransaccion)
        dtGastoAdicional = CSistema.ExecuteToDataTable("Select * From VTicketBasculaGastoAdicional Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)


        txtID.txt.Text = oRow("Numero").ToString
        txtFechaTicket.SetValueFromString(CDate(oRow("Fecha").ToString))
        'cbxSucursal.Texto = oRow("ReferenciaSucursal").ToString
        cbxTipoComprobante.Texto = oRow("ReferenciaTipoComprobante").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxDeposito.cbx.Text = oRow("Deposito").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        'txtTotal.txt.Text = oRow("Total").ToString
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        txtAcuerdo.txt.Text = oRow("NroAcuerdo").ToString

        cbxTipoFlete.Texto = oRow("TipoFlete").ToString
        cbxChofer.Texto = oRow("Chofer").ToString
        cbxChapa.Texto = oRow("Chapa").ToString
        cbxCamion.Texto = oRow("Camion").ToString
        txtRemision.txt.Text = oRow("NroRemision").ToString
        txtRecepcion.txt.Text = oRow("NroRecepcion").ToString

        'Moneda
        'OcxCotizacion1.cbxMoneda.txt.Text = oRow("ReferenciaMoneda")
        'OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")
        'OcxCotizacion1.SetValue(oRow("ReferenciaMoneda").ToString, oRow("Cotizacion").ToString)
        'OcxCotizacion1.cbxMoneda.txt.Text = oRow("ReferenciaMoneda")
        'OcxCotizacion1.cbxMoneda.cbx.SelectedValue = oRow("IDMoneda")
        cbxMoneda.Texto = oRow("Moneda").ToString
        txtCotizacionUs.txt.Text = oRow("Cotizacion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        'lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString
        lblUsuarioRegistro.Text = oRow("Usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dt, False)


        'Inicializamos el Asiento
        '  CAsiento.Limpiar()

    End Sub
    Sub CargarAcuerdo()
        ctrError.Clear()
        tsslEstado.Text = ""
        Dim dtAcuerdo As DataTable

        If txtAcuerdo.txt.Text = "" Then
            Dim mensaje As String = "Ingrese Nro de Comprobante del Acuerdo!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtAcuerdo = CSistema.ExecuteToDataTable("Select * From vAcuerdo Where NroAcuerdo = '" & txtAcuerdo.txt.Text & "'")

        'Cargamos la cabecera
        If dtAcuerdo Is Nothing Or dtAcuerdo.Rows.Count = 0 Then
            Dim mensaje As String = "El Acuerdo no existe."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtAcuerdo, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dtAcuerdo.Rows(0)
        txtProveedor.SetValue(oRow("IDProveedor").ToString)
        cbxTipoFlete.Texto = oRow("TipoFlete").ToString
        'Moneda
        cbxMoneda.Texto = oRow("Moneda").ToString
        If oRow("IDMoneda").ToString = 1 Then
            txtCostoUnitario.txt.Text = oRow("Precio").ToString
            txtCostoUnitarioUs.txt.Text = CSistema.FormatoMoneda3Decimales((oRow("Precio").ToString / CSistema.FormatoMonedaBaseDatos(txtCotizacionUs.txt.Text, False)), True)
        End If
        If oRow("IDMoneda").ToString <> 1 Then
            txtCostoUnitarioUs.txt.Text = CSistema.FormatoMoneda(oRow("Precio").ToString, True)
            txtCostoUnitario.txt.Text = CSistema.FormatoMoneda3Decimales((oRow("Precio").ToString * CSistema.FormatoMonedaBaseDatos(txtCotizacionUs.txt.Text, False)), False)
        End If
        txtProducto.SetValue(oRow("IDProducto").ToString)
        txtProducto.txt.Texto = oRow("Producto").ToString
        cbxUnidad.Texto = oRow("UnidadMedida").ToString
        'cbxUnidad.Texto = "TONELADAS"


        'Cargamos el detalle
        'ListarDetalle()
        'CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos el Asiento
        'CAsiento.Limpiar()
        dtAcuerdo.Clear()
    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        Dim Comprobante As String = cbxTipoComprobante.cbx.Text & ": " & txtComprobante.txt.Text

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante
            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From vTicketBascula Where Numero=" & txtID.ObtenerValor & "), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)


        Else

            If cbxSucursal.cbx.SelectedValue Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente la sucursal de operacion!"
                ctrError.SetError(cbxSucursal, mensaje)
                ctrError.SetIconAlignment(cbxSucursal, ErrorIconAlignment.TopLeft)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            Dim frm As New frmAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.SizableToolWindow
            frm.Text = Comprobante

            '     GenerarAsiento()

            'frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            'CAsiento.ListarDetalle(frm.dgv)
            frm.CalcularTotales()

            '   frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            'Mostramos
            frm.ShowDialog(Me)

            ''Actualizamos el asiento si es que este tuvo alguna modificacion
            'CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            'CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            'CAsiento.Bloquear = frm.CAsiento.Bloquear
            'CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            'CAsiento.NroCaja = frm.CAsiento.NroCaja
            'CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            'If frm.VolverAGenerar = True Then
            '    CAsiento.Generado = False
            '    CAsiento.dtAsiento.Clear()
            '    CAsiento.dtDetalleAsiento.Clear()
            '    VisualizarAsiento()
            'End If

        End If

    End Sub

    'Sub GenerarAsiento()

    '    'EstablecerCabecera
    '    Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

    '    oRow("IDCiudad") = CData.GetRow("ID=" & cbxSucursal.GetValue, "VSucursal")("IDCiudad")
    '    oRow("IDSucursal") = cbxSucursal.cbx.SelectedValue
    '    oRow("Fecha") = txtFechaTicket.GetValue

    '    'Dim CuentaRow As DataRow = CData.GetRow("ID=" & cbxCuenta.GetValue, dtCuentaBancaria)

    '    oRow("IDMoneda") = cbxMoneda.GetValue
    '    If cbxMoneda.GetValue = 1 Then
    '        oRow("Cotizacion") = 1
    '    Else
    '        oRow("Cotizacion") = txtCotizacionUs.ObtenerValor
    '    End If
    '    oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
    '    oRow("NroComprobante") = txtComprobante.txt.Text
    '    oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
    '    oRow("Detalle") = txtProveedor.Registro("RazonSocial").ToString & "-" & cbxTipoComprobante.cbx.Text & "-" & txtComprobante.txt.Text & " --- " & txtObservacion.txt.Text
    '    If cbxMoneda.GetValue = 1 Then
    '        oRow("Total") = CSistema.dtSumColumn(dt, "Total")
    '    Else
    '        oRow("Total") = CSistema.dtSumColumn(dt, "TotalUS")
    '    End If
    '    CAsiento.dtAsiento.Rows.Clear()
    '    CAsiento.dtAsiento.Rows.Add(oRow)

    '    CAsiento.IDTipoProducto = txtProducto.Registro("IDTipoProducto").ToString
    '    If cbxMoneda.GetValue = 1 Then
    '        CAsiento.Total = CSistema.dtSumColumn(dt, "Total")
    '    Else
    '        CAsiento.Total = CSistema.dtSumColumn(dt, "TotalUS")
    '    End If
    '    CAsiento.IDSucursal = cbxSucursal.GetValue
    '    CAsiento.Generar()

    'End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VTicketBascula Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VTicketBascula Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Sub BloquearCostos()
        txtCostoUnitario.Visible = vgVerCosto
        txtCostoUnitarioUs.Visible = vgVerCosto
        Label7.Visible = vgVerCosto
        Label9.Visible = vgVerCosto
    End Sub
    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click, Button8.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click, Button7.Click
        Imprimir()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click, Button4.Click
        Anular()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub btnAsiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub



    'Private Sub OcxCotizacion1_CambioMoneda()
    '    OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
    '    OcxCotizacion1.Recargar()
    '    MostrarDecimales(OcxCotizacion1.Registro("ID"))
    '    vDecimalOperacion = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & OcxCotizacion1.Registro("ID"), "vMoneda")("Decimales").ToString)
    '    txtProducto.IDMoneda = OcxCotizacion1.Registro("ID")
    '    txtProducto.Cotizacion = OcxCotizacion1.Registro("Cotizacion")
    'End Sub

    Private Sub txtFechaTicket_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtFechaTicket.TeclaPrecionada
        'OcxCotizacion1.FiltroFecha = CSistema.FormatoFechaBaseDatos(txtFechaTicket.txt.Text, True, False)
        'OcxCotizacion1.Recargar()
        ObtenerCotizacion()
    End Sub


    Private Sub frmTicketBascula_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmTicketBascula_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub txtCostoUnitario_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCostoUnitario.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub txtCostoUnitarioUs_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCostoUnitarioUs.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub txtCantidadBascula_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCantidadBascula.TeclaPrecionada
        If txtCostoUnitario.txt.Text <> "" Then
            If e.KeyCode = Keys.Enter Then
                If txtCostoUnitario.SoloLectura = True Then
                    CargarProducto()
                End If
            End If
        End If
    End Sub

    Private Sub frmTicketBascula_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(sender As System.Object, e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            cbxUnidad.Focus()
            cbxUnidad.Texto = txtProducto.Registro("Uni. Med.")
        End If
    End Sub

    Private Sub txtAcuerdo_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtAcuerdo.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarAcuerdo()
        End If
    End Sub

    Private Sub txtFechaTicket_Validated(sender As System.Object, e As System.EventArgs) Handles txtFechaTicket.Validated
        ObtenerCotizacion()
    End Sub


    Private Sub cbxChapa_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxChapa.PropertyChanged
        cbxCamion.cbx.DataSource = Nothing
        If cbxChapa.Validar = False Then
            Exit Sub
        End If
        CSistema.SqlToComboBox(cbxCamion.cbx, CData.GetTable("vCamionProveedor", " ID = " & cbxChapa.GetValue), "ID", "Descripcion")
        'Chofer
        CSistema.SqlToComboBox(cbxChofer.cbx, "Select ID, concat(Nombres,'-',nrodocumento)  From ChoferProveedor where IDCamion = " & cbxChapa.GetValue & " Order By 2")
        cbxChofer.Texto = CType(CSistema.ExecuteScalar("Select top(1) Nombres from ChoferProveedor where IDCamion = " & cbxChapa.GetValue), String)
    End Sub

    Private Sub btnRecargarChoferes_Click(sender As System.Object, e As System.EventArgs) Handles btnRecargarChoferes.Click
        'Chofer
        CSistema.SqlToComboBox(cbxChofer.cbx, "Select ID, concat(Nombres,'-',nrodocumento)  From ChoferProveedor Order By 2")
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        Dim frm As New frmTicketdeBasculaModificar

        frm.Recepcion = txtRecepcion.txt.Text
        frm.Remision = txtRemision.txt.Text
        frm.Observacion = txtObservacion.txt.Text
        frm.IDTransaccion = IDTransaccion
        frm.IDOperacion = IDOperacion

        FGMostrarFormulario(Me, frm, "Modificar Ticket de Bascula", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True)
        CargarOperacion()

    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmTicketBascula_Activate()
        Me.Refresh()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMacheoFacturaTicket
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNumero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPesoRemision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colPesoBascula = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporteUs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.bntAgregarFactura = New System.Windows.Forms.Button()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblMonedaNotaCredito = New System.Windows.Forms.Label()
        Me.lblFechaNotaCredito = New System.Windows.Forms.Label()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregarTicket = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.txtDiferenciaImporte = New ERP.ocxTXTNumeric()
        Me.txtDiferenciaCantidad = New ERP.ocxTXTNumeric()
        Me.txtTotalUs = New ERP.ocxTXTNumeric()
        Me.txtTotalGs = New ERP.ocxTXTNumeric()
        Me.txtTotalBascula = New ERP.ocxTXTNumeric()
        Me.txtAcuerdo = New ERP.ocxTXTString()
        Me.txtProveedor = New ERP.ocxTXTString()
        Me.txtPrecio = New ERP.ocxTXTNumeric()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.txtProducto = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtTotalRemision = New ERP.ocxTXTNumeric()
        Me.txtCantidadFactura = New ERP.ocxTXTNumeric()
        Me.txtTotalFactura = New ERP.ocxTXTNumeric()
        Me.txtFechaFactura = New ERP.ocxTXTString()
        Me.txtCotizacionFactura = New ERP.ocxTXTNumeric()
        Me.txtComprobanteFactura = New ERP.ocxTXTString()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCabecera.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 504)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(612, 22)
        Me.StatusStrip1.TabIndex = 44
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(528, 477)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(453, 477)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(378, 477)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(303, 477)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 6
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(5, 74)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 15
        Me.lblMoneda.Text = "Moneda:"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colIDTransaccion, Me.colNumero, Me.colFecha, Me.colCotizacion, Me.colPesoRemision, Me.colPesoBascula, Me.colImporte, Me.colImporteUs})
        Me.DataGridView1.Location = New System.Drawing.Point(1, 244)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(602, 134)
        Me.DataGridView1.TabIndex = 34
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        '
        'colNumero
        '
        Me.colNumero.HeaderText = "Comprobante"
        Me.colNumero.Name = "colNumero"
        Me.colNumero.ReadOnly = True
        Me.colNumero.Width = 80
        '
        'colFecha
        '
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = "01/01/2017"
        Me.colFecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.colFecha.HeaderText = "Fecha"
        Me.colFecha.MaxInputLength = 10
        Me.colFecha.Name = "colFecha"
        Me.colFecha.ReadOnly = True
        Me.colFecha.Width = 80
        '
        'colCotizacion
        '
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.colCotizacion.DefaultCellStyle = DataGridViewCellStyle2
        Me.colCotizacion.HeaderText = "Cotizacion"
        Me.colCotizacion.Name = "colCotizacion"
        Me.colCotizacion.ReadOnly = True
        Me.colCotizacion.Width = 80
        '
        'colPesoRemision
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = "0"
        Me.colPesoRemision.DefaultCellStyle = DataGridViewCellStyle3
        Me.colPesoRemision.HeaderText = "Remision"
        Me.colPesoRemision.Name = "colPesoRemision"
        Me.colPesoRemision.ReadOnly = True
        Me.colPesoRemision.Width = 90
        '
        'colPesoBascula
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = "0"
        Me.colPesoBascula.DefaultCellStyle = DataGridViewCellStyle4
        Me.colPesoBascula.HeaderText = "Bascula"
        Me.colPesoBascula.Name = "colPesoBascula"
        Me.colPesoBascula.ReadOnly = True
        Me.colPesoBascula.Width = 90
        '
        'colImporte
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = "0"
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle5
        Me.colImporte.HeaderText = "Importe GS"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        '
        'colImporteUs
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = "0"
        Me.colImporteUs.DefaultCellStyle = DataGridViewCellStyle6
        Me.colImporteUs.HeaderText = "Importe US"
        Me.colImporteUs.Name = "colImporteUs"
        Me.colImporteUs.ReadOnly = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(1, 477)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(75, 23)
        Me.btnBusquedaAvanzada.TabIndex = 7
        Me.btnBusquedaAvanzada.Text = "&Busqueda"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(462, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Fecha:"
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtAcuerdo)
        Me.gbxCabecera.Controls.Add(Me.Label9)
        Me.gbxCabecera.Controls.Add(Me.txtProveedor)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.txtPrecio)
        Me.gbxCabecera.Controls.Add(Me.Label7)
        Me.gbxCabecera.Controls.Add(Me.cbxMoneda)
        Me.gbxCabecera.Controls.Add(Me.lblMoneda)
        Me.gbxCabecera.Controls.Add(Me.txtCotizacion)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.Label6)
        Me.gbxCabecera.Controls.Add(Me.txtProducto)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(2, 3)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(601, 125)
        Me.gbxCabecera.TabIndex = 27
        Me.gbxCabecera.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(153, 46)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Proveedor:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Acuerdo:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(162, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Precio:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(266, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Producto:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(5, 101)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 0
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(180, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(32, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Suc.:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(281, 16)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(40, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comp.:"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(5, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'bntAgregarFactura
        '
        Me.bntAgregarFactura.Location = New System.Drawing.Point(164, 14)
        Me.bntAgregarFactura.Name = "bntAgregarFactura"
        Me.bntAgregarFactura.Size = New System.Drawing.Size(71, 21)
        Me.bntAgregarFactura.TabIndex = 3
        Me.bntAgregarFactura.Text = "Seleccionar"
        Me.bntAgregarFactura.UseVisualStyleBackColor = True
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(421, 42)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 10
        Me.lblTotal.Text = "Total:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(2, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Comprobante:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(178, 387)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "TOTALES:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtCantidadFactura)
        Me.GroupBox1.Controls.Add(Me.txtTotalFactura)
        Me.GroupBox1.Controls.Add(Me.txtFechaFactura)
        Me.GroupBox1.Controls.Add(Me.bntAgregarFactura)
        Me.GroupBox1.Controls.Add(Me.lblTotal)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblMonedaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtCotizacionFactura)
        Me.GroupBox1.Controls.Add(Me.lblFechaNotaCredito)
        Me.GroupBox1.Controls.Add(Me.txtComprobanteFactura)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 134)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(601, 66)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Factura"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(239, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Cantidad:"
        '
        'lblMonedaNotaCredito
        '
        Me.lblMonedaNotaCredito.AutoSize = True
        Me.lblMonedaNotaCredito.Location = New System.Drawing.Point(2, 43)
        Me.lblMonedaNotaCredito.Name = "lblMonedaNotaCredito"
        Me.lblMonedaNotaCredito.Size = New System.Drawing.Size(59, 13)
        Me.lblMonedaNotaCredito.TabIndex = 6
        Me.lblMonedaNotaCredito.Text = "Cotizacion:"
        '
        'lblFechaNotaCredito
        '
        Me.lblFechaNotaCredito.AutoSize = True
        Me.lblFechaNotaCredito.Location = New System.Drawing.Point(251, 19)
        Me.lblFechaNotaCredito.Name = "lblFechaNotaCredito"
        Me.lblFechaNotaCredito.Size = New System.Drawing.Size(40, 13)
        Me.lblFechaNotaCredito.TabIndex = 5
        Me.lblFechaNotaCredito.Text = "Fecha:"
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(226, 477)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 2
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(145, 477)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 45
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnAgregarTicket
        '
        Me.btnAgregarTicket.Location = New System.Drawing.Point(0, 217)
        Me.btnAgregarTicket.Name = "btnAgregarTicket"
        Me.btnAgregarTicket.Size = New System.Drawing.Size(93, 23)
        Me.btnAgregarTicket.TabIndex = 48
        Me.btnAgregarTicket.Text = "Agregar Ticket"
        Me.btnAgregarTicket.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(137, 414)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 13)
        Me.Label2.TabIndex = 50
        Me.Label2.Text = "Diferencia cantidad:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(330, 414)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(95, 13)
        Me.Label10.TabIndex = 52
        Me.Label10.Text = "Diferencia importe:"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.Label11)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(2, 437)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 53
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'txtDiferenciaImporte
        '
        Me.txtDiferenciaImporte.Color = System.Drawing.Color.Empty
        Me.txtDiferenciaImporte.Decimales = True
        Me.txtDiferenciaImporte.Enabled = False
        Me.txtDiferenciaImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtDiferenciaImporte.Location = New System.Drawing.Point(428, 410)
        Me.txtDiferenciaImporte.Name = "txtDiferenciaImporte"
        Me.txtDiferenciaImporte.Size = New System.Drawing.Size(84, 21)
        Me.txtDiferenciaImporte.SoloLectura = True
        Me.txtDiferenciaImporte.TabIndex = 51
        Me.txtDiferenciaImporte.TabStop = False
        Me.txtDiferenciaImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferenciaImporte.Texto = "0"
        '
        'txtDiferenciaCantidad
        '
        Me.txtDiferenciaCantidad.Color = System.Drawing.Color.Empty
        Me.txtDiferenciaCantidad.Decimales = True
        Me.txtDiferenciaCantidad.Enabled = False
        Me.txtDiferenciaCantidad.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtDiferenciaCantidad.Location = New System.Drawing.Point(243, 410)
        Me.txtDiferenciaCantidad.Name = "txtDiferenciaCantidad"
        Me.txtDiferenciaCantidad.Size = New System.Drawing.Size(84, 21)
        Me.txtDiferenciaCantidad.SoloLectura = True
        Me.txtDiferenciaCantidad.TabIndex = 49
        Me.txtDiferenciaCantidad.TabStop = False
        Me.txtDiferenciaCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDiferenciaCantidad.Texto = "0"
        '
        'txtTotalUs
        '
        Me.txtTotalUs.Color = System.Drawing.Color.Empty
        Me.txtTotalUs.Decimales = True
        Me.txtTotalUs.Enabled = False
        Me.txtTotalUs.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalUs.Location = New System.Drawing.Point(519, 383)
        Me.txtTotalUs.Name = "txtTotalUs"
        Me.txtTotalUs.Size = New System.Drawing.Size(84, 21)
        Me.txtTotalUs.SoloLectura = True
        Me.txtTotalUs.TabIndex = 47
        Me.txtTotalUs.TabStop = False
        Me.txtTotalUs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalUs.Texto = "0"
        '
        'txtTotalGs
        '
        Me.txtTotalGs.Color = System.Drawing.Color.Empty
        Me.txtTotalGs.Decimales = False
        Me.txtTotalGs.Enabled = False
        Me.txtTotalGs.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalGs.Location = New System.Drawing.Point(428, 383)
        Me.txtTotalGs.Name = "txtTotalGs"
        Me.txtTotalGs.Size = New System.Drawing.Size(84, 21)
        Me.txtTotalGs.SoloLectura = True
        Me.txtTotalGs.TabIndex = 46
        Me.txtTotalGs.TabStop = False
        Me.txtTotalGs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalGs.Texto = "0"
        '
        'txtTotalBascula
        '
        Me.txtTotalBascula.Color = System.Drawing.Color.Empty
        Me.txtTotalBascula.Decimales = True
        Me.txtTotalBascula.Enabled = False
        Me.txtTotalBascula.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalBascula.Location = New System.Drawing.Point(337, 383)
        Me.txtTotalBascula.Name = "txtTotalBascula"
        Me.txtTotalBascula.Size = New System.Drawing.Size(84, 21)
        Me.txtTotalBascula.SoloLectura = True
        Me.txtTotalBascula.TabIndex = 1
        Me.txtTotalBascula.TabStop = False
        Me.txtTotalBascula.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalBascula.Texto = "0"
        '
        'txtAcuerdo
        '
        Me.txtAcuerdo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAcuerdo.Color = System.Drawing.Color.Empty
        Me.txtAcuerdo.Indicaciones = Nothing
        Me.txtAcuerdo.Location = New System.Drawing.Point(78, 42)
        Me.txtAcuerdo.Multilinea = False
        Me.txtAcuerdo.Name = "txtAcuerdo"
        Me.txtAcuerdo.Size = New System.Drawing.Size(71, 21)
        Me.txtAcuerdo.SoloLectura = False
        Me.txtAcuerdo.TabIndex = 12
        Me.txtAcuerdo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAcuerdo.Texto = ""
        '
        'txtProveedor
        '
        Me.txtProveedor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProveedor.Color = System.Drawing.Color.Empty
        Me.txtProveedor.Indicaciones = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(217, 42)
        Me.txtProveedor.Multilinea = False
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Size = New System.Drawing.Size(364, 21)
        Me.txtProveedor.SoloLectura = True
        Me.txtProveedor.TabIndex = 14
        Me.txtProveedor.TabStop = False
        Me.txtProveedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtProveedor.Texto = ""
        '
        'txtPrecio
        '
        Me.txtPrecio.Color = System.Drawing.Color.Empty
        Me.txtPrecio.Decimales = True
        Me.txtPrecio.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPrecio.Location = New System.Drawing.Point(202, 70)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(62, 21)
        Me.txtPrecio.SoloLectura = True
        Me.txtPrecio.TabIndex = 20
        Me.txtPrecio.TabStop = False
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecio.Texto = "0"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(78, 70)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(31, 21)
        Me.cbxMoneda.SoloLectura = True
        Me.cbxMoneda.TabIndex = 16
        Me.cbxMoneda.TabStop = False
        Me.cbxMoneda.Texto = ""
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = ""
        Me.txtCotizacion.Location = New System.Drawing.Point(111, 70)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(47, 21)
        Me.txtCotizacion.SoloLectura = True
        Me.txtCotizacion.TabIndex = 17
        Me.txtCotizacion.TabStop = False
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(211, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(60, 21)
        Me.cbxSucursal.SoloLectura = True
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.TabStop = False
        Me.cbxSucursal.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 3, 18, 14, 6, 37, 841)
        Me.txtFecha.Location = New System.Drawing.Point(507, 13)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 10
        '
        'txtProducto
        '
        Me.txtProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProducto.Color = System.Drawing.Color.Empty
        Me.txtProducto.Indicaciones = Nothing
        Me.txtProducto.Location = New System.Drawing.Point(323, 69)
        Me.txtProducto.Multilinea = False
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(258, 21)
        Me.txtProducto.SoloLectura = True
        Me.txtProducto.TabIndex = 22
        Me.txtProducto.TabStop = False
        Me.txtProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtProducto.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(321, 12)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(66, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(393, 12)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(64, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 8
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(78, 12)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(51, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(131, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(46, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(78, 97)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(503, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 1
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtTotalRemision
        '
        Me.txtTotalRemision.Color = System.Drawing.Color.Empty
        Me.txtTotalRemision.Decimales = True
        Me.txtTotalRemision.Enabled = False
        Me.txtTotalRemision.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalRemision.Location = New System.Drawing.Point(243, 383)
        Me.txtTotalRemision.Name = "txtTotalRemision"
        Me.txtTotalRemision.Size = New System.Drawing.Size(84, 21)
        Me.txtTotalRemision.SoloLectura = True
        Me.txtTotalRemision.TabIndex = 0
        Me.txtTotalRemision.TabStop = False
        Me.txtTotalRemision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRemision.Texto = "0"
        '
        'txtCantidadFactura
        '
        Me.txtCantidadFactura.Color = System.Drawing.Color.Empty
        Me.txtCantidadFactura.Decimales = True
        Me.txtCantidadFactura.Enabled = False
        Me.txtCantidadFactura.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidadFactura.Location = New System.Drawing.Point(299, 38)
        Me.txtCantidadFactura.Name = "txtCantidadFactura"
        Me.txtCantidadFactura.Size = New System.Drawing.Size(62, 21)
        Me.txtCantidadFactura.SoloLectura = True
        Me.txtCantidadFactura.TabIndex = 9
        Me.txtCantidadFactura.TabStop = False
        Me.txtCantidadFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadFactura.Texto = "0"
        '
        'txtTotalFactura
        '
        Me.txtTotalFactura.Color = System.Drawing.Color.Empty
        Me.txtTotalFactura.Decimales = True
        Me.txtTotalFactura.Enabled = False
        Me.txtTotalFactura.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalFactura.Location = New System.Drawing.Point(469, 38)
        Me.txtTotalFactura.Name = "txtTotalFactura"
        Me.txtTotalFactura.Size = New System.Drawing.Size(112, 21)
        Me.txtTotalFactura.SoloLectura = True
        Me.txtTotalFactura.TabIndex = 11
        Me.txtTotalFactura.TabStop = False
        Me.txtTotalFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalFactura.Texto = "0"
        '
        'txtFechaFactura
        '
        Me.txtFechaFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFechaFactura.Color = System.Drawing.Color.Empty
        Me.txtFechaFactura.Enabled = False
        Me.txtFechaFactura.Indicaciones = Nothing
        Me.txtFechaFactura.Location = New System.Drawing.Point(299, 15)
        Me.txtFechaFactura.Multilinea = False
        Me.txtFechaFactura.Name = "txtFechaFactura"
        Me.txtFechaFactura.Size = New System.Drawing.Size(62, 20)
        Me.txtFechaFactura.SoloLectura = True
        Me.txtFechaFactura.TabIndex = 10
        Me.txtFechaFactura.TabStop = False
        Me.txtFechaFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtFechaFactura.Texto = ""
        '
        'txtCotizacionFactura
        '
        Me.txtCotizacionFactura.Color = System.Drawing.Color.Empty
        Me.txtCotizacionFactura.Decimales = False
        Me.txtCotizacionFactura.Enabled = False
        Me.txtCotizacionFactura.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacionFactura.Location = New System.Drawing.Point(76, 39)
        Me.txtCotizacionFactura.Name = "txtCotizacionFactura"
        Me.txtCotizacionFactura.Size = New System.Drawing.Size(51, 21)
        Me.txtCotizacionFactura.SoloLectura = True
        Me.txtCotizacionFactura.TabIndex = 7
        Me.txtCotizacionFactura.TabStop = False
        Me.txtCotizacionFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacionFactura.Texto = "0"
        '
        'txtComprobanteFactura
        '
        Me.txtComprobanteFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobanteFactura.Color = System.Drawing.Color.Empty
        Me.txtComprobanteFactura.Enabled = False
        Me.txtComprobanteFactura.Indicaciones = Nothing
        Me.txtComprobanteFactura.Location = New System.Drawing.Point(76, 14)
        Me.txtComprobanteFactura.Multilinea = False
        Me.txtComprobanteFactura.Name = "txtComprobanteFactura"
        Me.txtComprobanteFactura.Size = New System.Drawing.Size(84, 21)
        Me.txtComprobanteFactura.SoloLectura = True
        Me.txtComprobanteFactura.TabIndex = 2
        Me.txtComprobanteFactura.TabStop = False
        Me.txtComprobanteFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobanteFactura.Texto = ""
        '
        'frmMacheoFacturaTicket
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(612, 526)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.txtDiferenciaImporte)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtDiferenciaCantidad)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnAgregarTicket)
        Me.Controls.Add(Me.txtTotalUs)
        Me.Controls.Add(Me.txtTotalGs)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.txtTotalBascula)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.txtTotalRemision)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmMacheoFacturaTicket"
        Me.Text = "frmMacheoFacturaTicket"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtTotalBascula As ERP.ocxTXTNumeric
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtTotalRemision As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotalFactura As ERP.ocxTXTNumeric
    Friend WithEvents txtFechaFactura As ERP.ocxTXTString
    Friend WithEvents bntAgregarFactura As System.Windows.Forms.Button
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtProducto As ERP.ocxTXTString
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblFechaNotaCredito As System.Windows.Forms.Label
    Friend WithEvents txtComprobanteFactura As ERP.ocxTXTString
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtPrecio As ERP.ocxTXTNumeric
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblMonedaNotaCredito As System.Windows.Forms.Label
    Friend WithEvents txtCotizacionFactura As ERP.ocxTXTNumeric
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadFactura As ERP.ocxTXTNumeric
    Friend WithEvents txtAcuerdo As ERP.ocxTXTString
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtProveedor As ERP.ocxTXTString
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents txtTotalUs As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalGs As ERP.ocxTXTNumeric
    Friend WithEvents btnAgregarTicket As System.Windows.Forms.Button
    Friend WithEvents colIDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNumero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCotizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPesoRemision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPesoBascula As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colImporteUs As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtDiferenciaImporte As ERP.ocxTXTNumeric
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtDiferenciaCantidad As ERP.ocxTXTNumeric
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents Label11 As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
End Class

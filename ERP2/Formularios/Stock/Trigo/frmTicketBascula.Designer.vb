﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTicketBascula
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.cbxUnidad = New ERP.ocxCBX()
        Me.txtCostoUnitarioUs = New ERP.ocxTXTNumeric()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.txtPesoRemision = New ERP.ocxTXTNumeric()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.txtCostoUnitario = New ERP.ocxTXTNumeric()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCantidadBascula = New ERP.ocxTXTNumeric()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnRecargarChoferes = New System.Windows.Forms.Button()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtRecepcion = New ERP.ocxTXTString()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtRemision = New ERP.ocxTXTString()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.cbxChapa = New ERP.ocxCBX()
        Me.cbxCamion = New ERP.ocxCBX()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCotizacionUs = New ERP.ocxTXTNumeric()
        Me.txtAcuerdo = New ERP.ocxTXTString()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.cbxTipoFlete = New ERP.ocxCBX()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtFechaTicket = New ERP.ocxTXTDate()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 359)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(870, 22)
        Me.StatusStrip1.TabIndex = 20
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(175, 323)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 15
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(94, 323)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 14
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(767, 323)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 10
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(685, 323)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 18
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(13, 323)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 13
        Me.btnAnular.Text = "Anu&lar"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(601, 323)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 17
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(518, 323)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 16
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(434, 323)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 21
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.cbxUnidad)
        Me.GroupBox1.Controls.Add(Me.txtCostoUnitarioUs)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.flpAnuladoPor)
        Me.GroupBox1.Controls.Add(Me.txtPesoRemision)
        Me.GroupBox1.Controls.Add(Me.flpRegistradoPor)
        Me.GroupBox1.Controls.Add(Me.txtCostoUnitario)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtCantidadBascula)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.dgw)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 169)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(849, 148)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(4, 32)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(347, 20)
        Me.txtProducto.SoloLectura = True
        Me.txtProducto.TabIndex = 1
        Me.txtProducto.TabStop = False
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'cbxUnidad
        '
        Me.cbxUnidad.CampoWhere = Nothing
        Me.cbxUnidad.CargarUnaSolaVez = False
        Me.cbxUnidad.DataDisplayMember = Nothing
        Me.cbxUnidad.DataFilter = Nothing
        Me.cbxUnidad.DataOrderBy = Nothing
        Me.cbxUnidad.DataSource = Nothing
        Me.cbxUnidad.DataValueMember = Nothing
        Me.cbxUnidad.dtSeleccionado = Nothing
        Me.cbxUnidad.FormABM = Nothing
        Me.cbxUnidad.Indicaciones = Nothing
        Me.cbxUnidad.Location = New System.Drawing.Point(357, 31)
        Me.cbxUnidad.Name = "cbxUnidad"
        Me.cbxUnidad.SeleccionMultiple = False
        Me.cbxUnidad.SeleccionObligatoria = True
        Me.cbxUnidad.Size = New System.Drawing.Size(91, 21)
        Me.cbxUnidad.SoloLectura = True
        Me.cbxUnidad.TabIndex = 3
        Me.cbxUnidad.TabStop = False
        Me.cbxUnidad.Texto = ""
        '
        'txtCostoUnitarioUs
        '
        Me.txtCostoUnitarioUs.Color = System.Drawing.Color.Empty
        Me.txtCostoUnitarioUs.Decimales = True
        Me.txtCostoUnitarioUs.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCostoUnitarioUs.Location = New System.Drawing.Point(760, 32)
        Me.txtCostoUnitarioUs.Name = "txtCostoUnitarioUs"
        Me.txtCostoUnitarioUs.Size = New System.Drawing.Size(81, 21)
        Me.txtCostoUnitarioUs.SoloLectura = True
        Me.txtCostoUnitarioUs.TabIndex = 10
        Me.txtCostoUnitarioUs.TabStop = False
        Me.txtCostoUnitarioUs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCostoUnitarioUs.Texto = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(777, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(55, 13)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Costo US:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(459, 16)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(98, 13)
        Me.Label22.TabIndex = 4
        Me.Label22.Text = "Cantidad Remision:"
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(287, 116)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(295, 20)
        Me.flpAnuladoPor.TabIndex = 12
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'txtPesoRemision
        '
        Me.txtPesoRemision.Color = System.Drawing.Color.Empty
        Me.txtPesoRemision.Decimales = True
        Me.txtPesoRemision.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtPesoRemision.Location = New System.Drawing.Point(454, 32)
        Me.txtPesoRemision.Name = "txtPesoRemision"
        Me.txtPesoRemision.Size = New System.Drawing.Size(104, 21)
        Me.txtPesoRemision.SoloLectura = False
        Me.txtPesoRemision.TabIndex = 4
        Me.txtPesoRemision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPesoRemision.Texto = "0"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.Label4)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(4, 116)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 18
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(79, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'txtCostoUnitario
        '
        Me.txtCostoUnitario.Color = System.Drawing.Color.Empty
        Me.txtCostoUnitario.Decimales = False
        Me.txtCostoUnitario.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCostoUnitario.Location = New System.Drawing.Point(673, 32)
        Me.txtCostoUnitario.Name = "txtCostoUnitario"
        Me.txtCostoUnitario.Size = New System.Drawing.Size(81, 21)
        Me.txtCostoUnitario.SoloLectura = True
        Me.txtCostoUnitario.TabIndex = 8
        Me.txtCostoUnitario.TabStop = False
        Me.txtCostoUnitario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCostoUnitario.Texto = "0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(690, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(37, 13)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Costo:"
        '
        'txtCantidadBascula
        '
        Me.txtCantidadBascula.Color = System.Drawing.Color.Empty
        Me.txtCantidadBascula.Decimales = True
        Me.txtCantidadBascula.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidadBascula.Location = New System.Drawing.Point(564, 32)
        Me.txtCantidadBascula.Name = "txtCantidadBascula"
        Me.txtCantidadBascula.Size = New System.Drawing.Size(104, 21)
        Me.txtCantidadBascula.SoloLectura = False
        Me.txtCantidadBascula.TabIndex = 6
        Me.txtCantidadBascula.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadBascula.Texto = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(570, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(93, 13)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Cantidad Bascula:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(359, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Unidad:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Producto:"
        '
        'dgw
        '
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.Location = New System.Drawing.Point(4, 59)
        Me.dgw.Name = "dgw"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgw.Size = New System.Drawing.Size(837, 51)
        Me.dgw.TabIndex = 11
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnRecargarChoferes)
        Me.GroupBox2.Controls.Add(Me.txtProveedor)
        Me.GroupBox2.Controls.Add(Me.txtRecepcion)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.txtRemision)
        Me.GroupBox2.Controls.Add(Me.Label27)
        Me.GroupBox2.Controls.Add(Me.cbxChofer)
        Me.GroupBox2.Controls.Add(Me.cbxChapa)
        Me.GroupBox2.Controls.Add(Me.cbxCamion)
        Me.GroupBox2.Controls.Add(Me.cbxMoneda)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtCotizacionUs)
        Me.GroupBox2.Controls.Add(Me.txtAcuerdo)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.lblProveedor)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.cbxSucursal)
        Me.GroupBox2.Controls.Add(Me.txtObservacion)
        Me.GroupBox2.Controls.Add(Me.cbxTipoComprobante)
        Me.GroupBox2.Controls.Add(Me.cbxDeposito)
        Me.GroupBox2.Controls.Add(Me.cbxTipoFlete)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.txtFechaTicket)
        Me.GroupBox2.Controls.Add(Me.txtID)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.txtComprobante)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(849, 163)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'btnRecargarChoferes
        '
        Me.btnRecargarChoferes.Location = New System.Drawing.Point(714, 134)
        Me.btnRecargarChoferes.Name = "btnRecargarChoferes"
        Me.btnRecargarChoferes.Size = New System.Drawing.Size(127, 23)
        Me.btnRecargarChoferes.TabIndex = 22
        Me.btnRecargarChoferes.Text = "&Ver todos los choferes"
        Me.btnRecargarChoferes.UseVisualStyleBackColor = True
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(322, 42)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(519, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 15
        '
        'txtRecepcion
        '
        Me.txtRecepcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRecepcion.Color = System.Drawing.Color.Empty
        Me.txtRecepcion.Indicaciones = Nothing
        Me.txtRecepcion.Location = New System.Drawing.Point(746, 13)
        Me.txtRecepcion.Multilinea = False
        Me.txtRecepcion.Name = "txtRecepcion"
        Me.txtRecepcion.Size = New System.Drawing.Size(95, 21)
        Me.txtRecepcion.SoloLectura = False
        Me.txtRecepcion.TabIndex = 10
        Me.txtRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRecepcion.Texto = ""
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(677, 17)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(62, 13)
        Me.Label28.TabIndex = 9
        Me.Label28.Text = "Recepcion:"
        '
        'txtRemision
        '
        Me.txtRemision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemision.Color = System.Drawing.Color.Empty
        Me.txtRemision.Indicaciones = Nothing
        Me.txtRemision.Location = New System.Drawing.Point(569, 13)
        Me.txtRemision.Multilinea = False
        Me.txtRemision.Name = "txtRemision"
        Me.txtRemision.Size = New System.Drawing.Size(95, 21)
        Me.txtRemision.SoloLectura = False
        Me.txtRemision.TabIndex = 8
        Me.txtRemision.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtRemision.Texto = ""
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(516, 17)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(50, 13)
        Me.Label27.TabIndex = 7
        Me.Label27.Text = "Remision"
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = Nothing
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = Nothing
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = Nothing
        Me.cbxChofer.DataSource = Nothing
        Me.cbxChofer.DataValueMember = Nothing
        Me.cbxChofer.dtSeleccionado = Nothing
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(537, 106)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionMultiple = False
        Me.cbxChofer.SeleccionObligatoria = True
        Me.cbxChofer.Size = New System.Drawing.Size(304, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 27
        Me.cbxChofer.Texto = ""
        '
        'cbxChapa
        '
        Me.cbxChapa.CampoWhere = Nothing
        Me.cbxChapa.CargarUnaSolaVez = False
        Me.cbxChapa.DataDisplayMember = Nothing
        Me.cbxChapa.DataFilter = Nothing
        Me.cbxChapa.DataOrderBy = Nothing
        Me.cbxChapa.DataSource = Nothing
        Me.cbxChapa.DataValueMember = Nothing
        Me.cbxChapa.dtSeleccionado = Nothing
        Me.cbxChapa.FormABM = Nothing
        Me.cbxChapa.Indicaciones = Nothing
        Me.cbxChapa.Location = New System.Drawing.Point(308, 106)
        Me.cbxChapa.Name = "cbxChapa"
        Me.cbxChapa.SeleccionMultiple = False
        Me.cbxChapa.SeleccionObligatoria = True
        Me.cbxChapa.Size = New System.Drawing.Size(173, 21)
        Me.cbxChapa.SoloLectura = False
        Me.cbxChapa.TabIndex = 25
        Me.cbxChapa.Texto = ""
        '
        'cbxCamion
        '
        Me.cbxCamion.CampoWhere = Nothing
        Me.cbxCamion.CargarUnaSolaVez = False
        Me.cbxCamion.DataDisplayMember = Nothing
        Me.cbxCamion.DataFilter = Nothing
        Me.cbxCamion.DataOrderBy = Nothing
        Me.cbxCamion.DataSource = Nothing
        Me.cbxCamion.DataValueMember = Nothing
        Me.cbxCamion.dtSeleccionado = Nothing
        Me.cbxCamion.FormABM = Nothing
        Me.cbxCamion.Indicaciones = Nothing
        Me.cbxCamion.Location = New System.Drawing.Point(82, 106)
        Me.cbxCamion.Name = "cbxCamion"
        Me.cbxCamion.SeleccionMultiple = False
        Me.cbxCamion.SeleccionObligatoria = True
        Me.cbxCamion.Size = New System.Drawing.Size(173, 21)
        Me.cbxCamion.SoloLectura = True
        Me.cbxCamion.TabIndex = 23
        Me.cbxCamion.TabStop = False
        Me.cbxCamion.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(82, 79)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(174, 21)
        Me.cbxMoneda.SoloLectura = True
        Me.cbxMoneda.TabIndex = 17
        Me.cbxMoneda.TabStop = False
        Me.cbxMoneda.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(261, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Cotizacion US:"
        '
        'txtCotizacionUs
        '
        Me.txtCotizacionUs.Color = System.Drawing.Color.Empty
        Me.txtCotizacionUs.Decimales = False
        Me.txtCotizacionUs.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacionUs.Location = New System.Drawing.Point(341, 79)
        Me.txtCotizacionUs.Name = "txtCotizacionUs"
        Me.txtCotizacionUs.Size = New System.Drawing.Size(118, 21)
        Me.txtCotizacionUs.SoloLectura = True
        Me.txtCotizacionUs.TabIndex = 19
        Me.txtCotizacionUs.TabStop = False
        Me.txtCotizacionUs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacionUs.Texto = "0"
        '
        'txtAcuerdo
        '
        Me.txtAcuerdo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAcuerdo.Color = System.Drawing.Color.Empty
        Me.txtAcuerdo.Indicaciones = Nothing
        Me.txtAcuerdo.Location = New System.Drawing.Point(313, 12)
        Me.txtAcuerdo.Multilinea = False
        Me.txtAcuerdo.Name = "txtAcuerdo"
        Me.txtAcuerdo.Size = New System.Drawing.Size(70, 21)
        Me.txtAcuerdo.SoloLectura = True
        Me.txtAcuerdo.TabIndex = 4
        Me.txtAcuerdo.TabStop = False
        Me.txtAcuerdo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAcuerdo.Texto = ""
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(261, 16)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(50, 13)
        Me.Label23.TabIndex = 3
        Me.Label23.Text = "Acuerdo:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(35, 108)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(45, 13)
        Me.Label21.TabIndex = 22
        Me.Label21.Text = "Camion:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(261, 110)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(41, 13)
        Me.Label20.TabIndex = 24
        Me.Label20.Text = "Chapa:"
        '
        'lblProveedor
        '
        Me.lblProveedor.AutoSize = True
        Me.lblProveedor.Location = New System.Drawing.Point(261, 49)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(59, 13)
        Me.lblProveedor.TabIndex = 14
        Me.lblProveedor.Text = "Proveedor:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(495, 109)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(41, 13)
        Me.Label19.TabIndex = 26
        Me.Label19.Text = "Chofer:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(31, 82)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(49, 13)
        Me.Label12.TabIndex = 16
        Me.Label12.Text = "Moneda:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(82, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(72, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(337, 137)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(359, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 31
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(82, 45)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(72, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 12
        Me.cbxTipoComprobante.Texto = ""
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = ""
        Me.cbxDeposito.DataFilter = ""
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = ""
        Me.cbxDeposito.DataValueMember = ""
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(537, 79)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = True
        Me.cbxDeposito.Size = New System.Drawing.Size(304, 21)
        Me.cbxDeposito.SoloLectura = True
        Me.cbxDeposito.TabIndex = 21
        Me.cbxDeposito.TabStop = False
        Me.cbxDeposito.Texto = ""
        '
        'cbxTipoFlete
        '
        Me.cbxTipoFlete.CampoWhere = Nothing
        Me.cbxTipoFlete.CargarUnaSolaVez = False
        Me.cbxTipoFlete.DataDisplayMember = Nothing
        Me.cbxTipoFlete.DataFilter = Nothing
        Me.cbxTipoFlete.DataOrderBy = Nothing
        Me.cbxTipoFlete.DataSource = Nothing
        Me.cbxTipoFlete.DataValueMember = Nothing
        Me.cbxTipoFlete.dtSeleccionado = Nothing
        Me.cbxTipoFlete.Enabled = False
        Me.cbxTipoFlete.FormABM = Nothing
        Me.cbxTipoFlete.Indicaciones = Nothing
        Me.cbxTipoFlete.Location = New System.Drawing.Point(82, 136)
        Me.cbxTipoFlete.Name = "cbxTipoFlete"
        Me.cbxTipoFlete.SeleccionMultiple = False
        Me.cbxTipoFlete.SeleccionObligatoria = True
        Me.cbxTipoFlete.Size = New System.Drawing.Size(174, 21)
        Me.cbxTipoFlete.SoloLectura = True
        Me.cbxTipoFlete.TabIndex = 29
        Me.cbxTipoFlete.TabStop = False
        Me.cbxTipoFlete.Texto = ""
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(17, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Operacion:"
        '
        'txtFechaTicket
        '
        Me.txtFechaTicket.AñoFecha = 0
        Me.txtFechaTicket.Color = System.Drawing.Color.Empty
        Me.txtFechaTicket.Fecha = New Date(2013, 4, 8, 9, 22, 55, 62)
        Me.txtFechaTicket.Location = New System.Drawing.Point(433, 13)
        Me.txtFechaTicket.MesFecha = 0
        Me.txtFechaTicket.Name = "txtFechaTicket"
        Me.txtFechaTicket.PermitirNulo = False
        Me.txtFechaTicket.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaTicket.SoloLectura = False
        Me.txtFechaTicket.TabIndex = 6
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(159, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(97, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(40, 139)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(33, 13)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = "Flete:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(481, 82)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(52, 13)
        Me.Label15.TabIndex = 20
        Me.Label15.Text = "Deposito:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(261, 139)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(70, 13)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = "Observacion:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(389, 16)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(40, 13)
        Me.Label17.TabIndex = 5
        Me.Label17.Text = "Fecha:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(3, 49)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(73, 13)
        Me.Label18.TabIndex = 11
        Me.Label18.Text = "Comprobante:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(159, 46)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(97, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 13
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(13, 323)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "Anu&lar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(94, 323)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "&Imprimir"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(175, 323)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(126, 23)
        Me.Button8.TabIndex = 5
        Me.Button8.Text = "&Busqueda Avanzada"
        Me.Button8.UseVisualStyleBackColor = True
        Me.Button8.Visible = False
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(349, 323)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 22
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'frmTicketBascula
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(870, 381)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnAsiento)
        Me.Name = "frmTicketBascula"
        Me.Tag = "frmTicketBascula"
        Me.Text = "frmTicketBascula"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents btnAsiento As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents cbxTipoFlete As ERP.ocxCBX
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtFechaTicket As ERP.ocxTXTDate
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents txtCostoUnitario As ERP.ocxTXTNumeric
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadBascula As ERP.ocxTXTNumeric
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtPesoRemision As ERP.ocxTXTNumeric
    Friend WithEvents txtCostoUnitarioUs As ERP.ocxTXTNumeric
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtAcuerdo As ERP.ocxTXTString
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCotizacionUs As ERP.ocxTXTNumeric
    Friend WithEvents cbxUnidad As ERP.ocxCBX
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents cbxChapa As ERP.ocxCBX
    Friend WithEvents cbxCamion As ERP.ocxCBX
    Friend WithEvents txtRecepcion As ERP.ocxTXTString
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtRemision As ERP.ocxTXTString
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents btnRecargarChoferes As System.Windows.Forms.Button
    Friend WithEvents btnModificar As Button
End Class

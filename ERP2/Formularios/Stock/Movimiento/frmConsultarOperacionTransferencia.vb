﻿Public Class frmConsultarOperacionTransferencia
    Public Property Enviar As Boolean = False
    Public Property Recibir As Boolean = False
    Public Property Cancelar As Boolean = False

    Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click
        Enviar = True
        Me.Close()
    End Sub

    Private Sub btnRecibir_Click(sender As Object, e As EventArgs) Handles btnRecibir.Click
        Recibir = True
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Cancelar = True
        Me.Close()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultarOperacionTransferencia_Activate()
        Me.Refresh()
    End Sub
End Class
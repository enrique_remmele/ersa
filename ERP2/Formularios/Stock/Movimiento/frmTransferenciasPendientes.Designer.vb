﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransferenciasPendientes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.cbxDepositoOrigen = New ERP.ocxCBX()
        Me.chkDepositoOrigen = New ERP.ocxCHK()
        Me.cbxSucursalDestino = New ERP.ocxCBX()
        Me.chkSucursalDestino = New ERP.ocxCHK()
        Me.cbxSucursalOrigen = New ERP.ocxCBX()
        Me.chkSucursalOrigen = New ERP.ocxCHK()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv
        '
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 90)
        Me.dgv.Name = "dgv"
        Me.dgv.Size = New System.Drawing.Size(965, 340)
        Me.dgv.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.09238!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.90762!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(971, 472)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnListar)
        Me.Panel1.Controls.Add(Me.cbxDepositoOrigen)
        Me.Panel1.Controls.Add(Me.chkDepositoOrigen)
        Me.Panel1.Controls.Add(Me.cbxSucursalDestino)
        Me.Panel1.Controls.Add(Me.chkSucursalDestino)
        Me.Panel1.Controls.Add(Me.cbxSucursalOrigen)
        Me.Panel1.Controls.Add(Me.chkSucursalOrigen)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(971, 87)
        Me.Panel1.TabIndex = 1
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(305, 41)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(266, 23)
        Me.btnListar.TabIndex = 8
        Me.btnListar.Text = "Listar Movimientos"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'cbxDepositoOrigen
        '
        Me.cbxDepositoOrigen.CampoWhere = Nothing
        Me.cbxDepositoOrigen.CargarUnaSolaVez = False
        Me.cbxDepositoOrigen.DataDisplayMember = Nothing
        Me.cbxDepositoOrigen.DataFilter = Nothing
        Me.cbxDepositoOrigen.DataOrderBy = Nothing
        Me.cbxDepositoOrigen.DataSource = Nothing
        Me.cbxDepositoOrigen.DataValueMember = Nothing
        Me.cbxDepositoOrigen.dtSeleccionado = Nothing
        Me.cbxDepositoOrigen.Enabled = False
        Me.cbxDepositoOrigen.FormABM = Nothing
        Me.cbxDepositoOrigen.Indicaciones = Nothing
        Me.cbxDepositoOrigen.Location = New System.Drawing.Point(416, 12)
        Me.cbxDepositoOrigen.Name = "cbxDepositoOrigen"
        Me.cbxDepositoOrigen.SeleccionMultiple = False
        Me.cbxDepositoOrigen.SeleccionObligatoria = False
        Me.cbxDepositoOrigen.Size = New System.Drawing.Size(155, 23)
        Me.cbxDepositoOrigen.SoloLectura = False
        Me.cbxDepositoOrigen.TabIndex = 5
        Me.cbxDepositoOrigen.Texto = ""
        '
        'chkDepositoOrigen
        '
        Me.chkDepositoOrigen.BackColor = System.Drawing.Color.Transparent
        Me.chkDepositoOrigen.Color = System.Drawing.Color.Empty
        Me.chkDepositoOrigen.Location = New System.Drawing.Point(305, 12)
        Me.chkDepositoOrigen.Name = "chkDepositoOrigen"
        Me.chkDepositoOrigen.Size = New System.Drawing.Size(96, 21)
        Me.chkDepositoOrigen.SoloLectura = False
        Me.chkDepositoOrigen.TabIndex = 4
        Me.chkDepositoOrigen.Texto = "Deposito Origen:"
        Me.chkDepositoOrigen.Valor = False
        '
        'cbxSucursalDestino
        '
        Me.cbxSucursalDestino.CampoWhere = "IDSucursalDestino"
        Me.cbxSucursalDestino.CargarUnaSolaVez = False
        Me.cbxSucursalDestino.DataDisplayMember = "Descripcion"
        Me.cbxSucursalDestino.DataFilter = Nothing
        Me.cbxSucursalDestino.DataOrderBy = Nothing
        Me.cbxSucursalDestino.DataSource = "VSucursal"
        Me.cbxSucursalDestino.DataValueMember = "ID"
        Me.cbxSucursalDestino.dtSeleccionado = Nothing
        Me.cbxSucursalDestino.Enabled = False
        Me.cbxSucursalDestino.FormABM = Nothing
        Me.cbxSucursalDestino.Indicaciones = Nothing
        Me.cbxSucursalDestino.Location = New System.Drawing.Point(123, 41)
        Me.cbxSucursalDestino.Name = "cbxSucursalDestino"
        Me.cbxSucursalDestino.SeleccionMultiple = False
        Me.cbxSucursalDestino.SeleccionObligatoria = False
        Me.cbxSucursalDestino.Size = New System.Drawing.Size(155, 23)
        Me.cbxSucursalDestino.SoloLectura = False
        Me.cbxSucursalDestino.TabIndex = 3
        Me.cbxSucursalDestino.Texto = ""
        '
        'chkSucursalDestino
        '
        Me.chkSucursalDestino.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalDestino.Color = System.Drawing.Color.Empty
        Me.chkSucursalDestino.Location = New System.Drawing.Point(12, 41)
        Me.chkSucursalDestino.Name = "chkSucursalDestino"
        Me.chkSucursalDestino.Size = New System.Drawing.Size(105, 21)
        Me.chkSucursalDestino.SoloLectura = False
        Me.chkSucursalDestino.TabIndex = 2
        Me.chkSucursalDestino.Texto = "Sucursal Destino:"
        Me.chkSucursalDestino.Valor = False
        '
        'cbxSucursalOrigen
        '
        Me.cbxSucursalOrigen.CampoWhere = "IDSucursal"
        Me.cbxSucursalOrigen.CargarUnaSolaVez = False
        Me.cbxSucursalOrigen.DataDisplayMember = "Descripcion"
        Me.cbxSucursalOrigen.DataFilter = Nothing
        Me.cbxSucursalOrigen.DataOrderBy = Nothing
        Me.cbxSucursalOrigen.DataSource = "VSucursal"
        Me.cbxSucursalOrigen.DataValueMember = "ID"
        Me.cbxSucursalOrigen.dtSeleccionado = Nothing
        Me.cbxSucursalOrigen.Enabled = False
        Me.cbxSucursalOrigen.FormABM = Nothing
        Me.cbxSucursalOrigen.Indicaciones = Nothing
        Me.cbxSucursalOrigen.Location = New System.Drawing.Point(123, 12)
        Me.cbxSucursalOrigen.Name = "cbxSucursalOrigen"
        Me.cbxSucursalOrigen.SeleccionMultiple = False
        Me.cbxSucursalOrigen.SeleccionObligatoria = False
        Me.cbxSucursalOrigen.Size = New System.Drawing.Size(155, 23)
        Me.cbxSucursalOrigen.SoloLectura = False
        Me.cbxSucursalOrigen.TabIndex = 1
        Me.cbxSucursalOrigen.Texto = ""
        '
        'chkSucursalOrigen
        '
        Me.chkSucursalOrigen.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalOrigen.Color = System.Drawing.Color.Empty
        Me.chkSucursalOrigen.Location = New System.Drawing.Point(12, 12)
        Me.chkSucursalOrigen.Name = "chkSucursalOrigen"
        Me.chkSucursalOrigen.Size = New System.Drawing.Size(96, 21)
        Me.chkSucursalOrigen.SoloLectura = False
        Me.chkSucursalOrigen.TabIndex = 0
        Me.chkSucursalOrigen.Texto = "Sucursal Origen:"
        Me.chkSucursalOrigen.Valor = False
        '
        'frmTransferenciasPendientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(971, 472)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmTransferenciasPendientes"
        Me.Text = "frmTransferenciasPendientes"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgv As DataGridView
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnListar As Button
    Friend WithEvents cbxDepositoOrigen As ocxCBX
    Friend WithEvents chkDepositoOrigen As ocxCHK
    Friend WithEvents cbxSucursalDestino As ocxCBX
    Friend WithEvents chkSucursalDestino As ocxCHK
    Friend WithEvents cbxSucursalOrigen As ocxCBX
    Friend WithEvents chkSucursalOrigen As ocxCHK
End Class

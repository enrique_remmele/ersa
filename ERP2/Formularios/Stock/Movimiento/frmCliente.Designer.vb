﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMovimientoModificar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxDepositoEntrada = New ERP.ocxCBX()
        Me.cbxMotivo = New ERP.ocxCBX()
        Me.cbxDepositoSalida = New ERP.ocxCBX()
        Me.cbxTipoOperacion = New ERP.ocxCBX()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblTipoOperacion = New System.Windows.Forms.Label()
        Me.lblDepositoEntrada = New System.Windows.Forms.Label()
        Me.lblMotivo = New System.Windows.Forms.Label()
        Me.lblDepositoSalida = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.PnlTipoMovimiento = New System.Windows.Forms.Panel()
        Me.rdbMateriaPrima = New System.Windows.Forms.RadioButton()
        Me.rdbCompras = New System.Windows.Forms.RadioButton()
        Me.rdbCombustible = New System.Windows.Forms.RadioButton()
        Me.rdbDescargaStock = New System.Windows.Forms.RadioButton()
        Me.rdbMovimiento = New System.Windows.Forms.RadioButton()
        Me.gbxCabecera.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.flpRegistradoPor.SuspendLayout()
        Me.PnlTipoMovimiento.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxDepositoEntrada)
        Me.gbxCabecera.Controls.Add(Me.cbxMotivo)
        Me.gbxCabecera.Controls.Add(Me.cbxDepositoSalida)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblTipoOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblDepositoEntrada)
        Me.gbxCabecera.Controls.Add(Me.lblMotivo)
        Me.gbxCabecera.Controls.Add(Me.lblDepositoSalida)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 12)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(760, 101)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(82, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(76, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(409, 38)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(343, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 13
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(81, 38)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(77, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 10
        Me.cbxTipoComprobante.Texto = ""
        '
        'cbxDepositoEntrada
        '
        Me.cbxDepositoEntrada.CampoWhere = Nothing
        Me.cbxDepositoEntrada.CargarUnaSolaVez = False
        Me.cbxDepositoEntrada.DataDisplayMember = Nothing
        Me.cbxDepositoEntrada.DataFilter = Nothing
        Me.cbxDepositoEntrada.DataOrderBy = Nothing
        Me.cbxDepositoEntrada.DataSource = Nothing
        Me.cbxDepositoEntrada.DataValueMember = Nothing
        Me.cbxDepositoEntrada.dtSeleccionado = Nothing
        Me.cbxDepositoEntrada.FormABM = Nothing
        Me.cbxDepositoEntrada.Indicaciones = Nothing
        Me.cbxDepositoEntrada.Location = New System.Drawing.Point(458, 66)
        Me.cbxDepositoEntrada.Name = "cbxDepositoEntrada"
        Me.cbxDepositoEntrada.SeleccionMultiple = False
        Me.cbxDepositoEntrada.SeleccionObligatoria = True
        Me.cbxDepositoEntrada.Size = New System.Drawing.Size(294, 21)
        Me.cbxDepositoEntrada.SoloLectura = True
        Me.cbxDepositoEntrada.TabIndex = 17
        Me.cbxDepositoEntrada.TabStop = False
        Me.cbxDepositoEntrada.Texto = ""
        '
        'cbxMotivo
        '
        Me.cbxMotivo.CampoWhere = Nothing
        Me.cbxMotivo.CargarUnaSolaVez = False
        Me.cbxMotivo.DataDisplayMember = Nothing
        Me.cbxMotivo.DataFilter = Nothing
        Me.cbxMotivo.DataOrderBy = Nothing
        Me.cbxMotivo.DataSource = Nothing
        Me.cbxMotivo.DataValueMember = Nothing
        Me.cbxMotivo.dtSeleccionado = Nothing
        Me.cbxMotivo.FormABM = Nothing
        Me.cbxMotivo.Indicaciones = Nothing
        Me.cbxMotivo.Location = New System.Drawing.Point(570, 12)
        Me.cbxMotivo.Name = "cbxMotivo"
        Me.cbxMotivo.SeleccionMultiple = False
        Me.cbxMotivo.SeleccionObligatoria = True
        Me.cbxMotivo.Size = New System.Drawing.Size(184, 21)
        Me.cbxMotivo.SoloLectura = False
        Me.cbxMotivo.TabIndex = 8
        Me.cbxMotivo.Texto = ""
        '
        'cbxDepositoSalida
        '
        Me.cbxDepositoSalida.CampoWhere = Nothing
        Me.cbxDepositoSalida.CargarUnaSolaVez = False
        Me.cbxDepositoSalida.DataDisplayMember = Nothing
        Me.cbxDepositoSalida.DataFilter = Nothing
        Me.cbxDepositoSalida.DataOrderBy = Nothing
        Me.cbxDepositoSalida.DataSource = Nothing
        Me.cbxDepositoSalida.DataValueMember = Nothing
        Me.cbxDepositoSalida.dtSeleccionado = Nothing
        Me.cbxDepositoSalida.FormABM = Nothing
        Me.cbxDepositoSalida.Indicaciones = Nothing
        Me.cbxDepositoSalida.Location = New System.Drawing.Point(81, 66)
        Me.cbxDepositoSalida.Name = "cbxDepositoSalida"
        Me.cbxDepositoSalida.SeleccionMultiple = False
        Me.cbxDepositoSalida.SeleccionObligatoria = True
        Me.cbxDepositoSalida.Size = New System.Drawing.Size(294, 21)
        Me.cbxDepositoSalida.SoloLectura = True
        Me.cbxDepositoSalida.TabIndex = 15
        Me.cbxDepositoSalida.TabStop = False
        Me.cbxDepositoSalida.Texto = ""
        '
        'cbxTipoOperacion
        '
        Me.cbxTipoOperacion.CampoWhere = Nothing
        Me.cbxTipoOperacion.CargarUnaSolaVez = False
        Me.cbxTipoOperacion.DataDisplayMember = Nothing
        Me.cbxTipoOperacion.DataFilter = Nothing
        Me.cbxTipoOperacion.DataOrderBy = Nothing
        Me.cbxTipoOperacion.DataSource = Nothing
        Me.cbxTipoOperacion.DataValueMember = Nothing
        Me.cbxTipoOperacion.dtSeleccionado = Nothing
        Me.cbxTipoOperacion.FormABM = Nothing
        Me.cbxTipoOperacion.Indicaciones = Nothing
        Me.cbxTipoOperacion.Location = New System.Drawing.Point(362, 12)
        Me.cbxTipoOperacion.Name = "cbxTipoOperacion"
        Me.cbxTipoOperacion.SeleccionMultiple = False
        Me.cbxTipoOperacion.SeleccionObligatoria = True
        Me.cbxTipoOperacion.Size = New System.Drawing.Size(156, 21)
        Me.cbxTipoOperacion.SoloLectura = True
        Me.cbxTipoOperacion.TabIndex = 6
        Me.cbxTipoOperacion.TabStop = False
        Me.cbxTipoOperacion.Texto = ""
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(16, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 4, 8, 9, 22, 55, 62)
        Me.txtFecha.Location = New System.Drawing.Point(253, 12)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = True
        Me.txtFecha.TabIndex = 4
        Me.txtFecha.TabStop = False
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(159, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(52, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblTipoOperacion
        '
        Me.lblTipoOperacion.AutoSize = True
        Me.lblTipoOperacion.Location = New System.Drawing.Point(328, 16)
        Me.lblTipoOperacion.Name = "lblTipoOperacion"
        Me.lblTipoOperacion.Size = New System.Drawing.Size(31, 13)
        Me.lblTipoOperacion.TabIndex = 5
        Me.lblTipoOperacion.Text = "Tipo:"
        '
        'lblDepositoEntrada
        '
        Me.lblDepositoEntrada.AutoSize = True
        Me.lblDepositoEntrada.Location = New System.Drawing.Point(408, 70)
        Me.lblDepositoEntrada.Name = "lblDepositoEntrada"
        Me.lblDepositoEntrada.Size = New System.Drawing.Size(47, 13)
        Me.lblDepositoEntrada.TabIndex = 16
        Me.lblDepositoEntrada.Text = "Entrada:"
        '
        'lblMotivo
        '
        Me.lblMotivo.AutoSize = True
        Me.lblMotivo.Location = New System.Drawing.Point(525, 16)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(42, 13)
        Me.lblMotivo.TabIndex = 7
        Me.lblMotivo.Text = "Motivo:"
        '
        'lblDepositoSalida
        '
        Me.lblDepositoSalida.AutoSize = True
        Me.lblDepositoSalida.Location = New System.Drawing.Point(36, 70)
        Me.lblDepositoSalida.Name = "lblDepositoSalida"
        Me.lblDepositoSalida.Size = New System.Drawing.Size(39, 13)
        Me.lblDepositoSalida.TabIndex = 14
        Me.lblDepositoSalida.Text = "Salida:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(333, 42)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 12
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(214, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 3
        Me.lblFecha.Text = "Fecha:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(2, 42)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 9
        Me.lblComprobante.Text = "Comprobante:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(159, 39)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(168, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 11
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 268)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(778, 22)
        Me.StatusStrip1.TabIndex = 18
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(17, 233)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 13
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(689, 233)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 17
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(504, 233)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 16
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(423, 233)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 15
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(342, 233)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 14
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(601, 123)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 18
        Me.lblTotal.Text = "Total:"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(17, 197)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 20
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 18
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 15
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 17
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = False
        Me.txtTotal.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotal.Location = New System.Drawing.Point(634, 119)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(130, 21)
        Me.txtTotal.SoloLectura = True
        Me.txtTotal.TabIndex = 19
        Me.txtTotal.TabStop = False
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'PnlTipoMovimiento
        '
        Me.PnlTipoMovimiento.Controls.Add(Me.rdbMateriaPrima)
        Me.PnlTipoMovimiento.Controls.Add(Me.rdbCompras)
        Me.PnlTipoMovimiento.Controls.Add(Me.rdbCombustible)
        Me.PnlTipoMovimiento.Controls.Add(Me.rdbDescargaStock)
        Me.PnlTipoMovimiento.Controls.Add(Me.rdbMovimiento)
        Me.PnlTipoMovimiento.Location = New System.Drawing.Point(12, 118)
        Me.PnlTipoMovimiento.Name = "PnlTipoMovimiento"
        Me.PnlTipoMovimiento.Size = New System.Drawing.Size(583, 24)
        Me.PnlTipoMovimiento.TabIndex = 55
        '
        'rdbMateriaPrima
        '
        Me.rdbMateriaPrima.AutoSize = True
        Me.rdbMateriaPrima.BackColor = System.Drawing.Color.Transparent
        Me.rdbMateriaPrima.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbMateriaPrima.Location = New System.Drawing.Point(435, 3)
        Me.rdbMateriaPrima.Name = "rdbMateriaPrima"
        Me.rdbMateriaPrima.Size = New System.Drawing.Size(132, 17)
        Me.rdbMateriaPrima.TabIndex = 4
        Me.rdbMateriaPrima.TabStop = True
        Me.rdbMateriaPrima.Text = "Mat.Prima Balanceado"
        Me.rdbMateriaPrima.UseVisualStyleBackColor = False
        '
        'rdbCompras
        '
        Me.rdbCompras.AutoSize = True
        Me.rdbCompras.BackColor = System.Drawing.Color.Transparent
        Me.rdbCompras.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbCompras.Location = New System.Drawing.Point(310, 3)
        Me.rdbCompras.Name = "rdbCompras"
        Me.rdbCompras.Size = New System.Drawing.Size(115, 17)
        Me.rdbCompras.TabIndex = 3
        Me.rdbCompras.TabStop = True
        Me.rdbCompras.Text = "Descarga Compras"
        Me.rdbCompras.UseVisualStyleBackColor = False
        '
        'rdbCombustible
        '
        Me.rdbCombustible.AutoSize = True
        Me.rdbCombustible.BackColor = System.Drawing.Color.Transparent
        Me.rdbCombustible.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbCombustible.Location = New System.Drawing.Point(212, 3)
        Me.rdbCombustible.Name = "rdbCombustible"
        Me.rdbCombustible.Size = New System.Drawing.Size(82, 17)
        Me.rdbCombustible.TabIndex = 2
        Me.rdbCombustible.TabStop = True
        Me.rdbCombustible.Text = "Combustible"
        Me.rdbCombustible.UseVisualStyleBackColor = False
        '
        'rdbDescargaStock
        '
        Me.rdbDescargaStock.AutoSize = True
        Me.rdbDescargaStock.BackColor = System.Drawing.Color.Transparent
        Me.rdbDescargaStock.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbDescargaStock.Location = New System.Drawing.Point(91, 3)
        Me.rdbDescargaStock.Name = "rdbDescargaStock"
        Me.rdbDescargaStock.Size = New System.Drawing.Size(102, 17)
        Me.rdbDescargaStock.TabIndex = 1
        Me.rdbDescargaStock.TabStop = True
        Me.rdbDescargaStock.Text = "Descarga Stock"
        Me.rdbDescargaStock.UseVisualStyleBackColor = False
        '
        'rdbMovimiento
        '
        Me.rdbMovimiento.AutoSize = True
        Me.rdbMovimiento.BackColor = System.Drawing.Color.Transparent
        Me.rdbMovimiento.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbMovimiento.Location = New System.Drawing.Point(6, 3)
        Me.rdbMovimiento.Name = "rdbMovimiento"
        Me.rdbMovimiento.Size = New System.Drawing.Size(79, 17)
        Me.rdbMovimiento.TabIndex = 0
        Me.rdbMovimiento.TabStop = True
        Me.rdbMovimiento.Text = "Movimiento"
        Me.rdbMovimiento.UseVisualStyleBackColor = False
        '
        'frmMovimientoModificar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(778, 290)
        Me.Controls.Add(Me.PnlTipoMovimiento)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Name = "frmMovimientoModificar"
        Me.Text = "frmMovimientoModificar"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.PnlTipoMovimiento.ResumeLayout(False)
        Me.PnlTipoMovimiento.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents cbxDepositoEntrada As ERP.ocxCBX
    Friend WithEvents cbxMotivo As ERP.ocxCBX
    Friend WithEvents cbxDepositoSalida As ERP.ocxCBX
    Friend WithEvents cbxTipoOperacion As ERP.ocxCBX
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblTipoOperacion As System.Windows.Forms.Label
    Friend WithEvents lblDepositoEntrada As System.Windows.Forms.Label
    Friend WithEvents lblMotivo As System.Windows.Forms.Label
    Friend WithEvents lblDepositoSalida As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents PnlTipoMovimiento As System.Windows.Forms.Panel
    Friend WithEvents rdbDescargaStock As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMovimiento As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCompras As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCombustible As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMateriaPrima As System.Windows.Forms.RadioButton
End Class

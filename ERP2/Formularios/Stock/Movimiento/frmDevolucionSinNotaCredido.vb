﻿Imports ERP.Reporte
Public Class frmDevolucionSinNotaCredido

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData
    Dim CReporte As New CReporteNotaCredito


    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Public Property tipoComprobante() As String
    Public Property Procesar As Boolean = True
    Public Property Anulado As Boolean = False
    Public Property Visualizar As Boolean = False

    'EVENTOS

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim BloquearDepositoDevolucion As Boolean

    Public dtPuntoExpedicion As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        txtCliente.Conectar()
        'txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente Where Estado='ACTIVO' "
        txtCliente.frm = Me
        txtCliente.MostrarSucursal = True
        BloquearDepositoDevolucion = CSistema.ExecuteScalar("Select BloquearDepositoDevolucion from Configuraciones where IDSucursal = " & vgIDSucursal)
        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "DEVOLUCION SIN NOTA DE CREDITO", "DEVSNC", VGCadenaConexion)
        vNuevo = False
        txtProducto.Conectar()
        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False
        gbxCabecera.BringToFront()

        'Funciones
        CargarInformacion()

        'Foco
        txtID.Focus()

        If IDTransaccion <> 0 Then
            CargarOperacion(IDTransaccion)
        Else
            MoverRegistro(New KeyEventArgs(Keys.End))
        End If

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtCliente)
        CSistema.CargaControl(vControles, txtNumeroSolicitudCliente)

        'Detalle
        'NCDevolucion
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtCosto)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, btnImportar)

        'Detalles
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleDevolucionSinNotaCredito").Clone

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal"), "ID", "Codigo")
        cbxSucursal.SelectedValue(vgIDSucursal)

        'Deposito
        CSistema.SqlToComboBox(cbxDeposito.cbx, "Select ID, Deposito From VDeposito where idsucursal = " & cbxSucursal.cbx.SelectedValue & " Order By IDSucursal,IDTipoDeposito")

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From VDevolucionSinNotaCredito where Anulado = 'False'),1)"), Integer)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As CSistema.NUMHabilitacionBotonesRegistros)

        'Configurar botones
        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnula, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles, btnEliminar)

    End Sub

    Function ExecuteToDataTableXLS(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableXLS = Nothing
        Try
            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
            Dim MiDataSet As New DataSet()
            Dim MiEnlazador As New BindingSource

            Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
            MiConexion.Open()
            MiAdaptador.Fill(MiDataSet)
            Return MiDataSet.Tables(0)

        Catch ex As Exception
            'XtraMessageBox.Show("Verifique el formato del archivo!", "No es posible importar.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return Nothing
        End Try


    End Function


    Function ValidarArchivo(ByVal dt As DataTable) As Boolean

        ValidarArchivo = False

        If dt Is Nothing Then
            MessageBox.Show("El formato del archivo no es correcto! No fue encontrado la hoja: 'Importar'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If


        'Obtener muestra
        Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select 'Codigo'=0,'Cantidad'=0")
        Dim NoEncontrado As Boolean = True

        'Controlar las columnas
        For c1 As Integer = 0 To dt.Columns.Count - 1
            NoEncontrado = True
            For c2 As Integer = 0 To vdt.Columns.Count - 1
                If dt.Columns(c1).ColumnName = vdt.Columns(c2).ColumnName Then
                    NoEncontrado = False
                    GoTo seguir
                End If
            Next

seguir:
            If NoEncontrado = True Then
                MessageBox.Show("El formato del archivo no es correcto! No fue encontrado la columna: " & dt.Columns(c1).ColumnName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If
        Next

        Return True

    End Function

    Sub CargarArchivo()

        Dim txt As New OpenFileDialog
        Dim ColumnaIdentificador As String = "Codigo"
        Dim ColumnaValor As String = "Cantidad"
        Dim dt As DataTable
        txt.Filter = "*.xls | *.xls"
        If txt.ShowDialog <> DialogResult.OK Then
            Me.Close()
        End If

        dt = ExecuteToDataTableXLS("Select " & ColumnaIdentificador & ", " & ColumnaValor & " from [Importar$]", txt.FileName)
        If ValidarArchivo(dt) = False Then
            Me.Close()
            Exit Sub

        End If

        Dim CantidadRowsImportar As Integer = 0
        Dim CantidadRowsError As Integer = 0

        For i = 0 To dt.Rows.Count - 1
            If CargarProductoImportado(dt.Rows(i)(ColumnaIdentificador), dt.Rows(i)(ColumnaValor), i + 1) = False Then
                MessageBox.Show("No se importaron correctamente todos los productos, favor verifique el archivo importado", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
        Next
        ListarDetalle()

    End Sub

    Sub Nuevo()

        IDTransaccion = 0
        Procesar = False
        txtCliente.Conectar()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        ''Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        'Cabecera
        txtFecha.Hoy()
        txtNumeroSolicitudCliente.txt.Text = ""
        txtCliente.Clear()

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM VDevolucionSinNotaCredito Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)
        txtID.SoloLectura = True
        'Poner el foco en el proveedor
        txtID.txt.SelectAll()
        txtID.txt.Focus()

        ObtenerDeposito()

        txtObservacion.SoloLectura = False


    End Sub


    Sub ObtenerDeposito()

        If cbxSucursal.cbx.SelectedValue Is Nothing Then
            Exit Sub
        End If
        Dim dttemp As DataTable
        Dim DepositoRow As DataRow

        If vNuevo Then

            dttemp = CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursal.cbx.SelectedValue & " and TipoDeposito = 'DEVOLUCION'").Copy

            CSistema.SqlToComboBox(cbxDeposito.cbx, dttemp)

            DepositoRow = CData.GetRow("IDSucursal=" & cbxSucursal.cbx.SelectedValue & " and TipoDeposito = 'DEVOLUCION'", "VDeposito")

            cbxDeposito.cbx.Text = DepositoRow("Deposito").ToString
        Else

            dttemp = CData.GetTable("VDeposito", "IDSucursal=" & cbxSucursal.cbx.SelectedValue).Copy

            CSistema.SqlToComboBox(cbxDeposito.cbx, dttemp)

            DepositoRow = CData.GetRow("IDSucursal=" & cbxSucursal.cbx.SelectedValue, "VDeposito")

            cbxDeposito.cbx.Text = DepositoRow("Deposito").ToString
        End If

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Seleccion de Cliente
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If txtCliente.Seleccionado = False Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If txtCliente.Registro Is Nothing Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If IsNumeric(txtCliente.Registro("ID").ToString) = False Then
                Dim mensaje As String = "Seleccione correctamente el cliente!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            'Seleccion de Deposito
            If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el deposito!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If
        End If

        'Existencia en Detalle
        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If dtDetalle.Rows.Count = 0 Then
                Dim mensaje As String = "El documento debe tener por lo menos 1 detalle!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            Dim frm As New frmAgregarMotivoAnulacionNotaCredito
            frm.Text = " Motivo Anulacion NC"
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.ShowDialog(Me)
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        ValidarDocumento = True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID").ToString, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NumeroSolicitudCliente", txtNumeroSolicitudCliente.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "Total"), False), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDeposito.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)


        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)



        Dim MensajeRetorno As String = ""

        'Insertar Registro
        Try


            If CSistema.ExecuteStoreProcedure(param, "SpDevolucionSinNotaCredito", False, False, MensajeRetorno, IDTransaccion) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro si es que se registro
                'If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From DevolucionSinNotaCredito Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                '    param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                '    CSistema.ExecuteStoreProcedure(param, "SpDevolucionSinNotaCredito", False, False, MensajeRetorno, IDTransaccion)
                'End If

                Exit Sub

            End If
        Catch ex As Exception
            Exit Sub
        End Try

        Procesar = True

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            ' CSistema.ExecuteNonQuery("Execute SpDevolucionSinNotaCredito @IDTransaccion= " & IDTransaccion & ", @Operacion ='ANULAR'")

            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            Exit Sub
        End If

        'Guardar detalle
        If IDTransaccion > 0 Then

            Procesar = InsertarDetalle(IDTransaccion)

            'Cargamos el DetalleImpuesto

        End If

        'Procesar
        'CSistema.ExecuteNonQuery("Execute SpDevolucionSinNotaCreditoProcesar @IDTransaccion= " & IDTransaccion & ", @Operacion ='INS'")

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer) As Boolean

        InsertarDetalle = True

        'Recorrer el detalle
        For Each oRow As DataRow In dtDetalle.Rows

            Dim sql As String = ""

            sql = "Insert Into DetalleDevolucionSinNotaCredito(IDTransaccion, IDProducto, ID, Observacion, Cantidad, PrecioUnitario, Total) Values(" _
                                                            & IDTransaccion & "," & oRow("IDProducto").ToString & ", " & oRow("ID").ToString & ",'" & oRow("Observacion").ToString & "'," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad").ToString, True) & "," & CSistema.FormatoMonedaBaseDatos(oRow("PrecioUnitario").ToString, False) & "," & CSistema.FormatoNumeroBaseDatos(oRow("Cantidad") * oRow("PrecioUnitario"), False) & " )"


            CSistema.ExecuteNonQuery(sql)

        Next

    End Function

    Public Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        txtObservacion.SoloLectura = True


        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VDevolucionSinNotaCredito Where Numero = '" & txtID.ObtenerValor & "' and idSucursal = " & cbxSucursal.GetValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            txtID.txt.Focus()
            txtID.txt.SelectAll()
            Exit Sub
        End If
        ObtenerDeposito()

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VDevolucionSinNotaCredito Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select * from VDetalleDevolucionSinNotaCredito Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)
        cbxSucursal.cbx.SelectedValue = oRow("IDSucursal").ToString
        cbxSucursal.cbx.Text = oRow("Suc").ToString
        txtID.SetValue(oRow("Numero").ToString)

        txtCliente.SetValue(oRow("IDCliente").ToString)
        txtCliente.txtRazonSocial.txt.Text = oRow("Cliente").ToString
        txtCliente.txtRUC.txt.Text = oRow("RUC").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxDeposito.SelectedValue(oRow("IDDeposito"))

        cbxDeposito.cbx.Text = oRow("Deposito").ToString

        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtNumeroSolicitudCliente.txt.Text = oRow("NumeroSolicitudCliente").ToString

        'Autorizador
        txtEstadoAutorizacion.txt.Text = oRow("Estado").ToString
        txtUsuarioAutorizador.txt.Text = oRow("NombreUsuarioAprobado").ToString
        Try
            txtFechaAutorizacion.SetValueFromString(CDate(oRow("FechaAprobado").ToString))
        Catch ex As Exception
            txtFechaAutorizacion.txt.Text = oRow("FechaAprobado").ToString
        End Try

        txtObservacionAutorizacion.txt.Text = oRow("ObservacionAutorizador").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("Usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            Anulado = True
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulado").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioAnulado").ToString

            btnEliminar.Visible = True
            btnAnular.Visible = False
        Else
            flpAnuladoPor.Visible = False
            btnEliminar.Visible = False
            btnAnular.Visible = True
        End If

        'Cargamos el detalle
        ListarDetalle()
        dgw.ReadOnly = True

        'Foco
        txtID.txt.Focus()
        txtID.txt.SelectAll()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)


    End Sub

    Sub Cancelar()

        txtID.Enabled = True
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID.SoloLectura = False
        txtID.txt.Focus()

        Dim ID As Integer
        ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From DevolucionSinNotaCredito"), Integer)

        txtID.txt.Text = ID
        txtID.txt.SelectAll()
        CargarOperacion()
        vNuevo = False

    End Sub

    Sub CargarProducto()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito Entrada
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        Else
            If cbxDeposito.cbx.Text = "" Then
                Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If cbxDeposito.cbx.SelectedValue = Nothing Then
                Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Verificar que exista el producto en el deposito de salida
        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        CantidadProducto = txtCantidad.ObtenerValor

        'Si existe, sumar la cantidad
        If dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID")).Count > 0 Then
            Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID"))
            CantidadProducto += Rows(0)("Cantidad")
            Rows(0)("Cantidad") = CantidadProducto
            Rows(0)("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)

        Else

            ' Insertar
            'Cargamos el registro en el detalle
            Dim dRow As DataRow = dtDetalle.NewRow()

            'Obtener Valores
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("Ref") = txtProducto.Registro("Ref").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString

            dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue


            dRow("Cantidad") = CantidadProducto
            dRow("Observacion") = txtObservacionProducto.txt.Text

            'Totales
            dRow("PrecioUnitario") = txtCosto.ObtenerValor
            dRow("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

        End If

        ListarDetalle()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtCosto.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Function CargarProductoImportado(ByVal Codigo As String, ByVal Cantidad As Decimal, ByVal fila As Integer) As Boolean

        CargarProductoImportado = True

        Dim Row As DataRow = CData.GetTable("VProducto").Select("referencia = '" & Codigo & "'")(0)
        'Validar
        ''Seleccion de Producto
        'If txtProducto.Seleccionado = False Then
        '    Dim mensaje As String = "Seleccione correctamente el producto!"
        '    ctrError.SetError(txtProducto, mensaje)
        '    ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'End If

        'Seleccion de Deposito Entrada
        If IsNumeric(cbxDeposito.cbx.SelectedValue) = False Then
            Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
            MessageBox.Show(mensaje)
            Return False
        Else
            If cbxDeposito.cbx.Text = "" Then
                Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                MessageBox.Show(mensaje)
                Return False
            End If

            If cbxDeposito.cbx.SelectedValue = Nothing Then
                Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                MessageBox.Show(mensaje)
                Return False
            End If

        End If

        'Cantidad
        If IsNumeric(Cantidad) = False Then
            Dim mensaje As String = "Error en la fila " & fila & ". La cantidad no es correcta!"
            MessageBox.Show(mensaje)
            Return False
        End If

        If CDec(Cantidad) <= 0 Then
            Dim mensaje As String = "Error en la fila " & fila & ". La cantidad no es correcta!"
            MessageBox.Show(mensaje)
            Return False
        End If

        'Verificar que exista el producto en el deposito de salida
        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        CantidadProducto = Cantidad

        'Si existe, sumar la cantidad
        If dtDetalle.Select(" IDProducto=" & Row("ID")).Count > 0 Then
            Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & Row("ID"))
            CantidadProducto += Rows(0)("Cantidad")
            Rows(0)("Cantidad") = CantidadProducto
            Rows(0)("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)

        Else

            ' Insertar
            'Cargamos el registro en el detalle
            Dim dRow As DataRow = dtDetalle.NewRow()

            'Obtener Valores
            dRow("IDProducto") = Row("ID").ToString
            dRow("Ref") = Row("Ref").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("Descripcion") = Row("Descripcion").ToString

            dRow("IDDeposito") = cbxDeposito.cbx.SelectedValue


            dRow("Cantidad") = CantidadProducto
            dRow("Observacion") = txtObservacionProducto.txt.Text

            'Totales
            dRow("PrecioUnitario") = Row("Costo")
            dRow("Total") = CantidadProducto * CDec(Row("Costo"))

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

        End If

    End Function

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim IDProducto As Integer = dgw.SelectedRows(0).Cells("IDProducto").Value
        Dim dtAux As DataTable = dtDetalle.Clone()

        For Each Row As DataRow In dtDetalle.Rows
            If Row("IDProducto") = IDProducto Then
                GoTo Seguir
            End If
            Dim NewRow As DataRow = dtAux.NewRow()
            For I = 0 To dtDetalle.Columns.Count - 1
                NewRow(I) = Row(I)
            Next
            dtAux.Rows.Add(NewRow)
Seguir:

        Next

        'dtDetalle.Rows(dgw.SelectedRows(0).Index).Delete()
        dtDetalle.Clear()
        dtDetalle = dtAux.Copy()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro
        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Ref").DisplayIndex = 1
        dgw.Columns("Descripcion").DisplayIndex = 2
        dgw.Columns("Cantidad").DisplayIndex = 3
        dgw.Columns("PrecioUnitario").DisplayIndex = 4
        dgw.Columns("Total").DisplayIndex = 5

        dgw.Columns("Ref").Visible = True
        dgw.Columns("Descripcion").Visible = True
        dgw.Columns("Cantidad").Visible = True
        dgw.Columns("PrecioUnitario").Visible = True
        dgw.Columns("Total").Visible = True

        dgw.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"

        dgw.Columns("Ref").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("Cantidad").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("PrecioUnitario").HeaderText = "Costo"

        Total = CSistema.dtSumColumn(dtDetalle, "Total")

        'Calculamos el Total
        txtTotal.txt.Text = Total


    End Sub

    Sub MoverRegistro(ByRef e As System.Windows.Forms.KeyEventArgs)

        If vNuevo = True Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VDevolucionSinNotaCredito Where IDSucursal = " & cbxSucursal.cbx.SelectedValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VDevolucionSinNotaCredito Where IDSucursal = " & cbxSucursal.cbx.SelectedValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub Imprimir()

        Dim Total As Double = txtTotal.ObtenerValor
        Dim NumeroALetra As String = CSistema.NumeroALetra(txtTotal.ObtenerValor)

        Dim Where As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & IDTransaccion

        Dim dtComprobanteVenta As New DataTable
        Dim Comprobantes As String = ""

        For Each oRow As DataRow In dtComprobanteVenta.Rows
            Dim param As New DataTable
            Comprobantes = Comprobantes & oRow("Comprobante").ToString & "    "
        Next

        Dim Path As String = vgImpresionNotaCreditoPath
        CReporte.ImpresionNotaCredito(frm, Where, vgImpresionNotaCreditoPath, vgImpresionFacturaImpresora, NumeroALetra, CSistema.FormatoMoneda(Total, False), Comprobantes)

    End Sub


    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        lblAnulado.Visible = False

        Dim frm As New frmConsultaMovimiento
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.TipoMovimiento = "MovimientoStock"
        FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub Imprimir2(Optional ByVal ImprimirDirecto As Boolean = False)
        Dim CImpresion As New CImpresion
        CImpresion.IDTransaccion = IDTransaccion
        CImpresion.IDFormularioImpresion = CSistema.RetornarValorString(CData.GetRow("Descripcion='NOTA DE CREDITO'", "FormularioImpresion")("IDFormularioImpresion").ToString)
        CImpresion.Impresora = vgImpresionFacturaImpresora
        CImpresion.Test = False
        CImpresion.Inicializar()
        CImpresion.Imprimir()
    End Sub

    Private Sub frmNotaCreditoCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'GuardarInformacion()
    End Sub

    Private Sub frmNotaCreditoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Enter Then

            If dgw.Focused = True Then
                Exit Sub
            End If
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmNotaCreditoCliente_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado

        If vNuevo = True Then
            txtFecha.txt.Focus()
        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)

    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnula.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Close()
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.DEL)
    End Sub

    Private Sub btnAnular_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub txtID_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        MoverRegistro(e)

    End Sub

    Private Sub txtProductoDevolucion_ItemSeleccionado(sender As Object, e As EventArgs) Handles txtProducto.ItemSeleccionado
        txtCantidad.Focus()

    End Sub

    Private Sub txtCosto_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCosto.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As Object, e As EventArgs) Handles cbxSucursal.PropertyChanged
        If Visualizar = False Then
            BloquearDepositoDevolucion = CSistema.ExecuteScalar("Select BloquearDepositoDevolucion from Configuraciones where IDSucursal = " & cbxSucursal.cbx.SelectedValue)
            MoverRegistro(New KeyEventArgs(Keys.End))
        End If

    End Sub


    Private Sub OcxTXTProducto1_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado

        If txtProducto.Seleccionado = True Then

            'Obtenemos el costo
            Dim Costo As Decimal = 0

            Costo = txtProducto.Registro("Costo").ToString()

            txtCosto.txt.Text = Costo
            txtCantidad.Focus()
            If CBool(vgConfiguraciones("MovimientoBloquearCampoCosto").ToString) = True Then

                txtCosto.SoloLectura = True

                If Costo = 0 Then
                    txtCosto.SoloLectura = False
                End If

            Else
                txtCosto.SoloLectura = False
            End If

        End If

    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If txtCosto.SoloLectura = True Then
                CargarProducto()
            End If
        End If
    End Sub

    Private Sub btnImportar_Click(sender As Object, e As EventArgs) Handles btnImportar.Click
        If vNuevo Then
            CargarArchivo()
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDevolucionSinNotaCredido_Activate()
        Me.Refresh()
    End Sub
End Class
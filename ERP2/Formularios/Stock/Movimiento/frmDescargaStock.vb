﻿Imports System.IO
Imports System.Data.SqlClient

Public Class frmDescargaStock
    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CAsiento As New CAsientoDescarga
    Dim CData As New CData
    Dim dtComprobantes As New DataTable
    Dim Seccion As Boolean = False

    'Dim CReporte As New CReporte

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim CancelarTransferencia As Boolean = False
    Dim IDTransaccionTransferenciaEnvio As Integer
    Dim Enviar As Boolean = False
    Dim Recibir As Boolean = False
    Dim IDTransaccionEnvio As Integer = 0

    'SC: 01-09-2021 - Variables
    Dim TotalCreditoAsiento As Decimal = 0
    Dim TotalDebitoAsiento As Decimal = 0
    Dim SaldoAsiento As Decimal = 0
    Dim DesbalanceoAsiento As Boolean = False

    Dim dt As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Compra = True
        txtProducto.ConectarMovimientoStock()

        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'SC: 01-09-2021
        DesbalanceoAsiento = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "DESCARGASTOCK", "DEST")
        IDTransaccion = 0
        vNuevo = False
        lklSeleccionarProductos.Enabled = False

        'Funciones
        CargarInformacion()

        'Clases
        CAsiento.InicializarAsiento()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxTipoOperacion)
        CSistema.CargaControl(vControles, cbxMotivo)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxDepositoSalida)
        CSistema.CargaControl(vControles, cbxDepositoEntrada)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtObservacionProducto)
        CSistema.CargaControl(vControles, cbxUnidad)
        CSistema.CargaControl(vControles, txtCantidad)
        CSistema.CargaControl(vControles, cbxUnidadNegocio)
        CSistema.CargaControl(vControles, cbxDepartamentoEmpresa)
        CSistema.CargaControl(vControles, cbxSeccion)

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleMovimiento").Clone

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR DATOS DE OPERACION
        dtOperaciones = CSistema.ExecuteToDataTable("Select ID, Descripcion, Activo, Entrada, Salida From TipoOperacion Where IDOperacion=" & IDOperacion)
        GenerarTipoOperacion()

        'CARGAR UNIDAD
        cbxUnidad.Items.Add("UNIDAD")
        cbxUnidad.Items.Add("CAJA")
        cbxUnidad.Items.Add("KILOGRAMOS")
        cbxUnidad.Items.Add("BOLSAS")
        cbxUnidad.Items.Add("LITROS")
        cbxUnidad.Items.Add("CM")
        cbxUnidad.Items.Add("METRO")
        cbxUnidad.DropDownStyle = ComboBoxStyle.DropDownList

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Tipo de Operaciones
        CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion=" & IDOperacion)

        'Tipo de Comprobante
        dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dtComprobantes, "ID", "Codigo")

        'Depositos
        dtDepositos = CSistema.ExecuteToDataTable(" Select * From VDeposito Where Activo='True' Order By 2").Copy

        'Deposito de Origen
        cbxDepositoEntrada.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoEntrada.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoEntrada.cbx.DataSource = dtDepositos.Copy

        'Deposito de Destino
        cbxDepositoSalida.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoSalida.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoSalida.cbx.DataSource = dtDepositos.Copy
        'cbxDepositoSalida.cbx.DataSource = CData.FiltrarDataTable(dtDepositos.Copy, " IDSucursal = " & vgIDSucursal)

        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento ")

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo Operacion
        cbxSucursal.SelectedValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "0"))

        'Tipo Operacion
        cbxTipoOperacion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO OPERACION", "")

        'Tipo Motivo
        cbxMotivo.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MOTIVO", "")

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Deposito de Origen
        cbxDepositoSalida.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO ORIGEN", "")

        'Deposito de Destino
        cbxDepositoEntrada.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO DESTINO", "")

        'Unidad
        cbxUnidad.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", "UNIDAD")

        'Unidad de Negocio
        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, CData.GetTable("vUnidadNegocio", "Estado = 1"), "ID", "Descripcion")
        cbxUnidadNegocio.cbx.Text = ""

        'CONFIGURACIONES
        If CBool(vgConfiguraciones("MovimientoBloquearFecha").ToString) = True Then
            If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) = 0 Then
                txtFecha.Enabled = False
            Else
                txtFecha.Enabled = True
            End If

            txtFecha.SetValue(VGFechaHoraSistema)
        Else
            txtFecha.Enabled = True
        End If

        'If cbxTipoOperacion.Texto = "ENTRADA" Then
        '    cbxDepositoEntrada.Texto = True
        'End If

        'If cbxTipoOperacion.Texto = "SALIDA" Then
        '    cbxDepositoSalida.Texto = True
        'End If

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From Movimiento where DescargaStock = 'True'),1)"), Integer)

    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.GetValue)

        'Tipo Operacion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO OPERACION", cbxTipoOperacion.cbx.Text)

        'Motivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MOTIVO", cbxMotivo.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Deposito de Origen
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO ORIGEN", cbxDepositoSalida.cbx.Text)

        'Deposito de Destino
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO DESTINO", cbxDepositoEntrada.cbx.Text)

        'Unidad
        CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "UNIDAD", cbxUnidad.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where Estado = 1 and IDOperacion=" & IDOperacion)

        txtProducto.txt.Text = ""
        txtCantidad.txt.Text = ""
        txtCosto.txt.Text = ""

        'Depositos
        dtDepositos = CSistema.ExecuteToDataTable(" Select * From VDeposito Where Activo='True' and DescargaStock = 'True' and (IDSUcursal= " & cbxSucursal.GetValue & " or IDSucursalDestinoTransito = " & cbxSucursal.GetValue & ")  Order By 2").Copy
        'Deposito de Origen
        cbxDepositoEntrada.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoEntrada.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoEntrada.cbx.DataSource = dtDepositos.Copy

        'Deposito de Destino
        cbxDepositoSalida.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoSalida.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoSalida.cbx.DataSource = dtDepositos.Copy

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle)

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        'SC: 01-09-2021
        DesbalanceoAsiento = False
        TotalCreditoAsiento = 0
        TotalDebitoAsiento = 0
        SaldoAsiento = 0

        cbxTipoOperacion.cbx.Text = ""
        cbxMotivo.cbx.Text = ""
        cbxTipoComprobante.cbx.Text = ""
        txtComprobante.txt.Clear()
        cbxDepositoSalida.cbx.Text = ""
        cbxDepositoSalida.Texto = ""
        cbxDepositoEntrada.cbx.Text = ""
        cbxDepositoEntrada.Texto = ""

        txtObservacion.txt.Clear()
        txtTotal.txt.ResetText()

        CAsiento.Limpiar()
        CAsiento.Inicializar()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM VMovimiento Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        If cbxSucursal.GetValue = 1 Then
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True'")
            'Dim dtDepartamentos As DataTable = CData.GetTable("select ID,Descripcion from vdepartamentoempresa Where Estado= 'True' order by Descripcion")
            CSistema.SqlToComboBox(cbxDepartamentoEmpresa.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamentoEmpresa.cbx.Text = ""
            cbxUnidadNegocio.cbx.Text = ""
            cbxMotivo.cbx.Text = ""
        Else
            Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' and IDSucursal=" & cbxSucursal.GetValue)
            CSistema.SqlToComboBox(cbxDepartamentoEmpresa.cbx, dtDepartamentos, "ID", "Descripcion")
            cbxDepartamentoEmpresa.cbx.Text = ""
            cbxUnidadNegocio.cbx.Text = ""
            cbxMotivo.cbx.Text = ""
        End If
        'Poner el foco en la fecha
        txtFecha.txt.Focus()
        txtFecha.Hoy()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VMovimiento Where DescargaStock = 'True' and IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Verificar que no se transfiera al mismo tipo de deposito y con Transferir = False
        'If cbxDepositoEntrada.Enabled = True And cbxDepositoSalida.Enabled = True Then
        '    If CType(CSistema.ExecuteScalar("Select IsNull((Select IDTipoDeposito from Deposito D join TipoDeposito TD on D.IDTipoDeposito = TD.ID and D.ID = " & cbxDepositoEntrada.GetValue & "),0)"), Integer) = CType(CSistema.ExecuteScalar("Select IsNull((Select IDTipoDeposito from Deposito D join TipoDeposito TD on D.IDTipoDeposito = TD.ID and D.ID = " & cbxDepositoSalida.GetValue & "),0)"), Integer) Then
        '        If CType(CSistema.ExecuteScalar("Select IsNull((Select Transferir from Deposito D join TipoDeposito TD on D.IDTipoDeposito = TD.ID and D.ID = " & cbxDepositoEntrada.GetValue & "),'False')"), Boolean) = False Then
        '            MessageBox.Show("Atencion! No se puede transferir entre estos depositos", "Transferencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
        '            Exit Function
        '        End If
        '    End If
        'End If

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Observacion
        If txtObservacion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese una observacion!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        'Controlar que no se guarden consumos sin asignar UN/DEP/SEC
        If cbxTipoOperacion.cbx.SelectedValue = 4 Then
            If cbxUnidadNegocio.cbx.Text = "" Or cbxDepartamentoEmpresa.cbx.Text = "" Then
                If Seccion = True Then
                    Dim mensaje As String = "Selecione la Unidad de negocio, el Departamento o la Seccion del consumo!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                Else
                    Dim mensaje As String = "Seleccione la Unidad de Negocio o El Departamento del consumo!"
                    ctrError.SetError(btnGuardar, mensaje)
                    ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Function
                End If
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'GenerarAsiento()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoOperacion", cbxTipoOperacion.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivo", cbxMotivo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoSalida", cbxDepositoSalida.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoEntrada", cbxDepositoEntrada.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Autorizacion", "", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotal.ObtenerValor), ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalImpuesto")), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado")), ParameterDirection.Input)

        'Transferencias
        If cbxTipoOperacion.Texto = "TRANSFERENCIA" And CancelarTransferencia = False Then
            CSistema.SetSQLParameter(param, "@Enviar", Enviar, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Recibir", Recibir, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Recibido", Recibir, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionEnvio", IDTransaccionEnvio, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'FA - 26/01/2022 PLAN DE CUENTA
        CSistema.SetSQLParameter(param, "@IDUnidadNegocio", cbxUnidadNegocio.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", cbxDepartamentoEmpresa.GetValue, ParameterDirection.Input)
        If Seccion = True Then
            CSistema.SetSQLParameter(param, "@IDSeccion", cbxSeccion.GetValue, ParameterDirection.Input)
        End If

        'Tipo de movimiento DescargaStock
        CSistema.SetSQLParameter(param, "@DescargaStock", True, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input) 'JGR 20140816
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpMovimiento", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

                'Eliminar Registro

                Exit Sub

            End If

            'Cargamos el DetalleImpuesto
            CDetalleImpuesto.Guardar(IDTransaccion)

            'Cargamos el asiento
            'GenerarAsiento()
            'If CAsiento.dtDetalleAsiento.Rows.Count > 0 Then
            '    CAsiento.IDTransaccion = IDTransaccion
            '    GenerarAsiento()
            '    CAsiento.Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
            'End If

            ' Ejecutar el SpAsientoMovimiento (si asiento no bloqueado)
            If CAsiento.Bloquear = False And cbxTipoComprobante.GetValue <> 71 Then
                CSistema.ExecuteNonQuery(" Execute SpAsientoMovimiento " & IDTransaccion)
            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        'SC: 01-09-2021 -Valida Asiento Desbalanceado al GUARDAR
        TotalCreditoAsiento = CSistema.ExecuteScalar("Select IsNull((Select Credito From VAsiento Where IDTransaccion = " & IDTransaccion & "), 0 )")
        TotalDebitoAsiento = CSistema.ExecuteScalar("Select IsNull((Select Debito From VAsiento Where IDTransaccion = " & IDTransaccion & "), 0 )")
        SaldoAsiento = CSistema.ExecuteScalar("Select IsNull((Select Saldo From VAsiento Where IDTransaccion = " & IDTransaccion & "), 0 )")

        If TotalCreditoAsiento <> TotalDebitoAsiento Then

            DesbalanceoAsiento = True
        ElseIf SaldoAsiento <> 0 Then

            DesbalanceoAsiento = True
        ElseIf TotalDebitoAsiento <> txtTotal.ObtenerValor Then

            DesbalanceoAsiento = True
        ElseIf TotalCreditoAsiento <> txtTotal.ObtenerValor Then

            DesbalanceoAsiento = True
        End If

        If DesbalanceoAsiento = True Then
            Dim mensaje As String = "ASIENTO DESBALANCEADO. FAVOR VERIFICAR! "
            MessageBox.Show(mensaje, "ATENCION", MessageBoxButtons.OK, MessageBoxIcon.Error)
            tsslEstado.Text = mensaje
            VisualizarAsiento()
            'Exit Sub

        End If
        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)

            If CBool(dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)("Salida").ToString) = True Then
                CSistema.SetSQLParameter(param, "@IDDepositoSalida", oRow("IDDepositoSalida").ToString, ParameterDirection.Input)
            End If

            If CBool(dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)("Entrada").ToString) = True Then
                CSistema.SetSQLParameter(param, "@IDDepositoEntrada", oRow("IDDepositoEntrada").ToString, ParameterDirection.Input)
            End If

            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoMonedaBaseDatos(oRow("Cantidad").ToString, True), ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@PrecioUnitario", oRow("PrecioUnitario").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", oRow("TotalImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", oRow("TotalDiscriminado").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Caja", oRow("Caja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@CantidadCaja", oRow("CantidadCaja").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleMovimiento", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If

        Next

    End Function

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

    End Sub

    Sub CargarProducto()

        'Validar
        'Seleccion de Producto

        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Operacion
        If cbxTipoOperacion.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxTipoOperacion.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito Salida
        If cbxDepositoSalida.cbx.Enabled = True Then
            If IsNumeric(cbxDepositoSalida.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                If cbxDepositoSalida.cbx.Text = "" Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

                If cbxDepositoSalida.cbx.SelectedValue = Nothing Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

            End If
        End If

        'Seleccion de Deposito Entrada
        If cbxDepositoEntrada.cbx.Enabled = True Then
            If IsNumeric(cbxDepositoEntrada.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                If cbxDepositoEntrada.cbx.Text = "" Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

                If cbxDepositoEntrada.cbx.SelectedValue = Nothing Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

            End If
        End If

        'Verificar que no se transfiera al mismo tipo de deposito y con Transferir = False
        If cbxDepositoEntrada.Enabled = True And cbxDepositoSalida.Enabled = True And cbxTipoOperacion.cbx.Text = "TRANSFERENCIA" Then
            Dim IDTipoDepositoEntrada As Integer = CSistema.RetornarValorInteger(CData.GetRow(" ID = " & cbxDepositoEntrada.GetValue(), "VDeposito")("IDTipoDeposito").ToString)
            Dim IDTipoDepositoSalida As Integer = CSistema.RetornarValorInteger(CData.GetRow(" ID = " & cbxDepositoSalida.GetValue(), "VDeposito")("IDTipoDeposito").ToString)

            'Validar tipos de depositos
            If IDTipoDepositoEntrada = 0 Or IDTipoDepositoSalida = 0 Then
                MessageBox.Show("Atencion! Uno de los depositos elegidos no tiene definido el tipo de deposito! Se debe primeramente definir para continuar.", "Transferencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                Exit Sub
            End If

            Dim Transferencia As Boolean = CData.GetRow(" ID = " & IDTipoDepositoEntrada, "VTipoDeposito")("Transferir")

            'Si los depositos son de tipos iguales y el tipo de deposito no permite la transferencia
            If IDTipoDepositoEntrada = IDTipoDepositoSalida And Transferencia = False Then
                MessageBox.Show("Atencion! No se puede transferir entre estos depositos", "Transferencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                Exit Sub
            End If

        End If

        'Cantidad
        If IsNumeric(txtCantidad.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtCantidad.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Verificar que exista el producto en el deposito de salida

        Dim ExistenciaActual As Decimal = 0
        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        If cbxUnidad.Text = "UNIDAD" Then
            CantidadProducto = txtCantidad.ObtenerValor
            EsCaja = False
        Else
            CantidadCaja = txtCantidad.ObtenerValor
            CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
            EsCaja = True
        End If
        'SC 08/11/2021 Se comenta el If para que pueda acumular
        'If cbxDepositoSalida.SoloLectura = False Then

        'Obtenemos la cantidad que existe en el deposito el producto seleccionado
        ExistenciaActual = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & txtProducto.Registro("ID").ToString & ", " & cbxDepositoSalida.cbx.SelectedValue & ")")
        Dim vControlarExistencia As Boolean
        Dim vDescargarSinExistencia As Boolean
        vControlarExistencia = CSistema.ExecuteScalar("Select ControlarExistencia from Producto where ID =" & txtProducto.Registro("ID").ToString)
        vDescargarSinExistencia = CSistema.ExecuteScalar("select top(1) DescargaStockSinExistencia from configuraciones")


        'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
        For Each oRow As DataRow In dtDetalle.Rows
            If oRow("IDProducto").ToString = txtProducto.Registro("ID").ToString Then
                CantidadProducto = CantidadProducto + CDec(oRow("Cantidad").ToString)
            End If
        Next

        If cbxDepositoSalida.SoloLectura = False Then
            If CantidadProducto > ExistenciaActual And vControlarExistencia = True Then
                Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If CantidadProducto > ExistenciaActual And vControlarExistencia = False Then
                If vDescargarSinExistencia = False Then
                    Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If
            End If
        End If
        'End If

        'Si existe, sumar la cantidad
        If dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID")).Count > 0 Then
            Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID"))
            Rows(0)("Cantidad") = CantidadProducto
            Rows(0)("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)
            Rows(0)("TotalImpuesto") = 0
            Rows(0)("TotalDiscriminado") = 0
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(Rows(0)("Total").ToString), False, True, Rows(0)("TotalDiscriminado"), Rows(0)("TotalImpuesto"))

            Rows(0)("Caja") = EsCaja.ToString
            Rows(0)("CantidadCaja") = CantidadCaja

        Else

            ' Insertar
            'Cargamos el registro en el detalle
            Dim dRow As DataRow = dtDetalle.NewRow()

            'Obtener Valores
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("Ref") = txtProducto.Registro("Ref").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString

            If Not cbxDepositoEntrada.cbx.SelectedValue Is Nothing Then
                dRow("IDDepositoEntrada") = cbxDepositoEntrada.cbx.SelectedValue
            End If

            If Not cbxDepositoSalida.cbx.SelectedValue Is Nothing Then
                dRow("IDDepositoSalida") = cbxDepositoSalida.cbx.SelectedValue
            End If

            dRow("Cantidad") = CantidadProducto
            dRow("Observacion") = txtObservacionProducto.txt.Text

            'Impuestos
            dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
            dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString

            'Totales
            dRow("PrecioUnitario") = txtCosto.ObtenerValor
            dRow("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)
            dRow("TotalImpuesto") = 0
            dRow("TotalDiscriminado") = 0
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))


            dRow("Caja") = EsCaja.ToString
            dRow("CantidadCaja") = CantidadCaja
            dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

        End If

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtCosto.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtDetalle.Rows(dgw.SelectedRows(0).Index).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro


        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Ref").DisplayIndex = 1
        dgw.Columns("Descripcion").DisplayIndex = 2
        dgw.Columns("Cantidad").DisplayIndex = 3
        dgw.Columns("PrecioUnitario").DisplayIndex = 4
        dgw.Columns("Total").DisplayIndex = 5

        dgw.Columns("Ref").Visible = True
        dgw.Columns("Descripcion").Visible = True
        dgw.Columns("Cantidad").Visible = True
        dgw.Columns("PrecioUnitario").Visible = True
        dgw.Columns("Total").Visible = True

        dgw.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Format = "N0"
        dgw.Columns("Total").DefaultCellStyle.Format = "N0"

        dgw.Columns("Ref").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("Cantidad").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("PrecioUnitario").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Total").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("PrecioUnitario").HeaderText = "Costo"

        Total = CSistema.dtSumColumn(dtDetalle, "Total")


        'Bloqueamos la cabecera si corresponde
        If dtDetalle.Rows.Count > 0 And vNuevo = True Then
            cbxTipoOperacion.cbx.Enabled = False
            cbxMotivo.cbx.Enabled = False
            cbxDepositoEntrada.cbx.Enabled = False
            cbxDepositoSalida.cbx.Enabled = False
        Else
            cbxTipoOperacion.cbx.Enabled = True
            cbxMotivo.cbx.Enabled = True
            cbxTipoOperacion.cbx.SelectedValue = cbxTipoOperacion.GetValue
            cbxDepositoEntrada.cbx.Enabled = True
            cbxDepositoSalida.cbx.Enabled = True
        End If

        If dtDetalle.Rows.Count > 0 Then
            lklSeleccionarProductos.Enabled = False
        Else
            If vNuevo And Recibir Then
                lklSeleccionarProductos.Enabled = True
            End If
        End If

        'Calculamos el Total
        txtTotal.txt.Text = Total


    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpMovimiento", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Sub Imprimir()

        Dim CMovimiento As New Reporte.CReporteStock
        CMovimiento.MovimientoStock(IDTransaccion)

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        lblAnulado.Visible = False

        Dim frm As New frmConsultaMovimiento
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.TipoMovimiento = "DescargaStock"
        FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False
        lklSeleccionarProductos.Enabled = False
        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()
        'Tipo de Comprobante
        dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dtComprobantes, "ID", "Codigo")

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VMovimiento Where DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        'Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Numero, Fecha, Operacion, Motivo, [Cod.],  NroComprobante, DepositoEntrada, DepositoSalida, Observacion, Autorizacion, Total, Anulado, UsuarioIdentificacionAnulacion, FechaAnulacion, Usuario, UsuarioIdentificador, FechaTransaccion,IDUnidadNegocio,UnidadNegocio,IDDepartamento,Departamento From VMovimiento Where IDTransaccion=" & IDTransaccion)
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * From VMovimiento Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select IDProducto, ID, Ref, 'Descripcion'=Producto, IDDepositoEntrada, IDDepositoSalida, Cantidad, Observacion, PrecioUnitario, IDImpuesto, Impuesto, Total, TotalImpuesto, TotalDiscriminado, Caja, CantidadCaja, CodigoBarra From VDetalleMovimiento Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)


        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxTipoOperacion.cbx.Text = oRow("Operacion").ToString
        cbxMotivo.cbx.Text = oRow("Motivo").ToString
        cbxTipoComprobante.Texto = oRow("Cod.").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxDepositoEntrada.cbx.Text = oRow("DepositoEntrada").ToString
        cbxDepositoSalida.cbx.Text = oRow("DepositoSalida").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtTotal.txt.Text = oRow("Total").ToString

        If oRow("DepositoEntrada").ToString = "---" Then
            cbxDepositoEntrada.txt.Text = ""
        End If
        If oRow("DepositoSalida").ToString = "---" Then
            cbxDepositoSalida.txt.Text = ""
        End If

        cbxUnidadNegocio.cbx.Text = oRow("UnidadNegocio").ToString
        cbxDepartamentoEmpresa.cbx.Text = oRow("Departamento").ToString
        cbxSeccion.cbx.Text = oRow("Seccion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        'lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString
        lblUsuarioRegistro.Text = oRow("Usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos el Asiento
        CAsiento.Limpiar()

    End Sub

    Sub VisualizarAsiento()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Si es nuevo
        If vNuevo = False Then

            Dim frm As New frmVisualizarAsiento
            frm.WindowState = FormWindowState.Normal
            frm.StartPosition = FormStartPosition.CenterScreen
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            frm.Text = "MOVIMIENTO " & cbxTipoOperacion.cbx.Text & " - " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text

            Dim IDTransaccion As Integer = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VMovimiento Where Numero=" & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.GetValue & " Order By IDTransaccion Desc), 0 )")
            frm.IDTransaccion = IDTransaccion

            'Mostramos
            frm.ShowDialog(Me)

        Else
            MessageBox.Show("Debe Guardar la Operacion para visualizar el asiento", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            '    If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
            '        Dim mensaje As String = "Seleccione correctamente el tipo de comprobante!"
            '        ctrError.SetError(cbxTipoComprobante, mensaje)
            '        ctrError.SetIconAlignment(cbxTipoComprobante, ErrorIconAlignment.TopLeft)
            '        tsslEstado.Text = mensaje
            '        Exit Sub
            '    End If

            '    Dim frm As New frmAsiento
            '    frm.WindowState = FormWindowState.Normal
            '    frm.StartPosition = FormStartPosition.CenterScreen
            '    frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
            '    frm.Text = "MOVIMIENTO " & cbxTipoOperacion.cbx.Text & " - " & cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
            '    GenerarAsiento()

            '    frm.CAsiento.dtAsiento = CAsiento.dtAsiento
            '    CAsiento.ListarDetalle(frm.dgv)
            '    frm.CalcularTotales()

            '    frm.CAsiento.dtDetalleAsiento = CAsiento.dtDetalleAsiento

            '    'Mostramos
            '    frm.ShowDialog(Me)

            '    'Actualizamos el asiento si es que este tuvo alguna modificacion
            '    CAsiento.dtAsiento = frm.CAsiento.dtAsiento
            '    CAsiento.dtDetalleAsiento = frm.CAsiento.dtDetalleAsiento
            '    CAsiento.Bloquear = frm.CAsiento.Bloquear
            '    CAsiento.CajaHabilitada = frm.CAsiento.CajaHabilitada
            '    CAsiento.NroCaja = frm.CAsiento.NroCaja
            '    CAsiento.IDCentroCosto = frm.CAsiento.IDCentroCosto

            '    If frm.VolverAGenerar = True Then
            '        CAsiento.Generado = False
            '        CAsiento.dtAsiento.Clear()
            '        CAsiento.dtDetalleAsiento.Clear()
            '        CAsiento.dtDetalleProductos.Clear()
            '        VisualizarAsiento()
            '    End If

        End If


    End Sub

    'Sub GenerarAsiento()

    '    'Establecer Cabecera
    '    Dim oRow As DataRow = CAsiento.dtAsiento.NewRow

    '    Dim oRowDepositos As DataRow = dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)

    '    If CBool(oRowDepositos("Salida").ToString) = True Then
    '        oRow("IDDeposito") = cbxDepositoSalida.cbx.SelectedValue
    '        oRow("IDSucursal") = dtDepositos.Select("ID=" & cbxDepositoSalida.cbx.SelectedValue)(0)("IDSucursal").ToString
    '        oRow("IDCiudad") = dtDepositos.Select(" ID=" & cbxDepositoSalida.cbx.SelectedValue)(0)("IDCiudad").ToString
    '    End If

    '    If CBool(oRowDepositos("Entrada").ToString) = True Then
    '        oRow("IDDeposito") = cbxDepositoEntrada.cbx.SelectedValue
    '        oRow("IDSucursal") = dtDepositos.Select("ID=" & cbxDepositoEntrada.cbx.SelectedValue)(0)("IDSucursal").ToString
    '        oRow("IDCiudad") = dtDepositos.Select(" ID=" & cbxDepositoEntrada.cbx.SelectedValue)(0)("IDCiudad").ToString
    '    End If

    '    Debug.Print(dtDepositos.Select("ID=" & cbxDepositoEntrada.cbx.SelectedValue)(0)("IDSucursal").ToString)

    '    oRow("Fecha") = txtFecha.GetValue
    '    oRow("IDMoneda") = 1
    '    oRow("Cotizacion") = 1
    '    oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
    '    oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
    '    oRow("NroComprobante") = txtComprobante.txt.Text
    '    oRow("Comprobante") = cbxTipoComprobante.cbx.Text & " " & txtComprobante.txt.Text
    '    oRow("Detalle") = txtObservacion.txt.Text
    '    oRow("Total") = txtTotal.ObtenerValor

    '    CAsiento.dtAsiento.Rows.Clear()
    '    CAsiento.dtAsiento.Rows.Add(oRow)

    '    CAsiento.dtDetalleProductos = dtDetalle
    '    CAsiento.IDSucursal = oRow("IDSucursal").ToString
    '    CAsiento.IDMoneda = 1
    '    CAsiento.IDTipoOperacion = cbxTipoOperacion.GetValue

    '    CAsiento.Generar()

    'End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From VMovimiento Where Num > " & ID & " and DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID)
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), " & ID & ") From VMovimiento Where Num < " & ID & " and DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), " & ID & ") From VMovimiento Where DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), " & ID & ") From VMovimiento Where DescargaStock = 'True' and IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub

    Sub GenerarTipoOperacion()

        'Validar si ya existe
        If dtOperaciones.Rows.Count > 0 Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = SQL & "Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida) Values(1, " & IDOperacion & ", 'ENTRADA', 1, 1, 0)" & vbCrLf
        SQL = SQL & "Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida) Values(2, " & IDOperacion & ", 'SALIDA', 1, 0, 1)" & vbCrLf
        SQL = SQL & "Insert Into TipoOperacion(ID, IDOperacion, Descripcion, Activo, Entrada, Salida) Values(3, " & IDOperacion & ", 'TRANSFERENCIA', 1, 1, 1)" & vbCrLf

        CSistema.ExecuteNonQuery(SQL)

        'Volvemos a cargar
        dtOperaciones = CSistema.ExecuteToDataTable("Select ID, Descripcion, Activo, Entrada, Salida From TipoOperacion Where IDOperacion=" & IDOperacion)

    End Sub

    Sub Importar()


        'Dim txt As New OpenFileDialog
        ''txt.Filter = "*.xls | *.xlsx"
        'txt.Filter = "*.xls |"
        'If txt.ShowDialog <> DialogResult.OK Then
        '    Me.Close()
        'End If

        'dt = ExecuteToDataTableXLS("Select * from [Datos$]", txt.FileName)

        'If ValidarArchivo(dt) = False Then
        '    Me.Close()
        '    Exit Sub
        'End If
        'If OpenFileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
        '    CargarPlanillaImportada()
        'End If

        OpenFileDialog1.Filter = "Text files (*.txt)|*.txt"
        OpenFileDialog1.RestoreDirectory = True

        Try
            If OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                CargarPlanillaImportada()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

    End Sub

    Function ValidarArchivo(ByVal dtImportado As DataTable) As Boolean

        ValidarArchivo = False

        If dtImportado Is Nothing Then
            MessageBox.Show("El formato del archivo no es correcto! No fue encontrado la hoja: 'Datos'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        'Obtener muestra
        Dim vdt As DataTable = CSistema.ExecuteToDataTable("Select Referencia='',Cantidad='',Costo=''")
        Dim NoEncontrado As Boolean = True

        'Controlar las columnas
        For c1 As Integer = 0 To vdt.Columns.Count - 1
            NoEncontrado = True
            For c2 As Integer = 0 To dtImportado.Columns.Count - 1
                If dtImportado.Columns(c2).ColumnName = vdt.Columns(c1).ColumnName Then
                    NoEncontrado = False
                    GoTo seguir
                End If
            Next

seguir:
            If NoEncontrado = True Then
                MessageBox.Show("El formato del archivo no es correcto! No fue encontrado la columna: " & vdt.Columns(c1).ColumnName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If
        Next

        Return True

    End Function

    Function ExecuteToDataTableXLS(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableXLS = Nothing
        Try
            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If
            '            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
            Dim MiDataSet As New DataSet()
            Dim MiEnlazador As New BindingSource

            Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
            MiConexion.Open()
            MiAdaptador.Fill(MiDataSet)
            Return MiDataSet.Tables(0)

        Catch ex As Exception
            'XtraMessageBox.Show("Verifique el formato del archivo!", "No es posible importar.", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return Nothing
        End Try


    End Function

    Sub CargarProductoAutomatico()

        'Operacion
        If cbxTipoOperacion.cbx.SelectedValue = Nothing Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If cbxTipoOperacion.cbx.Text = "" Then
            Dim mensaje As String = "Seleccione correctamente el tipo de operacion!"
            ctrError.SetError(txtCantidad, mensaje)
            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito Salida
        If cbxDepositoSalida.cbx.Enabled = True Then
            If IsNumeric(cbxDepositoSalida.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                If cbxDepositoSalida.cbx.Text = "" Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

                If cbxDepositoSalida.cbx.SelectedValue = Nothing Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

            End If
        End If

        'Seleccion de Deposito Entrada
        If cbxDepositoEntrada.cbx.Enabled = True Then
            If IsNumeric(cbxDepositoEntrada.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                If cbxDepositoEntrada.cbx.Text = "" Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

                If cbxDepositoEntrada.cbx.SelectedValue = Nothing Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de entrada!"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

            End If
        End If

        'Verificar que no se transfiera al mismo tipo de deposito y con Transferir = False
        If cbxDepositoEntrada.Enabled = True And cbxDepositoSalida.Enabled = True And cbxTipoOperacion.cbx.Text = "TRANSFERENCIA" Then
            Dim IDTipoDepositoEntrada As Integer = CSistema.RetornarValorInteger(CData.GetRow(" ID = " & cbxDepositoEntrada.GetValue(), "VDeposito")("IDTipoDeposito").ToString)
            Dim IDTipoDepositoSalida As Integer = CSistema.RetornarValorInteger(CData.GetRow(" ID = " & cbxDepositoSalida.GetValue(), "VDeposito")("IDTipoDeposito").ToString)

            'Validar tipos de depositos
            If IDTipoDepositoEntrada = 0 Or IDTipoDepositoSalida = 0 Then
                MessageBox.Show("Atencion! Uno de los depositos elegidos no tiene definido el tipo de deposito! Se debe primeramente definir para continuar.", "Transferencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                Exit Sub
            End If

            Dim Transferencia As Boolean = CData.GetRow(" ID = " & IDTipoDepositoEntrada, "VTipoDeposito")("Transferir")

            'Si los depositos son de tipos iguales y el tipo de deposito no permite la transferencia
            If IDTipoDepositoEntrada = IDTipoDepositoSalida And Transferencia = False Then
                MessageBox.Show("Atencion! No se puede transferir entre estos depositos", "Transferencia", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2)
                Exit Sub
            End If

        End If

        ''Cantidad
        'If IsNumeric(txtCantidad.ObtenerValor) = False Then
        '    Dim mensaje As String = "La cantidad no es correcta!"
        '    ctrError.SetError(txtCantidad, mensaje)
        '    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'End If

        'If CDec(txtCantidad.ObtenerValor) <= 0 Then
        '    Dim mensaje As String = "La cantidad no es correcta!"
        '    ctrError.SetError(txtCantidad, mensaje)
        '    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'End If

        'For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Comprobante = '" & Row("Comprobante Venta") & "'")
        For Each oRow As DataRow In dt.Select("")

        Next
        'Verificar que exista el producto en el deposito de salida

        Dim ExistenciaActual As Decimal = 0
        Dim CantidadProducto As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        If cbxUnidad.Text = "UNIDAD" Then
            CantidadProducto = txtCantidad.ObtenerValor
            EsCaja = False
        Else
            CantidadCaja = txtCantidad.ObtenerValor
            CantidadProducto = CInt(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
            EsCaja = True
        End If
        'SC 08/11/2021 Se comenta el If para que pueda acumular
        'If cbxDepositoSalida.SoloLectura = False Then

        'Obtenemos la cantidad que existe en el deposito el producto seleccionado
        ExistenciaActual = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & txtProducto.Registro("ID").ToString & ", " & cbxDepositoSalida.cbx.SelectedValue & ")")
        Dim vControlarExistencia As Boolean
        Dim vDescargarSinExistencia As Boolean
        vControlarExistencia = CSistema.ExecuteScalar("Select ControlarExistencia from Producto where ID =" & txtProducto.Registro("ID").ToString)
        vDescargarSinExistencia = CSistema.ExecuteScalar("select top(1) DescargaStockSinExistencia from configuraciones")


        'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
        For Each oRow As DataRow In dtDetalle.Rows
            If oRow("IDProducto").ToString = txtProducto.Registro("ID").ToString Then
                CantidadProducto = CantidadProducto + CDec(oRow("Cantidad").ToString)
            End If
        Next

        If cbxDepositoSalida.SoloLectura = False Then
            If CantidadProducto > ExistenciaActual And vControlarExistencia = True Then
                Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
                ctrError.SetError(txtCantidad, mensaje)
                ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If

            If CantidadProducto > ExistenciaActual And vControlarExistencia = False Then
                If vDescargarSinExistencia = False Then
                    Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
                    ctrError.SetError(txtCantidad, mensaje)
                    ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If
            End If
        End If
        'End If

        'Si existe, sumar la cantidad
        If dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID")).Count > 0 Then
            Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID"))
            Rows(0)("Cantidad") = CantidadProducto
            Rows(0)("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)
            Rows(0)("TotalImpuesto") = 0
            Rows(0)("TotalDiscriminado") = 0
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(Rows(0)("Total").ToString), False, True, Rows(0)("TotalDiscriminado"), Rows(0)("TotalImpuesto"))

            Rows(0)("Caja") = EsCaja.ToString
            Rows(0)("CantidadCaja") = CantidadCaja

        Else

            ' Insertar
            'Cargamos el registro en el detalle
            Dim dRow As DataRow = dtDetalle.NewRow()

            'Obtener Valores
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("Ref") = txtProducto.Registro("Ref").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString

            If Not cbxDepositoEntrada.cbx.SelectedValue Is Nothing Then
                dRow("IDDepositoEntrada") = cbxDepositoEntrada.cbx.SelectedValue
            End If

            If Not cbxDepositoSalida.cbx.SelectedValue Is Nothing Then
                dRow("IDDepositoSalida") = cbxDepositoSalida.cbx.SelectedValue
            End If

            dRow("Cantidad") = CantidadProducto
            dRow("Observacion") = txtObservacionProducto.txt.Text

            'Impuestos
            dRow("IDImpuesto") = txtProducto.Registro("IDImpuesto").ToString
            dRow("Impuesto") = txtProducto.Registro("Impuesto").ToString

            'Totales
            dRow("PrecioUnitario") = txtCosto.ObtenerValor
            dRow("Total") = CantidadProducto * CDec(txtCosto.ObtenerValor)
            dRow("TotalImpuesto") = 0
            dRow("TotalDiscriminado") = 0
            CSistema.CalcularIVA(txtProducto.Registro("IDImpuesto").ToString, CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))


            dRow("Caja") = EsCaja.ToString
            dRow("CantidadCaja") = CantidadCaja
            dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString

            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

        End If

        ListarDetalle()
        CDetalleImpuesto.EstablecerImpuestosDetalle(dtDetalle, False)

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCantidad.txt.Text = "0"
        txtCosto.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub CargarPlanillaImportada()
        Dim filebyte As Byte() = Nothing
        Dim line As String = System.IO.File.ReadAllText(OpenFileDialog1.FileName)
        Dim partes As String() = line.Split(New [Char]() {CChar(vbTab), " "c, Environment.NewLine})
        Dim vReferenciaProducto As String
        Dim vCantidad As Decimal
        Dim vCosto As Decimal
        Dim vTotalCosto As Decimal

        Try
            For i As Integer = 0 To partes.Length


                vReferenciaProducto = partes(i).Trim
                i = i + 1
                vCantidad = partes(i).Trim
                i = i + 1
                vCosto = partes(i)
                i = i + 1
                vTotalCosto = partes(i)

                Dim dtProducto As DataTable = CSistema.ExecuteToDataTable("Select * from vproducto where referencia = '" & vReferenciaProducto & "'")

                'Verificar que exista el producto en el deposito de salida

                Dim ExistenciaActual As Decimal = 0
                Dim CantidadProducto As Decimal = 0
                Dim CantidadCaja As Integer = 0
                Dim EsCaja As Boolean = False

                'Si es por unidad, cantidad igual a seleccionado
                'Si es por caja, multiplicar
                If cbxUnidad.Text = "UNIDAD" Then
                    CantidadProducto = vCantidad
                    EsCaja = False
                Else
                    CantidadCaja = vCantidad
                    CantidadProducto = CInt(dtProducto.Rows(0)("UnidadPorCaja")) * CantidadCaja
                    EsCaja = True
                End If
                'SC 08/11/2021 Se comenta el If para que pueda acumular
                'If cbxDepositoSalida.SoloLectura = False Then

                'Obtenemos la cantidad que existe en el deposito el producto seleccionado
                ExistenciaActual = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & dtProducto.Rows(0)("ID") & ", " & cbxDepositoSalida.cbx.SelectedValue & ")")
                Dim vControlarExistencia As Boolean
                Dim vDescargarSinExistencia As Boolean
                vControlarExistencia = CSistema.ExecuteScalar("Select ControlarExistencia from Producto where ID =" & dtProducto.Rows(0)("ID"))
                vDescargarSinExistencia = CSistema.ExecuteScalar("select top(1) DescargaStockSinExistencia from configuraciones")


                'Obtenemos la cantidad de productos incluyendo los que estan en el detalle
                For Each oRow As DataRow In dtDetalle.Rows
                    If oRow("IDProducto").ToString = dtProducto.Rows(0)("ID") Then
                        CantidadProducto = CantidadProducto + CDec(oRow("Cantidad").ToString)
                    End If
                Next

                If cbxDepositoSalida.SoloLectura = False Then
                    If CantidadProducto > ExistenciaActual And vControlarExistencia = True Then
                        Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
                        ctrError.SetError(txtCantidad, mensaje)
                        ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                        tsslEstado.Text = mensaje
                        Exit Sub
                    End If

                    If CantidadProducto > ExistenciaActual And vControlarExistencia = False Then
                        If vDescargarSinExistencia = False Then
                            Dim mensaje As String = "Stock insuficiente! El sistema tiene " & ExistenciaActual & " producto(s) registrado(s)"
                            ctrError.SetError(txtCantidad, mensaje)
                            ctrError.SetIconAlignment(txtCantidad, ErrorIconAlignment.MiddleRight)
                            tsslEstado.Text = mensaje
                            Exit Sub
                        End If
                    End If
                End If
                'End If

                'Si existe, sumar la cantidad
                'If dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID")).Count > 0 Then
                'If dtDetalle.Select(" IDProducto=" & dtProducto.Rows(0)("ID")) <> "" Then
                '    Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & dtProducto.Rows(0)("ID"))
                '    Rows(0)("Cantidad") = CantidadProducto
                '    Rows(0)("Total") = CantidadProducto * CDec(vCosto)
                '    Rows(0)("TotalImpuesto") = 0
                '    Rows(0)("TotalDiscriminado") = 0
                '    CSistema.CalcularIVA(dtProducto.Rows(0)("IDImpuesto"), CDec(Rows(0)("Total").ToString), False, True, Rows(0)("TotalDiscriminado"), Rows(0)("TotalImpuesto"))

                '    Rows(0)("Caja") = EsCaja.ToString
                '    Rows(0)("CantidadCaja") = CantidadCaja

                'Else
                'strSql = "Exec SpImportarClientes @RazonSocial ='" & partes(i + 1).ToString.Replace("'", "").Trim & "', @RUC='" & partes(i).ToString.Trim & "-" & DigitoVerificador(partes(i).ToString.Trim) & "', @Referencia='" & partes(i).ToString.Trim & "'"
                'Cargamos el registro en el detalle


                If dtProducto Is Nothing Then
                        GoTo Seguir
                    End If

                    If dtProducto.Rows.Count = 0 Then
                        GoTo Seguir
                    End If

                    Dim dRow As DataRow = dtDetalle.NewRow()

                    'Obtener Valores
                    dRow("IDProducto") = dtProducto.Rows(0)("ID")
                    dRow("Ref") = vReferenciaProducto
                    dRow("ID") = dtDetalle.Rows.Count
                    dRow("Descripcion") = dtProducto.Rows(0)("Descripcion")

                    If Not cbxDepositoEntrada.cbx.SelectedValue Is Nothing Then
                        dRow("IDDepositoEntrada") = cbxDepositoEntrada.cbx.SelectedValue
                    End If

                    If Not cbxDepositoSalida.cbx.SelectedValue Is Nothing Then
                        dRow("IDDepositoSalida") = cbxDepositoSalida.cbx.SelectedValue
                    End If

                    dRow("Observacion") = ""
                    'dRow("CostoAnterior") = dtProducto.Rows(0)("costopromedio")
                    dRow("Cantidad") = vCantidad
                    dRow("PrecioUnitario") = vCosto
                    dRow("CodigoBarra") = dtProducto.Rows(0)("codigobarra")

                    'Impuestos
                    dRow("IDImpuesto") = dtProducto.Rows(0)("IDImpuesto")
                    dRow("Impuesto") = dtProducto.Rows(0)("Impuesto")

                    'Totales
                    dRow("Total") = vCantidad * CDec(vCosto)
                    dRow("TotalImpuesto") = 0
                    dRow("TotalDiscriminado") = 0
                    CSistema.CalcularIVA(dtProducto.Rows(0)("IDImpuesto"), CDec(dRow("Total").ToString), False, True, dRow("TotalDiscriminado"), dRow("TotalImpuesto"))

                    'Agregamos al detalle
                    dtDetalle.Rows.Add(dRow)
                'End If

Seguir:

            Next

            ' MessageBox.Show("Se ha guardado correctamente", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch Ex As Exception
            'MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally

        End Try


        ListarDetalle()

        'Inicializamos los valores
        txtObservacionProducto.txt.Clear()
        txtCosto.txt.Text = "0"
        txtProducto.txt.Clear()

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""


    End Sub

    Private Sub txtProducto_ItemSeleccionado(sender As System.Object, e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then

            'Obtenemos el costo
            Dim Costo As Decimal = 0
            If cbxTipoComprobante.cbx.SelectedValue = CInt(vgConfiguraciones("IDTipoComprobanteImportacion")) Then
                txtCosto.txt.Text = 0
                txtCosto.SoloLectura = False
            Else
                Costo = txtProducto.Registro("Costo").ToString()
            End If

            If Costo = 0 Then
                Costo = txtProducto.Registro("Costo").ToString()
            End If

            txtCosto.txt.Text = Costo
            cbxUnidad.Focus()

            If CBool(vgConfiguraciones("MovimientoBloquearCampoCosto").ToString) = True Then

                txtCosto.SoloLectura = True

                If Costo = 0 Then
                    txtCosto.SoloLectura = False
                End If

            Else
                txtCosto.SoloLectura = False
            End If

        End If
    End Sub

    Private Sub frmDescargaStock_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GuardarInformacion()
        LiberarMemoria()
    End Sub

    Private Sub frmDescargaStock_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = vgKeyConsultar Then

            'Si esta en el producto
            If txtProducto.Focus = True Then

                'Transferencia
                If cbxDepositoEntrada.SoloLectura = False And cbxDepositoSalida.SoloLectura = False Then
                    txtProducto.Consulta = "Select Top(100) ID, Descripcion, 'Codigo de Barra'=CodigoBarra, Ref, 'Tipo de Producto'=TipoProducto, 'Dep. Entrada'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoEntrada.cbx.SelectedValue & ")), 'Dep. Salida'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoSalida.cbx.SelectedValue & ")) From VProducto Where ID>0"
                    txtProducto.IDDeposito = cbxDepositoSalida.cbx.SelectedValue
                    txtProducto.ColumnasNumericas = {"5", "6"}
                    Exit Sub
                End If

                'Entrada
                If cbxDepositoEntrada.SoloLectura = False And cbxDepositoSalida.SoloLectura = True Then
                    txtProducto.Consulta = "Select Top(100) ID, Descripcion, 'Codigo de Barra'=CodigoBarra, Ref, 'Tipo de Producto'=TipoProducto, 'Dep. Entrada'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoEntrada.cbx.SelectedValue & ")) From VProducto Where ID>0 "
                    txtProducto.IDDeposito = cbxDepositoEntrada.GetValue
                    txtProducto.ColumnasNumericas = {"5"}
                    Exit Sub
                End If

                'Salida
                'If cbxDepositoEntrada.SoloLectura = False And cbxDepositoSalida.SoloLectura = False Then
                '    txtProducto.Consulta = "Select Top(100) ID, Descripcion, 'Codigo de Barra'=CodigoBarra, Ref, 'Tipo de Producto'=TipoProducto, 'Dep. Salida'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoSalida.cbx.SelectedValue & ")) From VProducto Where ID>0 "
                '    txtProducto.IDDeposito = cbxDepositoSalida.cbx.SelectedValue
                '    txtProducto.ColumnasNumericas = {"5"}
                '    Exit Sub
                'End If

                'SC 08/11/2021
                If cbxDepositoEntrada.SoloLectura = True And cbxDepositoSalida.SoloLectura = False Then
                    txtProducto.Consulta = "Select Top(100) ID, Descripcion, 'Codigo de Barra'=CodigoBarra, Ref, 'Tipo de Producto'=TipoProducto, 'Dep. Salida'=(Select dbo.FExistenciaProducto(ID, " & cbxDepositoSalida.cbx.SelectedValue & ")) From VProducto Where ID>0 "
                    txtProducto.IDDeposito = cbxDepositoSalida.cbx.SelectedValue
                    txtProducto.ColumnasNumericas = {"5"}
                    Exit Sub
                End If
            End If

            Buscar()

            Exit Sub

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmDescargaStock_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(sender As System.Object, e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnAnular_Click(sender As System.Object, e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If txtCosto.SoloLectura = True Then
                CargarProducto()
            End If
        End If
    End Sub

    Private Sub cbxTipoOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxTipoOperacion.PropertyChanged
        If vNuevo = False Then
            Exit Sub
        End If


        cbxMotivo.cbx.DataSource = Nothing

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If CBool(vgConfiguraciones("RecibirTransferenciasTransitoBloqueada").ToString) Then

            If cbxTipoOperacion.Texto = "TRANSFERENCIA" And CancelarTransferencia = False Then

                Dim frm As New frmConsultarOperacionTransferencia
                frm.StartPosition = FormStartPosition.CenterScreen
                frm.ShowDialog()

                If frm.Enviar Then
                    Enviar = True
                End If

                If frm.Recibir Then
                    txtProducto.Enabled = False
                    txtObservacionProducto.Enabled = False
                    cbxUnidad.Enabled = False
                    txtCantidad.Enabled = False
                    txtCosto.Enabled = False
                    lklSeleccionarProductos.Enabled = True
                    Recibir = True
                Else
                    txtProducto.Enabled = True
                    txtObservacionProducto.Enabled = True
                    cbxUnidad.Enabled = True
                    txtCantidad.Enabled = True
                    txtCosto.Enabled = True
                    lklSeleccionarProductos.Enabled = False
                End If

                If frm.Cancelar Then
                    CancelarTransferencia = True
                    cbxTipoOperacion.Texto = "ENTRADA"
                    cbxTipoOperacion.cbx.Text = "ENTRADA"
                End If

                CancelarTransferencia = False
            Else
                lklSeleccionarProductos.Enabled = False
            End If

        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue & "and IDSucursal = " & cbxSucursal.cbx.SelectedValue & " and Activo = 1 and IDTipoComprobante = " & cbxTipoComprobante.GetValue)

        If vNuevo Then
            'Tipo de Comprobante
            dtComprobantes = CSistema.ExecuteToDataTable("Select 'ID'=IDTipoComprobante , 'Codigo'=CodigoComprobante, 'Descripcion'=TipoComprobante From VOperacionPermitida Where IDOperacion=" & IDOperacion & " and IDTipoOperacion = " & cbxTipoOperacion.cbx.SelectedValue)
            'dtComprobantes = CSistema.ExecuteToDataTable("Select ID, Codigo, Descripcion From TipoComprobante Where IDOperacion=" & IDOperacion)
            CSistema.SqlToComboBox(cbxTipoComprobante.cbx, dtComprobantes, "ID", "Codigo")

        End If

        'Habilitar e Inhabilitar Depositos
        Dim oRow As DataRow = dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)
        cbxDepositoSalida.SoloLectura = Not CBool(oRow("Salida").ToString)
        cbxDepositoSalida.txt.Text = ""
        cbxDepositoEntrada.SoloLectura = Not CBool(oRow("Entrada").ToString)
        cbxDepositoEntrada.txt.Text = ""

        If cbxTipoOperacion.cbx.SelectedValue <> 4 Then
            lblUnidadNegocio.Visible = False
            lblDepartamento.Visible = False
            cbxUnidadNegocio.Visible = False
            cbxDepartamentoEmpresa.Visible = False
            lblSeccion.Visible = False
            cbxSeccion.Visible = False
        Else
            lblUnidadNegocio.Visible = True
            lblDepartamento.Visible = True
            cbxUnidadNegocio.Visible = True
            cbxDepartamentoEmpresa.Visible = True
            lblSeccion.Visible = True
            cbxSeccion.Visible = True
        End If

    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If

    End Sub

    Private Sub txtID_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxMotivo_Editar(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxMotivo.Editar
        Dim frm As New frmMotivo
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        cbxMotivo.cbx.DataSource = Nothing

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue & "and IDSucursal = " & cbxSucursal.cbx.SelectedValue & " And Activo = 1 and IDTipoComprobante = " & cbxTipoComprobante.GetValue)

        'Habilitar e Inhabilitar Depositos
        Dim oRow As DataRow = dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)
        cbxDepositoSalida.cbx.Enabled = CBool(oRow("Salida").ToString)
        cbxDepositoEntrada.cbx.Enabled = CBool(oRow("Entrada").ToString)
    End Sub

    Private Sub txtCosto_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCosto.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub btnAsiento_Click(sender As System.Object, e As System.EventArgs) Handles btnAsiento.Click
        VisualizarAsiento()
    End Sub

    Private Sub dgw_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub

    Private Sub cbxTipoOperacion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoOperacion.TeclaPrecionada
        If vNuevo = False Then
            Exit Sub
        End If

        cbxMotivo.cbx.DataSource = Nothing

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue & "and IDSucursal = " & cbxSucursal.cbx.SelectedValue & " and Activo = 1 and IDTipoComprobante = " & cbxTipoComprobante.GetValue)

        'Habilitar e Inhabilitar Depositos
        Dim oRow As DataRow = dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)
        cbxDepositoSalida.SoloLectura = Not CBool(oRow("Salida").ToString)
        cbxDepositoSalida.txt.Text = ""
        cbxDepositoEntrada.SoloLectura = Not CBool(oRow("Entrada").ToString)
        cbxDepositoEntrada.txt.Text = ""
    End Sub

    Private Sub txtFecha_Leave(sender As Object, e As EventArgs) Handles txtFecha.Leave
        If vNuevo Then
            Dim DiferenciaDias As Long
            Dim date1 As Date = Now
            If CBool(vgConfiguraciones("MovimientoBloquearFecha").ToString) = False Then
                Exit Sub
            End If
            Try
                DiferenciaDias = DateDiff(DateInterval.Day, Date.Parse(Now.ToShortDateString), Date.Parse(txtFecha.txt.Text))
                If DiferenciaDias > 0 Then
                    If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) < DiferenciaDias Then
                        txtFecha.SetValue(VGFechaHoraSistema)
                    End If
                Else
                    If CInt(vgConfiguraciones("MovimientoCantidadDiasHabilitados")) < -DiferenciaDias Then
                        txtFecha.SetValue(VGFechaHoraSistema)
                    End If
                End If
            Catch ex As Exception
                txtFecha.SetValue(VGFechaHoraSistema)
            End Try


        End If

    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoComprobante.PropertyChanged
        If cbxTipoComprobante.cbx.SelectedValue Is Nothing Then
            lblNombreDocumento.Text = "---"
        Else
            lblNombreDocumento.Text = dtComprobantes.Select("ID = " & cbxTipoComprobante.cbx.SelectedValue)(0)("Descripcion").ToString
        End If

        If vNuevo = True Then
            CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue & " and IDSucursal = " & cbxSucursal.cbx.SelectedValue & " and Activo = 1 and IDTipoComprobante = " & cbxTipoComprobante.GetValue)
        End If
    End Sub

    Private Sub lklSeleccionarProductos_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklSeleccionarProductos.LinkClicked
        Dim frmTransferencias As New frmTransferenciasPendientes
        frmTransferencias.Recibir = True
        frmTransferencias.IDSucursalDestino = cbxSucursal.cbx.SelectedValue
        frmTransferencias.ShowDialog()

        If frmTransferencias.Procesado Then
            dtDetalle = frmTransferencias.dtDetalle
            cbxDepositoSalida.cbx.Text = frmTransferencias.DepositoEnviado
            IDTransaccionEnvio = frmTransferencias.IDTransaccion
            ListarDetalle()
        End If


    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmDescargaStock_Activate()
        Me.Refresh()
    End Sub

    Private Sub cbxDepartamentoEmpresa_PropertyChanged(sender As Object, e As EventArgs) Handles cbxDepartamentoEmpresa.PropertyChanged
        If vNuevo = True Then
            Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamentoEmpresa.cbx.SelectedValue)
            If Seccion = True Then
                Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamentoEmpresa.cbx.SelectedValue)
                CSistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
                cbxSeccion.cbx.Text = ""
                cbxSeccion.SoloLectura = False
            Else
                cbxSeccion.SoloLectura = True
            End If
        End If
    End Sub

    Private Sub cbxSucursal_PropertyChanged(sender As Object, e As EventArgs) Handles cbxSucursal.PropertyChanged
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnImportar_Click(sender As Object, e As EventArgs) Handles btnImportar.Click
        Importar()
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsumoInformatica
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        Me.lklSeleccionarProductos = New System.Windows.Forms.LinkLabel()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.txtObservacionProducto = New ERP.ocxTXTString()
        Me.lblObservacionProducto = New System.Windows.Forms.Label()
        Me.txtCosto = New ERP.ocxTXTNumeric()
        Me.lblCosto = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.cbxUnidad = New System.Windows.Forms.ComboBox()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblUnidad = New System.Windows.Forms.Label()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblDepartamento = New System.Windows.Forms.Label()
        Me.cbxDepartamentoEmpresa = New ERP.ocxCBX()
        Me.lblNombreDocumento = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxDepositoEntrada = New ERP.ocxCBX()
        Me.cbxMotivo = New ERP.ocxCBX()
        Me.cbxDepositoSalida = New ERP.ocxCBX()
        Me.cbxTipoOperacion = New ERP.ocxCBX()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblMotivo = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblTipoOperacion = New System.Windows.Forms.Label()
        Me.lblDepositoEntrada = New System.Windows.Forms.Label()
        Me.lblDepositoSalida = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxSeccion = New ERP.ocxCBX()
        Me.gbxDetalle.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCabecera.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxDetalle
        '
        Me.gbxDetalle.Controls.Add(Me.lklSeleccionarProductos)
        Me.gbxDetalle.Controls.Add(Me.txtProducto)
        Me.gbxDetalle.Controls.Add(Me.flpAnuladoPor)
        Me.gbxDetalle.Controls.Add(Me.flpRegistradoPor)
        Me.gbxDetalle.Controls.Add(Me.txtObservacionProducto)
        Me.gbxDetalle.Controls.Add(Me.lblObservacionProducto)
        Me.gbxDetalle.Controls.Add(Me.txtCosto)
        Me.gbxDetalle.Controls.Add(Me.lblCosto)
        Me.gbxDetalle.Controls.Add(Me.txtCantidad)
        Me.gbxDetalle.Controls.Add(Me.lblCantidad)
        Me.gbxDetalle.Controls.Add(Me.cbxUnidad)
        Me.gbxDetalle.Controls.Add(Me.txtTotal)
        Me.gbxDetalle.Controls.Add(Me.lblTotal)
        Me.gbxDetalle.Controls.Add(Me.lblUnidad)
        Me.gbxDetalle.Controls.Add(Me.lblProducto)
        Me.gbxDetalle.Controls.Add(Me.dgw)
        Me.gbxDetalle.Location = New System.Drawing.Point(12, 185)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(796, 352)
        Me.gbxDetalle.TabIndex = 56
        Me.gbxDetalle.TabStop = False
        '
        'lklSeleccionarProductos
        '
        Me.lklSeleccionarProductos.AutoSize = True
        Me.lklSeleccionarProductos.Location = New System.Drawing.Point(8, 35)
        Me.lklSeleccionarProductos.Name = "lklSeleccionarProductos"
        Me.lklSeleccionarProductos.Size = New System.Drawing.Size(63, 13)
        Me.lklSeleccionarProductos.TabIndex = 21
        Me.lklSeleccionarProductos.TabStop = True
        Me.lklSeleccionarProductos.Text = "Seleccionar"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(81, 32)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(317, 20)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 1
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(288, 321)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(295, 20)
        Me.flpAnuladoPor.TabIndex = 19
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 13
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 15
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 17
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(5, 321)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 18
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 18
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 15
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 17
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'txtObservacionProducto
        '
        Me.txtObservacionProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionProducto.Color = System.Drawing.Color.Empty
        Me.txtObservacionProducto.Indicaciones = Nothing
        Me.txtObservacionProducto.Location = New System.Drawing.Point(398, 31)
        Me.txtObservacionProducto.Multilinea = False
        Me.txtObservacionProducto.Name = "txtObservacionProducto"
        Me.txtObservacionProducto.Size = New System.Drawing.Size(172, 21)
        Me.txtObservacionProducto.SoloLectura = False
        Me.txtObservacionProducto.TabIndex = 3
        Me.txtObservacionProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionProducto.Texto = ""
        '
        'lblObservacionProducto
        '
        Me.lblObservacionProducto.AutoSize = True
        Me.lblObservacionProducto.Location = New System.Drawing.Point(395, 16)
        Me.lblObservacionProducto.Name = "lblObservacionProducto"
        Me.lblObservacionProducto.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacionProducto.TabIndex = 2
        Me.lblObservacionProducto.Text = "Observacion:"
        '
        'txtCosto
        '
        Me.txtCosto.Color = System.Drawing.Color.Empty
        Me.txtCosto.Decimales = False
        Me.txtCosto.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCosto.Location = New System.Drawing.Point(705, 31)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(81, 21)
        Me.txtCosto.SoloLectura = False
        Me.txtCosto.TabIndex = 9
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCosto.Texto = "0"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(749, 16)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(37, 13)
        Me.lblCosto.TabIndex = 8
        Me.lblCosto.Text = "Costo:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidad.Location = New System.Drawing.Point(642, 31)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(63, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 7
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(653, 16)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidad.TabIndex = 6
        Me.lblCantidad.Text = "Cantidad:"
        '
        'cbxUnidad
        '
        Me.cbxUnidad.FormattingEnabled = True
        Me.cbxUnidad.Location = New System.Drawing.Point(571, 31)
        Me.cbxUnidad.Name = "cbxUnidad"
        Me.cbxUnidad.Size = New System.Drawing.Size(71, 21)
        Me.cbxUnidad.TabIndex = 5
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = False
        Me.txtTotal.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotal.Location = New System.Drawing.Point(656, 324)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(130, 21)
        Me.txtTotal.SoloLectura = False
        Me.txtTotal.TabIndex = 12
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(623, 328)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 11
        Me.lblTotal.Text = "Total:"
        '
        'lblUnidad
        '
        Me.lblUnidad.AutoSize = True
        Me.lblUnidad.Location = New System.Drawing.Point(569, 16)
        Me.lblUnidad.Name = "lblUnidad"
        Me.lblUnidad.Size = New System.Drawing.Size(44, 13)
        Me.lblUnidad.TabIndex = 4
        Me.lblUnidad.Text = "Unidad:"
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Location = New System.Drawing.Point(78, 16)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(53, 13)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        '
        'dgw
        '
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgw.Location = New System.Drawing.Point(4, 59)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgw.Size = New System.Drawing.Size(782, 253)
        Me.dgw.TabIndex = 20
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = Nothing
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = Nothing
        Me.cbxUnidadNegocio.DataValueMember = Nothing
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(9, 145)
        Me.cbxUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(215, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 46
        Me.cbxUnidadNegocio.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 128)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 13)
        Me.Label4.TabIndex = 45
        Me.Label4.Text = "Unidad de Negocio:"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.Location = New System.Drawing.Point(257, 128)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(145, 13)
        Me.lblDepartamento.TabIndex = 47
        Me.lblDepartamento.Text = "Departamento que Consume:"
        '
        'cbxDepartamentoEmpresa
        '
        Me.cbxDepartamentoEmpresa.CampoWhere = Nothing
        Me.cbxDepartamentoEmpresa.CargarUnaSolaVez = False
        Me.cbxDepartamentoEmpresa.DataDisplayMember = Nothing
        Me.cbxDepartamentoEmpresa.DataFilter = Nothing
        Me.cbxDepartamentoEmpresa.DataOrderBy = Nothing
        Me.cbxDepartamentoEmpresa.DataSource = Nothing
        Me.cbxDepartamentoEmpresa.DataValueMember = Nothing
        Me.cbxDepartamentoEmpresa.dtSeleccionado = Nothing
        Me.cbxDepartamentoEmpresa.FormABM = Nothing
        Me.cbxDepartamentoEmpresa.Indicaciones = Nothing
        Me.cbxDepartamentoEmpresa.Location = New System.Drawing.Point(260, 145)
        Me.cbxDepartamentoEmpresa.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepartamentoEmpresa.Name = "cbxDepartamentoEmpresa"
        Me.cbxDepartamentoEmpresa.SeleccionMultiple = False
        Me.cbxDepartamentoEmpresa.SeleccionObligatoria = False
        Me.cbxDepartamentoEmpresa.Size = New System.Drawing.Size(270, 21)
        Me.cbxDepartamentoEmpresa.SoloLectura = False
        Me.cbxDepartamentoEmpresa.TabIndex = 44
        Me.cbxDepartamentoEmpresa.Texto = ""
        '
        'lblNombreDocumento
        '
        Me.lblNombreDocumento.BackColor = System.Drawing.Color.Yellow
        Me.lblNombreDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblNombreDocumento.ForeColor = System.Drawing.Color.Red
        Me.lblNombreDocumento.Location = New System.Drawing.Point(5, 66)
        Me.lblNombreDocumento.Name = "lblNombreDocumento"
        Me.lblNombreDocumento.Size = New System.Drawing.Size(783, 23)
        Me.lblNombreDocumento.TabIndex = 26
        Me.lblNombreDocumento.Text = "---"
        Me.lblNombreDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(82, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(76, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(409, 38)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(379, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 13
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(81, 38)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(77, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 10
        Me.cbxTipoComprobante.Texto = ""
        '
        'cbxDepositoEntrada
        '
        Me.cbxDepositoEntrada.CampoWhere = Nothing
        Me.cbxDepositoEntrada.CargarUnaSolaVez = False
        Me.cbxDepositoEntrada.DataDisplayMember = Nothing
        Me.cbxDepositoEntrada.DataFilter = Nothing
        Me.cbxDepositoEntrada.DataOrderBy = Nothing
        Me.cbxDepositoEntrada.DataSource = Nothing
        Me.cbxDepositoEntrada.DataValueMember = Nothing
        Me.cbxDepositoEntrada.dtSeleccionado = Nothing
        Me.cbxDepositoEntrada.FormABM = Nothing
        Me.cbxDepositoEntrada.Indicaciones = Nothing
        Me.cbxDepositoEntrada.Location = New System.Drawing.Point(465, 100)
        Me.cbxDepositoEntrada.Name = "cbxDepositoEntrada"
        Me.cbxDepositoEntrada.SeleccionMultiple = False
        Me.cbxDepositoEntrada.SeleccionObligatoria = True
        Me.cbxDepositoEntrada.Size = New System.Drawing.Size(321, 21)
        Me.cbxDepositoEntrada.SoloLectura = False
        Me.cbxDepositoEntrada.TabIndex = 17
        Me.cbxDepositoEntrada.Texto = ""
        '
        'cbxMotivo
        '
        Me.cbxMotivo.CampoWhere = Nothing
        Me.cbxMotivo.CargarUnaSolaVez = False
        Me.cbxMotivo.DataDisplayMember = Nothing
        Me.cbxMotivo.DataFilter = Nothing
        Me.cbxMotivo.DataOrderBy = Nothing
        Me.cbxMotivo.DataSource = Nothing
        Me.cbxMotivo.DataValueMember = Nothing
        Me.cbxMotivo.dtSeleccionado = Nothing
        Me.cbxMotivo.FormABM = Nothing
        Me.cbxMotivo.Indicaciones = Nothing
        Me.cbxMotivo.Location = New System.Drawing.Point(570, 12)
        Me.cbxMotivo.Name = "cbxMotivo"
        Me.cbxMotivo.SeleccionMultiple = False
        Me.cbxMotivo.SeleccionObligatoria = True
        Me.cbxMotivo.Size = New System.Drawing.Size(218, 21)
        Me.cbxMotivo.SoloLectura = False
        Me.cbxMotivo.TabIndex = 8
        Me.cbxMotivo.Texto = ""
        '
        'cbxDepositoSalida
        '
        Me.cbxDepositoSalida.CampoWhere = Nothing
        Me.cbxDepositoSalida.CargarUnaSolaVez = False
        Me.cbxDepositoSalida.DataDisplayMember = Nothing
        Me.cbxDepositoSalida.DataFilter = Nothing
        Me.cbxDepositoSalida.DataOrderBy = Nothing
        Me.cbxDepositoSalida.DataSource = Nothing
        Me.cbxDepositoSalida.DataValueMember = Nothing
        Me.cbxDepositoSalida.dtSeleccionado = Nothing
        Me.cbxDepositoSalida.FormABM = Nothing
        Me.cbxDepositoSalida.Indicaciones = Nothing
        Me.cbxDepositoSalida.Location = New System.Drawing.Point(81, 100)
        Me.cbxDepositoSalida.Name = "cbxDepositoSalida"
        Me.cbxDepositoSalida.SeleccionMultiple = False
        Me.cbxDepositoSalida.SeleccionObligatoria = True
        Me.cbxDepositoSalida.Size = New System.Drawing.Size(321, 21)
        Me.cbxDepositoSalida.SoloLectura = False
        Me.cbxDepositoSalida.TabIndex = 15
        Me.cbxDepositoSalida.Texto = ""
        '
        'cbxTipoOperacion
        '
        Me.cbxTipoOperacion.CampoWhere = Nothing
        Me.cbxTipoOperacion.CargarUnaSolaVez = False
        Me.cbxTipoOperacion.DataDisplayMember = Nothing
        Me.cbxTipoOperacion.DataFilter = Nothing
        Me.cbxTipoOperacion.DataOrderBy = Nothing
        Me.cbxTipoOperacion.DataSource = Nothing
        Me.cbxTipoOperacion.DataValueMember = Nothing
        Me.cbxTipoOperacion.dtSeleccionado = Nothing
        Me.cbxTipoOperacion.FormABM = Nothing
        Me.cbxTipoOperacion.Indicaciones = Nothing
        Me.cbxTipoOperacion.Location = New System.Drawing.Point(362, 12)
        Me.cbxTipoOperacion.Name = "cbxTipoOperacion"
        Me.cbxTipoOperacion.SeleccionMultiple = False
        Me.cbxTipoOperacion.SeleccionObligatoria = True
        Me.cbxTipoOperacion.Size = New System.Drawing.Size(156, 21)
        Me.cbxTipoOperacion.SoloLectura = False
        Me.cbxTipoOperacion.TabIndex = 6
        Me.cbxTipoOperacion.Texto = ""
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(16, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 4, 8, 9, 22, 55, 62)
        Me.txtFecha.Location = New System.Drawing.Point(253, 12)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 4
        '
        'lblMotivo
        '
        Me.lblMotivo.AutoSize = True
        Me.lblMotivo.Location = New System.Drawing.Point(525, 16)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(42, 13)
        Me.lblMotivo.TabIndex = 7
        Me.lblMotivo.Text = "Motivo:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 582)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(820, 22)
        Me.StatusStrip1.TabIndex = 54
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(183, 545)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(126, 23)
        Me.btnBusquedaAvanzada.TabIndex = 49
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(102, 545)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 48
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(689, 545)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 53
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(583, 545)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 52
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(21, 545)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 47
        Me.btnAnular.Text = "Anu&lar"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(502, 545)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 51
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.Label1)
        Me.gbxCabecera.Controls.Add(Me.cbxSeccion)
        Me.gbxCabecera.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxCabecera.Controls.Add(Me.Label4)
        Me.gbxCabecera.Controls.Add(Me.lblDepartamento)
        Me.gbxCabecera.Controls.Add(Me.cbxDepartamentoEmpresa)
        Me.gbxCabecera.Controls.Add(Me.lblNombreDocumento)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxDepositoEntrada)
        Me.gbxCabecera.Controls.Add(Me.cbxMotivo)
        Me.gbxCabecera.Controls.Add(Me.cbxDepositoSalida)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblTipoOperacion)
        Me.gbxCabecera.Controls.Add(Me.lblDepositoEntrada)
        Me.gbxCabecera.Controls.Add(Me.lblMotivo)
        Me.gbxCabecera.Controls.Add(Me.lblDepositoSalida)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Location = New System.Drawing.Point(12, 0)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(796, 179)
        Me.gbxCabecera.TabIndex = 46
        Me.gbxCabecera.TabStop = False
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(159, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(52, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblTipoOperacion
        '
        Me.lblTipoOperacion.AutoSize = True
        Me.lblTipoOperacion.Location = New System.Drawing.Point(328, 16)
        Me.lblTipoOperacion.Name = "lblTipoOperacion"
        Me.lblTipoOperacion.Size = New System.Drawing.Size(31, 13)
        Me.lblTipoOperacion.TabIndex = 5
        Me.lblTipoOperacion.Text = "Tipo:"
        '
        'lblDepositoEntrada
        '
        Me.lblDepositoEntrada.AutoSize = True
        Me.lblDepositoEntrada.Location = New System.Drawing.Point(408, 104)
        Me.lblDepositoEntrada.Name = "lblDepositoEntrada"
        Me.lblDepositoEntrada.Size = New System.Drawing.Size(47, 13)
        Me.lblDepositoEntrada.TabIndex = 16
        Me.lblDepositoEntrada.Text = "Entrada:"
        '
        'lblDepositoSalida
        '
        Me.lblDepositoSalida.AutoSize = True
        Me.lblDepositoSalida.Location = New System.Drawing.Point(36, 104)
        Me.lblDepositoSalida.Name = "lblDepositoSalida"
        Me.lblDepositoSalida.Size = New System.Drawing.Size(39, 13)
        Me.lblDepositoSalida.TabIndex = 14
        Me.lblDepositoSalida.Text = "Salida:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(333, 42)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 12
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(214, 16)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 3
        Me.lblFecha.Text = "Fecha:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(2, 42)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 9
        Me.lblComprobante.Text = "Comprobante:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(159, 39)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(168, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 11
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(340, 545)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 55
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(421, 545)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 50
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(535, 128)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = "Seccion:"
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = Nothing
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = Nothing
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = Nothing
        Me.cbxSeccion.DataSource = Nothing
        Me.cbxSeccion.DataValueMember = Nothing
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(538, 145)
        Me.cbxSeccion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(248, 21)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 48
        Me.cbxSeccion.Texto = ""
        '
        'frmConsumoInformatica
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(820, 604)
        Me.Controls.Add(Me.gbxDetalle)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Controls.Add(Me.btnAsiento)
        Me.Controls.Add(Me.btnNuevo)
        Me.Name = "frmConsumoInformatica"
        Me.Tag = "frmConsumoInformatica"
        Me.Text = "frmConsumoInformatica"
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gbxDetalle As GroupBox
    Friend WithEvents lklSeleccionarProductos As LinkLabel
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents flpAnuladoPor As FlowLayoutPanel
    Friend WithEvents lblAnulado As Label
    Friend WithEvents lblUsuarioAnulado As Label
    Friend WithEvents lblFechaAnulado As Label
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents txtObservacionProducto As ocxTXTString
    Friend WithEvents lblObservacionProducto As Label
    Friend WithEvents txtCosto As ocxTXTNumeric
    Friend WithEvents lblCosto As Label
    Friend WithEvents txtCantidad As ocxTXTNumeric
    Friend WithEvents lblCantidad As Label
    Friend WithEvents cbxUnidad As ComboBox
    Friend WithEvents txtTotal As ocxTXTNumeric
    Friend WithEvents lblTotal As Label
    Friend WithEvents lblUnidad As Label
    Friend WithEvents lblProducto As Label
    Friend WithEvents dgw As DataGridView
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents Label4 As Label
    Friend WithEvents lblDepartamento As Label
    Friend WithEvents cbxDepartamentoEmpresa As ocxCBX
    Friend WithEvents lblNombreDocumento As Label
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents cbxDepositoEntrada As ocxCBX
    Friend WithEvents cbxMotivo As ocxCBX
    Friend WithEvents cbxDepositoSalida As ocxCBX
    Friend WithEvents cbxTipoOperacion As ocxCBX
    Friend WithEvents lblOperacion As Label
    Friend WithEvents txtFecha As ocxTXTDate
    Friend WithEvents lblMotivo As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents btnBusquedaAvanzada As Button
    Friend WithEvents btnImprimir As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnAnular As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents gbxCabecera As GroupBox
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents lblTipoOperacion As Label
    Friend WithEvents lblDepositoEntrada As Label
    Friend WithEvents lblDepositoSalida As Label
    Friend WithEvents lblObservacion As Label
    Friend WithEvents lblFecha As Label
    Friend WithEvents lblComprobante As Label
    Friend WithEvents txtComprobante As ocxTXTString
    Friend WithEvents btnAsiento As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxSeccion As ocxCBX
End Class

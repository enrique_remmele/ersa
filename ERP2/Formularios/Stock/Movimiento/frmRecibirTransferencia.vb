﻿Public Class frmRecibirTransferencia

    Dim CSistema As New CSistema
    Public Property dtDetalleMovimiento As DataTable
    Public Property IDTransaccion As Integer
    Public Property Procesado As Boolean = False
    Dim Diferencias As Boolean = False

    Sub Inicializar()

        dtDetalleMovimiento = CSistema.ExecuteToDataTable("Select top(0) * from vdetallemovimiento ")
        Listar()
    End Sub

    Sub Listar()

        Dim Total As Decimal = 0
        'Cargamos registro por registro
        Try
            CSistema.SqlToDataGrid(dgv, "Select *,'Recibido'=0.00 from vdetallemovimiento where idtransaccion =" & IDTransaccion,,, False)
        Catch ex As Exception

        End Try

        For i As Integer = 0 To dgv.Columns.Count - 1
            dgv.Columns(i).Visible = False
        Next

        dgv.Columns("Ref").DisplayIndex = 1
        dgv.Columns("Descripcion").DisplayIndex = 2
        dgv.Columns("Cantidad").DisplayIndex = 3
        dgv.Columns("Recibido").DisplayIndex = 4

        dgv.Columns("Ref").Visible = True
        dgv.Columns("Descripcion").Visible = True
        dgv.Columns("Cantidad").Visible = True
        dgv.Columns("Recibido").Visible = True

        dgv.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgv.Columns("Recibido").DefaultCellStyle.Format = "N2"

        dgv.Columns("Ref").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("Recibido").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgv.Columns("Cantidad").HeaderText = "Enviado"


    End Sub

    Sub Procesar()
        dtDetalleMovimiento.Clear()

        For i = 0 To dgv.RowCount - 1

            Dim row As DataRow = dtDetalleMovimiento.NewRow()

            For j = 0 To dgv.ColumnCount - 1

                If dgv.Columns(j).Name = "Cantidad" Then
                    GoTo seguir
                End If

                If dgv.Columns(j).Name = "PrecioUnitario" Then
                    row("PrecioUnitario") = CSistema.FormatoMoneda(dgv.Rows(i).Cells(j).Value, True)
                    GoTo seguir
                End If

                If dgv.Columns(j).Name = "Recibido" Then
                    row("Cantidad") = dgv.Rows(i).Cells(j).Value
                    GoTo seguir
                End If

                row(j) = dgv.Rows(i).Cells(j).Value
seguir:

            Next
            dtDetalleMovimiento.Rows.Add(row)
            If dgv.Rows(i).Cells("Cantidad").Value <> dgv.Rows(i).Cells("Recibido").Value Then
                Diferencias = True
            End If
        Next

    End Sub

    Sub BloquearHabilitarEdicion()
        Select Case dgv.CurrentCell.OwningColumn.HeaderText
            Case "Recibido"
                dgv.ReadOnly = False
            Case Else
                dgv.ReadOnly = True
        End Select
    End Sub

    Private Sub frmRecibirTransferencia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgv_SelectionChanged(sender As Object, e As EventArgs) Handles dgv.SelectionChanged
        BloquearHabilitarEdicion()
    End Sub

    Private Sub dgv_CurrentCellChanged(sender As Object, e As EventArgs) Handles dgv.CurrentCellChanged
        BloquearHabilitarEdicion()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Procesar()
        If Diferencias Then
            If MessageBox.Show("Se guardaran diferencias entre las cantidades enviadas y recibidas", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then
                Exit Sub
            End If
        End If
        Procesado = True
        Me.Close()

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub


    Private Sub dgv_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellEndEdit
        If dgv.CurrentRow.Cells("Cantidad").Value <> dgv.CurrentRow.Cells("Recibido").Value Then
            dgv.CurrentRow.DefaultCellStyle.BackColor = Color.Red
            dgv.CurrentRow.DefaultCellStyle.BackColor = Color.DarkRed
        Else
            dgv.CurrentRow.DefaultCellStyle.BackColor = Color.FromKnownColor(KnownColor.Control)
            dgv.CurrentRow.DefaultCellStyle.SelectionBackColor = Color.FromKnownColor(KnownColor.Highlight)

        End If
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmRecibirTransferencia_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmTransferenciasPendientes
    Dim CSistema As New CSistema
    Dim CData As New CData
    Public Property IDTransaccion As Integer
    Public Property dtDetalle
    Public Property IDSucursalDestino As Integer
    Public Property DepositoEnviado As String
    Public Property Recibir As Boolean = False
    Public Property Procesado As Boolean = False

    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        If Recibir Then
            chkDepositoOrigen.Enabled = False
            chkSucursalDestino.Enabled = False
            chkSucursalOrigen.Enabled = False
            btnListar.Enabled = False
        Else
            chkDepositoOrigen.Enabled = True
            chkSucursalDestino.Enabled = True
            chkSucursalOrigen.Enabled = True
            btnListar.Enabled = True
        End If

        Dim sql As String = "Select * from VMovimiento where Anulado = 'False' and IDSucursalDestino = " & IDSucursalDestino & " And Enviar = 'True' And Recibido = 'False' "
        Listar(sql)
    End Sub

    Sub Listar(ByVal sql As String)


        Dim dt As DataTable = CSistema.ExecuteToDataTable(sql)
        CSistema.dtToGrid(dgv, dt)
        For i = 1 To dgv.ColumnCount - 1
            dgv.Columns(i).Visible = False
        Next

        CSistema.DataGridColumnasVisibles(dgv, {"Sucursal", "Numero", "Comprobante", "Fecha", "Motivo", "DepositoEntrada", "Observacion", "Usuario"})
        dgv.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


    End Sub

    Sub Seleccionar()

        IDTransaccion = dgv.CurrentRow.Cells("IDTransaccion").Value
        DepositoEnviado = dgv.CurrentRow.Cells("DepositoEntrada").Value

        Dim frm As New frmRecibirTransferencia
        frm.IDTransaccion = IDTransaccion
        frm.ShowDialog()
        Procesado = frm.Procesado
        If Recibir Then
            If Procesado Then
                dtDetalle = frm.dtDetalleMovimiento
                Me.Close()
            End If
        End If

    End Sub

    Sub Filtrar()
        Dim sql As String = "Select * from VMovimiento where Anulado = 'False' and Enviar = 'True' And Recibido = 'False' "
        Dim where As String = " And 1 = 1 "
        cbxSucursalOrigen.EstablecerCondicion(where)
        cbxDepositoOrigen.EstablecerCondicion(where)
        cbxSucursalDestino.EstablecerCondicion(where)
        Listar(sql & where)

    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Filtrar()
    End Sub

    Private Sub frmTransferenciasPendientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub dgv_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellDoubleClick
        Seleccionar()
    End Sub

    Private Sub chkDepositoOrigen_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkDepositoOrigen.PropertyChanged
        cbxDepositoOrigen.Enabled = value
    End Sub

    Private Sub chkSucursalOrigen_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSucursalOrigen.PropertyChanged
        cbxSucursalOrigen.Enabled = value
    End Sub

    Private Sub chkSucursalDestino_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSucursalDestino.PropertyChanged
        cbxSucursalDestino.Enabled = value
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmTransferenciasPendientes_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDevolucionSinNotaCredido
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDevolucionSinNotaCredido))
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.gbxDetalle = New System.Windows.Forms.GroupBox()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.btnImportar = New System.Windows.Forms.Button()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.txtObservacionProducto = New ERP.ocxTXTString()
        Me.lblObservacionProducto = New System.Windows.Forms.Label()
        Me.txtCosto = New ERP.ocxTXTNumeric()
        Me.lblCosto = New System.Windows.Forms.Label()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnAsiento = New System.Windows.Forms.Button()
        Me.btnAnula = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.txtNumeroSolicitudCliente = New ERP.ocxTXTString()
        Me.txtFechaAutorizacion = New ERP.ocxTXTDate()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtUsuarioAutorizador = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtEstadoAutorizacion = New ERP.ocxTXTString()
        Me.txtObservacionAutorizacion = New ERP.ocxTXTString()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel2.SuspendLayout()
        Me.gbxDetalle.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.gbxCabecera.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 436.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.gbxDetalle, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel1, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.gbxCabecera, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.Padding = New System.Windows.Forms.Padding(3)
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(801, 583)
        Me.TableLayoutPanel2.TabIndex = 5
        '
        'gbxDetalle
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.gbxDetalle, 2)
        Me.gbxDetalle.Controls.Add(Me.flpAnuladoPor)
        Me.gbxDetalle.Controls.Add(Me.flpRegistradoPor)
        Me.gbxDetalle.Controls.Add(Me.btnImportar)
        Me.gbxDetalle.Controls.Add(Me.txtProducto)
        Me.gbxDetalle.Controls.Add(Me.txtObservacionProducto)
        Me.gbxDetalle.Controls.Add(Me.lblObservacionProducto)
        Me.gbxDetalle.Controls.Add(Me.txtCosto)
        Me.gbxDetalle.Controls.Add(Me.lblCosto)
        Me.gbxDetalle.Controls.Add(Me.txtCantidad)
        Me.gbxDetalle.Controls.Add(Me.lblCantidad)
        Me.gbxDetalle.Controls.Add(Me.txtTotal)
        Me.gbxDetalle.Controls.Add(Me.lblTotal)
        Me.gbxDetalle.Controls.Add(Me.lblProducto)
        Me.gbxDetalle.Controls.Add(Me.dgw)
        Me.gbxDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxDetalle.Location = New System.Drawing.Point(6, 182)
        Me.gbxDetalle.Name = "gbxDetalle"
        Me.gbxDetalle.Size = New System.Drawing.Size(789, 364)
        Me.gbxDetalle.TabIndex = 15
        Me.gbxDetalle.TabStop = False
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(293, 331)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(315, 20)
        Me.flpAnuladoPor.TabIndex = 1
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(8, 331)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(277, 20)
        Me.flpRegistradoPor.TabIndex = 0
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'btnImportar
        '
        Me.btnImportar.Location = New System.Drawing.Point(8, 28)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(89, 23)
        Me.btnImportar.TabIndex = 21
        Me.btnImportar.Text = "Importar"
        Me.btnImportar.UseVisualStyleBackColor = True
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(103, 32)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(317, 20)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 1
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'txtObservacionProducto
        '
        Me.txtObservacionProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionProducto.Color = System.Drawing.Color.Empty
        Me.txtObservacionProducto.Indicaciones = Nothing
        Me.txtObservacionProducto.Location = New System.Drawing.Point(420, 32)
        Me.txtObservacionProducto.Multilinea = False
        Me.txtObservacionProducto.Name = "txtObservacionProducto"
        Me.txtObservacionProducto.Size = New System.Drawing.Size(208, 21)
        Me.txtObservacionProducto.SoloLectura = False
        Me.txtObservacionProducto.TabIndex = 3
        Me.txtObservacionProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionProducto.Texto = ""
        '
        'lblObservacionProducto
        '
        Me.lblObservacionProducto.AutoSize = True
        Me.lblObservacionProducto.Location = New System.Drawing.Point(417, 16)
        Me.lblObservacionProducto.Name = "lblObservacionProducto"
        Me.lblObservacionProducto.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacionProducto.TabIndex = 2
        Me.lblObservacionProducto.Text = "Observacion:"
        '
        'txtCosto
        '
        Me.txtCosto.Color = System.Drawing.Color.Empty
        Me.txtCosto.Decimales = False
        Me.txtCosto.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCosto.Location = New System.Drawing.Point(693, 32)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(81, 21)
        Me.txtCosto.SoloLectura = False
        Me.txtCosto.TabIndex = 9
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCosto.Texto = "0"
        '
        'lblCosto
        '
        Me.lblCosto.AutoSize = True
        Me.lblCosto.Location = New System.Drawing.Point(737, 16)
        Me.lblCosto.Name = "lblCosto"
        Me.lblCosto.Size = New System.Drawing.Size(37, 13)
        Me.lblCosto.TabIndex = 8
        Me.lblCosto.Text = "Costo:"
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = True
        Me.txtCantidad.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidad.Location = New System.Drawing.Point(630, 32)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(63, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 7
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(641, 16)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidad.TabIndex = 6
        Me.lblCantidad.Text = "Cantidad:"
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = False
        Me.txtTotal.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotal.Location = New System.Drawing.Point(647, 330)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(130, 21)
        Me.txtTotal.SoloLectura = False
        Me.txtTotal.TabIndex = 12
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = "0"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(614, 334)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 11
        Me.lblTotal.Text = "Total:"
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Location = New System.Drawing.Point(100, 16)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(53, 13)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        '
        'dgw
        '
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgw.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.Location = New System.Drawing.Point(8, 59)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgw.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgw.Size = New System.Drawing.Size(769, 265)
        Me.dgw.TabIndex = 20
        '
        'Panel1
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.btnAnular)
        Me.Panel1.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Panel1.Controls.Add(Me.btnImprimir)
        Me.Panel1.Controls.Add(Me.btnAsiento)
        Me.Panel1.Controls.Add(Me.btnAnula)
        Me.Panel1.Controls.Add(Me.btnNuevo)
        Me.Panel1.Controls.Add(Me.btnSalir)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Controls.Add(Me.btnEliminar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 549)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(795, 31)
        Me.Panel1.TabIndex = 6
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(204, 2)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 8
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(3, 2)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(120, 23)
        Me.btnBusquedaAvanzada.TabIndex = 0
        Me.btnBusquedaAvanzada.Text = "&Busqueda Avanzada"
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(126, 2)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 1
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        Me.btnImprimir.Visible = False
        '
        'btnAsiento
        '
        Me.btnAsiento.Location = New System.Drawing.Point(342, 2)
        Me.btnAsiento.Name = "btnAsiento"
        Me.btnAsiento.Size = New System.Drawing.Size(75, 23)
        Me.btnAsiento.TabIndex = 3
        Me.btnAsiento.Text = "&Asiento"
        Me.btnAsiento.UseVisualStyleBackColor = True
        Me.btnAsiento.Visible = False
        '
        'btnAnula
        '
        Me.btnAnula.Location = New System.Drawing.Point(-94, -558)
        Me.btnAnula.Name = "btnAnula"
        Me.btnAnula.Size = New System.Drawing.Size(75, 23)
        Me.btnAnula.TabIndex = 2
        Me.btnAnula.Text = "Anular"
        Me.btnAnula.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(423, 2)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 4
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(702, 2)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(504, 2)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 5
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(592, 2)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(204, 2)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        Me.btnEliminar.Visible = False
        '
        'gbxCabecera
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.gbxCabecera, 2)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.Label11)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Controls.Add(Me.txtNumeroSolicitudCliente)
        Me.gbxCabecera.Controls.Add(Me.txtFechaAutorizacion)
        Me.gbxCabecera.Controls.Add(Me.Label6)
        Me.gbxCabecera.Controls.Add(Me.Label5)
        Me.gbxCabecera.Controls.Add(Me.txtUsuarioAutorizador)
        Me.gbxCabecera.Controls.Add(Me.Label3)
        Me.gbxCabecera.Controls.Add(Me.txtEstadoAutorizacion)
        Me.gbxCabecera.Controls.Add(Me.txtObservacionAutorizacion)
        Me.gbxCabecera.Controls.Add(Me.Label2)
        Me.gbxCabecera.Controls.Add(Me.txtCliente)
        Me.gbxCabecera.Controls.Add(Me.cbxDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblDeposito)
        Me.gbxCabecera.Controls.Add(Me.lblCliente)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbxCabecera.Location = New System.Drawing.Point(6, 6)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(789, 170)
        Me.gbxCabecera.TabIndex = 1
        Me.gbxCabecera.TabStop = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(58, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(69, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = ""
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(595, 47)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(108, 13)
        Me.Label11.TabIndex = 52
        Me.Label11.Text = "Nro. Solicitud Cliente:"
        Me.ToolTip1.SetToolTip(Me.Label11, resources.GetString("Label11.ToolTip"))
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(127, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(59, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 6
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(0, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 5
        Me.lblOperacion.Text = "Operacion:"
        '
        'txtNumeroSolicitudCliente
        '
        Me.txtNumeroSolicitudCliente.BackColor = System.Drawing.SystemColors.Control
        Me.txtNumeroSolicitudCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroSolicitudCliente.Color = System.Drawing.Color.Empty
        Me.txtNumeroSolicitudCliente.Indicaciones = Nothing
        Me.txtNumeroSolicitudCliente.Location = New System.Drawing.Point(709, 42)
        Me.txtNumeroSolicitudCliente.Multilinea = False
        Me.txtNumeroSolicitudCliente.Name = "txtNumeroSolicitudCliente"
        Me.txtNumeroSolicitudCliente.Size = New System.Drawing.Size(74, 22)
        Me.txtNumeroSolicitudCliente.SoloLectura = False
        Me.txtNumeroSolicitudCliente.TabIndex = 51
        Me.txtNumeroSolicitudCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNumeroSolicitudCliente.Texto = ""
        '
        'txtFechaAutorizacion
        '
        Me.txtFechaAutorizacion.AñoFecha = 0
        Me.txtFechaAutorizacion.Color = System.Drawing.Color.Empty
        Me.txtFechaAutorizacion.Enabled = False
        Me.txtFechaAutorizacion.Fecha = New Date(2013, 5, 3, 16, 59, 18, 132)
        Me.txtFechaAutorizacion.Location = New System.Drawing.Point(709, 116)
        Me.txtFechaAutorizacion.MesFecha = 0
        Me.txtFechaAutorizacion.Name = "txtFechaAutorizacion"
        Me.txtFechaAutorizacion.PermitirNulo = False
        Me.txtFechaAutorizacion.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaAutorizacion.SoloLectura = True
        Me.txtFechaAutorizacion.TabIndex = 50
        Me.txtFechaAutorizacion.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(592, 118)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 13)
        Me.Label6.TabIndex = 49
        Me.Label6.Text = "Fecha Autorizacion:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(279, 118)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 13)
        Me.Label5.TabIndex = 47
        Me.Label5.Text = "Autorizador:"
        '
        'txtUsuarioAutorizador
        '
        Me.txtUsuarioAutorizador.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuarioAutorizador.Color = System.Drawing.Color.Empty
        Me.txtUsuarioAutorizador.Enabled = False
        Me.txtUsuarioAutorizador.Indicaciones = Nothing
        Me.txtUsuarioAutorizador.Location = New System.Drawing.Point(359, 115)
        Me.txtUsuarioAutorizador.Multilinea = False
        Me.txtUsuarioAutorizador.Name = "txtUsuarioAutorizador"
        Me.txtUsuarioAutorizador.Size = New System.Drawing.Size(212, 21)
        Me.txtUsuarioAutorizador.SoloLectura = True
        Me.txtUsuarioAutorizador.TabIndex = 46
        Me.txtUsuarioAutorizador.TabStop = False
        Me.txtUsuarioAutorizador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuarioAutorizador.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 118)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 13)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Estado Autorizacion:"
        '
        'txtEstadoAutorizacion
        '
        Me.txtEstadoAutorizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoAutorizacion.Color = System.Drawing.Color.Empty
        Me.txtEstadoAutorizacion.Enabled = False
        Me.txtEstadoAutorizacion.Indicaciones = Nothing
        Me.txtEstadoAutorizacion.Location = New System.Drawing.Point(132, 115)
        Me.txtEstadoAutorizacion.Multilinea = False
        Me.txtEstadoAutorizacion.Name = "txtEstadoAutorizacion"
        Me.txtEstadoAutorizacion.Size = New System.Drawing.Size(109, 21)
        Me.txtEstadoAutorizacion.SoloLectura = True
        Me.txtEstadoAutorizacion.TabIndex = 44
        Me.txtEstadoAutorizacion.TabStop = False
        Me.txtEstadoAutorizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEstadoAutorizacion.Texto = ""
        '
        'txtObservacionAutorizacion
        '
        Me.txtObservacionAutorizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacionAutorizacion.Color = System.Drawing.Color.Empty
        Me.txtObservacionAutorizacion.Indicaciones = Nothing
        Me.txtObservacionAutorizacion.Location = New System.Drawing.Point(132, 142)
        Me.txtObservacionAutorizacion.Multilinea = False
        Me.txtObservacionAutorizacion.Name = "txtObservacionAutorizacion"
        Me.txtObservacionAutorizacion.Size = New System.Drawing.Size(651, 21)
        Me.txtObservacionAutorizacion.SoloLectura = True
        Me.txtObservacionAutorizacion.TabIndex = 43
        Me.txtObservacionAutorizacion.TabStop = False
        Me.txtObservacionAutorizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacionAutorizacion.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 145)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(126, 13)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Observacion Autorizador:"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 61
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(240, 9)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(543, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 0
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = Nothing
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = ""
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = Nothing
        Me.cbxDeposito.DataSource = ""
        Me.cbxDeposito.DataValueMember = ""
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(240, 43)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = True
        Me.cbxDeposito.Size = New System.Drawing.Size(165, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 2
        Me.cbxDeposito.Texto = ""
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(182, 47)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 4
        Me.lblDeposito.Text = "Deposito:"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(192, 16)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Cliente:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(58, 73)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(725, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 6
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 3, 16, 59, 18, 132)
        Me.txtFecha.Location = New System.Drawing.Point(58, 43)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 1
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(2, 47)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Fecha:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(5, 76)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(29, 13)
        Me.lblObservacion.TabIndex = 14
        Me.lblObservacion.Text = "Obs:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 583)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(801, 22)
        Me.StatusStrip1.TabIndex = 4
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmDevolucionSinNotaCredido
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(801, 605)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmDevolucionSinNotaCredido"
        Me.Text = "frmDevolucionSinNotaCredido"
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.gbxDetalle.ResumeLayout(False)
        Me.gbxDetalle.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnAnular As Button
    Friend WithEvents btnBusquedaAvanzada As Button
    Friend WithEvents btnImprimir As Button
    Friend WithEvents btnAsiento As Button
    Friend WithEvents btnAnula As Button
    Friend WithEvents btnNuevo As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents lblOperacion As Label
    Friend WithEvents gbxCabecera As GroupBox
    Friend WithEvents Label11 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents txtNumeroSolicitudCliente As ocxTXTString
    Friend WithEvents txtFechaAutorizacion As ocxTXTDate
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtUsuarioAutorizador As ocxTXTString
    Friend WithEvents Label3 As Label
    Friend WithEvents txtEstadoAutorizacion As ocxTXTString
    Friend WithEvents txtObservacionAutorizacion As ocxTXTString
    Friend WithEvents Label2 As Label
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents cbxDeposito As ocxCBX
    Friend WithEvents lblDeposito As Label
    Friend WithEvents lblCliente As Label
    Friend WithEvents txtObservacion As ocxTXTString
    Friend WithEvents txtFecha As ocxTXTDate
    Friend WithEvents lblFecha As Label
    Friend WithEvents lblObservacion As Label
    Friend WithEvents flpRegistradoPor As FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As Label
    Friend WithEvents lblUsuarioRegistro As Label
    Friend WithEvents lblFechaRegistro As Label
    Friend WithEvents flpAnuladoPor As FlowLayoutPanel
    Friend WithEvents lblAnulado As Label
    Friend WithEvents lblUsuarioAnulado As Label
    Friend WithEvents lblFechaAnulado As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents gbxDetalle As GroupBox
    Friend WithEvents btnImportar As Button
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents txtObservacionProducto As ocxTXTString
    Friend WithEvents lblObservacionProducto As Label
    Friend WithEvents txtCosto As ocxTXTNumeric
    Friend WithEvents lblCosto As Label
    Friend WithEvents txtCantidad As ocxTXTNumeric
    Friend WithEvents lblCantidad As Label
    Friend WithEvents txtTotal As ocxTXTNumeric
    Friend WithEvents lblTotal As Label
    Friend WithEvents lblProducto As Label
    Friend WithEvents dgw As DataGridView
End Class

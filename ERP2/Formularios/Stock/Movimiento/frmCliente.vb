﻿Public Class frmMovimientoModificar

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData
 
    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar(ByVal Tipo As String)

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Otros
        flpRegistradoPor.Visible = False

        'Propiedades
        If Tipo = "Movimiento" Then
            IDOperacion = CSistema.ObtenerIDOperacion(frmMovimientoStock.Name, "MOVIMIENTOS", "MOV")
        End If
        If Tipo = "DescargaStock" Then
            IDOperacion = CSistema.ObtenerIDOperacion(frmDescargaStock.Name, "DESCARGASTOCK", "DEST")
        End If
        If Tipo = "Combustible" Then
            IDOperacion = CSistema.ObtenerIDOperacion(frmConsumoCombustible.Name, "CONSUMO COMBUSTIBLE", "CCOM")
        End If
        If Tipo = "MateriaPrima" Then
            IDOperacion = CSistema.ObtenerIDOperacion(frmMovimientoMateriaPrima.Name, "MOVIMIENTO MATERIA PRIMA", "MOMP")
        End If
        If Tipo = "Compras" Then
            IDOperacion = CSistema.ObtenerIDOperacion(frmMovimientoDescargaCompra.Name, "MOVIMIENTO DESCARGA DE COMPRA", "MO")
        End If
        IDTransaccion = 0

        'Funciones
        CargarInformacion()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxMotivo)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtObservacion)

        'CARGAR DATOS DE OPERACION
        dtOperaciones = CSistema.ExecuteToDataTable("Select ID, Descripcion, Activo, Entrada, Salida From TipoOperacion Where IDOperacion=" & IDOperacion)
       
        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Tipo de Operaciones
        CSistema.SqlToComboBox(cbxTipoOperacion.cbx, "Select ID, Descripcion From TipoOperacion Where IDOperacion=" & IDOperacion)

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Depositos
        dtDepositos = CSistema.ExecuteToDataTable(" Select * From VDeposito Where Activo='True' Order By 2").Copy

        'Deposito de Origen
        cbxDepositoEntrada.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoEntrada.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoEntrada.cbx.DataSource = dtDepositos.Copy

        'Deposito de Destino
        cbxDepositoSalida.cbx.ValueMember = dtDepositos.Columns("ID").ToString
        cbxDepositoSalida.cbx.DisplayMember = dtDepositos.Columns("Suc-Dep").ToString
        cbxDepositoSalida.cbx.DataSource = dtDepositos.Copy
       
        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo Operacion
        cbxSucursal.SelectedValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "0"))

        'Tipo Operacion
        cbxTipoOperacion.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO OPERACION", "")

        'Tipo Motivo
        cbxMotivo.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MOTIVO", "")

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Deposito de Origen
        cbxDepositoSalida.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO ORIGEN", "")

        'Deposito de Destino
        cbxDepositoEntrada.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO DESTINO", "")

    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.GetValue)

        'Tipo Operacion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO OPERACION", cbxTipoOperacion.cbx.Text)

        'Motivo
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MOTIVO", cbxMotivo.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Deposito de Origen
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO ORIGEN", cbxDepositoSalida.cbx.Text)

        'Deposito de Destino
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO DESTINO", cbxDepositoEntrada.cbx.Text)

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VMovimiento Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Function ValidarDocumento() As Boolean

        ValidarDocumento = False

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        Return True

    End Function

    Sub Modificar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where Estado = 1 and IDOperacion=" & IDOperacion)
        txtComprobante.SoloLectura = False
        cbxTipoComprobante.SoloLectura = False
        cbxMotivo.SoloLectura = False
        txtObservacion.SoloLectura = False

    End Sub

    Sub Guardar()

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If ValidarDocumento() = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
       
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoOperacion", cbxTipoOperacion.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMotivo", cbxMotivo.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoSalida", cbxDepositoSalida.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoEntrada", cbxDepositoEntrada.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Autorizacion", "", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(txtTotal.ObtenerValor), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.UPD.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input) 'JGR 20140816
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Actualizar
        If CSistema.ExecuteStoreProcedure(param, "SpMovimiento", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, New Button, New Button, btnBusquedaAvanzada, New Button, vControles, btnModificar)

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim frm As New frmConsultaMovimiento
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VMovimiento Where IDSucursal=" & cbxSucursal.GetValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Numero, Fecha, Operacion, Motivo, [Cod.],  NroComprobante, DepositoEntrada, DepositoSalida, Observacion, Autorizacion, Total, Anulado, UsuarioIdentificacionAnulacion, FechaAnulacion, Usuario, UsuarioIdentificador, FechaTransaccion From VMovimiento Where IDTransaccion=" & IDTransaccion)

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)


        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxTipoOperacion.cbx.Text = oRow("Operacion").ToString
        cbxMotivo.cbx.Text = oRow("Motivo").ToString
        cbxTipoComprobante.Texto = oRow("Cod.").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxDepositoEntrada.txt.Text = oRow("DepositoEntrada").ToString
        cbxDepositoSalida.txt.Text = oRow("DepositoSalida").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtTotal.txt.Text = oRow("Total").ToString

        If oRow("DepositoEntrada").ToString = "---" Then
            cbxDepositoEntrada.txt.Text = ""
        End If

        If oRow("DepositoSalida").ToString = "---" Then
            cbxDepositoSalida.txt.Text = ""
        End If

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Num), 1) From VMovimiento Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Num), 1) From VMovimiento Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

    End Sub

    Private Sub frmMovimientoStock_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmMovimientoStock_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmMovimientoStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar("Movimiento")
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxTipoComprobante_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoComprobante.Editar


        'Dim frm As New frmTipoComprobante
        'frm.WindowState = FormWindowState.Normal
        'frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        'frm.StartPosition = FormStartPosition.CenterParent
        'frm.ShowDialog()

        'cbxTipoComprobante.cbx.DataSource = Nothing
        'CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=20")

    End Sub

    Private Sub cbxMotivo_Editar(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxMotivo.Editar
        Dim frm As New frmMotivo
        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog()

        cbxMotivo.cbx.DataSource = Nothing

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue)

        'Habilitar e Inhabilitar Depositos
        Dim oRow As DataRow = dtOperaciones.Select(" ID=" & cbxTipoOperacion.cbx.SelectedValue)(0)
        cbxDepositoSalida.cbx.Enabled = CBool(oRow("Salida").ToString)
        cbxDepositoEntrada.cbx.Enabled = CBool(oRow("Entrada").ToString)

    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub btnModificar_Click(sender As System.Object, e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub cbxTipoOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxTipoOperacion.PropertyChanged

        cbxMotivo.cbx.DataSource = Nothing

        If IsNumeric(cbxTipoOperacion.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        'Motivos
        CSistema.SqlToComboBox(cbxMotivo.cbx, "Select ID, Descripcion From MotivoMovimiento Where IDTipoMovimiento=" & cbxTipoOperacion.cbx.SelectedValue)

    End Sub

    Private Sub rdbMovimiento_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbMovimiento.CheckedChanged
        If PnlTipoMovimiento.Enabled = True And rdbMovimiento.Checked = True Then
            Inicializar("Movimiento")
        End If
    End Sub

    Private Sub rdbDescargaStock_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbDescargaStock.CheckedChanged
        If PnlTipoMovimiento.Enabled = True And rdbDescargaStock.Checked = True Then
            Inicializar("DescargaStock")
        End If
    End Sub

    Private Sub rdbCombustible_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbCombustible.CheckedChanged
        If PnlTipoMovimiento.Enabled = True And rdbCombustible.Checked = True Then
            Inicializar("Combustible")
        End If
    End Sub

    Private Sub rdbCompras_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbCompras.CheckedChanged
        If PnlTipoMovimiento.Enabled = True And rdbCompras.Checked = True Then
            Inicializar("Compras")
        End If
    End Sub

    Private Sub rdbMateriaPrima_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbMateriaPrima.CheckedChanged
        If PnlTipoMovimiento.Enabled = True And rdbMateriaPrima.Checked = True Then
            Inicializar("MateriaPrima")
        End If
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmCliente_Activate()
        Me.Refresh()
    End Sub
End Class
﻿Public Class frmAjusteProvisorio

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDDepositoValue As Integer
    Public Property IDDeposito() As Integer
        Get
            Return IDDepositoValue
        End Get
        Set(ByVal value As Integer)
            IDDepositoValue = value
        End Set
    End Property

    Private IDProductoValue As Integer
    Public Property IDProducto() As Integer
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As Integer)
            IDProductoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Producto
        Dim oRow As DataRow = CData.GetRow(" ID = " & IDProducto, "VProducto")

        If oRow IsNot Nothing Then
            txtIDProducto.SetValue(oRow("ID"))
            txtProducto.SetValue(oRow("Descripcion"))
            txtReferencia.SetValue(oRow("Ref"))
            txtCodigoBarra.SetValue(oRow("CodigoBarra"))
        End If

        'Usuario
        lblUsuarioRegistro.Text = vgUsuarioIdentificador.ToUpper & " - " & vgUsuario.ToLower
        flpRegistradoPor.Visible = True

        'Variables
        Procesado = False

        'Foco
        txtTipoDocumento.txt.Focus()
        txtTipoDocumento.txt.SelectAll()

    End Sub

    Function ValidarDocumento() As Boolean

        ValidarDocumento = False

        'Tipo de Documento
        If txtTipoDocumento.txt.Text.Trim.Length = 0 Then
            CSistema.MostrarError("Complete el Tipo de Documento", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Return False
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            CSistema.MostrarError("Complete el comprobante", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Return False
        End If

        'Observacion
        If txtObservacion.txt.Text.Trim.Length = 0 Then
            CSistema.MostrarError("Complete la observacion o motivo de ajuste provisorio", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Return False
        End If

        'Cantidad
        If txtCantidad.ObtenerValor = 0 Then
            CSistema.MostrarError("La cantidad no puede ser 0", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Return False
        End If

        Return True

    End Function

    Sub Procesar()

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento() = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoAfectado", IDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoDocumento", txtTipoDocumento.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", txtComprobante.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cantidad", txtCantidad.ObtenerValor, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpControlInventarioAjusteProvisorio", False, False, MensajeRetorno) = False Then
            CSistema.MostrarError("Atencion: " & MensajeRetorno, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Procesado = True

        Me.Close()

    End Sub

    Sub Cancelar()

        Procesado = False
        Me.Close()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub frmAjusteProvisorio_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmAjusteProvisorio_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Procesar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmAjusteProvisorio_Activate()
        Me.Refresh()
    End Sub
End Class
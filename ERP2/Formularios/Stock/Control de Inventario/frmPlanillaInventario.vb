﻿Imports ERP.Reporte
Public Class frmPlanillaInventario

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CReporte As New CReporteStock

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private ConsultaValue As String
    Public Property Consulta() As String
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As String)
            ConsultaValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dt As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim frmFiltroProductos As New frmFiltroProductos

    'FUNCIONES
    Sub Inicializar()

        'Otros
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Propiedades
        IDTransaccion = 0
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "PLANILLA DE INVENTARIO", "INV")
        vNuevo = False

        'Funciones
        CargarInformacion()

        'Foco
        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxDeposito)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, btnCargar)
        CSistema.CargaControl(vControles, Button1)
        CSistema.CargaControl(vControles, txtReferenciaProducto)

        'CARGAR CONTROLES
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Order By 2")

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CData.GetStructure("VExistenciaDepositoInventario", " Select Top(0) * From VExistenciaDepositoInventario")

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Deposito
        cbxDeposito.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO", vgDeposito)

        'Obtener Ultimo Registro
        'txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From ControlInventario Where IDSucursal=" & cbxCiudad.cbx.SelectedValue & "),1) "), Integer)
        'txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Deposito
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO", cbxDeposito.cbx.Text)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select top 1 IsNull((Select top 1 IDTransaccion From ControlInventario Where Numero = " & txtID.ObtenerValor & " and IDSucursal=" & cbxSucursal.cbx.SelectedValue & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            CSistema.MostrarError(mensaje, ctrError, txtID, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        dt = CSistema.ExecuteToDataTable("Select * From VControlInventario Where IDTransaccion=" & IDTransaccion).Copy
        dtDetalle = CSistema.ExecuteToDataTable("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion & " ").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            CSistema.MostrarError("Error en la consulta! Problemas tecnicos.", ctrError, txtID, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        'Punto de Expedicion
        cbxCiudad.SelectedValue(oRow("IDCiudad"))
        cbxSucursal.SelectedValue(oRow("IDSucursalOperacion"))
        cbxDeposito.SelectedValue(oRow("IDDepositoOperacion"))
        txtFecha.SetValue(oRow("Fecha"))
        txtEstado.txt.Text = oRow("Estado").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            btnAnular.Enabled = False
        Else
            flpAnuladoPor.Visible = False
            btnAnular.Visible = True
        End If

        'Cargamos el detalle
        ListarDetalle()

    End Sub

    Sub CargarDetalle(Optional ByVal FiltroProducto As String = "")

        Dim SQL As String

        If FiltroProducto = "" Then
            SQL = "Select IDProducto, Referencia, Producto, CodigoBarra, 'CantidadDeposito'=Existencia, 'Provisorio'=0, 'Existencia'=Existencia From VExistenciaDeposito Where IDDeposito=" & cbxDeposito.GetValue & " And ControlarExistencia='True' and Estado = 'True' Order By Producto "
        Else
            SQL = "Select IDProducto, Referencia, Producto, CodigoBarra, 'CantidadDeposito'=Existencia, 'Provisorio'=0, 'Existencia'=Existencia From VExistenciaDeposito " & FiltroProducto & " And IDDeposito=" & cbxDeposito.GetValue & " And ControlarExistencia='True' and Estado = 'True' Order By Producto "
        End If

        dtDetalle = CSistema.ExecuteToDataTable(SQL).Copy
        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Limpiar la grilla
        dgw.Rows.Clear()

        For Each oRow As DataRowView In dtDetalle.DefaultView
            Dim Registro(6) As String
            Registro(0) = oRow("IDProducto").ToString
            Registro(1) = oRow("Referencia").ToString
            Registro(2) = oRow("Producto").ToString
            Registro(3) = oRow("CodigoBarra").ToString
            'Registro(4) = CSistema.FormatoNumero(oRow("CantidadDeposito").ToString)
            'Registro(5) = CSistema.FormatoNumero(oRow("Provisorio").ToString)
            'Registro(6) = CSistema.FormatoNumero(oRow("Existencia").ToString)
            Registro(4) = CSistema.FormatoNumero(oRow("CantidadDeposito").ToString, True)
            Registro(5) = CSistema.FormatoNumero(oRow("Provisorio").ToString, True)
            Registro(6) = CSistema.FormatoNumero(oRow("Existencia").ToString, True)

            dgw.Rows.Add(Registro)

        Next

        txtCantidad.SetValue(dtDetalle.Rows.Count)

    End Sub

    Sub ExpandirDetalle()

    End Sub

    Sub ModificarDetalle()

    End Sub

    Sub CargarDetalleporReferencia()

        If txtReferenciaProducto.txt.Text = "" Then
            Exit Sub
        End If
        Dim NewdtDetalle As DataTable
        Dim Ref As String = txtReferenciaProducto.txt.Text
        Consulta = "Select IDProducto, Referencia, Producto, CodigoBarra, 'CantidadDeposito'=Existencia, 'Provisorio'=0, 'Existencia'=Existencia From VExistenciaDeposito Where IDDeposito=" & cbxDeposito.GetValue & " And ControlarExistencia='True' And Referencia= '" & Ref & "'" & " Order By Producto "

        If dtDetalle.Rows.Count = 0 Then
            dtDetalle = CSistema.ExecuteToDataTable(Consulta).Copy
            ListarDetalle()
            Exit Sub
        End If

        NewdtDetalle = CSistema.ExecuteToDataTable(Consulta).Copy

        For Each oRow As DataRow In NewdtDetalle.Rows
            'Verificamos q no exista
            If dtDetalle.Select("Referencia='" & Ref & "'").Count = 0 Then

                Dim NewoRow As DataRow = dtDetalle.NewRow
                NewoRow("IDProducto") = oRow("IDProducto")
                NewoRow("Referencia") = oRow("Referencia")
                NewoRow("Producto") = oRow("Producto")
                NewoRow("CodigoBarra") = oRow("CodigoBarra")
                NewoRow("CantidadDeposito") = oRow("CantidadDeposito")
                NewoRow("Provisorio") = oRow("Provisorio")
                NewoRow("Existencia") = oRow("Existencia")

                dtDetalle.Rows.Add(NewoRow)

            Else
                CSistema.MostrarError("El producto ya existe..!!", ctrError, txtReferenciaProducto, tsslEstado, ErrorIconAlignment.BottomRight)
            End If
        Next

        ListarDetalle()

    End Sub

    Private Sub MostrarConsulta()

        Dim frm As New frmProductoBuscar
        frm.Consulta = Consulta
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.WindowState = FormWindowState.Maximized
        frm.ShowDialog()

        If frm.ID = 0 Then
            Exit Sub
        End If
        Dim sql As String = "Select IDProducto, Referencia, Producto, CodigoBarra, 'CantidadDeposito'=Existencia, 'Provisorio'=0, 'Existencia'=Existencia From VExistenciaDeposito Where IDDeposito=" & cbxDeposito.GetValue & " And ControlarExistencia='True' And IDProducto= '" & frm.ID & "'" & " Order By Producto "
        Dim dt As DataTable

        dt = CSistema.ExecuteToDataTable(sql).Copy

        For Each oRow As DataRow In dt.Rows
            txtReferenciaProducto.txt.Text = oRow("Referencia")
        Next


    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        'Controles
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Cabecera
        txtFecha.txt.Text = ""
        txtObservacion.txt.Clear()
        txtEstado.txt.Clear()
        txtReferenciaProducto.txt.Clear()

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From ControlInventario Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco
        cbxDeposito.Focus()
        cbxDeposito.cbx.Focus()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Ciudad
        If cbxCiudad.Validar("Seleccione correctamente la ciudad!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Deposito
        If cbxDeposito.Validar("Seleccione correctamente el deposito!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        CSistema.SetSQLParameter(param, "@Numero", CInt(txtID.ObtenerValor), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", txtFecha.GetValueString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpControlInventario", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub
        Else

            tsslEstado.Text = MensajeRetorno

        End If

        If IDTransaccion > 0 Then

            Dim SQL As String = InsertDetalle(IDTransaccion)
            CSistema.ExecuteNonQuery(SQL, "", 1000)

        End If

        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
            CargarOperacion(IDTransaccion)
            txtID.SoloLectura = False
            Exit Sub
        End If

        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

    End Sub

    Sub Anular(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        CSistema.SetSQLParameter(param, "@Numero", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepositoOperacion", cbxDeposito.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", txtFecha.GetValueString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpControlInventario", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub
        Else

            tsslEstado.Text = MensajeRetorno

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Function InsertDetalle(ByVal NuevoIDTransaccion As Integer) As String

        InsertDetalle = ""

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "IDTransaccion", NuevoIDTransaccion)
            CSistema.SetSQLParameter(param, "IDDeposito", cbxDeposito.GetValue)
            CSistema.SetSQLParameter(param, "IDProducto", oRow("IDProducto").ToString)
            CSistema.SetSQLParameter(param, "Existencia", CSistema.FormatoNumeroBaseDatos(oRow("Existencia").ToString, True))
            CSistema.SetSQLParameter(param, "CantidadDeposito", CSistema.FormatoNumeroBaseDatos(oRow("CantidadDeposito").ToString, True))
            CSistema.SetSQLParameter(param, "Provisorio", CSistema.FormatoNumeroBaseDatos(oRow("Provisorio").ToString, True))

            InsertDetalle = InsertDetalle & CSistema.InsertSQL(param, "ExistenciaDepositoInventario") & vbCrLf

        Next

    End Function

    Sub Cancelar()

        'Obtener Ultimo Registro
        ' txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From ControlInventario Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1) "), Integer)

        dgw.Rows.Clear()
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        txtID.txt.ReadOnly = False
        vNuevo = False

        'Ultimo Registro
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))

        txtID.txt.Focus()
        txtID.txt.SelectAll()
        txtReferenciaProducto.txt.Clear()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

    End Sub

    Sub Anular()

        Anular(ERP.CSistema.NUMOperacionesRegistro.ANULAR)

    End Sub

    Sub Imprimir()

        If IDTransaccion <= 0 Then
            Exit Sub
        End If

        Dim Titulo As String = "PLANILLA DE INVENTARIO"
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.PlanillaInventario(frm, Titulo, IDTransaccion, vgUsuarioIdentificador)

    End Sub

    Sub Busqueda()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Sub CargarAjusteProvisorio()

        If vNuevo = True Then
            Exit Sub
        End If

        If dgw.SelectedRows.Count = 0 Then
            CSistema.MostrarError("Seleccione un producto para continuar!", ctrError, dgw, tsslEstado, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim frm As New frmAjusteProvisorio
        frm.IDTransaccion = IDTransaccion
        frm.IDDeposito = cbxDeposito.GetValue
        frm.IDProducto = dgw.SelectedRows(0).Cells("colID").Value

        FGMostrarFormulario(Me, frm, "Ajuste Provisorio", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Procesado = True Then
            CargarOperacion(IDTransaccion)
        End If

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From ControlInventario Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From ControlInventario Where IDSucursal=" & cbxSucursal.GetValue), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Busqueda()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Sub ExportarPlanillaInventario()
        Dim frm As New frmExportarPlanillaInventario
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.IDTransaccion = IDTransaccion
        FGMostrarFormulario(Me, frm, "Exportar Planilla de Inventario", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Private Sub frmPlanillaInventario_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmPlanillaInventario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmPlanillaInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargar.Click
        CargarDetalle()
    End Sub

    Private Sub lklExpandir_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklExpandir.LinkClicked
        ExpandirDetalle()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Busqueda()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales

        CSistema.SqlToComboBox(cbxSucursal.cbx, CData.GetTable("VSucursal", " IDCiudad=" & cbxCiudad.GetValue), "ID", "Descripcion")

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged

        cbxDeposito.cbx.DataSource = Nothing

        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Deposito
        CSistema.SqlToComboBox(cbxDeposito.cbx, CData.GetTable("VDeposito", " IDSucursal=" & cbxSucursal.GetValue), "ID", "Deposito")

    End Sub

    Private Sub cbxSucursal_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxSucursal.TeclaPrecionada
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub

    Private Sub CargarAjusteProvisorioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CargarAjusteProvisorioToolStripMenuItem.Click
        CargarAjusteProvisorio()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FGMostrarFormulario(Me, frmFiltroProductos, "Filtro de Productos", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
        If frmFiltroProductos.Procesado = True Then
            CargarDetalle(frmFiltroProductos.Filtro)
        End If
    End Sub

    Private Sub txtReferenciaProducto_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferenciaProducto.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarDetalleporReferencia()
        End If

        If e.KeyCode = Keys.F1 Then
            MostrarConsulta()
        End If

    End Sub

    Private Sub btnExportar_Click(sender As System.Object, e As System.EventArgs) Handles btnExportar.Click
        ExportarPlanillaInventario()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmPlanillaInventario_Activate()
        Me.Refresh()
    End Sub
End Class
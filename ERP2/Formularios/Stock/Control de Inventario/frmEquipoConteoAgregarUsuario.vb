﻿Public Class frmEquipoConteoAgregarUsuario

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private IDEquipoValue As Integer
    Public Property IDEquipo() As Integer
        Get
            Return IDEquipoValue
        End Get
        Set(ByVal value As Integer)
            IDEquipoValue = value
        End Set
    End Property

    Private dtUsuarioValue As DataTable
    Public Property dtUsuario() As DataTable
        Get
            Return dtUsuarioValue
        End Get
        Set(ByVal value As DataTable)
            dtUsuarioValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Sub Inicializar()

        'Cargar lista
        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Select * from vusuario where Estado = 1")

        For Each oRow As DataRow In dttemp.Rows
            Dim item As New ListViewItem(oRow("Usuario").ToString)
            item.SubItems.Add(oRow("ID"))
            lvUsuario.Items.Add(item)
        Next

        'Marcar los seleccionados
        If dtUsuario Is Nothing Then
            Exit Sub
        End If
        For Each item As ListViewItem In lvUsuario.Items

            If dtUsuario.Select(" IDUsuario = " & item.SubItems(1).Text).Count > 0 Then
                item.Checked = True
            End If

        Next

    End Sub

    Sub Guardar()

        Dim SQL As String = "Delete From EquipoConteoUsuario Where IDEquipo = " & IDEquipo & vbCrLf

        For Each item As ListViewItem In lvUsuario.CheckedItems
            SQL = SQL & "Insert Into EquipoConteoUsuario(IDEquipo, IDUsuario) Values(" & IDEquipo & ", " & item.SubItems(1).Text & ")" & vbCrLf
        Next

        'Ejecutar
        Dim RowCount As Integer = CSistema.ExecuteNonQuery(SQL)
        If RowCount > 0 Then
            Procesado = True
            Me.Close()
        Else
            Procesado = False
        End If

    End Sub

    Sub Cancelar()
        Procesado = False
        Me.Close()
    End Sub

    Private Sub frmEquipoConteoAgregarUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmEquipoConteoAgregarUsuario_Activate()
        Me.Refresh()
    End Sub
End Class
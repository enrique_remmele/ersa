﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFiltroProductos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.chkCategoria = New ERP.ocxCHK()
        Me.chkPresentacion = New ERP.ocxCHK()
        Me.chkMarca = New ERP.ocxCHK()
        Me.chkSubLinea2 = New ERP.ocxCHK()
        Me.chkSubLinea = New ERP.ocxCHK()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxSubLinea2 = New ERP.ocxCBX()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.cbxCategoria = New ERP.ocxCBX()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.chkProducto = New ERP.ocxCHK()
        Me.cbxProducto = New ERP.ocxCBX()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkCategoria)
        Me.gbxFiltro.Controls.Add(Me.chkPresentacion)
        Me.gbxFiltro.Controls.Add(Me.chkMarca)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxCategoria)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxPresentacion)
        Me.gbxFiltro.Controls.Add(Me.cbxMarca)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxProducto)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(374, 269)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(290, 296)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(209, 296)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'chkProveedor
        '
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(20, 235)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(114, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 16
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'chkCategoria
        '
        Me.chkCategoria.Color = System.Drawing.Color.Empty
        Me.chkCategoria.Location = New System.Drawing.Point(20, 208)
        Me.chkCategoria.Name = "chkCategoria"
        Me.chkCategoria.Size = New System.Drawing.Size(114, 21)
        Me.chkCategoria.SoloLectura = False
        Me.chkCategoria.TabIndex = 14
        Me.chkCategoria.Texto = "Categoria:"
        Me.chkCategoria.Valor = False
        '
        'chkPresentacion
        '
        Me.chkPresentacion.Color = System.Drawing.Color.Empty
        Me.chkPresentacion.Location = New System.Drawing.Point(20, 181)
        Me.chkPresentacion.Name = "chkPresentacion"
        Me.chkPresentacion.Size = New System.Drawing.Size(114, 21)
        Me.chkPresentacion.SoloLectura = False
        Me.chkPresentacion.TabIndex = 12
        Me.chkPresentacion.Texto = "Presentacion:"
        Me.chkPresentacion.Valor = False
        '
        'chkMarca
        '
        Me.chkMarca.Color = System.Drawing.Color.Empty
        Me.chkMarca.Location = New System.Drawing.Point(20, 154)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Size = New System.Drawing.Size(114, 21)
        Me.chkMarca.SoloLectura = False
        Me.chkMarca.TabIndex = 10
        Me.chkMarca.Texto = "Marca:"
        Me.chkMarca.Valor = False
        '
        'chkSubLinea2
        '
        Me.chkSubLinea2.Color = System.Drawing.Color.Empty
        Me.chkSubLinea2.Location = New System.Drawing.Point(20, 127)
        Me.chkSubLinea2.Name = "chkSubLinea2"
        Me.chkSubLinea2.Size = New System.Drawing.Size(114, 21)
        Me.chkSubLinea2.SoloLectura = False
        Me.chkSubLinea2.TabIndex = 8
        Me.chkSubLinea2.Texto = "Sub-Linea2:"
        Me.chkSubLinea2.Valor = False
        '
        'chkSubLinea
        '
        Me.chkSubLinea.Color = System.Drawing.Color.Empty
        Me.chkSubLinea.Location = New System.Drawing.Point(20, 100)
        Me.chkSubLinea.Name = "chkSubLinea"
        Me.chkSubLinea.Size = New System.Drawing.Size(114, 21)
        Me.chkSubLinea.SoloLectura = False
        Me.chkSubLinea.TabIndex = 6
        Me.chkSubLinea.Texto = "Sub-Linea:"
        Me.chkSubLinea.Valor = False
        '
        'chkLinea
        '
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(20, 73)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(114, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 4
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxSubLinea2
        '
        Me.cbxSubLinea2.CampoWhere = "IDSubLinea2"
        Me.cbxSubLinea2.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea2.DataFilter = Nothing
        Me.cbxSubLinea2.DataOrderBy = Nothing
        Me.cbxSubLinea2.DataSource = "VSubLinea2"
        Me.cbxSubLinea2.DataValueMember = "ID"
        Me.cbxSubLinea2.Enabled = False
        Me.cbxSubLinea2.FormABM = Nothing
        Me.cbxSubLinea2.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea2.Location = New System.Drawing.Point(140, 127)
        Me.cbxSubLinea2.Name = "cbxSubLinea2"
        Me.cbxSubLinea2.SeleccionObligatoria = False
        Me.cbxSubLinea2.Size = New System.Drawing.Size(213, 21)
        Me.cbxSubLinea2.SoloLectura = False
        Me.cbxSubLinea2.TabIndex = 9
        Me.cbxSubLinea2.Texto = "NORMAL"
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = "IDSubLinea"
        Me.cbxSubLinea.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = Nothing
        Me.cbxSubLinea.DataSource = "VSubLinea"
        Me.cbxSubLinea.DataValueMember = "ID"
        Me.cbxSubLinea.Enabled = False
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea.Location = New System.Drawing.Point(140, 100)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(213, 21)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 7
        Me.cbxSubLinea.Texto = "AGUA"
        '
        'cbxCategoria
        '
        Me.cbxCategoria.CampoWhere = "IDCategoria"
        Me.cbxCategoria.DataDisplayMember = "Descripcion"
        Me.cbxCategoria.DataFilter = Nothing
        Me.cbxCategoria.DataOrderBy = Nothing
        Me.cbxCategoria.DataSource = "VCategoria"
        Me.cbxCategoria.DataValueMember = "ID"
        Me.cbxCategoria.Enabled = False
        Me.cbxCategoria.FormABM = Nothing
        Me.cbxCategoria.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxCategoria.Location = New System.Drawing.Point(140, 208)
        Me.cbxCategoria.Name = "cbxCategoria"
        Me.cbxCategoria.SeleccionObligatoria = False
        Me.cbxCategoria.Size = New System.Drawing.Size(213, 21)
        Me.cbxCategoria.SoloLectura = False
        Me.cbxCategoria.TabIndex = 15
        Me.cbxCategoria.Texto = ""
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.DataDisplayMember = "RazonSocial"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxProveedor.Location = New System.Drawing.Point(140, 235)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(213, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 17
        Me.cbxProveedor.Texto = "KYRIOS S.R.L."
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = "IDPresentacion"
        Me.cbxPresentacion.DataDisplayMember = "Descripcion"
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = Nothing
        Me.cbxPresentacion.DataSource = "VPresentacion"
        Me.cbxPresentacion.DataValueMember = "ID"
        Me.cbxPresentacion.Enabled = False
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPresentacion.Location = New System.Drawing.Point(140, 181)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(213, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 13
        Me.cbxPresentacion.Texto = "100 ML."
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = "IDMarca"
        Me.cbxMarca.DataDisplayMember = "Descripcion"
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = Nothing
        Me.cbxMarca.DataSource = "VMarca"
        Me.cbxMarca.DataValueMember = "ID"
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxMarca.Location = New System.Drawing.Point(140, 154)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(213, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 11
        Me.cbxMarca.Texto = "SAVORA"
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "IDLinea"
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "VLinea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxLinea.Location = New System.Drawing.Point(140, 73)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(213, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 5
        Me.cbxLinea.Texto = "BEBIDAS"
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxTipoProducto.Location = New System.Drawing.Point(140, 46)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 3
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(20, 46)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 2
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'chkProducto
        '
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(20, 19)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 0
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'cbxProducto
        '
        Me.cbxProducto.CampoWhere = "IDProducto"
        Me.cbxProducto.DataDisplayMember = "Descripcion"
        Me.cbxProducto.DataFilter = Nothing
        Me.cbxProducto.DataOrderBy = "Descripcion"
        Me.cbxProducto.DataSource = "VProducto"
        Me.cbxProducto.DataValueMember = "ID"
        Me.cbxProducto.Enabled = False
        Me.cbxProducto.FormABM = Nothing
        Me.cbxProducto.Indicaciones = Nothing
        Me.cbxProducto.Location = New System.Drawing.Point(140, 19)
        Me.cbxProducto.Name = "cbxProducto"
        Me.cbxProducto.SeleccionObligatoria = False
        Me.cbxProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxProducto.SoloLectura = False
        Me.cbxProducto.TabIndex = 1
        Me.cbxProducto.Texto = "AC. TRIMESTRAL SET/10 (50%)"
        '
        'frmFiltroProductos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(402, 332)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmFiltroProductos"
        Me.Text = "frmFiltroProductos"
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents cbxProducto As ERP.ocxCBX
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents chkCategoria As ERP.ocxCHK
    Friend WithEvents chkPresentacion As ERP.ocxCHK
    Friend WithEvents chkMarca As ERP.ocxCHK
    Friend WithEvents chkSubLinea2 As ERP.ocxCHK
    Friend WithEvents chkSubLinea As ERP.ocxCHK
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxSubLinea2 As ERP.ocxCBX
    Friend WithEvents cbxSubLinea As ERP.ocxCBX
    Friend WithEvents cbxCategoria As ERP.ocxCBX
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents cbxPresentacion As ERP.ocxCBX
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
End Class

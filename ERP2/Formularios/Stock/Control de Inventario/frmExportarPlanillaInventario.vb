﻿Public Class frmExportarPlanillaInventario

    Dim CMHilo As New CMHilo
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vProceso As Threading.Thread

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Sub Procesar()

        vProceso = New Threading.Thread(AddressOf ExportarPlanillaInventario)
        vProceso.Start()

    End Sub

    Sub Cancelar()

        vProceso.Abort()

    End Sub

    Sub ExportarPlanillaInventario()


        pbProgreso.Value = 0
        CMHilo.LabelText(Me, lblProgreso, "0 de 0 - 0%")

        Dim VSQL As String = "Select Sucursal, Deposito, Referencia, Producto, CodigoBarra, Existencia, Costo from vExistenciaDepositoInventario Where IDTransaccion = " & IDTransaccion
        Dim vNombreArchivo As String = txtNombreArchivo.Text

        Dim dt As DataTable = CSistema.ExecuteToDataTable(VSQL)
        Dim file As String = vNombreArchivo

        'Crear directorio si no existe
        If IO.Directory.Exists(VGCarpetaTemporal) = False Then
            'Eliminar
            IO.Directory.CreateDirectory(VGCarpetaTemporal)
        End If

        'Eliminamosel archivo si yq existe
        Try
            If IO.File.Exists(file) = True Then IO.File.Delete(file)
        Catch ex As Exception

        End Try

        'CABECERA

        Try

            Dim sw As New System.IO.StreamWriter(file, False)
            Dim cadena As String = ""
            For c As Integer = 0 To dt.Columns.Count - 1
                If cadena = "" Then
                    cadena = dt.Columns(c).ColumnName
                Else
                    cadena = cadena & vbTab & dt.Columns(c).ColumnName
                End If
            Next

            sw.WriteLine(cadena)


            'DETALLE
            Dim i As Integer = 0

            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    If cadena = "" Then
                        cadena = oRow(c).ToString
                    Else
                        cadena = cadena & vbTab & oRow(c).ToString
                    End If

                Next

                sw.WriteLine(cadena)

                Threading.Thread.Sleep(7)

                i = i + 1
                pbProgreso.Value = (i / dt.Rows.Count) * 100
                CMHilo.LabelText(Me, lblProgreso, i & " de " & dt.Rows.Count & " - " & pbProgreso.Value.ToString & "%")

            Next

            sw.Close()

        Catch ex As Exception
            MessageBox.Show("No se pudo generar con exito!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            pbProgreso.Value = 0
            CMHilo.LabelText(Me, lblProgreso, "0 de 0 - 0%")
            'btnGenerar.Text = "Generar"
            Exit Sub
        End Try

        MessageBox.Show("Generacion exitosa!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)

        'btnGenerar.Text = "Generar"
        pbProgreso.Value = 0
        CMHilo.LabelText(Me, lblProgreso, "0 de " & dt.Rows.Count & " - 0%")

        CSistema.SelectNextControl(Me)
        'HabilitarGuardar(True)
    End Sub

    Private Sub Exportar_Click(sender As System.Object, e As System.EventArgs) Handles btnExportar.Click
        Procesar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        vProceso.Abort()
    End Sub

    Private Sub btnExplorar_Click(sender As System.Object, e As System.EventArgs) Handles btnExplorar.Click
        SaveFileDialog1.Filter = "Documentos de texto (*.txt)|*.txt"
        SaveFileDialog1.ShowDialog(Me)
        txtNombreArchivo.Text = SaveFileDialog1.FileName
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmExportarPlanillaInventario_Activate()
        Me.Refresh()
    End Sub
End Class
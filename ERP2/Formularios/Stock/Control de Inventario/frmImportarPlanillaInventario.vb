﻿Public Class frmImportarPlanillaInventario
    Dim CMHilo As New CMHilo
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vProceso As Threading.Thread
    Dim dt As New DataTable

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Public Property Procesado() As Boolean

    Function CargarArchivo() As Boolean

        Try
            Dim Path As String = txtNombreArchivo.Text
            Dim myStream As System.IO.StreamReader = New System.IO.StreamReader(Path)
            Dim Linea As String
            Dim Separador As String = vbTab
            Dim Cabecera As Boolean = True

            'Inicializar dt
            dt = New DataTable

            Do
                Linea = myStream.ReadLine()
                If Linea Is Nothing Then
                    Exit Do
                End If

                Dim Vector As String() = Split(Linea, Separador)  ' separate the fields of the current row         

                'La primera linea, es la cabecera
                If Cabecera = True Then

                    For i As Integer = 0 To Vector.GetLength(0) - 1
                        dt.Columns.Add(Vector(i).ToString)
                    Next

                    Cabecera = False

                Else

                    'Cargamos los registros
                    Dim NewRow As DataRow = dt.NewRow

                    For i As Integer = 0 To Vector.GetLength(0) - 1
                        NewRow(i) = Vector(i).ToString
                    Next

                    dt.Rows.Add(NewRow)

                End If


            Loop

            myStream.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
        Return True

    End Function

    Sub Iniciar()

        vProceso = New Threading.Thread(AddressOf Procesar)
        vProceso.Start()

    End Sub

    Sub Procesar()

        If CargarArchivo() = False Then
            Exit Sub
        End If

        If ImportarPlanillaInventario() = False Then
            Exit Sub
        End If

        Procesado = True
        Me.Close()
    End Sub

    Function ImportarPlanillaInventario() As Boolean


        Dim contador As Integer = 1
        Dim indice As Integer = 1
        Dim SQL As String = ""


        If dt.Rows.Count > 50 Then
            For Each orow As DataRow In dt.Rows

                Select Case contador

                    Case 1
                        SQL = "Begin Tran T1"
                        SQL = SQL & vbCrLf & "Begin Try"
                        SQL = SQL & vbCrLf & "  Exec SpImportarConteoInvecont @IDTransaccion = " & IDTransaccion & ", @Codigo = '" & orow("Codigo").ToString & "', @Equipo1 = " & orow("Stock Fisico").ToString & ", @Averiado1 = 0, @Diferencia = " & orow("Dif").ToString & ""
                        contador = contador + 1
                        indice = indice + 1

                    Case 50

                        Dim texto As String = indice.ToString & " de " & dt.Rows.Count
                        Dim porcentaje As Integer = (indice / dt.Rows.Count) * 100

                        SQL = SQL & vbCrLf & "  Exec SpImportarConteoInvecont @IDTransaccion = " & IDTransaccion & ", @Codigo = '" & orow("Codigo").ToString & "', @Equipo1 = " & orow("Stock Fisico").ToString & ", @Averiado1 = 0, @Diferencia = " & orow("Dif").ToString & ""
                        SQL = SQL & vbCrLf & "  Commit Tran T1"
                        SQL = SQL & vbCrLf & "End try"
                        SQL = SQL & vbCrLf & "Begin Catch"
                        SQL = SQL & vbCrLf & "  Declare @vMensaje varchar(100) = ERROR_MESSAGE()"
                        SQL = SQL & vbCrLf & "  Rollback Tran T1"
                        SQL = SQL & vbCrLf & "  RaisError(@vMensaje, 16, 0)"
                        SQL = SQL & vbCrLf & "End Catch"

                        Dim Resultado As Integer = CSistema.ExecuteNonQuery(SQL, , 2000)

                        contador = 1
                        indice = indice + 1

                        pbProgreso.Value = porcentaje
                        lblProgreso.Text = texto

                    Case Else

                        SQL = SQL & vbCrLf & "  Exec SpImportarConteoInvecont @IDTransaccion = " & IDTransaccion & ", @Codigo = '" & orow("Codigo").ToString & "', @Equipo1 = " & orow("Stock Fisico").ToString & ", @Averiado1 = 0, @Diferencia = " & orow("Dif").ToString & ""

                       If indice = dt.Rows.Count Then
                            SQL = SQL & vbCrLf & "  Commit Tran T1"
                            SQL = SQL & vbCrLf & "End try"
                            SQL = SQL & vbCrLf & "Begin Catch"
                            SQL = SQL & vbCrLf & "  Declare @vMensaje varchar(100) = ERROR_MESSAGE()"
                            SQL = SQL & vbCrLf & "  Rollback Tran T1"
                            SQL = SQL & vbCrLf & "  RaisError(@vMensaje, 16, 0)"
                            SQL = SQL & vbCrLf & "End Catch"

                            Dim Resultado As Integer = CSistema.ExecuteNonQuery(SQL, , 2000)

                        End If

                        contador = contador + 1
                        indice = indice + 1
                        
                End Select

            Next
        Else
            For Each orow As DataRow In dt.Rows

                SQL = "Begin Tran T1"
                SQL = SQL & vbCrLf & "Begin Try"
                SQL = SQL & vbCrLf & "  Exec SpImportarConteoInvecont @IDTransaccion = " & IDTransaccion & ", @Codigo = '" & orow("Codigo").ToString & "', @Equipo1 = " & orow("Stock Fisico").ToString & ", @Averiado1 = 0, @Diferencia = " & orow("Dif").ToString & ""

                SQL = SQL & vbCrLf & "  Commit Tran T1"
                SQL = SQL & vbCrLf & "End try"
                SQL = SQL & vbCrLf & "Begin Catch"
                SQL = SQL & vbCrLf & "  Declare @vMensaje varchar(100) = ERROR_MESSAGE()"
                SQL = SQL & vbCrLf & "  Rollback Tran T1"
                SQL = SQL & vbCrLf & "  RaisError(@vMensaje, 16, 0)"
                SQL = SQL & vbCrLf & "End Catch"

                        Dim Resultado As Integer = CSistema.ExecuteNonQuery(SQL, , 2000)

            Next
        End If

        MessageBox.Show("Registro Procesado!", "Informe", MessageBoxButtons.OK)
        Return True

    End Function

    Private Sub btnImportar_Click(sender As System.Object, e As System.EventArgs) Handles btnImportar.Click
        Iniciar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        vProceso.Abort()
    End Sub

    Private Sub btnExplorar_Click(sender As System.Object, e As System.EventArgs) Handles btnExplorar.Click
        Dim OpenFileDialog1 As New OpenFileDialog
        OpenFileDialog1.Filter = "Documentos de texto (*.txt)|*.txt"
        OpenFileDialog1.ShowDialog(Me)
        txtNombreArchivo.Text = OpenFileDialog1.FileName
    End Sub

    Private Sub frmImportarPlanillaInventario_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        CheckForIllegalCrossThreadCalls = False
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmImportarPlanillaInventario_Activate()
        Me.Refresh()
    End Sub

End Class
﻿Imports ERP.Reporte
Public Class frmTomaInventario

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CReporte As New CReporteStock

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtDetalleEquipo As New DataTable
    Dim dt As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean
    Dim vEquipo As Integer
    Dim IDTemp As Integer

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Conectar()
        dgw.MultiSelect = False

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        cbxOperacion.SeleccionObligatoria = True

        'Foco
        cbxOperacion.Focus()
        cbxOperacion.cbx.Focus()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, cbxUnidad)
        CSistema.CargaControl(vControles, txtAveriados)
        CSistema.CargaControl(vControles, txtCantidad)

        'CARGAR CONTROLES
        cbxUnidad.Items.Add("UNIDAD")
        cbxUnidad.Items.Add("CAJA")
        cbxUnidad.DropDownStyle = ComboBoxStyle.DropDownList
        cbxUnidad.SelectedIndex = 0

        CargarOperacionesPendientes()

        'Mi equipo
        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Select Top(1) IDEquipo, Equipo, IDZonaDeposito, ZonaDeposito From VEquipoConteoUsuario Where IDUsuario=" & vgIDUsuario).Copy
        If dttemp Is Nothing Then
            Exit Sub
        End If

        If dttemp.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dttemp.Rows(0)

        txtEquipo.txt.Text = oRow("Equipo").ToString
        txtZona.txt.Text = oRow("ZonaDeposito").ToString
        vEquipo = oRow("IDEquipo")

    End Sub

    Sub CargarOperacionesPendientes()

        Dim SQL As String = "Select V.IDTransaccion, V.Num From VControlInventario V Where V.Procesado = 'False' And V.Anulado='False' And V.ControlEquipo='False' And V.IDSucursal=" & vgIDSucursal & " "
        CSistema.SqlToComboBox(cbxOperacion, SQL)

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, New Button, btnImprimir, New Button, New Button, vControles, btnModificar)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        If IDTemp = cbxOperacion.GetValue Then
            Exit Sub
        End If

        IDTemp = cbxOperacion.GetValue
        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        ' Validar
        If cbxOperacion.Validar("Seleccione correctamente una operacion!", ctrError, cbxOperacion, tsslEstado) = False Then
            Exit Sub
        End If

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From ControlInventario Where IDTransaccion = " & cbxOperacion.GetValue & " ), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            CSistema.MostrarError(mensaje, ctrError, cbxOperacion, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        dtDetalle.Clear()

        dt = CSistema.ExecuteToDataTable("Select * From VControlInventario Where IDTransaccion=" & IDTransaccion).Copy
        'dtDetalle = CSistema.ExecuteToDataTable("Select ED.IDProducto, ED.Referencia, ED.Producto, ED.CodigoBarra, 'CantidadDeposito'=ED.Existencia, 'Provisorio'=0, 'Existencia'=ED.Existencia From VExistenciaDepositoInventario ED Join ProductoZona PZ On ED.IDProducto=PZ.IDProducto Join Deposito D on PZ.IDDeposito=D.ID Where ED.IDTransaccion=" & IDTransaccion & " And PZ.IDZonaDeposito=" & vEquipo & " Order By Producto ").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            CSistema.MostrarError("Error en la consulta! Problemas tecnicos.", ctrError, cbxOperacion, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        'Cargar el detalle
        dtDetalle = CSistema.ExecuteToDataTable("Exec SpViewExistenciaDepositoInventario @IDTransaccion=" & IDTransaccion & ", @IDUsuario=" & vgIDUsuario & ", @IDDeposito=" & oRow("IDDeposito") & ", @IDEquipo=" & vEquipo & " ").Copy

        txtSucursal.txt.Text = oRow("Sucursal")
        txtDeposito.txt.Text = oRow("Deposito")
        txtFecha.SetValue(oRow("Fecha"))
        txtEstado.txt.Text = oRow("Estado").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'Cargamos el detalle
        ListarDetalle()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

    End Sub

    Sub CargarDetalle()

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Limpiar la grilla
        dgw.Rows.Clear()

        For Each oRow As DataRowView In dtDetalle.DefaultView
            Dim Registro(6) As String
            Registro(0) = oRow("IDProducto").ToString
            Registro(1) = oRow("Referencia").ToString
            Registro(2) = oRow("Producto").ToString
            Registro(3) = oRow("CodigoBarra").ToString

            Registro(4) = CSistema.FormatoNumero(oRow("Conteo").ToString)
            Registro(5) = CSistema.FormatoNumero(oRow("Averiados").ToString)
            Registro(6) = CSistema.FormatoNumero(oRow("Conteo") + oRow("Averiados"))

            dgw.Rows.Add(Registro)

        Next

    End Sub

    Sub ControlarConteoAnterior()


    End Sub

    Sub CargarProducto()

        ctrError.Clear()
        tsslEstado.Text = ""

        If txtCantidad.SoloLectura = True Then
            Exit Sub
        End If

        Dim ProductoRow As DataRow = txtProducto.Registro

        If txtProducto.Seleccionado = False Then
            txtProducto.txt.txt.Focus()
            Exit Sub
        End If

        'Verificar si existe el producto en la lista
        If dtDetalle.Select("IDProducto=" & ProductoRow("ID")).Count = 0 Then
            'Mostrar error
            CSistema.MostrarError("Este producto no pertenece a la planilla de inventario!", ctrError, txtCantidad, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim Cantidad As Decimal = txtCantidad.ObtenerValor
        Dim Averiados As Decimal = txtAveriados.ObtenerValor
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False
        Dim Total As Decimal = 0

        'Si es por unidad, cantidad igual a seleccionado
        'Si es por caja, multiplicar
        If cbxUnidad.Text = "UNIDAD" Then
            EsCaja = False
        Else
            CantidadCaja = txtCantidad.ObtenerValor
            Cantidad = CDec(txtProducto.Registro("UnidadPorCaja").ToString) * CantidadCaja
            EsCaja = True
        End If

        'Actualizar en el datatable
        For Each oRow As DataRow In dtDetalle.Select("IDProducto=" & ProductoRow("ID"))

            oRow("Conteo") = oRow("Conteo") + Cantidad
            oRow("Averiados") = oRow("Averiados") + Averiados
            Total = oRow("Conteo") + oRow("Averiados")


        Next

        'Actualizar en el detalle
        For i As Integer = 0 To dgw.RowCount - 1

            If dgw.Rows(i).Cells("colID").Value = ProductoRow("ID") Then

                dgw.Rows(i).Cells("colConteo").Value = dgw.Rows(i).Cells("colConteo").Value + Cantidad
                dgw.Rows(i).Cells("colAveriados").Value = dgw.Rows(i).Cells("colAveriados").Value + Averiados


                'Total
                dgw.Rows(i).Cells("colTotal").Value = Total

                dgw.Rows(i).Selected = True
                dgw.CurrentCell = dgw.Rows(i).Cells(0)

                Exit For

            End If

        Next

        'Inicializamos los valores
        txtAveriados.txt.Text = "0"

        txtCantidad.txt.Text = "0"
        txtProducto.SetValue(-1)


        'Retorno de Foco
        txtProducto.txt.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub Imprimir()

        Dim Titulo As String = "TOMA DE INVENTARIO"
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.TomaInventario(frm, Titulo, IDTransaccion, vgUsuarioIdentificador)

    End Sub

    Sub Guardar()

        Dim SQL As String = ""

        'Eliminar el Detalle
        SQL = "Delete From ExistenciaDepositoInventarioEquipo Where IDTransaccion=" & IDTransaccion & " And IDEquipo=" & vEquipo & vbCrLf

        SQL = SQL & InsertarDetalle() & vbCrLf

        'Actualizar
        SQL = SQL & "Exec SpActualizarControlInventario @IDTransaccion=" & IDTransaccion & vbCrLf

        'Guardamos
        Dim RowCount As Integer = CSistema.ExecuteNonQuery(SQL)

        If RowCount = 0 Then
            CSistema.MostrarError("Error de sistema!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        cbxOperacion.SoloLectura = False

    End Sub

    Function InsertarDetalle() As String

        InsertarDetalle = ""

        Dim ControlRow As DataRow = dt.Rows(0)

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param As New DataTable
            CSistema.SetSQLParameter(param, "IDTransaccion", cbxOperacion.GetValue)
            CSistema.SetSQLParameter(param, "IDDeposito", ControlRow("IDDepositoOperacion"))
            CSistema.SetSQLParameter(param, "IDProducto", oRow("IDProducto"))
            CSistema.SetSQLParameter(param, "IDEquipo", vEquipo)
            CSistema.SetSQLParameter(param, "Averiados", CSistema.FormatoNumeroBaseDatos(oRow("Averiados")))
            CSistema.SetSQLParameter(param, "Conteo", CSistema.FormatoNumeroBaseDatos(oRow("Conteo")))
            CSistema.SetSQLParameter(param, "Total", CSistema.FormatoNumeroBaseDatos(oRow("Averiados") + oRow("Conteo")))

            InsertarDetalle = InsertarDetalle & CSistema.InsertSQL(param, "ExistenciaDepositoInventarioEquipo") & vbCrLf

        Next

    End Function

    Sub Modificar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)
        cbxOperacion.SoloLectura = True

        txtProducto.SoloLectura = False
        cbxUnidad.Enabled = True
        txtCantidad.SoloLectura = False


        txtProducto.Focus()
        txtProducto.txt.Focus()

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        CargarOperacion()
        cbxOperacion.SoloLectura = False
        cbxOperacion.Focus()

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        Select Case e.KeyCode
            Case Keys.Enter

                If txtCantidad.txt.Focused = True Then
                    Exit Sub
                End If

                If txtAveriados.txt.Focused = True Then
                    Exit Sub
                End If

                If txtProducto.txt.Focused = True Then
                    Exit Sub
                End If

                If cbxOperacion.cbx.Focused = True Then
                    Exit Sub
                End If

            Case vgKeyConsultar

        End Select

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub cbxOperacion_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOperacion.Leave
        CargarOperacion()
    End Sub

    Private Sub frmTomaInventario_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmTomaInventario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        ManejarTecla(e)
    End Sub

    Private Sub frmTomaInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.TeclaPrecionada, txtAveriados.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub cbxOperacion_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxOperacion.PropertyChanged
        CargarOperacion()
    End Sub

    Private Sub cbxOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
            cbxOperacion.cbx.SelectAll()
        End If
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub OcxTXTProducto1_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            cbxUnidad.Focus()
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    
    Private Sub dgw_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgw.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            With Me.dgw
                Dim Hitest As DataGridView.HitTestInfo = .HitTest(e.X, e.Y)
                If Hitest.Type = DataGridViewHitTestType.Cell Then

                    .CurrentCell = .Rows(Hitest.RowIndex).Cells(Hitest.ColumnIndex)
                End If
            End With
        End If
    End Sub

    Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
        Dim ControlRow As DataRow = dt.Rows(0)
        Dim IDtransaccion As Integer
        Dim IDDeposito As Integer
        Dim IDProducto As Integer = dgw.SelectedRows(0).Cells("ColID").Value

        IDtransaccion = cbxOperacion.GetValue
        IDDeposito = ControlRow("IDDepositoOperacion")

        Dim RowCount As Integer = CSistema.ExecuteNonQuery("Exec SpRestablecerTomaInventarioConteo " & IDtransaccion & ", " & IDProducto & ", " & IDDeposito & ", " & vEquipo & "")

        If RowCount = 0 Then
            CSistema.MostrarError("Error de sistema!", ctrError, dgw, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        IDTemp = 0
        CargarOperacion()

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmTomaInventario_Activate()
        Me.Refresh()
    End Sub
End Class
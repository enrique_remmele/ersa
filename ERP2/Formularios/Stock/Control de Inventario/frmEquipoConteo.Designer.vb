﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEquipoConteo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblEquipo = New System.Windows.Forms.Label()
        Me.lblZona = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbxUsuarios = New System.Windows.Forms.ListBox()
        Me.lklAgregar = New System.Windows.Forms.LinkLabel()
        Me.rdbDesactivado = New System.Windows.Forms.RadioButton()
        Me.rdbActivo = New System.Windows.Forms.RadioButton()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lklQuitar = New System.Windows.Forms.LinkLabel()
        Me.lblUsuarios = New System.Windows.Forms.Label()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.lblEquipoConteo = New System.Windows.Forms.Label()
        Me.ndEquipoConteo = New System.Windows.Forms.NumericUpDown()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.lblDeposito = New System.Windows.Forms.Label()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.cbxZona = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtEquipo = New ERP.ocxTXTString()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ndEquipoConteo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblEquipo
        '
        Me.lblEquipo.AutoSize = True
        Me.lblEquipo.Location = New System.Drawing.Point(12, 45)
        Me.lblEquipo.Name = "lblEquipo"
        Me.lblEquipo.Size = New System.Drawing.Size(43, 13)
        Me.lblEquipo.TabIndex = 2
        Me.lblEquipo.Text = "Equipo:"
        '
        'lblZona
        '
        Me.lblZona.AutoSize = True
        Me.lblZona.Location = New System.Drawing.Point(11, 130)
        Me.lblZona.Name = "lblZona"
        Me.lblZona.Size = New System.Drawing.Size(35, 13)
        Me.lblZona.TabIndex = 4
        Me.lblZona.Text = "Zona:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(12, 18)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(304, 182)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(385, 182)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 11
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(223, 182)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 9
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(142, 182)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 8
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(61, 182)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 7
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 429)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(472, 22)
        Me.StatusStrip1.TabIndex = 19
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'lbxUsuarios
        '
        Me.lbxUsuarios.FormattingEnabled = True
        Me.lbxUsuarios.Location = New System.Drawing.Point(240, 32)
        Me.lbxUsuarios.Name = "lbxUsuarios"
        Me.lbxUsuarios.Size = New System.Drawing.Size(219, 82)
        Me.lbxUsuarios.TabIndex = 14
        '
        'lklAgregar
        '
        Me.lklAgregar.AutoSize = True
        Me.lklAgregar.Location = New System.Drawing.Point(291, 16)
        Me.lklAgregar.Name = "lklAgregar"
        Me.lklAgregar.Size = New System.Drawing.Size(44, 13)
        Me.lklAgregar.TabIndex = 12
        Me.lklAgregar.TabStop = True
        Me.lklAgregar.Text = "Agregar"
        '
        'rdbDesactivado
        '
        Me.rdbDesactivado.AutoSize = True
        Me.rdbDesactivado.Location = New System.Drawing.Point(126, 159)
        Me.rdbDesactivado.Name = "rdbDesactivado"
        Me.rdbDesactivado.Size = New System.Drawing.Size(85, 17)
        Me.rdbDesactivado.TabIndex = 6
        Me.rdbDesactivado.TabStop = True
        Me.rdbDesactivado.Text = "Desactivado"
        Me.rdbDesactivado.UseVisualStyleBackColor = True
        '
        'rdbActivo
        '
        Me.rdbActivo.AutoSize = True
        Me.rdbActivo.Location = New System.Drawing.Point(65, 159)
        Me.rdbActivo.Name = "rdbActivo"
        Me.rdbActivo.Size = New System.Drawing.Size(55, 17)
        Me.rdbActivo.TabIndex = 5
        Me.rdbActivo.TabStop = True
        Me.rdbActivo.Text = "Activo"
        Me.rdbActivo.UseVisualStyleBackColor = True
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(12, 161)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 6
        Me.lblEstado.Text = "Estado:"
        '
        'lklQuitar
        '
        Me.lklQuitar.AutoSize = True
        Me.lklQuitar.Location = New System.Drawing.Point(335, 16)
        Me.lklQuitar.Name = "lklQuitar"
        Me.lklQuitar.Size = New System.Drawing.Size(35, 13)
        Me.lklQuitar.TabIndex = 13
        Me.lklQuitar.TabStop = True
        Me.lklQuitar.Text = "Quitar"
        '
        'lblUsuarios
        '
        Me.lblUsuarios.AutoSize = True
        Me.lblUsuarios.Location = New System.Drawing.Point(240, 16)
        Me.lblUsuarios.Name = "lblUsuarios"
        Me.lblUsuarios.Size = New System.Drawing.Size(51, 13)
        Me.lblUsuarios.TabIndex = 9
        Me.lblUsuarios.Text = "Usuarios:"
        '
        'ListView1
        '
        Me.ListView1.Location = New System.Drawing.Point(66, 157)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(399, 184)
        Me.ListView1.TabIndex = 18
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'lvLista
        '
        Me.lvLista.Location = New System.Drawing.Point(60, 211)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(399, 192)
        Me.lvLista.TabIndex = 15
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'lblEquipoConteo
        '
        Me.lblEquipoConteo.AutoSize = True
        Me.lblEquipoConteo.Location = New System.Drawing.Point(117, 18)
        Me.lblEquipoConteo.Name = "lblEquipoConteo"
        Me.lblEquipoConteo.Size = New System.Drawing.Size(68, 13)
        Me.lblEquipoConteo.TabIndex = 92
        Me.lblEquipoConteo.Text = "Equi.Conteo:"
        '
        'ndEquipoConteo
        '
        Me.ndEquipoConteo.Location = New System.Drawing.Point(181, 15)
        Me.ndEquipoConteo.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.ndEquipoConteo.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.ndEquipoConteo.Name = "ndEquipoConteo"
        Me.ndEquipoConteo.Size = New System.Drawing.Size(50, 20)
        Me.ndEquipoConteo.TabIndex = 0
        Me.ndEquipoConteo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(11, 74)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 98
        Me.lblSucursal.Text = "Sucursal:"
        '
        'lblDeposito
        '
        Me.lblDeposito.AutoSize = True
        Me.lblDeposito.Location = New System.Drawing.Point(11, 102)
        Me.lblDeposito.Name = "lblDeposito"
        Me.lblDeposito.Size = New System.Drawing.Size(52, 13)
        Me.lblDeposito.TabIndex = 99
        Me.lblDeposito.Text = "Deposito:"
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "Deposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "Deposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(67, 98)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(161, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 3
        Me.cbxDeposito.Texto = "VENTAS01"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(67, 70)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(161, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 2
        Me.cbxSucursal.Texto = "ASUNCION - CENTRAL"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(68, 41)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(163, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 1
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'cbxZona
        '
        Me.cbxZona.CampoWhere = Nothing
        Me.cbxZona.CargarUnaSolaVez = False
        Me.cbxZona.DataDisplayMember = "Descripcion"
        Me.cbxZona.DataFilter = Nothing
        Me.cbxZona.DataOrderBy = "Descripcion"
        Me.cbxZona.DataSource = "VZonaDeposito"
        Me.cbxZona.DataValueMember = "ID"
        Me.cbxZona.dtSeleccionado = Nothing
        Me.cbxZona.FormABM = Nothing
        Me.cbxZona.Indicaciones = Nothing
        Me.cbxZona.Location = New System.Drawing.Point(67, 126)
        Me.cbxZona.Name = "cbxZona"
        Me.cbxZona.SeleccionMultiple = False
        Me.cbxZona.SeleccionObligatoria = False
        Me.cbxZona.Size = New System.Drawing.Size(161, 21)
        Me.cbxZona.SoloLectura = False
        Me.cbxZona.TabIndex = 4
        Me.cbxZona.Texto = "ASU CENTRAL 1"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(67, 13)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(40, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtEquipo
        '
        Me.txtEquipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEquipo.Color = System.Drawing.Color.Empty
        Me.txtEquipo.Indicaciones = Nothing
        Me.txtEquipo.Location = New System.Drawing.Point(64, 41)
        Me.txtEquipo.Multilinea = False
        Me.txtEquipo.Name = "txtEquipo"
        Me.txtEquipo.Size = New System.Drawing.Size(175, 21)
        Me.txtEquipo.SoloLectura = False
        Me.txtEquipo.TabIndex = 3
        Me.txtEquipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEquipo.Texto = ""
        '
        'frmEquipoConteo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(472, 451)
        Me.Controls.Add(Me.lblDeposito)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.cbxDeposito)
        Me.Controls.Add(Me.cbxSucursal)
        Me.Controls.Add(Me.ndEquipoConteo)
        Me.Controls.Add(Me.lblEquipoConteo)
        Me.Controls.Add(Me.lvLista)
        Me.Controls.Add(Me.lblUsuarios)
        Me.Controls.Add(Me.lklQuitar)
        Me.Controls.Add(Me.rdbDesactivado)
        Me.Controls.Add(Me.rdbActivo)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lklAgregar)
        Me.Controls.Add(Me.lbxUsuarios)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.txtDescripcion)
        Me.Controls.Add(Me.lblEquipo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.cbxZona)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.lblZona)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Name = "frmEquipoConteo"
        Me.Tag = "EQUIPOS DE CONTEO"
        Me.Text = "frmEquipoConteo"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ndEquipoConteo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblEquipo As System.Windows.Forms.Label
    Friend WithEvents cbxZona As ERP.ocxCBX
    Friend WithEvents lblZona As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lklAgregar As System.Windows.Forms.LinkLabel
    Friend WithEvents lbxUsuarios As System.Windows.Forms.ListBox
    Friend WithEvents rdbDesactivado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbActivo As System.Windows.Forms.RadioButton
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarios As System.Windows.Forms.Label
    Friend WithEvents lklQuitar As System.Windows.Forms.LinkLabel
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents txtEquipo As ERP.ocxTXTString
    Friend WithEvents ndEquipoConteo As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblEquipoConteo As System.Windows.Forms.Label
    Friend WithEvents lblDeposito As System.Windows.Forms.Label
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
End Class

﻿Imports ERP.Reporte
Public Class frmComparativoInventarioEquipo

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CReporte As New CReporteStock
    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtDetalleEquipo As New DataTable
    Dim dt As New DataTable
    Dim vControles() As Control
    Dim vEquipo As Integer
    Dim IDTemp As Integer

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        dgw.MultiSelect = False

        'Funciones
        CargarInformacion()
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        'Foco
        txtID.Focus()
        txtID.txt.Focus()

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        'Cabecera

        'Mi equipo
        Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Select Top(1) IDEquipo, Equipo, IDZonaDeposito, ZonaDeposito From VEquipoConteoUsuario Where IDUsuario=" & vgIDUsuario).Copy
        If dttemp Is Nothing Then
            Exit Sub
        End If

        If dttemp.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dttemp.Rows(0)

        txtEquipo.txt.Text = oRow("Equipo").ToString
        txtZona.txt.Text = oRow("ZonaDeposito").ToString
        vEquipo = oRow("IDEquipo")

    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, New Button, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBuscar, New Button, vControles, btnModificar)

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        ctrError.Clear()
        tsslEstado.Text = ""

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From ControlInventario Where IDSucursal=" & vgIDSucursal & " And Numero = " & txtID.ObtenerValor & " ), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If

        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            CSistema.MostrarError(mensaje, ctrError, txtID, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        dtDetalle.Clear()

        dt = CSistema.ExecuteToDataTable("Select * From VControlInventario Where IDTransaccion=" & IDTransaccion).Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            CSistema.MostrarError("Error en la consulta! Problemas tecnico.", ctrError, txtID, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        'Cargar el detalle
        dtDetalle = CSistema.ExecuteToDataTable("Select * From vExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion & " Order By Producto").Copy

        txtSucursal.txt.Text = oRow("Sucursal")
        txtDeposito.txt.Text = oRow("Deposito")
        txtFecha.SetValue(oRow("Fecha"))
        txtEstado.txt.Text = oRow("Estado").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString

        'Cargamos el detalle
        ListarDetalle()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

    End Sub

    Sub ListarDetalle()

        'Limpiar la grilla
        dgw.Rows.Clear()

        For Each oRow As DataRowView In dtDetalle.DefaultView
            Dim Registro(10) As String
            Registro(0) = oRow("IDProducto").ToString
            Registro(1) = oRow("Referencia").ToString
            Registro(2) = oRow("Producto").ToString
            Registro(3) = oRow("CodigoBarra").ToString

            Registro(4) = CSistema.FormatoNumero(oRow("Equipo1").ToString)
            Registro(5) = CSistema.FormatoNumero(oRow("Averiados1").ToString)

            Registro(6) = CSistema.FormatoNumero(oRow("Equipo2").ToString)
            Registro(7) = CSistema.FormatoNumero(oRow("Averiados2").ToString)

            Registro(8) = CSistema.FormatoNumero(oRow("ConteoDiferencia").ToString)
            Registro(9) = CSistema.FormatoNumero(oRow("AveriadosDiferencia").ToString)

            Registro(10) = CSistema.FormatoNumero(oRow("TotalDiferencia").ToString)

            dgw.Rows.Add(Registro)

        Next

        txtCantidad.txt.Text = dgw.Rows.Count

    End Sub

    Sub FiltarDataGridView(ByVal Filtro As Integer)

        Dim Cantidad As Integer

        For Each oRow As DataGridViewRow In dgw.Rows
            If Filtro = 0 Then
                oRow.Visible = True
                Cantidad = dgw.Rows.Count
                GoTo siguiente
            End If

            If Filtro = 1 Then
                If oRow.Cells("colDiferenciaTotal").Value > 0 Then
                    oRow.Visible = True
                    Cantidad = Cantidad + 1
                Else
                    oRow.Visible = False
                End If

                GoTo siguiente
            End If

            If Filtro = 2 Then
                If oRow.Cells("colDiferenciaTotal").Value > 0 Then
                    oRow.Visible = False
                Else
                    oRow.Visible = True
                    Cantidad = Cantidad + 1
                End If

                GoTo siguiente
            End If


siguiente:

        Next

        txtCantidad.txt.Text = Cantidad

    End Sub

    Sub Imprimir()

        If IDTransaccion <= 0 Then
            Exit Sub
        End If

        Dim Titulo As String = "COMPARATIVO INVENTARIO EQUIPO"
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ComparativoInventarioEquipo(frm, Titulo, IDTransaccion, vgUsuarioIdentificador)

    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)

    End Sub

    Sub Anular()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.ANULAR_ELIMINAR)
        Guardar(ERP.CSistema.NUMOperacionesRegistro.ANULAR)

    End Sub

    Sub Busqueda()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

    End Sub

    Sub Modificar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR)

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Si es para anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Desea anular el proceso? Obs: Podra igualmente procesar luego", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                Return True
            Else
                Return False
            End If

        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer = 0

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpControlInventarioControlarEquipo", False, False, MensajeRetorno) = False Then
            CSistema.MostrarError("Atencion: " & MensajeRetorno, ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub

        End If

        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)

    End Sub

    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
            txtID.txt.SelectAll()
            Exit Sub
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.ObtenerValor
            ID = CInt(ID) + 1
            txtID.SetValue(ID)
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.ObtenerValor

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.SetValue(ID)
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VControlInventario Where IDSucursal=" & vgIDSucursal & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VControlInventario Where IDSucursal=" & vgIDSucursal & " "), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            Busqueda()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            'Nuevo()
        End If

    End Sub

    Private Sub frmTomaInventario_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmTomaInventario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmTomaInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Imprimir()
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            FiltarDataGridView(0)
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            FiltarDataGridView(1)
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            FiltarDataGridView(2)
        End If
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        ManejarTecla(e)

    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnImprimir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Busqueda()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modificar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmComparativoInventarioEquipo_Activate()
        Me.Refresh()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAjusteProvisorio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtProducto = New ERP.ocxTXTString()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.gbxProducto = New System.Windows.Forms.GroupBox()
        Me.lblCodigoBarra = New System.Windows.Forms.Label()
        Me.txtCodigoBarra = New ERP.ocxTXTString()
        Me.txtIDProducto = New ERP.ocxTXTString()
        Me.lblReferencia = New System.Windows.Forms.Label()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.gbxMotivo = New System.Windows.Forms.GroupBox()
        Me.txtCantidad = New ERP.ocxTXTNumeric()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.txtTipoDocumento = New ERP.ocxTXTString()
        Me.lblTipoDocumento = New System.Windows.Forms.Label()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.gbxProducto.SuspendLayout()
        Me.gbxMotivo.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtProducto
        '
        Me.txtProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProducto.Color = System.Drawing.Color.Empty
        Me.txtProducto.Indicaciones = Nothing
        Me.txtProducto.Location = New System.Drawing.Point(124, 13)
        Me.txtProducto.Multilinea = False
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(308, 21)
        Me.txtProducto.SoloLectura = True
        Me.txtProducto.TabIndex = 2
        Me.txtProducto.TabStop = False
        Me.txtProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtProducto.Texto = ""
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Location = New System.Drawing.Point(30, 17)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(53, 13)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        '
        'gbxProducto
        '
        Me.gbxProducto.Controls.Add(Me.lblCodigoBarra)
        Me.gbxProducto.Controls.Add(Me.txtCodigoBarra)
        Me.gbxProducto.Controls.Add(Me.txtIDProducto)
        Me.gbxProducto.Controls.Add(Me.lblReferencia)
        Me.gbxProducto.Controls.Add(Me.txtReferencia)
        Me.gbxProducto.Controls.Add(Me.lblProducto)
        Me.gbxProducto.Controls.Add(Me.txtProducto)
        Me.gbxProducto.Location = New System.Drawing.Point(12, 12)
        Me.gbxProducto.Name = "gbxProducto"
        Me.gbxProducto.Size = New System.Drawing.Size(438, 67)
        Me.gbxProducto.TabIndex = 0
        Me.gbxProducto.TabStop = False
        '
        'lblCodigoBarra
        '
        Me.lblCodigoBarra.AutoSize = True
        Me.lblCodigoBarra.Location = New System.Drawing.Point(182, 44)
        Me.lblCodigoBarra.Name = "lblCodigoBarra"
        Me.lblCodigoBarra.Size = New System.Drawing.Size(91, 13)
        Me.lblCodigoBarra.TabIndex = 5
        Me.lblCodigoBarra.Text = "Codigo de Barras:"
        '
        'txtCodigoBarra
        '
        Me.txtCodigoBarra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoBarra.Color = System.Drawing.Color.Empty
        Me.txtCodigoBarra.Indicaciones = Nothing
        Me.txtCodigoBarra.Location = New System.Drawing.Point(279, 40)
        Me.txtCodigoBarra.Multilinea = False
        Me.txtCodigoBarra.Name = "txtCodigoBarra"
        Me.txtCodigoBarra.Size = New System.Drawing.Size(153, 21)
        Me.txtCodigoBarra.SoloLectura = True
        Me.txtCodigoBarra.TabIndex = 6
        Me.txtCodigoBarra.TabStop = False
        Me.txtCodigoBarra.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCodigoBarra.Texto = ""
        '
        'txtIDProducto
        '
        Me.txtIDProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIDProducto.Color = System.Drawing.Color.Empty
        Me.txtIDProducto.Indicaciones = Nothing
        Me.txtIDProducto.Location = New System.Drawing.Point(89, 13)
        Me.txtIDProducto.Multilinea = False
        Me.txtIDProducto.Name = "txtIDProducto"
        Me.txtIDProducto.Size = New System.Drawing.Size(35, 21)
        Me.txtIDProducto.SoloLectura = True
        Me.txtIDProducto.TabIndex = 1
        Me.txtIDProducto.TabStop = False
        Me.txtIDProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtIDProducto.Texto = ""
        '
        'lblReferencia
        '
        Me.lblReferencia.AutoSize = True
        Me.lblReferencia.Location = New System.Drawing.Point(56, 44)
        Me.lblReferencia.Name = "lblReferencia"
        Me.lblReferencia.Size = New System.Drawing.Size(27, 13)
        Me.lblReferencia.TabIndex = 3
        Me.lblReferencia.Text = "Ref:"
        '
        'txtReferencia
        '
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Indicaciones = Nothing
        Me.txtReferencia.Location = New System.Drawing.Point(89, 40)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(87, 21)
        Me.txtReferencia.SoloLectura = True
        Me.txtReferencia.TabIndex = 4
        Me.txtReferencia.TabStop = False
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtReferencia.Texto = ""
        '
        'gbxMotivo
        '
        Me.gbxMotivo.Controls.Add(Me.txtCantidad)
        Me.gbxMotivo.Controls.Add(Me.lblCantidad)
        Me.gbxMotivo.Controls.Add(Me.txtObservacion)
        Me.gbxMotivo.Controls.Add(Me.lblObservacion)
        Me.gbxMotivo.Controls.Add(Me.txtComprobante)
        Me.gbxMotivo.Controls.Add(Me.lblComprobante)
        Me.gbxMotivo.Controls.Add(Me.txtTipoDocumento)
        Me.gbxMotivo.Controls.Add(Me.lblTipoDocumento)
        Me.gbxMotivo.Location = New System.Drawing.Point(12, 85)
        Me.gbxMotivo.Name = "gbxMotivo"
        Me.gbxMotivo.Size = New System.Drawing.Size(438, 107)
        Me.gbxMotivo.TabIndex = 1
        Me.gbxMotivo.TabStop = False
        '
        'txtCantidad
        '
        Me.txtCantidad.Color = System.Drawing.Color.Empty
        Me.txtCantidad.Decimales = False
        Me.txtCantidad.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCantidad.Location = New System.Drawing.Point(369, 73)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(63, 21)
        Me.txtCantidad.SoloLectura = False
        Me.txtCantidad.TabIndex = 7
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidad.Texto = "0"
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Location = New System.Drawing.Point(311, 77)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(52, 13)
        Me.lblCantidad.TabIndex = 6
        Me.lblCantidad.Text = "Cantidad:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(89, 46)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(343, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 5
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(13, 50)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 4
        Me.lblObservacion.Text = "Observacion:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(306, 19)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(126, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 3
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(221, 23)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 2
        Me.lblComprobante.Text = "Comprobante:"
        '
        'txtTipoDocumento
        '
        Me.txtTipoDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoDocumento.Color = System.Drawing.Color.Empty
        Me.txtTipoDocumento.Indicaciones = Nothing
        Me.txtTipoDocumento.Location = New System.Drawing.Point(89, 19)
        Me.txtTipoDocumento.Multilinea = False
        Me.txtTipoDocumento.Name = "txtTipoDocumento"
        Me.txtTipoDocumento.Size = New System.Drawing.Size(126, 21)
        Me.txtTipoDocumento.SoloLectura = False
        Me.txtTipoDocumento.TabIndex = 1
        Me.txtTipoDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoDocumento.Texto = ""
        '
        'lblTipoDocumento
        '
        Me.lblTipoDocumento.AutoSize = True
        Me.lblTipoDocumento.Location = New System.Drawing.Point(5, 23)
        Me.lblTipoDocumento.Name = "lblTipoDocumento"
        Me.lblTipoDocumento.Size = New System.Drawing.Size(78, 13)
        Me.lblTipoDocumento.TabIndex = 0
        Me.lblTipoDocumento.Text = "T. Documento:"
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(12, 198)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(438, 20)
        Me.flpRegistradoPor.TabIndex = 2
        Me.flpRegistradoPor.Visible = False
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(3, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 0
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(369, 224)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(288, 224)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 256)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(463, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'frmAjusteProvisorio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(463, 278)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.gbxMotivo)
        Me.Controls.Add(Me.gbxProducto)
        Me.Name = "frmAjusteProvisorio"
        Me.Text = "frmAjusteProvisorio"
        Me.gbxProducto.ResumeLayout(False)
        Me.gbxProducto.PerformLayout()
        Me.gbxMotivo.ResumeLayout(False)
        Me.gbxMotivo.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtProducto As ERP.ocxTXTString
    Friend WithEvents lblProducto As System.Windows.Forms.Label
    Friend WithEvents gbxProducto As System.Windows.Forms.GroupBox
    Friend WithEvents lblReferencia As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As ERP.ocxTXTString
    Friend WithEvents lblCodigoBarra As System.Windows.Forms.Label
    Friend WithEvents txtCodigoBarra As ERP.ocxTXTString
    Friend WithEvents txtIDProducto As ERP.ocxTXTString
    Friend WithEvents gbxMotivo As System.Windows.Forms.GroupBox
    Friend WithEvents lblTipoDocumento As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtTipoDocumento As ERP.ocxTXTString
    Friend WithEvents txtCantidad As ERP.ocxTXTNumeric
    Friend WithEvents lblCantidad As System.Windows.Forms.Label
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class

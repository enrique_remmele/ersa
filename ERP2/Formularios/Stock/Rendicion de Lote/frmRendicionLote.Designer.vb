﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRendicionLote
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxCabecera = New System.Windows.Forms.GroupBox()
        Me.txtNroLote = New ERP.ocxTXTNumeric()
        Me.txtZona = New ERP.ocxTXTString()
        Me.txtChofer = New ERP.ocxTXTString()
        Me.txtVehiculo = New ERP.ocxTXTString()
        Me.txtDistribuidor = New ERP.ocxTXTString()
        Me.lblLote = New System.Windows.Forms.Label()
        Me.lblZona = New System.Windows.Forms.Label()
        Me.lblChofer = New System.Windows.Forms.Label()
        Me.lblVehiculo = New System.Windows.Forms.Label()
        Me.lblDistribuidor = New System.Windows.Forms.Label()
        Me.txtFechaReparto = New ERP.ocxTXTDate()
        Me.lblFechaReparto = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.gbxCheque = New System.Windows.Forms.GroupBox()
        Me.ocxCheque = New ERP.ocxCargaCheque()
        Me.gbxEfectivo = New System.Windows.Forms.GroupBox()
        Me.ocxEfectivo = New ERP.ocxDesgloseBilletes()
        Me.tcDocumentos = New System.Windows.Forms.TabControl()
        Me.tpDocumentos = New System.Windows.Forms.TabPage()
        Me.ocxDocumento = New ERP.ocxRendicionLoteDocumento()
        Me.tpVentasAnuladas = New System.Windows.Forms.TabPage()
        Me.ocxAnulados = New ERP.ocxRendicionLoteVentasAnuladas()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.OcxRendicionLoteCobranzaExtra1 = New ERP.ocxRendicionLoteCobranzaExtra()
        Me.btnBusquedaAvanzada = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.flpAnuladoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.lblUsuarioAnulado = New System.Windows.Forms.Label()
        Me.lblFechaAnulado = New System.Windows.Forms.Label()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.flpRegistradoPor = New System.Windows.Forms.FlowLayoutPanel()
        Me.lblRegistradoPor = New System.Windows.Forms.Label()
        Me.lblUsuarioRegistro = New System.Windows.Forms.Label()
        Me.lblFechaRegistro = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblTotalLote = New System.Windows.Forms.Label()
        Me.txtTotalContadoLote = New ERP.ocxTXTNumeric()
        Me.txtTotal = New ERP.ocxTXTNumeric()
        Me.gbxCabecera.SuspendLayout()
        Me.gbxCheque.SuspendLayout()
        Me.gbxEfectivo.SuspendLayout()
        Me.tcDocumentos.SuspendLayout()
        Me.tpDocumentos.SuspendLayout()
        Me.tpVentasAnuladas.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.flpAnuladoPor.SuspendLayout()
        Me.flpRegistradoPor.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxCabecera
        '
        Me.gbxCabecera.Controls.Add(Me.txtNroLote)
        Me.gbxCabecera.Controls.Add(Me.txtZona)
        Me.gbxCabecera.Controls.Add(Me.txtChofer)
        Me.gbxCabecera.Controls.Add(Me.txtVehiculo)
        Me.gbxCabecera.Controls.Add(Me.txtDistribuidor)
        Me.gbxCabecera.Controls.Add(Me.lblLote)
        Me.gbxCabecera.Controls.Add(Me.lblZona)
        Me.gbxCabecera.Controls.Add(Me.lblChofer)
        Me.gbxCabecera.Controls.Add(Me.lblVehiculo)
        Me.gbxCabecera.Controls.Add(Me.lblDistribuidor)
        Me.gbxCabecera.Controls.Add(Me.txtFechaReparto)
        Me.gbxCabecera.Controls.Add(Me.lblFechaReparto)
        Me.gbxCabecera.Controls.Add(Me.cbxSucursal)
        Me.gbxCabecera.Controls.Add(Me.lblSucursal)
        Me.gbxCabecera.Controls.Add(Me.txtObservacion)
        Me.gbxCabecera.Controls.Add(Me.txtFecha)
        Me.gbxCabecera.Controls.Add(Me.lblFecha)
        Me.gbxCabecera.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxCabecera.Controls.Add(Me.txtComprobante)
        Me.gbxCabecera.Controls.Add(Me.cbxCiudad)
        Me.gbxCabecera.Controls.Add(Me.txtID)
        Me.gbxCabecera.Controls.Add(Me.lblObservacion)
        Me.gbxCabecera.Controls.Add(Me.lblComprobante)
        Me.gbxCabecera.Controls.Add(Me.lblOperacion)
        Me.gbxCabecera.Location = New System.Drawing.Point(11, 2)
        Me.gbxCabecera.Name = "gbxCabecera"
        Me.gbxCabecera.Size = New System.Drawing.Size(698, 122)
        Me.gbxCabecera.TabIndex = 0
        Me.gbxCabecera.TabStop = False
        '
        'txtNroLote
        '
        Me.txtNroLote.Color = System.Drawing.Color.Empty
        Me.txtNroLote.Decimales = False
        Me.txtNroLote.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtNroLote.Location = New System.Drawing.Point(346, 12)
        Me.txtNroLote.Name = "txtNroLote"
        Me.txtNroLote.Size = New System.Drawing.Size(133, 21)
        Me.txtNroLote.SoloLectura = False
        Me.txtNroLote.TabIndex = 9
        Me.txtNroLote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroLote.Texto = "0"
        '
        'txtZona
        '
        Me.txtZona.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtZona.Color = System.Drawing.Color.Empty
        Me.txtZona.Indicaciones = Nothing
        Me.txtZona.Location = New System.Drawing.Point(573, 66)
        Me.txtZona.Multilinea = False
        Me.txtZona.Name = "txtZona"
        Me.txtZona.Size = New System.Drawing.Size(117, 21)
        Me.txtZona.SoloLectura = True
        Me.txtZona.TabIndex = 21
        Me.txtZona.TabStop = False
        Me.txtZona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtZona.Texto = ""
        '
        'txtChofer
        '
        Me.txtChofer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtChofer.Color = System.Drawing.Color.Empty
        Me.txtChofer.Indicaciones = Nothing
        Me.txtChofer.Location = New System.Drawing.Point(264, 66)
        Me.txtChofer.Multilinea = False
        Me.txtChofer.Name = "txtChofer"
        Me.txtChofer.Size = New System.Drawing.Size(189, 21)
        Me.txtChofer.SoloLectura = True
        Me.txtChofer.TabIndex = 19
        Me.txtChofer.TabStop = False
        Me.txtChofer.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtChofer.Texto = ""
        '
        'txtVehiculo
        '
        Me.txtVehiculo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVehiculo.Color = System.Drawing.Color.Empty
        Me.txtVehiculo.Indicaciones = Nothing
        Me.txtVehiculo.Location = New System.Drawing.Point(76, 66)
        Me.txtVehiculo.Multilinea = False
        Me.txtVehiculo.Name = "txtVehiculo"
        Me.txtVehiculo.Size = New System.Drawing.Size(147, 21)
        Me.txtVehiculo.SoloLectura = True
        Me.txtVehiculo.TabIndex = 17
        Me.txtVehiculo.TabStop = False
        Me.txtVehiculo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtVehiculo.Texto = ""
        '
        'txtDistribuidor
        '
        Me.txtDistribuidor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDistribuidor.Color = System.Drawing.Color.Empty
        Me.txtDistribuidor.Indicaciones = Nothing
        Me.txtDistribuidor.Location = New System.Drawing.Point(422, 41)
        Me.txtDistribuidor.Multilinea = False
        Me.txtDistribuidor.Name = "txtDistribuidor"
        Me.txtDistribuidor.Size = New System.Drawing.Size(268, 21)
        Me.txtDistribuidor.SoloLectura = True
        Me.txtDistribuidor.TabIndex = 15
        Me.txtDistribuidor.TabStop = False
        Me.txtDistribuidor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDistribuidor.Texto = ""
        '
        'lblLote
        '
        Me.lblLote.AutoSize = True
        Me.lblLote.Location = New System.Drawing.Point(319, 16)
        Me.lblLote.Name = "lblLote"
        Me.lblLote.Size = New System.Drawing.Size(31, 13)
        Me.lblLote.TabIndex = 8
        Me.lblLote.Text = "Lote:"
        '
        'lblZona
        '
        Me.lblZona.AutoSize = True
        Me.lblZona.Location = New System.Drawing.Point(459, 70)
        Me.lblZona.Name = "lblZona"
        Me.lblZona.Size = New System.Drawing.Size(108, 13)
        Me.lblZona.TabIndex = 20
        Me.lblZona.Text = "Zona de Distribucion:"
        '
        'lblChofer
        '
        Me.lblChofer.AutoSize = True
        Me.lblChofer.Location = New System.Drawing.Point(223, 70)
        Me.lblChofer.Name = "lblChofer"
        Me.lblChofer.Size = New System.Drawing.Size(41, 13)
        Me.lblChofer.TabIndex = 18
        Me.lblChofer.Text = "Chofer:"
        '
        'lblVehiculo
        '
        Me.lblVehiculo.AutoSize = True
        Me.lblVehiculo.Location = New System.Drawing.Point(25, 70)
        Me.lblVehiculo.Name = "lblVehiculo"
        Me.lblVehiculo.Size = New System.Drawing.Size(51, 13)
        Me.lblVehiculo.TabIndex = 16
        Me.lblVehiculo.Text = "Vehiculo:"
        '
        'lblDistribuidor
        '
        Me.lblDistribuidor.AutoSize = True
        Me.lblDistribuidor.Location = New System.Drawing.Point(360, 44)
        Me.lblDistribuidor.Name = "lblDistribuidor"
        Me.lblDistribuidor.Size = New System.Drawing.Size(62, 13)
        Me.lblDistribuidor.TabIndex = 14
        Me.lblDistribuidor.Text = "Distribuidor:"
        '
        'txtFechaReparto
        '
        Me.txtFechaReparto.AñoFecha = 0
        Me.txtFechaReparto.Color = System.Drawing.Color.Empty
        Me.txtFechaReparto.Fecha = New Date(2013, 2, 6, 14, 1, 51, 312)
        Me.txtFechaReparto.Location = New System.Drawing.Point(279, 40)
        Me.txtFechaReparto.MesFecha = 0
        Me.txtFechaReparto.Name = "txtFechaReparto"
        Me.txtFechaReparto.PermitirNulo = False
        Me.txtFechaReparto.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaReparto.SoloLectura = True
        Me.txtFechaReparto.TabIndex = 13
        Me.txtFechaReparto.TabStop = False
        '
        'lblFechaReparto
        '
        Me.lblFechaReparto.AutoSize = True
        Me.lblFechaReparto.Location = New System.Drawing.Point(177, 44)
        Me.lblFechaReparto.Name = "lblFechaReparto"
        Me.lblFechaReparto.Size = New System.Drawing.Size(96, 13)
        Me.lblFechaReparto.TabIndex = 12
        Me.lblFechaReparto.Text = "Fecha de Reparto:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = Nothing
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = Nothing
        Me.cbxSucursal.DataValueMember = Nothing
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(246, 12)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(67, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 4
        Me.cbxSucursal.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(195, 16)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 3
        Me.lblSucursal.Text = "Sucursal:"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(76, 93)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(614, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 23
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 2, 6, 14, 1, 51, 312)
        Me.txtFecha.Location = New System.Drawing.Point(76, 40)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(95, 20)
        Me.txtFecha.SoloLectura = True
        Me.txtFecha.TabIndex = 11
        Me.txtFecha.TabStop = False
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(36, 44)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 10
        Me.lblFecha.Text = "Fecha:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(556, 12)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(56, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 6
        Me.cbxTipoComprobante.Texto = ""
        Me.cbxTipoComprobante.Visible = False
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(612, 12)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(82, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 7
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        Me.txtComprobante.Visible = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(76, 12)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = True
        Me.cbxCiudad.Size = New System.Drawing.Size(63, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(139, 12)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(56, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 2
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(6, 97)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 22
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(483, 16)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 5
        Me.lblComprobante.Text = "Comprobante:"
        Me.lblComprobante.Visible = False
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(17, 16)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'gbxCheque
        '
        Me.gbxCheque.Controls.Add(Me.ocxCheque)
        Me.gbxCheque.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxCheque.Location = New System.Drawing.Point(11, 127)
        Me.gbxCheque.Name = "gbxCheque"
        Me.gbxCheque.Size = New System.Drawing.Size(698, 177)
        Me.gbxCheque.TabIndex = 1
        Me.gbxCheque.TabStop = False
        Me.gbxCheque.Text = "CHEQUES"
        '
        'ocxCheque
        '
        Me.ocxCheque.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ocxCheque.dtDetalleCheque = Nothing
        Me.ocxCheque.dtDetalleLote = Nothing
        Me.ocxCheque.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ocxCheque.IDTransaccion = 0
        Me.ocxCheque.Location = New System.Drawing.Point(3, 16)
        Me.ocxCheque.Name = "ocxCheque"
        Me.ocxCheque.Size = New System.Drawing.Size(692, 158)
        Me.ocxCheque.SoloLectura = False
        Me.ocxCheque.TabIndex = 0
        Me.ocxCheque.Total = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'gbxEfectivo
        '
        Me.gbxEfectivo.Controls.Add(Me.ocxEfectivo)
        Me.gbxEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxEfectivo.Location = New System.Drawing.Point(12, 306)
        Me.gbxEfectivo.Name = "gbxEfectivo"
        Me.gbxEfectivo.Size = New System.Drawing.Size(336, 169)
        Me.gbxEfectivo.TabIndex = 2
        Me.gbxEfectivo.TabStop = False
        Me.gbxEfectivo.Text = "EFECTIVO"
        '
        'ocxEfectivo
        '
        Me.ocxEfectivo.Cursor = System.Windows.Forms.Cursors.Default
        Me.ocxEfectivo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ocxEfectivo.dtDesglose = Nothing
        Me.ocxEfectivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ocxEfectivo.IDTransaccion = 0
        Me.ocxEfectivo.Location = New System.Drawing.Point(3, 16)
        Me.ocxEfectivo.Name = "ocxEfectivo"
        Me.ocxEfectivo.Size = New System.Drawing.Size(330, 150)
        Me.ocxEfectivo.SoloLectura = False
        Me.ocxEfectivo.TabIndex = 0
        Me.ocxEfectivo.Total = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tcDocumentos
        '
        Me.tcDocumentos.Controls.Add(Me.tpDocumentos)
        Me.tcDocumentos.Controls.Add(Me.tpVentasAnuladas)
        Me.tcDocumentos.Controls.Add(Me.TabPage1)
        Me.tcDocumentos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tcDocumentos.Location = New System.Drawing.Point(350, 306)
        Me.tcDocumentos.Name = "tcDocumentos"
        Me.tcDocumentos.SelectedIndex = 0
        Me.tcDocumentos.Size = New System.Drawing.Size(360, 169)
        Me.tcDocumentos.TabIndex = 3
        '
        'tpDocumentos
        '
        Me.tpDocumentos.Controls.Add(Me.ocxDocumento)
        Me.tpDocumentos.Location = New System.Drawing.Point(4, 22)
        Me.tpDocumentos.Name = "tpDocumentos"
        Me.tpDocumentos.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDocumentos.Size = New System.Drawing.Size(352, 143)
        Me.tpDocumentos.TabIndex = 0
        Me.tpDocumentos.Text = "DOCUMENTOS"
        Me.tpDocumentos.UseVisualStyleBackColor = True
        '
        'ocxDocumento
        '
        Me.ocxDocumento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ocxDocumento.dtDetalleDocumento = Nothing
        Me.ocxDocumento.dtDetalleLote = Nothing
        Me.ocxDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ocxDocumento.IDTransaccion = 0
        Me.ocxDocumento.Location = New System.Drawing.Point(3, 3)
        Me.ocxDocumento.Name = "ocxDocumento"
        Me.ocxDocumento.Size = New System.Drawing.Size(346, 137)
        Me.ocxDocumento.SoloLectura = False
        Me.ocxDocumento.TabIndex = 0
        Me.ocxDocumento.Total = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'tpVentasAnuladas
        '
        Me.tpVentasAnuladas.Controls.Add(Me.ocxAnulados)
        Me.tpVentasAnuladas.Location = New System.Drawing.Point(4, 22)
        Me.tpVentasAnuladas.Name = "tpVentasAnuladas"
        Me.tpVentasAnuladas.Padding = New System.Windows.Forms.Padding(3)
        Me.tpVentasAnuladas.Size = New System.Drawing.Size(352, 143)
        Me.tpVentasAnuladas.TabIndex = 1
        Me.tpVentasAnuladas.Text = "VENTAS ANULADAS"
        Me.tpVentasAnuladas.UseVisualStyleBackColor = True
        '
        'ocxAnulados
        '
        Me.ocxAnulados.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ocxAnulados.dtDetalleAnulados = Nothing
        Me.ocxAnulados.dtDetalleLote = Nothing
        Me.ocxAnulados.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ocxAnulados.IDTransaccion = 0
        Me.ocxAnulados.Location = New System.Drawing.Point(3, 3)
        Me.ocxAnulados.Name = "ocxAnulados"
        Me.ocxAnulados.Size = New System.Drawing.Size(346, 137)
        Me.ocxAnulados.SoloLectura = False
        Me.ocxAnulados.TabIndex = 0
        Me.ocxAnulados.Total = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.OcxRendicionLoteCobranzaExtra1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(352, 143)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "COBRANZA EXTRA"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'OcxRendicionLoteCobranzaExtra1
        '
        Me.OcxRendicionLoteCobranzaExtra1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxRendicionLoteCobranzaExtra1.dtDetalleCobranzaExtra = Nothing
        Me.OcxRendicionLoteCobranzaExtra1.dtDetalleLote = Nothing
        Me.OcxRendicionLoteCobranzaExtra1.IDTransaccion = 0
        Me.OcxRendicionLoteCobranzaExtra1.Location = New System.Drawing.Point(3, 3)
        Me.OcxRendicionLoteCobranzaExtra1.Name = "OcxRendicionLoteCobranzaExtra1"
        Me.OcxRendicionLoteCobranzaExtra1.Size = New System.Drawing.Size(346, 137)
        Me.OcxRendicionLoteCobranzaExtra1.SoloLectura = False
        Me.OcxRendicionLoteCobranzaExtra1.TabIndex = 0
        Me.OcxRendicionLoteCobranzaExtra1.Total = New Decimal(New Integer() {0, 0, 0, 0})
        '
        'btnBusquedaAvanzada
        '
        Me.btnBusquedaAvanzada.Location = New System.Drawing.Point(176, 528)
        Me.btnBusquedaAvanzada.Name = "btnBusquedaAvanzada"
        Me.btnBusquedaAvanzada.Size = New System.Drawing.Size(82, 23)
        Me.btnBusquedaAvanzada.TabIndex = 10
        Me.btnBusquedaAvanzada.Text = "&Busqueda "
        Me.btnBusquedaAvanzada.UseVisualStyleBackColor = True
        Me.btnBusquedaAvanzada.Visible = False
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 557)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(722, 22)
        Me.StatusStrip1.TabIndex = 15
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(95, 528)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 9
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(635, 528)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 14
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(554, 528)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 13
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Location = New System.Drawing.Point(14, 528)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAnular.TabIndex = 8
        Me.btnAnular.Text = "Anu&lar"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(473, 528)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 12
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'flpAnuladoPor
        '
        Me.flpAnuladoPor.Controls.Add(Me.lblAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblUsuarioAnulado)
        Me.flpAnuladoPor.Controls.Add(Me.lblFechaAnulado)
        Me.flpAnuladoPor.Location = New System.Drawing.Point(350, 502)
        Me.flpAnuladoPor.Name = "flpAnuladoPor"
        Me.flpAnuladoPor.Size = New System.Drawing.Size(356, 20)
        Me.flpAnuladoPor.TabIndex = 5
        Me.flpAnuladoPor.Visible = False
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblAnulado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAnulado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnulado.ForeColor = System.Drawing.Color.Maroon
        Me.lblAnulado.Location = New System.Drawing.Point(3, 0)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(61, 15)
        Me.lblAnulado.TabIndex = 0
        Me.lblAnulado.Text = "ANULADO"
        '
        'lblUsuarioAnulado
        '
        Me.lblUsuarioAnulado.AutoSize = True
        Me.lblUsuarioAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioAnulado.Location = New System.Drawing.Point(70, 0)
        Me.lblUsuarioAnulado.Name = "lblUsuarioAnulado"
        Me.lblUsuarioAnulado.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioAnulado.TabIndex = 1
        Me.lblUsuarioAnulado.Text = "Usuario:"
        '
        'lblFechaAnulado
        '
        Me.lblFechaAnulado.AutoSize = True
        Me.lblFechaAnulado.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaAnulado.Location = New System.Drawing.Point(122, 0)
        Me.lblFechaAnulado.Name = "lblFechaAnulado"
        Me.lblFechaAnulado.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaAnulado.TabIndex = 2
        Me.lblFechaAnulado.Text = "Fecha"
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(392, 528)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 11
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'flpRegistradoPor
        '
        Me.flpRegistradoPor.Controls.Add(Me.lblRegistradoPor)
        Me.flpRegistradoPor.Controls.Add(Me.lblUsuarioRegistro)
        Me.flpRegistradoPor.Controls.Add(Me.lblFechaRegistro)
        Me.flpRegistradoPor.Location = New System.Drawing.Point(15, 502)
        Me.flpRegistradoPor.Name = "flpRegistradoPor"
        Me.flpRegistradoPor.Size = New System.Drawing.Size(329, 20)
        Me.flpRegistradoPor.TabIndex = 4
        Me.flpRegistradoPor.Visible = False
        '
        'lblRegistradoPor
        '
        Me.lblRegistradoPor.AutoSize = True
        Me.lblRegistradoPor.Location = New System.Drawing.Point(3, 0)
        Me.lblRegistradoPor.Name = "lblRegistradoPor"
        Me.lblRegistradoPor.Size = New System.Drawing.Size(79, 13)
        Me.lblRegistradoPor.TabIndex = 0
        Me.lblRegistradoPor.Text = "Registrado por:"
        '
        'lblUsuarioRegistro
        '
        Me.lblUsuarioRegistro.AutoSize = True
        Me.lblUsuarioRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblUsuarioRegistro.Location = New System.Drawing.Point(88, 0)
        Me.lblUsuarioRegistro.Name = "lblUsuarioRegistro"
        Me.lblUsuarioRegistro.Size = New System.Drawing.Size(46, 13)
        Me.lblUsuarioRegistro.TabIndex = 1
        Me.lblUsuarioRegistro.Text = "Usuario:"
        '
        'lblFechaRegistro
        '
        Me.lblFechaRegistro.AutoSize = True
        Me.lblFechaRegistro.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblFechaRegistro.Location = New System.Drawing.Point(140, 0)
        Me.lblFechaRegistro.Name = "lblFechaRegistro"
        Me.lblFechaRegistro.Size = New System.Drawing.Size(37, 13)
        Me.lblFechaRegistro.TabIndex = 2
        Me.lblFechaRegistro.Text = "Fecha"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(565, 482)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(40, 13)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Text = "Total:"
        '
        'lblTotalLote
        '
        Me.lblTotalLote.AutoSize = True
        Me.lblTotalLote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalLote.Location = New System.Drawing.Point(375, 482)
        Me.lblTotalLote.Name = "lblTotalLote"
        Me.lblTotalLote.Size = New System.Drawing.Size(91, 13)
        Me.lblTotalLote.TabIndex = 4
        Me.lblTotalLote.Text = "Total Contado:"
        '
        'txtTotalContadoLote
        '
        Me.txtTotalContadoLote.Color = System.Drawing.Color.Empty
        Me.txtTotalContadoLote.Decimales = True
        Me.txtTotalContadoLote.Indicaciones = Nothing
        Me.txtTotalContadoLote.Location = New System.Drawing.Point(469, 477)
        Me.txtTotalContadoLote.Name = "txtTotalContadoLote"
        Me.txtTotalContadoLote.Size = New System.Drawing.Size(90, 22)
        Me.txtTotalContadoLote.SoloLectura = True
        Me.txtTotalContadoLote.TabIndex = 5
        Me.txtTotalContadoLote.TabStop = False
        Me.txtTotalContadoLote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalContadoLote.Texto = ""
        '
        'txtTotal
        '
        Me.txtTotal.Color = System.Drawing.Color.Empty
        Me.txtTotal.Decimales = True
        Me.txtTotal.Indicaciones = Nothing
        Me.txtTotal.Location = New System.Drawing.Point(613, 477)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(90, 22)
        Me.txtTotal.SoloLectura = True
        Me.txtTotal.TabIndex = 7
        Me.txtTotal.TabStop = False
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Texto = ""
        '
        'frmRendicionLote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(722, 579)
        Me.Controls.Add(Me.lblTotalLote)
        Me.Controls.Add(Me.txtTotalContadoLote)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.btnBusquedaAvanzada)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.flpAnuladoPor)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.flpRegistradoPor)
        Me.Controls.Add(Me.tcDocumentos)
        Me.Controls.Add(Me.gbxEfectivo)
        Me.Controls.Add(Me.gbxCheque)
        Me.Controls.Add(Me.gbxCabecera)
        Me.Name = "frmRendicionLote"
        Me.Tag = "RENDICION DE LOTE"
        Me.Text = "frmRendicionLote"
        Me.gbxCabecera.ResumeLayout(False)
        Me.gbxCabecera.PerformLayout()
        Me.gbxCheque.ResumeLayout(False)
        Me.gbxEfectivo.ResumeLayout(False)
        Me.tcDocumentos.ResumeLayout(False)
        Me.tpDocumentos.ResumeLayout(False)
        Me.tpVentasAnuladas.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.flpAnuladoPor.ResumeLayout(False)
        Me.flpAnuladoPor.PerformLayout()
        Me.flpRegistradoPor.ResumeLayout(False)
        Me.flpRegistradoPor.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents txtZona As ERP.ocxTXTString
    Friend WithEvents txtChofer As ERP.ocxTXTString
    Friend WithEvents txtVehiculo As ERP.ocxTXTString
    Friend WithEvents txtDistribuidor As ERP.ocxTXTString
    Friend WithEvents lblLote As System.Windows.Forms.Label
    Friend WithEvents lblZona As System.Windows.Forms.Label
    Friend WithEvents lblChofer As System.Windows.Forms.Label
    Friend WithEvents lblVehiculo As System.Windows.Forms.Label
    Friend WithEvents lblDistribuidor As System.Windows.Forms.Label
    Friend WithEvents txtFechaReparto As ERP.ocxTXTDate
    Friend WithEvents lblFechaReparto As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents ocxCheque As ERP.ocxCargaCheque
    Friend WithEvents gbxCheque As System.Windows.Forms.GroupBox
    Friend WithEvents gbxEfectivo As System.Windows.Forms.GroupBox
    Friend WithEvents tcDocumentos As System.Windows.Forms.TabControl
    Friend WithEvents tpDocumentos As System.Windows.Forms.TabPage
    Friend WithEvents tpVentasAnuladas As System.Windows.Forms.TabPage
    Friend WithEvents ocxDocumento As ERP.ocxRendicionLoteDocumento
    Friend WithEvents btnBusquedaAvanzada As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents flpAnuladoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioAnulado As System.Windows.Forms.Label
    Friend WithEvents lblFechaAnulado As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents flpRegistradoPor As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lblRegistradoPor As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioRegistro As System.Windows.Forms.Label
    Friend WithEvents lblFechaRegistro As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtTotal As ERP.ocxTXTNumeric
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblTotalLote As System.Windows.Forms.Label
    Friend WithEvents txtTotalContadoLote As ERP.ocxTXTNumeric
    Friend WithEvents ocxAnulados As ERP.ocxRendicionLoteVentasAnuladas
    Friend WithEvents txtNroLote As ERP.ocxTXTNumeric
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents OcxRendicionLoteCobranzaExtra1 As ERP.ocxRendicionLoteCobranzaExtra
    Friend WithEvents ocxEfectivo As ocxDesgloseBilletes
End Class

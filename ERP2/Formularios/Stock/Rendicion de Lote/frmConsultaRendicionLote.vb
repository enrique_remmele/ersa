﻿Public Class frmConsultaRendicionLote

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'EVENTOS
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Dim IDOperacion As Integer
    Dim Consulta As String
    Dim Where As String

    'FUNCIONES
    'Inicializar
    Sub Inicializar()

        'TextBox
        txtCantidadRendicionLote.txt.ResetText()
        txtCantidadCheques.txt.ResetText()
        txtOperacion.txt.ResetText()
        txtTotalRendicionLote.txt.ResetText()

        'CheckBox
        chkLote.Checked = False
        chkFecha.Checked = False

        'ComboBox
        cbxTipoComprobante.Enabled = False
        cbxLote.Enabled = False

        'ListView
        lvCheque.Items.Clear()
        lvOperacion.Items.Clear()

        'DateTimePicker
        dtpDesde.Value = Date.Now
        dtpHasta.Value = Date.Now

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "RENDICION DE LOTE", "RLT")
        CargarInformacion()

        'Foco

    End Sub

    'Cargar informacion
    Sub CargarInformacion()

        'Cliente
        CSistema.SqlToComboBox(cbxLote, "Select IDTransaccion,Lote From VRendicionLote Where Anulado = 'False' Order By 2")

        'TipoComprobante
        CSistema.SqlToComboBox(cbxTipoComprobante, "Select ID,Descripcion From VTipoComprobante Order By 2")


        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal, "Select ID, Descripcion From Sucursal Order By 2")

        'CARGAR LA ULTIMA CONFIGURACION
        'Lote
        chkLote.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "LOTE ACTIVO", "False")
        cbxLote.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "LOTE", "")

        'Tipo Comprobante
        cbxTipoComprobante.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", "")

        'Sucursal
        chkSucursal.Checked = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", "False")
        cbxSucursal.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name.ToString, "SUCURSAL", "")

    End Sub

    'Guardar Informacion
    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL ACTIVO", chkSucursal.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "SUCURSAL", cbxSucursal.Text)

        'Cliente
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "LOTE ACTIVO", chkLote.Checked.ToString)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "LOTE", cbxLote.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name.ToString, "TIPO COMPROBANTE", cbxTipoComprobante.Text)


    End Sub

    'Establecer Condicion
    Function EstablecerCondicion(ByVal cbx As ComboBox, ByVal chk As CheckBox, ByVal campo As String, ByVal MensajeError As String) As Boolean

        EstablecerCondicion = True

        If chk.Checked = True Then

            If IsNumeric(cbx.SelectedValue) = False Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If cbx.SelectedValue = 0 Then
                ctrError.SetError(cbx, MensajeError)
                ctrError.SetIconAlignment(cbx, ErrorIconAlignment.TopLeft)
                Return False
            End If

            If Where = "" Then
                Where = " Where (" & campo & "=" & cbx.SelectedValue & ") "
            Else
                Where = Where & " And (" & campo & "=" & cbx.SelectedValue & ") "
            End If

        End If


    End Function

    'Listar Cobranza
    Sub ListarRendicionLote(Optional ByVal Numero As Integer = 0, Optional ByVal Condicion As String = "")

        ctrError.Clear()

        Consulta = "Select Numero,Comprobante,Lote,Distribuidor,Fec,Sucursal,Observacion From VRendicionLote"

        Where = Condicion

        'Lote
        If EstablecerCondicion(cbxLote, chkLote, "IDTransaccion", "Seleccione correctamente el !") = False Then
            Exit Sub
        End If


        'Sucursal
        If EstablecerCondicion(cbxSucursal, chkSucursal, "IDSucursal", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If

        'Tipo Comprobante
        If EstablecerCondicion(cbxTipoComprobante, chkTipoComprobante, "ID", "Seleccione correctamente la sucursal!") = False Then
            Exit Sub
        End If

        'Fecha
        If chkFecha.Checked = True Then
            If Where = "" Then
                Where = " Where (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' ) "
            Else
                Where = Where & " And (Fecha Between '" & CSistema.FormatoFechaBaseDatos(dtpDesde, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(dtpHasta, True, False) & "' )  "
            End If
        End If

        lvOperacion.Items.Clear()
        lvCheque.Items.Clear()
        lvEfectivo.Items.Clear()

        'Solo por numero
        If Numero > 0 Then
            Where = " Where Numero = " & Numero & ""
        End If

        CSistema.SqlToLv(lvOperacion, Consulta & " " & Where & " Order By Fecha")

        'Formato

        'Totales
        'CSistema.FormatoMoneda(lvOperacion, 3)
        CSistema.TotalesLv(lvOperacion, txtTotalRendicionLote.txt, 3)
        txtCantidadRendicionLote.txt.Text = lvOperacion.Items.Count

    End Sub

    'Listar Comprobantes
    Sub ListarCheques(ByVal vIDTransaccion As Integer)

        'Limpiar ListView
        lvCheque.Items.Clear()

        Dim sql As String = "Select Banco,NroCheque,TipoCheque,[Fec.],Importe  From VDetalleRendicionLoteCheque Where IDTransaccion=" & vIDTransaccion

        CSistema.SqlToLv(lvCheque, sql)

        CSistema.FormatoMoneda(lvCheque, 4)
        CSistema.TotalesLv(lvCheque, txtTotalCheque.txt, 4)
        txtCantidadCheques.txt.Text = lvCheque.Items.Count

    End Sub

    'Listar Formas de Pago
    Sub ListarEfectivo(ByVal vIDTransaccion As Integer)

        'Limpiar ListView
        lvEfectivo.Items.Clear()

        Dim sql As String = "Select * from VDesgloseBillete Where IDTransaccion=" & vIDTransaccion & " Order By IDTransaccion"
        CSistema.SqlToLv(lvEfectivo, sql)

        CSistema.TotalesLv(lvEfectivo, txtTotalEfectivo.txt, 3)
        txtCantidadEfectivo.txt.Text = lvEfectivo.Items.Count

    End Sub

    'Habilitar Controles
    Sub HabilitarControles(ByVal chk As CheckBox, ByVal ctr As Control)

        If chk.Checked = True Then
            ctr.Enabled = True
        Else
            ctr.Enabled = False
        End If

    End Sub

    'Seleccionar Registro
    Sub SeleccionarRegistro()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(0).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        'Obtener el IDTransaccion
        IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From OrdenPago Where Numero=" & lvOperacion.SelectedItems(0).SubItems(0).Text & "), 0 )")

        If IDTransaccion > 0 Then
            Me.Close()
        End If

    End Sub

    Private Sub frmConsultaChequeCliente_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmConsultaChequeCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCliente_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLote.CheckedChanged
        HabilitarControles(chkLote, cbxLote)
    End Sub


    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        HabilitarControles(chkFecha, dtpDesde)
        HabilitarControles(chkFecha, dtpHasta)
    End Sub

    Private Sub btn4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn4.Click
        ListarRendicionLote()
    End Sub

    Private Sub chkSucursal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.CheckedChanged
        HabilitarControles(chkSucursal, cbxSucursal)
    End Sub

    Private Sub lvOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvOperacion.SelectedIndexChanged

        ctrError.Clear()

        'Validar
        If lvOperacion.SelectedItems.Count = 0 Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        If IsNumeric(lvOperacion.SelectedItems(0).SubItems(0).Text) = False Then
            Dim Mensaje As String = "Seleccione correctamente un registro!"
            ctrError.SetError(lvOperacion, Mensaje)
            ctrError.SetIconAlignment(lvOperacion, ErrorIconAlignment.TopLeft)
            Exit Sub
        End If

        Dim vIDTransaccion As Integer
        Dim Numero As Integer = lvOperacion.SelectedItems(0).SubItems(0).Text

        vIDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From RendicionLote Where Numero=" & Numero & "), 0 )")

        ListarCheques(vIDTransaccion)
        ListarEfectivo(vIDTransaccion)

    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        SeleccionarRegistro()
    End Sub

    Private Sub txtOperacion_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtOperacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarRendicionLote(txtOperacion.ObtenerValor)
            txtOperacion.txt.Focus()
            txtOperacion.txt.SelectAll()
        End If
    End Sub

    Private Sub dtpDesde_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDesde.ValueChanged
        dtpHasta.Value = dtpDesde.Text
    End Sub


    Private Sub chkTipoComprobante_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTipoComprobante.CheckedChanged
        HabilitarControles(chkTipoComprobante, cbxTipoComprobante)
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmConsultaRendicionLote_Activate()
        Me.Refresh()
    End Sub
End Class
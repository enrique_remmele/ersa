﻿Imports ERP.Reporte

Public Class frmRendicionLote

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CReporte As New CReporteLotes

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES
    Dim dtLote As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    Sub Inicializar()

        Me.KeyPreview = True

        'Otros
        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Controles
        ocxCheque.Inicializar()
        ocxDocumento.Inicializar()
        OcxRendicionLoteCobranzaExtra1.Inicializar()
        ocxAnulados.Inicializar()

        'Propiedades
        IDTransaccion = 0
        vNuevo = True

        'Funciones
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "RENDICION DE LOTE", "RLT")
        CargarInformacion()

        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)

        'Cargar el ultimo Registro
        ManejarTecla(New KeyEventArgs(Keys.End))


    End Sub

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        'Cabecera
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, txtNroLote)
        CSistema.CargaControl(vControles, txtObservacion)

        'Controles
        CSistema.CargaControl(vControles, ocxCheque)
        CSistema.CargaControl(vControles, ocxEfectivo)
        
        'Ciudad
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select Distinct IDCiudad, CodigoCiudad  From VSucursal Where ID=" & vgIDSucursal & " Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion & "")

        'Efectivo
        ocxEfectivo.Inicializar()

        'CARGAR LA ULTIMA CONFIGURACION
        'Ciudad
        cbxCiudad.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "CIUDAD", "")

        'Sucursal
        cbxSucursal.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", vgSucursal)

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

    End Sub

    Sub GuardarInformacion()

        'Ciudad
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "CIUDAD", cbxCiudad.cbx.Text)

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.cbx.Text)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)

        'Cargamos lotes pendientes
        If vgUsuarioEsChofer = False Then
            'dtLote = CSistema.ExecuteToDataTable("Select * From VLoteDistribucion Where IDSucursal=" & vgIDSucursal & " and IDTransaccion in (Select IDTransaccion From LoteDistribucion where Anulado = 'False') and IDTransaccion not in (Select IDTRansaccionLote From RendicionLote Where Anulado='False') and IDTransaccion not in (Select IDTransaccionLote From CobranzaContado Where Anulado='False') Order By Comprobante").Copy
            dtLote = CSistema.ExecuteToDataTable("Select * From VLoteDistribucion Where IDSucursal=" & vgIDSucursal & " and RENDIDO = 'PENDIENTE'").Copy
        Else
            'dtLote = CSistema.ExecuteToDataTable("Select * From VLoteDistribucion Where IDSucursal=" & vgIDSucursal & " And IDChofer=" & vgUsuarioIDChofer & " and IDTransaccion in (Select IDTransaccion From LoteDistribucion where Anulado = 'False') and IDTransaccion not in (Select IDTRansaccionLote From RendicionLote Where Anulado='False') and IDTransaccion not in (Select IDTransaccionLote From CobranzaContado Where Anulado='False') Order By Comprobante").Copy
            dtLote = CSistema.ExecuteToDataTable("Select * From VLoteDistribucion Where IDSucursal=" & vgIDSucursal & " And IDChofer=" & vgUsuarioIDChofer & " and RENDIDO = 'PENDIENTE'").Copy
        End If

        'ObtenerInformacionLote()
        'Limpiar Controles

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        'Cabecera
        txtComprobante.txt.Clear()
        txtObservacion.txt.Clear()

        flpRegistradoPor.Visible = False
        flpAnuladoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From RendicionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "),1)"), Integer)

        'Obtener registro nuevo
        If vgUsuarioEsChofer = False Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From RendicionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ),1)"), Integer)
        Else
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) From VRendicionLoteChofer Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDChofer=" & vgUsuarioIDChofer & " ),1)"), Integer)
        End If

        txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "RendicionLote", "Comprobante", cbxSucursal.cbx.SelectedValue)

        'Controles
        ocxCheque.Limpiar()
        ocxDocumento.Limpiar()
        OcxRendicionLoteCobranzaExtra1.Limpiar()
        ocxAnulados.Limpiar()
        ocxEfectivo.Limpiar()
        ocxDocumento.SoloLectura = False

        txtTotalContadoLote.txt.Clear()
        txtFechaReparto.txt.Clear()
        txtDistribuidor.txt.Clear()
        txtVehiculo.txt.Clear()
        txtChofer.txt.Clear()
        txtZona.txt.Clear()
        txtObservacion.txt.Clear()

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        txtNroLote.txt.Clear()
        txtNroLote.txt.Focus()

        'Poner el foco en el la fecha
        'txtComprobante.txt.Focus()

    End Sub

    Sub Cancelar()

        CSistema.ControlBotonesRegistro(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, New Button, vControles)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()
        ManejarTecla(New KeyEventArgs(Keys.End))

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros

        Dim frm As New frmConsultaRendicionLote
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        'frm.ShowDialog(Me)
        FGMostrarFormulario(Me, frm, "Consulta de Rendicion", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False, True)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub ObtenerInformacionLote()

        'Limpiar los controles
        txtDistribuidor.txt.Clear()
        txtVehiculo.txt.Clear()
        txtChofer.txt.Clear()
        txtFecha.txt.Clear()
        txtFechaReparto.txt.Clear()
        txtZona.txt.Clear()

        'Validar
        If IsNumeric(txtNroLote.ObtenerValor) = False Then
            CSistema.MostrarError("Seleccione correctamente el lote!", ctrError, txtNroLote, tsslEstado, ErrorIconAlignment.BottomLeft)
            Exit Sub
        End If

        If dtLote.Select(" Numero = " & txtNroLote.ObtenerValor).Count = 0 Then
            Exit Sub
        End If

        'Cargar el detalle del lote
        Dim oRowLote As DataRow = dtLote.Select(" Numero = " & txtNroLote.ObtenerValor)(0)

        'Asignamos los valores a los controles
        txtDistribuidor.txt.Text = oRowLote("Distribuidor").ToString
        txtVehiculo.txt.Text = oRowLote("Camion").ToString
        txtChofer.txt.Text = oRowLote("Chofer").ToString
        txtZona.txt.Text = oRowLote("Zona").ToString
        txtFecha.txt.Text = oRowLote("Fecha").ToString
        txtFechaReparto.txt.Text = oRowLote("FechaReparto").ToString
        txtTotalContadoLote.txt.Text = oRowLote("TotalContado").ToString

        Dim dtTemp As DataTable
        dtTemp = CSistema.ExecuteToDataTable("Select * From VVentaLoteDistribucion Where IDTransaccionLote=" & oRowLote("IDTransaccion").ToString).Copy

        ocxCheque.IDTransaccion = oRowLote("IDTransaccion").ToString
        ocxCheque.dtDetalleLote = dtTemp.Copy
        ocxCheque.CargarComprobantesLote()

        ocxDocumento.IDTransaccion = oRowLote("IDTransaccion").ToString
        ocxDocumento.dtDetalleLote = dtTemp.Copy
        ocxDocumento.CargarComprobantesLote()

        ocxAnulados.IDTransaccion = oRowLote("IDTransaccion").ToString
        ocxAnulados.dtDetalleLote = dtTemp.Copy
        ocxAnulados.CargarComprobantesLote()

        OcxRendicionLoteCobranzaExtra1.IDTransaccion = oRowLote("IDTransaccion").ToString
        OcxRendicionLoteCobranzaExtra1.dtDetalleLote = dtTemp.Copy

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Tipo Comprobante
        If cbxTipoComprobante.Validar("Seleccione correctamente el tipo de comprobante!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Sucursal
        If cbxSucursal.Validar("Seleccione correctamente la sucursal!", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Function
        End If

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            'Lote
            If txtNroLote.txt.Text.Trim.Length = 0 Then
                Dim mensaje As String = "Seleccione correctamente el lote!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

            If txtNroLote.ObtenerValor = 0 Then
                Dim mensaje As String = "Seleccione correctamente el lote!"
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Function
            End If

        End If

        'Totales

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then

            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        txtComprobante.txt.Text = txtNroLote.ObtenerValor
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IndiceOperacion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion.ToString, ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@IDTransaccionLote", dtLote.Select("Numero=" & txtNroLote.ObtenerValor)(0)("IDTransaccion").ToString, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@IDSucursalOperacion", cbxSucursal.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", CSistema.FormatoMonedaBaseDatos(txtID.txt.Text), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)

        'Totales
        CSistema.SetSQLParameter(param, "@TotalEfectivo", ocxEfectivo.Total, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalDocumento", ocxDocumento.Total, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalAnulados", ocxAnulados.Total, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalCobranzaExtra", OcxRendicionLoteCobranzaExtra1.Total, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalCheque", ocxCheque.Total, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalChequeAlDiaBancoLocal", ocxCheque.txtTotalBancoLocal.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalChequeAlDiaOtrosBancos", ocxCheque.txtTotalOtrosBancos.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TotalChequeDiferido", ocxCheque.txtTotalDiferidos.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", txtTotal.ObtenerValor, ParameterDirection.Input)

        'Chofer
        CSistema.SetSQLParameter(param, "@UsuarioEsChofer", vgUsuarioEsChofer, ParameterDirection.Input)
        If vgUsuarioEsChofer Then
            CSistema.SetSQLParameter(param, "@IDChofer", vgUsuarioIDChofer, ParameterDirection.Input)
        End If

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpRendicionLote", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then
                'Eliminar el Registro si es que se registro
                If CBool(CSistema.ExecuteScalar("Select 'Existe'=(Case When (Select IDTransaccion From RendicionLote Where IDTransaccion=" & IDTransaccion & ") Is Null Then 'False' Else 'True' End)")) = True Then
                    param(IndiceOperacion).Value = CSistema.NUMOperacionesRegistro.DEL.ToString
                    CSistema.ExecuteStoreProcedure(param, "SpRendicionLote", False, False, MensajeRetorno, IDTransaccion)
                End If
            End If

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            If Operacion = ERP.CSistema.NUMOperacionesRegistro.INS Then

                'Cargar los Efectivos
                ocxEfectivo.Guardar(IDTransaccion)

                'Cargar Cheques
                ocxCheque.Guardar(IDTransaccion)

                'Cargar Documentos
                ocxDocumento.Guardar(IDTransaccion)

                'Cargar Documentos
                OcxRendicionLoteCobranzaExtra1.Guardar(IDTransaccion)

                'Cargar Ventas Anuladas
                ocxAnulados.Guardar(IDTransaccion)

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)

        txtID.SoloLectura = False

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            'IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From RendicionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
            If vgUsuarioEsChofer = False Then
                IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select TOP 1 IDTransaccion From RendicionLote Where Numero = " & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " ), 0 )", "", 10)
            Else
                IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select TOP 1 IDTransaccion From VRendicionLoteChofer Where Numero = " & txtID.ObtenerValor & " And IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDChofer=" & vgUsuarioIDChofer & "), 0 )", "", 10)
            End If
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        Dim dt As DataTable = Nothing

        'Inicializar Controles
        If vgUsuarioEsChofer = False Then
            dt = CSistema.ExecuteToDataTable("Select * From VRendicionLote Where IDTransaccion=" & IDTransaccion)
        Else
            dt = CSistema.ExecuteToDataTable("Select * From VRendicionLoteChofer Where IDTransaccion=" & IDTransaccion)
        End If

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        cbxCiudad.txt.Text = oRow("Ciudad").ToString
        txtID.txt.Text = oRow("Numero").ToString
        cbxSucursal.txt.Text = oRow("Sucursal").ToString
        cbxTipoComprobante.cbx.Text = oRow("TipoComprobante").ToString
        txtComprobante.txt.Text = oRow("Comprobante").ToString
        txtNroLote.txt.Text = oRow("Lote").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        txtFechaReparto.SetValueFromString(CDate(oRow("FechaReparto").ToString))


        txtDistribuidor.txt.Text = oRow("Distribuidor").ToString
        txtVehiculo.txt.Text = oRow("Vehiculo").ToString
        txtChofer.txt.Text = oRow("Chofer").ToString
        txtZona.txt.Text = oRow("Zona").ToString

        txtObservacion.txt.Text = oRow("Observacion").ToString

        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("usuario").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargar Controles
        ocxCheque.CargarDatos(IDTransaccion)
        ocxEfectivo.CargarDatos(IDTransaccion)
        ocxDocumento.CargarDatos(IDTransaccion)
        OcxRendicionLoteCobranzaExtra1.CargarDatos(IDTransaccion)
        ocxAnulados.CargarDatos(IDTransaccion)
        ocxDocumento.SoloLectura = True

        CalcularTotales()

    End Sub

    Sub CalcularTotales()

        Dim Total As Decimal = 0

        Total = Total + ocxCheque.Total
        Total = Total + ocxEfectivo.Total
        Total = Total + ocxDocumento.Total
        Total = Total + ocxAnulados.Total
        Total = Total + OcxRendicionLoteCobranzaExtra1.Total

        txtTotal.SetValue(Total)

    End Sub

    Sub Imprimir()

        Dim Where As String
        Dim Titulo As String = "RENDICION DE COBRANZAS"

        'Filtrar IDTransaccion
        Where = " Where IDTransaccion =" & IDTransaccion

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        CReporte.ListadoRendicionCobranza(frm, Where, Titulo, vgUsuarioIdentificador)

    End Sub

    Sub ManejarTecla(ByRef e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            'ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VRendicionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            If vgUsuarioEsChofer = False Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VRendicionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VRendicionLoteChofer Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDChofer=" & vgUsuarioIDChofer), Integer)
            End If

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            'ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VRendicionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)

            If vgUsuarioEsChofer = False Then
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VRendicionLote Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " "), Integer)
            Else
                ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VRendicionLoteChofer Where IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDChofer=" & vgUsuarioIDChofer), Integer)
            End If

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If
        If e.KeyCode = vgKeyConsultar Then
            Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If

    End Sub

    Private Sub frmRendicionLote_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub frmRendicionLote_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmRendicionLote_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp


        If Me.ActiveControl.Name = "ocxEfectivo" Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.ANULAR)
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged

        cbxSucursal.cbx.DataSource = Nothing

        If IsNumeric(cbxCiudad.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxCiudad.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo  From VSucursal Where IDCiudad=" & cbxCiudad.cbx.SelectedValue)

    End Sub

    Private Sub cbxLote_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ObtenerInformacionLote()
    End Sub

    Private Sub ocxCheque_ErrorDetectado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal mensaje As String) Handles ocxCheque.ErrorDetectado
        CSistema.MostrarError(mensaje, ctrError, ocxCheque, tsslEstado, ErrorIconAlignment.TopLeft)
    End Sub

    Private Sub ocxCheque_UpdateTotal(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxCheque.UpdateTotal
        CalcularTotales()
    End Sub

    Private Sub ocxEfectivo_ErrorDetectado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal mensaje As String) Handles ocxEfectivo.ErrorDetectado
        CSistema.MostrarError(mensaje, ctrError, ocxEfectivo, tsslEstado, ErrorIconAlignment.TopLeft)
    End Sub

    Private Sub ocxEfectivo_UpdateTotal(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxEfectivo.UpdateTotal
        CalcularTotales()
    End Sub

    Private Sub ocxDocumento_ErrorDetectado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal mensaje As String) Handles ocxDocumento.ErrorDetectado
        CSistema.MostrarError(mensaje, ctrError, ocxDocumento, tsslEstado, ErrorIconAlignment.TopLeft)
    End Sub

    Private Sub ocxDocumento_UpdateTotal(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxDocumento.UpdateTotal
        CalcularTotales()
    End Sub

    Private Sub ocxCobranzaExtra_UpdateTotal(ByVal sender As Object, ByVal e As System.EventArgs) Handles OcxRendicionLoteCobranzaExtra1.UpdateTotal
        CalcularTotales()
    End Sub

    Private Sub ocxAnulados_ErrorDetectado(ByVal sender As Object, ByVal e As System.EventArgs, ByVal mensaje As String) Handles ocxAnulados.ErrorDetectado
        CSistema.MostrarError(mensaje, ctrError, ocxDocumento, tsslEstado, ErrorIconAlignment.TopLeft)
    End Sub

    Private Sub ocxAnulados_UpdateTotal(ByVal sender As Object, ByVal e As System.EventArgs) Handles ocxAnulados.UpdateTotal
        CalcularTotales()
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.PropertyChanged
        If vNuevo = True Then
            txtComprobante.txt.Text = CSistema.ObtenerProximoNroComprobante(cbxTipoComprobante.cbx.SelectedValue, "RendicionLote", "Comprobante", cbxSucursal.cbx.SelectedValue)
        End If
    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada

        ManejarTecla(e)

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub txtNroLote_Leave(sender As Object, e As System.EventArgs) Handles txtNroLote.Leave
        If vNuevo = True Then
            ObtenerInformacionLote()
        End If

    End Sub
    '09-06-2021 - SC - Actualiza datos
    Sub frmRendicionLote_Activate()
        Me.Refresh()
    End Sub


End Class
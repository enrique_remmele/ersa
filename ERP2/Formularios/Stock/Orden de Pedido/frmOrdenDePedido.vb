﻿Public Class frmOrdenDePedido

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDetalleImpuesto As New CDetalleImpuesto
    Dim CData As New CData
    'Dim CReporte As New CReporte

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtDetalle As New DataTable
    Dim dtOperaciones As New DataTable
    Dim dtDepositos As New DataTable
    Dim dtTotalKilos As New DataTable
    Dim vControles() As Control
    Dim vNuevo As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProducto.Conectar()

        'Otros
        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Propiedades
        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "ORDEN DE PEDIDO", "ODP")
        IDTransaccion = 0
        vNuevo = True
        chkPedidoCliente.Valor = False

        'Funciones
        CargarInformacion()


        'Botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.INICIO)
        'txtFechaEntrega.Hoy()
        txtFecha.Hoy()
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, cbxTipoComprobante)
        CSistema.CargaControl(vControles, txtComprobante)
        CSistema.CargaControl(vControles, cbxDepositoSalida)
        CSistema.CargaControl(vControles, txtObservacion)
        CSistema.CargaControl(vControles, txtProducto)
        CSistema.CargaControl(vControles, txtExtra)
        CSistema.CargaControl(vControles, txtDesde)
        CSistema.CargaControl(vControles, txtHasta)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, txtFechaEntrega)
        CSistema.CargaControl(vControles, chkPedidoCliente)

        dtTotalKilos.Columns.Add("FechaEntrega", Type.GetType("System.DateTime"))
        dtTotalKilos.Columns.Add("Kilos", Type.GetType("System.Decimal"))

        'CARGAR ESTRUCTURA DEL DETALLE
        dtDetalle = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleOrdenDePedido").Clone

        'INICIALIZAR EL DETALLE IMPUESTO
        CDetalleImpuesto.Inicializar()

        'CARGAR DATOS DE OPERACION
        dtOperaciones = CSistema.ExecuteToDataTable("Select ID, Descripcion, Activo, Entrada, Salida From TipoOperacion Where IDOperacion=" & IDOperacion)

        'CARGAR CONTROLES
        'Sucursales
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID, Codigo From VSucursal Order By 2")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Depositos
        'Depositos
        CSistema.SqlToComboBox(cbxDepositoSalida.cbx, "Select ID, Deposito From VDeposito where IDTipoDeposito = 1 and IDSucursal = " & cbxSucursal.GetValue)

        'CARGAR LA ULTIMA CONFIGURACION
        'Tipo Operacion
        cbxSucursal.SelectedValue(CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "SUCURSAL", "0"))

        'Tipo de Comprobante
        cbxTipoComprobante.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", "")

        'Deposito de Origen
        cbxDepositoSalida.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DEPOSITO", "")

        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) From OrdenDePedido),1)"), Integer)



    End Sub

    Sub GuardarInformacion()

        'Sucursal
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "SUCURSAL", cbxSucursal.GetValue)

        'Tipo de Comprobante
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "TIPO COMPROBANTE", cbxTipoComprobante.cbx.Text)

        'Deposito de Origen
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DEPOSITO", cbxDepositoSalida.cbx.Text)


    End Sub

    Sub Nuevo()

        'Configurar botones
        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.NUEVO)
        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where Estado = 1 and IDOperacion=" & IDOperacion)
        'Limpiar detalle
        dtDetalle.Rows.Clear()
        ListarDetalle()
        chkPedidoCliente.Valor = False

        'Otros
        ctrError.Clear()
        tsslEstado.Text = ""
        IDTransaccion = 0
        vNuevo = True

        cbxTipoComprobante.cbx.Text = ""
        txtComprobante.txt.Clear()
        cbxDepositoSalida.cbx.Text = ""
        cbxDepositoSalida.Texto = ""

        txtObservacion.txt.Clear()

        flpAnuladoPor.Visible = False
        flpRegistradoPor.Visible = False

        'Obtener registro nuevo
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero + 1) FROM VOrdenDePedido Where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        'Bloquear Nro de Operacion
        txtID.txt.ReadOnly = True

        'Poner el foco en la fecha
        txtFecha.txt.Focus()
        txtFecha.Hoy()
        txtDesde.Hoy()
        txtHasta.UnaSemana()
        'txtFechaEntrega.Hoy()
    End Sub

    Sub Cancelar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.CANCELAR)
        txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull((Select MAX(Numero) FROM VOrdenDePedido where IDSucursal = " & cbxSucursal.GetValue & "),1)"), Integer)

        txtID.txt.ReadOnly = False
        txtID.txt.Focus()

        CargarOperacion()

    End Sub

    Function ValidarDocumento(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        ValidarDocumento = False

        'Detalle
        If dgw.Rows.Count = 0 Then
            Dim mensaje As String = "El registro no tiene ningun detalle!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Comprobante
        If txtComprobante.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un numero de comprobante!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Function
        End If

        'Avisar que fecha de facturacion es menor a hoy
        If CDate(txtFecha.txt.Text) < Today Then
            If MessageBox.Show("Fecha menor a hoy! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                If txtFecha.Enabled = True Then
                    txtFecha.Focus()
                    Exit Function
                Else
                    Cancelar()
                    Exit Function
                End If
            End If
        End If


        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.ANULAR Then
            If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Function
            End If
        End If

        Return True

    End Function

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If ValidarDocumento(Operacion) = False Then
            Exit Sub
        End If

        If vNuevo = True Then
            'Avisar que fecha de facturacion es menor a hoy
            If CType(CSistema.ExecuteScalar("SELECT count(*) from vOrdenDePedido where DatePart(week, Fecha) = DatePart(week, '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "') and year(fecha) = year('" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "') and Anulado = 'False' and IDSucursal = " & cbxSucursal.GetValue), Integer) > 0 Then
                If MessageBox.Show("Ya hay pedido para la semana! Desea guardar como Extraordinario?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
                    Cancelar()
                    Exit Sub
                End If
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter
        Dim IDTransaccion As Integer

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        If Operacion <> ERP.CSistema.NUMOperacionesRegistro.INS Then
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Numero", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", cbxTipoComprobante.cbx.SelectedValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", txtComprobante.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", cbxDepositoSalida.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtObservacion.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Autorizacion", "", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaDesde", CSistema.FormatoFechaBaseDatos(txtDesde.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaHasta", CSistema.FormatoFechaBaseDatos(txtHasta.GetValue.ToShortDateString, True, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@PedidoCliente", chkPedidoCliente.Valor, ParameterDirection.Input)


        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input) 'JGR 20140816
        'CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpOrdenDePedido", False, False, MensajeRetorno, IDTransaccion) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

            'Eliminar el Registro

            Exit Sub

        End If

        If IDTransaccion > 0 Then

            'Insertamos el Detalle
            If InsertarDetalle(IDTransaccion, ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

                'Eliminar Registro

                Exit Sub

            End If

        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.GUARDAR)
        CargarOperacion(IDTransaccion)
        txtID.SoloLectura = False

    End Sub

    Function InsertarDetalle(ByVal IDTransaccion As Integer, ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtDetalle.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDProducto", oRow("IDProducto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDDeposito", cbxDepositoSalida.GetValue, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Existencia", CSistema.FormatoMonedaBaseDatos(oRow("Existencia").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Pedido", CSistema.FormatoMonedaBaseDatos(oRow("Pedido").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Extra", CSistema.FormatoMonedaBaseDatos(oRow("Extra").ToString, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaEntrega", CSistema.FormatoFechaBaseDatos(oRow("FechaEntrega").ToString, True, True), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleOrdenDePedido", False, False, MensajeRetorno) = False Then
                tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
                ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)

                'Eliminar el Registro

                Return False

            End If

        Next

    End Function

    Sub EstablecerBotones(ByVal Operacion As ERP.CSistema.NUMHabilitacionBotonesRegistros)

        CSistema.ControlBotonesRegistro(Operacion, btnNuevo, btnGuardar, btnCancelar, btnAnular, btnImprimir, btnBusquedaAvanzada, btnAsiento, vControles)

    End Sub

    Sub CargarProducto()

        'Validar
        'Seleccion de Producto
        If txtProducto.Seleccionado = False Then
            Dim mensaje As String = "Seleccione correctamente el producto!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If txtFechaEntrega.txt.Text = "  /  /" Then
            Dim mensaje As String = "Debe asignar fecha de entrega!"
            ctrError.SetError(txtFechaEntrega, mensaje)
            ctrError.SetIconAlignment(txtFechaEntrega, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CSistema.FormatoFechaBaseDatos(txtFechaEntrega.txt.Text, True, False) < CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) Then
            Dim mensaje As String = "La Fecha de Entrega no puede ser menor al pedido!"
            ctrError.SetError(txtProducto, mensaje)
            ctrError.SetIconAlignment(txtProducto, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de Deposito Salida
        If cbxDepositoSalida.cbx.Enabled = True Then
            If IsNumeric(cbxDepositoSalida.cbx.SelectedValue) = False Then
                Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                ctrError.SetError(txtExtra, mensaje)
                ctrError.SetIconAlignment(txtExtra, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                If cbxDepositoSalida.cbx.Text = "" Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                    ctrError.SetError(txtExtra, mensaje)
                    ctrError.SetIconAlignment(txtExtra, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

                If cbxDepositoSalida.cbx.SelectedValue = Nothing Then
                    Dim mensaje As String = "Seleccione correctamente el deposito de salida!"
                    ctrError.SetError(txtExtra, mensaje)
                    ctrError.SetIconAlignment(txtExtra, ErrorIconAlignment.MiddleRight)
                    tsslEstado.Text = mensaje
                    Exit Sub
                End If

            End If
        End If


        'Cantidad
        If IsNumeric(txtExtra.ObtenerValor) = False Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtExtra, mensaje)
            ctrError.SetIconAlignment(txtExtra, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If CDec(txtExtra.ObtenerValor) <= 0 Then
            Dim mensaje As String = "La cantidad no es correcta!"
            ctrError.SetError(txtExtra, mensaje)
            ctrError.SetIconAlignment(txtExtra, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Verificar que exista el producto en el deposito de salida

        Dim ExistenciaActual As Decimal = 0
        Dim Existencia As Decimal = 0
        Dim Pedido As Decimal = 0
        Dim Extra As Decimal = 0
        Dim CantidadCaja As Integer = 0
        Dim EsCaja As Boolean = False

        Extra = txtExtra.ObtenerValor
        Pedido = txtPedido.ObtenerValor
        Existencia = txtExistencia.ObtenerValor

        EsCaja = False

        If cbxDepositoSalida.SoloLectura = False Then

            'Obtenemos la cantidad que existe en el deposito el producto seleccionado
            ExistenciaActual = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & txtProducto.Registro("ID").ToString & ", " & cbxDepositoSalida.cbx.SelectedValue & ")")

            ''Obtenemos la cantidad de productos incluyendo los que estan en el detalle
            'For Each oRow As DataRow In dtDetalle.Rows
            '    If ((oRow("IDProducto").ToString = txtProducto.Registro("ID").ToString) And (oRow("IDProducto").ToString = txtFechaEntrega.txt.Text)) Then
            '        Extra += CDec(oRow("Extra"))
            '    End If
            'Next

        End If

        'Si existe, sumar la cantidad
        If dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID") & " and FechaEntrega = '" & txtFechaEntrega.txt.Text & "'").Count > 0 Then
            Dim Rows() As DataRow = dtDetalle.Select(" IDProducto=" & txtProducto.Registro("ID") & " and FechaEntrega = '" & txtFechaEntrega.txt.Text & "'")
            Rows(0)("Existencia") = Existencia
            Rows(0)("Pedido") = Pedido
            Extra += Rows(0)("Extra")
            Rows(0)("Extra") = Extra
            Rows(0)("Kilos") = (Extra) * txtProducto.Registro("Peso")

        Else

            ' Insertar
            'Cargamos el registro en el detalle
            Dim dRow As DataRow = dtDetalle.NewRow()

            'Obtener Valores
            dRow("IDProducto") = txtProducto.Registro("ID").ToString
            dRow("Ref") = txtProducto.Registro("Ref").ToString
            dRow("ID") = dtDetalle.Rows.Count
            dRow("Descripcion") = txtProducto.Registro("Descripcion").ToString
            dRow("FechaEntrega") = txtFechaEntrega.txt.Text


            If Not cbxDepositoSalida.cbx.SelectedValue Is Nothing Then
                dRow("IDDeposito") = cbxDepositoSalida.cbx.SelectedValue
            End If

            dRow("Existencia") = Existencia
            dRow("Pedido") = Pedido
            dRow("Extra") = Extra
            dRow("Observacion") = ""
            dRow("CodigoBarra") = txtProducto.Registro("CodigoBarra").ToString
            dRow("Kilos") = (Extra) * txtProducto.Registro("Peso")
            'Agregamos al detalle
            dtDetalle.Rows.Add(dRow)

        End If

        ListarDetalle()

        'Inicializamos los valores
        txtExtra.txt.Text = "0"
        txtPedido.txt.Text = "0"
        txtExistencia.txt.Text = "0"
        txtProducto.txt.Clear()
        txtFechaEntrega.txt.Text = "  /  /    "

        'Retorno de Foco
        txtProducto.txt.SelectAll()
        txtProducto.txt.Focus()

        ctrError.Clear()
        tsslEstado.Text = ""

    End Sub

    Sub EliminarProducto()

        If IDTransaccion > 0 Then
            Exit Sub
        End If

        'Validar
        If dgw.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro a eliminar!"
            ctrError.SetError(dgw, mensaje)
            ctrError.SetIconAlignment(dgw, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        dtDetalle.Rows(dgw.SelectedRows(0).Index).Delete()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtDetalle.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        ListarDetalle()

    End Sub

    Sub ListarDetalle()

        'Limpiamos todo el detalle
        ' dgw.Rows.Clear()

        'Variables
        Dim Total As Decimal = 0

        'Cargamos registro por registro


        CSistema.dtToGrid(dgw, dtDetalle)

        For i As Integer = 0 To dgw.Columns.Count - 1
            dgw.Columns(i).Visible = False
        Next

        dgw.Columns("Ref").DisplayIndex = 1
        dgw.Columns("Descripcion").DisplayIndex = 2
        dgw.Columns("Existencia").DisplayIndex = 3
        dgw.Columns("Pedido").DisplayIndex = 4
        dgw.Columns("Extra").DisplayIndex = 5
        dgw.Columns("FechaEntrega").DisplayIndex = 6

        dgw.Columns("Ref").Visible = True
        dgw.Columns("Descripcion").Visible = True
        dgw.Columns("Existencia").Visible = True
        dgw.Columns("Pedido").Visible = True
        dgw.Columns("Extra").Visible = True
        dgw.Columns("FechaEntrega").Visible = True


        dgw.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgw.Columns("Existencia").DefaultCellStyle.Format = "N2"
        dgw.Columns("Pedido").DefaultCellStyle.Format = "N2"
        dgw.Columns("Extra").DefaultCellStyle.Format = "N2"

        dgw.Columns("Ref").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgw.Columns("Existencia").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Pedido").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Extra").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgw.Columns("Existencia").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Pedido").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Extra").HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight


        'Total = CSistema.dtSumColumn(dtDetalle, "Total")
        Total = 0


        'Bloqueamos la cabecera si corresponde
        If dtDetalle.Rows.Count > 0 And vNuevo = True Then
            cbxDepositoSalida.cbx.Enabled = False
            txtDesde.Enabled = False
            txtHasta.Enabled = False
        Else
            cbxDepositoSalida.cbx.Enabled = True
            txtDesde.Enabled = True
            txtHasta.Enabled = True
        End If

        ListarTotalKilos()

    End Sub

    Sub ListarTotalKilos()
        dtTotalKilos.Clear()

        Dim Fecha As Date
        Dim TotalPeso As Decimal
        For index = 0 To dgw.RowCount - 1
            Fecha = dgw.Rows(index).Cells("FechaEntrega").Value
            TotalPeso = dgw.Rows(index).Cells("Kilos").Value
            If dtTotalKilos.Select("FechaEntrega = '" & dgw.Rows(index).Cells("FechaEntrega").Value & "'").Count = 0 Then
                Dim newRow As DataRow = dtTotalKilos.NewRow()
                newRow("FechaEntrega") = Fecha
                newRow("Kilos") = TotalPeso
                dtTotalKilos.Rows.Add(newRow)
            Else
                dtTotalKilos.Select("FechaEntrega = '" & dgw.Rows(index).Cells("FechaEntrega").Value & "'")(0)("Kilos") = dtTotalKilos.Select("FechaEntrega = '" & dgw.Rows(index).Cells("FechaEntrega").Value & "'")(0)("Kilos") + TotalPeso
            End If

        Next

        CSistema.dtToGrid(dgvTotalKilos, dtTotalKilos)

        CSistema.DataGridColumnasNumericas(dgvTotalKilos, {"Kilos"})
        dgvTotalKilos.Columns("Kilos").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub Anular()

        'Validar
        If IDTransaccion = 0 Then
            Dim mensaje As String = "Seleccione correctamente el registro para anular!"
            ctrError.SetError(btnAnular, mensaje)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopLeft)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Consulta
        If MessageBox.Show("Atencion! Esto anulara permanentemente el registro. Desea continuar?", "Anular", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Datos
        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesRegistro.ANULAR.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)
        CSistema.SetSQLParameter(param, "@IDTransaccionSalida", "0", ParameterDirection.Output, 18)

        'Anular
        Dim MensajeRetorno As String = ""

        If CSistema.ExecuteStoreProcedure(param, "SpOrdenDePedido", False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnAnular, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnAnular, ErrorIconAlignment.TopRight)

            Exit Sub

        Else
            tsslEstado.Text = MensajeRetorno
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)
        CargarOperacion(IDTransaccion)

    End Sub

    Sub Imprimir()

        Dim CMovimiento As New Reporte.CReporteOrdenDePedido
        CMovimiento.OrdenDePedido(IDTransaccion)

    End Sub

    Sub Buscar()

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        'Otros
        lblAnulado.Visible = False

        Dim frm As New frmConsultaMovimiento
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        FGMostrarFormulario(Me, frm, "Consulta de Registros", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False, True)

        If frm.IDTransaccion > 0 Then
            CargarOperacion(frm.IDTransaccion)
        End If

    End Sub

    Sub CargarOperacion(Optional ByVal vIDTransaccion As Integer = 0)

        vNuevo = False

        ctrError.Clear()
        tsslEstado.Text = ""

        txtID.txt.Focus()
        txtID.txt.SelectAll()

        'Obtenemos el IDTransaccion
        If vIDTransaccion = 0 Then
            IDTransaccion = CSistema.ExecuteScalar("Select IsNull((Select IDTransaccion From VOrdenDePedido Where IDSucursal=" & cbxSucursal.GetValue & " And Numero=" & txtID.ObtenerValor & "), 0 )")
        Else
            IDTransaccion = vIDTransaccion
        End If


        If IDTransaccion = 0 Then
            Dim mensaje As String = "El sistema no encuentra el registro!"
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        EstablecerBotones(ERP.CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA)

        dtDetalle.Clear()

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Numero, Fecha, [Cod.],  NroComprobante, Deposito, Observacion, Autorizacion, Anulado, UsuarioIdentificacionAnulacion, FechaAnulacion, Usuario, UsuarioIdentificador, FechaTransaccion, FechaDesde, FechaHasta, PedidoCliente From VOrdenDePedido Where IDTransaccion=" & IDTransaccion)
        dtDetalle = CSistema.ExecuteToDataTable("Select IDProducto, ID, Ref, 'Descripcion'=Producto, IDDeposito, Existencia, Pedido, Extra,Kilos,FechaEntrega, CodigoBarra,Observacion From VDetalleOrdenDePedido Where IDTransaccion=" & IDTransaccion & " Order By ID").Copy

        'Cargamos la cabecera
        If dt Is Nothing Then
            Dim mensaje As String = "Error en la consulta! Problemas tecnico."
            ctrError.SetError(txtID, mensaje)
            ctrError.SetIconAlignment(txtID, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)


        txtID.txt.Text = oRow("Numero").ToString
        txtFecha.SetValueFromString(CDate(oRow("Fecha").ToString))
        cbxTipoComprobante.Texto = oRow("Cod.").ToString
        txtComprobante.txt.Text = oRow("NroComprobante").ToString
        cbxDepositoSalida.cbx.Text = oRow("Deposito").ToString
        txtObservacion.txt.Text = oRow("Observacion").ToString
        txtDesde.SetValueFromString(CDate(oRow("FechaDesde").ToString))
        txtHasta.SetValueFromString(CDate(oRow("FechaHasta").ToString))
        chkPedidoCliente.Valor = CBool(oRow("PedidoCliente").ToString)


        flpRegistradoPor.Visible = True
        lblFechaRegistro.Text = CSistema.GetDateTimeFormatString(oRow("FechaTransaccion").ToString)
        lblUsuarioRegistro.Text = oRow("UsuarioIdentificador").ToString

        If CBool(oRow("Anulado").ToString) = True Then
            flpAnuladoPor.Visible = True
            lblFechaAnulado.Text = CSistema.GetDateTimeFormatString(oRow("FechaAnulacion").ToString)
            'lblUsuarioAnulado.Text = oRow("UsuarioIdentificacionAnulacion").ToString
            lblUsuarioAnulado.Text = oRow("UsuarioAnulacion").ToString
        Else
            flpAnuladoPor.Visible = False
        End If

        'Cargamos el detalle
        ListarDetalle()

    End Sub


    Sub ManejarTecla(ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            CargarOperacion()
        End If

        If e.KeyCode = Keys.Up Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            ID = CInt(ID) + 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Down Then
            Dim ID As String
            ID = txtID.txt.Text

            If IsNumeric(ID) = False Then
                Exit Sub
            End If

            If CInt(ID) = 1 Then
                Exit Sub
            End If

            ID = CInt(ID) - 1
            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.End Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Max(Numero), 1) From VOrdenDePedido Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        If e.KeyCode = Keys.Home Then

            Dim ID As Integer
            ID = CType(CSistema.ExecuteScalar("Select IsNull(Min(Numero), 1) From VAsignacionCamion Where IDSucursal=" & cbxSucursal.GetValue & ""), Integer)

            txtID.txt.Text = ID
            txtID.txt.SelectAll()
            CargarOperacion()

        End If

        'Nuevo
        If e.KeyCode = vgKeyConsultar Then
            'Buscar()
        End If

        If e.KeyCode = vgKeyNuevoRegistro Then
            Nuevo()
        End If
    End Sub


    Private Sub OcxTXTProducto1_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado

        If txtProducto.Seleccionado = True Then
            'Obtenemos el costo
            Dim ExistenciaActual As Decimal = CSistema.ExecuteScalar("Select dbo.FExistenciaProducto(" & txtProducto.Registro("ID").ToString & ", " & cbxDepositoSalida.cbx.SelectedValue & ")")
            Dim Pedido As Decimal = CSistema.ExecuteScalar("Select dbo.FPedidoProducto(" & txtProducto.Registro("ID").ToString & ", " & cbxSucursal.GetValue & ",'" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "','" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "')")
            txtPedido.txt.Text = Pedido
            txtExistencia.txt.Text = ExistenciaActual
            txtFechaEntrega.Focus()
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar(ERP.CSistema.NUMOperacionesRegistro.INS)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnBusquedaAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusquedaAvanzada.Click
        Buscar()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Imprimir()
    End Sub

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Anular()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub txtCantidad_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtExtra.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            CargarProducto()
        End If
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If

    End Sub

    Private Sub txtID_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtID.TeclaPrecionada
        ManejarTecla(e)
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        'Depositos
        CSistema.SqlToComboBox(cbxDepositoSalida.cbx, "Select ID, Deposito From VDeposito where IDTipoDeposito = 1 and IDSucursal = " & cbxSucursal.GetValue)
        txtID_TeclaPrecionada(New Object, New KeyEventArgs(Keys.End))
    End Sub


    Private Sub frmOrdenDePedido_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmOrdenDePedido_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = vgKeyConsultar Then

            'Si esta en el producto
            If txtProducto.Focus = True Then


            End If

            'Buscar()

            Exit Sub

        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmOrdenDePedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtFechaEntrega_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtFechaEntrega.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            txtExtra.Focus()
        End If
    End Sub

    '09-06-2021 - SC - Actualiza datos
    Sub frmOrdenDePedido_Activate()
        Me.Refresh()
    End Sub
End Class
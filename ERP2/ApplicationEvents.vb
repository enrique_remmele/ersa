﻿Imports System.Deployment
Imports System.IO

Namespace My

    ' Los siguientes eventos están disponibles para MyApplication:
    ' 
    ' Inicio: se desencadena cuando se inicia la aplicación, antes de que se cree el formulario de inicio.
    ' Apagado: generado después de cerrar todos los formularios de la aplicación. Este evento no se genera si la aplicación termina de forma anómala.
    ' UnhandledException: generado si la aplicación detecta una excepción no controlada.
    ' StartupNextInstance: se desencadena cuando se inicia una aplicación de instancia única y la aplicación ya está activa. 
    ' NetworkAvailabilityChanged: se desencadena cuando la conexión de red está conectada o desconectada.

    Partial Friend Class MyApplication

        Dim CLicencia As New CLicencia

        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup

            Try

                ' Define the path of the folder
                Dim folderPath As String = "C:\tmp"

                ' Check if the folder exists
                If Not Directory.Exists(folderPath) Then
                    ' Create the folder
                    Directory.CreateDirectory(folderPath)
                    Console.WriteLine("Folder 'tmp' created successfully in C:\")
                Else
                    Console.WriteLine("Folder 'tmp' already exists in C:\")
                End If

                ' Pause the console so you can see the message
                Console.ReadLine()

                FGIniciarSistema()
                'FGIniciarSistema2()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


            End Try
           

        End Sub

        Private Sub MyApplication_UnhandledException(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs) Handles Me.UnhandledException

            e.ExitApplication =
           MessageBox.Show(e.Exception.Message &
                   vbCrLf & "Continuar?", "Error del Sistema",
                   MessageBoxButtons.YesNo,
                   MessageBoxIcon.Error) _
                   = DialogResult.No

        End Sub


    End Class



End Namespace


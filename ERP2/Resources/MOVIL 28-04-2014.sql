
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFVentaVenta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CFVentaVenta](
	[IDOperacion] [tinyint] NOT NULL,
	[ID] [tinyint] NOT NULL,
 CONSTRAINT [PK_CFVentaVenta] PRIMARY KEY CLUSTERED 
(
	[IDOperacion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFVentaMercaderia]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CFVentaMercaderia](
	[IDOperacion] [tinyint] NOT NULL,
	[ID] [tinyint] NOT NULL,
 CONSTRAINT [PK_CFVentaCosto] PRIMARY KEY CLUSTERED 
(
	[IDOperacion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFVentaImpuesto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CFVentaImpuesto](
	[IDOperacion] [tinyint] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDImpuesto] [tinyint] NULL,
 CONSTRAINT [PK_CFVentaImpuesto] PRIMARY KEY CLUSTERED 
(
	[IDOperacion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFVentaDescuento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CFVentaDescuento](
	[IDOperacion] [tinyint] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDTipoDescuento] [tinyint] NOT NULL,
 CONSTRAINT [PK_CFVentaDescuento] PRIMARY KEY CLUSTERED 
(
	[IDOperacion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFVentaCostoMercaderia]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CFVentaCostoMercaderia](
	[IDOperacion] [tinyint] NOT NULL,
	[ID] [tinyint] NOT NULL,
 CONSTRAINT [PK_CFVentaCostoMercaderia] PRIMARY KEY CLUSTERED 
(
	[IDOperacion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFVentaCliente]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CFVentaCliente](
	[IDOperacion] [tinyint] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[Credito] [bit] NULL,
	[Contado] [bit] NULL,
 CONSTRAINT [PK_CFVentaCliente] PRIMARY KEY CLUSTERED 
(
	[IDOperacion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CFVenta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CFVenta](
	[IDOperacion] [tinyint] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[Credito] [bit] NULL,
	[Contado] [bit] NULL,
	[BuscarEnCliente] [bit] NULL,
	[BuscarEnProducto] [bit] NULL,
	[IDTipoCuentaFija] [tinyint] NULL,
	[IDTipoDescuento] [tinyint] NULL,
 CONSTRAINT [PK_CFVenta] PRIMARY KEY CLUSTERED 
(
	[IDOperacion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CF]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CF](
	[IDOperacion] [tinyint] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[IDTipoComprobante] [smallint] NULL,
	[CuentaContable] [varchar](50) NOT NULL,
	[Debe] [bit] NOT NULL,
	[Haber] [bit] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Orden] [tinyint] NOT NULL,
 CONSTRAINT [PK_CF] PRIMARY KEY CLUSTERED 
(
	[IDOperacion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CentroCosto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CentroCosto](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_CentroCosto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Categoria]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Categoria](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Camion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Camion](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Patente] [varchar](10) NULL,
	[Chasis] [varchar](20) NULL,
	[Capacidad] [smallint] NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_Camion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Barrio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Barrio](
	[ID] [smallint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Orden] [smallint] NULL,
	[Estado] [bit] NULL,
	[Referencia] [varchar](10) NULL,
 CONSTRAINT [PK_Barrio] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Banco]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Banco](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Referencia] [varchar](5) NULL,
	[Estado] [bit] NULL,
	[IDSucursal] [tinyint] NULL,
 CONSTRAINT [PK_Banco] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClasificacionProductoAgrupador]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClasificacionProductoAgrupador](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_ClasificacionProductoAgrupador] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClasificacionProducto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClasificacionProducto](
	[ID] [smallint] NOT NULL,
	[IDPadre] [smallint] NULL,
	[Nivel] [tinyint] NULL,
	[Numero] [tinyint] NULL,
	[Codigo] [varchar](10) NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Alias] [varchar](10) NULL,
	[Imagen] [image] NULL,
	[IDAgrupador] [tinyint] NULL,
 CONSTRAINT [PK_ClasificacionProducto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccesoEspecifico]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AccesoEspecifico](
	[ID] [int] NOT NULL,
	[IDMenu] [numeric](18, 0) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Funcion] [bit] NULL,
	[Control] [bit] NULL,
	[NombreFuncion] [varchar](50) NULL,
	[NombreControl] [varchar](50) NULL,
	[Enabled] [bit] NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_AccesoEspecifico] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[IDMenu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Actividad]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Actividad](
	[ID] [int] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDProveedor] [int] NOT NULL,
	[Codigo] [varchar](50) NOT NULL,
	[Mecanica] [varchar](500) NOT NULL,
	[ImporteAsignado] [money] NULL,
	[Entrada] [money] NULL,
	[Salida] [money] NULL,
	[DescuentoMaximo] [decimal](5, 2) NULL,
	[CarteraMaxima] [tinyint] NULL,
	[Tactico] [money] NULL,
	[Acuerdo] [money] NULL,
	[Total] [money] NULL,
	[PorcentajeUtilizado] [smallint] NULL,
	[Saldo] [money] NULL,
	[Excedente] [money] NULL,
	[DescuentoMinimo] [tinyint] NULL,
	[CarteraMinima] [tinyint] NULL,
	[Cobertura] [tinyint] NULL,
 CONSTRAINT [PK_DetalleActividad] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cobrador]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Cobrador](
	[ID] [tinyint] NOT NULL,
	[Nombres] [varchar](50) NOT NULL,
	[NroDocumento] [varchar](15) NULL,
	[Telefono] [varchar](20) NULL,
	[Celular] [varchar](20) NULL,
	[Direccion] [varchar](100) NULL,
	[Email] [varchar](50) NULL,
	[Estado] [bit] NULL,
	[Referencia] [varchar](10) NULL,
 CONSTRAINT [PK_Cobrador] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cotizacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Cotizacion](
	[ID] [int] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[Cotizacion] [money] NOT NULL,
 CONSTRAINT [PK_Cotizacion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Configuraciones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Configuraciones](
	[IDSucursal] [tinyint] NULL,
	[FormatoFecha] [varchar](50) NULL,
	[PropietarioBD] [varchar](50) NULL,
	[VersionSistema] [varchar](200) NULL,
	[MovimientoBloquearFecha] [bit] NULL,
	[MovimientoExigirAutorizacion] [bit] NULL,
	[MovimientoImprimirDocumento] [bit] NULL,
	[MovimientoBloquearCampoCosto] [bit] NULL,
	[MovimientoHabilitarSiCostoCero] [bit] NULL,
	[MovimientoCostoProducto] [varchar](50) NULL,
	[MovimientoBloquearAnulacion] [bit] NULL,
	[MovimientoDiasBloqueo] [tinyint] NULL,
	[VentaBloquearFecha] [bit] NULL,
	[VentaImprimirDocumento] [bit] NULL,
	[VentaBloquearAnulacion] [bit] NULL,
	[VentaDiasBloqueo] [tinyint] NULL,
	[VentaFechaFacturacionActivar] [bit] NULL,
	[VentaFechaFacturacion] [date] NULL,
	[VentaModificarDescuento] [bit] NULL,
	[VentaModificarCantidad] [bit] NULL,
	[VentaPedirCredencialAgregarDescuento] [bit] NULL,
	[VentaSuperarTacticoConCredencial] [bit] NULL,
	[ERPTablasPrincipales] [varchar](500) NULL,
	[ERPTablasMayores] [varchar](500) NULL,
	[CobranzaCreditoBloquearFecha] [bit] NULL,
	[CobranzaCreditoImprimirDocumento] [bit] NULL,
	[CobranzaCreditoBloquearAnulacion] [bit] NULL,
	[CobranzaCreditoDiasBloqueo] [tinyint] NULL,
	[CobranzaContadoBloquearFecha] [bit] NULL,
	[CobranzaContadoImprimirDocumento] [bit] NULL,
	[CobranzaContadoBloquearAnulacion] [bit] NULL,
	[CobranzaContadoDiasBloqueo] [tinyint] NULL,
	[OrdenPagoBloquearAnulacion] [bit] NULL,
	[OrdenPagoDiasBloqueo] [tinyint] NULL,
	[OrdenPagoALaOrdenPredefinido] [varchar](50) NULL,
	[PedidoCantidadProducto] [tinyint] NULL,
	[PedidoBloquearTactico] [bit] NULL,
	[PedidoNoSuperarTactico] [bit] NULL,
	[CompraPorcentajeRetencion] [tinyint] NULL,
	[CompraImporteMinimoRetencion] [money] NULL,
	[ProductoClasificacion1] [varchar](50) NULL,
	[ProductoClasificacion2] [varchar](50) NULL,
	[ProductoClasificacion3] [varchar](50) NULL,
	[ProductoClasificacion4] [varchar](50) NULL,
	[ProductoClasificacion5] [varchar](50) NULL,
	[ProductoClasificacion6] [varchar](50) NULL,
	[RetencionImprimirDocumento] [bit] NULL
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Distribuidor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Distribuidor](
	[ID] [tinyint] NOT NULL,
	[Nombres] [varchar](50) NOT NULL,
	[NroDocumento] [varchar](15) NULL,
	[Telefono] [varchar](20) NULL,
	[Celular] [varchar](20) NULL,
	[Direccion] [varchar](100) NULL,
	[Email] [varchar](50) NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_Distribuidor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EstadoCliente]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EstadoCliente](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[Orden] [tinyint] NULL,
	[Estado] [bit] NULL,
	[Codigo] [varchar](10) NULL,
 CONSTRAINT [PK_EstadoCliente] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Chofer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Chofer](
	[ID] [tinyint] NOT NULL,
	[Nombres] [varchar](50) NOT NULL,
	[NroDocumento] [varchar](15) NULL,
	[Telefono] [varchar](20) NULL,
	[Celular] [varchar](20) NULL,
	[Direccion] [varchar](100) NULL,
	[Email] [varchar](50) NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_Chofer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FormaPagoTarjeta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FormaPagoTarjeta](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[Comprobante] [varchar](50) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[ImporteMoneda] [money] NOT NULL,
	[Cotizacion] [money] NOT NULL,
	[Total] [money] NULL,
	[Saldo] [money] NULL,
	[Observacion] [varchar](100) NULL,
	[Cancelado] [bit] NULL,
	[TerminacionTarjeta] [char](4) NULL,
	[FechaVencimientoTarjeta] [smalldatetime] NULL,
	[IDTipoTarjeta] [tinyint] NULL,
	[Importe] [money] NULL,
	[TipoComprobante] [varchar](50) NULL,
	[Moneda] [varchar](50) NULL,
	[Boleta] [varchar](50) NULL,
 CONSTRAINT [PK_FormaPagoTarjeta] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LineaMarca]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LineaMarca](
	[IDLinea] [smallint] NOT NULL,
	[IDMarca] [tinyint] NOT NULL,
 CONSTRAINT [PK_LineaMarca] PRIMARY KEY CLUSTERED 
(
	[IDLinea] ASC,
	[IDMarca] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Linea]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Linea](
	[ID] [smallint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Linea] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Impuesto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Impuesto](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](10) NOT NULL,
	[FactorDiscriminado] [decimal](10, 7) NULL,
	[FactorImpuesto] [decimal](10, 7) NULL,
	[Exento] [bit] NULL,
	[Referencia] [varchar](5) NULL,
	[RetencionIVA] [money] NULL,
	[RetencionRenta] [money] NULL,
 CONSTRAINT [PK_Impuesto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExistenciaDepositoHistorial]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExistenciaDepositoHistorial](
	[IDDeposito] [tinyint] NULL,
	[IDProducto] [int] NULL,
	[ExistenciaActual] [decimal](10, 2) NULL,
	[Signo] [char](1) NULL,
	[Cantidad] [decimal](10, 2) NULL,
	[ExistenciaNueva] [decimal](10, 2) NULL,
	[CantidadGuardada] [decimal](10, 2) NULL,
	[Fecha] [datetime] NULL
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FechaFacturacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FechaFacturacion](
	[IDSucursal] [tinyint] NOT NULL,
	[Fecha] [date] NOT NULL
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FMes]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'Create Function [dbo].[FMes]
(

	--Entrada
	@Mes tinyint
	
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	Set @vRetorno = ''---''
	If @Mes = 1 Set @vRetorno = ''Enero''
	If @Mes = 2 Set @vRetorno = ''Febrero''
	If @Mes = 3 Set @vRetorno = ''Marzo''
	If @Mes = 4 Set @vRetorno = ''Abril''
	If @Mes = 5 Set @vRetorno = ''Mayo''
	If @Mes = 6 Set @vRetorno = ''Junio''
	If @Mes = 7 Set @vRetorno = ''Julio''
	If @Mes = 8 Set @vRetorno = ''Agosto''
	If @Mes = 9 Set @vRetorno = ''Setiembre''
	If @Mes = 10 Set @vRetorno = ''Octubre''
	If @Mes = 11 Set @vRetorno = ''Noviembre''
	If @Mes = 12 Set @vRetorno = ''Diciembre''
		
	
	return @vRetorno
	
End


' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FFormatoDosDigitos]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'Create Function [dbo].[FFormatoDosDigitos]
(

	--Entrada
	@Valor tinyint
	
)

Returns varchar(50)

As


Begin

	declare @vRetorno varchar(50)
	Set @vRetorno = ''00''
	if @Valor < 10 Set @vRetorno = ''0'' + convert(varchar(50), @Valor)
	if @Valor >= 10 Set @vRetorno = convert(varchar(50), @Valor)
	
	return @vRetorno
	
End' 
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TablasPrincipales]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TablasPrincipales](
	[Tabla] [varchar](50) NOT NULL,
	[NombreTabla] [varchar](50) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[OrderBy] [varchar](50) NULL,
	[Sp] [bit] NULL
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TablaLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TablaLog](
	[ID] [smallint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TablaLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TablaImportar]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TablaImportar](
	[ID] [smallint] NOT NULL,
	[Tabla] [varchar](50) NULL,
 CONSTRAINT [PK_TablaImportar] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Presentacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Presentacion](
	[ID] [smallint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Presentacion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlanCuenta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PlanCuenta](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Resolucion173] [bit] NOT NULL,
	[Titular] [bit] NOT NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_PlanCuenta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Perfil]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Perfil](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
	[Visualizar] [bit] NULL,
	[Agregar] [bit] NULL,
	[Modificar] [bit] NULL,
	[Eliminar] [bit] NULL,
	[Anular] [bit] NULL,
	[Imprimir] [bit] NULL,
 CONSTRAINT [PK_Perfil] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pais]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Pais](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Nacionalidad] [varchar](50) NULL,
	[Orden] [smallint] NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_Pais] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Operacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Operacion](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Codigo] [varchar](10) NOT NULL,
	[AsientoCredito] [bit] NULL,
	[AsientoDebito] [bit] NULL,
	[FormName] [varchar](50) NULL,
	[Tabla] [varchar](50) NULL,
 CONSTRAINT [PK_Operacion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Promotor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Promotor](
	[ID] [tinyint] NOT NULL,
	[Nombres] [varchar](50) NOT NULL,
	[NroDocumento] [varchar](15) NULL,
	[Telefono] [varchar](20) NULL,
	[Celular] [varchar](20) NULL,
	[Direccion] [varchar](100) NULL,
	[Email] [varchar](50) NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_Promotor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductoCombinado]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductoCombinado](
	[IDProducto] [int] NOT NULL,
	[IDProductoPrincipal] [int] NOT NULL,
	[IDProductoSecundario] [int] NOT NULL,
	[CantidadPrincipal] [decimal](10, 2) NOT NULL,
	[CantidadSecundario] [decimal](10, 2) NOT NULL,
 CONSTRAINT [PK_ProductoCombinado] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[IDProductoPrincipal] ASC,
	[IDProductoSecundario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Marca]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Marca](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Marca] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Menu]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Menu](
	[Codigo] [numeric](18, 0) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Nivel] [tinyint] NOT NULL,
	[NombreControl] [varchar](100) NOT NULL,
	[CodigoPadre] [numeric](18, 0) NOT NULL,
	[Tag] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MotivoRechazoCheque]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MotivoRechazoCheque](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_MotivoRechazoCheque] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MotivoEfectivo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MotivoEfectivo](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_MotivoEfectivo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MotivoAnulacionVenta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MotivoAnulacionVenta](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Motivo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Moneda]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Moneda](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Referencia] [varchar](10) NOT NULL,
	[Decimales] [bit] NOT NULL,
	[Estado] [bit] NULL,
	[Divide] [bit] NULL,
	[Multiplica] [bit] NULL,
 CONSTRAINT [PK_Moneda] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZonaVenta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ZonaVenta](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[Estado] [bit] NULL,
	[Orden] [smallint] NULL,
	[Poligono] [varchar](max) NULL,
	[ColorPoligono] [varchar](50) NULL,
	[Referencia] [varchar](10) NULL,
 CONSTRAINT [PK_ZonaVenta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Vendedor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Vendedor](
	[ID] [tinyint] NOT NULL,
	[Nombres] [varchar](50) NOT NULL,
	[Apellidos] [varchar](50) NULL,
	[NroDocumento] [varchar](15) NULL,
	[Telefono] [varchar](20) NULL,
	[Celular] [varchar](20) NULL,
	[Direccion] [varchar](100) NULL,
	[Email] [varchar](50) NULL,
	[Estado] [bit] NULL,
	[IDSucursal] [tinyint] NULL,
	[Referencia] [varchar](10) NULL,
	[Resumen] [varchar](50) NULL,
	[IDDeposito] [tinyint] NULL,
 CONSTRAINT [PK_Vendedor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoCliente]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoCliente](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NULL,
	[Referencia] [varchar](10) NULL,
 CONSTRAINT [PK_TipoCliente] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoProveedor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoProveedor](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NULL,
	[Referencia] [varchar](50) NULL,
 CONSTRAINT [PK_TipoProveedor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UnidadMedida]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UnidadMedida](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Referencia] [varchar](5) NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_UnidadMedida] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SubLinea2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SubLinea2](
	[ID] [smallint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_SubLinea2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SubLinea]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SubLinea](
	[ID] [smallint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_SubLinea] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoProducto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoProducto](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_TipoProducto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoOperacionLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoOperacionLog](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Codigo] [varchar](50) NULL,
 CONSTRAINT [PK_TipoLogSuceso] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoDescuento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoDescuento](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Codigo] [varchar](5) NOT NULL,
	[RangoFecha] [bit] NOT NULL,
	[Limite] [tinyint] NOT NULL,
	[CuentaVenta] [varchar](50) NULL,
	[CuentaCompra] [varchar](50) NULL,
	[Editable] [bit] NULL,
 CONSTRAINT [PK_TipoDescuento] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoCuentaFija]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoCuentaFija](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NULL,
	[Impuesto] [bit] NULL,
	[IDImpuesto] [tinyint] NULL,
	[Producto] [bit] NULL,
	[ProductoCosto] [bit] NULL,
	[Total] [bit] NULL,
	[Descuento] [bit] NULL,
	[IncluirImpuesto] [bit] NULL,
	[IncluirDescuento] [bit] NULL,
	[Campo] [varchar](50) NULL,
 CONSTRAINT [PK_DetalleTipoCuentaFija] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoComprobante]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoComprobante](
	[ID] [smallint] NOT NULL,
	[Cliente] [bit] NOT NULL,
	[Proveedor] [bit] NOT NULL,
	[IDOperacion] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Codigo] [varchar](10) NOT NULL,
	[Resumen] [varchar](20) NULL,
	[Estado] [bit] NOT NULL,
	[ComprobanteTimbrado] [bit] NOT NULL,
	[CalcularIVA] [bit] NOT NULL,
	[IVAIncluido] [bit] NOT NULL,
	[LibroVenta] [bit] NOT NULL,
	[LibroCompra] [bit] NOT NULL,
	[Signo] [char](1) NULL,
	[Autonumerico] [bit] NULL,
	[Deposito] [bit] NULL,
	[Restar] [bit] NULL,
	[HechaukaTipoDocumento] [varchar](50) NULL,
	[HechaukaTimbradoReemplazar] [bit] NULL,
	[HechaukaNumeroTimbrado] [varchar](50) NULL,
 CONSTRAINT [PK_TipoComprobante] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoOperacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoOperacion](
	[ID] [tinyint] NOT NULL,
	[IDOperacion] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Activo] [bit] NOT NULL,
	[Entrada] [bit] NOT NULL,
	[Salida] [bit] NOT NULL,
 CONSTRAINT [PK_TipoMovimiento] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transporte]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Transporte](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[IDZonaVenta] [tinyint] NULL,
	[IDDistribuidor] [tinyint] NULL,
	[IDCamion] [tinyint] NULL,
	[IDChofer] [tinyint] NULL,
	[FechaModificacion] [date] NOT NULL,
 CONSTRAINT [PK_Transporte] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VAccesoEspecifico]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VAccesoEspecifico] As Select * From AccesoEspecifico
'







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usuario]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Usuario](
	[ID] [smallint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Password] [varchar](500) NOT NULL,
	[IDPerfil] [tinyint] NOT NULL,
	[Estado] [bit] NULL,
	[Identificador] [varchar](3) NULL,
	[EsVendedor] [bit] NULL,
	[IDVendedor] [int] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipoProductoLinea]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipoProductoLinea](
	[IDTipoProducto] [tinyint] NOT NULL,
	[IDLinea] [smallint] NOT NULL,
 CONSTRAINT [PK_TipoProductoLinea] PRIMARY KEY CLUSTERED 
(
	[IDTipoProducto] ASC,
	[IDLinea] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VClasificacionProducto]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VClasificacionProducto]
As

Select 
CP.*,
--''Clasificacion''=Codigo + ''-'' + Descripcion
''Clasificacion''=CP.Descripcion,
''Grupo''=ISNULL((Select CPA.Descripcion From ClasificacionProductoAgrupador CPA Where CPA.ID=CP.IDAgrupador), '''')

From ClasificacionProducto CP'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCotizacion]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VCotizacion]
As
Select 
ID,
Fecha,
IDMoneda,
Cotizacion 
From Cotizacion'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCobrador]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VCobrador] As Select * From Cobrador
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VBarrio]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VBarrio]
As
Select 
*
From Barrio'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VBanco]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VBanco]
As
Select 
*
From Banco
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VLinea]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VLinea]
As
Select
L.ID,
L.Descripcion,
L.Estado

From Linea L'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VImpuesto]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VImpuesto] As Select * From Impuesto'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VSubLinea2]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VSubLinea2]
as
Select
SL2.* 
From SubLinea2 SL2
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VSubLinea]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VSubLinea]
as
Select
SL.* 
From SubLinea SL
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VMenu]'))
EXEC dbo.sp_executesql @statement = N'--INSERT INTO Menu(Codigo,Descripcion,Nivel,NombreControl,CodigoPadre) VALUES(''611'',''Gastos'',''0'',''GastosToolStripMenuItem1'',''6'')
--INSERT INTO Menu(Codigo,Descripcion,Nivel,NombreControl,CodigoPadre) VALUES(''612'',''Anticipos a Proveedor'',''0'',''AnticiposToolStripMenuItem'',''6'')
CREATE View [dbo].[VMenu]
As
Select * From Menu

'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VPresentacion]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VPresentacion]
as
Select
P.* 
From Presentacion P
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VPerfil]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VPerfil] 
As 
Select
*,
''V''=(Case When (Visualizar) = ''True'' Then ''SI'' Else ''---'' End),
''A''=(Case When (Agregar) = ''True'' Then ''SI'' Else ''---'' End),
''M''=(Case When (Modificar) = ''True'' Then ''SI'' Else ''---'' End),
''E''=(Case When (Eliminar) = ''True'' Then ''SI'' Else ''---'' End),
''N''=(Case When (Anular) = ''True'' Then ''SI'' Else ''---'' End),
''I''=(Case When (Imprimir) = ''True'' Then ''SI'' Else ''---'' End)

From Perfil

'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VMarca]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VMarca]
As
Select 
*,
''Activo''=Case When Estado = ''True'' Then ''SI'' Else ''---'' End
From Marca'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VTipoCliente]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VTipoCliente] As Select * From TipoCliente
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VZonaVenta]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VZonaVenta] As Select * From ZonaVenta'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VTipoProducto]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VTipoProducto]
as
Select
TP.* 
From TipoProducto TP
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VTipoOperacionLog]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VTipoOperacionLog]
As
Select * From TipoOperacionLog'







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Modelo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Modelo](
	[ID] [smallint] NOT NULL,
	[IDMarca] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Modelo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogSucesos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LogSucesos](
	[ID] [numeric](18, 0) NOT NULL,
	[IDTipoOperacionLog] [tinyint] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IDTabla] [smallint] NOT NULL,
	[Tabla] [varchar](50) NOT NULL,
	[IDTransaccion] [numeric](18, 0) NULL,
	[Comprobante] [varchar](50) NULL,
	[IDUsuario] [smallint] NULL,
	[Usuario] [varchar](50) NULL,
	[IDTerminal] [smallint] NULL,
 CONSTRAINT [PK_LogSucesos] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FFechaCotizacionAlDia]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'Create Function [dbo].[FFechaCotizacionAlDia]
(

	--Entrada
	@IDMoneda tinyint,
	@IDOperacion tinyint,
	@Formato tinyint
)

Returns varchar(50)

As

Begin

	Declare @vRetorno varchar(50)
	Set @vRetorno = ''''
	
	--Si no existe ninguna cotizacion, emitir ''---''
	If Not Exists(Select * From Cotizacion Where IDMoneda = @IDMoneda) Begin
		Set @vRetorno = ''---''
	End
	
	--Si el IDMoneda = 0, significa que es de la moneda local, retornar solo ''---''
	If @IDMoneda = 1 Begin
		Set @vRetorno = ''---''
	End
	
	--En caso de que ninguna de las condiciones de arriba se cumplan
	--Buscamos la ultima cotizacion de la moneda
	If @vRetorno = '''' Begin
		Set @vRetorno = Convert(varchar(50), (Select C1.Fecha From Cotizacion C1 Where C1.ID = (Select Max(C2.ID) From Cotizacion C2 Where C2.IDMoneda=@IDMoneda)), @Formato)		
	End
	
	Return @vRetorno	

End




' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FCotizacionAlDia]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FCotizacionAlDia]
(

	--Entrada
	@IDMoneda tinyint,
	@IDOperacion tinyint
)

Returns money

As

Begin

	Declare @vRetorno money
	
	Set @vRetorno = 0
	
	--Si no existe ninguna cotizacion, emitir 1
	If Not Exists(Select * From Cotizacion Where IDMoneda = @IDMoneda) Begin
		Set @vRetorno = 1
	End
	
	--Si el IDMoneda = 0, significa que es de la moneda local, retornar solo 1
	If @IDMoneda = 1 Begin
		Set @vRetorno = 1		
	End
	
	--En caso de que ninguna de las condiciones de arriba se cumplan
	--Buscamos la ultima cotizacion de la moneda
	If @vRetorno = 0 Begin
		Set @vRetorno = (Select C1.Cotizacion From Cotizacion C1 Where C1.ID = (Select Max(C2.ID) From Cotizacion C2 Where C2.IDMoneda=@IDMoneda))
	End
	
	Return @vRetorno	

End



' 
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Departamento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Departamento](
	[ID] [tinyint] NOT NULL,
	[IDPais] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NULL,
	[Orden] [smallint] NULL,
	[Codigo] [varchar](10) NULL,
 CONSTRAINT [PK_Departamento] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CuentaContable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CuentaContable](
	[ID] [int] NOT NULL,
	[IDPlanCuenta] [tinyint] NOT NULL,
	[Codigo] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Alias] [varchar](50) NULL,
	[Categoria] [tinyint] NOT NULL,
	[Imputable] [bit] NOT NULL,
	[IDPadre] [smallint] NULL,
	[Estado] [bit] NOT NULL,
	[Sucursal] [bit] NULL,
	[IDSucursal] [tinyint] NULL,
	[CajaChica] [bit] NULL,
 CONSTRAINT [PK_CuentaContable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AccesoPerfil](
	[IDMenu] [numeric](18, 0) NOT NULL,
	[IDPerfil] [tinyint] NOT NULL,
	[Visualizar] [bit] NULL,
	[Agregar] [bit] NULL,
	[Modificar] [bit] NULL,
	[Eliminar] [bit] NULL,
	[Anular] [bit] NULL,
	[Imprimir] [bit] NULL,
 CONSTRAINT [PK_AccesoPerfil] PRIMARY KEY CLUSTERED 
(
	[IDMenu] ASC,
	[IDPerfil] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccesoEspecificoPerfil]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AccesoEspecificoPerfil](
	[IDPerfil] [tinyint] NOT NULL,
	[IDAccesoEspecifico] [int] NOT NULL,
	[IDMenu] [numeric](18, 0) NOT NULL,
	[Habilitar] [bit] NOT NULL,
 CONSTRAINT [PK_AccesoEspecificoPerfil] PRIMARY KEY CLUSTERED 
(
	[IDPerfil] ASC,
	[IDAccesoEspecifico] ASC,
	[IDMenu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ciudad]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Ciudad](
	[ID] [smallint] NOT NULL,
	[IDPais] [tinyint] NOT NULL,
	[IDDepartamento] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Codigo] [varchar](5) NULL,
	[Estado] [bit] NULL,
	[Orden] [tinyint] NULL,
	[Referencia] [varchar](10) NULL,
 CONSTRAINT [PK_Ciudad] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DetalleEfectivo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DetalleEfectivo](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[ID] [smallint] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[Comprobante] [varchar](50) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[VentasCredito] [money] NOT NULL,
	[VentasContado] [money] NOT NULL,
	[Gastos] [money] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[ImporteMoneda] [money] NOT NULL,
	[Cotizacion] [money] NOT NULL,
	[Observacion] [varchar](100) NULL,
 CONSTRAINT [PK_DetalleEfectivo] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LineaSubLinea]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LineaSubLinea](
	[IDTipoProducto] [tinyint] NOT NULL,
	[IDLinea] [smallint] NOT NULL,
	[IDSubLinea] [smallint] NOT NULL,
 CONSTRAINT [PK_LineaSubLinea] PRIMARY KEY CLUSTERED 
(
	[IDTipoProducto] ASC,
	[IDLinea] ASC,
	[IDSubLinea] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpLogSuceso]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[SpLogSuceso]

	--Entrada
	@Operacion varchar(50),
	@Tabla varchar(50),
	@IDUsuario smallint,
	
	--No requerido
	@IDTransaccion numeric(18,0) = NULL,
	@Comprobante varchar(50) = NULL,
	@IDTerminal smallint = NULL
		
As

Begin

	--Variables
	Declare @vIDTipoOperacionLog tinyint
	Declare @vIDTabla smallint
	Declare @vID numeric(18,0)
	Declare @vUsuario varchar(50)
	
	--IDTipoOperacion
	Begin
		
		If Not Exists(Select * From TipoOperacionLog Where Codigo=@Operacion) Begin
		
			Set @vIDTipoOperacionLog = IsNull((Select MAX(ID) + 1 From TipoOperacionLog),1)
			 
			Insert Into TipoOperacionLog(ID, Descripcion, Codigo)
			Values(@vIDTipoOperacionLog, @Operacion, @Operacion)
		End Else Begin
			Set @vIDTipoOperacionLog = (Select ID From TipoOperacionLog Where Codigo=@Operacion)
		End
		
	End
	
	--Tabla
	Begin
		
		If Not Exists(Select * From TablaLog Where Descripcion=@Tabla) Begin
		
			Set @vIDTabla = IsNull((Select MAX(ID) + 1 From TablaLog),1)
			 
			Insert Into TablaLog(ID, Descripcion)
			Values(@vIDTabla, @Tabla)
			
		End Else Begin
			
			Set @vIDTabla = (Select ID From TablaLog Where Descripcion=@Tabla)
			
		End
		
	End
	
	--Uuario
	Set @vUsuario = (Select Usuario From Usuario Where ID=@IDUsuario)
	Set @vID = IsNull((Select MAX(ID) + 1 From LogSucesos),1)
	
	Insert Into LogSucesos(ID, IDTipoOperacionLog, Fecha, IDTabla, Tabla, IDTransaccion, Comprobante, IDTerminal, IDUsuario, Usuario)
	Values(@vID, @vIDTipoOperacionLog, GETDATE(), @vIDTabla, @Tabla, @IDTransaccion, @Comprobante, @IDTerminal, @IDUsuario, @vUsuario)
	
	Return @@rowcount

End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpCambiarContraseña]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpCambiarContraseña]

	--Entrada
	@IDUsuario smallint,
	@IDPerfil smallint,
	@IDSucursal tinyint,
	@IDTerminal smallint,
	
	@ContraseñaActual varchar(50),
	@ContraseñaNueva varchar(50),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output

As

Begin

	Declare  @vPasswordActual varchar(300)
	
	Set @vPasswordActual = Convert(varchar(300), (Select [Password] From Usuario Where ID=@IDUsuario))
	Set @Mensaje = ''---''
	Set @Procesado = ''False''
		
	--validar
	If @ContraseñaNueva = '''' Begin
		Set @Mensaje = ''La contraseña no es segura!''
		Set @Procesado = ''False''
		Return @@rowcount	
	End
	
	Set @ContraseñaActual = Convert(varchar(300), HASHBYTES(''Sha1'', @ContraseñaActual))
	
	If @vPasswordActual != @ContraseñaActual Begin
		Set @Mensaje = ''La contraseña anterior no es correcta!''
		Set @Procesado = ''False''
		Return @@rowcount	
	End	
		
	Set @ContraseñaNueva = Convert(varchar(300), HASHBYTES(''Sha1'', @ContraseñaNueva))
	
	Update Usuario Set [Password]=@ContraseñaNueva
	Where ID=@IDUsuario

	Set @Mensaje = ''Registro guardado!''
	Set @Procesado = ''True''	
		
End
' 
END





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VUsuario]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VUsuario]
As
Select 
U.*,

--Perfil
''Perfil''=IsNull(P.Descripcion, ''---''),

--Vendedor
''Vendedor''=(ISNULL((Select Nombres From Vendedor V Where V.ID=U.IDVendedor),''---''))


From Usuario U
Left Outer Join Perfil P On U.IDPerfil=P.ID
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VTipoProductoLinea]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VTipoProductoLinea]
As
Select
TPL.*,
''TipoProducto''=TP.Descripcion,
''Linea''=L.Descripcion
From TipoProductoLinea TPL
Join TipoProducto TP On TPL.IDTipoProducto=TP.ID
Join Linea L On TPL.IDLinea=L.ID
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VTipoOperacion]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VTipoOperacion]
As
Select 
T.ID, 
''Operacion''=O.Descripcion,
T.IDOperacion,
T.Descripcion,
T.Activo,
''Estado''=Case When (T.Activo) = ''True'' Then ''OK'' Else ''---'' End,
T.Entrada,
T.Salida
From TipoOperacion T 
Join Operacion O On T.IDOperacion=O.ID
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VTipoCuentaFija]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VTipoCuentaFija]
As
Select 
T.ID,
T.Descripcion,
T.Impuesto,
''TipoImpuesto''=ISNULL((Select I.Descripcion From Impuesto I Where I.ID=T.IDImpuesto), ''---''),
T.IDImpuesto,
T.Producto,
T.Total,
T.Descuento,
T.IncluirImpuesto,
T.IncluirDescuento,
''Tipo''=(Case When(T.Impuesto)=''True'' Then ''IMPUESTO'' Else (Case When(T.Producto)=''True'' Then ''PRODUCTO'' Else (Case When(T.Total)=''True'' Then ''TOTAL'' Else (Case When(T.Descuento)=''True'' Then ''DESCUENTO'' Else ''---'' End) End) End) End),
T.Campo
From TipoCuentaFija T




'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VTipoComprobante]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VTipoComprobante]
As
Select
TC.ID, 
TC.Cliente,
TC.Proveedor,
''Tipo''=(Case When (TC.Cliente) = ''True'' Then ''CLIENTE'' Else (Case When (TC.Proveedor) = ''True'' Then ''PROVEEDOR'' Else ''---'' End) End),
TC.IDOperacion,
''Operacion''=O.Descripcion,
TC.Descripcion,
TC.Codigo,
TC.Resumen,
TC.Estado,
TC.ComprobanteTimbrado,
TC.CalcularIVA,
TC.IVAIncluido,
TC.LibroVenta,
TC.LibroCompra,
''Libro''=(Case When TC.LibroVenta = ''True'' Then ''True'' Else (Case When TC.LibroCompra = ''True'' Then ''True'' Else ''False'' End) End),
TC.Signo,
TC.Autonumerico,
TC.Restar,

--Hechauka
 ''HechaukaTipoDocumento''=ISNULL(HechaukaTipoDocumento, ''0''), 
 ''HechaukaTimbradoReemplazar''=ISNULL(HechaukaTimbradoReemplazar, ''False''), 
 ''HechaukaNumeroTimbrado''=ISNULL(HechaukaNumeroTimbrado, ''''),
 
--Operacion
O.FormName


From TipoComprobante TC
Join Operacion O On TC.IDOperacion=O.ID







'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VMoneda]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VMoneda]
As
Select 
M.ID, 
''Local''=Case When (M.ID) = 1 Then ''True'' Else ''False'' End,
''Moneda''=Case When (M.ID) = 1 Then ''LOC'' Else ''EXT'' End,
M.Descripcion, 
M.Referencia, 
M.Decimales, 
M.Estado, 
M.Divide, 
M.Multiplica,
''Cotizacion''=dbo.FCotizacionAlDia(M.ID,1),
''Fec. Cot.''=dbo.FFechaCotizacionAlDia(M.ID,1,5)
From Moneda M


'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VAccesoPerfil]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VAccesoPerfil] 
As 
Select 
M.*,
''IDPerfil''=P.ID,
''Perfil''=P.Descripcion,
AP.Visualizar,
AP.Agregar,
AP.Modificar,
AP.Eliminar,
AP.Anular,
AP.Imprimir

From AccesoPerfil AP
Join VMenu M On AP.IDMenu=M.Codigo
Join VPerfil P On AP.IDPerfil=P.ID'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VAccesoEspecificoPerfil]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VAccesoEspecificoPerfil]
As
Select 

--Perfil
''IDPerfil''=P.ID,

--Acceso Perfil
''Acceso''=IsNull(A.Descripcion, ''---''),
''Funcion''=IsNull(A.Funcion, ''False''),
''Control''=IsNull(A.[Control], ''False''),
''NombreFuncion''=IsNull(A.NombreFuncion, ''---''),
''NombreControl''=IsNull(A.NombreControl, ''---''),
''Enabled''=IsNull(A.[Enabled], ''False''),
''Visible''=IsNull(A.Visible, ''False''),

''Habilitado''=(IsNull(AF.Habilitar, ''False'')),
''IDAccesoEspecifico''=IsNull(AF.IDAccesoEspecifico, ''0''),

--Menu
''IDMenu''=IsNull(A.IDMenu, ''0''),
''Tag''=IsNull(M.Tag, ''---''),
''Form''=IsNull(M.Tag, ''---'')



From Perfil P 
Left Outer JOin AccesoEspecificoPerfil AF On P.ID=AF.IDPerfil
Left Outer Join AccesoEspecifico A On AF.IDAccesoEspecifico=A.ID And AF.IDMenu=A.IDMenu
Left Outer Join Menu M On A.IDMenu=M.Codigo


'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetalleEfectivo]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VDetalleEfectivo]
As
Select 
DE.IDTransaccion,
DE.ID,
DE.IDTipoComprobante,
''TipoComprobante''=TC.Codigo,
DE.Comprobante,
DE.Fecha,
''Fec''=CONVERT(varchar(50), DE.Fecha, 6),

--Las descripciones en ''TIPO'' estan relacionadas al programa
--Si se quiere cambiar estas descripciones (CREDITO-CONTADO-GASTOS) se deberian cambiar tambien
--en el software (programacion)
''Tipo''=(Case When (DE.VentasCredito) > 0 Then ''CREDITO'' Else (Case When (DE.VentasContado) > 0 Then ''CONTADO'' Else (Case When (DE.Gastos) > 0 Then ''GASTOS'' Else ''---'' End) End) End),
''Importe''=(Case When (DE.VentasCredito) > 0 Then DE.VentasCredito Else (Case When (DE.VentasContado) > 0 Then DE.VentasContado Else (Case When (DE.Gastos) > 0 Then DE.Gastos Else 0 End) End) End),
DE.VentasCredito,
DE.VentasContado,
DE.Gastos,
DE.IDMoneda,
''Moneda''=M.Referencia,
DE.ImporteMoneda,
DE.Cotizacion,
DE.Observacion

From DetalleEfectivo DE
Join TipoComprobante TC On DE.IDTipoComprobante=TC.ID
Join Moneda M On DE.IDMoneda=M.ID
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCiudad]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VCiudad]
As
Select 
*
From Ciudad'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VLineaSubLinea]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VLineaSubLinea]
As
Select
LSL.*,
''TipoProducto''=TP.Descripcion,
''Linea''=L.Descripcion,
''SubLinea''=SL.Descripcion

From LineaSubLinea LSL
Join TipoProducto TP On LSL.IDTipoProducto=TP.ID
Join Linea L On LSL.IDLinea=L.ID
JOin SubLinea SL On LSL.IDSubLinea=SL.ID
'







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sucursal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Sucursal](
	[ID] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Codigo] [varchar](5) NULL,
	[IDPais] [tinyint] NULL,
	[IDDepartamento] [tinyint] NULL,
	[IDCiudad] [smallint] NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](20) NULL,
	[Referencia] [varchar](5) NULL,
	[Estado] [bit] NULL,
	[CodigoDistribuidor] [varchar](50) NULL,
	[CuentaContable] [varchar](50) NULL,
 CONSTRAINT [PK_Sucursal] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SubLineaSubLinea2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SubLineaSubLinea2](
	[IDTipoProducto] [tinyint] NOT NULL,
	[IDLinea] [smallint] NOT NULL,
	[IDSubLinea] [smallint] NOT NULL,
	[IDSubLinea2] [smallint] NOT NULL,
 CONSTRAINT [PK_SubLineaSubLinea2] PRIMARY KEY CLUSTERED 
(
	[IDTipoProducto] ASC,
	[IDLinea] ASC,
	[IDSubLinea] ASC,
	[IDSubLinea2] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Proveedor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Proveedor](
	[ID] [int] NOT NULL,
	[RazonSocial] [varchar](100) NOT NULL,
	[RUC] [varchar](15) NOT NULL,
	[Referencia] [varchar](50) NULL,
	[NombreFantasia] [varchar](100) NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](20) NULL,
	[Estado] [bit] NOT NULL,
	[Contado] [bit] NULL,
	[Credito] [bit] NULL,
	[PlazoCredito] [tinyint] NULL,
	[IDPais] [tinyint] NULL,
	[IDDepartamento] [tinyint] NULL,
	[IDCiudad] [smallint] NULL,
	[IDBarrio] [smallint] NULL,
	[IDSucursal] [tinyint] NULL,
	[IDTipoProveedor] [tinyint] NULL,
	[IDMoneda] [tinyint] NULL,
	[FechaAlta] [date] NULL,
	[IDUsuarioAlta] [smallint] NULL,
	[FechaUltimaCompra] [date] NULL,
	[PaginaWeb] [varchar](50) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[IDCuentaContableCompra] [int] NULL,
	[IDCuentaContableVenta] [smallint] NULL,
	[Deuda] [money] NULL,
	[Retentor] [bit] NULL,
	[TipoCompra] [char](10) NULL,
	[Exportador] [bit] NULL,
	[SujetoRetencion] [bit] NULL,
	[CuentaContableCompra] [varchar](50) NULL,
	[CuentaContableVenta] [varchar](50) NULL,
 CONSTRAINT [PK_Proveedor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PuntoExpedicion](
	[ID] [int] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[Referencia] [varchar](5) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Timbrado] [varchar](50) NOT NULL,
	[Vencimiento] [date] NOT NULL,
	[NumeracionDesde] [numeric](18, 0) NOT NULL,
	[NumeracionHasta] [numeric](18, 0) NOT NULL,
	[CantidadPorTalonario] [smallint] NOT NULL,
	[UltimoNumero] [numeric](18, 0) NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_PuntoVenta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SubLinea2Marca]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SubLinea2Marca](
	[IDTipoProducto] [tinyint] NOT NULL,
	[IDLinea] [smallint] NOT NULL,
	[IDSubLinea] [smallint] NOT NULL,
	[IDSubLinea2] [smallint] NOT NULL,
	[IDMarca] [tinyint] NOT NULL,
 CONSTRAINT [PK_SubLinea2Marca] PRIMARY KEY CLUSTERED 
(
	[IDTipoProducto] ASC,
	[IDLinea] ASC,
	[IDSubLinea] ASC,
	[IDSubLinea2] ASC,
	[IDMarca] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ListaPrecio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ListaPrecio](
	[ID] [int] NOT NULL,
	[IDSucursal] [tinyint] NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NULL,
	[Referencia] [varchar](10) NULL,
 CONSTRAINT [PK_ListaPrecio] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Division]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Division](
	[ID] [smallint] NOT NULL,
	[IDProveedor] [int] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Deposito]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Deposito](
	[ID] [tinyint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Deposito] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Terminal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Terminal](
	[ID] [tinyint] NOT NULL,
	[IDSucursal] [tinyint] NULL,
	[IDDeposito] [tinyint] NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Impresora] [varchar](50) NULL,
	[CodigoActivacion] [varchar](50) NULL,
	[Activado] [bit] NULL,
	[UltimoAcceso] [datetime] NULL,
 CONSTRAINT [PK_Terminal] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VLineaSubLinea2]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VLineaSubLinea2]
As
Select
SLSL.*,
''SubLinea''=SL.Descripcion,
''SubLinea2''=SL2.Descripcion
From
SubLineaSubLinea2 SLSL
Join SubLinea SL On SLSL.IDSubLinea=SL.ID
Join SubLinea2 SL2 On SLSL.IDSubLinea2=SL2.ID'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCF]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VCF]
As
Select 
CF.IDOperacion,
''Operacion''=O.Descripcion,
CF.ID,
CF.IDSucursal,
''Sucursal''=S.Descripcion,
''SucursalCodigo''=S.Codigo,
CF.IDMoneda,
''Moneda''=M.Descripcion,
''ReferenciaMoneda''=M.Referencia,
CF.IDTipoComprobante,
''TipoComprobante''=TC.Descripcion,
''CodigoComprobante''=TC.Codigo,

--Cuenta
CF.CuentaContable,
''Denominacion''=CC.Descripcion,
''IDCuentaContable''=CC.ID,
''Codigo''=CC.Codigo,
''Cuenta''=CC.Descripcion,
''Alias''=CC.Alias,

CF.Debe,
CF.Haber,
''Debe/Haber''=(Case When (CF.Debe) = ''True'' Then ''DEBE'' Else (Case When (CF.Haber) = ''True'' Then ''HABER'' Else ''---'' End) End),
CF.Descripcion,
CF.Orden,

''CajaChica''=isnull(CC.CajaChica,''False'') 

From CF 
Join Operacion O On CF.IDOperacion=O.ID
Join Sucursal S On CF.IDSucursal=S.ID
Join Moneda M On CF.IDMoneda=M.ID
Left Outer Join TipoComprobante TC On CF.IDTipoComprobante=TC.Id
Join CuentaContable CC On CF.CuentaContable=CC.Codigo 
Join PlanCuenta PC On CC.IDPlanCuenta=PC.ID

Where PC.Titular=''True''









'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCuentaContable]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VCuentaContable]
As
Select 
CC.ID,
''IDCuentaContable''=CC.ID,
CC.IDPlanCuenta,
''PlanCuenta''=PC.Descripcion,
''PlanCuentaTitular''=PC.Titular,
''Resolucion173''=PC.Resolucion173,
CC.Codigo,
CC.Descripcion,
''Cuenta''=CC.Codigo + '' - '' + CC.Descripcion,
''CuentaContable''=CC.Codigo + '' - '' + CC.Descripcion,
CC.Alias,
CC.Categoria,
CC.Imputable,
''Tipo''=(Select Case When(Select TOP (1) CC2.ID From CuentaContable CC2 Where CC2.IDPadre=CC.ID) Is Null Then ''IMPUTABLE'' Else ''TOTALIZADOR'' End),
''IDPadre''=IsNull(CC.IDPadre, 0),
''Padre''=(ISNULL((Select CC3.Codigo + '' - '' + CC3.Descripcion From CuentaContable CC3 Where CC3.ID=CC.IDPadre), ''---'')),
''CodigoPadre''=(ISNULL((Select Convert(varchar(50), CC3.Codigo) From CuentaContable CC3 Where CC3.ID=CC.IDPadre), ''---'')),
''Estado''=Case When (CC.Estado) = ''True'' Then ''ACTIVO'' Else ''INACTIVO'' End,
''Activo''=CC.Estado,
''Sucursal''=IsNull(CC.Sucursal, ''False''),
''IDSucursal''=IsNull(CC.IDSucursal, 0),
''Suc''=ISNULL((Select S.Descripcion From Sucursal S Where S.ID=CC.IDSucursal), ''---'')

From CuentaContable CC
Join PlanCuenta PC On CC.IDPlanCuenta=PC.ID

--Esto es necesario para el programa, TITULAR = ''True''
--Si en parte del codigo falla, crear otra vista para tal caso... No tocar este.
Where PC.Titular = ''True''













'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VSucursal]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VSucursal]
As
Select 
S.ID, 
S.Descripcion, 
S.Codigo,
S.IDPais, 
''Pais''=ISNULL(P.Descripcion, ''''),
S.IDDepartamento, 
''Departamento''=ISNULL(D.Descripcion, ''''),
S.IDCiudad, 
''Ciudad''=ISNULL(C.Descripcion, ''''),
''CodigoCiudad''=C.Codigo,
S.Direccion, 
S.Telefono, 
S.Referencia, 
''REF''=S.Referencia, 
S.Estado,
S.CodigoDistribuidor,
S.CuentaContable
  
From 
Sucursal S
Left Outer Join Pais P On S.IDPais=P.ID
Left Outer Join Departamento D On S.IDDepartamento=D.ID
Left Outer Join Ciudad C On S.IDCiudad=C.ID





'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VSubLineaSubLinea2]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VSubLineaSubLinea2]
As
Select
SLSL2.*,
''TipoProducto''=TP.Descripcion,
''Linea''=L.Descripcion,
''SubLinea''=SL.Descripcion,
''SubLinea2''=SL2.Descripcion

From SubLineaSubLinea2 SLSL2
Join TipoProducto TP On SLSL2.IDTipoProducto=TP.ID
Join Linea L On SLSL2.IDLinea=L.ID
JOin SubLinea SL On SLSL2.IDSubLinea=SL.ID
JOin SubLinea2 SL2 On SLSL2.IDSubLinea2=SL2.ID
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VVendedor]'))
EXEC dbo.sp_executesql @statement = N'


CREATE View [dbo].[VVendedor]
As
Select 
V.*,

--Sucursal
''Sucursal''=S.Descripcion,
''CodigoSucursal''=S.Codigo,

--Deposito
''Deposito''=ISNULL(D.Descripcion, ''---'')

From Vendedor V 
Join Sucursal S on V.IDSucursal=S.ID
LEFT OUTER JOIN Deposito D ON V.IDDeposito=D.ID




'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VTerminal]'))
EXEC dbo.sp_executesql @statement = N'


CREATE View [dbo].[VTerminal]
As
Select 
T.ID,
T.Descripcion,
T.Impresora,
T.CodigoActivacion,

--Sucursal
T.IDSucursal,
''Sucursal''=S.Descripcion,
''ReferenciaSucursal''=S.Referencia, 
''CodigoSucursal''=S.Codigo, 

--Deposito
T.IDDeposito,
''Deposito''=D.Descripcion,

T.Activado

From Terminal T
Join Sucursal S On T.IDSucursal=S.ID
Join Deposito D On T.IDDeposito=D.ID


'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VSubLinea2Marca]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VSubLinea2Marca]
As
Select
SL2M.*,
''TipoProducto''=TP.Descripcion,
''Linea''=L.Descripcion,
''SubLinea''=SL.Descripcion,
''SubLinea2''=SL2.Descripcion,
''Marca''=M.Descripcion

From SubLinea2Marca SL2M
Join TipoProducto TP On SL2M.IDTipoProducto=TP.ID
Join Linea L On SL2M.IDLinea=L.ID
JOin SubLinea SL On SL2M.IDSubLinea=SL.ID
JOin SubLinea2 SL2 On SL2M.IDSubLinea2=SL2.ID
JOin Marca M On SL2M.IDMarca=M.ID
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCiudadSucursal]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VCiudadSucursal]
As
Select
Distinct IDCiudad,
CodigoCiudad,
''IDSucursal''=ID
From VSucursal'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDeposito]'))
EXEC dbo.sp_executesql @statement = N'



CREATE View [dbo].[VDeposito]

As

Select
D.ID,
''Deposito''=D.Descripcion,
D.IDSucursal,
''Sucursal''=S.Descripcion,
''Suc-Dep''=S.Descripcion + '' - '' + D.Descripcion,
''Estado''=Case When (D.Estado) = ''True'' Then ''OK'' Else ''-'' End,
''Activo''=D.Estado,
''IDCiudad''=S.IDCiudad,
''Ciudad''=C.Descripcion,
''CodigoCiudad''=C.Codigo,
''SucDeposito''=S.Codigo+'' - '' + D.Descripcion


From Deposito D
Join Sucursal S On D.IDSucursal=S.ID
Left Outer Join Ciudad C On S.IDCiudad=C.ID



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCFVentaVenta]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VCFVentaVenta]
As
Select
V.*
From VCF V
Join CFVentaVenta CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCFVentaMercaderia]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VCFVentaMercaderia]
As
Select
V.*
From VCF V
Join CFVentaMercaderia CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCFVentaImpuesto]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VCFVentaImpuesto]
As
Select
V.*,
CFV.IDImpuesto,
''Impuesto''=I.Descripcion,
''Referencia''=I.Referencia

From VCF V
Join CFVentaImpuesto CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join Impuesto I On CFV.IDImpuesto=I.ID

'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCFVentaDescuento]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VCFVentaDescuento]
As
Select
V.*,
CFV.IDTipoDescuento,
''TipoDescuento''=TD.Descripcion,
''Tipo''=TD.Codigo
From VCF V
Join CFVentaDescuento CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join TipoDescuento TD On CFV.IDTipoDescuento=TD.ID



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCFVentaCostoMercaderia]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VCFVentaCostoMercaderia]
As
Select
V.*
From VCF V
Join CFVentaCostoMercaderia CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID




'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCFVentaCliente]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VCFVentaCliente]
As
Select
V.*,
CFV.Contado,
CFV.Credito

From VCF V
Join CFVentaCliente CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID








'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCFVenta]'))
EXEC dbo.sp_executesql @statement = N'





CREATE View [dbo].[VCFVenta]
As
Select
V.*,
CFV.Credito,
CFV.Contado,
''CRE/CONT''=(Case When(CFV.Credito) = ''True'' Then ''CREDITO'' Else (Case When(CFV.Contado) = ''True'' Then ''CONTADO'' Else ''---'' End) End),
CFV.BuscarEnCliente,
CFV.BuscarEnProducto,

--Tipo Cuenta Fija
CFV.IDTipoCuentaFija,
''TipoCuentaFija''=TC.Tipo,
''CuentaFija''=TC.Descripcion,
TC.IDImpuesto,

--Descuento
CFV.IDTipoDescuento,
''TipoDescuento''=(ISNULL(TD.Descripcion, '''')),

--Tipo de Cuenta
TC.Campo,
TC.IncluirDescuento,
TC.IncluirImpuesto

From VCF V
Join CFVenta CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join VTipoCuentaFija TC On CFV.IDTipoCuentaFija=TC.ID
Left Outer Join TipoDescuento TD On CFV.IDTipoDescuento=TD.ID




'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VProveedor]'))
EXEC dbo.sp_executesql @statement = N'





CREATE View [dbo].[VProveedor]

As

Select

--Comerciales
P.ID,
P.RazonSocial,
P.RUC,
P.Referencia,
P.NombreFantasia,
P.Direccion,
P.Telefono,
''TipoCompra''=IsNull(P.TipoCompra, ''''),
''RazonSocialReferencia''=P.RazonSocial + '' ('' + P.Referencia + '')'',

--De configuracion
''Estado''=P.Estado,
P.IDMoneda,
''Moneda''=(Select IsNull((Select V.Descripcion From Moneda V Where V.ID=P.IDMoneda ), '''')),
P.IDTipoProveedor,
''TipoProveedor''=(Select IsNull((Select V.Descripcion From TipoProveedor V Where V.ID=P.IDTipoProveedor), '''')),
P.Contado,
P.Credito,
''Condicion''=(Case When (P.Contado) = ''True'' Then ''CONTADO'' Else (Case When (P.Credito) = ''True'' Then ''CREDITO'' Else ''---'' End) End),
P.PlazoCredito,


P.IDCuentaContableCompra,
''CuentaCompra''=(Select IsNull((Select V.Descripcion From VCuentaContable V Where V.ID=P.IDCuentaContableCompra), '''')),
''CodigoContableCompra''=P.CuentaContableCompra,
''CuentaContableCompra''=P.CuentaContableCompra,

P.IDCuentaContableVenta,
''CuentaVenta''=(Select IsNull((Select V.Descripcion From VCuentaContable V Where V.ID=P.IDCuentaContableVenta), '''')),
''CodigoContableVenta''=P.CuentaContableVenta,
''CuentaContableVenta''=P.CuentaContableVenta,

--Localizacion
P.IDPais,
''Pais''=(Select IsNull((Select V.Descripcion From Pais V Where V.ID=P.IDPais), '''')),
P.IDDepartamento,
''Departamento''=(Select IsNull((Select V.Descripcion From Departamento V Where V.ID=P.IDDepartamento), '''')),
P.IDCiudad,
''Ciudad''=(Select IsNull((Select V.Descripcion From Ciudad V Where V.ID=P.IDCiudad), '''')),
P.IDBarrio,
''Barrio''=(Select IsNull((Select V.Descripcion From Barrio V Where V.ID=P.IDBarrio), '''')),

--Referencias
P.IDSucursal,
''Sucursal''=(Select IsNull((Select V.Descripcion From Sucursal V Where V.ID=P.IDSucursal), '''')),

--Estadisticos
''FechaUltimaCompra''=convert(date, P.FechaUltimaCompra),
''FechaUltimoPago''='''',

--Datos Adicionales
P.PaginaWeb,
P.Email,
P.Fax,

--Retentor
P.Retentor,
P.Exportador,
P.SujetoRetencion

From

Proveedor as P


















'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VListaPrecio]'))
EXEC dbo.sp_executesql @statement = N'--USE [SAIN]
--

--/****** Object:  View [dbo].[VListaPrecio]    Script Date: 08/05/2013 17:19:10 ******/
--
--

--
--


CREATE View [dbo].[VListaPrecio]
As

Select 
LP.*,
''Sucursal''= IsNull(S.Descripcion, '''')
From ListaPrecio LP
Left Outer Join Sucursal S On LP.IDSucursal=S.ID

'





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transaccion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Transaccion](
	[ID] [numeric](18, 0) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IDUsuario] [smallint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[IDTerminal] [tinyint] NOT NULL,
	[IDOperacion] [tinyint] NOT NULL,
 CONSTRAINT [PK_Transaccion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cliente]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Cliente](
	[ID] [int] NOT NULL,
	[RazonSocial] [varchar](100) NOT NULL,
	[RUC] [varchar](12) NOT NULL,
	[CI] [bit] NULL,
	[Referencia] [varchar](50) NOT NULL,
	[NombreFantasia] [varchar](100) NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](20) NULL,
	[IDEstado] [tinyint] NULL,
	[IDPais] [tinyint] NULL,
	[IDDepartamento] [tinyint] NULL,
	[IDCiudad] [smallint] NULL,
	[IDBarrio] [smallint] NULL,
	[IDZonaVenta] [tinyint] NULL,
	[IDListaPrecio] [int] NULL,
	[IDTipoCliente] [tinyint] NULL,
	[IDCuentaContable] [int] NULL,
	[CodigoCuentaContable] [varchar](50) NULL,
	[Contado] [bit] NULL,
	[Credito] [bit] NULL,
	[IDMoneda] [tinyint] NULL,
	[FechaAlta] [date] NULL,
	[IDUsuarioAlta] [smallint] NULL,
	[FechaModificacion] [date] NULL,
	[IDUsuarioModificacion] [smallint] NULL,
	[FechaUltimaCompra] [date] NULL,
	[FechaUltimaCobranza] [date] NULL,
	[Aniversario] [date] NULL,
	[IDSucursal] [tinyint] NULL,
	[IDPromotor] [tinyint] NULL,
	[IDVendedor] [tinyint] NULL,
	[IDCobrador] [tinyint] NULL,
	[IDDistribuidor] [tinyint] NULL,
	[LimiteCredito] [money] NULL,
	[Deuda] [money] NULL,
	[Descuento] [numeric](2, 0) NULL,
	[PlazoCredito] [tinyint] NULL,
	[PlazoCobro] [tinyint] NULL,
	[PlazoChequeDiferido] [tinyint] NULL,
	[IVAExento] [bit] NULL,
	[PaginaWeb] [varchar](50) NULL,
	[Fax] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[Horario] [varchar](50) NULL,
	[Latitud] [varchar](50) NULL,
	[Longitud] [varchar](50) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Producto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Producto](
	[ID] [int] NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Referencia] [varchar](30) NULL,
	[CodigoBarra] [varchar](30) NULL,
	[IDClasificacion] [smallint] NULL,
	[IDTipoProducto] [tinyint] NULL,
	[IDLinea] [smallint] NULL,
	[IDSubLinea] [smallint] NULL,
	[IDSubLinea2] [smallint] NULL,
	[IDMarca] [tinyint] NULL,
	[IDPresentacion] [smallint] NULL,
	[IDCategoria] [tinyint] NULL,
	[IDProveedor] [int] NULL,
	[IDDivision] [smallint] NULL,
	[IDProcedencia] [tinyint] NULL,
	[IDUnidadMedida] [tinyint] NULL,
	[UnidadPorCaja] [smallint] NULL,
	[IDUnidadMedidaConvertir] [tinyint] NULL,
	[UnidadConvertir] [decimal](10, 3) NULL,
	[Peso] [varchar](50) NULL,
	[Volumen] [varchar](15) NULL,
	[IDImpuesto] [tinyint] NOT NULL,
	[Estado] [bit] NOT NULL,
	[ControlarExistencia] [bit] NOT NULL,
	[IDCuentaContableCompra] [int] NULL,
	[CuentaContableCompra] [varchar](50) NULL,
	[IDCuentaContableVenta] [int] NULL,
	[CuentaContableVenta] [varchar](50) NULL,
	[UltimaEntrada] [datetime] NULL,
	[CantidadEntrada] [decimal](10, 2) NULL,
	[UltimaSalida] [datetime] NULL,
	[CantidadSalida] [decimal](10, 2) NULL,
	[UltimoCosto] [money] NULL,
	[CostoSinIVA] [money] NULL,
	[IDMonedaUltimoCosto] [tinyint] NULL,
	[TipoCambio] [money] NULL,
	[CostoCG] [money] NULL,
	[CostoPromedio] [money] NULL,
	[ExistenciaGeneral] [decimal](10, 2) NULL,
	[EnEspera] [bit] NULL,
	[Reemplazado] [bit] NULL,
	[CuentaContableCosto] [varchar](50) NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarcaPresentacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarcaPresentacion](
	[IDTipoProducto] [tinyint] NOT NULL,
	[IDLinea] [smallint] NOT NULL,
	[IDSubLinea] [smallint] NOT NULL,
	[IDSubLinea2] [smallint] NOT NULL,
	[IDMarca] [tinyint] NOT NULL,
	[IDPresentacion] [smallint] NOT NULL,
 CONSTRAINT [PK_MarcaPersentacion] PRIMARY KEY CLUSTERED 
(
	[IDTipoProducto] ASC,
	[IDLinea] ASC,
	[IDSubLinea] ASC,
	[IDSubLinea2] ASC,
	[IDMarca] ASC,
	[IDPresentacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FNroComprobante]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FNroComprobante]
(

	--Entrada
	@IDPuntoExpedicion int
)

Returns numeric(18,0)

As

Begin

	--Variables
	Declare @vRetorno numeric(18, 0)
	Declare @vUltimoNumero numeric(18, 0)
	Declare @vNumeracionDesde numeric(18,0)
	
	--Hayar el ultimo numero
	Set @vUltimoNumero = IsNull((Select PE.UltimoNumero From PuntoExpedicion PE Where PE.ID=@IDPuntoExpedicion), 0)
	Set @vNumeracionDesde = (Select PE.NumeracionDesde From PuntoExpedicion PE Where PE.ID=@IDPuntoExpedicion)
	
	--Controlar 
	If @vUltimoNumero < @vNumeracionDesde Begin
		Set @vUltimoNumero = @vNumeracionDesde
	End Else Begin
		Set @vUltimoNumero = @vUltimoNumero + 1
	End
	
	Set @vRetorno = @vUltimoNumero
		
	Return @vRetorno
End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpPuntoExpedicion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpPuntoExpedicion]

	--Entrada
	@ID tinyint,
	@IDTipoComprobante smallint,
	@IDSucursal tinyint,
	@Referencia varchar(5),
	@Descripcion varchar(50),
	@Timbrado varchar(50),
	@Vencimiento date,
	@NumeracionDesde numeric(18,0),
	@NumeracionHasta numeric(18,0),
	@CantidadPorTalonario smallint,
	@Estado bit = ''True'',
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Obtenemos el nuevo ID
		declare @vID int
		set @vID = (Select IsNull((Max(ID)+1), 1) From PuntoExpedicion)

		--Insertamos
		Insert Into PuntoExpedicion(ID, IDTipoComprobante, IDSucursal, Referencia, Descripcion, Timbrado, Vencimiento, NumeracionDesde, NumeracionHasta, CantidadPorTalonario, Estado)
		Values(@vID, @IDTipoComprobante, @IDSucursal, @Referencia, @Descripcion, @Timbrado, @Vencimiento, @NumeracionDesde, @NumeracionHasta, @CantidadPorTalonario, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''PUNTO DE EXPEDICION'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From PuntoExpedicion Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update PuntoExpedicion Set	IDTipoComprobante=@IDTipoComprobante,
									IDSucursal=@IDSucursal,
									Referencia=@Referencia,
									Descripcion=@Descripcion,
									Timbrado=@Timbrado,
									Vencimiento=@Vencimiento,
									NumeracionDesde=@NumeracionDesde,
									NumeracionHasta=@NumeracionHasta,
									CantidadPorTalonario=@CantidadPorTalonario,
									Estado=@Estado
									
		Where ID=@ID
		
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''PUNTO DE EXPEDICION'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From PuntoExpedicion Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Relacion con terminal transaccion
		if exists(Select * From TerminalPuntoExpedicion Where IDPuntoExpedicion=@ID) begin
			set @Mensaje = ''El registro tiene terminales relacionadas! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From PuntoExpedicion
		Where ID=@ID
		
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''PUNTO DE EXPEDICION'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End



' 
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NotaCredito]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NotaCredito](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDPuntoExpedicion] [int] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[NroComprobante] [int] NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[Fecha] [date] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[Cotizacion] [money] NULL,
	[Observacion] [varchar](500) NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalDescuento] [money] NOT NULL,
	[Saldo] [money] NOT NULL,
	[Anulado] [bit] NOT NULL,
	[FechaAnulado] [date] NULL,
	[IDUsuarioAnulado] [smallint] NULL,
	[Procesado] [bit] NULL,
	[Devolucion] [bit] NULL,
	[Descuento] [bit] NULL,
	[Aplicar] [bit] NULL,
	[ConComprobantes] [bit] NULL,
 CONSTRAINT [PK_NotaCredito] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Movimiento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Movimiento](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[Numero] [int] NOT NULL,
	[Fecha] [date] NOT NULL,
	[IDTipoOperacion] [tinyint] NOT NULL,
	[IDMotivo] [tinyint] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[NroComprobante] [varchar](50) NULL,
	[IDDepositoSalida] [tinyint] NULL,
	[IDDepositoEntrada] [tinyint] NULL,
	[Observacion] [varchar](200) NULL,
	[Autorizacion] [varchar](50) NOT NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[Anulado] [bit] NOT NULL,
 CONSTRAINT [PK_Movimiento] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pedido]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Pedido](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[Numero] [int] NULL,
	[IDCliente] [int] NOT NULL,
	[IDSucursalCliente] [tinyint] NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](50) NULL,
	[Fecha] [datetime] NOT NULL,
	[FechaFacturar] [date] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[IDListaPrecio] [int] NULL,
	[IDVendedor] [tinyint] NULL,
	[IDMoneda] [tinyint] NULL,
	[Cotizacion] [money] NULL,
	[Observacion] [varchar](500) NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalDescuento] [money] NULL,
	[Facturado] [bit] NOT NULL,
	[EsVentaSucursal] [bit] NULL,
	[Anulado] [bit] NOT NULL,
	[FechaAnulado] [datetime] NULL,
	[IDUsuarioAnulado] [smallint] NULL,
	[Procesado] [bit] NULL,
	[Vendedor] [bit] NULL,
 CONSTRAINT [PK_Pedido] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpBarrio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpBarrio]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Barrio Where Descripcion=@Descripcion) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDBarrio tinyint
		set @vIDBarrio = (Select IsNull((Max(ID)+1), 1) From Barrio)

		--Insertamos
		Insert Into Barrio(ID, Descripcion, Estado)
		Values(@vIDBarrio, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''BARRIO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From Barrio Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Barrio Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update Barrio Set Descripcion=@Descripcion, 
						Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''BARRIO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From Barrio Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
				
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDBarrio=@ID) begin
			set @Mensaje = ''El registro tiene clientes asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Barrio 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''BARRIO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpCategoria]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpCategoria]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
						
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Categoria Where Descripcion=@Descripcion) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDCategoria tinyint
		set @vIDCategoria = (Select IsNull((Max(ID)+1), 1) From Categoria)

		--Insertamos
		Insert Into Categoria(ID, Descripcion, Estado)
		Values(@vIDCategoria, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TIPO DE COMPROBANTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
	
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		If not exists(Select * From Categoria Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Categoria Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update Categoria Set Descripcion=@Descripcion,
						 Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TIPO DE COMPROBANTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From Categoria Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Producto Where IDCategoria=@ID) begin
			set @Mensaje = ''El registro tiene productos asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Categoria 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TIPO DE COMPROBANTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpInicializarTerminal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpInicializarTerminal]

	--Entrada
	@Terminal varchar(50),
	@CodigoActivacion varchar(500),
	@Version varchar(100)
		
As

Begin

	--Varialbes
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDTerminal int
	Declare @vIDSucursal tinyint
	Declare @vIDDeposito smallint
		
	Set @vMensaje = ''No funciona.''
	Set @vProcesado = ''false''
	Set @vIDTerminal = 0
	Set @vIDSucursal = 0
	Set @vIDDeposito = 0
	
	--Verificar la version del sistema
	If (Select Top(1) VersionSistema From Configuraciones) != @Version Begin
	
		Set @vMensaje = ''La version del sistema que esta utilizando no es la correcta! Version actual: '' + CONVERT(varchar(200), (Select Top(1) VersionSistema From Configuraciones ))
		Set @vProcesado = ''false''
		Set @vIDTerminal = 0
		
		GoTo Salir
		
	End
		
	--Verificar que exista la terminal
	If Not Exists(Select * From Terminal Where CodigoActivacion=@CodigoActivacion) Begin
	
		Set @vIDTerminal = (Select ISNULL(Max(ID) + 1, 1) From Terminal)
		
		Insert into Terminal(ID, Descripcion, CodigoActivacion, Activado, UltimoAcceso)
		Values(@vIDTerminal, @Terminal, @CodigoActivacion, ''True'', GETDATE())
		
		--Si es la primera terminal en entrar, darle acceso
		If (Select COUNT(*) From Terminal) = 1 Begin
			Set @vMensaje = ''Ingreso al sistema por primera vez!''
			Set @vProcesado = ''True''
			GoTo Salir
		End
		
		Set @vMensaje = ''Debe solicitar acceso desde esta terminal al administrador del sistema!''
		Set @vProcesado = ''false''
		Set @vIDTerminal = 0
		
		GoTo Salir
		
	End
	
	--Obtener el ID de la Terminal
	Set @vIDTerminal = (Select Top(1) ID From Terminal Where CodigoActivacion=@CodigoActivacion)
	
	--Verificar la terminal este activa
	If (Select IsNull(Activado, ''False'') From Terminal Where ID=@vIDTerminal) = ''False'' Begin
	
		Set @vMensaje = ''Debe solicitar acceso desde esta terminal al administrador del sistema!''
		Set @vProcesado = ''false''
				
		GoTo Salir
		
	End
	
	Set @vMensaje = ''Acceso correcto!''
	Set @vProcesado = ''True''
	Goto Salir
			
Salir:
	print @vMensaje
	print @vIDTerminal
	Select T.*, ''Mensaje''=@vMensaje, ''Procesado''=@vProcesado From VTerminal T Where ID=@vIDTerminal
End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpUsuario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[SpUsuario]

	--Entrada
	@ID tinyint,
	@Nombre varchar(50),
	@Usuario varchar(50),
	@Password varchar(50) = ''12345'',
	@IDPerfil tinyint,
	@Identificador varchar(3),
	@Estado bit,
	@EsVendedor bit = ''False'',
	@IDVendedor int = NULL,
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Nombre=@Nombre) begin
			set @Mensaje = ''El Nombre ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		if exists(Select * From Usuario Where Usuario=@Usuario) begin
			set @Mensaje = ''El Usuario ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que el Identi ya existe
		if exists(Select * From Usuario Where Identificador=@Identificador) begin
			set @Mensaje = ''El identificador ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDUsuario tinyint
		set @vIDUsuario = (Select IsNull((Max(ID)+1), 1) From Usuario)

		Set @Password = Convert(varchar(300), HASHBYTES(''Sha1'', ''12345''))
						
		--Insertamos
		Insert Into Usuario(ID, Nombre, Usuario, [Password], Identificador, IDPerfil, Estado, EsVendedor, IDVendedor)
		Values(@vIDUsuario, @Nombre, @Usuario, @Password, @Identificador, @IDPerfil, @Estado, @EsVendedor, @IDVendedor)		
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		If not exists(Select * From Usuario Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Nombre=@Nombre And ID!=@ID) begin
			set @Mensaje = ''El Nombre ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		if exists(Select * From Usuario Where Usuario=@Usuario And ID!=@ID) begin
			set @Mensaje = ''El Usuario ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la Nombre ya existe
		if exists(Select * From Usuario Where Identificador=@Identificador And ID!=@ID) begin
			set @Mensaje = ''El identificador ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update Usuario Set Nombre=@Nombre,
						 Estado = @Estado,
						 Usuario=@Usuario, 
						 IDPerfil=@IDPerfil,
						 Identificador=@Identificador,
						 EsVendedor=@EsVendedor,
						 IDVendedor=@IDVendedor
		Where ID=@ID
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From Usuario Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Transaccion Where IDUsuario=@ID) begin
			set @Mensaje = ''El registro tiene operaciones realizadas! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Usuario 
		Where ID=@ID
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End



' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpTransaccion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[SpTransaccion]

	--CARLOS 21-02-2014
	
	--Entrada
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
	@IDOperacion int,

	--Beta, sacar si algo sale mal
	@MostrarTransaccion bit = ''True'',
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccion numeric(18,0) output
As

Begin

	--Validar
	--Usuario
	
	--Sucursal
	
	---Deposito
	
	--Terminal
	
	--Obtener el nuevo ID
	Set @IDTransaccion = (Select ISNULL((Select MAX(ID + 1) From Transaccion),1))
	
	--Todo OK, Insertamos
	Insert Into Transaccion(ID, Fecha, IDUsuario, IDSucursal, IDDeposito, IDTerminal, IDOperacion)
	values(@IDTransaccion, GETDATE(), @IDUsuario, @IDSucursal, @IDDeposito, @IDTerminal, @IDOperacion)
	
	--Auditoria
	Declare @vTabla varchar(50)
	Set @vTabla = (Select Descripcion From Operacion Where ID=@IDOperacion)
	
	Exec SpLogSuceso @Operacion=''INS'', @Tabla=@vTabla, @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @IDTransaccion=@IDTransaccion
	
	If @MostrarTransaccion = ''True'' Begin	
		Select ''IDTransaccion''=@IDTransaccion
	End
			
	Set @Mensaje = ''Registro cargado!''
	Set @Procesado = ''True''
	return @@rowcount

End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpTipoCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpTipoCliente]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From TipoCliente Where Descripcion=@Descripcion) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDTipoCliente tinyint
		set @vIDTipoCliente = (Select IsNull((Max(ID)+1), 1) From TipoCliente)

		--Insertamos
		Insert Into TipoCliente(ID, Descripcion, Estado)
		Values(@vIDTipoCliente, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TIPO DE CLIENTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From TipoCliente Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From TipoCliente Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update TipoCliente Set Descripcion=@Descripcion,
						Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TIPO DE CLIENTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From TipoCliente Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDTipoCliente=@ID) begin
			set @Mensaje = ''El registro tiene clientes asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From TipoCliente 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TIPO DE CLIENTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpTerminal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[SpTerminal]

	--Entrada
	@ID tinyint,
	@IDSucursal tinyint = NULL,
	@IDDeposito tinyint,
	@Descripcion varchar(50),
	@CodigoActivacion varchar(300) = NULL,
	@Impresora varchar(50) = NULL,
	
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @vIDSucursal tinyint
	
	If @IDSucursal Is Null Begin
		Set @vIDSucursal=(Select IDSucursal From Deposito Where ID = @IDDeposito)		
		Set @IDSucursal = @vIDSucursal
	End
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Terminal Where Descripcion=@Descripcion) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
				
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From Terminal)

		--Insertamos
		Insert Into Terminal(ID, IDSucursal, IDDeposito, Descripcion, Impresora, Activado, CodigoActivacion)
		Values(@vID, @IDSucursal, @IDDeposito, @Descripcion, @Impresora, ''True'', @CodigoActivacion)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TERMINAL'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From Terminal Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Terminal Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update Terminal Set IDSucursal=@IDSucursal, 
							IDDeposito=@IDDeposito, 
							Descripcion=@Descripcion, 
							Impresora=@Impresora
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TERMINAL'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From Terminal Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con Transaccion
		if exists(Select * From Transaccion Where IDTerminal=@ID) begin
			set @Mensaje = ''El registro tiene transacciones asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Terminal
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''TERMINAL'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End


' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpSucursal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpSucursal]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Codigo varchar(5),
	@CodigoDistribuidor varchar(50)=NULL,
	@IDPais tinyint = NULL,
	@IDDepartamento tinyint = NULL,
	@IDCiudad smallint = NULL,
	@Direccion varchar(100) = NULL,
	@Telefono varchar(20) = NULL,
	@Referencia varchar(5) = NULL,
	@CuentaContable varchar(50) = NULL,
	@Estado bit,
	@Operacion varchar(10),
		
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Sucursal Where Descripcion=@Descripcion) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que el codigo ya existe
		if exists(Select * From Sucursal Where Codigo=@Codigo) begin
			set @Mensaje = ''El codigo ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDSucursal tinyint
		set @vIDSucursal = (Select IsNull((Max(ID)+1), 1) From Sucursal)

		--Insertamos
		Insert Into Sucursal(ID, Descripcion, IDPais, IDDepartamento, Codigo, IDCiudad, Direccion, Telefono, Referencia, CuentaContable, Estado, CodigoDistribuidor)
		Values(@vIDSucursal, @Descripcion, @IDPais, @IDDepartamento, @Codigo, @IDCiudad, @Direccion, @Telefono, @Referencia, @CuentaContable, @Estado, @CodigoDistribuidor)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''SUCURSAL'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From Sucursal Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Sucursal Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que el codigo ya existe
		if exists(Select * From Sucursal Where Codigo=@Codigo And ID!=@ID) begin
			set @Mensaje = ''El codigo ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update Sucursal Set Descripcion=@Descripcion,
							Codigo=@Codigo,
							IDPais=@IDPais, 
							IDDepartamento=@IDDepartamento, 
							IDCiudad=@IDCiudad, 
							Direccion=@Direccion, 
							Telefono=@Telefono, 
							Referencia=@Referencia, 
							CuentaContable=@CuentaContable, 
							Estado = @Estado,
							CodigoDistribuidor=@CodigoDistribuidor
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''SUCURSAL'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From Sucursal Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con Transaccion
		if exists(Select * From Transaccion Where IDSucursal=@ID) begin
			set @Mensaje = ''El registro tiene transacciones asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDSucursal=@ID) begin
			set @Mensaje = ''El registro tiene clientes asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Sucursal 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''SUCURSAL'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpEstadoCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpEstadoCliente]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),

	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From EstadoCliente Where Descripcion=@Descripcion) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDEstadoCliente tinyint
		set @vIDEstadoCliente = (Select IsNull((Max(ID)+1), 1) From EstadoCliente)

		--Insertamos
		Insert Into EstadoCliente(ID, Descripcion, Estado)
		Values(@vIDEstadoCliente, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''ESTADO DE CLIENTES'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From EstadoCliente Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From EstadoCliente Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update EstadoCliente Set Descripcion=@Descripcion,
						Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''ESTADO DE CLIENTES'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From EstadoCliente Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDEstado=@ID) begin
			set @Mensaje = ''El registro tiene clientes asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From EstadoCliente 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''ESTADO DE CLIENTES'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpDepartamento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpDepartamento]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@IDPais tinyint,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Departamento Where Descripcion=@Descripcion And IDPais=@IDPais) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDDepartamento tinyint
		set @vIDDepartamento = (Select IsNull((Max(ID)+1), 1) From Departamento)

		--Insertamos
		Insert Into Departamento(ID, Descripcion, IDPais, Orden, Estado)
		Values(@vIDDepartamento, @Descripcion, @IDPais, 0, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''DEPARTAMENTO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From Departamento Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Departamento Where Descripcion=@Descripcion And ID!=@ID And IDPais=@IDPais) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update Departamento Set Descripcion=@Descripcion, 
						IDPais=@IDPais,
						Estado = @Estado
		Where ID=@ID
				
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''DEPARTAMENTO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
				
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From Departamento Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con CIUDAD
		if exists(Select * From Ciudad Where IDDepartamento=@ID) begin
			set @Mensaje = ''El registro tiene ciudades asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDDepartamento=@ID) begin
			set @Mensaje = ''El registro tiene clientes asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Departamento 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''DEPARTAMENTO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
				
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpCiudad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpCiudad]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@IDPais tinyint,
	@IDDepartamento tinyint,
	@Codigo varchar(5),
	@Estado bit,
	@Operacion varchar(10),
		
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Ciudad Where Descripcion=@Descripcion And IDPais=@IDPais And IDDepartamento=@IDDepartamento) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDCiudad tinyint
		set @vIDCiudad = (Select IsNull((Max(ID)+1), 1) From Ciudad)

		--Insertamos
		Insert Into Ciudad(ID, Descripcion, IDPais, IDDepartamento, Codigo, Estado)
		Values(@vIDCiudad, @Descripcion, @IDPais, @IDDepartamento, @Codigo, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''CIUDAD'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From Ciudad Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Ciudad Where Descripcion=@Descripcion And ID!=@ID And IDPais=@IDPais And IDDepartamento=@IDDepartamento) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update Ciudad Set Descripcion=@Descripcion, 
						IDPais=@IDPais,
						IDDepartamento=@IDDepartamento, 
						Codigo=@Codigo,
						Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''CIUDAD'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From Ciudad Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con CLIENTES
		if exists(Select * From Cliente Where IDCiudad=@ID) begin
			set @Mensaje = ''El registro tiene clientes asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From Ciudad 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''CIUDAD'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LoteDistribucion](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[Numero] [int] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[Comprobante] [varchar](50) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[FechaReparto] [date] NOT NULL,
	[IDDistribuidor] [tinyint] NULL,
	[IDCamion] [tinyint] NULL,
	[IDChofer] [tinyint] NULL,
	[IDZonaVenta] [tinyint] NULL,
	[TotalContado] [money] NOT NULL,
	[TotalCredito] [money] NOT NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalDescuento] [money] NULL,
	[CantidadComprobantes] [smallint] NOT NULL,
	[Anulado] [bit] NOT NULL,
	[Observacion] [varchar](100) NULL,
	[FechaAnulado] [bit] NULL,
	[IDUsuarioAnulado] [smallint] NULL,
 CONSTRAINT [PK_LoteDistribucion] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductoZona]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductoZona](
	[IDSucursal] [tinyint] NOT NULL,
	[IDDeposito] [smallint] NOT NULL,
	[IDZonaDeposito] [tinyint] NOT NULL,
	[IDProducto] [int] NOT NULL,
 CONSTRAINT [PK_ProductoZona] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[IDSucursal] ASC,
	[IDDeposito] ASC,
	[IDZonaDeposito] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductoListaPrecioExcepciones](
	[IDProducto] [int] NOT NULL,
	[IDListaPrecio] [int] NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDTipoDescuento] [tinyint] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[Descuento] [money] NOT NULL,
	[Porcentaje] [decimal](5, 2) NOT NULL,
	[Desde] [date] NULL,
	[Hasta] [date] NULL,
 CONSTRAINT [PK_ProductoListaPrecioExcepciones] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[IDListaPrecio] ASC,
	[IDCliente] ASC,
	[IDSucursal] ASC,
	[IDTipoDescuento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductoListaPrecio](
	[IDListaPrecio] [int] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[Precio] [money] NOT NULL,
	[TPR] [money] NULL,
	[TPRPorcentaje] [decimal](10, 3) NULL,
	[TPRDesde] [date] NULL,
	[TPRHasta] [date] NULL,
 CONSTRAINT [PK_ProductoListaPrecio] PRIMARY KEY CLUSTERED 
(
	[IDListaPrecio] ASC,
	[IDProducto] ASC,
	[IDMoneda] ASC,
	[IDSucursal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NotaDebito]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NotaDebito](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDPuntoExpedicion] [int] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[NroComprobante] [varchar](50) NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[Fecha] [date] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[Cotizacion] [money] NULL,
	[Observacion] [varchar](50) NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalDescuento] [money] NOT NULL,
	[Saldo] [money] NOT NULL,
	[Anulado] [bit] NOT NULL,
	[FechaAnulado] [date] NULL,
	[IDUsuarioAnulado] [smallint] NULL,
	[Procesado] [bit] NULL,
	[Reposicion] [bit] NULL,
	[Debito] [bit] NULL,
	[Aplicar] [bit] NULL,
	[ConComprobantes] [bit] NULL,
 CONSTRAINT [PK_NotaDebito] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DetalleImpuesto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DetalleImpuesto](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDImpuesto] [tinyint] NOT NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalDescuento] [money] NULL,
	[RetencionIVA] [money] NULL,
	[RetencionRenta] [money] NULL,
 CONSTRAINT [PK_DetalleImpuesto] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[IDImpuesto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DocumentoAnulado]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DocumentoAnulado](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IDUsuario] [smallint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDTerminal] [smallint] NOT NULL,
 CONSTRAINT [PK_DocumentoAnulado] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FCostoProducto]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FCostoProducto]
(

	--Entrada
	@IDProducto int
)

Returns money

As

Begin

	Declare @vRetorno money
	Declare @vConfiguracionCosto varchar(50)
	
	--Obtenemos la configuracion del Costo
	Set @vConfiguracionCosto = (Select IsNull((Select Top(1) MovimientoCostoProducto From Configuraciones), ''PROMEDIO''))
	
	--Establecemos el monto
	
	Set @vRetorno = Case 
						When @vConfiguracionCosto = ''ULTIMO'' Then
							--(Select IsNull((Select UltimoCosto From RegistroCosto Where IDProducto=@IDProducto),0))
							(Select IsNull((Select CostoSinIVA From Producto Where ID=@IDProducto),0))
						When @vConfiguracionCosto = ''PROMEDIO'' Then
							--(Select IsNull((Select Promedio From RegistroCosto Where IDProducto=@IDProducto),0))
							(Select IsNull((Select CostoPromedio From Producto Where ID=@IDProducto),0))
						When @vConfiguracionCosto = '''' Then 0
					End
	
	Return @vRetorno

End




' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExistenciaDeposito]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExistenciaDeposito](
	[IDDeposito] [tinyint] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[Existencia] [decimal](10, 2) NOT NULL,
	[ExistenciaMinima] [decimal](10, 2) NULL,
	[ExistenciaCritica] [decimal](10, 2) NULL,
	[PlazoReposicion] [tinyint] NULL,
 CONSTRAINT [PK_ExistenciaDeposito] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[IDDeposito] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Asiento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Asiento](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[Numero] [int] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[Fecha] [date] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[Cotizacion] [money] NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[NroComprobante] [varchar](50) NOT NULL,
	[Detalle] [varchar](500) NOT NULL,
	[Total] [money] NOT NULL,
	[Debito] [money] NOT NULL,
	[Credito] [money] NOT NULL,
	[Saldo] [money] NOT NULL,
	[Anulado] [bit] NULL,
	[IDCentroCosto] [tinyint] NULL,
	[Conciliado] [bit] NULL,
	[FechaConciliado] [date] NULL,
	[IDUsuarioConciliado] [smallint] NULL,
	[Bloquear] [bit] NULL,
 CONSTRAINT [PK_Asiento] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClienteSucursal](
	[IDCliente] [int] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[Sucursal] [varchar](50) NOT NULL,
	[Direccion] [varchar](200) NULL,
	[Contacto] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[Estado] [bit] NULL,
	[IDPais] [tinyint] NULL,
	[IDDepartamento] [tinyint] NULL,
	[IDCiudad] [smallint] NULL,
	[IDBarrio] [smallint] NULL,
	[IDVendedor] [tinyint] NULL,
	[IDZonaVenta] [tinyint] NULL,
	[Latitud] [varchar](50) NULL,
	[Longitud] [varchar](50) NULL,
	[IDSucursal] [tinyint] NULL,
	[IDListaPrecio] [tinyint] NULL,
	[Contado] [bit] NULL,
	[Credito] [bit] NULL,
	[IDCobrador] [tinyint] NULL,
	[IDEstado] [tinyint] NULL,
 CONSTRAINT [PK_ClienteSucursal] PRIMARY KEY CLUSTERED 
(
	[IDCliente] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClienteCuentaBancaria]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClienteCuentaBancaria](
	[ID] [int] NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDBanco] [tinyint] NOT NULL,
	[CuentaBancaria] [varchar](50) NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
 CONSTRAINT [PK_ClienteCuentaBancaria] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClienteContacto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClienteContacto](
	[IDCliente] [int] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[Nombres] [varchar](50) NOT NULL,
	[Cargo] [varchar](50) NULL,
	[Telefono] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
 CONSTRAINT [PK_ClienteContacto] PRIMARY KEY CLUSTERED 
(
	[IDCliente] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ComisionProducto]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ComisionProducto](
	[IDProducto] [int] NOT NULL,
	[IDVendedor] [tinyint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[PorcentajeBase] [decimal](5, 3) NULL,
	[PorcentajeEsteProducto] [decimal](5, 3) NULL,
	[PorcentajeAnterior] [decimal](5, 3) NULL,
	[FechaUltimoCambio] [date] NULL,
	[IDUsuario] [smallint] NULL,
 CONSTRAINT [PK_ComisionProducto] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[IDVendedor] ASC,
	[IDSucursal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CobranzaCredito](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDSucursal] [tinyint] NULL,
	[Numero] [int] NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[NroComprobante] [numeric](18, 0) NOT NULL,
	[Comprobante] [varchar](50) NOT NULL,
	[NroPlanilla] [nchar](10) NULL,
	[IDCliente] [int] NOT NULL,
	[FechaEmision] [date] NOT NULL,
	[IDCobrador] [tinyint] NOT NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalDescuento] [money] NULL,
	[Anulado] [bit] NOT NULL,
	[FechaAnulado] [date] NULL,
	[IDUsuarioAnulado] [smallint] NULL,
	[Observacion] [varchar](200) NULL,
	[Procesado] [bit] NULL,
 CONSTRAINT [PK_ReciboCliente] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpZonaVenta]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpZonaVenta]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From ZonaVenta Where Descripcion=@Descripcion) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDZonaVenta tinyint
		set @vIDZonaVenta = (Select IsNull((Max(ID)+1), 1) From ZonaVenta)

		--Insertamos
		Insert Into ZonaVenta(ID, Descripcion, Estado)
		Values(@vIDZonaVenta, @Descripcion, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''ZONA DE VENTA'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		If not exists(Select * From ZonaVenta Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From ZonaVenta Where Descripcion=@Descripcion And ID!=@ID) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update ZonaVenta Set Descripcion=@Descripcion,
						 Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''ZONA DE VENTA'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From ZonaVenta Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una relacion con Productos
		if exists(Select * From Cliente Where IDZonaVenta=@ID) begin
			set @Mensaje = ''El registro tiene clientes asociados! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ZonaVenta 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''ZONA DE VENTA'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VMarcaPresentacion]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VMarcaPresentacion]
As
Select
MP.*,
''TipoProducto''=TP.Descripcion,
''Linea''=L.Descripcion,
''SubLinea''=SL.Descripcion,
''SubLinea2''=SL2.Descripcion,
''Marca''=M.Descripcion,
''Presentacion''=P.Descripcion

From MarcaPresentacion MP
Join TipoProducto TP On MP.IDTipoProducto=TP.ID
Join Linea L On MP.IDLinea=L.ID
JOin SubLinea SL On MP.IDSubLinea=SL.ID
JOin SubLinea2 SL2 On MP.IDSubLinea2=SL2.ID
JOin Marca M On MP.IDMarca=M.ID
Join Presentacion P On MP.IDPresentacion=P.ID
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VPuntoExpedicion]'))
EXEC dbo.sp_executesql @statement = N'





CREATE View [dbo].[VPuntoExpedicion]
As

Select 
PE.ID,
PE.IDTipoComprobante,
''TipoComprobante''=TC.Codigo,
PE.IDSucursal,
''Sucursal''=S.Codigo,
''Suc''=S.Descripcion,
S.IDCiudad,
''Ciudad''=S.CodigoCiudad,
''CiudadDesc.''= s.Descripcion, 
''ReferenciaSucursal''=S.Referencia,
''ReferenciaPunto''=PE.Referencia,
PE.Descripcion,
PE.Timbrado,
PE.Vencimiento,
PE.NumeracionDesde,
PE.NumeracionHasta,
PE.CantidadPorTalonario,
''Talonarios''= CEILING((PE.NumeracionHasta - PE.NumeracionDesde)  / (Case When(PE.CantidadPorTalonario) = 0 Then 1 Else PE.CantidadPorTalonario End)),
''UltimoNumeroExpedido''=IsNull(PE.UltimoNumero, 0),
''ProximoNumero''=dbo.FNroComprobante(PE.ID),
''ProximoComprobante''=S.Referencia + ''-'' + PE.Referencia + ''-'' + Convert(varchar(50), dbo.FNroComprobante(PE.ID)),
''TalonarioActual''=Floor(((dbo.FNroComprobante(PE.ID) - PE.NumeracionDesde)/PE.CantidadPorTalonario) + 1),
''Saldo''=(PE.NumeracionHasta - dbo.FNroComprobante(PE.ID)) + 1, 
PE.Estado,

--OPERACION
TC.IDOperacion,
''Operacion''=O.Descripcion,
''CodigoOperacion''=O.Codigo

From PuntoExpedicion PE
Join TipoComprobante TC On PE.IDTipoComprobante=TC.ID
Join Operacion O On TC.IDOperacion=O.ID
Join VSucursal S On PE.IDSucursal=S.ID






'







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Venta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Venta](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDPuntoExpedicion] [int] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[NroComprobante] [numeric](18, 0) NOT NULL,
	[Comprobante] [varchar](50) NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDSucursalCliente] [tinyint] NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](50) NULL,
	[FechaEmision] [datetime] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[Credito] [bit] NOT NULL,
	[FechaVencimiento] [date] NULL,
	[IDListaPrecio] [int] NULL,
	[Descuento] [numeric](2, 0) NULL,
	[IDVendedor] [tinyint] NULL,
	[IDPromotor] [tinyint] NULL,
	[IDMoneda] [tinyint] NULL,
	[Cotizacion] [money] NULL,
	[Observacion] [varchar](500) NULL,
	[NroComprobanteRemision] [varchar](50) NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalDescuento] [money] NULL,
	[Cobrado] [money] NULL,
	[Descontado] [money] NULL,
	[Acreditado] [money] NULL,
	[Saldo] [money] NOT NULL,
	[Cancelado] [bit] NOT NULL,
	[Despachado] [bit] NULL,
	[EsVentaSucursal] [bit] NULL,
	[Anulado] [bit] NOT NULL,
	[FechaAnulado] [bit] NULL,
	[IDUsuarioAnulado] [smallint] NULL,
	[Procesado] [bit] NULL,
	[Autoriza] [varchar](50) NULL,
	[IDMotivo] [tinyint] NULL,
	[FecAnulado] [date] NULL,
	[IDVendedorTemp] [int] NULL,
 CONSTRAINT [PK_Venta] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VClienteSucursal]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VClienteSucursal]

As

Select
CS.IDCliente,
CS.ID,
CS.Sucursal,
CS.Direccion,
CS.Contacto,
CS.Telefono,
CS.Estado,

CS.IDVendedor,
''Vendedor''=(Select ISNULL((Select V.Nombres From Vendedor V Where V.ID=CS.IDVendedor), '''')),

CS.IDPais,
''Pais''=(Select ISNULL((Select P.Descripcion From Pais P Where P.ID=CS.IDPais), '''')),

CS.IDDepartamento,
''Departamento''=(Select ISNULL((Select D.Descripcion From Departamento D Where D.ID=CS.IDDepartamento), '''')),

CS.IDCiudad,
''Ciudad''=(Select ISNULL((Select CIU.Descripcion From Ciudad CIU Where CIU.ID=CS.IDCiudad), '''')),

CS.IDBarrio,
''Barrio''=(Select ISNULL((Select B.Descripcion From Barrio B Where B.ID=CS.IDBarrio), '''')),

CS.IDZonaVenta,
''ZonaVenta''=(Select ISNULL((Select Z.Descripcion From ZonaVenta Z Where Z.ID=CS.IDZonaVenta), '''')),

CS.IDSucursal,
''ClienteSucursal''=(Select ISNULL((Select S.Descripcion From Sucursal S Where S.ID=CS.IDSucursal), '''')),

CS.IDListaPrecio,
''ListaPrecio''= (Select ISNULL ((Select LP.Descripcion  From ListaPrecio LP Where LP.ID = CS.IDListaPrecio), '''')),

''Contado''=IsNull(CS.Contado, ''True''),
''Credito''=IsNull(CS.Credito, ''False''),


CS.IDCobrador,
''Cobrador''= (Select ISNULL (( Select C.Nombres  From Cobrador C Where C.ID=CS.Idcobrador), '''')),

CS.IDEstado,
''Estados''= (Select ISNULL (( Select EC.Descripcion  From EstadoCliente EC Where EC.ID=CS.IDEstado), '''')),

''Latitud''=IsNull(CS.Latitud, 0),
''Longitud''=IsNull(CS.Longitud,0)

From ClienteSucursal CS
Join Cliente C On CS.IDCliente=C.ID



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VClienteContacto]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VClienteContacto]
As
Select 
* 
From ClienteContacto'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCliente]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VCliente]
As

Select
--Comerciales
C.ID,
C.RazonSocial,
C.RUC,
''Tipo''= (Case When (C.CI)= ''True'' Then ''CI'' Else ''RUC'' End),
C.CI,
C.Referencia,
C.NombreFantasia,
C.Direccion,
C.Telefono,
''RazonSocialReferencia''=C.RazonSocial + '' ('' + C.Referencia + '')'',

--De configuracion
C.IDEStado,
''Estado''=(Select IsNull((Select V.Descripcion From EstadoCliente V Where V.ID=C.IDEstado), '''')),
''Contado''=IsNull(C.Contado, ''True''),
''Credito''=IsNull(C.Credito, ''False''),
''Condicion''=(Case When (IsNull(C.Contado, ''True'')) = ''True'' Then ''CONTADO'' Else ''CREDITO'' End),
''IDMoneda''=(IsNull(C.IDMoneda, 1)),
''Moneda''=(Select IsNull((Select V.Descripcion From Moneda V Where V.ID=C.IDMoneda ), '''')),
C.IDListaPrecio,
''ListaPrecio''=(Select IsNull((Select V.Descripcion From ListaPrecio V Where V.ID=C.IDListaPrecio), '''')),
C.IDTipoCliente,
''TipoCliente''=(Select IsNull((Select V.Descripcion From TipoCliente V Where V.ID=C.IDTipoCliente), '''')),
''IVAExento''=IsNull(C.IVAExento, ''False''),

--Contabilidar
C.CodigoCuentaContable,
''Cuenta''=(Select IsNull((Select V.Cuenta From VCuentaContable V Where V.Codigo=C.CodigoCuentaContable), '''')),

--Localizacion
C.IDPais,
''Pais''=(Select IsNull((Select V.Descripcion From Pais V Where V.ID=C.IDPais), '''')),
C.IDDepartamento,
''Departamento''=(Select IsNull((Select V.Descripcion From Departamento V Where V.ID=C.IDDepartamento), '''')),
C.IDCiudad,
''Ciudad''=(Select IsNull((Select V.Descripcion From Ciudad V Where V.ID=C.IDCiudad), '''')),
C.IDBarrio,
''Barrio''=(Select IsNull((Select V.Descripcion From Barrio V Where V.ID=C.IDBarrio), '''')),
C.IDZonaVenta,
''ZonaVenta''=(Select IsNull((Select V.Descripcion From ZonaVenta V Where V.ID=C.IDZonaVenta), '''')),
''Latitud''=IsNull(C.Latitud, 0),
''Longitud''=IsNull(C.Longitud, 0),

--Referencias
C.IDSucursal,
''Sucursal''=(Select IsNull((Select V.Descripcion From Sucursal V Where V.ID=C.IDSucursal), '''')),
C.IDPromotor,
''Promotor''=(Select IsNull((Select V.Nombres From Promotor V Where V.ID=C.IDPromotor), '''')),
''IDVendedor''=isnull(C.IDVendedor,0),
''Vendedor''=(Select IsNull((Select V.Nombres From Vendedor V Where V.ID=C.IDVendedor), '''')),
C.IDCobrador,
''Cobrador''=(Select IsNull((Select V.Nombres From Cobrador V Where V.ID=C.IDCobrador), '''')),
C.IDDistribuidor,
''Distribuidor''=(Select IsNull((Select V.Nombres From Distribuidor V Where V.ID=C.IDDistribuidor), '''')),


--Estadisticos
''FechaAlta''=C.FechaAlta,
''Alta''=IsNull(Convert(varchar(50), C.FechaAlta, 6), ''---''),
''UsuarioAlta''=IsNull((Select U.Usuario From Usuario U Where U.ID=C.IDUsuarioAlta), ''---''),
''FechaModificacion''=C.FechaModificacion,
''Modificacion''=IsNull(Convert(varchar(50), C.FechaModificacion, 6), ''---''),
''UsuarioModificacion''=IsNull((Select U.Usuario From Usuario U Where U.ID=C.IDUsuarioModificacion), ''---''),
''UltimaCobranza''=C.FechaUltimaCobranza,
''Ult. Cobranza''=IsNull(Convert(varchar(50), C.FechaUltimaCobranza, 6), ''---''),
''UltimaCompra''=C.FechaUltimaCompra,
''Ult. Compra''=IsNull(Convert(varchar(50), C.FechaUltimaCompra, 6), ''---''),

--Credito
''LimiteCredito''=IsNull(LimiteCredito, 0),
''Deuda''=IsNull(Deuda, 0),
''SaldoCredito''= IsNull(LimiteCredito, 0) - (Select IsNull((Select SUM(Saldo) From Venta Where IDCliente=C.ID And Anulado=''False'' And Cancelado=''False'' And Procesado=''True''),0)),
''Descuento''=IsNull(Descuento, 0),
''PlazoCredito''=IsNull(PlazoCredito, 0),
''PlazoCobro''=IsNull(PlazoCobro, 0),
''PlazoChequeDiferido''=IsNull(PlazoChequeDiferido, 0),
''DeudaTotal''=(Select IsNull((Select SUM(Saldo) From Venta Where IDCliente=C.ID And Anulado=''False'' And Cancelado=''False'' And Procesado=''True''),0)),
--Datos Adicionales
C.PaginaWeb,
C.Email,
C.Fax,

''TieneSucursales''=Case When (Select Top(1) CS.ID From ClienteSucursal CS Where IDCliente=C.ID) Is NULL Then ''False'' Else ''True'' End


From

Cliente as C'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VAsiento]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VAsiento]
As
Select 
A.IDTransaccion,
A.Numero,
A.IDSucursal,
''Sucursal''=S.Descripcion,
''Suc''=S.Codigo,
S.IDCiudad,
''Ciudad''=C.Codigo,
A.Fecha,
A.IDMoneda,
''Moneda''=M.Referencia,
A.Cotizacion,
A.IDTipoComprobante,
''TipoComprobante''=IsNull(TP.Codigo, ''---''),
''NroComprobante''=A.NroComprobante,
''Comprobante''=IsNull(TP.Codigo, ''---'') + '' '' + A.NroComprobante,
A.Detalle,
A.Debito,
A.Credito,
A.Saldo,
A.Total,
A.Anulado,
A.IDCentroCosto,
''CentroCosto''=(Select ISNULL((Select CC.Descripcion From CentroCosto CC Where CC.ID=A.IDCentroCosto),''---'')),
''Conciliado''=IsNull(A.Conciliado, ''False''),
''Estado''=(Select Case When(IsNull(A.Conciliado, ''False'')) = ''True'' Then ''CONCILIADO'' Else ''---'' End),
A.FechaConciliado,
A.IDUsuarioConciliado,
''Balanceado''=(Case When (A.Credito) != A.Debito Then ''False'' Else ''True'' End),
A.Bloquear,
''CajaChica''=''False''

From Asiento A
Join Transaccion T On A.IDTransaccion=T.ID
Join Sucursal S On A.IDSucursal=S.ID
Join Ciudad C On S.IDCiudad=C.ID
Join Moneda M On A.IDMoneda=M.ID
Left Outer Join TipoComprobante TP On A.IDTipoComprobante=TP.ID









'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VCobranzaCredito]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VCobranzaCredito]
As

Select 

--Cabecera
''Ciudad''=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
CC.Numero,
CC.IDSucursal,
''Sucursal''=S.Descripcion,
''Suc''=S.Codigo,
CC.IDTipoComprobante,
''DescripcionTipoComprobante''=TC.Descripcion,
''TipoComprobante''=TC.Codigo,
''Cod.''=TC.Codigo,
CC.NroComprobante,
''Comprobante''=CC.NroComprobante,
CC.FechaEmision,
''Fec''=CONVERT(varchar(50), CC.FechaEmision, 6),
CC.IDCobrador,
''Cobrador''=CO.Nombres,
CC.Observacion,
CC.Anulado,
--Totales
CC.Total,
CC.NroPlanilla,

--Cliente
''IDCliente''=ISNULL(CC.IDCliente,0),
''Cliente''=ISNULL(C.RazonSocial,''''),
''IDTipoCliente''=ISNULL(TCLI.ID,''''),
''TipoCliente''=ISNULL(TCLI.Descripcion,''''),
''RUC''=ISNULL(C.RUC,''''),
''Referencia''=ISNULL(C.Referencia,''''),
''IDVendedor''=isnull(C.IDVendedor,0),

--Transaccion
CC.IDTransaccion,
''FechaTransaccion''=T.Fecha,
''IDDepositoTransaccion''=T.IDDeposito,
''IDSucursalTransaccion''=T.IDSucursal,
''IDTerminalTransaccion''=T.IDTerminal,
T.IDUsuario,
''usuario''=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
''UsuarioIdentificador''=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),


--Anulacion
''Estado''=Case When CC.Anulado=''True'' Then ''Anulado'' Else ''---'' End,
''IDUsuarioAnulacion''=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion), 0)),
''UsuarioAnulacion''=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), ''---'')),
''UsuarioIdentificacionAnulacion''=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), ''---'')),
''FechaAnulacion''=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion)

From CobranzaCredito CC
Join Transaccion T On CC.IDTransaccion=T.ID
Left Outer Join TipoComprobante TC On CC.IDTipoComprobante=TC.ID
Left Outer Join Cliente C On CC.IDCliente=C.ID
Left Outer Join TipoCliente TCLI On TCLI.ID=C.IDTipoCliente
Left Outer Join Sucursal S On CC.IDSucursal=S.ID
Left Outer Join Cobrador CO On CC.IDCobrador=CO.ID












'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetalleImpuesto]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VDetalleImpuesto]
As
Select
DI.IDTransaccion,
DI.IDImpuesto,
''COD''=(Case When DI.IDImpuesto=1 Then 1 When DI.IDImpuesto=2 Then 2 Else 3 End),
''Impuesto''=I.Descripcion,
DI.Total,
DI.TotalDiscriminado,
DI.TotalImpuesto,
''TotalSinImpuesto''=DI.Total - DI.TotalImpuesto,
''TotalSinDescuento''=0,
DI.TotalDescuento,

--Redondeado
''TotalDiscriminadoRedondeado''=Round(DI.TotalDiscriminado,0,0),
''TotalImpuestoRedondeado''=Round(DI.TotalImpuesto, 0,0)


From DetalleImpuesto DI
Join Impuesto I On DI.IDImpuesto=I.ID




'





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VentaLoteDistribucion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VentaLoteDistribucion](
	[IDTransaccionVenta] [numeric](18, 0) NOT NULL,
	[IDTransaccionLote] [numeric](18, 0) NOT NULL,
	[ID] [smallint] NOT NULL,
	[Importe] [money] NOT NULL,
 CONSTRAINT [PK_VentaLoteDistribucion] PRIMARY KEY CLUSTERED 
(
	[IDTransaccionVenta] ASC,
	[IDTransaccionLote] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VProductoListaPrecio]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VProductoListaPrecio]
As
Select 
''IDListaPrecio''=LP.ID,
''ListaPrecio''=LP.Descripcion,
''Estado''=Case When LP.Estado = ''True'' Then ''OK'' Else ''-'' End,
''IDProducto''=P.ID,
''Producto''=P.Descripcion,
''Referencia''=P.Referencia ,
''Precio''=PLP.Precio,
''IDMoneda''=PLP.IDMoneda,
''Moneda''=ISNULL(M.Referencia, ''-''),
''Decimales''=ISNULL(M.Decimales, ''False''),

--TPR
''TPR''=IsNull(PLP.TPR, 0),
''TPRPorcentaje''=IsNull(PLP.TPRPorcentaje, 0),
''TPRDesde''=IsNull(convert(varchar(50), PLP.TPRDesde, 3), ''---''),
''TPRHasta''=IsNull(convert(varchar(50), PLP.TPRHasta, 3), ''---''),

--Sucursal
LP.IDSucursal,
''Sucursal''=S.Descripcion

From ListaPrecio LP
Join ProductoListaPrecio PLP On lp.ID = PLP.IDListaPrecio
Join Producto P On PLP.IDProducto  = P.ID
Left Outer Join Moneda M on PLP.IDMoneda = M.ID
Join Sucursal S On LP.IDSucursal=S.ID



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VProductoGrupo]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VProductoGrupo]
As
Select
TP.IDTipoProducto,
TP.TipoProducto,
L.IDLinea,
L.Linea,
SL.IDSubLinea,
SL.SubLinea,
SL2.IDSubLinea2,
SL2.SubLinea2,
M.IDMarca,
M.Marca,
M.IDPresentacion,
M.Presentacion


From
VTipoProductoLinea TP
Join VLineaSubLinea L On TP.IDTipoProducto=L.IDTipoProducto
JOin VSubLineaSubLinea2 SL On L.IDTipoProducto=SL.IDTipoProducto And L.IDLinea=SL.IDLinea
Join VSubLinea2Marca SL2 On SL.IDTipoProducto=SL2.IDTipoProducto And SL.IDLinea=SL2.IDLinea And SL.IDSubLinea=SL.IDSubLinea
Join VMarcaPresentacion M On SL2.IDTipoProducto=M.IDTipoProducto And SL2.IDLinea=M.IDLinea And SL2.IDSubLinea=M.IDSubLinea

'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VProducto]'))
EXEC dbo.sp_executesql @statement = N'



CREATE View [dbo].[VProducto]

As

Select 

--CARLOS 05-03-2014

--Identificadores
P.ID,
P.Descripcion,
P.CodigoBarra,
P.Referencia,
''Ref''=P.Referencia,
''ProductoReferencia''=P.Descripcion + '' ('' + P.Referencia + '')'',
--Esto no se puede usar, porque se pueden repetir
--Si es necesario, buscar otra alternativa
''IDSucursal''=0,
''IDDeposito''=0,

--Clasificaciones
P.IDTipoProducto,
''TipoProducto''=ISNULL(TP.Descripcion, ''''),
P.IDLinea,
''Linea''=ISNULL(L.Descripcion, ''''),
P.IDSubLinea,
''SubLinea''=ISNULL(SL.Descripcion, ''''),
P.IDSubLinea2,
''SubLinea2''=ISNULL(SL2.Descripcion, ''''),
P.IDMarca,
''Marca''=ISNULL(M.Descripcion, ''''),
P.IDPresentacion,
''Presentacion''=ISNULL(PRE.Descripcion, ''''),
P.IDCategoria,
''Categoria''=ISNULL(CA.Descripcion, ''''),
P.IDProveedor,
''ReferenciaProveedor''=PRO.Referencia,
''Proveedor''=ISNULL(PRO.RazonSocial, ''''),
''ProveedorReferencia''=PRO.RazonSocial + '' ('' + PRO.Referencia + '')'',
P.IDDivision,
''Division''=ISNULL(D.Descripcion, ''''),
P.IDProcedencia,
''Procedencia''=ISNULL(PA.Descripcion , ''''),

--Nueva Clasificacion
''IDClasificacion''=IsNull(P.IDClasificacion, 0),
''Clasificacion''=IsNull(CLA.Descripcion, ''''),
''CodigoClasificacion''=IsNull(CLA.Codigo, 0),
''NivelClasificacion''=IsNull(CLA.Nivel, 0),
''NumeroClasificacion''=IsNull(CLA.Numero, 0),

--Stock
P.IDUnidadMedida,
''UnidadMedida''=ISNULL(U.Referencia, ''UND''),
''Uni. Med.''=ISNULL(U.Descripcion, ''UNIDAD''),
''UnidadPorCaja''=ISNULL(P.UnidadPorCaja, 1),
P.IdUnidadMedidaConvertir,

''UnidadMedidaConvertir''= ISNULL((Select Referencia from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),ISNULL(U.Referencia, ''UND'')),
''Uni. Med. Conver.''= ISNULL((Select Descripcion from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),ISNULL(U.Descripcion, ''UND'')),
''UnidadConvertir''=Isnull (P.UnidadConvertir,1),

P.Peso,
P.Volumen,

--Impuestos
P.IDImpuesto,
''Impuesto''=I.Descripcion,
''Ref Imp''=I.Referencia,
''Exento''=I.Exento,

--Configuraciones
''Estado''=Isnull(P.Estado, ''True''),
''EstadoDescripcion''=Case When P.Estado = ''True'' Then ''Activo'' Else ''Inactivo'' End,
''ControlarExistencia''=IsNull(P.ControlarExistencia, ''False''),
P.ExistenciaGeneral,
''Existencia''=P.ExistenciaGeneral,

--Contabilidad 
''CuentaCompra''=(Select IsNull((Select V.Cuenta From VCuentaContable V Where V.Codigo=P.CuentaContableCompra), '''')),
''CodigoCuentaCompra''=(Select IsNull((Select Convert(varchar(50), V.Codigo) From VCuentaContable V Where V.Codigo=P.CuentaContableCompra), '''')),
''CuentaVenta''=(Select IsNull((Select V.Cuenta From VCuentaContable V Where V.Codigo=P.CuentaContableVenta), '''')),
''CodigoCuentaVenta''=(Select IsNull((Select Convert(varchar(50), V.Codigo) From VCuentaContable V Where V.Codigo=P.CuentaContableVenta), '''')),
''CuentaCosto''=(Select IsNull((Select V.Cuenta From VCuentaContable V Where V.Codigo=P.CuentaContableCosto), '''')),
''CodigoCuentaCosto''=(Select IsNull((Select Convert(varchar(50), V.Codigo) From VCuentaContable V Where V.Codigo=P.CuentaContableCosto), '''')),

--Costo
''Precio''=0,
''UltimaEntrada''=Convert(varchar(50), P.UltimaEntrada, 6), 
''CantidadEntrada''=IsNull(P.CantidadEntrada, 0),
''UltimaSalida''=Convert(varchar(50), P.UltimaSalida, 6), 
''Costo''=dbo.FCostoProducto(P.ID),
''UltimoCosto''=IsNull(P.UltimoCosto,0),
P.CostoSinIVA,
P.CostoCG,
P.CostoPromedio,

--Descuentos
''TotalDescuento''=0,
''DescuentoUnitario''=0,
''PorcentajeDescuento''=0

--''UltimoCosto''=(Select IsNull((Select RC.UltimoCosto From RegistroCosto RC Where RC.IDProducto=P.ID), 0)),
--''CostoPromedio''=(Select IsNull((Select RC.Promedio From RegistroCosto RC Where RC.IDProducto=P.ID), 0)),
--''UltimaActualizacion''=(Select IsNull((Select Convert(varchar(50), RC.UltimaActualizacion, 6) From RegistroCosto RC Where RC.IDProducto=P.ID), ''---'')),



From Producto P
Left Outer Join TipoProducto TP On P.IDTipoProducto=TP.ID
Left Outer Join Linea L On P.IDLinea=L.ID
Left Outer Join SubLinea SL On P.IDSubLinea=SL.ID
Left Outer Join SubLinea2 SL2 On P.IDSubLinea2=SL2.ID
Left Outer Join Marca M On P.IDMarca=M.ID
Left Outer Join Presentacion PRE On P.IDPresentacion=PRE.ID
Left Outer Join Categoria CA On P.IDCategoria=CA.ID
Left Outer Join Proveedor PRO On P.IDProveedor=PRO.ID
Left Outer Join Division D On P.IDDivision=D.ID
Left Outer Join Pais PA On P.IDProcedencia=PA.ID
Left Outer Join UnidadMedida U On P.IDUnidadMedida=U.ID
Left Outer Join ClasificacionProducto CLA On P.IDClasificacion=CLA.ID
Left Outer Join Impuesto I On P.IDImpuesto=I.ID

--No se puede hacer join con zona, se duplican los productos
--Left Outer Join ProductoZona PZ On P.ID=PZ.IDProducto





'







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CobranzaContado]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CobranzaContado](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[Numero] [int] NOT NULL,
	[IDTransaccionLote] [numeric](18, 0) NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[NroComprobante] [numeric](18, 0) NOT NULL,
	[Comprobante] [varchar](50) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalDescuento] [money] NULL,
	[Anulado] [bit] NOT NULL,
	[FechaAnulado] [date] NULL,
	[IDUsuarioAnulado] [smallint] NULL,
	[IDDistribuidor] [tinyint] NULL,
	[Observacion] [varchar](100) NULL,
	[Procesado] [bit] NOT NULL,
 CONSTRAINT [PK_CobranzaContado] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChequeCliente]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChequeCliente](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[Numero] [int] NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDBanco] [tinyint] NOT NULL,
	[IDCuentaBancaria] [int] NULL,
	[Fecha] [date] NOT NULL,
	[NroCheque] [varchar](50) NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[Cotizacion] [money] NOT NULL,
	[ImporteMoneda] [money] NULL,
	[Importe] [money] NOT NULL,
	[Diferido] [bit] NOT NULL,
	[FechaVencimiento] [date] NULL,
	[ChequeTercero] [bit] NULL,
	[Librador] [varchar](50) NULL,
	[Saldo] [money] NOT NULL,
	[Cancelado] [bit] NOT NULL,
	[Cartera] [bit] NOT NULL,
	[Depositado] [bit] NOT NULL,
	[Rechazado] [bit] NOT NULL,
	[Conciliado] [bit] NULL,
	[IDMotivoRechazo] [tinyint] NULL,
	[SaldoACuenta] [money] NULL,
	[Titular] [varchar](150) NULL,
	[FechaCobranza] [date] NULL,
 CONSTRAINT [PK_ChequeCliente] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FVentaHabilitadaParaCobranzaCredito]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- User Defined Function

CREATE Function [dbo].[FVentaHabilitadaParaCobranzaCredito]
(
	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns bit

As

Begin

	Declare @vRetorno bit
	Set @vRetorno = ''True''

	--Vemos que la venta exista
	If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
		Set @vRetorno = ''False''
	End

	--Vemos si la venta no esta anulada
	If (Select Anulado From Venta Where IDTransaccion=@IDTransaccion) = ''True'' Begin
		Set @vRetorno = ''False''
	End

	--Si ya esta en un lote
	--If Exists(Select * From VentaLoteDistribucion Where IDTransaccionVenta=@IDTransaccion) Begin
	--	Set @vRetorno = ''False''
	--End

	--Si ya esta despachado
	If (Select Despachado From Venta Where IDTransaccion=@IDTransaccion) = ''True'' Begin
		Set @vRetorno = ''False''
	End

	Return @vRetorno

End



' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FVentaEstado]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'Create Function [dbo].[FVentaEstado]
(

	--Entrada
	@IDTransaccion numeric(18,0)
)

Returns varchar(50)

As

Begin

	--Variables
	Declare @vRetorno varchar(50)
	
	Set @vRetorno = ''---''
	
	--Si no se proceso
	If (Select Procesado From Venta Where IDTransaccion=@IDTransaccion) = ''False'' Begin
		Set @vRetorno = ''No se proceso correctamente''
		GoTo Salir
	End
	
	--Si Tiene Saldo 0 y No esta Cancelado
	If (Select Saldo From Venta Where IDTransaccion=@IDTransaccion) = 0 Begin
		If (Select Cancelado From Venta Where IDTransaccion=@IDTransaccion) = ''False'' Begin
			Set @vRetorno = ''No cancelado, saldo=0''
			GoTo Salir
		End
	End
	
	--Si esta anulado
	If (Select Anulado From Venta Where IDtransaccion=@IDtransaccion)= ''True'' Begin
		Set @vRetorno = ''Anuladao''
		Goto Salir
	End
		
Salir:
	Return @vRetorno
End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FPrecioProducto]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'Create Function [dbo].[FPrecioProducto]
(

	--Entrada 
	@IDProducto int,
	@IDCliente int,
	@IDSucursal tinyint

)

Returns money

As

Begin
	
	--Variables
	Declare @vRetorno money
	Declare @vIDListaPrecio int
	Declare @vPrecio money	
			
	Set @vRetorno = 0
	
	--Obtener la lista de precio del cliente
	Set @vIDListaPrecio = (Select IsNull((Select IDListaPrecio From Cliente Where ID=@IDCliente), 0))
		
	--Precio 
	-- Le saque la SUCURSAL, porque no me estaba funcionando para Vender a un cliente de una sucursal a otra.
	--Set @vPrecio = IsNull((Select Precio From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDSucursal=@IDSucursal), 0)
	Set @vPrecio = IsNull((Select Precio From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio), 0)
	
	Set @vRetorno = @vPrecio
	
	Return @vRetorno
	
End


' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FExistenciaProductoReal]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FExistenciaProductoReal]
(

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint
)

Returns decimal(10,2)

As


Begin

	declare @vRetorno decimal(10,2)
	declare @vExistenciaReal decimal(10,2)
	Set @vExistenciaReal = (IsNull((Select Existencia From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto),0))
	Set @vRetorno = @vExistenciaReal 
	return @vRetorno
	
End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FExistenciaProductoValida]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FExistenciaProductoValida]
(

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint,
	@Cantidad decimal(10, 2)
)

Returns bit

As


Begin

	--Variables
	declare @vRetorno bit
	declare @vExistencia decimal(10,2)
	
	--Obtener valores
	Set @vRetorno = ''True''
	Set @vExistencia = (Select IsNull((Select Existencia From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto),0))
	
	--Validar
	If @vExistencia < @Cantidad Begin
		Set @vRetorno = ''False''
	End
	
	return @vRetorno
	
End


' 
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DetalleVenta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DetalleVenta](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[Observacion] [varchar](50) NOT NULL,
	[IDImpuesto] [tinyint] NOT NULL,
	[Cantidad] [decimal](10, 2) NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[PorcentajeDescuento] [numeric](5, 2) NULL,
	[DescuentoUnitario] [money] NULL,
	[DescuentoUnitarioDiscriminado] [money] NULL,
	[TotalDescuento] [money] NULL,
	[TotalDescuentoDiscriminado] [money] NULL,
	[CostoUnitario] [money] NULL,
	[TotalCosto] [money] NULL,
	[TotalCostoImpuesto] [money] NULL,
	[TotalCostoDiscriminado] [money] NULL,
	[Caja] [bit] NULL,
	[CantidadCaja] [decimal](10, 2) NULL,
 CONSTRAINT [PK_DetalleVenta] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[IDProducto] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DetallePedido]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DetallePedido](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[Observacion] [varchar](50) NOT NULL,
	[IDImpuesto] [tinyint] NOT NULL,
	[Cantidad] [decimal](10, 2) NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[PorcentajeDescuento] [numeric](5, 2) NULL,
	[DescuentoUnitario] [money] NULL,
	[DescuentoUnitarioDiscriminado] [money] NULL,
	[TotalDescuento] [money] NULL,
	[TotalDescuentoDiscriminado] [money] NULL,
	[CostoUnitario] [money] NULL,
	[TotalCosto] [money] NULL,
	[TotalCostoImpuesto] [money] NULL,
	[TotalCostoDiscriminado] [money] NULL,
	[Caja] [bit] NULL,
	[CantidadCaja] [decimal](10, 2) NULL,
	[Secuencia] [varchar](50) NULL,
 CONSTRAINT [PK_DetallePedido_1] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[IDProducto] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DetalleNotaDebito](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDTransaccionVenta] [numeric](18, 0) NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[Observacion] [varchar](50) NOT NULL,
	[IDImpuesto] [tinyint] NOT NULL,
	[Cantidad] [decimal](10, 2) NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[CostoUnitario] [money] NULL,
	[DescuentoUnitario] [money] NULL,
	[DescuentoUnitarioDiscriminado] [money] NULL,
	[PorcentajeDescuento] [numeric](2, 0) NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalCostoImpuesto] [money] NULL,
	[TotalCosto] [money] NULL,
	[TotalDescuento] [money] NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalCostoDiscriminado] [money] NULL,
	[TotalDescuentoDiscriminado] [money] NULL,
	[Caja] [bit] NULL,
	[CantidadCaja] [decimal](10, 2) NULL,
 CONSTRAINT [PK_DetalleNotaDebito] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[IDProducto] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DetalleNotaCredito](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDTransaccionVenta] [numeric](18, 0) NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[Observacion] [varchar](50) NOT NULL,
	[IDImpuesto] [tinyint] NOT NULL,
	[Cantidad] [decimal](10, 2) NOT NULL,
	[PrecioUnitario] [money] NOT NULL,
	[CostoUnitario] [money] NULL,
	[DescuentoUnitario] [money] NULL,
	[DescuentoUnitarioDiscriminado] [money] NULL,
	[PorcentajeDescuento] [numeric](2, 0) NULL,
	[Total] [money] NOT NULL,
	[TotalImpuesto] [money] NOT NULL,
	[TotalCostoImpuesto] [money] NULL,
	[TotalCosto] [money] NULL,
	[TotalDescuento] [money] NULL,
	[TotalDiscriminado] [money] NOT NULL,
	[TotalCostoDiscriminado] [money] NULL,
	[TotalDescuentoDiscriminado] [money] NULL,
	[Caja] [bit] NULL,
	[CantidadCaja] [decimal](10, 2) NULL,
 CONSTRAINT [PK_DetalleNotaCredito] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[IDProducto] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END









IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DetalleAsiento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DetalleAsiento](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[ID] [smallint] NOT NULL,
	[IDCuentaContable] [int] NULL,
	[CuentaContable] [varchar](50) NULL,
	[Credito] [money] NOT NULL,
	[Debito] [money] NOT NULL,
	[Importe] [money] NULL,
	[Observacion] [varchar](100) NULL,
	[IDSucursal] [tinyint] NULL,
	[TipoComprobante] [varchar](50) NULL,
	[NroComprobante] [varchar](50) NULL,
 CONSTRAINT [PK_DetalleAsiento_1] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PedidoVenta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PedidoVenta](
	[IDTransaccionPedido] [numeric](18, 0) NOT NULL,
	[IDTransaccionVenta] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_PedidoVenta] PRIMARY KEY CLUSTERED 
(
	[IDTransaccionPedido] ASC,
	[IDTransaccionVenta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PedidoVendedor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PedidoVendedor](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDVendedor] [tinyint] NOT NULL,
	[Numero] [int] NOT NULL,
 CONSTRAINT [PK_PedidoVendedor] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[IDVendedor] ASC,
	[Numero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NotaCreditoVenta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NotaCreditoVenta](
	[IDTransaccionNotaCredito] [numeric](18, 0) NOT NULL,
	[IDTransaccionVentaGenerada] [numeric](18, 0) NOT NULL,
	[ID] [tinyint] NOT NULL,
	[Importe] [money] NOT NULL,
	[cobrado] [money] NOT NULL,
	[Descontado] [money] NOT NULL,
	[Saldo] [money] NOT NULL,
 CONSTRAINT [PK_NotaCreditoVenta_1] PRIMARY KEY CLUSTERED 
(
	[IDTransaccionNotaCredito] ASC,
	[IDTransaccionVentaGenerada] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpDeposito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[SpDeposito]

	--Entrada
	@ID tinyint,
	@Descripcion varchar(50),
	@IDSucursal tinyint,
	@Estado bit,
	@Operacion varchar(10),
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From Deposito Where Descripcion=@Descripcion And IDSucursal=@IDSucursal) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vIDDeposito tinyint
		set @vIDDeposito = (Select IsNull((Max(ID)+1), 1) From Deposito)

		--Insertamos
		Insert Into Deposito(ID, Descripcion, IDSucursal, Estado)
		Values(@vIDDeposito, @Descripcion, @IDSucursal, @Estado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''DEPOSITO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From Deposito Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From Deposito Where Descripcion=@Descripcion And ID!=@ID And IDSucursal=@IDSucursal) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update Deposito Set Descripcion=@Descripcion, 
						IDSucursal=@IDSucursal,
						Estado = @Estado
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''DEPOSITO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From Deposito Where ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una transaccion
		if exists(Select * From Transaccion Where IDDeposito=@ID) begin
			set @Mensaje = ''El registro tiene transacciones asociadas! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene productos asociados
		if exists(Select * From ExistenciaDeposito Where IDDeposito=@ID) begin
			set @Mensaje = ''El registro tiene productos asociadas! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		
		--Eliminamos
		Delete From Deposito 
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''DEPOSITO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpDocumentoAnulado]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpDocumentoAnulado]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDTerminal tinyint
	
As

Begin

	If Exists(Select * From DocumentoAnulado Where IDTransaccion=@IDTransaccion) begin
		return @@rowcount
	End
	
	--Insertar
	Insert Into DocumentoAnulado(IDTransaccion, Fecha, IDUsuario, IDSucursal, IDTerminal)
	values(@IDTransaccion, GETDATE(), @IDUsuario, @IDSucursal, @IDTerminal)
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpProductoListaPrecioExcepciones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpProductoListaPrecioExcepciones]


	--Entrada
	@IDProducto int = NULL,
	@IDListaPrecio tinyint = NULL,
	@IDCliente int = NULL,
	@IDTipoDescuento tinyint = NULL,
	@Porcentaje decimal(5,2) = NULL,
	@Desde date = NULL,
	@Hasta date = NULL,
	@Operacion varchar(10),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @Precio money
	Declare @vDescuento money
	Declare @vIDMoneda tinyint
	
	--Obtener el precio unitario
	Set @vIDMoneda = (Select Min(IDMoneda) From ProductoListaPrecio Where IDListaPrecio=@IDListaPrecio And IDProducto=@IDProducto)
	Set @Precio = (Select Precio From ProductoListaPrecio Where IDListaPrecio=@IDListaPrecio And IDProducto=@IDProducto And IDMoneda=@vIDMoneda)
	
	If @Porcentaje Is Not Null Begin
		Set @vDescuento = Round((@Precio * @Porcentaje) / 100, 0, 0)
	End
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la configuracion ya existe
		if exists(Select * From ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal And IDTipoDescuento=@IDTipoDescuento) begin
			set @Mensaje = ''La configuracion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end

		--si es que los montos son 0
		if @Porcentaje = 0 Begin
			set @Mensaje = ''El descuento no puede ser 0!''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Insertamos
		Insert Into ProductoListaPrecioExcepciones(IDProducto, IDCliente, IDListaPrecio, IDMoneda, IDSucursal, IDTipoDescuento, Descuento, Porcentaje, Desde, Hasta)
		Values(@IDProducto, @IDCliente, @IDListaPrecio, @vIDMoneda, @IDSucursal, @IDTipoDescuento, @vDescuento, @Porcentaje, @Desde, @Hasta)		
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	End
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si es que la configuracion ya existe
		if Not exists(Select * From ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal And IDTipoDescuento=@IDTipoDescuento) begin
			set @Mensaje = ''La configuracion no existe!''
			set @Procesado = ''False''
			return @@rowcount
		end

		--si es que los montos son 0
		if @Porcentaje = 0 Begin
			set @Mensaje = ''El descuento no puede ser 0!''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Actualizar
		Update ProductoListaPrecioExcepciones Set Porcentaje=@Porcentaje,
											Descuento=@vDescuento,
											IDTipoDescuento=@IDTipoDescuento,
											Desde=@Desde,
											Hasta=@Hasta
		Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDCliente=@IDCliente And IDSucursal=@IDSucursal And IDMoneda=@vIDMoneda and IDTipoDescuento=@IDTipoDescuento
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		if Not Exists(Select * From ProductoListaPrecioExcepciones Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio and IDTipoDescuento=@IDTipoDescuento) begin
			set @Mensaje = ''El registro no existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		Delete From ProductoListaPrecioExcepciones 
		Where IDProducto=@IDProducto And IDCliente=@IDCliente And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal And IDMoneda=@vIDMoneda and IDTipoDescuento=@IDTipoDescuento
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpProductoListaPrecio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE Procedure [dbo].[SpProductoListaPrecio]

	--Entrada
	@IDProducto int,
	@IDListaPrecio int,
	@IDMoneda tinyint = 1,
	@Precio money,
	@TPRPorcentaje decimal(10, 3) = NULL,
	@TPRDesde date = NULL,
	@TPRHasta date = NULL,
	 
	@Operacion varchar(10),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	Declare @vTPR money
	Declare @vIDListaPrecio int
	Declare @vPrecioTemp money
		
	If @TPRPorcentaje Is Not Null Begin
		If @Precio != 0 And @TPRPorcentaje != 0 Begin
			Set @vTPR = Round((@Precio * @TPRPorcentaje) / 100, 0, 0)						
		End
	End
	
	--INSERTAR
	If @Operacion=''INS'' Begin
	
		--Verificar que los ID´s ya no esten cargados
		If Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio) Begin
			set @Mensaje = ''El producto y la lista de precio ya estan cargados! Seleccione una lista de precio diferencte.''
			set @Procesado = ''True''
			return @@rowcount
		End
		
		--Monto mayor a 0
		If @Precio <= 0 Begin
			set @Mensaje = ''El precio debe ser mayor a 0!''
			set @Procesado = ''True''
			return @@rowcount
		End
		
		--Insertar
		
		Insert Into ProductoListaPrecio(IDProducto, IDListaPrecio,Precio,IDMoneda, IDSucursal, TPRPorcentaje, TPR, TPRDesde, TPRHasta)
		values(@IDProducto, @IDListaPrecio, @Precio, @IDMoneda, @IDSucursal, @TPRPorcentaje, @vTPR, @TPRDesde, @TPRHasta)
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
	End
	
	--ACTUALIZAR
	If @Operacion=''UPD'' Begin
	
		--Verificar que los ID´s ya no esten cargados
		If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio) Begin
			set @Mensaje = ''El sistema no encontro el registro seleccionado! Favor de verificar la informacion y continuar.''
			set @Procesado = ''True''
			return @@rowcount
		End
		
		--Monto mayor a 0
		If @Precio <= 0 Begin
			set @Mensaje = ''El precio debe ser mayor a 0!''
			set @Procesado = ''True''
			return @@rowcount
		End
		
		--Actualizar
		Update ProductoListaPrecio Set Precio=@Precio,
										TPR=@vTPR,
										TPRPorcentaje=@TPRPorcentaje,
										TPRDesde=@TPRDesde ,
										TPRHasta=@TPRHasta
		Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
	End
	
	--ELIMINAR
	If @Operacion=''DEL'' Begin

		--Verificar que los ID´s ya no esten cargados
		If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio) Begin
			set @Mensaje = ''El sistema no encontro el registro seleccionado! Favor de verificar la informacion y continuar.''
			set @Procesado = ''True''
			return @@rowcount
		End
		
		--Eliminar
		Delete From ProductoListaPrecio
		Where IDProducto=@IDProducto And IDListaPrecio=@IDListaPrecio And IDSucursal=@IDSucursal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount	
	End
		
	--PRECIO
	If @Operacion = ''PRECIO'' Begin
		
		Declare db_cursor cursor for
		Select ID From ListaPrecio Where Estado=''True'' And IDSucursal=@IDSucursal And ID!=@IDListaPrecio
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDListaPrecio
		While @@FETCH_STATUS = 0 Begin  
			
			--Si Existe, actualizamos
			If Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin
			
				Update ProductoListaPrecio Set Precio=@Precio
				Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal
				
			End
			
			--Si no existe la configuracion
			If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin 
			
				Insert Into ProductoListaPrecio(IDProducto, IDListaPrecio, IDSucursal, Precio, IDMoneda)
				values(@IDProducto, @vIDListaPrecio, @IDSucursal, @Precio, @IDMoneda)
				
			End
			
			
			Fetch Next From db_cursor Into @vIDListaPrecio
		End
		
		Close db_cursor   
		Deallocate db_cursor		
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount	
		
	End
	
	--TPR
	If @Operacion = ''TPR'' Begin
		
		Declare db_cursor cursor for
		Select ID From ListaPrecio Where Estado=''True'' And IDSucursal=@IDSucursal And ID!=@IDListaPrecio
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDListaPrecio
		While @@FETCH_STATUS = 0 Begin  
			
			--Obtener el Precio
			Set @vPrecioTemp=(Select Precio From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal)
			
			--Calculamos el TPR
			If @TPRPorcentaje Is Not Null Begin
				If @vPrecioTemp != 0 And @TPRPorcentaje != 0 Begin
					Set @vTPR = Round((@vPrecioTemp * @TPRPorcentaje) / 100, 0, 0)						
				End
			End
				
			--Si Existe, actualizamos
			If Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin
			
				Update ProductoListaPrecio Set  Precio=@vPrecioTemp,
										TPR=@vTPR,
										TPRPorcentaje=@TPRPorcentaje,
										TPRDesde=@TPRDesde ,
										TPRHasta=@TPRHasta
				Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal
				
			End
			
			--Si no existe la configuracion
			If Not Exists(Select * From ProductoListaPrecio Where IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDMoneda=@IDMoneda And IDSucursal=@IDSucursal) Begin
			
				Insert Into ProductoListaPrecio(IDProducto, IDListaPrecio, Precio, IDMoneda, IDSucursal, TPR, TPRPorcentaje, TPRDesde, TPRHasta)
				values(@IDProducto, @vIDListaPrecio, @vPrecioTemp, @IDMoneda, @IDSucursal, @vTPR, @TPRPorcentaje, @TPRDesde, @TPRHasta)
				
			End
			
			
			Fetch Next From db_cursor Into @vIDListaPrecio
		End
		
		Close db_cursor   
		Deallocate db_cursor		
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount	
		
	End
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount
		
End





' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpSincronizarExistenciaDeposito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[SpSincronizarExistenciaDeposito]

	--Entrada
	@IDDeposito int,
	@IDProducto int,
	@Existencia decimal(10,2),
	@ExistenciaMinima decimal(10,2),	
	@ExistenciaCritica decimal(10,2),
	@PlazoReposicion int

As

Begin

	--Variables
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDTransaccion numeric(18,0)
	 
	Set @vMensaje = ''No se proceso''
	Set @vProcesado = ''False''
 
	--Si no existe, Insertar
	If Not Exists(Select * From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto) Begin
		
		Insert Into ExistenciaDeposito(IDDeposito, IDProducto, Existencia, ExistenciaMinima, ExistenciaCritica, PlazoReposicion)
		Values(@IDDeposito, @IDProducto, @Existencia, @ExistenciaMinima, @ExistenciaCritica, 0)
		
		Set @vMensaje = ''Registro procesado!''
		Set @vProcesado = ''True''
		GoTo Salir
		
	End 
 
	--Si existe, Actualizar
	If Exists(Select * From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto) Begin
		
		Update ExistenciaDeposito Set	Existencia=@Existencia, 
										ExistenciaMinima=@ExistenciaMinima, 
										ExistenciaCritica=@ExistenciaCritica, 
										PlazoReposicion=@PlazoReposicion
		Where IDDeposito=@IDDeposito And IDProducto=@IDProducto
		
		Set @vMensaje = ''Registro actualizado!''
		Set @vProcesado = ''True''
		GoTo Salir
		
	End 
	
	
Salir:
	Select ''Mensaje''=@vMensaje, ''Procesado''=@vProcesado	
End	' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpExistenciaDeposito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpExistenciaDeposito]

	--Entrada
	@IDDeposito tinyint,
	@IDProducto int,
	@ExistenciaMinima decimal(10,2),
	@ExistenciaCritica decimal(10,2),
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--Verificar que existe el producto
	If Not Exists(Select * From Producto Where ID=@IDProducto) Begin
		Set @Mensaje = ''El sistema no encontro el producto seleccionado!''
		Set @Procesado = ''False''
		return @@rowcount	
	End
	
	--Verificar que existe el deposito
	If Not Exists(Select * From Deposito Where ID=@IDDeposito) Begin
		Set @Mensaje = ''El sistema no encontro el deposito seleccionado!''
		Set @Procesado = ''False''
		return @@rowcount	
	End		
	
	--Si existe, actualizar
	Declare @vInsertar bit
	Set @vInsertar = ''True''
	
	If Exists(Select * From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto) Begin
		Set @vInsertar = ''False''	
	End
	
	if @vInsertar = ''False'' Begin
		
		Update ExistenciaDeposito Set ExistenciaMinima=@ExistenciaMinima,
										ExistenciaCritica=@ExistenciaCritica
		where IDDeposito=@IDDeposito And IDProducto=@IDProducto
		
		Set @Mensaje = ''Registro actualizado!''
		Set @Procesado = ''True''
		return @@rowcount
	
	End 
	
	If @vInsertar=''True'' Begin
		Insert Into ExistenciaDeposito(IDDeposito, IDProducto, Existencia, ExistenciaMinima, ExistenciaCritica, PlazoReposicion)
		Values(@IDDeposito, @IDProducto, 0, @ExistenciaMinima, @ExistenciaCritica, 0)		
		
		Set @Mensaje = ''Registro guardado!''
		Set @Procesado = ''True''
		return @@rowcount
	
	End
			
	Set @Mensaje = ''No se proceso!''
	Set @Procesado = ''False''
	return @@rowcount
		
End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpClienteSucursal]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpClienteSucursal]

	--Entrada
	@IDCliente int,
	@ID tinyint = NULL,
	@Sucursal varchar(50) = NULL,
	@Direccion varchar(50) = NULL,
	@Contacto varchar(50) = NULL,
	@Telefono varchar(50) = NULL,
	
	@IDPais tinyint = NULL,
	@IDDepartamento tinyint = NULL,
	@IDCiudad smallint = NULL,
	@IDBarrio smallint = NULL,
	@IDVendedor tinyint = NULL,
	@IDZonaVenta tinyint = NULL,
	@IDSucursal tinyint = NULL,
	@IDListaPrecio tinyint = NULL,
	@Contado bit = NULL,
	@Credito bit = NULL,
	@IDCobrador tinyint = NULL,
	@IDEstado tinyint = NULL,
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
	
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Si es que la descripcion ya existe
		if exists(Select * From ClienteSucursal Where Sucursal=@Sucursal And IDCliente=@IDCliente) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From ClienteSucursal Where IDCliente=@IDCliente)

		--Insertamos
		Insert Into ClienteSucursal(IDCliente,  ID,   Sucursal,  Direccion,  Contacto,  Telefono,  Estado, IDPais,  IDDepartamento,  IDCiudad,  IDBarrio,  IDZonaVenta,  IDVendedor, Latitud, Longitud, IDSucursal, IDListaPrecio, Contado, Credito, IDCobrador, IDEstado)
		                    Values (@IDCliente, @vID, @Sucursal, @Direccion, @Contacto, @Telefono, ''True'', @IDPais, @IDDepartamento, @IDCiudad, @IDBarrio, @IDZonaVenta, @IDVendedor, ''0'', ''0'', @IDSucursal, @IDListaPrecio, @Contado , @Credito, @IDCobrador, @IDEstado)		
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''BANCO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From ClienteSucursal Where IDCliente=@IDCliente And ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si es que la descripcion ya existe
		if exists(Select * From ClienteSucursal Where Sucursal=@Sucursal And IDCliente=@IDCliente And ID!=@ID) begin
			set @Mensaje = ''La descripcion ya existe!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update ClienteSucursal Set	Sucursal=@Sucursal, 
									Direccion=@Direccion,
									Contacto = @Contacto,
									Telefono = @Telefono,
									IDDepartamento = @IDDepartamento,
									IDCiudad = @IDCiudad,
									IDBarrio = @IDBarrio,
									IDZonaVenta = @IDZonaVenta,
									IDVendedor = @IDVendedor,
									IDSucursal=@IDSucursal,
									IDListaPrecio= @IDListaPrecio,
									Contado= @Contado,
									Credito= @Credito,
									IDCobrador= @IDCobrador,
									IDEstado= @IDCobrador 
		Where ID=@ID And IDCliente=@IDCliente 
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''BANCO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From ClienteSucursal Where ID=@ID And IDCliente=@IDCliente ) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Si tiene una transaccion
		if exists(Select * From Venta Where IDSucursalCliente=@ID And IDCliente=@IDCliente) begin
			set @Mensaje = ''El registro tiene transacciones asociadas! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ClienteSucursal 
		Where ID=@ID And IDCliente=@IDCliente 
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--Auditoria
	Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''BANCO'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpClienteContacto]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[SpClienteContacto]

	--Entrada
	@IDCliente int,
	@ID tinyint = NULL,
	@Nombres varchar(50) = NULL,
	@Cargo varchar(50) = NULL,
	@Email varchar(50) = NULL,
	@Telefono varchar(50) = NULL,
	
	@Operacion varchar(10),
			
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
As

Begin

	--BLOQUES
	
	--INSERTAR
	if @Operacion=''INS'' begin
		
		--Obtenemos el nuevo ID
		declare @vID tinyint
		set @vID = (Select IsNull((Max(ID)+1), 1) From ClienteContacto Where IDCliente=@IDCliente)

		--Insertamos
		Insert Into ClienteContacto(IDCliente,  ID,   Nombres,  Cargo,  Email,  Telefono)
		Values (@IDCliente, @vID, @Nombres, @Cargo, @Email, @Telefono)		
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ACTUALIZAR
	if @Operacion=''UPD'' begin
		
		--Si el ID existe
		if not exists(Select * From ClienteContacto Where IDCliente=@IDCliente And ID=@ID) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Actualizamos
		Update ClienteContacto Set	Nombres=@Nombres, 
									Cargo=@Cargo,
									Email = @Email,
									Telefono = @Telefono
		Where ID=@ID And IDCliente=@IDCliente 
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	--ELIMINAR
	if @Operacion=''DEL'' begin
		
		--Si el ID existe
		if not exists(Select * From ClienteContacto Where ID=@ID And IDCliente=@IDCliente ) begin
			set @Mensaje = ''El sistema no encuentra el registro solicitado!''
			set @Procesado = ''False''
			return @@rowcount
		end
		
		--Eliminamos
		Delete From ClienteContacto 
		Where ID=@ID And IDCliente=@IDCliente 
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		return @@rowcount
			
	end
	
	set @Mensaje = ''No se realizo ninguna operacion!''
	return @@rowcount

End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpCliente]

	--Entrada
	@ID int,
	
	--Datos Obligatorios
	@RazonSocial varchar(50),
	@RUC varchar(12),
	@CI bit = NULL,
	@Referencia varchar(50),
	@Operacion varchar(10),
		
	--Datos Comerciales
	@NombreFantasia varchar(50) = NULL,
	@Direccion varchar(100) = NULL,
	@Telefono varchar(20) = NULL,
	@IDEstado tinyint = NULL,
	
	--Localizacion
	@IDPais tinyint = NULL,
	@IDDepartamento tinyint = NULL,
	@IDCiudad smallint = NULL,
	@IDBarrio smallint = NULL,
	@IDZonaVenta tinyint = NULL,
	
	--Configuraciones
	@IDListaPrecio tinyint = NULL,
	@IDTipoCliente tinyint = NULL,
	@Contado bit = NULL,
	@Credito bit = NULL,
	@IDMoneda tinyint = NULL,
    @IVAExento bit = NULL,
	@CodigoCuentaContable varchar(50) = NULL,
	        		 
	--De Referencias
	@IDSucursal tinyint = NULL,
	@IDPromotor tinyint = NULL,
	@IDVendedor tinyint = NULL,
	@IDCobrador tinyint = NULL,
	@IDDistribuidor tinyint = NULL,

	--Credito
	@LimiteCredito money = NULL,
	@Descuento money = NULL,
	@PlazoCredito tinyint = NULL,
	@PlazoCobro tinyint = NULL,
	@PlazoChequeDiferido tinyint = NULL,
	
	--Adicionales
	@PaginaWeb varchar(50) = NULL,
	@Email varchar(50) = NULL,
	@Fax varchar(20) = NULL,
	
	--Localizacion
	
	--Auditoria
	@IDUsuario smallint = NULL,
	@IDTerminal smallint = NULL,
				
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As
	
Begin

	--BLOQUES
	Set @Mensaje = ''No se proceso!''
	Set @Procesado = ''False''
	
	--INSERTAR
	If @Operacion=''INS'' Begin
	
		--Validar
		Begin
			--Si es que el RUC ya existe
			If CHARINDEX(''X'', @RUC, 0) = 0 And CHARINDEX(''x'', @RUC, 0) = 0 Begin
				If Exists(Select * From Cliente Where RUC=@RUC And @RUC!='''' And @RUC!='' '') Begin
					Set @Mensaje = ''El RUC ya existe!''
					Set @Procesado = ''False''
					return @@rowcount
				End
			End
			
			--Si es que la Referencia ya existe
			If Exists(Select * From Cliente Where Referencia=@Referencia And IDSucursal=@IDSucursal) Begin
				Set @Mensaje = ''La referencia ya existe!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Direccion
			If @Direccion='''' Begin
				Set @Mensaje = ''La direccion no es correcta!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Telefono
			If @Telefono='''' Begin
				Set @Mensaje = ''El telefono no es correcto!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Sucursal
			If @IDSucursal Is Null Begin
				Set @Mensaje = ''Seleccione una sucursal!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Vendedor
			If @IDVendedor Is Null Begin
				Set @Mensaje = ''Seleccione un vendedor!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Lista de Precio
			If @IDListaPrecio Is Null Begin
				Set @Mensaje = ''Seleccione una lista de precio!''
				Set @Procesado = ''False''
				return @@rowcount
			End			
						
		End
			
		--Solo Crear el Registro con los datos mas importantes,
		--Luego pasamos a actualizar!
		Declare @vID int
		Set @vID = (Select ISNULL(Max(ID)+1, 1) From Cliente)
		
		Insert Into Cliente(ID, RazonSocial, RUC, Referencia, FechaAlta, IDUsuarioAlta)
		values(@vID, @RazonSocial, @RUC, @Referencia, GETDATE(), @IDUsuario)
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''CLIENTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		--Actualizamos el registro
		Set @Operacion=''UPD''
		Set @ID = @vID		
		Set @Mensaje = ''Registro guardado!''
		Set @Procesado = ''True''
		
	End
	
	--ACTUALIZAR
	If @Operacion=''UPD'' Begin
	
		--Validar
		Begin
			--Si es que no existe el Cliente
			If Not Exists(Select * From Cliente Where ID=@ID) Begin
				Set @Mensaje = ''El sistema no encuentra el registro!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Si es que el RUC ya existe - MOMENTANEAMENTE SE PUEDE REPETIR EL RUC!!!!
			If CHARINDEX(''X'', @RUC, 0) = 0 And CHARINDEX(''x'', @RUC, 0) = 0  Begin
				If Exists(Select * From Cliente Where RUC=@RUC And ID!=@ID And @RUC!='''') Begin
					Set @Mensaje = ''El RUC ya existe.....!''
					Set @Procesado = ''False''
					return @@rowcount
				End
			End
			
			--Si es que el RUC ya existe
			If Exists(Select * From Cliente Where Referencia=@Referencia And IDSucursal=@IDSucursal And ID!=@ID) Begin
				Set @Mensaje = ''La referencia ya existe!''
				Set @Procesado = ''False''
				return @@rowcount
			End
		
			--Direccion
			If @Direccion='''' Begin
				Set @Mensaje = ''La direccion no es correcta!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Telefono
			If @Telefono='''' Begin
				Set @Mensaje = ''El telefono no es correcto!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Sucursal
			If @IDSucursal Is Null Begin
				Set @Mensaje = ''Seleccione una sucursal!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Vendedor
			If @IDVendedor Is Null Begin
				Set @Mensaje = ''Seleccione un vendedor!''
				Set @Procesado = ''False''
				return @@rowcount
			End
			
			--Lista de Precio
			If @IDListaPrecio Is Null Begin
				Set @Mensaje = ''Seleccione una lista de precio!''
				Set @Procesado = ''False''
				return @@rowcount
			End
						
		End
		
		--Actualizamos
		--Datos Comerciales
		Update Cliente Set Referencia=@Referencia,
							RazonSocial=@RazonSocial, 
							RUC=@RUC,
							CI=@CI, 
							NombreFantasia=@NombreFantasia, 	
							Direccion=@Direccion,
							Telefono= @Telefono,
							IDEstado=@IDEstado
		Where ID=@ID
		
		--Localizaciones
		Update Cliente Set IDPais=@IDPais, 
						   IDDepartamento=@IDDepartamento,
						   IDCiudad=@IDCiudad,
						   IDBarrio=@IDBarrio ,
						   IDZonaVenta=@IDZonaVenta						   
		Where ID=@ID
		
		--Referencia
		Update Cliente Set IDSucursal=@IDSucursal, 
						   IDPromotor=@IDPromotor,
						   IDVendedor=@IDVendedor,
						   IDCobrador=@IDCobrador,
						   IDDistribuidor=@IDDistribuidor
		Where ID=@ID
		
		--Configuraciones
		Update Cliente Set	IDListaPrecio=@IDListaPrecio,
							IDTipoCliente=@IDTipoCliente,
							Contado=@Contado,
							Credito=@Credito,
							IDMoneda=@IDMoneda,
							IVAExento=@IVAExento,
							CodigoCuentaContable=@CodigoCuentaContable
		Where ID=@ID
		
		--Estadisticos
		Update Cliente Set	FechaModificacion=GETDATE(),
							IDUsuarioModificacion=@IDUsuario
		Where ID=@ID
		
		--Credito
		--Ejecutar ANALISIS DE CREDITO antes de entrar en el bloque
		Update Cliente Set	LimiteCredito=@LimiteCredito,
							Descuento=@Descuento,
							PlazoCobro=@PlazoCobro,
							PlazoCredito=@PlazoCredito,
							PlazoChequeDiferido=@PlazoChequeDiferido
							
		Where ID=@ID
		
		--Adicionales
		Update Cliente Set	PaginaWeb=@PaginaWeb,
							Email=@Email,
							Fax=@Fax
		Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''CLIENTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		Set @Mensaje = ''Registro guardado!''
		Set @Procesado = ''True''
					
	End
	
	--ELIMNAR
	If @Operacion=''DEL'' Begin
	
		--Si es que no existe el Cliente
		If Not Exists(Select * From Cliente Where ID=@ID) Begin
			Set @Mensaje = ''El sistema no encuentra el registro!''
			Set @Procesado = ''False''
			return @@rowcount
		End	
		
		--Si es que tiene ventas
		If Exists(Select * From Venta Where IDCliente=@ID) Begin
			Set @Mensaje = ''El cliente tiene ventas realizadas! No se puede eliminar''
			Set @Procesado = ''False''
			return @@rowcount			
		End	
		
		--Eliminar Cliente Sucursal
		Delete From ClienteSucursal Where IDCliente=@ID
		
		--Eliminar Cliente Contacto
		Delete From ClienteContacto Where IDCliente=@ID
		
		--Eliminar Cliente
		Delete From Cliente Where ID=@ID
		
		--Auditoria
		Exec SpLogSuceso @Operacion=@Operacion, @Tabla=''CLIENTE'', @IDUsuario=@IDUsuario, @IDTerminal=@IDTerminal, @Comprobante=@ID
		
		Set @Mensaje = ''Registro guardado!''
		Set @Procesado = ''True''
		
	End
	
	return @@rowcount
	
End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpActualizarCobranzaCredito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpActualizarCobranzaCredito]

	--Entrada
	@IDTransaccion int,
	@Comprobante varchar(50),
	@IDCobrador int,
	@Observacion varchar(150),
	@NroPlanilla varchar(10),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin
	
	--Si la cobranza esta anulada
	If Exists(Select * From CobranzaCredito Where IDTransaccion=@IDTransaccion And Anulado=''True'') Begin
		set @Mensaje = ''La cobranza ya está anulada! No se puede modificar.''
		set @Procesado = ''False''
		return @@rowcount
	End
		
			
	--Actualizar 
	Update CobranzaCredito  Set NroComprobante=@Comprobante,
								  IDCobrador=@IDCobrador,
								  Observacion =@Observacion,
								  NroPlanilla=@NroPlanilla  
	Where IDTransaccion=@IDTransaccion 
	
			
	set @Mensaje = ''Registro guardado''
	set @Procesado = ''True''
	return @@rowcount
		

End
	
	
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpAcceso]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpAcceso]

	--Entrada
	@Usuario varchar(50),
	@Password varchar(300),
	@IDTerminal smallint,
	@Version varchar(100)
		
As

Begin

	Declare @vIDUsuario int
	Declare @vMensaje varchar(150)
	Declare @vProcesado bit
	
	Set @vMensaje = ''''
	Set @vProcesado = ''False''
	Set @vIDUsuario = 0
	
	--Verificar la version del sistema
	--If (Select Top(1) IsNull(VersionSistema,'''') From Configuraciones) != @Version Begin	
	--	Set @vMensaje = ''La version del sistema que esta utilizando no es la correcta! Version actual: '' + CONVERT(varchar(200), (Select Top(1) IsNull(VersionSistema, '''') From Configuraciones ))
	--	Set @vProcesado = ''false''
	--	GoTo Salir		
	--End
	
	--Si las credenciales no existen
	Set @Password = Convert(varchar(300), HASHBYTES(''Sha1'', @Password))
	--Set @Password = (Select Top(1) [Password] From USuario) 
	
	If Not Exists(Select * From Usuario where Usuario=@Usuario And [Password]=@Password) Begin
	
		--Primera vez
		If (Select COUNT(*) From Usuario) = 0 Begin
			
			--Si el usuario es incorrecto.
			If @Usuario != ''admin'' Begin
				Set @vMensaje = ''El usuario es incorrecto!''
				Set @vProcesado = ''False''
				GoTo Salir
			End
			
			--Cargamos el usuario
			Insert Into Perfil(ID, Descripcion, Estado, Agregar, Modificar, Eliminar, Anular, Visualizar, Imprimir)
			values(1, ''ADMINISTRADOR'', ''True'', ''True'', ''True'', ''True'', ''True'', ''True'', ''True'')
			
			Exec SpUsuario @ID=1, @Nombre=''Administrador'', @Usuario=''admin'', @Password=''12345'', @IDPerfil=1, @Identificador=''ADM'', @Estado=''True'', @Operacion=''INS'', @Mensaje=@vMensaje output, @Procesado=@vProcesado output
			
			Set @vMensaje = ''Acceso correcto!''
			Set @vProcesado = ''True''
			
			Goto Salir
			
		End Else Begin 
			Set @vMensaje = ''Credenciales no validas!''
			Set @vProcesado = ''False''
			GoTo Salir
		End
	End
	
	Set @vIDUsuario = (Select Top(1) ID From Usuario where Usuario=@Usuario And Password=@Password)

	--Que el usuario este activo
	If (Select Estado From Usuario where ID=@vIDUsuario) = ''False'' Begin
		Set @vMensaje = ''Acceso denegado!''
		Set @vProcesado = ''False''
		GoTo Salir
	End
		
	Set @vMensaje = ''Acceso correcto!''
	Set @vProcesado = ''True''
		
Salir:
	If @vProcesado = ''True'' Begin
		Select V.*, ''Mensaje''=@vMensaje, ''Procesado''=@vProcesado From VUsuario V Where V.ID=@vIDUsuario
	End Else Begin
		Select ''Mensaje''=@vMensaje, ''Procesado''=@vProcesado
	End	
End

' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpActualizarProductoDeposito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE Procedure [dbo].[SpActualizarProductoDeposito]

	--Entrada
	@IDDeposito tinyint,
	@IDProducto int,
	@Cantidad decimal(10,2),
	@Signo char(1),
	@ControlStock bit = ''True'',
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output	
	
As

Begin

	--Si no existe el registro, crearlo.
	If Not Exists(Select * From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto) Begin
		
		Insert Into ExistenciaDeposito(IDDeposito, IDProducto, Existencia, ExistenciaMinima, ExistenciaCritica, PlazoReposicion)
		Values(@IDDeposito, @IDProducto, 0, 0, 0, 0)
		
	End
	
	--Obtenemos la cantidad nueva
	Declare @vCantidadNueva decimal(10,2)
	Declare @vCantidadActual decimal (10,2)
	--En ocaciones el calculo sale muy mal,
	--temporalmente obtenemos la cantidad actual de otra forma para ver si se corrige el problema
	Set @vCantidadActual = dbo.FExistenciaProductoReal(@IDProducto, @IDDeposito)
	--Set @vCantidadActual = (IsNull((Select Existencia From ExistenciaDeposito Where IDDeposito=@IDDeposito And IDProducto=@IDProducto),0))
	
	--Verificar la Cantidad
	If @Signo=''-'' Begin
	
		Set @vCantidadNueva = @vCantidadActual - @Cantidad
		
		If @vCantidadNueva < 0 And @ControlStock= ''True'' Begin
			Set @Mensaje = ''Stock insuficiente en deposito para el producto: '' + CONVERT(varchar(50), @IDProducto) + '' ''
			Set @Procesado = ''False''
			print ''[SpActualizarProductoDeposito] Stock insuficiente en deposito para el producto: '' + CONVERT(varchar(50), @IDProducto) + '' ''
			return @@rowcount
		End
		
	End Else Begin
		
		Set @vCantidadNueva = @Cantidad + @vCantidadActual
		
	End
	
	--Actualizar el Deposito
	Update ExistenciaDeposito Set Existencia = @vCantidadNueva
	Where IDDeposito = @IDDeposito And IDProducto = @IDProducto
	
	--Insertar historial
	Declare @vCantidadGuardada decimal(10,2)
	Set @vCantidadGuardada = dbo.FExistenciaProductoReal(@IDProducto, @IDDeposito)
	Insert into ExistenciaDepositoHistorial(IDDeposito, IDProducto,ExistenciaActual,Signo,Cantidad,ExistenciaNueva, CantidadGuardada, Fecha)
	Values (@IDDeposito, @IDProducto, @vCantidadActual, @Signo, @Cantidad, @vCantidadNueva, @vCantidadGuardada, GETDATE())
		
	--Actualizar el Producto
	Declare @vExistenciaGeneral decimal(10,2)
	Set @vExistenciaGeneral = (Select IsNull((Select SUM(Existencia) From ExistenciaDeposito Where IDProducto = @IDProducto), 0))
	
	Update Producto Set ExistenciaGeneral=@vExistenciaGeneral
	Where ID=@IDProducto
		
	Set @Mensaje = ''Registro cargado!''
	Set @Procesado = ''True''
	return @@rowcount

End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpChequeClienteModificar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE Procedure [dbo].[SpChequeClienteModificar]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalDocumento tinyint = NULL,
	@Numero int = NULL,
	@IDCliente int = NULL,
	@IDBanco tinyint = NULL,
	@CuentaBancaria int = NULL,
	@Fecha date = NULL,
	@NroCheque varchar(50) = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Importe money = NULL,
	@ImporteMoneda money = NULL,
	@Diferido bit = NULL,
	@FechaVencimiento date = NULL,
	@ChequeTercero bit = NULL,
	@Librador varchar(50) = NULL,
	@Saldo money = NULL,
	@Operacion varchar(50),
		
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
	
		--ACTUALIZAR
	If @Operacion=''UPD'' Begin
	
		--Validar
	
		--Actualizamos
		Update ChequeCliente Set IDTransaccion=@IDTransaccion ,
								 IDSucursal=@IDSucursal,
								 Numero=@Numero, 
								 IDCliente=IDCliente, 
								 IDBanco=@IDBanco,
								 IDCuentaBancaria=@CuentaBancaria, 
								 Fecha=@Fecha, 
								 NroCheque=@NroCheque, 
								 IDMoneda=@IDMoneda, 
								 Cotizacion=@Cotizacion, 
								 Importe=@Importe, 
								 ImporteMoneda=@ImporteMoneda,
								 Diferido=@Diferido, 
								 FechaVencimiento=@FechaVencimiento, 
								 ChequeTercero=@ChequeTercero, 
								 Librador=@Librador, 
								 Saldo=@Saldo 
		   	
		Where IDTransaccion=@IDTransaccion 
		
		
	
	
	set @Mensaje = ''No se proceso ninguna transaccion!''
	set @Procesado = ''False''
	set @IDTransaccionSalida  = ''0''
	return @@rowcount
		
End


End

' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChequeClienteCobranzaContado]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChequeClienteCobranzaContado](
	[IDTransaccionCheque] [numeric](18, 0) NOT NULL,
	[IDTransaccionVenta] [numeric](18, 0) NOT NULL,
	[IDTransaccionCObranza] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ChequeClienteCobranzaContado] PRIMARY KEY CLUSTERED 
(
	[IDTransaccionCheque] ASC,
	[IDTransaccionVenta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DescuentoPedido]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DescuentoPedido](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDDescuento] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[IDTipoDescuento] [tinyint] NOT NULL,
	[IDActividad] [int] NULL,
	[Descuento] [money] NOT NULL,
	[Porcentaje] [tinyint] NOT NULL,
	[DescuentoDiscriminado] [money] NULL,
 CONSTRAINT [PK_DescuentoPedido] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[IDProducto] ASC,
	[ID] ASC,
	[IDDescuento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Descuento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Descuento](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[IDProducto] [int] NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDDescuento] [tinyint] NOT NULL,
	[IDDeposito] [tinyint] NOT NULL,
	[IDTipoDescuento] [tinyint] NOT NULL,
	[IDActividad] [int] NULL,
	[Descuento] [money] NOT NULL,
	[Porcentaje] [tinyint] NOT NULL,
	[DescuentoDiscriminado] [money] NULL,
	[DecuentoImpuesto] [money] NULL,
 CONSTRAINT [PK_Descuento] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[IDProducto] ASC,
	[ID] ASC,
	[IDDescuento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FExistenciaProductoReservado]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FExistenciaProductoReservado]
(

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint
)

Returns decimal(10,2)

As


Begin

	declare @vRetorno decimal(10,2)
	declare @vReservado decimal(10,2)
	
	Set @vReservado = (IsNull((Select ''Cantidad''=Sum(Cantidad) 
						From DetallePedido DP 
						Join Pedido P On DP.IDTransaccion=P.IDTransaccion
						Where DP.IDProducto=@IDProducto 
						And DP.IDDeposito=@IDDeposito
						And P.Procesado=''True''
						And P.Anulado=''False''
						--And P.Facturado=''False''
						
						--Que este facturado
						And (Case When (Select Max(PV.IDTransaccionVenta) From PedidoVenta PV Where PV.IDTransaccionPedido=P.IDTransaccion) Is Null Then ''False'' Else ''True'' End) = ''False''
						
						--Que el pedido este dentro delos 3 dias proximos
						And DateDiff(d, GetDate(), P.FechaFacturar) Between 0 And 3) ,0))
						
							
	Set @vRetorno = @vReservado
	
	return @vRetorno
	
End

' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FormaPago]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FormaPago](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[ID] [tinyint] NOT NULL,
	[Efectivo] [bit] NOT NULL,
	[ImporteEfectivo] [money] NULL,
	[IDMonedaEfectivo] [tinyint] NULL,
	[ImporteMonedaEfectivo] [money] NULL,
	[Cheque] [bit] NOT NULL,
	[ImporteCheque] [money] NULL,
	[IDTransaccionCheque] [numeric](18, 0) NULL,
	[IDMonedaCheque] [tinyint] NULL,
	[CancelarCheque] [bit] NULL,
	[Documento] [bit] NULL,
	[ImporteDocumento] [money] NULL,
	[IDMonedaDocumento] [tinyint] NULL,
	[ImporteMonedaDocumento] [money] NULL,
	[Importe] [money] NOT NULL,
	[Tarjeta] [bit] NOT NULL,
	[IDTipoTarjeta] [tinyint] NULL,
	[ImporteMonedaTarjeta] [money] NULL,
 CONSTRAINT [PK_FormaPago] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[IDTransaccion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VProductoListaPrecioExcepciones]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VProductoListaPrecioExcepciones]
As
Select 
PLPE.*,
''TipoDescuento''=TD.Descripcion,
''TipoDescuentoCodigo''=TD.Codigo,
''FechaDesde''=IsNull(CONVERT(varchar(50), PLPE.Desde, 5), ''---''),
''FechaHasta''=IsNull(CONVERT(varchar(50), PLPE.Hasta, 5), ''---''),
''Cliente''=C.RazonSocial,
''Moneda''=M.Referencia,
''Sucursal''=S.Descripcion
From ProductoListaPrecioExcepciones PLPE
Join VProducto P On PLPE.IDProducto=P.ID
Join VListaPrecio LP On PLPE.IDListaPrecio=LP.ID
Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID
Join VCliente C On PLPE.IDCliente = C.ID
Join VMoneda M On PLPE.IDMoneda = M.ID
Join Sucursal S On PLPE.IDSucursal=S.ID




'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VExistenciaDeposito]'))
EXEC dbo.sp_executesql @statement = N'







CREATE View [dbo].[VExistenciaDeposito]
As
Select
''IDDeposito''=D.ID,
''Deposito''=D.Descripcion,
''IDSucursal''=D.IDSucursal,
''Sucursal''=S.Descripcion,
''Suc-Dep''=S.Descripcion + '' - '' + D.Descripcion,

--Producto
''IDProducto''=P.ID,
''Producto''=P.Descripcion,
''Referencia''=P.Referencia,
''CodigoBarra''=P.CodigoBarra,
''ControlarExistencia''=P.ControlarExistencia,
P.UnidadMedida ,
P.Estado,
P.UltimaEntrada,


--Clasificacion
P.IDTipoProducto,
P.TipoProducto,
P.IDLinea,
P.Linea,
P.IDSubLinea,
P.IDSubLinea2,
P.IDMarca,
P.Marca,
P.IDPresentacion,
P.IDCategoria,
P.IDProveedor,

--Existencias
''Existencia''=ED.Existencia,
''ExistenciaMinima''=ED.ExistenciaMinima,
''ExistenciaCritica''=ED.ExistenciaCritica,
''ExistenciaGeneral''=P.ExistenciaGeneral,
''PlazoReposicion''=ED.PlazoReposicion,

--Costos
P.Costo,
P.CostoPromedio,
P.UltimoCosto 

From Deposito D
Join Sucursal S On D.IDSucursal=S.ID
Join ExistenciaDeposito ED On D.ID=ED.IDDeposito
Join VProducto P On ED.IDProducto=P.ID







'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VLoteDistribucion]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VLoteDistribucion]
As

Select 

--Cabecera
''Ciudad''=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
LD.Numero,
LD.IDSucursal,
''Sucursal''=S.Descripcion,
''Suc''=S.Codigo,
LD.IDTipoComprobante,
''DescripcionTipoComprobante''=TC.Descripcion,
''TipoComprobante''=TC.Codigo,
''Cod.''=TC.Codigo,
''Comprobante''=LD.Comprobante,
LD.Fecha,
''Fec''=CONVERT(varchar(50), LD.Fecha, 6),
LD.FechaReparto,
''Fec Reparto''=CONVERT(varchar(50), LD.FechaReparto, 6),
LD.Observacion,

LD.IDDistribuidor,
''Distribuidor''=D.Nombres,
LD.IDCamion,
''Camion''=C.Descripcion,
C.Patente ,
LD.IDChofer,
''Chofer''=CH.Nombres,
''CI''=CH.NroDocumento ,
CH.Direccion ,
LD.IDZonaVenta,
''Zona''=ISNULL(ZV.Descripcion, ''---''),

TR.ID,
''Transporte''=TR.Descripcion,

--Totales
LD.TotalContado,
LD.TotalCredito,
LD.Total,
LD.TotalImpuesto,
LD.TotalDiscriminado,
LD.TotalDescuento,
''CantidadComprobantes''=(Select Count(*) From VentaLoteDistribucion VLD Where VLD.IDTransaccionLote=LD.IDTransaccion),

--Transaccion
LD.IDTransaccion,
''FechaTransaccion''=T.Fecha,
''IDDepositoTransaccion''=T.IDDeposito,
''IDSucursalTransaccion''=T.IDSucursal,
''IDTerminalTransaccion''=T.IDTerminal,
T.IDUsuario,
''usuario''=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
''UsuarioIdentificador''=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
LD.Anulado,
''Estado''=Case When LD.Anulado=''True'' Then ''Anulado'' Else ''---'' End,
''IDUsuarioAnulacion''=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=LD.IDTransaccion), 0)),
''UsuarioAnulacion''=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=LD.IDTransaccion), ''---'')),
''UsuarioIdentificacionAnulacion''=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=LD.IDTransaccion), ''---'')),
''FechaAnulacion''=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=LD.IDTransaccion)

From LoteDistribucion LD
Join Transaccion T On LD.IDTransaccion=T.ID
Left Outer Join Sucursal S On LD.IDSucursal=S.ID
Left Outer Join TipoComprobante TC On LD.IDTipoComprobante=TC.ID
Left Outer Join Distribuidor D On LD.IDDistribuidor=D.ID
Left Outer Join Camion C On LD.IDCamion=C.ID
Left Outer Join Chofer CH On LD.IDChofer=CH.ID
Left Outer Join ZonaVenta ZV On LD.IDZonaVenta=ZV.ID
Left Outer Join Transporte TR On TR.IDDistribuidor=LD.IDDistribuidor And TR.IDCamion=C.ID And TR.IDChofer=CH.ID And TR.IDZonaVenta=ZV.ID

'





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VentaCobranza]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VentaCobranza](
	[IDTransaccionVenta] [numeric](18, 0) NOT NULL,
	[IDTransaccionCobranza] [numeric](18, 0) NOT NULL,
	[Importe] [money] NOT NULL,
	[Cancelar] [bit] NOT NULL,
 CONSTRAINT [PK_VentaReciboDinero] PRIMARY KEY CLUSTERED 
(
	[IDTransaccionVenta] ASC,
	[IDTransaccionCobranza] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END





IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TGDetalleNotaCreditoDevolucion]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[TGDetalleNotaCreditoDevolucion] ON [dbo].[DetalleNotaCredito] 
AFTER INSERT,DELETE
AS 
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

END
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetalleImpuestoDesglosadoTotales]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VDetalleImpuestoDesglosadoTotales]

As

Select 
IDTransaccion, 
''GRAVADO10%''=IsNull([1],0), 
''GRAVADO5%''=IsNull([2],0), 
''EXENTO''=IsNull([3],0),
''DISCRIMINADO10%''=IsNull([11],0),
''IVA10%''=IsNull([21],0),
''DISCRIMINADO5%''=IsNull([12],0),
''IVA5%''=IsNull([22],0)

From (SELECT IDTransaccion, 
			IDImpuesto, 
			IDImpuesto+10 as IDImpuesto2,
			IDImpuesto+20 as IDImpuesto3,
			SUM(Total) As Total, 
			Sum(TotalDiscriminado) as TotalDiscriminado,
			Sum(TotalImpuesto) as TotalImpuesto
FROM VDetalleImpuesto Group By IDTransaccion, IDImpuesto) AS SourceTable


PIVOT
(
	Sum(Total)
	FOR IDImpuesto IN ([1], [2], [3])
) 

AS P1

PIVOT
(
	Sum(TotalDiscriminado)
	FOR IDImpuesto2 IN ([11], [12])
) 

AS P2

PIVOT
(
	Sum(TotalImpuesto)
	FOR IDImpuesto3 IN ([21], [22])
) 

AS P3




'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetalleImpuestoDesglosadoGravado]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VDetalleImpuestoDesglosadoGravado]

As

Select IDTransaccion, ''GRAVADO10%''=IsNull([1],0), ''GRAVADO5%''=IsNull([2],0), ''EXENTO''=IsNull([3],0)
From (SELECT IDTransaccion, IDImpuesto, Total
    FROM VDetalleImpuesto) AS SourceTable
PIVOT
(
Sum(Total)
FOR IDImpuesto IN ([1], [2], [3])
) AS PivotTable;'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetalleImpuestoDesglosado]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VDetalleImpuestoDesglosado]
As
Select IDTransaccion, ''IVA10%''=IsNull([1],0), ''IVA5%''=IsNull([2],0), ''EX''=IsNull([3],0)
From (SELECT IDTransaccion, IDImpuesto, TotalImpuesto
    FROM VDetalleImpuesto) AS SourceTable
PIVOT
(
Sum(TotalImpuesto)
FOR IDImpuesto IN ([1], [2], [3])
) AS PivotTable;'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetalleAsiento]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VDetalleAsiento]
As
Select 
DA.IDTransaccion,
DA.IDCuentaContable,
''Codigo''=(IsNull(CC.Codigo, DA.CuentaContable)),
''Descripcion''=IsNull(CC.Descripcion, ''---''),
''Cuenta''=IsNull(CC.Codigo, DA.CuentaContable) + '' - '' + IsNull(CC.Descripcion, ''---''),
''Alias''=IsNull(CC.Alias, ''---''),
DA.ID,
DA.Credito,
DA.Debito,
DA.Importe,
''Observacion''=(Case When DA.Observacion Is Null Then A.Detalle Else (Case When DA.Observacion != '''' Then DA.Observacion Else A.Detalle End) End),
''Orden''=DA.ID,
S.Codigo as CodSucursal,
M.Referencia as Moneda,
A.Fecha,

DA.IDSucursal,
DA.TipoComprobante,
''NroComprobante''=(Case When (DA.NroComprobante) Is Null Then A.NroComprobante 
					   When (DA.NroComprobante) = '''' Then A.NroComprobante 
					   Else (DA.NroComprobante) End)

From DetalleAsiento DA
Join Asiento A On DA.IDTransaccion=A.IDTransaccion
Left Outer Join VCuentaContable CC On DA.CuentaContable=CC.Codigo 
Join Sucursal S on A.IDSucursal=S.ID
Join Moneda M on A.IDMoneda=M.ID

Where CC.PlanCuentaTitular=''True''


'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetallePedido]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VDetallePedido]
As
Select
DP.IDTransaccion,

--Producto
DP.IDProducto,
DP.ID,
''Producto''=P.Descripcion,
''Descripcion''=(Case When DP.Observacion='''' Then P.Descripcion Else P.Descripcion + '' - '' + DP.Observacion End),
''CodigoBarra''=P.CodigoBarra,
''UnidadMedida''=P.UnidadMedida,
P.Referencia,
DP.Observacion,

--Deposito
DP.IDDeposito,
''Deposito''=D.Descripcion,

--Cantidad y Precio
DP.Cantidad,
DP.PrecioUnitario,
''Pre. Uni.''=DP.PrecioUnitario,
DP.Total,

--Impuesto
DP.IDImpuesto,
''Impuesto''=I.Descripcion,
''Ref Imp''=I.Referencia,
DP.TotalImpuesto,
DP.TotalDiscriminado,
''Exento''=I.Exento,

--Descuento
DP.TotalDescuento,
''Descuento''=DP.TotalDescuento,
DP.DescuentoUnitario,
''Desc Uni''=DP.DescuentoUnitario,
DP.DescuentoUnitarioDiscriminado,
DP.TotalDescuentoDiscriminado,
DP.PorcentajeDescuento,
''Desc %''=DP.PorcentajeDescuento,
''PrecioNeto''=IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0),
''TotalPrecioNeto''=(IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0)) * DP.Cantidad,

--Costos
DP.CostoUnitario,
DP.TotalCosto,
DP.TotalCostoImpuesto,
DP.TotalCostoDiscriminado,

----Cantidad de Caja
''Caja''=IsNull(DP.Caja, ''False''),
DP.CantidadCaja,
''Cajas''=convert (decimal (10,2) ,IsNull((DP.Cantidad / P.UnidadPorCaja), 0)),
''UnidadMedidas''= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),''UND''),
--Unidad de medida
''UnidadMedidaConvertir''= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),''UND''),
''UnidadConvertir''=Isnull (P.UnidadConvertir,1),

--Totales
''TotalBruto''= isnull(DP.TotalDescuento,0)+isnull(DP.Total,0),
''TotalSinDescuento''=DP.Total - DP.TotalDescuento,
''TotalSinImpuesto''=DP.Total - DP.TotalImpuesto,
''TotalNeto''=DP.Total - (DP.TotalImpuesto + DP.TotalDescuento),
''TotalNetoConDescuentoNeto''=DP.TotalDiscriminado + DP.TotalDescuentoDiscriminado,

--Pedido
PE.Fecha,
PE.IDCliente


From DetallePedido DP
Join Pedido PE On DP.IDTransaccion=PE.IDTransaccion
Join VProducto P On DP.IDProducto=P.ID
Join Deposito D On DP.IDDeposito=D.ID
Join Impuesto I On DP.IDImpuesto=I.ID

Where PE.Procesado=''True''


'





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpViewProducto]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE Procedure [dbo].[SpViewProducto]

	--Opciones
		
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint
	
As

Begin


	Select 
	ID, 
	Descripcion, 
	CodigoBarra, 
	Ref,
	ProductoReferencia, 
	UnidadPorCaja,
	UnidadMedida, 
	''UnidadConvertir''=convert (decimal (10,3),UnidadConvertir ),
	UnidadMedidaConvertir,
	IDImpuesto, 
	Impuesto, 
	Costo,
	UltimoCosto,
	[Ref Imp],
	''Precio''=0,
	Exento,
	ControlarExistencia,
	Estado,
	
	--Descuentos
	''TotalDescuento''=0,
	''DescuentoUnitario''=0,
	''PorcentajeDescuento''=0,
	
	--Clasificaciones
	IDClasificacion,
	Clasificacion,
	CodigoClasificacion,
	NivelClasificacion,
	NumeroClasificacion,
	
	IDTipoProducto,
	IDLinea,
	IDSubLinea,
	IDSubLinea2,
	IDMarca,
	IDPresentacion,
	
	--''Existencia''=dbo.FExistenciaProducto(ID, @IDDeposito),
	''Existencia''=0,
	
	--CuentaContable
	CodigoCuentaVenta,
	CodigoCuentaCompra
		 
	From VProducto 
	Where Estado=''True''
			
	Order By Descripcion
		
	
End


' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpViewCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE Procedure [dbo].[SpViewCliente]

	--Opciones
		
	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint
	
As

Begin

	Declare @vEsVendedor bit
	Declare @vIDvendedor int
	
	Set @vEsVendedor = (Select EsVendedor From Usuario Where ID=@IDUsuario)
	If @vEsVendedor = ''True'' Begin
		Set @vIDvendedor = (Select IDVendedor From Usuario Where ID=@IDUsuario)
	End
	
	If @vEsVendedor = ''False'' Begin
		Select 
		ID, 
		Referencia,
		RazonSocial, 
		RUC, 
		RazonSocialReferencia,
		NombreFantasia, 
		Direccion, 
		Telefono, 
		ListaPrecio, 
		Descuento, 
		Deuda, 
		SaldoCredito, 
		Vendedor, 
		Promotor, 
		Cobrador, 
		Ciudad, 
		Barrio, 
		ZonaVenta, 
		TieneSucursales,
		Condicion,
		IDMoneda,
		IDListaPrecio,
		CodigoCuentaContable,
		PlazoCredito,
		IDSucursal,
		LimiteCredito 
		
		From VCliente
		Where Estado=''ACTIVA''
		And (IDSucursal=@IDSucursal Or (Case When (Select Top(1) ID From ClienteSucursal Where IDCliente=VCliente.ID And IDSucursal=@IDSucursal) Is Null Then ''False'' Else ''True'' End) = ''True'')
		--And DateDiff(day, GetDate(), UltimaCompra) < 60 
		Order By RazonSocial
	End
	
	If @vEsVendedor = ''True'' Begin
		Select 
		ID, 
		Referencia,
		RazonSocial, 
		RUC, 
		NombreFantasia, 
		Direccion, 
		Telefono, 
		ListaPrecio, 
		Descuento, 
		Deuda, 
		SaldoCredito, 
		Vendedor, 
		Promotor, 
		Cobrador, 
		Ciudad, 
		Barrio, 
		ZonaVenta, 
		TieneSucursales,
		Condicion,
		IDMoneda,
		IDListaPrecio,
		CodigoCuentaContable,
		PlazoCredito,
		IDSucursal 
		
		From VCliente
		Where Estado=''ACTIVA''
		And (IDSucursal=@IDSucursal 
		Or (Case When 
		(Select Top(1) ID From ClienteSucursal 
		Where IDCliente=VCliente.ID And IDSucursal=@IDSucursal) Is Null Then ''False'' Else ''True'' End) = ''True'')
		And IDVendedor=@vIDvendedor
		--And DateDiff(day, GetDate(), UltimaCompra) < 60 
				
		Union All
		
		Select 
		ID, 
		Referencia,
		RazonSocial, 
		RUC, 
		NombreFantasia, 
		Direccion, 
		Telefono, 
		ListaPrecio, 
		Descuento, 
		Deuda, 
		SaldoCredito, 
		Vendedor, 
		Promotor, 
		Cobrador, 
		Ciudad, 
		Barrio, 
		ZonaVenta, 
		TieneSucursales,
		Condicion,
		IDMoneda,
		IDListaPrecio,
		CodigoCuentaContable,
		PlazoCredito,
		IDSucursal 
		
		From VCliente
		Where Estado=''ACTIVA''
		And  (Case When (Select Top(1) ID From ClienteSucursal CS
					   Where CS.IDCliente=VCliente.ID And CS.IDSucursal=@IDSucursal And CS.IDVendedor=@vIDvendedor) 
		     Is Null Then ''False'' Else ''True'' End) = ''True''
		  
		 
		--And DateDiff(day, GetDate(), UltimaCompra) < 60 
		Order By RazonSocial
		
	End
		
	
End









' 
END





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VVenta]'))
EXEC dbo.sp_executesql @statement = N'




CREATE View [dbo].[VVenta]
As
Select
V.IDTransaccion,

--Punto de Expedicion
V.IDPuntoExpedicion,
''PE''=PE.Descripcion,
PE.Ciudad,
PE.[CiudadDesc.],
PE.IDCiudad,
PE.IDSucursal,
PE.Sucursal,
PE.ReferenciaSucursal,
PE.ReferenciaPunto,
PE.Timbrado,

V.IDTipoComprobante,
''DescripcionTipoComprobante''=TC.Descripcion,
''TipoComprobante''=TC.Codigo,
''Cod.''=TC.Codigo,
V.NroComprobante,
''Comprobante''=V.Comprobante,

--Cliente
V.IDCliente,
''Cliente''=C.RazonSocial,
''ReferenciaCliente''=C.Referencia,
C.RUC,
V.IDSucursalCliente,
''SucursalCliente''=(Case When (V.IDSucursalCliente) IS Null Then ''MATRIZ'' Else (Case When (V.IDSucursalCliente) = 0 Then ''MATRIZ'' Else (Select Top(1) CS.Sucursal From ClienteSucursal CS Where V.IDCliente=CS.IDCliente And CS.ID=V.IDSucursalCliente) End) End),
''IDCiudadCliente''=C.IDCiudad,
V.Direccion,
''Telefono''=(Case When (C.Telefono) = '''' Then ''X'' Else C.Telefono End),
C.IDZonaVenta,
C.IDTipoCliente,
''TipoCliente''=IsNull(TP.Descripcion, ''---''),
C.Referencia,
C.IDCobrador ,
C.Cobrador ,
C.IDEstado,
C.Estado ,

--Opciones
V.IDDeposito,
''Deposito''=D.Descripcion,
V.FechaEmision,
''Fec''=CONVERT(varchar(50), V.FechaEmision, 3),
''Fecha''=V.FechaEmision,
V.Credito,
''CreditoContado''=(Case When (V.Credito) = ''True'' Then ''CREDITO'' Else ''CONTADO'' End),
''Condicion''=(Case When (V.Credito) = ''True'' Then ''CRED'' Else ''CONT'' End),
''Operacion''=convert(varchar(50),TC.Codigo) +''   ''+convert(Varchar(50),V.NroComprobante)+''  ''+ (Case When (V.Credito) = ''True'' Then ''CRED'' Else ''CONT'' End),
V.FechaVencimiento,
''Fec. Venc.''= (Case When V.Credito=''True'' Then (CONVERT(varchar(50), V.FechaVencimiento, 3)) Else ''---'' End),
V.Observacion,
V.IDMoneda,
''Moneda''=M.Referencia,
''DescripcionMoneda''=M.Descripcion,
V.Cotizacion,
V.Anulado,

--Otros
V.IDListaPrecio,
''Lista de Precio''=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=V.IDListaPrecio), ''---''),
''Descuento''=ISNULL(V.Descuento, 0),
V.IDVendedor,
''Vendedor''=ISNULL((Select VE.Nombres From Vendedor VE Where VE.ID=V.IDVendedor), ''---''),
V.IDPromotor,
''Promotor''=ISNULL((Select PRO.Nombres From Promotor PRO Where PRO.ID=V.IDPromotor), ''---''),
V.NroComprobanteRemision,
''Remision''=V.NroComprobanteRemision,
V.EsVentaSucursal,

--Totales
V.Total,
V.TotalImpuesto,
V.TotalDiscriminado,
V.TotalDescuento,
''TotalBruto''=isnull(V.Total,0) + isnull(V.TotalDescuento,0),

--Credito
V.Saldo,
V.Cobrado,
V.Descontado,
V.Cancelado,
V.Acreditado,

--Transaccion
''FechaTransaccion''=T.Fecha,
''IDDepositoTransaccion''=T.IDDeposito,
''IDSucursalTransaccion''=T.IDSucursal,
''IDTerminalTransaccion''=T.IDTerminal,
T.IDUsuario,
''usuario''=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
''UsuarioIdentificador''=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Estado
V.Procesado,
''EstadoVenta''=dbo.FVentaEstado(V.IDTransaccion),

--Anulacion
''IDUsuarioAnulacion''=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion), 0)),
''UsuarioAnulacion''=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=V.IDTransaccion), ''---'')),
''UsuarioIdentificacionAnulacion''=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=V.IDTransaccion), ''---'')),
''FechaAnulacion''=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion),

--Mes y Año
''DiaP''=dbo.FFormatoDosDigitos(DatePart(dd,V.FechaEmision )),
''MesP''=dbo.FMes(DatePart(MM,V.FechaEmision )),
''Año''= Convert (Varchar (50),YEAR(V.FechaEmision)),
''Fec.Vencimiento''= Convert (Varchar (50),YEAR (V.FechaVencimiento))+Convert (Varchar(50),dbo.FFormatoDosDigitos (DATEPART (MM,V.FechaVencimiento)))+Convert (Varchar (50), dbo.FFormatoDosDigitos (DATEPART (DD,V.FechaVencimiento ))),

V.Autoriza,
''IDMotivo''=MAV.ID,
''MotivoAnulacion''=ISNULL(MAV.Descripcion,''---''),

--Direccion Alternativa
''DireccionAlternativa''=(Case When V.EsVentaSucursal=''False'' Then C.RazonSocial Else C.RazonSocial + '' - '' + (Case When (V.IDSucursalCliente) IS Null Then ''MATRIZ'' Else (Case When (V.IDSucursalCliente) = 0 Then ''MATRIZ'' Else (Select Top(1) CS.Sucursal From ClienteSucursal CS Where V.IDCliente=CS.IDCliente And CS.ID=V.IDSucursalCliente) End) End) End),

--Condicion Contado
''Contado''=(Case When (V.Credito) = ''True'' Then ''False'' Else ''True'' End)

From Venta V
Join Transaccion T On V.IDTransaccion=T.ID
Left Outer Join VPuntoExpedicion PE On V.IDPuntoExpedicion=PE.ID
Left Outer Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join VCliente C On V.IDCliente=C.ID
Left Outer Join TipoCliente TP on TP.ID=C.IDTipoCliente
Left Outer Join Sucursal S On V.IDSucursal=S.ID
Left Outer Join Deposito D On V.IDDeposito=D.ID
Left Outer Join Moneda M On V.IDMoneda=M.ID
Left Outer Join MotivoAnulacionVenta MAV On MAV.ID=V.IDMotivo

Where V.Procesado=''True''










'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VVentasPendientes]'))
EXEC dbo.sp_executesql @statement = N'






--USE [EXPRESS]
--

--/****** Object:  View [dbo].[VVentasPendientes]    Script Date: 07/03/2012 09:29:05 ******/
--
--

--
--


CREATE View [dbo].[VVentasPendientes]

As

Select
V.IDCliente,
V.Cliente,
V.IDTransaccion,
''Sel''=''False'',
V.Comprobante,
''Tipo''=V.TipoComprobante,
V.Condicion,
V.Credito,
V.FechaEmision,
V.Fec,
''Vencimiento''=V.[Fec. Venc.],
V.FechaVencimiento,
V.Total,
V.Cobrado,
V.Descontado,
V.Acreditado ,
V.Saldo,
''Importe''=V.Saldo,
''Cancelar''=''False'',
V.IDMoneda,
V.Moneda,
V.Cotizacion,
V.IDSucursal,
V.IDTipoComprobante

From VVenta V
Where V.Cancelado = ''False''
And V.Anulado=''False''











'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VVentaLoteDistribucion]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VVentaLoteDistribucion]

As

Select
VLD.IDTransaccionLote,
VLD.IDTransaccionVenta,
VLD.ID,
V.IDSucursal,
''Suc''=V.Sucursal,
V.[Cod.],
V.Comprobante,
''FechaFact''=V.Fec,
''VencimientoFact''= Case When V.Condicion = ''CONT'' Then V.Fec  else V.[Fec. Venc.] End ,

V.[Fec. Venc.],
''Saldo''=VLD.Importe,
V.Condicion,
V.Moneda,
V.Cotizacion,
V.Vendedor,

--Lote
LD.Numero,
''CiudadLote''=LD.Ciudad,
LD.IDCiudad ,
''SucLote''=LD.Suc ,
''FechaLote''=LD.Fec,
LD.[Fec Reparto], 
LD.IDDistribuidor,
LD.Distribuidor,
LD.IDCamion,
LD.Camion ,
LD.Chofer,
LD.IDChofer,
V.IDCliente,
''Cliente''=V.Cliente,
V.Referencia,
''Suc. Cliente''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.Sucursal From ClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else ''MATRIZ'' End),
''ZonaVenta''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.ZonaVenta From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.ZonaVenta End),
''Ciudad''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.Ciudad From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Ciudad End),
''Barrio''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.Barrio From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Barrio End),
''Direccion''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.Direccion From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Direccion End),
''Telefono''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.Telefono From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Telefono End),
''Contacto''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.Contacto From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.NombreFantasia End),

''Latitud''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.Latitud From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Latitud End),
''Longitud''=(Case When (V.EsVentaSucursal)=''True'' Then (Select CS.Longitud From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Longitud End),

''IDSucursalCliente''= (Case When (V.EsVentaSucursal)=''True'' Then V.IDSucursalCliente Else 1 End),
''IDUsuario''= LD.IDUsuario,
''Usuario''= LD.Usuario,

--Totales
''Contado''=Case When V.Credito=''True'' Then 0 Else VLD.Importe End,
''Credito''=Case When V.Credito=''True'' Then VLD.Importe Else 0 End,
''Total''=V.Total,
''TotalImpuesto''=V.TotalImpuesto,
''TotalDiscriminado''=V.TotalDiscriminado,
''TotalDescuento''=V.TotalDescuento,
''SaldoComprobante''=V.Saldo


From VentaLoteDistribucion VLD
Join VVenta V On VLD.IDTransaccionVenta=V.IDTransaccion
Join VCliente C On V.IDCliente = C.ID
Join VLoteDistribucion LD on LD.IDTransaccion=VLD.IDTransaccionLote '





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VVentaDetalleCobranzaCredito]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VVentaDetalleCobranzaCredito]
As
Select
V.IDTransaccion,
V.[Cod.],
V.IDSucursal,
V.Sucursal,
V.IDDeposito,
CC.IDCobrador,
CC.Cobrador,
V.IDVendedor,
V.Vendedor,
V.Comprobante,
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.Fec,
V.FechaVencimiento,
''Vencimiento''=V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,
VC.Importe,
V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
VC.IDTransaccionCobranza,

--Cobranza
CC.Numero,
''FechaCobranza''=CC.FechaEmision,
''Recibo''=CC.Comprobante,
CC.NroPlanilla,

--Para asiento
V.IDMoneda,
V.IDTipoComprobante,

''CobranzaAnulada''=CC.Anulado 



From VentaCobranza VC
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join VCobranzaCredito CC On VC.IDTransaccionCobranza = CC.IDTransaccion












'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VVentaDetalleCobranzaContado]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VVentaDetalleCobranzaContado]
As
Select
V.IDTransaccion,
V.IDSucursal,
V.Sucursal,
V.IDDeposito,
V.Comprobante,
V.[Cod.],
V.IDCliente,
V.Cliente,
V.ReferenciaCliente,
V.IDCobrador,
V.IDVendedor,
V.Referencia,
V.Fecha,
V.Fec,
V.FechaVencimiento,
V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,
V.Cobrador,
V.Vendedor,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,
VC.Importe,
V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
VC.IDTransaccionCobranza,
''Lote''=LD.Comprobante, 

--Cobranza
CC.Numero,
''FechaCobranza''=CC.Fecha,
''ComprobanteCobranza''=CC.Comprobante,


--Para asiento
V.IDMoneda,
V.IDTipoComprobante,

''CobranzaAnulada''=CC.Anulado 



From VentaCobranza VC
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join CobranzaContado CC On VC.IDTransaccionCobranza = CC.IDTransaccion
Join LoteDistribucion LD on LD.IDTransaccion=CC.IDTransaccionLote 































'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VVentaDetalleCobranza]'))
EXEC dbo.sp_executesql @statement = N'



CREATE view [dbo].[VVentaDetalleCobranza]

as 

Select
V.IDTransaccion,
V.IDSucursal,
V.Comprobante,
V.[Cod.],
V.IDCliente,
V.Cliente,
V.ReferenciaCliente,
V.Fecha ,
V.Fec ,
V.FechaVencimiento,
V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,

--Si es anulado, Importe = 0, si es necesario agregar el valor real, crear otro campo
''Importe''=(Case When(CC.Anulado) = ''False'' Then VC.Importe Else 0 End),
''ImporteReal''=VC.Importe ,
V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
VC.IDTransaccionCobranza,

--Cobranza
CC.Numero,
''FechaCobranza''= CC.FechaEmision ,
''Fec. Cob.''= CC.Fec,
''T. Comp.''= CC.TipoComprobante + '' ('' + CONVERT(varchar(50), CC.Numero) + '')'',
''Comp. Cob.''=CC.Comprobante,
''Estado Cob''=CC.Estado,
''AnuladoCobranza''=CC.Anulado,
CC.Cobrador,
''Usuario''=CC.usuario,
''Observacion''=CC.Observacion,
CC.NroPlanilla,

--Para asiento
V.IDMoneda,
V.IDTipoComprobante

From VentaCobranza VC
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join VCobranzaCredito CC On VC.IDTransaccionCobranza = CC.IDTransaccion
 

 










'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VChequeClienteDocumentosPagados]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VChequeClienteDocumentosPagados]
As
Select
CC.IDTransaccion,
''Numero''=CC.Numero,
''TipoComprobante''=TC.Descripcion,
CCRE.NroComprobante,
CC.NroCheque,
''Comprobante''=TC.Codigo + '' - '' + CCRE.Comprobante,
''Fecha''=CCRE.FechaEmision,
''Fec''=CONVERT(varchar(50), CCRE.FechaEmision, 6),
''ImporteCheque''=FP.ImporteCheque,
''Importe''=FP.ImporteCheque
 
From ChequeCliente CC
Join FormaPago FP On CC.IDTransaccion=FP.IDTransaccionCheque
Join CobranzaCredito CCRE On FP.IDTransaccion=CCRE.IDTransaccion
Join TipoComprobante TC On CCRE.IDTipoComprobante=TC.ID




'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VChequeCliente]'))
EXEC dbo.sp_executesql @statement = N'


CREATE View [dbo].[VChequeCliente]
As

Select 
CHQ.IDTransaccion,
CHQ.Numero,
CHQ.IDSucursal,
''Ciudad''=S.CodigoCiudad,

--Cliente
CHQ.IDCliente,
''Cliente''=C.RazonSocial,
''CodigoCliente''=C.Referencia,
C.RUC,
''Emitido Por''= Case When CHQ.Librador is Null Then  C.RazonSocial    Else chq.Librador End ,

--Banco
CHQ.IDBanco,
''Banco''=B.Descripcion,
''IDSucursalBanco''=B.IDSucursal,
''BancoLocal''=(Case When CHQ.IDSucursal=B.IDSucursal Then ''True'' Else ''False''End),
''CodigoBanco''=B.Referencia,

--CuentaBancaria
CHQ.IDCuentaBancaria,
CCB.CuentaBancaria,

CHQ.Fecha,
''Fec''=CONVERT(varchar(50), CHQ.Fecha, 5),
CHQ.NroCheque,
CHQ.IDMoneda,
''Moneda''=M.Referencia,
CHQ.Cotizacion,
''ImporteMoneda''= (Case When S.ID=CHQ.IDSucursal Then CHQ.ImporteMoneda Else 0 End),
''Importe''=(Case When S.ID=CHQ.IDSucursal Then CHQ.Importe Else 0 End),
''ImporteOtroCheque''=(Case When S.ID<> CHQ.IDSucursal Then CHQ.Importe Else 0 End),
''Total''=CHQ.Importe,
CHQ.Diferido,
''Tipo''=Case When(CHQ.Diferido) = ''True'' Then ''DIFERIDO'' Else ''AL DIA'' End,
''CodigoTipo''=Case When(CHQ.Diferido) = ''True'' Then ''DIF'' Else ''CHQ'' End,
CHQ.FechaVencimiento,
''Vencimiento''=Case When(CHQ.Diferido) = ''True'' Then Convert(varchar(50), CHQ.FechaVencimiento, 5) Else ''---'' End,
CHQ.ChequeTercero,
CHQ.Librador,
CHQ.Saldo,
CHQ.Cancelado,
CHQ.Cartera,
CHQ.Depositado,
CHQ.Rechazado,
CHQ.Conciliado,
CHQ.SaldoACuenta,
''Estado''=(Case When(CHQ.Cartera) = ''True'' Then ''CARTERA'' Else 
         (Case When(CHQ.Depositado) = ''True'' Then ''DEPOSITADO'' Else 
         (Case When(CHQ.Rechazado) = ''True'' Then (Case When (CHQ.SAldoACuenta)=0 Then ''RECHAZADO CANCELADO'' ELSE ''RECHAZADO'' End ) Else 
         --(Case When(CHQ.Rechazado) = ''True'' Then ''RECHAZADO'' Else 
         (Case When(CHQ.Conciliado) = ''True'' Then ''CONCILIADO'' 
         Else ''---'' End) End) End) End),

--Rechazo
''Motivo''=(Case When (CHQ.IDMotivoRechazo) IS Not Null Then (Select MOT.Descripcion From MotivoRechazoCheque MOT Where MOT.ID=CHQ.IDMotivoRechazo) Else ''---'' End),

''IDOperacion''=O.ID,
''Operacion''=O.Descripcion,
''OP''=O.Codigo,

CHQ.Titular,
''FecCobranza''= CHQ.FechaCobranza,
''FechaCobranza''=convert(varchar(50),CHQ.FechaCobranza,5),

''Condicion''=(Select Top 1  Credito From ChequeCliente CC 
Join FormaPago FP on CC.IDTransaccion=FP.IDTransaccionCheque 
Join VentaCobranza VC on VC.IDTransaccionCobranza=FP.IDTransaccion 
Join Venta V on V.IDTransaccion=VC.IDTransaccionVenta
Where CC.IDTransaccion=CHQ.IDTransaccion)

--Cobrado, se saco porque en la carga de cobranza duplicaba
From ChequeCliente CHQ
Join VSucursal S On CHQ.IDSucursal=S.ID
Join Cliente C On CHQ.IDCliente=C.ID
Join Banco B On CHQ.IDBanco=B.ID
Left Outer Join ClienteCuentaBancaria CCB On CHQ.IDCuentaBancaria=CCB.ID
Join Moneda M On CHQ.IDMoneda=M.ID
Join Transaccion T On CHQ.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID

'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetalleImpuestoDesglosadoTotales2]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VDetalleImpuestoDesglosadoTotales2]
As
Select 
IDTransaccion,
''GRAVADO10%''=Sum([GRAVADO10%]),
''GRAVADO5%''=Sum([GRAVADO5%]),
''EXENTO''=Sum(EXENTO),
''DISCRIMINADO10%''=Sum([DISCRIMINADO10%]),
''IVA10%''=Sum([IVA10%]),
''DISCRIMINADO5%''=Sum([DISCRIMINADO5%]),
''IVA5%''=Sum([IVA5%])
From VDetalleImpuestoDesglosadoTotales
Group By IDTransaccion

'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDescuento]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VDescuento] As 

Select
D.IDTransaccion,
D.IDProducto,
P.Referencia,
''Producto''=P.Descripcion,
D.ID,
D.IDDescuento,
D.IDDeposito,
''Deposito''=DEP.Descripcion,
D.IDTipoDescuento,
''Tipo''=(Case When (TD.Descripcion) Is Null Then (Case when (IDTipoDescuento) = 0 Then ''TPR'' Else ''---'' End) Else (TD.Descripcion) End),
''T. Desc.''=(Case When (TD.Codigo) Is Null Then (Case when (IDTipoDescuento) = 0 Then ''TPR'' Else ''---'' End) Else (TD.Codigo) End),
D.IDActividad,
''Actividad''=ISNULL((A.Codigo), ''''),

D.Porcentaje,

--Detalle
''Cantidad''=V.Cantidad,

--Totales
''Descuento''=D.Descuento * V.Cantidad,
''Total''=D.Descuento * V.Cantidad,
''DescuentoDiscriminado''=D.DescuentoDiscriminado * V.Cantidad,
''TotalDescuentoDiscriminado''=D.DescuentoDiscriminado * V.Cantidad,

--Unitario
''DescuentoUnitario''=D.Descuento,
''DescuentoUnitarioDiscriminado''=D.DescuentoDiscriminado,

--Venta
''Fecha''=VE.FechaEmision,
VE.FechaEmision,
VE.Comprobante,
VE.Anulado,
VE.Procesado,

--Proveedor
P.IDProveedor

From Descuento D
Join DetalleVenta V On D.IDTransaccion=V.IDTransaccion And D.IDProducto=V.IDProducto And D.ID=V.ID
Join Venta VE On V.IDTransaccion=VE.IDTransaccion
Left Outer Join TipoDescuento TD On D.IDTipoDescuento=TD.ID
Left Outer Join Actividad A On D.IDActividad=A.ID
Left Outer Join Producto P On V.IDProducto=P.ID
Left Outer Join Deposito DEP On V.IDDeposito=DEP.ID

















'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VDetalleVenta]'))
EXEC dbo.sp_executesql @statement = N'


CREATE View [dbo].[VDetalleVenta]
As

--28-02-2014 Milner ->Camp UnixCaja

Select

DT.IDTransaccion,

--Producto
DT.IDProducto,
DT.ID,
''Producto''=P.Descripcion,
''Descripcion''=(Case When DT.Observacion='''' Then P.Descripcion Else P.Descripcion + '' - '' + DT.Observacion End),
''CodigoBarra''=P.CodigoBarra,
P.IDLinea,
''Linea''=ISNULL(L.Descripcion, ''''),
P.IDSubLinea,
''SubLinea''=ISNULL(SL.Descripcion, ''''),
P.IDSubLinea2,
''SubLinea2''=ISNULL(SL2.Descripcion, ''''),
P.IDTipoProducto,
''TipoProducto''=ISNULL(TP.Descripcion, ''''),
P.IDMarca,
''Marca''=ISNULL(M.Descripcion, ''''),
P.IDPresentacion,
P.Referencia,
DT.Observacion,
P.IDUnidadMedida,
''PesoUnitario''= ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0 ),
''Peso''= DT.Cantidad * ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0),
''UnidadMedida''=UM.Referencia ,
P.ControlarExistencia,
P.IDProveedor,
''Proveedor''=P.Proveedor,
''ReferenciaProveedor''=P.ReferenciaProveedor ,

--Deposito
DT.IDDeposito,
''Deposito''=D.Descripcion,

--Cantidad y Precio
DT.Cantidad,
DT.PrecioUnitario,
''Pre. Uni.''=DT.PrecioUnitario,
''Total''=DT.Total,
''Bruto''=DT.PrecioUnitario * DT.Cantidad,

--Impuesto
DT.IDImpuesto,
''Impuesto''=I.Descripcion,
''Ref Imp''=I.Referencia,
DT.TotalImpuesto,
DT.TotalDiscriminado,
''Exento''=I.Exento,

--Descuento
DT.TotalDescuento,
''Descuento''=DT.TotalDescuento,
DT.DescuentoUnitario,
''Desc Uni''=DT.DescuentoUnitario,
''DescuentoUnitarioDiscriminado''=IsNull(DT.DescuentoUnitarioDiscriminado, 0.00),
''TotalDescuentoDiscriminado''=IsNull(DT.DescuentoUnitarioDiscriminado*DT.Cantidad,0.00),
DT.PorcentajeDescuento,
''Desc %''=DT.PorcentajeDescuento,
''PrecioNeto''=IsNull(DT.PrecioUnitario/(Select IMP.FactorDiscriminado From Impuesto IMP Where IMP.ID=DT.IDImpuesto),0) - IsNull(DT.DescuentoUnitarioDiscriminado,0),
''TotalPrecioNeto''=(IsNull(DT.PrecioUnitario/(Select IMP.FactorDiscriminado From Impuesto IMP Where IMP.ID=DT.IDImpuesto),0) - IsNull(DT.DescuentoUnitarioDiscriminado,0)) * DT.Cantidad,

--Costos
DT.CostoUnitario,
''TotalCosto''=isnull(DT.TotalCosto,0),
DT.TotalCostoImpuesto,
DT.TotalCostoDiscriminado,

--Cantidad de Caja
''Caja''=IsNull(DT.Caja, ''False''),
DT.CantidadCaja,
''Cajas''=convert (decimal (10,2) ,IsNull((DT.Cantidad / P.UnidadPorCaja), 0)),
''UnidadMedidas''= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),''UND''),
''UnidadPorCaja''= P.UnidadPorCaja,
--Unidad de medida
''UnidadMedidaConvertir''= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),''UND''),
''UnidadConvertir''=Isnull (P.UnidadConvertir,''1''),

--Totales
''TotalBruto''= isnull(DT.TotalDescuento,0)+isnull(DT.Total,0),
''TotalSinDescuento''=DT.Total - DT.TotalDescuento,
''TotalSinImpuesto''=DT.Total - DT.TotalImpuesto,
''TotalNeto''=DT.Total - (DT.TotalImpuesto + DT.TotalDescuento),
''TotalNetoConDescuentoNeto''=DT.TotalDiscriminado + DT.TotalDescuentoDiscriminado,

--Contabilidad
''CuentaContableVenta''=P.CodigoCuentaVenta,
''CuentaContableCosto''=P.CodigoCuentaCosto,

--Venta
V.IDCliente,
V.Vendedor,
V.Cliente,
V.ReferenciaCliente,
V.TipoCliente,
V.IDTipoCliente,
V.IDListaPrecio,
''ListaPrecio''=V.[Lista de Precio],
V.IDCiudadCliente,
V.FechaEmision,
''Fecha''=V.FechaEmision,
v.Fec,
V.IDMoneda,
V.Moneda,
V.Credito,
V.IDVendedor,
V.IDZonaVenta,
V.ReferenciaSucursal,
V.ReferenciaPunto,
V.IDTipoComprobante, 
V.Comprobante,
V.NroComprobante,
V.[Cod.],
V.Anulado,

''MesNumero''=MONTH(V.Fec),
V.IDSucursal,

--Direccion Alternativa
''DireccionAlternativa''=(Case When V.EsVentaSucursal=''False'' Then V.Cliente Else V.Cliente + '' - '' + V.SucursalCliente End),
''IDSucursalCliente''=IsNull(V.IDSucursalCliente,0),

P.IDCategoria,
P.IDDivision,
P.IDProcedencia

From DetalleVenta DT
Join VVenta V On DT.IDTransaccion=V.IDTransaccion
right outer Join VProducto P On DT.IDProducto=P.ID
Join Deposito D On DT.IDDeposito=D.ID
Join Impuesto I On DT.IDImpuesto=I.ID
--Left Outer Join Descuento DE on DT.IDTransaccion=DE.IDTransaccion And DT.IDProducto=DE.IDProducto And DT.ID=DE.ID
Left Outer Join Linea L on L.ID=P.IDLinea
Left Outer Join SubLinea SL on SL.ID=P.IDSubLinea
Left Outer Join SubLinea2 SL2 on SL2.ID=P.IDSubLinea2 
Left Outer Join TipoProducto TP on TP.ID=P.IDTipoProducto
Left Outer Join Marca M on M.ID=P.IDMarca
Left Outer Join Categoria C on P.IDCategoria=C.ID
Left Outer Join UnidadMedida UM on UM.ID=P.IDUnidadMedida



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VPedidoVendedor]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VPedidoVendedor]

As

Select

P.IDTransaccion,

--Cabecera
''Ciudad''=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
PV.Numero,
P.IDSucursal,
''Sucursal''=S.Descripcion,
''Suc''=S.Codigo,
P.Fecha,
''Fec''=CONVERT(varchar(50), P.Fecha, 6),
P.FechaFacturar,
''Facturar''=CONVERT(varchar(50), P.FechaFacturar, 6),
''FacturaNro''= IsNull((Select V.[Cod.]  + ''  '' + V.Comprobante  from VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=V.IDTransaccion), ''---''), 

--Cliente
P.IDCliente,
''Cliente''=C.RazonSocial,
''Ref.Cliente''=C.Referencia ,
C.RUC,
P.IDSucursalCliente,
P.Direccion,
P.Telefono,
''IDZonaVenta''=IsNull(C.IDZonaVenta, 0),
C.ZonaVenta,
P.Procesado, 

--Opciones
P.IDDeposito,
''Deposito''=D.Descripcion,
P.Observacion,
P.IDMoneda,
''Moneda''=M.Referencia,
P.Cotizacion,
P.Anulado,

--Otros
P.IDListaPrecio,
''Lista de Precio''=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=P.IDListaPrecio), ''---''),
PV.IDVendedor,
''Vendedor''=ISNULL((Select VE.Nombres From Vendedor VE Where VE.ID=PV.IDVendedor), ''---''),
P.EsVentaSucursal,
--P.Facturado,
''Facturado''=(Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then ''False'' Else ''True'' End),

''Estado''=(Case When (Anulado) = ''True'' Then ''Anulado'' Else (Case When (Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then ''False'' Else ''True'' End) = ''True'' Then ''Facturado'' Else ''Pendiente'' End) End),
''Venta''=IsNull((Select Top(1) V.Comprobante From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc), ''---''),

--Totales
P.Total,
P.TotalImpuesto,
P.TotalDiscriminado,
P.TotalDescuento,

--Transaccion
''FechaTransaccion''=T.Fecha,
''IDDepositoTransaccion''=T.IDDeposito,
''IDSucursalTransaccion''=T.IDSucursal,
''IDTerminalTransaccion''=T.IDTerminal,
T.IDUsuario,
''usuario''=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
''UsuarioIdentificador''=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Importado
''Comprobante''=ISNULL(P.Numero, ''---''),

--Anulacion
''IDUsuarioAnulacion''=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion), 0)),
''UsuarioAnulacion''=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), ''---'')),
''UsuarioIdentificacionAnulacion''=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), ''---'')),
''FechaAnulacion''=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion)

From Pedido P
Join Transaccion T On P.IDTransaccion=T.ID
Join PedidoVendedor PV On P.IDTransaccion=PV.IDTransaccion
Left Outer Join VCliente C On P.IDCliente=C.ID
Left Outer Join Sucursal S On P.IDSucursal=S.ID
Left Outer Join Deposito D On P.IDDeposito=D.ID
Left Outer Join Moneda M On P.IDMoneda=M.ID



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VPedido]'))
EXEC dbo.sp_executesql @statement = N'Create View [dbo].[VPedido]

As

Select

P.IDTransaccion,

--Cabecera
''Ciudad''=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
P.Numero,
P.IDSucursal,
''Sucursal''=S.Descripcion,
''Suc''=S.Codigo,
P.Fecha,
''Fec''=CONVERT(varchar(50), P.Fecha, 6),
P.FechaFacturar,
''Facturar''=CONVERT(varchar(50), P.FechaFacturar, 6),
''FacturaNro''= IsNull((Select V.[Cod.]  + ''  '' + V.Comprobante  from VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=V.IDTransaccion), ''---''), 

--Cliente
P.IDCliente,
''Cliente''=C.RazonSocial,
''Ref.Cliente''=C.Referencia ,
C.RUC,
P.IDSucursalCliente,
P.Direccion,
P.Telefono,
''IDZonaVenta''=IsNull(C.IDZonaVenta, 0),
C.ZonaVenta,

--Opciones
P.IDDeposito,
''Deposito''=D.Descripcion,
P.Observacion,
P.IDMoneda,
''Moneda''=M.Referencia,
P.Cotizacion,
P.Anulado,

--Otros
P.IDListaPrecio,
''Lista de Precio''=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=P.IDListaPrecio), ''---''),
P.IDVendedor,
''Vendedor''=ISNULL((Select VE.Nombres From Vendedor VE Where VE.ID=P.IDVendedor), ''---''),
P.EsVentaSucursal,
--P.Facturado,
''Facturado''=(Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then ''False'' Else ''True'' End),

''Estado''=(Case When (Anulado) = ''True'' Then ''Anulado'' Else (Case When (Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then ''False'' Else ''True'' End) = ''True'' Then ''Facturado'' Else ''Pendiente'' End) End),
''Venta''=IsNull((Select Top(1) V.Comprobante From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc), ''---''),

--Totales
P.Total,
P.TotalImpuesto,
P.TotalDiscriminado,
P.TotalDescuento,

--Transaccion
''FechaTransaccion''=T.Fecha,
''IDDepositoTransaccion''=T.IDDeposito,
''IDSucursalTransaccion''=T.IDSucursal,
''IDTerminalTransaccion''=T.IDTerminal,
T.IDUsuario,
''usuario''=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
''UsuarioIdentificador''=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Importado
''Comprobante''=ISNULL(P.Numero, ''---''),

--Anulacion
''IDUsuarioAnulacion''=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion), 0)),
''UsuarioAnulacion''=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), ''---'')),
''UsuarioIdentificacionAnulacion''=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), ''---'')),
''FechaAnulacion''=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion)

From Pedido P
Join Transaccion T On P.IDTransaccion=T.ID
Left Outer Join VCliente C On P.IDCliente=C.ID
Left Outer Join Sucursal S On P.IDSucursal=S.ID
Left Outer Join Deposito D On P.IDDeposito=D.ID
Left Outer Join Moneda M On P.IDMoneda=M.ID
'





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpViewProductoListaPrecioExcepciones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpViewProductoListaPrecioExcepciones]

	--Identificadores
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint
	
As

Begin

	Select 
	* 
	From VProductoListaPrecioExcepciones 
	Where IDSucursal=@IDSucursal

End' 
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FormaPagoDocumento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FormaPagoDocumento](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[Comprobante] [varchar](50) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[ImporteMoneda] [money] NOT NULL,
	[Cotizacion] [money] NOT NULL,
	[Total] [money] NULL,
	[Saldo] [money] NULL,
	[Observacion] [varchar](100) NULL,
	[Cancelado] [bit] NULL,
 CONSTRAINT [PK_FormaPagoDocumento] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FExistenciaProducto]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE Function [dbo].[FExistenciaProducto]
(

	--Entrada
	@IDProducto int,
	@IDDeposito tinyint
)

Returns decimal(10,2)

As


Begin

	declare @vRetorno decimal(10,2)
	declare @vExistenciaReal decimal(10,2)
	declare @vReservado decimal(10,2)
	
	--Si no controla stock, retornar 1
	If (Select ControlarExistencia From Producto Where ID=@IDProducto) = ''False'' Begin
		Set @vRetorno = 1
		return @vRetorno
	End
	
	Set @vExistenciaReal = (Select dbo.FExistenciaProductoReal(@IDProducto, @IDDeposito))
	Set @vReservado = (Select dbo.FExistenciaProductoReservado(@IDProducto, @IDDeposito)) 						
	
	--Esto no funciona para VENTAS, solo para PEDIDOS, crear uno nuevo 
	--que incluya RESERVADOS						
	--Set @vRetorno = @vExistenciaReal - @vReservado
	Set @vRetorno = @vExistenciaReal
	
	return @vRetorno
	
End
' 
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Efectivo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Efectivo](
	[IDTransaccion] [numeric](18, 0) NOT NULL,
	[ID] [tinyint] NOT NULL,
	[IDSucursal] [tinyint] NOT NULL,
	[Numero] [int] NULL,
	[IDTipoComprobante] [smallint] NOT NULL,
	[Comprobante] [varchar](50) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[VentasCredito] [money] NOT NULL,
	[VentasContado] [money] NOT NULL,
	[Gastos] [money] NOT NULL,
	[IDMoneda] [tinyint] NOT NULL,
	[ImporteMoneda] [money] NOT NULL,
	[Cotizacion] [money] NOT NULL,
	[Observacion] [varchar](100) NULL,
	[Depositado] [money] NULL,
	[Cancelado] [bit] NULL,
	[Total] [money] NULL,
	[Saldo] [money] NULL,
	[ImporteHabilitado] [money] NULL,
	[Habilitado] [bit] NULL,
	[IDMotivo] [tinyint] NULL,
	[Anulado] [bit] NULL,
	[FechaAnulado] [datetime] NULL,
	[IDUsuarioAnulado] [smallint] NULL,
 CONSTRAINT [PK_Efectivo_1] PRIMARY KEY CLUSTERED 
(
	[IDTransaccion] ASC,
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END







IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpVentaCobranza]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpVentaCobranza]

	--Entrada
	@IDTransaccionCobranza numeric(18,0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	--Variables
	Declare @vIDTransaccionVenta numeric(18,0)
	Declare @vIDTransaccionCobranza numeric(18,0)
	Declare @vImporte money
	Declare @vCancelar bit
	Declare @vCancelarVenta bit
	Declare @vCobrado money
	Declare @vDescontado money
	Declare @vTotalVenta money
	Declare @vSaldoTotal money
	Declare @vCancelado bit
		
	--Procesar Saldos de Ventas
	If @Operacion = ''INS'' Begin
	
		Declare db_cursor cursor for
		Select IDTransaccionVenta, IDTransaccionCobranza, Importe, Cancelar From VentaCobranza Where IDTransaccionCobranza=@IDTransaccionCobranza
		
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionCobranza, @vImporte, @vCancelar
		While @@FETCH_STATUS = 0 Begin  

			--Obtener Informacion
			Set @vTotalVenta = (Select IsNull((Select Total From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
			Set @vCobrado = (Select IsNull((Select Cobrado From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
			Set @vDescontado = (Select IsNull((Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
			
			Set @vCancelarVenta = ''False''
			
			--Calcular
			Set @vCobrado = @vCobrado + @vImporte
			Set @vSaldoTotal = @vTotalVenta - (@vCobrado + @vDescontado)
			   
			--Si el saldo es 0, cancelamos la venta   
			If @vSaldoTotal <= 0 Begin
				Set @vCancelarVenta = ''True''
			End
			 
			--Si el usuario cancela explicitamente la venta    	
			If @vCancelar = ''True'' Begin
				Set @vCancelarVenta = ''True''
			End
			
			--Actualizar Venta			
			Update Venta Set	Saldo=@vSaldoTotal, 
								Cobrado=@vCobrado, 
								Cancelado=@vCancelarVenta
			Where IDTransaccion = @vIDTransaccionVenta
			
			--Siguiente
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionCobranza, @vImporte, @vCancelar
			
		End
		
		Close db_cursor   
		Deallocate db_cursor		   			
		
		Set @Mensaje = ''Registo procesado!''
		Set @Procesado = ''True''
		Return @@rowcount
		
	End
	
	--Eliminar Ventas
	If @Operacion = ''ANULAR'' Begin
	
		Declare db_cursor cursor for
		Select IDTransaccionVenta, IDTransaccionCobranza, Importe, Cancelar From VentaCobranza Where IDTransaccionCobranza=@IDTransaccionCobranza
		
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionCobranza, @vImporte, @vCancelar
		While @@FETCH_STATUS = 0 Begin  

			--Obtener Informacion
			Set @vTotalVenta = (Select IsNull((Select Total From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
			Set @vCobrado = (Select IsNull((Select Cobrado From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
			Set @vDescontado = (Select IsNull((Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
			
			--Calcular
			Set @vCobrado = @vCobrado - @vImporte
			
			If @vCobrado < 0 Begin
				Set @vCobrado = (@vCobrado * -1)
			End
			
			Set @vSaldoTotal = @vTotalVenta - (@vCobrado + @vDescontado)
			
			--Actualizar Venta			
			Update Venta Set	Saldo=@vSaldoTotal, 
								Cobrado=@vCobrado, 
								Cancelado=''False''
			Where IDTransaccion = @vIDTransaccionVenta
					
			--Siguiente
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionCobranza, @vImporte, @vCancelar
			
		End
		
		Close db_cursor   
		Deallocate db_cursor	
		
		Set @Mensaje = ''Registo procesado!''
		Set @Procesado = ''True''
		Return @@rowcount
		
	End
	
	--Eliminar Ventas
	If @Operacion = ''DEL'' Begin
	
		If (Select Anulado From CobranzaCredito Where IDTransaccion=@IDTransaccionCobranza) = ''False'' Begin
			Declare db_cursor cursor for
			Select IDTransaccionVenta, IDTransaccionCobranza, Importe, Cancelar From VentaCobranza Where IDTransaccionCobranza=@IDTransaccionCobranza
			
			Open db_cursor   
			Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionCobranza, @vImporte, @vCancelar
			While @@FETCH_STATUS = 0 Begin  

				--Obtener Informacion
				Set @vTotalVenta = (Select IsNull((Select Total From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
				Set @vCobrado = (Select IsNull((Select Cobrado From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
				Set @vDescontado = (Select IsNull((Select Descontado From Venta Where IDTransaccion=@vIDTransaccionVenta), 0))
				
				--Calcular
				Set @vCobrado = @vCobrado - @vImporte
				
				If @vCobrado < 0 Begin
					Set @vCobrado = (@vCobrado * -1)
				End
				
				Set @vSaldoTotal = @vTotalVenta - (@vCobrado + @vDescontado)
				
				--Actualizar Venta			
				Update Venta Set	Saldo=@vSaldoTotal, 
									Cobrado=@vCobrado, 
									Cancelado=''False''
				Where IDTransaccion = @vIDTransaccionVenta
				
				--Siguiente
				Fetch Next From db_cursor Into @vIDTransaccionVenta, @vIDTransaccionCobranza, @vImporte, @vCancelar
				
			End
			
			Close db_cursor   
			Deallocate db_cursor		   			
			
		End
		
		--Eliminar las relaciones
		Delete From VentaCobranza 
		Where IDTransaccionCobranza=@IDTransaccionCobranza
		
		Set @Mensaje = ''Registo procesado!''
		Set @Procesado = ''True''
		Return @@rowcount
		
	End
		
	Set @Mensaje = ''No procesado''
	Set @Procesado = ''False''
	
End
	
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpPedido]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpPedido]

	--Entrada
	@IDTransaccion as numeric(18,0),
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion int,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	
	
As

Begin

	If @Operacion = ''ANULAR'' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Pedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Verificamos que el pedido no este facturado
		If Exists(Select * From PedidoVenta Where IDTransaccionPedido=@IDTransaccion) Begin
			set @Mensaje = ''El regitro esta facturado! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		----Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
		
		----Anular
		Update Pedido Set Anulado=''True'', IDUsuarioAnulado=@IDUsuario, FechaAnulado = GETDATE()
		Where IDTransaccion = @IDTransaccion
						
		Set @Mensaje = ''Reistro guardado!''
		Set @Procesado = ''True''
		return @@rowcount
			
	End
	
	If @Operacion = ''DEL'' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Pedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Verificamos que el pedido no este facturado
		If Exists(Select * From PedidoVenta Where IDTransaccionPedido=@IDTransaccion) Begin
			set @Mensaje = ''El regitro esta facturado! No se puede eliminar.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Eliminamos los descuentos
		Delete DescuentoPedido where IDTransaccion = @IDTransaccion				
		
		--Eliminamos el detalle
		Delete DetallePedido where IDTransaccion = @IDTransaccion				
				
		--Eliminamos el registro
		Delete Pedido Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion				
				
		Set @Mensaje = ''Reistro guardado!''
		Set @Procesado = ''True''
		return @@rowcount
			
	End
	
	Set @Mensaje = ''No se proceso ningun registro!''
	Set @Procesado = ''False''
	Return @@rowcount
	
End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpAcualizarSaldoCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpAcualizarSaldoCliente]

	--Entrada
	@IDCliente int
	
As

Begin
	
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	Declare @vSaldo money
	
	--Valores Negativos
	Declare @vTotalVenta money
	Declare @vTotalNotaDebito money
	
	--Valores Positivos
	Declare @vTotalCobranzaCredito money
	Declare @vTotalCobranzaContado money
	Declare @vTotalNotaCredito money
	
	Set @vTotalVenta = (IsNull((Select SUM(Total) From Venta Where Credito=''True'' And IDCliente=@IDCliente And Anulado=''False'' And Procesado=''True''),0))
	PRINT ''Total venta: '' + CONVERT(varchar(50), CONVERT(INT, @vTotalVenta)) 
	
	Set @vTotalNotaDebito = (IsNull((Select SUM(Total) From NotaDebito Where IDCliente=@IDCliente And Anulado=''False'' And Procesado=''True''),0))
	PRINT ''Total Nota Debito: '' + CONVERT(varchar(50), CONVERT(INT, @vTotalNotaDebito)) 
	
	Set @vTotalCobranzaContado = (IsNull((Select SUM(VC.Importe) From VentaCobranza VC 
											Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion 
											Join CobranzaContado CC On VC.IDTransaccionCobranza=CC.IDTransaccion
											Where V.Credito=''True'' And V.IDCliente=@IDCliente And V.Anulado=''False'' And V.Procesado=''True''
											And CC.Anulado=''False'' And CC.Procesado=''True''),0))
	PRINT ''Total Cobranza Contado: '' + CONVERT(varchar(50), CONVERT(INT, @vTotalCobranzaContado)) 
	
	Set @vTotalCobranzaCredito = (IsNull((Select SUM(VC.Importe) From VentaCobranza VC 
											Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion 
											Join CobranzaCredito CC On VC.IDTransaccionCobranza=CC.IDTransaccion
											Where V.Credito=''True'' And V.IDCliente=@IDCliente And V.Anulado=''False'' And V.Procesado=''True''
											And CC.Anulado=''False'' And CC.Procesado=''True''),0))
	PRINT ''Total Cobranza Credito: '' + CONVERT(varchar(50), CONVERT(INT, @vTotalCobranzaCredito)) 
	
	Set @vTotalNotaCredito = (IsNull((Select SUM(Total) From NotaCredito Where IDCliente=@IDCliente And Anulado=''False'' And Procesado=''True''),0))
	PRINT ''Total Nota Credito: '' + CONVERT(varchar(50), CONVERT(INT, @vTotalNotaCredito)) 
	
	Set @vSaldo = (@vTotalVenta + @vTotalNotaDebito) - (@vTotalCobranzaContado + @vTotalCobranzaCredito + @vTotalNotaCredito)
	PRINT ''Saldo: '' + CONVERT(varchar(50), CONVERT(INT, @vSaldo)) 
	
	Declare @vCredito money
	Set @vCredito = (Select LimiteCredito From Cliente Where ID=@IDCliente)
	PRINT ''Credito: '' + CONVERT(varchar(50), CONVERT(INT, @vCredito)) 
	
	--Hasta aclarar bien, usar el saldo de las facturas solamente
	Set @vSaldo = (Select SUM(Saldo) From Venta Where IDCliente=@IDCliente And Anulado=''False'' And Cancelado=''False'' And Procesado=''True'')
	
	Set @vSaldo = @vCredito - @vSaldo
	PRINT ''Deuda: '' + CONVERT(varchar(50), CONVERT(INT, @vSaldo)) 
	
	Update Cliente Set Deuda=@vSaldo
	Where ID=@IDCliente
	
	--Select ''Mensaje''=''Registro guardado'', ''Procesado''=''True'', ''Deuda''=@vSaldo
			
End
	
	' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpChequeCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpChequeCliente]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalDocumento tinyint = NULL,
	@Numero int = NULL,
	@IDCliente int = NULL,
	@IDBanco tinyint = NULL,
	@CuentaBancaria varchar(50) = NULL,
	@Fecha date = NULL,
	@NroCheque varchar(50) = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Importe money = NULL,
	@ImporteMoneda money = NULL,
	@Diferido bit = NULL,
	@FechaVencimiento date = NULL,
	@ChequeTercero bit = NULL,
	@Librador varchar(50) = NULL,
	@Operacion varchar(50),
	@Titular varchar(50)=NULL,
	@FechaCobranza date=NULL,
		
		
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
	
	--INSERTAR
	If @Operacion = ''INS'' Begin
	
		--Validar
		--Numero
		Declare @vNumero int
		Set @vNumero = ISNULL((Select MAX(Numero)+1 From ChequeCliente Where IDSucursal=@IDSucursalDocumento),1)
		
		--Cuenta Bancaria
		If @CuentaBancaria = '''' Begin
			set @Mensaje = ''Falta la cuenta bancaria''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
			
		--La Fecha de Entrada no puede ser menor a la del comprobante
		if @Diferido = ''True'' Begin
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) < 0 Begin
				set @Mensaje = ''La fecha de vencimiento no puede ser menor a la del documento!''
				set @Procesado = ''False''
				return @@rowcount
			End
		End
				
		--Insertar la transaccion			
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccion OUTPUT
		
		--------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		Declare @vIDCuentaBancaria int
		
		Set @vSaldo = @Importe
		Set @vCancelado = ''False''
				
		--Cuenta Bancaria
		Begin
		
			If Exists(Select ID From ClienteCuentaBancaria Where IDCliente=@IDCliente And IDBanco=@IDBanco And CuentaBancaria=@CuentaBancaria) Begin
				Set @vIDCuentaBancaria = (Select ID From ClienteCuentaBancaria Where IDCliente=@IDCliente And IDBanco=@IDBanco And CuentaBancaria=@CuentaBancaria)
			End Else Begin
				Set @vIDCuentaBancaria = (Select ISNULL(Max(ID) + 1, 1) From ClienteCuentaBancaria)
				Insert Into ClienteCuentaBancaria(ID, IDCliente, IDBanco, CuentaBancaria, IDMoneda)
				Values(@vIDCuentaBancaria, @IDCliente, @IDBanco, @CuentaBancaria, @IDMoneda)
			End
			
		End
		
		----Insertar el Registro de Cheque
		Insert Into ChequeCliente(IDTransaccion, IDSucursal, Numero, IDCliente, IDBanco, IDCuentaBancaria, Fecha, NroCheque, IDMoneda, Cotizacion, Importe, ImporteMoneda, Diferido, FechaVencimiento, ChequeTercero, Librador, Saldo, SaldoACuenta, Cancelado, Cartera, Depositado, Rechazado,Titular,FechaCobranza)
		values(@IDTransaccion, @IDSucursalDocumento, @vNumero, @IDCliente, @IDBanco, @vIDCuentaBancaria, @Fecha, @NroCheque, @IDMoneda, @Cotizacion, @Importe, @ImporteMoneda, @Diferido, @FechaVencimiento, @ChequeTercero, @Librador, @vSaldo, @Importe, @vCancelado, ''True'', ''False'', ''False'',@Titular,@FechaCobranza)
															
		set @Mensaje = ''Registro guardado''
		set @Procesado = ''True''
		set @IDTransaccionSalida  = ''0''
		return @@rowcount
			
	End
	
	--ELIMINAR
	If @Operacion = ''DEL'' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From ChequeCliente Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End

		--Verificar que el documento no tenga otros registros asociados
		--Forma de Pago
		If Exists(Select * From FormaPago FP Where FP.IDTransaccion=@IDTransaccion) Begin
			Set @Mensaje = ''El registro ya esta usado en otros documentos! No se puede eliminar.''
			Set @Procesado = ''False''
			return @@rowcount			
		End
		--Forma de Pago Cheques
		If Exists(Select * From FormaPago FP Where FP.IDTransaccionCheque =@IDTransaccion) Begin
			Set @Mensaje = ''El registro ya esta usado en otros documentos! No se puede eliminar.''
			Set @Procesado = ''False''
			return @@rowcount			
		End
		
		--Detalle Deposito Bancario
		If Exists(Select * From DetalleDepositoBancario D Where D.IDTransaccionCheque =@IDTransaccion) Begin
			Set @Mensaje = ''El registro ya esta usado en otros documentos! No se puede eliminar.''
			Set @Procesado = ''False''
			return @@rowcount			
		End
								
		--Eliminamos

		--Eliminamos el registro
		Delete ChequeCliente Where IDTransaccion = @IDTransaccion
				
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
				
		Set @Mensaje = ''Reistro guardado!''
		Set @Procesado = ''True''
		return @@rowcount
			
	End
		
	--ACTUALIZAR
	If @Operacion=''UPD'' Begin
	
		--Validar
		--Numero de Cheque
		If Exists(Select * From ChequeCliente Where NroCheque=@NroCheque And IDTransaccion!=@IDTransaccion And IDSucursal=@IDSucursal) Begin
			set @Mensaje = ''El sistema detecto que el numero de cheque ya esta registrado! Obtenga un numero siguiente.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
	
		--Cuenta Bancaria
		Begin
		
			If Exists(Select ID From ClienteCuentaBancaria Where IDCliente=@IDCliente And IDBanco=@IDBanco And CuentaBancaria=@CuentaBancaria) Begin
				Set @vIDCuentaBancaria = (Select ID From ClienteCuentaBancaria Where IDCliente=@IDCliente And IDBanco=@IDBanco And CuentaBancaria=@CuentaBancaria)
			End Else Begin
				Set @vIDCuentaBancaria = (Select ISNULL(Max(ID) + 1, 1) From ClienteCuentaBancaria)
				Insert Into ClienteCuentaBancaria(ID, IDCliente, IDBanco, CuentaBancaria, IDMoneda)
				Values(@vIDCuentaBancaria, @IDCliente, @IDBanco, @CuentaBancaria, @IDMoneda)
			End
			
		End
		
		Set @vSaldo = @Importe
		--Actualizamos Cheque Cliente
		Update ChequeCliente Set IDSucursal=@IDSucursalDocumento,								
								 IDCliente=@IDCliente, 
								 IDBanco=@IDBanco,
								 IDCuentaBancaria=@vIDCuentaBancaria, 
								 Fecha=@Fecha, 
								 NroCheque=@NroCheque, 
								 IDMoneda=@IDMoneda, 
								 Cotizacion=@Cotizacion, 
								 Importe=@Importe, 
								 ImporteMoneda=@ImporteMoneda,
								 Diferido=@Diferido, 
								 FechaVencimiento=@FechaVencimiento, 
								 ChequeTercero=@ChequeTercero, 
								 Librador=@Librador,
								 Titular=@Titular,
								 FechaCobranza=@FechaCobranza,
								 Saldo=@vSaldo
		   	
		Where IDTransaccion=@IDTransaccion 
	
		
		set @Mensaje = ''Registro guardado!''
		set @Procesado = ''True''
		
		return @@rowcount
		
	End
	

End

' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpActualizarFormaPagoEfectivo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpActualizarFormaPagoEfectivo]

	--Entrada
	@IDTransaccion int,
	@Comprobante varchar(50),
	@Observacion varchar(150),
	@ID int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

			
	--Actualizar 
	Update Efectivo Set Comprobante=@Comprobante,
								 Observacion =@Observacion 
								  
	Where IDTransaccion=@IDTransaccion And ID=@ID
	
			
	set @Mensaje = ''Registro guardado''
	set @Procesado = ''True''
	return @@rowcount
		

End
	
	
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpActualizarFormaPagoDocumento]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpActualizarFormaPagoDocumento]

	--Entrada
	@IDTransaccion int,
	@Comprobante varchar(50),
	@Fecha date,
	@Observacion varchar(150),
	@ID int,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

			
	--Actualizar 
	Update FormaPagoDocumento Set Comprobante=@Comprobante,
								  Fecha=@Fecha,
								  Observacion =@Observacion 
								  
	Where IDTransaccion=@IDTransaccion And ID=@ID
	
			
	set @Mensaje = ''Registro guardado''
	set @Procesado = ''True''
	return @@rowcount
		

End
	
	
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpActualizarEfectivo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpActualizarEfectivo]
	
	--Entrada
	@IDTransaccion numeric(18,0),
	@ImporteHabilitado decimal,
	@Habilitado bit,
	@ID tinyint,
		
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
	

As

Begin
	
	
	Begin
	
		--Actualizar Efectivo 
		Update Efectivo Set Habilitado = @Habilitado,
							ImporteHabilitado=@ImporteHabilitado 
		Where IDTransaccion = @IDTransaccion And ID=@ID
		
		Set @Mensaje = ''Registro procesado!''
		Set @Procesado = ''True''	
		Return @@rowcount
	
	End
	
End
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpAcualizarClienteVenta]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpAcualizarClienteVenta]

	--Entrada
	@IDCliente int,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output

As

Begin

	--Variables
	Declare @vFechaVenta date
	
	--Obtener valores	
	Set @vFechaVenta = (Select Max(FechaEmision) From Venta Where IDCliente=@IDCliente And Anulado=''False'' And Procesado=''True'')
		
	--Actualizar la ultima compra
	Update Cliente Set FechaUltimaCompra=@vFechaVenta
	Where ID=@IDCliente
	
	--Actualizar la Deuda	
	Exec SpAcualizarSaldoCliente @IDCliente
			
	set @Mensaje = ''Registro guardado''
	set @Procesado = ''True''
	return @@rowcount
		

End
	
	
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpPrecioProducto2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpPrecioProducto2]

	--Entrada
	@IDProducto int,
	@IDCliente int,
	@IDSucursal tinyint,
	@IDDeposito tinyint = 0
	
As

Begin

	--Variables
	Declare @vIDListaPrecio int
	Declare @vExistencia decimal(10,2)
	
	Declare @vFecha date
	
	Set @vFecha = (Select Top(1) VentaFechaFacturacion From Configuraciones Where IDSucursal=@IDSucursal)
		
	--Primero Hayamos el Precio			
	--Obtener la lista de precio del cliente
	Set @vIDListaPrecio = (Select IsNull((Select IDListaPrecio From Cliente Where ID=@IDCliente), 0))
	
	--Aprovechamos y sacamos la existencia, para la venta y pedido
	--y asi no consultamos tantas veces a la BD desde la aplicacion
	Set @vExistencia = dbo.FExistenciaProducto(@IDProducto, @IDDeposito)
	
	--ATENCION!!! - Saque la sucursal para poder vender a clientes de otra sucursal	
	--Este es para Precio Normal	
	Select ''IDTipo''=-1, ''Tipo''=''Precio'', ''TipoDescuento''='''', ''IDActividad''=NULL, ''Importe''=P.Precio, ''Porcentaje''=0, ''Existencia''=@vExistencia
	From ProductoListaPrecio P
	Join ListaPrecio LP On P.IDListaPrecio=LP.ID
	Where P.IDProducto=@IDProducto 
	And P.IDListaPrecio=@vIDListaPrecio 
	--And LP.IDSucursal=@IDSucursal 
	
	Union All
	
	--Este es para TPR	
	Select ''IDTipoDescuento''=0, ''Tipo''=''TPR'', ''T. Desc.''=''TPR'', ''IDActividad''=NULL, ''Importe''=IsNull(P.TPR,0), ''Porcentaje''=IsNull(P.TPRPorcentaje,0), ''Existencia''=@vExistencia  
	From ProductoListaPrecio P
	Join ListaPrecio LP On P.IDListaPrecio=LP.ID
	Where P.IDProducto=@IDProducto 
	And P.IDListaPrecio=@vIDListaPrecio 
	--And LP.IDSucursal=@IDSucursal 
	And (@vFecha Between IsNull(P.TPRDesde, DateAdd(d,-1, @vFecha)) And IsNull(P.TPRHasta, DateAdd(d,1, @vFecha)))
	
	Union all
	
	--Excepciones Express y Acuerdo
	Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento, PLPE.Porcentaje, ''Existencia''=@vExistencia 
	From ProductoListaPrecioExcepciones PLPE 
	Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
	Where PLPE.IDProducto=@IDProducto And IDListaPrecio=@vIDListaPrecio And IDCliente=@IDCliente  
	--And IDSucursal = @IDSucursal 
	And TD.ID <> 1
	And (@vFecha Between Desde And Hasta)
	
	--Union All
	
	----Excepciones TACTICO
	--Select PLPE.IDTipoDescuento, TD.Descripcion, TD.Codigo, NULL, PLPE.Descuento, PLPE.Porcentaje, ''Existencia''=@vExistencia 
	--From ProductoListaPrecioExcepciones PLPE 
	--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
	--Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID 
	--Where PLPE.IDProducto=@IDProducto And PLPE.IDListaPrecio=@vIDListaPrecio And PLPE.IDCliente=@IDCliente  
	----And LP.IDSucursal = @IDSucursal 
	--And @vFecha between PLPE.Desde And PLPE.Hasta
	--And TD.ID = 1
	--And (Case When Exists(Select * From VDetalleActividad D 
	--									Join VActividad A On D.IDActividad=A.ID 
	--									Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad 
	--									Join PlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion 
	--									Where D.ID=@IDProducto And (@vFecha between P.Desde And P.Hasta) And P.IDSucursal=@IDSucursal) Then ''True'' Else ''False'' End)=''True''

End


' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpPedidoProcesar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpPedidoProcesar]

	--CARLOS 21-02-2014
	--Milner 26-02-2014 Validar Fecha
	
	--Entrada
	@IDTransaccion numeric(18,0)
	
As

Begin

	--Variables
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
	Declare @vNumero int
	Declare @vCantidad decimal(18,0)
	Declare @vIDProducto int
	Declare @vIDDeposito tinyint
	Declare @vIDCliente int
	Declare @vIDSucursal int
		
	--Obtener valores
	Set @vIDCliente = (Select IDCliente From Pedido Where IDTransaccion=@IDTransaccion)
	set @vIDSucursal = (Select IDSucursal  from Pedido where IDTransaccion= @IDTransaccion)
	
	Set @Mensaje = ''No se realizo ninguna accion''
	Set @Procesado = ''False''


	--Validar
	
	--Pedido
	Begin
	
		--Transaccion
		If Not Exists(Select * From Pedido Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''Error: El sistema no encuentra el registro de Pedido! Intente nuevamente.''
			set @Procesado = ''False''
			Goto Error
		End
		
		--Validar Fecha
		If (Select DATEDIFF (DD   , GETDATE(), (Select FechaFacturar  From Pedido  Where IDTransaccion=@IDTransaccion) )) >10 Begin
			set @Mensaje = ''Error: La fecha de pedido no puede ser mayor a 10 dias.''
			set @Procesado = ''False''
			Goto Error
		End
		
		If (Select DATEDIFF (DD   , GETDATE(), (Select FechaFacturar From Pedido  Where IDTransaccion=@IDTransaccion) )) <0 Begin
			set @Mensaje = ''Error: La fecha de pedido no puede ser menor al de hoy''
			set @Procesado = ''False''
			Goto Error
		End
		
	End
		
	--Stock
	Begin
		
		Declare db_cursor cursor for
		Select Cantidad, IDProducto, IDDeposito From DetallePedido Where IDTransaccion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vIDDeposito
		While @@FETCH_STATUS = 0 Begin  
		
			--Cantidad
			If @vCantidad <= 0 Begin
				Set @Mensaje = ''La cantidad no puede ser igual ni menor a 0!''
				Set @Procesado = ''False''
				Goto Error		
			End	
			
			If Exists (Select * from Producto Where ControlarExistencia= ''False'' and ID=@vIDProducto) begin
				Goto Siguiente
			End
			
			--Verificar Existencia
			If (Select dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito)) < @vCantidad Begin
				Declare @vProducto varchar(50)
				Declare @vExistencia decimal(18,0)
				
				Set @vProducto = (Select Descripcion From Producto Where ID=@vIDProducto)
				Set @vExistencia = (Select dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito))
				
				Set @Mensaje = ''Stock insuficiente! '' + CONVERT(varchar(50), @vProducto) + '': '' + CONVERT(varchar(50), @vExistencia)
				Set @Procesado = ''False''
								
				Close db_cursor   
				Deallocate db_cursor			
				
				GoTo Error
				
			End
			
			Siguiente:
			
			Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vIDDeposito
			
		End
		
		Close db_cursor   
		Deallocate db_cursor	
		
	End
	
	
	--Procesar
	Begin
		
		--Actualizamos el Pedido
		Update Pedido Set Procesado=''True'',
							Numero=(Select IsNull(Max(Numero+1),1) From Pedido Where IDSucursal=@vIDSucursal)
		Where IDTransaccion=@IDTransaccion
		
		--Insertar en PedidoVendedor
		Declare @vIDVendedor int
		
		Set @vIDVendedor = (Select IDVendedor From Pedido Where IDTransaccion=@IDTransaccion)
		Set @vNumero = (Select ISNULL(MAX(Numero)+1, 1) From PedidoVendedor Where IDVendedor=@vIDVendedor)
		
		Insert Into PedidoVendedor(IDTransaccion, IDVendedor, Numero)
		Values(@IDTransaccion, @vIDVendedor, @vNumero)
		
		Select ''Procesado''=''True'', ''Mensaje''=''Registro procesado!'', ''Procesados''=@@ROWCOUNT
		Return @@Rowcount
			
	End
	
	Error:

	--Si se produjo un error, eliminar toda la transaccion
	Delete From DetalleImpuesto Where IDTransaccion=@IDTransaccion	
	Delete From DescuentoPedido Where IDTransaccion=@IDTransaccion	
	Delete From DetallePedido Where IDTransaccion=@IDTransaccion	
	Delete From Pedido Where IDTransaccion=@IDTransaccion	
	Delete From Transaccion Where ID=@IDTransaccion	
	
	Select ''Procesado''=''False'', ''Mensaje''=@Mensaje, ''Procesados''=@@ROWCOUNT
					
End

' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpDetalleVenta]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpDetalleVenta]

	--Entrada
	@IDTransaccion numeric(18,0),
	@IDProducto int = NULL,
	@ID tinyint = NULL,
	@IDDeposito tinyint = NULL,
	@Observacion varchar(50) = NULL,
	@IDImpuesto tinyint = NULL,
	@Cantidad decimal(10,2) = NULL,
	@PrecioUnitario money = NULL,
	@DescuentoUnitario money = NULL,
	@PorcentajeDescuento numeric(2,0) = NULL,
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDescuento money = NULL,
	@TotalDiscriminado money = NULL,
	@Caja bit = NULL,
	@CantidadCaja smallint = NULL,
	@Operacion varchar(20),
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output
		
As

Begin

	--Validar
	--Transaccion
	If Not Exists(Select ID From Transaccion Where ID=@IDTransaccion) Begin
		Set @Mensaje = ''El sistema no encuentra la transaccion!''
		Set @Procesado = ''False''
		return @@rowcount
	End
	
	--Transaccion
	If Not Exists(Select IDTransaccion From Venta Where IDTransaccion=@IDTransaccion) Begin
		Set @Mensaje = ''El sistema no encuentra la transaccion!''
		Set @Procesado = ''False''
		return @@rowcount
	End
	
	--BLOQUES
	if @Operacion=''INS'' Begin
	
		--Cantidad
		If @Cantidad <= 0 Begin
			Set @Mensaje = ''La cantidad no puede ser igual ni menor a 0!''
			Set @Procesado = ''False''
			return @@rowcount
		End	
		
		--Verificar Existencia
		If (Select dbo.FExistenciaProducto(@IDProducto, @IDDeposito)) < @Cantidad Begin
			Set @Mensaje = ''Stock insuficiente!''
			Set @Procesado = ''False''
			return @@rowcount
		End
		 
		--Insertar el detalle
		Insert Into DetalleVenta(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, DescuentoUnitario, PorcentajeDescuento, Total, TotalImpuesto, TotalDescuento, TotalDiscriminado, Caja, CantidadCaja)
		Values(@IDTransaccion, @IDProducto, @ID, @IDDeposito, @Observacion, @IDImpuesto, @Cantidad, @PrecioUnitario, @DescuentoUnitario, @PorcentajeDescuento, @Total, @TotalImpuesto, @TotalDescuento, @TotalDiscriminado, @Caja, @CantidadCaja)
		
		EXEC SpActualizarProductoDeposito
			@IDDeposito = @IDDeposito,
			@IDProducto = @IDProducto,
			@Cantidad = @Cantidad,
			@Signo = N''-'',
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
		
		Set @Mensaje = ''Registro guardado''
		Set @Procesado = ''True''
		return @@rowcount	
		
		--EXPRESS - Proceso de Tacticos
			
	End
	
	Declare @vID tinyint
	Declare @vIDDeposito tinyint
	Declare @vIDProducto int
	Declare @vCantidad decimal(10,2)
	
	if @Operacion=''DEL'' Begin
		
		Begin
			
			Declare db_cursor cursor for
			Select DV.ID, DV.IDProducto, DV.IDDeposito, DV.Cantidad
			From Venta V Join DetalleVenta DV On V.IDTransaccion=DV.IDTransaccion Join Producto P On DV.IDProducto=P.ID
			Where P.ControlarExistencia=''True'' And V.IDTransaccion=@IDTransaccion And V.Anulado=''False''
			Open db_cursor   
			fetch next from db_cursor into @vID, @vIDProducto, @vIDDeposito, @vCantidad   

			While @@FETCH_STATUS = 0 Begin  			  
					
				  EXEC SpActualizarProductoDeposito
					@IDDeposito = @vIDDeposito,
					@IDProducto = @vIDProducto,
					@Cantidad = @vCantidad,
					@Signo = N''+'',
					@Mensaje = @Mensaje OUTPUT,
					@Procesado = @Procesado OUTPUT
					
					--Siguiente
				   fetch next from db_cursor into @vID, @vIDProducto, @vIDDeposito, @vCantidad   
			End   

			Close db_cursor   
			deallocate db_cursor
		End
		
		--Eliminamos los registros
		Delete From DetalleVenta Where IDTransaccion=@IDTransaccion
						
		Set @Mensaje = ''Registro guardado''
		Set @Procesado = ''True''
		return @@rowcount	
			
	End
	
	if @Operacion=''ANULAR'' Begin
		
		Begin
						
			Declare db_cursor cursor for
			Select DV.ID, DV.IDProducto, DV.IDDeposito, DV.Cantidad
			From Venta V Join DetalleVenta DV On V.IDTransaccion=DV.IDTransaccion Join Producto P On DV.IDProducto=P.ID
			Where P.ControlarExistencia=''True'' And V.IDTransaccion=@IDTransaccion And V.Anulado=''False''
			Open db_cursor   
			fetch next from db_cursor into @vID, @vIDProducto, @vIDDeposito, @vCantidad   

			While @@FETCH_STATUS = 0 Begin  			  
					
				  EXEC SpActualizarProductoDeposito
					@IDDeposito = @vIDDeposito,
					@IDProducto = @vIDProducto,
					@Cantidad = @vCantidad,
					@Signo = N''+'',
					@Mensaje = @Mensaje OUTPUT,
					@Procesado = @Procesado OUTPUT
					
					--Siguiente
				   fetch next from db_cursor into @vID, @vIDProducto, @vIDDeposito, @vCantidad   
			End   

			Close db_cursor   
			deallocate db_cursor
		End
						
		Set @Mensaje = ''Registro guardado''
		Set @Procesado = ''True''
		return @@rowcount	
			
	End
	
	Set @Mensaje = ''No se proceso!''
	Set @Procesado = ''False''
	return @@rowcount
	
End

' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpControlarVenta]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpControlarVenta]

	--Entrada
	@IDTransaccion numeric(18,0)
	
As

Begin

	Declare @vProcesado bit
	Declare @vMensaje varchar(100)
	Declare @vHabilitado bit
	
	Declare @vIDProducto int
	Declare @vIDDeposito int
	Declare @vCantidad decimal(10,2)
	Declare @vExistencia decimal(10,2)
	Declare @vProducto varchar(50)
	Declare @vReferencia varchar(50)
	
	Set @vMensaje = ''No se proceso''
	Set @vProcesado = ''False''
	Set @vHabilitado = ''False''
	
	--CONTROLAR CANTIDAD DE PRODUCTOS
	Declare db_DetalleVenta cursor for
	Select IDProducto, IDDeposito, ''Cantidad''=Sum(Cantidad) 
	From VDetalleVenta 
	Where IDTransaccion=@IDTransaccion
	Group By IDProducto, IDDeposito
	
	Open db_DetalleVenta 
	Fetch Next From db_DetalleVenta Into @vIDProducto, @vIDDeposito, @vCantidad
	While @@FETCH_STATUS = 0 Begin 

		Set @vExistencia = IsNull((Select Sum(Existencia) From ExistenciaDeposito Where IDProducto=@vIDProducto And IDDeposito=@vIDDeposito),0)
		
		Set @vProducto = (Select Descripcion From Producto Where ID=@vIDProducto)
		Set @vReferencia = (Select Referencia From Producto Where ID=@vIDProducto)
		
		print @vProducto + '': '' +CONVERT(varchar(50), @vCantidad) + '' - '' + CONVERT(varchar(50), @vExistencia)	
		
		--Controlar Existencia
		If @vExistencia<@vCantidad Begin
			
			
			
			Set @vMensaje = ''No hay suciciente stock! '' + @vProducto + '' ('' + @vReferencia + '')'' + '' - Cantidad: '' + CONVERT(varchar(50), @vCantidad)
			Set @vProcesado = ''False''
			Set @vHabilitado = ''False''	
	
			--Cierra el cursor
			Close db_DetalleVenta
			Deallocate db_DetalleVenta
			
			GoTo Salir
			
		End
		
		Fetch Next From db_DetalleVenta Into @vIDProducto, @vIDDeposito, @vCantidad
									
	End

	--Cierra el cursor
	Close db_DetalleVenta
	Deallocate db_DetalleVenta

	Set @vMensaje = ''Habilitado''
	Set @vProcesado = ''True''
	Set @vHabilitado = ''True''	
	
Salir:
	Select ''Procesado''=@vProcesado, ''Mensaje''=@vMensaje, ''Habilidado''=@vHabilitado	
End' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpCobranzaCreditoProcesar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpCobranzaCreditoProcesar]

	--Entrada
	@IDTransaccion numeric(18,0),
	@Operacion varchar(10),
	
	--Salida
	@Mensaje varchar(100) output,
	@Procesado bit output
	
As

Begin

	Set @Mensaje = ''No procesado''
	Set @Procesado = ''False''
		
	--Aplicar Cobranza
	If @Operacion = ''INS'' Begin
	
		--Procesamos las Ventas y Saldo del Cliente
		EXEC SpVentaCobranza @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Procesamos los Cheques si es que hay
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Generar Asiento
		Exec SpAsientoCobranzaCredito @IDTransaccion=@IDTransaccion
		
		--Damos el OK a la Cobranza
		If @Procesado = ''True'' Begin
			Update CobranzaCredito Set Procesado=''True'' Where IDTransaccion=@IDTransaccion
			Set @Mensaje = ''Registro procesado!''
			GoTo Salir			
		End
		
		If @Procesado = ''True'' Begin
			--Si hubo un error, eliminar todo el registro
			Set @Operacion = ''DEL''			
		End
				
	End
	
	--Eliminamos todo el registro
	If @Operacion = ''DEL'' Begin
		
		--Reestablecemos las Ventas y Saldo del Cliente
		EXEC SpVentaCobranza @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Reestablecemos el cheque
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
				
		Delete From Efectivo Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoDocumento Where IDTransaccion = @IDTransaccion
		Delete From FormaPago Where IDTransaccion = @IDTransaccion
		Delete From VentaCobranza Where IDTransaccionCobranza = @IDTransaccion
		Delete From CobranzaCredito Where IDTransaccion = @IDTransaccion
				
	End
	
Salir:
	
	--Actualizar saldos de clientes
	Begin
		Declare @vIDTransaccionVenta numeric(18,0)
		Declare @vIDCliente int
		
		Declare db_cursor cursor for
		Select IDTransaccionVenta From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta
		While @@FETCH_STATUS = 0 Begin 
			
			Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@vIDTransaccionVenta)
			
			Exec SpAcualizarSaldoCliente @IDCliente=@vIDCliente
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta
										
		End
		
		Close db_cursor   
		Deallocate db_cursor
		
	End
End

' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpCobranzaCredito]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpCobranzaCredito]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@Numero int,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante varchar(50) = NULL,
	@Comprobante varchar(50) = NULL,	
	@IDCliente int = NULL,
	@FechaEmision date = NULL,
	@IDCobrador tinyint = NULL, 
	@Total money = NULL,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	@Observacion varchar(200) = NULL,
	@Operacion varchar(50),
	@NroPlanilla varchar(10),
		
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
		
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
	
	--INSERTAR
	If @Operacion = ''INS'' Begin
	
		--Validar
		--Numero
		Declare @vNumero int
		Set @vNumero = ISNull((Select MAX(Numero)+1 From CobranzaCredito Where IDSucursal=@IDSucursalOperacion),1)
		
		--Si ya existe el documento
		If @NroComprobante!='''' Begin
			If Exists(Select * From CobranzaCredito Where IDTipoComprobante=@IDTipoComprobante And NroComprobante=@NroComprobante And IDSucursal=@IDSucursalOperacion And Anulado=''False'' ) Begin
				set @Mensaje = ''El numero de comprobante ya existe!''
				set @Procesado = ''False''
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		End
		
		--Total
		If @Total <= 0 Begin
			set @Mensaje = ''El importe debe ser mayor a 0!''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End

		--Insertar Transaccion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
			
		--Insertar en Cobranza Credito
		Insert Into CobranzaCredito(IDTransaccion, IDSucursal, Numero, IDTipoComprobante, NroComprobante, Comprobante,NroPlanilla ,IDCliente, FechaEmision, IDCobrador, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Anulado, FechaAnulado, IDUsuarioAnulado, Observacion )
		Values(@IDTransaccionSalida, @IDSucursalOperacion, @vNumero, @IDTipoComprobante, @NroComprobante, @Comprobante, @NroPlanilla,   @IDCliente, @FechaEmision, @IDCobrador, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, ''False'', NULL, NULL, @Observacion)
					
		set @Mensaje = ''Registro guardado''
		set @Procesado = ''True''
		return @@rowcount
		
	End
	
	--ANULAR
	If @Operacion = ''ANULAR'' Begin
	
		--Validar
		--Si el efectivo ya se usó en una orden de pago
		If Exists(Select * From VOrdenPagoEfectivo OPE Join Efectivo E on OPE.IDTransaccion=E.IDTransaccion Where E.IDTransaccion= @IDTransaccion) Begin
			set @Mensaje = ''Este efectivo ya fué usado en una orden de pago.''
			set @Procesado = ''False''
			return @@rowcount
		End
				
		--Si el efectivo esta depositado
		If (Select Depositado From Efectivo Where IDTransaccion=@IDTransaccion) > 0 Begin
			set @Mensaje = ''Parte o el parcial del importe pagado en efectivo ya fue depositado! No se puede anular el registro.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Si los cheques estan depositados
		If (Select CC.Depositado From FormaPago FP Join ChequeCliente CC on FP.IDTransaccionCheque=CC.IDTransaccion Where FP.IDTransaccion=@IDTransaccion) = 1 Begin
			set @Mensaje = ''El documento se pago con un cheque que ya fue depositado! No se puede anular el registro.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado=''True'') Begin
			set @Mensaje = ''El asiento de este documento ya esta conciliado! No se puede anular.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Verificar dias de bloqueo
		If (Select Top(1) CobranzaCreditoBloquearAnulacion From Configuraciones) = ''True'' Begin
			
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) CobranzaCreditoDiasBloqueo From Configuraciones)

			If (Select DATEDIFF(dd,(Select V.FechaEmision From CobranzaCredito V Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = ''El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!''
				set @Procesado = ''False''
				return @@rowcount
				
			End
			
		End
		
		--Procesar
		--Reestablecemos las Ventas y Saldo del Cliente
		EXEC SpVentaCobranza @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Reestablecemos el cheque
		EXEC SpChequeClienteSaldar @IDTransaccionCobranza = @IDTransaccion, @Operacion = @Operacion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la forma de pago		
		Delete From Efectivo Where IDTransaccion = @IDTransaccion
		Delete From FormaPagoDocumento Where IDTransaccion = @IDTransaccion
		Delete From FormaPago Where IDTransaccion = @IDTransaccion
				
		--Anulamos el registro
		Update CobranzaCredito Set Anulado=''True'', IDUsuarioAnulado=@IDUsuario, FechaAnulado=GETDATE()
		Where IDTransaccion = @IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal		
		
		set @Mensaje = ''Registro guardado''
		set @Procesado = ''True''
		Set @IDTransaccionSalida = @IDTransaccion
		GoTo Salir
		
	End
	
	--ELIMINAR
	If @Operacion = ''DEL'' Begin
	
		--Validar
		--Si el efectivo esta depositado
		
		--Si los cheques estan depositados
		
		--Controlar los asientos asociados
		
		--Procesar
		--Eliminar la Forma de Pago (Eliminar el efectivo y actualizarlo - Reestablecer el saldo del cheque)

		--Eliminar relacion de las ventas asociadas (Actualizar saldos de la venta y cliente)
		
		--Eliminamos el registro
		Delete CobranzaCredito Where IDTransaccion = @IDTransaccion
		
		--Eliminamos los asientos
		Delete DetalleAsiento Where IDTransaccion = @IDTransaccion
		Delete Asiento Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
		
		set @Mensaje = ''Registro guardado''
		set @Procesado = ''True''
		Set @IDTransaccionSalida = @IDTransaccion
		GoTo Salir
		
	End

Salir:
	
	--Actualizar saldos de clientes
	Begin
		Declare @vIDTransaccionVenta numeric(18,0)
		Declare @vIDCliente int
		
		Declare db_cursor cursor for
		Select IDTransaccionVenta From VentaCobranza Where IDTransaccionCobranza=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vIDTransaccionVenta
		While @@FETCH_STATUS = 0 Begin 
			
			Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@vIDTransaccionVenta)
			
			Exec SpAcualizarSaldoCliente @IDCliente=@vIDCliente
			
			Fetch Next From db_cursor Into @vIDTransaccionVenta
										
		End
		
		Close db_cursor   
		Deallocate db_cursor
		
	End
		
End

	
	
' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpEfectivo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpEfectivo]

	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDSucursalDocumento tinyint = NULL,
	@Numero int = NULL,
	@Fecha date = NULL,
	@IDTipoComprobante smallint = NULL,
	@Comprobante varchar(50) = NULL,
	
	@IDMotivo tinyint = NULL,
	@Observacion varchar(200) = NULL,
	
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@ImporteMoneda money = NULL,
	@Total money = NULL,
	
	--Operacion
	@Operacion varchar(50),
	
	--Transaccion
	@IDOperacion tinyint,
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--Variable
	
	--BLOQUES
		
	--INSERTAR
	If @Operacion = ''INS'' Begin
	
		--Validar
		Declare @vNumero int
		Declare @vID int
		Set @vNumero = IsNull((Select MAX(Numero)+1 From Efectivo Where IDSucursal=@IDSucursalDocumento),1)
		Set @vID = 0
		
		----Insertar la transaccion				
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @IDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		--Insertar el Registro de Gasto		
		Insert Into Efectivo(IDTransaccion, ID, IDSucursal, Numero, IDTipoComprobante, Comprobante, Fecha, IDMoneda, ImporteMoneda, Cotizacion, IDMotivo, Depositado, Cancelado, Total, Saldo, ImporteHabilitado, Habilitado, Observacion)
		Values(@IDTransaccionSalida, @vID, @IDSucursalDocumento, @vNumero, @IDTipoComprobante, @Comprobante, @Fecha, @IDMoneda, @ImporteMoneda, @Cotizacion, @IDMotivo, 0, ''False'', @Total, @Total, 0, ''False'', @Observacion)
								
		set @Mensaje = ''Registro guardado''
		set @Procesado = ''True''
		return @@rowcount
			
	End
	
	If @Operacion = ''DEL'' Begin
	
		--Solo si esta anulado
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Efectivo  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End

		--Verificar que el documento no tenga otros registros asociados
		If (Select Depositado From Efectivo  Where IDTransaccion=@IDTransaccion) > 0 Begin
			set @Mensaje = ''El registro ya esta depositado!''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		If Exists(Select * From DetalleDepositoBancario Where IDTransaccionEfectivo=@IDTransaccion) Begin
			set @Mensaje = ''El registro ya esta depositado!''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Eliminamos el registro
		Delete Efectivo Where IDTransaccion = @IDTransaccion
		
		--Eliminamos la transaccion
		Delete Transaccion Where ID = @IDTransaccion
				
		Set @Mensaje = ''Registro guardado!''
		Set @Procesado = ''True''
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = ''UPD'' Begin
		
		--Si esta anulado
		If (Select Anulado From Efectivo  Where IDTransaccion=@IDTransaccion) = ''True'' Begin
			set @Mensaje = ''El registro esta anulado! No se puede modificar.''
			set @Procesado = ''False''
			return @@rowcount
		End
				
		--Actualizar Datos
		Update Efectivo Set Fecha=@Fecha,	
							IDTipoComprobante=@IDTipoComprobante,
							Comprobante=@Comprobante,     
							IDMotivo=@IDMotivo,
							Observacion=@Observacion						
		Where IDTransaccion=@IDTransaccion     

		If Not Exists(Select * From DetalleDepositoBancario Where IDTransaccionEfectivo=@IDTransaccion) Begin
			Update Efectivo Set IDMoneda=@IDMoneda,
								Cotizacion=@Cotizacion,
								ImporteMoneda=@ImporteMoneda,
								Total=@Total
			Where IDTransaccion=@IDTransaccion     
		End
									 	
		Set @Mensaje = ''Registro guardado!''
		Set @Procesado = ''True''
		Set @IDTransaccionSalida = @IDTransaccion
		print @Mensaje
		return @@rowcount
			
	End
	
	--ANULAR
	If @Operacion = ''ANULAR'' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Efectivo  Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End

		--Si esta anulado
		If (Select Anulado From Efectivo  Where IDTransaccion=@IDTransaccion) = ''True'' Begin
			set @Mensaje = ''El registro esta anulado! No se puede modificar.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Verificar que el documento no tenga otros registros asociados
		If (Select Depositado From Efectivo  Where IDTransaccion=@IDTransaccion) > 0 Begin
			set @Mensaje = ''El registro ya esta depositado!''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		If Exists(Select * From DetalleDepositoBancario Where IDTransaccionEfectivo=@IDTransaccion) Begin
			set @Mensaje = ''El registro ya esta depositado!''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Eliminamos el registro
		Update Efectivo Set Anulado=''True''
		Where IDTransaccion = @IDTransaccion
		
		--Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal		
				
		Set @Mensaje = ''Registro guardado!''
		Set @Procesado = ''True''
		print @Mensaje
		return @@rowcount
		
	End
	
	set @Mensaje = ''No se proceso ninguna transacción!''
	set @Procesado = ''False''
	set @IDTransaccionSalida  = ''0''
	return @@rowcount
		
End



' 
END





IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TICalcularTotalEfectivo]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[TICalcularTotalEfectivo] ON  [dbo].[Efectivo]
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;

    Declare @vTotal money
	Declare @vSaldo money
	
	If Exists(Select * From Inserted) And Not Exists(Select * From Deleted) Begin

		Set @vTotal = (Select Cotizacion From Inserted)	* (Select ImporteMoneda From Inserted)	
		Set @vSaldo = (Select Cotizacion From Inserted)	* (Select ImporteMoneda From Inserted)	
		
	End

END
'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VResumenCobranzaDiariaFormaPago]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VResumenCobranzaDiariaFormaPago]
As
Select 
''Fecha''=Convert(Date, C.FechaEmision),
''TipoComprobante''=''EFE'',
C.IDSucursal,
''Cantidad''=COUNT(*),
''Valores''=SUM(FP.Importe),
''Planilla''=C.NroPlanilla,
C.IDCobrador
From FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Where FP.Efectivo=''True'' And C.Anulado=''False''
Group By C.FechaEmision, C.IDSucursal,C.NroPlanilla,C.IDCobrador

Union All

Select 
''Fecha''=Convert(Date, C.FechaEmision),
''TipoComprobante''=CHQ.CodigoTipo,
C.IDSucursal,
''Cantidad''=COUNT(*),
''Valores''=SUM(FP.ImporteCheque),
''Planilla''=C.NroPlanilla,
C.IDCobrador
From FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join VChequeCliente CHQ On FP.IDTransaccionCheque=CHQ.IDTransaccion
Where FP.Cheque=''True'' And C.Anulado=''False''
Group By C.FechaEmision, C.IDSucursal,C.NroPlanilla,CHQ.CodigoTipo,C.IDCobrador

Union All

--Documento
Select 
''Fecha''=Convert(Date, C.FechaEmision),
''TipoComprobante''=TC.Codigo,
C.IDSucursal,
''Cantidad''=COUNT(*),
''Valores''=SUM(FP.ImporteDocumento),
''Planilla''=C.NroPlanilla,
C.IDCobrador
From FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Where FP.Documento=''True''  And C.Anulado=''False''
Group By C.FechaEmision, C.IDSucursal,C.NroPlanilla,TC.Codigo,C.IDCobrador





'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VPedidosAFacturar]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VPedidosAFacturar]
As
Select
''ZonaVenta''=Case When (C.ZonaVenta) = '''' Then  ''---'' Else (C.ZonaVenta) End,
''IDZonaVenta''=IsNull(C.IDZonaVenta, 0),
V.FechaFacturar,
V.Vendedor,
V.IDVendedor,
V.Cliente,
''Dias''=DATEDIFF(d, GETDATE(), V.FechaFacturar),
V.IDSucursal
From VPedido V
Join VCliente C On V.IDCliente=C.ID
Where V.Anulado = ''False'' 
And V.Facturado = ''False'' 

--Sacar control de fecha, se hace desde el programa!
--And DATEDIFF(d, GETDATE(), V.FechaFacturar) >= 0



'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VFormaPagoDocumento]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VFormaPagoDocumento]
As
Select 

FPD.IDTransaccion,
FPD.ID,
FPD.IDTipoComprobante,
''TipoComprobante''=TC.Descripcion,
''CodigoComprobante''=TC.Codigo,
''ComprobanteLote''=0,
TC.IDOperacion,
FPD.IDSucursal,
FPD.Comprobante,
FPD.Fecha,
''Fec''=CONVERT(varchar(50), FPD.Fecha, 6),
FPD.IDMoneda,
''Moneda''=M.Referencia,
FPD.ImporteMoneda,
FPD.Cotizacion,
FPD.Total,
''Saldo''=ISNULL(FPD.Saldo,0),
''Importe''=FPD.Saldo,
FPD.Observacion,
''Cancelado''=ISNULL(FPD.Cancelado,0),
''Deposito''=ISNULL(TC.Deposito,''False''),
TC.Restar


From FormaPagoDocumento FPD
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID














'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VFormaPagoCobranzaCredito]'))
EXEC dbo.sp_executesql @statement = N'
CREATE View [dbo].[VFormaPagoCobranzaCredito]

As

--Efectivo
Select
FP.IDTransaccion,
FP.ID,
TC.IDOperacion,
''FormaPago''=''Efectivo'',
''Moneda''=M.Referencia,
''ImporteMoneda''=E.ImporteMoneda,
''Cambio''=E.Cotizacion,
''Banco''=''---'',
''Comprobante''=E.Comprobante,
''FechaEmision''=convert(varchar(50),E.Fecha,3),
''Vencimiento''=NULL,
''TipoComprobante''=TC.Descripcion,
''CodigoTipoComprobante''=TC.Codigo,
''Tipo''=''---'',
''Importe''=FP.Importe,
''CancelarCheque''=FP.CancelarCheque,
TC.Restar,
''Efectivo''=''True'',
''Cheque''=''False'',
''Tarjeta''=''False'' 

From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID

Union All

--Cheque Cliente
Select
FP.IDTransaccion,
FP.ID,
V.IDOperacion,
''FormaPago''=''Cheque'',
''Moneda''=V.Moneda,
''ImporteMoneda''=FP.ImporteCheque,
''Cambio''=V.Cotizacion,
''Banco''=V.Banco,
''Comprobante''=V.NroCheque,
''FechaEmision''=convert(varchar(50),V.Fecha,3),
''Vencimiento''=convert(varchar(50),V.FechaVencimiento,3),
''TipoComprobante''=(Case When (V.Diferido)=''True'' Then ''CHEQUE DIFERIDO'' Else ''CHEQUE'' End),
''CodigoTipoComprobante''=(Case When (V.Diferido)=''True'' Then ''DIF'' Else ''CHQ'' End),
''Tipo''=V.Tipo,
''Importe''=FP.Importe,
FP.CancelarCheque,
''Restar''=0,
''Efectivo''=''False'',
''Cheque''=''True'',
''Tarjeta''=''False'' 


From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion

Union All

--Documento
Select
FP.IDTransaccion,
FP.ID,
TC.IDOperacion,
''FormaPago''=''Documento'',
''Moneda''=M.Referencia,
''ImporteMoneda''=FPD.ImporteMoneda,
''Cambio''=FPD.Cotizacion,
''Banco''=''---'',
''Comprobante''=FPD.Comprobante,
''FechaEmision''=convert(varchar(50),FPD.Fecha,3),
''Vencimiento''=NULL,
''TipoComprobante''=TC.Descripcion,
''CodigoTipoComprobante''=TC.Codigo,
''Tipo''=''---'',
''Importe''=FP.Importe,
FP.CancelarCheque,
TC.Restar,
''Efectivo''=''False'',
''Cheque''=''False'' ,
''Tarjeta''=''False''


From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID



UNION ALL

--Tarjeta
Select
FP.IDTransaccion,
FP.ID,
TC.IDOperacion,
''FormaPago''=''Efectivo'',
''Moneda''=M.Referencia,
''ImporteMoneda''=E.ImporteMoneda,
''Cambio''=E.Cotizacion,
''Banco''=''---'',
''Comprobante''=E.Comprobante,
''FechaEmision''=convert(varchar(50),E.Fecha,3),
''Vencimiento''=NULL,
''TipoComprobante''=TC.Descripcion,
''CodigoTipoComprobante''=TC.Codigo,
''Tipo''=''---'',
''Importe''=FP.Importe,
''CancelarCheque''=FP.CancelarCheque,
TC.Restar,
''Efectivo''=''False'',
''Cheque''=''False'' ,
''Tarjeta''=''True''

From FormaPago FP
JOIN dbo.FormaPagoTarjeta E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID







'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VFormaPago]'))
EXEC dbo.sp_executesql @statement = N'

CREATE View [dbo].[VFormaPago]
As

--Efectivo
	Select
	FP.IDTransaccion,
	FP.ID,
	TC.IDOperacion,
	''FormaPago''=''Efectivo'',
	''Moneda''=M.Referencia,
	''ImporteMoneda''=E.ImporteMoneda,
	''Cambio''=E.Cotizacion,
	''Banco''=''---'',
	''Comprobante''=E.Comprobante,
	''TipoComprobante''=TC.Codigo,
	''Tipo''=''EFE'',
	''Importe''=FP.Importe,
	''CancelarCheque''=FP.CancelarCheque,
	FP.Cheque,
	FP.Documento,
	FP.Efectivo
	
	From FormaPago FP
	Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
	Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
	Join Moneda M On E.IDMoneda=M.ID

	Union All

	--Cheque Cliente
	Select
	FP.IDTransaccion,
	FP.ID,
	V.IDOperacion,
	''FormaPago''=''Cheque'',
	''Moneda''=V.Moneda,
	''ImporteMoneda''=V.ImporteMoneda,
	''Cambio''=V.Cotizacion,
	''Banco''=V.Banco,
	''Comprobante''=V.NroCheque,
	''TipoComprobante''=(Case When (V.Diferido)=''True'' Then ''DIF'' Else ''CHQ'' End),
	''Tipo''=V.Tipo,
	''Importe''=FP.Importe,
	FP.CancelarCheque,
	FP.Cheque,
	FP.Documento,
	FP.Efectivo

	From FormaPago FP
	Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion

	Union All

	--Documento
	Select
	FP.IDTransaccion,
	FP.ID,
	TC.IDOperacion,
	''FormaPago''=''Documento'',
	''Moneda''=M.Referencia,
	''ImporteMoneda''=FPD.ImporteMoneda,
	''Cambio''=FPD.Cotizacion,
	''Banco''=''---'',
	''Comprobante''=FPD.Comprobante,
	''TipoComprobante''=TC.Codigo,
	''Tipo''=''DOC'',
	''Importe''=FP.Importe,
	FP.CancelarCheque,
	FP.Cheque,
	FP.Documento,
	FP.Efectivo

	From FormaPago FP
	Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
	Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
	Join Moneda M On FPD.IDMoneda=M.ID






'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VEfectivo]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VEfectivo]
As
Select 

E.IDTransaccion,
E.ID,
E.IDTipoComprobante,
''TipoComprobante''=TC.Descripcion,
''CodigoComprobante''=TC.Codigo,
E.IDSucursal,
E.Comprobante,
E.Numero,
E.Fecha,
''Fec''=CONVERT(varchar(50), E.Fecha, 6),
''VentasCredito''=(Case When (E.VentasCredito) != 0 Then ''True'' Else ''False'' End),
''VentasContado''=(Case When (E.VentasContado) != 0 Then ''True'' Else ''False'' End),
''Gastos''=(Case When (E.Gastos) != 0 Then ''True'' Else ''False'' End),
''Tipo''=(Case When (E.VentasCredito) != 0 Then ''VENTA CREDITO'' Else (Case When (E.VentasContado) != 0 Then ''VENTA CONTADO'' Else (Case When (E.Gastos) != 0 Then ''GASTO'' Else ''---'' End) End) End),
E.IDMoneda,
''Moneda''=M.Referencia,
E.ImporteMoneda,
E.Cotizacion,
''Importe''=E.VentasCredito + E.VentasContado + E.Gastos,
E.Total,
E.Observacion,
E.Depositado,
''Saldo''=ISNULL(E.Saldo,0),
E.Cancelado,
''Estado''=(Case When (E.Cancelado) = ''True'' Then ''Cancelado'' Else ''Pendiente'' End),
E.Habilitado,
''Habilitado Pago''= (Case when (E.Habilitado)=''True'' Then ''SI'' Else ''---'' End),
''Select''=(Case when (E.Habilitado)=''True'' Then ''True'' Else ''False'' End),
''Generado''=O.Descripcion,
''ImporteHabilitado''=ISNUll(E.ImporteHabilitado,E.Saldo),
CC.IDCobrador,
CC.Cobrador,

--Motivo
E.IDMotivo,
''Motivo''=ME.Descripcion,

--Cobranza 
''NroOperacionCobranza''=CC.Numero,

--Transaccion
''FechaTransaccion''=T.Fecha,
''IDDepositoTransaccion''=T.IDDeposito,
''IDSucursalTransaccion''=T.IDSucursal,
''IDTerminalTransaccion''=T.IDTerminal,
T.IDUsuario,
''usuario''=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
''UsuarioIdentificador''=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
E.Anulado,
''IDUsuarioAnulacion''=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion), 0)),
''UsuarioAnulacion''=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), ''---'')),
''UsuarioIdentificacionAnulacion''=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), ''---'')),
''FechaAnulacion''=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion)

From Efectivo E
JOin Transaccion T On E.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Left Outer Join VCobranzaCredito  CC On CC.IDTransaccion=E.IDTransaccion
Left Outer Join MotivoEfectivo ME On E.IDMotivo=ME.ID



















'





IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VChequesClienteFormaPago]'))
EXEC dbo.sp_executesql @statement = N'CREATE View [dbo].[VChequesClienteFormaPago]
As

Select
V.IDTransaccion,
''ID''=0,
''Sel''=''False'',
V.Ciudad,
V.Banco,
V.NroCheque,
V.CuentaBancaria,
''Fecha''=convert(varchar(50),V.Fecha,3),
V.Fec,
''FechaVencimiento''=convert(varchar(50),V.FechaVencimiento,3),
V.Moneda,
V.ImporteMoneda,
V.Cotizacion,
''Total''=V.Importe,
V.Saldo,
''Importe''=V.Saldo,
V.IDCliente,
V.Cliente,
V.Tipo,
V.Diferido,
V.IDMoneda,
''Cancelar''=''False''

From VChequeCliente V
Where V.Cancelado=''False''






'





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpVentaProcesar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpVentaProcesar]

	--Entrada
	@IDTransaccion numeric(18,0),
	@EsPedido bit = ''False'',
	@EsMovil bit = ''False''	
	
As

Begin

	--Variables
	Declare @Mensaje varchar(200)
	Declare @Procesado bit
	Declare @vComprobante varchar(50)
	Declare @vNroComprobante int
	Declare @vIDPuntoExpedicion int
	Declare @vCantidad decimal(18,0)
	Declare @vIDProducto int
	Declare @vIDDeposito tinyint
	Declare @vIDCliente int
		
	--Obtener valores
	Set @vComprobante = (Select Comprobante From Venta Where IDTransaccion=@IDTransaccion)
	Set @vNroComprobante = (Select NroComprobante From Venta Where IDTransaccion=@IDTransaccion)
	Set @vIDPuntoExpedicion = (Select IDPuntoExpedicion From Venta Where IDTransaccion=@IDTransaccion)
	Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@IDTransaccion)
	
	Set @Mensaje = ''No se realizo ninguna accion''
	Set @Procesado = ''False''
	
	--Validar
	
	--VENTA
	Begin
	
		--Transaccion
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''Error: El sistema no encuentra el registro de venta! Intente nuevamente.''
			set @Procesado = ''False''
			Goto Error
		End
		
		--Comprobante
		If Exists(Select * From Venta Where IDPuntoExpedicion=@vIDPuntoExpedicion And NroComprobante=@vNroComprobante And Procesado=''True'') Begin
			set @Mensaje = ''El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.''
			set @Procesado = ''False''
			Goto Error
		End
		
		--Comprobante fuera del Rango
		If @vNroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Or @vNroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@vIDPuntoExpedicion) Begin
			set @Mensaje = ''El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.''
			set @Procesado = ''False''
			Goto Error
		End
		
		--Credito
		If (Select Credito From Venta Where IDTransaccion=@IDTransaccion)=''True'' Begin
			
			--Fecha
			If DATEDIFF(dd, (Select FechaEmision From Venta Where IDTransaccion=@IDTransaccion), (Select FechaVencimiento From Venta Where IDTransaccion=@IDTransaccion)) < 0 Begin
				set @Mensaje = ''La fecha de de vencimiento no puede ser menor al del comprobante!''
				set @Procesado = ''False''
				Goto Error
			End
		
			--Plazo credito
			Declare @vPlazoCredito smallint
			Set @vPlazoCredito = (Select ISNULL((Select PlazoCredito From Cliente Where ID=@vIDCliente),0))
			
			If DATEDIFF(dd, (Select FechaEmision From Venta Where IDTransaccion=@IDTransaccion), (Select FechaVencimiento From Venta Where IDTransaccion=@IDTransaccion)) > @vPlazoCredito Begin
				set @Mensaje = ''El plazo de credito es mayor al del que tiene asignado el cliente!''
				set @Procesado = ''False''
				Goto Error
			End
			
		End
	End
		
	--Stock
	Begin
		
		Declare db_cursor cursor for
		Select Cantidad, IDProducto, IDDeposito From DetalleVenta Where IDTransaccion=@IDTransaccion
		Open db_cursor   
		Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vIDDeposito
		While @@FETCH_STATUS = 0 Begin  
		
			--Cantidad
			If @vCantidad <= 0 Begin
				Set @Mensaje = ''La cantidad no puede ser igual ni menor a 0!''
				Set @Procesado = ''False''
				Goto Error		
			End	
			
			--Verificar Existencia
			Declare @vExistenciaProducto decimal(10,2)
			Declare @vExistenciaReservado decimal(10,2)
			Declare @vExistenciaReal decimal(10,2)
			
			If @EsPedido=''True'' Begin
				--No funciona con VENTAS el reservado, el calculo es erroneo.
				--Set @vExistenciaReservado = dbo.FExistenciaProductoReservado(@vIDProducto, @vIDDeposito)
				--Set @vExistenciaReservado = 0
				--Set @vExistenciaReal = dbo.FExistenciaProductoReal(@vIDProducto, @vIDDeposito)
				--Set @vExistenciaProducto = @vExistenciaReal - (@vExistenciaReservado - @vCantidad)
				
				Set @vExistenciaProducto = dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito)
				
			End Else Begin
				Set @vExistenciaProducto = dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito)
			End
			
			If  @vExistenciaProducto < @vCantidad Begin
				Declare @vProducto varchar(50)
				Declare @vExistencia decimal(18,0)
				
				Set @vProducto = (Select Descripcion From Producto Where ID=@vIDProducto)
				Set @vExistencia = (Select dbo.FExistenciaProducto(@vIDProducto, @vIDDeposito))
				
				Set @Mensaje = ''Stock insuficientes! '' + CONVERT(varchar(50), @vProducto) + '': '' + CONVERT(varchar(50), @vExistencia)
				Set @Procesado = ''False''
				
				Close db_cursor   
				Deallocate db_cursor
				
				Goto Error				
			End
			
			Fetch Next From db_cursor Into @vCantidad, @vIDProducto, @vIDDeposito
			
		End
		
		Close db_cursor   
		Deallocate db_cursor	
		
	End
	
	--Actividades
	Begin
	
		Declare @vHabilitar bit
		Set @vHabilitar = ''False''
	
		If @vHabilitar = ''True'' Begin
			Set @Mensaje = ''Stock insuficientes! '' + CONVERT(varchar(50), @vProducto) + '': '' + CONVERT(varchar(50), @vExistencia)
			Set @Procesado = ''False''
											
			Goto Error		
		End
		
	End
	
	--Procesar
	Begin
	
		--Actualizar Punto de Expedicion
		Update PuntoExpedicion Set UltimoNumero=@vNroComprobante
		Where ID=@vIDPuntoExpedicion
		
		--Actualizar los productos
		Declare db_cursorProducto cursor for
		Select Cantidad, IDProducto, IDDeposito From DetalleVenta Where IDTransaccion=@IDTransaccion
		Open db_cursorProducto   
		Fetch Next From db_cursorProducto Into @vCantidad, @vIDProducto, @vIDDeposito
		While @@FETCH_STATUS = 0 Begin  
			
			EXEC SpActualizarProductoDeposito
				@IDDeposito = @vIDDeposito,
				@IDProducto = @vIDProducto,
				@Cantidad = @vCantidad,
				@Signo = N''-'',
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
						 			
			Fetch Next From db_cursorProducto Into @vCantidad, @vIDProducto, @vIDDeposito
			
		End
		
		Close db_cursorProducto   
		Deallocate db_cursorProducto	
		
		Update Venta Set Procesado=''True''
						--IDVendedor=@vIDVendedor
		Where IDTransaccion=@IDTransaccion
	
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		EXEC SpAcualizarClienteVenta
			@IDCliente = @vIDCliente,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
			
		
		--Si MOVIL = ''False'' ejecutamos los siguientes procesos
		If @EsMovil = ''False'' Begin
		
			--Actualizar los descuentos tacticos
			EXEC SpActualizarActividad
				@Operacion = ''INS'',
				@IDTransaccion = @IDTransaccion,
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
					
			--Generar Asiento - OCULTAR TEMPORALMENTE
			Exec SpAsientoVenta @IDTransaccion=@IDTransaccion
			
			--Cargar Comisiones
			EXEC SpComision
				@IDTransaccion=@IDTransaccion,
				@Venta=''True'',
				@Devolucion=''False'',
				@Operacion= ''INS''
		
		End
		
		Select ''Procesado''=''True'', ''Mensaje''=''Registro procesado!'', ''Procesados''=@@ROWCOUNT
		Return @@Rowcount
			
	End
			
Error:
	
	--Si se produjo un error, eliminar toda la transaccion
	--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit
	
	EXEC SpAcualizarClienteVenta
		@IDCliente = @vIDCliente,
		@Mensaje = @vMensaje OUTPUT,
		@Procesado = @vProcesado OUTPUT
			
	--Eliminamos el Asiento
	Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
	Delete From Asiento Where IDTransaccion=@IDTransaccion	
	
	Delete From DetalleImpuesto Where IDTransaccion=@IDTransaccion	
	Delete From Descuento Where IDTransaccion=@IDTransaccion	
	Delete From DetalleVenta Where IDTransaccion=@IDTransaccion	
	Delete From Venta Where IDTransaccion=@IDTransaccion	
	--Delete From Comision Where IDTransaccion=@IDTransaccion	
	
	Select ''Procesado''=''False'', ''Mensaje''=@Mensaje, ''Procesados''=@@ROWCOUNT
				
End

' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpVenta]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpVenta]
--Sergio 03-04-2014, campo verificar anulado
	--Entrada
	@IDTransaccion numeric(18,0) = NULL,
	@IDPuntoExpedicion int = NULL,
	@IDTipoComprobante smallint = NULL,
	@NroComprobante numeric(18, 0) = NULL,
	@Comprobante varchar(50) = NULL,
	@IDCliente int = NULL,
	@IDSucursalCliente tinyint = NULL,
	@Direccion varchar(100) = NULL,
	@Telefono varchar(50) = NULL,
	@IDVendedor tinyint = NULL,	
	@Fecha date = NULL,
	@IDSucursalOperacion tinyint = NULL,
	@IDDepositoOperacion tinyint = NULL,
	@Credito bit = NULL,
	@FechaVencimiento date = NULL,
	@IDListaPrecio tinyint = NULL,
	@Descuento numeric(2,0) = NULL,
	@IDMoneda tinyint = NULL,
	@Cotizacion money = NULL,
	@Observacion varchar(200) = NULL,
	@NroComprobanteRemision varchar(50) = NULL,
	@Total money = 0,
	@TotalImpuesto money = NULL,
	@TotalDiscriminado money = NULL,
	@TotalDescuento money = NULL,
	
	--Motivo Anulacion
	@IDMotivo tinyint =NULL,
	@Motivo varchar(50)=NULL,
	@Autoriza varchar(20)=NULL,
	@Nuevo bit=NULL,
	@FechaAnulado date = NULL,
	@IDUsuarioAnulado int=NULL,
	
	
	@EsVentaSucursal bit = ''False'',
	@EsMovil bit = ''False'',	
	@Operacion varchar(50),
	
	--Transaccion
	@IDUsuario smallint,
	@IDSucursal tinyint,
	@IDDeposito tinyint,
	@IDTerminal tinyint,
	
	--Salida
	@Mensaje varchar(200) output,
	@Procesado bit output,
	@IDTransaccionSalida numeric(18,0) output
		
As

Begin

	--BLOQUES
	Declare @vIDCliente int 
	Declare @vMaxIDMotivo int
	
	--INSERTAR
	If @Operacion = ''INS'' Begin
	
		--Validar
		--Numero
		If Exists(Select * From Venta Where Comprobante=@Comprobante) Begin
			set @Mensaje = ''El sistema detecto que el numero de operacion ya esta registrado! Obtenga un numero siguiente.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante
		if Exists(Select * From Venta Where IDPuntoExpedicion=@IDPuntoExpedicion And NroComprobante=@NroComprobante) Begin
			set @Mensaje = ''El numero de comprobante ya existe! No se puede cargar. Asegurese de introducir los datos correctamente.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Comprobante fuera del Rango
		if @NroComprobante < (Select NumeracionDesde From PuntoExpedicion Where ID=@IDPuntoExpedicion) Or @NroComprobante > (Select NumeracionHasta From PuntoExpedicion Where ID=@IDPuntoExpedicion) Begin
			set @Mensaje = ''El numero de comprobante no esta dentro del rango correcto! Cambie el punto de expedicion o actualicelo.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--Credito
		If @Credito=''True'' Begin
			
			--Fecha
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) < 0 Begin
				set @Mensaje = ''La fecha de de vencimiento no puede ser menor al del comprobante!''
				set @Procesado = ''False''
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
		
			--Verificar que el cliente tenga saldo suficiente.
			If @Total > (Select SaldoCredito From VCliente Where ID=@IDCliente) Begin
				set @Mensaje = ''El cliente no tiene credito suficiente! Para continuar, incremente el credito al cliente o disminuya el total de la factura.''
				set @Procesado = ''False''
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
			
			
			--Plazo credito
			Declare @vPlazoCredito smallint
			Set @vPlazoCredito = (Select ISNULL((Select PlazoCredito From Cliente Where ID=@IDCliente),0))
			
			If DATEDIFF(dd, @Fecha, @FechaVencimiento) > @vPlazoCredito Begin
				set @Mensaje = ''El plazo de credito es mayor al del que tiene asignado el cliente!''
				set @Procesado = ''False''
				set @IDTransaccionSalida  = 0
				return @@rowcount
			End
			
		End
				
		----Insertar la transaccion
		Declare @vIDOperacion tinyint
		Set @vIDOperacion = 1
		Select * From Operacion
		EXEC SpTransaccion
			@IDUsuario = @IDUsuario,
			@IDSucursal = @IDSucursal,
			@IDDeposito = @IDDeposito,
			@IDTerminal = @IDTerminal,
			@IDOperacion = @vIDOperacion,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT,
			@IDTransaccion = @IDTransaccionSalida OUTPUT
		
		------Insertar
		Declare @vSaldo money
		Declare @vCancelado bit
		Set @vSaldo = @Total
		Set @vCancelado = ''False''
		
		--Insertar el Registro de Venta		
		Insert Into Venta(IDTransaccion, IDPuntoExpedicion, IDTipoComprobante, NroComprobante, Comprobante, IDCliente, IDSucursalCliente, Direccion, Telefono, FechaEmision, IDSucursal, IDDeposito, Credito, FechaVencimiento, IDListaPrecio, Descuento, IDMoneda, Cotizacion, Observacion, NroComprobanteRemision, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Cobrado, Descontado, Saldo, Cancelado, Despachado, EsVentaSucursal, Anulado, FechaAnulado, IDUsuarioAnulado, IDVendedor)
		Values(@IDTransaccionSalida, @IDPuntoExpedicion, @IDTipoComprobante, @NroComprobante, @Comprobante, @IDCliente, @IDSucursalCliente, @Direccion, @Telefono, @Fecha, @IDSucursalOperacion, @IDDepositoOperacion, @Credito, @FechaVencimiento, @IDListaPrecio, @Descuento, @IDMoneda, @Cotizacion, @Observacion, @NroComprobanteRemision, @Total, @TotalImpuesto, @TotalDiscriminado, @TotalDescuento, 0, 0, @vSaldo, @vCancelado, ''False'', @EsVentaSucursal, ''False'', NULL, NULL, @IDVendedor)
				
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		EXEC SpAcualizarClienteVenta
			@IDCliente = @IDCliente,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
			
		--Actualizar Punto de Expedicion
		Update PuntoExpedicion Set UltimoNumero=@NroComprobante
		Where ID=@IDPuntoExpedicion
		
		
								
		set @Mensaje = ''Registro guardado''
		set @Procesado = ''True''
		return @@rowcount
			
	End
	
	If @Operacion = ''ANULAR'' Begin
	
		print ''entro a anulacion''
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Verificar que el documento ya este anulado
		If Exists(Select * From Venta Where IDTransaccion=@IDTransaccion And Anulado=''True'') Begin
			set @Mensaje = ''El Documento ya esta anulado''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Verificar que el documento no tenga otros registros asociados
		--Recibos
		If Exists(Select * From VVentaDetalleCobranza Where IDTransaccion=@IDTransaccion And AnuladoCobranza=''False'') Begin
			set @Mensaje = ''El documento tiene recibos asociados! Elimine los recibos para continuar.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Notas de Credito/Debito
		
		--Asientos Cerrados
		If Exists(Select * From Asiento A Where A.IDTransaccion=@IDTransaccion And A.Conciliado=''True'') Begin
			set @Mensaje = ''El asiento de este documento ya esta conciliado! No se puede anular.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Remisiones
		
		----Verificar dias de bloqueo
		If (Select Top(1) VentaBloquearAnulacion From Configuraciones) = ''True'' Begin
			
			Declare @vDias tinyint
			Set @vDias = (Select Top(1) VentaDiasBloqueo From Configuraciones)

			If (Select DATEDIFF(dd,(Select V.FechaEmision From Venta V Where IDTransaccion=@IDTransaccion), GETDATE())) > @vDias Begin
			 			
				set @Mensaje = ''El sistema no puede anular este registro ya que la configuracion lo restringe por la antigüedad del documento. Cambie la configuracion de bloqueo para anular!''
				set @Procesado = ''False''
				return @@rowcount
				
			End
			
		End
		
	
		--Agregar nuevo motivo en el caso en que no exista
		If @Nuevo = ''True''Begin
		
			Set @vMaxIDMotivo =(Select MAX(ID+1)From MotivoAnulacionVenta)
			Set @IDMotivo =@vMaxIDMotivo
			Insert into MotivoAnulacionVenta(ID,Descripcion)
										values(@IDMotivo,@Motivo )
		End
		
					
		--Actualizamos el Stock
		Exec SpDetalleVenta @IDTransaccion=@IDTransaccion, @Operacion=''ANULAR'', @Mensaje=@Mensaje output, @Procesado=@Procesado output
		
		--Actualizar los descuentos tacticos
		If @EsMovil = ''False'' Begin
			EXEC SpActualizarActividad @Operacion = ''ANULAR'', @IDTransaccion = @IDTransaccion, @Mensaje = @Mensaje OUTPUT, @Procesado = @Procesado OUTPUT
		End
					
		---Insertamos el registro de anulacion
		Exec SpDocumentoAnulado @IDTransaccion=@IDTransaccion, @IDUsuario=@IDUsuario, @IDSucursal=@IDSucursal,@IDTerminal=@IDTerminal
				
		--Anulamos el registro
		Update Venta Set Anulado = ''True'',
						IDMotivo=@IDMotivo,
						Autoriza=@Autoriza,
						FecAnulado=@FechaAnulado 
		Where IDTransaccion = @IDTransaccion
		
		
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@IDTransaccion)
			
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
				
		--Actualiza Pedido
		Declare @vIDTransaccionPedido numeric(18,0)
		set @vIDTransaccionPedido = IsNull((Select IDTransaccionPedido From PedidoVenta Where IDTransaccionVenta = @IDTransaccion),0)
		
		If @vIDTransaccionPedido > 0 Begin
			Update Pedido Set Facturado=''False'' Where IDTransaccion = @vIDTransaccionPedido
		End
		
		--Liberar Pedido
		Delete From PedidoVenta 
		Where IDTransaccionVenta = @IDTransaccion
		
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		If @EsMovil = ''False'' Begin
			EXEC SpAcualizarClienteVenta
				@IDCliente = @vIDCliente,
				@Mensaje = @Mensaje OUTPUT,
				@Procesado = @Procesado OUTPUT
				
			--Cargar Comisiones
			EXEC SpComision
				@IDTransaccion=@IDTransaccion,
				@Venta=''True'',
				@Devolucion=''False'',
				@Operacion= ''Anular'' 	
		End	
			
		Set @Mensaje = ''Reistro guardado!''
		Set @Procesado = ''True''
		Set @IDTransaccionSalida = 0
		print @Mensaje
		return @@rowcount
			
	End
	
	If @Operacion = ''DEL'' Begin
	
		--IDTransaccion
		If Not Exists(Select * From Transaccion Where ID=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			set @IDTransaccionSalida  = 0
			return @@rowcount
		End
		
		--IDTransaccion
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion) Begin
			set @Mensaje = ''El sistema no encontro el registro solicitado.''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Solo se puede eliminar si esta aulado
		If Not Exists(Select * From Venta Where IDTransaccion=@IDTransaccion And Anulado=''True'') Begin
			set @Mensaje = ''El comprobante tiene que estar anulado para poder eliminar!''
			set @Procesado = ''False''
			return @@rowcount
		End
		
		--Actualizar el Cliente (Saldo Total, Saldo Credito, Ultima Compra)
		Set @vIDCliente = (Select IDCliente From Venta Where IDTransaccion=@IDTransaccion)
		
		EXEC SpAcualizarClienteVenta
			@IDCliente = @vIDCliente,
			@Mensaje = @Mensaje OUTPUT,
			@Procesado = @Procesado OUTPUT
			
		--Eliminamos el Asiento
		Delete From DetalleAsiento Where IDTransaccion=@IDTransaccion
		Delete From Asiento Where IDTransaccion=@IDTransaccion
		
		--Los detalle
		Delete From DetalleImpuesto Where IDTransaccion=@IDTransaccion
		Delete From Descuento Where IDTransaccion=@IDTransaccion
		Delete From DetalleVenta Where IDTransaccion=@IDTransaccion
		--Eliminar venta Cobanza
		Delete From VentaCobranza Where IDTransaccionVenta = @IDTransaccion 			
		--Eliminar la venta
		Delete From VentaLoteDistribucion Where IDTransaccionVenta=@IDTransaccion 
		Delete From Venta Where IDTransaccion=@IDTransaccion
		
		Set @Mensaje = ''Reistro guardado!''
		Set @Procesado = ''True''
		Set @IDTransaccionSalida = 0
		print @Mensaje
		return @@rowcount
				
		Set @Mensaje = ''Reistro guardado!''
		Set @Procesado = ''True''
		print @Mensaje
		return @@rowcount
			
	End
	
	set @Mensaje = ''No se proceso ninguna transaccion!''
	set @Procesado = ''False''
	set @IDTransaccionSalida  = ''0''
	return @@rowcount
		
End




' 
END





IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SpSincronizarVentaAnulacion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SpSincronizarVentaAnulacion]

	--Entrada
	@IDTransaccion	numeric(18, 0),
	
	--Transaccion
	@IDOperacion int,
	@IDUsuario int,
	@IDSucursalTransaccion int,
	@IDDepositoTransaccion int,
	@IDTerminal int
	
As

Begin

	Declare @vIDTransaccion numeric(18,0)
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	
	Set @vMensaje = ''No se proceso''
	Set @vProcesado = ''False''
	
	--Procesar
	Exec SpVenta @IDTransaccion=@IDTransaccion, 
		@Operacion=''ANULAR'', 
		@EsMovil=''True'',
		@IDUsuario=@IDUsuario, 
		@IDSucursal=@IDSucursalTransaccion,
		@IDDeposito=@IDDepositoTransaccion,
		@IDTerminal=@IDTerminal,
		@Mensaje=@vMensaje output,
		@Procesado=@vProcesado output,
		@IDTransaccionSalida=@vIDTransaccion output
		
	Set @vMensaje = ''Registro Guardado!''
	Set @vProcesado = ''True''
	GoTo Salir
			
Salir:
	Select ''Mensaje''=@vMensaje, ''Procesado''=@vProcesado
End' 
END

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CentroCosto_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[CentroCosto]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CentroCosto_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CentroCosto] ADD  CONSTRAINT [DF_CentroCosto_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Categoria_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Categoria]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Categoria_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Categoria] ADD  CONSTRAINT [DF_Categoria_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Barrio_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Barrio]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Barrio_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Barrio] ADD  CONSTRAINT [DF_Barrio_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Banco_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Banco]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Banco_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Banco] ADD  CONSTRAINT [DF_Banco_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Actividad_Mecanica]') AND parent_object_id = OBJECT_ID(N'[dbo].[Actividad]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Actividad_Mecanica]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Actividad] ADD  CONSTRAINT [DF_Actividad_Mecanica]  DEFAULT ('') FOR [Mecanica]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Cotizacion_Cotizacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cotizacion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Cotizacion_Cotizacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Cotizacion] ADD  CONSTRAINT [DF_Cotizacion_Cotizacion]  DEFAULT ((0)) FOR [Cotizacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_PropietarioBD]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_PropietarioBD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_PropietarioBD]  DEFAULT ('dbo') FOR [PropietarioBD]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_MovimientoBloquearFecha]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_MovimientoBloquearFecha]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_MovimientoBloquearFecha]  DEFAULT ('False') FOR [MovimientoBloquearFecha]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_MovimientoExigirAutorizacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_MovimientoExigirAutorizacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_MovimientoExigirAutorizacion]  DEFAULT ('False') FOR [MovimientoExigirAutorizacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_MovimientoImprimirDocumento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_MovimientoImprimirDocumento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_MovimientoImprimirDocumento]  DEFAULT ('False') FOR [MovimientoImprimirDocumento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_MovimientoBloquearCampoCosto]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_MovimientoBloquearCampoCosto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_MovimientoBloquearCampoCosto]  DEFAULT ('False') FOR [MovimientoBloquearCampoCosto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_MovimientoHabilitarSiCostoCero]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_MovimientoHabilitarSiCostoCero]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_MovimientoHabilitarSiCostoCero]  DEFAULT ('False') FOR [MovimientoHabilitarSiCostoCero]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_MovimientoCostoProducto]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_MovimientoCostoProducto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_MovimientoCostoProducto]  DEFAULT ('') FOR [MovimientoCostoProducto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_MovimientoBloquearAnulacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_MovimientoBloquearAnulacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_MovimientoBloquearAnulacion]  DEFAULT ('False') FOR [MovimientoBloquearAnulacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_MovimientoDiasBloqueo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_MovimientoDiasBloqueo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_MovimientoDiasBloqueo]  DEFAULT ((0)) FOR [MovimientoDiasBloqueo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_VentaBloquearFecha]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_VentaBloquearFecha]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_VentaBloquearFecha]  DEFAULT ('False') FOR [VentaBloquearFecha]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_VentaImprimirDocumento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_VentaImprimirDocumento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_VentaImprimirDocumento]  DEFAULT ('False') FOR [VentaImprimirDocumento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_VentaBloquearAnulacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_VentaBloquearAnulacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_VentaBloquearAnulacion]  DEFAULT ('False') FOR [VentaBloquearAnulacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_VentaDiasBloqueo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_VentaDiasBloqueo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_VentaDiasBloqueo]  DEFAULT ((0)) FOR [VentaDiasBloqueo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_VentaFechaFacturacionActivar]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_VentaFechaFacturacionActivar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_VentaFechaFacturacionActivar]  DEFAULT ('False') FOR [VentaFechaFacturacionActivar]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_ERPTablasPrincipales]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_ERPTablasPrincipales]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_ERPTablasPrincipales]  DEFAULT ('') FOR [ERPTablasPrincipales]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_ERPTablasMayores]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_ERPTablasMayores]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_ERPTablasMayores]  DEFAULT ('') FOR [ERPTablasMayores]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CobranzaCreditoBloquearFecha]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CobranzaCreditoBloquearFecha]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CobranzaCreditoBloquearFecha]  DEFAULT ('False') FOR [CobranzaCreditoBloquearFecha]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CobranzaCreditoImprimirDocumento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CobranzaCreditoImprimirDocumento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CobranzaCreditoImprimirDocumento]  DEFAULT ('False') FOR [CobranzaCreditoImprimirDocumento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CobranzaCreditoBloquearAnulacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CobranzaCreditoBloquearAnulacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CobranzaCreditoBloquearAnulacion]  DEFAULT ('False') FOR [CobranzaCreditoBloquearAnulacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CobranzaCreditoDiasBloqueo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CobranzaCreditoDiasBloqueo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CobranzaCreditoDiasBloqueo]  DEFAULT ((0)) FOR [CobranzaCreditoDiasBloqueo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CobranzaContadoBloquearFecha]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CobranzaContadoBloquearFecha]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CobranzaContadoBloquearFecha]  DEFAULT ('False') FOR [CobranzaContadoBloquearFecha]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CobranzaContadoImprimirDocumento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CobranzaContadoImprimirDocumento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CobranzaContadoImprimirDocumento]  DEFAULT ('False') FOR [CobranzaContadoImprimirDocumento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CobranzaContadoBloquearAnulacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CobranzaContadoBloquearAnulacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CobranzaContadoBloquearAnulacion]  DEFAULT ('False') FOR [CobranzaContadoBloquearAnulacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CobranzaContadoDiasBloqueo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CobranzaContadoDiasBloqueo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CobranzaContadoDiasBloqueo]  DEFAULT ((0)) FOR [CobranzaContadoDiasBloqueo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_OrdenPagoBloquearAnulacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_OrdenPagoBloquearAnulacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_OrdenPagoBloquearAnulacion]  DEFAULT ('False') FOR [OrdenPagoBloquearAnulacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_OrdenPagoDiasBloqueo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_OrdenPagoDiasBloqueo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_OrdenPagoDiasBloqueo]  DEFAULT ((0)) FOR [OrdenPagoDiasBloqueo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_OrdenPagoALaOrdenPredefinido]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_OrdenPagoALaOrdenPredefinido]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_OrdenPagoALaOrdenPredefinido]  DEFAULT ('') FOR [OrdenPagoALaOrdenPredefinido]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_PedidoCantidadProducto]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_PedidoCantidadProducto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_PedidoCantidadProducto]  DEFAULT ((17)) FOR [PedidoCantidadProducto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_PedidoBloquearTactico]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_PedidoBloquearTactico]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_PedidoBloquearTactico]  DEFAULT ('True') FOR [PedidoBloquearTactico]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CompraPorcentajeRetencion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CompraPorcentajeRetencion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CompraPorcentajeRetencion]  DEFAULT ((0)) FOR [CompraPorcentajeRetencion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Configuraciones_CompraImporteMinimoRetencion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Configuraciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Configuraciones_CompraImporteMinimoRetencion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Configuraciones] ADD  CONSTRAINT [DF_Configuraciones_CompraImporteMinimoRetencion]  DEFAULT ((0)) FOR [CompraImporteMinimoRetencion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Chofer_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Chofer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Chofer_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Chofer] ADD  CONSTRAINT [DF_Chofer_Estado]  DEFAULT ('False') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Linea_Estado00]') AND parent_object_id = OBJECT_ID(N'[dbo].[Linea]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Linea_Estado00]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Linea] ADD  CONSTRAINT [DF_Linea_Estado00]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Presentacion_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Presentacion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Presentacion_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Presentacion] ADD  CONSTRAINT [DF_Presentacion_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pais_Orden]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pais]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pais_Orden]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pais] ADD  CONSTRAINT [DF_Pais_Orden]  DEFAULT ((0)) FOR [Orden]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pais_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pais]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pais_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pais] ADD  CONSTRAINT [DF_Pais_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ProductoCombinado_CantidadPrincipal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoCombinado]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ProductoCombinado_CantidadPrincipal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductoCombinado] ADD  CONSTRAINT [DF_ProductoCombinado_CantidadPrincipal]  DEFAULT ((1)) FOR [CantidadPrincipal]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ProductoCombinado_CantidadSecundario]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoCombinado]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ProductoCombinado_CantidadSecundario]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductoCombinado] ADD  CONSTRAINT [DF_ProductoCombinado_CantidadSecundario]  DEFAULT ((1)) FOR [CantidadSecundario]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Marca_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Marca]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Marca_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Marca] ADD  CONSTRAINT [DF_Marca_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ZonaVenta_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZonaVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ZonaVenta_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZonaVenta] ADD  CONSTRAINT [DF_ZonaVenta_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ZonaVenta_Orden]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZonaVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ZonaVenta_Orden]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZonaVenta] ADD  CONSTRAINT [DF_ZonaVenta_Orden]  DEFAULT ((0)) FOR [Orden]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TipoProveedor_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoProveedor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TipoProveedor_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TipoProveedor] ADD  CONSTRAINT [DF_TipoProveedor_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_UnidadMedida_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[UnidadMedida]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_UnidadMedida_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[UnidadMedida] ADD  CONSTRAINT [DF_UnidadMedida_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_SubLinea2_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLinea2]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SubLinea2_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SubLinea2] ADD  CONSTRAINT [DF_SubLinea2_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_SubLinea_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLinea]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SubLinea_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SubLinea] ADD  CONSTRAINT [DF_SubLinea_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TipoProducto_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoProducto]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TipoProducto_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TipoProducto] ADD  CONSTRAINT [DF_TipoProducto_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TipoDescuento_RangoFecha]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoDescuento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TipoDescuento_RangoFecha]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TipoDescuento] ADD  CONSTRAINT [DF_TipoDescuento_RangoFecha]  DEFAULT ('True') FOR [RangoFecha]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TipoDescuento_Limite]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoDescuento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TipoDescuento_Limite]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TipoDescuento] ADD  CONSTRAINT [DF_TipoDescuento_Limite]  DEFAULT ((100)) FOR [Limite]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TipoComprobante_Automumerico]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoComprobante]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TipoComprobante_Automumerico]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TipoComprobante] ADD  CONSTRAINT [DF_TipoComprobante_Automumerico]  DEFAULT ('true') FOR [Autonumerico]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TipoComprobante_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoComprobante]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TipoComprobante_Deposito]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TipoComprobante] ADD  CONSTRAINT [DF_TipoComprobante_Deposito]  DEFAULT ('False') FOR [Deposito]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Usuario_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Usuario]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Usuario_Vendedor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [DF_Usuario_Vendedor]  DEFAULT ('False') FOR [EsVendedor]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CuentasContables_Imputable]') AND parent_object_id = OBJECT_ID(N'[dbo].[CuentaContable]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CuentasContables_Imputable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CuentaContable] ADD  CONSTRAINT [DF_CuentasContables_Imputable]  DEFAULT ('False') FOR [Imputable]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CuentaContable_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[CuentaContable]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CuentaContable_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CuentaContable] ADD  CONSTRAINT [DF_CuentaContable_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AccesoPerfil_Visualizar]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccesoPerfil_Visualizar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccesoPerfil] ADD  CONSTRAINT [DF_AccesoPerfil_Visualizar]  DEFAULT ('False') FOR [Visualizar]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AccesoPerfil_Agregar]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccesoPerfil_Agregar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccesoPerfil] ADD  CONSTRAINT [DF_AccesoPerfil_Agregar]  DEFAULT ('False') FOR [Agregar]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AccesoPerfil_Modificar]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccesoPerfil_Modificar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccesoPerfil] ADD  CONSTRAINT [DF_AccesoPerfil_Modificar]  DEFAULT ('False') FOR [Modificar]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AccesoPerfil_Eliminar]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccesoPerfil_Eliminar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccesoPerfil] ADD  CONSTRAINT [DF_AccesoPerfil_Eliminar]  DEFAULT ('False') FOR [Eliminar]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AccesoPerfil_Anular]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccesoPerfil_Anular]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccesoPerfil] ADD  CONSTRAINT [DF_AccesoPerfil_Anular]  DEFAULT ('False') FOR [Anular]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AccesoPerfil_Imprimir]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccesoPerfil_Imprimir]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccesoPerfil] ADD  CONSTRAINT [DF_AccesoPerfil_Imprimir]  DEFAULT ('False') FOR [Imprimir]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AccesoEspecificoPerfil_Habilitar]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoEspecificoPerfil]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AccesoEspecificoPerfil_Habilitar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AccesoEspecificoPerfil] ADD  CONSTRAINT [DF_AccesoEspecificoPerfil_Habilitar]  DEFAULT ('False') FOR [Habilitar]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Proveedor_Deuda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Proveedor_Deuda]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Proveedor] ADD  CONSTRAINT [DF_Proveedor_Deuda]  DEFAULT ((0)) FOR [Deuda]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PuntoExpedicion_Referencia]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PuntoExpedicion_Referencia]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PuntoExpedicion] ADD  CONSTRAINT [DF_PuntoExpedicion_Referencia]  DEFAULT ((1)) FOR [Referencia]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PuntoExpedicion_NumeracionDesde]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PuntoExpedicion_NumeracionDesde]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PuntoExpedicion] ADD  CONSTRAINT [DF_PuntoExpedicion_NumeracionDesde]  DEFAULT ((0)) FOR [NumeracionDesde]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PuntoExpedicion_NumeracionHasta]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PuntoExpedicion_NumeracionHasta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PuntoExpedicion] ADD  CONSTRAINT [DF_PuntoExpedicion_NumeracionHasta]  DEFAULT ((0)) FOR [NumeracionHasta]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PuntoExpedicion_CantidadPorTalonario]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PuntoExpedicion_CantidadPorTalonario]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PuntoExpedicion] ADD  CONSTRAINT [DF_PuntoExpedicion_CantidadPorTalonario]  DEFAULT ((0)) FOR [CantidadPorTalonario]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PuntoExpedicion_UltimoNumero]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PuntoExpedicion_UltimoNumero]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PuntoExpedicion] ADD  CONSTRAINT [DF_PuntoExpedicion_UltimoNumero]  DEFAULT ((0)) FOR [UltimoNumero]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Division_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Division]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Division_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Division] ADD  CONSTRAINT [DF_Division_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Deposito_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Deposito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Deposito_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Deposito] ADD  CONSTRAINT [DF_Deposito_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Cliente_CI]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Cliente_CI]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_CI]  DEFAULT ('False') FOR [CI]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Cliente_Deuda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Cliente_Deuda]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Cliente] ADD  CONSTRAINT [DF_Cliente_Deuda]  DEFAULT ((0)) FOR [Deuda]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Producto_IDIVA]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Producto_IDIVA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Producto] ADD  CONSTRAINT [DF_Producto_IDIVA]  DEFAULT ((1)) FOR [IDImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Producto_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Producto_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Producto] ADD  CONSTRAINT [DF_Producto_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Producto_ControlarExistencia]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Producto_ControlarExistencia]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Producto] ADD  CONSTRAINT [DF_Producto_ControlarExistencia]  DEFAULT ('True') FOR [ControlarExistencia]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_NotaCredito_ConComprobantes]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_NotaCredito_ConComprobantes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NotaCredito] ADD  CONSTRAINT [DF_NotaCredito_ConComprobantes]  DEFAULT ('True') FOR [ConComprobantes]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_Cotizacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_Cotizacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_Cotizacion]  DEFAULT ((1)) FOR [Cotizacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_TotalImpuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_TotalImpuesto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_TotalImpuesto]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_TotalDiscriminado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_TotalDiscriminado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_TotalDiscriminado]  DEFAULT ((0)) FOR [TotalDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_TotalDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_TotalDescuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_TotalDescuento]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_Facturado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_Facturado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_Facturado]  DEFAULT ('False') FOR [Facturado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_EsVentaSucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_EsVentaSucursal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_EsVentaSucursal]  DEFAULT ('False') FOR [EsVentaSucursal]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_Anulado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_Anulado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_Anulado]  DEFAULT ('False') FOR [Anulado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Pedido_Procesado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Pedido_Procesado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Pedido] ADD  CONSTRAINT [DF_Pedido_Procesado]  DEFAULT ('False') FOR [Procesado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_LoteDistribucion_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LoteDistribucion_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LoteDistribucion] ADD  CONSTRAINT [DF_LoteDistribucion_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_LoteDistribucion_TotalImpuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LoteDistribucion_TotalImpuesto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LoteDistribucion] ADD  CONSTRAINT [DF_LoteDistribucion_TotalImpuesto]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_LoteDistribucion_TotalDiscriminado]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LoteDistribucion_TotalDiscriminado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LoteDistribucion] ADD  CONSTRAINT [DF_LoteDistribucion_TotalDiscriminado]  DEFAULT ((0)) FOR [TotalDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_LoteDistribucion_TotalDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LoteDistribucion_TotalDescuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LoteDistribucion] ADD  CONSTRAINT [DF_LoteDistribucion_TotalDescuento]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_LoteDistribucion_Anulado]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_LoteDistribucion_Anulado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LoteDistribucion] ADD  CONSTRAINT [DF_LoteDistribucion_Anulado]  DEFAULT ('False') FOR [Anulado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ProductoListaPrecioExcepciones_Descuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ProductoListaPrecioExcepciones_Descuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones] ADD  CONSTRAINT [DF_ProductoListaPrecioExcepciones_Descuento]  DEFAULT ((0)) FOR [Descuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ProductoListaPrecioExcepciones_Porcentaje]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ProductoListaPrecioExcepciones_Porcentaje]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones] ADD  CONSTRAINT [DF_ProductoListaPrecioExcepciones_Porcentaje]  DEFAULT ((0)) FOR [Porcentaje]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_NotaDebito_ConComprobantes]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaDebito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_NotaDebito_ConComprobantes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NotaDebito] ADD  CONSTRAINT [DF_NotaDebito_ConComprobantes]  DEFAULT ('True') FOR [ConComprobantes]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Asiento_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Asiento_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Asiento] ADD  CONSTRAINT [DF_Asiento_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Asiento_Debito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Asiento_Debito]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Asiento] ADD  CONSTRAINT [DF_Asiento_Debito]  DEFAULT ((0)) FOR [Debito]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Asiento_Credito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Asiento_Credito]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Asiento] ADD  CONSTRAINT [DF_Asiento_Credito]  DEFAULT ((0)) FOR [Credito]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Asiento_Saldo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Asiento_Saldo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Asiento] ADD  CONSTRAINT [DF_Asiento_Saldo]  DEFAULT ((0)) FOR [Saldo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Asiento_Bloquear]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Asiento_Bloquear]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Asiento] ADD  CONSTRAINT [DF_Asiento_Bloquear]  DEFAULT ('False') FOR [Bloquear]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ClienteSucursal_Estado]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ClienteSucursal_Estado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ClienteSucursal] ADD  CONSTRAINT [DF_ClienteSucursal_Estado]  DEFAULT ('True') FOR [Estado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ReciboCliente_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ReciboCliente_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaCredito] ADD  CONSTRAINT [DF_ReciboCliente_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ReciboCliente_TotalImpuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ReciboCliente_TotalImpuesto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaCredito] ADD  CONSTRAINT [DF_ReciboCliente_TotalImpuesto]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ReciboCliente_TotalDiscriminado]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ReciboCliente_TotalDiscriminado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaCredito] ADD  CONSTRAINT [DF_ReciboCliente_TotalDiscriminado]  DEFAULT ((0)) FOR [TotalDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ReciboCliente_TotalDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ReciboCliente_TotalDescuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaCredito] ADD  CONSTRAINT [DF_ReciboCliente_TotalDescuento]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ReciboCliente_Anulado]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ReciboCliente_Anulado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaCredito] ADD  CONSTRAINT [DF_ReciboCliente_Anulado]  DEFAULT ('False') FOR [Anulado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CobranzaCredito_Procesado]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CobranzaCredito_Procesado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaCredito] ADD  CONSTRAINT [DF_CobranzaCredito_Procesado]  DEFAULT ('False') FOR [Procesado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Cotizacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Cotizacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Cotizacion]  DEFAULT ((1)) FOR [Cotizacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_TotalImpuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_TotalImpuesto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_TotalImpuesto]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_TotalDiscriminado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_TotalDiscriminado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_TotalDiscriminado]  DEFAULT ((0)) FOR [TotalDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_TotalDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_TotalDescuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_TotalDescuento]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Cobrado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Cobrado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Cobrado]  DEFAULT ((0)) FOR [Cobrado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Descontado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Descontado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Descontado]  DEFAULT ((0)) FOR [Descontado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Acreditado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Acreditado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Acreditado]  DEFAULT ((0)) FOR [Acreditado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Saldo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Saldo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Saldo]  DEFAULT ((0)) FOR [Saldo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Despachado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Despachado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Despachado]  DEFAULT ('False') FOR [Despachado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_EsVentaSucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_EsVentaSucursal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_EsVentaSucursal]  DEFAULT ('False') FOR [EsVentaSucursal]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Anulado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Anulado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Anulado]  DEFAULT ('False') FOR [Anulado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Venta_Procesado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Venta_Procesado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Venta] ADD  CONSTRAINT [DF_Venta_Procesado]  DEFAULT ('False') FOR [Procesado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CobranzaContado_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CobranzaContado_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaContado] ADD  CONSTRAINT [DF_CobranzaContado_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CobranzaContado_TotalImpuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CobranzaContado_TotalImpuesto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaContado] ADD  CONSTRAINT [DF_CobranzaContado_TotalImpuesto]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CobranzaContado_TotalDiscriminado]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CobranzaContado_TotalDiscriminado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaContado] ADD  CONSTRAINT [DF_CobranzaContado_TotalDiscriminado]  DEFAULT ((0)) FOR [TotalDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CobranzaContado_TotalDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CobranzaContado_TotalDescuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaContado] ADD  CONSTRAINT [DF_CobranzaContado_TotalDescuento]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CobranzaContado_Anulado]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CobranzaContado_Anulado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaContado] ADD  CONSTRAINT [DF_CobranzaContado_Anulado]  DEFAULT ('False') FOR [Anulado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CobranzaContado_Procesado]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CobranzaContado_Procesado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CobranzaContado] ADD  CONSTRAINT [DF_CobranzaContado_Procesado]  DEFAULT ('False') FOR [Procesado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Cotizacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Cotizacion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Cotizacion]  DEFAULT ((1)) FOR [Cotizacion]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Importe]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Importe]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Importe]  DEFAULT ((0)) FOR [Importe]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Diferido]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Diferido]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Diferido]  DEFAULT ('False') FOR [Diferido]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Saldo]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Saldo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Saldo]  DEFAULT ((0)) FOR [Saldo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Cancelado]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Cancelado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Cancelado]  DEFAULT ('False') FOR [Cancelado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Cartera]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Cartera]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Cartera]  DEFAULT ('False') FOR [Cartera]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Depositado]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Depositado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Depositado]  DEFAULT ('False') FOR [Depositado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Rechazado]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Rechazado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Rechazado]  DEFAULT ('False') FOR [Rechazado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ChequeCliente_Conciliado]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChequeCliente_Conciliado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChequeCliente] ADD  CONSTRAINT [DF_ChequeCliente_Conciliado]  DEFAULT ('False') FOR [Conciliado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleVenta_PrecioUnitario]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleVenta_PrecioUnitario]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleVenta] ADD  CONSTRAINT [DF_DetalleVenta_PrecioUnitario]  DEFAULT ((0)) FOR [PrecioUnitario]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleVenta_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleVenta_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleVenta] ADD  CONSTRAINT [DF_DetalleVenta_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleVenta_TotalImpuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleVenta_TotalImpuesto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleVenta] ADD  CONSTRAINT [DF_DetalleVenta_TotalImpuesto]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleVenta_DescuentoUnitario]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleVenta_DescuentoUnitario]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleVenta] ADD  CONSTRAINT [DF_DetalleVenta_DescuentoUnitario]  DEFAULT ((0)) FOR [DescuentoUnitario]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleVenta_DescuentoUnitario1]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleVenta_DescuentoUnitario1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleVenta] ADD  CONSTRAINT [DF_DetalleVenta_DescuentoUnitario1]  DEFAULT ((0)) FOR [DescuentoUnitarioDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleVenta_TotalDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleVenta_TotalDescuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleVenta] ADD  CONSTRAINT [DF_DetalleVenta_TotalDescuento]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleVenta_TotalDescuento1]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleVenta_TotalDescuento1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleVenta] ADD  CONSTRAINT [DF_DetalleVenta_TotalDescuento1]  DEFAULT ((0)) FOR [TotalDescuentoDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetallePedido_PrecioUnitario_2]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetallePedido_PrecioUnitario_2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetallePedido] ADD  CONSTRAINT [DF_DetallePedido_PrecioUnitario_2]  DEFAULT ((0)) FOR [PrecioUnitario]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetallePedido_Total_2]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetallePedido_Total_2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetallePedido] ADD  CONSTRAINT [DF_DetallePedido_Total_2]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetallePedido_TotalImpuesto_2]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetallePedido_TotalImpuesto_2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetallePedido] ADD  CONSTRAINT [DF_DetallePedido_TotalImpuesto_2]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetallePedido_DescuentoUnitario_2]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetallePedido_DescuentoUnitario_2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetallePedido] ADD  CONSTRAINT [DF_DetallePedido_DescuentoUnitario_2]  DEFAULT ((0)) FOR [DescuentoUnitario]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetallePedido_DescuentoUnitarioDiscriminado_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetallePedido_DescuentoUnitarioDiscriminado_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetallePedido] ADD  CONSTRAINT [DF_DetallePedido_DescuentoUnitarioDiscriminado_1]  DEFAULT ((0)) FOR [DescuentoUnitarioDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetallePedido_TotalDescuento_2]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetallePedido_TotalDescuento_2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetallePedido] ADD  CONSTRAINT [DF_DetallePedido_TotalDescuento_2]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetallePedido_TotalDescuentoDiscriminado_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetallePedido_TotalDescuentoDiscriminado_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetallePedido] ADD  CONSTRAINT [DF_DetallePedido_TotalDescuentoDiscriminado_1]  DEFAULT ((0)) FOR [TotalDescuentoDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleNotaDebito_DescuentoUnitario]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleNotaDebito_DescuentoUnitario]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleNotaDebito] ADD  CONSTRAINT [DF_DetalleNotaDebito_DescuentoUnitario]  DEFAULT ((0)) FOR [DescuentoUnitario]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleNotaDebito_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleNotaDebito_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleNotaDebito] ADD  CONSTRAINT [DF_DetalleNotaDebito_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleNotaDebito_TotalImpuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleNotaDebito_TotalImpuesto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleNotaDebito] ADD  CONSTRAINT [DF_DetalleNotaDebito_TotalImpuesto]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleNotaDebito_TotalDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleNotaDebito_TotalDescuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleNotaDebito] ADD  CONSTRAINT [DF_DetalleNotaDebito_TotalDescuento]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleNotaCredito_DescuentoUnitario]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleNotaCredito_DescuentoUnitario]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleNotaCredito] ADD  CONSTRAINT [DF_DetalleNotaCredito_DescuentoUnitario]  DEFAULT ((0)) FOR [DescuentoUnitario]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleNotaCredito_Total]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleNotaCredito_Total]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleNotaCredito] ADD  CONSTRAINT [DF_DetalleNotaCredito_Total]  DEFAULT ((0)) FOR [Total]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleNotaCredito_TotalImpuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleNotaCredito_TotalImpuesto]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleNotaCredito] ADD  CONSTRAINT [DF_DetalleNotaCredito_TotalImpuesto]  DEFAULT ((0)) FOR [TotalImpuesto]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleNotaCredito_TotalDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleNotaCredito_TotalDescuento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleNotaCredito] ADD  CONSTRAINT [DF_DetalleNotaCredito_TotalDescuento]  DEFAULT ((0)) FOR [TotalDescuento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleAsiento_Credito]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleAsiento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleAsiento_Credito]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleAsiento] ADD  CONSTRAINT [DF_DetalleAsiento_Credito]  DEFAULT ((0)) FOR [Credito]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleAsiento_Debito]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleAsiento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleAsiento_Debito]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleAsiento] ADD  CONSTRAINT [DF_DetalleAsiento_Debito]  DEFAULT ((0)) FOR [Debito]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DetalleAsiento_Importe]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleAsiento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DetalleAsiento_Importe]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DetalleAsiento] ADD  CONSTRAINT [DF_DetalleAsiento_Importe]  DEFAULT ((0)) FOR [Importe]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DescuentoPedido_DescuentoDiscriminado_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[DescuentoPedido]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DescuentoPedido_DescuentoDiscriminado_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DescuentoPedido] ADD  CONSTRAINT [DF_DescuentoPedido_DescuentoDiscriminado_1]  DEFAULT ((0)) FOR [DescuentoDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Descuento_DescuentoDiscriminado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Descuento]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Descuento_DescuentoDiscriminado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Descuento] ADD  CONSTRAINT [DF_Descuento_DescuentoDiscriminado]  DEFAULT ((0)) FOR [DescuentoDiscriminado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FormaPago_CancelarCheque]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPago]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FormaPago_CancelarCheque]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FormaPago] ADD  CONSTRAINT [DF_FormaPago_CancelarCheque]  DEFAULT ('False') FOR [CancelarCheque]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FormaPago_Documento]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPago]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FormaPago_Documento]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FormaPago] ADD  CONSTRAINT [DF_FormaPago_Documento]  DEFAULT ('False') FOR [Documento]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FormaPago_Tarjeta]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPago]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FormaPago_Tarjeta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FormaPago] ADD  CONSTRAINT [DF_FormaPago_Tarjeta]  DEFAULT ((0)) FOR [Tarjeta]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_VentaCobranza_Cancelar]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaCobranza]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_VentaCobranza_Cancelar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[VentaCobranza] ADD  CONSTRAINT [DF_VentaCobranza_Cancelar]  DEFAULT ('False') FOR [Cancelar]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Efectivo_VentasCredito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Efectivo_VentasCredito]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Efectivo] ADD  CONSTRAINT [DF_Efectivo_VentasCredito]  DEFAULT ((0)) FOR [VentasCredito]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Efectivo_VentasContado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Efectivo_VentasContado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Efectivo] ADD  CONSTRAINT [DF_Efectivo_VentasContado]  DEFAULT ((0)) FOR [VentasContado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Efectivo_Gastos]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Efectivo_Gastos]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Efectivo] ADD  CONSTRAINT [DF_Efectivo_Gastos]  DEFAULT ((0)) FOR [Gastos]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Efectivo_Depositado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Efectivo_Depositado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Efectivo] ADD  CONSTRAINT [DF_Efectivo_Depositado]  DEFAULT ((0)) FOR [Depositado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Efectivo_Cancelado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Efectivo_Cancelado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Efectivo] ADD  CONSTRAINT [DF_Efectivo_Cancelado]  DEFAULT ('False') FOR [Cancelado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Efectivo_Saldo]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Efectivo_Saldo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Efectivo] ADD  CONSTRAINT [DF_Efectivo_Saldo]  DEFAULT ((0)) FOR [Saldo]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Efectivo_Habilitado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Efectivo_Habilitado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Efectivo] ADD  CONSTRAINT [DF_Efectivo_Habilitado]  DEFAULT ('False') FOR [Habilitado]
END


End

IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Efectivo_Anulado]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Efectivo_Anulado]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Efectivo] ADD  CONSTRAINT [DF_Efectivo_Anulado]  DEFAULT ('False') FOR [Anulado]
END


End

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoCuentaFija_Impuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoCuentaFija]'))
ALTER TABLE [dbo].[TipoCuentaFija]  WITH NOCHECK ADD  CONSTRAINT [FK_TipoCuentaFija_Impuesto] FOREIGN KEY([IDImpuesto])
REFERENCES [dbo].[Impuesto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoCuentaFija_Impuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoCuentaFija]'))
ALTER TABLE [dbo].[TipoCuentaFija] CHECK CONSTRAINT [FK_TipoCuentaFija_Impuesto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoComprobante_Operacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoComprobante]'))
ALTER TABLE [dbo].[TipoComprobante]  WITH NOCHECK ADD  CONSTRAINT [FK_TipoComprobante_Operacion] FOREIGN KEY([IDOperacion])
REFERENCES [dbo].[Operacion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoComprobante_Operacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoComprobante]'))
ALTER TABLE [dbo].[TipoComprobante] CHECK CONSTRAINT [FK_TipoComprobante_Operacion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoMovimiento_Operacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoOperacion]'))
ALTER TABLE [dbo].[TipoOperacion]  WITH NOCHECK ADD  CONSTRAINT [FK_TipoMovimiento_Operacion] FOREIGN KEY([IDOperacion])
REFERENCES [dbo].[Operacion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoMovimiento_Operacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoOperacion]'))
ALTER TABLE [dbo].[TipoOperacion] CHECK CONSTRAINT [FK_TipoMovimiento_Operacion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transporte_Camion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transporte]'))
ALTER TABLE [dbo].[Transporte]  WITH NOCHECK ADD  CONSTRAINT [FK_Transporte_Camion] FOREIGN KEY([IDCamion])
REFERENCES [dbo].[Camion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transporte_Camion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transporte]'))
ALTER TABLE [dbo].[Transporte] CHECK CONSTRAINT [FK_Transporte_Camion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transporte_Chofer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transporte]'))
ALTER TABLE [dbo].[Transporte]  WITH NOCHECK ADD  CONSTRAINT [FK_Transporte_Chofer] FOREIGN KEY([IDChofer])
REFERENCES [dbo].[Chofer] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transporte_Chofer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transporte]'))
ALTER TABLE [dbo].[Transporte] CHECK CONSTRAINT [FK_Transporte_Chofer]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transporte_Distribuidor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transporte]'))
ALTER TABLE [dbo].[Transporte]  WITH NOCHECK ADD  CONSTRAINT [FK_Transporte_Distribuidor] FOREIGN KEY([IDDistribuidor])
REFERENCES [dbo].[Distribuidor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transporte_Distribuidor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transporte]'))
ALTER TABLE [dbo].[Transporte] CHECK CONSTRAINT [FK_Transporte_Distribuidor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Usuario_Perfil]') AND parent_object_id = OBJECT_ID(N'[dbo].[Usuario]'))
ALTER TABLE [dbo].[Usuario]  WITH NOCHECK ADD  CONSTRAINT [FK_Usuario_Perfil] FOREIGN KEY([IDPerfil])
REFERENCES [dbo].[Perfil] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Usuario_Perfil]') AND parent_object_id = OBJECT_ID(N'[dbo].[Usuario]'))
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Perfil]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoProductoLinea_Linea]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoProductoLinea]'))
ALTER TABLE [dbo].[TipoProductoLinea]  WITH NOCHECK ADD  CONSTRAINT [FK_TipoProductoLinea_Linea] FOREIGN KEY([IDLinea])
REFERENCES [dbo].[Linea] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoProductoLinea_Linea]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoProductoLinea]'))
ALTER TABLE [dbo].[TipoProductoLinea] CHECK CONSTRAINT [FK_TipoProductoLinea_Linea]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoProductoLinea_TipoProducto]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoProductoLinea]'))
ALTER TABLE [dbo].[TipoProductoLinea]  WITH NOCHECK ADD  CONSTRAINT [FK_TipoProductoLinea_TipoProducto] FOREIGN KEY([IDTipoProducto])
REFERENCES [dbo].[TipoProducto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TipoProductoLinea_TipoProducto]') AND parent_object_id = OBJECT_ID(N'[dbo].[TipoProductoLinea]'))
ALTER TABLE [dbo].[TipoProductoLinea] CHECK CONSTRAINT [FK_TipoProductoLinea_TipoProducto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Modelo_Marca]') AND parent_object_id = OBJECT_ID(N'[dbo].[Modelo]'))
ALTER TABLE [dbo].[Modelo]  WITH NOCHECK ADD  CONSTRAINT [FK_Modelo_Marca] FOREIGN KEY([IDMarca])
REFERENCES [dbo].[Marca] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Modelo_Marca]') AND parent_object_id = OBJECT_ID(N'[dbo].[Modelo]'))
ALTER TABLE [dbo].[Modelo] CHECK CONSTRAINT [FK_Modelo_Marca]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LogSucesos_TablaLog]') AND parent_object_id = OBJECT_ID(N'[dbo].[LogSucesos]'))
ALTER TABLE [dbo].[LogSucesos]  WITH NOCHECK ADD  CONSTRAINT [FK_LogSucesos_TablaLog] FOREIGN KEY([IDTabla])
REFERENCES [dbo].[TablaLog] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LogSucesos_TablaLog]') AND parent_object_id = OBJECT_ID(N'[dbo].[LogSucesos]'))
ALTER TABLE [dbo].[LogSucesos] CHECK CONSTRAINT [FK_LogSucesos_TablaLog]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LogSucesos_TipoOperacionLog]') AND parent_object_id = OBJECT_ID(N'[dbo].[LogSucesos]'))
ALTER TABLE [dbo].[LogSucesos]  WITH NOCHECK ADD  CONSTRAINT [FK_LogSucesos_TipoOperacionLog] FOREIGN KEY([IDTipoOperacionLog])
REFERENCES [dbo].[TipoOperacionLog] ([ID])
ON UPDATE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LogSucesos_TipoOperacionLog]') AND parent_object_id = OBJECT_ID(N'[dbo].[LogSucesos]'))
ALTER TABLE [dbo].[LogSucesos] CHECK CONSTRAINT [FK_LogSucesos_TipoOperacionLog]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Departamento_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Departamento]'))
ALTER TABLE [dbo].[Departamento]  WITH NOCHECK ADD  CONSTRAINT [FK_Departamento_Pais] FOREIGN KEY([IDPais])
REFERENCES [dbo].[Pais] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Departamento_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Departamento]'))
ALTER TABLE [dbo].[Departamento] CHECK CONSTRAINT [FK_Departamento_Pais]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CuentaContable_PlanCuenta]') AND parent_object_id = OBJECT_ID(N'[dbo].[CuentaContable]'))
ALTER TABLE [dbo].[CuentaContable]  WITH NOCHECK ADD  CONSTRAINT [FK_CuentaContable_PlanCuenta] FOREIGN KEY([IDPlanCuenta])
REFERENCES [dbo].[PlanCuenta] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CuentaContable_PlanCuenta]') AND parent_object_id = OBJECT_ID(N'[dbo].[CuentaContable]'))
ALTER TABLE [dbo].[CuentaContable] CHECK CONSTRAINT [FK_CuentaContable_PlanCuenta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccesoPerfil_Perfil]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]'))
ALTER TABLE [dbo].[AccesoPerfil]  WITH NOCHECK ADD  CONSTRAINT [FK_AccesoPerfil_Perfil] FOREIGN KEY([IDPerfil])
REFERENCES [dbo].[Perfil] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccesoPerfil_Perfil]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoPerfil]'))
ALTER TABLE [dbo].[AccesoPerfil] CHECK CONSTRAINT [FK_AccesoPerfil_Perfil]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccesoEspecificoPerfil_AccesoEspecifico]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoEspecificoPerfil]'))
ALTER TABLE [dbo].[AccesoEspecificoPerfil]  WITH NOCHECK ADD  CONSTRAINT [FK_AccesoEspecificoPerfil_AccesoEspecifico] FOREIGN KEY([IDAccesoEspecifico], [IDMenu])
REFERENCES [dbo].[AccesoEspecifico] ([ID], [IDMenu])
ON UPDATE CASCADE
ON DELETE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccesoEspecificoPerfil_AccesoEspecifico]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoEspecificoPerfil]'))
ALTER TABLE [dbo].[AccesoEspecificoPerfil] CHECK CONSTRAINT [FK_AccesoEspecificoPerfil_AccesoEspecifico]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccesoEspecificoPerfil_Perfil]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoEspecificoPerfil]'))
ALTER TABLE [dbo].[AccesoEspecificoPerfil]  WITH NOCHECK ADD  CONSTRAINT [FK_AccesoEspecificoPerfil_Perfil] FOREIGN KEY([IDPerfil])
REFERENCES [dbo].[Perfil] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccesoEspecificoPerfil_Perfil]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccesoEspecificoPerfil]'))
ALTER TABLE [dbo].[AccesoEspecificoPerfil] CHECK CONSTRAINT [FK_AccesoEspecificoPerfil_Perfil]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Ciudad_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Ciudad]'))
ALTER TABLE [dbo].[Ciudad]  WITH NOCHECK ADD  CONSTRAINT [FK_Ciudad_Departamento] FOREIGN KEY([IDDepartamento])
REFERENCES [dbo].[Departamento] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Ciudad_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Ciudad]'))
ALTER TABLE [dbo].[Ciudad] CHECK CONSTRAINT [FK_Ciudad_Departamento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Ciudad_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Ciudad]'))
ALTER TABLE [dbo].[Ciudad]  WITH NOCHECK ADD  CONSTRAINT [FK_Ciudad_Pais] FOREIGN KEY([IDPais])
REFERENCES [dbo].[Pais] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Ciudad_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Ciudad]'))
ALTER TABLE [dbo].[Ciudad] CHECK CONSTRAINT [FK_Ciudad_Pais]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleEfectivo_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleEfectivo]'))
ALTER TABLE [dbo].[DetalleEfectivo]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleEfectivo_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleEfectivo_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleEfectivo]'))
ALTER TABLE [dbo].[DetalleEfectivo] CHECK CONSTRAINT [FK_DetalleEfectivo_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LineaSubLinea_SubLinea]') AND parent_object_id = OBJECT_ID(N'[dbo].[LineaSubLinea]'))
ALTER TABLE [dbo].[LineaSubLinea]  WITH NOCHECK ADD  CONSTRAINT [FK_LineaSubLinea_SubLinea] FOREIGN KEY([IDSubLinea])
REFERENCES [dbo].[SubLinea] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LineaSubLinea_SubLinea]') AND parent_object_id = OBJECT_ID(N'[dbo].[LineaSubLinea]'))
ALTER TABLE [dbo].[LineaSubLinea] CHECK CONSTRAINT [FK_LineaSubLinea_SubLinea]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LineaSubLinea_TipoProductoLinea]') AND parent_object_id = OBJECT_ID(N'[dbo].[LineaSubLinea]'))
ALTER TABLE [dbo].[LineaSubLinea]  WITH NOCHECK ADD  CONSTRAINT [FK_LineaSubLinea_TipoProductoLinea] FOREIGN KEY([IDTipoProducto], [IDLinea])
REFERENCES [dbo].[TipoProductoLinea] ([IDTipoProducto], [IDLinea])
ON UPDATE CASCADE
ON DELETE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LineaSubLinea_TipoProductoLinea]') AND parent_object_id = OBJECT_ID(N'[dbo].[LineaSubLinea]'))
ALTER TABLE [dbo].[LineaSubLinea] CHECK CONSTRAINT [FK_LineaSubLinea_TipoProductoLinea]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sucursal_Ciudad]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sucursal]'))
ALTER TABLE [dbo].[Sucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_Sucursal_Ciudad] FOREIGN KEY([IDCiudad])
REFERENCES [dbo].[Ciudad] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sucursal_Ciudad]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sucursal]'))
ALTER TABLE [dbo].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Ciudad]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sucursal]'))
ALTER TABLE [dbo].[Sucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_Sucursal_Departamento] FOREIGN KEY([IDDepartamento])
REFERENCES [dbo].[Departamento] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sucursal]'))
ALTER TABLE [dbo].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Departamento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sucursal_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sucursal]'))
ALTER TABLE [dbo].[Sucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_Sucursal_Pais] FOREIGN KEY([IDPais])
REFERENCES [dbo].[Pais] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sucursal_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sucursal]'))
ALTER TABLE [dbo].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Pais]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubLineaSubLinea2_LineaSubLinea]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLineaSubLinea2]'))
ALTER TABLE [dbo].[SubLineaSubLinea2]  WITH NOCHECK ADD  CONSTRAINT [FK_SubLineaSubLinea2_LineaSubLinea] FOREIGN KEY([IDTipoProducto], [IDLinea], [IDSubLinea])
REFERENCES [dbo].[LineaSubLinea] ([IDTipoProducto], [IDLinea], [IDSubLinea])
ON UPDATE CASCADE
ON DELETE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubLineaSubLinea2_LineaSubLinea]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLineaSubLinea2]'))
ALTER TABLE [dbo].[SubLineaSubLinea2] CHECK CONSTRAINT [FK_SubLineaSubLinea2_LineaSubLinea]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubLineaSubLinea2_SubLinea2]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLineaSubLinea2]'))
ALTER TABLE [dbo].[SubLineaSubLinea2]  WITH NOCHECK ADD  CONSTRAINT [FK_SubLineaSubLinea2_SubLinea2] FOREIGN KEY([IDSubLinea2])
REFERENCES [dbo].[SubLinea2] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubLineaSubLinea2_SubLinea2]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLineaSubLinea2]'))
ALTER TABLE [dbo].[SubLineaSubLinea2] CHECK CONSTRAINT [FK_SubLineaSubLinea2_SubLinea2]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Barrio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor]  WITH NOCHECK ADD  CONSTRAINT [FK_Proveedor_Barrio] FOREIGN KEY([IDBarrio])
REFERENCES [dbo].[Barrio] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Barrio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_Barrio]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Ciudad]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor]  WITH NOCHECK ADD  CONSTRAINT [FK_Proveedor_Ciudad] FOREIGN KEY([IDCiudad])
REFERENCES [dbo].[Ciudad] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Ciudad]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_Ciudad]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_CuentaContable]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor]  WITH NOCHECK ADD  CONSTRAINT [FK_Proveedor_CuentaContable] FOREIGN KEY([IDCuentaContableCompra])
REFERENCES [dbo].[CuentaContable] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_CuentaContable]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_CuentaContable]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor]  WITH NOCHECK ADD  CONSTRAINT [FK_Proveedor_Departamento] FOREIGN KEY([IDDepartamento])
REFERENCES [dbo].[Departamento] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_Departamento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor]  WITH NOCHECK ADD  CONSTRAINT [FK_Proveedor_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor]  WITH NOCHECK ADD  CONSTRAINT [FK_Proveedor_Pais] FOREIGN KEY([IDPais])
REFERENCES [dbo].[Pais] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_Pais]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_TipoProveedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor]  WITH NOCHECK ADD  CONSTRAINT [FK_Proveedor_TipoProveedor] FOREIGN KEY([IDTipoProveedor])
REFERENCES [dbo].[TipoProveedor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_TipoProveedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_TipoProveedor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor]  WITH NOCHECK ADD  CONSTRAINT [FK_Proveedor_Usuario] FOREIGN KEY([IDUsuarioAlta])
REFERENCES [dbo].[Usuario] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Proveedor_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[Proveedor]'))
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_Usuario]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PuntoExpedicion_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
ALTER TABLE [dbo].[PuntoExpedicion]  WITH NOCHECK ADD  CONSTRAINT [FK_PuntoExpedicion_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PuntoExpedicion_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
ALTER TABLE [dbo].[PuntoExpedicion] CHECK CONSTRAINT [FK_PuntoExpedicion_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PuntoExpedicion_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
ALTER TABLE [dbo].[PuntoExpedicion]  WITH NOCHECK ADD  CONSTRAINT [FK_PuntoExpedicion_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PuntoExpedicion_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[PuntoExpedicion]'))
ALTER TABLE [dbo].[PuntoExpedicion] CHECK CONSTRAINT [FK_PuntoExpedicion_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubLinea2Marca_Marca]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLinea2Marca]'))
ALTER TABLE [dbo].[SubLinea2Marca]  WITH NOCHECK ADD  CONSTRAINT [FK_SubLinea2Marca_Marca] FOREIGN KEY([IDMarca])
REFERENCES [dbo].[Marca] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubLinea2Marca_Marca]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLinea2Marca]'))
ALTER TABLE [dbo].[SubLinea2Marca] CHECK CONSTRAINT [FK_SubLinea2Marca_Marca]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubLinea2Marca_SubLineaSubLinea2]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLinea2Marca]'))
ALTER TABLE [dbo].[SubLinea2Marca]  WITH NOCHECK ADD  CONSTRAINT [FK_SubLinea2Marca_SubLineaSubLinea2] FOREIGN KEY([IDTipoProducto], [IDLinea], [IDSubLinea], [IDSubLinea2])
REFERENCES [dbo].[SubLineaSubLinea2] ([IDTipoProducto], [IDLinea], [IDSubLinea], [IDSubLinea2])
ON UPDATE CASCADE
ON DELETE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubLinea2Marca_SubLineaSubLinea2]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubLinea2Marca]'))
ALTER TABLE [dbo].[SubLinea2Marca] CHECK CONSTRAINT [FK_SubLinea2Marca_SubLineaSubLinea2]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ListaPrecio_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ListaPrecio]'))
ALTER TABLE [dbo].[ListaPrecio]  WITH NOCHECK ADD  CONSTRAINT [FK_ListaPrecio_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ListaPrecio_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ListaPrecio]'))
ALTER TABLE [dbo].[ListaPrecio] CHECK CONSTRAINT [FK_ListaPrecio_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Division_Proveedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Division]'))
ALTER TABLE [dbo].[Division]  WITH NOCHECK ADD  CONSTRAINT [FK_Division_Proveedor] FOREIGN KEY([IDProveedor])
REFERENCES [dbo].[Proveedor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Division_Proveedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Division]'))
ALTER TABLE [dbo].[Division] CHECK CONSTRAINT [FK_Division_Proveedor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Deposito_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Deposito]'))
ALTER TABLE [dbo].[Deposito]  WITH NOCHECK ADD  CONSTRAINT [FK_Deposito_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Deposito_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Deposito]'))
ALTER TABLE [dbo].[Deposito] CHECK CONSTRAINT [FK_Deposito_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Terminal_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Terminal]'))
ALTER TABLE [dbo].[Terminal]  WITH NOCHECK ADD  CONSTRAINT [FK_Terminal_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Terminal_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Terminal]'))
ALTER TABLE [dbo].[Terminal] CHECK CONSTRAINT [FK_Terminal_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion]  WITH NOCHECK ADD  CONSTRAINT [FK_Transaccion_Deposito] FOREIGN KEY([IDDeposito])
REFERENCES [dbo].[Deposito] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion] CHECK CONSTRAINT [FK_Transaccion_Deposito]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Operacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion]  WITH NOCHECK ADD  CONSTRAINT [FK_Transaccion_Operacion] FOREIGN KEY([IDOperacion])
REFERENCES [dbo].[Operacion] ([ID])
ON UPDATE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Operacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion] CHECK CONSTRAINT [FK_Transaccion_Operacion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion]  WITH NOCHECK ADD  CONSTRAINT [FK_Transaccion_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion] CHECK CONSTRAINT [FK_Transaccion_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Terminal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion]  WITH NOCHECK ADD  CONSTRAINT [FK_Transaccion_Terminal] FOREIGN KEY([IDTerminal])
REFERENCES [dbo].[Terminal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Terminal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion] CHECK CONSTRAINT [FK_Transaccion_Terminal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion]  WITH NOCHECK ADD  CONSTRAINT [FK_Transaccion_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Transaccion_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transaccion]'))
ALTER TABLE [dbo].[Transaccion] CHECK CONSTRAINT [FK_Transaccion_Usuario]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Barrio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Barrio] FOREIGN KEY([IDBarrio])
REFERENCES [dbo].[Barrio] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Barrio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Barrio]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Ciudad]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Ciudad] FOREIGN KEY([IDCiudad])
REFERENCES [dbo].[Ciudad] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Ciudad]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Ciudad]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Cobrador]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Cobrador] FOREIGN KEY([IDCobrador])
REFERENCES [dbo].[Cobrador] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Cobrador]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Cobrador]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_CuentaContable]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_CuentaContable] FOREIGN KEY([IDCuentaContable])
REFERENCES [dbo].[CuentaContable] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_CuentaContable]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_CuentaContable]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Departamento] FOREIGN KEY([IDDepartamento])
REFERENCES [dbo].[Departamento] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Departamento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Distribuidor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Distribuidor] FOREIGN KEY([IDDistribuidor])
REFERENCES [dbo].[Distribuidor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Distribuidor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Distribuidor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_EstadoCliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_EstadoCliente] FOREIGN KEY([IDEstado])
REFERENCES [dbo].[EstadoCliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_EstadoCliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_EstadoCliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_ListaPrecio] FOREIGN KEY([IDListaPrecio])
REFERENCES [dbo].[ListaPrecio] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_ListaPrecio]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Pais] FOREIGN KEY([IDPais])
REFERENCES [dbo].[Pais] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Pais]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Promotor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Promotor] FOREIGN KEY([IDPromotor])
REFERENCES [dbo].[Promotor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Promotor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Promotor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_TipoCliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_TipoCliente] FOREIGN KEY([IDTipoCliente])
REFERENCES [dbo].[TipoCliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_TipoCliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_TipoCliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Usuario] FOREIGN KEY([IDUsuarioAlta])
REFERENCES [dbo].[Usuario] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Usuario]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_Vendedor] FOREIGN KEY([IDVendedor])
REFERENCES [dbo].[Vendedor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Vendedor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_ZonaVenta]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente]  WITH NOCHECK ADD  CONSTRAINT [FK_Cliente_ZonaVenta] FOREIGN KEY([IDZonaVenta])
REFERENCES [dbo].[ZonaVenta] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cliente_ZonaVenta]') AND parent_object_id = OBJECT_ID(N'[dbo].[Cliente]'))
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_ZonaVenta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Categoria]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_Categoria] FOREIGN KEY([IDCategoria])
REFERENCES [dbo].[Categoria] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Categoria]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Categoria]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_CuentaContable]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_CuentaContable] FOREIGN KEY([IDCuentaContableCompra])
REFERENCES [dbo].[CuentaContable] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_CuentaContable]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_CuentaContable]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_CuentaContable1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_CuentaContable1] FOREIGN KEY([IDCuentaContableVenta])
REFERENCES [dbo].[CuentaContable] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_CuentaContable1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_CuentaContable1]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Division]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_Division] FOREIGN KEY([IDDivision])
REFERENCES [dbo].[Division] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Division]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Division]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Linea]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_Linea] FOREIGN KEY([IDLinea])
REFERENCES [dbo].[Linea] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Linea]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Linea]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Marca]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_Marca] FOREIGN KEY([IDMarca])
REFERENCES [dbo].[Marca] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Marca]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Marca]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_Moneda] FOREIGN KEY([IDMonedaUltimoCosto])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_Pais] FOREIGN KEY([IDProcedencia])
REFERENCES [dbo].[Pais] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Pais]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Presentacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_Presentacion] FOREIGN KEY([IDPresentacion])
REFERENCES [dbo].[Presentacion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Presentacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Presentacion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Proveedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_Proveedor] FOREIGN KEY([IDProveedor])
REFERENCES [dbo].[Proveedor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_Proveedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Proveedor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_TipoProducto]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_TipoProducto] FOREIGN KEY([IDTipoProducto])
REFERENCES [dbo].[TipoProducto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_TipoProducto]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_TipoProducto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_UnidadMedida]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto]  WITH NOCHECK ADD  CONSTRAINT [FK_Producto_UnidadMedida] FOREIGN KEY([IDUnidadMedida])
REFERENCES [dbo].[UnidadMedida] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Producto_UnidadMedida]') AND parent_object_id = OBJECT_ID(N'[dbo].[Producto]'))
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_UnidadMedida]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarcaPresentacion_Presentacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarcaPresentacion]'))
ALTER TABLE [dbo].[MarcaPresentacion]  WITH NOCHECK ADD  CONSTRAINT [FK_MarcaPresentacion_Presentacion] FOREIGN KEY([IDPresentacion])
REFERENCES [dbo].[Presentacion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarcaPresentacion_Presentacion]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarcaPresentacion]'))
ALTER TABLE [dbo].[MarcaPresentacion] CHECK CONSTRAINT [FK_MarcaPresentacion_Presentacion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarcaPresentacion_SubLinea2Marca]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarcaPresentacion]'))
ALTER TABLE [dbo].[MarcaPresentacion]  WITH NOCHECK ADD  CONSTRAINT [FK_MarcaPresentacion_SubLinea2Marca] FOREIGN KEY([IDTipoProducto], [IDLinea], [IDSubLinea], [IDSubLinea2], [IDMarca])
REFERENCES [dbo].[SubLinea2Marca] ([IDTipoProducto], [IDLinea], [IDSubLinea], [IDSubLinea2], [IDMarca])
ON UPDATE CASCADE
ON DELETE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarcaPresentacion_SubLinea2Marca]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarcaPresentacion]'))
ALTER TABLE [dbo].[MarcaPresentacion] CHECK CONSTRAINT [FK_MarcaPresentacion_SubLinea2Marca]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCredito_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCredito]'))
ALTER TABLE [dbo].[NotaCredito]  WITH NOCHECK ADD  CONSTRAINT [FK_NotaCredito_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCredito_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCredito]'))
ALTER TABLE [dbo].[NotaCredito] CHECK CONSTRAINT [FK_NotaCredito_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCredito_PuntoExpedicion]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCredito]'))
ALTER TABLE [dbo].[NotaCredito]  WITH NOCHECK ADD  CONSTRAINT [FK_NotaCredito_PuntoExpedicion] FOREIGN KEY([IDPuntoExpedicion])
REFERENCES [dbo].[PuntoExpedicion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCredito_PuntoExpedicion]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCredito]'))
ALTER TABLE [dbo].[NotaCredito] CHECK CONSTRAINT [FK_NotaCredito_PuntoExpedicion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCredito_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCredito]'))
ALTER TABLE [dbo].[NotaCredito]  WITH NOCHECK ADD  CONSTRAINT [FK_NotaCredito_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCredito_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCredito]'))
ALTER TABLE [dbo].[NotaCredito] CHECK CONSTRAINT [FK_NotaCredito_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento]  WITH NOCHECK ADD  CONSTRAINT [FK_Movimiento_Deposito] FOREIGN KEY([IDDepositoSalida])
REFERENCES [dbo].[Deposito] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento] CHECK CONSTRAINT [FK_Movimiento_Deposito]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_Deposito1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento]  WITH NOCHECK ADD  CONSTRAINT [FK_Movimiento_Deposito1] FOREIGN KEY([IDDepositoEntrada])
REFERENCES [dbo].[Deposito] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_Deposito1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento] CHECK CONSTRAINT [FK_Movimiento_Deposito1]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento]  WITH NOCHECK ADD  CONSTRAINT [FK_Movimiento_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento] CHECK CONSTRAINT [FK_Movimiento_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_TipoMovimiento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento]  WITH NOCHECK ADD  CONSTRAINT [FK_Movimiento_TipoMovimiento] FOREIGN KEY([IDTipoOperacion])
REFERENCES [dbo].[TipoOperacion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_TipoMovimiento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento] CHECK CONSTRAINT [FK_Movimiento_TipoMovimiento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento]  WITH NOCHECK ADD  CONSTRAINT [FK_Movimiento_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Movimiento_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Movimiento]'))
ALTER TABLE [dbo].[Movimiento] CHECK CONSTRAINT [FK_Movimiento_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pedido_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
ALTER TABLE [dbo].[Pedido]  WITH NOCHECK ADD  CONSTRAINT [FK_Pedido_Deposito] FOREIGN KEY([IDDeposito])
REFERENCES [dbo].[Deposito] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pedido_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
ALTER TABLE [dbo].[Pedido] CHECK CONSTRAINT [FK_Pedido_Deposito]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pedido_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
ALTER TABLE [dbo].[Pedido]  WITH NOCHECK ADD  CONSTRAINT [FK_Pedido_ListaPrecio] FOREIGN KEY([IDListaPrecio])
REFERENCES [dbo].[ListaPrecio] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pedido_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
ALTER TABLE [dbo].[Pedido] CHECK CONSTRAINT [FK_Pedido_ListaPrecio]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pedido_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
ALTER TABLE [dbo].[Pedido]  WITH NOCHECK ADD  CONSTRAINT [FK_Pedido_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Pedido_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Pedido]'))
ALTER TABLE [dbo].[Pedido] CHECK CONSTRAINT [FK_Pedido_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoteDistribucion_Distribuidor]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
ALTER TABLE [dbo].[LoteDistribucion]  WITH NOCHECK ADD  CONSTRAINT [FK_LoteDistribucion_Distribuidor] FOREIGN KEY([IDDistribuidor])
REFERENCES [dbo].[Distribuidor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoteDistribucion_Distribuidor]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
ALTER TABLE [dbo].[LoteDistribucion] CHECK CONSTRAINT [FK_LoteDistribucion_Distribuidor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoteDistribucion_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
ALTER TABLE [dbo].[LoteDistribucion]  WITH NOCHECK ADD  CONSTRAINT [FK_LoteDistribucion_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoteDistribucion_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
ALTER TABLE [dbo].[LoteDistribucion] CHECK CONSTRAINT [FK_LoteDistribucion_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoteDistribucion_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
ALTER TABLE [dbo].[LoteDistribucion]  WITH NOCHECK ADD  CONSTRAINT [FK_LoteDistribucion_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_LoteDistribucion_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[LoteDistribucion]'))
ALTER TABLE [dbo].[LoteDistribucion] CHECK CONSTRAINT [FK_LoteDistribucion_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoZona_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoZona]'))
ALTER TABLE [dbo].[ProductoZona]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoZona_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([ID])
ON DELETE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoZona_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoZona]'))
ALTER TABLE [dbo].[ProductoZona] CHECK CONSTRAINT [FK_ProductoZona_Producto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecioExcepciones_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones] CHECK CONSTRAINT [FK_ProductoListaPrecioExcepciones_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecioExcepciones_ListaPrecio] FOREIGN KEY([IDListaPrecio])
REFERENCES [dbo].[ListaPrecio] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones] CHECK CONSTRAINT [FK_ProductoListaPrecioExcepciones_ListaPrecio]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecioExcepciones_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones] CHECK CONSTRAINT [FK_ProductoListaPrecioExcepciones_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecioExcepciones_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones] CHECK CONSTRAINT [FK_ProductoListaPrecioExcepciones_Producto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecioExcepciones_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecioExcepciones_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecioExcepciones]'))
ALTER TABLE [dbo].[ProductoListaPrecioExcepciones] CHECK CONSTRAINT [FK_ProductoListaPrecioExcepciones_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecio_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]'))
ALTER TABLE [dbo].[ProductoListaPrecio]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecio_ListaPrecio] FOREIGN KEY([IDListaPrecio])
REFERENCES [dbo].[ListaPrecio] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecio_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]'))
ALTER TABLE [dbo].[ProductoListaPrecio] CHECK CONSTRAINT [FK_ProductoListaPrecio_ListaPrecio]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecio_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]'))
ALTER TABLE [dbo].[ProductoListaPrecio]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecio_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecio_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]'))
ALTER TABLE [dbo].[ProductoListaPrecio] CHECK CONSTRAINT [FK_ProductoListaPrecio_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecio_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]'))
ALTER TABLE [dbo].[ProductoListaPrecio]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecio_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecio_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]'))
ALTER TABLE [dbo].[ProductoListaPrecio] CHECK CONSTRAINT [FK_ProductoListaPrecio_Producto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecio_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]'))
ALTER TABLE [dbo].[ProductoListaPrecio]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductoListaPrecio_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductoListaPrecio_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductoListaPrecio]'))
ALTER TABLE [dbo].[ProductoListaPrecio] CHECK CONSTRAINT [FK_ProductoListaPrecio_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaDebito_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaDebito]'))
ALTER TABLE [dbo].[NotaDebito]  WITH NOCHECK ADD  CONSTRAINT [FK_NotaDebito_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaDebito_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaDebito]'))
ALTER TABLE [dbo].[NotaDebito] CHECK CONSTRAINT [FK_NotaDebito_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaDebito_PuntoExpedicion]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaDebito]'))
ALTER TABLE [dbo].[NotaDebito]  WITH NOCHECK ADD  CONSTRAINT [FK_NotaDebito_PuntoExpedicion] FOREIGN KEY([IDPuntoExpedicion])
REFERENCES [dbo].[PuntoExpedicion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaDebito_PuntoExpedicion]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaDebito]'))
ALTER TABLE [dbo].[NotaDebito] CHECK CONSTRAINT [FK_NotaDebito_PuntoExpedicion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleImpuesto_Impuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleImpuesto]'))
ALTER TABLE [dbo].[DetalleImpuesto]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleImpuesto_Impuesto] FOREIGN KEY([IDImpuesto])
REFERENCES [dbo].[Impuesto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleImpuesto_Impuesto]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleImpuesto]'))
ALTER TABLE [dbo].[DetalleImpuesto] CHECK CONSTRAINT [FK_DetalleImpuesto_Impuesto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleImpuesto_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleImpuesto]'))
ALTER TABLE [dbo].[DetalleImpuesto]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleImpuesto_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleImpuesto_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleImpuesto]'))
ALTER TABLE [dbo].[DetalleImpuesto] CHECK CONSTRAINT [FK_DetalleImpuesto_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentoAnulado_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentoAnulado]'))
ALTER TABLE [dbo].[DocumentoAnulado]  WITH NOCHECK ADD  CONSTRAINT [FK_DocumentoAnulado_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocumentoAnulado_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocumentoAnulado]'))
ALTER TABLE [dbo].[DocumentoAnulado] CHECK CONSTRAINT [FK_DocumentoAnulado_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ExistenciaDeposito_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExistenciaDeposito]'))
ALTER TABLE [dbo].[ExistenciaDeposito]  WITH NOCHECK ADD  CONSTRAINT [FK_ExistenciaDeposito_Deposito] FOREIGN KEY([IDDeposito])
REFERENCES [dbo].[Deposito] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ExistenciaDeposito_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExistenciaDeposito]'))
ALTER TABLE [dbo].[ExistenciaDeposito] CHECK CONSTRAINT [FK_ExistenciaDeposito_Deposito]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ExistenciaDeposito_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExistenciaDeposito]'))
ALTER TABLE [dbo].[ExistenciaDeposito]  WITH NOCHECK ADD  CONSTRAINT [FK_ExistenciaDeposito_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ExistenciaDeposito_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ExistenciaDeposito]'))
ALTER TABLE [dbo].[ExistenciaDeposito] CHECK CONSTRAINT [FK_ExistenciaDeposito_Producto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Asiento_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
ALTER TABLE [dbo].[Asiento]  WITH NOCHECK ADD  CONSTRAINT [FK_Asiento_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Asiento_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
ALTER TABLE [dbo].[Asiento] CHECK CONSTRAINT [FK_Asiento_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Asiento_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
ALTER TABLE [dbo].[Asiento]  WITH NOCHECK ADD  CONSTRAINT [FK_Asiento_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Asiento_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
ALTER TABLE [dbo].[Asiento] CHECK CONSTRAINT [FK_Asiento_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Asiento_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
ALTER TABLE [dbo].[Asiento]  WITH NOCHECK ADD  CONSTRAINT [FK_Asiento_Usuario] FOREIGN KEY([IDUsuarioConciliado])
REFERENCES [dbo].[Usuario] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Asiento_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[Asiento]'))
ALTER TABLE [dbo].[Asiento] CHECK CONSTRAINT [FK_Asiento_Usuario]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Barrio]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteSucursal_Barrio] FOREIGN KEY([IDBarrio])
REFERENCES [dbo].[Barrio] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Barrio]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal] CHECK CONSTRAINT [FK_ClienteSucursal_Barrio]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Ciudad]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteSucursal_Ciudad] FOREIGN KEY([IDCiudad])
REFERENCES [dbo].[Ciudad] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Ciudad]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal] CHECK CONSTRAINT [FK_ClienteSucursal_Ciudad]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteSucursal_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal] CHECK CONSTRAINT [FK_ClienteSucursal_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteSucursal_Departamento] FOREIGN KEY([IDDepartamento])
REFERENCES [dbo].[Departamento] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Departamento]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal] CHECK CONSTRAINT [FK_ClienteSucursal_Departamento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteSucursal_Pais] FOREIGN KEY([IDPais])
REFERENCES [dbo].[Pais] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Pais]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal] CHECK CONSTRAINT [FK_ClienteSucursal_Pais]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteSucursal_Vendedor] FOREIGN KEY([IDVendedor])
REFERENCES [dbo].[Vendedor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteSucursal_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteSucursal]'))
ALTER TABLE [dbo].[ClienteSucursal] CHECK CONSTRAINT [FK_ClienteSucursal_Vendedor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteCuentaBancaria_Banco]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteCuentaBancaria]'))
ALTER TABLE [dbo].[ClienteCuentaBancaria]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteCuentaBancaria_Banco] FOREIGN KEY([IDBanco])
REFERENCES [dbo].[Banco] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteCuentaBancaria_Banco]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteCuentaBancaria]'))
ALTER TABLE [dbo].[ClienteCuentaBancaria] CHECK CONSTRAINT [FK_ClienteCuentaBancaria_Banco]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteCuentaBancaria_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteCuentaBancaria]'))
ALTER TABLE [dbo].[ClienteCuentaBancaria]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteCuentaBancaria_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteCuentaBancaria_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteCuentaBancaria]'))
ALTER TABLE [dbo].[ClienteCuentaBancaria] CHECK CONSTRAINT [FK_ClienteCuentaBancaria_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteCuentaBancaria_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteCuentaBancaria]'))
ALTER TABLE [dbo].[ClienteCuentaBancaria]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteCuentaBancaria_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteCuentaBancaria_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteCuentaBancaria]'))
ALTER TABLE [dbo].[ClienteCuentaBancaria] CHECK CONSTRAINT [FK_ClienteCuentaBancaria_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteContacto_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteContacto]'))
ALTER TABLE [dbo].[ClienteContacto]  WITH NOCHECK ADD  CONSTRAINT [FK_ClienteContacto_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClienteContacto_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClienteContacto]'))
ALTER TABLE [dbo].[ClienteContacto] CHECK CONSTRAINT [FK_ClienteContacto_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ComisionProducto_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ComisionProducto]'))
ALTER TABLE [dbo].[ComisionProducto]  WITH CHECK ADD  CONSTRAINT [FK_ComisionProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ComisionProducto_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[ComisionProducto]'))
ALTER TABLE [dbo].[ComisionProducto] CHECK CONSTRAINT [FK_ComisionProducto_Producto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ComisionProducto_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ComisionProducto]'))
ALTER TABLE [dbo].[ComisionProducto]  WITH CHECK ADD  CONSTRAINT [FK_ComisionProducto_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ComisionProducto_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ComisionProducto]'))
ALTER TABLE [dbo].[ComisionProducto] CHECK CONSTRAINT [FK_ComisionProducto_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ComisionProducto_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ComisionProducto]'))
ALTER TABLE [dbo].[ComisionProducto]  WITH CHECK ADD  CONSTRAINT [FK_ComisionProducto_Vendedor] FOREIGN KEY([IDVendedor])
REFERENCES [dbo].[Vendedor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ComisionProducto_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[ComisionProducto]'))
ALTER TABLE [dbo].[ComisionProducto] CHECK CONSTRAINT [FK_ComisionProducto_Vendedor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cobranza_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
ALTER TABLE [dbo].[CobranzaCredito]  WITH NOCHECK ADD  CONSTRAINT [FK_Cobranza_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Cobranza_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
ALTER TABLE [dbo].[CobranzaCredito] CHECK CONSTRAINT [FK_Cobranza_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaCredito_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
ALTER TABLE [dbo].[CobranzaCredito]  WITH CHECK ADD  CONSTRAINT [FK_CobranzaCredito_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaCredito_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
ALTER TABLE [dbo].[CobranzaCredito] CHECK CONSTRAINT [FK_CobranzaCredito_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ReciboCliente_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
ALTER TABLE [dbo].[CobranzaCredito]  WITH NOCHECK ADD  CONSTRAINT [FK_ReciboCliente_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ReciboCliente_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
ALTER TABLE [dbo].[CobranzaCredito] CHECK CONSTRAINT [FK_ReciboCliente_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ReciboCliente_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
ALTER TABLE [dbo].[CobranzaCredito]  WITH NOCHECK ADD  CONSTRAINT [FK_ReciboCliente_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ReciboCliente_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaCredito]'))
ALTER TABLE [dbo].[CobranzaCredito] CHECK CONSTRAINT [FK_ReciboCliente_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Deposito] FOREIGN KEY([IDDeposito])
REFERENCES [dbo].[Deposito] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Deposito]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Deposito]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH NOCHECK ADD  CONSTRAINT [FK_Venta_ListaPrecio] FOREIGN KEY([IDListaPrecio])
REFERENCES [dbo].[ListaPrecio] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_ListaPrecio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] NOCHECK CONSTRAINT [FK_Venta_ListaPrecio]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_MotivoAnulacionVenta]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH NOCHECK ADD  CONSTRAINT [FK_Venta_MotivoAnulacionVenta] FOREIGN KEY([IDMotivo])
REFERENCES [dbo].[MotivoAnulacionVenta] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_MotivoAnulacionVenta]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] NOCHECK CONSTRAINT [FK_Venta_MotivoAnulacionVenta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Promotor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Promotor] FOREIGN KEY([IDPromotor])
REFERENCES [dbo].[Promotor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Promotor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Promotor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_PuntoExpedicion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_PuntoExpedicion] FOREIGN KEY([IDPuntoExpedicion])
REFERENCES [dbo].[PuntoExpedicion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_PuntoExpedicion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_PuntoExpedicion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Vendedor] FOREIGN KEY([IDVendedor])
REFERENCES [dbo].[Vendedor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venta_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venta]'))
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Vendedor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VentaLoteDistribucion_LoteDistribucion]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaLoteDistribucion]'))
ALTER TABLE [dbo].[VentaLoteDistribucion]  WITH NOCHECK ADD  CONSTRAINT [FK_VentaLoteDistribucion_LoteDistribucion] FOREIGN KEY([IDTransaccionLote])
REFERENCES [dbo].[LoteDistribucion] ([IDTransaccion])
ON DELETE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VentaLoteDistribucion_LoteDistribucion]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaLoteDistribucion]'))
ALTER TABLE [dbo].[VentaLoteDistribucion] CHECK CONSTRAINT [FK_VentaLoteDistribucion_LoteDistribucion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VentaLoteDistribucion_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaLoteDistribucion]'))
ALTER TABLE [dbo].[VentaLoteDistribucion]  WITH NOCHECK ADD  CONSTRAINT [FK_VentaLoteDistribucion_Venta] FOREIGN KEY([IDTransaccionVenta])
REFERENCES [dbo].[Venta] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VentaLoteDistribucion_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaLoteDistribucion]'))
ALTER TABLE [dbo].[VentaLoteDistribucion] CHECK CONSTRAINT [FK_VentaLoteDistribucion_Venta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_LoteDistribucion]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado]  WITH NOCHECK ADD  CONSTRAINT [FK_CobranzaContado_LoteDistribucion] FOREIGN KEY([IDTransaccionLote])
REFERENCES [dbo].[LoteDistribucion] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_LoteDistribucion]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado] CHECK CONSTRAINT [FK_CobranzaContado_LoteDistribucion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado]  WITH NOCHECK ADD  CONSTRAINT [FK_CobranzaContado_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_Sucursal]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado] CHECK CONSTRAINT [FK_CobranzaContado_Sucursal]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado]  WITH NOCHECK ADD  CONSTRAINT [FK_CobranzaContado_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado] CHECK CONSTRAINT [FK_CobranzaContado_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado]  WITH NOCHECK ADD  CONSTRAINT [FK_CobranzaContado_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado] CHECK CONSTRAINT [FK_CobranzaContado_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado]  WITH NOCHECK ADD  CONSTRAINT [FK_CobranzaContado_Usuario] FOREIGN KEY([IDUsuarioAnulado])
REFERENCES [dbo].[Usuario] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CobranzaContado_Usuario]') AND parent_object_id = OBJECT_ID(N'[dbo].[CobranzaContado]'))
ALTER TABLE [dbo].[CobranzaContado] CHECK CONSTRAINT [FK_CobranzaContado_Usuario]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeCliente_Banco]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
ALTER TABLE [dbo].[ChequeCliente]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequeCliente_Banco] FOREIGN KEY([IDBanco])
REFERENCES [dbo].[Banco] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeCliente_Banco]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
ALTER TABLE [dbo].[ChequeCliente] CHECK CONSTRAINT [FK_ChequeCliente_Banco]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeCliente_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
ALTER TABLE [dbo].[ChequeCliente]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequeCliente_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeCliente_Cliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
ALTER TABLE [dbo].[ChequeCliente] CHECK CONSTRAINT [FK_ChequeCliente_Cliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeCliente_ClienteCuentaBancaria]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
ALTER TABLE [dbo].[ChequeCliente]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequeCliente_ClienteCuentaBancaria] FOREIGN KEY([IDCuentaBancaria])
REFERENCES [dbo].[ClienteCuentaBancaria] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeCliente_ClienteCuentaBancaria]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
ALTER TABLE [dbo].[ChequeCliente] CHECK CONSTRAINT [FK_ChequeCliente_ClienteCuentaBancaria]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeCliente_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
ALTER TABLE [dbo].[ChequeCliente]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequeCliente_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeCliente_Moneda]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeCliente]'))
ALTER TABLE [dbo].[ChequeCliente] CHECK CONSTRAINT [FK_ChequeCliente_Moneda]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleVenta_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
ALTER TABLE [dbo].[DetalleVenta]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleVenta_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleVenta_Producto]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
ALTER TABLE [dbo].[DetalleVenta] CHECK CONSTRAINT [FK_DetalleVenta_Producto]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleVenta_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
ALTER TABLE [dbo].[DetalleVenta]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleVenta_Venta] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Venta] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleVenta_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleVenta]'))
ALTER TABLE [dbo].[DetalleVenta] CHECK CONSTRAINT [FK_DetalleVenta_Venta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetallePedido_Pedido]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
ALTER TABLE [dbo].[DetallePedido]  WITH NOCHECK ADD  CONSTRAINT [FK_DetallePedido_Pedido] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Pedido] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetallePedido_Pedido]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetallePedido]'))
ALTER TABLE [dbo].[DetallePedido] CHECK CONSTRAINT [FK_DetallePedido_Pedido]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleNotaDebito_NotaDebito]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]'))
ALTER TABLE [dbo].[DetalleNotaDebito]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleNotaDebito_NotaDebito] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[NotaDebito] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleNotaDebito_NotaDebito]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]'))
ALTER TABLE [dbo].[DetalleNotaDebito] CHECK CONSTRAINT [FK_DetalleNotaDebito_NotaDebito]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleNotaDebito_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]'))
ALTER TABLE [dbo].[DetalleNotaDebito]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleNotaDebito_Venta] FOREIGN KEY([IDTransaccionVenta])
REFERENCES [dbo].[Venta] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleNotaDebito_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaDebito]'))
ALTER TABLE [dbo].[DetalleNotaDebito] CHECK CONSTRAINT [FK_DetalleNotaDebito_Venta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleNotaCredito_NotaCredito]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]'))
ALTER TABLE [dbo].[DetalleNotaCredito]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleNotaCredito_NotaCredito] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[NotaCredito] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleNotaCredito_NotaCredito]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]'))
ALTER TABLE [dbo].[DetalleNotaCredito] CHECK CONSTRAINT [FK_DetalleNotaCredito_NotaCredito]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleNotaCredito_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]'))
ALTER TABLE [dbo].[DetalleNotaCredito]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleNotaCredito_Venta] FOREIGN KEY([IDTransaccionVenta])
REFERENCES [dbo].[Venta] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleNotaCredito_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleNotaCredito]'))
ALTER TABLE [dbo].[DetalleNotaCredito] CHECK CONSTRAINT [FK_DetalleNotaCredito_Venta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleAsiento_Asiento]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleAsiento]'))
ALTER TABLE [dbo].[DetalleAsiento]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleAsiento_Asiento] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Asiento] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleAsiento_Asiento]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleAsiento]'))
ALTER TABLE [dbo].[DetalleAsiento] CHECK CONSTRAINT [FK_DetalleAsiento_Asiento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleAsiento_CuentaContable]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleAsiento]'))
ALTER TABLE [dbo].[DetalleAsiento]  WITH NOCHECK ADD  CONSTRAINT [FK_DetalleAsiento_CuentaContable] FOREIGN KEY([IDCuentaContable])
REFERENCES [dbo].[CuentaContable] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DetalleAsiento_CuentaContable]') AND parent_object_id = OBJECT_ID(N'[dbo].[DetalleAsiento]'))
ALTER TABLE [dbo].[DetalleAsiento] CHECK CONSTRAINT [FK_DetalleAsiento_CuentaContable]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PedidoVenta_Pedido]') AND parent_object_id = OBJECT_ID(N'[dbo].[PedidoVenta]'))
ALTER TABLE [dbo].[PedidoVenta]  WITH NOCHECK ADD  CONSTRAINT [FK_PedidoVenta_Pedido] FOREIGN KEY([IDTransaccionPedido])
REFERENCES [dbo].[Pedido] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PedidoVenta_Pedido]') AND parent_object_id = OBJECT_ID(N'[dbo].[PedidoVenta]'))
ALTER TABLE [dbo].[PedidoVenta] CHECK CONSTRAINT [FK_PedidoVenta_Pedido]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PedidoVenta_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[PedidoVenta]'))
ALTER TABLE [dbo].[PedidoVenta]  WITH NOCHECK ADD  CONSTRAINT [FK_PedidoVenta_Venta] FOREIGN KEY([IDTransaccionVenta])
REFERENCES [dbo].[Venta] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PedidoVenta_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[PedidoVenta]'))
ALTER TABLE [dbo].[PedidoVenta] CHECK CONSTRAINT [FK_PedidoVenta_Venta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PedidoVendedor_Pedido]') AND parent_object_id = OBJECT_ID(N'[dbo].[PedidoVendedor]'))
ALTER TABLE [dbo].[PedidoVendedor]  WITH CHECK ADD  CONSTRAINT [FK_PedidoVendedor_Pedido] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Pedido] ([IDTransaccion])
ON DELETE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PedidoVendedor_Pedido]') AND parent_object_id = OBJECT_ID(N'[dbo].[PedidoVendedor]'))
ALTER TABLE [dbo].[PedidoVendedor] CHECK CONSTRAINT [FK_PedidoVendedor_Pedido]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PedidoVendedor_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[PedidoVendedor]'))
ALTER TABLE [dbo].[PedidoVendedor]  WITH CHECK ADD  CONSTRAINT [FK_PedidoVendedor_Vendedor] FOREIGN KEY([IDVendedor])
REFERENCES [dbo].[Vendedor] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PedidoVendedor_Vendedor]') AND parent_object_id = OBJECT_ID(N'[dbo].[PedidoVendedor]'))
ALTER TABLE [dbo].[PedidoVendedor] CHECK CONSTRAINT [FK_PedidoVendedor_Vendedor]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCreditoVenta_NotaCredito]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCreditoVenta]'))
ALTER TABLE [dbo].[NotaCreditoVenta]  WITH NOCHECK ADD  CONSTRAINT [FK_NotaCreditoVenta_NotaCredito] FOREIGN KEY([IDTransaccionNotaCredito])
REFERENCES [dbo].[NotaCredito] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCreditoVenta_NotaCredito]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCreditoVenta]'))
ALTER TABLE [dbo].[NotaCreditoVenta] CHECK CONSTRAINT [FK_NotaCreditoVenta_NotaCredito]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCreditoVenta_Venta1]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCreditoVenta]'))
ALTER TABLE [dbo].[NotaCreditoVenta]  WITH NOCHECK ADD  CONSTRAINT [FK_NotaCreditoVenta_Venta1] FOREIGN KEY([IDTransaccionVentaGenerada])
REFERENCES [dbo].[Venta] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NotaCreditoVenta_Venta1]') AND parent_object_id = OBJECT_ID(N'[dbo].[NotaCreditoVenta]'))
ALTER TABLE [dbo].[NotaCreditoVenta] CHECK CONSTRAINT [FK_NotaCreditoVenta_Venta1]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeClienteCobranzaContado_ChequeCliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeClienteCobranzaContado]'))
ALTER TABLE [dbo].[ChequeClienteCobranzaContado]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequeClienteCobranzaContado_ChequeCliente] FOREIGN KEY([IDTransaccionCheque])
REFERENCES [dbo].[ChequeCliente] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeClienteCobranzaContado_ChequeCliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeClienteCobranzaContado]'))
ALTER TABLE [dbo].[ChequeClienteCobranzaContado] CHECK CONSTRAINT [FK_ChequeClienteCobranzaContado_ChequeCliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeClienteCobranzaContado_CobranzaContado]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeClienteCobranzaContado]'))
ALTER TABLE [dbo].[ChequeClienteCobranzaContado]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequeClienteCobranzaContado_CobranzaContado] FOREIGN KEY([IDTransaccionCObranza])
REFERENCES [dbo].[CobranzaContado] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeClienteCobranzaContado_CobranzaContado]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeClienteCobranzaContado]'))
ALTER TABLE [dbo].[ChequeClienteCobranzaContado] CHECK CONSTRAINT [FK_ChequeClienteCobranzaContado_CobranzaContado]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeClienteCobranzaContado_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeClienteCobranzaContado]'))
ALTER TABLE [dbo].[ChequeClienteCobranzaContado]  WITH NOCHECK ADD  CONSTRAINT [FK_ChequeClienteCobranzaContado_Venta] FOREIGN KEY([IDTransaccionVenta])
REFERENCES [dbo].[Venta] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChequeClienteCobranzaContado_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChequeClienteCobranzaContado]'))
ALTER TABLE [dbo].[ChequeClienteCobranzaContado] CHECK CONSTRAINT [FK_ChequeClienteCobranzaContado_Venta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DescuentoPedido_DetallePedido]') AND parent_object_id = OBJECT_ID(N'[dbo].[DescuentoPedido]'))
ALTER TABLE [dbo].[DescuentoPedido]  WITH NOCHECK ADD  CONSTRAINT [FK_DescuentoPedido_DetallePedido] FOREIGN KEY([IDTransaccion], [IDProducto], [ID])
REFERENCES [dbo].[DetallePedido] ([IDTransaccion], [IDProducto], [ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DescuentoPedido_DetallePedido]') AND parent_object_id = OBJECT_ID(N'[dbo].[DescuentoPedido]'))
ALTER TABLE [dbo].[DescuentoPedido] CHECK CONSTRAINT [FK_DescuentoPedido_DetallePedido]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DescuentoPedido_TipoDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[DescuentoPedido]'))
ALTER TABLE [dbo].[DescuentoPedido]  WITH NOCHECK ADD  CONSTRAINT [FK_DescuentoPedido_TipoDescuento] FOREIGN KEY([IDTipoDescuento])
REFERENCES [dbo].[TipoDescuento] ([ID])
ON UPDATE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DescuentoPedido_TipoDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[DescuentoPedido]'))
ALTER TABLE [dbo].[DescuentoPedido] CHECK CONSTRAINT [FK_DescuentoPedido_TipoDescuento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Descuento_DetalleVenta]') AND parent_object_id = OBJECT_ID(N'[dbo].[Descuento]'))
ALTER TABLE [dbo].[Descuento]  WITH NOCHECK ADD  CONSTRAINT [FK_Descuento_DetalleVenta] FOREIGN KEY([IDTransaccion], [IDProducto], [ID])
REFERENCES [dbo].[DetalleVenta] ([IDTransaccion], [IDProducto], [ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Descuento_DetalleVenta]') AND parent_object_id = OBJECT_ID(N'[dbo].[Descuento]'))
ALTER TABLE [dbo].[Descuento] CHECK CONSTRAINT [FK_Descuento_DetalleVenta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Descuento_TipoDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Descuento]'))
ALTER TABLE [dbo].[Descuento]  WITH NOCHECK ADD  CONSTRAINT [FK_Descuento_TipoDescuento] FOREIGN KEY([IDTipoDescuento])
REFERENCES [dbo].[TipoDescuento] ([ID])
ON UPDATE CASCADE

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Descuento_TipoDescuento]') AND parent_object_id = OBJECT_ID(N'[dbo].[Descuento]'))
ALTER TABLE [dbo].[Descuento] CHECK CONSTRAINT [FK_Descuento_TipoDescuento]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FormaPago_ChequeCliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPago]'))
ALTER TABLE [dbo].[FormaPago]  WITH NOCHECK ADD  CONSTRAINT [FK_FormaPago_ChequeCliente] FOREIGN KEY([IDTransaccionCheque])
REFERENCES [dbo].[ChequeCliente] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FormaPago_ChequeCliente]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPago]'))
ALTER TABLE [dbo].[FormaPago] CHECK CONSTRAINT [FK_FormaPago_ChequeCliente]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FormaPago_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPago]'))
ALTER TABLE [dbo].[FormaPago]  WITH NOCHECK ADD  CONSTRAINT [FK_FormaPago_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FormaPago_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPago]'))
ALTER TABLE [dbo].[FormaPago] CHECK CONSTRAINT [FK_FormaPago_Transaccion]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VentaCobranza_CobranzaContado]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaCobranza]'))
ALTER TABLE [dbo].[VentaCobranza]  WITH NOCHECK ADD  CONSTRAINT [FK_VentaCobranza_CobranzaContado] FOREIGN KEY([IDTransaccionCobranza])
REFERENCES [dbo].[CobranzaContado] ([IDTransaccion])
NOT FOR REPLICATION

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VentaCobranza_CobranzaContado]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaCobranza]'))
ALTER TABLE [dbo].[VentaCobranza] NOCHECK CONSTRAINT [FK_VentaCobranza_CobranzaContado]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VentaCobranza_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaCobranza]'))
ALTER TABLE [dbo].[VentaCobranza]  WITH NOCHECK ADD  CONSTRAINT [FK_VentaCobranza_Venta] FOREIGN KEY([IDTransaccionVenta])
REFERENCES [dbo].[Venta] ([IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VentaCobranza_Venta]') AND parent_object_id = OBJECT_ID(N'[dbo].[VentaCobranza]'))
ALTER TABLE [dbo].[VentaCobranza] CHECK CONSTRAINT [FK_VentaCobranza_Venta]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FormaPagoDocumento_FormaPago]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPagoDocumento]'))
ALTER TABLE [dbo].[FormaPagoDocumento]  WITH NOCHECK ADD  CONSTRAINT [FK_FormaPagoDocumento_FormaPago] FOREIGN KEY([ID], [IDTransaccion])
REFERENCES [dbo].[FormaPago] ([ID], [IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FormaPagoDocumento_FormaPago]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPagoDocumento]'))
ALTER TABLE [dbo].[FormaPagoDocumento] CHECK CONSTRAINT [FK_FormaPagoDocumento_FormaPago]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FormaPagoDocumento_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPagoDocumento]'))
ALTER TABLE [dbo].[FormaPagoDocumento]  WITH CHECK ADD  CONSTRAINT [FK_FormaPagoDocumento_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FormaPagoDocumento_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[FormaPagoDocumento]'))
ALTER TABLE [dbo].[FormaPagoDocumento] CHECK CONSTRAINT [FK_FormaPagoDocumento_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Efectivo_FormaPago]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
ALTER TABLE [dbo].[Efectivo]  WITH NOCHECK ADD  CONSTRAINT [FK_Efectivo_FormaPago] FOREIGN KEY([ID], [IDTransaccion])
REFERENCES [dbo].[FormaPago] ([ID], [IDTransaccion])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Efectivo_FormaPago]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
ALTER TABLE [dbo].[Efectivo] NOCHECK CONSTRAINT [FK_Efectivo_FormaPago]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Efectivo_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
ALTER TABLE [dbo].[Efectivo]  WITH NOCHECK ADD  CONSTRAINT [FK_Efectivo_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Efectivo_TipoComprobante]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
ALTER TABLE [dbo].[Efectivo] CHECK CONSTRAINT [FK_Efectivo_TipoComprobante]

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Efectivo_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
ALTER TABLE [dbo].[Efectivo]  WITH CHECK ADD  CONSTRAINT [FK_Efectivo_Transaccion] FOREIGN KEY([IDTransaccion])
REFERENCES [dbo].[Transaccion] ([ID])

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Efectivo_Transaccion]') AND parent_object_id = OBJECT_ID(N'[dbo].[Efectivo]'))
ALTER TABLE [dbo].[Efectivo] CHECK CONSTRAINT [FK_Efectivo_Transaccion]


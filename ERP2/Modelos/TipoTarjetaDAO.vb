﻿#Region "Author"
'Class created with Luna 3.5.0.19
'Author: Diego Lunadei
'Date: 08/04/2014
#End Region

Imports System
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Data
Imports System.Data.SqlClient

Partial Public Class TipoTarjeta
    Inherits LUNA.LunaBaseClassEntity
    '******IMPORTANT: Don't write your code here. Write your code in the Class object that use this Partial Class.
    '******So you can replace DAOClass and EntityClass without lost your code

    Public Sub New()
        Tipo = "C" ''CREDITO
    End Sub

#Region "Database Field Map"

    Protected _ID As Byte = Nothing

    <XmlElementAttribute("ID")> _
    Public Property ID() As Byte
        Get
            Return _ID
        End Get
        Set(ByVal value As Byte)
            If _ID <> value Then
                IsChanged = True
                _ID = value
            End If
        End Set
    End Property

    Protected _Descripcion As String = ""

    <XmlElementAttribute("Descripcion")> _
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            If _Descripcion <> value Then
                IsChanged = True
                _Descripcion = value
            End If
        End Set
    End Property

    Protected _Tipo As String = ""

    <XmlElementAttribute("Tipo")> _
    Public Property Tipo() As String
        Get
            Return _Tipo
        End Get
        Set(ByVal value As String)
            If _Tipo <> value Then
                IsChanged = True
                _Tipo = value
            End If
        End Set
    End Property
#End Region

#Region "Method"
    ''' <summary>
    '''This method read an TipoTarjeta from DB.
    ''' </summary>
    ''' <returns>
    '''Return 0 if all ok, 1 if error
    ''' </returns>
    Public Overridable Function Read(ByVal Id As Integer) As Integer
        'Return 0 if all ok
        Dim Ris As Integer = 0
        Try
            Dim Mgr As New TipoTarjetaDAO
            Dim int As TipoTarjeta = Mgr.Read(Id)
            _ID = int.ID
            _Descripcion = int.Descripcion
            _Tipo = int.Tipo
            Mgr.Dispose()
        Catch ex As Exception
            ManageError(ex)
            Ris = 1
        End Try
        Return Ris
    End Function

    ''' <summary>
    '''This method save an TipoTarjeta on DB.
    ''' </summary>
    ''' <returns>
    '''Return Id insert in DB if all ok, 0 if error
    ''' </returns>
    Public Overridable Function Save() As Integer
        'Return the id Inserted
        Dim Ris As Integer = 0
        Try
            Dim Mgr As New TipoTarjetaDAO
            Ris = Mgr.Save(Me)
            Mgr.Dispose()
        Catch ex As Exception
            ManageError(ex)
        End Try
        Return Ris
    End Function

    Private Function InternalIsValid() As Boolean
        Dim Ris As Boolean = True
        If _Descripcion.Length = 0 Then Ris = False
        If _Descripcion.Length > 50 Then Ris = False
        If _Tipo.Length = 0 Then Ris = False
        If _Tipo.Length > 1 Then Ris = False
        Return Ris
    End Function

#End Region

#Region "Embedded Class"

#End Region

End Class

''' <summary>
'''This class manage persistency on db of TipoTarjeta object
''' </summary>
''' <remarks>
'''
''' </remarks>
Partial Public Class TipoTarjetaDAO
    Inherits LUNA.LunaBaseClassDAO(Of TipoTarjeta)

    ''' <summary>
    '''New() create an istance of this class. Use default DB Connection
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Sub New()
        MyBase.New(VGCadenaConexion)
    End Sub

    ''' <summary>
    '''New() create an istance of this class and specify an OPENED DB connection
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Sub New(ByVal Connection As Data.SqlClient.SqlConnection)
        MyBase.New(Connection)
    End Sub

    ''' <summary>
    '''New() create an istance of this class and specify a DB connectionstring
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Sub New(ByVal ConnectionString As String)
        MyBase.New(ConnectionString)
    End Sub

    ''' <summary>
    '''Read from DB table TipoTarjeta
    ''' </summary>
    ''' <returns>
    '''Return an TipoTarjeta object
    ''' </returns>
    Public Overrides Function Read(ByVal Id As Integer) As TipoTarjeta
        Dim cls As New TipoTarjeta

        Try
            Dim myCommand As SqlCommand = _cn.CreateCommand()
            myCommand.CommandText = "SELECT * FROM TipoTarjeta where ID = " & Id
            If Not DbTransaction Is Nothing Then myCommand.Transaction = DbTransaction
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            myReader.Read()
            If myReader.HasRows Then
                cls.ID = myReader("ID")
                cls.Descripcion = myReader("Descripcion")
                cls.Tipo = myReader("Tipo")
            End If
            myReader.Close()
            myCommand.Dispose()

        Catch ex As Exception
            ManageError(ex)
        End Try
        Return cls
    End Function

    ''' <summary>
    '''Save on DB table TipoTarjeta
    ''' </summary>
    ''' <returns>
    '''Return ID insert in DB
    ''' </returns>
    Public Overrides Function Save(ByRef cls As TipoTarjeta) As Integer

        Dim Ris As Integer = 0 'in Ris return Insert Id
        Try
            If cls.IsValid Then
                If cls.IsChanged Then
                    Dim DbCommand As SqlCommand = New SqlCommand()
                    Try
                        Dim sql As String
                        DbCommand.Connection = _cn
                        If Not DbTransaction Is Nothing Then DbCommand.Transaction = DbTransaction
                        If cls.IsNew Then
                            sql = "INSERT INTO TipoTarjeta ("
                            sql &= "ID,"
                            sql &= "Descripcion,"
                            sql &= "Tipo"
                            sql &= ") VALUES ("
                            sql &= "@ID,"
                            sql &= "@Descripcion,"
                            sql &= "@Tipo"
                            sql &= ")"
                        Else
                            sql = "UPDATE TipoTarjeta SET "
                            'sql &= "ID = @ID,"
                            sql &= "Descripcion = @Descripcion,"
                            sql &= "Tipo = @Tipo"
                            sql &= " WHERE ID= " & cls.ID
                        End If
                        DbCommand.Parameters.Add(New SqlClient.SqlParameter("@ID", cls.ID))
                        DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Descripcion", cls.Descripcion))
                        DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Tipo", cls.Tipo))
                        DbCommand.CommandType = CommandType.Text
                        DbCommand.CommandText = sql
                        DbCommand.ExecuteNonQuery()

                        Ris = cls.ID
                        DbCommand.Dispose()

                    Catch ex As Exception
                        ManageError(ex)
                    End Try
                Else
                    Ris = cls.ID
                End If

            Else
                'Err.Raise(602, "Object data is not valid")
                MessageBox.Show("Verificar datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End If
        Catch ex As Exception

        End Try
        
        Return Ris
    End Function

    Private Sub DestroyPermanently(ByVal Id As Integer)
        Try

            Dim UpdateCommand As SqlCommand = New SqlCommand()
            UpdateCommand.Connection = _cn

            '******IMPORTANT: You can use this commented instruction to make a logical delete .
            '******Replace DELETED Field with your logic deleted field name.
            'Dim Sql As String = "UPDATE TipoTarjeta SET DELETED=True "
            Dim Sql As String = "DELETE FROM TipoTarjeta"
            Sql &= " Where ID = " & Id

            UpdateCommand.CommandText = Sql
            If Not DbTransaction Is Nothing Then UpdateCommand.Transaction = DbTransaction
            UpdateCommand.ExecuteNonQuery()
            UpdateCommand.Dispose()
        Catch ex As Exception
            ManageError(ex)
        End Try
    End Sub

    ''' <summary>
    '''Delete from DB table TipoTarjeta. Accept id of object to delete.
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Overrides Sub Delete(ByVal Id As Integer)

        DestroyPermanently(Id)

    End Sub

    ''' <summary>
    '''Delete from DB table TipoTarjeta. Accept object to delete and optional a List to remove the object from.
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Overrides Sub Delete(ByRef obj As TipoTarjeta, Optional ByRef ListaObj As List(Of TipoTarjeta) = Nothing)

        DestroyPermanently(obj.ID)
        If Not ListaObj Is Nothing Then ListaObj.Remove(obj)

    End Sub

    Public Overloads Function Find(ByVal OrderBy As String, ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of TipoTarjeta)
        Dim So As New LUNA.LunaSearchOption With {.OrderBy = OrderBy}
        Return FindReal(So, Parameter)
    End Function

    Public Overloads Function Find(ByVal Top As Integer, ByVal OrderBy As String, ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of TipoTarjeta)
        Dim So As New LUNA.LunaSearchOption With {.Top = Top, .OrderBy = OrderBy}
        Return FindReal(So, Parameter)
    End Function

    Public Overrides Function Find(ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of TipoTarjeta)
        Dim So As New LUNA.LunaSearchOption
        Return FindReal(So, Parameter)
    End Function

    Public Overloads Function Find(ByVal SearchOption As LUNA.LunaSearchOption, ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of TipoTarjeta)
        Return FindReal(SearchOption, Parameter)
    End Function

    Private Function FindReal(ByVal SearchOption As LUNA.LunaSearchOption, ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of TipoTarjeta)
        Dim Ls As New List(Of TipoTarjeta)
        Try

            Dim sql As String = ""
            sql = "SELECT " & IIf(SearchOption.Top, " TOP " & SearchOption.Top, "") & " * "
            sql &= " from TipoTarjeta"
            For Each Par As LUNA.LunaSearchParameter In Parameter
                If Not Par Is Nothing Then
                    If sql.IndexOf("WHERE") = -1 Then sql &= " WHERE " Else sql &= " " & Par.LogicOperatorStr & " "
                    sql &= Par.FieldName & " " & Par.SqlOperator & " " & Ap(Par.Value)
                End If
            Next

            If SearchOption.OrderBy.Length Then sql &= " ORDER BY " & SearchOption.OrderBy

            Ls = GetData(sql)

        Catch ex As Exception
            ManageError(ex)
        End Try
        Return Ls
    End Function

    Public Overrides Function GetAll(Optional ByVal OrderByField As String = "", Optional ByVal AddEmptyItem As Boolean = False) As IEnumerable(Of TipoTarjeta)
        Dim Ls As New List(Of TipoTarjeta)
        Try

            Dim sql As String = ""
            sql = "SELECT ID," & _
             "Descripcion," & _
             "Tipo"
            sql &= " from TipoTarjeta"
            If OrderByField.Length Then
                sql &= " ORDER BY " & OrderByField
            End If

            Ls = GetData(sql, AddEmptyItem)

        Catch ex As Exception
            ManageError(ex)
        End Try
        Return Ls
    End Function
    Private Function GetData(ByVal sql As String, Optional ByVal AddEmptyItem As Boolean = False) As IEnumerable(Of TipoTarjeta)
        Dim Ls As New List(Of TipoTarjeta)
        Try
            Dim myCommand As SqlCommand = _cn.CreateCommand()
            myCommand.CommandText = sql
            If Not DbTransaction Is Nothing Then myCommand.Transaction = DbTransaction
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            If AddEmptyItem Then Ls.Add(New TipoTarjeta() With {.ID = Nothing, .Descripcion = "", .Tipo = ""})
            While myReader.Read
                Dim classe As New TipoTarjeta
                If Not myReader("ID") Is DBNull.Value Then classe.ID = myReader("ID")
                If Not myReader("Descripcion") Is DBNull.Value Then classe.Descripcion = myReader("Descripcion")
                If Not myReader("Tipo") Is DBNull.Value Then classe.Tipo = myReader("Tipo")
                Ls.Add(classe)
            End While
            myReader.Close()
            myCommand.Dispose()

        Catch ex As Exception
            ManageError(ex)
        End Try
        Return Ls
    End Function
End Class



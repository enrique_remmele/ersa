﻿#Region "Author"
'Class created with Luna 3.5.0.19
'Author: Diego Lunadei
'Date: 08/04/2014
#End Region

''' <summary>
'''Entity Class for table Tipotarjeta
''' </summary>
''' <remarks>
'''Write your custom method and property here
''' </remarks>
Public Class TipoTarjeta

#Region "Logic Field"

#End Region

#Region "Method"

    Public Overrides Function IsValid() As Boolean
        'RETURN TRUE IF THE OBJECT IS READY FOR SAVE
        'RETURN FALSE IF LOGIC CONTROL FAIL
        'INTERNALISVALID FUNCTION MADE SIMPLE DB CONTROL
        Dim Ris As Boolean = InternalIsValid
        'PUT YOUR LOGIC VALIDATION CODE HERE
        Return Ris
    End Function

#End Region

End Class

''' <summary>
'''DAO Class for table Tipotarjeta
''' </summary>
''' <remarks>
'''Write your DATABASE custom method here
''' </remarks>
Public Class TipoTarjetaDAO

End Class


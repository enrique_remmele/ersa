﻿#Region "Author"
'Class created with Luna 3.5.0.19
'Author: JGR
'Date: 09/04/2014
#End Region

Imports System
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb

Partial Public Class FormaPagoTarjeta
    Inherits LUNA.LunaBaseClassEntity
    '******IMPORTANT: Don't write your code here. Write your code in the Class object that use this Partial Class.
    '******So you can replace DAOClass and EntityClass without lost your code

    Public Sub New()

    End Sub

#Region "Database Field Map"

    Protected _IDTransaccion As Decimal = 0

    <XmlElementAttribute("IDTransaccion")> _
    Public Property IDTransaccion() As Decimal
        Get
            Return _IDTransaccion
        End Get
        Set(ByVal value As Decimal)
            If _IDTransaccion <> value Then
                IsChanged = True
                _IDTransaccion = value
            End If
        End Set
    End Property

    Protected _ID As Byte = Nothing

    <XmlElementAttribute("ID")> _
    Public Property ID() As Byte
        Get
            Return _ID
        End Get
        Set(ByVal value As Byte)
            If _ID <> value Then
                IsChanged = True
                _ID = value
            End If
        End Set
    End Property

    Protected _IDSucursal As Byte = Nothing

    <XmlElementAttribute("IDSucursal")> _
    Public Property IDSucursal() As Byte
        Get
            Return _IDSucursal
        End Get
        Set(ByVal value As Byte)
            If _IDSucursal <> value Then
                IsChanged = True
                _IDSucursal = value
            End If
        End Set
    End Property

    Protected _IDTipoComprobante As Integer = 0

    <XmlElementAttribute("IDTipoComprobante")> _
    Public Property IDTipoComprobante() As Integer
        Get
            Return _IDTipoComprobante
        End Get
        Set(ByVal value As Integer)
            If _IDTipoComprobante <> value Then
                IsChanged = True
                _IDTipoComprobante = value
            End If
        End Set
    End Property

    Protected _Comprobante As String = ""

    <XmlElementAttribute("Comprobante")> _
    Public Property Comprobante() As String
        Get
            Return _Comprobante
        End Get
        Set(ByVal value As String)
            If _Comprobante <> value Then
                IsChanged = True
                _Comprobante = value
            End If
        End Set
    End Property

    Protected _Fecha As DateTime = Nothing

    <XmlElementAttribute("Fecha")> _
    Public Property Fecha() As DateTime
        Get
            Return _Fecha
        End Get
        Set(ByVal value As DateTime)
            If _Fecha <> value Then
                IsChanged = True
                _Fecha = value
            End If
        End Set
    End Property

    Protected _IDMoneda As Byte = Nothing

    <XmlElementAttribute("IDMoneda")> _
    Public Property IDMoneda() As Byte
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As Byte)
            If _IDMoneda <> value Then
                IsChanged = True
                _IDMoneda = value
            End If
        End Set
    End Property

    Protected _ImporteMoneda As Decimal = 0

    <XmlElementAttribute("ImporteMoneda")> _
    Public Property ImporteMoneda() As Decimal
        Get
            Return _ImporteMoneda
        End Get
        Set(ByVal value As Decimal)
            If _ImporteMoneda <> value Then
                IsChanged = True
                _ImporteMoneda = value
            End If
        End Set
    End Property

    Protected _Cotizacion As Decimal = 0

    <XmlElementAttribute("Cotizacion")> _
    Public Property Cotizacion() As Decimal
        Get
            Return _Cotizacion
        End Get
        Set(ByVal value As Decimal)
            If _Cotizacion <> value Then
                IsChanged = True
                _Cotizacion = value
            End If
        End Set
    End Property

    Protected _Total As Decimal = 0

    <XmlElementAttribute("Total")> _
    Public Property Total() As Decimal
        Get
            Return _Total
        End Get
        Set(ByVal value As Decimal)
            If _Total <> value Then
                IsChanged = True
                _Total = value
            End If
        End Set
    End Property

    Protected _Saldo As Decimal = 0

    <XmlElementAttribute("Saldo")> _
    Public Property Saldo() As Decimal
        Get
            Return _Saldo
        End Get
        Set(ByVal value As Decimal)
            If _Saldo <> value Then
                IsChanged = True
                _Saldo = value
            End If
        End Set
    End Property

    Protected _Observacion As String = ""

    <XmlElementAttribute("Observacion")> _
    Public Property Observacion() As String
        Get
            Return _Observacion
        End Get
        Set(ByVal value As String)
            If _Observacion <> value Then
                IsChanged = True
                _Observacion = value
            End If
        End Set
    End Property

    Protected _Cancelado As Boolean = False

    <XmlElementAttribute("Cancelado")> _
    Public Property Cancelado() As Boolean
        Get
            Return _Cancelado
        End Get
        Set(ByVal value As Boolean)
            If _Cancelado <> value Then
                IsChanged = True
                _Cancelado = value
            End If
        End Set
    End Property

    Protected _TerminacionTarjeta As String = ""

    <XmlElementAttribute("TerminacionTarjeta")> _
    Public Property TerminacionTarjeta() As String
        Get
            Return _TerminacionTarjeta
        End Get
        Set(ByVal value As String)
            If _TerminacionTarjeta <> value Then
                IsChanged = True
                _TerminacionTarjeta = value
            End If
        End Set
    End Property

    Protected _FechaVencimientoTarjeta As DateTime = Nothing

    <XmlElementAttribute("FechaVencimientoTarjeta")> _
    Public Property FechaVencimientoTarjeta() As DateTime
        Get
            Return _FechaVencimientoTarjeta
        End Get
        Set(ByVal value As DateTime)
            If _FechaVencimientoTarjeta <> value Then
                IsChanged = True
                _FechaVencimientoTarjeta = value
            End If
        End Set
    End Property

    Protected _IDTipoTarjeta As Byte = Nothing

    <XmlElementAttribute("IDTipoTarjeta")> _
    Public Property IDTipoTarjeta() As Byte
        Get
            Return _IDTipoTarjeta
        End Get
        Set(ByVal value As Byte)
            If _IDTipoTarjeta <> value Then
                IsChanged = True
                _IDTipoTarjeta = value
            End If
        End Set
    End Property
#End Region

#Region "Method"
    ''' <summary>
    '''This method read an FormaPagoTarjeta from DB.
    ''' </summary>
    ''' <returns>
    '''Return 0 if all ok, 1 if error
    ''' </returns>
    Public Overridable Function Read(ByVal Id As Integer) As Integer
        'Return 0 if all ok
        Dim Ris As Integer = 0
        Try
            Dim Mgr As New FormaPagoTarjetaDAO
            Dim int As FormaPagoTarjeta = Mgr.Read(Id)
            _IDTransaccion = int.IDTransaccion
            _ID = int.ID
            _IDSucursal = int.IDSucursal
            _IDTipoComprobante = int.IDTipoComprobante
            _Comprobante = int.Comprobante
            _Fecha = int.Fecha
            _IDMoneda = int.IDMoneda
            _ImporteMoneda = int.ImporteMoneda
            _Cotizacion = int.Cotizacion
            _Total = int.Total
            _Saldo = int.Saldo
            _Observacion = int.Observacion
            _Cancelado = int.Cancelado
            _TerminacionTarjeta = int.TerminacionTarjeta
            _FechaVencimientoTarjeta = int.FechaVencimientoTarjeta
            _IDTipoTarjeta = int.IDTipoTarjeta
            Mgr.Dispose()
        Catch ex As Exception
            ManageError(ex)
            Ris = 1
        End Try
        Return Ris
    End Function

    ''' <summary>
    '''This method save an FormaPagoTarjeta on DB.
    ''' </summary>
    ''' <returns>
    '''Return Id insert in DB if all ok, 0 if error
    ''' </returns>
    Public Overridable Function Save() As Integer
        'Return the id Inserted
        Dim Ris As Integer = 0
        Try
            Dim Mgr As New FormaPagoTarjetaDAO
            Ris = Mgr.Save(Me)
            Mgr.Dispose()
        Catch ex As Exception
            ManageError(ex)
        End Try
        Return Ris
    End Function

    Private Function InternalIsValid() As Boolean
        Dim Ris As Boolean = True
        If _Comprobante.Length = 0 Then Ris = False
        If _Comprobante.Length > 50 Then Ris = False
        If _Observacion.Length > 100 Then Ris = False
        If _TerminacionTarjeta.Length > 4 Then Ris = False
        Return Ris
    End Function

#End Region

#Region "Embedded Class"

#End Region

End Class

''' <summary>
'''This class manage persistency on db of Formapagotarjeta object
''' </summary>
''' <remarks>
'''
''' </remarks>
Partial Public Class FormaPagoTarjetaDAO
    Inherits LUNA.LunaBaseClassDAO(Of FormaPagoTarjeta)

    ''' <summary>
    '''New() create an istance of this class. Use default DB Connection
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Sub New()
        MyBase.New(VGCadenaConexion)
    End Sub

    ''' <summary>
    '''New() create an istance of this class and specify an OPENED DB connection
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Sub New(ByVal Connection As Data.SqlClient.SqlConnection)
        MyBase.New(Connection)
    End Sub

    ''' <summary>
    '''New() create an istance of this class and specify a DB connectionstring
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Sub New(ByVal ConnectionString As String)
        MyBase.New(ConnectionString)
    End Sub

    ''' <summary>
    '''Read from DB table Formapagotarjeta
    ''' </summary>
    ''' <returns>
    '''Return an FormaPagoTarjeta object
    ''' </returns>
    Public Overrides Function Read(ByVal Id As Integer) As FormaPagoTarjeta
        Dim cls As New FormaPagoTarjeta

        Try
            Dim myCommand As SqlCommand = _cn.CreateCommand()
            myCommand.CommandText = "SELECT * FROM Formapagotarjeta where IDTransaccion = " & Id
            If Not DbTransaction Is Nothing Then myCommand.Transaction = DbTransaction
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            myReader.Read()
            If myReader.HasRows Then
                cls.IDTransaccion = myReader("IDTransaccion")
                cls.ID = myReader("ID")
                cls.IDSucursal = myReader("IDSucursal")
                cls.IDTipoComprobante = myReader("IDTipoComprobante")
                cls.Comprobante = myReader("Comprobante")
                cls.Fecha = myReader("Fecha")
                cls.IDMoneda = myReader("IDMoneda")
                cls.ImporteMoneda = myReader("ImporteMoneda")
                cls.Cotizacion = myReader("Cotizacion")
                If Not myReader("Total") Is DBNull.Value Then
                    cls.Total = myReader("Total")
                End If
                If Not myReader("Saldo") Is DBNull.Value Then
                    cls.Saldo = myReader("Saldo")
                End If
                If Not myReader("Observacion") Is DBNull.Value Then
                    cls.Observacion = myReader("Observacion")
                End If
                If Not myReader("Cancelado") Is DBNull.Value Then
                    cls.Cancelado = myReader("Cancelado")
                End If
                If Not myReader("TerminacionTarjeta") Is DBNull.Value Then
                    cls.TerminacionTarjeta = myReader("TerminacionTarjeta")
                End If
                If Not myReader("FechaVencimientoTarjeta") Is DBNull.Value Then
                    cls.FechaVencimientoTarjeta = myReader("FechaVencimientoTarjeta")
                End If
                If Not myReader("IDTipoTarjeta") Is DBNull.Value Then
                    cls.IDTipoTarjeta = myReader("IDTipoTarjeta")
                End If
            End If
            myReader.Close()
            myCommand.Dispose()

        Catch ex As Exception
            ManageError(ex)
        End Try
        Return cls
    End Function

    ''' <summary>
    '''Save on DB table Formapagotarjeta
    ''' </summary>
    ''' <returns>
    '''Return ID insert in DB
    ''' </returns>
    Public Overrides Function Save(ByRef cls As FormaPagoTarjeta) As Integer

        Dim Ris As Integer = 0 'in Ris return Insert Id

        If cls.IsValid Then
            If cls.IsChanged Then
                Dim DbCommand As SqlCommand = New SqlCommand()
                Try
                    Dim sql As String
                    DbCommand.Connection = _cn
                    If Not DbTransaction Is Nothing Then DbCommand.Transaction = DbTransaction
                    If cls.IDTransaccion = 0 Then
                        sql = "INSERT INTO Formapagotarjeta ("
                        sql &= "IDTransaccion,"
                        sql &= "ID,"
                        sql &= "IDSucursal,"
                        sql &= "IDTipoComprobante,"
                        sql &= "Comprobante,"
                        sql &= "Fecha,"
                        sql &= "IDMoneda,"
                        sql &= "ImporteMoneda,"
                        sql &= "Cotizacion,"
                        sql &= "Total,"
                        sql &= "Saldo,"
                        sql &= "Observacion,"
                        sql &= "Cancelado,"
                        sql &= "TerminacionTarjeta,"
                        sql &= "FechaVencimientoTarjeta,"
                        sql &= "IDTipoTarjeta"
                        sql &= ") VALUES ("
                        sql &= "@IDTransaccion,"
                        sql &= "@ID,"
                        sql &= "@IDSucursal,"
                        sql &= "@IDTipoComprobante,"
                        sql &= "@Comprobante,"
                        sql &= "@Fecha,"
                        sql &= "@IDMoneda,"
                        sql &= "@ImporteMoneda,"
                        sql &= "@Cotizacion,"
                        sql &= "@Total,"
                        sql &= "@Saldo,"
                        sql &= "@Observacion,"
                        sql &= "@Cancelado,"
                        sql &= "@TerminacionTarjeta,"
                        sql &= "@FechaVencimientoTarjeta,"
                        sql &= "@IDTipoTarjeta"
                        sql &= ")"
                    Else
                        sql = "UPDATE Formapagotarjeta SET "
                        sql &= "IDTransaccion = @IDTransaccion,"
                        sql &= "ID = @ID,"
                        sql &= "IDSucursal = @IDSucursal,"
                        sql &= "IDTipoComprobante = @IDTipoComprobante,"
                        sql &= "Comprobante = @Comprobante,"
                        sql &= "Fecha = @Fecha,"
                        sql &= "IDMoneda = @IDMoneda,"
                        sql &= "ImporteMoneda = @ImporteMoneda,"
                        sql &= "Cotizacion = @Cotizacion,"
                        sql &= "Total = @Total,"
                        sql &= "Saldo = @Saldo,"
                        sql &= "Observacion = @Observacion,"
                        sql &= "Cancelado = @Cancelado,"
                        sql &= "TerminacionTarjeta = @TerminacionTarjeta,"
                        sql &= "FechaVencimientoTarjeta = @FechaVencimientoTarjeta,"
                        sql &= "IDTipoTarjeta = @IDTipoTarjeta"
                        sql &= " WHERE IDTransaccion= " & cls.IDTransaccion
                    End If
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@IDTransaccion", cls.IDTransaccion))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@ID", cls.ID))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@IDSucursal", cls.IDSucursal))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@IDTipoComprobante", cls.IDTipoComprobante))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Comprobante", cls.Comprobante))
                    If cls.Fecha <> Date.MinValue Then
                        Dim DataPar As New SqlClient.SqlParameter("@Fecha", OleDbType.Date)
                        DataPar.Value = cls.Fecha
                        DbCommand.Parameters.Add(DataPar)
                    Else
                        DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Fecha", DBNull.Value))
                    End If
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@IDMoneda", cls.IDMoneda))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@ImporteMoneda", cls.ImporteMoneda))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Cotizacion", cls.Cotizacion))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Total", cls.Total))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Saldo", cls.Saldo))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Observacion", cls.Observacion))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@Cancelado", cls.Cancelado))
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@TerminacionTarjeta", cls.TerminacionTarjeta))
                    If cls.FechaVencimientoTarjeta <> Date.MinValue Then
                        Dim DataPar As New SqlClient.SqlParameter("@FechaVencimientoTarjeta", OleDbType.Date)
                        DataPar.Value = cls.FechaVencimientoTarjeta
                        DbCommand.Parameters.Add(DataPar)
                    Else
                        DbCommand.Parameters.Add(New SqlClient.SqlParameter("@FechaVencimientoTarjeta", DBNull.Value))
                    End If
                    DbCommand.Parameters.Add(New SqlClient.SqlParameter("@IDTipoTarjeta", cls.IDTipoTarjeta))
                    DbCommand.CommandType = CommandType.Text
                    DbCommand.CommandText = sql
                    DbCommand.ExecuteNonQuery()

                    Ris = cls.IDTransaccion
                    DbCommand.Dispose()

                Catch ex As Exception
                    ManageError(ex)
                End Try
            Else
                Ris = cls.IDTransaccion
            End If

        Else
            Err.Raise(602, "Object data is not valid")
        End If
        Return Ris
    End Function

    Private Sub DestroyPermanently(ByVal Id As Integer)
        Try

            Dim UpdateCommand As SqlCommand = New SqlCommand()
            UpdateCommand.Connection = _cn

            '******IMPORTANT: You can use this commented instruction to make a logical delete .
            '******Replace DELETED Field with your logic deleted field name.
            'Dim Sql As String = "UPDATE Formapagotarjeta SET DELETED=True "
            Dim Sql As String = "DELETE FROM Formapagotarjeta"
            Sql &= " Where IDTransaccion = " & Id

            UpdateCommand.CommandText = Sql
            If Not DbTransaction Is Nothing Then UpdateCommand.Transaction = DbTransaction
            UpdateCommand.ExecuteNonQuery()
            UpdateCommand.Dispose()
        Catch ex As Exception
            ManageError(ex)
        End Try
    End Sub

    ''' <summary>
    '''Delete from DB table Formapagotarjeta. Accept id of object to delete.
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Overrides Sub Delete(ByVal Id As Integer)

        DestroyPermanently(Id)

    End Sub

    ''' <summary>
    '''Delete from DB table Formapagotarjeta. Accept object to delete and optional a List to remove the object from.
    ''' </summary>
    ''' <returns>
    '''
    ''' </returns>
    Public Overrides Sub Delete(ByRef obj As FormaPagoTarjeta, Optional ByRef ListaObj As List(Of FormaPagoTarjeta) = Nothing)

        DestroyPermanently(obj.IDTransaccion)
        If Not ListaObj Is Nothing Then ListaObj.Remove(obj)

    End Sub

    Public Overloads Function Find(ByVal OrderBy As String, ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of FormaPagoTarjeta)
        Dim So As New LUNA.LunaSearchOption With {.OrderBy = OrderBy}
        Return FindReal(So, Parameter)
    End Function

    Public Overloads Function Find(ByVal Top As Integer, ByVal OrderBy As String, ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of FormaPagoTarjeta)
        Dim So As New LUNA.LunaSearchOption With {.Top = Top, .OrderBy = OrderBy}
        Return FindReal(So, Parameter)
    End Function

    Public Overrides Function Find(ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of FormaPagoTarjeta)
        Dim So As New LUNA.LunaSearchOption
        Return FindReal(So, Parameter)
    End Function

    Public Overloads Function Find(ByVal SearchOption As LUNA.LunaSearchOption, ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of FormaPagoTarjeta)
        Return FindReal(SearchOption, Parameter)
    End Function

    Private Function FindReal(ByVal SearchOption As LUNA.LunaSearchOption, ByVal ParamArray Parameter() As LUNA.LunaSearchParameter) As IEnumerable(Of FormaPagoTarjeta)
        Dim Ls As New List(Of FormaPagoTarjeta)
        Try

            Dim sql As String = ""
            sql = "SELECT " & IIf(SearchOption.Top, " TOP " & SearchOption.Top, "") & " * "
            sql &= " from Formapagotarjeta"
            For Each Par As LUNA.LunaSearchParameter In Parameter
                If Not Par Is Nothing Then
                    If sql.IndexOf("WHERE") = -1 Then sql &= " WHERE " Else sql &= " " & Par.LogicOperatorStr & " "
                    sql &= Par.FieldName & " " & Par.SqlOperator & " " & Ap(Par.Value)
                End If
            Next

            If SearchOption.OrderBy.Length Then sql &= " ORDER BY " & SearchOption.OrderBy

            Ls = GetData(sql)

        Catch ex As Exception
            ManageError(ex)
        End Try
        Return Ls
    End Function

    Public Overrides Function GetAll(Optional ByVal OrderByField As String = "", Optional ByVal AddEmptyItem As Boolean = False) As IEnumerable(Of FormaPagoTarjeta)
        Dim Ls As New List(Of FormaPagoTarjeta)
        Try

            Dim sql As String = ""
            sql = "SELECT IDTransaccion," & _
             "ID," & _
             "IDSucursal," & _
             "IDTipoComprobante," & _
             "Comprobante," & _
             "Fecha," & _
             "IDMoneda," & _
             "ImporteMoneda," & _
             "Cotizacion," & _
             "Total," & _
             "Saldo," & _
             "Observacion," & _
             "Cancelado," & _
             "TerminacionTarjeta," & _
             "FechaVencimientoTarjeta," & _
             "IDTipoTarjeta"
            sql &= " from Formapagotarjeta"
            If OrderByField.Length Then
                sql &= " ORDER BY " & OrderByField
            End If

            Ls = GetData(sql, AddEmptyItem)

        Catch ex As Exception
            ManageError(ex)
        End Try
        Return Ls
    End Function
    Private Function GetData(ByVal sql As String, Optional ByVal AddEmptyItem As Boolean = False) As IEnumerable(Of FormaPagoTarjeta)
        Dim Ls As New List(Of FormaPagoTarjeta)
        Try
            Dim myCommand As SqlCommand = _cn.CreateCommand()
            myCommand.CommandText = sql
            If Not DbTransaction Is Nothing Then myCommand.Transaction = DbTransaction
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            If AddEmptyItem Then Ls.Add(New FormaPagoTarjeta() With {.IDTransaccion = 0, .ID = Nothing, .IDSucursal = Nothing, .IDTipoComprobante = 0, .Comprobante = "", .Fecha = Nothing, .IDMoneda = Nothing, .ImporteMoneda = 0, .Cotizacion = 0, .Total = 0, .Saldo = 0, .Observacion = "", .Cancelado = False, .TerminacionTarjeta = "", .FechaVencimientoTarjeta = Nothing, .IDTipoTarjeta = Nothing})
            While myReader.Read
                Dim classe As New FormaPagoTarjeta
                If Not myReader("IDTransaccion") Is DBNull.Value Then classe.IDTransaccion = myReader("IDTransaccion")
                If Not myReader("ID") Is DBNull.Value Then classe.ID = myReader("ID")
                If Not myReader("IDSucursal") Is DBNull.Value Then classe.IDSucursal = myReader("IDSucursal")
                If Not myReader("IDTipoComprobante") Is DBNull.Value Then classe.IDTipoComprobante = myReader("IDTipoComprobante")
                If Not myReader("Comprobante") Is DBNull.Value Then classe.Comprobante = myReader("Comprobante")
                If Not myReader("Fecha") Is DBNull.Value Then classe.Fecha = myReader("Fecha")
                If Not myReader("IDMoneda") Is DBNull.Value Then classe.IDMoneda = myReader("IDMoneda")
                If Not myReader("ImporteMoneda") Is DBNull.Value Then classe.ImporteMoneda = myReader("ImporteMoneda")
                If Not myReader("Cotizacion") Is DBNull.Value Then classe.Cotizacion = myReader("Cotizacion")
                If Not myReader("Total") Is DBNull.Value Then classe.Total = myReader("Total")
                If Not myReader("Saldo") Is DBNull.Value Then classe.Saldo = myReader("Saldo")
                If Not myReader("Observacion") Is DBNull.Value Then classe.Observacion = myReader("Observacion")
                If Not myReader("Cancelado") Is DBNull.Value Then classe.Cancelado = myReader("Cancelado")
                If Not myReader("TerminacionTarjeta") Is DBNull.Value Then classe.TerminacionTarjeta = myReader("TerminacionTarjeta")
                If Not myReader("FechaVencimientoTarjeta") Is DBNull.Value Then classe.FechaVencimientoTarjeta = myReader("FechaVencimientoTarjeta")
                If Not myReader("IDTipoTarjeta") Is DBNull.Value Then classe.IDTipoTarjeta = myReader("IDTipoTarjeta")
                Ls.Add(classe)
            End While
            myReader.Close()
            myCommand.Dispose()

        Catch ex As Exception
            ManageError(ex)
        End Try
        Return Ls
    End Function
End Class



﻿Public Class ocxOrdenPagoDocumento

    'CLASES
    Dim CSistema As New CSistema
    Protected CData As New CData

    'EVENTOS
    Public Event ImporteModificado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemModificado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow)
    Public Event RegistroInsertado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow)
    Public Event RegistroEliminado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow)

    'PROPIEDADES
    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value

            btnDocumentos.Enabled = Not value
            btnEliminar.Enabled = Not value
        End Set
    End Property

    Private dtDocumentoValue As DataTable
    Public Property dtDocumento() As DataTable
        Get
            Return dtDocumentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDocumentoValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal

        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private TotalPagoValue As Decimal
    Public Property TotalPago() As Decimal

        Get
            Return TotalPagoValue
        End Get
        Set(ByVal value As Decimal)
            TotalPagoValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal

        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private TipoValue As String
    Public Property Tipo() As String
        Get
            Return TipoValue
        End Get
        Set(ByVal value As String)
            TipoValue = value
        End Set
    End Property

    Private FormValue As Form
    Public Property Form() As Form
        Get
            Return FormValue
        End Get
        Set(ByVal value As Form)
            FormValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
            SetMoneda()
        End Set
    End Property

    Private DecimalesValue As Integer
    Public Property Decimales() As Integer
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Integer)
            DecimalesValue = value
        End Set
    End Property

    Public Property SoloComprobanteProveedor As Boolean
    Public Property SolocomprobanteCliente As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        CargarInformacion()
        Reset()

    End Sub

    Sub CargarInformacion()

        'Crear estructura de Documentos
        dtDocumento = CSistema.ExecuteToDataTable("Select Top(0) * From VFormaPagoDocumento ").Clone
        dtDocumento.Columns("ImporteMoneda").SetOrdinal(dtDocumento.Columns.Count - 1)

    End Sub

    Sub Reset()

        dtDocumento.Rows.Clear()
        ListarFormaPago()

    End Sub

    Sub SeleccionarDocumento()

        Dim frm As New frmSeleccionFormaPagoDocumento
        frm.Fecha = Fecha
        frm.Saldo = Saldo
        frm.Comprobante = Comprobante
        frm.dt = dtDocumento.Clone
        frm.ID = dtDocumento.Rows.Count + 1
        frm.IDMoneda = IDMoneda
        frm.DecimalesOper = Decimales
        FGMostrarFormulario(Me.ParentForm, frm, "Documentos de Pago y/o Cobro", FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.dt.Rows.Count > 0 Then

            'Validar monedas
            If dtDocumento.Rows.Count > 0 Then
                If dtDocumento.Rows(0)("IDMoneda") <> frm.dt.Rows(0)("IDMoneda") Then

                    MessageBox.Show("No se puede agregar documentos con monedas diferentes!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Exit Sub

                End If

                If dtDocumento.Rows(0)("Cotizacion") <> frm.dt.Rows(0)("Cotizacion") Then

                    MessageBox.Show("No se puede agregar documentos con cotizacion diferente!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Exit Sub

                End If

            End If

            dtDocumento.ImportRow(frm.dt.Rows(0))
            ListarFormaPago()
            RaiseEvent RegistroInsertado(New Object, New EventArgs, frm.dt.Rows(0))

        End If

    End Sub

    Sub ListarFormaPago()

        dgw.DataSource = Nothing

        If dtDocumento Is Nothing Then
            Exit Sub
        End If
        If dtDocumento.Rows.Count() > 0 Then
            Decimales = IIf(CSistema.RetornarValorBoolean(CData.GetRow(" id = " & dtDocumento.Rows.Item(0).ItemArray(11), "vMoneda")("Decimales").ToString), 1, 0)
            txtTotalFormaPago.Decimales = Decimales
        End If
        CSistema.dtToGrid(dgw, dtDocumento)

        'Formato
        'Ocultar Todo
        For c As Integer = 0 To dgw.ColumnCount - 1
            dgw.Columns(c).Visible = False
        Next

        'Mostrar columnas para el usuario
        dgw.Columns("ID").Visible = True
        dgw.Columns("TipoComprobante").Visible = True
        dgw.Columns("Comprobante").Visible = True
        dgw.Columns("Fecha").Visible = True
        dgw.Columns("Moneda").Visible = True
        dgw.Columns("Cotizacion").Visible = True
        dgw.Columns("Observacion").Visible = True
        dgw.Columns("ImporteMoneda").Visible = True
        dgw.Columns("ImporteMoneda").HeaderText = "Importe"

        'Cambiamos los nombres de las columnas
        dgw.Columns("TipoComprobante").HeaderText = "Tipo"
        dgw.Columns("Moneda").HeaderText = "Mon."
        dgw.Columns("Cotizacion").HeaderText = "Coti."

        'Formato numeros
        CSistema.DataGridColumnasNumericas(dgw, {"ImporteMoneda"}, Decimales)
        dgw.Columns("Cotizacion").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgw.Columns("Cotizacion").DefaultCellStyle.Format = "N0"

        'Columna FILL
        dgw.Columns("Observacion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        CalcularTotales()

        RaiseEvent ImporteModificado(New Object, New EventArgs)

    End Sub

    Public Sub ListarFormaPago(ByVal IDTransaccion As Integer)

        dtDocumento = CSistema.ExecuteToDataTable("Select * From VFormaPagoDocumento Where IDTransaccion = " & IDTransaccion & " Order By ID ASC ").Copy

        ListarFormaPago()

    End Sub

    Sub EliminarFormaPago()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim ID As Integer = dgw.SelectedRows(0).Cells("ID").Value

        For Each oRow As DataRow In dtDocumento.Select(" ID=" & ID)

            dtDocumento.Rows.Remove(oRow)
            RaiseEvent RegistroEliminado(New Object, New EventArgs, oRow)
        Next

        ListarFormaPago()

    End Sub

    Sub CalcularTotales()

        Dim tmp As Decimal = 0

        Try
            'Sumar
            For Each oRow As DataRow In dtDocumento.Rows
                If CBool(oRow("Restar")) = True Then
                    tmp = tmp - CDec(oRow("ImporteMoneda").ToString)
                Else
                    tmp = tmp + CDec(oRow("ImporteMoneda").ToString)
                End If
            Next

        Catch ex As Exception

        End Try

        txtTotalFormaPago.SetValue(tmp.ToString())

        Total = tmp

    End Sub

    Sub ModificarFormaPago()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If dgw.SelectedRows(0).Cells(11).Value = 0 Then
            Exit Sub
        End If

        Dim mnuContextMenu As New ContextMenuStrip()
        mnuContextMenu.Items.Add("Modificar")
        dgw.ContextMenuStrip = mnuContextMenu

        AddHandler mnuContextMenu.Click, AddressOf SeleccionarDocumentoModificar

    End Sub

    Sub SeleccionarDocumentoModificar()

        Dim frm As New frmSeleccionModificarFormaPagoDocumento
        frm.Text = "Modificar Documentos de Pago y/o Cobro"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        Dim vIDTransaccion As Integer = dgw.SelectedRows(0).Cells(11).Value
        Dim vID As Integer = dgw.SelectedRows(0).Cells(0).Value
        Dim FormaPago As String = ""
        Dim Where As String = ""

        If vIDTransaccion = 0 Then
            Exit Sub
        End If

        dtDocumento = CSistema.ExecuteToDataTable("Select * From VFormaPagoOrdenPago Where IDTransaccion=" & vIDTransaccion & " And ID=" & vID)

        If dtDocumento.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtDocumento.Rows(0)

        frm.IDTransaccion = oRow("IDTransaccion").ToString
        frm.txtID.txt.Text = oRow("ID").ToString
        frm.ShowDialog(Me)

        ListarFormaPago(vIDTransaccion)

    End Sub

    Sub SetMoneda()

        Decimales = CSistema.MonedaDecimal(IDMoneda)
        ListarFormaPago()
        txtTotalFormaPago.Decimales = Decimales

    End Sub

    Public Function ObtenerImporte(Optional EnMonedaLocal As Boolean = True, Optional DocumentoResta As Boolean = False) As Decimal

        ObtenerImporte = 0

        'Validar
        If dtDocumento Is Nothing Then
            Return 0
        End If

        If EnMonedaLocal Then
            ObtenerImporte = CSistema.dtSumColumn(dtDocumento, "Importe", " Restar = '" & DocumentoResta & "' ")
        Else
            ObtenerImporte = CSistema.dtSumColumn(dtDocumento, "ImporteMoneda", " Restar = '" & DocumentoResta & "' ")
        End If

    End Function

    Private Sub btnDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocumentos.Click
        SeleccionarDocumento()
    End Sub

    Private Sub dgw_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgw.MouseDown
        ModificarFormaPago()
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        EliminarFormaPago()
    End Sub

End Class

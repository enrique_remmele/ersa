﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxOrdenPagoDocumento
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnDocumentos = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.lblTotalFormaPago = New System.Windows.Forms.Label()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.txtTotalFormaPago = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(601, 161)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'btnDocumentos
        '
        Me.btnDocumentos.Location = New System.Drawing.Point(3, 3)
        Me.btnDocumentos.Name = "btnDocumentos"
        Me.btnDocumentos.Size = New System.Drawing.Size(75, 23)
        Me.btnDocumentos.TabIndex = 0
        Me.btnDocumentos.Text = "Agregar"
        Me.btnDocumentos.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtTotalFormaPago)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblTotalFormaPago)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(303, 129)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(295, 29)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.btnDocumentos)
        Me.FlowLayoutPanel4.Controls.Add(Me.btnEliminar)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 129)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(294, 29)
        Me.FlowLayoutPanel4.TabIndex = 1
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgw, 2)
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 3)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.Size = New System.Drawing.Size(595, 120)
        Me.dgw.TabIndex = 0
        '
        'lblTotalFormaPago
        '
        Me.lblTotalFormaPago.Location = New System.Drawing.Point(121, 3)
        Me.lblTotalFormaPago.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.lblTotalFormaPago.Name = "lblTotalFormaPago"
        Me.lblTotalFormaPago.Size = New System.Drawing.Size(45, 20)
        Me.lblTotalFormaPago.TabIndex = 0
        Me.lblTotalFormaPago.Text = "Total:"
        Me.lblTotalFormaPago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(84, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 1
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'txtTotalFormaPago
        '
        Me.txtTotalFormaPago.Color = System.Drawing.Color.Empty
        Me.txtTotalFormaPago.Decimales = False
        Me.txtTotalFormaPago.Indicaciones = Nothing
        Me.txtTotalFormaPago.Location = New System.Drawing.Point(172, 3)
        Me.txtTotalFormaPago.Name = "txtTotalFormaPago"
        Me.txtTotalFormaPago.Size = New System.Drawing.Size(120, 20)
        Me.txtTotalFormaPago.SoloLectura = True
        Me.txtTotalFormaPago.TabIndex = 1
        Me.txtTotalFormaPago.TabStop = False
        Me.txtTotalFormaPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalFormaPago.Texto = "0"
        '
        'ocxOrdenPagoDocumento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxOrdenPagoDocumento"
        Me.Size = New System.Drawing.Size(601, 161)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnDocumentos As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents lblTotalFormaPago As System.Windows.Forms.Label
    Friend WithEvents txtTotalFormaPago As ERP.ocxTXTNumeric
    Friend WithEvents btnEliminar As System.Windows.Forms.Button

End Class

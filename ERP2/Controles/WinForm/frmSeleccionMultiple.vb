﻿Public Class frmSeleccionMultiple

    Public Property DataSource As String
    Public Property DataValueMember As String
    Public Property DataDisplayMember As String
    Public Property dtSeleccionado As DataTable

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData


    'VARIABLES
    Dim Titulo As String
    Dim TipoInforme As String = ""
    Dim dt As DataTable

    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        dt = CSistema.ExecuteToDataTable("Select 'Sel'=Convert(bit, 'False'), * from " & DataSource & " order by " & DataDisplayMember)
        dtSeleccionado = CSistema.ExecuteToDataTable("Select Top(0) 'Sel'=Convert(bit, 'False'), * from " & DataSource & " order by " & DataDisplayMember)

        Listar()

    End Sub

    Sub Listar()

        If dt Is Nothing Then
            Exit Sub
        End If

        CSistema.dtToGrid(dgv, dt)

        'Formato
        For i As Integer = 0 To dgv.Columns.Count - 1
            dgv.Columns(i).Visible = False
        Next

        dgv.Columns("Sel").Visible = True
        dgv.Columns(DataDisplayMember).Visible = True
        dgv.Columns(DataDisplayMember).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill


    End Sub

    Sub Marcar()

        If dgv.CurrentRow Is Nothing Then
            Exit Sub
        End If

        dtSeleccionado.Clear()

        For Each oRow As DataRow In dt.Rows
            If dgv.CurrentRow.Cells("ID").Value = oRow("ID") Then
                If dgv.CurrentRow.Cells("Sel").Value = False Then
                    oRow("Sel") = 1
                Else
                    oRow("Sel") = 0
                End If

            End If
        Next


        For Each dRow As DataRow In dt.Select("Sel= 1")

            Dim NewRow As DataRow = dtSeleccionado.NewRow

            For i As Integer = 0 To dtSeleccionado.Columns.Count - 1
                Dim NombreColumna As String = dtSeleccionado.Columns(i).ColumnName
                NewRow(NombreColumna) = dRow(NombreColumna)
            Next

            dtSeleccionado.Rows.Add(NewRow)

        Next


        Listar()

    End Sub

    Sub MarcarTodo(ByVal Valor As Boolean)

        dtSeleccionado.Clear()

        For Each oRow As DataRow In dt.Rows
            oRow("Sel") = Valor
        Next

        For Each dRow As DataRow In dt.Select("Sel= 1")

            Dim NewRow As DataRow = dtSeleccionado.NewRow

            For i As Integer = 0 To dtSeleccionado.Columns.Count - 1
                Dim NombreColumna As String = dtSeleccionado.Columns(i).ColumnName
                NewRow(NombreColumna) = dRow(NombreColumna)
            Next

            dtSeleccionado.Rows.Add(NewRow)

        Next


        Listar()

    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyUp

        If e.KeyCode = Keys.Space Then
            Marcar()
        End If


    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Me.Close()

    End Sub

    Private Sub dgw_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick
        Marcar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click

        dtSeleccionado.Clear()
        Me.Close()

    End Sub

    Private Sub frmSeleccionMultiple_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lklSeleccionarTodo_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklSeleccionarTodo.LinkClicked
        If lklSeleccionarTodo.Text = "Seleccionar Todo" Then
            lklSeleccionarTodo.Text = "Quitar Seleccion"
            MarcarTodo(True)
        Else
            lklSeleccionarTodo.Text = "Seleccionar Todo"
            MarcarTodo(False)
        End If
    End Sub
End Class
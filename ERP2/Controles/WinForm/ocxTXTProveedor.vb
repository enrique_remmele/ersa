﻿Public Class ocxTXTProveedor

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private RegistroValue As DataRow
    Public Property Registro() As DataRow
        Get
            Return RegistroValue
        End Get
        Set(ByVal value As DataRow)
            RegistroValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private AlturaMaximaValue As Integer
    Public Property AlturaMaxima() As Integer
        Get
            Return AlturaMaximaValue
        End Get
        Set(ByVal value As Integer)
            AlturaMaximaValue = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value

            txtID.SoloLectura = value
            txtRazonSocial.SoloLectura = value
            txtReferencia.SoloLectura = value
            txtRUC.SoloLectura = value

            Me.TabStop = Not value

        End Set
    End Property

    Private SucursalSeleccionadaValue As Boolean
    Public Property SucursalSeleccionada() As Boolean
        Get
            Return SucursalSeleccionadaValue
        End Get
        Set(ByVal value As Boolean)
            SucursalSeleccionadaValue = value
        End Set
    End Property

    Private SucursalValue As DataRow
    Public Property Sucursal() As DataRow

        Get
            Return SucursalValue
        End Get
        Set(ByVal value As DataRow
)
            SucursalValue = value
        End Set
    End Property

    Private ConsultaValue As String
    Public Property Consulta() As String
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As String)
            ConsultaValue = value
        End Set
    End Property

    Private frmValue As Form
    Public Property frm() As Form
        Get
            Return frmValue
        End Get
        Set(ByVal value As Form)
            frmValue = value
        End Set
    End Property

    'EVENTOS
    Public Event ItemSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemMalSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemInicializado(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES
    Private dt As New DataTable
    Private dtTemp As New DataTable
    Private BindingSource1 As New BindingSource
    Private vTabla As String = "VProveedor"
    Private vTablaSp As String = "SpViewProveedor"

    Sub Inicializar()

        'Controles
        txtReferencia.ResetText()
        txtRazonSocial.ResetText()
        txtID.ResetText()
        txtRUC.ResetText()
        SucursalSeleccionada = False

        txtRUC.Visible = True

        grid.DataSource = Nothing
        grid.Visible = False

        'Funciones
        Conectar()

    End Sub

    Public Sub Conectar()

        'Dim Consulta As String = "Select * From VProveedor Where Estado='True' Order By RazonSocial"
        'dt = CSistema.ExecuteToDataTable(Consulta)

        dt = CData.GetTable(vTabla)
        BindingSource1.DataSource = dt

    End Sub

    Public Sub Conectar(ByVal consulta As String)

        dt = CSistema.ExecuteToDataTable(consulta)
        BindingSource1.DataSource = dt

    End Sub

    Public Sub Conectar(ByVal dtTemp As DataTable)

        dt = dtTemp.Copy
        BindingSource1.DataSource = dt

    End Sub

    Private Sub Listar(ByVal ctr As TextBox, ByVal condicion As String)

        'Validar
        If ctr.Focused = False Then
            Exit Sub
        End If

        If ctr.Text = "" Then
            OcultarLista()
            Exit Sub
        End If

        Seleccionado = False

        ConfigurarTamaño()

        Dim Where As String = ""
        Dim Valor As String = ctr.Text.Trim

        Try

            Select Case UCase(condicion)

                Case "ID"

                    If IsNumeric(ctr.Text) = False Then
                        Exit Sub
                    End If

                    Where = " ID=" & ctr.Text

                Case "REFERENCIA"

                    Where = " Referencia = '" & ctr.Text.Trim & "' "

                Case "RAZONSOCIAL"

                    Where = " RazonSocial Like '%" & ctr.Text.Trim & "%' "

                Case "RUC"

                    Where = " RUC  Like '%" & ctr.Text.Trim & "%' "

                Case Else

            End Select


            BindingSource1.Filter = Where
            grid.DataSource = BindingSource1.DataSource

            grid.Visible = True
            If AlturaMaxima > Math.Round(grid.RowCount * 20, 0) Then
                grid.Height = Math.Round(grid.RowCount * 20, 0)
            Else
                grid.Height = AlturaMaxima
            End If

            grid.BringToFront()
            Me.Height = grid.Height + TableLayoutPanel1.Height

            'Ocultar todas las columnas
            For i As Integer = 0 To grid.ColumnCount - 1
                grid.Columns(i).Visible = False
            Next

            'Mostrar las Columnas
            grid.Columns("Referencia").DisplayIndex = 0
            grid.Columns("Referencia").Visible = True
            grid.Columns("RazonSocial").Visible = True
            grid.Columns("RUC").Visible = True

            'Tamaños
            If grid.Columns.Count > 0 Then
                grid.Columns("Referencia").Width = txtReferencia.Width + 6
                grid.Columns("RazonSocial").Width = txtRazonSocial.Width + 6
                grid.Columns("RUC").Width = txtRUC.Width + 6
            End If


            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.None
            'grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader

        Catch ex As Exception

        End Try

    End Sub

    Public Sub OcultarLista()

        grid.Visible = False
        Me.Height = TableLayoutPanel1.Height

    End Sub

    Public Sub LimpiarSeleccion()

        Seleccionado = False
        OcultarLista()
        txtID.txt.Clear()
        txtReferencia.txt.Clear()
        txtRazonSocial.txt.Clear()
        txtRUC.txt.Clear()
        RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        Registro = Nothing

    End Sub

    Public Sub Clear()

        txtID.txt.Clear()
        txtReferencia.txt.Clear()
        txtRazonSocial.txt.Clear()
        txtRUC.txt.Clear()

        LimpiarSeleccion()

    End Sub

    Private Sub Seleccionar(Optional ByVal GridFocus As Boolean = False)

        If SoloLectura = True Then
            Exit Sub
        End If

        'Validar
        If Seleccionado = False Then
            If grid.RowCount = 0 Then
                LimpiarSeleccion()
                RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
                Exit Sub
            End If
        End If

        'Si ya esta seleccionado
        If Seleccionado = True Then
            If txtRUC.txt.Focused = True Then
                Me.ParentForm.SelectNextControl(Me, True, True, True, True)
            Else
                Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
            End If

            Exit Sub

        End If

        'Si los controles estan vacios
        If txtReferencia.txt.Text = "" And txtRazonSocial.txt.Text = "" And txtRUC.txt.Text = "" Then
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
            Exit Sub
        End If

        If GridFocus = True Then
            Exit Sub
        End If

        SeleccionarRegistro()

    End Sub

    Private Sub SeleccionarGrid()

        'Validar
        If grid.RowCount = 0 Then
            LimpiarSeleccion()
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
            Exit Sub
        End If

        'Si ya esta seleccionado
        If Seleccionado = True Then
            Exit Sub
        End If

        SeleccionarRegistro()

    End Sub

    Sub SeleccionarRegistro(Optional ByVal IDCliente As Integer = 0)

        Try

            Dim ID As Integer

            If IDCliente = 0 Then
                ID = grid.SelectedRows(0).Cells(0).Value
            Else
                ID = IDCliente
            End If

            Dim oRow As DataRow = dt.Select(" ID=" & ID)(0)

            Registro = oRow
            txtID.txt.Text = oRow("ID").ToString
            txtReferencia.txt.Text = oRow("Referencia").ToString
            txtRazonSocial.txt.Text = oRow("RazonSocial").ToString
            txtRUC.txt.Text = oRow("RUC").ToString

            OcultarLista()
            Seleccionado = True
            RaiseEvent ItemSeleccionado(New Object, New EventArgs)

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try

    End Sub

    Private Sub ConfigurarTamaño()

        'Tañanos
        txtID.Width = TableLayoutPanel1.Width
        grid.Width = TableLayoutPanel1.Width - 3

        If grid.Visible = False Then
            Me.Height = TableLayoutPanel1.Height
        End If

        'Localizacion
        grid.Top = TableLayoutPanel1.Height - 4
        grid.Left = TableLayoutPanel1.Left + 3

    End Sub

    Public Sub SetValue(ByVal ID As Integer)

        Try

            If dt.Select(" ID=" & ID).Count = 0 Then
                Registro = Nothing
                txtID.txt.Text = ""
                txtReferencia.txt.Text = ""
                txtRazonSocial.txt.Text = ""
                txtRUC.txt.Text = ""

                OcultarLista()
                Seleccionado = False
                RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)

            Else

                Dim oRow As DataRow = dt.Select(" ID=" & ID)(0)

                Registro = oRow
                txtID.txt.Text = oRow("ID").ToString
                txtReferencia.txt.Text = oRow("Referencia").ToString
                txtRazonSocial.txt.Text = oRow("RazonSocial").ToString
                txtRUC.txt.Text = oRow("RUC").ToString

                OcultarLista()
                Seleccionado = True
                RaiseEvent ItemSeleccionado(New Object, New EventArgs)

            End If

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try

    End Sub

    Private Sub MostrarPropiedades()

        Dim ID As Integer
        Dim RazonSocial As String = ""

        If Seleccionado = False Then

            If grid.RowCount = 0 Then
                Exit Sub
            End If

            If grid.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            ID = grid.SelectedRows(0).Cells(0).Value
            RazonSocial = grid.SelectedRows(0).Cells(1).Value
        Else
            ID = Registro("ID").ToString
            RazonSocial = Registro("RazonSocial").ToString
        End If



        Dim frm As New frmPropiedad
        frm.TituloVentana = "Propiedades del Proveedor"
        frm.Titulo = RazonSocial
        frm.Consulta = "Select * From VProveedor Where ID=" & ID & " Order By ID"
        frm.ShowDialog()

    End Sub

    Private Sub MostrarConsulta()

        Dim frm As New frmProveedorBuscar
        frm.Consulta = Consulta
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.WindowState = FormWindowState.Maximized
        frm.ShowDialog()

        If frm.ID = 0 Then
            Exit Sub
        End If

        'Si no existe en el DATATABLE, agregar
        If CData.GetTable(vTabla).Select("ID=" & frm.ID).Count = 0 Then

            'Actualizamos el Registro
            CData.Insertar(frm.ID, vTabla)

            dt = CData.GetTable(vTabla)
            BindingSource1.DataSource = dt

        End If


        SeleccionarRegistro(frm.ID)

    End Sub

    Public Function IsFocus() As Boolean

        IsFocus = False

        If txtID.txt.Focused = True Then
            Return True
        End If

        If txtReferencia.txt.Focused = True Then
            Return True
        End If

        If txtRazonSocial.txt.Focused = True Then
            Return True
        End If

        If txtRUC.txt.Focused = True Then
            Return True
        End If

    End Function

    Public Sub SetFocus()
        txtID.txt.Focus()
        txtID.txt.SelectAll()
        If txtID.txt.Focused = False Then
            txtID.txt.Focus()
        End If

    End Sub

    Private Sub ManejoTecla(ByVal txt As TextBox, ByVal e As System.Windows.Forms.KeyEventArgs, ByVal Condicion As String)

        'Si es solo lectura, salir
        If SoloLectura = True Then
            GoTo pasar
        End If

        If e.KeyCode = Keys.Down Then
            If grid.RowCount > 0 Then
                grid.Focus()
                If grid.SelectedRows.Count = 0 Then
                    grid.Rows(0).Selected = True
                End If
            End If

            Exit Sub

        End If

        If e.KeyCode = Keys.Enter Then
            If txt.Text.Trim = "" Then
                If txtRUC.txt.Focused = True Then
                    Me.ParentForm.SelectNextControl(Me, True, True, True, True)
                Else
                    Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
                End If
            Else
                Seleccionar()
            End If
            Exit Sub
        End If

       

        If e.KeyCode = Keys.ShiftKey Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Tab Then
            If Seleccionado = False Then
                Seleccionar()
            End If
            Exit Sub
        End If

        If e.KeyCode = vgKeyConsultar Then
            MostrarConsulta()
            Exit Sub
        End If

        If txt.Text = "" Then
            LimpiarSeleccion()
            Exit Sub
        End If

        Listar(txt, Condicion)

pasar:
        If e.KeyCode = vgKeyVerInformacionDetallada Then
            MostrarPropiedades()
            Exit Sub
        End If

        If e.KeyCode = vgKeyActualizarTabla Then

            CData.ResetTable(vTabla, "Exec " & vTablaSp & " @IDUsuario = " & vgIDUsuario & ", @IDSucursal = " & vgIDSucursal & ", @IDDeposito = " & vgIDDeposito & ", @IDTerminal = " & vgIDTerminal & " ")
            dt = CData.GetTable(vTabla)
            BindingSource1.DataSource = dt

            Exit Sub

        End If

    End Sub

    Public Sub SetValueString(ByVal ID As Integer, ByVal RazonSocial As String, ByVal RUC As String, Optional ByVal Referencia As String = "")

        txtID.txt.Text = ID
        txtRazonSocial.txt.Text = RazonSocial
        txtRUC.txt.Text = RUC
        txtReferencia.txt.Text = RUC

    End Sub

    Private Sub ocxTXTProveedor_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        If txtReferencia.GetValue.ToString.Length = 0 Then
            Exit Sub
        End If

        Listar(txtReferencia.txt, "Referencia='" & txtReferencia.GetValue & "'")
    End Sub

    Private Sub ocxTXTProveedor_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Seleccionar(False)
    End Sub

    Private Sub ocxTXTProveedor_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        OcultarLista()
    End Sub

    Private Sub ocxTXTProveedor_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus
        OcultarLista()
    End Sub

    Private Sub grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            SeleccionarGrid()
        End If
    End Sub

    Private Sub ocxTXTProveedor_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        ConfigurarTamaño()
    End Sub

    Private Sub txtRazonSocial_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRazonSocial.KeyUp
        e.Handled = True
        ManejoTecla(txtRazonSocial.txt, e, "RAZONSOCIAL")
    End Sub

    Private Sub txtReferencia_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtReferencia.GotFocus
      
    End Sub

    Private Sub txtReferencia_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.KeyUp
        e.Handled = True
        ManejoTecla(txtReferencia.txt, e, "REFERENCIA")
    End Sub

    Private Sub txtRUC_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRUC.KeyUp
        e.Handled = True
        ManejoTecla(txtRUC.txt, e, "RUC")
    End Sub

    Private Sub grid_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyUp
        If e.KeyCode = vgKeyVerInformacionDetallada Then
            MostrarPropiedades()
        End If

        If e.KeyCode = vgKeyConsultar Then
            MostrarConsulta()
        End If
    End Sub

End Class

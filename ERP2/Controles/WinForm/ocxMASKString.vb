﻿Public Class ocxMASKString

    'EVENTOS
    Public Event TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Public Shadows Event KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Public Shadows Event TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Public Shadows Event DoubleClick(sender As System.Object, e As System.EventArgs)

    'PROPIEDADES
    Private TextAlignValue As System.Windows.Forms.HorizontalAlignment
    ''' <summary>
    ''' Indica como estara alineado el texto dentro del control
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TextAlign() As System.Windows.Forms.HorizontalAlignment
        Get
            Return TextAlignValue
        End Get
        Set(ByVal value As System.Windows.Forms.HorizontalAlignment)
            TextAlignValue = value
            txt.TextAlign = value
        End Set
    End Property

    Private TextoValue As String
    Public Property Texto() As String
        Get
            Return txt.Text
        End Get
        Set(ByVal value As String)
            TextoValue = value
            txt.Text = value
        End Set
    End Property


    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            txt.ReadOnly = value
            txt.TabStop = Not value
            Me.TabStop = Not value
            If value = True Then
                txt.BackColor = Color.FromArgb(vgColorSoloLectura)
            Else
                txt.BackColor = Color.FromArgb(vgColorBackColor)
            End If

        End Set
    End Property

    Private LockedValue As Boolean
    Public Property Locked() As Boolean
        Get
            Return LockedValue
        End Get
        Set(ByVal value As Boolean)
            LockedValue = value
            If value = False Then
                txt.ReadOnly = True
            Else
                txt.ReadOnly = False
            End If
        End Set
    End Property

    Private MultilineaValue As Boolean
    Public Property Multilinea() As Boolean
        Get
            Return MultilineaValue
        End Get
        Set(ByVal value As Boolean)
            MultilineaValue = value
            txt.Multiline = Multilinea
        End Set
    End Property

    Private IndicacionesValue As String
    ''' <summary>
    ''' Propiedad en donde se puede definir lo que hace el control.
    ''' Esto se mostrara al usuario.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Indicaciones() As String
        Get
            Return IndicacionesValue
        End Get
        Set(ByVal value As String)
            IndicacionesValue = value
            ToolTip1.SetToolTip(txt, value)
        End Set
    End Property

    Private ColorValue As System.Drawing.Color
    Public Property Color() As System.Drawing.Color
        Get
            Return ColorValue
        End Get
        Set(ByVal value As System.Drawing.Color)
            ColorValue = value
            txt.BackColor = Color
        End Set
    End Property

    Public Function GetValue() As String
        Return Texto
    End Function

    Public Sub Clear()
        txt.Clear()
    End Sub

    Public Sub SelectAll()
        txt.SelectAll()
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        TextAlign = HorizontalAlignment.Left
        Texto = ""
        Multilinea = False

    End Sub

    Public Sub SetValue(ByVal value As String)

        txt.Text = value

    End Sub

    Private Sub txt_DoubleClick(sender As Object, e As System.EventArgs)
        RaiseEvent DoubleClick(sender, e)
    End Sub

    Private Sub txt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        If txt.ReadOnly = False Then
            txt.BackColor = Color.FromArgb(vgColorFocus)
            'txt.Font = New Font(txt.Font.FontFamily.ToString, txt.Font.SizeInPoints, FontStyle.Bold)
        End If
    End Sub

    Private Sub txt_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If txt.ReadOnly = False Then
            txt.BackColor = Drawing.Color.White
            'txt.Font = New Font(txt.Font.FontFamily.ToString, txt.Font.SizeInPoints, FontStyle.Regular)
        End If

    End Sub

    Private Sub txt_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        If SoloLectura = True Then
            Exit Sub
        End If

        RaiseEvent TeclaPrecionada(sender, e)
        RaiseEvent KeyUp(sender, e)

    End Sub


    Private Sub txt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent TextChanged(sender, e)
    End Sub

End Class

﻿Public Class ocxTXTDate

    'EVENTOS
    Public Event TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Public Event Foco()
    Public Event FocoPerdido()

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)

            SoloLecturaValue = value
            txt.ReadOnly = value
            txt.TabStop = Not value
            Me.TabStop = Not value

            If value = True Then
                txt.BackColor = Color.FromArgb(vgColorSoloLectura)
            Else
                txt.BackColor = Color.FromArgb(vgColorBackColor)
            End If

        End Set
    End Property

    Private PermitirNuloValue As Boolean
    Public Property PermitirNulo() As Boolean
        Get
            Return PermitirNuloValue
        End Get
        Set(ByVal value As Boolean)
            PermitirNuloValue = value
        End Set
    End Property

    Private ColorValue As System.Drawing.Color
    Public Property Color() As System.Drawing.Color
        Get
            Return ColorValue
        End Get
        Set(ByVal value As System.Drawing.Color)
            ColorValue = value
            txt.BackColor = Color
        End Set
    End Property
    Public Property MesFecha As Integer
    Public Property AñoFecha As Integer
    Sub Inicializar()

        Fecha = VGFechaHoraSistema
        txt.Text = Fecha

        txt.AsciiOnly = True
        txt.BeepOnError = True
        txt.CutCopyMaskFormat = MaskFormat.IncludePromptAndLiterals
        txt.HidePromptOnLeave = True
        txt.HideSelection = True
        txt.InsertKeyMode = InsertKeyMode.Overwrite
        txt.PromptChar = "_"
        txt.RejectInputOnFirstFailure = False
        txt.ResetOnPrompt = False
        txt.ResetOnSpace = False
        txt.SkipLiterals = True

    End Sub

    Function SetValue(ByVal value As Date) As Boolean

        txt.BeepOnError = False
        Fecha = value
        txt.Text = Fecha.ToShortDateString
        Return True

    End Function

    Function SetValue(ByVal value As String) As Boolean

        txt.BeepOnError = False

        If IsDate(value) = True Then
            Fecha = value
            txt.Text = Fecha.ToShortDateString
        Else
            If PermitirNulo = False Then
                Fecha = Date.Now
                txt.Text = Fecha.ToShortDateString
            Else
                txt.Clear()
            End If

        End If

        txt.BeepOnError = True

        Return True

    End Function

    Function SetValueFromString(ByVal value As String) As Boolean

        txt.BeepOnError = False
        Fecha = Validar(value)

        If IsDate(value) = False Then
            Return False
        Else
            txt.Text = Fecha.ToShortDateString
            Return True
        End If


    End Function

    Public Sub Clear()
        txt.BeepOnError = False
        txt.Clear()
    End Sub

    Public Sub PrimerDiaAño()
        SetValueFromString("01/01/" & Date.Now.Year)
    End Sub

    Public Sub UltimoDiaAño()
        SetValueFromString("31/12/" & Date.Now.Year)
    End Sub

    Public Sub PrimerDiaMes()
        SetValueFromString("01/" & Date.Now.Month & "/" & Date.Now.Year)
    End Sub

    Public Sub UltimoDiaMes()
        SetValueFromString(Date.DaysInMonth(Date.Now.Year, Date.Now.Month) & "/" & Date.Now.Month & "/" & Date.Now.Year)
    End Sub

    Public Sub Hoy()
        SetValueFromString(Date.Now())
    End Sub

    Public Sub Ayer()
        SetValueFromString(Date.Now.AddDays(-1))
    End Sub

    Public Sub PrimerDiaSemana()

        Dim DiaHoy As Integer = Now.DayOfWeek - 1
        SetValueFromString(Date.Now.AddDays(-DiaHoy))

    End Sub

    Public Sub UltimoDiaSemana()
        Dim DiaHoy As Integer = Now.DayOfWeek + 1
        Dim valor As Integer = 7 - DiaHoy
        SetValueFromString(Date.Now.AddDays(valor))
    End Sub

    Public Sub UnaSemana()
        SetValueFromString(Date.Now.AddDays(+7))
    End Sub

    Function GetValue() As Date

        Dim FechaRetorno As String
        FechaRetorno = Validar(txt.Text)
        If IsDate(FechaRetorno) = False Then
            FechaRetorno = Date.Now
        End If

        Return FechaRetorno

    End Function

    Function GetValueString() As String

        Dim FechaRetorno As String
        Dim CSistema As New CSistema

        If IsDate(txt.Text) = False Then
            FechaRetorno = Date.Now
        Else
            FechaRetorno = CDate(txt.Text)
        End If

        FechaRetorno = CSistema.FormatoFechaBaseDatos(FechaRetorno, True, False)
        Return FechaRetorno

    End Function

    Private Function Validar(ByVal value As String) As Date

        txt.BeepOnError = False

        Validar = Date.Now

        Dim Dia As String
        Dim Mes As String
        Dim Año As String
        Dim Fecha() As String

        Fecha = value.Split("/")

        If Fecha.GetLength(0) <> 3 Then
            Return CDate(VGFechaHoraSistema)
        End If

        Dia = Fecha(0).Trim
        Mes = Fecha(1).Trim
        Año = Fecha(2).Trim
        

        If Año.Contains(" ") Then
            Try
                Año = Año.Split(" ")(0)
            Catch ex As Exception

            End Try
        End If

        If Dia = "" Then
            'Dia = VGFechaHoraSistema.Day <- No funcionna heredando
            Dia = Date.Now.Day
        Else
            If IsNumeric(Dia) = False Then
                'Dia = VGFechaHoraSistema.Day <- No funcionna heredando
                Dia = Date.Now.Day
            End If
        End If

        If Mes = "" Then
            'Mes = VGFechaHoraSistema.Month <- No funcionna heredando
            Mes = Date.Now.Month
        Else
            If IsNumeric(Mes) = False Then
                'Mes = VGFechaHoraSistema.Month <- No funcionna heredando
                Mes = Date.Now.Month
            End If
        End If

        If Año = "" Then
            'Año = VGFechaHoraSistema.Year <- No funcionna heredando
            Año = Date.Now.Year
        Else
            If IsNumeric(Año) = False Then
                'Año = VGFechaHoraSistema.Year <- No funcionna heredando
                Año = Date.Now.Year
            End If
        End If

        Dim temp As String = Dia & "/" & Mes & "/" & Año

        If IsDate(temp) = True Then
            Return CDate(temp)
            If Mes <> "" Then
                MesFecha = Mes
            End If
            If Año <> "" Then
                AñoFecha = Año
            End If
        Else
            MessageBox.Show("La fecha no es correcta!", "Fecha", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            'Return CDate(VGFechaHoraSistema) <- No funcionna heredando
            Return CDate(Date.Now)
        End If

    End Function

    Sub SeleccionarFechaForm()
        If SoloLectura = True Then
            Exit Sub
        End If

        If txt.Enabled = False Then
            Exit Sub
        End If

        Dim frm As New frmSeleccionFecha
        frm.Location = MousePosition
        frm.StartPosition = FormStartPosition.Manual
        frm.ShowDialog()
        If frm.Seleccionado = True Then
            SetValue(frm.MonthCalendar1.SelectionStart)
        End If
    End Sub

    Private Sub txt_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt.LostFocus

        If PermitirNulo = False Then
            txt.Text = Validar(txt.Text).ToString
        Else
            If txt.Text.Replace("/", "").Trim <> "" Then
                txt.Text = Validar(txt.Text).ToString
            End If
        End If

        RaiseEvent FocoPerdido()

    End Sub

    Private Sub txt_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt.DoubleClick
        SeleccionarFechaForm()
    End Sub

    Private Sub txt_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt.KeyUp

        RaiseEvent TeclaPrecionada(sender, e)

        If e.KeyCode = vgKeyConsultar Then
            SeleccionarFechaForm()
            Exit Sub
        End If

        Dim value As Date

        'Sumar
        If e.KeyCode = Keys.Add Then

            'Si es por mes
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                value = Me.GetValue.AddMonths(1)
                Me.SetValue(value)
                Exit Sub
            End If

            value = Me.GetValue.AddDays(1)
            Me.SetValue(value)

            Exit Sub

        End If

        'Restar
        If e.KeyCode = Keys.Subtract Then

            'Si es por mes
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                value = Me.GetValue.AddMonths(-1)
                Me.SetValue(value)
                Exit Sub
            End If

            value = Me.GetValue.AddDays(-1)
            Me.SetValue(value)

            Exit Sub

        End If

        'Accesos rapidos
        Select Case e.KeyCode

            Case Keys.H
                value = Date.Now
            Case Keys.S
                value = Date.Now
                Dim Dia As Integer = value.DayOfWeek
                value = value.AddDays(-(Dia - 1))
            Case Keys.M
                value = Date.Now
                value = value.AddDays(-(value.Day - 1))
            Case Keys.A
                value = Date.Now
                value = value.AddMonths(-(value.Month - 1))
                value = value.AddDays(-(value.Day - 1))
            Case Keys.End
                value = Date.Now
                value = value.AddDays(Date.DaysInMonth(value.Year, value.Month) - value.Day)
            Case Keys.Home
                value = Date.Now
                value = value.AddDays((value.Day * -1) + 1)
            Case Else
                Exit Sub
        End Select

        Me.SetValue(value)


    End Sub

    Private Sub txt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt.GotFocus

        'txt_KeyUp(sender, New KeyEventArgs(Keys.Insert))

        txt.Text = Validar(txt.Text).ToString
        txt.SelectAll()

        If txt.ReadOnly = False Then
            txt.BackColor = Color.FromArgb(vgColorFocus)
        End If

        RaiseEvent Foco()

    End Sub

    Private Sub txt_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt.Leave

        'txt_KeyUp(sender, New KeyEventArgs(Keys.Insert))

        If txt.ReadOnly = False Then
            txt.BackColor = Drawing.Color.White
        End If

        RaiseEvent FocoPerdido()

    End Sub

    Private Sub ocxTXTDate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If VGFechaHoraSistema.Year <> Date.Now.Year Then
        '    SetValue(Date.Now)
        'Else
        '    SetValue(VGFechaHoraSistema)
        'End If

        'PermitirNulo = False

        'Inicializar()

    End Sub

    Private Sub txt_MaskInputRejected(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MaskInputRejectedEventArgs) Handles txt.MaskInputRejected
        txt.BeepOnError = False
    End Sub

    Private Sub ToolTip1_Popup(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PopupEventArgs) Handles ToolTip1.Popup

    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        PermitirNulo = True

    End Sub

End Class

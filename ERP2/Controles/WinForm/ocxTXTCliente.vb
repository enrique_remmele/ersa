﻿Imports System.IO
Imports DevExpress.XtraPrinting.Native.WebClientUIControl
Imports Newtonsoft.Json




Public Class ocxTXTCliente

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    'Dim ClienteVario As Boolean

    'PROPIEDADES
    ''EC-ORIGINAL
    Private RegistroValue As DataRow
    Public Property Registro() As DataRow
        Get
            Return RegistroValue
        End Get
        Set(ByVal value As DataRow)
            RegistroValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private AlturaMaximaValue As Integer
    Public Property AlturaMaxima() As Integer
        Get
            Return AlturaMaximaValue
        End Get
        Set(ByVal value As Integer)
            AlturaMaximaValue = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            txtID.SoloLectura = value
            txtRazonSocial.SoloLectura = value
            txtReferencia.SoloLectura = value
            txtRUC.SoloLectura = value

        End Set
    End Property

    Private SucursalSeleccionadaValue As Boolean
    Public Property SucursalSeleccionada() As Boolean
        Get
            Return SucursalSeleccionadaValue
        End Get
        Set(ByVal value As Boolean)
            SucursalSeleccionadaValue = value
        End Set
    End Property

    Private SucursalValue As DataRow
    Public Property Sucursal() As DataRow

        Get
            Return SucursalValue
        End Get
        Set(ByVal value As DataRow
)
            SucursalValue = value
        End Set
    End Property

    Private ConsultaValue As String
    Public Property Consulta() As String
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As String)
            ConsultaValue = value
        End Set
    End Property

    Private MostrarSucursalValue As Boolean
    Public Property MostrarSucursal() As Boolean
        Get
            Return MostrarSucursalValue
        End Get
        Set(ByVal value As Boolean)
            MostrarSucursalValue = value
        End Set
    End Property

    Private frmValue As Form
    Public Property frm() As Form
        Get
            Return frmValue
        End Get
        Set(ByVal value As Form)
            frmValue = value
        End Set
    End Property
    Private ClienteVarioValue As Boolean
    Public Property ClienteVario() As Boolean
        Get
            Return ClienteVarioValue
        End Get
        Set(ByVal value As Boolean)
            ClienteVarioValue = value
        End Set
    End Property

    Private ControlCortoValue As Boolean = False
    Public Property ControlCorto() As Boolean
        Get
            Return ControlCortoValue
        End Get
        Set(ByVal value As Boolean)
            ControlCortoValue = value
        End Set
    End Property


    Public Property Actualizar As Boolean = True

    'EVENTOS
    Public Event ItemSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemMalSeleccionado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemInicializado(ByVal sender As Object, ByVal e As EventArgs)



    'VARIABLES
    Private dt As New DataTable
    Private dtTemp As New DataTable
    Private BindingSource1 As New BindingSource
    Private vTabla As String = "VCliente"
    Friend WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents BarClientes As DevExpress.XtraWaitForm.ProgressPanel
    Private vTablaSp As String = "SpViewCliente"

    Sub Inicializar()

        'Controles
        txtReferencia.ResetText()
        txtRazonSocial.ResetText()
        txtID.ResetText()
        txtRUC.ResetText()
        SucursalSeleccionada = False

        If ControlCorto Then
            txtRUC.Visible = False
            txtReferencia.Visible = False
        End If

        grid.DataSource = Nothing
        grid.Visible = False

        'Funciones
        If nmModuloActivo = "PedidoNotaCredito" Then
            PVtaConectar()
            'Conectar()
        ElseIf nmModuloActivo = "frmVentaPedido" Then
            PVtaConectar()
            'Conectar()
        Else
            Conectar()

            ''ec- Conectar()
        End If


    End Sub

    Public Sub PVtaConectar()

        'dt = CData.GetTable(vTabla)
        'dt = CSistema.ExecuteToDataTable("Select * from VCliente where idestado = 1")

        dt = CSistema.ExecuteToDataTable("Select * from tClienteVacio")

        BindingSource1.DataSource = dt


    End Sub
    ''original 
    'Public Sub Conectar()

    '    '    'dt = CData.GetTable(vTabla)
    '    '    'dt = CSistema.ExecuteToDataTable("Select * from VCliente where idestado = 1")
    '    dt = CSistema.ExecuteToDataTable("Select * from VCliente")
    '    BindingSource1.DataSource = dt

    'End Sub


    'Prueba 4  FUNCIONA OK 

    Public Sub Conectar()
        Dim directory As String = "C:\JASONSAIN\"
        Dim filePattern As String = "clientes_*.json"

        ' Crear directorio si no existe
        My.Computer.FileSystem.CreateDirectory(directory)




        ' Buscar archivos en el directorio
        Dim archivos As New List(Of String)
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(directory, FileIO.SearchOption.SearchTopLevelOnly, filePattern)
            archivos.Add(foundFile)
        Next

        ' Obtener el último archivo JSON
        Dim filePath As String = If(archivos.Count > 0, archivos(0), "")

        ' Variables para almacenar los últimos registros
        Dim ultimoRegistroJSON As (ID As Integer, FechaModificacion As DateTime) = (0, DateTime.MinValue)
        Dim ultimoRegistroBD As (ID As Integer, FechaModificacion As DateTime) = (0, DateTime.MinValue)

        ' Cargar el último registro del JSON si existe
        If Not String.IsNullOrEmpty(filePath) AndAlso File.Exists(filePath) Then
            Try
                Dim jsonData As String = File.ReadAllText(filePath)
                Dim dtJson As DataTable = JsonConvert.DeserializeObject(Of DataTable)(jsonData)

                If dtJson.Rows.Count > 0 Then
                    Dim row = dtJson.Select("", "FechaModificacion DESC").First()
                    ultimoRegistroJSON = (CInt(row("ID")), CDate(row("FechaModificacion")))
                End If
            Catch ex As Exception
                MessageBox.Show("Error al leer el JSON: " & ex.Message)
            End Try
        End If

        ' Obtener el último registro de la base de datos
        Try
            Dim dtBD As DataTable = CSistema.ExecuteToDataTable("SELECT TOP 1 ID, FechaModificacion FROM VCliente WHERE idestado = 1 ORDER BY FechaModificacion DESC")

            If dtBD.Rows.Count > 0 Then
                Dim row = dtBD.Rows(0)
                ultimoRegistroBD = (CInt(row("ID")), CDate(row("FechaModificacion")))
            End If
        Catch ex As Exception
            MessageBox.Show("Error al consultar la base de datos: " & ex.Message)
        End Try

        ' Comparar registros
        If ultimoRegistroJSON.ID <> ultimoRegistroBD.ID OrElse ultimoRegistroJSON.FechaModificacion <> ultimoRegistroBD.FechaModificacion Then
            ' Si son diferentes, eliminar el JSON y generar uno nuevo
            If Not String.IsNullOrEmpty(filePath) AndAlso File.Exists(filePath) Then
                File.Delete(filePath)
                MessageBox.Show("Se encontraron registros nuevos , se actualizará la base de datos locales de clientes.")
            End If
            dt = ObtenerDatosDesdeBD(directory)
        Else
            ' Si son iguales, cargar el JSON existente
            Try
                Dim jsonData As String = File.ReadAllText(filePath)
                dt = JsonConvert.DeserializeObject(Of DataTable)(jsonData)
            Catch ex As Exception
                dt = ObtenerDatosDesdeBD(directory)
            End Try
        End If

        ' Asignar al BindingSource
        BindingSource1.DataSource = dt
    End Sub

    Private Function ObtenerDatosDesdeBD(directory As String) As DataTable
        Dim dt As New DataTable()
        Try
            MessageBox.Show("Se creará un archivo temporal para mayor velocidad del sistema AGUARDE")



            ' Consulta a la base de datos
            dt = CSistema.ExecuteToDataTable("SELECT * FROM VCliente WHERE idestado = 1")
            'Select getdate()

            ' Convertir el DataTable a JSON
            Dim jsonData As String = JsonConvert.SerializeObject(dt, Formatting.Indented)

            ' Generar nombre del archivo con fecha y hora
            Dim filePath As String = Path.Combine(directory, $"clientes_{DateTime.Now:yyyy-MM-dd_HH-mm-ss}.json")

            ' Guardar el JSON en el archivo
            File.WriteAllText(filePath, jsonData)
        Catch ex As Exception
            MessageBox.Show("Error al consultar la base de datos: " & ex.Message)
        End Try

        Return dt
    End Function

    ''prueba 5 json - escribir solo cambios encontrados , no el json completo












    Public Sub Conectar(ByVal sql As String, Optional ByVal vCadenaConexion As String = "")

        dt = CSistema.ExecuteToDataTable(sql, vCadenaConexion)
        BindingSource1.DataSource = dt

    End Sub

    Public Sub Conectar(ByVal dtTemp As DataTable)

        dt = dtTemp.Copy
        BindingSource1.DataSource = dt

    End Sub

    Private Sub Listar(ByVal ctr As TextBox, ByVal condicion As String)

        'Validar
        If ctr.Focused = False Then
            Exit Sub
        End If

        If ctr.Text = "" Then
            OcultarLista()
            Exit Sub
        End If
        ''
        Seleccionado = False



        ConfigurarTamaño()

        Dim Where As String = ""
        Dim Valor As String = ctr.Text.Trim

        Try

            Select Case UCase(condicion)

                Case "ID"

                    If IsNumeric(ctr.Text) = False Then
                        Exit Sub
                    End If

                    Where = " ID=" & ctr.Text

                Case "REFERENCIA"

                    Where = " Referencia = '" & ctr.Text.Trim & "' "

                Case "RAZONSOCIAL"

                    Where = " RazonSocial Like '%" & ctr.Text.Trim & "%' "

                Case "RUC"

                    Where = " RUC  Like '%" & ctr.Text.Trim & "%' "

                Case Else

            End Select

            BindingSource1.Filter = Where
            grid.DataSource = BindingSource1.DataSource

            grid.Visible = True
            If AlturaMaxima > Math.Round(grid.RowCount * 20, 0) Then
                grid.Height = Math.Round(grid.RowCount * 20, 0)
            Else
                grid.Height = AlturaMaxima
            End If

            grid.BringToFront()
            Me.Height = grid.Height + TableLayoutPanel1.Height

            'Ocultar todas las columnas
            For i As Integer = 0 To grid.ColumnCount - 1
                grid.Columns(i).Visible = False
            Next

            'Mostrar las Columnas
            grid.Columns("Referencia").Visible = True
            grid.Columns("RazonSocial").Visible = True
            grid.Columns("RUC").Visible = True

            'Tamaños
            If grid.Columns.Count > 0 Then
                grid.Columns("Referencia").Width = txtReferencia.Width + 6
                grid.Columns("RazonSocial").Width = txtRazonSocial.Width + 6
                grid.Columns("RUC").Width = txtRUC.Width + 6

                grid.Columns("Referencia").DisplayIndex = 0
                grid.Columns("RazonSocial").DisplayIndex = 1
                grid.Columns("RUC").DisplayIndex = 2


            End If


            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.None

        Catch ex As Exception

        End Try

    End Sub

    Public Sub OcultarLista()

        grid.Visible = False
        Me.Height = TableLayoutPanel1.Height

    End Sub

    Public Sub LimpiarSeleccion()

        Seleccionado = False
        OcultarLista()
        txtID.txt.Clear()
        txtReferencia.txt.Clear()
        txtRazonSocial.txt.Clear()
        txtRUC.txt.Clear()
        RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        Registro = Nothing

    End Sub

    Public Sub Clear()

        txtID.txt.Clear()
        txtReferencia.txt.Clear()
        txtRazonSocial.txt.Clear()
        txtRUC.txt.Clear()

        LimpiarSeleccion()

    End Sub

    Private Sub Seleccionar(Optional ByVal GridFocus As Boolean = False)
        Try
            If SoloLectura = True Then
                Exit Sub
            End If

            'Validar
            If grid.RowCount = 0 Then

                If txtReferencia.GetValue = "" Then
                    LimpiarSeleccion()
                    RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
                    Exit Sub
                End If
                Dim consulta As String = "Select Top(1) * From VCliente Where Estado='ACTIVA' And Referencia='" & txtReferencia.GetValue & "'"
                If vgUsuarioEsVendedor Then
                    consulta = consulta & " AND IDVendedor = " & vgUsuarioIDVendedor
                End If
                Dim dtCliente As DataTable = CSistema.ExecuteToDataTable(consulta, "", 10)

                If dtCliente Is Nothing OrElse dtCliente.Rows.Count = 0 Then
                    LimpiarSeleccion()
                    RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
                    Exit Sub
                End If

                dt = BindingSource1.DataSource

                If dt Is Nothing Then
                    dt = dtCliente.Clone
                End If

                For Each orow As DataRow In dtCliente.Rows
                    Dim NewRow As DataRow = dt.NewRow
                    For c As Integer = 0 To dt.Columns.Count - 1
                        NewRow(dt.Columns(c).ColumnName) = orow(dt.Columns(c).ColumnName)
                    Next

                    dt.Rows.Add(NewRow)

                Next

                BindingSource1.DataSource = dt
                If vgData.Tables("VCliente") IsNot Nothing Then
                    vgData.Tables.Remove("VCliente")
                End If

                dt.TableName = "VCliente"
                vgData.Tables.Add(dt)

                SeleccionarRegistro(dtCliente.Rows(0)("ID").ToString)


            End If

            'Si ya esta seleccionado
            If Seleccionado = True Then
                If txtRUC.txt.Focused = True Then
                    Me.ParentForm.SelectNextControl(Me, True, True, True, True)
                Else
                    Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
                End If


                Exit Sub
            End If

            'Si los controles estan vacios
            If txtReferencia.txt.Text = "" And txtRazonSocial.txt.Text = "" And txtRUC.txt.Text = "" Then
                RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
                Exit Sub
            End If

            If GridFocus = True Then
                Exit Sub
            End If

            SeleccionarRegistro()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub SeleccionarGrid()

        'Validar
        If grid.RowCount = 0 Then
            LimpiarSeleccion()
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
            Exit Sub
        End If

        'Si ya esta seleccionado
        If Seleccionado = True Then
            Exit Sub
        End If

        SeleccionarRegistro()

    End Sub

    Sub SeleccionarRegistro(Optional ByVal IDCliente As Integer = 0)

        Try

            Dim ID As Integer

            If IDCliente = 0 Then
                ID = grid.SelectedRows(0).Cells(0).Value
            Else
                ID = IDCliente
            End If

            Dim oRow As DataRow = dt.Select(" ID=" & ID)(0)

            Registro = oRow
            txtID.txt.Text = oRow("ID").ToString
            txtReferencia.txt.Text = oRow("Referencia").ToString
            txtRazonSocial.txt.Text = oRow("RazonSocial").ToString
            'dbs
            Dim SQL As String = "Exec SpAcualizarSaldoCliente " & txtID.ObtenerValor
            Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL, VGCadenaConexion)
            If Resultado Is Nothing Then
                MessageBox.Show("Ocurrio un error al actualizar saldo de Cliente!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If


            txtRUC.txt.Text = oRow("RUC").ToString
            ClienteVario = CBool(oRow("ClienteVario").ToString)

            'Comprobar si el Cliente tiene sucursales
            SucursalSeleccionada = False

            ''en duro para JSON
            'MostrarSucursal = True
            If MostrarSucursal = True Then

                If CBool(oRow("TieneSucursales").ToString) = True Then

                    'Seleccionar Sucursal
                    Dim frm As New frmSeleccionSucursalCliente
                    frm.Cliente = oRow
                    frm.IDCliente = oRow("ID")
                    frm.StartPosition = FormStartPosition.CenterScreen
                    frm.Icon = Me.ParentForm.Icon
                    FGMostrarFormulario(Me.ParentForm, frm, "Sucursales de Clientes", FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

                    'Si se selecciono un registro
                    If frm.Seleccionado = False Then
                        OcultarLista()
                        Seleccionado = False
                    End If

                    If frm.MatrizSeleccionada = False Then
                        SucursalSeleccionada = True
                        Sucursal = frm.Sucursal
                    End If

                    OcultarLista()
                    Seleccionado = True
                    RaiseEvent ItemSeleccionado(New Object, New EventArgs)

                Else

                    OcultarLista()
                    Seleccionado = True
                    SucursalSeleccionada = False
                    RaiseEvent ItemSeleccionado(New Object, New EventArgs)

                End If
            Else
                OcultarLista()
                Seleccionado = True
                RaiseEvent ItemSeleccionado(New Object, New EventArgs)
            End If
            If ClienteVario = True Then
                txtRazonSocial.Focus()
            End If


        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try

    End Sub

    Private Sub ConfigurarTamaño()

        'Tañanos
        txtID.Width = TableLayoutPanel1.Width
        grid.Width = TableLayoutPanel1.Width - 3

        If grid.Visible = False Then
            Me.Height = TableLayoutPanel1.Height
        End If

        'Localizacion
        grid.Top = TableLayoutPanel1.Height - 4
        grid.Left = TableLayoutPanel1.Left + 3

    End Sub

    Public Sub SetValue(ByVal ID As Integer)

        Try
            If dt.Select(" ID=" & ID).Count = 0 Then
                Registro = CSistema.ExecuteToDataTable("Select * from VCliente where ID = " & ID)(0)
                If Not Registro Is Nothing Then
                    txtID.txt.Text = Registro("ID").ToString
                    txtReferencia.txt.Text = Registro("Referencia").ToString
                    txtRazonSocial.txt.Text = Registro("RazonSocial").ToString
                    txtRUC.txt.Text = Registro("RUC").ToString
                    'dbs
                    Dim SQL As String = "Exec SpAcualizarSaldoCliente " & txtID.ObtenerValor
                    Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL, VGCadenaConexion)
                    If Resultado Is Nothing Then
                        MessageBox.Show("Ocurrio un error al actualizar saldo de Cliente!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If

                    OcultarLista()
                    Seleccionado = True
                    RaiseEvent ItemSeleccionado(New Object, New EventArgs)
                Else
                    Registro = Nothing
                    txtID.txt.Text = ""
                    txtReferencia.txt.Text = ""
                    txtRazonSocial.txt.Text = ""
                    txtRUC.txt.Text = ""

                    OcultarLista()
                    Seleccionado = False
                    RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
                End If

            Else

                Dim oRow As DataRow = dt.Select(" ID=" & ID)(0)

                Registro = oRow
                txtID.txt.Text = oRow("ID").ToString
                txtReferencia.txt.Text = oRow("Referencia").ToString
                txtRazonSocial.txt.Text = oRow("RazonSocial").ToString
                txtRUC.txt.Text = oRow("RUC").ToString
                'dbs
                Dim SQL As String = "Exec SpAcualizarSaldoCliente " & txtID.ObtenerValor
                Dim Resultado As DataTable = CSistema.ExecuteToDataTable(SQL, VGCadenaConexion)
                If Resultado Is Nothing Then
                    MessageBox.Show("Ocurrio un error al actualizar saldo de Cliente!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If

                OcultarLista()
                Seleccionado = True
                RaiseEvent ItemSeleccionado(New Object, New EventArgs)

            End If

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try

    End Sub

    Public Sub SetValue(ByVal Referencia As String, RazonSocial As String, RUC As String)

        Try

            txtReferencia.txt.Text = Referencia
            txtRazonSocial.txt.Text = RazonSocial
            txtRUC.txt.Text = RUC

        Catch ex As Exception
            RaiseEvent ItemMalSeleccionado(New Object, New EventArgs)
        End Try

    End Sub

    Private Sub MostrarPropiedades()

        Dim ID As Integer
        Dim RazonSocial As String = ""

        If Seleccionado = False Then

            If grid.RowCount = 0 Then
                Exit Sub
            End If

            If grid.SelectedRows.Count = 0 Then
                Exit Sub
            End If

            ID = grid.SelectedRows(0).Cells(0).Value
            RazonSocial = grid.SelectedRows(0).Cells(1).Value
        Else
            ID = Registro("ID").ToString
            RazonSocial = Registro("RazonSocial").ToString
        End If



        Dim frm As New frmPropiedad
        frm.TituloVentana = "Propiedades del Cliente"
        frm.Titulo = RazonSocial
        frm.Consulta = "Select ID, RazonSocial, RUC, NombreFantasia, Direccion, Telefono, Estado, ListaPrecio, TipoCliente, Sucursal, Ciudad, Barrio From VCliente Where ID=" & ID & " Order By ID"
        frm.ShowDialog()

    End Sub

    Private Sub MostrarConsulta()

        Dim frm As New frmClienteBuscar
        frm.Size = New Size(frmPrincipal2.Size.Width - 50, frmPrincipal2.Height - 130)
        frm.Location = New Point(5, 10)
        frm.Consulta = Consulta
        FGMostrarFormulario(Me.ParentForm, frm, "Busqueda de Clientes", FormBorderStyle.Sizable, FormStartPosition.CenterScreen, True, False)

        If frm.ID = 0 Then
            Exit Sub
        End If

        'Si no existe en el DATATABLE, agregar
        If CData.GetTable(vTabla).Select("ID=" & frm.ID).Count = 0 Then

            'Actualizamos el Registro
            CData.Insertar(frm.ID, vTabla)

            'dt = CData.GetTable(vTabla)
            'BindingSource1.DataSource = dt

        End If
        dt = CData.GetTable(vTabla)
        BindingSource1.DataSource = dt

        SeleccionarRegistro(frm.ID)

    End Sub

    Public Function IsFocus() As Boolean

        IsFocus = False

        If txtID.txt.Focused = True Then
            Return True
        End If

        If txtReferencia.txt.Focused = True Then
            Return True
        End If

        If txtRazonSocial.txt.Focused = True Then
            Return True
        End If

        If txtRUC.txt.Focused = True Then
            Return True
        End If

    End Function

    Public Sub SetFocus()
        txtID.txt.Focus()
        txtID.txt.SelectAll()
        If txtID.txt.Focused = False Then
            txtID.txt.Focus()
        End If

    End Sub

    Private Sub ManejoTecla(ByVal txt As TextBox, ByVal e As System.Windows.Forms.KeyEventArgs, ByVal Condicion As String)

        'Si es solo lectura, salir
        If SoloLectura = True Then
            GoTo pasar
        End If

        If e.KeyCode = Keys.Down Then
            If grid.RowCount > 0 Then
                grid.Focus()
                If grid.SelectedRows.Count = 0 Then
                    grid.Rows(0).Selected = True
                End If
            End If

            Exit Sub

        End If

        If e.KeyCode = Keys.Enter Then
            If txt.Text.Trim = "" Then
                If txtRUC.txt.Focused = True Then
                    Me.ParentForm.SelectNextControl(Me, True, True, True, True)
                Else
                    Me.SelectNextControl(Me.ActiveControl, True, True, True, True)
                End If
            Else
                Seleccionar()
            End If
            Exit Sub
        End If

        If e.KeyCode = Keys.ShiftKey Then
            Exit Sub
        End If

        If e.KeyCode = Keys.Tab Then
            If Seleccionado = False Then
                Seleccionar()
            End If
            Exit Sub
        End If

        If e.KeyCode = vgKeyConsultar Then
            MostrarConsulta()
            Exit Sub
        End If

        If txt.Text = "" Then
            LimpiarSeleccion()
            Exit Sub
        End If

        Listar(txt, Condicion)

pasar:
        If e.KeyCode = vgKeyVerInformacionDetallada Then
            MostrarPropiedades()
            Exit Sub
        End If

        If e.KeyCode = vgKeyActualizarTabla Then

            CData.ResetTable(vTabla, "Exec " & vTablaSp & " @IDUsuario = " & vgIDUsuario & ", @IDSucursal = " & vgIDSucursal & ", @IDDeposito = " & vgIDDeposito & ", @IDTerminal = " & vgIDTerminal & " ")
            dt = CData.GetTable(vTabla)
            BindingSource1.DataSource = dt

            Exit Sub

        End If

    End Sub

    Private Sub ocxTXTCliente_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        OcultarLista()
    End Sub

    Private Sub ocxTXTCliente_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus
        OcultarLista()
    End Sub

    Private Sub txtRazonSocial_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRazonSocial.KeyUp
        If ClienteVario = "False" Or txtReferencia.txt.Text = "" Then
            e.Handled = True
            ManejoTecla(txtRazonSocial.txt, e, "RAZONSOCIAL")
        End If
    End Sub

    Private Sub txtReferencia_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.KeyUp
        e.Handled = True
        ManejoTecla(txtReferencia.txt, e, "REFERENCIA")
    End Sub

    Private Sub txtRUC_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRUC.KeyUp
        If ClienteVario = "False" Or txtReferencia.txt.Text = "" Then
            e.Handled = True
            ManejoTecla(txtRUC.txt, e, "RUC")
        End If
    End Sub

    Private Sub grid_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            SeleccionarGrid()
        End If
    End Sub

    Private Sub ocxTXTCliente_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        ConfigurarTamaño()
    End Sub

    Private Sub grid_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grid.KeyUp

        If e.KeyCode = vgKeyVerInformacionDetallada Then
            MostrarPropiedades()
        End If

        If e.KeyCode = vgKeyConsultar Then
            MostrarConsulta()
        End If

    End Sub

    Private Sub ocxTXTCliente_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Seleccionar(False)
    End Sub

    Private Sub barcliente()
        Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl()
        Me.BarClientes = New DevExpress.XtraWaitForm.ProgressPanel()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ProgressBarControl1
        '
        Me.ProgressBarControl1.Location = New System.Drawing.Point(40, 52)
        Me.ProgressBarControl1.Name = "ProgressBarControl1"
        Me.ProgressBarControl1.Size = New System.Drawing.Size(291, 60)
        Me.ProgressBarControl1.TabIndex = 0
        '
        'BarClientes
        '
        Me.BarClientes.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.BarClientes.Appearance.Options.UseBackColor = True
        Me.BarClientes.Location = New System.Drawing.Point(40, 46)
        Me.BarClientes.Name = "BarClientes"
        Me.BarClientes.Size = New System.Drawing.Size(246, 66)
        Me.BarClientes.TabIndex = 1
        Me.BarClientes.Text = "Cargando Clientes"
        '
        'ocxTXTCliente
        '
        Me.Controls.Add(Me.BarClientes)
        Me.Controls.Add(Me.ProgressBarControl1)
        Me.Name = "ocxTXTCliente"
        Me.Size = New System.Drawing.Size(346, 150)
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub


    ''ec-
    'Private Sub ocxTXTCliente_Load(sender As Object, e As EventArgs) Handles Me.Load
    '    Inicializar()
    'End Sub



End Class

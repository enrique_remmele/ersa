﻿Public Class ocxCHK

    'EVENTOS
    Public Event TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As EventArgs, ByVal value As Boolean)

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)

            SoloLecturaValue = value

            If value = True Then
                'chk.BackColor = System.Drawing.SystemColors.Control
                lbl.BackColor = System.Drawing.SystemColors.Control
            Else
                'chk.BackColor = System.Drawing.SystemColors.Control
                lbl.BackColor = System.Drawing.SystemColors.Control
            End If
        End Set

    End Property

    Private TextoValue As String
    Public Property Texto() As String
        Get
            Return TextoValue
        End Get
        Set(ByVal value As String)
            chk.Text = value
            chk.Refresh()
            TextoValue = value
        End Set
    End Property

    Private ColorValue As System.Drawing.Color
    Public Property Color() As System.Drawing.Color
        Get
            Return ColorValue
        End Get
        Set(ByVal value As System.Drawing.Color)
            ColorValue = value
            lbl.BackColor = Color
            'chk.BackColor = Color
        End Set
    End Property

    Private ValorValue As Boolean
    Public Property Valor() As Boolean
        Get
            Return ValorValue
        End Get
        Set(ByVal value As Boolean)
            ValorValue = value
            chk.Checked = value
            Me.Refresh()
            Me.Update()
        End Set

    End Property

    Sub Inicializar()
        'lbl.Text = Texto
        lbl.Visible = False
        chk.Text = Texto

    End Sub

    Sub CambiarColor(ByVal Foco As Boolean)

        If SoloLectura = True Then
            Exit Sub
        End If

        If Foco = True Then
            lbl.BackColor = Color.FromArgb(vgColorFocus)
            chk.BackColor = Color.FromArgb(vgColorFocus)
            Me.BorderStyle = BorderStyle.FixedSingle
            Me.BackColor = Color.FromArgb(vgColorFocus)
        Else
            lbl.BackColor = Me.ParentForm.BackColor
            chk.BackColor = Me.ParentForm.BackColor
            Me.BackColor = Me.ParentForm.BackColor
            Me.BorderStyle = BorderStyle.None
        End If

    End Sub

    Sub CambiarValor()

        If SoloLectura = True Then
            If chk.Checked <> Valor Then
                Valor = Not chk.Checked
            End If
        Else
            If chk.Checked <> Valor Then
                Valor = chk.Checked
            End If
        End If

        RaiseEvent PropertyChanged(New Object, New EventArgs, Valor)


    End Sub

    Private Sub chk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk.CheckedChanged
        CambiarValor()
    End Sub

    Private Sub lbl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbl.Click

        If SoloLectura = True Then
            Exit Sub
        End If

        chk.Checked = Not chk.Checked

        CambiarValor()
        CambiarColor(True)

    End Sub

    Private Sub chk_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk.GotFocus
        CambiarColor(True)
    End Sub

    Private Sub chk_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk.Leave
        CambiarColor(False)
    End Sub

    Private Sub ocxCHK_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        CambiarColor(True)
    End Sub

    Private Sub ocxCHK_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        CambiarColor(False)
    End Sub

    Private Sub lbl_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl.MouseEnter
        'chk.Focus()
        CambiarColor(True)
    End Sub

    Private Sub lbl_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbl.MouseLeave
        CambiarColor(False)
    End Sub

    Private Sub chk_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk.MouseEnter
        'chk.Focus()
        CambiarColor(True)
    End Sub

    Private Sub chk_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk.MouseLeave
        CambiarColor(False)
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().


    End Sub

    Private Sub ocxCHK_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub FlowLayoutPanel1_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FlowLayoutPanel1.SizeChanged
        lbl.Size = New Size((FlowLayoutPanel1.Size.Width - chk.Size.Width) - 3, lbl.Size.Height)
    End Sub

End Class

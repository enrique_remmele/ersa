﻿
Public Class ocxRendicionLoteCobranzaExtra


    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private dtDetalleCobranzaExtraValue As DataTable
    Public Property dtDetalleCobranzaExtra() As DataTable
        Get
            Return dtDetalleCobranzaExtraValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleCobranzaExtraValue = value
        End Set
    End Property

    Private dtDetalleLoteValue As DataTable
    Public Property dtDetalleLote() As DataTable
        Get
            Return dtDetalleLoteValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleLoteValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal
        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            btnAgregar.Enabled = Not value
            btnEliminar.Enabled = Not value
            Me.TabStop = Not value
        End Set

    End Property

    'EVENTOS
    Public Event ErrorDetectado(ByVal sender As Object, ByVal e As EventArgs, ByVal mensaje As String)
    Public Event UpdateTotal(ByVal sender As Object, ByVal e As EventArgs)

    'FUNCIONES
    Public Sub Inicializar()

        'Estructura del detalle
        dtDetalleCobranzaExtra = CSistema.ExecuteToDataTable("Select Top(0) * From DetalleRendicionLoteCobranzaExtra").Copy


    End Sub

    Public Sub CargarComprobantesLote()


        dtDetalleLote.Columns.Add("Sel")
        For Each oRow As DataRow In dtDetalleLote.Rows
            oRow("Sel") = False
            oRow("Importe") = 0
        Next

    End Sub

    Private Sub CargarDocumento()

        Try
            Dim frm As New frmRendicionLoteCargaCobranzaExtra

            frm.Inicializar()
            frm.ShowDialog(Me)

            If frm.Procesado = True Then

                Dim NewRow As DataRow = dtDetalleCobranzaExtra.NewRow

                NewRow("IDTransaccion") = IDTransaccion
                NewRow("ID") = dtDetalleCobranzaExtra.Rows.Count
                NewRow("Cliente") = frm.Cliente
                NewRow("Comprobante") = frm.Comprobante
                NewRow("Importe") = frm.Importe

                dtDetalleCobranzaExtra.Rows.Add(NewRow)

                Listar()

            End If

        Catch ex As Exception
            CSistema.CargarError(ex, Me.Name, "CobranzaExtra")
        End Try


    End Sub

    Private Sub Listar()

        lvLista.Items.Clear()

        For Each oRow As DataRow In dtDetalleCobranzaExtra.Rows
            Dim item As ListViewItem = lvLista.Items.Add(oRow("Cliente").ToString)
            item.SubItems.Add(oRow("Comprobante").ToString)
            item.SubItems.Add(oRow("Importe").ToString)
        Next

        CSistema.FormatoMoneda(lvLista, 2)

        CalcularTotales()

    End Sub

    'Public Sub CargarComprobantesLote()

    '    dtDetalleLote.Columns.Add("Sel")
    '    dtDetalleLote.Columns.Add("Importe")
    '    dtDetalleLote.Columns.Add("Motivo")
    '    dtDetalleLote.Columns.Add("IDMotivo")

    '    For Each oRow As DataRow In dtDetalleLote.Rows
    '        oRow("Sel") = False
    '        oRow("Importe") = oRow("Saldo")
    '    Next

    'End Sub

    Private Sub CalcularTotales()

        Total = CSistema.dtSumColumn(dtDetalleCobranzaExtra, "Importe")
        txtTotal.SetValue(Total)

        RaiseEvent UpdateTotal(New Object, New EventArgs)

    End Sub

    Sub EliminarRegistros()

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvLista.SelectedItems

            Dim Venta As String = item.SubItems(0).Text

            Dim Where As String = " Venta = '" & Venta & "' "
            For Each oRow As DataRow In dtDetalleCobranzaExtra.Select(Where)
                dtDetalleCobranzaExtra.Rows.Remove(oRow)
            Next

        Next

        Listar()

    End Sub

    Sub Limpiar()

        IDTransaccion = 0
        Total = 0
        dtDetalleCobranzaExtra.Rows.Clear()
        Listar()

    End Sub

    Public Sub Guardar(ByVal vIDTransaccion As Integer)

        IDTransaccion = vIDTransaccion

        Dim i As Integer = 1

        For Each oRow As DataRow In dtDetalleCobranzaExtra.Rows

            Dim param(-1) As SqlClient.SqlParameter

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", i, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cliente", oRow("Cliente"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Comprobante", oRow("Comprobante"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", oRow("Importe"), ParameterDirection.Input)

            'Operacion
            CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.INS.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""
            If CSistema.ExecuteStoreProcedure1(param, "SpDetalleRendicionLoteCobranzaExtra", False, False, MensajeRetorno) = False Then

            End If

            i = i + 1

        Next

    End Sub

    Public Sub CargarDatos(ByVal vIDTransaccion As Integer)

        IDTransaccion = vIDTransaccion
        dtDetalleCobranzaExtra = CSistema.ExecuteToDataTable("Select * From DetalleRendicionLoteCobranzaExtra where IDTransaccion = " & IDTransaccion)
        Listar()

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        CargarDocumento()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        EliminarRegistros()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarRegistros()
        End If
    End Sub


End Class

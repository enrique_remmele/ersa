﻿Public Class frmRendicionChequesDetalle

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Sub Inicializar()

        Me.Text = "Detalle de Cheques para el lote: " & Comprobante

        'Cargar los Bancos Locales
        For Each oRow As DataRow In dt.Select(" BancoLocal='True' And Diferido='False'")
            CargarItem(oRow, lvBancoLocal)
        Next

        'Cargar otros Bancos
        For Each oRow As DataRow In dt.Select(" BancoLocal='False' And Diferido='False'")
            CargarItem(oRow, lvOtrosBancos)
        Next

        'Cargar diferidos
        For Each oRow As DataRow In dt.Select(" Diferido='True'")
            CargarItem(oRow, lvDiferidos)
        Next

        CalcularTotales()

    End Sub

    Private Sub CargarItem(ByVal row As DataRow, ByVal lv As ListView)

        Dim item As ListViewItem = lv.Items.Add(row("Banco").ToString)
        item.SubItems.Add(row("NroCheque").ToString)
        item.SubItems.Add(row("Comprobante").ToString)
        item.SubItems.Add(row("Cliente").ToString)
        item.SubItems.Add(row("Fec Venc.").ToString)
        item.SubItems.Add(row("Importe").ToString)

    End Sub

    Private Sub CalcularTotales()

        'Formato
        CSistema.FormatoMoneda(lvBancoLocal, 5, 0)
        CSistema.FormatoMoneda(lvDiferidos, 5, 0)
        CSistema.FormatoMoneda(lvOtrosBancos, 5, 0)

        'Totales
        CSistema.TotalesLv(lvBancoLocal, txtTotalBancoLocal.txt, 5, True)
        CSistema.TotalesLv(lvDiferidos, txtTotalDiferidos.txt, 5, True)
        CSistema.TotalesLv(lvOtrosBancos, txtTotalOtrosBancos.txt, 5, True)

    End Sub

    Private Sub btnSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

End Class
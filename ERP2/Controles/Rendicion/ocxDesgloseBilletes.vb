﻿Public Class ocxDesgloseBilletes

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event ErrorDetectado(ByVal sender As Object, ByVal e As EventArgs, ByVal mensaje As String)
    Public Event UpdateTotal(ByVal sender As Object, ByVal e As EventArgs)

    'PROPIEADES
    Private TotalValue As Decimal
    Public Property Total() As Decimal
        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            dg.Columns(2).ReadOnly = value
            Me.TabStop = Not value
        End Set
    End Property

    Private dtDesgloseValue As DataTable
    Public Property dtDesglose() As DataTable
        Get
            Return dtDesgloseValue
        End Get
        Set(ByVal value As DataTable)
            dtDesgloseValue = value
        End Set
    End Property


    'FUNCIONES
    Sub Inicializar()

        'Cargar la informacion en la grilla
        dg.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray

        'Cargar Los tipos de impuestos
        Dim dtTemp As DataTable = CSistema.ExecuteToDataTable("Select ID, Valor From Billete Order By Valor Desc").Copy
        For Each oRow As DataRow In dtTemp.Rows
            Dim rIndex As Integer = dg.Rows.Add()
            dg.Item(0, rIndex).Value = oRow("ID").ToString
            dg.Item(1, rIndex).Value = CSistema.FormatoMoneda(oRow("Valor").ToString)
            dg.Item(2, rIndex).Value = 0
            dg.Item(3, rIndex).Value = 0
        Next

        Total = 0

    End Sub

    Sub CalcularTotales()

        CSistema.TotalesGrid(dg, txtTotal.txt, 3)
        Total = txtTotal.ObtenerValor

        RaiseEvent UpdateTotal(New Object, New EventArgs)

    End Sub

    Public Sub Guardar(ByVal vIDTransaccion As Integer)

        IDTransaccion = vIDTransaccion

        Dim i As Integer = 1

        For Each oRow As DataGridViewRow In dg.Rows

            Dim param(-1) As SqlClient.SqlParameter

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", i, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDBillete", oRow.Cells(0).Value, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoNumeroBaseDatos(oRow.Cells(2).Value, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(oRow.Cells(3).Value, False), ParameterDirection.Input)

            'Operacion
            CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.INS.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure1(param, "SpDesgloseBillete", False, False, MensajeRetorno) = False Then

            End If

            i = i + 1

        Next

    End Sub

    Sub Listar()

        For Each oRow As DataGridViewRow In dg.Rows
            oRow.Cells(2).Value = 0
            oRow.Cells(3).Value = 0
            For Each i As DataRow In dtDesglose.Select(" IDBillete = " & oRow.Cells(0).Value)
                oRow.Cells(2).Value = i("Cantidad")
                oRow.Cells(3).Value = i("Cantidad") * oRow.Cells(1).Value
            Next
        Next

        CalcularTotales()

    End Sub

    Sub Limpiar()

        IDTransaccion = 0
        Total = 0


        If dtDesglose Is Nothing Then
            Exit Sub
        End If

        dtDesglose.Rows.Clear()

        Listar()

    End Sub

    Public Sub CargarDatos(ByVal vIDTransaccion As Integer)

        IDTransaccion = vIDTransaccion
        dtDesglose = CSistema.ExecuteToDataTable("Select * From DesgloseBillete Where IDTransaccion=" & IDTransaccion).Copy
        Listar()

    End Sub

    Private Sub ocxDesgloseBilletes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Inicializar()
    End Sub

    Private Sub dg_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg.CellEndEdit
        If e.ColumnIndex = 2 Then

            'Pasa que si el usuario pone 0 (cero) el campo se muestra como nothing!
            'NO emitir error.
            If dg.CurrentCell.Value Is Nothing Then
                dg.CurrentCell.Value = 0
                dg.Rows(dg.CurrentCell.RowIndex).Cells(2).Value = 0
                dg.Rows(dg.CurrentCell.RowIndex).Cells(3).Value = dg.Rows(dg.CurrentCell.RowIndex).Cells(1).Value * dg.Rows(dg.CurrentCell.RowIndex).Cells(2).Value
                CalcularTotales()
                Exit Sub
            End If

            'Si no es numerico, poner todo 0
            If IsNumeric(dg.CurrentCell.Value) = False Then
                Dim mensaje As String = "El importe debe ser valido!"
                RaiseEvent ErrorDetectado(Me, New EventArgs, mensaje)
                dg.CurrentCell.Value = 0
                dg.Rows(dg.CurrentCell.RowIndex).Cells(2).Value = 0
                CalcularTotales()
                Exit Sub
            End If

            'Calcular
            If IsNumeric(dg.CurrentCell.Value) = True Then
                dg.Rows(dg.CurrentCell.RowIndex).Cells(3).Value = dg.Rows(dg.CurrentCell.RowIndex).Cells(1).Value * dg.Rows(dg.CurrentCell.RowIndex).Cells(2).Value
                CalcularTotales()
                Exit Sub
            End If

        End If

    End Sub

    Private Sub dg_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dg.Rows
            If oRow.Index = e.RowIndex Then
                oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                oRow.DefaultCellStyle.Font = f1
                oRow.Cells(2).Selected = True
            Else
                oRow.DefaultCellStyle.Font = f2


                oRow.DefaultCellStyle.BackColor = Color.White
            End If
        Next
    End Sub

End Class

﻿Public Class frmRendicionLoteCargaVentasAnuladas

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private dtComprobantesValue As DataTable
    Public Property dtComprobantes() As DataTable
        Get
            Return dtComprobantesValue
        End Get
        Set(ByVal value As DataTable)
            dtComprobantesValue = value
        End Set
    End Property

    Private dtAnuladosValue As DataTable
    Public Property dtAnulados() As DataTable
        Get
            Return dtAnuladosValue
        End Get
        Set(ByVal value As DataTable)
            dtAnuladosValue = value
        End Set
    End Property

    Private RegistroCompletadoValue As Boolean
    Public Property RegistroCompletado() As Boolean
        Get
            Return RegistroCompletadoValue
        End Get
        Set(ByVal value As Boolean)
            RegistroCompletadoValue = value
        End Set
    End Property

    'VARIABLES
    Dim dtMotivos As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Form
        Me.Text = "Documentos para el Lote: " & Comprobante

        'Funciones
        ListarComprobantes()

        RegistroCompletado = False

        'Foco


    End Sub

    Sub ListarComprobantes()

        dgw.Rows.Clear()
        For Each oRow As DataRow In dtComprobantes.Rows

            Dim Registro(6) As String
            Registro(0) = oRow("IDTransaccionVenta").ToString
            Registro(1) = oRow("Sel").ToString
            Registro(2) = oRow("Comprobante").ToString
            Registro(3) = oRow("Cliente").ToString
            Registro(4) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(5) = CSistema.FormatoMoneda(oRow("Importe").ToString)

            dgw.Rows.Add(Registro)

        Next

        'Veremos Si funciona
        Dim cbxMotivo As New DataGridViewComboBoxColumn
        cbxMotivo.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
        dtMotivos = CSistema.ExecuteToDataTable("Select ID, Descripcion From MotivoAnulacionVenta Order By 2").Copy
        CSistema.SqlToComboBox(cbxMotivo, dtMotivos, "ID", "Descripcion")
        dgw.Columns.RemoveAt(6)
        dgw.Columns.Insert(6, cbxMotivo)

    End Sub

    Public Sub CargarComprobantes()


    End Sub

    Private Sub CalcularTotales()

        'variables
        Dim TotalVentas = 0
        Dim TotalCheque = 0
        Dim Saldo As Decimal = 0
        Dim CantidadComprobante As Integer = 0
        Dim Seleccionado As Boolean = False

        'Poner seleccionado
        For index As Integer = 0 To dgw.RowCount - 1

            Seleccionado = dgw.Rows(index).Cells(1).Value

            Dim IDTransaccion As Integer = dgw.Rows(index).Cells(0).Value

            For Each orow As DataRow In dtComprobantes.Select(" IDTransaccionVenta = " & IDTransaccion)
                orow("Sel") = Seleccionado

                'Importe
                orow("Importe") = dgw.Rows(index).Cells(5).Value

            Next

        Next

        'Sumar Ventas
        For Each orow As DataRow In dtComprobantes.Select(" Sel = 'True' ")
            TotalVentas = TotalVentas + orow("Importe")
            CantidadComprobante = CantidadComprobante + 1
        Next

        TotalCheque = txtTotalCobrado.ObtenerValor

        'Calucluar Saldo
        Saldo = TotalCheque - TotalVentas

        'Valores
        txtTotalCobrado.txt.Text = TotalVentas
        txtCantidadCobrado.txt.Text = CantidadComprobante

    End Sub

    Private Sub Procesar()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Validar

        Dim i As Integer = 0

        For Each oRow As DataRow In dtComprobantes.Select(" Sel = 'True' ")

            Dim NewRow As DataRow = dtAnulados.NewRow

            NewRow("IDTransaccion") = IDTransaccion
            NewRow("ID") = i
            NewRow("Motivo") = oRow("Motivo")
            NewRow("IDMotivo") = oRow("IDMotivo")
            NewRow("IDTransaccionVenta") = oRow("IDTransaccionVenta")
            NewRow("Venta") = oRow("Comprobante")
            NewRow("Cliente") = oRow("Cliente")
            NewRow("Saldo") = oRow("Saldo")
            NewRow("Credito") = oRow("Credito")
            NewRow("Importe") = oRow("Importe")

            dtAnulados.Rows.Add(NewRow)

            i = i + 1

        Next

        RegistroCompletado = True
        Me.Close()

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick
        ctrError.Clear()
        tsslEstado.Text = ""

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(6)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellEndEdit

        If e.ColumnIndex = 6 Then

            Dim IDMotivo As String = dgw.CurrentCell.Value

            If IsNumeric(IDMotivo) = False Then
                Exit Sub
            End If

            Dim Motivo As String = dtMotivos.Select(" ID = " & IDMotivo)(0)("Descripcion").ToString
            Dim Venta As String = dgw.CurrentRow.Cells(2).Value

            For Each oRow As DataRow In dtComprobantes.Select(" Comprobante = '" & Venta & "' ")
                oRow("Motivo") = Motivo
                oRow("IDMotivo") = IDMotivo
            Next


            CalcularTotales()

        End If


    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Space Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells(1).Value = False Then
                        oRow.Cells(1).Value = True
                        oRow.Cells(1).ReadOnly = False
                        oRow.Cells(6).ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgw.CurrentCell = dgw.Rows(oRow.Index).Cells(6)

                    Else
                        oRow.Cells(1).ReadOnly = True
                        oRow.Cells(6).ReadOnly = True
                        oRow.Cells(1).Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgw.Rows.Count - 1 Then
                dgw.CurrentCell = dgw.Rows(RowIndex + 1).Cells("colImporte")
            End If


            CalcularTotales()

            dgw.Update()



            ' Your code here
            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgw.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                oRow.Cells(6).Selected = True

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells(1).Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Tab Then
            If dgw.SelectedCells.Count = 0 Then
                Exit Sub
            End If
            dgw.CurrentCell = dgw.Rows(dgw.CurrentRow.Index).Cells(6)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Procesar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmRendicionLoteCargaVentasAnuladas_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub
End Class
﻿Public Class ocxCargaCheque

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private dtDetalleChequeValue As DataTable
    Public Property dtDetalleCheque() As DataTable
        Get
            Return dtDetalleChequeValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleChequeValue = value
        End Set
    End Property

    Private dtDetalleLoteValue As DataTable
    Public Property dtDetalleLote() As DataTable
        Get
            Return dtDetalleLoteValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleLoteValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal
        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            btnCargar.Enabled = Not value
            lklEliminarCheque.Enabled = Not value
            Me.TabStop = Not value
        End Set

    End Property

    'EVENTOS
    Public Event ErrorDetectado(ByVal sender As Object, ByVal e As EventArgs, ByVal mensaje As String)
    Public Event UpdateTotal(ByVal sender As Object, ByVal e As EventArgs)

    'VARIABLES

    'FUNCIONES
    Public Sub Inicializar()

        'Estructura del detalle
        dtDetalleCheque = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleRendicionLoteCheque Order By Comprobante").Copy
        dtDetalleCheque.Columns.Add("Sel")

        'Inicializar valores extras
        For Each oRow As DataRow In dtDetalleCheque.Rows
            oRow("Sel") = False
        Next

    End Sub

    Private Sub CargarCheque()

        Try
            Dim frm As New frmRendicionLoteCargaCheque

            'Comprobantes de venta del Lote
            frm.dtComprobantes = dtDetalleLote.Copy
            frm.dtCheques = dtDetalleCheque.Copy

            frm.Inicializar()
            frm.ShowDialog(Me)

            If frm.RegistroCompletado = True Then

                dtDetalleCheque.Rows.Clear()

                For Each oRow As DataRow In frm.dtCheques.Rows
                    dtDetalleCheque.Rows.Add(oRow.ItemArray)
                Next
                ListarCheques()
            End If
        Catch ex As Exception
            CSistema.CargarError(ex, Me.Name, "CargarCheque")
        End Try
      

    End Sub

    Public Sub CargarComprobantesLote()


        dtDetalleLote.Columns.Add("Sel")
        dtDetalleLote.Columns.Add("Importe")
        dtDetalleLote.Columns.Add("Cancelar")
        For Each oRow As DataRow In dtDetalleLote.Rows
            oRow("Sel") = False
            oRow("Cancelar") = False
            oRow("Importe") = oRow("Saldo")
        Next

    End Sub

    Private Sub ListarCheques()

        lvLista.Items.Clear()

        For Each oRow As DataRow In dtDetalleCheque.Rows
            Dim item As ListViewItem = lvLista.Items.Add(oRow("Banco").ToString)
            item.SubItems.Add(oRow("NroCheque").ToString)
            item.SubItems.Add(oRow("Fec.").ToString)
            item.SubItems.Add(oRow("Comprobante").ToString)
            item.SubItems.Add(oRow("Dif").ToString)
            item.SubItems.Add(oRow("Fec Venc.").ToString)
            item.SubItems.Add(oRow("Local").ToString)
            item.SubItems.Add(oRow("Importe").ToString)
        Next

        CSistema.FormatoMoneda(lvLista, 7)

        CalcularTotales()

    End Sub

    Private Sub CalcularTotales()

        'Total Banco Local
        Dim TotalBancoLocal As Decimal = 0
        dtDetalleCheque.DefaultView.RowFilter = " BancoLocal = 'True' And Diferido = 'False' "
        TotalBancoLocal = CSistema.TotalesDataTable(dtDetalleCheque.DefaultView, "Importe")

        'Otros Bancos
        Dim TotalOtrosBancos As Decimal = 0
        dtDetalleCheque.DefaultView.RowFilter = " BancoLocal = 'False' And Diferido = 'False' "
        TotalOtrosBancos = CSistema.TotalesDataTable(dtDetalleCheque.DefaultView, "Importe")

        'Diferidos
        Dim TotalDiferidos As Decimal = 0
        dtDetalleCheque.DefaultView.RowFilter = " Diferido = 'True' "
        TotalDiferidos = CSistema.TotalesDataTable(dtDetalleCheque.DefaultView, "Importe")

        txtTotalBancoLocal.SetValue(TotalBancoLocal)
        txtTotalOtrosBancos.SetValue(TotalOtrosBancos)
        txtTotalDiferidos.SetValue(TotalDiferidos)

        txtTotal.SetValue(TotalBancoLocal + TotalOtrosBancos + TotalDiferidos)
        Total = TotalBancoLocal + TotalOtrosBancos + TotalDiferidos

        RaiseEvent UpdateTotal(New Object, New EventArgs)

    End Sub

    Sub EliminarRegistros()

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        For Each item As ListViewItem In lvLista.SelectedItems

            Dim NroCheque As String = item.SubItems(1).Text
            Dim Banco As String = item.SubItems(0).Text
            Dim ComprobanteVenta As String = item.SubItems(3).Text

            Dim Where As String = " Banco = '" & Banco & "' And NroCheque = '" & NroCheque & "' And Comprobante = '" & ComprobanteVenta & "' "
            For Each oRow As DataRow In dtDetalleCheque.Select(Where)
                dtDetalleCheque.Rows.Remove(oRow)
            Next

        Next

        ListarCheques()

    End Sub

    Sub VerDetalle()

        Dim frm As New frmRendicionChequesDetalle
        frm.dt = dtDetalleCheque
        frm.Comprobante = ""
        frm.Inicializar()
        frm.Show(Me)

    End Sub

    Sub Limpiar()

        IDTransaccion = 0
        Total = 0
        dtDetalleCheque.Rows.Clear()
        ListarCheques()

    End Sub

    Public Sub Guardar(ByVal vIDTransaccion As Integer)

        IDTransaccion = vIDTransaccion

        Dim i As Integer = 1

        For Each oRow As DataRow In dtDetalleCheque.Rows

            Dim param(-1) As SqlClient.SqlParameter

            'Entrada
            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", i, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Banco", oRow("Banco"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroCheque", oRow("NroCheque"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(oRow("Fecha"), True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@FechaVencimiento", CSistema.FormatoFechaBaseDatos(oRow("FechaVencimiento"), True, False), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDTransaccionVenta", oRow("IDTransaccionVenta"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", oRow("Importe"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Diferido", oRow("Diferido"), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@BancoLocal", oRow("BancoLocal"), ParameterDirection.Input)

            'Operacion
            CSistema.SetSQLParameter(param, "@Operacion", ERP.CSistema.NUMOperacionesRegistro.INS.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

            Dim MensajeRetorno As String = ""
            If CSistema.ExecuteStoreProcedure1(param, "SpDetalleRendicionLoteCheque", False, False, MensajeRetorno) = False Then

            End If

            i = i + 1

        Next

    End Sub

    Public Sub CargarDatos(ByVal vIDTransaccion As Integer)

        IDTransaccion = vIDTransaccion
        dtDetalleCheque = CSistema.ExecuteToDataTable("Select * From VDetalleRendicionLoteCheque Where IDTransaccion=" & IDTransaccion).Copy
        ListarCheques()

    End Sub

    Private Sub btnCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargar.Click
        CargarCheque()
    End Sub

    Private Sub lklVerDetalle_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklVerDetalle.LinkClicked
        VerDetalle()
    End Sub

    Private Sub lvLista_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvLista.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarRegistros()
        End If
    End Sub

    Private Sub lklEliminarCheque_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarCheque.LinkClicked
        EliminarRegistros()
    End Sub

End Class

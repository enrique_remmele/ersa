﻿Public Class frmRendicionLoteCargaCobranzaExtra
       'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio


    Public Property Procesado As Boolean = False
    Public Property Cliente As String
    Public Property Comprobante As String
    Public Property Importe As Decimal

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True



    End Sub

    Private Sub Guardar()

        ctrError.Clear()
        tsslEstado.Text = ""

        'Cliente
        If txtCliente.txt.Text.Trim = "" Then
            CSistema.MostrarError("Debe ingresar un nombre cliente!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Nro de Comprobante
        If txtComprobante.txt.Text.Trim = "" Then
            CSistema.MostrarError("El Nro del comprobante no es correcto!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        'Importe
        If txtImporte.ObtenerValor = 0 Then
            CSistema.MostrarError("El importe del cheque no es correcto!", ctrError, btnAceptar, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If

        Cliente = txtCliente.Texto
        Comprobante = txtComprobante.Texto
        Importe = txtImporte.ObtenerValor
        Procesado = True

        Me.Close()

    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class
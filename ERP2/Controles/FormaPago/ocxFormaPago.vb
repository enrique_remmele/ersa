﻿Public Class ocxFormaPago

    'CLASES
    Dim CSistema As New CSistema
    Dim vdecimales As Boolean
    Protected CData As New CData

    'ENUMERACIONES
    Enum ENUMFormaPago
        Efectivo = 2
        Cheque = 3
        ChequeTercero = 4
        Documento = 5
        Tarjeta = 6
    End Enum

    'EVENTOS
    Public Event ImporteModificado(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ListarCheques(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ItemModificado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow)
    Public Event RegistroInsertado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow, ByVal Tipo As ENUMFormaPago)
    Public Event RegistroEliminado(ByVal sender As Object, ByVal e As EventArgs, ByVal oRow As DataRow, ByVal Tipo As ENUMFormaPago)

    'PROPIEDADES
    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value

            btnCheque.Enabled = Not value
            btnChequeTerceros.Enabled = Not value
            btnDocumentos.Enabled = Not value
            btnEfectivo.Enabled = Not value
            btnTarjeta.Enabled = Not value
            btnNC.Enabled = Not value
            lklEliminarFormaPago.Enabled = Not value
            lklNuevoChequeCliente.Enabled = Not value

        End Set
    End Property

    Private RowClienteValue As DataRow
    Public Property RowCliente() As DataRow
        Get
            Return RowClienteValue
        End Get
        Set(ByVal value As DataRow)
            RowClienteValue = value
        End Set
    End Property

    Private dtFormaPagoValue As DataTable
    Public Property dtFormaPago() As DataTable
        Get
            Return dtFormaPagoValue
        End Get
        Set(ByVal value As DataTable)
            dtFormaPagoValue = value
        End Set
    End Property

    Private dtChequesValue As DataTable
    Public Property dtCheques() As DataTable
        Get
            Return dtChequesValue
        End Get
        Set(ByVal value As DataTable)
            dtChequesValue = value
        End Set
    End Property

    Private dtChequesTerceroValue As DataTable
    Public Property dtChequesTercero() As DataTable
        Get
            Return dtChequesTerceroValue
        End Get
        Set(ByVal value As DataTable)
            dtChequesTerceroValue = value
        End Set
    End Property

    Private dtEfectivoValue As DataTable
    Public Property dtEfectivo() As DataTable
        Get
            Return dtEfectivoValue
        End Get
        Set(ByVal value As DataTable)
            dtEfectivoValue = value
        End Set
    End Property

    Private dtDocumentoValue As DataTable
    Public Property dtDocumento() As DataTable
        Get
            Return dtDocumentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDocumentoValue = value
        End Set
    End Property

    Private dtTarjetaValue As DataTable
    Public Property dtTarjeta() As DataTable
        Get
            Return dtTarjetaValue
        End Get
        Set(ByVal value As DataTable)
            dtTarjetaValue = value
        End Set
    End Property

    Private dtVentaRetencionValue As DataTable
    Public Property dtVentaRetencion() As DataTable
        Get
            Return dtVentaRetencionValue
        End Get
        Set(ByVal value As DataTable)
            dtVentaRetencionValue = value
        End Set
    End Property

    Private dtVentaNotaCreditoValue As DataTable
    Public Property dtVentaNotaCredito() As DataTable
        Get
            Return dtVentaNotaCreditoValue
        End Get
        Set(ByVal value As DataTable)
            dtVentaNotaCreditoValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal

        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private TotalOperValue As Decimal
    Public Property TotalOper() As Decimal

        Get
            Return TotalOperValue
        End Get
        Set(ByVal value As Decimal)
            TotalOperValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal

        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As Decimal
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As Decimal)
            ComprobanteValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private TipoValue As String
    Public Property Tipo() As String
        Get
            Return TipoValue
        End Get
        Set(ByVal value As String)
            TipoValue = value
        End Set
    End Property

    Private FormValue As Form
    Public Property Form() As Form
        Get
            Return FormValue
        End Get
        Set(ByVal value As Form)
            FormValue = value
        End Set
    End Property

    Private DecimalesOperValue As Boolean
    Public Property DecimalesOper() As Boolean
        Get
            Return DecimalesOperValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesOperValue = value
        End Set
    End Property

    Private CotizacionValue As Double
    Public Property Cotizacion() As Double
        Get
            Return CotizacionValue
        End Get
        Set(ByVal value As Double)
            CotizacionValue = value
        End Set
    End Property

    Private IDMonedavalue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedavalue
        End Get
        Set(ByVal value As Integer)
            IDMonedavalue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        CargarInformacion()
        txtTotalFormaPagoOper.Decimales = DecimalesOper
    End Sub

    Sub CargarInformacion()

        'Crear estructura de Cheque
        dtCheques = CSistema.ExecuteToDataTable("Select Top(0) * From VChequesClienteFormaPago ").Clone

        'Crear estructura de Cheque de Tercero
        dtChequesTercero = dtCheques.Clone

        'Crear estructura de Efectivo
        dtEfectivo = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleEfectivo ").Clone

        'Crear estructura de Documentos
        dtDocumento = CSistema.ExecuteToDataTable("Select Top(0) * From VFormaPagoDocumento ").Clone

        'Crear estructura de la Forma de Pago

        dtFormaPago = CSistema.ExecuteToDataTable("Select Top(0) * From VFormaPagoCobranzaCredito  Order By  ID ASC").Clone

        'Crear estructura de Tarjeta
        dtTarjeta = CSistema.ExecuteToDataTable("Select Top(0) * From FormaPagoTarjeta ").Clone

        'Crear estructura de NC
        dtVentaNotaCredito = CSistema.ExecuteToDataTable("Select top(0) 'Sel'='False', IDTransaccion, 'ID'=0,Referencia,Cliente, 'NotaCredito'=Comprobante,Fecha,Saldo as 'Importe' From VNotaCredito").Clone

    End Sub

    Sub Reset()

        dtFormaPago.Rows.Clear()
        dtEfectivo.Rows.Clear()
        dtCheques.Rows.Clear()
        dtDocumento.Rows.Clear()
        dtTarjeta.Rows.Clear()
        dtVentaNotaCredito.Rows.Clear()
        ListarFormaPago()
        ListarChequesPendientes()
        ListarNotaCreditoPendiente()

    End Sub

    Sub SeleccionarCheque()

        Dim frm As New frmSeleccionChequeCliente
        frm.Decimales = DecimalesOper
        frm.Cotizacion = Cotizacion
        frm.Text = "Seleccionar cheques de cliente"
        frm.RowCliente = RowCliente
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        'Eliminar todas las configuraciones
        If dtCheques.Rows.Count > 0 Then
            For Each oRow As DataRow In dtCheques.Select("Sel='True'")
                RaiseEvent RegistroEliminado(New Object, New EventArgs, oRow, ENUMFormaPago.Cheque)
            Next
        End If

        frm.dt = dtCheques
        'frm.ShowDialog(Me)
        FGMostrarFormulario(Me.ParentForm, frm, "Seleccionar Cheques", FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)
        dtCheques = frm.dt
        If dtCheques.Rows.Count > 0 Then

            For Each oRow As DataRow In dtCheques.Select("Sel='True'")
                RaiseEvent RegistroInsertado(New Object, New EventArgs, oRow, ENUMFormaPago.Cheque)
            Next

        End If

        btnDocumentos.Focus()

        ListarFormaPago()

    End Sub

    Sub SeleccionarChequeTercero()

        Dim frm As New frmSeleccionChequeTercero
        frm.Text = "Seleccionar cheques de terceros"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.dt = dtChequesTercero.Clone
        frm.ID = dtChequesTercero.Rows.Count + 1
        'frm.ShowDialog(Me)
        FGMostrarFormulario(Me.ParentForm, frm, "Seleccionar Cheques", FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.dt Is Nothing Then
            Exit Sub
        End If

        'dtChequesTercero = frm.dt.Clone

        'Cargamos los Seleccionados
        For Each oRow As DataRow In frm.dt.Rows

            If CBool(oRow("Sel").ToString) = True Then
                dtChequesTercero.ImportRow(oRow)
                RaiseEvent RegistroInsertado(New Object, New EventArgs, oRow, ENUMFormaPago.ChequeTercero)
            End If

        Next

        ListarFormaPago()

    End Sub

    Sub SeleccionarEfectivo()
        Dim frm As New frmSeleccionEfectivo
        frm.Text = "Seleccionar el tipo de efectivo"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Fecha = Fecha
        frm.Comprobante = Comprobante
        frm.Saldo = Saldo
        frm.IDMoneda = IDMoneda
        frm.dt = dtEfectivo.Clone
        frm.ID = dtEfectivo.Rows.Count + 1
        frm.Tipo = Tipo
        'frm.ShowDialog(Me)
        FGMostrarFormulario(Me.ParentForm, frm, "Seleccionar el tipo de efectivo", FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.dt.Rows.Count > 0 Then
            If frm.dt.Rows(0)("Importe") Then

            End If
            dtEfectivo.ImportRow(frm.dt.Rows(0))
            RaiseEvent RegistroInsertado(New Object, New EventArgs, frm.dt.Rows(0), ENUMFormaPago.Efectivo)

        End If

        btnCheque.Focus()

        ListarFormaPago()

    End Sub

    Public Sub ImportarRetencion(ByVal RowRetencion As DataRow, ByVal ID As Integer)

        Dim ComprobanteRetencion As String = ""
        Dim ComprobanteRetencion0 As Integer
        Dim ComprobanteRetencion1 As Integer
        Dim ComprobanteRetencion2 As Integer
        Dim SplitComprobanteRetencion As String() = RowRetencion("Comprobante").ToString.Split("-")
        ComprobanteRetencion0 = CInt(SplitComprobanteRetencion(0))
        ComprobanteRetencion1 = CInt(SplitComprobanteRetencion(1))
        ComprobanteRetencion2 = CInt(SplitComprobanteRetencion(2))
        ComprobanteRetencion = ComprobanteRetencion0 & "-" & ComprobanteRetencion1 & "-" & ComprobanteRetencion2
        RowRetencion("Comprobante") = ComprobanteRetencion
        Dim oRow As DataRow = dtDocumento.NewRow
        oRow("IDTransaccion") = 0
        oRow("ID") = ID
        oRow("IDTipoComprobante") = vgConfiguraciones("CobranzaCreditoTipoComprobanteRetencion")
        oRow("TipoComprobante") = "RETEC"
        oRow("Comprobante") = RowRetencion("Comprobante")
        oRow("Fecha") = RowRetencion("Fecha Emisión")
        oRow("Importe") = CSistema.FormatoNumeroBaseDatos(RowRetencion("Retenido IVA"), False)
        oRow("IDMoneda") = 1
        oRow("Moneda") = "GS"
        oRow("ImporteMoneda") = CSistema.FormatoMoneda(RowRetencion("Retenido IVA"), False)
        oRow("Cotizacion") = 1
        oRow("Observacion") = ""

        oRow("Banco") = ""
        oRow("IDBanco") = 0
        Dim dtTipoComprobante As DataTable = CData.GetTable("VTipoComprobante")

        For Each TipoComprobanteRow As DataRow In dtTipoComprobante.Select("ID=" & vgConfiguraciones("CobranzaCreditoTipoComprobanteRetencion"))
            oRow("Restar") = TipoComprobanteRow("Restar")
        Next

        ' For Each VentaGridRow As DataRow In dtVentaRetencion.Rows

        Dim Comprobante As String() = RowRetencion("Comprobante Venta").ToString.Split("-")
            Dim NroComprobante As String = Comprobante(2)
            Dim Nro As Integer = CInt(NroComprobante)
            'For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Comprobante = '" & Row("Comprobante Venta") & "'")
            For Each VentaRow As DataRow In dtVentaRetencion.Select("ComprobanteVenta like '%" & Nro & " %'")

                'Actualizamos la venta
                'For Each VentaRow As DataRow In dtVenta.Select("NroCo=" & VentaGridRow.Cells("IDTransaccionVenta").Value & "")

                VentaRow("Sel") = True
                VentaRow("ID") = ID
                VentaRow("ComprobanteRetencion") = RowRetencion("Comprobante")
                VentaRow("Porcentaje") = 30
                VentaRow("Importe") = CSistema.FormatoNumeroBaseDatos(RowRetencion("Retenido IVA"), True)

                'Aplicamos
                VentaRow("Aplicado") = True

            Next
            'siguiente:
            '        Next

            dtDocumento.Rows.Add(oRow)

        ListarFormaPago()


    End Sub

    Sub SeleccionarDocumento()

        Dim frm As New frmSeleccionFormaPagoDocumento
        frm.DecimalesOper = DecimalesOper
        frm.Text = "Documentos de Pago y/o Cobro"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Fecha = Today.Date
        frm.Saldo = Saldo
        frm.IDMoneda = IDMonedavalue
        frm.dt = dtDocumento.Clone
        frm.ID = dtDocumento.Rows.Count + 1
        frm.PreseleccionarRetenciones = True

        'Estructura de Retencion
        frm.dtVenta = dtVentaRetencion.Copy
        FGMostrarFormulario(Me.ParentForm, frm, "Documentos de Pago y/o Cobro", FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.dt.Rows.Count > 0 Then
            dtDocumento.ImportRow(frm.dt.Rows(0))
            RaiseEvent RegistroInsertado(New Object, New EventArgs, frm.dt.Rows(0), ENUMFormaPago.Documento)
        End If

        btnChequeTerceros.Focus()

        ListarFormaPago()

        'Asignamos los valores de la tabla de relacion, si es que es retencion
        dtVentaRetencion = frm.dtVenta.Copy

    End Sub

    Sub SeleccionarTarjeta()

        Dim frm As New frmFormaPagoTipoTarjeta
        frm.Text = "Seleccionar tarjeta"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Fecha = Fecha
        frm.Comprobante = Comprobante
        frm.Saldo = Saldo
        frm.IDMoneda = IDMonedavalue
        frm.dt = dtTarjeta.Clone
        frm.ID = dtTarjeta.Rows.Count + 1
        frm.Tipo = Tipo
        'frm.ShowDialog(Me)
        FGMostrarFormulario(Me.ParentForm, frm, "Seleccionar tarjeta", FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.dt.Rows.Count > 0 Then

            If frm.dt.Rows(0)("Importe") Then

            End If

            dtTarjeta.ImportRow(frm.dt.Rows(0))
            RaiseEvent RegistroInsertado(New Object, New EventArgs, frm.dt.Rows(0), ENUMFormaPago.Tarjeta)

        End If

        btnEfectivo.Focus()

        ListarFormaPago()

    End Sub

    Sub SeleccionarDocumentoNC()

        ListarNotaCreditoPendiente()

        Dim frm As New frmSeleccionFormaPagoDocumentoNC
        frm.DecimalesOper = DecimalesOper
        frm.Text = "Documentos de Pago y/o Cobro"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Fecha = Today.Date
        frm.Saldo = Saldo
        frm.IDMoneda = IDMonedavalue
        frm.dt = dtDocumento.Clone
        frm.ID = dtDocumento.Rows.Count + 1
        frm.PreseleccionarRetenciones = True

        'Estructura de Retencion
        frm.dtNC = dtVentaNotaCredito.Copy
        FGMostrarFormulario(Me.ParentForm, frm, "Documentos de Pago y/o Cobro", FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.dt.Rows.Count > 0 Then
            dtDocumento = frm.dt.Copy
            RaiseEvent RegistroInsertado(New Object, New EventArgs, frm.dt.Rows(0), ENUMFormaPago.Documento)
        End If

        btnChequeTerceros.Focus()

        ListarFormaPago()

        'Asignamos los valores de la tabla de relacion, si es que es retencion
        dtVentaNotaCredito = frm.dtNC.Copy

    End Sub

    Sub ListarChequesPendientes()

        If RowCliente Is Nothing Then
            dtCheques.Rows.Clear()
            Exit Sub
        End If

        dtCheques = CSistema.ExecuteToDataTable("Select * From VChequesClienteFormaPago Where IDCliente=" & RowCliente("ID").ToString).Copy
        Dim CotizacionFechaCobranza As Integer
        CotizacionFechaCobranza = CSistema.CotizacionDelDiaFecha(2, 19, "'" & CSistema.FormatoFechaBaseDatos(FechaValue, True, False) & "'").ToString
        dtCheques.Columns.Add("CotizacionFechaCobranza")
        For Each oRow As DataRow In dtCheques.Select("IDMoneda= " & 2)
            oRow("CotizacionFechaCobranza") = CotizacionFechaCobranza
        Next

        dtFormaPago.Rows.Clear()

        ListarFormaPago()

    End Sub


    Sub ListarNotaCreditoPendiente()

        If RowCliente Is Nothing Then
            dtCheques.Rows.Clear()
            Exit Sub
        End If

        dtVentaNotaCredito = CSistema.ExecuteToDataTable("Select 'Sel'='False', IDTransaccion, 'ID'=0,Referencia,Cliente, 'NotaCredito'=Comprobante,Fecha,Saldo as 'Importe' From VNotaCredito Where Saldo > 0 and IDCliente = " & RowCliente("ID").ToString).Copy

        dtFormaPago.Rows.Clear()

        ListarFormaPago()

    End Sub

    Sub ListarFormaPago()

        dgw.Rows.Clear()
        dtFormaPago.Rows.Clear()

        'Agregar el Efectivo

        For Each oRow As DataRow In dtEfectivo.Rows
            Dim NewRow As DataRow = dtFormaPago.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("ID") = dtFormaPago.Rows.Count + 1
            'NewRow("FormaPago") = "EFECTIVO"
            NewRow("FormaPago") = oRow("TipoComprobante").ToString
            NewRow("Moneda") = oRow("Moneda").ToString
            NewRow("ImporteMoneda") = oRow("ImporteMoneda").ToString
            NewRow("Cambio") = oRow("Cotizacion").ToString
            NewRow("Banco") = "---"
            NewRow("Comprobante") = oRow("Comprobante").ToString
            NewRow("Tipo") = "---"
            NewRow("FechaEmision") = (oRow("Fecha").ToString)
            NewRow("Importe") = oRow("Importe").ToString
            NewRow("Restar") = False

            dtFormaPago.Rows.Add(NewRow)

        Next

        'Agregar los Cheques
        For Each oRow As DataRow In dtCheques.Rows

            If CBool(oRow("Sel").ToString) = True Then
                Dim NewRow As DataRow = dtFormaPago.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("ID") = dtFormaPago.Rows.Count + 1
                NewRow("FormaPago") = "CHEQUE"
                NewRow("Moneda") = oRow("Moneda").ToString
                NewRow("ImporteMoneda") = oRow("Importe").ToString
                NewRow("Cambio") = oRow("Cotizacion").ToString
                'NewRow("Cambio") = oRow("CotizacionHoy").ToString
                'NewRow("Cambio") = Cotizacion
                NewRow("Cambio") = Cotizacion
                NewRow("Banco") = oRow("Banco").ToString
                NewRow("Comprobante") = oRow("NroCheque").ToString
                NewRow("Tipo") = oRow("Tipo").ToString
                NewRow("FechaEmision") = oRow("Fecha").ToString
                NewRow("Vencimiento") = oRow("FechaVencimiento").ToString
                NewRow("Importe") = oRow("ImporteGS").ToString
                NewRow("Restar") = False

                dtFormaPago.Rows.Add(NewRow)

            End If

        Next

        'Agregar los Cheques de Terceros
        For Each oRow As DataRow In dtChequesTercero.Rows

            If CBool(oRow("Sel").ToString) = True Then
                Dim NewRow As DataRow = dtFormaPago.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("ID") = dtFormaPago.Rows.Count + 1
                NewRow("FormaPago") = "CHEQUE TERCERO"
                NewRow("Moneda") = oRow("Moneda").ToString
                NewRow("ImporteMoneda") = oRow("Importe").ToString
                NewRow("Cambio") = oRow("Cotizacion").ToString
                NewRow("Banco") = oRow("Banco").ToString
                NewRow("Comprobante") = oRow("NroCheque").ToString
                NewRow("Tipo") = oRow("Tipo").ToString
                NewRow("Importe") = oRow("Importe").ToString
                NewRow("Restar") = False

                dtFormaPago.Rows.Add(NewRow)

            End If

        Next

        'Agregar la Retención
        For Each oRow As DataRow In dtDocumento.Rows

            Dim NewRow As DataRow = dtFormaPago.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("ID") = dtFormaPago.Rows.Count + 1
            NewRow("FormaPago") = "DOCUMENTO"
            NewRow("FormaPago") = oRow("TipoComprobante").ToString
            NewRow("Moneda") = oRow("Moneda").ToString
            NewRow("ImporteMoneda") = oRow("ImporteMoneda").ToString
            NewRow("Cambio") = oRow("Cotizacion").ToString
            NewRow("Banco") = "---"
            NewRow("Comprobante") = oRow("Comprobante").ToString
            NewRow("Tipo") = "---"
            NewRow("Importe") = oRow("Importe").ToString
            NewRow("FechaEmision") = oRow("Fecha").ToString
            NewRow("Restar") = CBool(oRow("Restar").ToString)
            NewRow("Banco") = oRow("Banco").ToString
            NewRow("IdBanco") = oRow("IDBanco").ToString

            dtFormaPago.Rows.Add(NewRow)

        Next

        'Agregar las Tarjetas
        For Each oRow As DataRow In dtTarjeta.Rows
            Dim NewRow As DataRow = dtFormaPago.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("ID") = dtFormaPago.Rows.Count + 1
            NewRow("FormaPago") = oRow("TipoComprobante").ToString
            NewRow("Moneda") = oRow("Moneda").ToString
            NewRow("ImporteMoneda") = oRow("ImporteMoneda").ToString
            NewRow("Cambio") = oRow("Cotizacion").ToString
            NewRow("Banco") = "---"
            NewRow("Comprobante") = oRow("Comprobante").ToString
            NewRow("FechaEmision") = oRow("Fecha").ToString
            NewRow("Tipo") = "---"
            NewRow("Importe") = oRow("Importe").ToString
            NewRow("Restar") = False

            dtFormaPago.Rows.Add(NewRow)

        Next

        'Cargar en la Lista
        For Each oRow As DataRow In dtFormaPago.Rows
            Dim Decimales As Boolean = False
            Dim vMoneda As String
            Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" referencia = '" & oRow("Moneda").ToString & "'", "VMoneda")("Decimales"))
            vMoneda = CSistema.RetornarValorString(CData.GetRow(" ID = '" & IDMoneda & "'", "VMoneda")("Referencia"))

            Dim Registro(13) As String
            Registro(0) = oRow("ID").ToString
            Registro(1) = oRow("FormaPago").ToString
            Registro(2) = oRow("Moneda").ToString
            Registro(3) = CSistema.FormatoMoneda3Decimales(oRow("ImporteMoneda").ToString, Decimales) 'importe del efectivo
            Registro(4) = CSistema.FormatoMoneda3Decimales(oRow("Cambio").ToString)
            Registro(5) = oRow("Banco").ToString
            Registro(6) = oRow("Comprobante").ToString
            Registro(7) = oRow("Tipo").ToString
            Registro(8) = oRow("FechaEmision").ToString
            Registro(9) = oRow("Vencimiento").ToString
            Registro(13) = oRow("Importe").ToString 'Gs
            If vMoneda = oRow("Moneda").ToString Then
                Registro(10) = CSistema.FormatoMoneda3Decimales(oRow("ImporteMoneda").ToString, Decimales) 'importe del efectivo
            Else
                Registro(10) = CSistema.FormatoMoneda3Decimales(oRow("Importe").ToString / Cotizacion, DecimalesOper) 'importe Cobranza
            End If

            Registro(11) = CSistema.FormatoMoneda(oRow("IDTransaccion").ToString)

            dgw.Rows.Add(Registro)

        Next

        CalcularTotales()
        RaiseEvent ImporteModificado(New Object, New EventArgs)

    End Sub

    Public Sub ListarFormaPago(ByVal IDTransaccion As Integer)

        dgw.Rows.Clear()
        If dtFormaPago IsNot Nothing Then
            dtFormaPago.Rows.Clear()
        End If

        dtFormaPago = CSistema.ExecuteToDataTable("Select * From VFormaPagoCobranzaCredito Where IDTransaccion = " & IDTransaccion & " Order By ID ASC ").Copy
        Dim IDMonedaFP As Integer = 0
        Dim Decimales As Boolean = False

        'Cargar en la Lista
        For Each oRow As DataRow In dtFormaPago.Rows

            Try
                IDMonedaFP = CSistema.RetornarValorInteger(oRow("IDMoneda").ToString)
                Decimales = CSistema.RetornarValorBoolean(CData.GetRow(" ID = " & IDMonedaFP, "VMoneda")("Decimales"))

            Catch ex As Exception

            End Try

            Dim Registro(13) As String
            Dim vMoneda As String
            vMoneda = CSistema.RetornarValorString(CData.GetRow(" ID = '" & IDMoneda & "'", "VMoneda")("Referencia"))
            Registro(0) = oRow("ID").ToString
            Registro(1) = oRow("CodigoTipoComprobante").ToString
            Registro(2) = oRow("Moneda").ToString
            Registro(3) = CSistema.FormatoMoneda3Decimales(oRow("ImporteMoneda").ToString, Decimales)
            Registro(4) = CSistema.FormatoMoneda3Decimales(oRow("Cambio").ToString)
            Registro(5) = oRow("Banco").ToString
            Registro(6) = oRow("Comprobante").ToString
            Registro(7) = oRow("Tipo").ToString
            Registro(8) = oRow("FechaEmision").ToString
            Registro(9) = oRow("Vencimiento").ToString
            If vMoneda = oRow("Moneda").ToString Then
                Registro(10) = CSistema.FormatoMoneda3Decimales(oRow("ImporteMoneda").ToString, DecimalesOper)  'Importe operacion
            Else
                Registro(10) = CSistema.FormatoMoneda3Decimales(oRow("Importe").ToString / Cotizacion, DecimalesOper) 'Importe operacion
            End If

            Registro(13) = CSistema.FormatoMoneda3Decimales(oRow("Importe").ToString)
            Registro(11) = CSistema.FormatoMoneda3Decimales(oRow("IDTransaccion").ToString)
            Registro(12) = oRow("FormaPago").ToString

            dgw.Rows.Add(Registro)

            If Decimales = True Then
                dgw.Columns("colImporteMoneda").DefaultCellStyle.Format = "N2"
                dgw.Columns("colImporte").DefaultCellStyle.Format = "N2"
            Else
                dgw.Columns("colImporteMoneda").DefaultCellStyle.Format = "N0"
                dgw.Columns("colImporte").DefaultCellStyle.Format = "N0"
            End If

        Next

        CalcularTotales()
        RaiseEvent ImporteModificado(New Object, New EventArgs)

    End Sub

    Sub EliminarFormaPago()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim ID As Integer = dgw.SelectedRows(0).Cells(0).Value

        For Each oRow As DataRow In dtFormaPago.Select(" ID=" & ID)

            Dim TipoComprobante As String = oRow("FormaPago").ToString
            Dim Comprobante As String = oRow("Comprobante").ToString

            'CHEQUES
            If TipoComprobante = "CHEQUE" Then
                Dim NroCheque As String = oRow("Comprobante").ToString
                Dim ChequeRow As DataRow = dtCheques.Select("NroCheque='" & NroCheque & "'")(0)
                ChequeRow("Sel") = False
                RaiseEvent RegistroEliminado(New Object, New EventArgs, ChequeRow, ENUMFormaPago.Cheque)
            End If

            'CHEQUES TERCERO
            If TipoComprobante = "CHEQUE TERCERO" Then
                Dim NroCheque As String = oRow("Comprobante").ToString
                Dim ChequeRow As DataRow = dtChequesTercero.Select("NroCheque='" & NroCheque & "'")(0)
                ChequeRow("Sel") = False
                RaiseEvent RegistroEliminado(New Object, New EventArgs, ChequeRow, ENUMFormaPago.ChequeTercero)
            End If

            'EFECTIVO
            If dtEfectivo.Select("TipoComprobante='" & TipoComprobante & "' And Comprobante='" & Comprobante & "'").GetLength(0) > 0 Then
                If dtEfectivo.Select("ID=" & ID).Count > 0 Then
                    Dim EfectivoRow As DataRow = dtEfectivo.Select("ID=" & ID)(0)
                    RaiseEvent RegistroEliminado(New Object, New EventArgs, EfectivoRow, ENUMFormaPago.Efectivo)
                    dtEfectivo.Rows.Remove(EfectivoRow)
                End If
            End If

            'DOCUMENTOS
            If dtDocumento.Select("TipoComprobante='" & TipoComprobante & "' And Comprobante='" & Comprobante & "'").GetLength(0) > 0 Then
                Dim DocumentoRow As DataRow = dtDocumento.Select("TipoComprobante='" & TipoComprobante & "' And Comprobante='" & Comprobante & "'")(0)

                'Retenciones
                If dtVentaRetencion.Select("ID='" & DocumentoRow("ID").ToString & "' And ComprobanteRetencion='" & Comprobante & "'").Count > 0 Then
                    For Each RetencionesRow As DataRow In dtVentaRetencion.Select("ID='" & DocumentoRow("ID").ToString & "' And ComprobanteRetencion='" & Comprobante & "'")
                        RetencionesRow("Sel") = False
                        RetencionesRow("Aplicado") = False
                        RetencionesRow("Importe") = RetencionesRow("TotalImpuesto") * (RetencionesRow("Porcentaje") / 100)
                    Next
                End If

                RaiseEvent RegistroEliminado(New Object, New EventArgs, DocumentoRow, ENUMFormaPago.Documento)
                dtDocumento.Rows.Remove(DocumentoRow)

            End If

            'TARJETA
            If dtTarjeta.Select("TipoComprobante='" & TipoComprobante & "' And Comprobante='" & Comprobante & "'").GetLength(0) > 0 Then
                Dim TarjetaRow As DataRow = dtTarjeta.Select("ID=" & ID)(0)
                RaiseEvent RegistroEliminado(New Object, New EventArgs, TarjetaRow, ENUMFormaPago.Tarjeta)
                dtTarjeta.Rows.Remove(TarjetaRow)
            End If

        Next


        ListarFormaPago()

    End Sub

    Sub CalcularTotales()

        Dim tmp As Decimal = 0
        Dim tmpOper As Decimal = 0
        txtTotalFormaPagoOper.Decimales = DecimalesOper

        Try
            'Sumar
            For Each oRow As DataRow In dtFormaPago.Rows
                Dim vMoneda As String
                vMoneda = CSistema.RetornarValorString(CData.GetRow(" ID = '" & IDMoneda & "'", "VMoneda")("Referencia"))
                If CBool(oRow("Restar")) = True Then
                    tmp = tmp - CDec(oRow("Importe").ToString)
                    tmpOper = tmpOper - CSistema.FormatoMoneda3Decimales((CDec(oRow("Importe").ToString) / Cotizacion), DecimalesOper)
                Else
                    tmp = tmp + CDec(oRow("Importe").ToString)
                    If vMoneda = oRow("Moneda").ToString Then
                        tmpOper = tmpOper + CSistema.FormatoMoneda3Decimales((CDec(oRow("ImporteMoneda").ToString)), DecimalesOper)
                    Else
                        tmpOper = tmpOper + CSistema.FormatoMoneda3Decimales((CDec(oRow("Importe").ToString) / Cotizacion), DecimalesOper)
                    End If

                End If
            Next

        Catch ex As Exception

        End Try

        txtTotalFormaPago.SetValue(tmp)
        Total = tmp
        txtTotalFormaPagoOper.SetValue(tmpOper)
        TotalOper = tmpOper

    End Sub


    Sub ModificarFormaPago()

        If dgw.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        If dgw.SelectedRows(0).Cells(11).Value = 0 Then
            Exit Sub
        End If

        Dim mnuContextMenu As New ContextMenuStrip()
        mnuContextMenu.Items.Add("Modificar")
        dgw.ContextMenuStrip = mnuContextMenu

        If dgw.SelectedRows(0).Cells("colFP").Value = "Documento" Then
            AddHandler mnuContextMenu.Click, AddressOf SeleccionarDocumentoModificar
        End If

        If dgw.SelectedRows(0).Cells("colFP").Value = "Efectivo" Then
            AddHandler mnuContextMenu.Click, AddressOf SeleccionarEfectivoModificar
        End If

        If dgw.SelectedRows(0).Cells("colFP").Value = "Tarjeta" Then
            AddHandler mnuContextMenu.Click, AddressOf SeleccionarTarjeta
        End If


    End Sub

    Sub SeleccionarDocumentoModificar()

        Dim frm As New frmSeleccionModificarFormaPagoDocumento
        frm.Text = "Modificar Documentos de Pago y/o Cobro"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        Dim vIDTransaccion As Integer = dgw.SelectedRows(0).Cells(11).Value
        Dim vID As Integer = dgw.SelectedRows(0).Cells(0).Value
        Dim FormaPago As String = ""
        Dim Where As String = ""


        If vIDTransaccion = 0 Then
            Exit Sub
        End If


        dtFormaPago = CSistema.ExecuteToDataTable("Select * From VFormaPagoCobranzaCredito Where IDTransaccion=" & vIDTransaccion & " And ID=" & vID)

        If dtFormaPago.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtFormaPago.Rows(0)

        FormaPago = oRow("FormaPago").ToString

        If FormaPago = "Documento" Then
            frm.IDTransaccion = oRow("IDTransaccion").ToString
            frm.txtID.txt.Text = oRow("ID").ToString
            frm.ShowDialog(Me)

            ListarFormaPago(vIDTransaccion)
        Else
            Exit Sub
        End If

    End Sub

    Sub SeleccionarEfectivoModificar()

        Dim frm As New frmSeleccionModificarFormaPagoEfectivo
        frm.Text = "Modificar Efectivo de Pago y/o Cobro"
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen

        Dim vIDTransaccion As Integer = dgw.SelectedRows(0).Cells(11).Value
        Dim vID As Integer = dgw.SelectedRows(0).Cells(0).Value
        Dim FormaPago As String = ""
        Dim Where As String = ""


        If vIDTransaccion = 0 Then
            Exit Sub
        End If


        dtFormaPago = CSistema.ExecuteToDataTable("Select * From VFormaPagoCobranzaCredito Where IDTransaccion=" & vIDTransaccion & " And ID=" & vID)

        If dtFormaPago.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dtFormaPago.Rows(0)

        FormaPago = oRow("FormaPago").ToString

        If FormaPago = "Efectivo" Then
            frm.IDTransaccion = oRow("IDTransaccion").ToString
            frm.txtID.txt.Text = oRow("ID").ToString
            frm.ShowDialog(Me)

            ListarFormaPago(vIDTransaccion)
        Else
            Exit Sub
        End If
    End Sub

    Private Sub btnCheque_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheque.Click
        SeleccionarCheque()
    End Sub

    Private Sub btnEfectivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEfectivo.Click
        SeleccionarEfectivo()
    End Sub

    Private Sub lklEliminarFormaPago_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklEliminarFormaPago.LinkClicked
        EliminarFormaPago()
    End Sub

    Private Sub lklNuevoChequeCliente_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNuevoChequeCliente.LinkClicked

        FGMostrarFormulario(Form, frmChequeCliente, "Ingreso de Cheques de Clientes", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, True, False)

        RaiseEvent ListarCheques(sender, e)
        ListarChequesPendientes()
    End Sub

    Private Sub btnChequeTerceros_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChequeTerceros.Click
        SeleccionarChequeTercero()
    End Sub

    Private Sub btnDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDocumentos.Click
        SeleccionarDocumento()
    End Sub

    Private Sub dgw_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgw.MouseDown
        'ModificarFormaPago()
    End Sub

    Private Sub btnTarjeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTarjeta.Click
        SeleccionarTarjeta()
    End Sub

    Private Sub dgw_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles dgw.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarFormaPago()
        End If
    End Sub

    Private Sub btnNC_Click(sender As Object, e As EventArgs) Handles btnNC.Click
        SeleccionarDocumentoNC()
    End Sub
End Class

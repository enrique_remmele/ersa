﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFormaPagoTipoTarjeta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblCambio = New System.Windows.Forms.Label()
        Me.gbxDatos = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtBoleta = New ERP.ocxTXTString()
        Me.txtFechaVencimientoTarjeta = New ERP.ocxTXTDate()
        Me.txtTerminacionTarjeta = New ERP.ocxTXTString()
        Me.cbxTipoTarjeta = New ERP.ocxCBX()
        Me.txtTipoDocumento = New ERP.ocxTXTString()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.txtImporteMoneda = New ERP.ocxTXTNumeric()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.gbxDatos.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tipo Documento:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(392, 74)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 15
        Me.lblID.Text = "ID:"
        '
        'lblCambio
        '
        Me.lblCambio.AutoSize = True
        Me.lblCambio.Location = New System.Drawing.Point(155, 101)
        Me.lblCambio.Name = "lblCambio"
        Me.lblCambio.Size = New System.Drawing.Size(45, 13)
        Me.lblCambio.TabIndex = 19
        Me.lblCambio.Text = "Cambio:"
        '
        'gbxDatos
        '
        Me.gbxDatos.Controls.Add(Me.Label5)
        Me.gbxDatos.Controls.Add(Me.txtBoleta)
        Me.gbxDatos.Controls.Add(Me.Label4)
        Me.gbxDatos.Controls.Add(Me.txtFechaVencimientoTarjeta)
        Me.gbxDatos.Controls.Add(Me.Label3)
        Me.gbxDatos.Controls.Add(Me.txtTerminacionTarjeta)
        Me.gbxDatos.Controls.Add(Me.Label2)
        Me.gbxDatos.Controls.Add(Me.cbxTipoTarjeta)
        Me.gbxDatos.Controls.Add(Me.txtTipoDocumento)
        Me.gbxDatos.Controls.Add(Me.Label1)
        Me.gbxDatos.Controls.Add(Me.btnCancelar)
        Me.gbxDatos.Controls.Add(Me.btnAceptar)
        Me.gbxDatos.Controls.Add(Me.lblID)
        Me.gbxDatos.Controls.Add(Me.txtID)
        Me.gbxDatos.Controls.Add(Me.lblComprobante)
        Me.gbxDatos.Controls.Add(Me.txtObservacion)
        Me.gbxDatos.Controls.Add(Me.lblMoneda)
        Me.gbxDatos.Controls.Add(Me.lblObservacion)
        Me.gbxDatos.Controls.Add(Me.cbxMoneda)
        Me.gbxDatos.Controls.Add(Me.txtCotizacion)
        Me.gbxDatos.Controls.Add(Me.lblImporte)
        Me.gbxDatos.Controls.Add(Me.txtImporteMoneda)
        Me.gbxDatos.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxDatos.Controls.Add(Me.lblCambio)
        Me.gbxDatos.Controls.Add(Me.txtComprobante)
        Me.gbxDatos.Controls.Add(Me.txtImporte)
        Me.gbxDatos.Controls.Add(Me.lblFecha)
        Me.gbxDatos.Controls.Add(Me.txtFecha)
        Me.gbxDatos.Location = New System.Drawing.Point(0, 0)
        Me.gbxDatos.Name = "gbxDatos"
        Me.gbxDatos.Size = New System.Drawing.Size(528, 185)
        Me.gbxDatos.TabIndex = 0
        Me.gbxDatos.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(41, 47)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Boleta Nº:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(320, 47)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Fecha vto. tarjeta:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(184, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Terminación Nº:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(250, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Tipo tarjeta:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(434, 151)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 27
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(353, 151)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 26
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(23, 74)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 11
        Me.lblComprobante.Text = "Comprobante:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(47, 101)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 17
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(26, 128)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 24
        Me.lblObservacion.Text = "Observacion:"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(250, 101)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 21
        Me.lblImporte.Text = "Importe:"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(250, 74)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 13
        Me.lblFecha.Text = "Fecha:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 187)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(527, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'txtBoleta
        '
        Me.txtBoleta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBoleta.Color = System.Drawing.Color.Empty
        Me.txtBoleta.Indicaciones = Nothing
        Me.txtBoleta.Location = New System.Drawing.Point(102, 43)
        Me.txtBoleta.Multilinea = False
        Me.txtBoleta.Name = "txtBoleta"
        Me.txtBoleta.Size = New System.Drawing.Size(76, 21)
        Me.txtBoleta.SoloLectura = False
        Me.txtBoleta.TabIndex = 6
        Me.txtBoleta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBoleta.Texto = ""
        '
        'txtFechaVencimientoTarjeta
        '
        Me.txtFechaVencimientoTarjeta.Color = System.Drawing.Color.Empty
        Me.txtFechaVencimientoTarjeta.Fecha = New Date(2013, 6, 27, 15, 21, 5, 127)
        Me.txtFechaVencimientoTarjeta.Location = New System.Drawing.Point(418, 43)
        Me.txtFechaVencimientoTarjeta.Name = "txtFechaVencimientoTarjeta"
        Me.txtFechaVencimientoTarjeta.PermitirNulo = False
        Me.txtFechaVencimientoTarjeta.Size = New System.Drawing.Size(91, 20)
        Me.txtFechaVencimientoTarjeta.SoloLectura = False
        Me.txtFechaVencimientoTarjeta.TabIndex = 10
        '
        'txtTerminacionTarjeta
        '
        Me.txtTerminacionTarjeta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTerminacionTarjeta.Color = System.Drawing.Color.Empty
        Me.txtTerminacionTarjeta.Indicaciones = Nothing
        Me.txtTerminacionTarjeta.Location = New System.Drawing.Point(273, 43)
        Me.txtTerminacionTarjeta.Multilinea = False
        Me.txtTerminacionTarjeta.Name = "txtTerminacionTarjeta"
        Me.txtTerminacionTarjeta.Size = New System.Drawing.Size(41, 21)
        Me.txtTerminacionTarjeta.SoloLectura = False
        Me.txtTerminacionTarjeta.TabIndex = 8
        Me.txtTerminacionTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTerminacionTarjeta.Texto = ""
        '
        'cbxTipoTarjeta
        '
        Me.cbxTipoTarjeta.CampoWhere = Nothing
        Me.cbxTipoTarjeta.CargarUnaSolaVez = False
        Me.cbxTipoTarjeta.DataDisplayMember = Nothing
        Me.cbxTipoTarjeta.DataFilter = Nothing
        Me.cbxTipoTarjeta.DataOrderBy = Nothing
        Me.cbxTipoTarjeta.DataSource = Nothing
        Me.cbxTipoTarjeta.DataValueMember = Nothing
        Me.cbxTipoTarjeta.FormABM = Nothing
        Me.cbxTipoTarjeta.Indicaciones = Nothing
        Me.cbxTipoTarjeta.Location = New System.Drawing.Point(319, 16)
        Me.cbxTipoTarjeta.Name = "cbxTipoTarjeta"
        Me.cbxTipoTarjeta.SeleccionObligatoria = True
        Me.cbxTipoTarjeta.Size = New System.Drawing.Size(190, 21)
        Me.cbxTipoTarjeta.SoloLectura = False
        Me.cbxTipoTarjeta.TabIndex = 4
        Me.cbxTipoTarjeta.Texto = ""
        '
        'txtTipoDocumento
        '
        Me.txtTipoDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoDocumento.Color = System.Drawing.Color.Empty
        Me.txtTipoDocumento.Indicaciones = Nothing
        Me.txtTipoDocumento.Location = New System.Drawing.Point(161, 16)
        Me.txtTipoDocumento.Multilinea = False
        Me.txtTipoDocumento.Name = "txtTipoDocumento"
        Me.txtTipoDocumento.Size = New System.Drawing.Size(78, 21)
        Me.txtTipoDocumento.SoloLectura = False
        Me.txtTipoDocumento.TabIndex = 2
        Me.txtTipoDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoDocumento.Texto = ""
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(418, 70)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(91, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 16
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(102, 124)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(407, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 25
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(102, 97)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(53, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 18
        Me.cbxMoneda.Texto = ""
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(200, 97)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(39, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 20
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'txtImporteMoneda
        '
        Me.txtImporteMoneda.Color = System.Drawing.Color.Empty
        Me.txtImporteMoneda.Decimales = False
        Me.txtImporteMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMoneda.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporteMoneda.Location = New System.Drawing.Point(319, 97)
        Me.txtImporteMoneda.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporteMoneda.Name = "txtImporteMoneda"
        Me.txtImporteMoneda.Size = New System.Drawing.Size(91, 21)
        Me.txtImporteMoneda.SoloLectura = False
        Me.txtImporteMoneda.TabIndex = 22
        Me.txtImporteMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteMoneda.Texto = "0"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(102, 16)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(53, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = ""
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(102, 70)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(137, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 12
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(418, 97)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(91, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 23
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 6, 27, 15, 21, 5, 127)
        Me.txtFecha.Location = New System.Drawing.Point(319, 70)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(67, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 14
        '
        'frmFormaPagoTipoTarjeta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(527, 209)
        Me.Controls.Add(Me.gbxDatos)
        Me.Controls.Add(Me.StatusStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmFormaPagoTipoTarjeta"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarjeta"
        Me.gbxDatos.ResumeLayout(False)
        Me.gbxDatos.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTipoDocumento As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents txtImporteMoneda As ERP.ocxTXTNumeric
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblCambio As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents gbxDatos As System.Windows.Forms.GroupBox
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtFechaVencimientoTarjeta As ERP.ocxTXTDate
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTerminacionTarjeta As ERP.ocxTXTString
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoTarjeta As ERP.ocxCBX
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtBoleta As ERP.ocxTXTString
End Class

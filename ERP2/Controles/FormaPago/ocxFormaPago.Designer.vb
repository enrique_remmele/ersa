﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxFormaPago
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lklNuevoChequeCliente = New System.Windows.Forms.LinkLabel()
        Me.btnChequeTerceros = New System.Windows.Forms.Button()
        Me.btnCheque = New System.Windows.Forms.Button()
        Me.btnEfectivo = New System.Windows.Forms.Button()
        Me.lblTotalFormaPago = New System.Windows.Forms.Label()
        Me.lklEliminarFormaPago = New System.Windows.Forms.LinkLabel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFormaPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporteMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colCambio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colComprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFechaEmision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colFP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colImporteGs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnDocumentos = New System.Windows.Forms.Button()
        Me.btnTarjeta = New System.Windows.Forms.Button()
        Me.btnNC = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalFormaPago = New ERP.ocxTXTNumeric()
        Me.txtTotalFormaPagoOper = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'lklNuevoChequeCliente
        '
        Me.lklNuevoChequeCliente.Location = New System.Drawing.Point(135, 0)
        Me.lklNuevoChequeCliente.Name = "lklNuevoChequeCliente"
        Me.lklNuevoChequeCliente.Size = New System.Drawing.Size(144, 23)
        Me.lklNuevoChequeCliente.TabIndex = 1
        Me.lklNuevoChequeCliente.TabStop = True
        Me.lklNuevoChequeCliente.Text = "Nuevo cheque del cliente"
        Me.lklNuevoChequeCliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnChequeTerceros
        '
        Me.btnChequeTerceros.Location = New System.Drawing.Point(123, 3)
        Me.btnChequeTerceros.Name = "btnChequeTerceros"
        Me.btnChequeTerceros.Size = New System.Drawing.Size(115, 22)
        Me.btnChequeTerceros.TabIndex = 0
        Me.btnChequeTerceros.Text = "Cheque de terceros"
        Me.btnChequeTerceros.UseVisualStyleBackColor = True
        '
        'btnCheque
        '
        Me.btnCheque.Location = New System.Drawing.Point(80, 3)
        Me.btnCheque.Name = "btnCheque"
        Me.btnCheque.Size = New System.Drawing.Size(71, 22)
        Me.btnCheque.TabIndex = 1
        Me.btnCheque.Text = "Cheque"
        Me.btnCheque.UseVisualStyleBackColor = True
        '
        'btnEfectivo
        '
        Me.btnEfectivo.Location = New System.Drawing.Point(3, 3)
        Me.btnEfectivo.Name = "btnEfectivo"
        Me.btnEfectivo.Size = New System.Drawing.Size(71, 22)
        Me.btnEfectivo.TabIndex = 0
        Me.btnEfectivo.Text = "Efectivo"
        Me.btnEfectivo.UseVisualStyleBackColor = True
        '
        'lblTotalFormaPago
        '
        Me.lblTotalFormaPago.Location = New System.Drawing.Point(62, 0)
        Me.lblTotalFormaPago.Name = "lblTotalFormaPago"
        Me.lblTotalFormaPago.Size = New System.Drawing.Size(58, 23)
        Me.lblTotalFormaPago.TabIndex = 0
        Me.lblTotalFormaPago.Text = "Total Gs:"
        Me.lblTotalFormaPago.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lklEliminarFormaPago
        '
        Me.lklEliminarFormaPago.Location = New System.Drawing.Point(3, 0)
        Me.lklEliminarFormaPago.Name = "lklEliminarFormaPago"
        Me.lklEliminarFormaPago.Size = New System.Drawing.Size(126, 23)
        Me.lklEliminarFormaPago.TabIndex = 0
        Me.lklEliminarFormaPago.TabStop = True
        Me.lklEliminarFormaPago.Text = "Eliminar forma de pago"
        Me.lklEliminarFormaPago.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 247.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 1, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(810, 358)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        Me.dgw.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colID, Me.colFormaPago, Me.colMoneda, Me.colImporteMoneda, Me.colCambio, Me.colBanco, Me.colComprobante, Me.colTipo, Me.colFechaEmision, Me.colVencimiento, Me.colImporte, Me.colIDTransaccion, Me.colFP, Me.colImporteGs})
        Me.TableLayoutPanel1.SetColumnSpan(Me.dgw, 2)
        Me.dgw.Location = New System.Drawing.Point(3, 37)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(786, 285)
        Me.dgw.TabIndex = 2
        Me.dgw.TabStop = False
        '
        'colID
        '
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Visible = False
        Me.colID.Width = 20
        '
        'colFormaPago
        '
        Me.colFormaPago.HeaderText = "Forma Pago"
        Me.colFormaPago.Name = "colFormaPago"
        Me.colFormaPago.ReadOnly = True
        '
        'colMoneda
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colMoneda.DefaultCellStyle = DataGridViewCellStyle2
        Me.colMoneda.HeaderText = "Mon"
        Me.colMoneda.Name = "colMoneda"
        Me.colMoneda.ReadOnly = True
        Me.colMoneda.Width = 40
        '
        'colImporteMoneda
        '
        Me.colImporteMoneda.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colImporteMoneda.DefaultCellStyle = DataGridViewCellStyle3
        Me.colImporteMoneda.HeaderText = "Importe"
        Me.colImporteMoneda.Name = "colImporteMoneda"
        Me.colImporteMoneda.ReadOnly = True
        '
        'colCambio
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colCambio.DefaultCellStyle = DataGridViewCellStyle4
        Me.colCambio.HeaderText = "Cambio"
        Me.colCambio.Name = "colCambio"
        Me.colCambio.ReadOnly = True
        Me.colCambio.Width = 50
        '
        'colBanco
        '
        Me.colBanco.HeaderText = "Banco"
        Me.colBanco.Name = "colBanco"
        Me.colBanco.ReadOnly = True
        Me.colBanco.Width = 55
        '
        'colComprobante
        '
        Me.colComprobante.HeaderText = "Comprobante"
        Me.colComprobante.Name = "colComprobante"
        Me.colComprobante.ReadOnly = True
        Me.colComprobante.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colComprobante.Width = 68
        '
        'colTipo
        '
        Me.colTipo.HeaderText = "Tipo"
        Me.colTipo.Name = "colTipo"
        Me.colTipo.ReadOnly = True
        Me.colTipo.Width = 60
        '
        'colFechaEmision
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colFechaEmision.DefaultCellStyle = DataGridViewCellStyle5
        Me.colFechaEmision.HeaderText = "Fecha"
        Me.colFechaEmision.Name = "colFechaEmision"
        Me.colFechaEmision.ReadOnly = True
        Me.colFechaEmision.Width = 70
        '
        'colVencimiento
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.colVencimiento.DefaultCellStyle = DataGridViewCellStyle6
        Me.colVencimiento.HeaderText = "Venc."
        Me.colVencimiento.Name = "colVencimiento"
        Me.colVencimiento.ReadOnly = True
        Me.colVencimiento.Width = 70
        '
        'colImporte
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colImporte.DefaultCellStyle = DataGridViewCellStyle7
        Me.colImporte.HeaderText = "Importe"
        Me.colImporte.Name = "colImporte"
        Me.colImporte.ReadOnly = True
        '
        'colIDTransaccion
        '
        Me.colIDTransaccion.HeaderText = "IDTransaccion"
        Me.colIDTransaccion.Name = "colIDTransaccion"
        Me.colIDTransaccion.ReadOnly = True
        Me.colIDTransaccion.Visible = False
        Me.colIDTransaccion.Width = 5
        '
        'colFP
        '
        Me.colFP.HeaderText = "FP"
        Me.colFP.Name = "colFP"
        Me.colFP.ReadOnly = True
        Me.colFP.Visible = False
        '
        'colImporteGs
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N0"
        Me.colImporteGs.DefaultCellStyle = DataGridViewCellStyle8
        Me.colImporteGs.HeaderText = "Importe Gs"
        Me.colImporteGs.Name = "colImporteGs"
        Me.colImporteGs.ReadOnly = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEfectivo)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCheque)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnDocumentos)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnTarjeta)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnNC)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(557, 28)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'btnDocumentos
        '
        Me.btnDocumentos.Location = New System.Drawing.Point(157, 3)
        Me.btnDocumentos.Name = "btnDocumentos"
        Me.btnDocumentos.Size = New System.Drawing.Size(82, 22)
        Me.btnDocumentos.TabIndex = 2
        Me.btnDocumentos.Text = "Documentos"
        Me.btnDocumentos.UseVisualStyleBackColor = True
        '
        'btnTarjeta
        '
        Me.btnTarjeta.Location = New System.Drawing.Point(245, 3)
        Me.btnTarjeta.Name = "btnTarjeta"
        Me.btnTarjeta.Size = New System.Drawing.Size(71, 22)
        Me.btnTarjeta.TabIndex = 3
        Me.btnTarjeta.Text = "Tarjeta"
        Me.btnTarjeta.UseVisualStyleBackColor = True
        '
        'btnNC
        '
        Me.btnNC.Location = New System.Drawing.Point(322, 3)
        Me.btnNC.Name = "btnNC"
        Me.btnNC.Size = New System.Drawing.Size(71, 22)
        Me.btnNC.TabIndex = 6
        Me.btnNC.Text = "NotaCredito"
        Me.btnNC.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.lklEliminarFormaPago)
        Me.FlowLayoutPanel2.Controls.Add(Me.lklNuevoChequeCliente)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 328)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(557, 27)
        Me.FlowLayoutPanel2.TabIndex = 3
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.txtTotalFormaPago)
        Me.FlowLayoutPanel3.Controls.Add(Me.lblTotalFormaPago)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtTotalFormaPagoOper)
        Me.FlowLayoutPanel3.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(566, 328)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(241, 27)
        Me.FlowLayoutPanel3.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(60, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 23)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Total Oper:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.btnChequeTerceros)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(566, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(241, 28)
        Me.FlowLayoutPanel4.TabIndex = 1
        '
        'txtTotalFormaPago
        '
        Me.txtTotalFormaPago.Color = System.Drawing.Color.Empty
        Me.txtTotalFormaPago.Decimales = True
        Me.txtTotalFormaPago.Indicaciones = Nothing
        Me.txtTotalFormaPago.Location = New System.Drawing.Point(126, 3)
        Me.txtTotalFormaPago.Name = "txtTotalFormaPago"
        Me.txtTotalFormaPago.Size = New System.Drawing.Size(112, 20)
        Me.txtTotalFormaPago.SoloLectura = True
        Me.txtTotalFormaPago.TabIndex = 1
        Me.txtTotalFormaPago.TabStop = False
        Me.txtTotalFormaPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalFormaPago.Texto = "0"
        '
        'txtTotalFormaPagoOper
        '
        Me.txtTotalFormaPagoOper.Color = System.Drawing.Color.Empty
        Me.txtTotalFormaPagoOper.Decimales = True
        Me.txtTotalFormaPagoOper.Indicaciones = Nothing
        Me.txtTotalFormaPagoOper.Location = New System.Drawing.Point(126, 29)
        Me.txtTotalFormaPagoOper.Name = "txtTotalFormaPagoOper"
        Me.txtTotalFormaPagoOper.Size = New System.Drawing.Size(112, 20)
        Me.txtTotalFormaPagoOper.SoloLectura = True
        Me.txtTotalFormaPagoOper.TabIndex = 7
        Me.txtTotalFormaPagoOper.TabStop = False
        Me.txtTotalFormaPagoOper.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalFormaPagoOper.Texto = "0"
        '
        'ocxFormaPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxFormaPago"
        Me.Size = New System.Drawing.Size(810, 358)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lklNuevoChequeCliente As System.Windows.Forms.LinkLabel
    Friend WithEvents btnChequeTerceros As System.Windows.Forms.Button
    Friend WithEvents btnCheque As System.Windows.Forms.Button
    Friend WithEvents btnEfectivo As System.Windows.Forms.Button
    Friend WithEvents lblTotalFormaPago As System.Windows.Forms.Label
    Friend WithEvents lklEliminarFormaPago As System.Windows.Forms.LinkLabel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnDocumentos As System.Windows.Forms.Button
    Friend WithEvents dgw As System.Windows.Forms.DataGridView
    Friend WithEvents btnTarjeta As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalFormaPago As ERP.ocxTXTNumeric
    Friend WithEvents txtTotalFormaPagoOper As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents colID As DataGridViewTextBoxColumn
    Friend WithEvents colFormaPago As DataGridViewTextBoxColumn
    Friend WithEvents colMoneda As DataGridViewTextBoxColumn
    Friend WithEvents colImporteMoneda As DataGridViewTextBoxColumn
    Friend WithEvents colCambio As DataGridViewTextBoxColumn
    Friend WithEvents colBanco As DataGridViewTextBoxColumn
    Friend WithEvents colComprobante As DataGridViewTextBoxColumn
    Friend WithEvents colTipo As DataGridViewTextBoxColumn
    Friend WithEvents colFechaEmision As DataGridViewTextBoxColumn
    Friend WithEvents colVencimiento As DataGridViewTextBoxColumn
    Friend WithEvents colImporte As DataGridViewTextBoxColumn
    Friend WithEvents colIDTransaccion As DataGridViewTextBoxColumn
    Friend WithEvents colFP As DataGridViewTextBoxColumn
    Friend WithEvents colImporteGs As DataGridViewTextBoxColumn
    Friend WithEvents btnNC As Button
End Class

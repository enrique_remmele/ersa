﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeleccionFormaPagoDocumento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblComprobante = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.gbxDatos = New System.Windows.Forms.GroupBox()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.lblBanco = New System.Windows.Forms.Label()
        Me.txtTipoDocumento = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtObservacion = New ERP.ocxTXTString()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtCotizacion = New ERP.ocxTXTNumeric()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.txtImporteMoneda = New ERP.ocxTXTNumeric()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblCambio = New System.Windows.Forms.Label()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tlpGeneral = New System.Windows.Forms.TableLayoutPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.dgvVenta = New System.Windows.Forms.DataGridView()
        Me.IDTransaccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDFormaPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDTransaccionVenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ComprobanteVenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaVenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalImpuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Porcentaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtTotalRetencion = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkNoExigirRetencion = New ERP.ocxCHK()
        Me.gbxDatos.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tlpGeneral.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.dgvVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(348, 43)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 7
        Me.lblID.Text = "ID:"
        '
        'lblComprobante
        '
        Me.lblComprobante.AutoSize = True
        Me.lblComprobante.Location = New System.Drawing.Point(19, 43)
        Me.lblComprobante.Name = "lblComprobante"
        Me.lblComprobante.Size = New System.Drawing.Size(73, 13)
        Me.lblComprobante.TabIndex = 3
        Me.lblComprobante.Text = "Comprobante:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(43, 70)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 9
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(22, 97)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 16
        Me.lblObservacion.Text = "Observacion:"
        '
        'gbxDatos
        '
        Me.gbxDatos.Controls.Add(Me.cbxBanco)
        Me.gbxDatos.Controls.Add(Me.lblBanco)
        Me.gbxDatos.Controls.Add(Me.txtTipoDocumento)
        Me.gbxDatos.Controls.Add(Me.Label1)
        Me.gbxDatos.Controls.Add(Me.txtFecha)
        Me.gbxDatos.Controls.Add(Me.lblID)
        Me.gbxDatos.Controls.Add(Me.txtID)
        Me.gbxDatos.Controls.Add(Me.lblComprobante)
        Me.gbxDatos.Controls.Add(Me.txtObservacion)
        Me.gbxDatos.Controls.Add(Me.lblMoneda)
        Me.gbxDatos.Controls.Add(Me.lblObservacion)
        Me.gbxDatos.Controls.Add(Me.cbxMoneda)
        Me.gbxDatos.Controls.Add(Me.txtCotizacion)
        Me.gbxDatos.Controls.Add(Me.lblImporte)
        Me.gbxDatos.Controls.Add(Me.txtImporteMoneda)
        Me.gbxDatos.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxDatos.Controls.Add(Me.lblCambio)
        Me.gbxDatos.Controls.Add(Me.txtComprobante)
        Me.gbxDatos.Controls.Add(Me.txtImporte)
        Me.gbxDatos.Controls.Add(Me.lblFecha)
        Me.gbxDatos.Location = New System.Drawing.Point(3, 3)
        Me.gbxDatos.Name = "gbxDatos"
        Me.gbxDatos.Size = New System.Drawing.Size(470, 148)
        Me.gbxDatos.TabIndex = 0
        Me.gbxDatos.TabStop = False
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = Nothing
        Me.cbxBanco.CargarUnaSolaVez = False
        Me.cbxBanco.DataDisplayMember = Nothing
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = Nothing
        Me.cbxBanco.DataValueMember = Nothing
        Me.cbxBanco.dtSeleccionado = Nothing
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(93, 120)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionMultiple = False
        Me.cbxBanco.SeleccionObligatoria = False
        Me.cbxBanco.Size = New System.Drawing.Size(147, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 19
        Me.cbxBanco.Texto = ""
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(48, 125)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(41, 13)
        Me.lblBanco.TabIndex = 18
        Me.lblBanco.Text = "Banco:"
        '
        'txtTipoDocumento
        '
        Me.txtTipoDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoDocumento.Color = System.Drawing.Color.Empty
        Me.txtTipoDocumento.Indicaciones = Nothing
        Me.txtTipoDocumento.Location = New System.Drawing.Point(163, 11)
        Me.txtTipoDocumento.Multilinea = False
        Me.txtTipoDocumento.Name = "txtTipoDocumento"
        Me.txtTipoDocumento.Size = New System.Drawing.Size(294, 21)
        Me.txtTipoDocumento.SoloLectura = False
        Me.txtTipoDocumento.TabIndex = 2
        Me.txtTipoDocumento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTipoDocumento.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tipo Documento:"
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 7, 16, 13, 58, 9, 927)
        Me.txtFecha.Location = New System.Drawing.Point(275, 39)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 6
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(375, 39)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(82, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 8
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtObservacion
        '
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Color = System.Drawing.Color.Empty
        Me.txtObservacion.Indicaciones = Nothing
        Me.txtObservacion.Location = New System.Drawing.Point(93, 93)
        Me.txtObservacion.Multilinea = False
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(364, 21)
        Me.txtObservacion.SoloLectura = False
        Me.txtObservacion.TabIndex = 17
        Me.txtObservacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtObservacion.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(93, 66)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(53, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 10
        Me.cbxMoneda.Texto = ""
        '
        'txtCotizacion
        '
        Me.txtCotizacion.Color = System.Drawing.Color.Empty
        Me.txtCotizacion.Decimales = False
        Me.txtCotizacion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtCotizacion.Location = New System.Drawing.Point(191, 66)
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(39, 21)
        Me.txtCotizacion.SoloLectura = False
        Me.txtCotizacion.TabIndex = 12
        Me.txtCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCotizacion.Texto = "0"
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Location = New System.Drawing.Point(230, 70)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(45, 13)
        Me.lblImporte.TabIndex = 13
        Me.lblImporte.Text = "Importe:"
        '
        'txtImporteMoneda
        '
        Me.txtImporteMoneda.Color = System.Drawing.Color.Empty
        Me.txtImporteMoneda.Decimales = False
        Me.txtImporteMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMoneda.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporteMoneda.Location = New System.Drawing.Point(275, 66)
        Me.txtImporteMoneda.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporteMoneda.Name = "txtImporteMoneda"
        Me.txtImporteMoneda.Size = New System.Drawing.Size(91, 21)
        Me.txtImporteMoneda.SoloLectura = False
        Me.txtImporteMoneda.TabIndex = 14
        Me.txtImporteMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteMoneda.Texto = "0"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(94, 11)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(70, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblCambio
        '
        Me.lblCambio.AutoSize = True
        Me.lblCambio.Location = New System.Drawing.Point(146, 70)
        Me.lblCambio.Name = "lblCambio"
        Me.lblCambio.Size = New System.Drawing.Size(45, 13)
        Me.lblCambio.TabIndex = 11
        Me.lblCambio.Text = "Cambio:"
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(93, 39)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(137, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 4
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = False
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtImporte.Location = New System.Drawing.Point(366, 66)
        Me.txtImporte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(91, 21)
        Me.txtImporte.SoloLectura = True
        Me.txtImporte.TabIndex = 15
        Me.txtImporte.TabStop = False
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Location = New System.Drawing.Point(235, 43)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(40, 13)
        Me.lblFecha.TabIndex = 5
        Me.lblFecha.Text = "Fecha:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 438)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(478, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(394, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(313, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'tlpGeneral
        '
        Me.tlpGeneral.ColumnCount = 1
        Me.tlpGeneral.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpGeneral.Controls.Add(Me.Label2, 0, 1)
        Me.tlpGeneral.Controls.Add(Me.FlowLayoutPanel1, 0, 4)
        Me.tlpGeneral.Controls.Add(Me.gbxDatos, 0, 0)
        Me.tlpGeneral.Controls.Add(Me.dgvVenta, 0, 2)
        Me.tlpGeneral.Controls.Add(Me.FlowLayoutPanel2, 0, 3)
        Me.tlpGeneral.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpGeneral.Location = New System.Drawing.Point(0, 0)
        Me.tlpGeneral.Name = "tlpGeneral"
        Me.tlpGeneral.RowCount = 5
        Me.tlpGeneral.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 154.0!))
        Me.tlpGeneral.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.tlpGeneral.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpGeneral.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.tlpGeneral.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tlpGeneral.Size = New System.Drawing.Size(478, 438)
        Me.tlpGeneral.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 154)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(472, 22)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Seleccione las facturas y el importe afectado:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 401)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(472, 34)
        Me.FlowLayoutPanel1.TabIndex = 4
        '
        'dgvVenta
        '
        Me.dgvVenta.AllowUserToAddRows = False
        Me.dgvVenta.AllowUserToDeleteRows = False
        Me.dgvVenta.BackgroundColor = System.Drawing.Color.White
        Me.dgvVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVenta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDTransaccion, Me.IDFormaPago, Me.IDTransaccionVenta, Me.Sel, Me.ComprobanteVenta, Me.FechaVenta, Me.TotalImpuesto, Me.Porcentaje, Me.Importe})
        Me.dgvVenta.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvVenta.Location = New System.Drawing.Point(3, 179)
        Me.dgvVenta.MultiSelect = False
        Me.dgvVenta.Name = "dgvVenta"
        Me.dgvVenta.RowHeadersVisible = False
        Me.dgvVenta.Size = New System.Drawing.Size(472, 178)
        Me.dgvVenta.StandardTab = True
        Me.dgvVenta.TabIndex = 2
        '
        'IDTransaccion
        '
        Me.IDTransaccion.HeaderText = "IDTransaccion"
        Me.IDTransaccion.Name = "IDTransaccion"
        Me.IDTransaccion.ReadOnly = True
        Me.IDTransaccion.Visible = False
        '
        'IDFormaPago
        '
        Me.IDFormaPago.HeaderText = "ID"
        Me.IDFormaPago.Name = "IDFormaPago"
        Me.IDFormaPago.ReadOnly = True
        Me.IDFormaPago.Visible = False
        '
        'IDTransaccionVenta
        '
        Me.IDTransaccionVenta.HeaderText = "IDTransaccionVenta"
        Me.IDTransaccionVenta.Name = "IDTransaccionVenta"
        Me.IDTransaccionVenta.ReadOnly = True
        Me.IDTransaccionVenta.Visible = False
        '
        'Sel
        '
        Me.Sel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Sel.HeaderText = "Sel"
        Me.Sel.Name = "Sel"
        Me.Sel.Width = 28
        '
        'ComprobanteVenta
        '
        Me.ComprobanteVenta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ComprobanteVenta.HeaderText = "Comprobante"
        Me.ComprobanteVenta.Name = "ComprobanteVenta"
        Me.ComprobanteVenta.ReadOnly = True
        '
        'FechaVenta
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.FechaVenta.DefaultCellStyle = DataGridViewCellStyle1
        Me.FechaVenta.HeaderText = "Fecha"
        Me.FechaVenta.Name = "FechaVenta"
        Me.FechaVenta.ReadOnly = True
        '
        'TotalImpuesto
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        Me.TotalImpuesto.DefaultCellStyle = DataGridViewCellStyle2
        Me.TotalImpuesto.HeaderText = "IVA"
        Me.TotalImpuesto.Name = "TotalImpuesto"
        Me.TotalImpuesto.ReadOnly = True
        Me.TotalImpuesto.Width = 75
        '
        'Porcentaje
        '
        Me.Porcentaje.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        Me.Porcentaje.DefaultCellStyle = DataGridViewCellStyle3
        Me.Porcentaje.HeaderText = "%"
        Me.Porcentaje.Name = "Porcentaje"
        Me.Porcentaje.ReadOnly = True
        Me.Porcentaje.Width = 40
        '
        'Importe
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle4
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.Width = 75
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.txtTotalRetencion)
        Me.FlowLayoutPanel2.Controls.Add(Me.Label3)
        Me.FlowLayoutPanel2.Controls.Add(Me.chkNoExigirRetencion)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 363)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(472, 32)
        Me.FlowLayoutPanel2.TabIndex = 3
        '
        'txtTotalRetencion
        '
        Me.txtTotalRetencion.Color = System.Drawing.Color.Empty
        Me.txtTotalRetencion.Decimales = False
        Me.txtTotalRetencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalRetencion.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtTotalRetencion.Location = New System.Drawing.Point(361, 3)
        Me.txtTotalRetencion.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtTotalRetencion.Name = "txtTotalRetencion"
        Me.txtTotalRetencion.Size = New System.Drawing.Size(107, 21)
        Me.txtTotalRetencion.SoloLectura = True
        Me.txtTotalRetencion.TabIndex = 2
        Me.txtTotalRetencion.TabStop = False
        Me.txtTotalRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalRetencion.Texto = "0"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(313, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 29)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Total:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkNoExigirRetencion
        '
        Me.chkNoExigirRetencion.BackColor = System.Drawing.Color.Transparent
        Me.chkNoExigirRetencion.Color = System.Drawing.Color.Empty
        Me.chkNoExigirRetencion.Location = New System.Drawing.Point(179, 3)
        Me.chkNoExigirRetencion.Name = "chkNoExigirRetencion"
        Me.chkNoExigirRetencion.Size = New System.Drawing.Size(128, 21)
        Me.chkNoExigirRetencion.SoloLectura = False
        Me.chkNoExigirRetencion.TabIndex = 0
        Me.chkNoExigirRetencion.Texto = "No exigir la retencion"
        Me.chkNoExigirRetencion.Valor = False
        '
        'frmSeleccionFormaPagoDocumento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(478, 460)
        Me.Controls.Add(Me.tlpGeneral)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmSeleccionFormaPagoDocumento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmSeleccionFormaPagoDocumento"
        Me.gbxDatos.ResumeLayout(False)
        Me.gbxDatos.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tlpGeneral.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.dgvVenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblComprobante As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ERP.ocxTXTString
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents gbxDatos As System.Windows.Forms.GroupBox
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents txtCotizacion As ERP.ocxTXTNumeric
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents txtImporteMoneda As ERP.ocxTXTNumeric
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblCambio As System.Windows.Forms.Label
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents txtImporte As ERP.ocxTXTNumeric
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTipoDocumento As ERP.ocxTXTString
    Friend WithEvents tlpGeneral As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvVenta As System.Windows.Forms.DataGridView
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtTotalRetencion As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents IDTransaccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDFormaPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDTransaccionVenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ComprobanteVenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaVenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalImpuesto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Porcentaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkNoExigirRetencion As ERP.ocxCHK
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents lblBanco As System.Windows.Forms.Label
End Class

﻿Public Class frmSeleccionFormaPagoDocumentoNC
    'CLASE
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PRIPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private dtNCValue As DataTable
    Public Property dtNC() As DataTable
        Get
            Return dtNCValue
        End Get
        Set(ByVal value As DataTable)
            dtNCValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal

        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    Private RestarValue As Boolean
    Public Property Restar() As Boolean
        Get
            Return RestarValue
        End Get
        Set(ByVal value As Boolean)
            RestarValue = value
        End Set
    End Property

    Private PreseleccionarRetencionesValue As Boolean
    Public Property PreseleccionarRetenciones() As Boolean
        Get
            Return PreseleccionarRetencionesValue
        End Get
        Set(ByVal value As Boolean)
            PreseleccionarRetencionesValue = value
        End Set
    End Property

    Private DecimalesOperValue As Boolean
    Public Property DecimalesOper() As Boolean
        Get
            Return DecimalesOperValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesOperValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property
    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        IDOperacion = CSistema.ObtenerIDOperacion("frmSeleccionFormaPagoDocumento", "PAGOS Y COBROS CON DOCUMENTOS", "NC")
        txtImporteMoneda.Decimales = DecimalesOper
        txtTotalRetencion.Decimales = DecimalesOper

        CargarInformacion()
        txtComprobante.txt.Text = Comprobante
        txtID.SetValue(ID)
        txtFecha.SetValue(Fecha)
        If Saldo < 0 Then
            Saldo = Saldo * -1
            txtImporteMoneda.SetValue(Saldo)
        Else
            txtImporteMoneda.SetValue(Saldo)
        End If
        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

        'Foco
        cbxTipoComprobante.cbx.Focus()

    End Sub

    Sub CargarInformacion()

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion & " and Estado = 'True' and ID= 154")

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = IDMoneda
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'txtCotizacion.SetValue(1)
        FormatoMoneda()
        'Banco
        CSistema.SqlToComboBox(cbxBanco.cbx, "Select ID, referencia  From Banco Order By 2")
    End Sub

    Public Sub ImportarRetencion(ByVal RowRetencion As DataRow)

        Dim oRow As DataRow = dt.NewRow
        oRow("IDTransaccion") = 0
        oRow("ID") = ID
        oRow("IDTipoComprobante") = vgConfiguraciones("CobranzaCreditoTipoComprobanteRetencion")
        oRow("TipoComprobante") = "RETEC"
        oRow("Comprobante") = RowRetencion("Comprobante")
        oRow("Fecha") = RowRetencion("Fecha Emisión")
        oRow("Importe") = CSistema.FormatoNumeroBaseDatos(RowRetencion("Retenido IVA"), False)
        oRow("IDMoneda") = 1
        oRow("Moneda") = "GS"
        oRow("ImporteMoneda") = CSistema.FormatoMoneda(RowRetencion("Retenido IVA"), False)
        oRow("Cotizacion") = 1
        oRow("Observacion") = ""

        oRow("Banco") = ""
        oRow("IDBanco") = 0
        Dim dtTipoComprobante As DataTable = CData.GetTable("VTipoComprobante")

        For Each TipoComprobanteRow As DataRow In dtTipoComprobante.Select("ID=" & vgConfiguraciones("CobranzaCreditoTipoComprobanteRetencion"))
            oRow("Restar") = TipoComprobanteRow("Restar")
        Next

        For Each VentaGridRow As DataGridViewRow In dgvNotaCredito.Rows

            'Si no existe coincidencia, ir al siguiente
            If dtNC.Select("IDTransaccionVenta=" & VentaGridRow.Cells("IDTransaccionVenta").Value & "").Count = 0 Then
                GoTo siguiente
            End If

            Dim Comprobante As String() = RowRetencion("Comprobante Venta").ToString.Split("-")
            Dim NroComprobante As String = Comprobante(2)
            Dim Nro As Integer = CInt(NroComprobante)
            'For Each oRow As DataRow In dtDetalleCobranzaVenta.Select("Comprobante = '" & Row("Comprobante Venta") & "'")
            For Each VentaRow As DataRow In dtNC.Select("NroComprobante = " & Nro)

                'Actualizamos la venta
                'For Each VentaRow As DataRow In dtVenta.Select("NroCo=" & VentaGridRow.Cells("IDTransaccionVenta").Value & "")

                VentaRow("Sel") = True
                VentaRow("ID") = ID
                VentaRow("ComprobanteRetencion") = RowRetencion("Comprobante")
                VentaRow("Porcentaje") = 30
                VentaRow("Importe") = CSistema.FormatoNumeroBaseDatos(RowRetencion("Retenido IVA"), True)

                'Aplicamos
                VentaRow("Aplicado") = True

            Next
siguiente:
        Next

        dt.Rows.Add(oRow)

        Me.Close()

    End Sub

    Sub Aceptar()

        'Validar
        'No puede ser 0
        If txtImporte.ObtenerValor = 0 Then
            MessageBox.Show("El importe no puede ser 0!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        For Each oRow1 As DataGridViewRow In dgvNotaCredito.Rows

            Dim IDTransaccion As Integer = oRow1.Cells(2).Value
            If oRow1.Cells(3).Value = True Then
                For Each oRow2 As DataRow In dtNC.Select("IDTransaccion=" & IDTransaccion)
                    oRow2("Sel") = oRow1.Cells(3).Value
                    oRow2("Importe") = oRow1.Cells(7).Value
                Next
                Dim oRow As DataRow = dt.NewRow
                oRow("IDTransaccion") = 0
                oRow("ID") = ID
                oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
                oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
                'oRow("Comprobante") = dgvNotaCredito.CurrentRow.Cells("ComprobanteVenta").Value
                oRow("Comprobante") = oRow1.Cells("ComprobanteVenta").Value
                oRow("Fecha") = txtFecha.GetValue
                'oRow("Importe") = CSistema.FormatoNumeroBaseDatos(txtImporte.txt.Text, CSistema.RetornarValorBoolean(CData.GetRow(" id = " & cbxMoneda.GetValue, "vMoneda")("Decimales").ToString))
                oRow("Importe") = oRow1.Cells("Importe").Value
                oRow("IDMoneda") = cbxMoneda.cbx.SelectedValue
                oRow("Moneda") = cbxMoneda.cbx.Text
                oRow("ImporteMoneda") = oRow1.Cells("Importe").Value
                oRow("Cotizacion") = txtCotizacion.ObtenerValor
                oRow("Observacion") = txtObservacion.txt.Text
                If cbxBanco.Enabled = True Then
                    oRow("Banco") = cbxBanco.txt.Text
                    oRow("IDBanco") = cbxBanco.GetValue
                Else
                    oRow("Banco") = ""
                    oRow("IDBanco") = 0
                End If
                Dim dtTipoComprobante As DataTable = CData.GetTable("VTipoComprobante")

                For Each TipoComprobanteRow As DataRow In dtTipoComprobante.Select("ID=" & cbxTipoComprobante.GetValue)
                    oRow("Restar") = TipoComprobanteRow("Restar")
                Next

                'Actualizar los datos de las ventas, solo si es retencion

                If Not dtNC Is Nothing Then
                    For Each VentaGridRow As DataGridViewRow In dgvNotaCredito.Rows

                        'Si no existe coincidencia, ir al siguiente
                        If dtNC.Select("IDTransaccion=" & VentaGridRow.Cells("IDTransaccion").Value & "").Count = 0 Then
                            GoTo siguiente
                        End If

                        'Actualizamos la venta
                        For Each VentaRow As DataRow In dtNC.Select("IDTransaccionVenta=" & VentaGridRow.Cells("IDTransaccionVenta").Value & "")

                            VentaRow("Sel") = VentaGridRow.Cells("Sel").Value

                            'Si no esta seleccionado, ir al siguiente
                            If VentaGridRow.Cells("Sel").Value = False Then
                                GoTo siguiente
                            End If

                            VentaRow("ID") = ID
                            VentaRow("ComprobanteRetencion") = txtComprobante.GetValue
                            VentaRow("Porcentaje") = VentaGridRow.Cells("Porcentaje").Value
                            VentaRow("Importe") = CSistema.FormatoNumeroBaseDatos(VentaGridRow.Cells("Importe").Value, True)

                            'Aplicamos
                            VentaRow("Aplicado") = True

                        Next
siguiente:
                    Next

                End If


                'Dim NroComprobanteRET As String = ""
                Dim NroComprobanteAYB As String = ""
                NroComprobanteAYB = CSistema.ExecuteScalar("Select Comprobante  From vformapago Where IDTipoComprobante in (44, 52) and comprobante = '" & txtComprobante.txt.Text & " ' and banco = '" & cbxBanco.txt.Text & "'")
                If NroComprobanteAYB <> "" Then
                    Dim mensaje As String = "ERROR, El comprobante ya existe!"
                    ctrError.SetError(txtComprobante, mensaje)
                    ctrError.SetIconAlignment(txtComprobante, ErrorIconAlignment.TopRight)
                    tsslEstado.Text = mensaje
                    txtComprobante.Focus()
                    Exit Sub
                End If

                dt.Rows.Add(oRow)
            End If
        Next

        Me.Close()

    End Sub

    Sub HabilitarRetencion()

        Dim IDTipoComprobanteNC As Integer = 154
        Dim IDTipoComprobanteSeleccionado As Integer

        Try

            'IDTipoComprobanteRetencion = CSistema.RetornarValorInteger(vgConfiguraciones("CobranzaCreditoTipoComprobanteRetencion").ToString)
            IDTipoComprobanteSeleccionado = cbxTipoComprobante.GetValue

            'Si es una retencion
            If IDTipoComprobanteNC = IDTipoComprobanteSeleccionado Then
                tlpGeneral.RowStyles(1).SizeType = SizeType.Absolute
                tlpGeneral.RowStyles(1).Height = 20
                tlpGeneral.RowStyles(2).SizeType = SizeType.Percent
                tlpGeneral.RowStyles(2).Height = 100
                tlpGeneral.RowStyles(3).SizeType = SizeType.Absolute
                tlpGeneral.RowStyles(3).Height = 35
                dgvNotaCredito.Enabled = True

                Me.Size = New Size(494, 460)

                MostrarFacturas()

            Else

                tlpGeneral.RowStyles(1).SizeType = SizeType.Absolute
                tlpGeneral.RowStyles(1).Height = 0
                tlpGeneral.RowStyles(2).SizeType = SizeType.Absolute
                tlpGeneral.RowStyles(2).Height = 0
                tlpGeneral.RowStyles(3).SizeType = SizeType.Absolute
                tlpGeneral.RowStyles(3).Height = 0
                dgvNotaCredito.Enabled = False

                'Me.Size = New Size(494, 225)
                Me.Size = New Size(494, 250)

            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub HabilitarAcreditacionBancaria()
        Try
            Dim dtComprobante As DataTable = CData.GetTable("TipoComprobante", " ID =" & cbxTipoComprobante.GetValue)

            For Each ComprobanteRow As DataRow In dtComprobante.Select("ID=" & cbxTipoComprobante.GetValue)
                cbxBanco.Enabled = ComprobanteRow("Deposito")
            Next
        Catch ex As Exception

        End Try

    End Sub

    Sub MostrarFacturas()

        'Validar
        If dtNC Is Nothing Then
            Exit Sub
        End If

        dgvNotaCredito.Rows.Clear()

        'Solo los que no estan aplicados
        For Each oRow As DataRow In dtNC.Select()

            Dim NewRow(dtNC.Columns.Count - 1) As String
            NewRow(0) = 0
            NewRow(1) = ID
            NewRow(2) = oRow("IDTransaccion")
            NewRow(3) = False
            NewRow(4) = oRow("NotaCredito")
            NewRow(5) = oRow("Fecha")
            'NewRow(6) = CSistema.FormatoMoneda(oRow("TotalImpuesto"), DecimalesOper)
            'NewRow(7) = oRow("Porcentaje")
            NewRow(6) = CSistema.FormatoMoneda(oRow("Importe"), DecimalesOper)

            'Si hay que preseleccionar
            If PreseleccionarRetenciones = True Then
                If oRow("Importe") > 0 Then
                    NewRow(3) = True
                End If
            End If

            dgvNotaCredito.Rows.Add(NewRow)

        Next

        For Each Row As DataGridViewRow In dgvNotaCredito.Rows
            Row.Cells("Sel").Value = False
        Next

        PintarCelda()
        CalcularTotales()

    End Sub

    Sub PintarCelda()

        If dgvNotaCredito.Rows.Count = 0 Then
            Exit Sub
        End If

        For Each Row As DataGridViewRow In dgvNotaCredito.Rows
            If Row.Cells("Sel").Value = True Then
                Row.DefaultCellStyle.BackColor = Color.PaleTurquoise
            Else
                Row.DefaultCellStyle.BackColor = Color.White
            End If
        Next

        dgvNotaCredito.CurrentRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow

    End Sub

    Sub CalcularTotales()

        txtTotalRetencion.SetValue(CSistema.gridSumColumn(dgvNotaCredito, "Importe", "Sel", "True"))
        txtImporteMoneda.SetValue(CSistema.FormatoMoneda(txtTotalRetencion.txt.Text, DecimalesOper))
        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

    End Sub

    Sub FormatoMoneda()

        Dim Decimales As Boolean = CSistema.MonedaDecimal(cbxMoneda.GetValue)
        txtCotizacion.txt.Text = CSistema.CotizacionDelDia(cbxMoneda.GetValue, 2)
        DecimalesOper = Decimales
        txtImporteMoneda.Decimales = Decimales


    End Sub

    Sub CalcularImporte()

        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporteMoneda.TeclaPrecionada
        If txtImporte.txt.Focused = True Then
            Exit Sub
        End If

        CalcularImporte()

    End Sub

    Private Sub txtCotizacion_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCotizacion.TeclaPrecionada

        If txtCotizacion.txt.Focused = True Then
            Exit Sub
        End If

        CalcularImporte()

    End Sub

    Private Sub frmSeleccionFormaPagoDocumento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        'Controles para obviar
        Dim Controles() As String = {"dgvNotaCredito", "txtObservacion"}
        If Controles.Contains(Me.ActiveControl.Name) Then
            Exit Sub
        End If

        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoComprobante_Leave(sender As Object, e As System.EventArgs) Handles cbxTipoComprobante.Leave
        HabilitarRetencion()
        HabilitarAcreditacionBancaria()
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.PropertyChanged
        txtTipoDocumento.txt.Text = CSistema.ExecuteScalar("Select Descripcion From TipoComprobante Where ID = " & cbxTipoComprobante.GetValue)
        HabilitarRetencion()
        HabilitarAcreditacionBancaria()
    End Sub

    Private Sub dgw_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNotaCredito.CellEndEdit

        If dgvNotaCredito.Columns(e.ColumnIndex).Name = "Importe" Then

            'Que el valor sea numerico
            If IsNumeric(dgvNotaCredito.CurrentCell.Value) = False Then

                Dim mensaje As String = "El importe debe ser valido!"
                ctrError.SetError(dgvNotaCredito, mensaje)
                ctrError.SetIconAlignment(dgvNotaCredito, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                dgvNotaCredito.CurrentCell.Value = 0

                Exit Sub

            End If

            'Que el valor sea mayor a 0
            If CDec(dgvNotaCredito.CurrentCell.Value) <= 0 Then

                Dim mensaje As String = "El importe debe ser mayor a 0!"
                ctrError.SetError(dgvNotaCredito, mensaje)
                ctrError.SetIconAlignment(dgvNotaCredito, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                Exit Sub

            End If

            'Que el valor sea mayor a 0
            If CDec(dgvNotaCredito.CurrentCell.Value) > dgvNotaCredito.CurrentRow.Cells("TotalImpuesto").Value Then

                Dim mensaje As String = "El importe no puede ser mayor al saldo!"
                ctrError.SetError(dgvNotaCredito, mensaje)
                ctrError.SetIconAlignment(dgvNotaCredito, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje

                Exit Sub

            End If

            dgvNotaCredito.CurrentCell.Value = CSistema.FormatoMoneda3Decimales(dgvNotaCredito.CurrentRow.Cells("Importe").Value, True)

            CalcularTotales()

        End If

    End Sub

    Private Sub dgw_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvNotaCredito.KeyDown

        If e.KeyCode = Keys.Enter Then

            ctrError.Clear()
            tsslEstado.Text = ""

            If dgvNotaCredito.SelectedCells.Count = 0 Then
                Exit Sub
            End If

            Dim RowIndex As Integer = dgvNotaCredito.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgvNotaCredito.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Sel").Value = False Then
                        oRow.Cells("Sel").Value = True
                        oRow.Cells("Sel").ReadOnly = False
                        oRow.Cells("Importe").ReadOnly = False
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgvNotaCredito.CurrentCell = dgvNotaCredito.Rows(oRow.Index).Cells("Importe")

                    Else
                        oRow.Cells("Sel").ReadOnly = True
                        oRow.Cells("Importe").ReadOnly = True
                        oRow.Cells("Sel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgvNotaCredito.Rows.Count - 1 Then
                dgvNotaCredito.CurrentCell = dgvNotaCredito.Rows(RowIndex + 1).Cells("Importe")
            End If

            CalcularTotales()

            dgvNotaCredito.Update()

            ' Your code here
            e.SuppressKeyPress = True

        End If
    End Sub

    Private Sub dgw_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNotaCredito.RowEnter

        Dim f1 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
        Dim f2 As New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Regular)

        For Each oRow As DataGridViewRow In dgvNotaCredito.Rows
            If oRow.Index = e.RowIndex Then
                If oRow.Cells("Sel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                End If

                oRow.DefaultCellStyle.Font = f1

                Try
                    If oRow.Cells("Importe").Selected = False Then
                        oRow.Cells("Importe").Selected = True
                    End If
                Catch ex As Exception

                End Try

            Else

                oRow.DefaultCellStyle.Font = f2

                If oRow.Cells("Sel").Value = False Then
                    oRow.DefaultCellStyle.BackColor = Color.White
                Else
                    oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                End If
            End If
        Next
    End Sub

    Private Sub dgw_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvNotaCredito.KeyUp

    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNotaCredito.CellContentClick

        ctrError.Clear()
        tsslEstado.Text = ""

        If dgvNotaCredito.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = dgvNotaCredito.Columns.Item("Sel").Index Then

            Dim RowIndex As Integer = dgvNotaCredito.SelectedCells.Item(0).RowIndex


            For Each oRow As DataGridViewRow In dgvNotaCredito.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("Sel").Value = False Then
                        oRow.Cells("Sel").Value = True
                        oRow.Cells("Sel").ReadOnly = False
                        oRow.Cells("Importe").ReadOnly = False
                        oRow.Cells("Importe").Value = CSistema.FormatoMoneda(oRow.Cells("TotalImpuesto").Value)
                        oRow.DefaultCellStyle.BackColor = Color.PaleTurquoise
                        dgvNotaCredito.CurrentCell = dgvNotaCredito.Rows(oRow.Index).Cells("Importe")
                    Else
                        oRow.Cells("Sel").ReadOnly = True
                        oRow.Cells("Importe").ReadOnly = True
                        oRow.Cells("Importe").Value = 0
                        oRow.Cells("Sel").Value = False
                        oRow.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow
                    End If

                    Exit For

                End If

            Next

            If RowIndex < dgvNotaCredito.Rows.Count - 1 Then
                dgvNotaCredito.CurrentCell = dgvNotaCredito.Rows(RowIndex + 1).Cells("Importe")
            End If

            CalcularTotales()

            dgvNotaCredito.Update()

        End If
    End Sub

    Private Sub txtObservacion_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtObservacion.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If PreseleccionarRetenciones = True Then
                btnAceptar.Focus()
            Else
                dgvNotaCredito.Focus()
            End If
        End If
    End Sub

    Private Sub chkNoExigirRetencion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkNoExigirRetencion.PropertyChanged
        dgvNotaCredito.Enabled = Not value
    End Sub

    Private Sub cbxMoneda_Leave(sender As System.Object, e As System.EventArgs) Handles cbxMoneda.Leave
        FormatoMoneda()
    End Sub

    Private Sub cbxMoneda_PropertyChanged(sender As Object, e As System.EventArgs) Handles cbxMoneda.PropertyChanged
        FormatoMoneda()
    End Sub

    Private Sub cbxTipoComprobante_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoComprobante.TeclaPrecionada
        txtTipoDocumento.txt.Text = CSistema.ExecuteScalar("Select Descripcion From TipoComprobante Where ID = " & cbxTipoComprobante.GetValue)
        HabilitarRetencion()
        HabilitarAcreditacionBancaria()
    End Sub

End Class
﻿Public Class frmModificarFormaPagoDocumento

    'CLASE
    Dim CSistema As New CSistema

    'PRIPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal

        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As String
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As String)
            ComprobanteValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "PAGOS Y COBROS CON DOCUMENTOS", "DOC")

        CargarInformacion()
        txtComprobante.txt.Text = Comprobante
        txtID.SetValue(ID)
        txtFecha.SetValue(Fecha)
        txtImporteMoneda.SetValue(Saldo)
        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

        'Foco
        cbxTipoComprobante.cbx.Focus()

    End Sub

    Sub CargarInformacion()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        txtCotizacion.SetValue(1)

    End Sub

    Sub Aceptar()

        Dim oRow As DataRow = dt.NewRow
        oRow("IDTransaccion") = 0
        oRow("ID") = ID
        oRow("IDTipoComprobante") = cbxTipoComprobante.cbx.SelectedValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("Comprobante") = txtComprobante.txt.Text
        oRow("Fecha") = txtFecha.GetValue
        oRow("Importe") = txtImporte.ObtenerValor
        oRow("IDMoneda") = cbxMoneda.cbx.SelectedValue
        oRow("Moneda") = cbxMoneda.cbx.Text
        oRow("ImporteMoneda") = txtImporteMoneda.ObtenerValor
        oRow("Cotizacion") = txtCotizacion.ObtenerValor
        oRow("Observacion") = txtObservacion.txt.Text

        dt.Rows.Add(oRow)

        Me.Close()

    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporteMoneda.TeclaPrecionada
        'If txtImporte.txt.Focused = True Then
        '    Exit Sub
        'End If

        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

    End Sub

    Private Sub frmSeleccionFormaPagoDocumento_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

End Class
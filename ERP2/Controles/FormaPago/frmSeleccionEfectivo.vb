﻿Public Class frmSeleccionEfectivo

    'CLASE
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PRIPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private ComprobanteValue As String
    Public Property Comprobante() As Decimal
        Get
            Return ComprobanteValue
        End Get
        Set(ByVal value As Decimal)
            ComprobanteValue = value
        End Set
    End Property

    Private FechaValue As Date
    Public Property Fecha() As Date
        Get
            Return FechaValue
        End Get
        Set(ByVal value As Date)
            FechaValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal
        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private TipoValue As String
    Public Property Tipo() As String
        Get
            Return TipoValue
        End Get
        Set(ByVal value As String)
            TipoValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        IDOperacion = CSistema.ObtenerIDOperacion(Me.Name, "PAGOS Y COBROS EN EFECTIVO", "EFE")

        CargarInformacion()
        txtComprobante.SetValue(Comprobante)
        txtID.SetValue(ID)
        txtFecha.SetValue(Today.Date)
        txtImporteMoneda.SetValue(Saldo)
        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

        'Foco
        cbxTipoComprobante.cbx.Focus()

    End Sub

    Sub CargarInformacion()

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, "Select ID, Codigo From TipoComprobante Where IDOperacion=" & IDOperacion)

        'Monedas
        CSistema.SqlToComboBox(cbxMoneda.cbx, "Select ID, Referencia From Moneda")
        cbxMoneda.cbx.SelectedValue = IDMoneda
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'txtCotizacion.SetValue(1)
        ConfigurarMoneda()

    End Sub

    Sub Aceptar()

        'Valirar
        If txtImporteMoneda.ObtenerValor <= 0 Then
            MessageBox.Show("El importe debe ser mayor a 0!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim oRow As DataRow = dt.NewRow
        oRow("IDTransaccion") = 0
        oRow("ID") = ID
        oRow("IDTipoComprobante") = cbxTipoComprobante.GetValue
        oRow("TipoComprobante") = cbxTipoComprobante.cbx.Text
        oRow("Comprobante") = txtComprobante.txt.Text
        oRow("Fecha") = txtFecha.txt.Text
        oRow("Tipo") = Tipo
        oRow("Importe") = txtImporte.ObtenerValor

        Select Case Tipo

            Case "CREDITO"
                oRow("VentasCredito") = txtImporte.ObtenerValor
                oRow("VentasContado") = 0
                oRow("Gastos") = 0
            Case "CONTADO"
                oRow("VentasCredito") = 0
                oRow("VentasContado") = txtImporte.ObtenerValor
                oRow("Gastos") = 0
            Case "GASTOS"
                oRow("VentasCredito") = 0
                oRow("VentasContado") = 0
                oRow("Gastos") = txtImporte.ObtenerValor
        End Select

        oRow("IDMoneda") = cbxMoneda.cbx.SelectedValue
        oRow("Moneda") = cbxMoneda.cbx.Text
        oRow("ImporteMoneda") = txtImporteMoneda.ObtenerValor
        oRow("Cotizacion") = txtCotizacion.ObtenerValor
        oRow("Observacion") = txtObservacion.txt.Text

        dt.Rows.Add(oRow)

        Me.Close()

    End Sub

    Sub ConfigurarMoneda()

        If cbxMoneda.GetValue = 0 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VMoneda", " ID = " & cbxMoneda.GetValue)

        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim Moneda As DataRow = dt.Rows(0)

        txtImporteMoneda.Decimales = Moneda("Decimales")

        If cbxMoneda.GetValue <> 1 Then
            txtCotizacion.SetValue(CSistema.CotizacionDelDia(cbxMoneda.GetValue, IDOperacion))
        Else
            txtCotizacion.SetValue(1)
        End If

    End Sub

    Private Sub txtImporte_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtImporteMoneda.TeclaPrecionada
        'If txtImporte.txt.Focused = True Then
        '    Exit Sub
        'End If

        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))

    End Sub

    Private Sub frmSeleccionEfectivo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmSeleccionEfectivo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Aceptar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.PropertyChanged
        txtTipoDocumento.txt.Text = CSistema.ExecuteScalar("Select Descripcion From TipoComprobante Where ID = " & cbxTipoComprobante.GetValue)
    End Sub

    Private Sub cbxMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMoneda.PropertyChanged
        ConfigurarMoneda()
    End Sub

    Private Sub cbxMoneda_Leave(sender As System.Object, e As System.EventArgs) Handles cbxMoneda.Leave
        ConfigurarMoneda()
        txtImporte.SetValue(CSistema.Cotizador(txtCotizacion.ObtenerValor, txtImporteMoneda.ObtenerValor, cbxMoneda.cbx.SelectedValue))
    End Sub

End Class
﻿Public Class ocxOpcionesGenerales

    'CLASES
    Dim CSistema As New CSistema
    Public Property IDSucursal As Integer = 0
    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            If IDSucursal = 0 Then
                IDSucursal = vgIDSucursal
            End If

            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones Where IDSucursal=" & IDSucursal)(0)

            txtFormatoFecha.SetValue(CSistema.RetornarValorString(oRow("FormatoFecha").ToString))
            txtPropietarioBD.SetValue(CSistema.RetornarValorString(oRow("PropietarioBD").ToString))
            chkBloquearDocumentos.Checked = CSistema.RetornarValorBoolean(oRow("BloquearDocumentos").ToString)
            nudDias.Value = CSistema.RetornarValorInteger(oRow("DiasBloqueo").ToString)
            chkBloquearFechaOperacion.Checked = CSistema.RetornarValorBoolean(oRow("OperacionBloquearFechaActiva").ToString)
            chkFechaTesoreria.Checked = CSistema.RetornarValorBoolean(oRow("TesoreriaBloquerFecha").ToString)
        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim CostoProducto As String = ""

        Dim sql As String

        sql = "Update Configuraciones Set "
        sql = sql & "OperacionBloquearFechaActiva = '" & chkBloquearFechaOperacion.Checked.ToString & "', "
        sql = sql & "FormatoFecha = '" & txtFormatoFecha.GetValue & "', "
        sql = sql & "PropietarioBD = '" & txtPropietarioBD.GetValue & "', "
        sql = sql & "BloquearDocumentos = '" & chkBloquearDocumentos.Checked.ToString & "', "
        sql = sql & "TesoreriaBloquerFecha = '" & chkFechaTesoreria.Checked.ToString & "', "
        sql = sql & "DiasBloqueo = '" & nudDias.Value & "' "
        sql = sql & "Where IDSucursal = " & IDSucursal & " "

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Opciones Generales", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub lblRangoFecha_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lblRangoFecha.LinkClicked
        Dim frm As New frmFechaOperaciones
        frm.StartPosition = FormStartPosition.CenterParent
        frm.WindowState = FormWindowState.Normal
        frm.ShowDialog()
    End Sub

    Private Sub lblUsuarioFueraRango_LinkClicked(sender As System.Object, e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lblUsuarioFueraRango.LinkClicked
        Dim frm As New frmUsuarioOperacionFueraRango
        frm.StartPosition = FormStartPosition.CenterParent
        frm.WindowState = FormWindowState.Normal
        frm.ShowDialog()
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Dim frm As New frmFechaOperacionesContabilidad
        frm.StartPosition = FormStartPosition.CenterParent
        frm.WindowState = FormWindowState.Normal
        frm.ShowDialog()
    End Sub
End Class

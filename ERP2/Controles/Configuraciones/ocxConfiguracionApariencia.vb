﻿Public Class ocxConfiguracionApariencia

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio

    'FUNCIONES
    Sub Inicializar()

        CargarDatos()

        pbxColorFondoFormulario.BackColor = Color.FromArgb(vgColorFormulario)
        pbxColorFondoFormulario2.BackColor = Color.FromArgb(vgColorFormulario2)
        pbxColorTextoFormulario.BackColor = Color.FromArgb(vgColorTextos)

        pbxColorFondoBoton.BackColor = Color.FromArgb(vgColorBoton)
        pbxColorTextoBoton.BackColor = Color.FromArgb(vgColorTextosBoton)

        pbxColorFoco.BackColor = Color.FromArgb(vgColorFocus)
        pbxColorSoloLectura.BackColor = Color.FromArgb(vgColorSoloLectura)
        pbxColorSeleccion.BackColor = Color.FromArgb(vgColorSeleccionado)

    End Sub

    Sub Cambiar(ByVal pbx As PictureBox)

        ColorDialog1.Color = pbx.BackColor
        ColorDialog1.ShowDialog()
        pbx.BackColor = ColorDialog1.Color

    End Sub

    Sub CargarDatos()

        'Gardar en el archivo ini
        vgColorFocus = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR FOCO", pbxColorFoco.BackColor.ToArgb)
        vgColorSoloLectura = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR SOLO LECTURA", pbxColorSoloLectura.BackColor.ToArgb)
        vgColorSeleccionado = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR SELECCION LISTA", pbxColorSeleccion.BackColor.ToArgb)

        vgColorFormulario = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE FONDO", pbxColorFondoFormulario.BackColor.ToArgb)
        vgColorFormulario2 = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE FONDO 2", pbxColorFondoFormulario2.BackColor.ToArgb)
        vgColorTextos = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE TEXTO", pbxColorTextoFormulario.BackColor.ToArgb)

        vgColorBoton = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE BOTON", pbxColorFondoBoton.BackColor.ToArgb)
        vgColorTextosBoton = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE TEXTO BOTON", pbxColorTextoBoton.BackColor.ToArgb)

    End Sub

    Sub Guardar()

        'Gardar en el archivo ini
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR FOCO", pbxColorFoco.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR SOLO LECTURA", pbxColorSoloLectura.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR SELECCION LISTA", pbxColorSeleccion.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE FONDO", pbxColorFondoFormulario.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE FONDO 2", pbxColorFondoFormulario2.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE TEXTO", pbxColorTextoFormulario.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE BOTON", pbxColorFondoBoton.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE TEXTO BOTON", pbxColorTextoBoton.BackColor.ToArgb)

        'Actualizar la variable
        vgColorFormulario = pbxColorFondoFormulario.BackColor.ToArgb
        vgColorFormulario2 = pbxColorFondoFormulario2.BackColor.ToArgb
        vgColorTextos = pbxColorTextoFormulario.BackColor.ToArgb
        vgColorBoton = pbxColorFondoBoton.BackColor.ToArgb
        vgColorTextosBoton = pbxColorTextoBoton.BackColor.ToArgb
        vgColorFocus = pbxColorFoco.BackColor.ToArgb
        vgColorSoloLectura = pbxColorSoloLectura.BackColor.ToArgb
        vgColorSeleccionado = pbxColorSeleccion.BackColor.ToArgb

        'Cambiamos los fromualrios activos
        For Each frm As Form In My.Application.OpenForms
            FGEstiloFormulario(frm)
        Next


    End Sub

    Sub Reestablecer()

        If MessageBox.Show("Esta seguro/a de reestablecer a la configuracion inicial?", "Reestablecer", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
            Exit Sub
        End If

        vgColorFocus = -8075777
        vgColorSoloLectura = -196962
        vgColorBackColor = -197380
        vgColorSeleccionado = -16711936
        vgColorFormulario = -197380
        vgColorFormulario2 = -855300
        vgColorTextos = -12582912
        vgColorBoton = -855310
        vgColorTextosBoton = -12582912

        'Gardar en el archivo ini
        pbxColorFoco.BackColor = Color.FromArgb(vgColorFocus)
        pbxColorSoloLectura.BackColor = Color.FromArgb(vgColorSoloLectura)
        pbxColorSeleccion.BackColor = Color.FromArgb(vgColorSeleccionado)
        pbxColorFondoFormulario.BackColor = Color.FromArgb(vgColorFormulario)
        pbxColorFondoFormulario2.BackColor = Color.FromArgb(vgColorFormulario2)
        pbxColorTextoFormulario.BackColor = Color.FromArgb(vgColorTextos)
        pbxColorFondoBoton.BackColor = Color.FromArgb(vgColorBoton)
        pbxColorTextoBoton.BackColor = Color.FromArgb(vgColorTextosBoton)

        'Gardar en el archivo ini
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR FOCO", pbxColorFoco.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR SOLO LECTURA", pbxColorSoloLectura.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR SELECCION LISTA", pbxColorSeleccion.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE FONDO", pbxColorFondoFormulario.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE FONDO 2", pbxColorFondoFormulario2.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE TEXTO", pbxColorTextoFormulario.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE BOTON", pbxColorFondoBoton.BackColor.ToArgb)
        CArchivoInicio.IniWrite(VGArchivoINI, "FORMULARIO", "COLOR DE TEXTO BOTON", pbxColorTextoBoton.BackColor.ToArgb)

        'Cambiamos los fromualrios activos
        For Each frm As Form In My.Application.OpenForms
            FGEstiloFormulario(frm)
        Next


    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Reestablecer()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub ocxConfiguracionApariencia_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Cambiar(pbxColorFondoFormulario)
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Cambiar(pbxColorTextoFormulario)
    End Sub

    Private Sub LinkLabel3_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel3.LinkClicked
        Cambiar(pbxColorTextoBoton)
    End Sub

    Private Sub LinkLabel4_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel4.LinkClicked
        Cambiar(pbxColorFondoBoton)
    End Sub

    Private Sub LinkLabel6_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel6.LinkClicked
        Cambiar(pbxColorFoco)
    End Sub

    Private Sub LinkLabel5_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel5.LinkClicked
        Cambiar(pbxColorSoloLectura)
    End Sub

    Private Sub LinkLabel7_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel7.LinkClicked
        Cambiar(pbxColorSeleccion)
    End Sub

    Private Sub LinkLabel8_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel8.LinkClicked
        Cambiar(pbxColorFondoFormulario2)
    End Sub

End Class

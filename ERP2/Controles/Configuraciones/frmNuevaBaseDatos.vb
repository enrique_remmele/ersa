﻿Imports System.IO

Public Class frmNuevaBaseDatos

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private CreadoValue As Boolean
    Public Property Creado() As Boolean
        Get
            Return CreadoValue
        End Get
        Set(ByVal value As Boolean)
            CreadoValue = value
        End Set
    End Property

    Private CarpetaValue As String
    Public Property Carpeta() As String
        Get
            Return CarpetaValue
        End Get
        Set(ByVal value As String)
            CarpetaValue = value
        End Set
    End Property

    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private NombreBDValue As String
    Public Property NombreBD() As String
        Get
            Return NombreBDValue
        End Get
        Set(ByVal value As String)
            NombreBDValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Generar()

        'Nombre
        If txtNombre.GetValue.Trim.Length = 0 Then
            MessageBox.Show("Se debe especificar primeramente un nombre para continuar!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Verificar primeramente que el nombre ya no exista en 
        Dim Existe As Integer = CSistema.ExecuteScalar("SELECT COUNT(*) FROM sys.databases Where name='" & txtNombre.GetValue & "'", CadenaConexion)
        If Existe > 0 Then
            MessageBox.Show("Ya existe una base de datos con ese nombre!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim sql As String = ""

        'Crear Base de Datos
        sql = sql & "IF Not Exists(SELECT * FROM sys.databases Where name='" & txtNombre.GetValue & "') Begin" & vbCrLf
        sql = sql & "   CREATE DATABASE [" & txtNombre.GetValue & "] " & vbCrLf
        sql = sql & "End" & vbCrLf

        'Crear la base de datos
        Dim Resultado As Integer = CSistema.ExecuteNonQuery(sql, CadenaConexion)

        'Verificar si se ejecuto correctamente
        If Resultado = 0 Then
            MessageBox.Show("No se pudo crear la base de datos!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Cambiar la cadena de conexion
        CadenaConexion = CadenaConexion.Replace("Initial Catalog=master", "Initial Catalog=" & txtNombre.GetValue & "")
        sql = My.Resources.bdMovil

        'Ejecutar el scrip
        Resultado = CSistema.ExecuteNonQuery(sql, CadenaConexion)
        If Resultado = 0 Then
            MessageBox.Show("La base de datos fue creada correctamente, pero no se pudo completar la creacion de uno o varios componentes de la misma! Esto puede producir un error en el funcionamiento del sistema.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        'Ejecutar nuevamente
        Resultado = CSistema.ExecuteNonQuery(sql, CadenaConexion)
        If Resultado = 0 Then
            MessageBox.Show("La base de datos fue creada correctamente, pero no se pudo completar la creacion de uno o varios componentes de la misma! Esto puede producir un error en el funcionamiento del sistema.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        NombreBD = txtNombre.GetValue

        MessageBox.Show("La operacion se completo exitosamente.", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Creado = True
        Me.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Generar()
    End Sub
   
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

End Class
﻿Public Class ocxClasificacionProducto

    'CLASES
    Dim CSistema As New CSistema

    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones")(0)

            txtClasificacion1.SetValue(CSistema.RetornarValorString(oRow("ProductoClasificacion1").ToString))
            txtClasificacion2.SetValue(CSistema.RetornarValorString(oRow("ProductoClasificacion2").ToString))
            txtClasificacion3.SetValue(CSistema.RetornarValorString(oRow("ProductoClasificacion3").ToString))
            txtClasificacion4.SetValue(CSistema.RetornarValorString(oRow("ProductoClasificacion4").ToString))
            txtClasificacion5.SetValue(CSistema.RetornarValorString(oRow("ProductoClasificacion5").ToString))
            txtClasificacion6.SetValue(CSistema.RetornarValorString(oRow("ProductoClasificacion6").ToString))
            txtClasificacion7.SetValue(CSistema.RetornarValorString(oRow("ProductoClasificacion7").ToString))

            txtClasificacion1.txt.CharacterCasing = CharacterCasing.Normal
            txtClasificacion2.txt.CharacterCasing = CharacterCasing.Normal
            txtClasificacion3.txt.CharacterCasing = CharacterCasing.Normal
            txtClasificacion4.txt.CharacterCasing = CharacterCasing.Normal
            txtClasificacion5.txt.CharacterCasing = CharacterCasing.Normal
            txtClasificacion6.txt.CharacterCasing = CharacterCasing.Normal
            txtClasificacion7.txt.CharacterCasing = CharacterCasing.Normal

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim CostoProducto As String = ""

        Dim sql As String

        sql = "Update Configuraciones Set ProductoClasificacion1 = '" & txtClasificacion1.GetValue & "', ProductoClasificacion2 = '" & txtClasificacion2.GetValue & "', ProductoClasificacion3 = '" & txtClasificacion3.GetValue & "', ProductoClasificacion4 = '" & txtClasificacion4.GetValue & "', ProductoClasificacion5 = '" & txtClasificacion5.GetValue & "', ProductoClasificacion6 = '" & txtClasificacion6.GetValue & "' , ProductoClasificacion7 = '" & txtClasificacion7.GetValue & "' "

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            MessageBox.Show("Registro procesado!", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information)
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Movimiento", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ObtenerInformacion()
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub
End Class

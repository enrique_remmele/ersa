﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxConfiguracionesPedido
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.nudCantidadPedido = New System.Windows.Forms.NumericUpDown()
        Me.lblCantidadPedido = New System.Windows.Forms.Label()
        Me.chkBloquearTactico = New System.Windows.Forms.CheckBox()
        Me.chkControlarDescuentoTactico = New System.Windows.Forms.CheckBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkBloquearListaPrecio = New System.Windows.Forms.CheckBox()
        Me.chkAdvertirCantidadNegativa = New System.Windows.Forms.CheckBox()
        Me.chkPermitirCantidadNegativa = New System.Windows.Forms.CheckBox()
        Me.chkControlarTacticoImporte = New System.Windows.Forms.CheckBox()
        Me.chkControlarTacticoPorcentaje = New System.Windows.Forms.CheckBox()
        Me.chkCargarExcepcionSinAutorizacion = New System.Windows.Forms.CheckBox()
        Me.rdProductoPrecioSI = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdProductoPrecioNO = New System.Windows.Forms.RadioButton()
        CType(Me.nudCantidadPedido, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'nudCantidadPedido
        '
        Me.nudCantidadPedido.Location = New System.Drawing.Point(173, 111)
        Me.nudCantidadPedido.Name = "nudCantidadPedido"
        Me.nudCantidadPedido.Size = New System.Drawing.Size(62, 20)
        Me.nudCantidadPedido.TabIndex = 5
        '
        'lblCantidadPedido
        '
        Me.lblCantidadPedido.AutoSize = True
        Me.lblCantidadPedido.Location = New System.Drawing.Point(10, 113)
        Me.lblCantidadPedido.Name = "lblCantidadPedido"
        Me.lblCantidadPedido.Size = New System.Drawing.Size(157, 13)
        Me.lblCantidadPedido.TabIndex = 4
        Me.lblCantidadPedido.Text = "Cantidad de lineas en el detalle:"
        '
        'chkBloquearTactico
        '
        Me.chkBloquearTactico.AutoSize = True
        Me.chkBloquearTactico.Location = New System.Drawing.Point(13, 14)
        Me.chkBloquearTactico.Name = "chkBloquearTactico"
        Me.chkBloquearTactico.Size = New System.Drawing.Size(172, 17)
        Me.chkBloquearTactico.TabIndex = 0
        Me.chkBloquearTactico.Text = "Bloquear Descuentos Tacticos"
        Me.chkBloquearTactico.UseVisualStyleBackColor = True
        '
        'chkControlarDescuentoTactico
        '
        Me.chkControlarDescuentoTactico.AutoSize = True
        Me.chkControlarDescuentoTactico.Location = New System.Drawing.Point(13, 37)
        Me.chkControlarDescuentoTactico.Name = "chkControlarDescuentoTactico"
        Me.chkControlarDescuentoTactico.Size = New System.Drawing.Size(175, 17)
        Me.chkControlarDescuentoTactico.TabIndex = 1
        Me.chkControlarDescuentoTactico.Text = "Controlar Descuentos Tacticos:"
        Me.chkControlarDescuentoTactico.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(316, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(235, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 348)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(394, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(400, 380)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.chkCargarExcepcionSinAutorizacion)
        Me.Panel1.Controls.Add(Me.chkBloquearListaPrecio)
        Me.Panel1.Controls.Add(Me.chkAdvertirCantidadNegativa)
        Me.Panel1.Controls.Add(Me.chkPermitirCantidadNegativa)
        Me.Panel1.Controls.Add(Me.chkControlarTacticoImporte)
        Me.Panel1.Controls.Add(Me.chkControlarTacticoPorcentaje)
        Me.Panel1.Controls.Add(Me.chkBloquearTactico)
        Me.Panel1.Controls.Add(Me.nudCantidadPedido)
        Me.Panel1.Controls.Add(Me.chkControlarDescuentoTactico)
        Me.Panel1.Controls.Add(Me.lblCantidadPedido)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 339)
        Me.Panel1.TabIndex = 0
        '
        'chkBloquearListaPrecio
        '
        Me.chkBloquearListaPrecio.AutoSize = True
        Me.chkBloquearListaPrecio.Location = New System.Drawing.Point(13, 184)
        Me.chkBloquearListaPrecio.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.chkBloquearListaPrecio.Name = "chkBloquearListaPrecio"
        Me.chkBloquearListaPrecio.Size = New System.Drawing.Size(255, 17)
        Me.chkBloquearListaPrecio.TabIndex = 8
        Me.chkBloquearListaPrecio.Text = "Bloquear lista de precios en pedido y facturacion"
        Me.chkBloquearListaPrecio.UseVisualStyleBackColor = True
        '
        'chkAdvertirCantidadNegativa
        '
        Me.chkAdvertirCantidadNegativa.AutoSize = True
        Me.chkAdvertirCantidadNegativa.Enabled = False
        Me.chkAdvertirCantidadNegativa.Location = New System.Drawing.Point(39, 160)
        Me.chkAdvertirCantidadNegativa.Name = "chkAdvertirCantidadNegativa"
        Me.chkAdvertirCantidadNegativa.Size = New System.Drawing.Size(200, 17)
        Me.chkAdvertirCantidadNegativa.TabIndex = 7
        Me.chkAdvertirCantidadNegativa.Text = "Advertir si la cantidad es insuficiente."
        Me.chkAdvertirCantidadNegativa.UseVisualStyleBackColor = True
        '
        'chkPermitirCantidadNegativa
        '
        Me.chkPermitirCantidadNegativa.AutoSize = True
        Me.chkPermitirCantidadNegativa.Location = New System.Drawing.Point(13, 137)
        Me.chkPermitirCantidadNegativa.Name = "chkPermitirCantidadNegativa"
        Me.chkPermitirCantidadNegativa.Size = New System.Drawing.Size(287, 17)
        Me.chkPermitirCantidadNegativa.TabIndex = 6
        Me.chkPermitirCantidadNegativa.Text = "Permitir reservar los productos sin verificar su existencia"
        Me.chkPermitirCantidadNegativa.UseVisualStyleBackColor = True
        '
        'chkControlarTacticoImporte
        '
        Me.chkControlarTacticoImporte.AutoSize = True
        Me.chkControlarTacticoImporte.Location = New System.Drawing.Point(37, 83)
        Me.chkControlarTacticoImporte.Name = "chkControlarTacticoImporte"
        Me.chkControlarTacticoImporte.Size = New System.Drawing.Size(124, 17)
        Me.chkControlarTacticoImporte.TabIndex = 3
        Me.chkControlarTacticoImporte.Text = "Controlar por Importe"
        Me.chkControlarTacticoImporte.UseVisualStyleBackColor = True
        '
        'chkControlarTacticoPorcentaje
        '
        Me.chkControlarTacticoPorcentaje.AutoSize = True
        Me.chkControlarTacticoPorcentaje.Location = New System.Drawing.Point(37, 60)
        Me.chkControlarTacticoPorcentaje.Name = "chkControlarTacticoPorcentaje"
        Me.chkControlarTacticoPorcentaje.Size = New System.Drawing.Size(140, 17)
        Me.chkControlarTacticoPorcentaje.TabIndex = 2
        Me.chkControlarTacticoPorcentaje.Text = "Controlar por Porcentaje"
        Me.chkControlarTacticoPorcentaje.UseVisualStyleBackColor = True
        '
        'chkCargarExcepcionSinAutorizacion
        '
        Me.chkCargarExcepcionSinAutorizacion.AutoSize = True
        Me.chkCargarExcepcionSinAutorizacion.Location = New System.Drawing.Point(13, 223)
        Me.chkCargarExcepcionSinAutorizacion.Margin = New System.Windows.Forms.Padding(2)
        Me.chkCargarExcepcionSinAutorizacion.Name = "chkCargarExcepcionSinAutorizacion"
        Me.chkCargarExcepcionSinAutorizacion.Size = New System.Drawing.Size(189, 17)
        Me.chkCargarExcepcionSinAutorizacion.TabIndex = 9
        Me.chkCargarExcepcionSinAutorizacion.Text = "Cargar Excepcion Sin Autorizacion"
        Me.chkCargarExcepcionSinAutorizacion.UseVisualStyleBackColor = True
        '
        'rdProductoPrecioSI
        '
        Me.rdProductoPrecioSI.AutoSize = True
        Me.rdProductoPrecioSI.Location = New System.Drawing.Point(6, 19)
        Me.rdProductoPrecioSI.Name = "rdProductoPrecioSI"
        Me.rdProductoPrecioSI.Size = New System.Drawing.Size(107, 17)
        Me.rdProductoPrecioSI.TabIndex = 10
        Me.rdProductoPrecioSI.Text = "Producto - Precio"
        Me.rdProductoPrecioSI.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdProductoPrecioSI)
        Me.GroupBox1.Controls.Add(Me.rdProductoPrecioNO)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 257)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(365, 51)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Configuracion de Precio"
        '
        'rdProductoPrecioNO
        '
        Me.rdProductoPrecioNO.AutoSize = True
        Me.rdProductoPrecioNO.Checked = True
        Me.rdProductoPrecioNO.Location = New System.Drawing.Point(160, 19)
        Me.rdProductoPrecioNO.Name = "rdProductoPrecioNO"
        Me.rdProductoPrecioNO.Size = New System.Drawing.Size(186, 17)
        Me.rdProductoPrecioNO.TabIndex = 11
        Me.rdProductoPrecioNO.TabStop = True
        Me.rdProductoPrecioNO.Text = "Producto - Lista de Precio - Precio"
        Me.rdProductoPrecioNO.UseVisualStyleBackColor = True
        '
        'ocxConfiguracionesPedido
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxConfiguracionesPedido"
        Me.Size = New System.Drawing.Size(400, 380)
        CType(Me.nudCantidadPedido, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkBloquearTactico As System.Windows.Forms.CheckBox
    Friend WithEvents chkControlarDescuentoTactico As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents nudCantidadPedido As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblCantidadPedido As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkControlarTacticoImporte As System.Windows.Forms.CheckBox
    Friend WithEvents chkControlarTacticoPorcentaje As System.Windows.Forms.CheckBox
    Friend WithEvents chkAdvertirCantidadNegativa As System.Windows.Forms.CheckBox
    Friend WithEvents chkPermitirCantidadNegativa As System.Windows.Forms.CheckBox
    Friend WithEvents chkBloquearListaPrecio As System.Windows.Forms.CheckBox
    Friend WithEvents chkCargarExcepcionSinAutorizacion As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rdProductoPrecioSI As RadioButton
    Friend WithEvents rbProductoPrecioNO As RadioButton
    Friend WithEvents rdProductoPrecioNO As RadioButton
End Class

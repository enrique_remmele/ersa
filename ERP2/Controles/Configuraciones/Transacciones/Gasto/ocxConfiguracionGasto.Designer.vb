﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxConfiguracionGasto
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkBloquearEliminacion = New System.Windows.Forms.CheckBox()
        Me.chkSeleccionDistribucion = New System.Windows.Forms.CheckBox()
        Me.nudDiasEliminacion = New System.Windows.Forms.NumericUpDown()
        Me.chkSeleccionDeposito = New System.Windows.Forms.CheckBox()
        Me.lblDiasAntiguedad = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.nudDiasEliminacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(400, 300)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 268)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(394, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(316, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(235, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkBloquearEliminacion)
        Me.Panel1.Controls.Add(Me.chkSeleccionDistribucion)
        Me.Panel1.Controls.Add(Me.nudDiasEliminacion)
        Me.Panel1.Controls.Add(Me.chkSeleccionDeposito)
        Me.Panel1.Controls.Add(Me.lblDiasAntiguedad)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 259)
        Me.Panel1.TabIndex = 0
        '
        'chkBloquearEliminacion
        '
        Me.chkBloquearEliminacion.AutoSize = True
        Me.chkBloquearEliminacion.Location = New System.Drawing.Point(15, 14)
        Me.chkBloquearEliminacion.Name = "chkBloquearEliminacion"
        Me.chkBloquearEliminacion.Size = New System.Drawing.Size(217, 17)
        Me.chkBloquearEliminacion.TabIndex = 0
        Me.chkBloquearEliminacion.Text = "Bloquear eliminacion de documentos de "
        Me.chkBloquearEliminacion.UseVisualStyleBackColor = True
        '
        'chkSeleccionDistribucion
        '
        Me.chkSeleccionDistribucion.AutoSize = True
        Me.chkSeleccionDistribucion.Location = New System.Drawing.Point(15, 60)
        Me.chkSeleccionDistribucion.Name = "chkSeleccionDistribucion"
        Me.chkSeleccionDistribucion.Size = New System.Drawing.Size(344, 17)
        Me.chkSeleccionDistribucion.TabIndex = 4
        Me.chkSeleccionDistribucion.Text = "Habilitar Distribucion (Permite asignarle al gasto un camion y chofer)"
        Me.chkSeleccionDistribucion.UseVisualStyleBackColor = True
        '
        'nudDiasEliminacion
        '
        Me.nudDiasEliminacion.Location = New System.Drawing.Point(238, 13)
        Me.nudDiasEliminacion.Name = "nudDiasEliminacion"
        Me.nudDiasEliminacion.Size = New System.Drawing.Size(40, 20)
        Me.nudDiasEliminacion.TabIndex = 1
        '
        'chkSeleccionDeposito
        '
        Me.chkSeleccionDeposito.AutoSize = True
        Me.chkSeleccionDeposito.Location = New System.Drawing.Point(15, 37)
        Me.chkSeleccionDeposito.Name = "chkSeleccionDeposito"
        Me.chkSeleccionDeposito.Size = New System.Drawing.Size(170, 17)
        Me.chkSeleccionDeposito.TabIndex = 3
        Me.chkSeleccionDeposito.Text = "Habilitar seleccion de deposito"
        Me.chkSeleccionDeposito.UseVisualStyleBackColor = True
        '
        'lblDiasAntiguedad
        '
        Me.lblDiasAntiguedad.AutoSize = True
        Me.lblDiasAntiguedad.Location = New System.Drawing.Point(284, 15)
        Me.lblDiasAntiguedad.Name = "lblDiasAntiguedad"
        Me.lblDiasAntiguedad.Size = New System.Drawing.Size(97, 13)
        Me.lblDiasAntiguedad.TabIndex = 2
        Me.lblDiasAntiguedad.Text = "dias de antiguedad"
        '
        'ocxConfiguracionGasto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Name = "ocxConfiguracionGasto"
        Me.Size = New System.Drawing.Size(400, 300)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.nudDiasEliminacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkBloquearEliminacion As System.Windows.Forms.CheckBox
    Friend WithEvents chkSeleccionDistribucion As System.Windows.Forms.CheckBox
    Friend WithEvents nudDiasEliminacion As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkSeleccionDeposito As System.Windows.Forms.CheckBox
    Friend WithEvents lblDiasAntiguedad As System.Windows.Forms.Label

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxConfiguracionOrdenPago
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtALaOrden = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.lblDiasAntiguedad = New System.Windows.Forms.Label()
        Me.nudDiasEliminacion = New System.Windows.Forms.NumericUpDown()
        Me.chkBloquearEliminacion = New System.Windows.Forms.CheckBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtImporteRetencion = New ERP.ocxTXTNumeric()
        Me.lblImporteRetencion = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPorcentajeRetencion = New ERP.ocxTXTNumeric()
        Me.lblRetencion = New System.Windows.Forms.Label()
        Me.lblPorcentajeRetencion = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.nudDiasEliminacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtALaOrden
        '
        Me.txtALaOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtALaOrden.Color = System.Drawing.Color.Empty
        Me.txtALaOrden.Indicaciones = Nothing
        Me.txtALaOrden.Location = New System.Drawing.Point(16, 84)
        Me.txtALaOrden.Multilinea = False
        Me.txtALaOrden.Name = "txtALaOrden"
        Me.txtALaOrden.Size = New System.Drawing.Size(373, 21)
        Me.txtALaOrden.SoloLectura = False
        Me.txtALaOrden.TabIndex = 8
        Me.txtALaOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtALaOrden.Texto = ""
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(13, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(376, 29)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "- Nombre o Razon Social predefinida para la emision del cheque (Si es que no    s" & _
    "e selecciona un proveedor)"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 265)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(394, 32)
        Me.FlowLayoutPanel1.TabIndex = 5
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(316, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(235, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'lblDiasAntiguedad
        '
        Me.lblDiasAntiguedad.AutoSize = True
        Me.lblDiasAntiguedad.Location = New System.Drawing.Point(282, 15)
        Me.lblDiasAntiguedad.Name = "lblDiasAntiguedad"
        Me.lblDiasAntiguedad.Size = New System.Drawing.Size(97, 13)
        Me.lblDiasAntiguedad.TabIndex = 4
        Me.lblDiasAntiguedad.Text = "dias de antiguedad"
        '
        'nudDiasEliminacion
        '
        Me.nudDiasEliminacion.Location = New System.Drawing.Point(236, 13)
        Me.nudDiasEliminacion.Name = "nudDiasEliminacion"
        Me.nudDiasEliminacion.Size = New System.Drawing.Size(40, 20)
        Me.nudDiasEliminacion.TabIndex = 3
        '
        'chkBloquearEliminacion
        '
        Me.chkBloquearEliminacion.AutoSize = True
        Me.chkBloquearEliminacion.Location = New System.Drawing.Point(13, 14)
        Me.chkBloquearEliminacion.Name = "chkBloquearEliminacion"
        Me.chkBloquearEliminacion.Size = New System.Drawing.Size(217, 17)
        Me.chkBloquearEliminacion.TabIndex = 2
        Me.chkBloquearEliminacion.Text = "Bloquear eliminacion de documentos de "
        Me.chkBloquearEliminacion.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(400, 300)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtImporteRetencion)
        Me.Panel1.Controls.Add(Me.lblImporteRetencion)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtPorcentajeRetencion)
        Me.Panel1.Controls.Add(Me.lblRetencion)
        Me.Panel1.Controls.Add(Me.lblPorcentajeRetencion)
        Me.Panel1.Controls.Add(Me.txtALaOrden)
        Me.Panel1.Controls.Add(Me.chkBloquearEliminacion)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.nudDiasEliminacion)
        Me.Panel1.Controls.Add(Me.lblDiasAntiguedad)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 256)
        Me.Panel1.TabIndex = 3
        '
        'txtImporteRetencion
        '
        Me.txtImporteRetencion.Color = System.Drawing.Color.Empty
        Me.txtImporteRetencion.Decimales = True
        Me.txtImporteRetencion.Indicaciones = Nothing
        Me.txtImporteRetencion.Location = New System.Drawing.Point(140, 167)
        Me.txtImporteRetencion.Name = "txtImporteRetencion"
        Me.txtImporteRetencion.Size = New System.Drawing.Size(89, 21)
        Me.txtImporteRetencion.SoloLectura = False
        Me.txtImporteRetencion.TabIndex = 15
        Me.txtImporteRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteRetencion.Texto = "0"
        '
        'lblImporteRetencion
        '
        Me.lblImporteRetencion.AutoSize = True
        Me.lblImporteRetencion.Location = New System.Drawing.Point(48, 171)
        Me.lblImporteRetencion.Name = "lblImporteRetencion"
        Me.lblImporteRetencion.Size = New System.Drawing.Size(86, 13)
        Me.lblImporteRetencion.TabIndex = 14
        Me.lblImporteRetencion.Text = "- Importe minimo:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(235, 143)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(15, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "%"
        '
        'txtPorcentajeRetencion
        '
        Me.txtPorcentajeRetencion.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeRetencion.Decimales = True
        Me.txtPorcentajeRetencion.Indicaciones = Nothing
        Me.txtPorcentajeRetencion.Location = New System.Drawing.Point(205, 138)
        Me.txtPorcentajeRetencion.Name = "txtPorcentajeRetencion"
        Me.txtPorcentajeRetencion.Size = New System.Drawing.Size(24, 22)
        Me.txtPorcentajeRetencion.SoloLectura = False
        Me.txtPorcentajeRetencion.TabIndex = 12
        Me.txtPorcentajeRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeRetencion.Texto = "0"
        '
        'lblRetencion
        '
        Me.lblRetencion.AutoSize = True
        Me.lblRetencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetencion.Location = New System.Drawing.Point(23, 119)
        Me.lblRetencion.Name = "lblRetencion"
        Me.lblRetencion.Size = New System.Drawing.Size(69, 13)
        Me.lblRetencion.TabIndex = 11
        Me.lblRetencion.Text = "Retencion:"
        '
        'lblPorcentajeRetencion
        '
        Me.lblPorcentajeRetencion.AutoSize = True
        Me.lblPorcentajeRetencion.Location = New System.Drawing.Point(48, 143)
        Me.lblPorcentajeRetencion.Name = "lblPorcentajeRetencion"
        Me.lblPorcentajeRetencion.Size = New System.Drawing.Size(154, 13)
        Me.lblPorcentajeRetencion.TabIndex = 10
        Me.lblPorcentajeRetencion.Text = "- Porcentaje de Retencion IVA:"
        '
        'ocxConfiguracionOrdenPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxConfiguracionOrdenPago"
        Me.Size = New System.Drawing.Size(400, 300)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.nudDiasEliminacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents lblDiasAntiguedad As System.Windows.Forms.Label
    Friend WithEvents nudDiasEliminacion As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkBloquearEliminacion As System.Windows.Forms.CheckBox
    Friend WithEvents txtALaOrden As ERP.ocxTXTString
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtImporteRetencion As ERP.ocxTXTNumeric
    Friend WithEvents lblImporteRetencion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeRetencion As ERP.ocxTXTNumeric
    Friend WithEvents lblRetencion As System.Windows.Forms.Label
    Friend WithEvents lblPorcentajeRetencion As System.Windows.Forms.Label

End Class

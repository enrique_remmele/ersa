﻿Public Class ocxConfiguracionOrdenPago

    'CLASES
    Dim CSistema As New CSistema
    Public Property IDSucursal As Integer = 0
    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            If IDSucursal = 0 Then
                IDSucursal = vgIDSucursal
            End If

            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones Where IDSucursal=" & IDSucursal)(0)

            Dim Dias As Integer = CSistema.RetornarValorInteger(oRow("OrdenPagoDiasBloqueo").ToString)
            Dim Bloquear As Boolean = CSistema.RetornarValorBoolean(oRow("OrdenPagoBloquearAnulacion").ToString)
            Dim ALAOrden As String = CSistema.RetornarValorString(oRow("OrdenPagoALaOrdenPredefinido").ToString)

            chkBloquearEliminacion.Checked = Bloquear
            nudDiasEliminacion.Value = Dias
            txtALaOrden.txt.Text = ALAOrden

            txtImporteRetencion.SetValue(oRow("CompraImporteMinimoRetencion"))
            txtPorcentajeRetencion.SetValue(oRow("CompraPorcentajeRetencion").ToString)

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim sql As String
        sql = "Update Configuraciones Set "
        sql = sql & " OrdenPagoBloquearAnulacion = '" & chkBloquearEliminacion.Checked.ToString & "'" & vbCrLf
        sql = sql & ", OrdenPagoDiasBloqueo = '" & nudDiasEliminacion.Value & "'" & vbCrLf
        sql = sql & ", OrdenPagoALaOrdenPredefinido = '" & txtALaOrden.txt.Text & "'" & vbCrLf
        sql = sql & ", CompraImporteMinimoRetencion = '" & txtImporteRetencion.ObtenerValor & "'" & vbCrLf
        sql = sql & ", CompraPorcentajeRetencion = '" & txtPorcentajeRetencion.ObtenerValor & "'" & vbCrLf
        sql = sql & "Where IDSucursal = " & IDSucursal & " "

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            MessageBox.Show("Registro procesado!", "Orden de Pago", MessageBoxButtons.OK, MessageBoxIcon.Information)
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Orden de Pago", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ObtenerInformacion()
    End Sub

    Private Sub ocxConfiguracionOrdenPago_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

End Class

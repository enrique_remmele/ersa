﻿Public Class ocxConfiguracionCobranzaCredito

    'CLASES
    Dim CSistema As New CSistema
    Public Property IDSucursal As Integer = 0
    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            If IDSucursal = 0 Then
                IDSucursal = vgIDSucursal
            End If

            Dim IDOperacion As Integer = CSistema.ObtenerIDOperacion("frmSeleccionFormaPagoDocumento", "PAGOS Y COBROS CON DOCUMENTOS", "RET")
            CSistema.SqlToComboBox(cbxTipoComprobanteRetencion, "Select ID, Descripcion From VTipoComprobante Where IDOperacion=" & IDOperacion)
            'Acreditacion Bancaria
            CSistema.SqlToComboBox(cbxTipoAcreditacionBancaria, "Select ID, Descripcion From VTipoComprobante Where IDOperacion=" & IDOperacion)
            Dim IDAcreditacionBancaria As Integer = CSistema.ExecuteScalar("Select IsNull((Select Top(1) CobranzaCreditoTipoCAcreditacionBancaria From Configuraciones Where IDSucursal=" & IDSucursal & "), 0 )")
            'cbxTipoAcreditacionBancaria.cbx.SelectedValue = CSistema.ExecuteScalar("Select Top(1) ID From TipoComprobante Where ID=" & IDAcreditacionBancaria)
            cbxTipoAcreditacionBancaria.cbx.SelectedValue = IDAcreditacionBancaria

            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones Where IDSucursal=" & IDSucursal)(0)

            chkBloquearFecha.Checked = CSistema.RetornarValorBoolean(oRow("CobranzaCreditoBloquearFecha").ToString)
            chkImprimirDocumento.Checked = CSistema.RetornarValorBoolean(oRow("CobranzaCreditoImprimirDocumento").ToString)
            chkBloquearAnulacion.Checked = CSistema.RetornarValorBoolean(oRow("CobranzaCreditoBloquearAnulacion").ToString)
            chkControlarNroRecibo.Checked = CSistema.RetornarValorBoolean(oRow("CobranzaControlarNroRecibo").ToString)
            Dim Dias As Integer = CSistema.RetornarValorInteger(oRow("CobranzaCreditoDiasBloqueo").ToString)
            nudDiasAnulacion.Value = Dias
            cbxTipoComprobanteRetencion.SelectedValue(CSistema.RetornarValorInteger(oRow("CobranzaCreditoTipoComprobanteRetencion").ToString))
            cbxTipoAcreditacionBancaria.SelectedValue(CSistema.RetornarValorInteger(oRow("CobranzaCreditoTipoComprobanteRetencion").ToString))
            chkContadoChequeDiferido.Checked = CSistema.RetornarValorBoolean(oRow("ContadoChequeDiferido").ToString)
        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim CostoProducto As String = ""

        Dim sql As String = ""

        sql = sql & "Update Configuraciones Set " & vbCrLf
        sql = sql & "CobranzaCreditoBloquearFecha = '" & chkBloquearFecha.Checked.ToString & "', " & vbCrLf
        sql = sql & "CobranzaCreditoImprimirDocumento = '" & chkImprimirDocumento.Checked.ToString & "', " & vbCrLf
        sql = sql & "CobranzaCreditoBloquearAnulacion = '" & chkBloquearAnulacion.Checked.ToString & "', " & vbCrLf
        sql = sql & "CobranzaCreditoDiasBloqueo = '" & nudDiasAnulacion.Value & "', " & vbCrLf
        sql = sql & "CobranzaCreditoTipoComprobanteRetencion = " & cbxTipoComprobanteRetencion.GetValue & ", " & vbCrLf
        sql = sql & "ContadoChequeDiferido = '" & chkContadoChequeDiferido.Checked.ToString & "', " & vbCrLf
        sql = sql & "CobranzaCreditoTipoCAcreditacionBancaria = " & cbxTipoAcreditacionBancaria.GetValue & ", " & vbCrLf
        sql = sql & "CobranzaControlarNroRecibo= '" & chkControlarNroRecibo.Checked & "'" & vbCrLf
        sql = sql & "Where IDSucursal=" & IDSucursal

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            MessageBox.Show("Registro procesado!", "Cobranza Credito", MessageBoxButtons.OK, MessageBoxIcon.Information)
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Cobranza Credito", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

End Class

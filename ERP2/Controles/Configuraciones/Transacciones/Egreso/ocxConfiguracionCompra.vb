Public Class ocxConfiguracionCompra

    'CLASES
    Dim CSistema As New CSistema
    Public Property IDSucursal As Integer = 0

    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            If IDSucursal = 0 Then
                IDSucursal = vgIDSucursal
            End If

            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones Where IDSucursal=" & IDSucursal)(0)

            chkBloquearEliminacion.Checked = CSistema.RetornarValorBoolean(oRow("CompraBloquearEliminacion").ToString)
            nudDiasEliminacion.Value = CSistema.RetornarValorInteger(oRow("CompraDiasBloqueo").ToString)
            chkBloquearCondicionCompra.Checked = CSistema.RetornarValorBoolean(oRow("CompraBloquearCondicion").ToString)
            txtImporteRetencion.SetValue(oRow("CompraImporteMinimoRetencion"))
            txtPorcentajeRetencion.SetValue(oRow("CompraPorcentajeRetencion").ToString)

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim sql As String
        sql = "Update Configuraciones Set "
        sql = sql & "CompraBloquearEliminacion = '" & chkBloquearEliminacion.Checked.ToString & "'" & vbCrLf
        sql = sql & ", CompraDiasBloqueo = '" & nudDiasEliminacion.Value & "'" & vbCrLf
        sql = sql & ", CompraBloquearCondicion = '" & chkBloquearCondicionCompra.Checked & "'" & vbCrLf
        sql = sql & ", CompraImporteMinimoRetencion = '" & txtImporteRetencion.ObtenerValor & "'" & vbCrLf
        sql = sql & ", CompraPorcentajeRetencion = '" & txtPorcentajeRetencion.ObtenerValor & "'" & vbCrLf
        sql = sql & "Where IDSucursal = " & IDSucursal & " "

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            MessageBox.Show("Registro procesado!", "Gasto", MessageBoxButtons.OK, MessageBoxIcon.Information)
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Gasto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ObtenerInformacion()
    End Sub

    Private Sub ocxConfiguracionGasto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ObtenerInformacion()
    End Sub


End Class
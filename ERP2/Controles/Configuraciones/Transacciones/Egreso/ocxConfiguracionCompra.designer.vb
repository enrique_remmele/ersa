<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxConfiguracionCompra
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.nudDiasEliminacion = New System.Windows.Forms.NumericUpDown()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.chkBloquearEliminacion = New System.Windows.Forms.CheckBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblDiasAntiguedad = New System.Windows.Forms.Label()
        Me.lblPorcentajeRetencion = New System.Windows.Forms.Label()
        Me.chkBloquearCondicionCompra = New System.Windows.Forms.CheckBox()
        Me.lblRetencion = New System.Windows.Forms.Label()
        Me.txtPorcentajeRetencion = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtImporteRetencion = New ERP.ocxTXTNumeric()
        Me.lblImporteRetencion = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDiasEliminacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'nudDiasEliminacion
        '
        Me.nudDiasEliminacion.Location = New System.Drawing.Point(238, 13)
        Me.nudDiasEliminacion.Name = "nudDiasEliminacion"
        Me.nudDiasEliminacion.Size = New System.Drawing.Size(40, 20)
        Me.nudDiasEliminacion.TabIndex = 1
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 288)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(428, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(350, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(269, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'chkBloquearEliminacion
        '
        Me.chkBloquearEliminacion.AutoSize = True
        Me.chkBloquearEliminacion.Location = New System.Drawing.Point(15, 14)
        Me.chkBloquearEliminacion.Name = "chkBloquearEliminacion"
        Me.chkBloquearEliminacion.Size = New System.Drawing.Size(217, 17)
        Me.chkBloquearEliminacion.TabIndex = 0
        Me.chkBloquearEliminacion.Text = "Bloquear eliminacion de documentos de "
        Me.chkBloquearEliminacion.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(434, 320)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtImporteRetencion)
        Me.Panel1.Controls.Add(Me.lblImporteRetencion)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtPorcentajeRetencion)
        Me.Panel1.Controls.Add(Me.lblRetencion)
        Me.Panel1.Controls.Add(Me.chkBloquearCondicionCompra)
        Me.Panel1.Controls.Add(Me.lblPorcentajeRetencion)
        Me.Panel1.Controls.Add(Me.chkBloquearEliminacion)
        Me.Panel1.Controls.Add(Me.nudDiasEliminacion)
        Me.Panel1.Controls.Add(Me.lblDiasAntiguedad)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(428, 279)
        Me.Panel1.TabIndex = 0
        '
        'lblDiasAntiguedad
        '
        Me.lblDiasAntiguedad.AutoSize = True
        Me.lblDiasAntiguedad.Location = New System.Drawing.Point(284, 15)
        Me.lblDiasAntiguedad.Name = "lblDiasAntiguedad"
        Me.lblDiasAntiguedad.Size = New System.Drawing.Size(97, 13)
        Me.lblDiasAntiguedad.TabIndex = 2
        Me.lblDiasAntiguedad.Text = "dias de antiguedad"
        '
        'lblPorcentajeRetencion
        '
        Me.lblPorcentajeRetencion.AutoSize = True
        Me.lblPorcentajeRetencion.Location = New System.Drawing.Point(37, 101)
        Me.lblPorcentajeRetencion.Name = "lblPorcentajeRetencion"
        Me.lblPorcentajeRetencion.Size = New System.Drawing.Size(154, 13)
        Me.lblPorcentajeRetencion.TabIndex = 3
        Me.lblPorcentajeRetencion.Text = "- Porcentaje de Retencion IVA:"
        '
        'chkBloquearCondicionCompra
        '
        Me.chkBloquearCondicionCompra.AutoSize = True
        Me.chkBloquearCondicionCompra.Location = New System.Drawing.Point(15, 37)
        Me.chkBloquearCondicionCompra.Name = "chkBloquearCondicionCompra"
        Me.chkBloquearCondicionCompra.Size = New System.Drawing.Size(170, 17)
        Me.chkBloquearCondicionCompra.TabIndex = 4
        Me.chkBloquearCondicionCompra.Text = "Bloquear condicion de compra"
        Me.chkBloquearCondicionCompra.UseVisualStyleBackColor = True
        '
        'lblRetencion
        '
        Me.lblRetencion.AutoSize = True
        Me.lblRetencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetencion.Location = New System.Drawing.Point(12, 77)
        Me.lblRetencion.Name = "lblRetencion"
        Me.lblRetencion.Size = New System.Drawing.Size(69, 13)
        Me.lblRetencion.TabIndex = 5
        Me.lblRetencion.Text = "Retencion:"
        '
        'txtPorcentajeRetencion
        '
        Me.txtPorcentajeRetencion.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeRetencion.Decimales = True
        Me.txtPorcentajeRetencion.Indicaciones = Nothing
        Me.txtPorcentajeRetencion.Location = New System.Drawing.Point(194, 96)
        Me.txtPorcentajeRetencion.Name = "txtPorcentajeRetencion"
        Me.txtPorcentajeRetencion.Size = New System.Drawing.Size(24, 22)
        Me.txtPorcentajeRetencion.SoloLectura = False
        Me.txtPorcentajeRetencion.TabIndex = 6
        Me.txtPorcentajeRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeRetencion.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(224, 101)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(15, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "%"
        '
        'txtImporteRetencion
        '
        Me.txtImporteRetencion.Color = System.Drawing.Color.Empty
        Me.txtImporteRetencion.Decimales = True
        Me.txtImporteRetencion.Indicaciones = Nothing
        Me.txtImporteRetencion.Location = New System.Drawing.Point(129, 125)
        Me.txtImporteRetencion.Name = "txtImporteRetencion"
        Me.txtImporteRetencion.Size = New System.Drawing.Size(89, 21)
        Me.txtImporteRetencion.SoloLectura = False
        Me.txtImporteRetencion.TabIndex = 9
        Me.txtImporteRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteRetencion.Texto = "0"
        '
        'lblImporteRetencion
        '
        Me.lblImporteRetencion.AutoSize = True
        Me.lblImporteRetencion.Location = New System.Drawing.Point(37, 129)
        Me.lblImporteRetencion.Name = "lblImporteRetencion"
        Me.lblImporteRetencion.Size = New System.Drawing.Size(86, 13)
        Me.lblImporteRetencion.TabIndex = 8
        Me.lblImporteRetencion.Text = "- Importe minimo:"
        '
        'ocxConfiguracionCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Name = "ocxConfiguracionCompra"
        Me.Size = New System.Drawing.Size(434, 320)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDiasEliminacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkBloquearEliminacion As System.Windows.Forms.CheckBox
    Friend WithEvents nudDiasEliminacion As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDiasAntiguedad As System.Windows.Forms.Label
    Friend WithEvents txtImporteRetencion As ERP.ocxTXTNumeric
    Friend WithEvents lblImporteRetencion As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeRetencion As ERP.ocxTXTNumeric
    Friend WithEvents lblRetencion As System.Windows.Forms.Label
    Friend WithEvents chkBloquearCondicionCompra As System.Windows.Forms.CheckBox
    Friend WithEvents lblPorcentajeRetencion As System.Windows.Forms.Label

End Class
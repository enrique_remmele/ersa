﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxConexionBaseDeDatos
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblServidor = New System.Windows.Forms.Label()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.lblBaseDatos = New System.Windows.Forms.Label()
        Me.btnProbar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel8 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cbxBaseDatos = New ERP.ocxCBX()
        Me.lklNuevo = New System.Windows.Forms.LinkLabel()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtPassword = New ERP.ocxTXTString()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtUsuario = New ERP.ocxTXTString()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cbxServidor = New ERP.ocxCBX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblEmpresa = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtEmpresa = New ERP.ocxTXTString()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.gbxUsuario = New System.Windows.Forms.GroupBox()
        Me.txtPath = New ERP.ocxTXTString()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtContraseña = New ERP.ocxTXTString()
        Me.lblContraseña = New System.Windows.Forms.Label()
        Me.txtUsuarioFTP = New ERP.ocxTXTString()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chkConfiarSSL = New ERP.ocxCHK()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel8.SuspendLayout()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.gbxUsuario.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblServidor
        '
        Me.lblServidor.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblServidor.Location = New System.Drawing.Point(17, 0)
        Me.lblServidor.Name = "lblServidor"
        Me.lblServidor.Size = New System.Drawing.Size(49, 24)
        Me.lblServidor.TabIndex = 0
        Me.lblServidor.Text = "Servidor:"
        Me.lblServidor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUsuario
        '
        Me.lblUsuario.Location = New System.Drawing.Point(20, 0)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(46, 24)
        Me.lblUsuario.TabIndex = 0
        Me.lblUsuario.Text = "Usuario:"
        Me.lblUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPassword
        '
        Me.lblPassword.Location = New System.Drawing.Point(10, 0)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(56, 24)
        Me.lblPassword.TabIndex = 0
        Me.lblPassword.Text = "Password:"
        Me.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBaseDatos
        '
        Me.lblBaseDatos.Location = New System.Drawing.Point(-14, 0)
        Me.lblBaseDatos.Name = "lblBaseDatos"
        Me.lblBaseDatos.Size = New System.Drawing.Size(80, 24)
        Me.lblBaseDatos.TabIndex = 0
        Me.lblBaseDatos.Text = "Base de Datos:"
        Me.lblBaseDatos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnProbar
        '
        Me.btnProbar.Location = New System.Drawing.Point(70, 5)
        Me.btnProbar.Name = "btnProbar"
        Me.btnProbar.Size = New System.Drawing.Size(75, 23)
        Me.btnProbar.TabIndex = 0
        Me.btnProbar.Text = "&Probar"
        Me.btnProbar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(80, 241)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(182, 241)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.8718!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.1282!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel8, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel7, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel6, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel5, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel4, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 1, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(303, 205)
        Me.TableLayoutPanel1.TabIndex = 0
        Me.TableLayoutPanel1.TabStop = True
        '
        'FlowLayoutPanel8
        '
        Me.FlowLayoutPanel8.Controls.Add(Me.cbxBaseDatos)
        Me.FlowLayoutPanel8.Controls.Add(Me.lklNuevo)
        Me.FlowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel8.Location = New System.Drawing.Point(78, 127)
        Me.FlowLayoutPanel8.Name = "FlowLayoutPanel8"
        Me.FlowLayoutPanel8.Size = New System.Drawing.Size(222, 27)
        Me.FlowLayoutPanel8.TabIndex = 8
        '
        'cbxBaseDatos
        '
        Me.cbxBaseDatos.CampoWhere = Nothing
        Me.cbxBaseDatos.CargarUnaSolaVez = False
        Me.cbxBaseDatos.DataDisplayMember = Nothing
        Me.cbxBaseDatos.DataFilter = Nothing
        Me.cbxBaseDatos.DataOrderBy = Nothing
        Me.cbxBaseDatos.DataSource = Nothing
        Me.cbxBaseDatos.DataValueMember = Nothing
        Me.cbxBaseDatos.dtSeleccionado = Nothing
        Me.cbxBaseDatos.FormABM = Nothing
        Me.cbxBaseDatos.Indicaciones = Nothing
        Me.cbxBaseDatos.Location = New System.Drawing.Point(3, 3)
        Me.cbxBaseDatos.Name = "cbxBaseDatos"
        Me.cbxBaseDatos.SeleccionMultiple = False
        Me.cbxBaseDatos.SeleccionObligatoria = True
        Me.cbxBaseDatos.Size = New System.Drawing.Size(142, 21)
        Me.cbxBaseDatos.SoloLectura = False
        Me.cbxBaseDatos.TabIndex = 0
        Me.cbxBaseDatos.Texto = ""
        '
        'lklNuevo
        '
        Me.lklNuevo.Location = New System.Drawing.Point(151, 0)
        Me.lklNuevo.Name = "lklNuevo"
        Me.lklNuevo.Size = New System.Drawing.Size(49, 21)
        Me.lklNuevo.TabIndex = 1
        Me.lklNuevo.TabStop = True
        Me.lklNuevo.Text = "Nuevo"
        Me.lklNuevo.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.Controls.Add(Me.lblBaseDatos)
        Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel7.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(3, 127)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(69, 27)
        Me.FlowLayoutPanel7.TabIndex = 7
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.Controls.Add(Me.txtPassword)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(78, 96)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(222, 25)
        Me.FlowLayoutPanel6.TabIndex = 6
        '
        'txtPassword
        '
        Me.txtPassword.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPassword.Color = System.Drawing.Color.Empty
        Me.txtPassword.Indicaciones = Nothing
        Me.txtPassword.Location = New System.Drawing.Point(3, 3)
        Me.txtPassword.Multilinea = False
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(197, 21)
        Me.txtPassword.SoloLectura = False
        Me.txtPassword.TabIndex = 0
        Me.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPassword.Texto = ""
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.lblPassword)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(3, 96)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(69, 25)
        Me.FlowLayoutPanel5.TabIndex = 5
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.txtUsuario)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(78, 65)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(222, 25)
        Me.FlowLayoutPanel4.TabIndex = 4
        '
        'txtUsuario
        '
        Me.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuario.Color = System.Drawing.Color.Empty
        Me.txtUsuario.Indicaciones = Nothing
        Me.txtUsuario.Location = New System.Drawing.Point(3, 3)
        Me.txtUsuario.Multilinea = False
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(197, 21)
        Me.txtUsuario.SoloLectura = False
        Me.txtUsuario.TabIndex = 0
        Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuario.Texto = ""
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.lblUsuario)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 65)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(69, 25)
        Me.FlowLayoutPanel3.TabIndex = 3
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cbxServidor)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(78, 34)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(222, 25)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'cbxServidor
        '
        Me.cbxServidor.CampoWhere = Nothing
        Me.cbxServidor.CargarUnaSolaVez = False
        Me.cbxServidor.DataDisplayMember = Nothing
        Me.cbxServidor.DataFilter = Nothing
        Me.cbxServidor.DataOrderBy = Nothing
        Me.cbxServidor.DataSource = Nothing
        Me.cbxServidor.DataValueMember = Nothing
        Me.cbxServidor.dtSeleccionado = Nothing
        Me.cbxServidor.FormABM = Nothing
        Me.cbxServidor.Indicaciones = Nothing
        Me.cbxServidor.Location = New System.Drawing.Point(3, 3)
        Me.cbxServidor.Name = "cbxServidor"
        Me.cbxServidor.SeleccionMultiple = False
        Me.cbxServidor.SeleccionObligatoria = False
        Me.cbxServidor.Size = New System.Drawing.Size(197, 21)
        Me.cbxServidor.SoloLectura = False
        Me.cbxServidor.TabIndex = 0
        Me.cbxServidor.Texto = ""
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.lblServidor)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 34)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(69, 25)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'Panel1
        '
        Me.TableLayoutPanel1.SetColumnSpan(Me.Panel1, 2)
        Me.Panel1.Controls.Add(Me.btnProbar)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 160)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(297, 42)
        Me.Panel1.TabIndex = 9
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(172, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&Refrescar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblEmpresa)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(69, 25)
        Me.Panel2.TabIndex = 9
        '
        'lblEmpresa
        '
        Me.lblEmpresa.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblEmpresa.Location = New System.Drawing.Point(0, 0)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(69, 24)
        Me.lblEmpresa.TabIndex = 0
        Me.lblEmpresa.Text = "Empresa:"
        Me.lblEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txtEmpresa)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(78, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(222, 25)
        Me.Panel3.TabIndex = 0
        '
        'txtEmpresa
        '
        Me.txtEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmpresa.Color = System.Drawing.Color.Empty
        Me.txtEmpresa.Indicaciones = Nothing
        Me.txtEmpresa.Location = New System.Drawing.Point(3, 1)
        Me.txtEmpresa.Multilinea = False
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(197, 21)
        Me.txtEmpresa.SoloLectura = False
        Me.txtEmpresa.TabIndex = 0
        Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEmpresa.Texto = ""
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(317, 237)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.TableLayoutPanel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(309, 211)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Conexión BD"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.gbxUsuario)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(309, 211)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Conexión FTP"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'gbxUsuario
        '
        Me.gbxUsuario.Controls.Add(Me.txtPath)
        Me.gbxUsuario.Controls.Add(Me.Label1)
        Me.gbxUsuario.Controls.Add(Me.txtContraseña)
        Me.gbxUsuario.Controls.Add(Me.lblContraseña)
        Me.gbxUsuario.Controls.Add(Me.txtUsuarioFTP)
        Me.gbxUsuario.Controls.Add(Me.Label2)
        Me.gbxUsuario.Location = New System.Drawing.Point(8, 6)
        Me.gbxUsuario.Name = "gbxUsuario"
        Me.gbxUsuario.Size = New System.Drawing.Size(291, 199)
        Me.gbxUsuario.TabIndex = 3
        Me.gbxUsuario.TabStop = False
        Me.gbxUsuario.Text = "CUENTA DE ACCESO"
        '
        'txtPath
        '
        Me.txtPath.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPath.Color = System.Drawing.Color.Empty
        Me.txtPath.Indicaciones = Nothing
        Me.txtPath.Location = New System.Drawing.Point(75, 24)
        Me.txtPath.Multilinea = False
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(191, 21)
        Me.txtPath.SoloLectura = False
        Me.txtPath.TabIndex = 1
        Me.txtPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPath.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Path:"
        '
        'txtContraseña
        '
        Me.txtContraseña.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtContraseña.Color = System.Drawing.Color.Empty
        Me.txtContraseña.Indicaciones = Nothing
        Me.txtContraseña.Location = New System.Drawing.Point(75, 78)
        Me.txtContraseña.Multilinea = False
        Me.txtContraseña.Name = "txtContraseña"
        Me.txtContraseña.Size = New System.Drawing.Size(191, 21)
        Me.txtContraseña.SoloLectura = False
        Me.txtContraseña.TabIndex = 5
        Me.txtContraseña.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtContraseña.Texto = ""
        '
        'lblContraseña
        '
        Me.lblContraseña.AutoSize = True
        Me.lblContraseña.Location = New System.Drawing.Point(3, 82)
        Me.lblContraseña.Name = "lblContraseña"
        Me.lblContraseña.Size = New System.Drawing.Size(64, 13)
        Me.lblContraseña.TabIndex = 4
        Me.lblContraseña.Text = "Contraseña:"
        '
        'txtUsuarioFTP
        '
        Me.txtUsuarioFTP.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtUsuarioFTP.Color = System.Drawing.Color.Empty
        Me.txtUsuarioFTP.Indicaciones = Nothing
        Me.txtUsuarioFTP.Location = New System.Drawing.Point(75, 51)
        Me.txtUsuarioFTP.Multilinea = False
        Me.txtUsuarioFTP.Name = "txtUsuarioFTP"
        Me.txtUsuarioFTP.Size = New System.Drawing.Size(191, 21)
        Me.txtUsuarioFTP.SoloLectura = False
        Me.txtUsuarioFTP.TabIndex = 3
        Me.txtUsuarioFTP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtUsuarioFTP.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Usuario:"
        '
        'chkConfiarSSL
        '
        Me.chkConfiarSSL.BackColor = System.Drawing.Color.Transparent
        Me.chkConfiarSSL.Color = System.Drawing.Color.Empty
        Me.chkConfiarSSL.Location = New System.Drawing.Point(119, 272)
        Me.chkConfiarSSL.Name = "chkConfiarSSL"
        Me.chkConfiarSSL.Size = New System.Drawing.Size(129, 21)
        Me.chkConfiarSSL.SoloLectura = False
        Me.chkConfiarSSL.TabIndex = 8
        Me.chkConfiarSSL.Texto = "ConfiarSSL"
        Me.chkConfiarSSL.Valor = False
        Me.chkConfiarSSL.Visible = False
        '
        'ocxConexionBaseDeDatos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.chkConfiarSSL)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnSalir)
        Me.Name = "ocxConexionBaseDeDatos"
        Me.Size = New System.Drawing.Size(323, 312)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel8.ResumeLayout(False)
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.gbxUsuario.ResumeLayout(False)
        Me.gbxUsuario.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblServidor As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As ERP.ocxTXTString
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents txtPassword As ERP.ocxTXTString
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents lblBaseDatos As System.Windows.Forms.Label
    Friend WithEvents cbxBaseDatos As ERP.ocxCBX
    Friend WithEvents cbxServidor As ERP.ocxCBX
    Friend WithEvents btnProbar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel8 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel7 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel6 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblEmpresa As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtEmpresa As ERP.ocxTXTString
    Friend WithEvents lklNuevo As System.Windows.Forms.LinkLabel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents gbxUsuario As System.Windows.Forms.GroupBox
    Friend WithEvents txtPath As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtContraseña As ERP.ocxTXTString
    Friend WithEvents lblContraseña As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioFTP As ERP.ocxTXTString
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkConfiarSSL As ocxCHK
End Class

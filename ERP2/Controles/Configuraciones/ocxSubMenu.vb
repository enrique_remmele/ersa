﻿Imports System.Reflection

Public Class ocxSubMenu

    'EVENTOS
    Dim WithEvents SubMenuValue As New Windows.Forms.ToolStripMenuItem
    Public Property SubMenu() As Windows.Forms.ToolStripMenuItem
        Get
            Return SubMenuValue
        End Get
        Set(ByVal value As Windows.Forms.ToolStripMenuItem)
            SubMenuValue = value
        End Set
    End Property

    'PROPIEDADES
    Private TipoFormularioValue As Boolean
    Public Property TipoFormulario() As Boolean
        Get
            Return TipoFormularioValue
        End Get
        Set(ByVal value As Boolean)
            TipoFormularioValue = value
        End Set
    End Property

    Private frmValue As String
    Public Property frm() As String
        Get
            Return frmValue
        End Get
        Set(ByVal value As String)
            frmValue = value
        End Set
    End Property

    Private NombreValue As String
    Public Property Nombre() As String
        Get
            Return NombreValue
        End Get
        Set(ByVal value As String)
            SubMenu.Text = value
            NombreValue = value
        End Set
    End Property

    Private DescripcionValue As String
    Public Property Descripcion() As String
        Get
            Return DescripcionValue
        End Get
        Set(ByVal value As String)
            SubMenu.Tag = value
            DescripcionValue = value
            SubMenu.Name = value
        End Set
    End Property

    Private InicializarValue As Boolean
    Public Property Inicializar() As Boolean
        Get
            Return InicializarValue
        End Get
        Set(ByVal value As Boolean)
            InicializarValue = value
        End Set
    End Property

    'FUNCIONES
    Sub frmShow()

        Dim type As Type = Assembly.GetExecutingAssembly().GetType("ERP." & frm)

        'Verificar primero que no este abierto el formulario
        For Each f As Form In My.Application.OpenForms
            If frm = f.Name Then
                f.Show()
                f.BringToFront()
                Exit Sub
            End If
        Next

        If Not type Is Nothing Then
            Dim form As Object = Activator.CreateInstance(type)

            FGMostrarFormulario(frmPrincipal3, form, Nombre)

            If Inicializar = True Then
                form.inicializar()
            End If

        End If

        FuncionesEspeciales()

    End Sub

    Sub FuncionesEspeciales()

        Select Case Nombre
            Case "Salir"
                If My.Application.OpenForms.Count > 1 Then
                    If MessageBox.Show("Desea cerrar el sistema?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        End
                    End If
                Else
                    End
                End If
            Case "Cerrar Sesion"
                Try
                    For Each frm As Form In My.Application.OpenForms
                        If frm.Name <> frmPrincipal3.Name Then
                            frm.Close()
                        End If
                    Next
                Catch ex As Exception

                End Try

                My.Application.ApplicationContext.MainForm = frmLogin
                FGMostrarFormulario(New Form, frmLogin, "Inicio de Sesion", FormBorderStyle.None)
                frmPrincipal3.Close()

        End Select

    End Sub

    Public Sub EstablecerValores(ByVal vfrm As String, ByVal vNombre As String, ByVal vDescripcion As String, ByVal vInicializar As Boolean)

        Nombre = vNombre
        Descripcion = vDescripcion
        frm = vfrm
        Inicializar = vInicializar

    End Sub

    Private Sub SubMenuValue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubMenuValue.Click
        frmShow()
    End Sub

End Class

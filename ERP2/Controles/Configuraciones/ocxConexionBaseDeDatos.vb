﻿Imports System.Data.SqlClient

Public Class ocxConexionBaseDeDatos

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDecode As New ERP2ED.CDecode

    'PROPIEDADES
    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private HabilitarNuevaBaseDatosValue As Boolean
    Public Property HabilitarNuevaBaseDatos() As Boolean
        Get
            Return HabilitarNuevaBaseDatosValue
        End Get
        Set(ByVal value As Boolean)
            HabilitarNuevaBaseDatosValue = value
        End Set
    End Property

    'Variables
    Dim dt As New DataTable

    Public Sub Inicializar()

        'Form
        Me.ParentForm.AcceptButton = New Button

        Procesado = False

        txtContraseña.txt.UseSystemPasswordChar = True
        txtContraseña.txt.PasswordChar = "*"

        CargarDatos()

        txtUsuario.txt.CharacterCasing = CharacterCasing.Normal
        txtPassword.txt.CharacterCasing = CharacterCasing.Normal

        txtPassword.txt.UseSystemPasswordChar = True
        txtPassword.txt.PasswordChar = "*"
        cbxServidor.cbx.Items.Clear()


        txtEmpresa.SetValue(vgNombreEmpresa)
        cbxServidor.cbx.Items.Add(vgNombreServidor)
        txtUsuario.SetValue(vgNombreUsuarioBD)
        txtPassword.SetValue(vgPasswordBD)
        cbxBaseDatos.cbx.Text = vgNombreBaseDatos

        InstanciasInstaladas()

        cbxServidor.cbx.Text = vgNombreServidor

        lklNuevo.Enabled = False

        If vgNombreServidor IsNot String.Empty AndAlso vgNombreUsuarioBD IsNot String.Empty _
            AndAlso vgNombreBaseDatos IsNot String.Empty AndAlso vgPasswordBD IsNot String.Empty Then

            ListarBaseDatos()
        End If


    End Sub

    Private Sub InstanciasInstaladas()

        Try
            Dim rk As Microsoft.Win32.RegistryKey
            rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Microsoft SQL Server", False)
            Dim instancias() As String
            instancias = CType(rk.GetValue("InstalledInstances"), String())

            For Each s As String In instancias
                If s = "MSSQLSERVER" Then
                    cbxServidor.cbx.Items.Add(My.Computer.Name)
                Else
                    cbxServidor.cbx.Items.Add(My.Computer.Name & "\" & s)
                End If
            Next
        Catch ex As Exception

        End Try

    End Sub

    Function Probar(ByVal BaseDatos As String, Optional ByVal MostrarMensaje As Boolean = True) As Boolean
        Try
            Using conn As New SqlConnection()
                conn.ConnectionString = "Data Source=" & cbxServidor.cbx.Text & ";Initial Catalog=" & BaseDatos & ";User ID=" & txtUsuario.txt.Text & ";Password=" & txtPassword.txt.Text & ";Connection Timeout=5" & ";trustServerCertificate=true"
                conn.Open()
                If MostrarMensaje = True Then
                    MessageBox.Show("¡Conexión exitosa!", "SAIN", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End Using
            Return True
        Catch ex As Exception
            cbxServidor.cbx.Focus()
            MessageBox.Show("¡Conexión fallida! " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End Try

        btnAceptar.Focus()

    End Function

    Sub CargarDatos()

        vgNombreEmpresa = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "EMPRESA", vgLicenciaOtorgadoRazonSocial)
        vgNombreServidor = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "SERVIDOR", ".\SQLEXPRESS")
        vgNombreBaseDatos = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "BASE DE DATOS", "")
        vgNombreUsuarioBD = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "USUARIO", "sa")
        vgPasswordBD = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "PASSWORD", "")
        CDecode.InClearText = vgPasswordBD
        CDecode.Decrypt()
        vgPasswordBD = CDecode.CryptedText

        txtEmpresa.SetValue(vgNombreEmpresa)
        cbxServidor.cbx.Text = vgNombreServidor
        txtUsuario.SetValue(vgNombreUsuarioBD)
        cbxBaseDatos.cbx.Text = vgNombreBaseDatos
        txtPassword.SetValue(vgPasswordBD)

        'Actualizacion
        vgActualizacionPath = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "PATH", "ftp://sain.expressalimentos.com.py")
        vgActualizacionUsuario = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "USUARIO", "sain@expressalimentos.com.py")
        CDecode.InClearText = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "PASSWORD", "")

        CDecode.Decrypt()
        vgActualizacionPassword = CDecode.CryptedText

        txtPath.SetValue(vgActualizacionPath)
        txtUsuarioFTP.SetValue(vgActualizacionUsuario)
        txtContraseña.SetValue(vgActualizacionPassword)

    End Sub

    Sub Guardar()

        If Probar(cbxBaseDatos.cbx.Text, False) = False Then
            Exit Sub
        End If

        'Grabar identificadodes
        vgNombreEmpresa = txtEmpresa.GetValue
        vgNombreServidor = cbxServidor.cbx.Text
        vgNombreUsuarioBD = txtUsuario.GetValue
        vgNombreBaseDatos = cbxBaseDatos.cbx.Text
        vgPasswordBD = txtPassword.GetValue
        VGCadenaConexion = "Data Source=" & MVariablesGlobales.vgNombreServidor & ";Initial Catalog=" & MVariablesGlobales.vgNombreBaseDatos & ";User Id=" & MVariablesGlobales.vgNombreUsuarioBD & ";Password=" & MVariablesGlobales.vgPasswordBD & "" & ";trustServerCertificate=true"

        CArchivoInicio.IniWrite(VGArchivoINI, "BASE DE DATOS", "EMPRESA", vgNombreEmpresa)
        CArchivoInicio.IniWrite(VGArchivoINI, "BASE DE DATOS", "SERVIDOR", vgNombreServidor)
        CArchivoInicio.IniWrite(VGArchivoINI, "BASE DE DATOS", "BASE DE DATOS", vgNombreBaseDatos)
        CArchivoInicio.IniWrite(VGArchivoINI, "BASE DE DATOS", "USUARIO", vgNombreUsuarioBD)
        CDecode.InClearText = vgPasswordBD
        CDecode.Encrypt()
        CArchivoInicio.IniWrite(VGArchivoINI, "BASE DE DATOS", "PASSWORD", CDecode.CryptedText)

        vgActualizacionPassword = txtContraseña.GetValue
        vgActualizacionPath = txtPath.Texto
        vgActualizacionUsuario = txtUsuarioFTP.Texto

        CArchivoInicio.IniWrite(VGArchivoINI, "ACTUALIZACION", "PATH", vgActualizacionPath)
        CArchivoInicio.IniWrite(VGArchivoINI, "ACTUALIZACION", "USUARIO", vgActualizacionUsuario)

        CDecode.InClearText = vgActualizacionPassword
        CDecode.Encrypt()
        CArchivoInicio.IniWrite(VGArchivoINI, "ACTUALIZACION", "PASSWORD", CDecode.CryptedText)

        Procesado = True
        Me.ParentForm.Close()

    End Sub

    Sub ListarBaseDatos()

        lklNuevo.Enabled = False

        cbxBaseDatos.cbx.DataSource = Nothing
        cbxBaseDatos.cbx.Items.Clear()

        If Probar("master", False) = False Then
            Exit Sub
        End If

        Dim sql As String = "SELECT 'ID'=database_id, 'Descripcion'=name, create_date FROM sys.databases"
        CSistema.SqlToComboBox(cbxBaseDatos.cbx, sql, "Data Source=" & cbxServidor.cbx.Text & ";Initial Catalog=master;User ID=" & txtUsuario.txt.Text & ";Password=" & txtPassword.txt.Text & ";Connection Timeout=15" & ";trustServerCertificate=true")
        Try
            If vgNombreBaseDatos IsNot String.Empty Then
                Dim dt As DataTable = cbxBaseDatos.cbx.DataSource
                If dt IsNot Nothing Then
                    For Each row As DataRow In dt.Rows
                        If row.Item(1) = vgNombreBaseDatos Then
                            cbxBaseDatos.cbx.SelectedValue = row.Item(0)
                            Exit For
                        End If
                    Next
                End If

                cbxBaseDatos.cbx.SelectedItem = vgNombreBaseDatos
            End If
        Catch
        End Try

        'Habilitar nuevo si es que esta configurado
        If HabilitarNuevaBaseDatos Then
            lklNuevo.Enabled = True
        End If

    End Sub

    Sub NuevoBD()

        Dim frm As New frmNuevaBaseDatos
        frm.CadenaConexion = "Data Source=" & cbxServidor.cbx.Text & ";Initial Catalog=master;User ID=" & txtUsuario.txt.Text & ";Password=" & txtPassword.txt.Text & ";Connection Timeout=5"
        FGMostrarFormulario(Me.ParentForm, frm, "Base de Datos", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

        If frm.Creado = False Then
            Exit Sub
        End If

        ListarBaseDatos()

        cbxBaseDatos.cbx.Text = frm.NombreBD

    End Sub

    Private Sub btnProbar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProbar.Click
        Probar(cbxBaseDatos.cbx.Text)
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub ocxConexionBaseDeDatos_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me.ParentForm)
    End Sub

    Private Sub ocxConexionBaseDeDatos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.ParentForm.Close()
    End Sub

    Private Sub cbxBaseDatos_ContextMenuStripChanged(sender As Object, e As System.EventArgs) Handles cbxBaseDatos.ContextMenuStripChanged

    End Sub

    Private Sub cbxBaseDatos_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxBaseDatos.GotFocus
        'ListarBaseDatos()
    End Sub

    Private Sub cbxBaseDatos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxBaseDatos.Click
        ListarBaseDatos()
    End Sub

    Private Sub lklNuevo_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklNuevo.LinkClicked
        NuevoBD()
    End Sub

    Private Sub txtEmpresa_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtEmpresa.TeclaPrecionada
        'CSistema.SelectNextControl(TableLayoutPanel1)
        'txtEmpresa.SelectNextControl(sender, True, True, True, True)
        'No funciona asi, hacer manualmente
        If e.KeyCode = Keys.Enter Then
            cbxServidor.cbx.Focus()
        End If

    End Sub

    Private Sub cbxServidor_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxServidor.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            txtUsuario.txt.Focus()
        End If

    End Sub

    Private Sub txtUsuario_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            txtPassword.txt.Focus()
        End If
    End Sub

    Private Sub cbxBaseDatos_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxBaseDatos.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            btnProbar.Focus()
        End If
    End Sub

    Private Sub txtPassword_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPassword.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            ListarBaseDatos()
            cbxBaseDatos.cbx.Focus()

        End If

    End Sub

    Private Sub txtPassword_Load(sender As System.Object, e As System.EventArgs) Handles txtPassword.Load

    End Sub

    Private Sub txtPassword_LostFocus(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        ListarBaseDatos()
    End Sub

    Private Sub cbxBaseDatos_Load(sender As System.Object, e As System.EventArgs) Handles cbxBaseDatos.Load

    End Sub

    Private Sub cbxBaseDatos_Enter(sender As Object, e As System.EventArgs) Handles cbxBaseDatos.Enter
        If cbxBaseDatos.cbx.Items.Count <= 1 Then
            ListarBaseDatos()
        End If
    End Sub

End Class

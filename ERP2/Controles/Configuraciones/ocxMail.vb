﻿Public Class ocxMail

    'CLASES
    Dim CSistema As New CSistema
    Public Property IDSucursal As Integer = 0
    Sub Inicializar()

        txtPassword.txt.PasswordChar = "*"
        txtPassword.txt.UseSystemPasswordChar = True

    End Sub

    'FUNCIONES
    Public Sub ObtenerInformacion()

        Try
            If IDSucursal = 0 Then
                IDSucursal = vgIDSucursal
            End If

            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select Top(1) * From Configuraciones Where IDSucursal=" & IDSucursal)(0)

            txtServidor.SetValue(CSistema.RetornarValorString(oRow("MailServer").ToString))
            nudPuerto.Value = CSistema.RetornarValorInteger(oRow("MailPuerto").ToString)
            chkSSL.Valor = CSistema.RetornarValorBoolean(oRow("MailSSL").ToString)
            txtCuenta.SetValue(CSistema.RetornarValorString(oRow("MailCuenta").ToString))
            txtPassword.SetValue(CSistema.RetornarValorString(oRow("MailPassword").ToString))

        Catch ex As Exception

        End Try


    End Sub

    Public Sub Guardar()

        Dim CostoProducto As String = ""

        Dim sql As String

        sql = "Update Configuraciones Set "
        sql = sql & "MailServer = '" & txtServidor.GetValue & "', "
        sql = sql & "MailPuerto = '" & nudPuerto.Value & "', "
        sql = sql & "MailSSL= '" & chkSSL.Valor.ToString & "', "
        sql = sql & "MailCuenta = '" & txtCuenta.GetValue & "', "
        sql = sql & "MailPassword = '" & txtPassword.GetValue & "' "

        sql = sql & "Where IDSucursal = " & IDSucursal & " "

        If CSistema.ExecuteNonQuery(sql) > 0 Then
            FGConfiguraciones()
            ObtenerInformacion()
        Else
            MessageBox.Show("Probablemente no se produjeron los cambios! Intente nuevamente", "Movimiento", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        ObtenerInformacion()
    End Sub

    Private Sub ocxMail_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

End Class

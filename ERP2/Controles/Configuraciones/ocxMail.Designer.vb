﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxMail
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtCuenta = New ERP.ocxTXTString()
        Me.lblPuerto = New System.Windows.Forms.Label()
        Me.txtServidor = New ERP.ocxTXTString()
        Me.lblServidor = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkSSL = New ERP.ocxCHK()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.txtPassword = New ERP.ocxTXTString()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.nudPuerto = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.nudPuerto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtCuenta
        '
        Me.txtCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCuenta.Color = System.Drawing.Color.Empty
        Me.txtCuenta.Indicaciones = Nothing
        Me.txtCuenta.Location = New System.Drawing.Point(74, 97)
        Me.txtCuenta.Multilinea = False
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Size = New System.Drawing.Size(178, 20)
        Me.txtCuenta.SoloLectura = False
        Me.txtCuenta.TabIndex = 6
        Me.txtCuenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCuenta.Texto = ""
        '
        'lblPuerto
        '
        Me.lblPuerto.AutoSize = True
        Me.lblPuerto.Location = New System.Drawing.Point(12, 75)
        Me.lblPuerto.Name = "lblPuerto"
        Me.lblPuerto.Size = New System.Drawing.Size(41, 13)
        Me.lblPuerto.TabIndex = 2
        Me.lblPuerto.Text = "Puerto:"
        '
        'txtServidor
        '
        Me.txtServidor.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtServidor.Color = System.Drawing.Color.Empty
        Me.txtServidor.Indicaciones = Nothing
        Me.txtServidor.Location = New System.Drawing.Point(74, 45)
        Me.txtServidor.Multilinea = False
        Me.txtServidor.Name = "txtServidor"
        Me.txtServidor.Size = New System.Drawing.Size(178, 20)
        Me.txtServidor.SoloLectura = False
        Me.txtServidor.TabIndex = 1
        Me.txtServidor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtServidor.Texto = ""
        '
        'lblServidor
        '
        Me.lblServidor.AutoSize = True
        Me.lblServidor.Location = New System.Drawing.Point(12, 49)
        Me.lblServidor.Name = "lblServidor"
        Me.lblServidor.Size = New System.Drawing.Size(49, 13)
        Me.lblServidor.TabIndex = 0
        Me.lblServidor.Text = "Servidor:"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(423, 359)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 327)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(417, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(339, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(258, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.chkSSL)
        Me.Panel1.Controls.Add(Me.lblPassword)
        Me.Panel1.Controls.Add(Me.txtPassword)
        Me.Panel1.Controls.Add(Me.lblCuenta)
        Me.Panel1.Controls.Add(Me.txtCuenta)
        Me.Panel1.Controls.Add(Me.lblPuerto)
        Me.Panel1.Controls.Add(Me.txtServidor)
        Me.Panel1.Controls.Add(Me.lblServidor)
        Me.Panel1.Controls.Add(Me.nudPuerto)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(417, 318)
        Me.Panel1.TabIndex = 0
        '
        'chkSSL
        '
        Me.chkSSL.BackColor = System.Drawing.Color.Transparent
        Me.chkSSL.Color = System.Drawing.Color.Empty
        Me.chkSSL.Location = New System.Drawing.Point(133, 72)
        Me.chkSSL.Name = "chkSSL"
        Me.chkSSL.Size = New System.Drawing.Size(119, 18)
        Me.chkSSL.SoloLectura = False
        Me.chkSSL.TabIndex = 4
        Me.chkSSL.Texto = "SSL"
        Me.chkSSL.Valor = False
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Location = New System.Drawing.Point(12, 127)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(56, 13)
        Me.lblPassword.TabIndex = 7
        Me.lblPassword.Text = "Password:"
        '
        'txtPassword
        '
        Me.txtPassword.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtPassword.Color = System.Drawing.Color.Empty
        Me.txtPassword.Indicaciones = Nothing
        Me.txtPassword.Location = New System.Drawing.Point(74, 123)
        Me.txtPassword.Multilinea = False
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(178, 20)
        Me.txtPassword.SoloLectura = False
        Me.txtPassword.TabIndex = 8
        Me.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtPassword.Texto = ""
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(12, 103)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 5
        Me.lblCuenta.Text = "Cuenta:"
        '
        'nudPuerto
        '
        Me.nudPuerto.Location = New System.Drawing.Point(74, 71)
        Me.nudPuerto.Maximum = New Decimal(New Integer() {65500, 0, 0, 0})
        Me.nudPuerto.Name = "nudPuerto"
        Me.nudPuerto.Size = New System.Drawing.Size(53, 20)
        Me.nudPuerto.TabIndex = 3
        Me.nudPuerto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(385, 32)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "* Esta cuenta se utilizara predeterminadamente  para el envio de correos como: So" & _
    "licitud de Permisos, advertencias, avisos de tareas, etc."
        '
        'ocxMail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxMail"
        Me.Size = New System.Drawing.Size(423, 359)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.nudPuerto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCuenta As ERP.ocxTXTString
    Friend WithEvents lblPuerto As System.Windows.Forms.Label
    Friend WithEvents txtServidor As ERP.ocxTXTString
    Friend WithEvents lblServidor As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents nudPuerto As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents txtPassword As ERP.ocxTXTString
    Friend WithEvents chkSSL As ERP.ocxCHK
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class

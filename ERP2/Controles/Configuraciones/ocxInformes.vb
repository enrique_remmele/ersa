﻿Imports System.IO

Public Class ocxInformes

    Dim CArchivoInicio As New CArchivoInicio
    Dim Cantidad As Integer
    Dim t As System.Threading.Thread

    Sub Inicializar()

        txtPath.CharacterCasing = CharacterCasing.Normal
        txtPath.SetValue(VGCarpetaReporteTemporal)



    End Sub

    Sub ActualizarPath()

        'Bucamos el archivo
        Dim f As New FolderBrowserDialog
        f.Description = "Directorio de Reportes SAIN"
        f.SelectedPath = VGCarpetaReporteTemporal
        f.ShowNewFolderButton = False

        If f.ShowDialog() = DialogResult.Cancel Then
            Exit Sub
        End If

        VGCarpetaReporteTemporal = f.SelectedPath & "\"
        CArchivoInicio.IniWrite(VGArchivoINI, "INFORMES", "PATH", VGCarpetaReporteTemporal)
        txtPath.SetValue(VGCarpetaReporteTemporal)


    End Sub

    Sub ProcesarCopiado()

        t = New System.Threading.Thread(AddressOf ActualizarInformes)
        t.Start()

    End Sub

    Sub ActualizarInformes()

        'Verificar si existe el directorio
        If Directory.Exists(VGCarpetaReporteTemporal) = False Then
            MessageBox.Show("El directorio no es correcto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Cantidad = 0
        CalcularCantidad()

        If Cantidad = 0 Then
            MessageBox.Show("No se encontro ningun archivo de informe", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim Index As Integer = 0


        If Directory.GetDirectories(VGCarpetaReporteTemporal).Count > 0 Then
            ActualizarInformes(VGCarpetaReporteTemporal, Index)
        End If

        For Each F As String In Directory.GetFiles(VGCarpetaReporteTemporal)
            If System.IO.Path.GetExtension(F) = ".rpt" Then
                FileIO.FileSystem.CopyFile(F, VGCarpetaReporte & System.IO.Path.GetFileName(F), True)
                Index = Index + 1
            End If

            ProgressBar1.Value = CInt((Index / Cantidad) * 100)
            lblProgreso.Text = CInt((Index / Cantidad) * 100) & " %"

            System.Threading.Thread.Sleep(100)

        Next

    End Sub

    Sub ActualizarInformes(ByVal DirRoot As String, ByRef Index As Integer)

        If Cantidad = 0 Then
            Exit Sub
        End If

        For Each Dir As String In Directory.GetDirectories(DirRoot)

            If Directory.GetDirectories(Dir).Count > 0 Then
                ActualizarInformes(Dir, Index)
            End If

            For Each F As String In Directory.GetFiles(Dir)
                If System.IO.Path.GetExtension(F) = ".rpt" Then
                    FileIO.FileSystem.CopyFile(F, VGCarpetaReporte & System.IO.Path.GetFileName(F), True)
                    Index = Index + 1
                End If
            Next

            ProgressBar1.Value = CInt((Index / Cantidad) * 100)
            lblProgreso.Text = CInt((Index / Cantidad) * 100) & " %"

            System.Threading.Thread.Sleep(100)

        Next

    End Sub

    Sub CalcularCantidad()

        If Directory.GetDirectories(VGCarpetaReporteTemporal).Count > 0 Then
            CalcularCantidad(VGCarpetaReporteTemporal)
        End If

        For Each F As String In Directory.GetFiles(VGCarpetaReporteTemporal)
            If System.IO.Path.GetExtension(F) = ".rpt" Then
                Cantidad = Cantidad + 1
            End If
        Next


    End Sub

    Sub CalcularCantidad(ByVal DirRoot As String)

        For Each Dir As String In Directory.GetDirectories(DirRoot)

            If Directory.GetDirectories(Dir).Count > 0 Then
                CalcularCantidad(Dir)
            End If

            For Each F As String In Directory.GetFiles(Dir)
                If System.IO.Path.GetExtension(F) = ".rpt" Then
                    Cantidad = Cantidad + 1
                End If
            Next

        Next

    End Sub

    Private Sub lklPathInformes_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lklPathInformes.LinkClicked
        ActualizarPath()
    End Sub

    Private Sub ocxInformes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ProcesarCopiado()
    End Sub

End Class

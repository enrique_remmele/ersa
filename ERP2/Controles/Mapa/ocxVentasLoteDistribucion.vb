﻿Imports System.IO

Public Class ocxVentasLoteDistribucion

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private PoligonoValue As String
    Public Property Poligono() As String
        Get
            Return PoligonoValue
        End Get
        Set(ByVal value As String)
            PoligonoValue = value
        End Set
    End Property

    Private ColorValue As String
    Public Property Color() As String
        Get
            Return ColorValue
        End Get
        Set(ByVal value As String)
            ColorValue = value
        End Set
    End Property

    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Crear Archivo
        CrearArchivo()
        Iniciado = True

    End Sub

    'Crear Archivo
    Sub CrearArchivo()

        Dim Archivo As String = VGCarpetaAplicacion & "\htmVentasLotesDistribucion.htm"

        'Si es que existe, eliminar
        Try

            If File.Exists(Archivo) Then
                File.Delete(Archivo)
            End If

        Catch ex As Exception

        End Try


        'Crear Archivo
        Try
            Dim oSW As New StreamWriter(Archivo)
            oSW.Write(My.Resources.htmVentasLotesDistribucion)
            oSW.Flush()

        Catch ex As Exception

        End Try

        Try
            Threading.Thread.Sleep(1000)
            WebBrowser1.Navigate(Archivo)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ConvertirAVector(ByRef variable As String, ByVal valor As String, ByVal delimitador As String)

        If variable.Length = 0 Then
            variable = valor
        Else
            variable = variable & delimitador & valor
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        WebBrowser1.Document.GetElementById("btnCargar").InvokeMember("click")
        Timer1.Enabled = False
    End Sub

    Private Sub WebBrowser1_DocumentCompleted(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted

        Dim Cliente As String = ""
        Dim Latitud As String = ""
        Dim Longitud As String = ""
        Dim Sucursal As String = ""
        Dim Barrio As String = ""
        Dim Direccion As String = ""
        Dim Ciudad As String = ""
        Dim Vendedor As String = ""
        Dim Telefono As String = ""
        Dim Contacto As String = ""
        Dim Comprobantes As String = ""

        Dim dtAgrupados As DataTable = dt.Clone
        dtAgrupados.Columns.Add("Comprobantes")

        Try
            For Each oRow As DataRow In dt.Rows
                If oRow("Seleccionar").ToString = "True" Then

                    'Agregar solo si no existe en el datatable
                    Dim filtro As String = " IDCliente = " & oRow("IDCliente").ToString & " And IDSucursalCliente = " & oRow("IDSucursalCliente").ToString & " "
                    If dtAgrupados.Select(filtro).Count = 0 Then
                        dtAgrupados.Rows.Add(oRow.ItemArray)
                        dtAgrupados.Rows(dtAgrupados.Rows.Count - 1)("Comprobantes") = GetComprobantes(oRow("IDCliente"), oRow("IDSucursalCliente"))
                    End If

                End If
            Next
        Catch ex As Exception

        End Try
        


        For Each oRow As DataRow In dtAgrupados.Rows
            ConvertirAVector(Cliente, oRow("Cliente").ToString, "|")
            ConvertirAVector(Latitud, oRow("Latitud").ToString, "|")
            ConvertirAVector(Longitud, oRow("Longitud").ToString, "|")
            ConvertirAVector(Sucursal, oRow("Suc. Cliente").ToString, "|")
            ConvertirAVector(Direccion, oRow("Direccion").ToString, "|")
            ConvertirAVector(Barrio, oRow("Barrio").ToString, "|")
            ConvertirAVector(Ciudad, oRow("Ciudad").ToString, "|")
            ConvertirAVector(Vendedor, oRow("Vendedor").ToString, "|")
            ConvertirAVector(Telefono, oRow("Telefono").ToString, "|")
            ConvertirAVector(Contacto, oRow("Contacto").ToString, "|")
            ConvertirAVector(Comprobantes, oRow("Comprobantes").ToString, "|")
        Next

        WebBrowser1.Document.GetElementById("txtCliente").InnerText = Cliente
        WebBrowser1.Document.GetElementById("txtLatitud").InnerText = Latitud.Replace(",", ".")
        WebBrowser1.Document.GetElementById("txtLongitud").InnerText = Longitud.Replace(",", ".")
        WebBrowser1.Document.GetElementById("txtSucursal").InnerText = Sucursal
        WebBrowser1.Document.GetElementById("txtDireccion").InnerText = Direccion
        WebBrowser1.Document.GetElementById("txtBarrio").InnerText = Barrio
        WebBrowser1.Document.GetElementById("txtCiudad").InnerText = Ciudad
        WebBrowser1.Document.GetElementById("txtVendedor").InnerText = Vendedor
        WebBrowser1.Document.GetElementById("txtTelefono").InnerText = Telefono
        WebBrowser1.Document.GetElementById("txtContacto").InnerText = Contacto
        WebBrowser1.Document.GetElementById("txtComprobantes").InnerText = Comprobantes
        WebBrowser1.Document.GetElementById("txtPoligono").InnerText = Poligono
        WebBrowser1.Document.GetElementById("txtColor").InnerText = Color

        Timer1.Interval = 1000
        Timer1.Enabled = True
    End Sub

    Private Function GetComprobantes(ByVal IDCliente As Integer, ByVal IDSucursal As Integer) As String

        GetComprobantes = ""
        Dim filtro As String = " IDCliente = " & IDCliente & " And IDSucursalCliente = " & IDSucursal & " "

        For Each oRow As DataRow In dt.Select(filtro)
            If GetComprobantes.Length = 0 Then
                GetComprobantes = oRow("Comprobante").ToString
            Else
                GetComprobantes = GetComprobantes & ", " & oRow("Comprobante").ToString
            End If

        Next

    End Function

End Class

﻿Imports System.IO

Public Class ocxZona

    'CLASES
    Dim CSistema As New CSistema

    'POPIEDADES
    Private ZonaVentaValue As String
    Public Property ZonaVenta() As String
        Get
            Return ZonaVentaValue
        End Get
        Set(ByVal value As String)
            ZonaVentaValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private PoligonoValue As String
    Public Property Poligono() As String
        Get
            Return PoligonoValue
        End Get
        Set(ByVal value As String)
            PoligonoValue = value
        End Set
    End Property

    Private ColorValue As String
    Public Property Color() As String
        Get
            Return ColorValue
        End Get
        Set(ByVal value As String)
            ColorValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Crear Archivo
        CrearArchivo()

        'Variables
        Panel2.BackColor = System.Drawing.ColorTranslator.FromHtml(Color)
        lblZona.Text = ZonaVenta
        Timer1.Enabled = False

    End Sub

    'Crear Archivo
    Sub CrearArchivo()

        Dim Archivo As String = VGCarpetaAplicacion & "\htmZona.htm"

        'Si es que existe, eliminar
        Try

            If File.Exists(Archivo) Then
                File.Delete(Archivo)
            End If

        Catch ex As Exception

        End Try
        

        'Crear Archivo
        Try
            Dim oSW As New StreamWriter(Archivo)
            oSW.Write(My.Resources.htmZona)
            oSW.Flush()

        Catch ex As Exception

        End Try

        Try
            Threading.Thread.Sleep(1000)
            WebBrowser1.Navigate(Archivo)
        Catch ex As Exception

        End Try

    End Sub

    Sub EstablecerColor()

        Panel2.BackColor = Me.ColorDialog1.Color
        Color = System.Drawing.ColorTranslator.ToHtml(Panel2.BackColor)

    End Sub

    Sub Guardar()

        Poligono = WebBrowser1.Document.GetElementById("txtPoligono").GetAttribute("value").ToString
        Color = WebBrowser1.Document.GetElementById("txtColor").GetAttribute("value").ToString
        Dim sql As String = "Update ZonaVenta Set Poligono='" & Poligono & "', ColorPoligono='" & Color & "' Where ID=" & ID
        If CSistema.ExecuteNonQuery(sql) = 0 Then
            MessageBox.Show("El sistema no actualizo el registro! Intente nuevamente.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        WebBrowser1.Document.GetElementById("btnCargar").InvokeMember("click")
        Timer1.Enabled = False
    End Sub

    Private Sub WebBrowser1_DocumentCompleted(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted
        WebBrowser1.Document.GetElementById("txtPoligono").InnerText = Poligono
        WebBrowser1.Document.GetElementById("txtColor").InnerText = Color
        Timer1.Interval = 1000
        Timer1.Enabled = True
    End Sub

    Private Sub Panel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Panel2.Click
        ColorDialog1.ShowDialog()

        Panel2.BackColor = Me.ColorDialog1.Color
        Color = System.Drawing.ColorTranslator.ToHtml(Panel2.BackColor)

        WebBrowser1.Document.GetElementById("btnActualizarPoligono").InvokeMember("click")
        Poligono = WebBrowser1.Document.GetElementById("txtPoligono").GetAttribute("value").ToString
        Inicializar()
    End Sub

    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Inicializar()
    End Sub

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxClientesMapa
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnEstablecer = New System.Windows.Forms.Button()
        Me.rdbSucursal = New System.Windows.Forms.RadioButton()
        Me.rdbMatriz = New System.Windows.Forms.RadioButton()
        Me.cbxSucursales = New System.Windows.Forms.ComboBox()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.GMAP = New GMap.NET.WindowsForms.GMapControl()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.lblLat = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtLatitud = New ERP.ocxTXTString()
        Me.txtLongitud = New ERP.ocxTXTString()
        Me.Timer1 = New System.Windows.Forms.Timer()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.GMAP, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(527, 343)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnEstablecer)
        Me.Panel1.Controls.Add(Me.rdbSucursal)
        Me.Panel1.Controls.Add(Me.rdbMatriz)
        Me.Panel1.Controls.Add(Me.cbxSucursales)
        Me.Panel1.Controls.Add(Me.btnGuardar)
        Me.Panel1.Controls.Add(Me.btnCancelar)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(521, 28)
        Me.Panel1.TabIndex = 0
        '
        'btnEstablecer
        '
        Me.btnEstablecer.Location = New System.Drawing.Point(305, 2)
        Me.btnEstablecer.Name = "btnEstablecer"
        Me.btnEstablecer.Size = New System.Drawing.Size(67, 23)
        Me.btnEstablecer.TabIndex = 5
        Me.btnEstablecer.Text = "Establecer"
        Me.btnEstablecer.UseVisualStyleBackColor = True
        '
        'rdbSucursal
        '
        Me.rdbSucursal.AutoSize = True
        Me.rdbSucursal.Enabled = False
        Me.rdbSucursal.Location = New System.Drawing.Point(62, 6)
        Me.rdbSucursal.Name = "rdbSucursal"
        Me.rdbSucursal.Size = New System.Drawing.Size(69, 17)
        Me.rdbSucursal.TabIndex = 1
        Me.rdbSucursal.TabStop = True
        Me.rdbSucursal.Text = "Sucursal:"
        Me.rdbSucursal.UseVisualStyleBackColor = True
        '
        'rdbMatriz
        '
        Me.rdbMatriz.AutoSize = True
        Me.rdbMatriz.Location = New System.Drawing.Point(3, 6)
        Me.rdbMatriz.Name = "rdbMatriz"
        Me.rdbMatriz.Size = New System.Drawing.Size(53, 17)
        Me.rdbMatriz.TabIndex = 0
        Me.rdbMatriz.TabStop = True
        Me.rdbMatriz.Text = "Matriz"
        Me.rdbMatriz.UseVisualStyleBackColor = True
        '
        'cbxSucursales
        '
        Me.cbxSucursales.Enabled = False
        Me.cbxSucursales.FormattingEnabled = True
        Me.cbxSucursales.Location = New System.Drawing.Point(137, 3)
        Me.cbxSucursales.Name = "cbxSucursales"
        Me.cbxSucursales.Size = New System.Drawing.Size(167, 21)
        Me.cbxSucursales.TabIndex = 2
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(378, 2)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(67, 23)
        Me.btnGuardar.TabIndex = 3
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(451, 2)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(67, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'GMAP
        '
        Me.GMAP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.GMAP.Bearing = 0.0!
        Me.GMAP.CanDragMap = True
        Me.GMAP.CausesValidation = False
        Me.GMAP.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GMAP.EmptyTileColor = System.Drawing.Color.Navy
        Me.GMAP.GrayScaleMode = False
        ' Me.GMAP.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow
        Me.GMAP.ImeMode = System.Windows.Forms.ImeMode.Close
        Me.GMAP.LevelsKeepInMemmory = 5
        Me.GMAP.Location = New System.Drawing.Point(3, 37)
        Me.GMAP.MarkersEnabled = True
        Me.GMAP.MaxZoom = 17
        Me.GMAP.MinZoom = 4
        '  Me.GMAP.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter
        Me.GMAP.Name = "GMAP"
        Me.GMAP.NegativeMode = False
        Me.GMAP.PolygonsEnabled = False
        Me.GMAP.RetryLoadTile = 0
        Me.GMAP.RoutesEnabled = True
        '  Me.GMAP.ScaleMode = GMap.NET.WindowsForms.ScaleModes.[Integer]
        Me.GMAP.SelectedAreaFillColor = System.Drawing.Color.FromArgb(CType(CType(33, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.GMAP.ShowTileGridLines = False
        Me.GMAP.Size = New System.Drawing.Size(521, 260)
        Me.GMAP.TabIndex = 1
        Me.GMAP.Zoom = 16.0R
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnBuscar)
        Me.Panel2.Controls.Add(Me.lblLat)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.txtLatitud)
        Me.Panel2.Controls.Add(Me.txtLongitud)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 303)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(521, 37)
        Me.Panel2.TabIndex = 2
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(451, 7)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(67, 23)
        Me.btnBuscar.TabIndex = 5
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'lblLat
        '
        Me.lblLat.AutoSize = True
        Me.lblLat.Location = New System.Drawing.Point(101, 12)
        Me.lblLat.Name = "lblLat"
        Me.lblLat.Size = New System.Drawing.Size(42, 13)
        Me.lblLat.TabIndex = 4
        Me.lblLat.Text = "Latitud:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(274, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Longitud:"
        '
        'txtLatitud
        '
        Me.txtLatitud.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLatitud.Color = System.Drawing.Color.Empty
        Me.txtLatitud.Indicaciones = Nothing
        Me.txtLatitud.Location = New System.Drawing.Point(156, 8)
        Me.txtLatitud.Multilinea = False
        Me.txtLatitud.Name = "txtLatitud"
        Me.txtLatitud.Size = New System.Drawing.Size(107, 20)
        Me.txtLatitud.SoloLectura = False
        Me.txtLatitud.TabIndex = 2
        Me.txtLatitud.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLatitud.Texto = ""
        '
        'txtLongitud
        '
        Me.txtLongitud.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLongitud.Color = System.Drawing.Color.Empty
        Me.txtLongitud.Indicaciones = Nothing
        Me.txtLongitud.Location = New System.Drawing.Point(338, 8)
        Me.txtLongitud.Multilinea = False
        Me.txtLongitud.Name = "txtLongitud"
        Me.txtLongitud.Size = New System.Drawing.Size(107, 20)
        Me.txtLongitud.SoloLectura = False
        Me.txtLongitud.TabIndex = 1
        Me.txtLongitud.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtLongitud.Texto = ""
        '
        'Timer1
        '
        '
        'ocxClientesMapa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxClientesMapa"
        Me.Size = New System.Drawing.Size(527, 343)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents cbxSucursales As System.Windows.Forms.ComboBox
    Friend WithEvents rdbSucursal As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMatriz As System.Windows.Forms.RadioButton
    Friend WithEvents GMAP As GMap.NET.WindowsForms.GMapControl
    Friend WithEvents btnEstablecer As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblLat As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtLatitud As ERP.ocxTXTString
    Friend WithEvents txtLongitud As ERP.ocxTXTString
    Friend WithEvents btnBuscar As System.Windows.Forms.Button

End Class

﻿Imports GMap.NET.MapProviders
Imports GMap.NET
Imports System.IO

Public Class ocxProveedoresMapa

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private LatitudValue As Decimal
    Public Property Latitud() As Decimal
        Get
            Return LatitudValue
        End Get
        Set(ByVal value As Decimal)
            LatitudValue = value
        End Set
    End Property

    Private LongitudValue As Decimal
    Public Property Longitud() As Decimal
        Get
            Return LongitudValue
        End Get
        Set(ByVal value As Decimal)
            LongitudValue = value
        End Set
    End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        Latitud = 0
        Longitud = 0

    End Sub

    Sub Guardar()

        Try
            Dim lat As String = ""
            Dim lng As String = ""

            '   lat = WebBrowser1.Document.GetElementById("txtLatitudMarker").GetAttribute("value").ToString
            '  lng = WebBrowser1.Document.GetElementById("txtLongitudMarker").GetAttribute("value").ToString

            Dim sql As String = ""

            sql = "Update Proveedor Set Latitud=" & CStr(Latitud).Replace(",", ".") & ", Longitud=" & CStr(Longitud).Replace(",", ".") & " Where ID=" & ID
           

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                MessageBox.Show("El sistema no actualizo el registro! Intente nuevamente.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Catch ex As Exception
            CerrarMapa()
        End Try


    End Sub
    Sub CerrarMapa()

        TableLayoutPanel1.Enabled = False
        '  WebBrowser1.Dispose()

        Dim lbl As New Label
        lbl.Text = "SIN CONEXION"
        lbl.Name = "lbl"
        lbl.AutoSize = False
        lbl.Dock = DockStyle.Fill
        lbl.TextAlign = ContentAlignment.MiddleCenter
        lbl.ForeColor = Color.Red
        lbl.Font = New Font("Arial", 24, FontStyle.Bold)

        For Each ctr As Control In TableLayoutPanel1.Controls

            If ctr.Name = "lbl" Then
                ctr.Dispose()
            End If

        Next

        TableLayoutPanel1.Controls.Add(lbl)


    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        Try
            Timer1.Enabled = False
        Catch ex As Exception
            CerrarMapa()
        End Try


    End Sub

    Sub CargarPunto()

        Dim overlayOne As New GMap.NET.WindowsForms.GMapOverlay
        Dim location As GMap.NET.PointLatLng

        GMAP.InitializeLifetimeService()

        If Latitud = "0" Or Longitud = "0" Then
            Latitud = -25.262551
            Longitud = -57.593911
        End If
        Latitud = Latitud.ToString.Replace(".", ",")
        Longitud = Longitud.ToString.Replace(".", ",")
        location.Lat = CDec(Latitud)
        location.Lng = CDec(Longitud)
        GMAP.Position = location

        overlayOne.Markers.Add(New GMap.NET.WindowsForms.Markers.GMarkerGoogle(location, Global.GMap.NET.WindowsForms.Markers.GMarkerGoogleType.green))

        GMAP.Overlays.Add(overlayOne)

        GMAP.MapProvider = GMapProviders.GoogleMap

        GMAP.Zoom = 16.0
        GMAP.Zoom = 12.0
        GMAP.Zoom = 16.0


    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Guardar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Inicializar()
    End Sub

    Private Sub GMAP_Load(sender As System.Object, e As System.EventArgs) Handles GMAP.Load

        CargarPunto()

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnEstablecer.Click

        GMAP.Overlays.Clear()

        Latitud = GMAP.Position.Lat
        Longitud = GMAP.Position.Lng

        CargarPunto()

    End Sub

    Private Sub btnBuscar_Click(sender As System.Object, e As System.EventArgs) Handles btnBuscar.Click
        Dim location As GMap.NET.PointLatLng

        location.Lat = txtLatitud.GetValue
        location.Lng = txtLongitud.GetValue

        GMAP.Position = location
    End Sub

    Private Sub GMAP_OnPositionChanged(point As GMap.NET.PointLatLng) Handles GMAP.OnPositionChanged
        txtLatitud.Texto = GMAP.Position.Lat
        txtLongitud.Texto = GMAP.Position.Lng
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxCuentaFijaOrdenPago
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.chkBuscarProveedor = New System.Windows.Forms.CheckBox()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.chkBuscarCuentaBancaria = New System.Windows.Forms.CheckBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblDebeHaber = New System.Windows.Forms.Label()
        Me.lblTipoComprobante = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.rdbProveedor = New System.Windows.Forms.RadioButton()
        Me.rdbCheque = New System.Windows.Forms.RadioButton()
        Me.rdbEfectivo = New System.Windows.Forms.RadioButton()
        Me.txtCuenta = New ERP.ocxTXTCuentaContable()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.cbxDebeHaber = New ERP.ocxCBX()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtOrden = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(13, 41)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 2
        Me.lblCuenta.Text = "Cuenta:"
        '
        'chkBuscarProveedor
        '
        Me.chkBuscarProveedor.AutoSize = True
        Me.chkBuscarProveedor.Location = New System.Drawing.Point(80, 144)
        Me.chkBuscarProveedor.Name = "chkBuscarProveedor"
        Me.chkBuscarProveedor.Size = New System.Drawing.Size(137, 17)
        Me.chkBuscarProveedor.TabIndex = 16
        Me.chkBuscarProveedor.Text = "Buscar en el Proveedor"
        Me.chkBuscarProveedor.UseVisualStyleBackColor = True
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(386, 67)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 8
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(390, 95)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 13)
        Me.lblOrden.TabIndex = 12
        Me.lblOrden.Text = "Orden:"
        '
        'lvLista
        '
        Me.lvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLista.Location = New System.Drawing.Point(3, 172)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(519, 96)
        Me.lvLista.TabIndex = 18
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'chkBuscarCuentaBancaria
        '
        Me.chkBuscarCuentaBancaria.AutoSize = True
        Me.chkBuscarCuentaBancaria.Location = New System.Drawing.Point(223, 144)
        Me.chkBuscarCuentaBancaria.Name = "chkBuscarCuentaBancaria"
        Me.chkBuscarCuentaBancaria.Size = New System.Drawing.Size(241, 17)
        Me.chkBuscarCuentaBancaria.TabIndex = 17
        Me.chkBuscarCuentaBancaria.Text = "Utilizar la configuracion de la cuenta bancaria"
        Me.chkBuscarCuentaBancaria.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(247, 0)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 22
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(361, 0)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 23
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(166, 0)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 21
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(85, 0)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 20
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(4, 0)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 19
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 305)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(525, 22)
        Me.StatusStrip1.TabIndex = 26
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(44, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'lblDebeHaber
        '
        Me.lblDebeHaber.AutoSize = True
        Me.lblDebeHaber.Location = New System.Drawing.Point(13, 68)
        Me.lblDebeHaber.Name = "lblDebeHaber"
        Me.lblDebeHaber.Size = New System.Drawing.Size(55, 13)
        Me.lblDebeHaber.TabIndex = 4
        Me.lblDebeHaber.Text = "Deb/Hab."
        '
        'lblTipoComprobante
        '
        Me.lblTipoComprobante.AutoSize = True
        Me.lblTipoComprobante.Location = New System.Drawing.Point(173, 67)
        Me.lblTipoComprobante.Name = "lblTipoComprobante"
        Me.lblTipoComprobante.Size = New System.Drawing.Size(61, 13)
        Me.lblTipoComprobante.TabIndex = 6
        Me.lblTipoComprobante.Text = "Tipo Comp:"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(13, 95)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 10
        Me.lblTipo.Text = "Tipo:"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(13, 122)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 14
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(13, 15)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'rdbProveedor
        '
        Me.rdbProveedor.AutoSize = True
        Me.rdbProveedor.Location = New System.Drawing.Point(81, 93)
        Me.rdbProveedor.Name = "rdbProveedor"
        Me.rdbProveedor.Size = New System.Drawing.Size(74, 17)
        Me.rdbProveedor.TabIndex = 25
        Me.rdbProveedor.TabStop = True
        Me.rdbProveedor.Text = "Proveedor"
        Me.rdbProveedor.UseVisualStyleBackColor = True
        '
        'rdbCheque
        '
        Me.rdbCheque.AutoSize = True
        Me.rdbCheque.Location = New System.Drawing.Point(160, 93)
        Me.rdbCheque.Name = "rdbCheque"
        Me.rdbCheque.Size = New System.Drawing.Size(62, 17)
        Me.rdbCheque.TabIndex = 26
        Me.rdbCheque.TabStop = True
        Me.rdbCheque.Text = "Cheque"
        Me.rdbCheque.UseVisualStyleBackColor = True
        '
        'rdbEfectivo
        '
        Me.rdbEfectivo.AutoSize = True
        Me.rdbEfectivo.Location = New System.Drawing.Point(223, 93)
        Me.rdbEfectivo.Name = "rdbEfectivo"
        Me.rdbEfectivo.Size = New System.Drawing.Size(64, 17)
        Me.rdbEfectivo.TabIndex = 27
        Me.rdbEfectivo.TabStop = True
        Me.rdbEfectivo.Text = "Efectivo"
        Me.rdbEfectivo.UseVisualStyleBackColor = True
        '
        'txtCuenta
        '
        Me.txtCuenta.AlturaMaxima = 211
        Me.txtCuenta.ListarTodas = False
        Me.txtCuenta.Location = New System.Drawing.Point(80, 37)
        Me.txtCuenta.SoloLectura = False
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Registro = Nothing
        Me.txtCuenta.Resolucion173 = False
        Me.txtCuenta.Seleccionado = False
        Me.txtCuenta.Size = New System.Drawing.Size(432, 20)
        Me.txtCuenta.TabIndex = 3
        Me.txtCuenta.Texto = Nothing
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(80, 9)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(63, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(80, 117)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(289, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 15
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'cbxDebeHaber
        '
        Me.cbxDebeHaber.CampoWhere = Nothing
        Me.cbxDebeHaber.DataDisplayMember = Nothing
        Me.cbxDebeHaber.DataFilter = Nothing
        Me.cbxDebeHaber.DataOrderBy = Nothing
        Me.cbxDebeHaber.DataSource = Nothing
        Me.cbxDebeHaber.DataValueMember = Nothing
        Me.cbxDebeHaber.FormABM = Nothing
        Me.cbxDebeHaber.Indicaciones = Nothing
        Me.cbxDebeHaber.Location = New System.Drawing.Point(80, 63)
        Me.cbxDebeHaber.Name = "cbxDebeHaber"
        Me.cbxDebeHaber.SeleccionObligatoria = False
        Me.cbxDebeHaber.Size = New System.Drawing.Size(87, 21)
        Me.cbxDebeHaber.SoloLectura = False
        Me.cbxDebeHaber.TabIndex = 5
        Me.cbxDebeHaber.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(240, 63)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(140, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 7
        Me.cbxTipoComprobante.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(435, 63)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(77, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 9
        Me.cbxMoneda.Texto = ""
        '
        'txtOrden
        '
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Decimales = True
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(435, 90)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(76, 22)
        Me.txtOrden.SoloLectura = False
        Me.txtOrden.TabIndex = 13
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOrden.Texto = "0"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lvLista, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 169.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(525, 305)
        Me.TableLayoutPanel1.TabIndex = 28
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.txtCuenta)
        Me.Panel1.Controls.Add(Me.lblCuenta)
        Me.Panel1.Controls.Add(Me.rdbEfectivo)
        Me.Panel1.Controls.Add(Me.chkBuscarProveedor)
        Me.Panel1.Controls.Add(Me.rdbCheque)
        Me.Panel1.Controls.Add(Me.lblOrden)
        Me.Panel1.Controls.Add(Me.rdbProveedor)
        Me.Panel1.Controls.Add(Me.txtOrden)
        Me.Panel1.Controls.Add(Me.lblMoneda)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.cbxMoneda)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.chkBuscarCuentaBancaria)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Controls.Add(Me.lblTipoComprobante)
        Me.Panel1.Controls.Add(Me.lblTipo)
        Me.Panel1.Controls.Add(Me.cbxTipoComprobante)
        Me.Panel1.Controls.Add(Me.cbxDebeHaber)
        Me.Panel1.Controls.Add(Me.lblDebeHaber)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(519, 163)
        Me.Panel1.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Controls.Add(Me.btnEditar)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 274)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(519, 28)
        Me.Panel2.TabIndex = 1
        '
        'ocxCuentaFijaOrdenPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "ocxCuentaFijaOrdenPago"
        Me.Size = New System.Drawing.Size(525, 327)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents chkBuscarProveedor As System.Windows.Forms.CheckBox
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtOrden As ERP.ocxTXTNumeric
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents chkBuscarCuentaBancaria As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cbxDebeHaber As ERP.ocxCBX
    Friend WithEvents lblDebeHaber As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents lblTipoComprobante As System.Windows.Forms.Label
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents rdbEfectivo As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCheque As System.Windows.Forms.RadioButton
    Friend WithEvents rdbProveedor As System.Windows.Forms.RadioButton
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel

End Class

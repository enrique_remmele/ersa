﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ocxCuentaFijaOrdenPago2
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkSoloDiferido = New ERP.ocxCHK()
        Me.rdbChequeDiferido = New System.Windows.Forms.RadioButton()
        Me.rdbDocumento = New System.Windows.Forms.RadioButton()
        Me.lblTipoComprobante = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.rdbDiferenciaCambio = New System.Windows.Forms.RadioButton()
        Me.rdbRetencion = New System.Windows.Forms.RadioButton()
        Me.rdbEfectivo = New System.Windows.Forms.RadioButton()
        Me.chkBuscarCuentaBancaria = New ERP.ocxCHK()
        Me.rdbCheque = New System.Windows.Forms.RadioButton()
        Me.chkBuscarEnProveedores = New ERP.ocxCHK()
        Me.rdbProveedor = New System.Windows.Forms.RadioButton()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.txtCuenta = New ERP.ocxTXTCuentaContable()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblDebeHaber = New System.Windows.Forms.Label()
        Me.cbxDebeHaber = New ERP.ocxCBX()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.txtOrden = New ERP.ocxTXTNumeric()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.lvLista = New System.Windows.Forms.ListView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lvLista, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 205.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(791, 467)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkSoloDiferido)
        Me.Panel1.Controls.Add(Me.rdbChequeDiferido)
        Me.Panel1.Controls.Add(Me.rdbDocumento)
        Me.Panel1.Controls.Add(Me.lblTipoComprobante)
        Me.Panel1.Controls.Add(Me.cbxTipoComprobante)
        Me.Panel1.Controls.Add(Me.rdbDiferenciaCambio)
        Me.Panel1.Controls.Add(Me.rdbRetencion)
        Me.Panel1.Controls.Add(Me.rdbEfectivo)
        Me.Panel1.Controls.Add(Me.chkBuscarCuentaBancaria)
        Me.Panel1.Controls.Add(Me.rdbCheque)
        Me.Panel1.Controls.Add(Me.chkBuscarEnProveedores)
        Me.Panel1.Controls.Add(Me.rdbProveedor)
        Me.Panel1.Controls.Add(Me.lblTipo)
        Me.Panel1.Controls.Add(Me.txtCuenta)
        Me.Panel1.Controls.Add(Me.lblSucursal)
        Me.Panel1.Controls.Add(Me.cbxSucursal)
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.lblCuenta)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.lblMoneda)
        Me.Panel1.Controls.Add(Me.cbxMoneda)
        Me.Panel1.Controls.Add(Me.lblDebeHaber)
        Me.Panel1.Controls.Add(Me.cbxDebeHaber)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.lblOrden)
        Me.Panel1.Controls.Add(Me.txtOrden)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(785, 199)
        Me.Panel1.TabIndex = 0
        '
        'chkSoloDiferido
        '
        Me.chkSoloDiferido.BackColor = System.Drawing.Color.Transparent
        Me.chkSoloDiferido.Color = System.Drawing.Color.Empty
        Me.chkSoloDiferido.Location = New System.Drawing.Point(425, 171)
        Me.chkSoloDiferido.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSoloDiferido.Name = "chkSoloDiferido"
        Me.chkSoloDiferido.Size = New System.Drawing.Size(170, 17)
        Me.chkSoloDiferido.SoloLectura = False
        Me.chkSoloDiferido.TabIndex = 26
        Me.chkSoloDiferido.Texto = "Solo CHQ Diferido"
        Me.chkSoloDiferido.Valor = False
        '
        'rdbChequeDiferido
        '
        Me.rdbChequeDiferido.AutoSize = True
        Me.rdbChequeDiferido.Location = New System.Drawing.Point(286, 123)
        Me.rdbChequeDiferido.Name = "rdbChequeDiferido"
        Me.rdbChequeDiferido.Size = New System.Drawing.Size(101, 17)
        Me.rdbChequeDiferido.TabIndex = 25
        Me.rdbChequeDiferido.TabStop = True
        Me.rdbChequeDiferido.Text = "Cheque Diferido"
        Me.rdbChequeDiferido.UseVisualStyleBackColor = True
        '
        'rdbDocumento
        '
        Me.rdbDocumento.AutoSize = True
        Me.rdbDocumento.Location = New System.Drawing.Point(149, 93)
        Me.rdbDocumento.Name = "rdbDocumento"
        Me.rdbDocumento.Size = New System.Drawing.Size(85, 17)
        Me.rdbDocumento.TabIndex = 24
        Me.rdbDocumento.TabStop = True
        Me.rdbDocumento.Text = "Documentos"
        Me.rdbDocumento.UseVisualStyleBackColor = True
        '
        'lblTipoComprobante
        '
        Me.lblTipoComprobante.AutoSize = True
        Me.lblTipoComprobante.Location = New System.Drawing.Point(3, 125)
        Me.lblTipoComprobante.Name = "lblTipoComprobante"
        Me.lblTipoComprobante.Size = New System.Drawing.Size(53, 13)
        Me.lblTipoComprobante.TabIndex = 22
        Me.lblTipoComprobante.Text = "T. Comp.:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(70, 119)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(194, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 23
        Me.cbxTipoComprobante.Texto = ""
        '
        'rdbDiferenciaCambio
        '
        Me.rdbDiferenciaCambio.AutoSize = True
        Me.rdbDiferenciaCambio.Location = New System.Drawing.Point(388, 95)
        Me.rdbDiferenciaCambio.Name = "rdbDiferenciaCambio"
        Me.rdbDiferenciaCambio.Size = New System.Drawing.Size(94, 17)
        Me.rdbDiferenciaCambio.TabIndex = 15
        Me.rdbDiferenciaCambio.TabStop = True
        Me.rdbDiferenciaCambio.Text = "Dif. de Cambio"
        Me.rdbDiferenciaCambio.UseVisualStyleBackColor = True
        '
        'rdbRetencion
        '
        Me.rdbRetencion.AutoSize = True
        Me.rdbRetencion.Location = New System.Drawing.Point(308, 95)
        Me.rdbRetencion.Name = "rdbRetencion"
        Me.rdbRetencion.Size = New System.Drawing.Size(74, 17)
        Me.rdbRetencion.TabIndex = 14
        Me.rdbRetencion.TabStop = True
        Me.rdbRetencion.Text = "Retencion"
        Me.rdbRetencion.UseVisualStyleBackColor = True
        '
        'rdbEfectivo
        '
        Me.rdbEfectivo.AutoSize = True
        Me.rdbEfectivo.Location = New System.Drawing.Point(238, 95)
        Me.rdbEfectivo.Name = "rdbEfectivo"
        Me.rdbEfectivo.Size = New System.Drawing.Size(64, 17)
        Me.rdbEfectivo.TabIndex = 13
        Me.rdbEfectivo.TabStop = True
        Me.rdbEfectivo.Text = "Efectivo"
        Me.rdbEfectivo.UseVisualStyleBackColor = True
        '
        'chkBuscarCuentaBancaria
        '
        Me.chkBuscarCuentaBancaria.BackColor = System.Drawing.Color.Transparent
        Me.chkBuscarCuentaBancaria.Color = System.Drawing.Color.Empty
        Me.chkBuscarCuentaBancaria.Location = New System.Drawing.Point(267, 171)
        Me.chkBuscarCuentaBancaria.Margin = New System.Windows.Forms.Padding(4)
        Me.chkBuscarCuentaBancaria.Name = "chkBuscarCuentaBancaria"
        Me.chkBuscarCuentaBancaria.Size = New System.Drawing.Size(170, 17)
        Me.chkBuscarCuentaBancaria.SoloLectura = False
        Me.chkBuscarCuentaBancaria.TabIndex = 21
        Me.chkBuscarCuentaBancaria.Texto = "Buscar en Cuenta Bancaria"
        Me.chkBuscarCuentaBancaria.Valor = False
        '
        'rdbCheque
        '
        Me.rdbCheque.AutoSize = True
        Me.rdbCheque.Location = New System.Drawing.Point(389, 121)
        Me.rdbCheque.Name = "rdbCheque"
        Me.rdbCheque.Size = New System.Drawing.Size(90, 17)
        Me.rdbCheque.TabIndex = 12
        Me.rdbCheque.TabStop = True
        Me.rdbCheque.Text = "Cheque al dia"
        Me.rdbCheque.UseVisualStyleBackColor = True
        '
        'chkBuscarEnProveedores
        '
        Me.chkBuscarEnProveedores.BackColor = System.Drawing.Color.Transparent
        Me.chkBuscarEnProveedores.Color = System.Drawing.Color.Empty
        Me.chkBuscarEnProveedores.Location = New System.Drawing.Point(125, 171)
        Me.chkBuscarEnProveedores.Margin = New System.Windows.Forms.Padding(4)
        Me.chkBuscarEnProveedores.Name = "chkBuscarEnProveedores"
        Me.chkBuscarEnProveedores.Size = New System.Drawing.Size(169, 17)
        Me.chkBuscarEnProveedores.SoloLectura = False
        Me.chkBuscarEnProveedores.TabIndex = 20
        Me.chkBuscarEnProveedores.Texto = "Buscar en proveedores"
        Me.chkBuscarEnProveedores.Valor = False
        '
        'rdbProveedor
        '
        Me.rdbProveedor.AutoSize = True
        Me.rdbProveedor.Location = New System.Drawing.Point(69, 95)
        Me.rdbProveedor.Name = "rdbProveedor"
        Me.rdbProveedor.Size = New System.Drawing.Size(74, 17)
        Me.rdbProveedor.TabIndex = 11
        Me.rdbProveedor.TabStop = True
        Me.rdbProveedor.Text = "Proveedor"
        Me.rdbProveedor.UseVisualStyleBackColor = True
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(2, 97)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 10
        Me.lblTipo.Text = "Tipo:"
        '
        'txtCuenta
        '
        Me.txtCuenta.AlturaMaxima = 100
        Me.txtCuenta.Consulta = Nothing
        Me.txtCuenta.IDCentroCosto = 0
        Me.txtCuenta.IDUnidadNegocio = 0
        Me.txtCuenta.ListarTodas = False
        Me.txtCuenta.Location = New System.Drawing.Point(70, 35)
        Me.txtCuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Registro = Nothing
        Me.txtCuenta.Resolucion173 = False
        Me.txtCuenta.Seleccionado = False
        Me.txtCuenta.Size = New System.Drawing.Size(432, 27)
        Me.txtCuenta.SoloLectura = False
        Me.txtCuenta.TabIndex = 7
        Me.txtCuenta.Texto = Nothing
        Me.txtCuenta.whereFiltro = Nothing
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(159, 12)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Text = "Sucursal:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(216, 8)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(143, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(3, 12)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(3, 40)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 6
        Me.lblCuenta.Text = "Cuenta:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(70, 7)
        Me.txtID.Margin = New System.Windows.Forms.Padding(4)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(73, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(365, 12)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 4
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(425, 8)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(77, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 5
        Me.cbxMoneda.Texto = "GS"
        '
        'lblDebeHaber
        '
        Me.lblDebeHaber.AutoSize = True
        Me.lblDebeHaber.Location = New System.Drawing.Point(2, 72)
        Me.lblDebeHaber.Name = "lblDebeHaber"
        Me.lblDebeHaber.Size = New System.Drawing.Size(55, 13)
        Me.lblDebeHaber.TabIndex = 8
        Me.lblDebeHaber.Text = "Deb/Hab."
        '
        'cbxDebeHaber
        '
        Me.cbxDebeHaber.CampoWhere = Nothing
        Me.cbxDebeHaber.CargarUnaSolaVez = False
        Me.cbxDebeHaber.DataDisplayMember = Nothing
        Me.cbxDebeHaber.DataFilter = Nothing
        Me.cbxDebeHaber.DataOrderBy = Nothing
        Me.cbxDebeHaber.DataSource = Nothing
        Me.cbxDebeHaber.DataValueMember = Nothing
        Me.cbxDebeHaber.dtSeleccionado = Nothing
        Me.cbxDebeHaber.FormABM = Nothing
        Me.cbxDebeHaber.Indicaciones = Nothing
        Me.cbxDebeHaber.Location = New System.Drawing.Point(69, 68)
        Me.cbxDebeHaber.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDebeHaber.Name = "cbxDebeHaber"
        Me.cbxDebeHaber.SeleccionMultiple = False
        Me.cbxDebeHaber.SeleccionObligatoria = False
        Me.cbxDebeHaber.Size = New System.Drawing.Size(87, 21)
        Me.cbxDebeHaber.SoloLectura = False
        Me.cbxDebeHaber.TabIndex = 9
        Me.cbxDebeHaber.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(70, 145)
        Me.txtDescripcion.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(433, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 17
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(5, 177)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 13)
        Me.lblOrden.TabIndex = 18
        Me.lblOrden.Text = "Orden:"
        '
        'txtOrden
        '
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Decimales = True
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(70, 171)
        Me.txtOrden.Margin = New System.Windows.Forms.Padding(4)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(47, 22)
        Me.txtOrden.SoloLectura = False
        Me.txtOrden.TabIndex = 19
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOrden.Texto = "0"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(3, 150)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 16
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'lvLista
        '
        Me.lvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLista.Location = New System.Drawing.Point(3, 244)
        Me.lvLista.Name = "lvLista"
        Me.lvLista.Size = New System.Drawing.Size(785, 220)
        Me.lvLista.TabIndex = 0
        Me.lvLista.UseCompatibleStateImageBehavior = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Controls.Add(Me.btnEditar)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 208)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(785, 30)
        Me.Panel2.TabIndex = 1
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(0, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(81, 3)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 1
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(243, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(162, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(426, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 467)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(791, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'ocxCuentaFijaOrdenPago2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "ocxCuentaFijaOrdenPago2"
        Me.Size = New System.Drawing.Size(791, 489)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblDebeHaber As System.Windows.Forms.Label
    Friend WithEvents cbxDebeHaber As ERP.ocxCBX
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents txtOrden As ERP.ocxTXTNumeric
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lvLista As System.Windows.Forms.ListView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents rdbRetencion As System.Windows.Forms.RadioButton
    Friend WithEvents rdbEfectivo As System.Windows.Forms.RadioButton
    Friend WithEvents chkBuscarCuentaBancaria As ERP.ocxCHK
    Friend WithEvents rdbCheque As System.Windows.Forms.RadioButton
    Friend WithEvents chkBuscarEnProveedores As ERP.ocxCHK
    Friend WithEvents rdbProveedor As System.Windows.Forms.RadioButton
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents rdbDiferenciaCambio As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDocumento As System.Windows.Forms.RadioButton
    Friend WithEvents lblTipoComprobante As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents rdbChequeDiferido As System.Windows.Forms.RadioButton
    Friend WithEvents chkSoloDiferido As ERP.ocxCHK

End Class

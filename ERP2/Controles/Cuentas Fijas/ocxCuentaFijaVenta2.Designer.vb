﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ocxCuentaFijaNotaCredito2
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaVentaCliente1 = New ERP.ocxCuentaFijaVentaCliente()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaVentaImpuesto1 = New ERP.ocxCuentaFijaVentaImpuesto()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaVentaVenta1 = New ERP.ocxCuentaFijaVentaVenta()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaVentaMercaderia1 = New ERP.ocxCuentaFijaVentaMercaderia()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaVentaCostoMercaderia1 = New ERP.ocxCuentaFijaVentaCostoMercaderia()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.OcxCuentaFijaVentaDescuento1 = New ERP.ocxCuentaFijaVentaDescuento()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(827, 528)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.OcxCuentaFijaVentaCliente1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(819, 502)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Cliente"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaVentaCliente1
        '
        Me.OcxCuentaFijaVentaCliente1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaVentaCliente1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaVentaCliente1.Name = "OcxCuentaFijaVentaCliente1"
        Me.OcxCuentaFijaVentaCliente1.Size = New System.Drawing.Size(819, 502)
        Me.OcxCuentaFijaVentaCliente1.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.OcxCuentaFijaVentaImpuesto1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(819, 502)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Impuesto"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaVentaImpuesto1
        '
        Me.OcxCuentaFijaVentaImpuesto1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaVentaImpuesto1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaVentaImpuesto1.Name = "OcxCuentaFijaVentaImpuesto1"
        Me.OcxCuentaFijaVentaImpuesto1.Size = New System.Drawing.Size(819, 502)
        Me.OcxCuentaFijaVentaImpuesto1.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.OcxCuentaFijaVentaVenta1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(819, 502)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Venta"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaVentaVenta1
        '
        Me.OcxCuentaFijaVentaVenta1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaVentaVenta1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaVentaVenta1.Name = "OcxCuentaFijaVentaVenta1"
        Me.OcxCuentaFijaVentaVenta1.Size = New System.Drawing.Size(819, 502)
        Me.OcxCuentaFijaVentaVenta1.TabIndex = 0
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.OcxCuentaFijaVentaMercaderia1)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(819, 502)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Mercaderia"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaVentaMercaderia1
        '
        Me.OcxCuentaFijaVentaMercaderia1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaVentaMercaderia1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaVentaMercaderia1.Name = "OcxCuentaFijaVentaMercaderia1"
        Me.OcxCuentaFijaVentaMercaderia1.Size = New System.Drawing.Size(819, 502)
        Me.OcxCuentaFijaVentaMercaderia1.TabIndex = 0
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.OcxCuentaFijaVentaCostoMercaderia1)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(819, 502)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Costo"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaVentaCostoMercaderia1
        '
        Me.OcxCuentaFijaVentaCostoMercaderia1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaVentaCostoMercaderia1.Location = New System.Drawing.Point(3, 3)
        Me.OcxCuentaFijaVentaCostoMercaderia1.Name = "OcxCuentaFijaVentaCostoMercaderia1"
        Me.OcxCuentaFijaVentaCostoMercaderia1.Size = New System.Drawing.Size(813, 496)
        Me.OcxCuentaFijaVentaCostoMercaderia1.TabIndex = 0
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.OcxCuentaFijaVentaDescuento1)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(819, 502)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Descuento"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'OcxCuentaFijaVentaDescuento1
        '
        Me.OcxCuentaFijaVentaDescuento1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OcxCuentaFijaVentaDescuento1.Location = New System.Drawing.Point(0, 0)
        Me.OcxCuentaFijaVentaDescuento1.Name = "OcxCuentaFijaVentaDescuento1"
        Me.OcxCuentaFijaVentaDescuento1.Size = New System.Drawing.Size(819, 502)
        Me.OcxCuentaFijaVentaDescuento1.TabIndex = 0
        '
        'ocxCuentaFijaNotaCredito2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "ocxCuentaFijaNotaCredito2"
        Me.Size = New System.Drawing.Size(827, 528)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents OcxCuentaFijaVentaCliente1 As ERP.ocxCuentaFijaVentaCliente
    Friend WithEvents OcxCuentaFijaVentaImpuesto1 As ERP.ocxCuentaFijaVentaImpuesto
    Friend WithEvents OcxCuentaFijaVentaVenta1 As ERP.ocxCuentaFijaVentaVenta
    Friend WithEvents OcxCuentaFijaVentaDescuento1 As ERP.ocxCuentaFijaVentaDescuento
    Friend WithEvents OcxCuentaFijaVentaMercaderia1 As ERP.ocxCuentaFijaVentaMercaderia
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents OcxCuentaFijaVentaCostoMercaderia1 As ERP.ocxCuentaFijaVentaCostoMercaderia
End Class

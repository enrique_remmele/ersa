﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxCuentaFijaDescargaStock
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtCuenta = New ERP.ocxTXTCuentaContable()
        Me.lblTipoOperacion = New System.Windows.Forms.Label()
        Me.cbxTipoOperacion = New ERP.ocxCBX()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxDetalleTipo = New ERP.ocxCBX()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblTipoComprobante = New System.Windows.Forms.Label()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblDebeHaber = New System.Windows.Forms.Label()
        Me.cbxDebeHaber = New ERP.ocxCBX()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.txtOrden = New ERP.ocxTXTNumeric()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtCuenta)
        Me.Panel1.Controls.Add(Me.lblTipoOperacion)
        Me.Panel1.Controls.Add(Me.cbxTipoOperacion)
        Me.Panel1.Controls.Add(Me.lblSucursal)
        Me.Panel1.Controls.Add(Me.cbxSucursal)
        Me.Panel1.Controls.Add(Me.cbxDetalleTipo)
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.lblTipoComprobante)
        Me.Panel1.Controls.Add(Me.cbxTipo)
        Me.Panel1.Controls.Add(Me.lblCuenta)
        Me.Panel1.Controls.Add(Me.lblTipo)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.cbxTipoComprobante)
        Me.Panel1.Controls.Add(Me.lblMoneda)
        Me.Panel1.Controls.Add(Me.cbxMoneda)
        Me.Panel1.Controls.Add(Me.lblDebeHaber)
        Me.Panel1.Controls.Add(Me.cbxDebeHaber)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.lblOrden)
        Me.Panel1.Controls.Add(Me.txtOrden)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Location = New System.Drawing.Point(9, 10)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(683, 170)
        Me.Panel1.TabIndex = 2
        '
        'txtCuenta
        '
        Me.txtCuenta.AlturaMaxima = 100
        Me.txtCuenta.Consulta = Nothing
        Me.txtCuenta.IDCentroCosto = 0
        Me.txtCuenta.IDUnidadNegocio = 0
        Me.txtCuenta.ListarTodas = False
        Me.txtCuenta.Location = New System.Drawing.Point(70, 35)
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Registro = Nothing
        Me.txtCuenta.Resolucion173 = False
        Me.txtCuenta.Seleccionado = False
        Me.txtCuenta.Size = New System.Drawing.Size(432, 27)
        Me.txtCuenta.SoloLectura = False
        Me.txtCuenta.TabIndex = 7
        Me.txtCuenta.Texto = Nothing
        Me.txtCuenta.whereFiltro = Nothing
        '
        'lblTipoOperacion
        '
        Me.lblTipoOperacion.AutoSize = True
        Me.lblTipoOperacion.Location = New System.Drawing.Point(159, 65)
        Me.lblTipoOperacion.Name = "lblTipoOperacion"
        Me.lblTipoOperacion.Size = New System.Drawing.Size(80, 13)
        Me.lblTipoOperacion.TabIndex = 10
        Me.lblTipoOperacion.Text = "Tip. Operacion:"
        '
        'cbxTipoOperacion
        '
        Me.cbxTipoOperacion.CampoWhere = Nothing
        Me.cbxTipoOperacion.CargarUnaSolaVez = False
        Me.cbxTipoOperacion.DataDisplayMember = Nothing
        Me.cbxTipoOperacion.DataFilter = Nothing
        Me.cbxTipoOperacion.DataOrderBy = Nothing
        Me.cbxTipoOperacion.DataSource = Nothing
        Me.cbxTipoOperacion.DataValueMember = Nothing
        Me.cbxTipoOperacion.dtSeleccionado = Nothing
        Me.cbxTipoOperacion.FormABM = Nothing
        Me.cbxTipoOperacion.Indicaciones = Nothing
        Me.cbxTipoOperacion.Location = New System.Drawing.Point(241, 61)
        Me.cbxTipoOperacion.Name = "cbxTipoOperacion"
        Me.cbxTipoOperacion.SeleccionMultiple = False
        Me.cbxTipoOperacion.SeleccionObligatoria = False
        Me.cbxTipoOperacion.Size = New System.Drawing.Size(118, 21)
        Me.cbxTipoOperacion.SoloLectura = False
        Me.cbxTipoOperacion.TabIndex = 11
        Me.cbxTipoOperacion.Texto = ""
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(159, 12)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Text = "Sucursal:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(216, 8)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(143, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'cbxDetalleTipo
        '
        Me.cbxDetalleTipo.CampoWhere = Nothing
        Me.cbxDetalleTipo.CargarUnaSolaVez = False
        Me.cbxDetalleTipo.DataDisplayMember = Nothing
        Me.cbxDetalleTipo.DataFilter = Nothing
        Me.cbxDetalleTipo.DataOrderBy = Nothing
        Me.cbxDetalleTipo.DataSource = Nothing
        Me.cbxDetalleTipo.DataValueMember = Nothing
        Me.cbxDetalleTipo.dtSeleccionado = Nothing
        Me.cbxDetalleTipo.FormABM = Nothing
        Me.cbxDetalleTipo.Indicaciones = Nothing
        Me.cbxDetalleTipo.Location = New System.Drawing.Point(161, 88)
        Me.cbxDetalleTipo.Name = "cbxDetalleTipo"
        Me.cbxDetalleTipo.SeleccionMultiple = False
        Me.cbxDetalleTipo.SeleccionObligatoria = False
        Me.cbxDetalleTipo.Size = New System.Drawing.Size(173, 21)
        Me.cbxDetalleTipo.SoloLectura = False
        Me.cbxDetalleTipo.TabIndex = 16
        Me.cbxDetalleTipo.Texto = ""
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(3, 12)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'lblTipoComprobante
        '
        Me.lblTipoComprobante.AutoSize = True
        Me.lblTipoComprobante.Location = New System.Drawing.Point(357, 65)
        Me.lblTipoComprobante.Name = "lblTipoComprobante"
        Me.lblTipoComprobante.Size = New System.Drawing.Size(61, 13)
        Me.lblTipoComprobante.TabIndex = 12
        Me.lblTipoComprobante.Text = "Tipo Comp:"
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.dtSeleccionado = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(69, 88)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionMultiple = False
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(87, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 15
        Me.cbxTipo.Texto = ""
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(3, 40)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 6
        Me.lblCuenta.Text = "Cuenta:"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(3, 92)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 14
        Me.lblTipo.Text = "Tipo:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(70, 7)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(73, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = Nothing
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = Nothing
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = Nothing
        Me.cbxTipoComprobante.DataValueMember = Nothing
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(424, 61)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(77, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 13
        Me.cbxTipoComprobante.Texto = ""
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(365, 12)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 4
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(425, 8)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(77, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 5
        Me.cbxMoneda.Texto = "GS"
        '
        'lblDebeHaber
        '
        Me.lblDebeHaber.AutoSize = True
        Me.lblDebeHaber.Location = New System.Drawing.Point(3, 65)
        Me.lblDebeHaber.Name = "lblDebeHaber"
        Me.lblDebeHaber.Size = New System.Drawing.Size(55, 13)
        Me.lblDebeHaber.TabIndex = 8
        Me.lblDebeHaber.Text = "Deb/Hab."
        '
        'cbxDebeHaber
        '
        Me.cbxDebeHaber.CampoWhere = Nothing
        Me.cbxDebeHaber.CargarUnaSolaVez = False
        Me.cbxDebeHaber.DataDisplayMember = Nothing
        Me.cbxDebeHaber.DataFilter = Nothing
        Me.cbxDebeHaber.DataOrderBy = Nothing
        Me.cbxDebeHaber.DataSource = Nothing
        Me.cbxDebeHaber.DataValueMember = Nothing
        Me.cbxDebeHaber.dtSeleccionado = Nothing
        Me.cbxDebeHaber.FormABM = Nothing
        Me.cbxDebeHaber.Indicaciones = Nothing
        Me.cbxDebeHaber.Location = New System.Drawing.Point(70, 61)
        Me.cbxDebeHaber.Name = "cbxDebeHaber"
        Me.cbxDebeHaber.SeleccionMultiple = False
        Me.cbxDebeHaber.SeleccionObligatoria = False
        Me.cbxDebeHaber.Size = New System.Drawing.Size(87, 21)
        Me.cbxDebeHaber.SoloLectura = False
        Me.cbxDebeHaber.TabIndex = 9
        Me.cbxDebeHaber.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(69, 115)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(433, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 18
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(3, 147)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 13)
        Me.lblOrden.TabIndex = 19
        Me.lblOrden.Text = "Orden:"
        '
        'txtOrden
        '
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Decimales = True
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(69, 142)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(76, 22)
        Me.txtOrden.SoloLectura = False
        Me.txtOrden.TabIndex = 20
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOrden.Texto = "0"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(3, 120)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 17
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Controls.Add(Me.btnEditar)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Location = New System.Drawing.Point(9, 184)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(683, 32)
        Me.Panel2.TabIndex = 3
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(2, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(81, 3)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 1
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(243, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(162, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(426, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgv.Location = New System.Drawing.Point(9, 222)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.ShowEditingIcon = False
        Me.dgv.Size = New System.Drawing.Size(683, 240)
        Me.dgv.TabIndex = 4
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 465)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(698, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'ocxCuentaFijaDescargaStock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ocxCuentaFijaDescargaStock"
        Me.Size = New System.Drawing.Size(698, 487)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents txtCuenta As ocxTXTCuentaContable
    Friend WithEvents lblTipoOperacion As Label
    Friend WithEvents cbxTipoOperacion As ocxCBX
    Friend WithEvents lblSucursal As Label
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents cbxDetalleTipo As ocxCBX
    Friend WithEvents lblID As Label
    Friend WithEvents lblTipoComprobante As Label
    Friend WithEvents cbxTipo As ocxCBX
    Friend WithEvents lblCuenta As Label
    Friend WithEvents lblTipo As Label
    Friend WithEvents txtID As ocxTXTNumeric
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents lblMoneda As Label
    Friend WithEvents cbxMoneda As ocxCBX
    Friend WithEvents lblDebeHaber As Label
    Friend WithEvents cbxDebeHaber As ocxCBX
    Friend WithEvents txtDescripcion As ocxTXTString
    Friend WithEvents lblOrden As Label
    Friend WithEvents txtOrden As ocxTXTNumeric
    Friend WithEvents lblDescripcion As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnNuevo As Button
    Friend WithEvents btnEditar As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnEliminar As Button
    Friend WithEvents dgv As DataGridView
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxCuentaFijaDepositoBancario
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RBDebito = New System.Windows.Forms.RadioButton()
        Me.RBCredito = New System.Windows.Forms.RadioButton()
        Me.lblTipoTarjeta = New System.Windows.Forms.Label()
        Me.CHKTarjeta = New ERP.ocxCHK()
        Me.chkDocumentos = New ERP.ocxCHK()
        Me.chkDiferencia = New ERP.ocxCHK()
        Me.lblTipoCuenta = New System.Windows.Forms.Label()
        Me.cbxTipoCuenta = New ERP.ocxCBX()
        Me.chkChequeRechazado = New ERP.ocxCHK()
        Me.chkChequeDiferido = New ERP.ocxCHK()
        Me.chkChequeAlDia = New ERP.ocxCHK()
        Me.chkEfectivo = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkBuscarEnCuentaBancaria = New ERP.ocxCHK()
        Me.txtCuenta = New ERP.ocxTXTCuentaContable()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblDebeHaber = New System.Windows.Forms.Label()
        Me.cbxDebeHaber = New ERP.ocxCBX()
        Me.txtDescripcion = New ERP.ocxTXTString()
        Me.lblOrden = New System.Windows.Forms.Label()
        Me.txtOrden = New ERP.ocxTXTNumeric()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxFiltroSucursal = New ERP.ocxCBX()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgvLista, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 198.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(797, 408)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLista.Location = New System.Drawing.Point(3, 277)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(791, 128)
        Me.dgvLista.TabIndex = 4
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RBDebito)
        Me.Panel1.Controls.Add(Me.RBCredito)
        Me.Panel1.Controls.Add(Me.lblTipoTarjeta)
        Me.Panel1.Controls.Add(Me.CHKTarjeta)
        Me.Panel1.Controls.Add(Me.chkDocumentos)
        Me.Panel1.Controls.Add(Me.chkDiferencia)
        Me.Panel1.Controls.Add(Me.lblTipoCuenta)
        Me.Panel1.Controls.Add(Me.cbxTipoCuenta)
        Me.Panel1.Controls.Add(Me.chkChequeRechazado)
        Me.Panel1.Controls.Add(Me.chkChequeDiferido)
        Me.Panel1.Controls.Add(Me.chkChequeAlDia)
        Me.Panel1.Controls.Add(Me.chkEfectivo)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.chkBuscarEnCuentaBancaria)
        Me.Panel1.Controls.Add(Me.txtCuenta)
        Me.Panel1.Controls.Add(Me.lblSucursal)
        Me.Panel1.Controls.Add(Me.cbxSucursal)
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.lblCuenta)
        Me.Panel1.Controls.Add(Me.txtID)
        Me.Panel1.Controls.Add(Me.lblMoneda)
        Me.Panel1.Controls.Add(Me.cbxMoneda)
        Me.Panel1.Controls.Add(Me.lblDebeHaber)
        Me.Panel1.Controls.Add(Me.cbxDebeHaber)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.lblOrden)
        Me.Panel1.Controls.Add(Me.txtOrden)
        Me.Panel1.Controls.Add(Me.lblDescripcion)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(791, 192)
        Me.Panel1.TabIndex = 0
        '
        'RBDebito
        '
        Me.RBDebito.AutoSize = True
        Me.RBDebito.Location = New System.Drawing.Point(312, 141)
        Me.RBDebito.Name = "RBDebito"
        Me.RBDebito.Size = New System.Drawing.Size(56, 17)
        Me.RBDebito.TabIndex = 27
        Me.RBDebito.TabStop = True
        Me.RBDebito.Text = "Debito"
        Me.RBDebito.UseVisualStyleBackColor = True
        '
        'RBCredito
        '
        Me.RBCredito.AutoSize = True
        Me.RBCredito.Location = New System.Drawing.Point(249, 141)
        Me.RBCredito.Name = "RBCredito"
        Me.RBCredito.Size = New System.Drawing.Size(58, 17)
        Me.RBCredito.TabIndex = 26
        Me.RBCredito.TabStop = True
        Me.RBCredito.Text = "Credito"
        Me.RBCredito.UseVisualStyleBackColor = True
        '
        'lblTipoTarjeta
        '
        Me.lblTipoTarjeta.AutoSize = True
        Me.lblTipoTarjeta.Location = New System.Drawing.Point(215, 142)
        Me.lblTipoTarjeta.Name = "lblTipoTarjeta"
        Me.lblTipoTarjeta.Size = New System.Drawing.Size(31, 13)
        Me.lblTipoTarjeta.TabIndex = 25
        Me.lblTipoTarjeta.Text = "Tipo:"
        '
        'CHKTarjeta
        '
        Me.CHKTarjeta.BackColor = System.Drawing.Color.Transparent
        Me.CHKTarjeta.Color = System.Drawing.Color.Empty
        Me.CHKTarjeta.Location = New System.Drawing.Point(152, 139)
        Me.CHKTarjeta.Name = "CHKTarjeta"
        Me.CHKTarjeta.Size = New System.Drawing.Size(80, 21)
        Me.CHKTarjeta.SoloLectura = False
        Me.CHKTarjeta.TabIndex = 24
        Me.CHKTarjeta.Texto = "Tarjeta"
        Me.CHKTarjeta.Valor = False
        '
        'chkDocumentos
        '
        Me.chkDocumentos.BackColor = System.Drawing.Color.Transparent
        Me.chkDocumentos.Color = System.Drawing.Color.Empty
        Me.chkDocumentos.Location = New System.Drawing.Point(70, 139)
        Me.chkDocumentos.Name = "chkDocumentos"
        Me.chkDocumentos.Size = New System.Drawing.Size(80, 21)
        Me.chkDocumentos.SoloLectura = False
        Me.chkDocumentos.TabIndex = 23
        Me.chkDocumentos.Texto = "Documento"
        Me.chkDocumentos.Valor = False
        '
        'chkDiferencia
        '
        Me.chkDiferencia.BackColor = System.Drawing.Color.Transparent
        Me.chkDiferencia.Color = System.Drawing.Color.Empty
        Me.chkDiferencia.Location = New System.Drawing.Point(471, 115)
        Me.chkDiferencia.Name = "chkDiferencia"
        Me.chkDiferencia.Size = New System.Drawing.Size(80, 21)
        Me.chkDiferencia.SoloLectura = False
        Me.chkDiferencia.TabIndex = 22
        Me.chkDiferencia.Texto = "Dif. Cambio"
        Me.chkDiferencia.Valor = False
        '
        'lblTipoCuenta
        '
        Me.lblTipoCuenta.AutoSize = True
        Me.lblTipoCuenta.Location = New System.Drawing.Point(163, 69)
        Me.lblTipoCuenta.Name = "lblTipoCuenta"
        Me.lblTipoCuenta.Size = New System.Drawing.Size(83, 13)
        Me.lblTipoCuenta.TabIndex = 10
        Me.lblTipoCuenta.Text = "Tipo de Cuenta:"
        '
        'cbxTipoCuenta
        '
        Me.cbxTipoCuenta.CampoWhere = Nothing
        Me.cbxTipoCuenta.CargarUnaSolaVez = False
        Me.cbxTipoCuenta.DataDisplayMember = Nothing
        Me.cbxTipoCuenta.DataFilter = Nothing
        Me.cbxTipoCuenta.DataOrderBy = Nothing
        Me.cbxTipoCuenta.DataSource = Nothing
        Me.cbxTipoCuenta.DataValueMember = Nothing
        Me.cbxTipoCuenta.dtSeleccionado = Nothing
        Me.cbxTipoCuenta.FormABM = Nothing
        Me.cbxTipoCuenta.Indicaciones = Nothing
        Me.cbxTipoCuenta.Location = New System.Drawing.Point(252, 65)
        Me.cbxTipoCuenta.Name = "cbxTipoCuenta"
        Me.cbxTipoCuenta.SeleccionMultiple = False
        Me.cbxTipoCuenta.SeleccionObligatoria = False
        Me.cbxTipoCuenta.Size = New System.Drawing.Size(131, 21)
        Me.cbxTipoCuenta.SoloLectura = False
        Me.cbxTipoCuenta.TabIndex = 11
        Me.cbxTipoCuenta.Texto = ""
        '
        'chkChequeRechazado
        '
        Me.chkChequeRechazado.BackColor = System.Drawing.Color.Transparent
        Me.chkChequeRechazado.Color = System.Drawing.Color.Empty
        Me.chkChequeRechazado.Location = New System.Drawing.Point(349, 115)
        Me.chkChequeRechazado.Name = "chkChequeRechazado"
        Me.chkChequeRechazado.Size = New System.Drawing.Size(120, 21)
        Me.chkChequeRechazado.SoloLectura = False
        Me.chkChequeRechazado.TabIndex = 18
        Me.chkChequeRechazado.Texto = "Cheque Rechazado"
        Me.chkChequeRechazado.Valor = False
        '
        'chkChequeDiferido
        '
        Me.chkChequeDiferido.BackColor = System.Drawing.Color.Transparent
        Me.chkChequeDiferido.Color = System.Drawing.Color.Empty
        Me.chkChequeDiferido.Location = New System.Drawing.Point(244, 115)
        Me.chkChequeDiferido.Name = "chkChequeDiferido"
        Me.chkChequeDiferido.Size = New System.Drawing.Size(103, 21)
        Me.chkChequeDiferido.SoloLectura = False
        Me.chkChequeDiferido.TabIndex = 17
        Me.chkChequeDiferido.Texto = "Cheque Diferido"
        Me.chkChequeDiferido.Valor = False
        '
        'chkChequeAlDia
        '
        Me.chkChequeAlDia.BackColor = System.Drawing.Color.Transparent
        Me.chkChequeAlDia.Color = System.Drawing.Color.Empty
        Me.chkChequeAlDia.Location = New System.Drawing.Point(152, 115)
        Me.chkChequeAlDia.Name = "chkChequeAlDia"
        Me.chkChequeAlDia.Size = New System.Drawing.Size(90, 21)
        Me.chkChequeAlDia.SoloLectura = False
        Me.chkChequeAlDia.TabIndex = 16
        Me.chkChequeAlDia.Texto = "Cheque Al Dia"
        Me.chkChequeAlDia.Valor = False
        '
        'chkEfectivo
        '
        Me.chkEfectivo.BackColor = System.Drawing.Color.Transparent
        Me.chkEfectivo.Color = System.Drawing.Color.Empty
        Me.chkEfectivo.Location = New System.Drawing.Point(70, 115)
        Me.chkEfectivo.Name = "chkEfectivo"
        Me.chkEfectivo.Size = New System.Drawing.Size(73, 21)
        Me.chkEfectivo.SoloLectura = False
        Me.chkEfectivo.TabIndex = 15
        Me.chkEfectivo.Texto = "Efectivo"
        Me.chkEfectivo.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 117)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Tipo:"
        '
        'chkBuscarEnCuentaBancaria
        '
        Me.chkBuscarEnCuentaBancaria.BackColor = System.Drawing.Color.Transparent
        Me.chkBuscarEnCuentaBancaria.Color = System.Drawing.Color.Empty
        Me.chkBuscarEnCuentaBancaria.Location = New System.Drawing.Point(162, 165)
        Me.chkBuscarEnCuentaBancaria.Name = "chkBuscarEnCuentaBancaria"
        Me.chkBuscarEnCuentaBancaria.Size = New System.Drawing.Size(341, 21)
        Me.chkBuscarEnCuentaBancaria.SoloLectura = False
        Me.chkBuscarEnCuentaBancaria.TabIndex = 21
        Me.chkBuscarEnCuentaBancaria.Texto = "Buscar en Cuenta Bancaria"
        Me.chkBuscarEnCuentaBancaria.Valor = False
        '
        'txtCuenta
        '
        Me.txtCuenta.AlturaMaxima = 100
        Me.txtCuenta.Consulta = Nothing
        Me.txtCuenta.IDCentroCosto = 0
        Me.txtCuenta.IDUnidadNegocio = 0
        Me.txtCuenta.ListarTodas = False
        Me.txtCuenta.Location = New System.Drawing.Point(70, 35)
        Me.txtCuenta.Name = "txtCuenta"
        Me.txtCuenta.Registro = Nothing
        Me.txtCuenta.Resolucion173 = False
        Me.txtCuenta.Seleccionado = False
        Me.txtCuenta.Size = New System.Drawing.Size(432, 27)
        Me.txtCuenta.SoloLectura = False
        Me.txtCuenta.TabIndex = 7
        Me.txtCuenta.Texto = Nothing
        Me.txtCuenta.whereFiltro = Nothing
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.Location = New System.Drawing.Point(159, 12)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 2
        Me.lblSucursal.Text = "Sucursal:"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = Nothing
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(216, 8)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(143, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Location = New System.Drawing.Point(3, 12)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 0
        Me.lblID.Text = "ID:"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Location = New System.Drawing.Point(3, 40)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(44, 13)
        Me.lblCuenta.TabIndex = 6
        Me.lblCuenta.Text = "Cuenta:"
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = True
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(70, 7)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(73, 22)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 1
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(365, 12)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 4
        Me.lblMoneda.Text = "Moneda:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Referencia"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(425, 8)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(77, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 5
        Me.cbxMoneda.Texto = "GS"
        '
        'lblDebeHaber
        '
        Me.lblDebeHaber.AutoSize = True
        Me.lblDebeHaber.Location = New System.Drawing.Point(3, 69)
        Me.lblDebeHaber.Name = "lblDebeHaber"
        Me.lblDebeHaber.Size = New System.Drawing.Size(55, 13)
        Me.lblDebeHaber.TabIndex = 8
        Me.lblDebeHaber.Text = "Deb/Hab."
        '
        'cbxDebeHaber
        '
        Me.cbxDebeHaber.CampoWhere = Nothing
        Me.cbxDebeHaber.CargarUnaSolaVez = False
        Me.cbxDebeHaber.DataDisplayMember = Nothing
        Me.cbxDebeHaber.DataFilter = Nothing
        Me.cbxDebeHaber.DataOrderBy = Nothing
        Me.cbxDebeHaber.DataSource = Nothing
        Me.cbxDebeHaber.DataValueMember = Nothing
        Me.cbxDebeHaber.dtSeleccionado = Nothing
        Me.cbxDebeHaber.FormABM = Nothing
        Me.cbxDebeHaber.Indicaciones = Nothing
        Me.cbxDebeHaber.Location = New System.Drawing.Point(70, 65)
        Me.cbxDebeHaber.Name = "cbxDebeHaber"
        Me.cbxDebeHaber.SeleccionMultiple = False
        Me.cbxDebeHaber.SeleccionObligatoria = False
        Me.cbxDebeHaber.Size = New System.Drawing.Size(87, 21)
        Me.cbxDebeHaber.SoloLectura = False
        Me.cbxDebeHaber.TabIndex = 9
        Me.cbxDebeHaber.Texto = ""
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Color = System.Drawing.Color.Empty
        Me.txtDescripcion.Indicaciones = Nothing
        Me.txtDescripcion.Location = New System.Drawing.Point(70, 88)
        Me.txtDescripcion.Multilinea = False
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(433, 21)
        Me.txtDescripcion.SoloLectura = False
        Me.txtDescripcion.TabIndex = 13
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDescripcion.Texto = ""
        '
        'lblOrden
        '
        Me.lblOrden.AutoSize = True
        Me.lblOrden.Location = New System.Drawing.Point(1, 169)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(39, 13)
        Me.lblOrden.TabIndex = 19
        Me.lblOrden.Text = "Orden:"
        '
        'txtOrden
        '
        Me.txtOrden.Color = System.Drawing.Color.Empty
        Me.txtOrden.Decimales = True
        Me.txtOrden.Indicaciones = Nothing
        Me.txtOrden.Location = New System.Drawing.Point(70, 164)
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(76, 22)
        Me.txtOrden.SoloLectura = False
        Me.txtOrden.TabIndex = 20
        Me.txtOrden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOrden.Texto = "0"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(3, 93)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(66, 13)
        Me.lblDescripcion.TabIndex = 12
        Me.lblDescripcion.Text = "Descripcion:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Controls.Add(Me.btnEditar)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 201)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(791, 31)
        Me.Panel2.TabIndex = 1
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(0, 3)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(81, 3)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 1
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(243, 3)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(162, 3)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(426, 3)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.cbxFiltroSucursal)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 238)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(791, 33)
        Me.Panel3.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Filtrar por Sucursal:"
        '
        'cbxFiltroSucursal
        '
        Me.cbxFiltroSucursal.CampoWhere = Nothing
        Me.cbxFiltroSucursal.CargarUnaSolaVez = False
        Me.cbxFiltroSucursal.DataDisplayMember = "Descripcion"
        Me.cbxFiltroSucursal.DataFilter = Nothing
        Me.cbxFiltroSucursal.DataOrderBy = "Descripcion"
        Me.cbxFiltroSucursal.DataSource = "VSucursal"
        Me.cbxFiltroSucursal.DataValueMember = "ID"
        Me.cbxFiltroSucursal.dtSeleccionado = Nothing
        Me.cbxFiltroSucursal.FormABM = Nothing
        Me.cbxFiltroSucursal.Indicaciones = Nothing
        Me.cbxFiltroSucursal.Location = New System.Drawing.Point(113, 3)
        Me.cbxFiltroSucursal.Name = "cbxFiltroSucursal"
        Me.cbxFiltroSucursal.SeleccionMultiple = False
        Me.cbxFiltroSucursal.SeleccionObligatoria = False
        Me.cbxFiltroSucursal.Size = New System.Drawing.Size(143, 21)
        Me.cbxFiltroSucursal.SoloLectura = False
        Me.cbxFiltroSucursal.TabIndex = 1
        Me.cbxFiltroSucursal.Texto = "ASUNCION"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 408)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(797, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'ocxCuentaFijaDepositoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "ocxCuentaFijaDepositoBancario"
        Me.Size = New System.Drawing.Size(797, 430)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkChequeRechazado As ERP.ocxCHK
    Friend WithEvents chkChequeDiferido As ERP.ocxCHK
    Friend WithEvents chkChequeAlDia As ERP.ocxCHK
    Friend WithEvents chkEfectivo As ERP.ocxCHK
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkBuscarEnCuentaBancaria As ERP.ocxCHK
    Friend WithEvents txtCuenta As ERP.ocxTXTCuentaContable
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblDebeHaber As System.Windows.Forms.Label
    Friend WithEvents cbxDebeHaber As ERP.ocxCBX
    Friend WithEvents txtDescripcion As ERP.ocxTXTString
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents txtOrden As ERP.ocxTXTNumeric
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblTipoCuenta As System.Windows.Forms.Label
    Friend WithEvents cbxTipoCuenta As ERP.ocxCBX
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxFiltroSucursal As ERP.ocxCBX
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents chkDiferencia As ERP.ocxCHK
    Friend WithEvents chkDocumentos As ERP.ocxCHK
    Friend WithEvents CHKTarjeta As ocxCHK
    Friend WithEvents RBDebito As RadioButton
    Friend WithEvents RBCredito As RadioButton
    Friend WithEvents lblTipoTarjeta As Label
End Class

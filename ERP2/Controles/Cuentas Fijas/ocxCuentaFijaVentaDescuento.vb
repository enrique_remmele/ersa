﻿Public Class ocxCuentaFijaVentaDescuento

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS

    'PROPIEDADES

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim IDOperacion As Integer

    'FUNCIONES
    Sub Inicializar()

        'Controles
        txtCuenta.Conectar()
        IDOperacion = CSistema.ObtenerIDOperacion(frmVenta.Name, "VENTAS CLIENTES", "FATCLI")
        txtProveedor.Conectar()

        'Funciones
        CargarInformacion()
        InicializarControles()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

    End Sub

    Sub CargarInformacion()

        'Cargar controles
        ReDim vControles(-1)

        CSistema.CargaControl(vControles, cbxSucursal)
        CSistema.CargaControl(vControles, cbxMoneda)
        CSistema.CargaControl(vControles, txtCuenta)
        CSistema.CargaControl(vControles, cbxDebeHaber)
        CSistema.CargaControl(vControles, txtOrden)
        CSistema.CargaControl(vControles, txtDescripcion)
        CSistema.CargaControl(vControles, cbxTipoDescuento)
        CSistema.CargaControl(vControles, txtProveedor)

        'DebeHaber
        cbxDebeHaber.cbx.Items.Add("DEBE")
        cbxDebeHaber.cbx.Items.Add("HABER")
        cbxDebeHaber.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        '
        'Monedas
        cbxMoneda.cbx.SelectedValue = 1
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar

        'Si es que se selecciono el registro.
        If dgvLista.SelectedRows.Count = 0 Then
            ctrError.SetError(dgvLista, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgvLista, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        Dim ID As Integer
        ID = dgvLista.SelectedRows(0).Cells("ID").Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VCFVentaDescuento Where ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxSucursal.SelectedValue(oRow("IDSucursal").ToString)
            cbxMoneda.SelectedValue(oRow("IDMoneda").ToString)

            txtCuenta.SetValue(oRow("CuentaContable").ToString)
            cbxDebeHaber.cbx.Text = oRow("Debe/Haber").ToString

            txtOrden.txt.Text = oRow("Orden").ToString
            txtDescripcion.txt.Text = oRow("Descripcion").ToString

            cbxTipoDescuento.SelectedValue(oRow("IDTipoDescuento").ToString)

            If IsNumeric(oRow("IDProveedor").ToString) = True Then
                txtProveedor.SetValue(oRow("IDProveedor").ToString)
            Else
                txtProveedor.SetValue(0)
            End If

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        CSistema.SqlToDataGrid(dgvLista, "Select * From VCFVentaDescuento Where IDSucursal=" & cbxFiltroSucursal.GetValue & " Order By Sucursal, Moneda, Orden")

        If dgvLista Is Nothing Then
            Exit Sub
        End If

        'Formato
        For c As Integer = 0 To dgvLista.Columns.Count - 1
            dgvLista.Columns(c).Visible = False
        Next

        If dgvLista.ColumnCount = 0 Then
            Exit Sub
        End If

        dgvLista.Columns("Orden").Visible = True
        dgvLista.Columns("Orden").DisplayIndex = 0
        dgvLista.Columns("ReferenciaMoneda").Visible = True
        dgvLista.Columns("ReferenciaMoneda").HeaderText = "Mon."
        dgvLista.Columns("Tipo").DisplayIndex = 1
        dgvLista.Columns("Tipo").Visible = True
        dgvLista.Columns("Codigo").Visible = True
        dgvLista.Columns("Denominacion").Visible = True
        dgvLista.Columns("Debe/Haber").Visible = True
        dgvLista.Columns("Descripcion").Visible = True

        dgvLista.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgvLista.Refresh()

    End Sub

    Sub InicializarControles()

        txtID.Text = CInt(CSistema.ExecuteScalar("Select ISNULL((Max(ID)+1), 1) From CF Where IDOperacion=" & IDOperacion & " "))
        txtCuenta.LimpiarSeleccion()

        txtCuenta.limpiarseleccion()
        cbxDebeHaber.cbx.Text = ""
        txtOrden.SetValue(0)
        txtDescripcion.txt.Clear()


    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro y sus dependientes. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim Debe As Boolean
        Dim Haber As Boolean

        If cbxDebeHaber.cbx.Text = "DEBE" Then
            Debe = True
            Haber = False
        End If

        If cbxDebeHaber.cbx.Text = "HABER" Then
            Debe = False
            Haber = True
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Cuenta Fija
        CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ID", txtID.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", cbxSucursal.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", cbxMoneda.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CuentaContable", txtCuenta.txtCodigo.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Debe", Debe, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Haber", Haber, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Orden", txtOrden.ObtenerValor, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoDescuento", cbxTipoDescuento.GetValue, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDProveedor", txtProveedor.Registro("ID"), ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpCFVentaDescuento", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If


    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
        txtCuenta.txtCodigo.Focus()

    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtCuenta.Focus()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
        'InicializarControles()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub dgvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.SelectionChanged
        ObtenerInformacion()
    End Sub

    Private Sub txtCuenta_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuenta.ItemSeleccionado
        cbxDebeHaber.cbx.Focus()
    End Sub

    Private Sub ocxCuentaFijaGasto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Inicializar()
    End Sub

    Private Sub cbxFiltroSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxFiltroSucursal.PropertyChanged
        Listar()
    End Sub

End Class

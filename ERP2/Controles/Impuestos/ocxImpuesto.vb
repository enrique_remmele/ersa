﻿Public Class ocxImpuesto

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS
    Public Event ImporteCambiado()

    'PROPIEDADES
    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private TotalRetencionIVAValue As Decimal
    Public Property TotalRetencionIVA() As Decimal
        Get
            Return TotalRetencionIVAValue
        End Get
        Set(ByVal value As Decimal)
            TotalRetencionIVAValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
            SetIDMoneda()
        End Set
    End Property

    Private DecimalesValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesValue = value
        End Set
    End Property

    'FUNCIONES
    Public Sub Inicializar()


        dg.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray

        'Cargar Los tipos de impuestos
        'Dim dtTemp As DataTable = CData.GetTable("VImpuesto")
        Dim dtTemp As DataTable = CSistema.ExecuteToDataTable("select * from VImpuesto")
        dg.Rows.Clear()
        For Each oRow As DataRow In dtTemp.Rows
            Dim rIndex As Integer = dg.Rows.Add()
            dg.Item(0, rIndex).Value = oRow("Descripcion").ToString
            dg.Item(1, rIndex).Value = 0
            dg.Item(2, rIndex).Value = 0
            dg.Item(3, rIndex).Value = 0
            dg.Item(4, rIndex).Value = 0
            dg.Item(5, rIndex).Value = oRow("ID").ToString
        Next

        Dim F As New Font(Me.Font.FontFamily, 14, FontStyle.Bold, GraphicsUnit.Pixel)
        txtTotal.txt.Font = F
        txtTotal.txt.BorderStyle = BorderStyle.None
        txtTotal.txt.BackColor = Color.WhiteSmoke

        txtTotalImpuesto.txt.Font = F
        txtTotalImpuesto.txt.BorderStyle = BorderStyle.None
        txtTotalImpuesto.txt.BackColor = Color.WhiteSmoke

        'Formato
        dg.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dg.Columns(1).DefaultCellStyle.SelectionBackColor = Color.Navy
        dg.Columns(1).DefaultCellStyle.SelectionForeColor = Color.White

        dg.Columns(0).ReadOnly = True
        dg.Columns(2).ReadOnly = True
        dg.Columns(3).ReadOnly = True
        dg.Columns(4).ReadOnly = True

        If IDMoneda = 0 Then
            IDMoneda = 1
        End If


    End Sub

    Public Sub SetIDMoneda()

        Try

            'Verificar si es que la moneda admite decimales
            Decimales = False
            Dim CantidadDecimal As Integer = 0

            'Dim dt As DataTable = CData.GetTable("VMoneda", " ID=" & IDMoneda)
            Dim dt As DataTable = CSistema.ExecuteToDataTable("select * from vMoneda where ID = " & IDMoneda)

            If dt.Rows.Count = 0 Then
                Exit Try
            End If

            Decimales = dt.Rows(0)("Decimales")

            If Decimales = True Then
                CantidadDecimal = 2
            End If


            dg.Columns(1).DefaultCellStyle.Format = "N" & CantidadDecimal
            dg.Columns(2).DefaultCellStyle.Format = "N" & CantidadDecimal
            dg.Columns(3).DefaultCellStyle.Format = "N" & CantidadDecimal
            dg.Columns(4).DefaultCellStyle.Format = "N" & CantidadDecimal

        Catch ex As Exception

        End Try

    End Sub

    Public Sub SetIDMoneda(vIDMoneda As Integer)

        IDMoneda = vIDMoneda
        SetIDMoneda()

    End Sub

    Public Sub EstablecerSoloLectura()

        dg.AllowUserToAddRows = False
        dg.AllowUserToDeleteRows = False
        dg.AllowUserToOrderColumns = False
        dg.AllowUserToResizeColumns = False

    End Sub

    Public Sub CargarValores1(Optional ByVal dt As DataTable = Nothing)

        If Not dt Is Nothing Then
            Me.dtImpuesto = dt.Copy
        End If

        dg.DataSource = dtImpuesto

        SumarTotales()

    End Sub

    Public Sub CargarValores(Optional ByVal dt As DataTable = Nothing)

        If Not dt Is Nothing Then
            Me.dtImpuesto = dt.Copy
        End If


        Dim TotalImpuesto As Decimal = 0
        Dim Total As Decimal = 0

        'Primero inicializamos los valores
        For Each dgv As DataGridViewRow In dg.Rows
            dgv.Cells("colTotal").Value = 0
            dgv.Cells("colImpuesto").Value = 0
            dgv.Cells("colDiscriminado").Value = 0
            dgv.Cells("colDescuento").Value = 0
        Next

        For Each oRow As DataRow In dtImpuesto.Rows
            Dim Impuesto As String = oRow("Impuesto").ToString

            Total = Total + CDec(oRow("Total").ToString)
            TotalImpuesto = TotalImpuesto + Math.Round(oRow("TotalImpuesto"))

            For Each dgv As DataGridViewRow In dg.Rows

                If Impuesto = CStr(dgv.Cells("colTipo").Value) Then
                    dgv.Cells("colTotal").Value = CSistema.FormatoMoneda(oRow("Total").ToString, Decimales)
                    dgv.Cells("colImpuesto").Value = CSistema.FormatoMoneda(oRow("TotalImpuesto").ToString, Decimales)
                    dgv.Cells("colDiscriminado").Value = CSistema.FormatoMoneda(oRow("TotalDiscriminado").ToString, Decimales)
                    dgv.Cells("colDescuento").Value = CSistema.FormatoMoneda(oRow("TotalDescuento").ToString, Decimales)
                End If
            Next
        Next

        SumarTotales()

    End Sub

    Public Sub CargarValores(ByVal Impuesto As String, ByVal Total As Decimal, ByVal TotalImpuesto As Decimal, ByVal TotalDescuento As Decimal, ByVal TotalDescriminado As Decimal)

        For Each dgv As DataGridViewRow In dg.Rows
            If Impuesto = CStr(dgv.Cells("colTipo").Value) Then
                dgv.Cells("colTotal").Value = CSistema.FormatoMoneda(dgv.Cells("colTotal").Value + Total, Decimales)
                dgv.Cells("colImpuesto").Value = CSistema.FormatoMoneda(dgv.Cells("colImpuesto").Value + TotalImpuesto, Decimales)
                dgv.Cells("colDiscriminado").Value = CSistema.FormatoMoneda(dgv.Cells("colDiscriminado").Value + TotalDescriminado, Decimales)
                dgv.Cells("colDescuento").Value = dgv.Cells("colDescuento").Value + TotalDescuento
                Exit For
            End If
        Next

        SumarTotales()

    End Sub

    Sub Reestablecer()

        SetIDMoneda()

        'Primero inicializamos los valores
        For Each dgv As DataGridViewRow In dg.Rows
            dgv.Cells("colTotal").Value = 0
            dgv.Cells("colImpuesto").Value = 0
            dgv.Cells("colDiscriminado").Value = 0
            dgv.Cells("colDescuento").Value = 0
        Next

        SumarTotales()

        Try
            dg.CurrentCell = dg.Rows(0).Cells(1)
        Catch ex As Exception

        End Try

    End Sub

    Public Sub CargarValores(ByVal IDTransaccion As Integer)

        Dim dtTemp As DataTable
        dtTemp = CSistema.ExecuteToDataTable("Select * From VDetalleImpuesto Where IDTransaccion=" & IDTransaccion).Copy
        CargarValores(dtTemp)

    End Sub

    Public Sub Generar(Optional ByVal IDTransaccion As Integer = 0)

        If dtImpuesto Is Nothing Then
            dtImpuesto = New DataTable
            dtImpuesto.Columns.Add("IDTransaccion")
            dtImpuesto.Columns.Add("Impuesto")
            dtImpuesto.Columns.Add("IDImpuesto")
            dtImpuesto.Columns.Add("Total")
            dtImpuesto.Columns.Add("TotalImpuesto")
            dtImpuesto.Columns.Add("TotalDiscriminado")
            dtImpuesto.Columns.Add("TotalDescuento")
        Else
            dtImpuesto.Rows.Clear()
        End If

        For Each dgv As DataGridViewRow In dg.Rows

            Dim NewRow As DataRow = dtImpuesto.NewRow
            NewRow("IDTransaccion") = IDTransaccion
            NewRow("Impuesto") = dgv.Cells("colImpuesto").Value
            NewRow("IDImpuesto") = dgv.Cells("colIDImpuesto").Value

            'No convierto a decimal, porque el datarow ya lo toma como decimal!!!
            NewRow("Total") = CSistema.FormatoNumero(dgv.Cells("colTotal").Value, Decimales)
            NewRow("TotalImpuesto") = CSistema.FormatoNumero(dgv.Cells("colImpuesto").Value, Decimales)
            NewRow("TotalDiscriminado") = CSistema.FormatoNumero(dgv.Cells("colDiscriminado").Value, Decimales)
            NewRow("TotalDescuento") = CSistema.FormatoNumero(dgv.Cells("colDescuento").Value, Decimales)

            dtImpuesto.Rows.Add(NewRow)

        Next


    End Sub

    Sub SumarTotales()

        txtTotalImpuesto.txt.Text = CSistema.FormatoMoneda(CSistema.gridSumColumn(dg, "colImpuesto"), Decimales)
        txtTotal.txt.Text = CSistema.FormatoMoneda(CSistema.gridSumColumn(dg, "colTotal"), Decimales)

        CalcularRetencion()
        RaiseEvent ImporteCambiado()

    End Sub

    Sub CalcularRetencion()

        TotalRetencionIVA = 0

        Dim Porcentaje As String = vgConfiguraciones("CompraPorcentajeRetencion").ToString
        Dim MinimoImporte As String = vgConfiguraciones("CompraImporteMinimoRetencion").ToString

        'Validar
        If IsNumeric(Porcentaje) = False Then
            Exit Sub
        End If

        If IsNumeric(MinimoImporte) = False Then
            Exit Sub
        End If

        'Si no supera, no calcular
        If txtTotal.ObtenerValor < CDec(MinimoImporte) Then
            TotalRetencionIVA = 0
            Exit Sub
        End If

        TotalRetencionIVA = CDec(txtTotalImpuesto.ObtenerValor) * (Porcentaje / 100)

    End Sub

    Sub Formato()

    End Sub

    Public Function Total() As Decimal

        Dim Valor As Decimal = 0
        Valor = CSistema.gridSumColumn(dg, "colTotal")
        Return Valor

    End Function

    Public Function TotalBD() As String

        Dim Valor As String = "0"

        Valor = CSistema.gridSumColumn(dg, "colTotal")
        Valor = CSistema.FormatoMonedaBaseDatos(Valor, IDMoneda)

        Return Valor


    End Function

    Public Function TotalImpuestoBD() As String

        Dim Valor As String = "0"

        Valor = CSistema.gridSumColumn(dg, "colImpuesto")
        Valor = CSistema.FormatoMonedaBaseDatos(Valor, IDMoneda)

        Return Valor

    End Function

    Public Function TotalDiscriminadoBD() As String

        Dim Valor As String = "0"

        Valor = CSistema.gridSumColumn(dg, "colDiscriminado")
        Valor = CSistema.FormatoMonedaBaseDatos(Valor, IDMoneda)

        Return Valor

    End Function

    Private Sub dg_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg.CellEndEdit

        If e.ColumnIndex = 1 Then

            'Validar que el total (colTotal) sea numerico, si no = 0
            If IsNumeric(dg.Rows(e.RowIndex).Cells(1).Value) = False Then
                dg.Rows(e.RowIndex).Cells(1).Value = 0
            End If

            'Calcular Impuesto, importe Discriminado y dejar descuento a 0
            'Para hayar el Impuesto usar CSistema.CalcularIVA()
            Dim IDImpuesto As Integer
            Dim Importe As Decimal
            Dim TotalDiscriminado As Decimal = 0
            Dim TotalImpuesto As Decimal = 0

            IDImpuesto = dg.Rows(e.RowIndex).Cells(5).Value
            Importe = dg.Rows(e.RowIndex).Cells(1).Value

            Dim Redondear As Boolean = True
            If Decimales = True Then
                Redondear = False
            End If

            CSistema.CalcularIVA(IDImpuesto, Importe, Decimales, Redondear, TotalDiscriminado, TotalImpuesto)
            dg.Rows(e.RowIndex).Cells(1).Value = CSistema.FormatoMoneda(Importe.ToString, Decimales)
            dg.Rows(e.RowIndex).Cells(2).Value = CSistema.FormatoMoneda(TotalImpuesto.ToString, Decimales)
            dg.Rows(e.RowIndex).Cells(3).Value = CSistema.FormatoMoneda(TotalDiscriminado.ToString, Decimales)

            'Sumar Totales, Total Impuestos y Total Comprobante
            SumarTotales()

        End If
        

    End Sub

    Private Sub ocxImpuesto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Inicializar()
    End Sub

    Private Sub dg_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dg.CellContentClick

    End Sub

    Private Sub dg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dg.GotFocus
        dg.CurrentCell = dg.CurrentRow.Cells(1)
    End Sub

    Private Sub dg_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dg.KeyUp

    End Sub

    Private Sub dg_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dg.KeyDown

        If e.KeyCode = Keys.Enter Then
            If dg.CurrentRow.Index = dg.Rows.Count - 1 Then
                Me.ParentForm.SelectNextControl(Me.ParentForm.ActiveControl, True, True, True, False)
            End If
        End If

        If e.KeyCode = Keys.Down Then
            If dg.CurrentRow.Index = dg.Rows.Count - 1 Then
                Me.ParentForm.SelectNextControl(Me.ParentForm.ActiveControl, True, True, True, False)
            End If
        End If

    End Sub

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificarClasificacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblSubLinea = New System.Windows.Forms.Label()
        Me.lblPresentacion = New System.Windows.Forms.Label()
        Me.lblMarca = New System.Windows.Forms.Label()
        Me.lblLinea = New System.Windows.Forms.Label()
        Me.lblTipoProducto = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.cbxSubLinea2 = New ERP.ocxCBX()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 134)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Sub-Linea 2:"
        '
        'lblSubLinea
        '
        Me.lblSubLinea.AutoSize = True
        Me.lblSubLinea.Location = New System.Drawing.Point(22, 107)
        Me.lblSubLinea.Name = "lblSubLinea"
        Me.lblSubLinea.Size = New System.Drawing.Size(58, 13)
        Me.lblSubLinea.TabIndex = 5
        Me.lblSubLinea.Text = "Sub-Linea:"
        '
        'lblPresentacion
        '
        Me.lblPresentacion.AutoSize = True
        Me.lblPresentacion.Location = New System.Drawing.Point(22, 188)
        Me.lblPresentacion.Name = "lblPresentacion"
        Me.lblPresentacion.Size = New System.Drawing.Size(72, 13)
        Me.lblPresentacion.TabIndex = 11
        Me.lblPresentacion.Text = "Presentacion:"
        '
        'lblMarca
        '
        Me.lblMarca.AutoSize = True
        Me.lblMarca.Location = New System.Drawing.Point(22, 161)
        Me.lblMarca.Name = "lblMarca"
        Me.lblMarca.Size = New System.Drawing.Size(40, 13)
        Me.lblMarca.TabIndex = 9
        Me.lblMarca.Text = "Marca:"
        '
        'lblLinea
        '
        Me.lblLinea.AutoSize = True
        Me.lblLinea.Location = New System.Drawing.Point(22, 80)
        Me.lblLinea.Name = "lblLinea"
        Me.lblLinea.Size = New System.Drawing.Size(36, 13)
        Me.lblLinea.TabIndex = 3
        Me.lblLinea.Text = "Linea:"
        '
        'lblTipoProducto
        '
        Me.lblTipoProducto.AutoSize = True
        Me.lblTipoProducto.Location = New System.Drawing.Point(22, 53)
        Me.lblTipoProducto.Name = "lblTipoProducto"
        Me.lblTipoProducto.Size = New System.Drawing.Size(92, 13)
        Me.lblTipoProducto.TabIndex = 1
        Me.lblTipoProducto.Text = "Tipo de Producto:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(287, 226)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 14
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(206, 226)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 13
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'txtPath
        '
        Me.txtPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPath.Location = New System.Drawing.Point(25, 21)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(337, 18)
        Me.txtPath.TabIndex = 0
        '
        'cbxSubLinea2
        '
        Me.cbxSubLinea2.CampoWhere = Nothing
        Me.cbxSubLinea2.DataDisplayMember = Nothing
        Me.cbxSubLinea2.DataFilter = Nothing
        Me.cbxSubLinea2.DataOrderBy = Nothing
        Me.cbxSubLinea2.DataSource = Nothing
        Me.cbxSubLinea2.DataValueMember = Nothing
        Me.cbxSubLinea2.Enabled = False
        Me.cbxSubLinea2.FormABM = Nothing
        Me.cbxSubLinea2.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea2.Location = New System.Drawing.Point(120, 130)
        Me.cbxSubLinea2.Name = "cbxSubLinea2"
        Me.cbxSubLinea2.SeleccionObligatoria = False
        Me.cbxSubLinea2.Size = New System.Drawing.Size(242, 21)
        Me.cbxSubLinea2.SoloLectura = False
        Me.cbxSubLinea2.TabIndex = 8
        Me.cbxSubLinea2.Texto = ""
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = Nothing
        Me.cbxSubLinea.DataDisplayMember = Nothing
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = Nothing
        Me.cbxSubLinea.DataSource = Nothing
        Me.cbxSubLinea.DataValueMember = Nothing
        Me.cbxSubLinea.Enabled = False
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea.Location = New System.Drawing.Point(120, 103)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(242, 21)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 6
        Me.cbxSubLinea.Texto = ""
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = Nothing
        Me.cbxPresentacion.DataDisplayMember = Nothing
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = Nothing
        Me.cbxPresentacion.DataSource = Nothing
        Me.cbxPresentacion.DataValueMember = Nothing
        Me.cbxPresentacion.Enabled = False
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPresentacion.Location = New System.Drawing.Point(120, 184)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(242, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 12
        Me.cbxPresentacion.Texto = ""
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = Nothing
        Me.cbxMarca.DataDisplayMember = Nothing
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = Nothing
        Me.cbxMarca.DataSource = Nothing
        Me.cbxMarca.DataValueMember = Nothing
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxMarca.Location = New System.Drawing.Point(120, 157)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(242, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 10
        Me.cbxMarca.Texto = ""
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = Nothing
        Me.cbxLinea.DataDisplayMember = Nothing
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = Nothing
        Me.cbxLinea.DataSource = Nothing
        Me.cbxLinea.DataValueMember = Nothing
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxLinea.Location = New System.Drawing.Point(120, 76)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(242, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 4
        Me.cbxLinea.Texto = ""
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = Nothing
        Me.cbxTipoProducto.DataDisplayMember = Nothing
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = Nothing
        Me.cbxTipoProducto.DataSource = Nothing
        Me.cbxTipoProducto.DataValueMember = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxTipoProducto.Location = New System.Drawing.Point(120, 49)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(242, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 2
        Me.cbxTipoProducto.Texto = ""
        '
        'frmModificarClasificacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(378, 262)
        Me.Controls.Add(Me.txtPath)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.cbxSubLinea2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbxSubLinea)
        Me.Controls.Add(Me.lblSubLinea)
        Me.Controls.Add(Me.cbxPresentacion)
        Me.Controls.Add(Me.lblPresentacion)
        Me.Controls.Add(Me.cbxMarca)
        Me.Controls.Add(Me.lblMarca)
        Me.Controls.Add(Me.cbxLinea)
        Me.Controls.Add(Me.lblLinea)
        Me.Controls.Add(Me.cbxTipoProducto)
        Me.Controls.Add(Me.lblTipoProducto)
        Me.Name = "frmModificarClasificacion"
        Me.Text = "frmModificarClasificacion"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbxSubLinea2 As ERP.ocxCBX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbxSubLinea As ERP.ocxCBX
    Friend WithEvents lblSubLinea As System.Windows.Forms.Label
    Friend WithEvents cbxPresentacion As ERP.ocxCBX
    Friend WithEvents lblPresentacion As System.Windows.Forms.Label
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents lblMarca As System.Windows.Forms.Label
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents lblLinea As System.Windows.Forms.Label
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents lblTipoProducto As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
End Class

﻿Public Class frmExcepciones

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim vIDClienteEliminar As Integer = 0

    'EVENTOS

    'PROPIEDADES
    Private IDProductoValue As String
    Public Property IDProducto() As String
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As String)
            IDProductoValue = value
        End Set
    End Property

    Private IDListaPrecioValue As String
    Public Property IDListaPrecio() As String
        Get
            Return IDListaPrecioValue
        End Get
        Set(ByVal value As String)
            IDListaPrecioValue = value
        End Set
    End Property

    Private PrecioValue As Double
    Public Property Precio() As Double
        Get
            Return PrecioValue
        End Get
        Set(ByVal value As Double)
            PrecioValue = value
        End Set
    End Property

    'Variables
    Dim dt As DataTable
    Dim oRowProducto As DataRow
    Dim oRowListaPrecio As DataRow
    Dim ctr() As Control
    Dim vNuevo As Boolean


    Sub Inicializar()

        'Controles
        Dim dtCliente As DataTable = CData.GetTable("VCliente", "IDListaPrecio=" & IDListaPrecio).Copy

        txtCliente.Conectar(dtCliente)
        txtDesde.PermitirNulo = True
        txtHasta.PermitirNulo = True

        'Funcioines
        CargarInformacion()

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)

        Listar()

    End Sub

    Sub CargarInformacion()

        'Contoles
        ReDim ctr(-1)
        CSistema.CargaControl(ctr, txtCliente)
        CSistema.CargaControl(ctr, txtDescuento)
        CSistema.CargaControl(ctr, cbxTipoDescuento)
        CSistema.CargaControl(ctr, txtDesde)
        CSistema.CargaControl(ctr, txtHasta)
        CSistema.CargaControl(ctr, txtMontoDescuento)
        CSistema.CargaControl(ctr, txtCantidadLimite)

        CSistema.SqlToComboBox(cbxTipoDescuento.cbx, CData.GetTable("VTipoDescuento", "ID>0"), "ID", "Codigo")

        'Registro de Producto
        oRowProducto = CData.GetTable("VProducto", " ID = " & IDProducto)(0)
        oRowListaPrecio = CData.GetTable("VListaPrecio", " ID = " & IDListaPrecio)(0)
        txtPrecioLista.txt.Text = Precio

        If oRowProducto Is Nothing Then
            Exit Sub
        End If

        If oRowListaPrecio Is Nothing Then
            Exit Sub
        End If

        txtIDProducto.SetValue(IDProducto)
        txtProducto.txt.Text = oRowProducto("Descripcion").ToString
        txtReferencia.txt.Text = oRowProducto("Ref").ToString

        txtIDListaPrecio.SetValue(IDListaPrecio)
        txtListaPrecio.txt.Text = oRowListaPrecio("Descripcion").ToString


    End Sub

    Sub Seleccionar()

        txtCliente.Clear()
        txtDescuento.SetValue(0)
        cbxTipoDescuento.SelectedValue(0)
        txtDesde.txt.Clear()
        txtHasta.txt.Clear()

        If lvExcepciones.SelectedItems.Count = 0 Then
            Exit Sub
        End If


        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)

        For Each item As ListViewItem In lvExcepciones.SelectedItems
            vIDClienteEliminar = item.Text
            txtCliente.SetValue(item.Text)
            cbxTipoDescuento.cbx.Text = item.SubItems(3).Text
            txtDescuento.SetValue(item.SubItems(4).Text.Replace("%", "").Trim)
            txtDesde.SetValue(item.SubItems(5).Text)
            txtHasta.SetValue(item.SubItems(6).Text)
            txtMontoDescuento.SetValue(item.SubItems(7).Text)
            txtCantidadLimite.SetValue(item.SubItems(8).Text)
            txtCantidadLimiteSaldo.SetValue(item.SubItems(9).Text)
            txtCantidadLimiteKG.SetValue(item.SubItems(10).Text)
            txtCantidadLimiteSaldoKG.SetValue(item.SubItems(11).Text)
        Next



    End Sub

    Sub Listar(Optional ByVal condicion As String = "")

        lvExcepciones.Items.Clear()

        dt = CSistema.ExecuteToDataTable("Select IDCliente, Referencia, Cliente, Moneda, TipoDescuentoCodigo, Descuento, Porcentaje, FechaDesde, FechaHasta, CantidadLimite, CantidadLimiteSaldo, CantidadLimiteKG, CantidadLimiteSaldoKG From VProductoListaPrecioExcepciones Where IDListaPrecio=" & IDListaPrecio & "  And IDProducto=" & IDProducto & condicion & " ").Copy

        For Each oRow As DataRow In dt.Rows
            Dim item As New ListViewItem(oRow("IDCliente").ToString)
            item.SubItems.Add(oRow("Referencia").ToString)
            item.SubItems.Add(oRow("Cliente").ToString)
            item.SubItems.Add(oRow("TipoDescuentoCodigo").ToString)
            item.SubItems.Add(oRow("Porcentaje").ToString & "%")
            item.SubItems.Add(oRow("FechaDesde").ToString)
            item.SubItems.Add(oRow("FechaHasta").ToString)
            item.SubItems.Add(oRow("Descuento").ToString)
            item.SubItems.Add(oRow("CantidadLimite"))
            item.SubItems.Add(oRow("CantidadLimiteSaldo"))
            item.SubItems.Add(oRow("CantidadLimiteKG"))
            item.SubItems.Add(oRow("CantidadLimiteSaldoKG"))

            lvExcepciones.Items.Add(item)

        Next

    End Sub

    Sub Guardar(ByVal operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar
        If (operacion = ERP.CSistema.NUMOperacionesABM.INS Or operacion = ERP.CSistema.NUMOperacionesABM.UPD) Then
            'Seleccione de cliente
            If txtCliente.Seleccionado = False Then
                CSistema.MostrarError("Seleccione correctamente el cliente!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If

            'Seleccione de Tipo de Descuento
            If cbxTipoDescuento.GetValue = 0 And cbxTipoDescuento.cbx.Text <> "TPR" Then
                CSistema.MostrarError("Seleccione correctamente el tipo de descuento!", ctrError, btnGuardar, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If
        End If
        If operacion <> ERP.CSistema.NUMOperacionesABM.DEL Then
            'Validar que el descuento no sobrepase el limite
            If CSistema.ExecuteScalar("Select dbo.FValidarDescuento(" & vgIDUsuario & "," & IDListaPrecio & "," & CSistema.FormatoNumeroBaseDatos(txtDescuento.txt.Text, True) & ")") = False Then
                MessageBox.Show("El porcentaje excede el permitido para Usuario-Lista de Precio!", "Atencion!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        End If
        

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDListaPrecio", IDListaPrecio, ParameterDirection.Input)
        If operacion <> ERP.CSistema.NUMOperacionesABM.DEL Then
            CSistema.SetSQLParameter(param, "@IDCliente", txtCliente.Registro("ID"), ParameterDirection.Input)
        Else
            CSistema.SetSQLParameter(param, "@IDCliente", vIDClienteEliminar, ParameterDirection.Input)
        End If
        CSistema.SetSQLParameter(param, "@IDTipoDescuento", cbxTipoDescuento.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ImporteDescuento", CSistema.FormatoNumeroBaseDatos(txtMontoDescuento.txt.Text, True), ParameterDirection.Input)
        If IsDate(txtDesde.txt.Text) = True Then
            CSistema.SetSQLParameter(param, "@Desde", CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False), ParameterDirection.Input)
        End If

        If IsDate(txtHasta.txt.Text) = True Then
            CSistema.SetSQLParameter(param, "@Hasta", CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False), ParameterDirection.Input)
        End If

        CSistema.SetSQLParameter(param, "@Cantidad", CSistema.FormatoNumeroBaseDatos(txtCantidadLimite.ObtenerValor, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", operacion.ToString, ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpProductoListaPrecioExcepciones", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Sub Solicitar()

        Dim ListaPrecio As String
        ListaPrecio = CSistema.ExecuteScalar("Select Descripcion from Listaprecio where id = " & IDListaPrecio)


        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer
        'Si fue seleccionado una sucursal del cliente
        CSistema.SetSQLParameter(param, "@Producto", txtReferencia.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ListaPrecio", ListaPrecio, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cliente", txtCliente.Registro("Referencia"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoDescuento", cbxTipoDescuento.Texto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descuento", CSistema.FormatoNumeroBaseDatos(txtMontoDescuento.txt.Text, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Moneda", "GS", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Desde", CSistema.FormatoFechaDesdeHastaBaseDatos(txtDesde.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Hasta", CSistema.FormatoFechaDesdeHastaBaseDatos(txtHasta.GetValue, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CantidadLimite", CSistema.FormatoNumeroBaseDatos(txtCantidadLimite.ObtenerValor, True), ParameterDirection.Input)

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpSolicitudExcepcion", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)
            ctrError.Clear()
            Listar()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub



    Sub Nuevo()

        vNuevo = True

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)
        txtCliente.Focus()
        txtCliente.txtID.Focus()
        txtCliente.txtID.txt.SelectAll()
    End Sub

    Sub Editar()

        vNuevo = False
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)
        txtCliente.Focus()
        txtCliente.txtID.Focus()
        txtCliente.txtID.txt.SelectAll()

    End Sub

    Sub Cancelar()


        ctrError.Clear()
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)

    End Sub

    Sub Eliminar()

        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, ctr)

        Guardar(ERP.CSistema.NUMOperacionesABM.DEL)

    End Sub

    Sub ComportamientoDescuento()

        txtDesde.Enabled = False
        txtHasta.Enabled = False
        txtDesde.txt.Clear()
        txtHasta.txt.Clear()

        If cbxTipoDescuento.GetValue = 0 Then
            Exit Sub
        End If

        Dim dt As DataTable = CData.GetTable("VTipoDescuento", " ID = " & cbxTipoDescuento.GetValue).Copy
        If dt Is Nothing Then
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        txtDesde.Enabled = CBool(oRow("RangoFecha"))
        txtHasta.Enabled = CBool(oRow("RangoFecha"))

    End Sub

    Private Sub lvExcepciones_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvExcepciones.SelectedIndexChanged
        Seleccionar()
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Nuevo()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Editar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            If vgConfiguraciones("CargarExcepcionSinAutorizacion") = True Then
                Guardar(ERP.CSistema.NUMOperacionesABM.INS)
            Else
                Solicitar()
            End If

        Else
                'Guardar(ERP.CSistema.NUMOperacionesABM.UPD)
            End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Eliminar()
    End Sub

    Private Sub frmExcepciones_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmExcepciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub txtCliente_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.ItemSeleccionado
        cbxTipoDescuento.cbx.Focus()
    End Sub

    Private Sub cbxTipoDescuento_Leave(sender As Object, e As System.EventArgs) Handles cbxTipoDescuento.Leave
        ComportamientoDescuento()
    End Sub

    Private Sub cbxTipoDescuento_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoDescuento.PropertyChanged
        ComportamientoDescuento()
    End Sub

    Private Sub txtDescuento_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescuento.Leave

        'Dim dt As DataTable = CData.GetTable("VTipoDescuento", " ID = " & cbxTipoDescuento.GetValue).Copy
        'If dt Is Nothing Then
        '    Exit Sub
        'End If

        If dt.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = dt.Rows(0)

        'If CDec(txtDescuento.txt.Text) > CDec(oRow("Limite")) Then
        Try
            If CDec(txtDescuento.txt.Text) > (100) Then
                'CSistema.MostrarError("El limite maximo para este tipo de descuento descuento es de " & oRow("Limite"), ctrError, txtDescuento, tsslEstado, ErrorIconAlignment.TopRight)
                CSistema.MostrarError("El limite maximo para este tipo de descuento descuento es de 100", ctrError, txtDescuento, tsslEstado, ErrorIconAlignment.TopRight)
                txtDescuento.txt.SelectAll()
                txtDescuento.txt.Focus()
                txtDescuento.SetValue(oRow("Limite"))
            End If
        Catch ex As Exception
            txtDescuento.txt.Text = "0"
            CSistema.MostrarError("El limite maximo para este tipo de descuento descuento es de 100", ctrError, txtDescuento, tsslEstado, ErrorIconAlignment.TopRight)
            txtDescuento.txt.SelectAll()
            txtDescuento.txt.Focus()
            txtDescuento.SetValue(oRow("Limite"))

        End Try


    End Sub


    Private Sub txtMontoDescuento_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtMontoDescuento.TeclaPrecionada
        'If txtMontoDescuento.txt.Text > txtPrecioLista.txt.Text Then
        '    CSistema.MostrarError("El precio no puede ser mayor al precio Fijo", ctrError, txtMontoDescuento, tsslEstado, ErrorIconAlignment.TopRight)
        '    txtMontoDescuento.txt.Text = "0"
        'End If

        'If CSistema.FormatoNumeroBaseDatos(txtDescuento.Text, 6) < 0 Then
        '    CSistema.MostrarError("El descuento no puede ser menor a Cero", ctrError, txtMontoDescuento, tsslEstado, ErrorIconAlignment.TopRight)
        '    txtMontoDescuento.txt.Text = "0"
        '    txtDescuento.txt.Text = "0"
        'End If

        If txtMontoDescuento.txt.Text <> "" Then
            'txtDescuento.txt.Text = (100 - (CSistema.FormatoNumeroBaseDatos(txtMontoDescuento.txt.Text, 6) * 100) / (CSistema.FormatoNumeroBaseDatos(txtPrecioLista.txt.Text, 0)))
            txtDescuento.txt.Text = ((CSistema.FormatoNumeroBaseDatos(txtMontoDescuento.txt.Text, 6) * 100) / (CSistema.FormatoNumeroBaseDatos(txtPrecioLista.txt.Text, 0)))
        End If
    End Sub

    Private Sub cbxTipoDescuento_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxTipoDescuento.TeclaPrecionada
        ComportamientoDescuento()
    End Sub

    Private Sub txtClienteBuscar_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtClienteBuscar.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            If txtClienteBuscar.txt.Text <> "" Then
                Listar(" and Cliente like '%" & txtClienteBuscar.txt.Text & "%' ")
            Else
                Listar()
            End If
        End If
    End Sub

    Private Sub txtCantidadLimite_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtCantidadLimite.TeclaPrecionada
        txtCantidadLimiteKG.Texto = txtCantidadLimite.ObtenerValor * oRowProducto("peso")
    End Sub

    Private Sub txtCantidadLimiteSaldo_VisibleChanged(sender As Object, e As EventArgs)

    End Sub
End Class
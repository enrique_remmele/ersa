﻿Public Class ocxProductoClasificacion

    'CLASES
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    'EVENTOS

    'VARIABLES
    Dim dt As New DataTable

    'FUNCIONES
    Sub Inicializar()

        'Controles

        'Propiedades

        'Otros

        'Funciones
        CargarInformacion()
        Listar()

        'Foco

    End Sub

    Sub CargarInformacion()


    End Sub

    Sub GuardarInformacion()

    End Sub

    Sub Listar()

        'Limpiamos todo
        tv.Nodes.Clear()

        'Crear el primer nodo
        Dim Nodo1 As TreeNode = New TreeNode()
        Nodo1.Name = "CLASIFICACION"
        Nodo1.Text = "CLASIFICACION             "
        Nodo1.NodeFont = New Font(Me.Font.Name, Me.Font.SizeInPoints, FontStyle.Bold, GraphicsUnit.Point)

        tv.Nodes.Add(Nodo1)

        'Cargamos 
        dt = CSistema.ExecuteToDataTable("Select * From ClasificacionProducto Order By Nivel, Numero")

        Listar(0, Nothing)

    End Sub

    Sub Nuevo()

        Dim frm As New frmAdministrarProductoClasificacion
        frm.Nuevo = True
        frm.btnEliminar.Enabled = False
        frm.ID = 0

        frm.IDPlanCuenta = tv.Nodes(0).Name
        frm.PlanCuenta = tv.Nodes(0).Text

        Dim Categoria As Integer
        Dim Tipo As String
        Categoria = tv.SelectedNode.Level + 1
        Tipo = "IMPUTABLE"


        frm.Categoria = Categoria
        frm.Tipo = Tipo

        If tv.SelectedNode.Level > 0 Then

            frm.IDPadre = tv.SelectedNode.Name
            frm.Padre = tv.SelectedNode.Text
            frm.CodigoPadre = dt.Select(" ID = " & tv.SelectedNode.Name)(0)("Codigo").ToString

        Else
            frm.IDPadre = 0
            frm.Padre = "---"
        End If

        frm.ShowDialog(Me)

        If frm.Procesado = True Then
            AgregarNodo(frm.ID)
        End If

    End Sub

    Sub Editar()

        If tv.SelectedNode.Level = 0 Then
            Exit Sub
        End If

        Dim frm As New frmAdministrarProductoClasificacion
        frm.Nuevo = False
        frm.btnEliminar.Enabled = False
        frm.ID = tv.SelectedNode.Name

        'Plan de Cuentas
        frm.IDPlanCuenta = tv.Nodes(0).Name
        frm.PlanCuenta = tv.Nodes(0).Text

        'Categorias y Tipos
        Dim Categoria As Integer
        Dim Tipo As String
        Categoria = tv.SelectedNode.Level + 1
        If tv.SelectedNode.Nodes.Count > 0 Then
            Tipo = "TOTALIZADOR"
        Else
            Tipo = "IMPUTABLE"
        End If

        frm.Categoria = Categoria
        frm.Tipo = Tipo

        frm.IDPadre = tv.SelectedNode.Parent.Name
        frm.Padre = tv.SelectedNode.Parent.Text

        frm.ShowDialog(Me)

        If frm.Procesado = True Then
            ModificarNodo(tv.SelectedNode.Name)
        End If

    End Sub

    Sub Eliminar()

        If tv.SelectedNode.Level = 0 Then
            Exit Sub
        End If

        Dim frm As New frmAdministrarProductoClasificacion
        frm.Nuevo = False
        frm.btnGuardar.Enabled = False
        frm.ID = tv.SelectedNode.Name

        'Plan de Cuentas
        frm.IDPlanCuenta = tv.Nodes(0).Name
        frm.PlanCuenta = tv.Nodes(0).Text

        'Categorias y Tipos
        Dim Categoria As Integer
        Dim Tipo As String
        Categoria = tv.SelectedNode.Level + 1
        If tv.SelectedNode.Nodes.Count > 0 Then
            Tipo = "TOTALIZADOR"
        Else
            Tipo = "IMPUTABLE"
        End If

        frm.Categoria = Categoria
        frm.Tipo = Tipo

        frm.IDPadre = tv.SelectedNode.Parent.Name
        frm.Padre = tv.SelectedNode.Parent.Text

        frm.ShowDialog(Me)

        If frm.Procesado = True Then
            tv.Nodes.Remove(tv.SelectedNode)

            If tv.SelectedNode.Nodes.Count = 0 Then
                For Each oRow As DataRow In dt.Select("ID=" & tv.SelectedNode.Name)
                    oRow("Tipo") = "IMPUTABLE"
                    tv.SelectedNode.ImageIndex = 2
                    tv.SelectedNode.SelectedImageIndex = 2
                Next
            End If

        End If


    End Sub

    Sub ModificarNodo(ByVal ID As Integer)

        Dim newRow As DataRow = CSistema.ExecuteToDataTable("Select * From VCuentaContable Where ID=" & ID & " ")(0)

        For Each oRow As DataRow In dt.Select("ID=" & ID)
            For i As Integer = 0 To dt.Columns.Count - 1
                oRow(i) = newRow(i).ToString
            Next
        Next

        tv.SelectedNode.Text = newRow("Cuenta").ToString

    End Sub

    Sub AgregarNodo(ByVal ID As Integer)

        Dim newRow As DataRow = CSistema.ExecuteToDataTable("Select * From VCuentaContable Where ID=" & ID & " ")(0)
        dt.ImportRow(newRow)

        Dim nodo As New TreeNode
        nodo.Name = ID
        nodo.Text = newRow("Cuenta").ToString
        nodo.SelectedImageIndex = 2
        nodo.ImageIndex = 2
        tv.SelectedNode.Nodes.Add(nodo)
        tv.SelectedNode = nodo

        'Poner al padre como totalizador y cambiar su icono
        For Each oRow As DataRow In dt.Select("ID=" & tv.SelectedNode.Parent.Name)
            oRow("Tipo") = "TOTALIZADOR"
        Next

        tv.SelectedNode.Parent.ImageIndex = 0
        tv.SelectedNode.Parent.SelectedImageIndex = 0

    End Sub

    Sub SeleccionarUltimoItemSeleccionado()

        Dim Index As Integer = tv.SelectedNode.Name


    End Sub

    Private Sub Listar(ByVal indicePadre As Integer, ByVal nodePadre As TreeNode)

        Dim dataViewHijos As DataView

        ' Crear un DataView con los Nodos que dependen del Nodo padre pasado como parámetro.
        dataViewHijos = New DataView(dt)

        dataViewHijos.RowFilter = dt.Columns("IDPadre").ColumnName + " = " + indicePadre.ToString()

        If dataViewHijos.Count = 0 Then
            If nodePadre Is Nothing Then

            Else
                nodePadre.ImageIndex = 2
                nodePadre.SelectedImageIndex = 2
            End If

        End If

        ' Agregar al TreeView los nodos Hijos que se han obtenido en el DataView.
        For Each dataRowCurrent As DataRowView In dataViewHijos

            Dim nuevoNodo As New TreeNode
            nuevoNodo.Text = dataRowCurrent("Cuenta").ToString().Trim()
            nuevoNodo.Name = dataRowCurrent("ID").ToString().Trim()
            nuevoNodo.ImageIndex = 0
            nuevoNodo.SelectedImageIndex = 0

            ' si el parámetro nodoPadre es nulo es porque es la primera llamada, son los Nodos
            ' del primer nivel que no dependen de otro nodo.
            If nodePadre Is Nothing Then
                'tvCuentas.Nodes.Add(nuevoNodo)
                tv.Nodes(0).Nodes.Add(nuevoNodo)
            Else
                ' se añade el nuevo nodo al nodo padre.
                nodePadre.Nodes.Add(nuevoNodo)
            End If

            ' Llamada recurrente al mismo método para agregar los Hijos del Nodo recién agregado.
            Listar(Int32.Parse(dataRowCurrent("ID").ToString()), nuevoNodo)

        Next dataRowCurrent

    End Sub

    Sub SeleccionarHaciaAbajo(ByVal valor As Boolean, Optional ByVal nodo As TreeNode = Nothing)

        Dim nodo1 As New TreeNode

        If nodo Is Nothing Then
            nodo = tv.SelectedNode
        End If

        For Each nodo2 As TreeNode In nodo1.Nodes
            nodo2.Checked = True
            If nodo2.GetNodeCount(False) > 0 Then
                SeleccionarHaciaAbajo(valor, nodo2)
            End If
        Next

    End Sub

    Private Sub tv_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tv.AfterCheck
        SeleccionarHaciaAbajo(e.Node.Checked)
    End Sub

    'Private Sub NuevoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoToolStripMenuItem.Click
    '    Nuevo()
    'End Sub

    'Private Sub EditarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditarToolStripMenuItem.Click
    '    Editar()
    'End Sub

    'Private Sub EliminarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripMenuItem.Click
    '    Eliminar()
    'End Sub

    'Private Sub ExpandirNodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExpandirNodoToolStripMenuItem.Click

    '    Try
    '        tv.SelectedNode.Expand()
    '    Catch ex As Exception

    '    End Try

    'End Sub

    'Private Sub ExpandirTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExpandirTodoToolStripMenuItem.Click
    '    tv.ExpandAll()
    'End Sub

    'Private Sub ContraerNodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContraerNodoToolStripMenuItem.Click
    '    tv.SelectedNode.Collapse()
    'End Sub

    'Private Sub ContraerTodoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContraerTodoToolStripMenuItem.Click
    '    tv.CollapseAll()
    'End Sub

    Private Sub tvCuentas_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tv.AfterCollapse

        Try
            If e.Node.IsExpanded = False Then
                e.Node.SelectedImageIndex = 0
                e.Node.ImageIndex = 0
            Else
                e.Node.SelectedImageIndex = 1
                e.Node.ImageIndex = 1
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub tvCuentas_AfterExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tv.AfterExpand
        Try
            If e.Node.IsExpanded = False Then
                e.Node.SelectedImageIndex = 0
                e.Node.ImageIndex = 0
            Else
                e.Node.SelectedImageIndex = 1
                e.Node.ImageIndex = 1
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub tvCuentas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tv.Click

    End Sub

    Private Sub tvCuentas_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tv.MouseDown

    End Sub

    Private Sub tvCuentas_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tv.NodeMouseClick

        If e.Button = Windows.Forms.MouseButtons.Right Then

            ' Referenciamos el control
            Dim tv As Windows.Forms.TreeView = DirectCast(sender, Windows.Forms.TreeView)

            ' Seleccionamos el nodo
            tv.SelectedNode = e.Node

        End If

    End Sub


End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExcepciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblProducto = New System.Windows.Forms.Label()
        Me.lblListaPrecio = New System.Windows.Forms.Label()
        Me.lvExcepciones = New System.Windows.Forms.ListView()
        Me.colIDCliente = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colReferencia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colCliente = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colTipoDescuento = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDescuento = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colDesde = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHasta = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colMontoDescuento = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colCantidadLimite = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colCantidadLimitesSaldo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblHasta = New System.Windows.Forms.Label()
        Me.lblDesde = New System.Windows.Forms.Label()
        Me.lblDescuento = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtClienteBuscar = New ERP.ocxTXTString()
        Me.txtProducto = New ERP.ocxTXTString()
        Me.txtIDProducto = New ERP.ocxTXTNumeric()
        Me.txtReferencia = New ERP.ocxTXTString()
        Me.txtListaPrecio = New ERP.ocxTXTString()
        Me.txtIDListaPrecio = New ERP.ocxTXTNumeric()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCantidadLimiteSaldoKG = New ERP.ocxTXTNumeric()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCantidadLimiteKG = New ERP.ocxTXTNumeric()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCantidadLimiteSaldo = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCantidadLimite = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMontoDescuento = New ERP.ocxTXTNumeric()
        Me.lblPrecioDeLista = New System.Windows.Forms.Label()
        Me.txtPrecioLista = New ERP.ocxTXTNumeric()
        Me.lblTipoDescuento = New System.Windows.Forms.Label()
        Me.cbxTipoDescuento = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.txtDescuento = New ERP.ocxTXTNumeric()
        Me.txtDesde = New ERP.ocxTXTDate()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblProducto
        '
        Me.lblProducto.AutoSize = True
        Me.lblProducto.Location = New System.Drawing.Point(9, 11)
        Me.lblProducto.Name = "lblProducto"
        Me.lblProducto.Size = New System.Drawing.Size(53, 13)
        Me.lblProducto.TabIndex = 0
        Me.lblProducto.Text = "Producto:"
        '
        'lblListaPrecio
        '
        Me.lblListaPrecio.AutoSize = True
        Me.lblListaPrecio.Location = New System.Drawing.Point(9, 39)
        Me.lblListaPrecio.Name = "lblListaPrecio"
        Me.lblListaPrecio.Size = New System.Drawing.Size(80, 13)
        Me.lblListaPrecio.TabIndex = 3
        Me.lblListaPrecio.Text = "Lista de Precio:"
        '
        'lvExcepciones
        '
        Me.lvExcepciones.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colIDCliente, Me.colReferencia, Me.colCliente, Me.colTipoDescuento, Me.colDescuento, Me.colDesde, Me.colHasta, Me.colMontoDescuento, Me.colCantidadLimite, Me.colCantidadLimitesSaldo})
        Me.lvExcepciones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvExcepciones.FullRowSelect = True
        Me.lvExcepciones.GridLines = True
        Me.lvExcepciones.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvExcepciones.HideSelection = False
        Me.lvExcepciones.Location = New System.Drawing.Point(3, 70)
        Me.lvExcepciones.MultiSelect = False
        Me.lvExcepciones.Name = "lvExcepciones"
        Me.lvExcepciones.Size = New System.Drawing.Size(880, 226)
        Me.lvExcepciones.TabIndex = 1
        Me.lvExcepciones.UseCompatibleStateImageBehavior = False
        Me.lvExcepciones.View = System.Windows.Forms.View.Details
        '
        'colIDCliente
        '
        Me.colIDCliente.Text = "ID"
        Me.colIDCliente.Width = 30
        '
        'colReferencia
        '
        Me.colReferencia.Text = "Referencia"
        Me.colReferencia.Width = 70
        '
        'colCliente
        '
        Me.colCliente.Text = "Cliente"
        Me.colCliente.Width = 280
        '
        'colTipoDescuento
        '
        Me.colTipoDescuento.Text = "Tipo"
        Me.colTipoDescuento.Width = 89
        '
        'colDescuento
        '
        Me.colDescuento.Text = "Desc."
        Me.colDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colDescuento.Width = 73
        '
        'colDesde
        '
        Me.colDesde.Text = "Desde"
        Me.colDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colDesde.Width = 72
        '
        'colHasta
        '
        Me.colHasta.Text = "Hasta"
        Me.colHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colHasta.Width = 80
        '
        'colMontoDescuento
        '
        Me.colMontoDescuento.Text = "Monto"
        '
        'colCantidadLimite
        '
        Me.colCantidadLimite.Text = "Cantidad"
        '
        'colCantidadLimitesSaldo
        '
        Me.colCantidadLimitesSaldo.Text = "Conteo"
        '
        'lblCliente
        '
        Me.lblCliente.AutoSize = True
        Me.lblCliente.Location = New System.Drawing.Point(3, 4)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(42, 13)
        Me.lblCliente.TabIndex = 0
        Me.lblCliente.Text = "Cliente:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 512)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(886, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'lblHasta
        '
        Me.lblHasta.AutoSize = True
        Me.lblHasta.Location = New System.Drawing.Point(768, 4)
        Me.lblHasta.Name = "lblHasta"
        Me.lblHasta.Size = New System.Drawing.Size(38, 13)
        Me.lblHasta.TabIndex = 8
        Me.lblHasta.Text = "Hasta:"
        Me.lblHasta.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(692, 4)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(41, 13)
        Me.lblDesde.TabIndex = 6
        Me.lblDesde.Text = "Desde:"
        Me.lblDesde.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblDescuento
        '
        Me.lblDescuento.AutoSize = True
        Me.lblDescuento.Location = New System.Drawing.Point(624, 4)
        Me.lblDescuento.Name = "lblDescuento"
        Me.lblDescuento.Size = New System.Drawing.Size(62, 13)
        Me.lblDescuento.TabIndex = 4
        Me.lblDescuento.Text = "Descuento:"
        Me.lblDescuento.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.lvExcepciones, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 213.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(886, 512)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtClienteBuscar)
        Me.Panel1.Controls.Add(Me.lblProducto)
        Me.Panel1.Controls.Add(Me.txtProducto)
        Me.Panel1.Controls.Add(Me.txtIDProducto)
        Me.Panel1.Controls.Add(Me.txtReferencia)
        Me.Panel1.Controls.Add(Me.lblListaPrecio)
        Me.Panel1.Controls.Add(Me.txtListaPrecio)
        Me.Panel1.Controls.Add(Me.txtIDListaPrecio)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(880, 61)
        Me.Panel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(563, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Buscar por Cliente:"
        '
        'txtClienteBuscar
        '
        Me.txtClienteBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClienteBuscar.Color = System.Drawing.Color.Empty
        Me.txtClienteBuscar.Indicaciones = Nothing
        Me.txtClienteBuscar.Location = New System.Drawing.Point(662, 36)
        Me.txtClienteBuscar.Multilinea = False
        Me.txtClienteBuscar.Name = "txtClienteBuscar"
        Me.txtClienteBuscar.Size = New System.Drawing.Size(180, 20)
        Me.txtClienteBuscar.SoloLectura = False
        Me.txtClienteBuscar.TabIndex = 15
        Me.txtClienteBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtClienteBuscar.Texto = ""
        '
        'txtProducto
        '
        Me.txtProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProducto.Color = System.Drawing.Color.Empty
        Me.txtProducto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProducto.Indicaciones = Nothing
        Me.txtProducto.Location = New System.Drawing.Point(189, 6)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Multilinea = False
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(369, 22)
        Me.txtProducto.SoloLectura = True
        Me.txtProducto.TabIndex = 2
        Me.txtProducto.TabStop = False
        Me.txtProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtProducto.Texto = ""
        '
        'txtIDProducto
        '
        Me.txtIDProducto.Color = System.Drawing.Color.Empty
        Me.txtIDProducto.Decimales = True
        Me.txtIDProducto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDProducto.Indicaciones = Nothing
        Me.txtIDProducto.Location = New System.Drawing.Point(106, 6)
        Me.txtIDProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIDProducto.Name = "txtIDProducto"
        Me.txtIDProducto.Size = New System.Drawing.Size(84, 22)
        Me.txtIDProducto.SoloLectura = True
        Me.txtIDProducto.TabIndex = 1
        Me.txtIDProducto.TabStop = False
        Me.txtIDProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIDProducto.Texto = "0"
        '
        'txtReferencia
        '
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferencia.Indicaciones = Nothing
        Me.txtReferencia.Location = New System.Drawing.Point(558, 6)
        Me.txtReferencia.Margin = New System.Windows.Forms.Padding(4)
        Me.txtReferencia.Multilinea = False
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(99, 22)
        Me.txtReferencia.SoloLectura = True
        Me.txtReferencia.TabIndex = 6
        Me.txtReferencia.TabStop = False
        Me.txtReferencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtReferencia.Texto = ""
        '
        'txtListaPrecio
        '
        Me.txtListaPrecio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtListaPrecio.Color = System.Drawing.Color.Empty
        Me.txtListaPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtListaPrecio.Indicaciones = Nothing
        Me.txtListaPrecio.Location = New System.Drawing.Point(189, 34)
        Me.txtListaPrecio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtListaPrecio.Multilinea = False
        Me.txtListaPrecio.Name = "txtListaPrecio"
        Me.txtListaPrecio.Size = New System.Drawing.Size(369, 22)
        Me.txtListaPrecio.SoloLectura = True
        Me.txtListaPrecio.TabIndex = 5
        Me.txtListaPrecio.TabStop = False
        Me.txtListaPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtListaPrecio.Texto = ""
        '
        'txtIDListaPrecio
        '
        Me.txtIDListaPrecio.Color = System.Drawing.Color.Empty
        Me.txtIDListaPrecio.Decimales = True
        Me.txtIDListaPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDListaPrecio.Indicaciones = Nothing
        Me.txtIDListaPrecio.Location = New System.Drawing.Point(106, 34)
        Me.txtIDListaPrecio.Margin = New System.Windows.Forms.Padding(4)
        Me.txtIDListaPrecio.Name = "txtIDListaPrecio"
        Me.txtIDListaPrecio.Size = New System.Drawing.Size(84, 22)
        Me.txtIDListaPrecio.SoloLectura = True
        Me.txtIDListaPrecio.TabIndex = 4
        Me.txtIDListaPrecio.TabStop = False
        Me.txtIDListaPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIDListaPrecio.Texto = "0"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.txtCantidadLimiteSaldoKG)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.txtCantidadLimiteKG)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.txtCantidadLimiteSaldo)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.txtCantidadLimite)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.txtMontoDescuento)
        Me.Panel2.Controls.Add(Me.lblPrecioDeLista)
        Me.Panel2.Controls.Add(Me.txtPrecioLista)
        Me.Panel2.Controls.Add(Me.lblTipoDescuento)
        Me.Panel2.Controls.Add(Me.cbxTipoDescuento)
        Me.Panel2.Controls.Add(Me.txtCliente)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnNuevo)
        Me.Panel2.Controls.Add(Me.btnEditar)
        Me.Panel2.Controls.Add(Me.lblCliente)
        Me.Panel2.Controls.Add(Me.btnSalir)
        Me.Panel2.Controls.Add(Me.lblHasta)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.txtHasta)
        Me.Panel2.Controls.Add(Me.btnEliminar)
        Me.Panel2.Controls.Add(Me.txtDescuento)
        Me.Panel2.Controls.Add(Me.lblDesde)
        Me.Panel2.Controls.Add(Me.lblDescuento)
        Me.Panel2.Controls.Add(Me.txtDesde)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 302)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(880, 207)
        Me.Panel2.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(704, 105)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Conteo Kg:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtCantidadLimiteSaldoKG
        '
        Me.txtCantidadLimiteSaldoKG.Color = System.Drawing.Color.Empty
        Me.txtCantidadLimiteSaldoKG.Decimales = True
        Me.txtCantidadLimiteSaldoKG.Indicaciones = Nothing
        Me.txtCantidadLimiteSaldoKG.Location = New System.Drawing.Point(768, 97)
        Me.txtCantidadLimiteSaldoKG.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadLimiteSaldoKG.Name = "txtCantidadLimiteSaldoKG"
        Me.txtCantidadLimiteSaldoKG.Size = New System.Drawing.Size(62, 21)
        Me.txtCantidadLimiteSaldoKG.SoloLectura = True
        Me.txtCantidadLimiteSaldoKG.TabIndex = 26
        Me.txtCantidadLimiteSaldoKG.TabStop = False
        Me.txtCantidadLimiteSaldoKG.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadLimiteSaldoKG.Texto = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(515, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Cantidad Limite(Kg):"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtCantidadLimiteKG
        '
        Me.txtCantidadLimiteKG.Color = System.Drawing.Color.Empty
        Me.txtCantidadLimiteKG.Decimales = True
        Me.txtCantidadLimiteKG.Indicaciones = Nothing
        Me.txtCantidadLimiteKG.Location = New System.Drawing.Point(624, 97)
        Me.txtCantidadLimiteKG.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadLimiteKG.Name = "txtCantidadLimiteKG"
        Me.txtCantidadLimiteKG.Size = New System.Drawing.Size(62, 21)
        Me.txtCantidadLimiteKG.SoloLectura = True
        Me.txtCantidadLimiteKG.TabIndex = 24
        Me.txtCantidadLimiteKG.TabStop = False
        Me.txtCantidadLimiteKG.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadLimiteKG.Texto = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(721, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Conteo:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtCantidadLimiteSaldo
        '
        Me.txtCantidadLimiteSaldo.Color = System.Drawing.Color.Empty
        Me.txtCantidadLimiteSaldo.Decimales = True
        Me.txtCantidadLimiteSaldo.Indicaciones = Nothing
        Me.txtCantidadLimiteSaldo.Location = New System.Drawing.Point(768, 74)
        Me.txtCantidadLimiteSaldo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadLimiteSaldo.Name = "txtCantidadLimiteSaldo"
        Me.txtCantidadLimiteSaldo.Size = New System.Drawing.Size(62, 21)
        Me.txtCantidadLimiteSaldo.SoloLectura = True
        Me.txtCantidadLimiteSaldo.TabIndex = 22
        Me.txtCantidadLimiteSaldo.TabStop = False
        Me.txtCantidadLimiteSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadLimiteSaldo.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(449, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(169, 13)
        Me.Label3.TabIndex = 21
        Me.Label3.Text = "Cantidad Limite(Unidades/Bolsas):"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtCantidadLimite
        '
        Me.txtCantidadLimite.Color = System.Drawing.Color.Empty
        Me.txtCantidadLimite.Decimales = True
        Me.txtCantidadLimite.Indicaciones = Nothing
        Me.txtCantidadLimite.Location = New System.Drawing.Point(624, 74)
        Me.txtCantidadLimite.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadLimite.Name = "txtCantidadLimite"
        Me.txtCantidadLimite.Size = New System.Drawing.Size(62, 21)
        Me.txtCantidadLimite.SoloLectura = True
        Me.txtCantidadLimite.TabIndex = 20
        Me.txtCantidadLimite.TabStop = False
        Me.txtCantidadLimite.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadLimite.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(508, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 13)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Precio de Descuento:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtMontoDescuento
        '
        Me.txtMontoDescuento.Color = System.Drawing.Color.Empty
        Me.txtMontoDescuento.Decimales = True
        Me.txtMontoDescuento.Indicaciones = Nothing
        Me.txtMontoDescuento.Location = New System.Drawing.Point(624, 45)
        Me.txtMontoDescuento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtMontoDescuento.Name = "txtMontoDescuento"
        Me.txtMontoDescuento.Size = New System.Drawing.Size(62, 21)
        Me.txtMontoDescuento.SoloLectura = True
        Me.txtMontoDescuento.TabIndex = 18
        Me.txtMontoDescuento.TabStop = False
        Me.txtMontoDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMontoDescuento.Texto = "0"
        '
        'lblPrecioDeLista
        '
        Me.lblPrecioDeLista.AutoSize = True
        Me.lblPrecioDeLista.Location = New System.Drawing.Point(322, 51)
        Me.lblPrecioDeLista.Name = "lblPrecioDeLista"
        Me.lblPrecioDeLista.Size = New System.Drawing.Size(80, 13)
        Me.lblPrecioDeLista.TabIndex = 17
        Me.lblPrecioDeLista.Text = "Precio de Lista:"
        Me.lblPrecioDeLista.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtPrecioLista
        '
        Me.txtPrecioLista.Color = System.Drawing.Color.Empty
        Me.txtPrecioLista.Decimales = True
        Me.txtPrecioLista.Indicaciones = Nothing
        Me.txtPrecioLista.Location = New System.Drawing.Point(407, 45)
        Me.txtPrecioLista.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPrecioLista.Name = "txtPrecioLista"
        Me.txtPrecioLista.Size = New System.Drawing.Size(62, 21)
        Me.txtPrecioLista.SoloLectura = True
        Me.txtPrecioLista.TabIndex = 16
        Me.txtPrecioLista.TabStop = False
        Me.txtPrecioLista.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioLista.Texto = "0"
        '
        'lblTipoDescuento
        '
        Me.lblTipoDescuento.AutoSize = True
        Me.lblTipoDescuento.Location = New System.Drawing.Point(526, 4)
        Me.lblTipoDescuento.Name = "lblTipoDescuento"
        Me.lblTipoDescuento.Size = New System.Drawing.Size(31, 13)
        Me.lblTipoDescuento.TabIndex = 2
        Me.lblTipoDescuento.Text = "Tipo:"
        Me.lblTipoDescuento.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'cbxTipoDescuento
        '
        Me.cbxTipoDescuento.CampoWhere = Nothing
        Me.cbxTipoDescuento.CargarUnaSolaVez = False
        Me.cbxTipoDescuento.DataDisplayMember = ""
        Me.cbxTipoDescuento.DataFilter = Nothing
        Me.cbxTipoDescuento.DataOrderBy = ""
        Me.cbxTipoDescuento.DataSource = ""
        Me.cbxTipoDescuento.DataValueMember = ""
        Me.cbxTipoDescuento.dtSeleccionado = Nothing
        Me.cbxTipoDescuento.FormABM = Nothing
        Me.cbxTipoDescuento.Indicaciones = Nothing
        Me.cbxTipoDescuento.Location = New System.Drawing.Point(527, 18)
        Me.cbxTipoDescuento.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoDescuento.Name = "cbxTipoDescuento"
        Me.cbxTipoDescuento.SeleccionMultiple = False
        Me.cbxTipoDescuento.SeleccionObligatoria = False
        Me.cbxTipoDescuento.Size = New System.Drawing.Size(91, 21)
        Me.cbxTipoDescuento.SoloLectura = True
        Me.cbxTipoDescuento.TabIndex = 3
        Me.cbxTipoDescuento.TabStop = False
        Me.cbxTipoDescuento.Texto = "TAC"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 85
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(3, 15)
        Me.txtCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(518, 24)
        Me.txtCliente.SoloLectura = True
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(249, 163)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 13
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(6, 163)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 10
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(87, 163)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 11
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        Me.btnEditar.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(761, 163)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 15
        Me.btnSalir.Text = "Cerrar"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(168, 163)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 12
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2012, 12, 6, 16, 15, 7, 562)
        Me.txtHasta.Location = New System.Drawing.Point(768, 18)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(76, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 9
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(330, 163)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 14
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'txtDescuento
        '
        Me.txtDescuento.Color = System.Drawing.Color.Empty
        Me.txtDescuento.Decimales = True
        Me.txtDescuento.Indicaciones = Nothing
        Me.txtDescuento.Location = New System.Drawing.Point(624, 18)
        Me.txtDescuento.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(62, 21)
        Me.txtDescuento.SoloLectura = True
        Me.txtDescuento.TabIndex = 5
        Me.txtDescuento.TabStop = False
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuento.Texto = "0"
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2012, 12, 6, 16, 15, 7, 562)
        Me.txtDesde.Location = New System.Drawing.Point(692, 18)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(76, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 7
        '
        'frmExcepciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(886, 534)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.KeyPreview = True
        Me.Name = "frmExcepciones"
        Me.Text = "frmExcepciones"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblProducto As System.Windows.Forms.Label
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents txtProducto As ERP.ocxTXTString
    Friend WithEvents txtIDProducto As ERP.ocxTXTNumeric
    Friend WithEvents txtReferencia As ERP.ocxTXTString
    Friend WithEvents txtIDListaPrecio As ERP.ocxTXTNumeric
    Friend WithEvents txtListaPrecio As ERP.ocxTXTString
    Friend WithEvents lblListaPrecio As System.Windows.Forms.Label
    Friend WithEvents lvExcepciones As System.Windows.Forms.ListView
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents colIDCliente As System.Windows.Forms.ColumnHeader
    Friend WithEvents colCliente As System.Windows.Forms.ColumnHeader
    Friend WithEvents colDescuento As System.Windows.Forms.ColumnHeader
    Friend WithEvents colDesde As System.Windows.Forms.ColumnHeader
    Friend WithEvents colHasta As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblHasta As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents lblDesde As System.Windows.Forms.Label
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents lblDescuento As System.Windows.Forms.Label
    Friend WithEvents txtDescuento As ERP.ocxTXTNumeric
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblTipoDescuento As System.Windows.Forms.Label
    Friend WithEvents cbxTipoDescuento As ERP.ocxCBX
    Friend WithEvents colTipoDescuento As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtPrecioLista As ERP.ocxTXTNumeric
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMontoDescuento As ERP.ocxTXTNumeric
    Friend WithEvents lblPrecioDeLista As System.Windows.Forms.Label
    Friend WithEvents colReferencia As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtClienteBuscar As ERP.ocxTXTString
    Friend WithEvents Label4 As Label
    Friend WithEvents txtCantidadLimiteSaldo As ocxTXTNumeric
    Friend WithEvents Label3 As Label
    Friend WithEvents txtCantidadLimite As ocxTXTNumeric
    Friend WithEvents colCantidadLimite As ColumnHeader
    Friend WithEvents colCantidadLimitesSaldo As ColumnHeader
    Friend WithEvents colMontoDescuento As ColumnHeader
    Friend WithEvents Label5 As Label
    Friend WithEvents txtCantidadLimiteSaldoKG As ocxTXTNumeric
    Friend WithEvents Label6 As Label
    Friend WithEvents txtCantidadLimiteKG As ocxTXTNumeric
End Class

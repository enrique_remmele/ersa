﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxComisionProducto
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgw = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblTPR = New System.Windows.Forms.Label()
        Me.txtPorcentajeEsteProducto = New ERP.ocxTXTNumeric()
        Me.lblPrecioUnitario = New System.Windows.Forms.Label()
        Me.txtPorcentajeBase = New ERP.ocxTXTNumeric()
        Me.lblListaPrecio = New System.Windows.Forms.Label()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 620.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.dgw, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel3, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(721, 377)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'dgw
        '
        Me.dgw.AllowUserToAddRows = False
        Me.dgw.AllowUserToDeleteRows = False
        Me.dgw.AllowUserToOrderColumns = True
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.dgw.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgw.BackgroundColor = System.Drawing.Color.White
        Me.dgw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgw.Location = New System.Drawing.Point(3, 49)
        Me.dgw.Name = "dgw"
        Me.dgw.ReadOnly = True
        Me.dgw.RowHeadersVisible = False
        Me.dgw.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgw.Size = New System.Drawing.Size(614, 301)
        Me.dgw.TabIndex = 14
        Me.dgw.Tag = "frmRemuneracion"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblTPR)
        Me.Panel1.Controls.Add(Me.txtPorcentajeEsteProducto)
        Me.Panel1.Controls.Add(Me.lblPrecioUnitario)
        Me.Panel1.Controls.Add(Me.txtPorcentajeBase)
        Me.Panel1.Controls.Add(Me.lblListaPrecio)
        Me.Panel1.Controls.Add(Me.cbxListaPrecio)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(614, 40)
        Me.Panel1.TabIndex = 0
        '
        'lblTPR
        '
        Me.lblTPR.AutoSize = True
        Me.lblTPR.Location = New System.Drawing.Point(356, 0)
        Me.lblTPR.Name = "lblTPR"
        Me.lblTPR.Size = New System.Drawing.Size(85, 13)
        Me.lblTPR.TabIndex = 6
        Me.lblTPR.Text = "% Este Producto"
        Me.lblTPR.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtPorcentajeEsteProducto
        '
        Me.txtPorcentajeEsteProducto.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeEsteProducto.Decimales = True
        Me.txtPorcentajeEsteProducto.Indicaciones = Nothing
        Me.txtPorcentajeEsteProducto.Location = New System.Drawing.Point(356, 16)
        Me.txtPorcentajeEsteProducto.Name = "txtPorcentajeEsteProducto"
        Me.txtPorcentajeEsteProducto.Size = New System.Drawing.Size(83, 21)
        Me.txtPorcentajeEsteProducto.SoloLectura = False
        Me.txtPorcentajeEsteProducto.TabIndex = 7
        Me.txtPorcentajeEsteProducto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeEsteProducto.Texto = "0"
        '
        'lblPrecioUnitario
        '
        Me.lblPrecioUnitario.AutoSize = True
        Me.lblPrecioUnitario.Location = New System.Drawing.Point(271, 0)
        Me.lblPrecioUnitario.Name = "lblPrecioUnitario"
        Me.lblPrecioUnitario.Size = New System.Drawing.Size(42, 13)
        Me.lblPrecioUnitario.TabIndex = 4
        Me.lblPrecioUnitario.Text = "% Base"
        Me.lblPrecioUnitario.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtPorcentajeBase
        '
        Me.txtPorcentajeBase.Color = System.Drawing.Color.Empty
        Me.txtPorcentajeBase.Decimales = True
        Me.txtPorcentajeBase.Indicaciones = Nothing
        Me.txtPorcentajeBase.Location = New System.Drawing.Point(271, 16)
        Me.txtPorcentajeBase.Name = "txtPorcentajeBase"
        Me.txtPorcentajeBase.Size = New System.Drawing.Size(83, 21)
        Me.txtPorcentajeBase.SoloLectura = False
        Me.txtPorcentajeBase.TabIndex = 5
        Me.txtPorcentajeBase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPorcentajeBase.Texto = "0"
        '
        'lblListaPrecio
        '
        Me.lblListaPrecio.AutoSize = True
        Me.lblListaPrecio.Location = New System.Drawing.Point(3, 0)
        Me.lblListaPrecio.Name = "lblListaPrecio"
        Me.lblListaPrecio.Size = New System.Drawing.Size(80, 13)
        Me.lblListaPrecio.TabIndex = 0
        Me.lblListaPrecio.Text = "Lista de Precio:"
        Me.lblListaPrecio.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = Nothing
        Me.cbxListaPrecio.DataDisplayMember = ""
        Me.cbxListaPrecio.DataFilter = ""
        Me.cbxListaPrecio.DataOrderBy = ""
        Me.cbxListaPrecio.DataSource = ""
        Me.cbxListaPrecio.DataValueMember = ""
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(6, 16)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(242, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 1
        Me.cbxListaPrecio.Texto = ""
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnActualizar)
        Me.Panel2.Controls.Add(Me.btnCancelar)
        Me.Panel2.Controls.Add(Me.btnGuardar)
        Me.Panel2.Controls.Add(Me.btnModificar)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(623, 3)
        Me.Panel2.Name = "Panel2"
        Me.TableLayoutPanel1.SetRowSpan(Me.Panel2, 3)
        Me.Panel2.Size = New System.Drawing.Size(95, 371)
        Me.Panel2.TabIndex = 1
        '
        'btnActualizar
        '
        Me.btnActualizar.Location = New System.Drawing.Point(3, 154)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(84, 24)
        Me.btnActualizar.TabIndex = 3
        Me.btnActualizar.Text = "Actualizar"
        Me.btnActualizar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(3, 122)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(84, 24)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(3, 90)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(84, 24)
        Me.btnGuardar.TabIndex = 1
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(3, 58)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(84, 24)
        Me.btnModificar.TabIndex = 0
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 356)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(614, 18)
        Me.Panel3.TabIndex = 3
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'ocxComisionProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxComisionProducto"
        Me.Size = New System.Drawing.Size(721, 377)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dgw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblTPR As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeEsteProducto As ERP.ocxTXTNumeric
    Friend WithEvents lblPrecioUnitario As System.Windows.Forms.Label
    Friend WithEvents txtPorcentajeBase As ERP.ocxTXTNumeric
    Friend WithEvents lblListaPrecio As System.Windows.Forms.Label
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnActualizar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents dgw As System.Windows.Forms.DataGridView

End Class

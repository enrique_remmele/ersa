﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxProductoClasificacion
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ocxProductoClasificacion))
        Me.tv = New System.Windows.Forms.TreeView()
        Me.ImageList101 = New System.Windows.Forms.ImageList(Me.components)
        Me.SuspendLayout()
        '
        'tv
        '
        Me.tv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tv.Location = New System.Drawing.Point(0, 0)
        Me.tv.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.tv.Name = "tv"
        Me.tv.Size = New System.Drawing.Size(536, 185)
        Me.tv.TabIndex = 0
        '
        'ImageList101
        '
        Me.ImageList101.ImageStream = CType(resources.GetObject("ImageList101.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList101.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList101.Images.SetKeyName(0, "icon.folder.close.gif")
        Me.ImageList101.Images.SetKeyName(1, "icon.folder.open.gif")
        Me.ImageList101.Images.SetKeyName(2, "form_specific_completed.gif")
        '
        'ocxProductoClasificacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.tv)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "ocxProductoClasificacion"
        Me.Size = New System.Drawing.Size(536, 185)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tv As System.Windows.Forms.TreeView
    Friend WithEvents ImageList101 As System.Windows.Forms.ImageList

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxDocumentosVentas
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblCancelacionAutomatico = New System.Windows.Forms.Label()
        Me.txtSaldo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNotaDebito = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNotaCredito = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCobranza = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.OcxDocumentosNoAplicados1 = New ERP.ocxDocumentosNoAplicados()
        Me.OcxDocumentosVentasNotaDebito1 = New ERP.ocxDocumentosVentasNotaDebito()
        Me.OcxDocumentosVentasNotaCredito1 = New ERP.ocxDocumentosVentasNotaCredito()
        Me.OcxDocumentosVentasCobranzas1 = New ERP.ocxDocumentosVentasCobranzas()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbCobranza = New System.Windows.Forms.RadioButton()
        Me.rdbNotaCredito = New System.Windows.Forms.RadioButton()
        Me.rdbNotaDebito = New System.Windows.Forms.RadioButton()
        Me.rdbDocumentosNoAplicados = New System.Windows.Forms.RadioButton()
        Me.txtMigracion = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.43052!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.56948!))
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(734, 400)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtMigracion)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.lblCancelacionAutomatico)
        Me.GroupBox1.Controls.Add(Me.txtSaldo)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtNotaDebito)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtNotaCredito)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtCobranza)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtTotal)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(564, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.TableLayoutPanel1.SetRowSpan(Me.GroupBox1, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(167, 394)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Valores"
        '
        'lblCancelacionAutomatico
        '
        Me.lblCancelacionAutomatico.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblCancelacionAutomatico.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCancelacionAutomatico.ForeColor = System.Drawing.Color.Red
        Me.lblCancelacionAutomatico.Location = New System.Drawing.Point(3, 350)
        Me.lblCancelacionAutomatico.Name = "lblCancelacionAutomatico"
        Me.lblCancelacionAutomatico.Size = New System.Drawing.Size(161, 41)
        Me.lblCancelacionAutomatico.TabIndex = 11
        Me.lblCancelacionAutomatico.Tag = "No tocar"
        Me.lblCancelacionAutomatico.Text = "* El comprobante tiene asignado una forma de pago que se cancela automaticamente." &
    ""
        Me.lblCancelacionAutomatico.Visible = False
        '
        'txtSaldo
        '
        Me.txtSaldo.BackColor = System.Drawing.Color.White
        Me.txtSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldo.ForeColor = System.Drawing.Color.Maroon
        Me.txtSaldo.Location = New System.Drawing.Point(70, 160)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.ReadOnly = True
        Me.txtSaldo.Size = New System.Drawing.Size(91, 20)
        Me.txtSaldo.TabIndex = 10
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 164)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(37, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Saldo:"
        '
        'txtNotaDebito
        '
        Me.txtNotaDebito.BackColor = System.Drawing.Color.MintCream
        Me.txtNotaDebito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotaDebito.Location = New System.Drawing.Point(70, 44)
        Me.txtNotaDebito.Name = "txtNotaDebito"
        Me.txtNotaDebito.ReadOnly = True
        Me.txtNotaDebito.Size = New System.Drawing.Size(91, 20)
        Me.txtNotaDebito.TabIndex = 3
        Me.txtNotaDebito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "N. Debitos:"
        '
        'txtNotaCredito
        '
        Me.txtNotaCredito.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtNotaCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotaCredito.ForeColor = System.Drawing.Color.Maroon
        Me.txtNotaCredito.Location = New System.Drawing.Point(70, 97)
        Me.txtNotaCredito.Name = "txtNotaCredito"
        Me.txtNotaCredito.ReadOnly = True
        Me.txtNotaCredito.Size = New System.Drawing.Size(91, 20)
        Me.txtNotaCredito.TabIndex = 7
        Me.txtNotaCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 101)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "N. Creditos:"
        '
        'txtCobranza
        '
        Me.txtCobranza.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCobranza.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCobranza.ForeColor = System.Drawing.Color.Maroon
        Me.txtCobranza.Location = New System.Drawing.Point(70, 71)
        Me.txtCobranza.Name = "txtCobranza"
        Me.txtCobranza.ReadOnly = True
        Me.txtCobranza.Size = New System.Drawing.Size(91, 20)
        Me.txtCobranza.TabIndex = 5
        Me.txtCobranza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Cobranzas:"
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.Color.MintCream
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(70, 18)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(91, 20)
        Me.txtTotal.TabIndex = 1
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Total:"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(24, 142)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(137, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "_______________________________"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.OcxDocumentosNoAplicados1)
        Me.Panel1.Controls.Add(Me.OcxDocumentosVentasNotaDebito1)
        Me.Panel1.Controls.Add(Me.OcxDocumentosVentasNotaCredito1)
        Me.Panel1.Controls.Add(Me.OcxDocumentosVentasCobranzas1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(555, 356)
        Me.Panel1.TabIndex = 2
        '
        'OcxDocumentosNoAplicados1
        '
        Me.OcxDocumentosNoAplicados1.Cantidad = 0
        Me.OcxDocumentosNoAplicados1.IDTransaccion = Nothing
        Me.OcxDocumentosNoAplicados1.Location = New System.Drawing.Point(89, 43)
        Me.OcxDocumentosNoAplicados1.Name = "OcxDocumentosNoAplicados1"
        Me.OcxDocumentosNoAplicados1.Size = New System.Drawing.Size(383, 146)
        Me.OcxDocumentosNoAplicados1.TabIndex = 2
        Me.OcxDocumentosNoAplicados1.Visible = False
        '
        'OcxDocumentosVentasNotaDebito1
        '
        Me.OcxDocumentosVentasNotaDebito1.Cantidad = 0
        Me.OcxDocumentosVentasNotaDebito1.IDTransaccion = Nothing
        Me.OcxDocumentosVentasNotaDebito1.Location = New System.Drawing.Point(78, 122)
        Me.OcxDocumentosVentasNotaDebito1.Name = "OcxDocumentosVentasNotaDebito1"
        Me.OcxDocumentosVentasNotaDebito1.Size = New System.Drawing.Size(394, 147)
        Me.OcxDocumentosVentasNotaDebito1.TabIndex = 1
        Me.OcxDocumentosVentasNotaDebito1.Visible = False
        '
        'OcxDocumentosVentasNotaCredito1
        '
        Me.OcxDocumentosVentasNotaCredito1.Cantidad = 0
        Me.OcxDocumentosVentasNotaCredito1.IDTransaccion = Nothing
        Me.OcxDocumentosVentasNotaCredito1.Location = New System.Drawing.Point(127, 171)
        Me.OcxDocumentosVentasNotaCredito1.Name = "OcxDocumentosVentasNotaCredito1"
        Me.OcxDocumentosVentasNotaCredito1.Size = New System.Drawing.Size(380, 150)
        Me.OcxDocumentosVentasNotaCredito1.TabIndex = 0
        Me.OcxDocumentosVentasNotaCredito1.Visible = False
        '
        'OcxDocumentosVentasCobranzas1
        '
        Me.OcxDocumentosVentasCobranzas1.Cantidad = 0
        Me.OcxDocumentosVentasCobranzas1.IDTransaccion = Nothing
        Me.OcxDocumentosVentasCobranzas1.Location = New System.Drawing.Point(16, 101)
        Me.OcxDocumentosVentasCobranzas1.Name = "OcxDocumentosVentasCobranzas1"
        Me.OcxDocumentosVentasCobranzas1.Size = New System.Drawing.Size(305, 133)
        Me.OcxDocumentosVentasCobranzas1.TabIndex = 0
        Me.OcxDocumentosVentasCobranzas1.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbCobranza)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbNotaCredito)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbNotaDebito)
        Me.FlowLayoutPanel1.Controls.Add(Me.rdbDocumentosNoAplicados)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 365)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(555, 32)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'rdbCobranza
        '
        Me.rdbCobranza.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbCobranza.AutoSize = True
        Me.rdbCobranza.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbCobranza.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.rdbCobranza.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.rdbCobranza.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.rdbCobranza.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbCobranza.Location = New System.Drawing.Point(3, 3)
        Me.rdbCobranza.Name = "rdbCobranza"
        Me.rdbCobranza.Size = New System.Drawing.Size(69, 25)
        Me.rdbCobranza.TabIndex = 0
        Me.rdbCobranza.TabStop = True
        Me.rdbCobranza.Text = "Cobranzas"
        Me.rdbCobranza.UseVisualStyleBackColor = True
        '
        'rdbNotaCredito
        '
        Me.rdbNotaCredito.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbNotaCredito.AutoSize = True
        Me.rdbNotaCredito.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbNotaCredito.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.rdbNotaCredito.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.rdbNotaCredito.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.rdbNotaCredito.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbNotaCredito.Location = New System.Drawing.Point(78, 3)
        Me.rdbNotaCredito.Name = "rdbNotaCredito"
        Me.rdbNotaCredito.Size = New System.Drawing.Size(98, 25)
        Me.rdbNotaCredito.TabIndex = 1
        Me.rdbNotaCredito.TabStop = True
        Me.rdbNotaCredito.Text = "Notas de Credito"
        Me.rdbNotaCredito.UseVisualStyleBackColor = True
        '
        'rdbNotaDebito
        '
        Me.rdbNotaDebito.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbNotaDebito.AutoSize = True
        Me.rdbNotaDebito.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbNotaDebito.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.rdbNotaDebito.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.rdbNotaDebito.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.rdbNotaDebito.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbNotaDebito.Location = New System.Drawing.Point(182, 3)
        Me.rdbNotaDebito.Name = "rdbNotaDebito"
        Me.rdbNotaDebito.Size = New System.Drawing.Size(96, 25)
        Me.rdbNotaDebito.TabIndex = 2
        Me.rdbNotaDebito.TabStop = True
        Me.rdbNotaDebito.Text = "Notas de Debito"
        Me.rdbNotaDebito.UseVisualStyleBackColor = True
        '
        'rdbDocumentosNoAplicados
        '
        Me.rdbDocumentosNoAplicados.Appearance = System.Windows.Forms.Appearance.Button
        Me.rdbDocumentosNoAplicados.AutoSize = True
        Me.rdbDocumentosNoAplicados.Cursor = System.Windows.Forms.Cursors.Hand
        Me.rdbDocumentosNoAplicados.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.rdbDocumentosNoAplicados.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.rdbDocumentosNoAplicados.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.rdbDocumentosNoAplicados.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.rdbDocumentosNoAplicados.Location = New System.Drawing.Point(284, 3)
        Me.rdbDocumentosNoAplicados.Name = "rdbDocumentosNoAplicados"
        Me.rdbDocumentosNoAplicados.Size = New System.Drawing.Size(145, 25)
        Me.rdbDocumentosNoAplicados.TabIndex = 3
        Me.rdbDocumentosNoAplicados.TabStop = True
        Me.rdbDocumentosNoAplicados.Text = "Documentos No Aplicados"
        Me.rdbDocumentosNoAplicados.UseVisualStyleBackColor = True
        '
        'txtMigracion
        '
        Me.txtMigracion.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtMigracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMigracion.ForeColor = System.Drawing.Color.Maroon
        Me.txtMigracion.Location = New System.Drawing.Point(70, 123)
        Me.txtMigracion.Name = "txtMigracion"
        Me.txtMigracion.ReadOnly = True
        Me.txtMigracion.Size = New System.Drawing.Size(91, 20)
        Me.txtMigracion.TabIndex = 13
        Me.txtMigracion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 127)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Migracion:"
        '
        'ocxDocumentosVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxDocumentosVentas"
        Me.Size = New System.Drawing.Size(734, 400)
        Me.TableLayoutPanel1.ResumeLayout(false)
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.Panel1.ResumeLayout(false)
        Me.FlowLayoutPanel1.ResumeLayout(false)
        Me.FlowLayoutPanel1.PerformLayout
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents OcxDocumentosVentasCobranzas1 As ERP.ocxDocumentosVentasCobranzas
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSaldo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNotaDebito As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNotaCredito As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCobranza As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents OcxDocumentosVentasNotaCredito1 As ERP.ocxDocumentosVentasNotaCredito
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents rdbCobranza As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNotaCredito As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNotaDebito As System.Windows.Forms.RadioButton
    Friend WithEvents OcxDocumentosVentasNotaDebito1 As ERP.ocxDocumentosVentasNotaDebito
    Friend WithEvents lblCancelacionAutomatico As System.Windows.Forms.Label
    Friend WithEvents rdbDocumentosNoAplicados As RadioButton
    Friend WithEvents OcxDocumentosNoAplicados1 As ocxDocumentosNoAplicados
    Friend WithEvents txtMigracion As TextBox
    Friend WithEvents Label7 As Label
End Class

﻿Public Class ocxDocumentosNoAplicados

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As String
    Public Property IDTransaccion() As String
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As String)
            IDTransaccionValue = value
        End Set
    End Property

    Private CantidadValue As Integer
    Public Property Cantidad() As Integer
        Get
            Return CantidadValue
        End Get
        Set(ByVal value As Integer)
            CantidadValue = value
        End Set
    End Property

    Sub Inicializar()

        dgv.Rows.Clear()

    End Sub

    Public Sub Cargar()

        Try
            Dim SQL As String
            SQL = "Select IDTransaccionDocumento, Documento, [Comp.], Fecha, Tipo, [Estado], Usuario, Observacion, Total, Importe From VVentaDocumentosSinAplicar Where IDTransaccion=" & IDTransaccion
            Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL).Copy
            dgv.DataSource = dt

            'Formato
            'dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
            For c As Integer = 0 To dgv.ColumnCount - 1
                dgv.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            Next

            dgv.Columns(8).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill



            dgv.Columns(8).DefaultCellStyle.Format = "N0"
            dgv.Columns(9).DefaultCellStyle.Format = "N0"

            dgv.Columns(0).Visible = False
            dgv.Columns(1).Visible = False

            ''Cargar los que estan anulados
            'For i As Integer = 0 To dgv.Rows.Count - 1
            '    If dgv.Rows(i).Cells(10).Value = True Then
            '        dgv.Rows(i).DefaultCellStyle.BackColor = Color.Salmon
            '    End If
            'Next

        Catch ex As Exception

        End Try

        Cantidad = dgv.Rows.Count

        txtTotal.SetValue(CSistema.gridSumColumn(dgv, "Importe"))

    End Sub

    Sub VisualizarDocumento()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If
        Dim Documento As String = dgv.SelectedRows(0).Cells(1).Value.ToString

        Select Case Documento
            Case "NOTACREDITO"
                Dim IDTransaccionDocumento As Integer = dgv.SelectedRows(0).Cells(0).Value
                Dim frm As New frmNotaCreditoCliente
                frm.tipoComprobante = "NCR"
                FGMostrarFormulario(frmPrincipal2, frm, Documento)
                frm.CargarOperacion(IDTransaccionDocumento)

            Case "PEDIDONOTACREDITO"
                Dim IDTransaccionDocumento As Integer = dgv.SelectedRows(0).Cells(0).Value
                Dim frm As New frmPedidoNotaCredito
                FGMostrarFormulario(frmPrincipal2, frm, Documento, FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False, True, False)
                frm.CargarOperacion(IDTransaccionDocumento)

        End Select

    End Sub

    Private Sub LinkLabel1_Click(sender As Object, e As System.EventArgs) Handles LinkLabel1.LinkClicked
        VisualizarDocumento()
    End Sub

End Class

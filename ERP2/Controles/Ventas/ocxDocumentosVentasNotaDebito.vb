﻿Public Class ocxDocumentosVentasNotaDebito

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As String
    Public Property IDTransaccion() As String
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As String)
            IDTransaccionValue = value
        End Set
    End Property

    Private CantidadValue As Integer
    Public Property Cantidad() As Integer
        Get
            Return CantidadValue
        End Get
        Set(ByVal value As Integer)
            CantidadValue = value
        End Set
    End Property

    Sub Inicializar()

        dgv.Rows.Clear()

    End Sub

    Public Sub Cargar()

        Try
            Dim SQL As String
            SQL = "Select V.IDTransaccion, V.[Comp. ND], V.Fecha, V.Total, V.Importe, V.Observacion, V.[Usuario ND] From VNotaDebitoVenta V Where V.IDTransaccionVenta=" & IDTransaccion
            Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL).Copy
            dgv.DataSource = dt

            'Formato
            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
            For c As Integer = 0 To dgv.ColumnCount - 1
                dgv.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            Next

            dgv.Columns(6).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

            dgv.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgv.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgv.Columns(4).DefaultCellStyle.Format = "N0"
            dgv.Columns(5).DefaultCellStyle.Format = "N0"
            dgv.Columns(3).DefaultCellStyle.Format = "N0"

            dgv.Columns(0).Visible = False

        Catch ex As Exception

        End Try

        Cantidad = dgv.Rows.Count

        txtTotal.SetValue(CSistema.gridSumColumn(dgv, "Importe"))

    End Sub

    Sub VisualizarDocumento()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDTransaccionDocumento As Integer = dgv.SelectedRows(0).Cells(0).Value
        Dim frm As New frmNotaCreditoCliente
        FGMostrarFormulario(frmPrincipal2, frm, "Nota de Credito")
        frm.CargarOperacion(IDTransaccionDocumento)

    End Sub

    Private Sub LinkLabel1_Click(sender As Object, e As System.EventArgs) Handles LinkLabel1.LinkClicked
        VisualizarDocumento()
    End Sub

End Class

﻿Public Class ocxDocumentosVentas

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As String
    Public Property IDTransaccion() As String
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As String)
            IDTransaccionValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal
        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Public Property CancelarAutomatico As Boolean = False

    Sub Inicializar()

        OcxDocumentosVentasCobranzas1.Inicializar()
        OcxDocumentosVentasNotaCredito1.Inicializar()
        OcxDocumentosVentasNotaDebito1.Inicializar()

        If CancelarAutomatico Then
            lblCancelacionAutomatico.Visible = True
        Else
            lblCancelacionAutomatico.Visible = False
        End If

    End Sub

    Sub CargarDocumentos()

        OcxDocumentosVentasCobranzas1.IDTransaccion = IDTransaccion
        OcxDocumentosVentasCobranzas1.Cargar()

        OcxDocumentosVentasNotaCredito1.IDTransaccion = IDTransaccion
        OcxDocumentosVentasNotaCredito1.Cargar()

        OcxDocumentosVentasNotaDebito1.IDTransaccion = IDTransaccion
        OcxDocumentosVentasNotaDebito1.Cargar()

        OcxDocumentosNoAplicados1.IDTransaccion = IDTransaccion
        OcxDocumentosNoAplicados1.Cargar()

        If OcxDocumentosVentasCobranzas1.Cantidad > 0 Then
            rdbCobranza.Text = "Cobranzas ( " & OcxDocumentosVentasCobranzas1.Cantidad & " )"
            rdbCobranza.Font = New Font(rdbCobranza.Font.FontFamily, rdbCobranza.Font.Size, FontStyle.Bold)
        Else
            rdbCobranza.Text = "Cobranzas"
            rdbCobranza.Font = New Font(rdbCobranza.Font.FontFamily, rdbCobranza.Font.Size, FontStyle.Regular)
        End If

        If OcxDocumentosVentasNotaCredito1.Cantidad > 0 Then
            rdbNotaCredito.Text = "Notas de Credito ( " & OcxDocumentosVentasNotaCredito1.Cantidad & " )"
            rdbNotaCredito.Font = New Font(rdbNotaCredito.Font.FontFamily, rdbNotaCredito.Font.Size, FontStyle.Bold)
        Else
            rdbNotaCredito.Text = "Notas de Credito"
            rdbNotaCredito.Font = New Font(rdbNotaCredito.Font.FontFamily, rdbNotaCredito.Font.Size, FontStyle.Regular)
        End If

        If OcxDocumentosVentasNotaDebito1.Cantidad > 0 Then
            rdbNotaDebito.Text = "Notas de Debito ( " & OcxDocumentosVentasNotaDebito1.Cantidad & " )"
            rdbNotaDebito.Font = New Font(rdbNotaDebito.Font.FontFamily, rdbNotaDebito.Font.Size, FontStyle.Bold)
        Else
            rdbNotaDebito.Text = "Notas de Debito"
            rdbNotaDebito.Font = New Font(rdbNotaDebito.Font.FontFamily, rdbNotaDebito.Font.Size, FontStyle.Regular)
        End If

        If OcxDocumentosNoAplicados1.Cantidad > 0 Then
            rdbDocumentosNoAplicados.Text = "Documentos No Aplicados ( " & OcxDocumentosNoAplicados1.Cantidad & " )"
            rdbDocumentosNoAplicados.Font = New Font(rdbCobranza.Font.FontFamily, rdbCobranza.Font.Size, FontStyle.Bold)
        Else
            rdbDocumentosNoAplicados.Text = "Documentos No Aplicados"
            rdbDocumentosNoAplicados.Font = New Font(rdbCobranza.Font.FontFamily, rdbCobranza.Font.Size, FontStyle.Regular)
        End If



        Dim Saldo As Decimal = 0
        Dim Cobranzas As Decimal
        Dim NotaCredito As Decimal
        Dim Migracion As Decimal
        Dim NotaDebito As Decimal

        Cobranzas = OcxDocumentosVentasCobranzas1.txtTotal.ObtenerValor
        NotaCredito = OcxDocumentosVentasNotaCredito1.txtTotal.ObtenerValor
        Migracion = CSistema.ExecuteScalar("Select IsNull((Select Cobrado From VentaImportada Where IDTransaccion=" & IDTransaccion & "),0)")
        NotaDebito = OcxDocumentosVentasNotaDebito1.txtTotal.ObtenerValor

        Saldo = (Total + NotaDebito) - (Cobranzas + NotaCredito + Migracion)

        txtTotal.Text = CSistema.FormatoMoneda(Total.ToString, False)
        txtCobranza.Text = CSistema.FormatoMoneda(Cobranzas.ToString, False)
        txtNotaCredito.Text = CSistema.FormatoMoneda(NotaCredito.ToString, False)
        txtNotaDebito.Text = CSistema.FormatoMoneda(NotaDebito.ToString, False)
        txtMigracion.Text = CSistema.FormatoMoneda(Migracion.ToString, False)
        txtSaldo.Text = CSistema.FormatoMoneda(Saldo.ToString, False)

        If CancelarAutomatico Then
            lblCancelacionAutomatico.Visible = True
        Else
            lblCancelacionAutomatico.Visible = False
        End If

    End Sub

    Sub MostrarDocumento(tipo As String)

        For Each ctr As Control In Panel1.Controls
            ctr.Visible = False
        Next

        Select Case tipo.ToUpper
            Case "COBRANZAS"
                OcxDocumentosVentasCobranzas1.Dock = DockStyle.Fill
                OcxDocumentosVentasCobranzas1.Visible = True
            Case "NOTAS DE CREDITO"
                OcxDocumentosVentasNotaCredito1.Dock = DockStyle.Fill
                OcxDocumentosVentasNotaCredito1.Visible = True
            Case "NOTAS DE DEBITO"
                OcxDocumentosVentasNotaDebito1.Dock = DockStyle.Fill
                OcxDocumentosVentasNotaDebito1.Visible = True

            Case "DOCUMENTOS NO APLICADOS"
                OcxDocumentosNoAplicados1.Dock = DockStyle.Fill
                OcxDocumentosNoAplicados1.Visible = True

        End Select
    End Sub

    Private Sub rdbCobranza_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbCobranza.CheckedChanged
        If rdbCobranza.Checked = True Then
            MostrarDocumento("COBRANZAS")
        End If
    End Sub

    Private Sub rdbNotaCredito_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbNotaCredito.CheckedChanged
        If rdbNotaCredito.Checked = True Then
            MostrarDocumento("NOTAS DE CREDITO")
        End If
    End Sub

    Private Sub rdbNotaDebito_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbNotaDebito.CheckedChanged
        If rdbNotaDebito.Checked = True Then
            MostrarDocumento("NOTAS DE DEBITO")
        End If
    End Sub

    Private Sub rdbDocumentosNoAplicados_CheckedChanged(sender As Object, e As EventArgs) Handles rdbDocumentosNoAplicados.CheckedChanged
        MostrarDocumento("DOCUMENTOS NO APLICADOS")
    End Sub
End Class

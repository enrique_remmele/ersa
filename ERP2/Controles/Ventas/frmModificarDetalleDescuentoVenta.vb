﻿Public Class frmModificarDetalleDescuentoVenta

    'CLASES
    Dim CSistema As New CSistema
    Dim vIDActividad As String

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    Private SuperadoValue As Boolean
    Public Property Superado() As Boolean
        Get
            Return SuperadoValue
        End Get
        Set(ByVal value As Boolean)
            SuperadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        CargarDatos()

        txtPrecioUnitario.SetValue(dt.Rows(0)("PrecioUnitario"))
        txtcantidad.SetValue(dt.Rows(0)("Cantidad"))

        CalcularImporte(False, True, False)

    End Sub

    Sub CargarDatos()

        CSistema.dtToGrid(dgvDescuento, dtDescuento)

        'Formato
        For c As Integer = 0 To dgvDescuento.ColumnCount - 1
            dgvDescuento.Columns(c).Visible = False
        Next

        dgvDescuento.Columns("T. Desc.").Visible = True
        dgvDescuento.Columns("Actividad").Visible = True
        dgvDescuento.Columns("Porcentaje").Visible = True
        dgvDescuento.Columns("Descuento").Visible = True
        dgvDescuento.Columns("Total").Visible = True

        dgvDescuento.Columns("T. Desc.").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

        dgvDescuento.Columns("Porcentaje").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDescuento.Columns("Descuento").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvDescuento.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgvDescuento.Columns("Porcentaje").DefaultCellStyle.Format = "N2"
        dgvDescuento.Columns("Descuento").DefaultCellStyle.Format = "N0"
        dgvDescuento.Columns("Total").DefaultCellStyle.Format = "N0"

    End Sub

    Sub Procesar()

        'Validar
        If dgvDescuento.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        'Que el descuento seleccionado sea editable
        Dim IDDescuento As Integer = dgvDescuento.SelectedRows(0).Cells("IDTipoDescuento").Value
        Dim Editable As Boolean = CSistema.ExecuteScalar("Select Editable From TipoDescuento Where ID=" & IDDescuento)

        If Editable = False Then
            MessageBox.Show("El tipo de descuento no es editable!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If


        'Controlar si es que sobrepasa la planilla de tacticos
        'If vgConfiguraciones("VentaSuperarTacticoConCredencial") = False Then
        '    MessageBox.Show("El descuento supera lo especificado en la planilla de descuentos!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    Exit Sub
        'End If

        Dim Cantidad As Decimal = dt.Rows(0)("Cantidad")

        'Actualizar descuentos
        For Each oRow As DataRow In dtDescuento.Select("ID=" & dgvDescuento.SelectedRows(0).Cells("ID").Value)

            'Cambiar la cantidad y el total
            oRow("Descuento") = txtDescuento.ObtenerValor
            oRow("Porcentaje") = txtPorcentajeDescuento.ObtenerValor

            oRow("Total") = Cantidad * oRow("Descuento")
            oRow("DescuentoDiscriminado") = CSistema.CalcularPorcentaje(oRow("Descuento"), oRow("Porcentaje"), True, False)
            oRow("TotalDescuentoDiscriminado") = oRow("DescuentoDiscriminado") * Cantidad

            vIDActividad = oRow("IDActividad").ToString

        Next

        'Actualizaar el detalle
        For Each oRow As DataRow In dt.Rows

            'Cambiar la cantidad y el total
            oRow("TotalDescuento") = CSistema.dtSumColumn(dtDescuento, "Total")
            oRow("Descuento") = CSistema.dtSumColumn(dtDescuento, "Total")
            oRow("DescuentoUnitario") = CSistema.dtSumColumn(dtDescuento, "Descuento")
            oRow("Desc Uni") = CSistema.dtSumColumn(dtDescuento, "Descuento")

            CSistema.CalcularIVA(oRow("IDImpuesto"), oRow("DescuentoUnitario"), True, False, oRow("DescuentoUnitarioDiscriminado"))

            oRow("TotalDescuentoDiscriminado") = oRow("DescuentoUnitarioDiscriminado") * Cantidad

            oRow("PorcentajeDescuento") = txtPorcentajeDescuento.ObtenerValor
            oRow("Desc %") = txtPorcentajeDescuento.ObtenerValor
            oRow("PrecioNeto") = oRow("PrecioUnitario") - oRow("DescuentoUnitario")
            oRow("TotalPrecioNeto") = oRow("PrecioNeto") * Cantidad

        Next


        'Comparar Descuento aplicado contra la Planilla
        Dim vDescuentoMaximo As Decimal = CSistema.ExecuteScalar("Select DescuentoMaximo From Actividad Where ID=" & vIDActividad)
        If txtPorcentajeDescuento.ObtenerValor > vDescuentoMaximo Then
            Superado = True
        End If

        Procesado = True
        Me.Close()

    End Sub

    Sub CalcularImporte(ByVal Decimales As Boolean, ByVal Redondear As Boolean, ByVal PorTotal As Boolean)

        Dim PrecioUnitario As Decimal = dt.Rows(0)("PrecioUnitario")

        Dim Descuento As Decimal = CSistema.CalcularPorcentaje(PrecioUnitario, txtPorcentajeDescuento.ObtenerValor, True, False)

        txtDescuento.SetValue(Descuento)

    End Sub

    Sub Seleccionar()

        txtPorcentajeDescuento.SetValue(0)
        txtDescuento.SetValue(0)

        If dgvDescuento.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        txtPorcentajeDescuento.SetValue(dgvDescuento.SelectedRows(0).Cells("Porcentaje").Value)

    End Sub

    Private Sub frmModificarDetalleVenta_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        CSistema.SelectNextControl(Me, e.KeyCode)

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub frmModificarDetalleVenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnAplicar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAplicar.Click
        Procesar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub dgvDescuento_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDescuento.KeyUp

        If e.KeyCode = Keys.Enter Then

            e.SuppressKeyPress = True

        End If

    End Sub

    Private Sub txtPorcentajeDescuento_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPorcentajeDescuento.TeclaPrecionada
        If txtPorcentajeDescuento.txt.Focused = False Then
            Exit Sub
        End If

        CalcularImporte(False, True, False)

    End Sub

    Private Sub dgvDescuento_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDescuento.SelectionChanged
        Seleccionar()
    End Sub

End Class
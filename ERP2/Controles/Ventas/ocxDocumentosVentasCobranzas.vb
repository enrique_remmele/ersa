﻿Public Class ocxDocumentosVentasCobranzas

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As String
    Public Property IDTransaccion() As String
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As String)
            IDTransaccionValue = value
        End Set
    End Property

    Private CantidadValue As Integer
    Public Property Cantidad() As Integer
        Get
            Return CantidadValue
        End Get
        Set(ByVal value As Integer)
            CantidadValue = value
        End Set
    End Property

    Sub Inicializar()

        dgv.Rows.Clear()

    End Sub

    Public Sub Cargar()

        Try
            Dim SQL As String
            '25052021 - SMG - Se genera un nuevo campo Tipo Comprobante para la vista
            SQL = "Select IDTransaccionCobranza, [T. Comp.], [Fec. Cob.], [Comp. Cob.], Cobrador, [Estado Cob], Usuario, Observacion, Total, Importe, AnuladoCobranza,TipoComprobante From VVentaDetalleCobranza Where IDTransaccion=" & IDTransaccion
            Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL).Copy
            dgv.DataSource = dt

            'Formato
            'dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
            For c As Integer = 0 To dgv.ColumnCount - 1
                dgv.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            Next

            dgv.Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            dgv.Columns(7).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

            dgv.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgv.Columns(8).DefaultCellStyle.Format = "N0"
            dgv.Columns(9).DefaultCellStyle.Format = "N0"

            dgv.Columns(0).Visible = False
            dgv.Columns(10).Visible = False

            'Cargar los que estan anulados
            For i As Integer = 0 To dgv.Rows.Count - 1
                If dgv.Rows(i).Cells(10).Value = True Then
                    dgv.Rows(i).DefaultCellStyle.BackColor = Color.Salmon
                End If
            Next

        Catch ex As Exception

        End Try

        Cantidad = dgv.Rows.Count

        txtTotal.SetValue(CSistema.gridSumColumn(dgv, "Importe"))

    End Sub

    Sub VisualizarDocumento()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDTransaccionDocumento As Integer = dgv.SelectedRows(0).Cells(0).Value
        '25052021 - SMG - Se selecciona el Tipo de Comprobante agregado a la Vista
        Dim cadena As String = dgv.SelectedRows(0).Cells(11).Value
        If cadena = "CLOT" Then
            Dim frm As New frmCobranzaContado
            FGMostrarFormulario(frmPrincipal2, frm, "Cobranza")
            frm.CargarOperacion(IDTransaccionDocumento)
        Else
            Dim frm As New frmCobranzaCredito
            FGMostrarFormulario(frmPrincipal2, frm, "Cobranza")
            frm.CargarOperacion(IDTransaccionDocumento)
        End If

    End Sub

    Private Sub LinkLabel1_Click(sender As Object, e As System.EventArgs) Handles LinkLabel1.LinkClicked
        VisualizarDocumento()
    End Sub

End Class

﻿Public Class ocxDocumentosVentasNotaCredito

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private IDTransaccionValue As String
    Public Property IDTransaccion() As String
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As String)
            IDTransaccionValue = value
        End Set
    End Property

    Private CantidadValue As Integer
    Public Property Cantidad() As Integer
        Get
            Return CantidadValue
        End Get
        Set(ByVal value As Integer)
            CantidadValue = value
        End Set
    End Property

    Sub Inicializar()

        dgv.Rows.Clear()

    End Sub

    Public Sub Cargar()

        Try
            Dim SQL As String
            'SC: 08/02/2021 - Se agrega nuevo campo Aplicar y se discrimina Aplicacion de Notas de Credito que no se hayan Anulado
            SQL = "Select V.IDTransaccion, V.[Comp. NC], V.Fecha, V.[Tipo NC], V.Total, V.Importe, V.Observacion, V.[Usuario NC] , V.Aplicar From VNotaCreditoVenta V Where V.IDTransaccionVenta=" & IDTransaccion
            Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL).Copy
            dgv.DataSource = dt

            'SC: 08/02/2021 - Desde aqui se agrega para Discriminar Notas de Credito que no se dejan Pendiente a Aplicar y las Aplicaciones que no sean Anuladas
            Dim TotalNC As Decimal = 0
            For Each oRow As DataRow In dt.Rows
                If oRow("Tipo NC") = "Aplicacion" Then
                    TotalNC = TotalNC + CDec(oRow("Importe").ToString)
                End If
                If oRow("Tipo NC") = "DEVOLUCION" And oRow("Aplicar") = 0 Then
                    TotalNC = TotalNC + CDec(oRow("Importe").ToString)
                End If
                If oRow("Tipo NC") = "DESC." And oRow("Aplicar") = 0 Then
                    TotalNC = TotalNC + CDec(oRow("Importe").ToString)
                End If
            Next
            txtTotal.SetValue(TotalNC)
            'SC: 08/02/2021 - Hasta aqui se agrega para Discriminar Notas de Credito que no se dejan Pendiente a Aplicar y las Aplicaciones que no sean Anuladas


            'Formato
            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
            For c As Integer = 0 To dgv.ColumnCount - 1
                dgv.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            Next

            dgv.Columns(6).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

            dgv.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgv.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgv.Columns(4).DefaultCellStyle.Format = "N0"
            dgv.Columns(5).DefaultCellStyle.Format = "N0"

            dgv.Columns(0).Visible = False
            dgv.Columns(8).Visible = False
        Catch ex As Exception

        End Try

        Cantidad = dgv.Rows.Count

        'SC: 08/02/2021 - Se comenta porque se agregan ajustes arriba
        'txtTotal.SetValue(CSistema.gridSumColumn(dgv, "Importe"))


    End Sub

    Sub VisualizarDocumento()

        If dgv.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim IDTransaccionDocumento As Integer = dgv.SelectedRows(0).Cells(0).Value
        Dim frm As New frmNotaCreditoCliente
        frm.tipoComprobante = "NCR"
        frm.IDTransaccion = IDTransaccionDocumento
        'frm.Inicializar()
        'frm.ShowDialog()
        FGMostrarFormulario(frmPrincipal2, frm, "Nota de Crédito Cliente", Windows.Forms.FormBorderStyle.SizableToolWindow, FormStartPosition.CenterScreen, False)
        'FGMostrarFormulario(frmPrincipal2, frm, "Nota de Credito")


    End Sub

    Private Sub LinkLabel1_Click(sender As Object, e As System.EventArgs) Handles LinkLabel1.LinkClicked
        VisualizarDocumento()
    End Sub

End Class

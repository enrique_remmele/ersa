﻿Public Class ocxClasificador

    'CLASES
    Private CSistema As New CSistema
    Private CData As New CData

    'EVENTOS
    Public Event ItemAgegado(ByVal Item As DataRow)
    Public Event ItemModificado(ByVal Item As DataRow)
    Public Event ItemEliminado(ByVal Item As DataRow)

    'PROPIEDADES
    Private TablaValue As String
    Public Property Tabla() As String
        Get
            Return TablaValue
        End Get
        Set(ByVal value As String)
            TablaValue = value
        End Set
    End Property

    Private StoreProcedureValue As String
    Public Property StoreProcedure() As String
        Get
            Return StoreProcedureValue
        End Get
        Set(ByVal value As String)
            StoreProcedureValue = value
        End Set
    End Property

    Private TablaDetalleValue As String
    Public Property TablaDetalle() As String
        Get
            Return TablaDetalleValue
        End Get
        Set(ByVal value As String)
            TablaDetalleValue = value
        End Set
    End Property

    Private StoreProcedureDetalleValue As String
    Public Property StoreProcedureDetalle() As String
        Get
            Return StoreProcedureDetalleValue
        End Get
        Set(ByVal value As String)
            StoreProcedureDetalleValue = value
        End Set
    End Property

    Private frmPadreValue As Form
    Public Property frmPadre() As Form
        Get
            Return frmPadreValue
        End Get
        Set(ByVal value As Form)
            frmPadreValue = value
        End Set
    End Property

    'VARIABLES
    Private dtClasificacion As DataTable
    Private dtDetalle As DataTable

    Private DragDropTreeView As Boolean
    Private NodoOrigen As TreeNode
    Private VerProductos As Boolean
    Private VerFullPath As Boolean

    'FUNCIONES
    Sub Inicializar()

        TableLayoutPanel1.RowStyles(2).Height = 0

        CargarDatos()

    End Sub

    Sub InicializarMaximizado()

        VerFullPath = False
        VerProductos = False

        TableLayoutPanel1.RowStyles(2).Height = 0
        ToolStripDropDownButton1.Visible = True

        CargarDatos()
        ListarDetalle()

    End Sub

    Private Sub CargarDatos()

        Listar()

    End Sub

    Private Sub Listar()

        Dim dtTemp As DataTable = CData.GetTable(Tabla)

        For Each oRow As DataRow In dtTemp.Select(" Nivel = 1 ")

            Dim nodo1 As New TreeNode(oRow("Clasificacion").ToString)
            nodo1.Name = oRow("ID")

            'Verificar si tiene hijos
            If dtTemp.Select(" IDPadre = " & oRow("ID").ToString).Count > 0 Then
                Listar(dtTemp.Select(" IDPadre = " & oRow("ID").ToString), nodo1, dtTemp)
            End If

            tvClasificacion.Nodes.Add(nodo1)

        Next

    End Sub

    Private Sub Listar(ByVal oRows() As DataRow, ByVal nodo1 As TreeNode, ByVal dttemp As DataTable)


        For Each oRow As DataRow In oRows

            Dim nodo2 As New TreeNode(oRow("Clasificacion").ToString)
            nodo2.Name = oRow("ID")
            nodo1.Nodes.Add(nodo2)

            'Verificar si tiene hijos
            If dtTemp.Select(" IDPadre = " & oRow("ID").ToString).Count > 0 Then
                Listar(dttemp.Select(" IDPadre = " & oRow("ID").ToString), nodo2, dttemp)
            End If


        Next

    End Sub

    Private Sub ListarDetalle()

        If VerProductos = False Then
            Exit Sub
        End If

        dtDetalle = CData.GetTable(TablaDetalle)

        For Each oRow As DataRow In dtDetalle.Rows

            Dim nodeDetalle As New TreeNode(oRow("Descripcion").ToString)
            nodeDetalle.Name = "Detalle_" & oRow("ID").ToString

            If oRow("Estado") = True Then
                nodeDetalle.ImageIndex = 2
                nodeDetalle.SelectedImageIndex = 2
            Else
                nodeDetalle.ImageIndex = 3
                nodeDetalle.SelectedImageIndex = 3
            End If


            'Buscar el Padre
            Dim IDClasificacion As Integer = oRow("IDClasificacion")
            Dim nodeClasificacion As TreeNode = GetIndexNode(IDClasificacion)

            If nodeClasificacion Is Nothing Then
                tvClasificacion.Nodes.Add(nodeDetalle)
            Else
                nodeClasificacion.Nodes.Add(nodeDetalle)
                nodeClasificacion.Expand()
            End If



        Next


    End Sub

    Private Sub Nuevo()

        Dim frm As New frmAdministrarClasificacion

        frm.Tabla = Tabla
        frm.StoreProcedure = StoreProcedure

        frm.Nuevo = True
        frm.btnEliminar.Enabled = False
        frm.ID = 0

        If tvClasificacion.SelectedNode IsNot Nothing Then

            If IsNumeric(frm.IDPadre = tvClasificacion.SelectedNode.Name) = False Then
                Exit Sub
            End If

            frm.IDPadre = tvClasificacion.SelectedNode.Name
            frm.Padre = tvClasificacion.SelectedNode.Text
        Else
            frm.IDPadre = 0
            frm.Padre = "---"
        End If

        FGMostrarFormulario(frmPadre, frm, "Administrar Clasificaciones", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True)

        If frm.Procesado = True Then
            AgregarNodo(frm.ID)
        End If

    End Sub

    Private Sub Editar()

        Dim frm As New frmAdministrarClasificacion
        frm.Tabla = Tabla
        frm.StoreProcedure = StoreProcedure
        frm.Nuevo = False
        frm.btnEliminar.Enabled = False
        frm.ID = tvClasificacion.SelectedNode.Name

        If tvClasificacion.SelectedNode.Level > 0 Then

            If IsNumeric(frm.IDPadre = tvClasificacion.SelectedNode.Name) = False Then
                Exit Sub
            End If

            frm.IDPadre = tvClasificacion.SelectedNode.Parent.Name
            frm.Padre = tvClasificacion.SelectedNode.Parent.Text
        Else
            frm.IDPadre = "0"
            frm.Padre = "---"
        End If

        FGMostrarFormulario(frmPadre, frm, "Administrar Clasificaciones", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True)

        If frm.Procesado = True Then
            ModificarNodo(tvClasificacion.SelectedNode.Name)
        End If

    End Sub

    Private Sub Eliminar()

        Dim frm As New frmAdministrarClasificacion
        frm.Nuevo = False
        frm.Tabla = Tabla
        frm.StoreProcedure = StoreProcedure
        frm.btnGuardar.Enabled = False
        frm.ID = tvClasificacion.SelectedNode.Name
        If tvClasificacion.SelectedNode.Level > 0 Then

            If IsNumeric(frm.IDPadre = tvClasificacion.SelectedNode.Name) = False Then
                Exit Sub
            End If

            frm.IDPadre = tvClasificacion.SelectedNode.Parent.Name
            frm.Padre = tvClasificacion.SelectedNode.Parent.Text
        Else
            frm.IDPadre = "0"
            frm.Padre = "---"
        End If

        FGMostrarFormulario(frmPadre, frm, "Administrar Clasificaciones", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True)

        If frm.Procesado = True Then
            tvClasificacion.Nodes.Remove(tvClasificacion.SelectedNode)
        End If

    End Sub

    Private Sub AgregarNodo(ByVal ID As Integer)

        Dim oRow As DataRow = CData.GetTable(Tabla, " ID = " & ID)(0)

        Dim nodo As New TreeNode
        nodo.Name = ID
        nodo.Text = oRow("Clasificacion").ToString
        nodo.SelectedImageIndex = 1
        nodo.ImageIndex = 0

        'Seleccionar el Padre
        SeleccionarNodo(oRow("IDPadre"))

        tvClasificacion.SelectedNode.Nodes.Add(nodo)
        tvClasificacion.SelectedNode = nodo

    End Sub

    Private Sub SeleccionarNodo(ByVal ID As Integer)

        For Each node1 As TreeNode In tvClasificacion.Nodes

            If IsNumeric(node1.Name) = True Then
                If node1.Name = ID Then
                    tvClasificacion.SelectedNode = node1
                    tvClasificacion.SelectedNode.EnsureVisible()
                    Exit For
                Else
                    If node1.Nodes.Count > 0 Then
                        SeleccionarNodo(ID, node1)
                    End If
                End If
            End If

        Next

        ShowFullPath()

    End Sub

    Private Sub SeleccionarNodo(ByVal ID As Integer, ByVal nodo1 As TreeNode)

        For Each node2 As TreeNode In nodo1.Nodes

            If node2.Name = ID Then
                tvClasificacion.SelectedNode = node2
                tvClasificacion.SelectedNode.EnsureVisible()
                Exit For
            Else
                If node2.Nodes.Count > 0 Then
                    SeleccionarNodo(ID, node2)
                End If
            End If
        Next

    End Sub

    Private Sub ModificarNodo(ByVal ID As Integer)

        Dim oRow As DataRow = CData.GetTable(Tabla, " ID = " & ID)(0)
        tvClasificacion.SelectedNode.Text = oRow("Clasificacion")

        'Mover si el padre cambio
        Dim IDPadreActual As Integer = oRow("IDPadre")
        Dim IDPadreAnterior As Integer = tvClasificacion.SelectedNode.Parent.Name

        If IDPadreActual <> IDPadreAnterior Then

            Dim node1 As TreeNode = tvClasificacion.SelectedNode
            Dim nodeParent As TreeNode = GetIndexNode(IDPadreActual)

            'Colocar el nodo con el padre actual
            tvClasificacion.SelectedNode.Remove()
            nodeParent.Nodes.Add(node1)
            tvClasificacion.SelectedNode = node1

        End If

    End Sub

    Private Function GetIndexNode(ByVal ID As String) As TreeNode

        GetIndexNode = Nothing
        For Each node As TreeNode In tvClasificacion.Nodes

            If node.Name = ID Then
                Return node
            Else
                If node.Nodes.Count > 0 Then
                    GetIndexNode = GetIndexNode(ID, node)
                End If
            End If

            If Not GetIndexNode Is Nothing Then
                Exit For
            End If

        Next

    End Function

    Private Function GetIndexNode(ByVal ID As String, ByVal node1 As TreeNode) As TreeNode

        GetIndexNode = Nothing
        For Each node As TreeNode In node1.Nodes
            If node.Name = ID Then
                Return node
            Else
                If node.Nodes.Count > 0 Then
                    GetIndexNode = GetIndexNode(ID, node)
                End If
            End If

            If Not GetIndexNode Is Nothing Then
                Exit For
            End If

        Next

    End Function

    Public Function GetValue() As Integer

        If tvClasificacion.SelectedNode Is Nothing Then
            Return 0
        End If

        If IsNumeric(tvClasificacion.SelectedNode.Name) = False Then
            Return 0
        End If

        GetValue = tvClasificacion.SelectedNode.Name

    End Function

    Public Sub SetValue(ByVal ID As Integer)

        tvClasificacion.SelectedNode = Nothing

        SeleccionarNodo(ID)

    End Sub

    Function Guardar(ByVal ID As Integer, ByVal IDPadre As Integer) As Boolean

        Dim param(-1) As SqlClient.SqlParameter

        If CData.GetTable(Tabla, " ID = " & ID).Rows.Count = 0 Then
            Return False
        End If

        'Registro
        Dim oRow As DataRow = CData.GetTable(Tabla, " ID = " & ID).Rows(0)

        'Padre

        CSistema.SetSQLParameter(param, "@ID", oRow("ID"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPadre", IDPadre, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", oRow("Descripcion"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Alias", oRow("Alias"), ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesABM.UPD.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, StoreProcedure, False, False, MensajeRetorno) = False Then
            Return False
        End If

        'Actualizar DataTable
        CData.Actualizar(ID, Tabla)

        Return True


    End Function

    Function GuardarDetalle(ByVal ID As Integer, ByVal IDClasificacion As Integer) As Boolean

        Dim param(-1) As SqlClient.SqlParameter

        If CData.GetTable(TablaDetalle, " ID = " & ID).Rows.Count = 0 Then
            Return False
        End If

        'Registro
        Dim oRow As DataRow = CData.GetTable(TablaDetalle, " ID = " & ID).Rows(0)

        'Padre

        CSistema.SetSQLParameter(param, "@ID", oRow("ID"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDClasificacion", IDClasificacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", CSistema.NUMOperacionesABM.UPD.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, StoreProcedureDetalle, False, False, MensajeRetorno) = False Then
            Return False
        End If

        'Actualizar DataTable
        'CData.Actualizar(ID, Tabla)

        Return True


    End Function

    Private Sub ShowFullPath()

        txtFullPath.txt.Clear()

        If tvClasificacion.SelectedNode Is Nothing Then
            Exit Sub
        End If

        txtFullPath.txt.Text = tvClasificacion.SelectedNode.FullPath

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Nuevo()
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        Editar()
    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        Eliminar()
    End Sub

    Private Sub tvClasificacion_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvClasificacion.NodeMouseClick

        If e.Button = Windows.Forms.MouseButtons.Right Then

            ' Referenciamos el control
            Dim tv As Windows.Forms.TreeView = DirectCast(sender, Windows.Forms.TreeView)

            ' Seleccionamos el nodo
            tv.SelectedNode = e.Node

        End If

    End Sub

    Private Sub tvClasificacion_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvClasificacion.MouseDown

        Dim Left As String = e.Button.ToString
        ' pregunto si el botón que estoy pulsando es el izquierdo para poder arrastrar el nodo
        If Left = MouseButtons.Left.ToString Then
            ' señalo que se está haciendo un Drag and Drop dentro del TreeView
            DragDropTreeView = True

            ' obtengo el arbol del control TreeView1
            Dim tree As TreeView = CType(sender, TreeView)

            ' recupero el nodo debajo del mouse.
            Dim node As TreeNode
            node = tree.GetNodeAt(e.X, e.Y)

            ' establezco el nodo del árbol seleccionado actualmente en el control TreeView
            tree.SelectedNode = node

            ' guardo los datos del origen del nodo
            NodoOrigen = CType(node, TreeNode)

            ' inicio la operación Drag and Drop con una copia clonada del nodo.
            If Not node Is Nothing Then
                tree.DoDragDrop(node.Clone(), DragDropEffects.Copy)
            End If
        End If
    End Sub

    Private Sub tvClasificacion_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvClasificacion.DragDrop
        If DragDropTreeView = True Then
            ' determino si los datos almacenados en la instancia están asociados al formato especificado del TreeView
            If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", False) Then
                ' variable que sirve para guardar el valor de un punto en coordenadas X e Y
                Dim pt As Point
                ' variable que sirve para guardar el valor del nodo de destino
                Dim DestinationNode As TreeNode

                ' uso PointToClient para calcular la ubicación del mouse sobre el control TreeView
                pt = CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
                ' uso este punto para recuperar el nodo de destino dentro del árbol del control TreeView.
                DestinationNode = CType(sender, TreeView).GetNodeAt(pt)
                ' verifico que el nodo de destino sea distinto al nodo de origen
                If DestinationNode.FullPath <> NodoOrigen.FullPath Then

                    'Aqui ejecutamos el proceso de actualizacion de base de datos

                    'Primero verificamos si es entre clasificacion
                    If IsNumeric(NodoOrigen.Name) = True And IsNumeric(DestinationNode.Name) = True Then

                        If Guardar(NodoOrigen.Name, DestinationNode.Name) = False Then
                            Exit Sub
                        End If

                        DestinationNode.Nodes.Add(CType(NodoOrigen.Clone, TreeNode))

                        ' expando el nodo padre donde agregue el nuevo nodo. Sin esto, solo aparecería el signo +.
                        DestinationNode.Expand()

                        ' elimino el nodo de origen dentro del árbol
                        Dim nodoPadre As TreeNode = DestinationNode

                        'Guardar configuracion
                        Dim IDPadre As Integer = nodoPadre.Name
                        Dim ID As Integer = NodoOrigen.Name

                        NodoOrigen.Remove()

                    End If

                    'Primero verificamos si es entre clasificacion
                    If IsNumeric(NodoOrigen.Name) = False And IsNumeric(DestinationNode.Name) = True Then

                        If GuardarDetalle(NodoOrigen.Name.Replace("Detalle_", ""), DestinationNode.Name) = False Then
                            Exit Sub
                        End If

                        DestinationNode.Nodes.Add(CType(NodoOrigen.Clone, TreeNode))

                        ' expando el nodo padre donde agregue el nuevo nodo. Sin esto, solo aparecería el signo +.
                        DestinationNode.Expand()

                        ' elimino el nodo de origen dentro del árbol
                        Dim nodoPadre As TreeNode = DestinationNode

                        NodoOrigen.Remove()

                    End If

                End If

            End If
        End If
    End Sub

    Private Sub tvClasificacion_DragOver(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvClasificacion.DragOver
        ' Verifica si dentro del TreeView se está arrastrando
        If DragDropTreeView Then

            ' deshabilita la actualización en pantalla del control TreeView 
            tvClasificacion.BeginUpdate()

            ' obtengo el árbol
            Dim tree As TreeView = CType(sender, TreeView)

            ' establezco el efecto de la operación Drag and Drop
            e.Effect = DragDropEffects.None

            ' pregunto por si el formato es válido?
            If Not e.Data.GetData(GetType(TreeNode)) Is Nothing Then

                ' Obtengo el punto en la pantalla.
                Dim pt As New Point(e.X, e.Y)

                ' Convierto a un punto en el sistema de coordenadas del control TreeView
                pt = tree.PointToClient(pt)

                ' pregunto si el mouse está sobre un nodo válido
                Dim node As TreeNode = tree.GetNodeAt(pt)
                If Not node Is Nothing Then
                    ' establezco el efecto de la operación Drag and Drop
                    e.Effect = DragDropEffects.Copy
                    ' establezco el nodo del árbol seleccionado actualmente en el control TreeView
                    tree.SelectedNode = node

                End If

            End If
            ' habilita la actualización en pantalla del control TreeView
            tvClasificacion.EndUpdate()
        End If
    End Sub

    Private Sub tvClasificacion_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvClasificacion.MouseUp
        ' señalo que no está haciendo un Drag and Drop dentro del TreeView
        DragDropTreeView = False
    End Sub

    Private Sub tvClasificacion_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvClasificacion.BeforeExpand
        ' le asigno la imagen con la carpeta abierta
        tvClasificacion.SelectedImageIndex = 1
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click

        Dim frm As New Form
        Dim ctr As New ocxClasificador
        ctr.Tabla = Tabla
        ctr.StoreProcedure = StoreProcedure
        ctr.TablaDetalle = TablaDetalle
        ctr.StoreProcedureDetalle = StoreProcedureDetalle

        ctr.frmPadre = Me.ParentForm
        ctr.InicializarMaximizado()

        frm.Controls.Add(ctr)
        ctr.Dock = DockStyle.Fill
        frm.WindowState = FormWindowState.Maximized
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = FormBorderStyle.SizableToolWindow
        frm.Show()

    End Sub

    Private Sub tvClasificacion_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvClasificacion.AfterSelect
        ShowFullPath()
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        VerProductos = ToolStripMenuItem1.Checked
        ListarDetalle()
    End Sub

    Private Sub ToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem2.Click
        VerFullPath = ToolStripMenuItem2.Checked
        If VerFullPath = True Then
            TableLayoutPanel1.RowStyles(2).Height = 26
        Else
            TableLayoutPanel1.RowStyles(2).Height = 0
        End If
    End Sub

End Class

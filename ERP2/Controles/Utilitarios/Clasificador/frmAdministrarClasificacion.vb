﻿Public Class frmAdministrarClasificacion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private TablaValue As String
    Public Property Tabla() As String
        Get
            Return TablaValue
        End Get
        Set(ByVal value As String)
            TablaValue = value
        End Set
    End Property

    Private StoreProcedureValue As String
    Public Property StoreProcedure() As String
        Get
            Return StoreProcedureValue
        End Get
        Set(ByVal value As String)
            StoreProcedureValue = value
        End Set
    End Property

    Private IDValue As Integer
    Public Property ID() As Integer
        Get
            Return IDValue
        End Get
        Set(ByVal value As Integer)
            IDValue = value
        End Set
    End Property

    Private PadreValue As String
    Public Property Padre() As String
        Get
            Return PadreValue
        End Get
        Set(ByVal value As String)
            PadreValue = value
        End Set
    End Property

    Private IDPadreValue As Integer
    Public Property IDPadre() As Integer
        Get
            Return IDPadreValue
        End Get
        Set(ByVal value As Integer)
            IDPadreValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private ProcesadoValue As Boolean
    Public Property Procesado() As Boolean
        Get
            Return ProcesadoValue
        End Get
        Set(ByVal value As Boolean)
            ProcesadoValue = value
        End Set
    End Property

    'VARIABLES
    Dim oRow As DataRow

    'FUNCIONES
    Sub Inicializar()

        'Form
        If Nuevo = True Then
            Me.Text = "Agregar una Clasificacion"
        Else
            Me.Text = "Modificar la Clasificacion"
        End If

        'Funciones
        CargarInformacion()


    End Sub

    Sub CargarInformacion()

        Dim dtTemp As DataTable = CData.GetTable(Tabla).Copy
        CSistema.OrderDataTable(dtTemp, "Nivel, Numero")
        CSistema.SqlToComboBox(cbxPadre.cbx, dtTemp, "ID", "Descripcion")

        If Nuevo = True Then

            ID = CInt(CSistema.ExecuteScalar("Select ISNULL(MAX(ID)+1, 1) From ClasificacionProducto"))
            txtID.txt.Text = ID

            oRow = CData.GetTable(Tabla, " ID = " & IDPadre & " ").Rows(0)
            txtNivel.txt.Text = oRow("Nivel") + 1

            '
            Dim Codigo As String = ""
            Dim Numero As Integer = 0

            For Each HijosRows As DataRow In CData.GetTable(Tabla, " IDPadre = " & IDPadre & " ").Rows
                If Numero < HijosRows("Numero") Then
                    Numero = HijosRows("Numero")
                End If
            Next

            Numero = Numero + 1

            txtCodigo.txt.Text = (oRow("Nivel") + 1).ToString & Numero.ToString
            cbxGrupo.cbx.Text = ""
            txtNumero.SetValue(Numero)

            cbxPadre.SelectedValue(IDPadre)

        Else

            Try

                oRow = CData.GetTable(Tabla, "ID = " & ID & " ").Rows(0)
                txtID.txt.Text = oRow("ID").ToString
                txtNivel.txt.Text = oRow("Nivel").ToString
                txtCodigo.txt.Text = oRow("Codigo").ToString
                cbxPadre.SelectedValue(oRow("IDPadre"))
                txtDescripcion.txt.Text = oRow("Descripcion").ToString
                txtNumero.SetValue(oRow("Numero").ToString)
                txtAlias.txt.Text = oRow("Alias").ToString
                cbxGrupo.cbx.Text = oRow("Grupo").ToString

            Catch ex As Exception
                Dim mensaje As String = ex.Message
                ctrError.SetError(btnGuardar, mensaje)
                ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End Try


        End If


    End Sub

    Sub Guardar(ByVal Operacion As CSistema.NUMOperacionesABM)

        tsslEstado.Text = ""
        ctrError.Clear()

        'Validar

        'Descripcion
        If txtDescripcion.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Introduzca una descripcion valida!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion del Padre
        If cbxPadre.Validar("Seleccione correctamente la cuenta PADRE", ctrError, btnGuardar, tsslEstado) = False Then
            Exit Sub
        End If

        'Si va a anular
        If Operacion = ERP.CSistema.NUMOperacionesRegistro.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro y sus dependientes. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@ID", txtID.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDPadre", cbxPadre.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Descripcion", txtDescripcion.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Alias", txtAlias.txt.Text.Trim, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDAgrupador", cbxGrupo.cbx, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", txtNumero.ObtenerValor, ParameterDirection.Input)

        'Operacion
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, StoreProcedure, False, False, MensajeRetorno) = False Then
            tsslEstado.Text = "Atencion: " & MensajeRetorno.Substring(0, MensajeRetorno.Length)
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
            Procesado = False
            Exit Sub
        End If

        'Actualizar DataTable
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Then
            CData.Actualizar(ID, Tabla)
        Else
            CData.Insertar(ID, Tabla)
        End If

        Procesado = True

        Me.Close()

    End Sub

    Private Sub frmAdministrarCuentaContable_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmAdministrarCuentaContable_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Dim Opcion As CSistema.NUMOperacionesABM
        If Nuevo = True Then
            Opcion = ERP.CSistema.NUMOperacionesABM.INS
        Else
            Opcion = ERP.CSistema.NUMOperacionesABM.UPD
        End If

        Guardar(Opcion)

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Procesado = False
        Me.Close()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Guardar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub frmAdministrarClasificacion_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
       CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

End Class
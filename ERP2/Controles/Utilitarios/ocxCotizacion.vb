﻿Imports System.Data.SqlClient
Public Class ocxCotizacion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'EVENTOS
    Public Event CambioMoneda()
    Public Event CambioCotizacion()

    'PROPIEDADES
    Private SoloLecturaValue As Boolean
    Public Property SoloLectura() As Boolean
        Get
            Return SoloLecturaValue
        End Get
        Set(ByVal value As Boolean)
            SoloLecturaValue = value
            cbxMoneda.SoloLectura = value
            txtCotizacion.SoloLectura = value
        End Set
    End Property

    Private SaltarValue As Boolean
    Public Property Saltar() As Boolean
        Get
            Return SaltarValue
        End Get
        Set(ByVal value As Boolean)
            SaltarValue = value
        End Set
    End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private RegistroValue As DataRow
    Public Property Registro() As DataRow
        Get
            Return RegistroValue
        End Get
        Set(ByVal value As DataRow)
            RegistroValue = value
        End Set
    End Property

    Private SeleccionadoValue As Boolean
    Public Property Seleccionado() As Boolean
        Get
            Return SeleccionadoValue
        End Get
        Set(ByVal value As Boolean)
            SeleccionadoValue = value
        End Set
    End Property

    Private FiltroFechaValue As String
    Public Property FiltroFecha() As String
        Get
            Return FiltroFechaValue
        End Get
        Set(ByVal value As String)
            FiltroFechaValue = value
        End Set
    End Property

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'dt = CData.GetTable("VMoneda").Copy
            dt = CSistema.ExecuteToDataTable("Execute SpViewMoneda '" & FiltroFecha & "'").Copy()


            CSistema.SqlToComboBox(cbxMoneda.cbx, dt, "ID", "Referencia")
            Saltar = False

    End Sub

    Sub Actualizar()

        If cbxMoneda.cbx.SelectedValue Is Nothing Then
            txtCotizacion.SetValue(1)
            Exit Sub
        End If

        Dim Cotizacion As Decimal = 1
        For Each oRow As DataRow In dt.Select(" ID = " & cbxMoneda.cbx.SelectedValue)
            Cotizacion = oRow("Cotizacion")
        Next

        txtCotizacion.SetValue(Cotizacion)
        Seleccionar()

        RaiseEvent CambioMoneda()
    End Sub

    Sub Recargar()
        If FiltroFecha.Length = 8 Then
            dt = CSistema.ExecuteToDataTable("Execute SpViewMoneda '" & FiltroFecha & "'").Copy()
            Saltar = False
        End If
    End Sub

    Sub Seleccionar()

        For Each oRow As DataRow In dt.Select(" ID = " & cbxMoneda.cbx.SelectedValue)
            Registro = oRow
            Seleccionado = True
        Next

    End Sub

    Public Function GetValue() As Decimal

        GetValue = 0

        For Each oRow As DataRow In dt.Select(" ID = " & cbxMoneda.cbx.SelectedValue)
            GetValue = CSistema.FormatoMonedaBaseDatos(oRow("Cotizacion"))
        Next

    End Function

    Public Sub SetValue(ByVal Moneda As String, ByVal Cotizacion As String)

        cbxMoneda.cbx.Text = Moneda
        txtCotizacion.SetValue(Cotizacion)
        RaiseEvent CambioMoneda()

    End Sub

    Sub Cargar()

        Dim dttemp As DataTable = CData.GetTable("VCotizacion")

        If dttemp Is Nothing Then
            txtCotizacion.SetValue(1)
            Exit Sub
        End If

        If dttemp Is Nothing Then
            txtCotizacion.SetValue(1)
            Exit Sub
        End If

        Dim Fecha As Date = DateAdd(DateInterval.Year, -10, Date.Now)
        Dim Encontrado As Boolean = False

        For Each orow As DataRow In dttemp.Rows

            If Fecha < orow("Fecha") Then
                If cbxMoneda.GetValue = orow("IDMoneda") Then
                    txtCotizacion.SetValue(orow("Cotizacion"))
                    Encontrado = True
                End If
            End If

            Fecha = orow("Fecha")

        Next

        If Encontrado = False Then
            txtCotizacion.SetValue(1)
        End If


    End Sub

    Private Sub ocxCotizacion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Inicializar()
    End Sub

    Private Sub cbxMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMoneda.PropertyChanged, cbxMoneda.LostFocus

        If cbxMoneda.cbx.SelectedValue Is Nothing Then
            txtCotizacion.SetValue(1)
            Exit Sub
        End If

        Dim Cotizacion As Decimal = 1
        For Each oRow As DataRow In dt.Select(" ID = " & cbxMoneda.cbx.SelectedValue)
            Cotizacion = oRow("Cotizacion")
        Next

        txtCotizacion.SetValue(Cotizacion)
        Seleccionar()

        RaiseEvent CambioMoneda()

    End Sub

    Private Sub txtCotizacion_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCotizacion.Enter
        Saltar = True
    End Sub

    Private Sub txtCotizacion_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCotizacion.Leave
        Saltar = False
    End Sub

    Private Sub cbxMoneda_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMoneda.Enter
        Saltar = False
    End Sub

    Private Sub cbxMoneda_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxMoneda.TeclaPrecionada
        If e.KeyCode = Keys.Enter Then
            txtCotizacion.txt.Focus()
        End If

        If cbxMoneda.cbx.SelectedValue Is Nothing Then
            txtCotizacion.SetValue(1)
            Exit Sub
        End If

        Dim Cotizacion As Decimal = 1
        For Each oRow As DataRow In dt.Select(" ID = " & cbxMoneda.cbx.SelectedValue)
            Cotizacion = oRow("Cotizacion")
        Next

        txtCotizacion.SetValue(Cotizacion)
        Seleccionar()

        RaiseEvent CambioMoneda()
    End Sub

    Private Sub cbxMoneda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxMoneda.Load

    End Sub

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxLlamadaTeleventa
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpHora = New System.Windows.Forms.DateTimePicker()
        Me.lblID = New System.Windows.Forms.Label()
        Me.lblVendedor = New System.Windows.Forms.Label()
        Me.lblTipoLlamada = New System.Windows.Forms.Label()
        Me.lblAtendido = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.cbxResultado = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.txtID = New ERP.ocxTXTString()
        Me.cbxLlamador = New ERP.ocxCBX()
        Me.cbxTipoLlamada = New ERP.ocxCBX()
        Me.txtAtendido = New ERP.ocxTXTString()
        Me.txtComentario = New ERP.ocxTXTString()
        Me.chkRealizado = New ERP.ocxCHK()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.rdbEntrante = New System.Windows.Forms.RadioButton()
        Me.rdbSaliente = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgv.Location = New System.Drawing.Point(17, 12)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgv.Size = New System.Drawing.Size(629, 167)
        Me.dgv.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(57, 293)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 115
        Me.Label3.Text = "Resultado:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(409, 276)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 114
        Me.Label2.Text = "Hora:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(390, 255)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 113
        Me.Label1.Text = "Llamar el:"
        '
        'dtpHora
        '
        Me.dtpHora.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpHora.Location = New System.Drawing.Point(450, 270)
        Me.dtpHora.Name = "dtpHora"
        Me.dtpHora.ShowUpDown = True
        Me.dtpHora.Size = New System.Drawing.Size(88, 20)
        Me.dtpHora.TabIndex = 114
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblID.Location = New System.Drawing.Point(91, 229)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 101
        Me.lblID.Text = "ID:"
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblVendedor.Location = New System.Drawing.Point(58, 272)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(53, 13)
        Me.lblVendedor.TabIndex = 109
        Me.lblVendedor.Text = "Llamador:"
        '
        'lblTipoLlamada
        '
        Me.lblTipoLlamada.AutoSize = True
        Me.lblTipoLlamada.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTipoLlamada.Location = New System.Drawing.Point(27, 251)
        Me.lblTipoLlamada.Name = "lblTipoLlamada"
        Me.lblTipoLlamada.Size = New System.Drawing.Size(89, 13)
        Me.lblTipoLlamada.TabIndex = 107
        Me.lblTipoLlamada.Text = "Tipo de Llamada:"
        '
        'lblAtendido
        '
        Me.lblAtendido.AutoSize = True
        Me.lblAtendido.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAtendido.Location = New System.Drawing.Point(40, 314)
        Me.lblAtendido.Name = "lblAtendido"
        Me.lblAtendido.Size = New System.Drawing.Size(70, 13)
        Me.lblAtendido.TabIndex = 105
        Me.lblAtendido.Text = "Atendido por:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDireccion.Location = New System.Drawing.Point(47, 335)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(63, 13)
        Me.lblDireccion.TabIndex = 103
        Me.lblDireccion.Text = "Comentario:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(260, 197)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 99
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(341, 197)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 100
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(179, 197)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 98
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(98, 197)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 97
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(17, 197)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 96
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 366)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(659, 22)
        Me.StatusStrip1.TabIndex = 117
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'cbxResultado
        '
        Me.cbxResultado.CampoWhere = Nothing
        Me.cbxResultado.CargarUnaSolaVez = False
        Me.cbxResultado.DataDisplayMember = Nothing
        Me.cbxResultado.DataFilter = Nothing
        Me.cbxResultado.DataOrderBy = Nothing
        Me.cbxResultado.DataSource = Nothing
        Me.cbxResultado.DataValueMember = Nothing
        Me.cbxResultado.dtSeleccionado = Nothing
        Me.cbxResultado.FormABM = Nothing
        Me.cbxResultado.Indicaciones = Nothing
        Me.cbxResultado.Location = New System.Drawing.Point(115, 289)
        Me.cbxResultado.Name = "cbxResultado"
        Me.cbxResultado.SeleccionMultiple = False
        Me.cbxResultado.SeleccionObligatoria = False
        Me.cbxResultado.Size = New System.Drawing.Size(238, 21)
        Me.cbxResultado.SoloLectura = True
        Me.cbxResultado.TabIndex = 110
        Me.cbxResultado.TabStop = False
        Me.cbxResultado.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(CType(0, Long))
        Me.txtFecha.Location = New System.Drawing.Point(450, 248)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(67, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 113
        '
        'txtID
        '
        Me.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Enabled = False
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(115, 226)
        Me.txtID.Multilinea = False
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(58, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 102
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtID.Texto = ""
        '
        'cbxLlamador
        '
        Me.cbxLlamador.CampoWhere = Nothing
        Me.cbxLlamador.CargarUnaSolaVez = False
        Me.cbxLlamador.DataDisplayMember = Nothing
        Me.cbxLlamador.DataFilter = Nothing
        Me.cbxLlamador.DataOrderBy = Nothing
        Me.cbxLlamador.DataSource = Nothing
        Me.cbxLlamador.DataValueMember = Nothing
        Me.cbxLlamador.dtSeleccionado = Nothing
        Me.cbxLlamador.FormABM = Nothing
        Me.cbxLlamador.Indicaciones = Nothing
        Me.cbxLlamador.Location = New System.Drawing.Point(115, 268)
        Me.cbxLlamador.Name = "cbxLlamador"
        Me.cbxLlamador.SeleccionMultiple = False
        Me.cbxLlamador.SeleccionObligatoria = False
        Me.cbxLlamador.Size = New System.Drawing.Size(238, 21)
        Me.cbxLlamador.SoloLectura = True
        Me.cbxLlamador.TabIndex = 109
        Me.cbxLlamador.TabStop = False
        Me.cbxLlamador.Texto = ""
        '
        'cbxTipoLlamada
        '
        Me.cbxTipoLlamada.CampoWhere = Nothing
        Me.cbxTipoLlamada.CargarUnaSolaVez = False
        Me.cbxTipoLlamada.DataDisplayMember = Nothing
        Me.cbxTipoLlamada.DataFilter = Nothing
        Me.cbxTipoLlamada.DataOrderBy = Nothing
        Me.cbxTipoLlamada.DataSource = Nothing
        Me.cbxTipoLlamada.DataValueMember = Nothing
        Me.cbxTipoLlamada.dtSeleccionado = Nothing
        Me.cbxTipoLlamada.FormABM = Nothing
        Me.cbxTipoLlamada.Indicaciones = Nothing
        Me.cbxTipoLlamada.Location = New System.Drawing.Point(115, 247)
        Me.cbxTipoLlamada.Name = "cbxTipoLlamada"
        Me.cbxTipoLlamada.SeleccionMultiple = False
        Me.cbxTipoLlamada.SeleccionObligatoria = False
        Me.cbxTipoLlamada.Size = New System.Drawing.Size(238, 21)
        Me.cbxTipoLlamada.SoloLectura = True
        Me.cbxTipoLlamada.TabIndex = 108
        Me.cbxTipoLlamada.TabStop = False
        Me.cbxTipoLlamada.Texto = ""
        '
        'txtAtendido
        '
        Me.txtAtendido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAtendido.Color = System.Drawing.Color.Empty
        Me.txtAtendido.Indicaciones = Nothing
        Me.txtAtendido.Location = New System.Drawing.Point(115, 310)
        Me.txtAtendido.Multilinea = False
        Me.txtAtendido.Name = "txtAtendido"
        Me.txtAtendido.Size = New System.Drawing.Size(238, 21)
        Me.txtAtendido.SoloLectura = True
        Me.txtAtendido.TabIndex = 111
        Me.txtAtendido.TabStop = False
        Me.txtAtendido.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtAtendido.Texto = ""
        '
        'txtComentario
        '
        Me.txtComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComentario.Color = System.Drawing.Color.Empty
        Me.txtComentario.Indicaciones = Nothing
        Me.txtComentario.Location = New System.Drawing.Point(115, 331)
        Me.txtComentario.Multilinea = False
        Me.txtComentario.Name = "txtComentario"
        Me.txtComentario.Size = New System.Drawing.Size(423, 21)
        Me.txtComentario.SoloLectura = True
        Me.txtComentario.TabIndex = 112
        Me.txtComentario.TabStop = False
        Me.txtComentario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComentario.Texto = ""
        '
        'chkRealizado
        '
        Me.chkRealizado.BackColor = System.Drawing.Color.Transparent
        Me.chkRealizado.Color = System.Drawing.Color.Empty
        Me.chkRealizado.Location = New System.Drawing.Point(450, 295)
        Me.chkRealizado.Name = "chkRealizado"
        Me.chkRealizado.Size = New System.Drawing.Size(23, 14)
        Me.chkRealizado.SoloLectura = False
        Me.chkRealizado.TabIndex = 118
        Me.chkRealizado.Texto = ""
        Me.chkRealizado.Valor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(385, 296)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(57, 13)
        Me.Label4.TabIndex = 119
        Me.Label4.Text = "Realizado:"
        '
        'rdbEntrante
        '
        Me.rdbEntrante.AutoSize = True
        Me.rdbEntrante.Location = New System.Drawing.Point(448, 310)
        Me.rdbEntrante.Name = "rdbEntrante"
        Me.rdbEntrante.Size = New System.Drawing.Size(65, 17)
        Me.rdbEntrante.TabIndex = 120
        Me.rdbEntrante.Text = "Entrante"
        Me.rdbEntrante.UseVisualStyleBackColor = True
        '
        'rdbSaliente
        '
        Me.rdbSaliente.AutoSize = True
        Me.rdbSaliente.Checked = True
        Me.rdbSaliente.Location = New System.Drawing.Point(519, 310)
        Me.rdbSaliente.Name = "rdbSaliente"
        Me.rdbSaliente.Size = New System.Drawing.Size(63, 17)
        Me.rdbSaliente.TabIndex = 121
        Me.rdbSaliente.TabStop = True
        Me.rdbSaliente.Text = "Saliente"
        Me.rdbSaliente.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(411, 314)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 122
        Me.Label5.Text = "Tipo:"
        '
        'ocxLlamadaTeleventa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.rdbSaliente)
        Me.Controls.Add(Me.rdbEntrante)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.chkRealizado)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.cbxResultado)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtpHora)
        Me.Controls.Add(Me.txtFecha)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.cbxLlamador)
        Me.Controls.Add(Me.cbxTipoLlamada)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lblTipoLlamada)
        Me.Controls.Add(Me.lblAtendido)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.txtAtendido)
        Me.Controls.Add(Me.txtComentario)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.dgv)
        Me.Name = "ocxLlamadaTeleventa"
        Me.Size = New System.Drawing.Size(659, 388)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents cbxResultado As ERP.ocxCBX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents txtID As ERP.ocxTXTString
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents cbxLlamador As ERP.ocxCBX
    Friend WithEvents cbxTipoLlamada As ERP.ocxCBX
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lblTipoLlamada As System.Windows.Forms.Label
    Friend WithEvents lblAtendido As System.Windows.Forms.Label
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents txtAtendido As ERP.ocxTXTString
    Friend WithEvents txtComentario As ERP.ocxTXTString
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkRealizado As ERP.ocxCHK
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents rdbSaliente As System.Windows.Forms.RadioButton
    Friend WithEvents rdbEntrante As System.Windows.Forms.RadioButton

End Class

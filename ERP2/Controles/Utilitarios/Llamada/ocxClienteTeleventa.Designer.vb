﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxClienteTeleventa
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblNombreFantasia = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblTelefonos = New System.Windows.Forms.Label()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.lblVendedor = New System.Windows.Forms.Label()
        Me.lblLimiteCredito = New System.Windows.Forms.Label()
        Me.lblPlazoCredito = New System.Windows.Forms.Label()
        Me.lblDescuentoSimbolo = New System.Windows.Forms.Label()
        Me.lblDesuento = New System.Windows.Forms.Label()
        Me.PnlCondicionVenta = New System.Windows.Forms.Panel()
        Me.rdbContado = New System.Windows.Forms.RadioButton()
        Me.rdbCredito = New System.Windows.Forms.RadioButton()
        Me.lblCondicionVenta = New System.Windows.Forms.Label()
        Me.lblRUC = New System.Windows.Forms.Label()
        Me.lblTipoCliente = New System.Windows.Forms.Label()
        Me.lblZonaVenta = New System.Windows.Forms.Label()
        Me.lblBarrio = New System.Windows.Forms.Label()
        Me.lblCiudad = New System.Windows.Forms.Label()
        Me.lblDepartamento = New System.Windows.Forms.Label()
        Me.lblPais = New System.Windows.Forms.Label()
        Me.txtFechaUltimaCompra = New System.Windows.Forms.TextBox()
        Me.lblUltimaCompra = New System.Windows.Forms.Label()
        Me.txtUsuarioAlta = New System.Windows.Forms.TextBox()
        Me.txtFechaAlta = New System.Windows.Forms.TextBox()
        Me.lblAlta = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblObservacion = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtEmail = New ERP.ocxTXTString()
        Me.txtComentario = New ERP.ocxTXTString()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.cbxBarrio = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.cbxPais = New ERP.ocxCBX()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.chkCI = New ERP.ocxCHK()
        Me.txtRUC = New ERP.ocxTXTString()
        Me.txtPlazoCredito = New ERP.ocxTXTNumeric()
        Me.txtDescuento = New ERP.ocxTXTNumeric()
        Me.txtLimiteCredito = New ERP.ocxTXTNumeric()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.txtTelefonos = New ERP.ocxTXTString()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.txtNombreFantasia = New ERP.ocxTXTString()
        Me.cbxEstado = New ERP.ocxCBX()
        Me.lblCelular = New System.Windows.Forms.Label()
        Me.txtCelulares = New ERP.ocxTXTString()
        Me.txtSaldo = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PnlCondicionVenta.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblNombreFantasia
        '
        Me.lblNombreFantasia.AutoSize = True
        Me.lblNombreFantasia.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblNombreFantasia.Location = New System.Drawing.Point(3, 17)
        Me.lblNombreFantasia.Name = "lblNombreFantasia"
        Me.lblNombreFantasia.Size = New System.Drawing.Size(90, 13)
        Me.lblNombreFantasia.TabIndex = 1
        Me.lblNombreFantasia.Text = "Nombre Fantasia:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = True
        Me.lblDireccion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblDireccion.Location = New System.Drawing.Point(3, 62)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(55, 13)
        Me.lblDireccion.TabIndex = 6
        Me.lblDireccion.Text = "Direccion:"
        '
        'lblTelefonos
        '
        Me.lblTelefonos.AutoSize = True
        Me.lblTelefonos.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblTelefonos.Location = New System.Drawing.Point(3, 86)
        Me.lblTelefonos.Name = "lblTelefonos"
        Me.lblTelefonos.Size = New System.Drawing.Size(57, 13)
        Me.lblTelefonos.TabIndex = 8
        Me.lblTelefonos.Text = "Telefonos:"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(3, 109)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(35, 13)
        Me.lblEmail.TabIndex = 9
        Me.lblEmail.Text = "Email:"
        '
        'lblVendedor
        '
        Me.lblVendedor.AutoSize = True
        Me.lblVendedor.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblVendedor.Location = New System.Drawing.Point(4, 133)
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Size = New System.Drawing.Size(56, 13)
        Me.lblVendedor.TabIndex = 24
        Me.lblVendedor.Text = "Vendedor:"
        '
        'lblLimiteCredito
        '
        Me.lblLimiteCredito.AutoSize = True
        Me.lblLimiteCredito.Location = New System.Drawing.Point(4, 159)
        Me.lblLimiteCredito.Name = "lblLimiteCredito"
        Me.lblLimiteCredito.Size = New System.Drawing.Size(73, 13)
        Me.lblLimiteCredito.TabIndex = 25
        Me.lblLimiteCredito.Text = "Limite Crédito:"
        '
        'lblPlazoCredito
        '
        Me.lblPlazoCredito.AutoSize = True
        Me.lblPlazoCredito.Location = New System.Drawing.Point(5, 183)
        Me.lblPlazoCredito.Name = "lblPlazoCredito"
        Me.lblPlazoCredito.Size = New System.Drawing.Size(72, 13)
        Me.lblPlazoCredito.TabIndex = 27
        Me.lblPlazoCredito.Text = "Plazo Credito:"
        '
        'lblDescuentoSimbolo
        '
        Me.lblDescuentoSimbolo.AutoSize = True
        Me.lblDescuentoSimbolo.Enabled = False
        Me.lblDescuentoSimbolo.Location = New System.Drawing.Point(225, 182)
        Me.lblDescuentoSimbolo.Name = "lblDescuentoSimbolo"
        Me.lblDescuentoSimbolo.Size = New System.Drawing.Size(15, 13)
        Me.lblDescuentoSimbolo.TabIndex = 33
        Me.lblDescuentoSimbolo.Text = "%"
        '
        'lblDesuento
        '
        Me.lblDesuento.AutoSize = True
        Me.lblDesuento.Enabled = False
        Me.lblDesuento.Location = New System.Drawing.Point(125, 182)
        Me.lblDesuento.Name = "lblDesuento"
        Me.lblDesuento.Size = New System.Drawing.Size(62, 13)
        Me.lblDesuento.TabIndex = 31
        Me.lblDesuento.Text = "Descuento:"
        '
        'PnlCondicionVenta
        '
        Me.PnlCondicionVenta.Controls.Add(Me.rdbContado)
        Me.PnlCondicionVenta.Controls.Add(Me.rdbCredito)
        Me.PnlCondicionVenta.Location = New System.Drawing.Point(93, 204)
        Me.PnlCondicionVenta.Name = "PnlCondicionVenta"
        Me.PnlCondicionVenta.Size = New System.Drawing.Size(156, 24)
        Me.PnlCondicionVenta.TabIndex = 56
        '
        'rdbContado
        '
        Me.rdbContado.AutoSize = True
        Me.rdbContado.BackColor = System.Drawing.Color.Transparent
        Me.rdbContado.Enabled = False
        Me.rdbContado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbContado.Location = New System.Drawing.Point(78, 3)
        Me.rdbContado.Name = "rdbContado"
        Me.rdbContado.Size = New System.Drawing.Size(65, 17)
        Me.rdbContado.TabIndex = 1
        Me.rdbContado.TabStop = True
        Me.rdbContado.Text = "Contado"
        Me.rdbContado.UseVisualStyleBackColor = False
        '
        'rdbCredito
        '
        Me.rdbCredito.AutoSize = True
        Me.rdbCredito.BackColor = System.Drawing.Color.Transparent
        Me.rdbCredito.Enabled = False
        Me.rdbCredito.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbCredito.Location = New System.Drawing.Point(5, 3)
        Me.rdbCredito.Name = "rdbCredito"
        Me.rdbCredito.Size = New System.Drawing.Size(58, 17)
        Me.rdbCredito.TabIndex = 0
        Me.rdbCredito.TabStop = True
        Me.rdbCredito.Text = "Credito"
        Me.rdbCredito.UseVisualStyleBackColor = False
        '
        'lblCondicionVenta
        '
        Me.lblCondicionVenta.AutoSize = True
        Me.lblCondicionVenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCondicionVenta.Location = New System.Drawing.Point(5, 209)
        Me.lblCondicionVenta.Name = "lblCondicionVenta"
        Me.lblCondicionVenta.Size = New System.Drawing.Size(69, 13)
        Me.lblCondicionVenta.TabIndex = 55
        Me.lblCondicionVenta.Text = "Cond. Venta:"
        '
        'lblRUC
        '
        Me.lblRUC.AutoSize = True
        Me.lblRUC.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblRUC.Location = New System.Drawing.Point(3, 39)
        Me.lblRUC.Name = "lblRUC"
        Me.lblRUC.Size = New System.Drawing.Size(33, 13)
        Me.lblRUC.TabIndex = 60
        Me.lblRUC.Text = "RUC:"
        '
        'lblTipoCliente
        '
        Me.lblTipoCliente.AutoSize = True
        Me.lblTipoCliente.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblTipoCliente.Location = New System.Drawing.Point(454, 50)
        Me.lblTipoCliente.Name = "lblTipoCliente"
        Me.lblTipoCliente.Size = New System.Drawing.Size(66, 13)
        Me.lblTipoCliente.TabIndex = 63
        Me.lblTipoCliente.Text = "Tipo Cliente:"
        '
        'lblZonaVenta
        '
        Me.lblZonaVenta.AutoSize = True
        Me.lblZonaVenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblZonaVenta.Location = New System.Drawing.Point(456, 155)
        Me.lblZonaVenta.Name = "lblZonaVenta"
        Me.lblZonaVenta.Size = New System.Drawing.Size(81, 13)
        Me.lblZonaVenta.TabIndex = 73
        Me.lblZonaVenta.Text = "Zona de Venta:"
        '
        'lblBarrio
        '
        Me.lblBarrio.AutoSize = True
        Me.lblBarrio.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblBarrio.Location = New System.Drawing.Point(456, 134)
        Me.lblBarrio.Name = "lblBarrio"
        Me.lblBarrio.Size = New System.Drawing.Size(37, 13)
        Me.lblBarrio.TabIndex = 72
        Me.lblBarrio.Text = "Barrio:"
        '
        'lblCiudad
        '
        Me.lblCiudad.AutoSize = True
        Me.lblCiudad.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCiudad.Location = New System.Drawing.Point(456, 113)
        Me.lblCiudad.Name = "lblCiudad"
        Me.lblCiudad.Size = New System.Drawing.Size(43, 13)
        Me.lblCiudad.TabIndex = 71
        Me.lblCiudad.Text = "Ciudad:"
        '
        'lblDepartamento
        '
        Me.lblDepartamento.AutoSize = True
        Me.lblDepartamento.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblDepartamento.Location = New System.Drawing.Point(456, 92)
        Me.lblDepartamento.Name = "lblDepartamento"
        Me.lblDepartamento.Size = New System.Drawing.Size(77, 13)
        Me.lblDepartamento.TabIndex = 70
        Me.lblDepartamento.Text = "Departamento:"
        '
        'lblPais
        '
        Me.lblPais.AutoSize = True
        Me.lblPais.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblPais.Location = New System.Drawing.Point(456, 71)
        Me.lblPais.Name = "lblPais"
        Me.lblPais.Size = New System.Drawing.Size(32, 13)
        Me.lblPais.TabIndex = 68
        Me.lblPais.Text = "País:"
        '
        'txtFechaUltimaCompra
        '
        Me.txtFechaUltimaCompra.BackColor = System.Drawing.Color.White
        Me.txtFechaUltimaCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFechaUltimaCompra.Enabled = False
        Me.txtFechaUltimaCompra.Location = New System.Drawing.Point(543, 200)
        Me.txtFechaUltimaCompra.Name = "txtFechaUltimaCompra"
        Me.txtFechaUltimaCompra.ReadOnly = True
        Me.txtFechaUltimaCompra.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaUltimaCompra.TabIndex = 76
        Me.txtFechaUltimaCompra.TabStop = False
        Me.txtFechaUltimaCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblUltimaCompra
        '
        Me.lblUltimaCompra.AutoSize = True
        Me.lblUltimaCompra.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblUltimaCompra.Location = New System.Drawing.Point(454, 204)
        Me.lblUltimaCompra.Name = "lblUltimaCompra"
        Me.lblUltimaCompra.Size = New System.Drawing.Size(62, 13)
        Me.lblUltimaCompra.TabIndex = 78
        Me.lblUltimaCompra.Text = "Ul. Compra:"
        '
        'txtUsuarioAlta
        '
        Me.txtUsuarioAlta.BackColor = System.Drawing.Color.White
        Me.txtUsuarioAlta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuarioAlta.Enabled = False
        Me.txtUsuarioAlta.Location = New System.Drawing.Point(616, 177)
        Me.txtUsuarioAlta.Name = "txtUsuarioAlta"
        Me.txtUsuarioAlta.ReadOnly = True
        Me.txtUsuarioAlta.Size = New System.Drawing.Size(117, 20)
        Me.txtUsuarioAlta.TabIndex = 75
        Me.txtUsuarioAlta.TabStop = False
        Me.txtUsuarioAlta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFechaAlta
        '
        Me.txtFechaAlta.BackColor = System.Drawing.Color.White
        Me.txtFechaAlta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFechaAlta.Enabled = False
        Me.txtFechaAlta.Location = New System.Drawing.Point(543, 177)
        Me.txtFechaAlta.Name = "txtFechaAlta"
        Me.txtFechaAlta.ReadOnly = True
        Me.txtFechaAlta.Size = New System.Drawing.Size(67, 20)
        Me.txtFechaAlta.TabIndex = 74
        Me.txtFechaAlta.TabStop = False
        Me.txtFechaAlta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblAlta
        '
        Me.lblAlta.AutoSize = True
        Me.lblAlta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblAlta.Location = New System.Drawing.Point(456, 181)
        Me.lblAlta.Name = "lblAlta"
        Me.lblAlta.Size = New System.Drawing.Size(28, 13)
        Me.lblAlta.TabIndex = 77
        Me.lblAlta.Text = "Alta:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblEstado.Location = New System.Drawing.Point(456, 16)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 80
        Me.lblEstado.Text = "Estado:"
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblObservacion.Location = New System.Drawing.Point(3, 238)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(70, 13)
        Me.lblObservacion.TabIndex = 81
        Me.lblObservacion.Text = "Observacion:"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(168, 264)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 104
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(249, 264)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 105
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        Me.btnEliminar.Visible = False
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(87, 264)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 103
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(6, 264)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 102
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(330, 264)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 101
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        Me.btnNuevo.Visible = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 289)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStrip1.Size = New System.Drawing.Size(831, 22)
        Me.StatusStrip1.TabIndex = 106
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.Color.White
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmail.Color = System.Drawing.Color.Empty
        Me.txtEmail.Enabled = False
        Me.txtEmail.Indicaciones = Nothing
        Me.txtEmail.Location = New System.Drawing.Point(93, 105)
        Me.txtEmail.Multilinea = False
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(348, 21)
        Me.txtEmail.SoloLectura = False
        Me.txtEmail.TabIndex = 107
        Me.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtEmail.Texto = ""
        '
        'txtComentario
        '
        Me.txtComentario.BackColor = System.Drawing.Color.White
        Me.txtComentario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComentario.Color = System.Drawing.Color.Empty
        Me.txtComentario.Enabled = False
        Me.txtComentario.Indicaciones = Nothing
        Me.txtComentario.Location = New System.Drawing.Point(93, 234)
        Me.txtComentario.Multilinea = False
        Me.txtComentario.Name = "txtComentario"
        Me.txtComentario.Size = New System.Drawing.Size(452, 21)
        Me.txtComentario.SoloLectura = False
        Me.txtComentario.TabIndex = 82
        Me.txtComentario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComentario.Texto = ""
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = Nothing
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = "Descripcion"
        Me.cbxZonaVenta.DataSource = "VZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.dtSeleccionado = Nothing
        Me.cbxZonaVenta.Enabled = False
        Me.cbxZonaVenta.FormABM = "frmZonaVenta"
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(543, 151)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionMultiple = False
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(135, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 69
        Me.cbxZonaVenta.Texto = ""
        '
        'cbxBarrio
        '
        Me.cbxBarrio.CampoWhere = Nothing
        Me.cbxBarrio.CargarUnaSolaVez = False
        Me.cbxBarrio.DataDisplayMember = Nothing
        Me.cbxBarrio.DataFilter = Nothing
        Me.cbxBarrio.DataOrderBy = Nothing
        Me.cbxBarrio.DataSource = Nothing
        Me.cbxBarrio.DataValueMember = Nothing
        Me.cbxBarrio.dtSeleccionado = Nothing
        Me.cbxBarrio.Enabled = False
        Me.cbxBarrio.FormABM = Nothing
        Me.cbxBarrio.Indicaciones = Nothing
        Me.cbxBarrio.Location = New System.Drawing.Point(543, 130)
        Me.cbxBarrio.Name = "cbxBarrio"
        Me.cbxBarrio.SeleccionMultiple = False
        Me.cbxBarrio.SeleccionObligatoria = False
        Me.cbxBarrio.Size = New System.Drawing.Size(135, 21)
        Me.cbxBarrio.SoloLectura = False
        Me.cbxBarrio.TabIndex = 67
        Me.cbxBarrio.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = Nothing
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = Nothing
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = Nothing
        Me.cbxCiudad.DataValueMember = Nothing
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(543, 109)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(135, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 66
        Me.cbxCiudad.Texto = ""
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = Nothing
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = Nothing
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = Nothing
        Me.cbxDepartamento.DataValueMember = Nothing
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.Enabled = False
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(543, 88)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(135, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 65
        Me.cbxDepartamento.Texto = ""
        '
        'cbxPais
        '
        Me.cbxPais.CampoWhere = Nothing
        Me.cbxPais.CargarUnaSolaVez = False
        Me.cbxPais.DataDisplayMember = Nothing
        Me.cbxPais.DataFilter = Nothing
        Me.cbxPais.DataOrderBy = Nothing
        Me.cbxPais.DataSource = Nothing
        Me.cbxPais.DataValueMember = Nothing
        Me.cbxPais.dtSeleccionado = Nothing
        Me.cbxPais.Enabled = False
        Me.cbxPais.FormABM = Nothing
        Me.cbxPais.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPais.Location = New System.Drawing.Point(543, 67)
        Me.cbxPais.Name = "cbxPais"
        Me.cbxPais.SeleccionMultiple = False
        Me.cbxPais.SeleccionObligatoria = False
        Me.cbxPais.Size = New System.Drawing.Size(135, 21)
        Me.cbxPais.SoloLectura = False
        Me.cbxPais.TabIndex = 64
        Me.cbxPais.Texto = ""
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = Nothing
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = Nothing
        Me.cbxTipoCliente.DataSource = "VTipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = "frmTipoCliente"
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(541, 40)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(240, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 62
        Me.cbxTipoCliente.Texto = ""
        '
        'chkCI
        '
        Me.chkCI.BackColor = System.Drawing.Color.Transparent
        Me.chkCI.Color = System.Drawing.Color.Empty
        Me.chkCI.Enabled = False
        Me.chkCI.Location = New System.Drawing.Point(174, 35)
        Me.chkCI.Name = "chkCI"
        Me.chkCI.Size = New System.Drawing.Size(34, 21)
        Me.chkCI.SoloLectura = False
        Me.chkCI.TabIndex = 61
        Me.chkCI.Texto = "CI"
        Me.chkCI.Valor = False
        '
        'txtRUC
        '
        Me.txtRUC.BackColor = System.Drawing.Color.White
        Me.txtRUC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUC.Color = System.Drawing.Color.Empty
        Me.txtRUC.Enabled = False
        Me.txtRUC.Indicaciones = Nothing
        Me.txtRUC.Location = New System.Drawing.Point(93, 35)
        Me.txtRUC.Multilinea = False
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.Size = New System.Drawing.Size(75, 21)
        Me.txtRUC.SoloLectura = False
        Me.txtRUC.TabIndex = 59
        Me.txtRUC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtRUC.Texto = ""
        '
        'txtPlazoCredito
        '
        Me.txtPlazoCredito.Color = System.Drawing.Color.Empty
        Me.txtPlazoCredito.Decimales = True
        Me.txtPlazoCredito.Enabled = False
        Me.txtPlazoCredito.Indicaciones = Nothing
        Me.txtPlazoCredito.Location = New System.Drawing.Point(93, 179)
        Me.txtPlazoCredito.Name = "txtPlazoCredito"
        Me.txtPlazoCredito.Size = New System.Drawing.Size(26, 21)
        Me.txtPlazoCredito.SoloLectura = False
        Me.txtPlazoCredito.TabIndex = 28
        Me.txtPlazoCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlazoCredito.Texto = "0"
        '
        'txtDescuento
        '
        Me.txtDescuento.Color = System.Drawing.Color.Empty
        Me.txtDescuento.Decimales = True
        Me.txtDescuento.Enabled = False
        Me.txtDescuento.Indicaciones = Nothing
        Me.txtDescuento.Location = New System.Drawing.Point(193, 178)
        Me.txtDescuento.Name = "txtDescuento"
        Me.txtDescuento.Size = New System.Drawing.Size(26, 21)
        Me.txtDescuento.SoloLectura = False
        Me.txtDescuento.TabIndex = 32
        Me.txtDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDescuento.Texto = "0"
        '
        'txtLimiteCredito
        '
        Me.txtLimiteCredito.Color = System.Drawing.Color.Empty
        Me.txtLimiteCredito.Decimales = True
        Me.txtLimiteCredito.Enabled = False
        Me.txtLimiteCredito.Indicaciones = Nothing
        Me.txtLimiteCredito.Location = New System.Drawing.Point(93, 155)
        Me.txtLimiteCredito.Name = "txtLimiteCredito"
        Me.txtLimiteCredito.Size = New System.Drawing.Size(135, 21)
        Me.txtLimiteCredito.SoloLectura = False
        Me.txtLimiteCredito.TabIndex = 26
        Me.txtLimiteCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLimiteCredito.Texto = "0"
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = Nothing
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = Nothing
        Me.cbxVendedor.DataSource = ""
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = "frmVendedor"
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(93, 129)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(156, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 23
        Me.cbxVendedor.Texto = ""
        '
        'txtTelefonos
        '
        Me.txtTelefonos.BackColor = System.Drawing.Color.White
        Me.txtTelefonos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelefonos.Color = System.Drawing.Color.Empty
        Me.txtTelefonos.Enabled = False
        Me.txtTelefonos.Indicaciones = Nothing
        Me.txtTelefonos.Location = New System.Drawing.Point(93, 82)
        Me.txtTelefonos.Multilinea = False
        Me.txtTelefonos.Name = "txtTelefonos"
        Me.txtTelefonos.Size = New System.Drawing.Size(156, 21)
        Me.txtTelefonos.SoloLectura = False
        Me.txtTelefonos.TabIndex = 7
        Me.txtTelefonos.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtTelefonos.Texto = ""
        '
        'txtDireccion
        '
        Me.txtDireccion.BackColor = System.Drawing.Color.White
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Empty
        Me.txtDireccion.Enabled = False
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(93, 58)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(348, 21)
        Me.txtDireccion.SoloLectura = False
        Me.txtDireccion.TabIndex = 5
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'txtNombreFantasia
        '
        Me.txtNombreFantasia.BackColor = System.Drawing.Color.White
        Me.txtNombreFantasia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreFantasia.Color = System.Drawing.Color.Empty
        Me.txtNombreFantasia.Enabled = False
        Me.txtNombreFantasia.Indicaciones = Nothing
        Me.txtNombreFantasia.Location = New System.Drawing.Point(93, 13)
        Me.txtNombreFantasia.Multilinea = False
        Me.txtNombreFantasia.Name = "txtNombreFantasia"
        Me.txtNombreFantasia.Size = New System.Drawing.Size(348, 21)
        Me.txtNombreFantasia.SoloLectura = False
        Me.txtNombreFantasia.TabIndex = 2
        Me.txtNombreFantasia.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNombreFantasia.Texto = ""
        '
        'cbxEstado
        '
        Me.cbxEstado.CampoWhere = Nothing
        Me.cbxEstado.CargarUnaSolaVez = False
        Me.cbxEstado.DataDisplayMember = Nothing
        Me.cbxEstado.DataFilter = Nothing
        Me.cbxEstado.DataOrderBy = Nothing
        Me.cbxEstado.DataSource = Nothing
        Me.cbxEstado.DataValueMember = Nothing
        Me.cbxEstado.dtSeleccionado = Nothing
        Me.cbxEstado.Enabled = False
        Me.cbxEstado.FormABM = Nothing
        Me.cbxEstado.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxEstado.Location = New System.Drawing.Point(541, 13)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.SeleccionMultiple = False
        Me.cbxEstado.SeleccionObligatoria = False
        Me.cbxEstado.Size = New System.Drawing.Size(197, 21)
        Me.cbxEstado.SoloLectura = False
        Me.cbxEstado.TabIndex = 108
        Me.cbxEstado.Texto = ""
        '
        'lblCelular
        '
        Me.lblCelular.AutoSize = True
        Me.lblCelular.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblCelular.Location = New System.Drawing.Point(254, 87)
        Me.lblCelular.Name = "lblCelular"
        Me.lblCelular.Size = New System.Drawing.Size(53, 13)
        Me.lblCelular.TabIndex = 110
        Me.lblCelular.Text = "Celulares:"
        '
        'txtCelulares
        '
        Me.txtCelulares.BackColor = System.Drawing.Color.White
        Me.txtCelulares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCelulares.Color = System.Drawing.Color.Empty
        Me.txtCelulares.Enabled = False
        Me.txtCelulares.Indicaciones = Nothing
        Me.txtCelulares.Location = New System.Drawing.Point(317, 82)
        Me.txtCelulares.Multilinea = False
        Me.txtCelulares.Name = "txtCelulares"
        Me.txtCelulares.Size = New System.Drawing.Size(124, 21)
        Me.txtCelulares.SoloLectura = False
        Me.txtCelulares.TabIndex = 109
        Me.txtCelulares.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtCelulares.Texto = ""
        '
        'txtSaldo
        '
        Me.txtSaldo.Color = System.Drawing.Color.Empty
        Me.txtSaldo.Decimales = True
        Me.txtSaldo.Enabled = False
        Me.txtSaldo.Indicaciones = Nothing
        Me.txtSaldo.Location = New System.Drawing.Point(300, 155)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.Size = New System.Drawing.Size(141, 21)
        Me.txtSaldo.SoloLectura = False
        Me.txtSaldo.TabIndex = 111
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(257, 160)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 112
        Me.Label1.Text = "Saldo:"
        '
        'ocxClienteTeleventa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtSaldo)
        Me.Controls.Add(Me.lblCelular)
        Me.Controls.Add(Me.txtCelulares)
        Me.Controls.Add(Me.cbxEstado)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.txtComentario)
        Me.Controls.Add(Me.lblObservacion)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.txtFechaUltimaCompra)
        Me.Controls.Add(Me.lblUltimaCompra)
        Me.Controls.Add(Me.txtUsuarioAlta)
        Me.Controls.Add(Me.txtFechaAlta)
        Me.Controls.Add(Me.lblAlta)
        Me.Controls.Add(Me.cbxZonaVenta)
        Me.Controls.Add(Me.cbxBarrio)
        Me.Controls.Add(Me.cbxCiudad)
        Me.Controls.Add(Me.cbxDepartamento)
        Me.Controls.Add(Me.cbxPais)
        Me.Controls.Add(Me.lblZonaVenta)
        Me.Controls.Add(Me.lblBarrio)
        Me.Controls.Add(Me.lblCiudad)
        Me.Controls.Add(Me.lblDepartamento)
        Me.Controls.Add(Me.lblPais)
        Me.Controls.Add(Me.cbxTipoCliente)
        Me.Controls.Add(Me.lblTipoCliente)
        Me.Controls.Add(Me.chkCI)
        Me.Controls.Add(Me.txtRUC)
        Me.Controls.Add(Me.lblRUC)
        Me.Controls.Add(Me.PnlCondicionVenta)
        Me.Controls.Add(Me.lblCondicionVenta)
        Me.Controls.Add(Me.lblPlazoCredito)
        Me.Controls.Add(Me.lblDescuentoSimbolo)
        Me.Controls.Add(Me.lblDesuento)
        Me.Controls.Add(Me.txtPlazoCredito)
        Me.Controls.Add(Me.txtDescuento)
        Me.Controls.Add(Me.lblLimiteCredito)
        Me.Controls.Add(Me.txtLimiteCredito)
        Me.Controls.Add(Me.cbxVendedor)
        Me.Controls.Add(Me.lblVendedor)
        Me.Controls.Add(Me.lblEmail)
        Me.Controls.Add(Me.txtTelefonos)
        Me.Controls.Add(Me.lblTelefonos)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.lblDireccion)
        Me.Controls.Add(Me.txtNombreFantasia)
        Me.Controls.Add(Me.lblNombreFantasia)
        Me.Name = "ocxClienteTeleventa"
        Me.Size = New System.Drawing.Size(831, 311)
        Me.PnlCondicionVenta.ResumeLayout(False)
        Me.PnlCondicionVenta.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtNombreFantasia As ERP.ocxTXTString
    Friend WithEvents lblNombreFantasia As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents lblDireccion As System.Windows.Forms.Label
    Friend WithEvents txtTelefonos As ERP.ocxTXTString
    Friend WithEvents lblTelefonos As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents lblVendedor As System.Windows.Forms.Label
    Friend WithEvents lblLimiteCredito As System.Windows.Forms.Label
    Friend WithEvents txtLimiteCredito As ERP.ocxTXTNumeric
    Friend WithEvents lblPlazoCredito As System.Windows.Forms.Label
    Friend WithEvents lblDescuentoSimbolo As System.Windows.Forms.Label
    Friend WithEvents lblDesuento As System.Windows.Forms.Label
    Friend WithEvents txtPlazoCredito As ERP.ocxTXTNumeric
    Friend WithEvents txtDescuento As ERP.ocxTXTNumeric
    Friend WithEvents PnlCondicionVenta As System.Windows.Forms.Panel
    Friend WithEvents rdbContado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents lblCondicionVenta As System.Windows.Forms.Label
    Friend WithEvents chkCI As ERP.ocxCHK
    Friend WithEvents txtRUC As ERP.ocxTXTString
    Friend WithEvents lblRUC As System.Windows.Forms.Label
    Friend WithEvents cbxTipoCliente As ERP.ocxCBX
    Friend WithEvents lblTipoCliente As System.Windows.Forms.Label
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents cbxBarrio As ERP.ocxCBX
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxDepartamento As ERP.ocxCBX
    Friend WithEvents cbxPais As ERP.ocxCBX
    Friend WithEvents lblZonaVenta As System.Windows.Forms.Label
    Friend WithEvents lblBarrio As System.Windows.Forms.Label
    Friend WithEvents lblCiudad As System.Windows.Forms.Label
    Friend WithEvents lblDepartamento As System.Windows.Forms.Label
    Friend WithEvents lblPais As System.Windows.Forms.Label
    Friend WithEvents txtFechaUltimaCompra As System.Windows.Forms.TextBox
    Friend WithEvents lblUltimaCompra As System.Windows.Forms.Label
    Friend WithEvents txtUsuarioAlta As System.Windows.Forms.TextBox
    Friend WithEvents txtFechaAlta As System.Windows.Forms.TextBox
    Friend WithEvents lblAlta As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txtComentario As ERP.ocxTXTString
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtEmail As ERP.ocxTXTString
    Friend WithEvents cbxEstado As ERP.ocxCBX
    Friend WithEvents lblCelular As System.Windows.Forms.Label
    Friend WithEvents txtCelulares As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSaldo As ERP.ocxTXTNumeric

End Class

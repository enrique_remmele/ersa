﻿Public Class ocxVisitaTeleventa
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'PROPIEDADES
    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property
    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False

        'Funciones
        CargarInformacion()
        CSistema.InicializaControles(Me)
        Iniciado = True
        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        dgv.Focus()

    End Sub

    Sub CargarInformacion()
        'Llamador
        CSistema.SqlToComboBox(cbxLlamador.cbx, "Select ID, Nombres From Vendedor Order By 2")


        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtID)
        CSistema.CargaControl(vControles, txtFecha)
        CSistema.CargaControl(vControles, dtpHora)
        CSistema.CargaControl(vControles, cbxLlamador)
        CSistema.CargaControl(vControles, txtAtendido)
        CSistema.CargaControl(vControles, txtComentario)
        CSistema.CargaControl(vControles, txtFechaProxima)
        CSistema.CargaControl(vControles, dtpHoraProxima)

        'Cargamos los paises en el lv
        Listar()
    End Sub

    'Para cargar la informacion para editar
    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgv.SelectedRows.Count = 0 Then
            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = dgv.SelectedRows(0).Cells(0).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From vVisita Where Estado = 'True' and ID=" & ID & " And IDCliente=" & IDCliente)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtID.txt.Text = oRow("ID").ToString
            cbxLlamador.cbx.Text = oRow("Vendedor").ToString
            txtFecha.txt.Text = oRow("FechaVisita").ToString
            dtpHora.Text = oRow("HoraVisita").ToString
            cbxLlamador.cbx.Text = oRow("Vendedor").ToString
            txtAtendido.txt.Text = oRow("Atendido").ToString
            txtComentario.txt.Text = oRow("Comentario").ToString
            txtFechaProxima.txt.Text = oRow("FechaProximaVisita").ToString
            dtpHoraProxima.Text = oRow("HoraProximaVisita").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        CSistema.SqlToDataGrid(dgv, "Select ID, FechaVisita as 'Fecha de visita', HoraVisita as 'Hora de visita',Vendedor, FechaProximaVisita as 'Proxima visita', HoraProximaVisita as 'Hora' From vVisita Where Estado = 'True' and IDCliente=" & IDCliente & " Order By ID")

        'Ajustar la ultima columna
        dgv.Columns(dgv.Columns.Count - 1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub InicializarControles()

        'TextBox
        txtAtendido.txt.Clear()
        txtComentario.txt.Clear()
        txtFecha.Hoy()
        txtFechaProxima.Hoy()

        'ComoBox
        'Llamador
        CSistema.SqlToComboBox(cbxLlamador.cbx, "Select ID, Nombres From Vendedor Order By 2")
        cbxLlamador.txt.Clear()

        'ToolStripStatusLabel
        tsslEstado.Text = ""

        'Funciones
        If Me.vNuevo = True Then
            txtID.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From Visita "), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From Visita Where IDCliente=" & IDCliente), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        'txtSucursal.txt.Focus()
        'txtSucursal.txt.SelectAll()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        If cbxLlamador.cbx.SelectedValue = False Then
            Dim mensaje As String = "Seleccione un Vendedor valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If dgv.SelectedRows.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(dgv, mensaje)
                ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtID.txt.Text
        If vNuevo = True Then
            ID = 0
        End If

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)3

        'CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", IDCliente, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaVisita", CSistema.FormatoFechaBaseDatos(txtFecha.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@HoraVisita", CSistema.FormatoFechaBaseDatos(dtpHora.Value.ToShortTimeString, False, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDVendedor", cbxLlamador.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@FechaProxima", CSistema.FormatoFechaBaseDatos(txtFechaProxima.GetValue.ToShortDateString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@HoraProxima", CSistema.FormatoFechaBaseDatos(dtpHoraProxima.Value.ToShortTimeString, False, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Atendido", txtAtendido.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Comentario", txtComentario.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Estado", True, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpVisita", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            Listar()
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(sender As System.Object, e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        cbxLlamador.Focus()
        cbxLlamador.txt.SelectAll()
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub ocxVisitaTeleventa_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me)
        End If
    End Sub

    Private Sub dgv_SelectionChanged(sender As System.Object, e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub
End Class

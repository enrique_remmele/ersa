﻿Public Class ocxClienteTeleventa
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim IDVendedor As Integer

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'PROPIEDADES
    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property
    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    'PROPIEDADES
    Private dtDocumentosValue As DataTable
    Public Property dtDocumentos() As DataTable
        Get
            Return dtDocumentosValue
        End Get
        Set(ByVal value As DataTable)
            dtDocumentosValue = value
        End Set
    End Property
    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False

        If CadenaConexion Is Nothing Then
            CadenaConexion = VGCadenaConexion
        End If

        'Funciones
        'CargarInformacion()
        CSistema.InicializaControles(Me)
        Iniciado = True
        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Listar()

    End Sub
    Sub Listar()
        'Estdo de clientes
        CSistema.SqlToComboBox(cbxEstado.cbx, "Select ID, Descripcion From EstadoCliente Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Listar Paises
        CSistema.SqlToComboBox(cbxPais.cbx, "Select ID, Descripcion From Pais Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Listar Departamentos
        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID, Descripcion From Departamento Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Listar Ciudades
        CSistema.SqlToComboBox(cbxCiudad.cbx, "Select ID, Descripcion From Ciudad Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Listar Barrios
        CSistema.SqlToComboBox(cbxBarrio.cbx, "Select ID, Descripcion From Barrio Order By Orden Desc, Descripcion Asc", CadenaConexion)

        'Vendedor
        CSistema.SqlToComboBox(cbxVendedor.cbx, CData.GetTable("VVendedor", " IDSucursal=" & vgIDSucursal), "ID", "Nombres")

        'Listar Barrios
        CSistema.SqlToComboBox(cbxEstado.cbx, "Select ID, Descripcion From EstadoCliente Order By Orden Desc, Descripcion Asc", CadenaConexion)


        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtNombreFantasia)
        CSistema.CargaControl(vControles, txtRUC)
        CSistema.CargaControl(vControles, chkCI)
        CSistema.CargaControl(vControles, txtDireccion)
        CSistema.CargaControl(vControles, txtTelefonos)
        CSistema.CargaControl(vControles, txtCelulares)
        CSistema.CargaControl(vControles, txtEmail)
        CSistema.CargaControl(vControles, cbxVendedor)
        CSistema.CargaControl(vControles, txtLimiteCredito)
        CSistema.CargaControl(vControles, txtSaldo)

        CSistema.CargaControl(vControles, txtPlazoCredito)
        CSistema.CargaControl(vControles, txtDescuento)
        CSistema.CargaControl(vControles, rdbCredito)
        CSistema.CargaControl(vControles, rdbContado)
        CSistema.CargaControl(vControles, txtComentario)
        CSistema.CargaControl(vControles, cbxEstado)
        CSistema.CargaControl(vControles, cbxTipoCliente)
        CSistema.CargaControl(vControles, cbxPais)

        CSistema.CargaControl(vControles, cbxDepartamento)
        CSistema.CargaControl(vControles, cbxCiudad)
        CSistema.CargaControl(vControles, cbxBarrio)
        CSistema.CargaControl(vControles, cbxZonaVenta)
        CSistema.CargaControl(vControles, txtFechaAlta)
        CSistema.CargaControl(vControles, txtUsuarioAlta)
        CSistema.CargaControl(vControles, txtFechaUltimaCompra)



        'Cargamos los paises en el lv
        'Listar()
    End Sub

    Sub ListarCliente()
        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From vClienteTeleventa where ID =" & IDCliente, CadenaConexion)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count = 0 Then
            Exit Sub
        Else

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            txtNombreFantasia.txt.Text = oRow("NombreFantasia").ToString
            txtRUC.txt.Text = oRow("NombreFantasia").ToString
            chkCI.Valor = oRow("CI")
            txtRUC.txt.Text = oRow("RUC")
            txtDireccion.txt.Text = oRow("Direccion")
            txtTelefonos.txt.Text = oRow("Telefono")
            txtCelulares.txt.Text = oRow("Celular")
            txtEmail.Text = oRow("Email")
            'IDVendedor = oRow("IDVendedor").ToString
            cbxVendedor.cbx.Text = oRow("Vendedor").ToString
            txtLimiteCredito.txt.Text = oRow("LimiteCredito")
            txtSaldo.txt.Text = oRow("SaldoCredito")
            txtPlazoCredito.txt.Text = oRow("PlazoCredito")
            rdbContado.Checked = CBool(oRow("Contado").ToString)
            rdbCredito.Checked = CBool(oRow("Credito").ToString)
            txtComentario.txt.Text = oRow("Observacion")
            cbxEstado.cbx.Text = oRow("Estado").ToString
            cbxTipoCliente.cbx.Text = oRow("TipoCliente").ToString
            cbxPais.cbx.Text = oRow("Pais").ToString
            cbxDepartamento.cbx.Text = oRow("Departamento").ToString

            cbxCiudad.cbx.Text = oRow("Ciudad").ToString
            cbxBarrio.cbx.Text = oRow("Barrio").ToString
            cbxZonaVenta.cbx.Text = oRow("Zona").ToString
            cbxZonaVenta.cbx.SelectedValue = oRow("IDZona")

            txtFechaAlta.Text = oRow("FechaAlta").ToString
            txtUsuarioAlta.Text = oRow("UsuarioAlta").ToString
            txtFechaUltimaCompra.Text = oRow("UltimaCompra").ToString


            'Configuramos los controles ABM como EDITAR
            'CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)


        End If

        ctrError.Clear()

    End Sub

    Private Sub btnEditar_Click(sender As System.Object, e As System.EventArgs) Handles btnEditar.Click
        txtComentario.Enabled = True
        btnEditar.Enabled = False
        btnCancelar.Enabled = True
        btnGuardar.Enabled = True
    End Sub

    Private Sub btnCancelar_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelar.Click
        btnEditar.Enabled = True
        btnGuardar.Enabled = False
        txtComentario.Enabled = False
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)3

        CSistema.SetSQLParameter(param, "@IDCliente", IDCliente, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Observacion", txtComentario.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", "UPD", ParameterDirection.Input)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpClienteTeleventa", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            'CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            Listar()
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If
        txtComentario.Enabled = False
        btnGuardar.Enabled = False
        btnEditar.Enabled = True
        btnCancelar.Enabled = False
    End Sub
End Class

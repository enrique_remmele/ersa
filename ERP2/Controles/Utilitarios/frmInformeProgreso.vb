﻿Public Class frmInformeProgreso

    Dim CMultiHilo As New CMHilo
    Property Comentario As String
    Property Titulo As String

    Property tProgreso As Threading.Thread

    Property Cancelar As Boolean = False

    Sub Inicializar()

        Me.Text = Titulo
        Me.Icon = My.Resources.SAIN

        CMultiHilo.LabelText(Me, Label1, Comentario)
        Me.BringToFront()
        Cancelar = False

        Iniciar()

    End Sub

    Sub Iniciar()

        tProgreso = New Threading.Thread(AddressOf Progreso)
        tProgreso.Start()

    End Sub

    Sub Progreso()

        Dim i As Integer = 0
        Do While i <= 99

            If Cancelar = True Then
                Exit Do
            End If

            CMultiHilo.ProgressBarValue(Me, ProgressBar1, i)
            i = i + 1
            Threading.Thread.Sleep(50)
            If i = 100 Then
                i = 0
            End If
        Loop

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Cancelar = True
        Threading.Thread.Sleep(100)
        Me.Close()
    End Sub

    Private Sub frmInformeProgreso_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Cancelar = True
        Threading.Thread.Sleep(100)
        tProgreso.Abort()
    End Sub

    Private Sub frmInformeProgreso_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

End Class
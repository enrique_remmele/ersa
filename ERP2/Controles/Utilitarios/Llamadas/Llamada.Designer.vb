﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Llamada
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.rdbContado = New System.Windows.Forms.RadioButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlCondicionVentaSucursal = New System.Windows.Forms.Panel()
        Me.rdbCredito = New System.Windows.Forms.RadioButton()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblCondicionVenta = New System.Windows.Forms.Label()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtID = New ERP.ocxTXTString()
        Me.lblID = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.lblSucursal = New System.Windows.Forms.Label()
        Me.txtDireccion = New ERP.ocxTXTString()
        Me.txtSucursal = New ERP.ocxTXTString()
        Me.pnlCondicionVentaSucursal.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'rdbContado
        '
        Me.rdbContado.AutoSize = True
        Me.rdbContado.BackColor = System.Drawing.Color.Transparent
        Me.rdbContado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbContado.Location = New System.Drawing.Point(3, 1)
        Me.rdbContado.Margin = New System.Windows.Forms.Padding(1)
        Me.rdbContado.Name = "rdbContado"
        Me.rdbContado.Size = New System.Drawing.Size(65, 17)
        Me.rdbContado.TabIndex = 0
        Me.rdbContado.TabStop = True
        Me.rdbContado.Text = "Contado"
        Me.rdbContado.UseVisualStyleBackColor = False
        '
        'pnlCondicionVentaSucursal
        '
        Me.pnlCondicionVentaSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCondicionVentaSucursal.Controls.Add(Me.rdbContado)
        Me.pnlCondicionVentaSucursal.Controls.Add(Me.rdbCredito)
        Me.pnlCondicionVentaSucursal.Location = New System.Drawing.Point(78, 298)
        Me.pnlCondicionVentaSucursal.Name = "pnlCondicionVentaSucursal"
        Me.pnlCondicionVentaSucursal.Size = New System.Drawing.Size(238, 21)
        Me.pnlCondicionVentaSucursal.TabIndex = 56
        '
        'rdbCredito
        '
        Me.rdbCredito.AutoSize = True
        Me.rdbCredito.BackColor = System.Drawing.Color.Transparent
        Me.rdbCredito.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rdbCredito.Location = New System.Drawing.Point(74, 1)
        Me.rdbCredito.Margin = New System.Windows.Forms.Padding(1)
        Me.rdbCredito.Name = "rdbCredito"
        Me.rdbCredito.Size = New System.Drawing.Size(58, 17)
        Me.rdbCredito.TabIndex = 1
        Me.rdbCredito.TabStop = True
        Me.rdbCredito.Text = "Credito"
        Me.rdbCredito.UseVisualStyleBackColor = False
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblCondicionVenta
        '
        Me.lblCondicionVenta.AutoSize = True
        Me.lblCondicionVenta.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCondicionVenta.Location = New System.Drawing.Point(6, 302)
        Me.lblCondicionVenta.Name = "lblCondicionVenta"
        Me.lblCondicionVenta.Size = New System.Drawing.Size(69, 13)
        Me.lblCondicionVenta.TabIndex = 55
        Me.lblCondicionVenta.Text = "Cond. Venta:"
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(9, 4)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.Size = New System.Drawing.Size(597, 106)
        Me.dgv.TabIndex = 37
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'txtID
        '
        Me.txtID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Indicaciones = Nothing
        Me.txtID.Location = New System.Drawing.Point(78, 151)
        Me.txtID.Multilinea = False
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(58, 21)
        Me.txtID.SoloLectura = True
        Me.txtID.TabIndex = 44
        Me.txtID.TabStop = False
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtID.Texto = ""
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblID.Location = New System.Drawing.Point(54, 154)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(21, 13)
        Me.lblID.TabIndex = 43
        Me.lblID.Text = "ID:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 329)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(609, 22)
        Me.StatusStrip1.TabIndex = 73
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(306, 116)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 41
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(387, 116)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 42
        Me.btnEliminar.Text = "E&liminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(225, 116)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 40
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEditar
        '
        Me.btnEditar.Location = New System.Drawing.Point(144, 116)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 23)
        Me.btnEditar.TabIndex = 39
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(63, 116)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 38
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'lblSucursal
        '
        Me.lblSucursal.AutoSize = True
        Me.lblSucursal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSucursal.Location = New System.Drawing.Point(24, 176)
        Me.lblSucursal.Name = "lblSucursal"
        Me.lblSucursal.Size = New System.Drawing.Size(51, 13)
        Me.lblSucursal.TabIndex = 45
        Me.lblSucursal.Text = "Sucursal:"
        '
        'txtDireccion
        '
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Color = System.Drawing.Color.Empty
        Me.txtDireccion.Indicaciones = Nothing
        Me.txtDireccion.Location = New System.Drawing.Point(78, 193)
        Me.txtDireccion.Multilinea = False
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(238, 21)
        Me.txtDireccion.SoloLectura = True
        Me.txtDireccion.TabIndex = 48
        Me.txtDireccion.TabStop = False
        Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtDireccion.Texto = ""
        '
        'txtSucursal
        '
        Me.txtSucursal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSucursal.Color = System.Drawing.Color.Empty
        Me.txtSucursal.Indicaciones = Nothing
        Me.txtSucursal.Location = New System.Drawing.Point(78, 172)
        Me.txtSucursal.Multilinea = False
        Me.txtSucursal.Name = "txtSucursal"
        Me.txtSucursal.Size = New System.Drawing.Size(238, 21)
        Me.txtSucursal.SoloLectura = True
        Me.txtSucursal.TabIndex = 46
        Me.txtSucursal.TabStop = False
        Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtSucursal.Texto = ""
        '
        'Llamada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pnlCondicionVentaSucursal)
        Me.Controls.Add(Me.lblCondicionVenta)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.lblID)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.lblSucursal)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.txtSucursal)
        Me.Name = "Llamada"
        Me.Size = New System.Drawing.Size(609, 351)
        Me.pnlCondicionVentaSucursal.ResumeLayout(False)
        Me.pnlCondicionVentaSucursal.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rdbContado As System.Windows.Forms.RadioButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents pnlCondicionVentaSucursal As System.Windows.Forms.Panel
    Friend WithEvents rdbCredito As System.Windows.Forms.RadioButton
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblCondicionVenta As System.Windows.Forms.Label
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtID As ERP.ocxTXTString
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents lblSucursal As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As ERP.ocxTXTString
    Friend WithEvents txtSucursal As ERP.ocxTXTString

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxRegistroFirmas
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ocxRegistroFirmas))
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.lblBytes = New System.Windows.Forms.Label()
        Me.peImage = New System.Windows.Forms.PictureBox()
        CType(Me.peImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(645, 119)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 23)
        Me.btnGuardar.TabIndex = 5
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(645, 50)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(116, 23)
        Me.btnBuscar.TabIndex = 4
        Me.btnBuscar.Text = "&Seleccionar Imagen"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'lblBytes
        '
        Me.lblBytes.AutoSize = True
        Me.lblBytes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.lblBytes.Location = New System.Drawing.Point(642, 76)
        Me.lblBytes.Name = "lblBytes"
        Me.lblBytes.Size = New System.Drawing.Size(46, 13)
        Me.lblBytes.TabIndex = 6
        Me.lblBytes.Text = "Tamaño"
        '
        'peImage
        '
        Me.peImage.BackgroundImage = CType(resources.GetObject("peImage.BackgroundImage"), System.Drawing.Image)
        Me.peImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.peImage.Image = CType(resources.GetObject("peImage.Image"), System.Drawing.Image)
        Me.peImage.InitialImage = CType(resources.GetObject("peImage.InitialImage"), System.Drawing.Image)
        Me.peImage.Location = New System.Drawing.Point(0, 3)
        Me.peImage.Name = "peImage"
        Me.peImage.Size = New System.Drawing.Size(635, 322)
        Me.peImage.TabIndex = 0
        Me.peImage.TabStop = False
        '
        'ocxRegistroFirmas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblBytes)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.peImage)
        Me.Name = "ocxRegistroFirmas"
        Me.Size = New System.Drawing.Size(764, 328)
        CType(Me.peImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents lblBytes As System.Windows.Forms.Label
    Friend WithEvents peImage As System.Windows.Forms.PictureBox

End Class

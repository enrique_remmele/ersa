﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ocxClientePrecio
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtHasta = New System.Windows.Forms.DateTimePicker()
        Me.txtPrecioLista = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblKg = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCantidadMinima = New ERP.ocxTXTNumeric()
        Me.chkPrecioUnico = New System.Windows.Forms.CheckBox()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtImporte = New ERP.ocxTXTNumeric()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.txtDesde = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lklSeleccionar = New System.Windows.Forms.LinkLabel()
        Me.btnEliminarDescuento = New System.Windows.Forms.Button()
        Me.dgvSolicitudesPendientes = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.lklSeleccionarSolicitud = New System.Windows.Forms.LinkLabel()
        Me.btnEliminarSolicitud = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvSolicitudesPendientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvSolicitudesPendientes, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel4, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dgv, 0, 3)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 143.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.9823!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.0177!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(870, 682)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(864, 137)
        Me.Panel1.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(845, 104)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Solicitud de Descuento"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.txtHasta)
        Me.Panel3.Controls.Add(Me.txtPrecioLista)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.lblKg)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.txtCantidadMinima)
        Me.Panel3.Controls.Add(Me.chkPrecioUnico)
        Me.Panel3.Controls.Add(Me.txtProducto)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.txtImporte)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.btnAgregar)
        Me.Panel3.Controls.Add(Me.txtDesde)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 16)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(839, 85)
        Me.Panel3.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(538, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Hasta:"
        '
        'txtHasta
        '
        Me.txtHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHasta.Location = New System.Drawing.Point(538, 24)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.Size = New System.Drawing.Size(79, 20)
        Me.txtHasta.TabIndex = 3
        '
        'txtPrecioLista
        '
        Me.txtPrecioLista.Color = System.Drawing.Color.Empty
        Me.txtPrecioLista.Decimales = True
        Me.txtPrecioLista.Indicaciones = Nothing
        Me.txtPrecioLista.Location = New System.Drawing.Point(623, 23)
        Me.txtPrecioLista.Name = "txtPrecioLista"
        Me.txtPrecioLista.Size = New System.Drawing.Size(81, 21)
        Me.txtPrecioLista.SoloLectura = True
        Me.txtPrecioLista.TabIndex = 4
        Me.txtPrecioLista.TabStop = False
        Me.txtPrecioLista.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioLista.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(707, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Precio:"
        '
        'lblKg
        '
        Me.lblKg.AutoSize = True
        Me.lblKg.Location = New System.Drawing.Point(678, 58)
        Me.lblKg.Name = "lblKg"
        Me.lblKg.Size = New System.Drawing.Size(32, 13)
        Me.lblKg.TabIndex = 12
        Me.lblKg.Text = "0 Kg."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(538, 58)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Cantidad Minima:"
        '
        'txtCantidadMinima
        '
        Me.txtCantidadMinima.Color = System.Drawing.Color.Empty
        Me.txtCantidadMinima.Decimales = True
        Me.txtCantidadMinima.Indicaciones = Nothing
        Me.txtCantidadMinima.Location = New System.Drawing.Point(629, 54)
        Me.txtCantidadMinima.Name = "txtCantidadMinima"
        Me.txtCantidadMinima.Size = New System.Drawing.Size(37, 21)
        Me.txtCantidadMinima.SoloLectura = False
        Me.txtCantidadMinima.TabIndex = 8
        Me.txtCantidadMinima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadMinima.Texto = "1"
        '
        'chkPrecioUnico
        '
        Me.chkPrecioUnico.AutoSize = True
        Me.chkPrecioUnico.Location = New System.Drawing.Point(444, 56)
        Me.chkPrecioUnico.Name = "chkPrecioUnico"
        Me.chkPrecioUnico.Size = New System.Drawing.Size(87, 17)
        Me.chkPrecioUnico.TabIndex = 6
        Me.chkPrecioUnico.Text = "Precio Unico"
        Me.chkPrecioUnico.UseVisualStyleBackColor = True
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 0
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(12, 23)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(431, 20)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 1
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Producto:"
        '
        'txtImporte
        '
        Me.txtImporte.Color = System.Drawing.Color.Empty
        Me.txtImporte.Decimales = True
        Me.txtImporte.Indicaciones = Nothing
        Me.txtImporte.Location = New System.Drawing.Point(710, 23)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(81, 21)
        Me.txtImporte.SoloLectura = False
        Me.txtImporte.TabIndex = 5
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Texto = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(620, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Precio de Lista:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(450, 5)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Desde:"
        '
        'btnAgregar
        '
        Me.btnAgregar.Enabled = False
        Me.btnAgregar.Location = New System.Drawing.Point(729, 52)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(96, 23)
        Me.btnAgregar.TabIndex = 9
        Me.btnAgregar.Text = "Solicitar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'txtDesde
        '
        Me.txtDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesde.Location = New System.Drawing.Point(450, 24)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.Size = New System.Drawing.Size(79, 20)
        Me.txtDesde.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 113)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(121, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Descuentos Solicitados:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lklSeleccionar)
        Me.Panel2.Controls.Add(Me.btnEliminarDescuento)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 648)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(864, 31)
        Me.Panel2.TabIndex = 3
        '
        'lklSeleccionar
        '
        Me.lklSeleccionar.AutoSize = True
        Me.lklSeleccionar.Location = New System.Drawing.Point(3, 9)
        Me.lklSeleccionar.Name = "lklSeleccionar"
        Me.lklSeleccionar.Size = New System.Drawing.Size(91, 13)
        Me.lklSeleccionar.TabIndex = 3
        Me.lklSeleccionar.TabStop = True
        Me.lklSeleccionar.Text = "Seleccionar Todo"
        '
        'btnEliminarDescuento
        '
        Me.btnEliminarDescuento.Location = New System.Drawing.Point(100, 5)
        Me.btnEliminarDescuento.Name = "btnEliminarDescuento"
        Me.btnEliminarDescuento.Size = New System.Drawing.Size(139, 23)
        Me.btnEliminarDescuento.TabIndex = 0
        Me.btnEliminarDescuento.Text = "Eliminar Descuento"
        Me.btnEliminarDescuento.UseVisualStyleBackColor = True
        '
        'dgvSolicitudesPendientes
        '
        Me.dgvSolicitudesPendientes.BackgroundColor = System.Drawing.Color.White
        Me.dgvSolicitudesPendientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSolicitudesPendientes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvSolicitudesPendientes.Location = New System.Drawing.Point(3, 146)
        Me.dgvSolicitudesPendientes.Name = "dgvSolicitudesPendientes"
        Me.dgvSolicitudesPendientes.Size = New System.Drawing.Size(864, 148)
        Me.dgvSolicitudesPendientes.TabIndex = 5
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txtFecha)
        Me.Panel4.Controls.Add(Me.lklSeleccionarSolicitud)
        Me.Panel4.Controls.Add(Me.btnEliminarSolicitud)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(3, 300)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(864, 43)
        Me.Panel4.TabIndex = 6
        '
        'txtFecha
        '
        Me.txtFecha.AñoFecha = 0
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 5, 30, 8, 29, 32, 812)
        Me.txtFecha.Location = New System.Drawing.Point(398, 11)
        Me.txtFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.txtFecha.MesFecha = 0
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(69, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 15
        Me.txtFecha.Visible = False
        '
        'lklSeleccionarSolicitud
        '
        Me.lklSeleccionarSolicitud.AutoSize = True
        Me.lklSeleccionarSolicitud.Location = New System.Drawing.Point(6, 5)
        Me.lklSeleccionarSolicitud.Name = "lklSeleccionarSolicitud"
        Me.lklSeleccionarSolicitud.Size = New System.Drawing.Size(91, 13)
        Me.lklSeleccionarSolicitud.TabIndex = 0
        Me.lklSeleccionarSolicitud.TabStop = True
        Me.lklSeleccionarSolicitud.Text = "Seleccionar Todo"
        '
        'btnEliminarSolicitud
        '
        Me.btnEliminarSolicitud.Location = New System.Drawing.Point(103, 1)
        Me.btnEliminarSolicitud.Name = "btnEliminarSolicitud"
        Me.btnEliminarSolicitud.Size = New System.Drawing.Size(139, 23)
        Me.btnEliminarSolicitud.TabIndex = 1
        Me.btnEliminarSolicitud.Text = "Eliminar Solicitud"
        Me.btnEliminarSolicitud.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label6.Location = New System.Drawing.Point(0, 30)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(111, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Descuentos Vigentes:"
        '
        'dgv
        '
        Me.dgv.BackgroundColor = System.Drawing.Color.White
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv.Location = New System.Drawing.Point(3, 349)
        Me.dgv.Name = "dgv"
        Me.dgv.Size = New System.Drawing.Size(864, 293)
        Me.dgv.TabIndex = 0
        '
        'ocxClientePrecio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ocxClientePrecio"
        Me.Size = New System.Drawing.Size(870, 682)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvSolicitudesPendientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents dgv As DataGridView
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents lklSeleccionar As LinkLabel
    Friend WithEvents btnEliminarDescuento As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents txtImporte As ocxTXTNumeric
    Friend WithEvents Label2 As Label
    Friend WithEvents btnAgregar As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents txtDesde As DateTimePicker
    Friend WithEvents dgvSolicitudesPendientes As DataGridView
    Friend WithEvents Label7 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents chkPrecioUnico As CheckBox
    Friend WithEvents lblKg As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtCantidadMinima As ocxTXTNumeric
    Friend WithEvents txtPrecioLista As ocxTXTNumeric
    Friend WithEvents Label3 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label6 As Label
    Friend WithEvents lklSeleccionarSolicitud As LinkLabel
    Friend WithEvents btnEliminarSolicitud As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents txtHasta As DateTimePicker
    Friend WithEvents txtFecha As ocxTXTDate
End Class

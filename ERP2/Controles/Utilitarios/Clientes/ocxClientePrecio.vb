﻿Public Class ocxClientePrecio

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'PROPIEDADES
    Public Property IDCliente As Integer
    Public Property RowCliente As DataRow

    Sub CargarInformacion()
        txtProducto.Conectar()
        Listar()

    End Sub

    Sub Listar()

        CSistema.SqlToDataGrid(dgvSolicitudesPendientes, "Select 'Sel'=Cast(0 as bit), IDProducto,IDCliente,IDMoneda,Producto,Referencia,Moneda,PrecioLista, Descuento, PrecioConDescuento, TipoDescuento,PrecioUnico, CantidadMinimaPrecioUnico, Solicitante, FechaSolicitud, Desde, Hasta from vSolicitudProductoPrecio Where Estado='PENDIENTE' and IDCliente=" & RowCliente("ID") & " Order By Referencia")

        'Ajustar la ultima columna
        'dgv.Columns(dgv.Columns.Count - 1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgvSolicitudesPendientes.Columns("IDProducto").Visible = False
        dgvSolicitudesPendientes.Columns("IDCliente").Visible = False
        dgvSolicitudesPendientes.Columns("IDMoneda").Visible = False
        dgvSolicitudesPendientes.Columns("PrecioUnico").Visible = False
        dgvSolicitudesPendientes.Columns("Sel").DisplayIndex = 0
        dgvSolicitudesPendientes.Columns("Referencia").DisplayIndex = 1
        dgvSolicitudesPendientes.Columns("Producto").DisplayIndex = 2
        dgvSolicitudesPendientes.Columns("Moneda").DisplayIndex = 3
        dgvSolicitudesPendientes.Columns("PrecioLista").DisplayIndex = 4
        dgvSolicitudesPendientes.Columns("Descuento").DisplayIndex = 5
        dgvSolicitudesPendientes.Columns("PrecioConDescuento").DisplayIndex = 6
        dgvSolicitudesPendientes.Columns("TipoDescuento").DisplayIndex = 7
        dgvSolicitudesPendientes.Columns("CantidadMinimaPrecioUnico").DisplayIndex = 8
        dgvSolicitudesPendientes.Columns("Solicitante").DisplayIndex = 9
        dgvSolicitudesPendientes.Columns("FechaSolicitud").DisplayIndex = 10
        dgvSolicitudesPendientes.Columns("Desde").DisplayIndex = 11
        dgvSolicitudesPendientes.Columns("Hasta").DisplayIndex = 12

        txtFecha.Hoy()
        CSistema.SqlToDataGrid(dgv, "Select 'Sel'=Cast(0 as bit), IDProducto,IDCliente,IDMoneda,Producto,Referencia,Moneda,PrecioLista, Desde, Hasta, Descuento, PrecioConDescuento, TipoDescuento, PrecioUnico, CantidadMinimaPrecioUnico from vProductoPrecio Where IDCliente=" & RowCliente("ID") & "And Hasta >= '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "' Order By Referencia")

        'Ajustar la ultima columna
        'dgv.Columns(dgv.Columns.Count - 1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("IDProducto").Visible = False
        dgv.Columns("IDCliente").Visible = False
        dgv.Columns("IDMoneda").Visible = False
        dgv.Columns("PrecioUnico").Visible = False
        dgv.Columns("Sel").DisplayIndex = 0
        dgv.Columns("Referencia").DisplayIndex = 1
        dgv.Columns("Producto").DisplayIndex = 2
        dgv.Columns("Moneda").DisplayIndex = 3
        dgv.Columns("PrecioLista").DisplayIndex = 4
        dgv.Columns("Descuento").DisplayIndex = 5
        dgv.Columns("PrecioConDescuento").DisplayIndex = 6
        dgv.Columns("TipoDescuento").DisplayIndex = 7
        dgv.Columns("CantidadMinimaPrecioUnico").DisplayIndex = 8
        dgv.Columns("Desde").DisplayIndex = 9
        dgv.Columns("Hasta").DisplayIndex = 10


    End Sub

    Function Validar(ByVal Operacion As CSistema.NUMOperacionesABM) As Boolean

        If Operacion = CSistema.NUMOperacionesABM.INS Then
            If txtProducto.Seleccionado = False Then
                MessageBox.Show("Seleccione correctamente el producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtProducto.Focus()
                Return False
            End If

            If IsNumeric(txtImporte.ObtenerValor) = False Then
                MessageBox.Show("Introduzca el precio correctamente", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtImporte.Focus()
                Return False
            End If

            If txtImporte.ObtenerValor = 0 Then
                MessageBox.Show("El precio debe ser mayor a 0", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtImporte.Focus()
                Return False
            End If

        End If


        Return True
    End Function

    Sub Solicitar()

        Dim vTipoPrecio As String = "FIJO"

        If chkPrecioUnico.Checked Then
            vTipoPrecio = "UNICO"
        End If

        Dim param(-1) As SqlClient.SqlParameter

        'Si fue seleccionado una sucursal del cliente
        CSistema.SetSQLParameter(param, "@Producto", txtProducto.Registro("Referencia"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cliente", RowCliente("Referencia"), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Moneda", "GS", ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoPrecio", vTipoPrecio, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Precio", CSistema.FormatoMonedaBaseDatos(txtImporte.ObtenerValor, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Desde", CSistema.FormatoFechaDesdeHastaBaseDatos(txtDesde.Value, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Hasta", CSistema.FormatoFechaDesdeHastaBaseDatos(txtHasta.Value, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CantidadMinimaPrecioUnico", CSistema.FormatoNumeroBaseDatos(txtCantidadMinima.ObtenerValor, True), ParameterDirection.Input)


        'Transaccion
        'CSistema.SetSQLParameter(param, "@IDOperacion", IDOperacion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDSucursal", vgIDSucursal, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@IDDeposito", vgIDDeposito, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpSolicitudProductoPrecio", False, False, MensajeRetorno) = True Then
            MessageBox.Show("Informe: " & MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Listar()
        Else
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM, Optional ByVal index As Integer = 0)


        If Operacion = CSistema.NUMOperacionesABM.INS And index = 0 Then
            If Validar(Operacion) = False Then
                Exit Sub
            End If
        End If

        Dim IDProducto As Integer = 0
        Dim Precio As Decimal = 0

        If index > 0 Then
            IDProducto = dgv.Rows(index).Cells("IDProducto").Value
            Precio = dgv.Rows(index).Cells("Precio").Value
        Else
            IDProducto = txtProducto.Registro("ID")
            Precio = txtImporte.ObtenerValor
        End If


        Dim param(-1) As SqlClient.SqlParameter

        Dim IndiceOperacion As Integer

        CSistema.SetSQLParameter(param, "@IDProducto", IDProducto, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", 1, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Precio", Precio, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", IDCliente, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@TipoPrecio", chkPrecioUnico.Checked, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Precio", CSistema.FormatoMonedaBaseDatos(txtImporte.ObtenerValor, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Desde", CSistema.FormatoFechaDesdeHastaBaseDatos(txtDesde.Value, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Hasta", CSistema.FormatoFechaDesdeHastaBaseDatos(txtHasta.Value, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@CantidadMinimaPrecioUnico", CSistema.FormatoNumeroBaseDatos(txtCantidadMinima.ObtenerValor, True), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        'Capturamos el index de la Operacion para un posible proceso posterior
        IndiceOperacion = param.GetLength(0) - 1

        'Transaccion
        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTerminal", vgIDTerminal, ParameterDirection.Input)


        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)



        Dim MensajeRetorno As String = ""

        Try
            If CSistema.ExecuteStoreProcedure(param, "spProductoPrecio", False, False, MensajeRetorno) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub

            End If
        Catch ex As Exception
            Exit Sub
        End Try


    End Sub

    Sub EliminarSolicitud()
        For i = 0 To dgvSolicitudesPendientes.RowCount - 1
            If dgvSolicitudesPendientes.Rows(i).Cells("Sel").Value = True Then
                Dim IDProducto As Integer = dgvSolicitudesPendientes.Rows(i).Cells("IDProducto").Value
                Dim IDCliente As Integer = dgvSolicitudesPendientes.Rows(i).Cells("IDCliente").Value
                Dim IDMoneda As Integer = dgvSolicitudesPendientes.Rows(i).Cells("IDMoneda").Value
                Dim PrecioUnico As Boolean = dgvSolicitudesPendientes.Rows(i).Cells("PrecioUnico").Value
                Dim Desde As Date = dgvSolicitudesPendientes.Rows(i).Cells("Desde").Value
                Dim Hasta As Date = dgvSolicitudesPendientes.Rows(i).Cells("Hasta").Value
                Dim Precio As Decimal = dgvSolicitudesPendientes.Rows(i).Cells("PrecioConDescuento").Value
                Dim CantidadMinimaPrecioUnico As Integer = dgvSolicitudesPendientes.Rows(i).Cells("CantidadMinimaPrecioUnico").Value

                Dim dttemp As DataTable = CSistema.ExecuteToDataTable("Exec SpAutorizacionProductoPrecio @IDProducto = " & IDProducto & ", 
                                        @IDCliente = " & IDCliente & ", 
                                        @IDMoneda = " & IDMoneda & ", 
                                        @PrecioUnico = '" & PrecioUnico & "', 
                                        @Precio = " & Precio & ", 
                                        @Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "', 
                                        @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "', 
                                        @CantidadMinimaPrecioUnico = '" & CSistema.FormatoNumeroBaseDatos(CantidadMinimaPrecioUnico, True) & "', 
                                        @Operacion='ELIMINAR', 
                                        @IDUsuario = " & vgIDUsuario & ", 
                                        @IDTerminal = " & vgIDTerminal & "").Copy


            End If

        Next

        Listar()

    End Sub

    Sub EliminarDescuentoVigente()
        For i = 0 To dgv.RowCount - 1
            If dgv.Rows(i).Cells("Sel").Value = True Then
                Dim IDProducto As Integer = dgv.Rows(i).Cells("IDProducto").Value
                Dim IDCliente As Integer = dgv.Rows(i).Cells("IDCliente").Value
                Dim IDMoneda As Integer = dgv.Rows(i).Cells("IDMoneda").Value
                Dim PrecioUnico As Boolean = dgv.Rows(i).Cells("PrecioUnico").Value
                Dim Desde As Date = dgv.Rows(i).Cells("Desde").Value
                Dim Hasta As Date = dgv.Rows(i).Cells("Hasta").Value
                Dim Precio As Decimal = dgv.Rows(i).Cells("PrecioConDescuento").Value
                Dim CantidadMinimaPrecioUnico As Integer = dgv.Rows(i).Cells("CantidadMinimaPrecioUnico").Value

                Dim dttemp As DataTable = CSistema.ExecuteToDataTable("exec spProductoPrecio @IDProducto = " & IDProducto & ", 
										@IDCliente=" & IDCliente & ",
										@IDMoneda=" & IDMoneda & ",
										@Precio=" & Precio & ",
										@Desde = '" & CSistema.FormatoFechaBaseDatos(Desde, True, False) & "',
                                        @Hasta = '" & CSistema.FormatoFechaBaseDatos(Hasta, True, False) & "',
										@PrecioUnico=" & PrecioUnico & ",
										@CantidadMinimaPrecioUnico = " & CantidadMinimaPrecioUnico & ",
										@Operacion = 'DEL',
										@IDUsuario = " & vgIDUsuario & ",
										@IDTerminal = " & vgIDTerminal)
            End If

        Next

        Listar()

    End Sub

    Public Sub New()

        InitializeComponent()

    End Sub

    Private Sub lklSeleccionar_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklSeleccionar.LinkClicked
        If lklSeleccionar.Text = "Seleccionar Todo" Then
            For Each fila As DataGridViewRow In dgv.Rows
                fila.Cells("Sel").Value = True
            Next
            lklSeleccionar.Text = "Quitar Seleccion"
        Else
            For Each fila As DataGridViewRow In dgv.Rows
                fila.Cells("Sel").Value = True
            Next
            lklSeleccionar.Text = "Seleccionar Todo"
        End If

    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If vgConfiguraciones("CargarExcepcionSinAutorizacion") = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Solicitar()
        End If
    End Sub

    Private Sub txtProducto_ItemSeleccionado(sender As Object, e As EventArgs) Handles txtProducto.ItemSeleccionado
        Dim PrecioLista As Decimal
        If txtProducto.Registro("IDTipoProducto") <> 1 Then
            Dim ListaPrecio As String = "MAYORISTA"
            ListaPrecio = CData.GetTable("ListaPrecio", "ID = " & RowCliente("IDListaPrecio"))(0)("Descripcion")
            PrecioLista = CSistema.ExecuteScalar("Select top(1) Precio from vProductoListaPrecio where ListaPrecio = '" & ListaPrecio & "' and IDProducto = " & txtProducto.Registro("ID"))
        Else
            PrecioLista = CSistema.ExecuteScalar("Select top(1) Precio from ProductoListaPrecio where IDListaPrecio = 1 and IDProducto = " & txtProducto.Registro("ID"))
        End If

        If PrecioLista = 0 Then
            MessageBox.Show("No existe precio configurado para este producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtProducto.LimpiarSeleccion()
            btnAgregar.Enabled = False
            Exit Sub
        End If
        txtPrecioLista.SetValue(PrecioLista)
        btnAgregar.Enabled = True
        lblKg.Text = txtProducto.Registro("Peso") * txtCantidadMinima.ObtenerValor
        txtDesde.Focus()
    End Sub

    Private Sub txtCantidadMinima_TeclaPrecionada(sender As Object, e As KeyEventArgs) Handles txtCantidadMinima.TeclaPrecionada
        If txtProducto.Seleccionado Then
            Dim Peso As Decimal = txtProducto.Registro("Peso")
            lblKg.Text = Peso * txtCantidadMinima.ObtenerValor
        End If
    End Sub

    Private Sub btnEliminarDescuentoPuntual_Click(sender As Object, e As EventArgs) Handles btnEliminarDescuento.Click
        EliminarDescuentoVigente()
    End Sub

    Private Sub btnEliminarSolicitud_Click(sender As Object, e As EventArgs) Handles btnEliminarSolicitud.Click
        EliminarSolicitud()
    End Sub

    Private Sub lklSeleccionarSolicitud_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lklSeleccionarSolicitud.LinkClicked
        If lklSeleccionarSolicitud.Text = "Seleccionar Todo" Then
            For Each fila As DataGridViewRow In dgvSolicitudesPendientes.Rows
                fila.Cells("Sel").Value = True
            Next
            lklSeleccionarSolicitud.Text = "Quitar Seleccion"
        Else
            For Each fila As DataGridViewRow In dgvSolicitudesPendientes.Rows
                fila.Cells("Sel").Value = True
            Next
            lklSeleccionarSolicitud.Text = "Seleccionar Todo"
        End If
    End Sub
End Class

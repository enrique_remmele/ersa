﻿Public Class ocxKMEstimadoSucursal

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'PROPIEDADES
    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    'Obtener el ID Registro
    Dim ID As Integer

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False
        Iniciado = True

        'Funciones
        CargarInformacion()
        CSistema.InicializaControles(Me)

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, New Button, btnEditar, btnCancelar, btnGuardar, New Button, vControles)

        'Focus
        dgv.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtKMAsu)
        CSistema.CargaControl(vControles, txtKMMol)
        CSistema.CargaControl(vControles, txtKMCde)
        CSistema.CargaControl(vControles, txtKMCord)
        CSistema.CargaControl(vControles, txtKMCon)
        CSistema.CargaControl(vControles, txtKMAquay)

        'Cargamos los paises en el lv
        Listar()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        CSistema.SqlToDataGrid(dgv, "Select Sucursal, ID, Direccion, Ciudad, ZonaVenta From VClienteSucursal Where IDCliente=" & IDCliente & " Order By ID")

        'Ajustar la ultima columna
        dgv.Columns(dgv.Columns.Count - 1).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If dgv.SelectedRows.Count = 0 Then
            ctrError.SetError(dgv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(dgv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, New Button, btnEditar, btnCancelar, btnGuardar, New Button, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        'Dim ID As Integer

        ID = dgv.SelectedRows(0).Cells(1).Value

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select KMAsu,KMMol, KMCde, KMCord, KMCon, KMAquay From vClienteSucursal Where ID=" & ID & "And IDCliente = " & IDCliente)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtKMAsu.Text = oRow("KMAsu").ToString
            txtKMMol.Text = oRow("KMMol").ToString
            txtKMCde.Text = oRow("KMCde").ToString
            txtKMCord.Text = oRow("KMCord").ToString
            txtKMCon.Text = oRow("KMCon").ToString
            txtKMAquay.Text = oRow("KMAquay").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, New Button, btnEditar, btnCancelar, btnGuardar, New Button, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Este vector guarda todas las variables que se van a pasar por parametro al Procedimiento Almacenado.
        'Dimensione y configure todos los campos con sus respectivos valores, 
        'segun el Procedimiento Almacenado lo requiera. Ver esto en la Base de Datos.
        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCliente", IDCliente, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@KMAsu", txtKMAsu.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@KMMol", txtKMMol.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@KMCde", txtKMCde.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@KMCord", txtKMCord.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@KMCon", txtKMCon.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@KMAquay", txtKMAquay.Text, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", "UPD2", ParameterDirection.Input)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpClienteSucursal", False, False, MensajeRetorno, "", True) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, New Button, btnEditar, btnCancelar, btnGuardar, New Button, vControles)
            'Listar()
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnGuarda_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If
    End Sub

    Private Sub btnEdita_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, New Button, btnEditar, btnCancelar, btnGuardar, New Button, vControles)

        vNuevo = False

        'Foco
        txtKMAsu.Focus()

    End Sub

    Private Sub btnCancela_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, New Button, btnEditar, btnCancelar, btnGuardar, New Button, vControles)
        vNuevo = False
    End Sub

    Private Sub dgv_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged
        ObtenerInformacion()
    End Sub

End Class

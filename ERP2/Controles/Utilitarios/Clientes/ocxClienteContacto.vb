﻿Public Class ocxClienteContacto

    'CLASES
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control

    'PROPIEDADES
    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False
        Iniciado = True

        'Funciones
        CargarInformacion()
        CSistema.InicializaControles(Me)

        txtEmail.txt.CharacterCasing = CharacterCasing.Lower

        'Botones
        CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        lv.Focus()

    End Sub

    Sub CargarInformacion()

        'Este vector se utiliza para inhabilitar y habilitar segun la operacion a realizar.
        'Dimensione y cargue solo los controles que considere necesario.
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, txtCargo)
        CSistema.CargaControl(vControles, txtEmail)
        CSistema.CargaControl(vControles, txtNombre)
        CSistema.CargaControl(vControles, txtTelefono)
        CSistema.CargaControl(vControles, txtCumpleaños)

        'Cargamos los paises en el lv
        Listar()

    End Sub

    Sub ObtenerInformacion()

        'Validar
        'Si es que se selecciono el registro.
        If lv.SelectedItems.Count = 0 Then
            ctrError.SetError(lv, "Seleccione correctamente un registro!")
            ctrError.SetIconAlignment(lv, ErrorIconAlignment.TopLeft)

            'Establecemos los botones a INICIO
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

            Exit Sub
        End If

        'Obtener el ID Registro
        Dim ID As Integer

        ID = lv.SelectedItems(0).SubItems(1).Text

        'Obtenemos la informacion actualizada desde la base de datos
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("Select * From VClienteContacto Where IDCliente=" & IDCliente & " And ID=" & ID)

        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then

            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)

            'Asignamos los valores a los controles correspondientes
            txtCodigo.txt.Text = oRow("ID").ToString
            txtNombre.txt.Text = oRow("Nombres").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString
            txtEmail.txt.Text = oRow("Email").ToString
            txtTelefono.txt.Text = oRow("Telefono").ToString
            txtCumpleaños.txt.Text = oRow("Cumpleaños").ToString

            'Configuramos los controles ABM como EDITAR
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        End If

        ctrError.Clear()

    End Sub

    Sub Listar(Optional ByVal ID As Integer = 0)

        'Con este metodo "SqlToLv" el sistema carga automaticamente la consulta SQL en el ListView asociado.
        'Ten en cuenta que el Nombre del Campo de la consulta sera el titulo de la Columna en el ListView.
        CSistema.SqlToLv(lv, "Select Nombres, ID, Cargo, Telefono, Email, Cumpleaños From VClienteContacto Where IDCliente=" & IDCliente & " Order By ID")

        'Verificamos. Si columnas es mayor a 0, entonces el proceso fue satisfactorio
        If lv.Columns.Count > 0 Then

            'Esto hacemos para que: 
            '1- Que el ID sea visible en la primera columna
            '2- Y para que cuando el usuario escriba en el lv, el control filtre por su descripcion.
            lv.Columns(0).DisplayIndex = 1
            lv.Columns(0).Width = lv.Width / 3
            'Ahora seleccionamos automaticamente el registro especificado
            If lv.Items.Count > 0 Then
                If ID = 0 Then
                    lv.Items(0).Selected = True
                Else
                    For i As Integer = 0 To lv.Items.Count - 1
                        If lv.Items(i).SubItems(1).Text = ID Then
                            lv.Items(i).Selected = True
                            Exit For
                        End If
                    Next

                End If
            End If


        End If


        lv.Refresh()

    End Sub

    Sub InicializarControles()

        'TextBox
        txtNombre.txt.Clear()
        txtCargo.txt.Clear()
        txtEmail.txt.Clear()
        txtTelefono.txt.Clear()
        txtCumpleaños.txt.Clear()

        'ToolStripStatusLabel
        tsslEstado.Text = ""

        'Funciones
        If Me.vNuevo = True Then
            txtCodigo.txt.Text = CType(CSistema.ExecuteScalar("Select IsNull(Max(ID)+1, 1) From ClienteContacto Where IDCliente=" & IDCliente), Integer)
        Else
            Listar(CType(CSistema.ExecuteScalar("Select IsNull(Max(ID), 1) From ClienteContacto Where IDCliente=" & IDCliente), Integer))
        End If

        'Error
        ctrError.Clear()

        'Foco
        txtNombre.txt.Focus()
        txtNombre.txt.SelectAll()

    End Sub

    Sub Procesar(ByVal Operacion As CSistema.NUMOperacionesABM)

        'Validar
        'Poner una descripcion
        If txtNombre.txt.Text.Trim.Length = 0 Then
            Dim mensaje As String = "Ingrese un nombre valido!"
            ctrError.SetError(btnGuardar, mensaje)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        'Seleccion de registro si el proceso es de INSERCCION o ELIMINACION
        If Operacion = ERP.CSistema.NUMOperacionesABM.UPD Or Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If lv.SelectedItems.Count = 0 Then
                Dim mensaje As String = "Seleccione un registro!"
                ctrError.SetError(lv, mensaje)
                ctrError.SetIconAlignment(lv, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            End If
        End If

        'Si el proceso es de ELIMINACION, preguntar por seguridad si se esta seguro/a de la eliminacion.
        If Operacion = ERP.CSistema.NUMOperacionesABM.DEL Then
            If MessageBox.Show("Atencion! Esto eliminara permanentemente el registro. Desea continuar?", "Eliminar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
        End If

        'Procesar
        'Obtener el ID
        Dim ID As Integer

        ID = txtCodigo.txt.Text

        Dim param(-1) As SqlClient.SqlParameter
        CSistema.SetSQLParameter(param, "@IDCliente", IDCliente, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@ID", ID, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Nombres", txtNombre.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cargo", txtCargo.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Email", txtEmail.GetValue, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Telefono", txtTelefono.txt.Text, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cumpleaños", CSistema.FormatoFechaBaseDatos(txtCumpleaños.txt.Text, True, False), ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

        'Parametro que adquiere la informacion pasada por la Base de Datos
        Dim MensajeRetorno As String = ""
        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        If CSistema.ExecuteStoreProcedure(param, "SpClienteContacto", False, False, MensajeRetorno) = True Then
            tsslEstado.Text = "Informe: " & MensajeRetorno
            CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
            Listar()
            ctrError.Clear()
        Else
            tsslEstado.Text = "Atencion: " & MensajeRetorno
            ctrError.SetError(btnGuardar, "Atencion: " & MensajeRetorno)
            ctrError.SetIconAlignment(btnGuardar, ErrorIconAlignment.TopRight)
        End If

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.NUEVO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = True
        InicializarControles()
    End Sub

    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        'Establecemos los botones a Editando
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.EDITANDO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        vNuevo = False

        'Foco
        txtNombre.Focus()
        txtNombre.txt.SelectAll()

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.CANCELAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        vNuevo = False
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        If vNuevo = True Then
            Procesar(ERP.CSistema.NUMOperacionesABM.INS)
        Else
            Procesar(ERP.CSistema.NUMOperacionesABM.UPD)
        End If

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        CSistema.ControlBotonesABM(ERP.CSistema.NUMHabilitacionBotonesABM.ELIMINAR, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)
        Procesar(ERP.CSistema.NUMOperacionesABM.DEL)
    End Sub

    Private Sub lvs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lv.Click
        ObtenerInformacion()
    End Sub

    Private Sub lvs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lv.SelectedIndexChanged
        ObtenerInformacion()
    End Sub

    Private Sub frmLineas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

End Class

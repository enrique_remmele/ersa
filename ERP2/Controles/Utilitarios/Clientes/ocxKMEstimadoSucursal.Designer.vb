﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxKMEstimadoSucursal
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtKMCon = New System.Windows.Forms.TextBox()
        Me.txtKMMol = New System.Windows.Forms.TextBox()
        Me.txtKMCde = New System.Windows.Forms.TextBox()
        Me.txtKMCord = New System.Windows.Forms.TextBox()
        Me.txtKMAsu = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtKMAquay = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 274)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(596, 22)
        Me.StatusStrip1.TabIndex = 112
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnEditar
        '
        Me.btnEditar.BackColor = System.Drawing.SystemColors.Control
        Me.btnEditar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnEditar.Location = New System.Drawing.Point(180, 241)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(75, 25)
        Me.btnEditar.TabIndex = 109
        Me.btnEditar.Text = "&Editar"
        Me.btnEditar.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGuardar.Location = New System.Drawing.Point(261, 241)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 25)
        Me.btnGuardar.TabIndex = 110
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.SystemColors.Control
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancelar.Location = New System.Drawing.Point(342, 241)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 25)
        Me.btnCancelar.TabIndex = 111
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(3, 3)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.Size = New System.Drawing.Size(590, 150)
        Me.dgv.TabIndex = 113
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(87, 218)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(123, 13)
        Me.Label9.TabIndex = 95
        Me.Label9.Text = "KM Entrega desde CDE:"
        '
        'txtKMCon
        '
        Me.txtKMCon.BackColor = System.Drawing.Color.White
        Me.txtKMCon.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMCon.Location = New System.Drawing.Point(482, 189)
        Me.txtKMCon.Name = "txtKMCon"
        Me.txtKMCon.Size = New System.Drawing.Size(42, 20)
        Me.txtKMCon.TabIndex = 97
        '
        'txtKMMol
        '
        Me.txtKMMol.BackColor = System.Drawing.Color.White
        Me.txtKMMol.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMMol.Location = New System.Drawing.Point(250, 189)
        Me.txtKMMol.Name = "txtKMMol"
        Me.txtKMMol.Size = New System.Drawing.Size(42, 20)
        Me.txtKMMol.TabIndex = 91
        '
        'txtKMCde
        '
        Me.txtKMCde.BackColor = System.Drawing.Color.White
        Me.txtKMCde.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMCde.Location = New System.Drawing.Point(250, 215)
        Me.txtKMCde.Name = "txtKMCde"
        Me.txtKMCde.Size = New System.Drawing.Size(42, 20)
        Me.txtKMCde.TabIndex = 92
        '
        'txtKMCord
        '
        Me.txtKMCord.BackColor = System.Drawing.Color.White
        Me.txtKMCord.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMCord.Location = New System.Drawing.Point(482, 163)
        Me.txtKMCord.Name = "txtKMCord"
        Me.txtKMCord.Size = New System.Drawing.Size(42, 20)
        Me.txtKMCord.TabIndex = 93
        '
        'txtKMAsu
        '
        Me.txtKMAsu.BackColor = System.Drawing.Color.White
        Me.txtKMAsu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKMAsu.Location = New System.Drawing.Point(250, 163)
        Me.txtKMAsu.Name = "txtKMAsu"
        Me.txtKMAsu.Size = New System.Drawing.Size(42, 20)
        Me.txtKMAsu.TabIndex = 90
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(298, 166)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(169, 13)
        Me.Label10.TabIndex = 96
        Me.Label10.Text = "KM Entrega desde CORDILLERA:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(87, 192)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(143, 13)
        Me.Label8.TabIndex = 94
        Me.Label8.Text = "KM Entrega desde MOLINO:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(298, 192)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(179, 13)
        Me.Label11.TabIndex = 98
        Me.Label11.Text = "KM Entrega desde CONCENPCION:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(87, 166)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(157, 13)
        Me.Label7.TabIndex = 89
        Me.Label7.Text = "KM Entrega desde ASUNCION:"
        '
        'txtKMAquay
        '
        Me.txtKMAquay.BackColor = System.Drawing.Color.White
        Me.txtKMAquay.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMAquay.Location = New System.Drawing.Point(482, 215)
        Me.txtKMAquay.Name = "txtKMAquay"
        Me.txtKMAquay.Size = New System.Drawing.Size(42, 20)
        Me.txtKMAquay.TabIndex = 99
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(298, 218)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(138, 13)
        Me.Label12.TabIndex = 100
        Me.Label12.Text = "KM Entrega desde AQUAY:"
        '
        'ocxKMEstimadoSucursal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.txtKMAquay)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtKMAsu)
        Me.Controls.Add(Me.txtKMCord)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtKMCde)
        Me.Controls.Add(Me.txtKMCon)
        Me.Controls.Add(Me.txtKMMol)
        Me.Name = "ocxKMEstimadoSucursal"
        Me.Size = New System.Drawing.Size(596, 296)
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents btnEditar As Button
    Friend WithEvents btnGuardar As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Label12 As Label
    Friend WithEvents dgv As DataGridView
    Friend WithEvents txtKMAquay As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label10 As Label
    Public WithEvents txtKMAsu As TextBox
    Friend WithEvents txtKMCord As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtKMCde As TextBox
    Friend WithEvents txtKMCon As TextBox
    Friend WithEvents txtKMMol As TextBox
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ocxKMEstimadoMatriz
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnEdita = New System.Windows.Forms.Button()
        Me.btnGuarda = New System.Windows.Forms.Button()
        Me.btnCancela = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtKMAquay = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtKMAsu = New System.Windows.Forms.TextBox()
        Me.txtKMCord = New System.Windows.Forms.TextBox()
        Me.txtKMCde = New System.Windows.Forms.TextBox()
        Me.txtKMMol = New System.Windows.Forms.TextBox()
        Me.txtKMCon = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnEdita
        '
        Me.btnEdita.BackColor = System.Drawing.SystemColors.Control
        Me.btnEdita.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdita.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnEdita.Location = New System.Drawing.Point(12, 187)
        Me.btnEdita.Name = "btnEdita"
        Me.btnEdita.Size = New System.Drawing.Size(75, 25)
        Me.btnEdita.TabIndex = 104
        Me.btnEdita.Text = "&Editar"
        Me.btnEdita.UseVisualStyleBackColor = False
        '
        'btnGuarda
        '
        Me.btnGuarda.BackColor = System.Drawing.SystemColors.Control
        Me.btnGuarda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuarda.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnGuarda.Location = New System.Drawing.Point(93, 187)
        Me.btnGuarda.Name = "btnGuarda"
        Me.btnGuarda.Size = New System.Drawing.Size(75, 25)
        Me.btnGuarda.TabIndex = 105
        Me.btnGuarda.Text = "&Guardar"
        Me.btnGuarda.UseVisualStyleBackColor = False
        '
        'btnCancela
        '
        Me.btnCancela.BackColor = System.Drawing.SystemColors.Control
        Me.btnCancela.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancela.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCancela.Location = New System.Drawing.Point(174, 187)
        Me.btnCancela.Name = "btnCancela"
        Me.btnCancela.Size = New System.Drawing.Size(75, 25)
        Me.btnCancela.TabIndex = 106
        Me.btnCancela.Text = "&Cancelar"
        Me.btnCancela.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtKMAquay)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtKMAsu)
        Me.GroupBox1.Controls.Add(Me.txtKMCord)
        Me.GroupBox1.Controls.Add(Me.txtKMCde)
        Me.GroupBox1.Controls.Add(Me.txtKMMol)
        Me.GroupBox1.Controls.Add(Me.txtKMCon)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(257, 178)
        Me.GroupBox1.TabIndex = 103
        Me.GroupBox1.TabStop = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(47, 146)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(138, 13)
        Me.Label12.TabIndex = 100
        Me.Label12.Text = "KM Entrega desde AQUAY:"
        '
        'txtKMAquay
        '
        Me.txtKMAquay.BackColor = System.Drawing.Color.White
        Me.txtKMAquay.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMAquay.Location = New System.Drawing.Point(191, 143)
        Me.txtKMAquay.Name = "txtKMAquay"
        Me.txtKMAquay.Size = New System.Drawing.Size(42, 20)
        Me.txtKMAquay.TabIndex = 99
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(28, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(157, 13)
        Me.Label7.TabIndex = 89
        Me.Label7.Text = "KM Entrega desde ASUNCION:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(6, 120)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(179, 13)
        Me.Label11.TabIndex = 98
        Me.Label11.Text = "KM Entrega desde CONCENPCION:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(42, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(143, 13)
        Me.Label8.TabIndex = 94
        Me.Label8.Text = "KM Entrega desde MOLINO:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(16, 94)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(169, 13)
        Me.Label10.TabIndex = 96
        Me.Label10.Text = "KM Entrega desde CORDILLERA:"
        '
        'txtKMAsu
        '
        Me.txtKMAsu.BackColor = System.Drawing.Color.White
        Me.txtKMAsu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtKMAsu.Location = New System.Drawing.Point(191, 13)
        Me.txtKMAsu.Name = "txtKMAsu"
        Me.txtKMAsu.Size = New System.Drawing.Size(42, 20)
        Me.txtKMAsu.TabIndex = 90
        '
        'txtKMCord
        '
        Me.txtKMCord.BackColor = System.Drawing.Color.White
        Me.txtKMCord.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMCord.Location = New System.Drawing.Point(191, 91)
        Me.txtKMCord.Name = "txtKMCord"
        Me.txtKMCord.Size = New System.Drawing.Size(42, 20)
        Me.txtKMCord.TabIndex = 93
        '
        'txtKMCde
        '
        Me.txtKMCde.BackColor = System.Drawing.Color.White
        Me.txtKMCde.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMCde.Location = New System.Drawing.Point(191, 65)
        Me.txtKMCde.Name = "txtKMCde"
        Me.txtKMCde.Size = New System.Drawing.Size(42, 20)
        Me.txtKMCde.TabIndex = 92
        '
        'txtKMMol
        '
        Me.txtKMMol.BackColor = System.Drawing.Color.White
        Me.txtKMMol.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMMol.Location = New System.Drawing.Point(191, 39)
        Me.txtKMMol.Name = "txtKMMol"
        Me.txtKMMol.Size = New System.Drawing.Size(42, 20)
        Me.txtKMMol.TabIndex = 91
        '
        'txtKMCon
        '
        Me.txtKMCon.BackColor = System.Drawing.Color.White
        Me.txtKMCon.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtKMCon.Location = New System.Drawing.Point(191, 117)
        Me.txtKMCon.Name = "txtKMCon"
        Me.txtKMCon.Size = New System.Drawing.Size(42, 20)
        Me.txtKMCon.TabIndex = 97
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(62, 68)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(123, 13)
        Me.Label9.TabIndex = 95
        Me.Label9.Text = "KM Entrega desde CDE:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 235)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(284, 22)
        Me.StatusStrip1.TabIndex = 107
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'ocxKMEstimadoMatriz
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnEdita)
        Me.Controls.Add(Me.btnGuarda)
        Me.Controls.Add(Me.btnCancela)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "ocxKMEstimadoMatriz"
        Me.Size = New System.Drawing.Size(284, 257)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnEdita As Button
    Friend WithEvents btnGuarda As Button
    Friend WithEvents btnCancela As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtKMAquay As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label10 As Label
    Public WithEvents txtKMAsu As TextBox
    Friend WithEvents txtKMCord As TextBox
    Friend WithEvents txtKMCde As TextBox
    Friend WithEvents txtKMMol As TextBox
    Friend WithEvents txtKMCon As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
End Class

﻿
Public Class ocxRegistroFirmas
    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim vgRedimensionarImagen As Boolean
    Dim VGPathImagen As String
    'VARIABLES
    Dim vNuevo As Boolean
    Dim vControles() As Control
    Dim vgAnchoImagen As Integer
    Dim vgAltoImagen As Integer
    Dim NombreArchivo As String
    Dim vgPorPorcentaje As Boolean
    Dim vgPorcentajeRedimension As Decimal
    'PROPIEDADES
    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property
    Private IniciadoValue As Boolean
    Public Property Iniciado() As Boolean
        Get
            Return IniciadoValue
        End Get
        Set(ByVal value As Boolean)
            IniciadoValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Variables
        vNuevo = False

        'Funciones
        'CargarInformacion()
        CSistema.InicializaControles(Me)
        CargarImagen()

        'Botones
        'CSistema.ControlBotonesABM(CSistema.NUMHabilitacionBotonesABM.INICIO, btnNuevo, btnEditar, btnCancelar, btnGuardar, btnEliminar, vControles)

        'Focus
        'dgv.Focus()

    End Sub
    Sub CargarImagen()
        'Imagen
        If CType(CSistema.ExecuteScalar("Select count(*) From Cliente where Firma Is Not null and ID = " & IDCliente, VGCadenaConexion), Integer) > 0 Then
            peImage.Image = CSistema.Bytes2Image(CSistema.ExecuteScalar("Select Firma From Cliente Where ID=" & IDCliente))
            Dim Unidad As String = "Bytes"
            Dim Tamaño As String = "0"
            Tamaño = CSistema.CalculaTamaño(CSistema.ExecuteScalar("Select DATALENGTH(Firma) From Cliente Where ID=" & IDCliente), Unidad)
            lblBytes.Text = CSistema.FormatoNumero(Tamaño, True) & " " & Unidad
        Else
            peImage.Image = Nothing
        End If
    End Sub

    Sub SeleccionarImagen()

        Dim Path As String = ""
        If Path = "" Then
            Path = My.Computer.FileSystem.SpecialDirectories.Desktop
        End If

        Dim OpenFileDialog1 As New OpenFileDialog
        OpenFileDialog1.AddExtension = True
        OpenFileDialog1.AutoUpgradeEnabled = True
        OpenFileDialog1.CheckFileExists = True
        OpenFileDialog1.CheckPathExists = True
        OpenFileDialog1.DefaultExt = "jpg"
        OpenFileDialog1.DereferenceLinks = True
        OpenFileDialog1.Filter = "Todos |*.*|Bitmap (*.bmp)|*.bmp|JPG (*.jpg*)|*.jpg*|PNG (*.png*)|*.png*|GIF (*.gif*)|*.gif*"
        OpenFileDialog1.InitialDirectory = Path
        OpenFileDialog1.Multiselect = False
        OpenFileDialog1.ShowDialog()

        If FileIO.FileSystem.FileExists(OpenFileDialog1.FileName) = False Then
            Exit Sub
        End If

        'Asegurar que el archivo seleccionado tenga extensión
        Dim Extension As String = System.IO.Path.GetExtension(OpenFileDialog1.FileName)
        If Extension = "" Then
            MessageBox.Show("El archivo seleccinado no tiene una extensión válida", "Error de selección", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        'Asignar imagen al picterbox y a txtpathimagen
        'Se basa en el path que se asigna y el no
        'Se crea un FileStream para que despues libere y se pueda eliminar estando en el picturebox
        Dim fs As System.IO.FileStream
        fs = New System.IO.FileStream(OpenFileDialog1.FileName, IO.FileMode.Open, IO.FileAccess.Read)
        peImage.Image = System.Drawing.Image.FromStream(fs)
        Dim Unidad As String = "Bytes"
        Dim Tamaño As String = "0"
        Tamaño = CSistema.CalculaTamaño(fs.Length, Unidad)
        lblBytes.Text = CSistema.FormatoNumero(Tamaño, True) & " " & Unidad
        fs.Close()

        peImage.Image = ResizeImage(peImage.Image, New Size(700, 1200), True)

    End Sub

    ''''''''

    Public Shared Function ResizeImage(ByVal image As Image, ByVal size As Size, Optional ByVal preserveAspectRatio As Boolean = True) As Image

        Dim newWidth As Integer
        Dim newHeight As Integer

        If preserveAspectRatio Then
            Dim originalWidth As Integer = image.Width
            Dim originalHeight As Integer = image.Height
            Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
            Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
            Dim percent As Single = If(percentHeight < percentWidth, percentHeight, percentWidth)
            newWidth = CInt(originalWidth * percent)
            newHeight = CInt(originalHeight * percent)
        Else
            newWidth = size.Width
            newHeight = size.Height
        End If

        Dim newImage As Image = New Bitmap(newWidth, newHeight)
        Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
            'graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
            graphicsHandle.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
            graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight)
        End Using
        Return newImage

    End Function
    Sub ProcesarImagen(vIDCliente As Integer)

        Dim Mensaje As String = ""
        Dim Procesado As Boolean = False

        If peImage.Image Is Nothing Then
            Exit Sub
        End If

        Dim SQL(-1) As SqlClient.SqlParameter
        CSistema.SetSQLParameter(SQL, "@IDCliente", vIDCliente, ParameterDirection.Input)
        CSistema.SetSQLParameter(SQL, "@Imagen", CSistema.Image2Bytes(peImage.Image), ParameterDirection.Input, 0, SqlDbType.Image)




        'Salida
        CSistema.SetSQLParameter(SQL, "@Mensaje", Mensaje, ParameterDirection.Output)
        CSistema.SetSQLParameter(SQL, "@Procesado", Procesado, ParameterDirection.Output)

        'Ejecutamos
        If CSistema.ExecuteStoreProcedure2(SQL, "SpProductRegistroFirma", False, True) = False Then

        End If


    End Sub

    Private Sub btnBuscar_Click(sender As System.Object, e As System.EventArgs) Handles btnBuscar.Click
        SeleccionarImagen()
    End Sub

    Private Sub btnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btnGuardar.Click
        ProcesarImagen(IDCliente)
    End Sub
End Class

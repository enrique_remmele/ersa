﻿Public Class frmPropiedad

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Private TituloVentanaValue As String
    Public Property TituloVentana() As String
        Get
            Return TituloVentanaValue
        End Get
        Set(ByVal value As String)
            TituloVentanaValue = value
        End Set
    End Property

    Private TituloValue As String
    Public Property Titulo() As String
        Get
            Return TituloValue
        End Get
        Set(ByVal value As String)
            TituloValue = value
        End Set
    End Property

    Private ConsultaValue As String
    Public Property Consulta() As String
        Get
            Return ConsultaValue
        End Get
        Set(ByVal value As String)
            ConsultaValue = value
        End Set
    End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    'FUNIONES
    Sub Inicializar()

        'Formulario
        Me.Text = TituloVentana
        Me.txtTitulo.Text = Titulo

        'Consulta
        If Consulta <> "" Then
            dt = CSistema.ExecuteToDataTable(Consulta)
        End If

        If dt Is Nothing Then
            Exit Sub
        End If

        'Cargamos los Titulos
        Dim item As New ListViewItem
        For i As Integer = 0 To dt.Columns.Count - 1
            item = lvLista.Items.Add(dt.Columns(i).ColumnName)

            If IsNumeric(dt.Rows(0)(i).ToString) = True Then
                item.SubItems.Add(CSistema.FormatoNumero(dt.Rows(0)(i).ToString))
            Else
                item.SubItems.Add(dt.Rows(0)(i).ToString)
            End If

        Next

    End Sub

    Private Sub frmPropiedades_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

        If e.KeyCode = Keys.Enter Then
            Me.Close()
        End If
    End Sub

    Private Sub frmPropiedades_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub lvLista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvLista.SelectedIndexChanged

        txtValor.Clear()

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        txtValor.Text = lvLista.SelectedItems(0).SubItems(1).Text

    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        CSistema.LvToExcel(lvLista)
    End Sub

End Class
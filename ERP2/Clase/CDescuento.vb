﻿Public Class CDescuento


    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPEDADES
    Private IDProductoValue As Integer
    Public Property IDProducto() As Integer
        Get
            Return IDProductoValue
        End Get
        Set(ByVal value As Integer)
            IDProductoValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    'FUNCIONES
    Function ControlarDescuentoTactico(ByVal Porcentaje As Decimal, ByVal ImporteVenta As Decimal) As Boolean

        ControlarDescuentoTactico = False

        'Si no es descuento tactico
        Dim dtActividad As DataTable
        If vgData.Tables("VDetalleActividad") Is Nothing Then
            'CData.CreateTable("VDetalleActividad", "Select *  From VDetalleActividad", 10)
            CData.CreateTable("VDetalleActividad", "Exec SpViewDetalleActividad @IDUsuario = " & vgIDUsuario & ", @IDSucursal = " & vgIDSucursal & ", @IDDeposito = " & vgIDDeposito & ", @IDTerminal = " & vgIDTerminal & " ")
        End If

        dtActividad = CData.GetTable("VDetalleActividad").Copy
        dtActividad = CData.FiltrarDataTable(dtActividad, " IDProducto=" & IDProducto).Copy()
        If dtActividad.Rows.Count = 0 Then
            CData.Insertar(IDProducto, "VDetalleActividad")
            dtActividad = CData.GetTable("VDetalleActividad").Copy
            dtActividad = CData.FiltrarDataTable(dtActividad, " IDProducto=" & IDProducto).Copy()
        End If

        If dtActividad.Rows.Count = 0 Then
            MessageBox.Show("El producto no tiene descuentos tacticos", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If

        'Verificar si se controla
        If vgConfiguraciones("PedidoControlarTactico") = False Then
            'Si no se controla, retornar verdadero
            Return True
        End If

        'Controlar que no supere por porcentaje
        If vgConfiguraciones("PedidoControlarTacticoPorcentaje") = True Then
            Dim DescuentoMaximo As Decimal = dtActividad.Rows(0)("DescuentoMaximo")
            If Porcentaje > DescuentoMaximo Then
                MessageBox.Show("El porcentaje de descuento tactico es mayor al permitido!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return False
            End If
        End If

        'Controlar que no supere por importe
        If vgConfiguraciones("PedidoControlarTacticoImporte") = True Then

            'Hayar la actividad del producto seleccionado
            Dim IDActividad As Integer = 0

            For Each oRow As DataRow In dtActividad.Rows

                If IsNumeric(oRow("IDActividad")) = False Then
                    Return True
                End If

                IDActividad = oRow("IDActividad")

                'Sumamos lo acumulado en el detalle
                Dim DescuentoAcumulado As Decimal = CSistema.dtSumColumn(dtDescuento, "Total", " PlanillaTactico='True' And IDActividad=" & IDActividad)

                'Calulamos el descuento
                Dim Descuento As Decimal = CSistema.CalcularPorcentaje(ImporteVenta, Porcentaje, True, False)
                DescuentoAcumulado = DescuentoAcumulado + Descuento

                'Necesariamente hay que buscar el saldo de la actividad en la base de datos
                Dim dt As DataTable = CSistema.ExecuteToDataTable("Exec SpViewDetalleActividadConProducto @IDUsuario=" & vgIDUsuario & ", @IDSucursal=" & vgIDSucursal & ", @IDDeposito=" & vgIDDeposito & ", @IDTerminal=" & vgIDTerminal & ", @IDProducto=" & IDProducto & "")
                Dim Saldo As Decimal = dt.Rows(0)("SaldoReal")

                If DescuentoAcumulado > Saldo Then
                    MessageBox.Show("Este producto ya no tiene saldo de descuento!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return False
                End If

            Next

        End If

        Return True

    End Function

    Function GetPrecios(ByVal Producto As DataRow, ByVal Cantidad As Decimal, ByVal DetallePrecio As DataTable) As DataRow

        'Porcentaje de Descuento
        Dim Precio As Decimal = 0
        Dim PorcentajeDescuento As Decimal = 0
        Dim Descuento As Decimal = 0
        Dim TotalDescuento As Decimal = 0

        Dim dtPreciosProducto As New DataTable
        dtPreciosProducto.Columns.Add("Precio")
        dtPreciosProducto.Columns.Add("Cantidad")
        dtPreciosProducto.Columns.Add("PorcentajeDescuento")
        dtPreciosProducto.Columns.Add("DescuentoUnitario")
        dtPreciosProducto.Columns.Add("TotalDescuento")
        dtPreciosProducto.Columns.Add("Total")
        dtPreciosProducto.Columns.Add("TotalSinDescuento")
        dtPreciosProducto.Columns.Add("DescuentoUnitarioDiscriminado")
        dtPreciosProducto.Columns.Add("TotalDescuentoDiscriminado")

        Dim oRow As DataRow = dtPreciosProducto.NewRow

        'Descuentos
        If IsNumeric(Producto("Precio")) Then
            Precio = Producto("Precio")
        End If

        oRow("Precio") = Precio
        oRow("Cantidad") = Cantidad

        'Hayar porcentaje total y descueno
        PorcentajeDescuento = CSistema.dtSumColumn(DetallePrecio, "Porcentaje")
        Descuento = CSistema.CalcularPorcentaje(Precio, PorcentajeDescuento, True, False)

        oRow("PorcentajeDescuento") = CDec(PorcentajeDescuento)
        oRow("DescuentoUnitario") = Descuento
        oRow("TotalDescuento") = Descuento * Cantidad
        oRow("Total") = Precio * Cantidad
        oRow("TotalSinDescuento") = (Precio - Descuento) * Cantidad
        oRow("DescuentoUnitarioDiscriminado") = 0
        CSistema.CalcularIVA(Producto("IDImpuesto"), Descuento, True, False, oRow("DescuentoUnitarioDiscriminado"))
        oRow("TotalDescuentoDiscriminado") = oRow("DescuentoUnitarioDiscriminado") * Cantidad

        Return oRow

    End Function

    Function CargarDescuento(Producto As DataRow, DetallePrecio As DataTable, ByVal ID As Integer, ByVal Cantidad As Decimal, ByVal IDDeposito As Integer, ByVal IDSucursal As Integer, ByVal DescuentoTactico As Decimal)

        CargarDescuento = False

        Dim IDProducto As Integer = Producto("ID")
        Dim IDImpuesto As Integer = Producto("IDImpuesto")
        Dim Precio As Decimal = Producto("Precio")

        Dim IDTipoDescuento As Integer = -2
        Dim TipoDescuento As String = ""
        Dim Descuento As Decimal = 0
        Dim Porcentaje As Decimal = 0
        Dim PlanillaTactico As Boolean = False
        Dim TieneDescueno As Boolean = False

        For Each oRow As DataRow In CData.GetTable("VTipoDescuento").Rows

            'Verificar si existe el descuento en la ficha
            If DetallePrecio.Select(" IDTipo = " & oRow("ID")).Count = 0 Then
                TieneDescueno = False
            Else
                TieneDescueno = True
            End If

            'Si no tiene descuento en la ficha, entonces verificar si se puso descuento tactico
            If TieneDescueno = False Then

                'Solo si es tactico el descuento,
                'Pero solo si el descuento es tactico
                If oRow("DescuentoTactico") = True Then
                    If DescuentoTactico > 0 Then
                        TieneDescueno = True
                    Else
                        GoTo siguiente
                    End If
                Else
                    GoTo siguiente
                End If
            End If

            'Si es tactico, asignar lo que se envia como porcentaje
            If oRow("PlanillaTactico") = True AndAlso oRow("DescuentoTactico") = True Then
                Porcentaje = DescuentoTactico
                Descuento = CSistema.CalcularPorcentaje(Precio, Porcentaje, True, False) ' No se en que momento entra aqui a si que dejo como estaba para este caso
            Else
                'Porcentaje = DetallePrecio.Select(" IDTipo = " & oRow("ID"))(0)("Porcentaje")
                Descuento = DetallePrecio.Select(" IDTipo = " & oRow("ID"))(0)("Importe") 'Con esto ya esta
            End If

            IDTipoDescuento = oRow("ID")
            TipoDescuento = oRow("Descripcion")
            'Descuento = CSistema.CalcularPorcentaje(Precio, Porcentaje, True, False) 'japiro con este calculo
            PlanillaTactico = oRow("PlanillaTactico")


            'Descuento 
            Dim DescuentoDiscriminado As Decimal = 0
            Dim TotalDescuentoDiscriminado As Decimal = 0
            CSistema.CalcularIVA(IDImpuesto, Descuento, True, False, DescuentoDiscriminado)
            TotalDescuentoDiscriminado = Cantidad * DescuentoDiscriminado

            'Si no es tactico, agregamos normalmente
            If oRow("PlanillaTactico") = False Then
                Dim NewRow As DataRow = dtDescuento.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("IDProducto") = IDProducto
                NewRow("ID") = ID
                NewRow("IDDeposito") = IDDeposito
                NewRow("IDTipoDescuento") = IDTipoDescuento
                NewRow("Tipo") = TipoDescuento
                NewRow("PlanillaTactico") = PlanillaTactico
                NewRow("Descuento") = Descuento
                NewRow("Porcentaje") = Porcentaje
                NewRow("Cantidad") = Cantidad
                NewRow("Total") = Cantidad * Descuento
                NewRow("DescuentoDiscriminado") = DescuentoDiscriminado
                NewRow("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

                If Descuento <> 0 Then 'Se agrega <> en vez de > para que puedan hacerse excepciones en negativo y aumentar el precio

                    'Evitar duplicacion de descuentos
                    If dtDescuento.Select("Tipo='" & TipoDescuento & "'").Count > 0 Then
                        'GoTo siguiente dbordon 04/02/2016 Se comento porque no permitia agregar al pedido mas de 1 producto con el mismo tipo de descuesto
                    End If

                    dtDescuento.Rows.Add(NewRow)

                End If

            End If

            If oRow("PlanillaTactico") = True Then

                'Verificar primero si se tiene asignado en la ficha del producto
                'Descuentos TACTICOS y ACURDOS
                If Porcentaje = 0 Then
                    GoTo siguiente
                End If

                Dim dtActividad As DataTable = CData.GetTable("VDetalleActividad").Copy
                dtActividad = CData.FiltrarDataTable(dtActividad, " IDProducto=" & IDProducto).Copy()

                'Si es que hay un solo tactico, le ponemos todo
                If dtActividad.Rows.Count = 1 Then

                    Dim rowTactico As DataRow = dtDescuento.NewRow
                    rowTactico("IDTransaccion") = 0
                    rowTactico("IDProducto") = IDProducto
                    rowTactico("ID") = ID
                    rowTactico("IDDeposito") = IDDeposito
                    rowTactico("IDTipoDescuento") = IDTipoDescuento
                    rowTactico("Tipo") = TipoDescuento
                    rowTactico("PlanillaTactico") = PlanillaTactico
                    rowTactico("Descuento") = Descuento
                    rowTactico("Porcentaje") = Porcentaje
                    rowTactico("IDActividad") = dtActividad.Rows(0)("IDActividad")
                    rowTactico("Actividad") = dtActividad.Rows(0)("Codigo")
                    rowTactico("Cantidad") = Cantidad
                    rowTactico("Total") = Cantidad * Descuento
                    rowTactico("DescuentoDiscriminado") = DescuentoDiscriminado
                    rowTactico("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

                    If Descuento > 0 Then

                        'Evitar duplicacion de descuentos
                        If dtDescuento.Select("Tipo='" & TipoDescuento & "'").Count > 0 Then
                            GoTo siguiente
                        End If

                        dtDescuento.Rows.Add(rowTactico)
                    End If

                End If

                'Si tiene mas de 1 actividad
                If dtActividad.Rows.Count > 1 Then

                    'Llamamos al sistema de descuentos
                    Dim frm As New frmConfigurarDescuentosDeActividades
                    frm.dtActividad = dtActividad
                    frm.TipoDescuento = TipoDescuento
                    frm.rowDescuento = oRow
                    frm.IDProducto = IDProducto
                    frm.IDSucursal = IDSucursal
                    frm.IDDeposito = IDDeposito

                    frm.ShowDialog()
                    If frm.Seleccionado = False Then
                        Return False
                    End If

                    For Each oRowActividad As DataRow In frm.dtActividad.Select(" Sel = 'True' ")

                        Dim rowTactico As DataRow = dtDescuento.NewRow
                        rowTactico("IDTransaccion") = 0
                        rowTactico("IDProducto") = IDProducto
                        rowTactico("ID") = ID
                        rowTactico("IDDeposito") = IDDeposito
                        rowTactico("IDTipoDescuento") = IDTipoDescuento
                        rowTactico("Tipo") = TipoDescuento
                        rowTactico("PlanillaTactico") = PlanillaTactico
                        Descuento = CSistema.CalcularPorcentaje(Precio, oRowActividad("DescuentoMaximo"), True, False)
                        Porcentaje = oRowActividad("DescuentoMaximo")

                        'Calcular Descuento Unitario
                        CSistema.CalcularIVA(IDImpuesto, Descuento, True, False, DescuentoDiscriminado)
                        TotalDescuentoDiscriminado = Cantidad * DescuentoDiscriminado

                        rowTactico("Descuento") = Descuento
                        rowTactico("Porcentaje") = Porcentaje
                        rowTactico("Actividad") = oRowActividad("Mecanica")
                        rowTactico("IDActividad") = oRowActividad("IDActividad")
                        rowTactico("Cantidad") = Cantidad
                        rowTactico("Total") = Cantidad * Descuento
                        rowTactico("DescuentoDiscriminado") = DescuentoDiscriminado
                        rowTactico("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

                        If Descuento > 0 Then

                            'Evitar duplicacion de descuentos
                            If dtDescuento.Select("Tipo='" & TipoDescuento & "'").Count > 0 Then
                                GoTo siguiente
                            End If

                            dtDescuento.Rows.Add(rowTactico)
                        End If

                    Next

                End If

            End If

siguiente:

        Next

        'Actualizar Precio
        'rowDescuento("PorcentajeDescuento") = CSistema.dtSumColumn(dtDescuento, "Porcentaje", " IDProducto=" & IDProducto & " And ID=" & ID & " ")


        Return True

    End Function

    Function CargarDescuentoProductoPrecio(Producto As DataRow, DetallePrecio As DataTable, ByVal ID As Integer, ByVal Cantidad As Decimal, ByVal IDDeposito As Integer, ByVal IDSucursal As Integer, ByVal Unico As Boolean)

        CargarDescuentoProductoPrecio = False

        Dim IDProducto As Integer = Producto("ID")
        Dim IDImpuesto As Integer = Producto("IDImpuesto")
        Dim Precio As Decimal = Producto("Precio")

        Dim IDTipoDescuento As Integer = -2
        Dim TipoDescuento As String = ""
        Dim Descuento As Decimal = 0
        Dim Porcentaje As Decimal = 0
        'Dim PlanillaTactico As Boolean = False
        Dim TieneDescueno As Boolean = False

        For Each oRow As DataRow In CData.GetTable("VTipoDescuento", "DescuentosFijos=1").Rows
            'For Each value As String In Tipos

            'Verificar si existe el descuento en la ficha
            If DetallePrecio.Select(" IDTipo = " & oRow("ID")).Count = 0 Then
                TieneDescueno = False
            Else
                TieneDescueno = True
            End If

            If oRow("ID") = 5 And Unico = False Then
                TieneDescueno = False
            End If

            If oRow("ID") <> 5 And Unico = True Then
                TieneDescueno = False
            End If

            'Si no tiene descuento en la ficha, entonces verificar si se puso descuento tactico
            If TieneDescueno = False Then
                GoTo siguiente
            End If

            Descuento = Precio - DetallePrecio.Select(" IDTipo = " & oRow("ID"))(0)("Importe") 'Con esto ya esta
            'Porcentaje = DetallePrecio.Select(" IDTipo = " & oRow("ID"))(0)("Porcentaje")


            IDTipoDescuento = oRow("ID")
            TipoDescuento = oRow("Descripcion")
            'Descuento = CSistema.CalcularPorcentaje(Precio, Porcentaje, True, False) 'japiro con este calculo

            'Descuento 
            Dim DescuentoDiscriminado As Decimal = 0
            Dim TotalDescuentoDiscriminado As Decimal = 0
            CSistema.CalcularIVA(IDImpuesto, Descuento, True, False, DescuentoDiscriminado)
            TotalDescuentoDiscriminado = Cantidad * DescuentoDiscriminado

            'SC: 13122021 - Nueva porcion del codigo para usar la parte del acumulador
            If dtDescuento.Select(" IDProducto=" & IDProducto).Count > 0 Then
                Dim Rows() As DataRow = dtDescuento.Select(" IDProducto=" & IDProducto)
                Rows(0)("Cantidad") = Rows(0)("Cantidad") + Cantidad
                Rows(0)("Total") = Rows(0)("Cantidad") * Rows(0)("Descuento")
                Rows(0)("TotalDescuentoDiscriminado") = Rows(0)("TotalDescuentoDiscriminado") + TotalDescuentoDiscriminado
            Else
                'SC: Codigo Original Anterior 13122021
                Dim NewRow As DataRow = dtDescuento.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("IDProducto") = IDProducto
                NewRow("ID") = ID
                NewRow("IDDeposito") = IDDeposito
                NewRow("IDTipoDescuento") = IDTipoDescuento
                NewRow("Tipo") = TipoDescuento
                NewRow("PlanillaTactico") = False
                NewRow("Descuento") = Descuento
                NewRow("Porcentaje") = Porcentaje
                NewRow("Cantidad") = Cantidad
                NewRow("Total") = Cantidad * Descuento
                NewRow("DescuentoDiscriminado") = DescuentoDiscriminado
                NewRow("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

                If Descuento <> 0 Then 'Se agrega <> en vez de > para que puedan hacerse excepciones en negativo y aumentar el precio

                    'Evitar duplicacion de descuentos
                    If dtDescuento.Select("Tipo='" & TipoDescuento & "'").Count > 0 Then
                        'GoTo siguiente dbordon 04/02/2016 Se comento porque no permitia agregar al pedido mas de 1 producto con el mismo tipo de descuesto
                    End If

                    dtDescuento.Rows.Add(NewRow)

                End If
            End If

            'Dim NewRow As DataRow = dtDescuento.NewRow

            'NewRow("IDTransaccion") = 0
            'NewRow("IDProducto") = IDProducto
            'NewRow("ID") = ID
            'NewRow("IDDeposito") = IDDeposito
            'NewRow("IDTipoDescuento") = IDTipoDescuento
            'NewRow("Tipo") = TipoDescuento
            'NewRow("PlanillaTactico") = False
            'NewRow("Descuento") = Descuento
            'NewRow("Porcentaje") = Porcentaje
            'NewRow("Cantidad") = Cantidad
            'NewRow("Total") = Cantidad * Descuento
            'NewRow("DescuentoDiscriminado") = DescuentoDiscriminado
            'NewRow("TotalDescuentoDiscriminado") = TotalDescuentoDiscriminado

            'If Descuento <> 0 Then 'Se agrega <> en vez de > para que puedan hacerse excepciones en negativo y aumentar el precio

            '    'Evitar duplicacion de descuentos
            '    If dtDescuento.Select("Tipo='" & TipoDescuento & "'").Count > 0 Then
            '        'GoTo siguiente dbordon 04/02/2016 Se comento porque no permitia agregar al pedido mas de 1 producto con el mismo tipo de descuesto
            '    End If

            '    dtDescuento.Rows.Add(NewRow)

            'End If

siguiente:

        Next


        Return True

    End Function



    Sub VerDescuentos(lvLista As ListView)

        If lvLista.SelectedItems.Count = 0 Then
            Exit Sub
        End If

        Dim frm As New Form
        Dim dgv As New DataGridView
        frm.Controls.Add(dgv)
        dgv.Dock = DockStyle.Fill
        dgv.Columns.Add("Tipo", "Tipo")
        dgv.Columns.Add("Porcentaje", "%")
        dgv.Columns.Add("DescuentoUnitario", "Desc. Unit")
        dgv.Columns.Add("Cantidad", "Cant")
        dgv.Columns.Add("Total", "Total")
        dgv.Columns.Add("Actividad", "Actividad")

        Dim IDProducto As Integer = lvLista.SelectedItems(0).Text
        Dim ID As Integer = lvLista.SelectedItems(0).Index
        Dim Cantidad As Integer = lvLista.SelectedItems(0).SubItems(4).Text
        Dim DescuentoUnitario As Decimal = 0
        Dim Total As Decimal = 0
        Dim TotalPorcentaje As Decimal = 0

        For Each oRow As DataRow In dtDescuento.Select("IDProducto=" & IDProducto & " And ID=" & ID & " ")

            If oRow("Porcentaje") > 0 Then

                Dim Registro(dgv.Columns.Count - 1) As String

                Registro(0) = oRow("Tipo").ToString
                Registro(1) = oRow("Porcentaje").ToString
                Registro(2) = CSistema.FormatoMoneda(oRow("Descuento").ToString, True)
                Registro(3) = CSistema.FormatoNumero(oRow("Cantidad").ToString, True)
                Registro(4) = CSistema.FormatoMoneda(oRow("Total").ToString, True)
                Registro(5) = oRow("Actividad").ToString

                DescuentoUnitario = DescuentoUnitario + CDec(oRow("Descuento"))
                Total = Total + CDec(oRow("Total"))
                TotalPorcentaje = TotalPorcentaje + CDec(oRow("Porcentaje"))

                dgv.Rows.Add(Registro)

            End If

        Next

        Dim Totales(dgv.Columns.Count - 1) As String
        Totales(0) = "TOTALES"
        Totales(1) = CSistema.FormatoNumero(TotalPorcentaje, True)
        Totales(2) = CSistema.FormatoMoneda(DescuentoUnitario, True)
        Totales(3) = CSistema.FormatoNumero(Cantidad, True)
        Totales(4) = CSistema.FormatoMoneda(Total, True)
        Totales(5) = ""

        dgv.Rows.Add(Totales)

        'Formato
        dgv.RowHeadersVisible = False
        dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        dgv.AllowUserToAddRows = False
        dgv.AllowUserToDeleteRows = False
        dgv.AllowUserToResizeRows = False
        dgv.BackgroundColor = Color.White

        dgv.Columns("Actividad").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        dgv.Columns("Porcentaje").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("DescuentoUnitario").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("Cantidad").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        dgv.Columns("Porcentaje").DefaultCellStyle.Format = "N2"
        dgv.Columns("DescuentoUnitario").DefaultCellStyle.Format = "N2"
        dgv.Columns("Cantidad").DefaultCellStyle.Format = "N2"
        dgv.Columns("Total").DefaultCellStyle.Format = "N2"

        'Ultimo registro de totales
        dgv.Rows(dgv.Rows.Count - 1).DefaultCellStyle.Font = New Font(dgv.Font.FontFamily.Name, dgv.Font.Size, FontStyle.Bold)
        dgv.Rows(dgv.Rows.Count - 1).DefaultCellStyle.BackColor = Color.Beige


        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Size = New Size(450, 200)
        frm.Text = "(" & lvLista.SelectedItems(0).SubItems(1).Text & ") " & lvLista.SelectedItems(0).SubItems(2).Text
        frm.ShowDialog()

    End Sub

End Class

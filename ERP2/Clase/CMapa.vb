﻿Public Class CMapa

    Structure GLatLng
        Public Lat As Decimal
        Public Lng As Decimal
    End Structure

    Public Function GetPointsIntoPolygon(ByVal Polygono As List(Of GLatLng), ByVal punto As GLatLng) As Boolean
        Dim retorno As Boolean = False

        If Polygono Is Nothing Then
            Return False
        End If

        Dim x As Integer = 0
        Dim valor As Integer = 0
        x = 0
        valor = 0
        While x + 1 < Polygono.Count
            If IsOnRigth(Polygono(x), Polygono(x + 1), punto) Then
                valor += 1
            End If
            x += 1
        End While

        'si es impar entonces esta dentro de punto. 
        If (valor Mod 2) <> 0 Then
            retorno = True
        Else
            retorno = False
        End If

        Return retorno

    End Function

    Function LoadPoligon(ByVal pol As String, ByVal split1 As String, ByVal split2 As String) As List(Of CMapa.GLatLng)

        Dim returnvalue As New List(Of CMapa.GLatLng)
        Dim vector1() As String = pol.Split(split1)

        For i As Integer = 0 To vector1.GetLength(0) - 1
            Dim latlng As GLatLng
            Dim vector2() As String = vector1(i).Split(split2)
            latlng.Lat = vector2(0).Replace(".", ",")
            latlng.Lng = vector2(1).Replace(".", ",")

            returnvalue.Add(latlng)

        Next

        Return returnvalue

    End Function

    ''' <summary>
    ''' Retorna true si el punto esta a la derecha. 
    ''' </summary>
    ''' <param name="PolyPointA"></param>
    ''' <param name="PolyPointB"></param>
    ''' <param name="point"></param>
    ''' <returns></returns>
    Private Function IsOnRigth(ByVal PolyPointA As GLatLng, ByVal PolyPointB As GLatLng, ByVal point As GLatLng) As [Boolean]
        'x = Long
        'y = Lat
        Dim M As Double = 0
        Dim LngInFunction As Double = 0
        'Si el punto esta entre la Lat de los dos puntos
        If (PolyPointA.Lat >= point.Lat AndAlso PolyPointB.Lat <= point.Lat) OrElse (PolyPointB.Lat >= point.Lat AndAlso PolyPointA.Lat <= point.Lat) Then
            M = (PolyPointA.Lat - PolyPointB.Lat) / (PolyPointA.Lng - PolyPointB.Lng)
            LngInFunction = ((point.Lat - PolyPointA.Lat) / M) + PolyPointA.Lng
            'si esta a la derecha, sumo uno, sino no hago nada. 
            If LngInFunction <= point.Lng Then
                Return True
            End If
        End If
        Return False
    End Function

    Public Function GeoCoding(ByVal Lat As String, ByVal Lng As String) As String

        Dim NombreArchivo As String = ArchivoDisponible("xml")
        Dim urlGeo As String = "http://maps.googleapis.com/maps/api/geocode/xml?latlng=" & Lat & "," & Lng & "&sensor=false"
        My.Computer.Network.DownloadFile(urlGeo, NombreArchivo)
        Dim valores(0) As String
        Dim Index As Integer = 0
        Dim Nodo As String = ""

        Dim reader As System.Xml.XmlTextReader = New System.Xml.XmlTextReader(NombreArchivo)
        Do While (reader.Read())

            Dim Valor As String = ""

            Select Case reader.NodeType
                Case System.Xml.XmlNodeType.Element 'Mostrar comienzo del elemento.
                    Valor = Valor & "<" + reader.Name
                    If reader.HasAttributes Then 'If attributes exist
                        While reader.MoveToNextAttribute()
                            'Mostrar nombre y valor del atributo.
                            Valor = Valor & reader.Name & "=" & reader.Value
                        End While
                    End If
                    Valor = Valor & ">"
                Case System.Xml.XmlNodeType.Text 'Mostrar el texto de cada elemento.
                    Valor = Valor & reader.Value
                Case System.Xml.XmlNodeType.EndElement 'Mostrar final del elemento.
                    Valor = Valor & "</" + reader.Name
                    Valor = Valor & ">"
            End Select

            If Valor <> "" Then
                ReDim Preserve valores(Index)
                valores(Index) = Valor
                Index = Index + 1
            End If


        Loop

        Dim dt As New DataTable
        dt.Columns.Add("long_name")
        dt.Columns.Add("short_name")
        dt.Columns.Add("type")
        dt.Columns.Add("type2")

        For i As Integer = 0 To valores.GetLength(0) - 1
            If valores(i) = "<address_component>" Then
                Dim oRow As DataRow = dt.NewRow
                oRow("long_name") = valores(i + 2)
                oRow("short_name") = valores(i + 5)
                oRow("type") = valores(i + 8)

                If valores(i + 10) = "<type>" Then
                    oRow("type2") = valores(i + 11)
                Else
                    oRow("type2") = ""
                End If

                dt.Rows.Add(oRow)

            End If

        Next

        Dim Calle As String = ""
        Dim Region As String = ""
        Dim Ciudad As String = ""
        Dim Pais As String = ""


        Dim dt2 As New DataTable
        dt2.Columns.Add("Calle")
        dt2.Columns.Add("Ciudad")
        dt2.Columns.Add("Region")
        dt2.Columns.Add("Pais")

        Dim ReturnRow As DataRow = dt2.NewRow

        For Each oRow As DataRow In dt.Rows

            Select Case oRow("type").ToString
                Case "route"
                    If IsNumeric(oRow("long_name").ToString) = True Then
                        Calle = " Ruta=" & oRow("long_name").ToString
                    Else
                        Calle = " Calle=" & oRow("long_name").ToString
                    End If
                    ReturnRow("Calle") = oRow("long_name").ToString

                Case "locality"
                    Ciudad = " Ciudad=" & oRow("long_name").ToString
                    ReturnRow("Ciudad") = oRow("long_name").ToString
                Case "administrative_area_level_1"
                    Region = " Region=" & oRow("long_name").ToString
                    ReturnRow("Region") = oRow("long_name").ToString
                Case "country"
                    Pais = " Pais=" & oRow("long_name").ToString
                    ReturnRow("Pais") = oRow("long_name").ToString
            End Select

        Next

        If IsNumeric(ReturnRow("Calle").ToString) = True Then
            ReturnRow("Calle") = "Ruta " & ReturnRow("Calle").ToString
        End If

        GeoCoding = ReturnRow("Calle").ToString & ", " & ReturnRow("Ciudad").ToString & ", " & ReturnRow("Region").ToString & ", " & ReturnRow("Pais").ToString

    End Function

    Public Sub EliminarArchivos()

        Dim PathImage1 As String
        Dim PathImage2 As String
        Dim PathImage3 As String

        Dim Salir As Boolean = False

        Try
            For i As Integer = 0 To 1000
                PathImage1 = My.Application.Info.DirectoryPath & "\tmp" & i & ".bmp"
                PathImage2 = My.Application.Info.DirectoryPath & "\tmp" & i & ".jpg"
                PathImage3 = My.Application.Info.DirectoryPath & "\tmp" & i & ".xml"

                If IO.File.Exists(PathImage1) = True Then

                    Salir = False

                    Try
                        IO.File.Delete(PathImage1)
                    Catch ex As Exception

                    End Try
                Else
                    Salir = True
                End If

                If IO.File.Exists(PathImage2) = True Then

                    Salir = False

                    Try
                        IO.File.Delete(PathImage2)
                    Catch ex As Exception

                    End Try
                Else
                    Salir = True
                End If

                If IO.File.Exists(PathImage3) = True Then

                    Salir = False

                    Try
                        IO.File.Delete(PathImage3)
                    Catch ex As Exception

                    End Try
                Else
                    Salir = True
                End If

                If Salir = True Then
                    Exit For
                End If

            Next

        Catch ex As Exception

        End Try


    End Sub

    Private Function ArchivoDisponible(ByVal extension As String) As String

        ArchivoDisponible = My.Application.Info.DirectoryPath & "\tmp"

        For i As Integer = 0 To 1000
            If IO.File.Exists(ArchivoDisponible & i & "." & extension) = False Then
                ArchivoDisponible = ArchivoDisponible & i & "." & extension
                Exit For
            End If
        Next

    End Function

#Region "VERSION 2"

    Public Shared Function InsidePolygon(ByVal p As GLatLng, ByVal polygon As GLatLng()) As Boolean
        Dim angle As Double = 0
        Dim p1 As New GLatLng()
        Dim p2 As New GLatLng()

        For cont As Integer = 0 To polygon.Length - 1
            p1.Lng = polygon(cont).Lng - p.Lng
            p1.Lat = polygon(cont).Lat - p.Lat
            p2.Lng = polygon((cont + 1) Mod (polygon.Length)).Lng - p.Lng
            p2.Lat = polygon((cont + 1) Mod (polygon.Length)).Lat - p.Lat
            angle += Angle2D(p1.Lng, p1.Lat, p2.Lng, p2.Lat)
        Next

        Return (Not (Math.Abs(angle) < Math.PI))

    End Function

    Private Shared Function Angle2D(ByVal x1 As Double, ByVal y1 As Double, ByVal x2 As Double, ByVal y2 As Double) As Double
        Dim dtheta As Double, theta1 As Double, theta2 As Double
        Dim twopi As Double = 8.0 * Math.Atan(1.0)
        theta1 = Math.Atan2(y1, x1)
        theta2 = Math.Atan2(y2, x2)
        dtheta = theta2 - theta1

        While dtheta > Math.PI
            dtheta -= twopi
        End While

        While dtheta < (-Math.PI)
            dtheta += twopi
        End While

        Return (dtheta)
    End Function

    Public Function LoadPoligon2(ByVal pol As String, ByVal split1 As String, ByVal split2 As String) As GLatLng()

        Dim returnvalue(-1) As GLatLng
        Dim vector1() As String = pol.Split(split1)

        For i As Integer = 0 To vector1.GetLength(0) - 1
            Dim latlng As GLatLng
            Dim vector2() As String = vector1(i).Split(split2)

            If IsNumeric(vector2(0)) = False Then
                Exit For
            End If

            latlng.Lat = vector2(0).Replace(".", ",")
            latlng.Lng = vector2(1).Replace(".", ",")

            ReDim Preserve returnvalue(i)
            returnvalue(i) = latlng

        Next

        Return returnvalue

    End Function

#End Region
    

End Class

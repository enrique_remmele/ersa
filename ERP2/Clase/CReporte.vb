﻿Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows
Imports System.Drawing
Imports System.IO


Imports System.Threading.Tasks

Namespace Reporte

    Public Class CReporte

        'Timeout aumentada para algunas conexiones. Establecer a la propiedad SelectCommand TimeOut del SqlAdapter. Ejemplo en Sub ExistenciaValorizadaResumen //JGR 20140519
        Public Const CONNECTION_TIMEOUT_120 As Integer = 120

        'CLASES
        Protected CSistema As New CSistema
        Protected CData As New CData
        Protected CArchivoInicio As New CArchivoInicio

        'PROPIEDADES
        Private DescargandoValue As Boolean
        Public Property Descargando() As String
            Get
                Return DescargandoValue
            End Get
            Set(ByVal value As String)
                DescargandoValue = value
                If value = True Then
                    IniciarMostrarDescargando()
                Else
                    Try

                        If Not frmInformeProgreso.tProgreso Is Nothing Then
                            frmInformeProgreso.tProgreso.Abort()
                        End If

                        frmInformeProgreso.Close()

                    Catch ex As Exception

                    End Try

                End If
            End Set
        End Property

        Private ConsultandoValue As Boolean
        Public Property Consultando() As String
            Get
                Return ConsultandoValue
            End Get
            Set(ByVal value As String)
                ConsultandoValue = value
                If value = True Then
                    IniciarMostrarConsultando()
                Else
                    Try
                        If Not frmInformeProgreso.tProgreso Is Nothing Then
                            frmInformeProgreso.Cancelar = True
                            frmInformeProgreso.tProgreso.Abort()
                        End If

                        frmInformeProgreso.Close()

                    Catch ex As Exception

                    End Try
                End If
            End Set
        End Property

        Property frmInformeProgreso As New frmInformeProgreso

        'VARIABLES
        Dim tDescargando As Threading.Thread
        Dim tConsultando As Threading.Thread

        'FUNCIONES
        Protected Sub EstablecerConexion(ByVal report As ReportDocument)

            'Suprime la imagen de logo. JGR 20140604
            'No encontré la manera de modificar la imagen con esta version del Crystal.
            'El logo debe llamarse 'Picture3'. Puse este código aquí ya que es llamado por todos los informes.
            Try
                Dim pic As PictureObject = CType(report.ReportDefinition.ReportObjects("Picture3"), PictureObject)
                If pic IsNot Nothing Then
                    pic.ObjectFormat.EnableSuppress = True
                    'Dim MyfilePath As String = "C:\Visual Studio 2010\Projects\YourProject\YourimagesDir\YourImageName.jpg"
                    'conditionFormula.Text = "{TableName.TableFeild}" 'Your database field with the 
                End If
            Catch
            End Try


            Dim CRTLI As CrystalDecisions.Shared.TableLogOnInfo
            For Each CRTable In report.Database.Tables
                CRTLI = CRTable.LogOnInfo
                With CRTLI.ConnectionInfo
                    .ServerName = vgNombreServidor
                    .UserID = vgNombreUsuarioBD
                    .Password = vgPasswordBD
                    .DatabaseName = vgNombreBaseDatos
                    .Type = CrystalDecisions.Shared.ConnectionInfoType.Query
                    .IntegratedSecurity = False
                End With
                CRTable.ApplyLogOnInfo(CRTLI)
            Next CRTable

        End Sub

        ''' <summary>
        ''' Compara la revisión del archivo local con la de la base de datos para saber si debe actualizar el archivo .rpt local
        ''' </summary>
        ''' <param name="report">Objeto reporte</param>
        ''' <param name="Name">Nombre físico del archivo de informe .rpt</param>
        ''' <remarks></remarks>
        Protected Sub LoadReport(ByRef report As ReportDocument, ByVal Name As String)
            Try
                'Verificar que exista la carpeta de reportes
                If FileIO.FileSystem.DirectoryExists(VGCarpetaReporte) = False Then
                    FileIO.FileSystem.CreateDirectory(VGCarpetaReporte)
                End If

                Dim revisionLocal As Integer = 0
                If IO.File.Exists(VGCarpetaReporte & Name & ".rpt") Then
                    report.FileName = VGCarpetaReporte & Name & ".rpt"
                    Descargando = True
                    If report.SummaryInfo.ReportComments IsNot Nothing Then
                        revisionLocal = CType(report.SummaryInfo.ReportComments, Integer)
                    End If
                End If

                Dim revisionBD As Integer = 0
                'Dim rptNombre As String = System.IO.Path.GetFileNameWithoutExtension(Name)
                revisionBD = CType(CSistema.ExecuteScalar("SELECT ISNULL(MAX(version),0) FROM Informe WHERE nombre = '" & Name & "'",, 6000), Integer)
                'Se comenta para que descague automaticamente de la BD porque en algunos casos no actualizaba --DBS 12/10/2016
                'If revisionBD > 0 AndAlso revisionLocal < revisionBD Then

                Descargando = True
                'Get File data from dataset row.
                Dim FileData As Byte() = Nothing
                FileData = DirectCast(CSistema.ExecuteScalar("SELECT TOP 1 archivo FROM Informe WHERE nombre = '" &
                                                  Name & "' ORDER BY version DESC",, 60000), Byte())

                If FileData IsNot Nothing Then
                    'Write file data to selected file.
                    Using fs As New FileStream(VGCarpetaReporte & Name & ".rpt", FileMode.Create)
                        fs.Write(FileData, 0, FileData.Length)
                        fs.Close()
                    End Using
                    report.FileName = VGCarpetaReporte & Name & ".rpt"

                End If

                Descargando = False
                'End If
            Catch ex As Exception
                Descargando = False
                Throw
            End Try

            If Descargando = True Then
                Descargando = False
            End If

            'Dim ArchivoLocal As String = VGCarpetaReporte & Name & ".rpt"
            'Dim ArchivoTemporal As String = VGCarpetaReporteTemporal & Name & ".rpt"

            ''Verificar que exista la carpeta de reportes
            'If FileIO.FileSystem.DirectoryExists(VGCarpetaReporte) = False Then
            '    FileIO.FileSystem.CreateDirectory(VGCarpetaReporte)
            'End If

            ''Verificar que exista el archivo
            'If FileIO.FileSystem.FileExists(ArchivoLocal) = False Then

            '    'Buscamos en la carpeta temporal de informes
            '    Dim NuevoPath As String = ""
            '    If ExisteReporte(VGCarpetaReporteTemporal, Name, NuevoPath) Then

            '        'Copiar reporte
            '        FileIO.FileSystem.CopyFile(NuevoPath, ArchivoLocal, True)

            '    Else

            '        MessageBox.Show("El sistema no encontro el reporte", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)

            '        'Bucamos el archivo
            '        Dim f As New FolderBrowserDialog
            '        f.Description = "Directorio de Reportes SAIN"
            '        f.SelectedPath = VGCarpetaReporteTemporal
            '        f.ShowNewFolderButton = False

            '        If f.ShowDialog() = DialogResult.Cancel Then
            '            MessageBox.Show("No es posible emitir el informe", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            '            Exit Sub
            '        End If

            '        VGCarpetaReporteTemporal = f.SelectedPath & "\"
            '        ArchivoTemporal = VGCarpetaReporteTemporal & Name & ".rpt"

            '        If ExisteReporte(VGCarpetaReporteTemporal, Name, NuevoPath) = True Then
            '            FileIO.FileSystem.CopyFile(NuevoPath, ArchivoLocal)
            '            CArchivoInicio.IniWrite(VGArchivoINI, "INFORMES", "PATH", VGCarpetaReporteTemporal)
            '        End If

            '    End If

            '    If FileIO.FileSystem.FileExists(ArchivoLocal) = False Then
            '        MessageBox.Show("El sistema no encontro el reporte. Vuelva a intentarlo", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            '        Exit Sub
            '    End If

            'Else

            '    Try
            '        'Verificar si es que no hay un nuevo reporte
            '        Dim Archivo1 As Date = System.IO.File.GetLastWriteTime(ArchivoLocal).Date
            '        Dim Archivo2 As Date = System.IO.File.GetLastWriteTime(ArchivoTemporal).Date

            '        If Archivo1 < Archivo2 Then
            '            FileIO.FileSystem.CopyFile(ArchivoTemporal, ArchivoLocal, True)
            '        End If
            '    Catch ex As Exception

            '    End Try

            'End If

            'rpt.FileName = ArchivoLocal

        End Sub

        Private Function ExisteReporte(ByVal dir As String, ByVal rpt As String, ByRef NuevoPath As String) As Boolean

            ExisteReporte = False

            'Archivos dentro de la carpeta
            For Each F As String In Directory.GetFiles(dir)

                If System.IO.Path.GetExtension(F) = ".rpt" Then
                    Dim Archivo As String = dir & rpt & ".rpt"
                    If F.ToUpper = Archivo.ToUpper Then
                        NuevoPath = F
                        Return True
                    End If
                End If
            Next

            'Buscar otras carpetas
            For Each d As String In Directory.GetDirectories(dir)

                If Directory.GetDirectories(d).Count > 0 Then
                    If ExisteReporte(d, rpt, NuevoPath) = True Then
                        Return True
                    End If
                End If

            Next

        End Function

        Protected Sub EstablecerTitulos(ByRef rpt As ReportClass, ByVal Titulo As String, ByVal TipoInforme As String)

            Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
            txtField = Nothing

            For Each a As Section In rpt.ReportDefinition.Sections
                For i As Integer = 0 To a.ReportObjects.Count - 1

                    Try
                        Debug.Print(a.ReportObjects.Item(i).GetType.Name)

                        If a.ReportObjects.Item(i).GetType.Name = "TextObject" Or a.ReportObjects.Item(i).GetType.Name = "FieldHeadingObject" Then
                            txtField = CType(a.ReportObjects.Item(i), CrystalDecisions.CrystalReports.Engine.TextObject)

                            Select Case a.ReportObjects.Item(i).Name
                                Case "txtResponsable"
                                    VGFechaHoraSistema = CSistema.ExecuteScalar("select getdate()")
                                    txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString
                                Case "txtSistema"
                                    txtField.Text = VGSoftwareInfo
                                Case "txtTitulo"
                                    txtField.Text = "ERSA - " & Titulo.ToUpper
                                Case "txtTipoInforme"
                                    txtField.Text = TipoInforme
                                Case "txtEmpresa"
                                    txtField.Text = vgLicenciaOtorgadoRazonSocial & " - " & vgLicenciaOtorgadoRUC
                            End Select

                        End If

                    Catch ex As Exception

                    End Try


                Next
            Next

        End Sub

        ''' <summary>
        ''' Establece el texto para un campo de texto específico dentro del informe
        ''' </summary>
        ''' <param name="rpt">Objeto Informe</param>
        ''' <param name="nombre">Nombre del campo de texto (name)</param>
        ''' <param name="texto">Texto a mostrar</param>
        ''' <remarks></remarks>
        Protected Sub EstablecerTextoCampo(ByRef rpt As ReportClass, nombre As String, ByVal texto As String)

            Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
            txtField = Nothing

            For Each a As Section In rpt.ReportDefinition.Sections
                For i As Integer = 0 To a.ReportObjects.Count - 1

                    Try
                        Debug.Print(a.ReportObjects.Item(i).GetType.Name)

                        If a.ReportObjects.Item(i).GetType.Name = "TextObject" Then
                            txtField = CType(a.ReportObjects.Item(i), CrystalDecisions.CrystalReports.Engine.TextObject)

                            Select Case a.ReportObjects.Item(i).Name
                                Case nombre
                                    txtField.Text = texto
                                    Exit Sub
                            End Select

                        End If

                    Catch
                        Exit For
                    End Try


                Next
            Next

        End Sub

        Protected Sub MostrarReporte(ByVal rpt As ReportClass, ByVal Titulo As String, Optional ByVal Parametros As ParameterFields = Nothing, Optional ByVal ds As DataSet = Nothing)

            Try
                If Descargando = True Then
                    Descargando = False
                End If

                If Consultando = True Then
                    Consultando = False
                End If
                frmInformeProgreso.Cancelar = True
                frmInformeProgreso.Close()

            Catch ex As Exception

            End Try

            Dim frmReporte As New frmReporte

            'Configuracion del Reporte
            frmReporte.CrystalReportViewer1.ReportSource = rpt
            frmReporte.dsTemp = ds
            frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None

            If rpt.DataDefinition.Groups.Count > 0 Then
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
            Else
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
            End If

            frmReporte.CrystalReportViewer1.AutoSize = True
            frmReporte.StartPosition = FormStartPosition.CenterScreen

            frmReporte.Location = New Point(10, 10)
            frmReporte.Size = New Size(frmPrincipal2.ClientSize.Width - 10, frmPrincipal2.ClientSize.Height - 10)
            frmReporte.WindowState = FormWindowState.Maximized
            frmReporte.Icon = frmPrincipal2.Icon

            If Not Parametros Is Nothing Then
                frmReporte.CrystalReportViewer1.ParameterFieldInfo = Parametros
            End If

            If Titulo = "" Then
                frmReporte.Text = VGSoftwareInfo
            Else
                frmReporte.Text = VGSoftwareNombre & " :: " & Titulo
            End If

            'frmReporte.ShowDialog()
            frmReporte.Show()
        End Sub
        Protected Sub MostrarReportePrincipal(ByVal rpt As ReportClass, ByVal Titulo As String, Optional ByVal Parametros As ParameterFields = Nothing, Optional ByVal ds As DataSet = Nothing)

            Try
                If Descargando = True Then
                    Descargando = False
                End If

                If Consultando = True Then
                    Consultando = False
                End If
                frmInformeProgreso.Cancelar = True
                frmInformeProgreso.Close()

            Catch ex As Exception

            End Try

            Dim frmReporte As New frmReporte

            'Configuracion del Reporte
            frmReporte.CrystalReportViewer1.ReportSource = rpt
            frmReporte.dsTemp = ds
            frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None

            If rpt.DataDefinition.Groups.Count > 0 Then
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
            Else
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
            End If

            frmReporte.CrystalReportViewer1.AutoSize = True
            frmReporte.StartPosition = FormStartPosition.CenterScreen

            frmReporte.Location = New Point(10, 10)
            frmReporte.Size = New Size(frmPrincipal2.ClientSize.Width - 10, frmPrincipal2.ClientSize.Height - 10)
            frmReporte.WindowState = FormWindowState.Maximized
            frmReporte.Icon = frmPrincipal2.Icon

            If Not Parametros Is Nothing Then
                frmReporte.CrystalReportViewer1.ParameterFieldInfo = Parametros
            End If

            If Titulo = "" Then
                frmReporte.Text = VGSoftwareInfo
            Else
                frmReporte.Text = VGSoftwareNombre & " :: " & Titulo
            End If

            frmReporte.ShowDialog()
        End Sub

        Sub ArmarSubTitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox, ByVal txtDesde As ocxTXTDate, ByVal txtHasta As ocxTXTDate)

            SubTitulo = SubTitulo & " Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " "

            'Establecemos los filtros
            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next

        End Sub

        Sub ArmarSubtitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox)

            'Establecemos los filtros
            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next
        End Sub

        Sub ArmarSubTitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox, ByVal txtDesde As ocxTXTDate, ByVal txtHasta As ocxTXTDate, ByVal cbxTipo As ocxCBX, ByVal ChkResumido As ocxCHK)

            Dim InformeResumido As String
            If ChkResumido.Valor = False Then
                InformeResumido = "DETALLADO"
            Else
                InformeResumido = "RESUMIDO POR DIA"
            End If

            'Establecemos los filtros
            SubTitulo = "Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " - Tipo: " & cbxTipo.Texto & " - Informe: " & InformeResumido & ""

            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next

        End Sub

        Sub ArmarSubTitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox, ByVal nudRanking As NumericUpDown, ByVal cbxVentas As ocxCBX, ByVal cbxMoneda As ocxCBX, ByVal cbxOrdenadoPor As ocxCBX, ByVal cbxEnForma As ocxCBX, ByVal txtDesde As ocxTXTDate, ByVal txtHasta As ocxTXTDate)

            SubTitulo = "Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " - Ranking: " & nudRanking.Value.ToString & " - " & cbxVentas.Texto & " - " & cbxMoneda.Texto & " - Ordenado por: " & cbxOrdenadoPor.Texto & "/" & cbxEnForma.Texto & ""

            'Establecemos los filtros
            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next

        End Sub

        Sub ArmarSubTitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox, ByVal nudRanking As NumericUpDown, ByVal cbxMoneda As ocxCBX, ByVal cbxOrdenadoPor As ocxCBX, ByVal cbxEnForma As ocxCBX, ByVal txtDesde As ocxTXTDate, ByVal txtHasta As ocxTXTDate)

            SubTitulo = "Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " - Ranking: " & nudRanking.Value.ToString & " - " & cbxMoneda.Texto & " - Ordenado por: " & cbxOrdenadoPor.Texto & "/" & cbxEnForma.Texto & ""

            'Establecemos los filtros
            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next

        End Sub

        Public Sub GetPapersizeID(ByVal Reporte As CrystalDecisions.CrystalReports.Engine.ReportClass, ByVal PrinterName As String, ByVal PaperSizeName As String)

            Dim doctoprint As New System.Drawing.Printing.PrintDocument()
            Dim ppname As String = ""

            doctoprint.PrinterSettings.PrinterName = PrinterName

            If vgIDPaperSize >= 0 Then
                Reporte.PrintOptions.PaperSize = DirectCast(doctoprint.PrinterSettings.PaperSizes(vgIDPaperSize).RawKind, CrystalDecisions.Shared.PaperSize)
            Else
                For i As Integer = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1

                    Dim rawKind As Integer = 0
                    ppname = PaperSizeName

                    If doctoprint.PrinterSettings.PaperSizes(i).PaperName = ppname Then
                        Reporte.PrintOptions.PaperSize = DirectCast(doctoprint.PrinterSettings.PaperSizes(i).RawKind, CrystalDecisions.Shared.PaperSize)
                        vgIDPaperSize = i
                        Exit For
                    End If

                Next
            End If

        End Sub

        Sub GuardarLote(path As String)

            For Each vfile As String In Directory.GetFiles(path)
                If vfile.ToUpper.Contains(".RPT") Then

                    Dim Archivo As String = vfile
                    Dim Version As Integer = 0
                    Dim report As New CrystalDecisions.CrystalReports.Engine.ReportClass
                    report.FileName = Archivo
                    If report.SummaryInfo.ReportComments IsNot Nothing Then
                        Version = CType(report.SummaryInfo.ReportComments, Integer)
                    End If

                    Dim filebyte As Byte() = Nothing

                    Try
                        filebyte = System.IO.File.ReadAllBytes(Archivo)
                        If (filebyte IsNot Nothing) Then
                            Dim strSql As String = "INSERT INTO Informe (nombre, archivo, version, usuario, observacion) VALUES (@nombre, @archivo, @version, @usuario, @observacion)"
                            Using sqlConn As SqlConnection = New SqlConnection(VGCadenaConexion)
                                Using sqlComm As SqlCommand = New SqlCommand(strSql, sqlConn)
                                    sqlComm.Parameters.Add("@nombre", SqlDbType.VarChar, 50).Value = System.IO.Path.GetFileNameWithoutExtension(Archivo)
                                    sqlComm.Parameters.Add("@archivo", SqlDbType.Binary).Value = filebyte
                                    sqlComm.Parameters.Add("@version", SqlDbType.Int).Value = Version
                                    sqlComm.Parameters.Add("@usuario", SqlDbType.VarChar, 50).Value = "ADM"
                                    sqlComm.Parameters.Add("@observacion", SqlDbType.VarChar, 500).Value = ""
                                    sqlConn.Open()
                                    sqlComm.ExecuteNonQuery()
                                    sqlComm.CommandText = "DELETE FROM Informe WHERE nombre = '" & System.IO.Path.GetFileNameWithoutExtension(Archivo) & "' AND version < " & Version
                                    sqlComm.ExecuteNonQuery()

                                End Using
                            End Using

                        End If
                    Catch Ex As Exception
                        MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Finally
                        ' Check this again, since we need to make sure we didn't throw an exception on open. 
                        If (filebyte IsNot Nothing) Then
                            filebyte = Nothing
                        End If
                    End Try
                End If
            Next

            If Directory.GetDirectories(path).Length > 0 Then
                For Each vpath As String In Directory.GetDirectories(path)
                    GuardarLote(vpath)
                Next
            End If

        End Sub

        Sub MostrarDescargando()

            frmInformeProgreso = New frmInformeProgreso
            frmInformeProgreso.Comentario = "Descargando reporte..."
            frmInformeProgreso.Titulo = "Descargando reporte..."

            Try

                'Esperar 3 seg.
                For i As Integer = 0 To 2

                    If Descargando = False Then
                        Exit For
                    End If

                    Threading.Thread.Sleep(1000)

                Next

                'Esperar 3 seg.
                Dim Abierto As Boolean = False

                Do While Descargando = True

                    For Each f As Form In Application.OpenForms
                        If f.Name = frmInformeProgreso.Name Then
                            Abierto = True
                        End If
                    Next

                    If Abierto = False Then
                        If frmInformeProgreso.IsDisposed = False Then
                            frmInformeProgreso.ShowDialog(frmPrincipal2)
                        End If
                    End If

seguir:
                    Threading.Thread.Sleep(1000)

                Loop
            Catch ex As Exception

            End Try

            frmInformeProgreso.Close()

        End Sub

        Sub IniciarMostrarDescargando()

            tDescargando = New Threading.Thread(AddressOf MostrarDescargando)
            tDescargando.Start()

        End Sub

        Sub MostrarConsultando()

            frmInformeProgreso = New frmInformeProgreso
            frmInformeProgreso.Comentario = "Consultando la Base de Datos..."
            frmInformeProgreso.Titulo = "Consultando"

            'Esperar 3 seg.
            For i As Integer = 0 To 2

                If Consultando = False Then
                    Exit For
                End If

                Threading.Thread.Sleep(1000)

            Next

            'Esperar 3 seg.
            Dim Abierto As Boolean = False

            Try
                Do While Consultando = True

                    For Each f As Form In Application.OpenForms
                        If f.Name = frmInformeProgreso.Name Then
                            Abierto = True
                        End If
                    Next

                    If Abierto = False Then
                        frmInformeProgreso.ShowDialog(frmPrincipal2)
                    End If

seguir:
                    Threading.Thread.Sleep(1000)

                Loop
            Catch ex As Exception

            End Try

            Try
                frmInformeProgreso.Close()
            Catch ex As Exception

            End Try

        End Sub

        Sub IniciarMostrarConsultando()

            tConsultando = New Threading.Thread(AddressOf MostrarConsultando)
            tConsultando.Start()

        End Sub

    End Class

    Public Class CReporteVentas
        Inherits CReporte

        Dim DetalleVentaCampos As String = "IDTransaccion, IDProducto, Producto, ReferenciaProveedor, Proveedor, Descripcion, Linea, SubLinea, SubLinea2, TipoProducto, Marca, PesoUnitario, PorcentajeDescuento, UnidadMedida, Referencia, ControlarExistencia, Proveedor, Deposito, Cantidad, PrecioUnitario, PrecioNeto, Peso, IDCliente, Cliente, DireccionAlternativa, IDSucursalCliente, ReferenciaCliente, MesNumero, NroComprobante, Comprobante, Fecha, Condicion, Vendedor, Total, Bruto, TotalImpuesto, TotalDiscriminado, TotalDescuento, Descuento, TotalDescuentoDiscriminado, TotalPrecioNeto, TotalCosto, TotalCostoImpuesto, CantidadCaja, Cajas, TotalBruto, TotalSinDescuento, TotalSinImpuesto, TotalNeto, Utilidad, TipoCliente, FormaPagoFactura, CancelarAutomatico "

        Private Sub DescontarDevolucionesVenta(ByRef DS As DataSet, ByVal Where As String)

            Try

                'Obtenemos la tabla matriz
                Dim dt As DataTable = DS.Tables("VVenta")

                Dim CamposACambiar() As String = {"Total", "TotalImpuesto", "TotalDiscriminado", "TotalDescuento", "TotalBruto"}

                'Obtenemos las devoluciones
                Dim dtDevoluciones As DataTable = CSistema.ExecuteToDataTable("Select * from VVentaDevoluciones " & Where)

                'Recorremos la tabla de NC y restamos las devoluciones
                For Each DevolucionRow As DataRow In dtDevoluciones.Rows

                    Dim IDTransaccion As Integer = DevolucionRow("IDTransaccion")

                    Dim Filtro As String = " IDTransaccion=" & IDTransaccion

                    For Each VentaRow As DataRow In dt.Select(Filtro)
                        For c As Integer = 0 To CamposACambiar.GetLength(0) - 1
                            VentaRow(CamposACambiar(c)) = VentaRow(CamposACambiar(c)) - DevolucionRow(CamposACambiar(c))
                        Next
                    Next

                Next

            Catch ex As Exception
                MessageBox.Show("No se pudo aplicar las devoluciones! El sistema produjo un error. " & ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Sub

        Private Sub DescontarDevolucionesDetalle(ByRef DS As DataSet, ByVal Where As String, Optional ByVal vCampos() As String = Nothing)

            Try

                'Obtenemos la tabla matriz
                Dim dt As DataTable = DS.Tables("VDetalleVenta")

                Dim CamposACambiar() As String = {"Cantidad", "Total", "Bruto", "TotalImpuesto", "TotalDiscriminado", "TotalDescuento", "Descuento", "TotalPrecioNeto", "TotalCosto", "TotalCostoImpuesto", "TotalCostoDiscriminado", "CantidadCaja", "Cajas", "TotalBruto", "TotalSinDescuento", "TotalSinImpuesto", "TotalNeto", "TotalNetoConDescuentoNeto"}

                If vCampos IsNot Nothing Then
                    CamposACambiar = vCampos
                End If

                'Obtenemos las devoluciones
                Dim dtDevoluciones As DataTable = CSistema.ExecuteToDataTable("Select IDTransaccion, IDProducto, Cantidad, Total, Bruto, TotalImpuesto, TotalDiscriminado, TotalDescuento, Descuento, TotalPrecioNeto, TotalCosto, TotalCostoImpuesto, TotalCostoDiscriminado, CantidadCaja, Cajas, TotalBruto, TotalSinDescuento, TotalSinImpuesto, TotalNeto, TotalNetoConDescuentoNeto from VDetalleVentaDevoluciones " & Where & " Order By IDTransaccion, IDProducto")

                Debug.Print(CSistema.dtSumColumn(dtDevoluciones, "TotalDiscriminado"))
                'Recorremos la tabla de NC y restamos las devoluciones
                For Each DevolucionRow As DataRow In dtDevoluciones.Rows

                    Dim IDTransaccion As Integer = DevolucionRow("IDTransaccion")
                    Dim IDProducto As Integer = DevolucionRow("IDProducto")

                    Dim Filtro As String = " IDTransaccion=" & IDTransaccion & " And IDProducto=" & IDProducto

                    If dt.Select(Filtro).Count = 1 Then
                        For Each VentaRow As DataRow In dt.Select(Filtro)
                            For c As Integer = 0 To CamposACambiar.GetLength(0) - 1
                                VentaRow(CamposACambiar(c)) = VentaRow(CamposACambiar(c)) - DevolucionRow(CamposACambiar(c))
                            Next
                        Next
                    Else

                        'Pasamos el registro de devolucion a un registro temporal, en el cual acumularemos los saldos
                        Dim SaldoRow As DataRow = dtDevoluciones.NewRow
                        SaldoRow.ItemArray = DevolucionRow.ItemArray

                        'Recorremos el detalle de la venta
                        Dim v() As DataRow = dt.Select(Filtro)
                        For Each VentaRow As DataRow In v

                            'Recorremos campo por campo definido en la variable "CamposACambiar"
                            For c As Integer = 0 To CamposACambiar.GetLength(0) - 1

                                'Si el saldo acumulado ya es 0, no hace falta descontar nada,
                                'ya que en teoria ya no hay saldo en devoluciones
                                If SaldoRow(CamposACambiar(c)) <= 0 Then
                                    GoTo siguiente
                                End If

                                'Calculo interno, para saber la diferencia
                                Dim SaldoTemp As Decimal = SaldoRow(CamposACambiar(c)) - VentaRow(CamposACambiar(c))

                                'Si saldo es mayor a 0, entonces restar el saldo acumulado
                                'y se entiende que la venta es igual a 0
                                If SaldoTemp > 0 Then
                                    SaldoRow(CamposACambiar(c)) = SaldoRow(CamposACambiar(c)) - VentaRow(CamposACambiar(c))
                                    VentaRow(CamposACambiar(c)) = 0
                                Else
                                    'Es necesario guardar temporalmente el saldo acumulado en una variable "SaldoTemp"
                                    SaldoTemp = SaldoRow(CamposACambiar(c))
                                    'Actualizamos el saldo acumulado
                                    SaldoRow(CamposACambiar(c)) = SaldoRow(CamposACambiar(c)) - VentaRow(CamposACambiar(c))
                                    'Actualizamos la venta, con el saldo temporal guardado
                                    VentaRow(CamposACambiar(c)) = VentaRow(CamposACambiar(c)) - SaldoTemp
                                End If
siguiente:

                            Next
                        Next

                    End If
                Next

                'Por ultimo, sacamos todos los que quedaron en 0 en el detalle de venta
                dt = CData.FiltrarDataTable(dt, " Cantidad>0")
                DS.Tables.Remove("VDetalleVenta")
                DS.Tables.Add(dt)


            Catch ex As Exception
                MessageBox.Show("No se pudo aplicar las devoluciones! El sistema produjo un error. " & ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Sub

        Private Sub SepararSucursalClienteDetalle(ByRef DS As DataSet)

            'Obtenemos la tabla matriz
            Dim dt As DataTable = DS.Tables("VDetalleVenta")

            For Each oRow As DataRow In dt.Rows
                oRow("Cliente") = oRow("DireccionAlternativa") & "(" & oRow("ReferenciaCliente") & ")"
            Next

        End Sub

        Private Sub SepararSucursalClienteVenta(ByRef DS As DataSet)

            'Obtenemos la tabla matriz
            Dim dt As DataTable = DS.Tables("VVenta")

            For Each oRow As DataRow In dt.Rows
                oRow("Cliente") = oRow("DireccionAlternativa")
            Next

        End Sub

        Sub ListadoFacturasEmitidasProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Usuario As String, ByVal Vista As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'SC: 07-03-2022 Se modifica la vista para obtener resumido por producto
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta" & Where & Usuario & " " & OrderBy, conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From " & Vista & " " & WhereDetalle & Usuario & " " & OrderBy, conn)
            'Dim WhereDetalle As String = Where.Replace("vventa.", "VDetalleVenta.")
            Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select * from VDetalleVenta " & WhereDetalle & Usuario)
            'SC: 07-03-2022 Se modifica el where para obtener resumido por producto
            'Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select * from VDetalleVenta " & Where & Usuario)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                aTable1.SelectCommand.CommandTimeout = 200
                If Vista = "VVentayDetalle1" Then
                    DSDatos.Tables.Add("VVentayDetalle1")
                End If
                If Vista = "VVentayDetalle" Then
                    DSDatos.Tables.Add("VVentayDetalle")
                End If


                conn.Open()
                If Vista = "VVentayDetalle1" Then
                    aTable1.Fill(DSDatos.Tables("VVentayDetalle1"))
                End If
                If Vista = "VVentayDetalle" Then
                    aTable1.Fill(DSDatos.Tables("VVentayDetalle"))
                End If
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If Vista = "VVentayDetalle1" Then
                    LoadReport(Report, "rptListadoFacturasEmitidasConDescuento_Resumido")
                End If
                If Vista = "VVentayDetalle" Then
                    LoadReport(Report, "rptListadoFacturasEmitidasConDescuento_Resumido1")
                End If
                Dim TotalDescuento As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDescuento")
                Dim TotalDiscriminado As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado")
                Dim TotalDescuentoDiscriminado As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDescuentoDiscriminado")
                Dim TotalSinDescuentoDiscriminado As Decimal = TotalDiscriminado + TotalDescuentoDiscriminado

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalSinDescuentoDiscriminado"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalSinDescuentoDiscriminado.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDescuentoDiscriminado"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDescuentoDiscriminado.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDescuento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDescuento.ToString, False)


                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar el Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListadoFacturasEmitidas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Usuario As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & Usuario & " " & OrderBy, conn)
            'Dim WhereDetalle As String = Where.Replace("vventa.", "VDetalleVenta.")
            'Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select * from VDetalleVenta " & WhereDetalle & Usuario)
            Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select * from VDetalleVenta " & Where & Usuario)

            'Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select vdv.* from VDetalleVenta vdv join join VVenta v on vdv.idtransaccion = v.idtransaccion" & Where & Usuario)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("Comando_1")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("Comando_1"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidasConDescuento")

                Dim TotalDescuento As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDescuento")
                Dim TotalDiscriminado As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado")
                Dim TotalDescuentoDiscriminado As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDescuentoDiscriminado")
                Dim TotalSinDescuentoDiscriminado As Decimal = TotalDiscriminado + TotalDescuentoDiscriminado

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalSinDescuentoDiscriminado"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalSinDescuentoDiscriminado.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDescuentoDiscriminado"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDescuentoDiscriminado.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDescuento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDescuento.ToString, False)


                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar el Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturasEmitidasPorSucursal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Usuario As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & Usuario & " " & OrderBy, conn)
            'Dim WhereDetalle As String = Where.Replace("vventa.", "VDetalleVenta.")
            'Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select * from VDetalleVenta " & WhereDetalle & Usuario)
            Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select * from VDetalleVenta " & Where & Usuario)

            'Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("Select vdv.* from VDetalleVenta vdv join join VVenta v on vdv.idtransaccion = v.idtransaccion" & Where & Usuario)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("Comando_1")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("Comando_1"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidasPorSucursal")

                Dim TotalDescuento As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDescuento")
                Dim TotalDiscriminado As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDiscriminado")
                Dim TotalDescuentoDiscriminado As Decimal = CSistema.dtSumColumn(dtDetalle, "TotalDescuentoDiscriminado")
                Dim TotalSinDescuentoDiscriminado As Decimal = TotalDiscriminado + TotalDescuentoDiscriminado

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalSinDescuentoDiscriminado"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalSinDescuentoDiscriminado.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDescuentoDiscriminado"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDescuentoDiscriminado.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDescuento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDescuento.ToString, False)


                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar el Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturasEmitidasAGuaranies(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Usuario As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & Usuario & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("Comando_1")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("Comando_1"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidasConvertirAGuaranies")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar el Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub



        Sub ListadoFacturasEmitidasDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Usuario As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & Usuario & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle, conn)
            Dim aTable3 As New SqlDataAdapter("Select D.* From VVenta Join VDetalleImpuestoDesglosadoGravado D On VVenta.IDTransaccion=D.IDTransaccion " & Where, conn)
            Dim aTable4 As New SqlDataAdapter("Select D.* From VVenta Join VDetalleImpuestoDesglosado D On VVenta.IDTransaccion=D.IDTransaccion " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidasDetallada")

                Dim vTabla As String = ("VDetalleVenta")
                Dim temp As New DataTable
                temp = (DSDatos.Tables(vTabla))

                'Para pasar los totales sin sumar anulados
                Dim TotalGravado5 As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And Impuesto='IVA 5%'")
                Dim TotalGravado10 As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And Impuesto='IVA 10%'")
                Dim TotalExento As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And Impuesto='Exento'")
                Dim TotalGeneral As Decimal = TotalGravado5 + TotalGravado10 + TotalExento

                Dim TotalIVA5 As Decimal = CSistema.dtSumColumn(temp, "TotalImpuesto", "Anulado='False' And Impuesto='IVA 5%'")
                Dim TotalIVA10 As Decimal = CSistema.dtSumColumn(temp, "TotalImpuesto", "Anulado='False' And Impuesto='IVA 10%'")
                Dim TotalImpuesto As Decimal = TotalIVA5 + TotalIVA10

                Dim TotalEfectivo As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=1")
                Dim TotalChequeDia As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=6")
                Dim TotalChequeDiferido As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=2")
                Dim TotalDonacion As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=3")
                Dim TotalMuestra As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=5")
                Dim TotalBonificacion As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=4")

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalGravado5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalGravado5.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalGravado10"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalGravado10.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalExento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalExento.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalGeneral"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalGeneral.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalIVA5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalIVA5.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalIVA10"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalIVA10.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalImpuesto"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalImpuesto.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalEfectivo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalEfectivo.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalChequeDia"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalChequeDia.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalChequeDiferido"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalChequeDiferido.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDonacion"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDonacion.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalMuestra"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalMuestra.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalBonificacion"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalBonificacion.ToString, False)

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturasEmitidasDetalleNotaCredito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal WhereNC As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Usuario As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & Usuario & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle, conn)
            Dim aTable3 As New SqlDataAdapter("Select D.* From VVenta Join VDetalleImpuestoDesglosadoGravado D On VVenta.IDTransaccion=D.IDTransaccion " & Where, conn)
            Dim aTable4 As New SqlDataAdapter("Select D.* From VVenta Join VDetalleImpuestoDesglosado D On VVenta.IDTransaccion=D.IDTransaccion " & Where, conn)
            Dim aTable5 As New SqlDataAdapter("Select D.* From VVenta Join VDetalleNotaCreditoVenta D On VVenta.IDTransaccion=D.IDTransaccionVenta " & WhereNC, conn)
            Dim aTable6 As New SqlDataAdapter("select nc.* from VNotaCredito nc join NotaCreditoVenta ncv on nc.IDTransaccion = ncv.IDTransaccionNotaCredito join vventa on vventa.idtransaccion = ncv.IDtransaccionVentaGenerada" & WhereNC, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleNotaCreditoVenta")
                DSDatos.Tables.Add("VNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable5.Fill(DSDatos.Tables("VDetalleNotaCreditoVenta"))
                aTable6.Fill(DSDatos.Tables("VNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidasDetalladaNotaCreditoAsociada")

                Dim vTabla As String = ("VDetalleVenta")
                Dim temp As New DataTable
                temp = (DSDatos.Tables(vTabla))


                'Para pasar los totales sin sumar anulados

                Dim TotalGravado5 As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And Impuesto='IVA 5%'")
                Dim TotalGravado10 As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And Impuesto='IVA 10%'")
                Dim TotalExento As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And Impuesto='Exento'")
                Dim TotalGeneral As Decimal = TotalGravado5 + TotalGravado10 + TotalExento

                Dim TotalIVA5 As Decimal = CSistema.dtSumColumn(temp, "TotalImpuesto", "Anulado='False' And Impuesto='IVA 5%'")
                Dim TotalIVA10 As Decimal = CSistema.dtSumColumn(temp, "TotalImpuesto", "Anulado='False' And Impuesto='IVA 10%'")
                Dim TotalImpuesto As Decimal = TotalIVA5 + TotalIVA10

                Dim TotalEfectivo As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=1")
                Dim TotalChequeDia As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=6")
                Dim TotalChequeDiferido As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=2")
                Dim TotalDonacion As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=3")
                Dim TotalMuestra As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=5")
                Dim TotalBonificacion As Decimal = CSistema.dtSumColumn(temp, "Total", "Anulado='False' And IDFormaPagoFactura=4")

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalGravado5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalGravado5.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalGravado10"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalGravado10.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalExento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalExento.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalGeneral"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalGeneral.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalIVA5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalIVA5.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalIVA10"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalIVA10.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalImpuesto"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalImpuesto.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalEfectivo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalEfectivo.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalChequeDia"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalChequeDia.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalChequeDiferido"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalChequeDiferido.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDonacion"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDonacion.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalMuestra"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalMuestra.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalBonificacion"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalBonificacion.ToString, False)

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()
                aTable5.Dispose()
                aTable6.Dispose()
            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasProductoCliente(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True)

            'Aqui

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle & " Order By TipoProducto", conn)

            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()
                'aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasProductoCliente")

                'Aplicar Descuentos
                'If DescontarDevoluciones = True Then
                '    'DescontarDevolucionesVenta(DSDatos, Where)
                '    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                'End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                'aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        'Sub ListadoVentasTotalesProducto(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String, ByVal DescontarDevoluciones As Boolean, ByVal Reporte As String, Optional ByVal SepararSucursalCliente As Boolean = False, Optional ByVal WhereDetalleNCR As String = "", Optional vCadenaConexion As String = "")
        '=> SC:21-05-2022 nuevo
        Sub ListadoVentasTotalesProducto(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String, ByVal DescontarDevoluciones As Boolean, ByVal Reporte As String, Optional ByVal SepararSucursalCliente As Boolean = False, Optional ByVal WhereDetalleNCR As String = "", Optional ByVal WhereNCR As String = "", Optional vCadenaConexion As String = "")

            If vCadenaConexion <> "" Then
                VGCadenaConexion = vCadenaConexion
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleVenta"
            '=> SC:21-05-2022 nuevo, comentado desde aca
            If DescontarDevoluciones = True Then
                'vTabla = "VDetalleVentaConDevolucion"
                'vTabla = "VDetalleVentaConDevolucion2"
                vTabla = "VDetalleVentaConDevolucion3"
                If WhereDetalleNCR = "" Then
                    WhereDetalleNCR = " Where Tipo = 'Devolucion'"
                Else
                    WhereDetalleNCR = WhereDetalleNCR & " and Tipo = 'Devolucion'"
                End If

            End If

            Dim aTable1 As New SqlDataAdapter("Select " & Top & " " & DetalleVentaCampos & " From " & vTabla & " " & WhereDetalle & " " & OrderBy, conn)
            '=> SC:21-05-2022 nuevo, comentado desde aca, para usar solo vdetalleventa
            Dim aTable2 As New SqlDataAdapter("Select " & Top & "  ReferenciaCliente, DireccionAlternativa, NroNCR, FechaNCR, Comprobante, FechaEmision, 'TotalBrutoNCR'=Sum(TotalBrutoNCR), 'TotalDescuentoDiscriminadoNCR'=Sum(TotalDescuentoDiscriminadoNCR), 'TotalDiscriminadoNCR'=Sum(TotalDiscriminadoNCR), FormaPagoFactura, CancelarAutomatico From " & vTabla & " " & WhereDetalleNCR & " GROUP BY NroNCR, ReferenciaCliente, DireccionAlternativa, FechaNCR, Comprobante, FechaEmision,FormaPagoFactura, CancelarAutomatico Order By FechaNCR", conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 6000
                '=> SC:21-05-2022 nuevo, comentado desde aca, para usar solo vdetalleventa
                aTable2.SelectCommand.CommandTimeout = 6000

                Consultando = True
                DSDatos.Tables.Add("VDetalleVenta")
                ' DSDatos.Tables.Add("VDetalleVentaConDevolucion")
                ' DSDatos.Tables.Add("VDetalleVentaConDevolucion2")
                DSDatos.Tables.Add("VDetalleVentaConDevolucion3")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                '=> SC:21-05-2022 nuevo, comentado desde aca, para usar solo vdetalleventa
                If DescontarDevoluciones = True Then
                    'aTable2.Fill(DSDatos.Tables("VDetalleVentaConDevolucion"))
                    'aTable2.Fill(DSDatos.Tables("VDetalleVentaConDevolucion2"))
                    aTable2.Fill(DSDatos.Tables("VDetalleVentaConDevolucion3"))
                End If
                conn.Close()

                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Reporte)



                ' Cargamos subreporte en el Crystal
                If Reporte = "rptVentasTotalesProducto" Then
                    '=> SC:21-05-2022 nuevo, comentado desde aca, para usar solo vdetalleventa
                    'Report.Subreports(0).SetDataSource(DSDatos.Tables("VDetalleVentaConDevolucion"))
                    'Report.Subreports(0).SetDataSource(DSDatos.Tables("VDetalleVentaConDevolucion2"))
                    Report.Subreports(0).SetDataSource(DSDatos.Tables("VDetalleVentaConDevolucion3"))
                    Report.Subreports(0).SetDataSource(DSDatos.Tables("VDetalleVenta"))
                    Report.Subreports(0).SetDatabaseLogon(vgNombreUsuarioBD, vgPasswordBD)
                    Report.OpenSubreport(0)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Dim temp As New DataTable
                temp = (DSDatos.Tables(vTabla))


                'Calcular el total de NCR que no son del mes que afectan a facturas del mes indicado
                If Reporte = "rptVentasTotalesProducto" Then
                    '=> SC:21-05-2022 nuevo
                    'Dim TotalDevolucion As Decimal = CSistema.dtSumColumn(temp, "TotalDiscriminadoNCR")
                    Dim TotalDevolucion As Decimal = CSistema.ExecuteScalar("Select IsNull((Select sum(totaldiscriminado) From VDetalleNotaCredito" & WhereNCR & "), 0) ")
                    Dim TotalDiscriminado As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VDetalleVenta"), "TotalSinImpuesto")
                    'FA 08/09/2022 Se comenta para que el reporte no reste 2 veces el descuento
                    'Dim TotalDescuentoSinIVA As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VDetalleVenta"), "TotalDescuentoDiscriminado")

                    'Dim Resultado As Decimal = TotalDiscriminado + TotalDescuentoSinIVA - TotalDevolucion
                    '=> SC:21-05-2022 nuevo
                    Dim Resultado As Decimal
                    Dim TotalFletesSINIVA As Decimal = CSistema.ExecuteScalar("Select IsNull((Select sum(totaldiscriminado) From " & vTabla & " " & WhereDetalle & " and IdProducto in (206,220,2365)), 0) ")
                    If DescontarDevoluciones = True Then
                        'FA 08/09/2022 Se comenta para que el reporte no reste 2 veces el descuento
                        'Resultado = TotalDiscriminado + TotalDescuentoSinIVA - TotalFletesSINIVA
                        Resultado = TotalDiscriminado - TotalFletesSINIVA
                        TotalDevolucion = 0
                    Else
                        'FA 08/09/2022 Se comenta para que el reporte no reste 2 veces el descuento
                        'Resultado = TotalDiscriminado + TotalDescuentoSinIVA - TotalFletesSINIVA - TotalDevolucion
                        Resultado = TotalDiscriminado - TotalFletesSINIVA - TotalDevolucion
                    End If

                    Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                    txtField = Nothing

                    If DescontarDevoluciones = False Then
                        txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDescuento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                        txtField.Text = CSistema.FormatoNumero(TotalDevolucion.ToString, False)
                    End If
                    txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtVentas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                    txtField.Text = CSistema.FormatoNumero(Resultado.ToString, False)

                    'FA 08/09/2022 Se comenta para que el reporte no reste 2 veces el descuento
                    'txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtDescuento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                    'txtField.Text = CSistema.FormatoNumero(TotalDescuentoSinIVA.ToString, False)

                    txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtNeto"), CrystalDecisions.CrystalReports.Engine.TextObject)
                    txtField.Text = CSistema.FormatoNumero(TotalDiscriminado.ToString, False)
                    '=> SC:21-05-2022 nuevo
                    txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalFlete"), CrystalDecisions.CrystalReports.Engine.TextObject)
                    txtField.Text = CSistema.FormatoNumero(TotalFletesSINIVA.ToString, False)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoExcepciones(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal vFiltros As String, Optional vCadenaConexion As String = "")

            If vCadenaConexion <> "" Then
                VGCadenaConexion = vCadenaConexion
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VProductoListaPrecioExcepcionesInforme"

            Dim aTable1 As New SqlDataAdapter("Select * From " & vTabla & " " & WhereDetalle, conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 200

                Consultando = True
                DSDatos.Tables.Add("VProductoListaPrecioExcepcionesInforme")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProductoListaPrecioExcepcionesInforme"))
                conn.Close()

                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExcepciones")

                Dim temp As New DataTable
                temp = (DSDatos.Tables(vTabla))

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDetalladoDescuento(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal vFiltros As String, Optional vCadenaConexion As String = "")

            If vCadenaConexion <> "" Then
                VGCadenaConexion = vCadenaConexion
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDescuento"

            Dim aTable1 As New SqlDataAdapter("Select * From " & vTabla & " " & WhereDetalle, conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 200

                Consultando = True
                DSDatos.Tables.Add("VDescuento")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDescuento"))
                conn.Close()

                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDetalleDescuento")

                Dim temp As New DataTable
                temp = (DSDatos.Tables(vTabla))

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasParaExcel(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Reporte As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleVentaConDevolucionRemmele"

            Dim aTable1 As New SqlDataAdapter("Select * From " & vTabla & " " & WhereDetalle, conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 200

                Consultando = True
                DSDatos.Tables.Add("VDetalleVentaConDevolucionRemmele")
                conn.Open()

                aTable1.Fill(DSDatos.Tables("VDetalleVentaConDevolucionRemmele"))

                conn.Close()

                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Reporte)

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                'If SepararSucursalCliente = True Then
                '    SepararSucursalClienteDetalle(DSDatos)
                'End If

                Dim temp As New DataTable
                temp = (DSDatos.Tables(vTabla))


                Report.SetDataSource(DSDatos)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub PlanillaVentaDiaria(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String, ByVal DescontarDevoluciones As Boolean, ByVal Reporte As String, Optional ByVal SepararSucursalCliente As Boolean = False, Optional ByVal WhereDetalleNCR As String = "", Optional vCadenaConexion As String = "")
            If vCadenaConexion <> "" Then
                VGCadenaConexion = vCadenaConexion
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleVenta"

            Dim aTable1 As New SqlDataAdapter("Select * From " & vTabla & " " & WhereDetalle & " " & OrderBy, conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 6000

                Consultando = True
                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Reporte)

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesTipoProducto(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()
                'aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesTipoProducto")

                'Aplicar Descuentos
                'If DescontarDevoluciones = True Then
                '    DescontarDevolucionesVenta(DSDatos, Where)
                '    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                'End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                'aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Reporte As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                Debug.Print(CSistema.dtSumColumn(DSDatos.Tables("VDetalleVenta"), "TotalDiscriminado"))


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Reporte)

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesCliente")

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesTipoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesTipoCliente")

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaTotalesProductoCliente")

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDetalleVentasProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDetalleVentaProductoCliente")

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre) From VVentaTotalAnualCliente " & Where & " group by Cliente " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasAnualesTotalesCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalTipoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoCliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualTipoCliente " & Where & " group by TipoCliente" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualTipoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualTipoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasAnualesTotalesTipoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalClienteProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,Producto,Referencia,ReferenciaProducto,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualProductoCliente " & Where & " group by Cliente,Producto,Referencia,ReferenciaProducto" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualProductoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualProductoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoTotalAnualClienteProducto")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String, ByVal Del As String, ByVal Al As String, ByVal Moneda As String, ByVal EsCantidad As Boolean, ByVal DescontarDevoluciones As Boolean, ByVal SepararSucursalCliente As Boolean, ByVal SeleccionInforme As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleVenta"

            If DescontarDevoluciones = True Then
                vTabla = "VDetalleVentaConDevolucion"
            End If

            Dim aTable1 As New SqlDataAdapter("Select " & Top & " IDTransaccion, IDProducto, Producto, Cliente, DireccionAlternativa, ReferenciaCliente, MesNumero, Cantidad, Total, Bruto, TotalImpuesto, TotalDiscriminado, TotalDescuento, Descuento, TotalPrecioNeto, TotalCosto, TotalCostoImpuesto, TotalCostoDiscriminado, CantidadCaja, Cajas, TotalBruto, TotalSinDescuento, TotalSinImpuesto, TotalNeto, TotalNetoConDescuentoNeto, TipoCliente, IDListaPrecio From " & vTabla & " " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, SeleccionInforme)

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim Cantidad As ParameterField = New ParameterField()
                Dim CantidadValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Cantidad.ParameterFieldName = "EsCantidad"
                CantidadValue.Value = EsCantidad

                Cantidad.CurrentValues.Add(CantidadValue)

                Parametros.Add(Cantidad)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub



        Sub ListadoVentasAnualTotalProductoClienteCantidad(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String, ByVal Del As String, ByVal Al As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,Producto,Referencia,ReferenciaProducto,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualProductoClienteCantidad " & Where & " group by Cliente,Producto,Referencia,ReferenciaProducto" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualProductoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualProductoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoTotalAnualProductoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualCantidadCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre) From VVentaCantidadAnualCliente " & Where & " group by Cliente " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            Try

                DSDatos.Tables.Add("VVentaCantidadAnualCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaCantidadAnualCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaAnualCantidadCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoVentaMesTotal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Año As Integer)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select Mes,'Total'=Sum(Total) From VMesTotal Where Year(FechaEmision) = " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VMesTotal")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMesTotal"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoVentaMesTotal")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Año
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAño"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Año

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoRankingProductoRentable(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VProductoUtilidad Where " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VProductoUtilidad")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProductoUtilidad"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoProductoRentable")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Del
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                'Al
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoRankingClienteVentas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Total' =(sum(Total)) From VVenta Where " & Where & " And Anulado = 'False' Group By Cliente Order By Total Desc", conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoRankingClientesVenta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoRankingProductoMasVendido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Producto,'Cantidad'=Sum(Cantidad) From VProductoMasVendido  Where " & Where & " Group By Producto Order By Cantidad Desc", conn)

            Try

                DSDatos.Tables.Add("VProductoMasVendido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProductoMasVendido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoProductoMasVendido")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Del
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                'Al
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirFactura(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String)

Volver:

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VVenta " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If



                Dim vDecimalOperacion As Integer = DSDatos.Tables("VVenta").Rows(0)("Decimales")
                If vDecimalOperacion = 0 Then
                    Total = CSistema.FormatoMoneda(DSDatos.Tables("VVenta").Rows(0)("Total"), False)
                    NumeroALetra = CSistema.NumeroALetra(Total)
                Else
                    Total = CSistema.FormatoMoneda(DSDatos.Tables("VVenta").Rows(0)("Total"), True)
                    NumeroALetra = CSistema.NumeroALetra2(Replace(Total, ".", ""))
                End If

                'Numero a letras especificando las monedas definidas para imprimir
                If vDecimalOperacion <> 0 Then
                    Dim UltimasLetras As String = ""
                    Dim PrimerasLetras As String
                    Dim MonedaImpresion As String
                    Dim vCentavos As String = " centavos"
                    MonedaImpresion = DSDatos.Tables("VVenta").Rows(0)("Impresion")
                    'UltimasLetras = NumeroALetra.Substring(NumeroALetra.Length - 7)
                    'PrimerasLetras = NumeroALetra.Substring(0, NumeroALetra.Length - 7)

                    Dim ArrCadena As String() = NumeroALetra.Split("con")
                    PrimerasLetras = ArrCadena(0)
                    If InStr(NumeroALetra, " con ") <> 0 Then
                        UltimasLetras = NumeroALetra.Substring(NumeroALetra.Length - 7)
                    Else
                        vCentavos = ""
                    End If

                    'NumeroALetra = PrimerasLetras + " " + MonedaImpresion + IIf(String.IsNullOrEmpty(MonedaImpresion), "", UltimasLetras + vCentavos)
                    NumeroALetra = MonedaImpresion + " " + PrimerasLetras + " " + UltimasLetras + vCentavos
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                EstablecerConexion(Report)
                Report.SetDataSource(DSDatos)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total

                ' Asignamos el tamaño del papel
                GetPapersizeID(Report, Impresora, "Facturacion")
                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                ''Configuracion del Reporte
                'frmReporte.CrystalReportViewer1.ReportSource = Report
                'frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                'frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                'frmReporte.CrystalReportViewer1.AutoSize = True
                '
                'frmReporte.StartPosition = FormStartPosition.CenterScreen

                'frmReporte.Show()

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception

                MsgBox("Mensaje : " & ex.Message)

                If MessageBox.Show("Desea volver a intentarlo?", "Imprimir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    GoTo Volver
                End If

            End Try

        End Sub

        Sub ListadoVentasAnuladas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " *  From VVenta " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaAnulada")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DMSMinuta(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDMSMinuta " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDMSMinuta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDMSMinuta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDMSMinuta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesClienteProveedor(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String, ByVal DescontarDevoluciones As Boolean)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleVenta"

            If DescontarDevoluciones = True Then
                vTabla = "VDetalleVentaConDevolucion"
            End If

            Dim sql As New SqlDataAdapter("Select " & Top & " ReferenciaCliente, Cliente, DireccionAlternativa, ReferenciaProveedor, Proveedor, COUNT(Producto) as Cantidad FROM(Select ReferenciaCliente, Cliente, DireccionAlternativa, ReferenciaProveedor, Proveedor, Producto From " & vTabla & " " & WhereDetalle & " GROUP By ReferenciaCliente, Cliente, DireccionAlternativa, ReferenciaProveedor, Proveedor, Producto) as T GROUP By ReferenciaCliente, Cliente, DireccionAlternativa, ReferenciaProveedor, Proveedor " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()

                sql.Fill(DSDatos.Tables("VDetalleVenta"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesClienteProveedor")

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)

                SepararSucursalClienteDetalle(DSDatos)

                Report.SetDataSource(DSDatos)

                Dim temp As New DataTable
                temp = (DSDatos.Tables("VDetalleVenta"))

                'Sumar el total SKU, CANTIDAD DE PRODUCTOS/CLIENTE
                Dim Suma As Decimal = CSistema.dtSumColumn(temp, "Cantidad")

                'Contar diferentes clientes
                Dim SumaClientes As Decimal = CSistema.dtCountDistinct(temp, "Cliente")

                Dim Promedio As Decimal = Suma / SumaClientes

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                'sku promedio
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalSKU"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(Suma.ToString, False)

                'Promedio SKU
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtPromedio"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(Promedio.ToString, True)

                Report.SetDataSource(DSDatos)

                'Informacion de Responsable
                EstablecerTitulos(Report, "", vgUsuario)

                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCompras
        Inherits CReporte

        Sub ListadoFacturasComprasEmitidasDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleCompra C " & Where & " And " & WhereDetalle & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select " & Top & " * From VCompra C " & Where & " " & OrderBy, conn)
            Dim aTable3 As New SqlDataAdapter("Select " & Top & " * From VCompra C Join VDetalleImpuestoDesglosado D On C.IDTransaccion=D.IDtransaccion " & Where & " " & OrderBy, conn)
            Dim aTable4 As New SqlDataAdapter("Select " & Top & " * From VCompra C Join VDetalleImpuestoDesglosadoGravado D On C.IDTransaccion=D.IDtransaccion " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleCompra")
                DSDatos.Tables.Add("VCompra")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleCompra"))
                aTable2.Fill(DSDatos.Tables("VCompra"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturaCompraDetallada")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ListadoFacturasComprasEmitidas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleCompra C " & Where & " And " & WhereDetalle & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleCompra")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleCompra"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturaCompraResumida")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturasComprasporProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * from  VDetalleCompra C " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleCompra")


                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleCompra"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumenComprasProductoporProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class
    Public Class CReporteNotaCredito
        Inherits CReporte
        Sub ListadoPedidoNotaCredito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedidoNotaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPedidoNotaCredito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedidoNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListaPedidoNotaCredito")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPedidoNotaCreditoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal Titulo As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedidoNotaCredito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePedidoNotaCredito " & WhereDetalle & " and IDTransaccion in (Select IDTransaccion From VPedidoNotaCredito " & Where & ")" & OrderBy, conn)
            aTable1.SelectCommand.CommandTimeout = 600
            aTable2.SelectCommand.CommandTimeout = 600
            Try

                DSDatos.Tables.Add("VPedidoNotaCredito")
                DSDatos.Tables.Add("VDetallePedidoNotaCredito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedidoNotaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetallePedidoNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPedidoNotaCreditoDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaCredito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VNotaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaCredito")

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaCreditoConPedido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VNotaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaCreditoConPedido")

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDevolucionSinNC(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleDevolucionSinNotaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDevolucionSinNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDevolucionSinNotaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDevolucionSinNC")

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoNotaCreditoVentaAplicada(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal AgrupadoCliente As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCreditoVentaAplicacionInforme " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCreditoVentaAplicacionInforme")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCreditoVentaAplicacionInforme"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                If AgrupadoCliente Then
                    LoadReport(Report, "rptListadoNotaCreditoVenta")
                Else
                    LoadReport(Report, "rptListadoNotaCreditoVentaSinAgrupar")
                End If



                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ImprimirNotaCreditoDetallada(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal OrderBy As String, ByVal Top As String)
            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotacredito " & Where & " " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add(" VDetalleNotaCredito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables(" VDetalleNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptNotaDeCreditoDetallada")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                'Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ImprimirNotaCreditoAplicar(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoNotaCreditoAplicar")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ImprimirNotaCreditoAplicarDetalleFactura(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleNotaCreditoVentaAsociada " & Where & " " & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleNotaCreditoVentaAsociada")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleNotaCreditoVentaAsociada"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoNotaCreditoAplicarDetalleVenta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaCreditoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotacredito ", conn)

            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add(" VDetalleNotaCredito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables(" VDetalleNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpNotaCreditoDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                'Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImpresionNotaCredito(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String, ByVal Comprobantes As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VNotaCredito " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaCredito " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)
            Dim aTable5 As New SqlDataAdapter("Select * From VDetalleImpuesto " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add("VDetalleNotaCredito")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuesto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaCredito"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable5.Fill(DSDatos.Tables("VDetalleImpuesto"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtComprobantes"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Comprobantes

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListadoNotaCreditoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaCredito " & Where & " " & WhereDetalle, conn)
            Dim aTable3 As New SqlDataAdapter("Select D.* From VNotaCredito V Join VDetalleImpuestoDesglosadoGravado D On V.IDTransaccion=D.IDTransaccion " & Where, conn)
            Dim aTable4 As New SqlDataAdapter("Select D.* From VNotaCredito V Join VDetalleImpuestoDesglosado D On V.IDTransaccion=D.IDTransaccion " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add("VDetalleNotaCredito")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaCredito"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpNotaCreditoDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        'NotaCreditoDetallada dbordon
        Sub NotaCreditoDetallada(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Reporte As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaCredito " & WhereDetalle, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add("VDetalleNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Reporte)
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        'NotaCreditoDetallada dbordon

        Sub NotaCreditoDetalladaFechaVenta(ByVal frmReporte As frmReporte, ByVal Join As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Reporte As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where.Replace("VDetalleNotaCredito.", "") & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaCredito " & Join & " " & WhereDetalle, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add("VDetalleNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Reporte)
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        'dbordon

        Sub ListadodeAcuerdodeClientes(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VAcuerdodeClientes " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VAcuerdodeClientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VAcuerdodeClientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAcuerdodeClientes")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteNotaDebito
        Inherits CReporte

        Sub ImprimirNotaDebito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebito")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaDebitoAplicar(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VNotaDebito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebitoAplicar")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaDebitoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            ' Dim aTable1 As New SqlDataAdapter("Select  * From VNotaCredito ", conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaDebito ", conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos


            Try

                DSDatos.Tables.Add("VNotaDebito")
                DSDatos.Tables.Add(" VDetalleNotaDebito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))
                aTable2.Fill(DSDatos.Tables(" VDetalleNotaDebito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoNotaDebitoDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                'Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImpresionNotaDebito(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VNotaDebito " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaDebito " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)
            Dim aTable5 As New SqlDataAdapter("Select * From VDetalleImpuesto " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebito")
                DSDatos.Tables.Add("VDetalleNotaDebito")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuesto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaDebito"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable5.Fill(DSDatos.Tables("VDetalleImpuesto"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

    End Class
    Public Class CReporteTicketBascula
        Inherits CReporte
        Sub TicketBascula(ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VTicketBascula Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VTicketBasculaGastoAdicional Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VTicketBascula")
                DSDatos.Tables.Add("VTicketBasculaGastoAdicional")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VTicketBascula"))
                aTable2.Fill(DSDatos.Tables("VTicketBasculaGastoAdicional"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptTicketBascula")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoTicketBascula(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String, Optional SinCosto As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VTicketBascula " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VTicketBasculaGastoAdicional ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VTicketBascula")
                DSDatos.Tables.Add("VTicketBasculaGastoAdicional")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VTicketBascula"))
                aTable2.Fill(DSDatos.Tables("VTicketBasculaGastoAdicional"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If SinCosto = False Then
                    LoadReport(Report, "rptListadoTicketBascula")
                Else
                    LoadReport(Report, "rptListadoTicketBasculaSinCosto")
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "",, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExtractoPagoFacturaAcuerdo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Subtitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * from VPagoGastoAcuerdo " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPagoGastoAcuerdo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPagoGastoAcuerdo"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPagoGastoAcuerdo")

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "",, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ListadoTicketBasculaSinMachearAFecha(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Al As String, ByVal Subtitulo As String, Optional SinCosto As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("exec SpViewTicketBasculaPendientesPorFecha @Hasta= " & Where, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VTicketBasculaGastoAdicional ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VTicketBascula")
                DSDatos.Tables.Add("VTicketBasculaGastoAdicional")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VTicketBascula"))
                aTable2.Fill(DSDatos.Tables("VTicketBasculaGastoAdicional"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoTicketBasculaSinMachearAFecha")

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InformeMacheoFacturaTicketBascula(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Subtitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VMacheoFacturaTicketDetalle " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMacheoFacturaTicketDetalle")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMacheoFacturaTicketDetalle"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                LoadReport(Report, "rptListadoMacheoFacturaTicketBascula")

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class
    Public Class CReporteAccesoPerfil
        Inherits CReporte
        Sub AccesoPerfil(ByVal where As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From vAccesoPerfil " & where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vAccesoPerfil")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vAccesoPerfil"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInformeAccesoPerfil")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "Listado de Acceso por Perfil", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteReciboVenta
        Inherits CReporte
        Sub ReciboVenta(ByVal IDTransaccion As String, ByVal Impresora As String, ByVal Original As Boolean)
            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From vReciboVenta where Anulado= 0 and IDTransaccionVenta =" & IDTransaccion, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vReciboVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vReciboVenta"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptReciboVenta")
                Report.SetDataSource(DSDatos)

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                'Existencia Inicial
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMarcaDeAgua"), CrystalDecisions.CrystalReports.Engine.TextObject)

                Dim txtField2 As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField2 = Nothing
                'Existencia Inicial
                txtField2 = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMarcaDeAgua2"), CrystalDecisions.CrystalReports.Engine.TextObject)

                Dim txtFechaHora As CrystalDecisions.CrystalReports.Engine.TextObject
                txtFechaHora = Nothing
                'Existencia Inicial
                txtFechaHora = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFechaHora"), CrystalDecisions.CrystalReports.Engine.TextObject)
                Dim txtFechaHora2 As CrystalDecisions.CrystalReports.Engine.TextObject
                txtFechaHora2 = Nothing
                'Existencia Inicial
                txtFechaHora2 = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFechaHora2"), CrystalDecisions.CrystalReports.Engine.TextObject)


                If Original = False Then
                    txtField.Text = "COPIA"
                    txtField2.Text = "COPIA"
                    txtFechaHora.Text = "Fecha y Hora de Impresion: " & CDate(Today).ToShortDateString & " - " & CDate(TimeOfDay).ToShortTimeString
                    txtFechaHora2.Text = "Fecha y Hora de Impresion: " & CDate(Today).ToShortDateString & " - " & CDate(TimeOfDay).ToShortTimeString
                Else
                    txtField.Text = ""
                    txtField2.Text = ""
                    txtFechaHora.Text = ""
                    txtFechaHora2.Text = ""
                End If


                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)
                ''Mostrar Informe
                'MostrarReportePrincipal(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ReciboVentaBoletaContraBoleta(ByVal IDTransaccion As String, ByVal IDcliente As Integer, ByVal Impresora As String, ByVal Original As Boolean)
            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From vReciboVenta where Anulado = 0 and  IDTransaccionVenta =" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From vInventarioBoletaContraBoleta where IDCliente =" & IDcliente & " Order by FechaVencimiento asc", conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vReciboVenta")
                DSDatos.Tables.Add("vInventarioBoletaContraBoleta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vReciboVenta"))
                aTable2.Fill(DSDatos.Tables("vInventarioBoletaContraBoleta"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptReciboVentaBoletaContraBoleta")
                Report.SetDataSource(DSDatos)

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                'Existencia Inicial
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMarcaDeAgua"), CrystalDecisions.CrystalReports.Engine.TextObject)

                Dim txtField2 As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField2 = Nothing
                'Existencia Inicial
                txtField2 = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMarcaDeAgua2"), CrystalDecisions.CrystalReports.Engine.TextObject)

                Dim txtFechaHora As CrystalDecisions.CrystalReports.Engine.TextObject
                txtFechaHora = Nothing
                'Existencia Inicial
                txtFechaHora = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFechaHora"), CrystalDecisions.CrystalReports.Engine.TextObject)
                Dim txtFechaHora2 As CrystalDecisions.CrystalReports.Engine.TextObject
                txtFechaHora2 = Nothing
                'Existencia Inicial
                txtFechaHora2 = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFechaHora2"), CrystalDecisions.CrystalReports.Engine.TextObject)


                If Original = False Then
                    txtField.Text = "COPIA"
                    txtField2.Text = "COPIA"
                    txtFechaHora.Text = "Fecha y Hora de Impresion: " & CDate(Today).ToShortDateString & " - " & CDate(TimeOfDay).ToShortTimeString
                    txtFechaHora2.Text = "Fecha y Hora de Impresion: " & CDate(Today).ToShortDateString & " - " & CDate(TimeOfDay).ToShortTimeString
                Else
                    txtField.Text = ""
                    txtField2.Text = ""
                    txtFechaHora.Text = ""
                    txtFechaHora2.Text = ""
                End If


                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)


                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)
                ''Mostrar Informe
                'MostrarReportePrincipal(Report, "")


                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub Cobranza(ByVal IDTransaccion As String, ByVal Impresora As String, ByVal Original As Boolean)
            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From vCobranzaCredito where IDTransaccion =" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleDocumentosCobrados where IDTransaccion =" & IDTransaccion, conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VReciboFormaPago where IDTransaccion =" & IDTransaccion, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vCobranzaCredito")
                DSDatos.Tables.Add("VDetalleDocumentosCobrados")
                DSDatos.Tables.Add("VReciboFormaPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vCobranzaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetalleDocumentosCobrados"))
                aTable3.Fill(DSDatos.Tables("VReciboFormaPago"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCobranzaRecibo")
                Report.SetDataSource(DSDatos)

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                'Existencia Inicial
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMarcaDeAgua"), CrystalDecisions.CrystalReports.Engine.TextObject)

                Dim txtFechaHora As CrystalDecisions.CrystalReports.Engine.TextObject
                txtFechaHora = Nothing
                'Existencia Inicial
                txtFechaHora = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFechaHora"), CrystalDecisions.CrystalReports.Engine.TextObject)


                If Original = False Then
                    txtField.Text = "COPIA"
                    txtFechaHora.Text = "Fecha y Hora de Impresion: " & CDate(Today).ToShortDateString & " - " & CDate(TimeOfDay).ToShortTimeString
                Else
                    txtField.Text = ""
                    txtFechaHora.Text = ""
                End If


                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(2, False, 0, 0)





                ''''Informacion de Software
                '''EstablecerTitulos(Report, "", "")

                ''''Establecer conexion
                '''EstablecerConexion(Report)

                ''''Mostrar Informe
                '''MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoRecibosGenerados(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VReciboVenta " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VReciboVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VReciboVenta"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoRecibosGenerados")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CReporteStock
        Inherits CReporte

        Sub ListadoKardexProductoResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            'Advertencia
            If MessageBox.Show("Este proceso puede durar varios minutos! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.No Then
                Exit Sub
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim strSQL As String = " Exec SpViewKardexProductoResumen " & Where

            Dim aTable1 As New SqlDataAdapter(strSQL, conn)
            aTable1.SelectCommand.CommandTimeout = 500 'IMPORTANTE
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VKardexInforme")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VKardexInforme"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptKardexProducto")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoKardexProductoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            'Advertencia
            If MessageBox.Show("Este proceso puede durar varios minutos! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.No Then
                Exit Sub
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim strSQL As String = "Select * from vKardexInforme  " & Where & " order by indice"

            Dim aTable1 As New SqlDataAdapter(strSQL, conn)
            aTable1.SelectCommand.CommandTimeout = 500 'IMPORTANTE
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VKardexInforme")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VKardexInforme"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptKardexProductoDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ExistenciaValorizadaDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, EsInformeActual As Boolean, Optional ByVal FechaHasta As Date = Nothing)

            'Advertencia
            If MessageBox.Show("Este proceso puede durar varios minutos! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.No Then
                Exit Sub
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim strSQL As String
            If EsInformeActual Then
                Where = Where '& " And E.Existencia > 0 "
                strSQL = "Select " & Top & " * From VExistenciaDeposito E" & Where & " " & OrderBy
            Else
                'Where = Where '& " And E.Existencia > 0 And E.CostoPromedio > 0"
                'strSQL = "Select " & Top &
                '" E.IDSucursal,E.IDDeposito,E.Deposito,E.IDLinea,E.Linea" &
                '",E.IDTipoProducto,E.TipoProducto,E.IDMarca,E.Marca,E.IDProducto,E.Referencia" &
                '",E.Producto,dbo.FCostoProductoFechaKardex(E.IDProducto,'" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "') CostoPromedio" &
                '", dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "', '', 'False') AS Existencia, " &
                '"dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "', '', 'False') * dbo.FCostoProductoFechaKardex(E.IDProducto,'" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "') TotalValorizadoCostoPromedio, " &
                '" ISNULL(AVG(E.PesoUnitario),1) PesoUnitario " &
                '" From VExistenciaDeposito E " &
                '" /*JOIN VExistenciaMovimientoCalculado VEMC on E.IDProducto=VEMC.IDProducto and E.IDDeposito=VEMC.IDDeposito " &
                '" AND VEMC.Fecha >= '19000101' AND VEMC.Fecha <= '" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "' */" &
                'Where &
                '" GROUP BY E.IDSucursal,E.IDDeposito,E.Deposito,E.IDLinea,E.Linea,E.IDTipoProducto,E.TipoProducto," &
                '" E.IDMarca,E.Marca,E.IDProducto,E.Referencia,E.Producto " & OrderBy
                strSQL = "exec SpViewExistenciaValorizada " & Where
            End If
            Dim aTable1 As New SqlDataAdapter(strSQL, conn)
            aTable1.SelectCommand.CommandTimeout = 5000 'IMPORTANTE
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaValorizadaDetallado")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ExistenciaValorizadaComparativoKardex(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, EsInformeActual As Boolean, Optional ByVal FechaHasta As Date = Nothing)

            'Advertencia
            If MessageBox.Show("Este proceso puede durar varios minutos! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.No Then
                Exit Sub
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim strSQL As String
            If EsInformeActual Then
                Where = Where '& " And E.Existencia > 0 "
                strSQL = "Select " & Top & " * From VExistenciaDepositoComparativoKardex E" & Where & " " & OrderBy
            Else

                strSQL = "exec SpViewExistenciaValorizadaComparativoKardex " & Where

            End If
            Dim aTable1 As New SqlDataAdapter(strSQL, conn)
            aTable1.SelectCommand.CommandTimeout = 5000 'IMPORTANTE
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExistenciaDepositoComparativoKardex")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDepositoComparativoKardex"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaValorizadaDetalladoComparativoKardex")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaValorizadaDetalleCuentaContable(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, EsInformeActual As Boolean, Optional ByVal FechaHasta As Date = Nothing)

            'Advertencia
            If MessageBox.Show("Este proceso puede durar varios minutos! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.No Then
                Exit Sub
            End If

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim strSQL As String
            If EsInformeActual Then
                Where = Where '& " And E.Existencia > 0 "
                strSQL = "Select " & Top & " * From VExistenciaDepositoCuentaContable E" & Where & " " & OrderBy
            Else

                strSQL = "exec SpViewExistenciaValorizadaCuentaContable2 " & Where

            End If
            Dim aTable1 As New SqlDataAdapter(strSQL, conn)
            aTable1.SelectCommand.CommandTimeout = 50000 'IMPORTANTE
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExistenciaDepositoCuentaContable")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDepositoCuentaContable"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaValorizadaDetalladoDeposito")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        ''original valorizado
        'Sub ExistenciaValorizadaResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal EsInformeActual As Boolean, Optional ByVal FechaHasta As Date = Nothing)

        '    'Advertencia
        '    If MessageBox.Show("Este proceso puede durar varios minutos! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.No Then
        '        Exit Sub
        '    End If

        '    ' Creamos los componentes para obtener los datos.

        '    Dim conn As New SqlConnection(VGCadenaConexion)
        '    Dim DSDatos As New DataSet
        '    'Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoProducto,'Existencia'=SUM(Existencia),'Costo'=SUM(Costo),'CostoPromedio'=sum(CostoPromedio) From VExistenciaDeposito " & Where & " group by TipoProducto " & OrderBy, conn)
        '    Where = Where '& " And E.Existencia > 0 "
        '    Dim str As String
        '    If EsInformeActual Then
        '        str = "SELECT " & Top & " TipoProducto, SUM(Existencia) Existencia, " &
        '            "SUM(TotalValorizadoCostoPromedio) TotalValorizadoCostoPromedio " &
        '            "FROM( Select E.TipoProducto, SUM(E.Existencia) as Existencia, " &
        '            "SUM(E.Existencia) * dbo.FCostoProducto(E.IDProducto) as TotalValorizadoCostoPromedio From VExistenciaDeposito E " & Where & " GROUP BY E.TipoProducto, E.IDProducto " &
        '            ") as T GROUP BY TipoProducto " & OrderBy
        '    Else
        '        str = "exec SpViewExistenciaValorizada " & Where & " ,@Resumido = 1"

        '        '               "Select " &
        '        '          " E.TipoProducto," &
        '        '      "dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" &
        '        'CSistema.FormatoFechaBaseDatos(FechaHasta.AddDays(1), True, False) & "', '', 'False') AS Existencia, " &
        '        '"dbo.FCostoProducto(E.IDProducto) * dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" &
        '        'CSistema.FormatoFechaBaseDatos(FechaHasta.AddDays(1), True, False) & "', '', 'False') AS TotalValorizadoCostoPromedio " &
        '        '          " From VExistenciaDeposito E " &
        '        '          " JOIN VExistenciaMovimientoCalculado VEMC on E.IDProducto=VEMC.IDProducto and E.IDDeposito=VEMC.IDDeposito " &
        '        '          " AND VEMC.Fecha >= '19000101' AND VEMC.Fecha <= '" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "' " &
        '        '          Where &
        '        '          " GROUP BY E.IDTipoProducto,E.TipoProducto,E.IDProducto, E.IDDeposito " &
        '        '           " ) as T GROUP BY TipoProducto " & OrderBy

        '    End If
        '    Dim aTable1 As New SqlDataAdapter(str, conn)
        '    aTable1.SelectCommand.CommandTimeout = 500 'IMPORTANTE
        '    Try

        '        DSDatos.Tables.Add("VExistenciaDeposito")

        '        conn.Open()
        '        aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
        '        conn.Close()

        '        ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
        '        Dim Report As New ReportClass
        '        LoadReport(Report, "rptExistenciaValorizadaResumen")
        '        Report.SetDataSource(DSDatos)

        '        'Informacion de Software
        '        EstablecerTitulos(Report, Titulo, TipoInforme)

        '        'Establecer conexion
        '        EstablecerConexion(Report)

        '        'Mostrar Informe
        '        MostrarReporte(Report, Titulo, Nothing, DSDatos)

        '        aTable1.Dispose()

        '    Catch ex As Exception
        '        MsgBox("Mensaje : " & ex.Message)
        '    End Try

        'End Sub


        ''prueba await 



        Public Async Function ExistenciaValorizadaDetalleAsync(frmReporte As frmReporte, Where As String, Titulo As String, TipoInforme As String, UsuarioIdentificador As String, OrderBy As String, Top As String, EsInformeActual As Boolean, FechaHasta As Date) As Task
            ' Advertencia al usuario
            If MessageBox.Show("Este proceso puede durar varios minutos! ¿Desea continuar?", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.No Then
                Exit Function
            End If

            ' Crear dataset
            Dim DSDatos As New DataSet()
            Dim str As String

            ' Construcción de la consulta
            If EsInformeActual Then
                str = "SELECT " & Top & " TipoProducto, SUM(Existencia) Existencia, " &
              "SUM(TotalValorizadoCostoPromedio) TotalValorizadoCostoPromedio " &
              "FROM( Select E.TipoProducto, SUM(E.Existencia) as Existencia, " &
              "SUM(E.Existencia) * dbo.FCostoProducto(E.IDProducto) as TotalValorizadoCostoPromedio " &
              "From VExistenciaDeposito E " & Where & " GROUP BY E.TipoProducto, E.IDProducto " &
              ") as T GROUP BY TipoProducto " & OrderBy
            Else
                str = "exec SpViewExistenciaValorizada " & Where & " ,@Resumido = 1"
            End If

            ' Manejo seguro de la conexión con Using
            Using conn As New SqlConnection(VGCadenaConexion)
                Using aTable1 As New SqlDataAdapter(str, conn)
                    aTable1.SelectCommand.CommandTimeout = 500

                    Try
                        ' Agregar tabla al DataSet
                        DSDatos.Tables.Add("VExistenciaDeposito")

                        ' Abrir conexión y ejecutar consulta de manera asíncrona
                        Await conn.OpenAsync()
                        Await Task.Run(Sub() aTable1.Fill(DSDatos.Tables("VExistenciaDeposito")))
                    Catch ex As Exception
                        MessageBox.Show("Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Function
                    End Try
                End Using
            End Using

            ' Cargar los datos en el Crystal Report
            Dim Report As New ReportClass()
            LoadReport(Report, "rptExistenciaValorizadaDetalle")
            Report.SetDataSource(DSDatos)

            ' Información del software
            EstablecerTitulos(Report, Titulo, TipoInforme)
            EstablecerConexion(Report)

            ' Mostrar Informe
            MostrarReporte(Report, Titulo, Nothing, DSDatos)
        End Function





        ''


        Sub MovimientoProductoResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Producto As String, ByVal Subtitulo As String, Optional ByVal IDProducto As String = "0")

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim strSQL As String = "Select " & Top & "  Producto, Movimiento, Referencia,'Entrada'=sum(Entrada),'Salida'=sum(Salida) From VExtractoMovimientoProducto " & Where & " group by Referencia,Producto,Movimiento " & OrderBy
            Dim strSQL As String = String.Format("SELECT (SELECT TOP 1 Referencia FROM Producto WHERE ID={0}) Referencia, (SELECT TOP 1 Descripcion FROM Producto WHERE ID={1}) Producto, A.Movimiento, ISNULL(B.Entrada, 0) Entrada, ISNULL(B.Salida, 0) Salida FROM( SELECT Movimiento, 0 AS Entrada, 0 AS Salida FROM dbo.VExtractoMovimientoProducto GROUP BY Movimiento) AS A LEFT OUTER JOIN (Select   Producto, Referencia, Movimiento, 'Entrada'=sum(Entrada),'Salida'=sum(Salida) From VExtractoMovimientoProducto {2} group by Referencia,Producto,Movimiento) AS B ON A.Movimiento = B.Movimiento",
                                    IDProducto, IDProducto, Where)
            Dim aTable2 As New SqlDataAdapter(strSQL, conn)

            strSQL = "Select " & Top & " T.Movimiento,'Salida'=IsNull((Select SUM(Salida) From VExtractoMovimientoProducto E " & Where & " And E.Movimiento=T.Movimiento ),0),'Entrada'=IsNull((Select SUM(Entrada) From VExtractoMovimientoProducto E " & Where & " And E.Movimiento=T.Movimiento),0) From VTipoExtractoMovimientoProducto T " & OrderBy
            Dim aTable1 As New SqlDataAdapter(strSQL, conn)

            Try

                DSDatos.Tables.Add("VExtractoMovimientoProducto")

                conn.Open()
                aTable2.Fill(DSDatos.Tables("VExtractoMovimientoProducto"))
                conn.Close()

                Dim Entradas As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VExtractoMovimientoProducto"), "Entrada")
                Dim Salidas As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VExtractoMovimientoProducto"), "Salida")
                Dim ExitenciaFinal As Decimal = (Existencia + Entradas) - Salidas

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoProductoResumen")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Existencia Inicial
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtExistenciaInicial"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(Existencia, True)

                'Existencia Final
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExistenciaFinal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(ExitenciaFinal, True)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, , DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoProductoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String, Optional OrdenadoFechaOperacion As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * from VExtractoMovimientoProductoDetalle " & Where & " " & OrderBy, conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 500
                DSDatos.Tables.Add("VExtractoMovimientoProductoDetalle")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExtractoMovimientoProductoDetalle"))
                conn.Close()

                Dim dt As DataTable = DSDatos.Tables("VExtractoMovimientoProductoDetalle")
                Dim i As Integer = 0
                Dim SaldoAnterior As Decimal = 0

                For Each oRow As DataRow In dt.Rows

                    If i = 0 Then
                        SaldoAnterior = Existencia
                    End If

                    oRow("Saldo") = (SaldoAnterior + CDec(oRow("Entrada"))) - CDec(oRow("Salida"))
                    SaldoAnterior = CDec(oRow("Saldo"))

                    i = 1

                Next

                'Calculo para los totales
                Dim Compras As Decimal
                Dim Entrada As Decimal
                Dim Venta As Decimal
                Dim salida As Decimal

                For Each oRow As DataRow In dt.Rows
                    'Suma Compras + Entradas
                    If oRow("Tipo") = "COMPRA" Then
                        Compras = Compras + CDec(oRow("Entrada"))
                    End If

                    If oRow("Tipo") = "ENTRADA" Then
                        Entrada = Entrada + CDec(oRow("Entrada"))
                    End If

                    'Suma Venta + salida
                    If oRow("Tipo") = "VENTA" Then
                        Venta = Venta + CDec(oRow("Salida"))
                    End If
                    If oRow("Tipo") = "SALIDA" Then
                        salida = salida + CDec(oRow("Salida"))
                    End If

                Next

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                If OrdenadoFechaOperacion = False Then
                    LoadReport(Report, "rptMovimientoProductoDetalle")
                Else
                    LoadReport(Report, "rptMovimientoProductoDetalleFechaOperacion")
                End If

                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubtitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Subtitulo

                ''Existencia Anterior
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtExistenciaAnterior"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Existencia, True)

                ''Existencia Final
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExitFinal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(SaldoAnterior, True)

                ''Existencia Anterior
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExInicial"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Existencia, True)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalcompras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Compras, True)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalEntradas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Entrada, True)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalVentas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Venta, True)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalSalida"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(salida, True)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, , DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoProductoDetalleCosteado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * from VExtractoMovimientoProductoDetalleCosteado " & Where & " " & OrderBy, conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 500
                DSDatos.Tables.Add("VExtractoMovimientoProductoDetalleCosteado")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExtractoMovimientoProductoDetalleCosteado"))
                conn.Close()

                Dim dt As DataTable = DSDatos.Tables("VExtractoMovimientoProductoDetalleCosteado")
                Dim i As Integer = 0
                Dim SaldoAnterior As Decimal = 0

                For Each oRow As DataRow In dt.Rows

                    If i = 0 Then
                        SaldoAnterior = Existencia
                    End If

                    oRow("Saldo") = (SaldoAnterior + CDec(oRow("Entrada"))) - CDec(oRow("Salida"))
                    SaldoAnterior = CDec(oRow("Saldo"))

                    i = 1

                Next

                'Calculo para los totales
                Dim Compras As Decimal
                Dim Entrada As Decimal
                Dim Venta As Decimal
                Dim salida As Decimal

                For Each oRow As DataRow In dt.Rows
                    'Suma Compras + Entradas
                    If oRow("Tipo") = "COMPRA" Then
                        Compras = Compras + CDec(oRow("Entrada"))
                    End If

                    If oRow("Tipo") = "ENTRADA" Then
                        Entrada = Entrada + CDec(oRow("Entrada"))
                    End If

                    'Suma Venta + salida
                    If oRow("Tipo") = "VENTA" Then
                        Venta = Venta + CDec(oRow("Salida"))
                    End If
                    If oRow("Tipo") = "SALIDA" Then
                        salida = salida + CDec(oRow("Salida"))
                    End If

                Next

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                LoadReport(Report, "rptMovimientoProductoDetalleCosteado")

                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubtitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Subtitulo

                ''Existencia Anterior
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtExistenciaAnterior"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Existencia, True)

                ''Existencia Final
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExitFinal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(SaldoAnterior, True)

                ''Existencia Anterior
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExInicial"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Existencia, True)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalcompras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Compras, True)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalEntradas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Entrada, True)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalVentas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Venta, True)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalSalida"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(salida, True)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, , DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub PlanillaTomaInventarioFisico(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VProducto " & Where & " " & OrderBy, conn)
            Try

                DSDatos.Tables.Add("VProducto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProducto"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaTomaInventario")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub PlanillaTomaInventarioFisicoPorDeposito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoFiltro As String, ByVal Filtro As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExistenciaDeposito " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaTomaInventarioPorDeposito")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''TipoFiltro
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = TipoFiltro

                ''Filtro
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Filtro

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaBajoMinimoSobreMaximo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExistenciaDeposito " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaBajoStockMinimo")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaMovimientoCalculado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal MostrarPeso As Boolean, Optional ByVal FechaDesde As Date = Nothing, Optional ByVal FechaHasta As Date = Nothing, Optional ByVal ResumidoProducto As Boolean = False, Optional ByVal WhereResumenProducto As String = "")

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim strSQL As String = ""
            If ResumidoProducto Then
                strSQL = "Select * from (Select " &
                " 'IDSucursal'=0,'IDDeposito'=0,'Deposito'='---',E.IDLinea,E.Linea" &
                ",E.IDTipoProducto,E.TipoProducto,E.IDMarca,E.Marca,E.IDProducto,E.Referencia" &
                ",E.Producto,ISNULL(AVG(E.Existencia) ,0) Existencia,ISNULL(SUM(VEMC.Compras) ,0) Compras," &
                "ISNULL(SUM(VEMC.Entradas) ,0) Entradas,ISNULL(SUM(VEMC.Salidas) ,0) Salidas," &
                "ISNULL(SUM(VEMC.Ventas) ,0) Ventas," &
                "ISNULL(AVG(E.PesoUnitario),1) PesoUnitario," &
                " ExistenciaAnterior = IsNUll((Select (Sum(Isnull(Entrada,0))-Sum(IsNull(Salida,0))) FRom VExtractoMovimientoProductoDetalleDatosMinimos where IDProducto = E.IDProducto" &
                WhereResumenProducto & " and Fecha < ' " & CSistema.FormatoFechaBaseDatos(FechaDesde, True, False) & "'),0) " &
                " From VExistenciaDeposito E " &
                " LEFT JOIN VExistenciaMovimientoCalculado VEMC on E.IDProducto=VEMC.IDProducto and E.IDDeposito=VEMC.IDDeposito " &
                " AND VEMC.Fecha >= '" & CSistema.FormatoFechaBaseDatos(FechaDesde, True, False) & "' AND VEMC.Fecha <= '" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "' " &
                Where &
                " GROUP BY E.IDLinea,E.Linea,E.IDTipoProducto, E.TipoProducto," &
                " E.IDMarca,E.Marca,E.IDProducto,E.Referencia,E.Producto" & OrderBy
            Else
                strSQL = "Select * from (Select " &
                " E.IDSucursal,E.IDDeposito,E.Deposito,E.IDLinea,E.Linea" &
                ",E.IDTipoProducto,E.TipoProducto,E.IDMarca,E.Marca,E.IDProducto,E.Referencia" &
                ",E.Producto,ISNULL(AVG(E.Existencia) ,0) Existencia,ISNULL(SUM(VEMC.Compras) ,0) Compras," &
                "ISNULL(SUM(VEMC.Entradas) ,0) Entradas,ISNULL(SUM(VEMC.Salidas) ,0) Salidas," &
                "ISNULL(SUM(VEMC.Ventas) ,0) Ventas," &
                "ISNULL(AVG(E.PesoUnitario),1) PesoUnitario," &
                " ExistenciaAnterior = IsNUll((Select (Sum(Isnull(Entrada,0))-Sum(IsNull(Salida,0))) " &
                "   FRom VExtractoMovimientoProductoDetalle where IDProducto = E.IDProducto" &
                " and IDDeposito = E.IDDeposito and Fecha < ' " & CSistema.FormatoFechaBaseDatos(FechaDesde, True, False) & "'),0) " &
                " From VExistenciaDeposito E " &
                " LEFT JOIN VExistenciaMovimientoCalculado VEMC on E.IDProducto=VEMC.IDProducto and E.IDDeposito=VEMC.IDDeposito " &
                " AND VEMC.Fecha >= '" & CSistema.FormatoFechaBaseDatos(FechaDesde, True, False) & "' AND VEMC.Fecha <= '" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "' " &
                Where &
                " GROUP BY E.IDSucursal,E.IDDeposito,E.Deposito,E.IDLinea,E.Linea,E.IDTipoProducto,E.TipoProducto," &
                " E.IDMarca,E.Marca,E.IDProducto,E.Referencia,E.Producto" & OrderBy

            End If

            Dim aTable1 As New SqlDataAdapter(strSQL, conn)
            aTable1.SelectCommand.CommandTimeout = 500 'IMPORTANTE

            Try

                DSDatos.Tables.Add("VExistenciaMovimientoCalculado")

                conn.Open()

                aTable1.Fill(DSDatos.Tables("VExistenciaMovimientoCalculado"))
                conn.Close()


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If MostrarPeso Then
                    LoadReport(Report, "rptExistenciaMovimientoCalculadoPesos")
                Else
                    LoadReport(Report, "rptExistenciaMovimientoCalculado")
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                Dim fAnterior As Date = Date.Parse(FechaDesde)
                fAnterior = fAnterior.AddDays(-1)
                'Establecer campos
                EstablecerTextoCampo(Report, "txtCampo1", "Ex. al " & String.Format(fAnterior, "dd/MM/yyyy"))
                EstablecerTextoCampo(Report, "txtCampo2", "Ex. al " & FechaHasta)
                EstablecerTextoCampo(Report, "txtCampo3", "Ex. al " & String.Format(Date.Today, "dd/MM/yyyy"))
                EstablecerTextoCampo(Report, "txtCampo4", "Mov. del " & FechaDesde & " al " & FechaHasta)


                'Establecer conexion
                EstablecerConexion(Report)


                desBloquearEstadoOperacion(VGCadenaConexion, "existeCalc")

                'Mostrar Informe 
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VProducto " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProducto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProducto"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoProducto")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub AuditoriaProductos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VAProductoListaPrecio " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VAProductoListaPrecio")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VAProductoListaPrecio"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAuditoriaProductos")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub AuditoriaExcepciones(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VaProductoListaPrecioExcepciones " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VaProductoListaPrecioExcepciones")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VaProductoListaPrecioExcepciones"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAuditoriaExcepciones")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListaPrecioExcepcion(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VProductoListaPrecioExcepcionesPorVendedor " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProductoListaPrecioExcepcionesPorVendedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProductoListaPrecioExcepcionesPorVendedor"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExcepcionCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ListadoPrecioProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereListaPrecio As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal EsPrecio As Boolean, ByVal VerUnidad As Boolean)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VProducto " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VProductoListaPrecio " & WhereListaPrecio, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProducto")
                DSDatos.Tables.Add("VProductoListaPrecio")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProducto"))
                aTable2.Fill(DSDatos.Tables("VProductoListaPrecio"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPrecioProducto")
                Report.SetDataSource(DSDatos)

                'Parametros
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim Unidad As ParameterField = New ParameterField()
                Dim Precio As ParameterField = New ParameterField()
                Dim UnidadValue As ParameterDiscreteValue = New ParameterDiscreteValue()
                Dim PrecioValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Unidad.ParameterFieldName = "ParametroUnidad"
                UnidadValue.Value = VerUnidad

                Precio.ParameterFieldName = "ParametroPrecio"
                PrecioValue.Value = EsPrecio

                Unidad.CurrentValues.Add(UnidadValue)
                Precio.CurrentValues.Add(PrecioValue)

                Parametros.Add(Unidad)
                Parametros.Add(Precio)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub AjusteControlInventario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VControlInventario Where IDTransaccion =" & Where, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion =" & Where, conn)

            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAjusteControlInventario")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ListadoMovimientoCombustible(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VMovimiento " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoMovimientoCombustible")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoMovimientoCombustibleDetallado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal Tipo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VMovimiento " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleMovimiento " & WhereDetalle & " ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")
                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))
                aTable2.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                Select Case Tipo
                    Case "Detallado"
                        LoadReport(Report, "rptListadoMovimientoCombustibleDetallado")
                    Case "Chofer"
                        LoadReport(Report, "rptListadoMovimientoCombustibleDetalladoChofer")
                    Case "Camion"
                        LoadReport(Report, "rptListadoMovimientoCombustibleDetalladoCamion")
                End Select

                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoMovimientoProducto(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleMovimiento " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoMovimientoProducto")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoMovimientoProductoDetallado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VMovimiento " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleMovimiento " & WhereDetalle & " ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")
                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))
                aTable2.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoMovimientoProductoDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                ''Mostrar Informe
                'MostrarReporte(Report, Titulo,, DSDatos)
                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoMovimientoDetalladoProductoFecha(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VMovimiento " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleMovimiento " & WhereDetalle & " ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")
                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))
                aTable2.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoMovimientoDetalladoProductoFecha")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                ''Mostrar Informe
                'MostrarReporte(Report, Titulo,, DSDatos)
                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoOperacionProducto(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Producto, Operacion, DepositoSalida, DepositoEntrada, 'CantidadSalida' = sum(CantidadSalida), 'CantidadEntrada' = sum(CantidadEntrada), 'TotalSalida' = sum(TotalDetalleSalida), 'TotalEntrada' = sum(TotalDetalleEntrada), CuentaContableEntrada, CuentaContableSalida From VDetalleMovimiento " & Where & " group by Producto, Operacion, DepositoSalida, DepositoEntrada, CuentaContableEntrada, CuentaContableSalida " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoOperacionProducto")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCargaMercaderia(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim strSQL As String = "Select " & Top & " * From VCargaMercaderia " & Where & " " & OrderBy
            Dim aTable1 As New SqlDataAdapter(strSQL, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCargaMercaderia")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCargaMercaderiaDetallado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCargaMercaderia " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleCargaMercaderia " & WhereDetalle & " ", conn)

            Try

                DSDatos.Tables.Add("VCargaMercaderia")
                DSDatos.Tables.Add("VDetalleCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))
                aTable2.Fill(DSDatos.Tables("VDetalleCargaMercaderia"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCargaMercaderiaDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub CargaMercaderia(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCargaMercaderia Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleCargaMercaderia Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCargaMercaderia")
                DSDatos.Tables.Add("VDetalleCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))
                aTable2.Fill(DSDatos.Tables("VDetalleCargaMercaderia"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCargaMercaderia")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub PlanillaInventario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaInventario")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub TomaInventario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptTomaInventario")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComparativoInventarioEquipo(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComparativoInventarioEquipo")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComparativoInventarioSistema(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal strfiltro As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion & strfiltro, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComparativoInventarioSistema")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub PrecargaMovimientoStock(ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPrecargaMovimiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VPrecargaDetalleMovimiento Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPrecargaMovimiento")
                DSDatos.Tables.Add("VPrecargaDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPrecargaMovimiento"))
                aTable2.Fill(DSDatos.Tables("VPrecargaDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPrecargaMovimientos")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub MovimientoStock(ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VMovimiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VDetalleMovimiento Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")
                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))
                aTable2.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientos")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteAsignacionCamion
        Inherits CReporte
        Sub AbastecimientoCamion(ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From vAsignacionCamion Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From vDetalleAsignacionCamion Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vAsignacionCamion")
                DSDatos.Tables.Add("vDetalleAsignacionCamion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vAsignacionCamion"))
                aTable2.Fill(DSDatos.Tables("vDetalleAsignacionCamion"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAsignacionCamion")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CReporteOrdenDePedido
        Inherits CReporte
        Sub OrdenDePedido(ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VOrdenDePedido Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VDetalleOrdenDePedido Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenDePedido")
                DSDatos.Tables.Add("VDetalleOrdenDePedido")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenDePedido"))
                aTable2.Fill(DSDatos.Tables("VDetalleOrdenDePedido"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptOrdenDePedido")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoOrdenDePedido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String, Optional Resumido As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * from VOrdenDePedido " & Where & " ", conn)
            Dim aTable2 As New SqlDataAdapter("Select " & Top & " * from VDetalleOrdenDePedido " & WhereDetalle & " " & OrderBy, conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 500
                DSDatos.Tables.Add("VOrdenDePedido")
                DSDatos.Tables.Add("VDetalleOrdenDePedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenDePedido"))
                aTable2.Fill(DSDatos.Tables("VDetalleOrdenDePedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                If Resumido = True Then
                    LoadReport(Report, "rptListadoOrdenDePedidoResumido")
                Else
                    LoadReport(Report, "rptListadoOrdenDePedidoDetallado")
                End If

                Report.SetDataSource(DSDatos)


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDePedidoPorProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * from VPedido " & Where & " ", conn)
            Dim aTable2 As New SqlDataAdapter("Select " & Top & " * from VDetallePedido " & WhereDetalle & " ", conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 500
                DSDatos.Tables.Add("VPedido")
                DSDatos.Tables.Add("VDetallePedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                aTable2.Fill(DSDatos.Tables("VDetallePedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                LoadReport(Report, "rptListadoPedidoPorProducto")

                Report.SetDataSource(DSDatos)


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CReporteAcuerdo
        Inherits CReporte
        Sub Acuerdo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VAcuerdo " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VAcuerdo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VAcuerdo"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAcuerdo")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InformeAcuerdoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VAcuerdo " & Where & " ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VAcuerdo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VAcuerdo"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAcuerdoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteLotes
        Inherits CReporte

        Sub ListadoLotesEmitidos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteEmitido")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoLotesEmitidosDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim sql1 As String = "Select " & Top & " * From VLoteDistribucion " & Where
            Dim sql2 As String = "Select * From VVentaLoteDistribucion WHERE IDTransaccionLote IN(SELECT DISTINCT(IDTransaccion) FROM VLoteDistribucion " & Where & ")"


            Dim sql3 As String = "Select * from vDetalleLoteDistribucion"
            Dim sql4 As String = "Select * from vDetalleVenta where vDetalleVenta.IDtransaccion in (select IDTransaccionVenta from VentaLoteDistribucion where idtransaccionLote IN(SELECT DISTINCT(IDTransaccion) FROM VLoteDistribucion " & Where & "))"
            'Dim sql4 As String = "Select * from vDetalleVenta where IDTransaccion In (Select IDTransaccionVenta From VVentaLoteDistribucion WHERE IDTransaccionLote In(Select DISTINCT(IDTransaccion) FROM VLoteDistribucion " & Where & "))"
            'Dim sql4 As String = "Select * from VDetalleLoteVentaRemision where VDetalleLoteVentaRemision.IDtransaccion In (Select IDTransaccionVenta from VentaLoteDistribucion where idtransaccionLote  In(Select DISTINCT(IDTransaccion) FROM VLoteDistribucion " & Where & "))"
            Dim aTable1 As New SqlDataAdapter(sql1, conn)
            Dim aTable2 As New SqlDataAdapter(sql2, conn)
            Dim aTable3 As New SqlDataAdapter(sql3, conn)
            Dim aTable4 As New SqlDataAdapter(sql4, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")
                DSDatos.Tables.Add("vDetalleLoteDistribucion")
                DSDatos.Tables.Add("vDetalleVenta")
                'DSDatos.Tables.Add("VDetalleLoteVentaRemision")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                aTable3.Fill(DSDatos.Tables("vDetalleLoteDistribucion"))
                aTable4.Fill(DSDatos.Tables("vDetalleVenta"))
                'aTable4.Fill(DSDatos.Tables("VDetalleLoteVentaRemision"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteEmitidoDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ListadoKilosTransportados(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim sql1 As String = "Select " & Top & " * From VKilosTransportados " & Where
            Dim aTable1 As New SqlDataAdapter(sql1 & " " & OrderBy, conn)
            Try

                DSDatos.Tables.Add("VKilosTransportados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VKilosTransportados"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptKilosTransportados")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ListadoKilosTransportadosResumido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim sql1 As String = "Select " & Top & " * From VKilosTransportadosResumido " & Where
            Dim aTable1 As New SqlDataAdapter(sql1 & " " & OrderBy, conn)
            Try

                DSDatos.Tables.Add("VKilosTransportadosResumido")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VKilosTransportadosResumido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptKilosTransportadosResumido")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoLotesEmitidosDetalleOrdenCarga(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal vIDTransaccion As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim sql1 As String = "Select * From VLoteDistribucion " & Where
            Dim sql2 As String = "Select * From VVentaLoteDistribucion WHERE IDTransaccionLote IN(SELECT DISTINCT(IDTransaccion) FROM VLoteDistribucion " & Where & ")"
            'Dim sql2 As String = "Select * From VVentaLoteDistribucion1 WHERE IDTransaccionLote IN(SELECT DISTINCT(IDTransaccion) FROM VLoteDistribucion " & Where & ")"

            Dim aTable1 As New SqlDataAdapter(sql1 & " " & OrderBy, conn)
            'Dim aTable2 As New SqlDataAdapter(sql2 & WhereDetalle, conn)
            Dim aTable2 As New SqlDataAdapter(sql2 & OrderBy, conn)

            Dim sql3 As String = "Select * from vDetalleLoteDistribucion " & Where
            Dim sql4 As String = "Select * from vDetalleVenta where vDetalleVenta.IDtransaccion in (select IDTransaccionVenta from VentaLoteDistribucion where idtransaccionLote = " & vIDTransaccion & ")"
            'Dim sql4 As String = "Select * from VDetalleLoteVentaRemision where VDetalleLoteVentaRemision.IDtransaccion in (select IDTransaccionVenta from VentaLoteDistribucion where idtransaccionLote = " & vIDTransaccion & ")"

            Dim aTable3 As New SqlDataAdapter(sql3, conn)
            Dim aTable4 As New SqlDataAdapter(sql4, conn)

            'Dim sql5 As String = "Select * From vFacturaConRecibo Where (IDFormaPagoFactura = 2 or Boleta = 'True') and IDTransaccion in (select IDTransaccionVenta from VentaLoteDistribucion where idtransaccionLote = " & vIDTransaccion & ")"
            'Dim aTable5 As New SqlDataAdapter(sql5, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")
                ' DSDatos.Tables.Add("VVentaLoteDistribucion1")
                DSDatos.Tables.Add("vDetalleLoteDistribucion")
                DSDatos.Tables.Add("vDetalleVenta")
                'DSDatos.Tables.Add("vFacturaConRecibo")
                'DSDatos.Tables.Add("VDetalleLoteVentaRemision")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))

                'aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion1"))
                aTable3.Fill(DSDatos.Tables("vDetalleLoteDistribucion"))
                aTable4.Fill(DSDatos.Tables("vDetalleVenta"))
                'aTable5.Fill(DSDatos.Tables("vFacturaConRecibo"))
                'aTable4.Fill(DSDatos.Tables("VDetalleLoteVentaRemision"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteEmitidoDetalle")

                'LoadReport(Report, "rptFacturaConRecibo")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)


                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()
                'aTable5.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturaConRecibo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal vIDTransaccion As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim sql5 As String = "Select * From vFacturaConRecibo Where (IDFormaPagoFactura = 2 or Boleta = 'True') and IDTransaccion in (select IDTransaccionVenta from VentaLoteDistribucion where idtransaccionLote = " & vIDTransaccion & ")"
            Dim aTable5 As New SqlDataAdapter(sql5, conn)

            Try
                DSDatos.Tables.Add("vFacturaConRecibo")
                'DSDatos.Tables.Add("VDetalleLoteVentaRemision")

                conn.Open()
                aTable5.Fill(DSDatos.Tables("vFacturaConRecibo"))
                'aTable4.Fill(DSDatos.Tables("VDetalleLoteVentaRemision"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptFacturaConRecibo")

                'LoadReport(Report, "rptFacturaConRecibo")
                Report.SetDataSource(DSDatos)

                'Establecer conexion
                EstablecerConexion(Report)


                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable5.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDevolucionLotes(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            'Dim aTable2 As New SqlDataAdapter("Select * From VDetalleLoteDistribucion " & WhereDetalle, conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDevolucionLote " & Where & " " & OrderBy, conn)
            'Dim aTable2 As New SqlDataAdapter("Select * From VDetalleDevolucionLote " & WhereDetalle, conn)

            Try

                'DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VDevolucionLote")
                'DSDatos.Tables.Add("VDetalleDevolucionLote")
                ''DSDatos.Tables.Add("VVentaLoteDistribucion")
                'DSDatos.Tables.Add("VVentaLoteDistribucion1")

                conn.Open()
                'aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                'aTable2.Fill(DSDatos.Tables("VDetalleDevolucionLote"))
                aTable1.Fill(DSDatos.Tables("VDevolucionLote"))
                ''aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                'aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion1"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                'LoadReport(Report, "rptListadoLotedetalleDevolucion")
                LoadReport(Report, "rptListadoLoteDevolucion")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                '''Rango de Fechas
                ''txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                ''txtField.Text = Del

                ''txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                ''txtField.Text = Al

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoComprobantesSinLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVentasSinlotes " & Where & " and IDTransaccion not in(select IDTransaccion from VLoteDistribucion) " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VVentasSinlotes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentasSinlotes"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoComprobanteSinLote")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDetalleComprobantesSinLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " and IDTransaccion not in(select IDTransaccion from VLoteDistribucion)" & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VVentaLoteDistribucion " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDetalleCompranteSinLote")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub HojaRuta(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " Order By Comprobante", conn)
            Dim aTable2 As New SqlDataAdapter("Select Cliente, Direccion, Telefono, Referencia, [Cod.], Comprobante, Condicion, Total, Vendedor, Credito From VVentaLoteDistribucion  " & WhereDetalle & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptHojaRuta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub CargaPlanilla(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select * From VVenta ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleLoteDistribucion " & WhereDetalle, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VDetalleLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VDetalleLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCargaPlanilla")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDevolucionLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VDevolucionLote " & Where, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleDevolucionLote" & Where, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDevolucionLote")
                DSDatos.Tables.Add("VDetalleDevolucionLote")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDevolucionLote"))
                aTable2.Fill(DSDatos.Tables("VDetalleDevolucionLote"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDevolucionLotes")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoRendicionCobranza(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VRendicionLote " & Where, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VDetalleRendicionLoteCheque " & Where, conn)
            Dim aTable3 As New SqlDataAdapter("Select  * From VDetalleRendicionLoteVentasAnuladas " & Where, conn)
            Dim aTable4 As New SqlDataAdapter("Select  * From VDetalleRendicionLoteDocumento " & Where, conn)
            Dim aTable5 As New SqlDataAdapter("Select  * From VDesgloseBillete " & Where & " Order by Valor Desc", conn)
            Dim aTable6 As New SqlDataAdapter("Select  * From DetalleRendicionLoteCobranzaExtra " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRendicionLote")
                DSDatos.Tables.Add("VDetalleRendicionLoteCheque")
                DSDatos.Tables.Add("VDetalleRendicionLoteVentasAnuladas")
                DSDatos.Tables.Add("VDetalleRendicionLoteDocumento")
                DSDatos.Tables.Add("VDesgloseBillete")
                DSDatos.Tables.Add("DetalleRendicionLoteCobranzaExtra")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRendicionLote"))
                aTable2.Fill(DSDatos.Tables("VDetalleRendicionLoteCheque"))
                aTable3.Fill(DSDatos.Tables("VDetalleRendicionLoteVentasAnuladas"))
                aTable4.Fill(DSDatos.Tables("VDetalleRendicionLoteDocumento"))
                aTable5.Fill(DSDatos.Tables("VDesgloseBillete"))
                aTable6.Fill(DSDatos.Tables("DetalleRendicionLoteCobranzaExtra"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptRendicionCobranza")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()
                aTable5.Dispose()
                aTable6.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComposiciondeLotesparaDistribucion(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From vVentaLoteDistribucion " & Where & " " & OrderBy, conn)



            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vVentaLoteDistribucion"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComposicionLotesparaDistribucion")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class
    Public Class CReporteChequeEmitido
        Inherits CReporte
        Sub ListarChequeEmitido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Tipo As String, ByVal Estado As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal TipoInforme As String, Optional Agrupado As String = "")

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String
            If Agrupado = "Cuenta Contable" Then
                vTabla = "vOrdenPagoChequeCC"

            Else
                vTabla = "vOrdenPagoCheque"
            End If


            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From " & vTabla & " " & Where & " " & Tipo & " " & Estado & " " & OrderBy, conn)
            Dim Titulo As String = ""
            aTable1.SelectCommand.CommandTimeout = 60000
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(vTabla)

                conn.Open()
                aTable1.Fill(DSDatos.Tables(vTabla))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                Select Case Agrupado
                    Case "Proveedor"
                        LoadReport(Report, "rptListadoChequesEmitidosPorProveedor")
                        Titulo = "Listado de Cheques Emitidos / Por Proveedores"
                    Case "Cta/Cheque/Diferido"
                        LoadReport(Report, "rptListadoChequesEmitidosPorCheques")
                        Titulo = "Listado de Cheques Emitidos / Por Banco-Nro de Cheque"
                    Case "Cuenta Contable"

                        For Each row As DataRow In DSDatos.Tables(vTabla).Rows
                            row("CuentaContable") = CData.GetTable("vCuentaContable").Select("Codigo = '" & row("Codigo") & "'")(0)("CuentaContable")
                        Next

                        LoadReport(Report, "rptListadoChequesEmitidosPorCuentaContable")
                        Titulo = "Listado de Cheques Emitidos / Por Cuenta Contable"
                End Select

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class
    Public Class CReporteChequeras
        Inherits CReporte
        Sub ListarChequera(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)
            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewChequeras " & Where)
            dt.TableName = "SpViewChequeras;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptChequeras")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Sucursal
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Sucursal

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListarChequeraTotal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)
            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewChequerasTotal " & Where)
            dt.TableName = "SpViewChequerasTotal;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptChequerasTotal")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Sucursal
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Sucursal

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListarEstadoCheques(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)
            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewEstadoCheque " & Where)
            dt.TableName = "SpViewEstadoCheque;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoEstadoCheque")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Sucursal
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Sucursal

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

    End Class
    Public Class CReporteContrato
        Inherits CReporte
        Sub ListarContratos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)
            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewChequeras " & Where)
            dt.TableName = "SpViewChequeras;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptChequeras")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Sucursal
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Sucursal

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
    End Class


    Public Class CReporteCotizacion
        Inherits CReporte
        Sub ListarCotizacion(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)
            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("select * from VCotizacion " & Where & " order by Fecha, Moneda")
            dt.TableName = "VCotizacion"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCotizacionHistorial")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Sucursal
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Sucursal

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
    End Class
    Public Class CReporteCuentaBancaria
        Inherits CReporte
        Sub ListarCuentaBancariaSaldoDiario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)
            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("select * from vCuentaBancariaSaldoDiario " & Where & " order by Fecha")
            dt.TableName = "vCuentaBancariaSaldoDiario"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCuentaBancariaSaldoDiario")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Sucursal
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Sucursal

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
    End Class
    Public Class CReporteCheque
        Inherits CReporte

        Sub ImprimirCheque(ByRef frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal IDCuentaBancaria As Integer, ByVal Diferido As Boolean, ByVal Impresora As String, ByVal Cadena1 As String, ByVal Cadena2 As String, ByVal OP As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCheque")
                conn.Open()

                aTable1.Fill(DSDatos.Tables("VCheque"))
                conn.Close()

                Dim PathReporte As String = ""
                Dim dtCuentaContable As DataTable = CData.GetTable("VCuentaBancaria", " ID= " & IDCuentaBancaria).Copy

                If dtCuentaContable Is Nothing Then
                    'Mostrar error
                End If

                If dtCuentaContable.Rows.Count = 0 Then
                    'Mostrar error
                End If

                Dim oRow As DataRow = dtCuentaContable.Rows(0)

                If Diferido = True Then
                    PathReporte = oRow("FormatoDiferido").ToString
                Else
                    PathReporte = oRow("FormatoAlDia").ToString
                End If

                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    'Mostrar error
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtImporte1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Cadena1

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtImporte2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Cadena2

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOP"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = OP

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListarChequeCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Tipo As String, ByVal Estado As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal TipoInforme As String, Optional Agrupado As Boolean = False, Optional Grupo1 As String = "")

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " Numero, CodigoSucursal, Cliente, CodigoCliente, Banco, CodigoBanco, Fecha, NroCheque, Moneda, Cotizacion, Total, Saldo, CodigoTipo, Vencimiento, Estado, Tipo, Motivo, FechaRechazo, FechaCargaRechazo,ImporteMoneda, SaldoACuenta,FechaVencimiento, 'Grupo1'=" & Grupo1 & " From VChequeCliente " & Where & " " & Tipo & " " & Estado & " " & OrderBy, conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Numero, CodigoSucursal, Cliente, CodigoCliente, Banco, CuentaBancaria, CodigoBanco, Fecha, NroCheque, Moneda, Cotizacion, Total, Saldo, CodigoTipo, Vencimiento, Estado, Tipo, Motivo, FechaRechazo, FechaCargaRechazo,ImporteMoneda, SaldoACuenta,FechaVencimiento, 'Grupo1'=" & Grupo1 & " From VChequeCliente " & Where & " " & Tipo & " " & Estado & " " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequeCliente")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequeCliente"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView

                If Agrupado = False Then
                    LoadReport(Report, "rtpListadoCheque")
                Else
                    LoadReport(Report, "rptListadoChequeAgrupado")
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "Listado de Cheques de Clientes", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListarChequeDiferidoParaDescuento(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Tipo As String, ByVal Estado As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal TipoInforme As String, Optional Agrupado As Boolean = False, Optional Grupo1 As String = "")

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VChequeCliente " & Where & " " & Tipo & " " & Estado & " " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequeCliente")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequeCliente"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                LoadReport(Report, "rptDescuentoCheque")


                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "Listado de Cheques de Diferidos para Descuento", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ImprimirListadoCanje(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VPagoChequeCliente " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePagoChequeCliente " & WhereDetalle, conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VFormaPago ", conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Debug.Print(aTable2.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VPagoChequeCliente")
                DSDatos.Tables.Add("VDetallePagoChequeCliente")
                DSDatos.Tables.Add("VFormaPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPagoChequeCliente"))
                aTable2.Fill(DSDatos.Tables("VDetallePagoChequeCliente"))
                aTable3.Fill(DSDatos.Tables("VFormaPago"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoCanjes")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirListadoChequesRechazados(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal SubTitulo As String, ByVal Titulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From vChequeClienteRechazado " & Where & " " & OrderBy, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            'Debug.Print(aTable2.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("vChequeClienteRechazado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vChequeClienteRechazado"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoChequesRechazados")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListarChequesDiferidosDepositados(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Condicion As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VChequesDiferidosDepositados " & Where & Condicion & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequesDiferidosDepositados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequesDiferidosDepositados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpChequesDiferidosDepositados")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListarChequesDiferidosDescontados(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Responsable As String, ByVal SubTitulo As String, Optional ByVal Top As String = "")

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VListadoChequeDescontadoVencido " & Where, conn)

            ' Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VListadoChequeDescontadoVencido")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VListadoChequeDescontadoVencido"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptChequeDescontadoVencido")
                Report.SetDataSource(DSDatos)


                'Informacion de Software
                EstablecerTitulos(Report, "Listado de Cheques Descontados", SubTitulo)

                'Establecer conexion
                'EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListarChequeClienteDiferido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Condicion As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VchequeClienteVenta " & Where & " And Diferido='True' " & Condicion & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequeClienteVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequeClienteVenta"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptIngresoChequesDiferidos")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub SaldoChequesDiferidos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewChequeClienteDiferidoMovimiento2 " & Where)
            dt.TableName = "SpViewChequeClienteDiferidoMovimiento2;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpSaldoChequesDiferido")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Sucursal
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Sucursal

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DescuentoChequeDiferido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Responsable As String, ByVal Titulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VDetalleDescuentoCheque " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDescuentoCheque")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDescuentoCheque"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDescuentoCheque")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirEfectivizacion(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal Titulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VEfectivizacion Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleEfectivizacion Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VEfectivizacion")
                DSDatos.Tables.Add("VDetalleEfectivizacion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VEfectivizacion"))
                aTable2.Fill(DSDatos.Tables("VDetalleEfectivizacion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptEfectivizacion")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCuentasACobrar
        Inherits CReporte

        Sub InventarioDocumentoPendientePorCliente(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal TotalAnticipo As Decimal, ByVal Responsable As String, Optional ByVal NombreRPT As String = "")

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientes.Idtransaccion, VInventarioDocumentosPendientes.* From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)


            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 120


                DSDatos.Tables.Add("VInventarioDocumentosPendientes")
                Consultando = True
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If NombreRPT = "" Then
                    LoadReport(Report, "rptInventarioDocumentoPendientePorCliente")
                Else
                    LoadReport(Report, NombreRPT)
                End If

                Report.SetDataSource(DSDatos)


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)


                Try
                    Report.SetParameterValue("TotalAnticipo", TotalAnticipo)

                Catch ex As Exception

                End Try
                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub InventarioDocumentoPendiente(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal TotalAnticipo As Decimal, ByVal Responsable As String, Optional ByVal NombreRPT As String = "")

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientes.Idtransaccion, VInventarioDocumentosPendientes.* From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)


            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 500
                'aTable1.SelectCommand.CommandTimeout = 200

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")
                Consultando = True
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If NombreRPT = "" Then
                    LoadReport(Report, "rptInventarioDocumentoPendiente")
                Else
                    LoadReport(Report, NombreRPT)
                End If

                Report.SetDataSource(DSDatos)


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)


                Try
                    Report.SetParameterValue("TotalAnticipo", TotalAnticipo)

                Catch ex As Exception

                End Try
                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub InventarioDocumentoPendienteCuentaContable(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientes.Idtransaccion, VInventarioDocumentosPendientes.* From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                Consultando = True
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteCC")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub InventarioImporteOriginalGS(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientes.Idtransaccion, VInventarioDocumentosPendientes.* From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                Consultando = True
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioPendienteOriginalGS")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteVencimientoCliente(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientes.Idtransaccion, VInventarioDocumentosPendientes.* From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                'aTable1.SelectCommand.CommandTimeout = 120
                aTable1.SelectCommand.CommandTimeout = 200

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                Consultando = True
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteVencimientoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteCobradorFechaVencimiento(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientes.Idtransaccion, VInventarioDocumentosPendientes.* From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                Consultando = True
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteCobradorFechaVencimiento")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteVendedorFechaVencimiento(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientes.Idtransaccion, VInventarioDocumentosPendientes.* From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                Consultando = True
                aTable1.SelectCommand.CommandTimeout = 120
                DSDatos.Tables.Add("VInventarioDocumentosPendientes")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteVendedorFechaVencimiento")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendientePorComprobante(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientesLote.Idtransaccion, VInventarioDocumentosPendientesLote.* From VInventarioDocumentosPendientesLote  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                Consultando = True
                aTable1.SelectCommand.CommandTimeout = 120
                DSDatos.Tables.Add("VInventarioDocumentosPendientesLote")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesLote"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendientePorComprobante")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteDireccionAlternativa(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " VInventarioDocumentosPendientes.Idtransaccion,VInventarioDocumentosPendientes.* From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                Consultando = True
                aTable1.SelectCommand.CommandTimeout = 120
                DSDatos.Tables.Add("VInventarioDocumentosPendientes")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteDireccionAlternativa")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldo(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal Fecha3 As String, ByVal Fecha4 As String, ByVal Fecha5 As String, ByVal Fecha6 As String, ByVal Fecha7 As String, ByVal Fecha8 As String, ByVal Fecha9 As String, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Dia7 As String, ByVal Dia8 As String, ByVal Dia9 As String, ByVal Refencia As String, ByVal Moneda As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDistribucionSaldo  " & Where & "  " & Cond & "" & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                Consultando = True
                DSDatos.Tables.Add("VDistribucionSaldo")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDistribucionSaldo"))
                conn.Close()
                Consultando = False

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldo")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6
                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha7

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha8

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha9

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia7

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia8

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt10"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenMovimientoSaldo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal TipoInforme As String, Optional ByVal ASC As Boolean = True)

            'Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Consultando = True
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldoCobro " & Where)
            Consultando = False

            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            'Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumendeMovimientoSaldo")

                Report.SetDataSource(DSDatos)


                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenMovimientoSaldoDetallado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal TipoInforme As String, Optional ByVal ASC As Boolean = True)

            'Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Consultando = True
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumeMovimientosSaldoCobroDetallado " & Where)
            Consultando = False

            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            'Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpResumenMovimientoSaldoDetallado")

                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Fecha
                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldoCobro(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal Fecha3 As String, ByVal Fecha4 As String, ByVal Fecha5 As String, ByVal Fecha6 As String, ByVal Fecha7 As String, ByVal Fecha8 As String, ByVal Fecha9 As String, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Dia7 As String, ByVal Dia8 As String, ByVal Dia9 As String, ByVal Referencia As String, ByVal Moneda As String, Optional ByVal ASC As Boolean = True, Optional ByVal IDSucursal As Integer = 0)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Execute SPDistribucionPago " & Where, conn)

            Dim Scrip As String

            If IDSucursal = 0 Then
                Scrip = " SpViewDistribucionSaldoCobro "
            Else
                Scrip = " SpViewDistribucionSaldoCobroSucursal "
                Where = Where & "," & IDSucursal
            End If

            Consultando = True
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute " & Scrip & Where)
            Consultando = False

            If dt Is Nothing Then
                Exit Sub
            End If

            'El nombre no se debe cambiar aunque sea otro SP.
            dt.TableName = "SpViewDistribucionSaldoCobro;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldo")
                Consultando = True
                Report.SetDataSource(DSDatos)
                Consultando = False

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6
                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha7

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha8

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha9

                'Dia
                Dia1 = Dia1 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                'Dia
                Dia2 = Dia2 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                'Dia
                Dia3 = Dia3 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                'Dia
                Dia4 = Dia4 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                'Dia
                Dia5 = Dia5 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                'Dia
                Dia6 = Dia6 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                'Dia
                Dia7 = Dia7 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia7

                'Dia
                Dia8 = Dia8 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia8

                'Dia
                Dia9 = Dia9 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


    End Class


    Public Class CReporteCuentasAPagar
        Inherits CReporte
        Sub InventarioDocumentoPendientePagoPorPlazo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, Optional ByVal NombreRPT As String = "")

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " [Cod.], Comprobante, Condicion, Numero, Origen, FechaCarga, FechaEmision, FechaVencimiento, PlazoComprobante,PlazoProveedor, IDMoneda, Moneda, Total, Saldo, Proveedor, Referencia, Cotizacion, Cuota,  Observacion, IDMoneda From VInventarioDocumentosPendientesPorPlazo " & Where & "  " & Cond & "" & OrderBy, conn)
            Dim aTable1 As New SqlDataAdapter("Select * From VInventarioDocumentosPendientesPorPlazo " & Where & "  " & Cond & "" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientesPorPlazo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesPorPlazo"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If NombreRPT = "" Then
                    LoadReport(Report, "rptInventarioDocumentoPendientePagoPorPlazo")
                Else
                    LoadReport(Report, NombreRPT)
                End If
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub InventarioDocumentoPendientePago(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, Optional ByVal NombreRPT As String = "")

                ' Creamos los componenetes para obtener los datos.
                Dim conn As New SqlConnection(VGCadenaConexion)
                Dim DSDatos As New DataSet
                Dim aTable1 As New SqlDataAdapter("Select " & Top & " [Cod.], Comprobante, Condicion, Numero, Origen, Emision, FechaVencimiento, Plazo, Moneda, Total, Saldo, Proveedor, Referencia, Cotizacion, Cuota, FechaIngreso, Observacion, IDMoneda From VInventarioDocumentosPendientesPago  " & Where & "  " & Cond & "" & OrderBy, conn)

                Try

                    DSDatos.Tables.Add("VInventarioDocumentosPendientesPago")

                    conn.Open()
                    aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesPago"))

                    conn.Close()

                    ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                    Dim Report As New ReportClass
                    If NombreRPT = "" Then
                        LoadReport(Report, "rptInventarioDocumentoPendientePago")
                    Else
                        LoadReport(Report, NombreRPT)
                    End If
                    Report.SetDataSource(DSDatos)

                    'Informacion de Software
                    EstablecerTitulos(Report, Titulo, TipoInforme)

                    'Establecer conexion
                    EstablecerConexion(Report)

                    'Mostrar Informe
                    MostrarReporte(Report, Titulo,, DSDatos)

                    aTable1.Dispose()

                Catch ex As Exception
                    MsgBox("Mensaje : " & ex.Message)
                End Try

            End Sub

            Sub InventarioPagarOriginalGs(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String)

                ' Creamos los componenetes para obtener los datos.
                Dim conn As New SqlConnection(VGCadenaConexion)
                Dim DSDatos As New DataSet
                Dim aTable1 As New SqlDataAdapter("Select * From VInventarioDocumentosPendientesPago  " & Where & "  " & Cond & "" & OrderBy, conn)

                Try

                    DSDatos.Tables.Add("VInventarioDocumentosPendientesPago")

                    conn.Open()
                    aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesPago"))

                    conn.Close()

                    ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                    Dim Report As New ReportClass
                    LoadReport(Report, "rptInventarioPagoOriginalGS")
                    Report.SetDataSource(DSDatos)

                    'Informacion de Software
                    EstablecerTitulos(Report, Titulo, TipoInforme)

                    'Establecer conexion
                    EstablecerConexion(Report)

                    'Mostrar Informe
                    MostrarReporte(Report, Titulo,, DSDatos)

                    aTable1.Dispose()

                Catch ex As Exception
                    MsgBox("Mensaje : " & ex.Message)
                End Try

            End Sub

            Sub InventarioPagarOriginalGsCuentaContable(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VInventarioDocumentosPendientesPago  " & Where & "  " & Cond & "" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientesPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesPago"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioPagoOriginalGSCC")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldosPago(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Fecha1 As Date, ByVal Fecha2 As Date, ByVal Fecha3 As Date, ByVal Fecha4 As Date, ByVal Fecha5 As Date, ByVal Fecha6 As Date, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, Optional ByVal ASC As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewDistribucionSaldoPago " & Where)

            'El nombre no se debe cambiar aunque sea otro SP.
            dt.TableName = "SpViewDistribucionSaldoPago;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldoPago")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Dia1 y Fecha1
                Dia1 = Dia1 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                'Dia2 y Fecha2
                Dia2 = Dia2 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                'Dia3 y Fecha3
                Dia3 = Dia3 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                'Dia4 y Fecha4
                Dia4 = Dia4 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                'Dia5 y Fecha5
                Dia5 = Dia5 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                'Dia6 y Fecha6
                Dia6 = Dia6 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DocumentoPagado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDocumentoPagado" & Where & "" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDocumentoPagado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDocumentoPagado"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDocumentoPagado")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoSaldoPagoResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal WhereDetalle As String, ByVal Fecha As Date, ByVal Moneda As String, ByVal OrdernadoPor As String, ByVal OrderBy As String, ByVal Top As String, ByVal Orden As Boolean, SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldosPago " & Where, "", 120)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            dt.TableName = "SpViewResumenMovimientosSaldosPago;1"
            CData.OrderDataTable(dt, OrderBy, Orden)

            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoSaldoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Parametros
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim vFecha As ParameterField = New ParameterField()
                Dim vFechaValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                vFecha.ParameterFieldName = "Fecha"
                vFechaValue.Value = Fecha.ToShortDateString
                vFecha.CurrentValues.Add(vFechaValue)

                Parametros.Add(vFecha)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoSaldoPagoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal WhereDetalle As String, ByVal Fecha1 As Date, ByVal Fecha2 As Date, ByVal Moneda As String, ByVal OrdernadoPor As String, ByVal OrderBy As String, ByVal Top As String, ByVal Orden As Boolean, SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldosPagoDetalle " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, Orden)

            Try

                dt.TableName = "SpViewResumenMovimientosSaldosPagoDetalle;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoSaldoProveedorDetalle2")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Parametros
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim vFechaDesde As ParameterField = New ParameterField()
                Dim vFechaDesdeValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Dim vFechaHasta As ParameterField = New ParameterField()
                Dim vFechaHastaValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                vFechaDesde.ParameterFieldName = "@Fecha1"
                vFechaDesdeValue.Value = Now.Date
                vFechaDesde.CurrentValues.Add(vFechaDesdeValue)

                vFechaHasta.ParameterFieldName = "@Fecha2"
                vFechaHastaValue.Value = Now.Date
                vFechaHasta.CurrentValues.Add(vFechaHastaValue)

                Parametros.Add(vFechaHasta)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteFondoFijo
        Inherits CReporte

        Sub Rendicion(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal Responsable As String, ByVal Reponer As Integer, ByVal Saldo As Integer, ByVal Tope As Integer, ByVal Fecha As Date)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                conn.Open()
                dt.TableName = "VDetalleFondoFijo"
                DSDatos.Tables.Add(dt)
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComprobanteReponer")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Reponer
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtReponer"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Reponer)

                'Saldo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSaldo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Saldo)

                'Tope
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTope"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Tope)


                'Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub RendicionContabilidad(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal Responsable As String, ByVal Reponer As Integer, ByVal Saldo As Integer, ByVal Tope As Integer, ByVal Fecha As Date, ByVal CuentaCredito As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                conn.Open()
                dt.TableName = "VDetalleFondoFijoContabilidad"
                DSDatos.Tables.Add(dt)
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComprobanteReponerContabilidad")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Reponer
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtReponer"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Reponer)

                'Saldo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSaldo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Saldo)

                'Tope
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTope"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Tope)


                'Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha

                'CuentaCredito
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtCuentaCredito"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CuentaCredito

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ListadoVales(ByVal frmReporte As frmReporte, ByVal Titulo As String, TipoInforme As String, ByVal Where As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VVale " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVale")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVale"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVales")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub Vale(ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VVale Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVale")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVale"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInformeVales")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoValesGasto(ByVal frmReporte As frmReporte, ByVal Titulo As String, TipoInforme As String, ByVal Where As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From vValeGasto1 " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vValeGasto1")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vValeGasto1"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoValesGasto")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteBanco
        Inherits CReporte

        Sub ExtractoMovimientoBancario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal WhereDetalle As String)

            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewExtractoMovimientoBancario2 " & Where,, 6000)
            dt.TableName = "SpViewExtractoMovimientoBancario2;1"
            'Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * FRom VExtractoMovimientoBancario ")
            'dt.TableName = "VExtractoMovimientoBancario"
            'dt = CData.FiltrarDataTable(dt, WhereDetalle)
            ' CData.OrderDataTable(dt, Ordenado, Asc)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoBancario")
                'LoadReport(Report, "rptDecimal")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub CuentaBancariaSaldoDiario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal WhereDetalle As String)

            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewSaldoBancarioDiario " & Where, VGCadenaConexion, 0)
            dt.TableName = "SpViewSaldoBancarioDiario;1"
            'Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * FRom VExtractoMovimientoBancario ")
            'dt.TableName = "VExtractoMovimientoBancario"
            'dt = CData.FiltrarDataTable(dt, WhereDetalle)
            ' CData.OrderDataTable(dt, Ordenado, Asc)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCuentaBancariaSaldoDiario")
                'LoadReport(Report, "rptDecimal")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub CuentaBancariaSaldoDiarioDetallado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal WhereDetalle As String)


            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VSaldoBancoDiario " & Where, conn)


            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 2000

                DSDatos.Tables.Add("VSaldoBancoDiario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VSaldoBancoDiario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCuentaBancariaSaldoDiarioDetallado")
                Report.SetDataSource(DSDatos)


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub SaldoBanco()

            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewSaldoBancario")
            dt.TableName = "SpViewSaldoBancario;1"

            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptSaldoBanco")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDepositoBancario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDepositoBancario " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDepositoBancario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDepositoBancario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDepositoBancario")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDepositoBancarioDetalle(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDepositoBancarioDetalle " & Where & " Order By Fecha Asc " & OrderBy, conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDepositoBancarioDetalle " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDepositoBancarioDetalle")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDepositoBancarioDetalle"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDepositoBancarioDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPrestamoBancarioDetalle(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal TipoInforme As String, ByVal Informe As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetallePrestamoBancario " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetallePrestamoBancario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetallePrestamoBancario"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                If Informe = "Resumido" Then
                    LoadReport(Report, "rptPrestamoBancarioResumido")
                End If

                If Informe = "Detallado" Then
                    LoadReport(Report, "rptPrestamoBancarioDetallado")
                End If

                If Informe = "Cuotas" Then
                    LoadReport(Report, "rptVencimientoCuotasPrestamoBancario")
                End If

                If Informe = "DeudaTotal" Then
                    LoadReport(Report, "rptPrestamosBancarioDeudaTotalPeriodo")
                End If
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ListadoDebitoCreditoBancario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String, ByVal InformeContable As Boolean, Optional DesdeTransaccion As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDebitoCreditoBancario " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * from vDetalleAsiento" & Where, conn)
            Dim aTable3 As New SqlDataAdapter("Select * from vDetalleAsiento" & Where, conn)



            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDebitoCreditoBancario")
                DSDatos.Tables.Add("vDetalleAsiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDebitoCreditoBancario"))


                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If InformeContable = False Then
                    LoadReport(Report, "rptListadoDebitoCreditoBancario")
                Else
                    LoadReport(Report, "rptListadoDebitoCreditoBancarioCC")
                    If DesdeTransaccion = True Then
                        aTable3.Fill(DSDatos.Tables("vDetalleAsiento"))
                    Else
                        aTable2.Fill(DSDatos.Tables("vDetalleAsiento"))
                    End If
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CCliente
        Inherits CReporte

        Sub ExtractoMovimientoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Todos As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewExtractoMovimientoCliente " & Where, "", 0)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                dt.TableName = "SpViewExtractoMovimientoCliente;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If Todos = False Then
                    LoadReport(Report, "rptExtractoMovimientoCliente")
                Else
                    LoadReport(Report, "rptExtractoMovimientoClienteTodos")
                End If
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub Tarjetas(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From vFormaPagoTarjeta " & Where & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vFormaPagoTarjeta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vFormaPagoTarjeta"))


                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptTarjetas")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)
                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub AcreditacionBancaria(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From vFormaPagoDocumento " & Where & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vFormaPagoDocumento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vFormaPagoDocumento"))


                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAcreditacionBancaria")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)
                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub FormaPagoCobranzaDepositoRechazdo(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From vFormaPagoCobranzadeposito " & Where & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vFormaPagoCobranzadeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vFormaPagoCobranzadeposito"))


                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptFormaPagoCobranzaDeposito")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)
                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ListarCatastro(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)
            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("select * from VCliente " & Where)
            dt.TableName = "VCliente"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCatastroCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListadoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCliente " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCliente")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Fecha Inicio
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Orden

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoAniversarioCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VClienteFechaFestiva " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VClienteFechaFestiva")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VClienteFechaFestiva"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoAniversarioCliente")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub AuditoriaClientes(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VaCliente " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VaCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VaCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAuditoriaClientes")
                Report.SetDataSource(DSDatos)

                ' ''Informacion del Sistema
                'Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                'txtField = Nothing

                ''Fecha Inicio
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Orden

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub AuditoriaDocumentos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From vAuditoriaDocumentos " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vAuditoriaDocumentos")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("vAuditoriaDocumentos"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAuditoriaDocumentos")
                Report.SetDataSource(DSDatos)

                ' ''Informacion del Sistema
                'Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                'txtField = Nothing

                ''Fecha Inicio
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Orden

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoClienteNoCompra(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCliente " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                Debug.Print(aTable1.SelectCommand.CommandText)
                aTable1.SelectCommand.CommandTimeout = 200
                DSDatos.Tables.Add("VCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoClienteNoCompra")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoClienteNoCompraProducto(ByVal frmReporte As frmReporte, ByVal SQL As String, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter(SQL & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                Debug.Print(aTable1.SelectCommand.CommandText)
                aTable1.SelectCommand.CommandTimeout = 200
                DSDatos.Tables.Add("VCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoClienteNoCompra")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CInformeFinanciero
        Inherits CReporte
        Sub InformeFinancieroIngresos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute  SpViewInformeFinanciero " & " " & Where)
            dt = CData.FiltrarDataTable(dt, "")

            Try

                dt.TableName = "SpViewInformeFinanciero;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInformeFinancieroIngresos")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
        Sub InformeControlTesoreria(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereSR As String, ByVal WhereFiltro As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            'Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute  SpViewInformeControlTesoreria " & " " & Where,, 300)
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute  SpViewInformeControlTesoreria1 " & " " & Where,, 300)
            dt = CData.FiltrarDataTable(dt, WhereFiltro)
            'Dim conn As New SqlConnection(VGCadenaConexion)
            'Dim aTable1 As New SqlDataAdapter("Select  * From VResumenCobranzaDiariaFormaPago2 where Fecha =" & WhereSR, conn)

            Try
                'Debug.Print(aTable1.SelectCommand.CommandText)
                'DSDatos.Tables.Add("VResumenCobranzaDiariaFormaPago2")
                'conn.Open()
                'aTable1.Fill(DSDatos.Tables("VResumenCobranzaDiariaFormaPago2"))
                'conn.Close()

                dt.TableName = "SpViewInformeControlTesoreria"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInformeControlTesoreria")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                'MostrarReporte(Report, Titulo,, DSDatos)
                MostrarReporte(Report, Titulo)
                'aTable1.Dispose()
            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
    End Class

    Public Class CProveedor
        Inherits CReporte

        Sub ExtractoMovimientoProveedor(ByVal frmReporte As frmReporte, ByVal SP As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute " & SP & " " & Where)
            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    dt = CData.FiltrarDataTable(dt, WhereDetalle)
                End If
            End If


            Try

                dt.TableName = "SpViewExtractoMovimientoProveedor;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ExtractoMovimientoDocumentoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Select * from vExtractoMovimientoProveedorFactura " & Where & " Order by Fecha")
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            Try

                dt.TableName = "vExtractoMovimientoProveedorFactura"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoGastoCompra")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
        Sub ExtractoMovimientoProveedorMoneda(ByVal frmReporte As frmReporte, ByVal SP As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute " & SP & " " & Where,, 600)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            Try

                dt.TableName = "SpViewExtractoMovimientoProveedorMonedas;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoProveedorMoneda")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ExtractoMovimientoProveedorMonedaTodos(ByVal frmReporte As frmReporte, ByVal SP As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute " & SP & " " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            Try

                dt.TableName = "SpViewExtractoMovimientoProveedorMonedas;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoProveedorMonedaTodos")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ExtractoMovimientoProveedorMonedaTodosCC(ByVal frmReporte As frmReporte, ByVal SP As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute " & SP & " " & Where,, 600)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            Try

                dt.TableName = "SpViewExtractoMovimientoProveedorMonedasCC;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoProveedorMonedaTodosCC")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListadoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VProveedor " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProveedor")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProveedor"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub RankingProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VEgreso " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VEgreso")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VEgreso"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptRankingProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub RankingProveedorProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String, ByVal Tipo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VCompra " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleCompra " & WhereDetalle & " " & OrderBy, conn)


            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCompra")
                DSDatos.Tables.Add("VDetalleCompra")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCompra"))
                aTable2.Fill(DSDatos.Tables("VDetalleCompra"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Tipo)
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()
            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteAsiento
        Inherits CReporte

        Sub AsientoDiario(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal Responsable As String, ByVal VerDiferencia As Boolean, ByVal GastoContadoRes As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If GastoContadoRes = True Then
                    LoadReport(Report, "rptAsientoDiarioResumido")
                Else
                    LoadReport(Report, "rptAsientoDiario")
                End If


                Report.SetDataSource(DSDatos)

                'Parametros
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim Diferencia As ParameterField = New ParameterField()
                Dim DiferenciaValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Diferencia.ParameterFieldName = "Diferencia"
                DiferenciaValue.Value = VerDiferencia
                Diferencia.CurrentValues.Add(DiferenciaValue)

                Parametros.Add(Diferencia)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub AsientoDiarioAgrupado(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal Responsable As String, ByVal VerDiferencia As Boolean, ByVal GastoContadoRes As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If GastoContadoRes = True Then
                    LoadReport(Report, "rptAsientoDiarioResumido")
                Else
                    LoadReport(Report, "rptAsientoDiario")
                End If


                Report.SetDataSource(DSDatos)

                'Parametros
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim Diferencia As ParameterField = New ParameterField()
                Dim DiferenciaValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Diferencia.ParameterFieldName = "Diferencia"
                DiferenciaValue.Value = VerDiferencia
                Diferencia.CurrentValues.Add(DiferenciaValue)

                Parametros.Add(Diferencia)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DocumentoSinAsiento(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Titulo As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VDocumentosSinAsientos " & Where & " " & OrderBy, conn)
            aTable1.SelectCommand.CommandTimeout = 500

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDocumentosSinAsientos")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDocumentosSinAsientos"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDocumentoSinAsiento")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteDiario
        Inherits CReporte
        Sub LibroDiario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Subtitulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal PImprimirCabecera As Boolean, Optional ByVal FormatoImpresion As Boolean = False, Optional ByVal NumeroInicial As Integer = 0, Optional ByVal AgruparPorOperacion As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VLibroDiario " & Where & " AND Resumido='False' " & OrderBy, conn)

            aTable1.SelectCommand.CommandTimeout = 500

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleAsientoContable")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleAsientoContable"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If FormatoImpresion = False Then
                    If AgruparPorOperacion Then
                        LoadReport(Report, "rptLibroDiarioAgrupado")
                    Else
                        LoadReport(Report, "rptLibroDiario1")
                    End If

                Else
                    LoadReport(Report, "rptLibroDiarioImpresion")
                End If
                Report.SetDataSource(DSDatos)

                EstablecerConexion(Report)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = PImprimirCabecera

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)

                If FormatoImpresion Then
                    Dim NumeroInicialField As ParameterField = New ParameterField()
                    Dim NumeroInicialValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                    NumeroInicialField.ParameterFieldName = "NumeroInicial"
                    NumeroInicialValue.Value = NumeroInicial

                    NumeroInicialField.CurrentValues.Add(NumeroInicialValue)

                    Parametros.Add(NumeroInicialField)
                End If

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub LibroDiarioCG(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Subtitulo As String, ByVal Responsable As String, ByVal PImprimirCabecera As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VLibroDiarioCG " & Where & " AND Resumido='False' ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleAsientoContable")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleAsientoContable"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptLibroDiarioCG")
                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = PImprimirCabecera

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReportePedido
        Inherits CReporte

        Sub ImprimirPedido(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef Impresora As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VPedido " & Where & "  ", conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPedido")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))

                conn.Close()
                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos

                'If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                '    MsgBox("El archivo no existe!")
                '    Exit Sub
                'End If

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpPedidos")
                Report.SetDataSource(DSDatos)

                'Dim Report As New ReportClass
                'Report.FileName = rptPedidos
                'Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True

                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
    End Class


    Public Class CReporteOrdenPago

        Inherits CReporte

        Sub ImprimirOrdenPagoDirecto(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal MontoLetra As String, Impresora As String, Optional ByVal SeccionFacturasJunta As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VOrdenPago Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable3 As New SqlDataAdapter("Select  * From VOrdenPagoEgreso Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable4 As New SqlDataAdapter("Select  * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable5 As New SqlDataAdapter("Select SUM(Importe)as Importe from VOrdenPagoEfectivo Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable6 As New SqlDataAdapter("Select * from vFormaPagoDocumento Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenPago")
                DSDatos.Tables.Add("VCheque")
                DSDatos.Tables.Add("VOrdenPagoEgreso")
                DSDatos.Tables.Add("VDetalleAsiento")
                DSDatos.Tables.Add("VOrdenPagoEfectivo")
                DSDatos.Tables.Add("vFormaPagoDocumento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPago"))
                aTable2.Fill(DSDatos.Tables("VCheque"))
                aTable3.Fill(DSDatos.Tables("VOrdenPagoEgreso"))
                aTable4.Fill(DSDatos.Tables("VDetalleAsiento"))
                aTable5.Fill(DSDatos.Tables("VOrdenPagoEfectivo"))
                aTable6.Fill(DSDatos.Tables("vFormaPagoDocumento"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If SeccionFacturasJunta Then
                    LoadReport(Report, "rptOrdenPagoPaginaJunta")
                Else
                    LoadReport(Report, "rptOrdenPago")
                End If
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Monto Letra
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = MontoLetra

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()
                aTable5.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirOrdenPagoConObservacion(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal MontoLetra As String, Impresora As String, Optional ByVal SeccionFacturasJunta As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VOrdenPago Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable3 As New SqlDataAdapter("Select  * From VOrdenPagoEgreso Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable4 As New SqlDataAdapter("Select  * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable5 As New SqlDataAdapter("Select SUM(Importe)as Importe from VOrdenPagoEfectivo Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable6 As New SqlDataAdapter("Select * from vFormaPagoDocumento Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenPago")
                DSDatos.Tables.Add("VCheque")
                DSDatos.Tables.Add("VOrdenPagoEgreso")
                DSDatos.Tables.Add("VDetalleAsiento")
                DSDatos.Tables.Add("VOrdenPagoEfectivo")
                DSDatos.Tables.Add("vFormaPagoDocumento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPago"))
                aTable2.Fill(DSDatos.Tables("VCheque"))
                aTable3.Fill(DSDatos.Tables("VOrdenPagoEgreso"))
                aTable4.Fill(DSDatos.Tables("VDetalleAsiento"))
                aTable5.Fill(DSDatos.Tables("VOrdenPagoEfectivo"))
                aTable6.Fill(DSDatos.Tables("vFormaPagoDocumento"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If SeccionFacturasJunta Then
                    LoadReport(Report, "rptOrdenPagoObservacionPaginaJunta")
                Else
                    LoadReport(Report, "rptOrdenPagoObservacion")
                End If
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Monto Letra
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = MontoLetra

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()
                aTable5.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirOrdenPago(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal MontoLetra As String, Optional ByVal SeccionFacturasJunta As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VOrdenPago Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable3 As New SqlDataAdapter("Select  * From VOrdenPagoEgreso Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable4 As New SqlDataAdapter("Select  * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable5 As New SqlDataAdapter("Select SUM(Importe)as Importe from VOrdenPagoEfectivo Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable6 As New SqlDataAdapter("Select * from vFormaPagoDocumento Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenPago")
                DSDatos.Tables.Add("VCheque")
                DSDatos.Tables.Add("VOrdenPagoEgreso")
                DSDatos.Tables.Add("VDetalleAsiento")
                DSDatos.Tables.Add("VOrdenPagoEfectivo")
                DSDatos.Tables.Add("vFormaPagoDocumento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPago"))
                aTable2.Fill(DSDatos.Tables("VCheque"))
                aTable3.Fill(DSDatos.Tables("VOrdenPagoEgreso"))
                aTable4.Fill(DSDatos.Tables("VDetalleAsiento"))
                aTable5.Fill(DSDatos.Tables("VOrdenPagoEfectivo"))
                aTable6.Fill(DSDatos.Tables("vFormaPagoDocumento"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If SeccionFacturasJunta Then
                    LoadReport(Report, "rptOrdenPagoPaginaJunta")
                Else
                    LoadReport(Report, "rptOrdenPago")
                End If

                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Monto Letra
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = MontoLetra

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()
                aTable5.Dispose()
                aTable6.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirOrdenPagoObservacion(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal MontoLetra As String, Optional ByVal SeccionFacturasJunta As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VOrdenPago Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable3 As New SqlDataAdapter("Select  * From VOrdenPagoEgreso Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable4 As New SqlDataAdapter("Select  * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable5 As New SqlDataAdapter("Select SUM(Importe)as Importe from VOrdenPagoEfectivo Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable6 As New SqlDataAdapter("Select * from vFormaPagoDocumento Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenPago")
                DSDatos.Tables.Add("VCheque")
                DSDatos.Tables.Add("VOrdenPagoEgreso")
                DSDatos.Tables.Add("VDetalleAsiento")
                DSDatos.Tables.Add("VOrdenPagoEfectivo")
                DSDatos.Tables.Add("vFormaPagoDocumento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPago"))
                aTable2.Fill(DSDatos.Tables("VCheque"))
                aTable3.Fill(DSDatos.Tables("VOrdenPagoEgreso"))
                aTable4.Fill(DSDatos.Tables("VDetalleAsiento"))
                aTable5.Fill(DSDatos.Tables("VOrdenPagoEfectivo"))
                aTable6.Fill(DSDatos.Tables("vFormaPagoDocumento"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If SeccionFacturasJunta Then
                    LoadReport(Report, "rptOrdenPagoObservacionPaginaJunta")
                Else
                    LoadReport(Report, "rptOrdenPagoObservacion")
                    'LoadReport(Report, "NuevaOP")
                End If

                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Monto Letra
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = MontoLetra

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()
                aTable5.Dispose()
                aTable6.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirRetencionIVA(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal IDTransaccionOrdenPago As Integer, ByVal Responsable As String, ByVal MontoLetra As String, ByRef PathReporte As String, ByVal Impresora As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VRetencionIVA Where IDTransaccion=" & IDTransaccion, conn)
            'Dim aTable2 As New SqlDataAdapter("Select  * From VComprobanteRetencion Where IDTransaccion=" & IDTransaccion, conn)
            '08-06-2021 Agregado por SC para cambiar forma de proceso de RETENCION, obliga a asociar al OP
            Dim aTable2 As New SqlDataAdapter("Select  * From VComprobanteRetencionIVA Where IDTransaccion=" & IDTransaccion, conn)

            'If aTable2. = False Then

            'End If

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRetencionIVA")
                'DSDatos.Tables.Add("VComprobanteRetencion")
                '08-06-2021 Agregado por SC para cambiar forma de proceso de RETENCION, obliga a asociar al OP
                DSDatos.Tables.Add("VComprobanteRetencionIVA")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRetencionIVA"))
                'aTable2.Fill(DSDatos.Tables("VComprobanteRetencion"))
                '08-06-2021 Agregado por SC para cambiar forma de proceso de RETENCION, obliga a asociar al OP
                DSDatos.Tables.Add("VComprobanteRetencionIVA")
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = MontoLetra


                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)


                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoOrdenPago(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VOrdenPagoSaldo " & Where & "  " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                aTable1.SelectCommand.CommandTimeout = 600
                DSDatos.Tables.Add("VOrdenPagoSaldo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPagoSaldo"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoOrdenPago")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoEgresosAPagar(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, dt As DataTable)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                dt.TableName = "SpViewEgresosParaOrdenPago;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoEgresosAPagar")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteContabilidad
        Inherits CReporte

        Sub ControlIntegridad(ByVal frmReporte As frmReporte, ByVal Responsable As String, ByVal SubTitulo As String, ByVal CuentaContable As String, ByVal Año As Integer, ByVal Mes As Integer)
            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet

            Dim dt As DataTable = CSistema.ExecuteToDataTable("Exec SpMovimientosOperativosContraContables @vCodigo ='" & CuentaContable & "', @vMes = " & Mes & ", @vAño = " & Año,, 5000)

            dt.TableName = "SpMovimientosOperativosContraContables;1"

            DSDatos.Tables.Add(dt)


            Try
                Dim Report As New ReportClass

                LoadReport(Report, "rptComparativoContabilidadMovimientoStock")

                Report.SetDataSource(DSDatos)



                'Informacion de Software
                EstablecerTitulos(Report, "INFORME DE INTEGRIDAD - CUENTAS DE EXISTENCIA", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "INFORME DE INTEGRIDAD - CUENTAS DE EXISTENCIA",, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub


        Sub ImprimirPlanCuenta(ByVal frmReporte As frmReporte, ByVal Responsable As String, ByVal IDPlanCuenta As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VCuentaContableReporte Where IDPlanCuenta=" & IDPlanCuenta & " Order By Codigo", conn)

            Try

                DSDatos.Tables.Add("VCuentaContable")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCuentaContable"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanCuenta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "",, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroIVA(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal Resumido As Boolean = False, Optional ByVal PImprimirCabecera As Boolean = True, Optional ByVal NumeroInicial As Integer = 0)

            Dim SQL As String = ""

            If Resumido = False Then
                SQL = "Select " & Top & " * From VLibroIVA " & WhereDetalle & Where & " " & OrderBy
            End If

            If Resumido = True Then
                SQL = "Select " & Top & " * From VLibroIVA " & WhereDetalle & Where & " " & OrderBy
            End If

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter(SQL, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLibroIVA")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLibroIVA"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If NumeroInicial > 0 Then
                    LoadReport(Report, "rptLibroIVAImpresion")
                Else
                    If Resumido = False Then
                        LoadReport(Report, "rptLibroIVA3")
                    Else
                        LoadReport(Report, "rptLibroIVAAgrupado")
                    End If

                End If

                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = PImprimirCabecera

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)


                If NumeroInicial > 0 Then
                    Dim PNumeroInicial As ParameterField = New ParameterField()
                    Dim PNumeroInicialValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                    PNumeroInicial.ParameterFieldName = "NumeroInicial"
                    PNumeroInicialValue.Value = NumeroInicial

                    PNumeroInicial.CurrentValues.Add(PNumeroInicialValue)
                    Parametros.Add(PNumeroInicial)
                End If




                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroMayor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Resumido As Boolean, Optional ByVal PImprimirCabecera As Boolean = True, Optional ByVal Contabilidad As Boolean = False, Optional ByVal Vertical As Boolean = True, Optional ByVal NumeroInicial As Integer = 0)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim CG As String = ""
            If Contabilidad = True Then
                CG = "CG"
            End If
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayor" & CG & " " & Where, "", 5000)

            dt.TableName = "SpViewLibroMayor;1"

            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            DSDatos.Tables.Add(dt)

            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If Resumido = False Then
                    If Vertical = True Then
                        If NumeroInicial > 0 Then
                            LoadReport(Report, "rptLibroMayorImpresion")
                        Else
                            LoadReport(Report, "rptLibroMayorVertical")
                        End If

                    Else
                        LoadReport(Report, "rptLibroMayor")
                    End If
                Else
                    LoadReport(Report, "rptLibroMayorResumido")
                End If

                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = PImprimirCabecera

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)
                If NumeroInicial > 0 Then

                    Dim NumeroInicialField As ParameterField = New ParameterField()
                    Dim NumeroInicialValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                    NumeroInicialField.ParameterFieldName = "NumeroInicial"
                    NumeroInicialValue.Value = NumeroInicial

                    NumeroInicialField.CurrentValues.Add(NumeroInicialValue)

                    Parametros.Add(NumeroInicialField)
                End If


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroMayorDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim CG As String = ""

            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayorDetalle" & CG & " " & Where, "", 5000)

            dt.TableName = "SpViewLibroMayorDetalle;1"

            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            DSDatos.Tables.Add(dt)

            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass

                LoadReport(Report, "rptLibroMayorDetalle")

                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = True

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroMayorUNCC(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Resumido As Boolean, Optional ByVal PImprimirCabecera As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibromayorCC" & Where, "", 5000)

            dt.TableName = "SpViewLibromayorCC;1"

            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            DSDatos.Tables.Add(dt)

            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptLibroMayorCC")

                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = PImprimirCabecera

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroMayorDetalles(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Resumido As Boolean, Optional ByVal PImprimirCabecera As Boolean = True, Optional ByVal Contabilidad As Boolean = False, Optional ByVal Vertical As Boolean = True, Optional MasDetalles As Boolean = False, Optional VerMonedaCotizacion As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim CG As String = ""
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayor" & CG & " " & Where, "", 5000)

            dt.TableName = "SpViewLibroMayor;1"

            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            DSDatos.Tables.Add(dt)

            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If VerMonedaCotizacion = False Then
                    LoadReport(Report, "rptLibroMayorDetallado")
                Else
                    LoadReport(Report, "rptLibroMayorDetalladoMoneda")
                End If
                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = PImprimirCabecera

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        'Sub ImprimirLibroMayorSucursal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal IDSucursal As Integer, ByVal Resumido As Boolean, Optional ByVal PImprimirCabecera As Boolean = True, Optional ByVal Contabilidad As Boolean = False, Optional ByVal NumeroInicial As Integer = 0)

        '    ' Creamos los componenetes para obtener los datos.
        '    Dim DSDatos As New DataSet
        '    Dim CG As String = ""
        '    If Contabilidad = True Then
        '        CG = "CG"
        '    End If

        '    Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayorSucursal" & CG & " " & Where & "," & IDSucursal, "", 5000)
        '    dt = CData.FiltrarDataTable(dt, WhereDetalle)

        '    ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
        '    Try

        '        dt.TableName = "SpViewLibroMayor;1"

        '        DSDatos.Tables.Add(dt)

        '        ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
        '        Dim Report As New ReportClass
        '        If Resumido = False Then
        '            If NumeroInicial > 0 Then
        '                LoadReport(Report, "rptLibroMayorImpresion")
        '            Else
        '                LoadReport(Report, "rptLibroMayor")
        '            End If

        '        Else
        '            LoadReport(Report, "rptLibroMayorResumido")
        '        End If
        '        Report.SetDataSource(DSDatos)

        '        'Parametro para definir si el informe EsCantidad o importe
        '        Dim Parametros As ParameterFields = New ParameterFields()
        '        Dim ImprimirCabecera As ParameterField = New ParameterField()
        '        Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

        '        ImprimirCabecera.ParameterFieldName = "PrintCabecera"
        '        ImprimirCabeceraValue.Value = PImprimirCabecera

        '        ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

        '        Parametros.Add(ImprimirCabecera)


        '        Dim NumeroInicialField As ParameterField = New ParameterField()
        '        Dim NumeroInicialValue As ParameterDiscreteValue = New ParameterDiscreteValue()

        '        NumeroInicialField.ParameterFieldName = "NumeroInicial"
        '        NumeroInicialValue.Value = NumeroInicial

        '        NumeroInicialField.CurrentValues.Add(NumeroInicialValue)

        '        Parametros.Add(NumeroInicialField)


        '        'Informacion de Software
        '        EstablecerTitulos(Report, Titulo, TipoInforme)

        '        'Establecer conexion
        '        EstablecerConexion(Report)

        '        'Mostrar Informe
        '        MostrarReporte(Report, Titulo, Parametros, DSDatos)

        '    Catch ex As Exception
        '        MsgBox("Mensaje : " & ex.Message)
        '    End Try

        'End Sub

        Sub ImprimirLibroMayorSucursal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal IDSucursal As Integer, ByVal Resumido As Boolean, Optional ByVal PImprimirCabecera As Boolean = True, Optional ByVal Contabilidad As Boolean = False, Optional ByVal NumeroInicial As Integer = 0)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim CG As String = ""
            If Contabilidad = True Then
                CG = "CG"
            End If

            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayorSucursal" & CG & " " & Where & "," & IDSucursal, "", 5000)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                dt.TableName = "SpViewLibroMayor;1"

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If Resumido = False Then
                    If NumeroInicial > 0 Then
                        LoadReport(Report, "rptLibroMayorImpresion")
                    Else
                        LoadReport(Report, "rptLibroMayor")
                    End If

                Else
                    LoadReport(Report, "rptLibroMayorResumido")
                End If
                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = PImprimirCabecera

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceGeneral(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String, Optional ByVal vMostrarCodigo As Boolean = True, Optional ByVal Multicolumna As Boolean = False)

            Try

                Dim Report As New ReportClass
                If Multicolumna = False Then
                    'If FormatoImpresion Then
                    '    LoadReport(Report, "rptBalanceGeneralImpresion")
                    'Else
                    '    LoadReport(Report, "rptBalanceGeneral")
                    'End If
                    'Comentado para prueba de ordenamiento 
                    LoadReport(Report, "rptBalanceGeneral")
                    'LoadReport(Report, "rptBalanceGeneralPrueba")
                Else
                    LoadReport(Report, "rptEstadoResultadoMulticolumna")
                End If

                Report.SetDataSource(dt)
                Dim ds As New DataSet
                ds.Tables.Add(dt)


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim MostrarCodigo As ParameterField = New ParameterField()
                Dim MostrarCodigoValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                MostrarCodigo.ParameterFieldName = "MostrarCodigo"
                MostrarCodigoValue.Value = vMostrarCodigo

                MostrarCodigo.CurrentValues.Add(MostrarCodigoValue)

                Parametros.Add(MostrarCodigo)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, ds)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ImprimirBalanceGeneralyEstadoResultado(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String, Optional ByVal vMostrarCodigo As Boolean = True, Optional ByVal Multicolumna As Boolean = False)

            Try

                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceGeneralyEstadoResultado")

                'Para pasar los totales sin sumar anulados
                Dim TotalActivo As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo=1")
                Dim TotalPasivo As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='2'")
                Dim TotalPNeto As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='31'")
                Dim TotalGeneral As Decimal = TotalActivo - TotalPasivo - TotalPNeto
                Dim TotalIngreso As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='4'")
                Dim TotalEgreso As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='5'")
                Dim ResultadoEjercicio As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='310502'")
                Dim TotalDiferencia As Decimal = TotalIngreso - TotalEgreso

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalActivo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalActivo.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalPasivo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalPasivo.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalPNeto"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalPNeto.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalGeneral"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalGeneral.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalIngreso"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalIngreso.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalEgreso"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalEgreso.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDiferencia"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDiferencia.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtResultadoEjercicio"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(ResultadoEjercicio.ToString, False)

                Report.SetDataSource(dt)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim MostrarCodigo As ParameterField = New ParameterField()
                Dim MostrarCodigoValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                MostrarCodigo.ParameterFieldName = "MostrarCodigo"
                MostrarCodigoValue.Value = vMostrarCodigo

                MostrarCodigo.CurrentValues.Add(MostrarCodigoValue)

                Parametros.Add(MostrarCodigo)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros)


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        'Agregado el 22/02/2023
        Sub ImprimirBalanceRG49(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String, Optional ByVal vMostrarCodigo As Boolean = True, Optional ByVal Multicolumna As Boolean = False)

            Try

                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceRG49")

                'Para pasar los totales sin sumar anulados
                Dim TotalActivo As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo=1")
                Dim TotalPasivo As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='2'")
                Dim TotalPNeto As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='31'")
                Dim TotalGeneral As Decimal = TotalActivo - TotalPasivo - TotalPNeto
                Dim TotalIngreso As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='4'")
                Dim TotalEgreso As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='5'")
                Dim ResultadoEjercicio As Decimal = CSistema.dtSumColumn(dt, "SaldoAcumulado", "Codigo='310502'")
                Dim TotalDiferencia As Decimal = TotalIngreso - TotalEgreso

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalActivo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalActivo.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalPasivo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalPasivo.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalPNeto"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalPNeto.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalGeneral"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalGeneral.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalIngreso"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalIngreso.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalEgreso"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalEgreso.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDiferencia"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalDiferencia.ToString, False)

                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtResultadoEjercicio"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(ResultadoEjercicio.ToString, False)

                Report.SetDataSource(dt)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim MostrarCodigo As ParameterField = New ParameterField()
                Dim MostrarCodigoValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                MostrarCodigo.ParameterFieldName = "MostrarCodigo"
                MostrarCodigoValue.Value = vMostrarCodigo

                MostrarCodigo.CurrentValues.Add(MostrarCodigoValue)

                Parametros.Add(MostrarCodigo)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros)


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroInventario(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal NumeroInicial As Integer)

            Try

                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceGeneralImpresion")

                Report.SetDataSource(dt)
                Dim ds As New DataSet
                ds.Tables.Add(dt)


                'Informacion de Software
                EstablecerTitulos(Report, "LIBRO INVENTARIO", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim PNumeroInicial As ParameterField = New ParameterField()
                Dim PNumeroInicialValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Dim MostrarCodigo As ParameterField = New ParameterField()
                Dim MostrarCodigoValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                MostrarCodigo.ParameterFieldName = "MostrarCodigo"
                MostrarCodigoValue.Value = True

                MostrarCodigo.CurrentValues.Add(MostrarCodigoValue)

                Parametros.Add(MostrarCodigo)

                PNumeroInicial.ParameterFieldName = "NumeroInicial"
                PNumeroInicialValue.Value = NumeroInicial

                PNumeroInicial.CurrentValues.Add(PNumeroInicialValue)

                Parametros.Add(PNumeroInicial)

                'Mostrar Informe
                MostrarReporte(Report, "LIBRO INVENTARIO", Parametros, ds)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceComprobacion4(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceComprobacion4")
                Report.SetDataSource(dt)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceComprobacion3(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceComprobacion3")
                Report.SetDataSource(dt)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirComprobanteNoEmitido(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComprobantesNoEmitidos")
                Report.SetDataSource(dt)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CPedido
        Inherits CReporte
        Sub ListadoPedido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListaPedido")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPedidoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePedido " & WhereDetalle & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VPedido")
                DSDatos.Tables.Add("VDetallePedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                aTable2.Fill(DSDatos.Tables("VDetallePedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPedidoDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPedidoDetalleDescuento(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePedidoDescuentoDetalle " & WhereDetalle & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VPedido")
                DSDatos.Tables.Add("VDetallePedidoDescuentoDetalle")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                aTable2.Fill(DSDatos.Tables("VDetallePedidoDescuentoDetalle"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPedidoDetalleDescuento")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoTotalProductoPedido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePedido " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VPedido")
                DSDatos.Tables.Add("VDetallePedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                aTable2.Fill(DSDatos.Tables("VDetallePedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptTotalProductoPedido")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", Hasta)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                Dim titulo As String = "Listado de total de pedidos por Producto"
                MostrarReporte(Report, titulo,, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoTotalProductoPedidoDetalleDescuento(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Titulo As String, ByVal SubTitulo As String, ByVal TablaCabecera As String, ByVal TablaDetalle As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From " & TablaCabecera & " " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From " & TablaDetalle & " " & WhereDetalle, conn)

            Try
                aTable1.SelectCommand.CommandTimeout = 5000
                aTable2.SelectCommand.CommandTimeout = 5000
                DSDatos.Tables.Add("VPedido")
                DSDatos.Tables.Add("VDetallePedidoDescuentoDetalle")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                aTable2.Fill(DSDatos.Tables("VDetallePedidoDescuentoDetalle"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptTotalProductoPedidoDetalleDescuento")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                Titulo = "Listado de total de pedidos por Producto"
                MostrarReporte(Report, Titulo, , DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPedidoAsignacionCamion(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Where1 As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePedido " & Where1 & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VPedido")
                DSDatos.Tables.Add("VDetallePedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                aTable2.Fill(DSDatos.Tables("VDetallePedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPedidosEntregaDirecta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ListadoAbastecimientoCamion(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Titulo As String, ByVal Subtitulo As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VOrdenDePedido " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleOrdenDePedido " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VOrdenDePedido")
                DSDatos.Tables.Add("VDetalleOrdenDePedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenDePedido"))
                aTable2.Fill(DSDatos.Tables("VDetalleOrdenDePedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoAbastecimientoCamion")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoAbastecimientoDetallado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Titulo As String, ByVal Subtitulo As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VAsignacionCamion " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleAsignacionCamion " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VAsignacionCamion")
                DSDatos.Tables.Add("VDetalleAsignacionCamion")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VAsignacionCamion"))
                aTable2.Fill(DSDatos.Tables("VDetalleAsignacionCamion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoAbastecimientoDetallado")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, Subtitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPedidosAnticipados(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Todos As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewPedidoAnticipado " & Where, "", 0)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                dt.TableName = "SpViewPedidoAnticipado;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPedidosAnticipados")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CTeleventa
        Inherits CReporte
        Sub PlanDeLlamada(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal ParamOrder As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From vLlamada " & Where & " " & OrderBy, conn)
            Dim Titulo As String = "Plan de Llamadas DESDE "

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vLlamada")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("vLlamada"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpPlanDeLlamadas")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim Order As ParameterField = New ParameterField()
                Dim OrderValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Order.ParameterFieldName = "Order"
                OrderValue.Value = ParamOrder

                Order.CurrentValues.Add(OrderValue)

                Parametros.Add(Order)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Parametros)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CReporteNotaCreditoProveedor
        Inherits CReporte

        Sub ImprimirNotaCreditoProveedor(ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Top As String)

            Dim frmReporte As New frmReporte

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCreditoProveedor " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCreditoProveedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCreditoProveedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaCreditoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteNotaDebitoProveedor
        Inherits CReporte

        Sub ImprimirNotaDebitoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebitoProveedor " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebitoProveedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebitoProveedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebitoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCobranza
        Inherits CReporte

        Sub ListadoCobranzaCredito(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCobranzaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCobranzaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCobranzaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCobranzaCredito")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ' ''Desde
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Desde

                ' ''Hasta
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoAnticipoCliente(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal SoloConSaldo As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCobranzaCredito " & Where & " and AnticipoCliente = 'True' " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select " & Top & " * From VDetalleDocumentosCobrados " & WhereDetalle & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCobranzaCredito")
                DSDatos.Tables.Add("VDetalleDocumentosCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCobranzaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetalleDocumentosCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If SoloConSaldo Then
                    LoadReport(Report, "rptAnticipoClientesConSaldo")
                Else
                    LoadReport(Report, "rptAnticipoClientes")
                End If

                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ' ''Desde
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Desde

                ' ''Hasta
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Hasta

                'Informacion de Software
                Titulo = "Listado de Anticipos de Clientes"
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCobranzaContado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCobranzaContado " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCobranzaContado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCobranzaContado"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCobranzaContado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ListadoCobranzaContadoDetallado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From vVentaDetalleCobranzaContado " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vVentaDetalleCobranzaContado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vVentaDetalleCobranzaContado"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCobranzaContadoDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleDocumentosCobrados(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleDocumentosCobrados " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                'SC: 23-06-2022 Se modifica para extraer el reporte en un maximo de 200 segundos
                aTable1.SelectCommand.CommandTimeout = 200
                DSDatos.Tables.Add("VDetalleDocumentosCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDocumentosCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleDocumentoCobrado")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleDocumentosCobradosVendedor(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleDocumentosCobrados " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDocumentosCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDocumentosCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleDocumentoCobradoVendedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleDocumentosCobradosFecha(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleDocumentosCobrados " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDocumentosCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDocumentosCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleDocumentoCobradoFecha")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenCobranzaDiaria(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal WhereDetalle As String, Optional ByVal ASC As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt2 As DataTable = CSistema.ExecuteToDataTable("Select Fecha, Moneda, TipoComprobante, 'Cantidad'=SUM(Cantidad), 'Valores'=SUM(Valores) From VResumenCobranzaDiariaFormaPago2 " & Where & " Group By Fecha, Moneda, TipoComprobante")


            Try

                dt2.TableName = "VResumenCobranzaDiariaFormaPago2"

                DSDatos.Tables.Add(dt2)


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumenCobranzaDiaria2")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComprobanteCobranzaPeriodo(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal WhereDetalle As String, Optional ByVal ASC As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt2 As DataTable = CSistema.ExecuteToDataTable("Select Fecha, Moneda, TipoComprobante, 'Cantidad'=SUM(Cantidad), 'Valores'=SUM(Valores) From VResumenCobranzaDiariaFormaPago2 " & Where & " Group By Fecha, Moneda, TipoComprobante")


            Try

                dt2.TableName = "VResumenCobranzaDiariaFormaPago2"

                DSDatos.Tables.Add(dt2)


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumenCobranzaPeriodo")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleValoresCobrados(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleValoresCobrados " & Where & " " & OrderBy, conn)
            Try

                DSDatos.Tables.Add("VDetalleValoresCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleValoresCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleValoresCobrados")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleValoresCobradosPorComprobante(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleValoresCobrados " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDetalleValoresCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleValoresCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleValoresCobradosPorComprobante")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub AnticipoClientes(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable2 As New SqlDataAdapter("Select * From VAnticipoCliente where " & Where, conn)

            Try

                DSDatos.Tables.Add("VAnticipoCliente")

                conn.Open()
                aTable2.Fill(DSDatos.Tables("VAnticipoCliente"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAnticipoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenCobranzaContado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal dt As DataTable)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim Contador As Integer = 0


            For Each oRow As DataRow In dt.Rows
                Contador = Contador + 1
                If Contador = dt.Rows.Count Then
                    dt.Rows.Remove(oRow)
                    Exit For
                End If
            Next

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                conn.Open()
                dt.TableName = "VResumenCobranzaContado2"
                DSDatos.Tables.Add(dt)
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumenCobranzaContado")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                'aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoRetencionRecibida(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Where As String, ByVal OrderBy As String, ByRef PImprimirCabecera As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select * From VRetencionRecibida " & Where & " " & OrderBy, conn)
            Dim aTable1 As New SqlDataAdapter("select RR.* from CobranzaCredito CC JOIN VRetencionRecibida RR ON CC.IDTransaccion = RR.IDTransaccion " & Where & " " & OrderBy, conn)


            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRetencionRecibida")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRetencionRecibida"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoRetencionRecibida")
                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim ImprimirCabecera As ParameterField = New ParameterField()
                Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                ImprimirCabeceraValue.Value = PImprimirCabecera

                ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                Parametros.Add(ImprimirCabecera)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ListadoRetencionEmitida(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Where As String, ByVal OrderBy As String, ByRef PImprimirCabecera As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VRetencionIVA " & Where & " and anulado = 'False' " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRetencionIVA")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRetencionIVA"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptRetencionEmitida")
                Report.SetDataSource(DSDatos)

                ''Parametro para definir si el informe EsCantidad o importe
                'Dim Parametros As ParameterFields = New ParameterFields()
                'Dim ImprimirCabecera As ParameterField = New ParameterField()
                'Dim ImprimirCabeceraValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                'ImprimirCabecera.ParameterFieldName = "PrintCabecera"
                'ImprimirCabeceraValue.Value = PImprimirCabecera

                'ImprimirCabecera.CurrentValues.Add(ImprimirCabeceraValue)

                'Parametros.Add(ImprimirCabecera)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo,, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteGasto
        Inherits CReporte

        Sub ListadoGasto(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String, ByVal Tabla As String, ByRef InformeCC As Boolean, Optional FechaDesde As Date = Nothing, Optional ByVal FechaHasta As Date = Nothing)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From " & Tabla & " " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * from vDetalleAsiento where Fecha between '" & CSistema.FormatoFechaBaseDatos(FechaDesde, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(FechaHasta, True, False) & "'", conn)

            aTable1.SelectCommand.CommandTimeout = 600
            aTable2.SelectCommand.CommandTimeout = 600

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                DSDatos.Tables.Add("VGasto")
                DSDatos.Tables.Add("vDetalleAsiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VGasto"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If InformeCC = False Then
                    LoadReport(Report, "rptListadoGasto")
                Else
                    LoadReport(Report, "rptListadoGastoCC")
                    aTable2.Fill(DSDatos.Tables("vDetalleAsiento"))
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoGastoAcuerdoComercial(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String, ByVal Tabla As String, Optional FechaDesde As Date = Nothing, Optional ByVal FechaHasta As Date = Nothing)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From " & Tabla & " " & Where & " " & OrderBy, conn)

            aTable1.SelectCommand.CommandTimeout = 600

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                DSDatos.Tables.Add("VGastoAcuerdoComercialInforme")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VGastoAcuerdoComercialInforme"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoGastoAcuerdoComercial")

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteComisionVendedor
        Inherits CReporte

        Sub ListadoComision(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Titulo As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente, TipoProducto, IDTipoProducto, Producto, Vendedor, IDVendedor, CantidadVenta, CantidadDevuelta, ImporteVenta, ImporteDevuelto, Monto From VComisionVendedor" & Where & OrderBy, conn)


            Try

                DSDatos.Tables.Add("VComisionVendedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VComisionVendedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComisionVendedor")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoComisionDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoProducto,Cliente,ReferenciaCliente,Producto,Vendedor,IDVendedor,'CantidadVenta'=Sum(CantidadVenta),'CantidadDevuelta'=SUM(CantidadDevuelta),'ImporteVenta'=SUM(ImporteVenta),'ImporteDevuelto'=sum(ImporteDevuelto),'Monto'=sum(Monto)From VComisionVendedor" & Where & " Group By TipoProducto,Cliente,ReferenciaCliente,Producto,Vendedor,IDVendedor " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VComisionVendedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VComisionVendedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComisionVendedorDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteDescuentoTactico
        Inherits CReporte

        Sub ListadoProductoDetallado(ByVal Detallado As Boolean, ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleActividadDescuento Where IDTransaccion=" & IDTransaccion & " Order By FechaEmision ", conn)

            Try

                DSDatos.Tables.Add("VDetalleActividadDescuento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleActividadDescuento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaDescuentoTactico")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoProducto(ByVal Detallado As Boolean, ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select DA.* From VDetallePlanillaDescuentoTactico P Join VDetalleActividad DA On P.ID=DA.IDActividad Where IDTransaccion=" & IDTransaccion & " Order By Codigo, Producto ", conn)

            Try

                DSDatos.Tables.Add("VDetalleActividadDescuento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleActividadDescuento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaDescuentoTacticoListaProducto")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPosmorten(ByVal Detallado As Boolean, ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select Codigo , [Cod.] , Venta,ReferenciaCliente, Cliente, Referencia , Producto , Cantidad , [Desc %] , [Desc Uni] , Descuento, Total From VDetalleActividadDescuento Where IDTransaccion=" & IDTransaccion & "  Order by Codigo  ", conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleActividadDescuento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleActividadDescuento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaPosmorten")
                Report.SetDataSource(DSDatos)

                Dim SubTitulo As String = "LISTA DE PRODUCTOS"
                'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteConfiguracion
        Inherits CReporte


        Sub ImprimirCaja(ByRef frmReporte As frmReporte, ByRef Where As String, Optional ByRef OrderBy As String = "")

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleAsientoContableCaja"

            Dim aTable1 As New SqlDataAdapter("Select * From " & vTabla & " " & Where & " " & OrderBy, conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 200

                DSDatos.Tables.Add("VDetalleAsientoContableCaja")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleAsientoContableCaja"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleAsientoContableCaja")

                Dim temp As New DataTable
                temp = (DSDatos.Tables(vTabla))


                'Calcular total a reponer

                Dim TotalAReponer As Decimal = CSistema.dtSumColumn(temp, "Credito")

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalAReponer"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(TotalAReponer.ToString, False)

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "VERIFICACION DE CAJAS", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class
    Public Class CReporteRemision
        Inherits CReporte

        Sub Imprimir(ByRef frmReporte As frmReporte, ByRef IDTransaccion As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select Top(1) * From VRemision Where IDTransaccion=" & IDTransaccion & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleRemision Where IDTransaccion=" & IDTransaccion & " Order By ID", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRemision")
                DSDatos.Tables.Add("VDetalleRemision")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRemision"))
                aTable2.Fill(DSDatos.Tables("VDetalleRemision"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & vgImpresionRemisionPath) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & vgImpresionRemisionPath
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                Report.PrintOptions.PrinterName = vgImpresionFacturaImpresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Function Imprimir(dt As DataTable, dtDetalle As DataTable) As Boolean

            Imprimir = True

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)
                DSDatos.Tables.Add(dtDetalle)

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & vgImpresionRemisionPath) = False Then
                    If MessageBox.Show("El archivo de impresion no se encuentra! Desea continuar?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No Then
                        Return False
                    End If
                    Exit Function
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & vgImpresionRemisionPath
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                Report.PrintOptions.PrinterName = vgImpresionFacturaImpresora
                Report.PrintToPrinter(1, False, 1, 1)
                'MostrarReporte(Report, "")

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Function

        Sub ListadoRemision(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From vRemision " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vRemision")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vRemision"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoRemision")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
        Sub ListadpRemisionDetallada(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From vRemision " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From vDetalleRemision " & Where & " " & WhereDetalle, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vRemision")
                DSDatos.Tables.Add("vDetalleRemision")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vRemision"))
                aTable2.Fill(DSDatos.Tables("vDetalleRemision"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoRemisionDetallada")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteKardex
        Inherits CReporte

        Sub ListadoKardex(dt As DataTable, Producto As String, fecha As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Try

                dt.TableName = "VKardex"
                DSDatos.Tables.Add(dt.Copy)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptKardex")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "PLANILLA DE COSTOS KARDEX", Producto & " - " & fecha)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

End Namespace

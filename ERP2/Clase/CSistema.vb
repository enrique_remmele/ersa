﻿Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports System.Management
Imports Scripting
Imports System.IO
Imports Spire.Xls
Imports System.Text.RegularExpressions
Imports File = Scripting.File


Public Class CSistema
    Inherits CError


#Region "PROPIEDADES"

#End Region

#Region "ESTRUCTURAS, TIPOS DE DATOS, ENUMERADORES"

    ''' <summary>
    ''' Enumeracion para definir el comportamiento de los botonos ABM
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMHabilitacionBotonesABM

        INICIO = 0
        NUEVO = 1
        EDITAR = 2
        CANCELAR = 3
        GUARDAR = 4
        ELIMINAR = 5
        EDITANDO = 6

    End Enum

    ''' <summary>
    ''' Enumeracion para definir el comportamiento de los botonos de REGISTROS
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMHabilitacionBotonesRegistros

        INICIO = 0
        NUEVO = 1
        GUARDAR = 2
        CANCELAR = 3
        ANULAR_ELIMINAR = 4
        IMPRIMIR = 6
        BUSQUEDA = 7
        MODIFICAR = 10

    End Enum

    ''' <summary>
    ''' Enumeracion para definir las operaciones posibles en la base de datos Insertar, Actualizar o Eliminar
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMOperacionesABM

        INS = 1
        UPD = 2
        DEL = 3

    End Enum

    ''' <summary>
    ''' Enumeracion para definir las operaciones posibles en la base de datos Agregar, Anular, Eliminar
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMOperacionesRegistro

        INS = 1
        DEL = 3
        ANULAR = 4
        UPD = 2

        DEL1 = 5 'JGR 20140820 Agregue para más una opcion de eliminacion. SpVentaLoteDistribucion (frmLoteDistribucion)

    End Enum

    ''' <summary>
    ''' Enumeracion para definir las operaciones posibles en la base de datos Agregar, Anular, Eliminar
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMFechaPreestablecida

        HOY = 1
        AYER = 2
        ESTA_SEMANA = 3
        ESTE_MES = 5
        ESTE_AÑO = 6

    End Enum

    ''' <summary>
    ''' Tipos de Operaciones realizadas por el usuario, se utiliza especialmente por auditoria
    ''' </summary>
    ''' <remarks></remarks>
    Enum NUMTipoOperaciones

        ''' <summary>
        ''' Ingreso a formulario
        ''' </summary>
        ''' <remarks></remarks>
        ING = 1
        ''' <summary>
        ''' Salida de Formulario
        ''' </summary>
        ''' <remarks></remarks>
        SAL = 2

        ''' <summary>
        ''' Insertar registro
        ''' </summary>
        ''' <remarks></remarks>
        INS = 10
        ''' <summary>
        ''' Actualizar Registro
        ''' </summary>
        ''' <remarks></remarks>
        ACT = 11
        ''' <summary>
        ''' Eliminar registro
        ''' </summary>
        ''' <remarks></remarks>
        ELI = 12
        ''' <summary>
        ''' Anular registro
        ''' </summary>
        ''' <remarks></remarks>
        ANU = 13

        ''' <summary>
        ''' Imprimir documento
        ''' </summary>
        ''' <remarks></remarks>
        IMP = 20

        ''' <summary>
        ''' Ingreso de Sistema
        ''' </summary>
        ''' <remarks></remarks>
        ISI = 30
        ''' <summary>
        ''' Salida de Sistema
        ''' </summary>
        ''' <remarks></remarks>
        SSI = 31

    End Enum


    ''' <summary>
    ''' Enumeracion para definir el comportamiento de los botonos ABM
    ''' </summary>
    ''' <remarks></remarks>
    Enum ENUMPosicionesNavegar

        PRIMERO = 0
        ANTERIOR = 1
        SIGUIENTE = 2
        ULTIMO = 3

    End Enum

    Public Function RetornardtMeses() As DataTable
        Dim dt As New DataTable()
        dt.Columns.Add("ID")
        dt.Columns.Add("Descripcion")
        dt.Rows.Add(1, "Enero")
        dt.Rows.Add(2, "Febrero")
        dt.Rows.Add(3, "Marzo")
        dt.Rows.Add(4, "Abril")
        dt.Rows.Add(5, "Mayo")
        dt.Rows.Add(6, "Junio")
        dt.Rows.Add(7, "Julio")
        dt.Rows.Add(8, "Agosto")
        dt.Rows.Add(9, "Septiembre")
        dt.Rows.Add(10, "Octubre")
        dt.Rows.Add(11, "Noviembre")
        dt.Rows.Add(12, "Diciembre")

        Return dt
    End Function

#End Region

#Region "CONEXION CON BASE DE DATOS"

    ''' <summary>
    ''' Ejecuta un Procedimiento Almacenado en la Base de Datos, retorna el mensaje de la ejecucion.
    ''' </summary>
    ''' <param name="Parametros">Vector de tipo SqlParameter, esta coleccion guarda los parametros del Procedimiento Almacenado como: Nombre, Tipo de Dato, Ent/Sal, etc</param>
    ''' <param name="StoreProcedure">Indica como se llama el Procedimiento Almacenado en la Base de Datos</param>
    ''' <param name="MostrarMensaje">Opcional: TRUE si se quiere mostrar un mensaje si se proceso correctamente</param>
    ''' <param name="MostrarMensajeError">Opcional: TRUE si se quiere mostrar un mensaje si ocurrio un problema en el Procedimiento Almacenado</param>
    ''' <param name="Mensaje">Opcional: Informa de lo ocurrido en el la Base de Datos</param>
    ''' <param name="IDTransaccionSalida">Opcional: Retorna el IDTransaccion creada para el registro</param>
    ''' <returns>Retorna el mensaje del proceso</returns>
    ''' <remarks></remarks>
    Public Function ExecuteStoreProcedure(ByVal Parametros() As SqlParameter, ByVal StoreProcedure As String, Optional ByVal MostrarMensaje As Boolean = True, Optional ByVal MostrarMensajeError As Boolean = True, Optional ByRef Mensaje As String = "", Optional ByRef IDTransaccionSalida As String = "", Optional ByVal Auditoria As Boolean = False, Optional ByVal vConexion As String = "", Optional ByVal TimeOut As Integer = 60) As Boolean

        ExecuteStoreProcedure = False

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Try
            Dim Capturador As String = "Exec " & StoreProcedure & vbCrLf
            Dim rowCount As Integer
            Dim cmd As New SqlCommand
            Dim conn As New SqlConnection(VGCadenaConexion)



            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                'Establecemos las propiedades preliminares
                cmd.Connection = conn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = StoreProcedure
                cmd.CommandTimeout = TimeOut

                'Agregar la auditoria
                If Auditoria = True Then
                    cmd.Parameters.AddWithValue("@IDUsuario", vgIDUsuario)
                    cmd.Parameters.AddWithValue("@IDTerminal", vgIDTerminal)
                End If

                'Establecemos los parametros
                'Entrada
                For i = 0 To Parametros.GetLength(0) - 1
                    cmd.Parameters.Add(Parametros(i))
                    If i = 0 Then
                        Capturador = Capturador & Parametros(i).ParameterName & "='" & Parametros(i).Value & "'" & vbCrLf
                    Else
                        Capturador = Capturador & "," & Parametros(i).ParameterName & "='" & Parametros(i).Value & "'" & vbCrLf
                    End If
                Next

                'Ejecutamos el sp y evaluamos la cantidad de filas insertadas
                rowCount = cmd.ExecuteNonQuery()

                If rowCount > 0 Then
                    If MostrarMensaje = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else

                        If cmd.Parameters.IndexOf("@Mensaje") >= 0 Then
                            Mensaje = CStr(cmd.Parameters("@Mensaje").SqlValue.ToString)
                        End If

                        If IDTransaccionSalida <> "" Then
                            IDTransaccionSalida = CStr(cmd.Parameters("@IDTransaccionSalida").SqlValue.ToString)
                        End If

                    End If

                    Return True

                Else
                    If MostrarMensajeError = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Else
                        Mensaje = CStr(cmd.Parameters("@Mensaje").Value.ToString)
                    End If

                    Return False

                End If
            Catch ex As Exception
                CargarError(ex, "CSistema", "ExecuteStoreProcedure", cmd.Parameters.ToString, "Nombre del SP: " & StoreProcedure, MostrarMensajeError)
                Mensaje = ex.Message
                ExecuteStoreProcedure = False
                ''' <summary>
                ''' Registra un error del sistema y lo guarda en el directorio de ERROR
                ''' </summary>
                ''' <param name="Mensaje">Objeto que tiene la informacion del error.</param>
                ''' <param name="Origen">Desde donde se genero. Clase, Form, Modulo, etc.</param>
                ''' <param name="Bloque">Funcion o Procedimiento en donde se genero el error.</param>
                ''' <param name="Parametros">Opcional: Se puede dar informacion de los parametros que posiblemente que generaron el error.</param>
                ''' <param name="DatosExtras">Opcional: Se puede dar alguna informacion extra.</param>
                ''' <remarks></remarks>
                ''' 

                ''' CargarError(null, "frmPedido", "Guardar", "", "", "")
            Finally
                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If
            End Try
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteStoreProcedure", "", "Nombre del SP: " & StoreProcedure)
            Mensaje = ex.Message
            Return False
        End Try

    End Function
    Public Function ExecuteStoreProcedure2(ByVal Parametros() As SqlParameter, ByVal StoreProcedure As String, Optional ByVal MostrarMensaje As Boolean = True, Optional ByVal MostrarMensajeError As Boolean = True, Optional ByRef Mensaje As String = "", Optional ByRef IDTransaccionSalida As String = "", Optional ByVal Auditoria As Boolean = False, Optional ByVal vConexion As String = "") As Boolean

        ExecuteStoreProcedure2 = False

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Try
            Dim Capturador As String = "Exec " & StoreProcedure & vbCrLf
            Dim rowCount As Integer
            Dim cmd As New SqlCommand
            Dim conn As New SqlConnection(VGCadenaConexion)

            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                'Establecemos las propiedades preliminares
                cmd.Connection = conn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = StoreProcedure

                'Agregar la auditoria
                If Auditoria = True Then
                    cmd.Parameters.AddWithValue("@IDUsuario", vgIDUsuario)
                    cmd.Parameters.AddWithValue("@IDTerminal", vgIDTerminal)
                End If

                'Establecemos los parametros
                'Entrada
                For i = 0 To Parametros.GetLength(0) - 1
                    cmd.Parameters.Add(Parametros(i))
                    If i = 0 Then
                        Capturador = Capturador & Parametros(i).ParameterName & "='" & Parametros(i).Value & "'" & vbCrLf
                    Else
                        Capturador = Capturador & "," & Parametros(i).ParameterName & "='" & Parametros(i).ToString & "'" & vbCrLf
                    End If
                Next

                'Ejecutamos el sp y evaluamos la cantidad de filas insertadas
                rowCount = cmd.ExecuteNonQuery()

                If rowCount > 0 Then
                    If MostrarMensaje = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else

                        If cmd.Parameters.IndexOf("@Mensaje") >= 0 Then
                            Mensaje = CStr(cmd.Parameters("@Mensaje").SqlValue.ToString)
                        End If

                        If IDTransaccionSalida <> "" Then
                            IDTransaccionSalida = CStr(cmd.Parameters("@IDTransaccionSalida").SqlValue.ToString)
                        End If

                    End If

                    Return True

                Else
                    If MostrarMensajeError = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Else
                        Mensaje = CStr(cmd.Parameters("@Mensaje").Value.ToString)
                    End If

                    Return False

                End If
            Catch ex As Exception
                CargarError(ex, "CSistema", "ExecuteStoreProcedure", cmd.Parameters.ToString, "Nombre del SP: " & StoreProcedure, MostrarMensajeError)
                Mensaje = ex.Message
                ExecuteStoreProcedure2 = False
            Finally
                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If
            End Try
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteStoreProcedure", "", "Nombre del SP: " & StoreProcedure)
            Mensaje = ex.Message
            Return False
        End Try

    End Function

    '20-06-2022 - A modo de prueba se modifica el timeout = 120
    Public Function ExecuteStoreProcedure1(ByVal Parametros() As SqlParameter, ByVal StoreProcedure As String, Optional ByVal MostrarMensaje As Boolean = True, Optional ByVal MostrarMensajeError As Boolean = True, Optional ByRef Mensaje As String = "", Optional ByRef IDTransaccionSalida As String = "", Optional ByVal Auditoria As Boolean = False, Optional ByVal vConexion As String = "", Optional ByVal TimeOut As Integer = 120) As Boolean

        ExecuteStoreProcedure1 = False

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Try
            Dim Capturador As String = "Exec " & StoreProcedure & vbCrLf
            Dim rowCount As Integer
            Dim cmd As New SqlCommand
            Dim conn As New SqlConnection(VGCadenaConexion)



            Dim previousConnectionState As ConnectionState
            previousConnectionState = conn.State
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If

                'Establecemos las propiedades preliminares
                cmd.Connection = conn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = StoreProcedure
                cmd.CommandTimeout = TimeOut

                'Agregar la auditoria
                If Auditoria = True Then
                    cmd.Parameters.AddWithValue("@IDUsuario", vgIDUsuario)
                    cmd.Parameters.AddWithValue("@IDTerminal", vgIDTerminal)
                End If

                'Establecemos los parametros
                'Entrada
                For i = 0 To Parametros.GetLength(0) - 1
                    cmd.Parameters.Add(Parametros(i))
                    If i = 0 Then
                        Capturador = Capturador & Parametros(i).ParameterName & "='" & Parametros(i).Value & "'" & vbCrLf
                    Else
                        Capturador = Capturador & "," & Parametros(i).ParameterName & "='" & Parametros(i).Value & "'" & vbCrLf
                    End If
                Next

                'Ejecutamos el sp y evaluamos la cantidad de filas insertadas
                rowCount = cmd.ExecuteNonQuery()

                If rowCount > 0 Then
                    If MostrarMensaje = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else

                        If cmd.Parameters.IndexOf("@Mensaje") >= 0 Then
                            Mensaje = CStr(cmd.Parameters("@Mensaje").SqlValue.ToString)
                        End If

                        If IDTransaccionSalida <> "" Then
                            IDTransaccionSalida = CStr(cmd.Parameters("@IDTransaccionSalida").SqlValue.ToString)
                        End If

                    End If

                    Return True

                Else
                    If MostrarMensajeError = True Then
                        MessageBox.Show(CStr(cmd.Parameters("@Mensaje").Value.ToString), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Else
                        Mensaje = CStr(cmd.Parameters("@Mensaje").Value.ToString)
                    End If

                    Return False

                End If
            Catch ex As Exception
                CargarError(ex, "CSistema", "ExecuteStoreProcedure", cmd.Parameters.ToString, "Nombre del SP: " & StoreProcedure, MostrarMensajeError)
                Mensaje = ex.Message
                ExecuteStoreProcedure1 = False
            Finally
                If previousConnectionState = ConnectionState.Closed Then
                    conn.Close()
                End If
            End Try
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteStoreProcedure", "", "Nombre del SP: " & StoreProcedure)
            Mensaje = ex.Message
            Return False
        End Try

    End Function
    Public Function NuevaTransaccion(ByVal IDOperacion As Integer, Optional ByVal IDUsuario As Integer = 0, Optional ByVal IDPerfil As Integer = 0, Optional ByVal IDSucursal As Integer = 0, Optional ByVal IDDeposito As Integer = 0, Optional ByVal IDTerminal As Integer = 0, Optional ByVal vConexion As String = "", Optional ByVal vControlarCaja As Boolean = True) As Integer

        NuevaTransaccion = 0

        If IDUsuario = 0 Then
            IDUsuario = vgIDUsuario
        End If

        If IDSucursal = 0 Then
            IDSucursal = vgIDSucursal
        End If

        If IDDeposito = 0 Then
            IDDeposito = vgIDDeposito
        End If

        If IDTerminal = 0 Then
            IDTerminal = vgIDTerminal
        End If

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Dim Param(-1) As SqlClient.SqlParameter
        SetSQLParameter(Param, "@IDUsuario", IDUsuario, ParameterDirection.Input, 0, SqlDbType.Int)
        SetSQLParameter(Param, "@IDSucursal", IDSucursal, ParameterDirection.Input, 0, SqlDbType.Int)
        SetSQLParameter(Param, "@IDDeposito", IDDeposito, ParameterDirection.Input, 0, SqlDbType.Int)
        SetSQLParameter(Param, "@IDTerminal", IDTerminal, ParameterDirection.Input, 0, SqlDbType.Int)
        SetSQLParameter(Param, "@IDOperacion", IDOperacion, ParameterDirection.Input, 0, SqlDbType.Int)

        If vControlarCaja = True Then
            Dim CCaja As New CCaja
            CCaja.IDSucursal = IDSucursal
            CCaja.ObtenerUltimoNumero()
            SetSQLParameter(Param, "@IDTransaccionCaja", CCaja.IDTransaccion, ParameterDirection.Input, 0, SqlDbType.Int)
        Else
            SetSQLParameter(Param, "@IDTransaccionCaja", 0, ParameterDirection.Input, 0, SqlDbType.Int)
        End If

        'Salida
        SetSQLParameter(Param, "@Mensaje", "", ParameterDirection.Output, 200, SqlDbType.VarChar)
        SetSQLParameter(Param, "@Procesado", "False", ParameterDirection.Output, 10, SqlDbType.VarChar)
        SetSQLParameter(Param, "@IDTransaccion", "0", ParameterDirection.Output, 0, SqlDbType.Int)

        Dim SQL As String = SPToString(Param, "SpTransaccion")
        Dim dt As DataTable = ExecuteToDataTable(SQL, VGCadenaConexion, 10)

        If dt IsNot Nothing Then
            If dt.Rows.Count > 0 Then
                NuevaTransaccion = dt.Rows(0)("IDTransaccion")
            End If
        End If

    End Function

    ''' <summary>
    ''' Retorna un DataTable segun la consulta pasada por parametro
    ''' </summary>
    ''' <param name="consulta">Cualquier consulta tipo TSQL</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteToDataTable(ByVal consulta As String, Optional ByVal vConexion As String = "", Optional ByVal Timeout As Integer = 100) As DataTable

        ExecuteToDataTable = Nothing

        Try
            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Using Adapter As New SqlDataAdapter(consulta, VGCadenaConexion)
                Dim dt As New DataTable
                'Adapter.ContinueUpdateOnError = True
                Adapter.SelectCommand.CommandTimeout = Timeout
                Adapter.Fill(dt)
                Adapter.SelectCommand.Connection.Close()
                Adapter.Dispose()
                Return (dt)
            End Using

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTable", "", consulta)
            Return Nothing

        End Try


    End Function

    Public Function ExecuteToDataTableDBF(ByVal consulta As String, ByVal Path As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableDBF = New DataTable

        Try

            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Dim sConn As String = "Provider=vfpoledb.1;Data Source=" & Path & ";Collating Sequence=general;"
            Dim dbConn As New System.Data.OleDb.OleDbConnection(sConn)

            dbConn.Open()

            Dim Adapter As New System.Data.OleDb.OleDbDataAdapter(consulta, dbConn)
            Dim dt As New DataTable

            Adapter.Fill(dt)

            dbConn.Close()

            Return dt

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTable", "", consulta)
            Return Nothing
        End Try


    End Function

    Public Function ExecuteToDataTableXLS(ByVal consulta As String, ByVal Archivo As String, Optional ByVal vConexion As String = "") As DataTable

        ExecuteToDataTableXLS = Nothing
        Try
            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Dim MiConexion As New System.Data.OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Archivo & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'")
            Dim MiAdaptador As New System.Data.OleDb.OleDbDataAdapter(consulta, MiConexion)
            Dim MiDataSet As New DataSet()
            Dim MiEnlazador As New BindingSource

            Dim commandbuilder As New OleDb.OleDbCommandBuilder(MiAdaptador)
            MiConexion.Open()
            MiAdaptador.Fill(MiDataSet)
            Return MiDataSet.Tables(0)

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTable", "", consulta)
            Return Nothing
        End Try


    End Function

    Public Function ExecuteToDataTableTXT(ByVal path As String, Optional ByVal vConexion As String = "") As DataTable
        ExecuteToDataTableTXT = Nothing

        Try

            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Dim dt As New DataTable
            Dim myStream As System.IO.StreamReader = New System.IO.StreamReader(path)
            Dim Linea As String
            Dim Separador As String = vbTab
            Dim Cabecera As Boolean = True
            Do
                Linea = myStream.ReadLine()
                If Linea Is Nothing Then
                    Exit Do
                End If

                Dim Vector As String() = Split(Linea, Separador)

                If Cabecera = True Then

                    For i As Integer = 0 To Vector.GetLength(0) - 1
                        dt.Columns.Add(Vector(i).ToString)
                    Next

                    Cabecera = False

                Else

                    'Cargamos los registros
                    Dim NewRow As DataRow = dt.NewRow
                    For i As Integer = 0 To Vector.GetLength(0) - 1
                        NewRow(i) = Vector(i).ToString
                    Next

                    dt.Rows.Add(NewRow)

                End If

            Loop

            myStream.Close()

            Return dt

        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteToDataTableTXT", "", path)
            Return Nothing
        End Try



    End Function


    Public Function DataTableToTXT(vdt As DataTable, Path As String) As Boolean

        DataTableToTXT = False

        If IO.File.Exists(Path) = True Then
            Try
                IO.File.Delete(Path)
            Catch ex As Exception
                MessageBox.Show("No se pudo Generar", "Atención", MessageBoxButtons.OK)
                Return False
            End Try
        End If

        Dim sw As New System.IO.StreamWriter(Path, True, System.Text.Encoding.Unicode)

        Try
            'CABECERA
            Dim cadena As String = ""
            For c As Integer = 0 To vdt.Columns.Count - 1
                If c = vdt.Columns.Count - 1 Then
                    cadena = cadena & vdt.Columns(c).ColumnName
                Else
                    cadena = cadena & vdt.Columns(c).ColumnName & vbTab
                End If
            Next

            sw.WriteLine(cadena)

            'DETALLE
            For Each oRow As DataRow In vdt.Rows
                cadena = ""
                For c As Integer = 0 To vdt.Columns.Count - 1
                    If c = vdt.Columns.Count - 1 Then
                        cadena = cadena & oRow(c).ToString
                    Else
                        cadena = cadena & oRow(c).ToString & vbTab
                    End If
                Next

                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            sw.Close()
            Return False
        End Try


        Return True

    End Function

    ''' <summary>
    ''' Retorna un SqlDataReader segun la consulta pasada por parametro
    ''' </summary>
    ''' <param name="consulta">Cualquier consulta tipo TSQL</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Public Function ExecuteReader(ByVal consulta As String, Optional ByVal vConexion As String = "") As SqlDataReader


        ExecuteReader = Nothing

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Dim cmd As New SqlCommand
        Dim conn As New SqlConnection(VGCadenaConexion)
        Dim reader As SqlDataReader
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            reader = cmd.ExecuteReader()
            Using reader
                While reader.Read
                    ' Process SprocResults datareader here.
                    Console.WriteLine(reader.GetValue(0))
                End While
            End Using
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteReader", "", consulta)
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        End Try



    End Function

    ''' <summary>
    ''' Sirve para extraer alguna informacion especifica de la base de datos. No confundir con ExecuteReader
    ''' </summary>
    ''' <param name="Consulta">Consulta TSQL, debe ser especifica, o sea, se debe solicitar un unico valor.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecuteScalar(ByVal Consulta As String, Optional ByVal vConexion As String = "", Optional ByVal Timeout As Integer = 10) As Object


        ExecuteScalar = Nothing
        Dim cadena As String = Nothing
        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If
        cadena = VGCadenaConexion
        If Not cadena.Contains("Timeout") Then
            cadena = cadena & ";Connection Timeout=" & Timeout.ToString
        End If

        Dim conn As New SqlConnection(cadena)
        Dim cmd As New SqlCommand(Consulta, conn)
        Dim result As Object
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            result = cmd.ExecuteScalar()
            Return result
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteScalar", "", Consulta)
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        End Try

    End Function

    Public Function ExecuteNonQuery(ByVal Consulta As String, Optional ByVal vConexion As String = "", Optional ByVal Timeout As Integer = 30) As Integer

        ExecuteNonQuery = 0

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Dim conn As New SqlConnection(VGCadenaConexion)
        Dim cmd As New SqlCommand(Consulta, conn)
        cmd.CommandTimeout = Timeout
        Dim result As Integer
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            result = cmd.ExecuteNonQuery
            Return result
        Catch ex As Exception
            CargarError(ex, "CSistema", "ExecuteNonQuery", "", Consulta)
            Debug.Print(ex.Message)
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
                conn.Dispose()
            End If
        End Try

    End Function

    Public Function CerrarConexion(Optional ByVal vConexion As String = "") As Boolean

        CerrarConexion = 0

        If vConexion <> "" Then
            VGCadenaConexion = vConexion
        End If

        Dim conn As New SqlConnection(VGCadenaConexion)
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            Return True
        Catch ex As Exception
            CargarError(ex, "CSistema", "CerrarConexion", "", "")
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
            SqlConnection.ClearAllPools()
        End Try

    End Function

    Public Function SPToString(ByVal parametros() As SqlParameter, ByVal StoreProcedure As String, Optional ByVal DeclararVariablesSalida As Boolean = True) As String

        SPToString = ""

        Dim Variables As String = ""
        Dim Datos As String = ""
        Dim Salida As String = ""

        'Variables
        If DeclararVariablesSalida = True Then
            For i = 0 To parametros.GetLength(0) - 1
                If parametros(i).Direction = ParameterDirection.Output Then
                    If Variables = "" Then
                        Variables = "Declare " & parametros(i).ParameterName & " " & parametros(i).SqlDbType.ToString
                    Else
                        Variables = Variables & ", " & parametros(i).ParameterName & " " & parametros(i).SqlDbType.ToString
                    End If
                End If
            Next
        End If

        'Datos
        For i = 0 To parametros.GetLength(0) - 1
            If parametros(i).Direction = ParameterDirection.Input Then
                If Datos = "" Then
                    Datos = " " & parametros(i).ParameterName & " = '" & parametros(i).Value & "'"
                Else
                    Datos = Datos & ", " & parametros(i).ParameterName & " = '" & parametros(i).Value & "'"
                End If
            End If
        Next

        'Datos
        For i = 0 To parametros.GetLength(0) - 1
            If parametros(i).Direction = ParameterDirection.Output Then
                If Salida = "" Then
                    Salida = ", " & parametros(i).ParameterName & " = " & parametros(i).ParameterName
                Else
                    Salida = Salida & ", " & parametros(i).ParameterName & " = " & parametros(i).ParameterName
                End If
            End If
        Next

        SPToString = Variables & " Execute " & StoreProcedure & " " & Datos & " " & Salida

    End Function

    Public Function InsertSQL(ByVal dt As DataTable, ByVal tabla As String) As String

        InsertSQL = "INSERT INTO " & tabla
        Dim Campos As String = ""
        Dim Valores As String = ""

        'Generar los campos
        For Each oRow As DataRow In dt.Rows
            If Campos = "" Then
                Campos = "(" & oRow("Campo")
            Else
                Campos = Campos & "," & oRow("Campo")
            End If
        Next

        Campos = Campos & ")"

        'Generar los valores
        For Each oRow As DataRow In dt.Rows
            If Valores = "" Then
                Valores = " VALUES('" & oRow("Valor") & "'"
            Else
                Valores = Valores & ",'" & oRow("Valor") & "'"
            End If
        Next

        Valores = Valores & ")"

        InsertSQL = InsertSQL & Campos & Valores

    End Function

    Public Function FuncionPermitida(ByVal IDPerfil As Integer, ByVal frm As Form, ByVal Funcion As String) As Boolean
        Dim Retorno As Boolean = False

        Retorno = ExecuteScalar("Select Habilitado from vaccesoespecificoperfil where IDPerfil = " & IDPerfil & "and Tag= '" & frm.Name & "' and NombreFuncion = '" & Funcion & "'")
        If IDPerfil = 1 Then
            Retorno = True
        End If

        Return Retorno

    End Function


    Function SetSQLParameter(ByVal NombreParametro As String, ByVal Valor As String, ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.VarChar) As SqlParameter
        SetSQLParameter = Nothing

        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro
        Retorno.Value = Valor
        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Return Retorno

    End Function

    Sub SetSQLParameter(ByRef SQLParametros() As SqlParameter, ByVal NombreParametro As String, ByVal valor As Byte(), ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.Image)

        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro

        If IsNothing(valor) = False Then
            Retorno.Value = valor
        End If

        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Dim i As Integer = SQLParametros.GetLength(0)

        ReDim Preserve SQLParametros(i)

        SQLParametros(i) = Retorno

    End Sub

    Sub SetSQLParameter(ByRef SQLParametros As DataTable, ByVal Campo As String, ByVal Valor As String)

        If SQLParametros Is Nothing Then
            SQLParametros = New DataTable
            SQLParametros.Columns.Add("Campo")
            SQLParametros.Columns.Add("Valor")
        End If

        If SQLParametros.Columns.Count = 0 Then
            SQLParametros.Columns.Add("Campo")
            SQLParametros.Columns.Add("Valor")
        End If

        Dim NewRow As DataRow = SQLParametros.NewRow
        NewRow("Campo") = Campo
        NewRow("Valor") = Valor
        SQLParametros.Rows.Add(NewRow)

    End Sub

    Sub SetSQLParameter(ByRef SQLParametros() As SqlParameter, ByVal NombreParametro As String, ByVal cbx As ComboBox, ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.VarChar)
        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro

        If cbx.Text.Trim <> "" Then
            If IsNothing(cbx.SelectedValue) = False Then
                Retorno.Value = cbx.SelectedValue
            End If
        End If

        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Dim i As Integer = SQLParametros.GetLength(0)

        ReDim Preserve SQLParametros(i)

        SQLParametros(i) = Retorno

    End Sub

    Sub SetSQLParameter(ByRef SQLParametros() As SqlParameter, ByVal NombreParametro As String, ByVal Valor As String, ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.VarChar)

        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro

        Valor = Valor.Trim

        If IsNothing(Valor) = False Then
            Retorno.Value = Valor
        End If

        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Dim i As Integer = SQLParametros.GetLength(0)

        ReDim Preserve SQLParametros(i)

        SQLParametros(i) = Retorno

    End Sub

    Sub SetSQLParameter(ByRef SQLParametros() As SqlParameter, ByVal NombreParametro As String, ByVal ocxTXTCuentaContable As ocxTXTCuentaContable, ByVal Tipo As ParameterDirection, Optional ByVal Tamaño As Integer = 0, Optional ByVal TipoDato As SqlDbType = SqlDbType.VarChar)

        Dim Retorno As New SqlParameter
        Retorno.ParameterName = NombreParametro

        If ocxTXTCuentaContable.Seleccionado = False Then
            Exit Sub
        End If

        Try
            If IsNumeric(ocxTXTCuentaContable.Registro("ID").ToString) = False Then
                Exit Sub
            End If

        Catch ex As Exception

        End Try

        Dim valor As Integer = ocxTXTCuentaContable.Registro("ID").ToString

        Retorno.Value = valor
        Retorno.Direction = Tipo
        Retorno.SqlDbType = TipoDato

        If Tamaño > 0 Then
            Retorno.Size = Tamaño
        End If

        Dim i As Integer = SQLParametros.GetLength(0)

        ReDim Preserve SQLParametros(i)

        SQLParametros(i) = Retorno

    End Sub

    Sub ConcatenarParametro(ByRef SQL As String, ByVal Campo As String, ByVal Valor As String, Optional ByVal AgregarComa As Boolean = False)

        'Buscamos si existe una "," coma
        If SQL.IndexOf("@") > 0 Then
            SQL = SQL & ", " & Campo & "='" & Valor & "' "
        Else
            SQL = SQL & " " & Campo & "='" & Valor & "' "
        End If


        If AgregarComa = True Then
            SQL = SQL & ","
        End If

    End Sub

#End Region

#Region "CONTROLES WINFORM Y BASES DE DATOS"

    ''' <summary>
    ''' Carga cualquier tipo de consulta TSQL en el LsitView
    ''' </summary>
    ''' <param name="lv">Control Winform tipo ListView</param>
    ''' <param name="consulta">Consulta tipo TSQL</param>
    ''' <remarks></remarks>
    Public Sub SqlToLv(ByVal lv As ListView, ByVal consulta As String, Optional ByVal vConexion As String = "")
        Try

            If vConexion <> "" Then
                VGCadenaConexion = vConexion
            End If

            Dim Adapter As New SqlDataAdapter(consulta, VGCadenaConexion)
            Dim ds As New DataSet
            Adapter.Fill(ds, "Resultado")
            Dim objListItem As New ListViewItem
            lv.FullRowSelect = True
            lv.GridLines = True
            lv.AllowColumnReorder = True
            lv.Columns.Clear()
            lv.Scrollable = True
            lv.View = View.Details
            lv.HideSelection = False

            'Determinamos la cantidad de resultados
            Dim i As Integer

            For i = 0 To ds.Tables(0).Columns.Count - 1
                lv.Columns.Add(ds.Tables(0).Columns(i).ToString, ds.Tables(0).Columns(i).ToString)
            Next

            lv.Items.Clear()
            For Each drw As DataRow In ds.Tables("Resultado").Rows
                objListItem = lv.Items.Add(drw.Item(0).ToString, 0)
                If ds.Tables(0).Columns.Count > 1 Then
                    For i = 1 To ds.Tables(0).Columns.Count - 1
                        objListItem.SubItems.Add(drw.Item(i).ToString)
                    Next
                End If
            Next

            For i = 0 To ds.Tables(0).Columns.Count - 1
                lv.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
            Next

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToLv", "", consulta)
        End Try


    End Sub

    Public Sub dtToGrid(ByVal grid As DataGridView, ByVal dt As DataTable, Optional ByVal MostrarError As Boolean = True)
        Try
            grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(vgColorDataGridAlternancia)
            grid.BackgroundColor = Color.White
            grid.RowHeadersVisible = False
            grid.AllowUserToAddRows = False
            grid.AllowUserToDeleteRows = False
            grid.StandardTab = True
            grid.MultiSelect = False
            grid.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2
            grid.Cursor = Cursors.Hand
            grid.DataSource = dt

            Try
                grid.Columns("ControlarExistencia").Visible = False
            Catch
            End Try

            grid.AllowUserToAddRows = False
            grid.AllowUserToDeleteRows = False

            'Nombres de Columnas y vision
            For c As Integer = 0 To grid.ColumnCount - 1
                grid.Columns(c).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
                grid.Columns(c).Name = dt.Columns(c).ColumnName
            Next


        Catch ex As Exception
            If MostrarError Then
                CargarError(ex, "CSistema", "dtTogrid", "", "")
            End If

        End Try



    End Sub

    Public Sub SqlToDataGrid(ByVal dg As DataGridView, ByVal sql As String, Optional ByVal vConexion As String = "", Optional TimeOut As Integer = 30, Optional MostrarError As Boolean = True)
        Try
            Dim dt As DataTable = ExecuteToDataTable(sql, vConexion, TimeOut)
            dtToGrid(dg, dt, MostrarError)

        Catch ex As Exception
            If MostrarError Then
                CargarError(ex, "CSistema", "SqlToDataGrid", "", "")
            End If

        End Try


    End Sub

    Public Sub dtToLv(ByVal lv As ListView, ByVal dt As DataTable)
        Try

            Dim objListItem As New ListViewItem
            lv.FullRowSelect = True
            lv.GridLines = True
            lv.AllowColumnReorder = True
            lv.Columns.Clear()
            lv.Scrollable = True
            lv.View = View.Details
            lv.HideSelection = False

            'Determinamos la cantidad de resultados
            Dim i As Integer

            For i = 0 To dt.Columns.Count - 1
                lv.Columns.Add(dt.Columns(i).ToString, dt.Columns(i).ToString)
            Next

            lv.Items.Clear()
            For Each drw As DataRow In dt.Rows
                objListItem = lv.Items.Add(drw.Item(0).ToString)
                If dt.Columns.Count > 1 Then
                    For i = 1 To dt.Columns.Count - 1
                        'objListItem.SubItems.Add(drw.Item(i).ToString)
                        Dim subItem As New ListViewItem.ListViewSubItem
                        subItem.Name = dt.Columns(i).ColumnName
                        subItem.Text = drw.Item(i).ToString
                        objListItem.SubItems.Add(subItem)
                    Next
                End If
            Next

            For i = 0 To dt.Columns.Count - 1
                lv.Columns(i).AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize)
            Next

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToLv", "", "")
        End Try


    End Sub

    Public Sub dtToLv(ByVal lv As ListView, ByVal dt As DataTable, ByVal Campos() As String, Optional ByVal OrderBy As String = "")
        Try

            'Copiamos la estructura y datos
            Dim dttemp As DataTable = dt.Copy

            'Ahora recorremos los campos y eliminamos los que no estan definidos
            For i As Integer = 0 To dt.Columns.Count - 1

                Dim Existe As Integer = Array.IndexOf(Campos, dt.Columns(i).ColumnName)

                If Existe < 0 Then
                    dttemp.Columns.RemoveAt(i)
                End If

            Next

            'Ordenar segun campos
            For i As Integer = 0 To Campos.GetLength(0) - 1
                dttemp.Columns(Campos(i)).SetOrdinal(i)
            Next

            OrderDataTable(dttemp, OrderBy)

            dtToLv(lv, dttemp)

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToLv", "", "")
        End Try


    End Sub

    Public Sub OrderDataTable(ByRef dt As DataTable, ByVal OrderBy As String, Optional ByVal Asc As Boolean = True)

        If dt Is Nothing Then
            Exit Sub
        End If


        If OrderBy = "" Then
            Exit Sub
        End If

        Dim Tipo As String
        If Asc = True Then
            Tipo = " ASC "
        Else
            Tipo = " DESC "
        End If

        Dim dtv As DataView = dt.DefaultView
        dtv.Sort = OrderBy & Tipo

        dt = dtv.ToTable().Copy

    End Sub

    ''' <summary>
    ''' Carga la consulta SQL en el ComboBox pasado por parametro.
    ''' Esta funcion sirve para posteriormente obtener el ID de un registro.
    ''' Ej: Consulta="Select ID, RazonSocial From Cliente" donde el ID se obtiene mediante cbx.SelectedValue
    ''' </summary>
    ''' <param name="cbx"></param>
    ''' <param name="consulta"></param>
    ''' <remarks></remarks>
    Sub SqlToComboBox(ByRef cbx As ComboBox, ByVal consulta As String, Optional ByVal CadenaConexion As String = "")

        Try

            If CadenaConexion <> "" Then
                VGCadenaConexion = CadenaConexion
            End If

            Using conn As New SqlConnection(VGCadenaConexion)
                Using Adapter As New SqlDataAdapter(consulta, conn)
                    Dim table As New DataTable
                    Adapter.Fill(table)
                    cbx.ValueMember = table.Columns(0).ToString
                    cbx.DisplayMember = table.Columns(1).ToString
                    cbx.DataSource = table
                End Using

            End Using

        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", consulta)
        End Try

    End Sub

    Sub SqlToComboBox(ByVal cbx As DataGridViewComboBoxColumn, ByVal consulta As String)

        Try
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim Adapter As New SqlDataAdapter(consulta, conn)
            Dim table As New DataTable
            Adapter.Fill(table)
            cbx.ValueMember = table.Columns(0).ToString
            cbx.DisplayMember = table.Columns(1).ToString
            cbx.DataSource = table
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", consulta)
        End Try

    End Sub

    ''' <summary>
    ''' Carga la consulta SQL en el ComboBox pasado por parametro.
    ''' Esta funcion sirve para posteriormente obtener el ID de un registro.
    ''' Ej: Consulta="Select ID, RazonSocial From Cliente" donde el ID se obtiene mediante cbx.SelectedValue
    ''' </summary>
    ''' <param name="cbx"></param>
    ''' <param name="consulta"></param>
    ''' <remarks></remarks>
    Sub SqlToComboBox(ByVal cbx As ocxCBX, ByVal consulta As String, Optional ByVal CadenaConexion As String = "")
        Me.SqlToComboBox(cbx.cbx, consulta)
        cbx.DataDisplayMember = cbx.cbx.DisplayMember
        cbx.DataValueMember = cbx.cbx.ValueMember
    End Sub

    Sub SqlToComboBox(ByVal cbx As ComboBox, ByVal dt As DataTable)

        Try
            cbx.ValueMember = dt.Columns(0).ToString
            cbx.DisplayMember = dt.Columns(1).ToString
            cbx.DataSource = dt
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", dt.TableName)
        End Try

    End Sub

    Sub SqlToComboBox(ByVal cbx As ComboBox, ByVal dt As DataTable, ByVal ColumnValue As String, ByVal ColumnDisplay As String)

        Try
            cbx.ValueMember = dt.Columns(ColumnValue).ToString
            cbx.DisplayMember = dt.Columns(ColumnDisplay).ToString
            cbx.DataSource = dt
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", dt.TableName)
        End Try

    End Sub

    Sub SqlToComboBox(ByVal cbx As DataGridViewComboBoxColumn, ByVal dt As DataTable, ByVal ColumnValue As String, ByVal ColumnDisplay As String)

        Try
            cbx.ValueMember = dt.Columns(ColumnValue).ToString
            cbx.DisplayMember = dt.Columns(ColumnDisplay).ToString
            cbx.DataSource = dt
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToComboBox", "", dt.TableName)
        End Try

    End Sub

    Sub SqlToListBox(ByVal lbx As ListBox, ByVal dt As DataTable, ByVal ColumnValue As String, ByVal ColumnDisplay As String)

        Try
            lbx.ValueMember = dt.Columns(ColumnValue).ToString
            lbx.DisplayMember = dt.Columns(ColumnDisplay).ToString
            lbx.DataSource = dt
        Catch ex As Exception
            CargarError(ex, "CSistema", "SqlToListBox", "", dt.TableName)
        End Try

    End Sub

    ''' <summary>
    ''' Devuelve un DataTable Filtrado
    ''' </summary>
    ''' <param name="dt">DataTable con informacion que se quiere filtrar</param>
    ''' <param name="Where">Los parametros de filtro</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Function FiltrarDataTable(ByVal dt As DataTable, ByVal Where As String) As DataTable

        FiltrarDataTable = Nothing

        If dt Is Nothing Then
            Return Nothing
        End If

        Try

            Dim dttemp As DataTable = dt.Copy
            Dim dtv As DataView = dttemp.DefaultView
            dtv.RowFilter = Where
            FiltrarDataTable = dtv.ToTable


            Return FiltrarDataTable

        Catch ex As Exception
            CargarError(ex, "CSistema", "FiltrarDataTable", "", dt.TableName)
        End Try


    End Function

    Function ObtenerIDOperacion(ByVal FormName As String, ByVal Descripcion As String, ByVal Codigo As String, Optional ByVal vCadenaConexion As String = "", Optional PrimeraVez As Boolean = False) As Integer

        ObtenerIDOperacion = 0
        Dim Retorno As Integer = CType(ExecuteScalar("Select IsNull((Select Top(1) ID From Operacion Where FormName = '" & FormName & "'), -1)", vCadenaConexion), Integer)

        If Retorno > 0 Then
            PrimeraVez = False
            Return Retorno
            'Else
            '    MessageBox.Show("Se produjo un error interno. El sistema no puede generar el codigo de Operacion, sin este codigo no se puede procesar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            '    Return -1
        End If

        'Se comenta porque inserta operaciones repetidas
        If Retorno > 0 Then
            PrimeraVez = False
            Return Retorno
        End If

        If Retorno < 0 Then
            MessageBox.Show("Se produjo un error interno. El sistema no puede generar el codigo de Operacion, sin este codigo no se puede procesar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return -1
        End If

        ''Es por primera vez
        'PrimeraVez = True

        ''Agregar Operacion
        Dim IDOperacion As Integer = CType(ExecuteScalar("Select IsNull((Select MAX(ID)+1 From Operacion), 1)", vCadenaConexion), Integer)
        Dim Consulta As String = "Insert Into Operacion(ID, Descripcion, Codigo, AsientoCredito, AsientoDebito, FormName) values('" & IDOperacion & "', '" & Descripcion & "', '" & Codigo & "', NULL, NULL, '" & FormName & "')"

        If ExecuteNonQuery(Consulta, vCadenaConexion) > 0 Then
            Return IDOperacion
        Else
            MessageBox.Show("Se produjo un error interno. El sistema no puede generar el codigo de Operacion, sin este codigo no se puede procesar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return 0
        End If

    End Function

    Function ObtenerIDImportacionDatos(ByVal Descripcion As String, ByVal Scrip As String) As Integer

        ObtenerIDImportacionDatos = 0
        Dim Retorno As Integer = CType(ExecuteScalar("Select IsNull((Select ID From ImportarDatos Where Descripcion = '" & Descripcion & "'), 0)"), Integer)

        If Retorno > 0 Then
            Return Retorno
        End If

        'Agregar Operacion
        Dim IDOperacion As Integer = CType(ExecuteScalar("Select IsNull((Select MAX(ID)+1 From ImportarDatos), 1)"), Integer)
        Dim Consulta As String = "Insert Into ImportarDatos(ID, Descripcion, Scrip) values('" & IDOperacion & "', '" & Descripcion & "', '" & Scrip & "')"

        If ExecuteNonQuery(Consulta) > 0 Then
            Return IDOperacion
        Else
            MessageBox.Show("Se produjo un error interno. El sistema no puede generar el codigo de Operacion, sin este codigo no se puede procesar.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return 0
        End If

    End Function

    Function ObtenerProximoNroComprobante(ByVal IDTipoComprobante As Integer, ByVal Tabla As String, ByVal Campo As String, ByVal IDSucursal As Integer, Optional ByVal VCadenaConexion As String = "") As String

        ObtenerProximoNroComprobante = ""

        Dim dtTemp As DataTable = ExecuteToDataTable("Select 'Autonumerico'=(Select Autonumerico From TipoComprobante Where ID=" & IDTipoComprobante & "), 'MaxNroComprobante'=(Select IsNull((Select " & Campo & " From " & Tabla & " Where IDTransaccion=(Select Max(IDTransaccion) From " & Tabla & " Where IDSucursal=" & IDSucursal & ")), 0))", VCadenaConexion).Copy

        For Each oRow As DataRow In dtTemp.Rows

            If IsDBNull(oRow("Autonumerico")) Then
                Return ""
            End If

            If oRow("Autonumerico") = False Then
                Return ""
            End If

            Dim NroComprobante As String = oRow("MaxNroComprobante")

            If IsNumeric(NroComprobante) = True Then
                NroComprobante = CInt(NroComprobante) + 1
                Return NroComprobante
            End If

        Next

    End Function

#End Region

#Region "UTILITARIOS"

    Public Function validar_Mail(ByVal sMail As String) As Boolean
        ' retorna true o false   
        Return Regex.IsMatch(sMail,
                "^([\w-]+\.)*?[\w-]+@[\w-]+\.([\w-]+\.)*?[\w]+$")
    End Function


    Public Shared Function rtftoHTML(ByVal rtf As RichTextBox) As String
        Dim b, i, u As Boolean
        b = False : i = False : u = False
        Dim fontfamily As String = "Arial"
        Dim fontsize As Integer = 12
        Dim htmlstr As String = String.Format("<html>{0}<body>{0}<div style=""text-align: left;""><span style=""font-family: Arial; font-size: 12pt;"">", vbCrLf)
        Dim x As Integer = 0
        While x < rtf.Text.Length
            rtf.Select(x, 1)
            If rtf.SelectionFont.Bold AndAlso (Not b) Then
                htmlstr &= "<b>"
                b = True
            ElseIf (Not rtf.SelectionFont.Bold) AndAlso b Then
                htmlstr &= "</b>"
                b = False
            End If
            If rtf.SelectionFont.Italic AndAlso (Not i) Then
                htmlstr &= "<i>"
                i = True
            ElseIf (Not rtf.SelectionFont.Italic) AndAlso i Then
                htmlstr &= "</i>"
                i = False
            End If
            If rtf.SelectionFont.Underline AndAlso (Not u) Then
                htmlstr &= "<u>"
                u = True
            ElseIf (Not rtf.SelectionFont.Underline) AndAlso u Then
                htmlstr &= "</u>"
                u = False
            End If
            If fontfamily <> rtf.SelectionFont.FontFamily.Name Then
                htmlstr &= String.Format("</span><span style=""font-family: {0}; font-size: {0}pt;"">", rtf.SelectionFont.FontFamily.Name, fontsize)
                fontfamily = rtf.SelectionFont.FontFamily.Name
            End If
            If fontsize <> rtf.SelectionFont.SizeInPoints Then
                htmlstr &= String.Format("</span><span style=""font-family: {0}; font-size: {0}pt;"">", fontfamily, rtf.SelectionFont.SizeInPoints)
                fontsize = rtf.SelectionFont.SizeInPoints
            End If
            Dim curchar As String = rtf.SelectedText
            Select Case curchar
                Case vbCr, vbLf : curchar = "<br />"
                Case "&" : curchar = "&amp;" : x += "&amp;".Length - 1
                Case "<" : curchar = "&lt;" : x += "&lt;".Length - 1
                Case ">" : curchar = "&gt;" : x += "&gt;".Length - 1
                Case " " : curchar = "&nbsp;" : x += "&nbsp;".Length - 1
            End Select
            rtf.SelectedText = curchar
            x += 1
        End While
        Return htmlstr & String.Format("</span>{0}</body>{0}</html>", vbCrLf)
    End Function


    Sub ControlBotonesABM(ByVal Proceso As CSistema.NUMHabilitacionBotonesABM, ByVal btnNuevo As Button, ByVal btnEditar As Button, ByVal btnCancelar As Button, ByVal btnGuardar As Button, ByVal btnEliminar As Button, ByRef ctrl() As Object, Optional ByVal btnImprimir As Button = Nothing)

        Dim HabilitarControles As Boolean = False

        Try

            If btnImprimir Is Nothing Then
                btnImprimir = New Button
            End If

            If ctrl Is Nothing Then
                ReDim ctrl(-1)
            End If

            Select Case Proceso

                Case CSistema.NUMHabilitacionBotonesABM.INICIO
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = False
                    btnImprimir.Enabled = False
                Case CSistema.NUMHabilitacionBotonesABM.NUEVO
                    btnNuevo.Enabled = False
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = True
                    btnGuardar.Enabled = True
                    btnEliminar.Enabled = False
                    HabilitarControles = True
                    btnImprimir.Enabled = False
                Case CSistema.NUMHabilitacionBotonesABM.EDITAR
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = True
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = True
                    btnImprimir.Enabled = False
                Case CSistema.NUMHabilitacionBotonesABM.CANCELAR
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = False
                    btnImprimir.Enabled = True
                Case CSistema.NUMHabilitacionBotonesABM.GUARDAR
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = True
                    btnImprimir.Enabled = True
                Case CSistema.NUMHabilitacionBotonesABM.ELIMINAR
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = True
                    btnCancelar.Enabled = False
                    btnGuardar.Enabled = False
                    btnEliminar.Enabled = True
                    btnImprimir.Enabled = False
                Case CSistema.NUMHabilitacionBotonesABM.EDITANDO
                    btnNuevo.Enabled = True
                    btnEditar.Enabled = False
                    btnCancelar.Enabled = True
                    btnGuardar.Enabled = True
                    btnEliminar.Enabled = True
                    HabilitarControles = True
                    btnImprimir.Enabled = False
            End Select


            For i = 0 To ctrl.GetLength(0) - 1
                Select Case ctrl(i).GetType.Name
                    Case "TextBox"
                        ctrl(i).readonly = Not (HabilitarControles)
                    Case "ocxTXTString"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTNumeric"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTDate"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ComboBox"
                        ctrl(i).enabled = HabilitarControles
                    Case "ocxCBX"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ListView"
                        'ctrl(i).enabled = HabilitarControles
                    Case "ocxTXTProveedor"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTCliente"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTProducto"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxFormaPago"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxFormaPagoLote"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxDesgloseBilletes"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCargaCheque"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCHK"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCotizacion"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTCuentaContable"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case Else
                        ctrl(i).Enabled = HabilitarControles
                End Select

            Next

            'Botones
            FormatoBoton(btnNuevo)
            FormatoBoton(btnEditar)
            FormatoBoton(btnCancelar)
            FormatoBoton(btnGuardar)
            FormatoBoton(btnEliminar)

            'Formato
            btnNuevo.Cursor = Cursors.Hand
            btnEditar.Cursor = Cursors.Hand
            btnCancelar.Cursor = Cursors.Hand
            btnGuardar.Cursor = Cursors.Hand
            btnEliminar.Cursor = Cursors.Hand

        Catch ex As Exception

        End Try

    End Sub

    Private Sub FormatoBoton(ByVal btn As Button)

        If btn.Enabled = False Then
            btn.Font = New Font(btn.Font.FontFamily, btn.Font.Size, FontStyle.Regular)
        Else
            btn.Font = New Font(btn.Font.FontFamily, btn.Font.Size, FontStyle.Bold)
        End If

    End Sub

    Sub ControlBotonesRegistro(ByVal Proceso As CSistema.NUMHabilitacionBotonesRegistros, ByVal btnNuevo As Button, ByVal btnGuardar As Button, ByVal btnCancelar As Button, ByVal btnAnular As Button, ByVal btnImprimir As Button, ByVal btnBuscar As Button, ByVal btnAsiento As Button, ByRef ctrl() As Object, Optional ByVal btnModificar As Button = Nothing, Optional ByVal btnEliminar As Button = Nothing, Optional ControlarFuncionNuevo As Boolean = True)

        'Control la funcion nuevo
        Try
            If ControlarFuncionNuevo = False Then
                Exit Try
            End If

            If Proceso <> NUMHabilitacionBotonesRegistros.NUEVO Then
                Exit Try
            End If

            If btnNuevo Is Nothing Then
                Exit Try
            End If

            If btnNuevo.Visible = True Then
                Exit Try
            End If

            'Si llegamos hasta aqui, no se tiene permisos y debemos salir
            'MessageBox.Show("No tiene privilegios para realizar esta operacion!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub

        Catch ex As Exception

        End Try

        Dim HabilitarControles As Boolean = False

        If btnModificar Is Nothing Then
            btnModificar = New Button
        End If

        If btnEliminar Is Nothing Then
            btnEliminar = New Button
        End If

        Try

            Select Case Proceso

                Case CSistema.NUMHabilitacionBotonesRegistros.INICIO
                    ' btnAnular.Enabled = False
                    btnAnular.Enabled = True
                    btnImprimir.Enabled = False
                    btnBuscar.Enabled = True
                    btnAsiento.Enabled = False
                    btnNuevo.Enabled = True
                    btnGuardar.Enabled = False
                    btnCancelar.Enabled = False
                    btnModificar.Enabled = False
                    btnEliminar.Enabled = False
                Case CSistema.NUMHabilitacionBotonesRegistros.NUEVO
                    btnAnular.Enabled = False
                    btnImprimir.Enabled = False
                    btnBuscar.Enabled = False
                    btnAsiento.Enabled = True
                    btnNuevo.Enabled = False
                    btnGuardar.Enabled = True
                    btnCancelar.Enabled = True
                    HabilitarControles = True
                    btnModificar.Enabled = False
                    btnEliminar.Enabled = False
                Case CSistema.NUMHabilitacionBotonesRegistros.CANCELAR
                    btnAnular.Enabled = True
                    btnImprimir.Enabled = True
                    btnBuscar.Enabled = True
                    btnAsiento.Enabled = True
                    btnNuevo.Enabled = True
                    btnGuardar.Enabled = False
                    btnCancelar.Enabled = False
                    btnModificar.Enabled = True
                    btnEliminar.Enabled = True
                Case CSistema.NUMHabilitacionBotonesRegistros.GUARDAR
                    btnAnular.Enabled = True
                    btnImprimir.Enabled = True
                    btnBuscar.Enabled = True
                    btnAsiento.Enabled = False
                    btnNuevo.Enabled = True
                    btnGuardar.Enabled = False
                    btnCancelar.Enabled = False
                    btnModificar.Enabled = True
                    btnEliminar.Enabled = True
                Case CSistema.NUMHabilitacionBotonesRegistros.BUSQUEDA
                    btnAnular.Enabled = True
                    btnImprimir.Enabled = True
                    btnBuscar.Enabled = True
                    btnAsiento.Enabled = True
                    btnNuevo.Enabled = True
                    btnGuardar.Enabled = False
                    btnCancelar.Enabled = False
                    btnModificar.Enabled = True
                    btnEliminar.Enabled = True
                Case CSistema.NUMHabilitacionBotonesRegistros.MODIFICAR
                    btnAnular.Enabled = False
                    btnImprimir.Enabled = False
                    btnBuscar.Enabled = False
                    btnAsiento.Enabled = True
                    btnNuevo.Enabled = False
                    btnGuardar.Enabled = True
                    btnCancelar.Enabled = True
                    btnModificar.Enabled = False
                    HabilitarControles = False
                    'Habilitar manualmente los campos que se quieran modificar!!!!
                    btnEliminar.Enabled = False
            End Select


            For i = 0 To ctrl.GetLength(0) - 1
                Select Case ctrl(i).GetType.Name
                    Case "TextBox"
                        ctrl(i).readonly = Not (HabilitarControles)
                    Case "ocxTXTString"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTNumeric"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTDate"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ComboBox"
                        ctrl(i).enabled = HabilitarControles
                    Case "ocxCBX"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ListView"
                        ctrl(i).CheckBoxes = HabilitarControles
                    Case "ocxTXTProveedor"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTCliente"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxTXTProducto"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxFormaPago"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxFormaPagoLote"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxDesgloseBilletes"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCargaCheque"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCHK"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "ocxCotizacion"
                        ctrl(i).SoloLectura = Not HabilitarControles
                    Case "TreeView"
                        ctrl(i).CheckBoxes = HabilitarControles
                    Case Else
                        ctrl(i).Enabled = HabilitarControles
                End Select

            Next

        Catch ex As Exception

        End Try

    End Sub

    Sub InicializaControles(ByVal Control As Control)

        For Each ctr As Object In Control.Controls

            'TextBox
            If ctr.GetType.Name = "TextBox" Then
                ctr.Text = ""
            End If

            'ComboBox
            If ctr.GetType.Name = "ComboBox" Then
                ctr.Text = ""
            End If

            'ListView
            If ctr.GetType.Name = "ListView" Then
                ctr.ITEMS.CLEAR()
            End If

            'ocxTXTString
            If ctr.GetType.Name = "ocxTXTString" Then
                ctr.texto = ""
            End If

            'ocxTXTNumeric
            If ctr.GetType.Name = "ocxTXTNumeric" Then
                ctr.texto = "0"
            End If

            'ocxTXTDate
            If ctr.GetType.Name = "ocxTXTDate" Then
                ctr.setvalue(VGFechaHoraSistema.ToShortDateString)
            End If

            'ocxCBX
            If ctr.GetType.Name = "ocxCBX" Then
                ctr.texto = ""
            End If

            'ocxTXTCuentaContable
            If ctr.GetType.Name = "ocxTXTCuentaContable" Then
                ctr.clear()
            End If

            'StatusStrip
            If ctr.GetType.Name = "StatusStrip" Then
                ctr.text = ""
            End If

        Next

    End Sub

    Sub InicializaControles(ByVal ctrls() As Control)

        For Each ctr As Object In ctrls

            'TextBox
            If ctr.GetType.Name = "TextBox" Then
                ctr.Text = ""
            End If

            'ComboBox
            If ctr.GetType.Name = "ComboBox" Then
                ctr.Text = ""
            End If

            'ListView
            If ctr.GetType.Name = "ListView" Then
                ctr.clear()
            End If

            'ocxTXTString
            If ctr.GetType.Name = "ocxTXTString" Then
                ctr.texto = ""
            End If

            'ocxTXTNumeric
            If ctr.GetType.Name = "ocxTXTNumeric" Then
                ctr.texto = "0"
            End If

            'ocxTXTDate
            If ctr.GetType.Name = "ocxTXTDate" Then
                ctr.setvalue(VGFechaHoraSistema.ToShortDateString)
            End If

            'ocxCBX
            If ctr.GetType.Name = "ocxCBX" Then
                ctr.texto = ""
            End If

            'ocxCHK
            If ctr.GetType.Name = "ocxCHK" Then
                ctr.valor = False
            End If

        Next

    End Sub

    Function CargaControles(ByVal control As Control) As Control()

        Dim i As Integer = 0
        Dim vControl(i)

        For Each ctr As Object In control.Controls

            vControl(i) = ctr

        Next

        Return vControl

    End Function

    Sub CargaControl(ByRef ctrls() As Control, ByVal ctr As Control)

        Dim i As Integer = ctrls.GetLength(0)

        ReDim Preserve ctrls(i)

        ctrls(i) = ctr


    End Sub

    Sub CargarRowEnControl(ByVal oRow As DataRow, ByVal index As String, ByRef ctr As Object, Optional ByVal ocxCBXSelectedValue As Boolean = False)

        Try
            Select Case ctr.GetType.Name

                'TextBox
                Case "TextBox"
                    ctr.Text = oRow(index).ToString

                    'ComboBox
                Case "ComboBox"
                    ctr.selectedvalue = oRow(index).ToString

                Case "ListView"

                    'ocxTXTString
                Case "ocxTXTString"
                    ctr.texto = oRow(index).ToString

                    'ocxTXTNumeric
                Case "ocxTXTNumeric"
                    ctr.txt.text = oRow(index).ToString

                    'ocxTXTDate
                Case "ocxTXTDate"
                    ctr.txt.text = oRow(index).ToString

                    'ocxTXTCuentaContable
                Case "ocxTXTCuentaContable"
                    ctr.SetValue(oRow(index).ToString)

                    'ocxCBX
                Case "ocxCBX"
                    If ocxCBXSelectedValue = True Then
                        ctr.SelectedValue(oRow(index).ToString)
                    Else
                        ctr.texto = oRow(index).ToString
                    End If


                    'RadioButton
                Case "RadioButton"
                    ctr.checked = oRow(index).ToString


            End Select


        Catch ex As Exception

        End Try


    End Sub

    Function MonedaDecimal(IDMoneda As Integer) As Boolean

        Try
            Dim CData As New CData

            Dim oRow As DataRow = CData.GetRow("ID=" & IDMoneda, "VMoneda")
            MonedaDecimal = RetornarValorBoolean(oRow("Decimales").ToString)

        Catch ex As Exception
            MonedaDecimal = False
        End Try


    End Function

    Public Shared Function Image2Bytes(ByVal img As Bitmap) As Byte()

        Dim sTemp As String = IO.Path.GetTempFileName()
        Dim fs As New IO.FileStream(sTemp, IO.FileMode.OpenOrCreate, IO.FileAccess.ReadWrite)
        img.Save(fs, System.Drawing.Imaging.ImageFormat.Png)
        fs.Position = 0

        Dim imgLength As Integer = CInt(fs.Length)
        Dim bytes(0 To imgLength - 1) As Byte
        fs.Read(bytes, 0, imgLength)
        fs.Close()
        Return bytes

    End Function
    'Convierte un tipo de Arrary de Byte() a imagen
    Public Shared Function Bytes2Image(ByVal bytes() As Byte) As Image

        If bytes Is Nothing Then Return Nothing
        '
        Dim ms As New MemoryStream(bytes)
        Dim bm As Bitmap = Nothing
        Try
            bm = New Bitmap(ms)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.Message)
        End Try
        Return bm

    End Function


    Function FormatoMoneda(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoMoneda = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Or CDec(valor) < -1 Then
                    valor = CStr(Format(CDec(valor), "###,###,###.##"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.##"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If

        Catch ex As Exception

        End Try

        FormatoMoneda = valor

    End Function

    Function FormatoMoneda(ByVal valor As String, ByVal decimales As Boolean, Optional ByVal CantidadDecimales As Integer = 2) As String

        FormatoMoneda = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                Dim vDecimales As String = ""

                For i As Integer = 1 To CantidadDecimales
                    vDecimales = vDecimales & "#"
                Next

                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###,###,###." & vDecimales))
                Else
                    valor = CStr(Format(CDec(valor), "0." & vDecimales))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If



        Catch ex As Exception

        End Try

        FormatoMoneda = valor

    End Function

    Function FormatoMoneda3Decimales(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoMoneda3Decimales = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###,###,###.###"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.###"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If

        Catch ex As Exception

        End Try

        FormatoMoneda3Decimales = valor

    End Function

    Function FormatoMoneda2Decimales(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoMoneda2Decimales = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###,###,###.##"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.##"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If

        Catch ex As Exception

        End Try

        FormatoMoneda2Decimales = valor

    End Function

    Sub FormatoNumero(ByVal lv As ListView, ByVal Columna As Integer, Optional ByVal decimales As Boolean = False)

        Try


            For Each item As ListViewItem In lv.Items
                item.SubItems(Columna).Text = FormatoNumero(item.SubItems(Columna).Text, decimales)
            Next

            'Formato
            lv.Columns(Columna).TextAlign = HorizontalAlignment.Right

        Catch ex As Exception

        End Try

    End Sub

    Function FormatoNumero(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoNumero = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 0 Then
                    valor = CStr(Format(CDec(valor), "###,###,###.##"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.##"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###,###,###"))
            End If

        Catch ex As Exception

        End Try

        FormatoNumero = valor

    End Function

    Function ExtraerDecimal(ByVal valor As String) As Long

        Dim ParteEntera As Long
        Dim ParteDecimal As Double
        'Dim ImporteFinal As Integer
        Dim ImporteFinal As Long

        ParteEntera = CLng(valor)
        ParteDecimal = valor - ParteEntera

        If ParteDecimal < 0 Then
            ParteDecimal = ParteDecimal * -1
        End If
        ImporteFinal = valor - ParteDecimal

        Return ImporteFinal

    End Function

    Function FormatoMonedaBaseDatos(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoMonedaBaseDatos = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###.####"))
                Else
                    'valor = CDec(valor).ToString
                    valor = CStr(Format(CDec(valor), "0.####"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###"))
            End If

            valor = valor.Replace(".", "")
            valor = valor.Replace(",", ".")

        Catch ex As Exception

        End Try

        FormatoMonedaBaseDatos = valor

    End Function

    Function FormatoMonedaBaseDatos(ByVal valor As String, ByVal cbxMoneda As ERP.ocxCBX) As String

        FormatoMonedaBaseDatos = "0"

        Dim Decimales As Boolean = False
        Dim CData As New CData

        Try
            Dim IDMoneda As Integer = cbxMoneda.GetValue
            Dim oRow As DataRow = CData.GetRow("ID=" & IDMoneda, "VMoneda")
            Decimales = RetornarValorBoolean(oRow("Decimales").ToString)
            valor = FormatoMonedaBaseDatos(valor, Decimales)

        Catch ex As Exception
            valor = "0"
        End Try

        FormatoMonedaBaseDatos = valor

    End Function

    Function FormatoNumeroBaseDatos(ByVal valor As String, Optional ByVal decimales As Boolean = False) As String

        FormatoNumeroBaseDatos = "0"

        Try
            If IsNumeric(valor) = False Then
                valor = "0"
                Exit Function
            End If

            If CDec(valor) = 0 Then
                valor = "0"
                Exit Function
            End If

            If decimales = True Then
                If CDec(valor) > 1 Then
                    valor = CStr(Format(CDec(valor), "###.####"))
                Else
                    valor = CStr(Format(CDec(valor), "0.####"))
                End If

            Else
                valor = CStr(Format(CDec(valor), "###"))
            End If

            valor = valor.Replace(".", "")
            valor = valor.Replace(",", ".")

        Catch ex As Exception

        End Try

        FormatoNumeroBaseDatos = valor

    End Function

    Function FormatoFechaBaseDatos(ByVal dtp As DateTimePicker, ByVal Fecha As Boolean, ByVal Hora As Boolean) As String

        Dim Retorno As String = ""

        Retorno = FormatoFechaBaseDatos(dtp.Value, True, False)

        Return Retorno

    End Function

    Function FormatoFechaBaseDatos(ByVal value As Date, ByVal Fecha As Boolean, ByVal Hora As Boolean) As String

        Dim Retorno As String = ""

        If Fecha = True Then
            Retorno = value.Year & FormatDobleDigito(value.Month) & FormatDobleDigito(value.Day)
        End If

        If Hora = True Then
            If Retorno = "" Then
                Retorno = value.Hour & ":" & value.Minute & ":" & value.Second
            Else
                Retorno = Retorno & " " & value.Hour & ":" & value.Minute & ":" & value.Second
            End If
        End If

        Return Retorno

    End Function

    Function FormatoFechaDesdeHastaBaseDatos(ByVal dtp As DateTimePicker, Optional ByVal Desde As Boolean = True, Optional ByVal Hasta As Boolean = False) As String

        Dim Retorno As String = ""

        Retorno = FormatoFechaBaseDatos(dtp.Value, True, False)

        If Desde = True Then
            Retorno = Retorno & " 00:00:00.000"
        End If

        If Hasta = True Then
            Retorno = Retorno & " 23:59:59.999"
        End If

        Return Retorno

    End Function

    Function FormatoFechaDesdeHastaBaseDatos(ByVal value As Date, Optional ByVal Desde As Boolean = True, Optional ByVal Hasta As Boolean = False) As String

        Dim Retorno As String = ""

        Retorno = FormatoFechaBaseDatos(value, True, False)

        If Desde = True Then
            Retorno = Retorno & " 00:00:00.000"
        End If

        If Hasta = True Then
            Retorno = Retorno & " 23:59:59.999"
        End If

        Return Retorno

    End Function

    Function FormatoFechaBaseDatos(ByVal valor As String, ByVal Fecha As Boolean, ByVal Hora As Boolean) As String

        Dim Retorno As String = ""
        Dim dtp As DateTime

        If IsDate(valor) = False Then
            dtp = Date.Now
        Else
            dtp = CDate(valor)
        End If

        Retorno = FormatoFechaBaseDatos(dtp, Fecha, Hora)

        Return Retorno

    End Function

    Public Function EstablecerFechaPredeterminada(ByVal Rango As NUMFechaPreestablecida) As Date
        EstablecerFechaPredeterminada = VGFechaHoraSistema

        Select Case Rango
            Case NUMFechaPreestablecida.HOY
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.Day & "/" & EstablecerFechaPredeterminada.Month & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
            Case NUMFechaPreestablecida.AYER
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.Day & "/" & EstablecerFechaPredeterminada.Month & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.AddDays(-1)
            Case NUMFechaPreestablecida.ESTA_SEMANA
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.Day & "/" & EstablecerFechaPredeterminada.Month & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
                Dim Dia As Integer = EstablecerFechaPredeterminada.DayOfWeek - 1
                EstablecerFechaPredeterminada = EstablecerFechaPredeterminada.AddDays(-Dia)
            Case NUMFechaPreestablecida.ESTE_MES
                EstablecerFechaPredeterminada = "01/" & EstablecerFechaPredeterminada.Month & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
            Case NUMFechaPreestablecida.ESTE_AÑO
                EstablecerFechaPredeterminada = "01/01" & "/" & EstablecerFechaPredeterminada.Year & " 00:00:00"
        End Select

    End Function

    Sub FormatoMoneda(ByVal lv As ListView, ByVal Columna As Integer, Optional ByVal ColumnaDecimal As Integer = 0)

        Try


            For Each item As ListViewItem In lv.Items
                If ColumnaDecimal = 0 Then
                    item.SubItems(Columna).Text = FormatoMoneda(item.SubItems(Columna).Text, False)
                Else
                    item.SubItems(Columna).Text = FormatoMoneda(item.SubItems(Columna).Text, item.SubItems(ColumnaDecimal).Text)
                End If

            Next

            'Formato
            lv.Columns(Columna).TextAlign = HorizontalAlignment.Right
            If ColumnaDecimal <> 0 Then
                lv.Columns(ColumnaDecimal).Width = 0
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub FormatoMoneda(ByVal lv As ListView, ByVal Columna As String, Optional ByVal ColumnaDecimal As Integer = 0)

        Try


            For Each item As ListViewItem In lv.Items
                If ColumnaDecimal = 0 Then
                    item.SubItems(lv.Columns(Columna).Index).Text = FormatoMoneda(item.SubItems(lv.Columns(Columna).Index).Text, False)
                Else
                    item.SubItems(lv.Columns(Columna).Index).Text = FormatoMoneda(item.SubItems(lv.Columns(Columna).Index).Text, item.SubItems(lv.Columns(Columna).Index).Text)
                End If

            Next

            'Formato
            lv.Columns(Columna).TextAlign = HorizontalAlignment.Right
            If ColumnaDecimal <> 0 Then
                lv.Columns(ColumnaDecimal).Width = 0
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub TotalesLv(ByVal lv As ListView, ByVal txt As TextBox, ByVal Columna As Integer, Optional ByVal Moneda As Boolean = False)

        Dim Total As Decimal = 0

        For Each item As ListViewItem In lv.Items

            Dim value As String
            value = item.SubItems(Columna).Text

            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            txt.Text = FormatoNumero(CStr(Total))
        Else
            txt.Text = FormatoMoneda(CStr(Total))
        End If

    End Sub

    Sub TotalesLv(ByVal lv As ListView, ByVal txt As TextBox, ByVal Columna As String, Optional ByVal Moneda As Boolean = False)

        Dim Total As Decimal = 0

        For Each item As ListViewItem In lv.Items

            Dim value As String
            value = lv.Items(item.Index).SubItems(lv.Columns(Columna).Index).Text

            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            txt.Text = FormatoNumero(CStr(Total))
        Else
            txt.Text = FormatoMoneda(CStr(Total))
        End If

    End Sub

    Sub TotalesGrid(ByVal dg As DataGridView, ByVal txt As TextBox, ByVal Columna As Integer, Optional ByVal Moneda As Boolean = False)

        Dim Total As Decimal = 0

        For i As Integer = 0 To dg.Rows.Count - 1

            Dim value As String
            value = dg.Rows(i).Cells(Columna).Value.ToString


            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            txt.Text = FormatoNumero(CStr(Total))
        Else
            txt.Text = FormatoMoneda(CStr(Total))
        End If

    End Sub

    Function TotalesDataTable(ByVal dt As DataTable, ByVal Columna As String, Optional ByVal Moneda As Boolean = False) As String

        Dim Total As Decimal = 0

        For Each oRow As DataRow In dt.Rows

            Dim value As String
            value = oRow(Columna).ToString

            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            Return FormatoNumero(CStr(Total))
        Else
            Return FormatoMoneda(CStr(Total))
        End If

    End Function

    Function TotalesDataTable(ByVal dt As DataView, ByVal Columna As String, Optional ByVal Moneda As Boolean = False, Optional ByVal Decimales As Boolean = False) As String

        Dim Total As Decimal = 0

        For Each oRow As DataRowView In dt

            Dim value As String
            value = oRow(Columna).ToString

            If IsNumeric(value) = False Then
                value = "0"
            End If

            Total = Total + CDec(value)

        Next

        If Moneda = False Then
            Return FormatoNumero(CStr(Total), Decimales)
        Else
            Return FormatoMoneda(CStr(Total), Decimales)
        End If

    End Function

    Sub ShowForm(ByVal frmClient As Form, ByVal frmParent As Form)

        Dim frm As New Form
        frm = frmClient

        frm.WindowState = FormWindowState.Normal
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.StartPosition = FormStartPosition.CenterParent
        frm.ShowDialog(frmParent)

    End Sub

    Sub LvPintarAnulados(ByVal lv As ListView, ByVal colum As Integer, ByVal Filtro As String)
        Try
            If colum > lv.Columns.Count - 1 Then
                Exit Sub
            End If

            For Each item As ListViewItem In lv.Items

                Dim valor As String = item.SubItems(colum).Text
                If valor.Contains(Filtro) = True Then
                    item.BackColor = Color.LightYellow
                End If

            Next

        Catch ex As Exception

        End Try



    End Sub

    Function GetDateTimeString() As String
        GetDateTimeString = Date.Now.Year & Date.Now.Month & Date.Now.Day & "_" & Date.Now.Hour & Date.Now.Minute & Date.Now.Second
    End Function

    Function GetDateTimeFormatString(Optional ByVal value As String = "") As String

        If value = "" Then
            GetDateTimeFormatString = Date.Now.Day & "-" & Date.Now.Month & "-" & Date.Now.Year & " " & Date.Now.Hour & ":" & Date.Now.Minute
        Else
            If IsDate(value) = False Then
                GetDateTimeFormatString = Date.Now.Day & "-" & Date.Now.Month & "-" & Date.Now.Year & " " & Date.Now.Hour & ":" & Date.Now.Minute
            Else
                Dim d As Date = CDate(value)
                GetDateTimeFormatString = FormatDobleDigito(d.Day.ToString) & "-" & FormatDobleDigito(d.Month.ToString) & "-" & FormatDobleDigito(d.Year.ToString) & " " & FormatDobleDigito(d.Hour.ToString) & ":" & FormatDobleDigito(d.Minute.ToString)
            End If
        End If

    End Function

    Function FormatDobleDigito(ByVal value As String) As String

        FormatDobleDigito = "00"

        If IsNumeric(value) = False Then
            Exit Function
        End If

        If CInt(value) < 10 Then
            FormatDobleDigito = "0" & value
        Else
            FormatDobleDigito = value
        End If

    End Function

    Function CalcularIVA(ByVal IDImpuesto As Integer, ByVal Monto As Decimal, ByVal Decimales As Boolean, ByVal Redondear As Boolean, Optional ByRef TotalDiscriminado As Decimal = -1, Optional ByRef TotalImpuesto As Decimal = -1) As Decimal

        CalcularIVA = 0

        'Obtenermos valores de la Base de Datos
        Dim CData As New CData
        'Dim dt As DataTable = FiltrarDataTable(CData.GetTable("VImpuesto").Copy, " ID=" & IDImpuesto)
        Dim dt As DataTable = ExecuteToDataTable("Select * from VImpuesto where ID= " & IDImpuesto)
        Dim oRow As DataRow = dt.Rows(0)
        Dim FactorDiscrimindado As Decimal = CDec(oRow("FactorDiscriminado").ToString)
        Dim FactorImpuesto As Decimal = CDec(oRow("FactorImpuesto").ToString)

        CalcularIVA = Monto * FactorDiscrimindado

        If TotalDiscriminado >= 0 Then
            TotalDiscriminado = Monto / FactorDiscrimindado
        End If

        If TotalImpuesto >= 0 Then
            TotalImpuesto = Monto - TotalDiscriminado
        End If

        If Decimales = False Then
            Redondear = True
        Else
            Redondear = False
        End If

        If Redondear = True Then

            If TotalImpuesto >= 0 Then
                TotalImpuesto = CDec(Math.Round(TotalImpuesto, 0))
            End If

            'Si debe ser entero, el total discriminado debe ser el TOTAL - IMPUESTO
            If TotalDiscriminado >= 0 Then
                TotalDiscriminado = CDec(Math.Round(TotalDiscriminado, 0))
            End If

            CalcularIVA = CDec(Math.Round(CalcularIVA, 0))

        Else
            If TotalImpuesto >= 0 Then
                TotalImpuesto = CDec(Math.Round(TotalImpuesto, 2))
            End If

            'Si debe ser entero, el total discriminado debe ser el TOTAL - IMPUESTO
            If TotalDiscriminado >= 0 Then
                TotalDiscriminado = CDec(Math.Round(TotalDiscriminado, 2))
            End If

            CalcularIVA = CDec(Math.Round(CalcularIVA, 2))
        End If

    End Function



    Function CalcularPorcentaje(ByVal Importe As Decimal, ByVal Porcentaje As Decimal, ByVal Decimales As Boolean, ByVal Redondear As Boolean) As Decimal

        CalcularPorcentaje = 0
        Porcentaje = Porcentaje / 100
        Dim Descuento As Decimal = Importe * Porcentaje

        CalcularPorcentaje = Descuento

        If Decimales = False Then
            Redondear = True
        End If

        If Redondear = True Then
            CalcularPorcentaje = CInt(Math.Round(CalcularPorcentaje, 0))
        End If

    End Function

    Function CalcularSinIVA(ByVal IDImpuesto As Integer, ByVal Monto As Decimal, ByVal Decimales As Boolean, ByVal Redondear As Boolean) As Decimal

        CalcularSinIVA = 0

        'Obtenermos valores de la Base de Datos
        Dim dt As DataTable = ExecuteToDataTable("Select FactorDiscriminado, FactorImpuesto From Impuesto Where ID=" & IDImpuesto)

        If dt Is Nothing Then
            Return 0
        End If

        If dt.Rows.Count = 0 Then
            Return 0
        End If

        Dim oRow As DataRow = dt.Rows(0)

        Dim FactorDiscrimindado As Decimal = CDec(oRow("FactorDiscriminado").ToString)

        CalcularSinIVA = Monto / FactorDiscrimindado

        If Decimales = False Then
            Redondear = True
        End If

        If Redondear = True Then

            CalcularSinIVA = CInt(Math.Round(CalcularSinIVA, 0))

        End If

    End Function

    'Calcula Archivo
    Function CalculaTamaño(ByVal valor As Decimal, ByRef Unidad As String) As Decimal

        CalculaTamaño = 0
        Dim Resultado As Decimal = valor
        Resultado = Resultado / 1024

        Select Case Unidad
            Case "Bytes"
                Unidad = "KBytes"
            Case "KBytes"
                Unidad = "MBytes"
            Case "MBytes"
                Unidad = "GBytes"
            Case "GBytes"
                Unidad = "TBytes"
        End Select

        If Resultado >= 1000 Then
            Resultado = CalculaTamaño(Resultado, Unidad)
        End If

        Return Resultado

    End Function
    Function dtSumColumn(ByVal dt As DataTable, ByVal column As String) As Decimal

        dtSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each oRow As DataRow In dt.Rows

                If IsNumeric(oRow(column).ToString) = False Then
                    GoTo siguiente
                End If

                temp = temp + CDec(oRow(column).ToString)

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function dtCountDistinct(ByVal dt As DataTable, ByVal column As String) As Integer

        dtCountDistinct = 0

        Dim temp As Integer = 0
        Dim i As Integer = 0
        Dim Vector(-1) As String

        Try

            For Each oRow As DataRow In dt.Rows

                If Vector.Contains(oRow(column).ToString) = False Then

                    ReDim Preserve Vector(i)

                    Vector(i) = oRow(column).ToString

                    i = i + 1

                End If
siguiente:

            Next

            temp = Vector.GetLength(0)

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function dtSumColumn(ByVal dt As DataTable, ByVal column As String, ByVal Condicion As String) As Decimal

        dtSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each oRow As DataRow In dt.Select(Condicion)

                If IsNumeric(oRow(column).ToString) = False Then
                    GoTo siguiente
                End If

                temp = temp + CDec(oRow(column).ToString)

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function dtSumColumn(ByVal dt As DataTable, ByVal column As String, ByVal Condicion As String, ByVal ColumnaCondicion As String) As Decimal

        dtSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each oRow As DataRow In dt.Rows

                If Condicion <> oRow(ColumnaCondicion).ToString Then
                    GoTo siguiente
                End If

                If IsNumeric(oRow(column).ToString) = False Then
                    GoTo siguiente
                End If

                temp = temp + CDec(oRow(column).ToString)

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function gridSumColumn(ByVal dg As DataGridView, ByVal column As String) As Decimal

        gridSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each dgv As DataGridViewRow In dg.Rows

                If IsNumeric(dgv.Cells(column).Value) = False Then
                    GoTo siguiente
                End If

                temp = temp + CDec(dgv.Cells(column).Value)

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function gridSumColumn(ByVal dg As DataGridView, ByVal column As String, ByVal ColumnaCondicion As String, ByVal Valor As String) As Decimal

        gridSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each dgv As DataGridViewRow In dg.Rows

                If dgv.Cells(ColumnaCondicion).Value = Valor Then

                    If IsNumeric(dgv.Cells(column).Value) = False Then
                        GoTo siguiente
                    End If

                    temp = temp + CDec(dgv.Cells(column).Value)

                End If

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Function gridCountRows(ByVal dg As DataGridView, ByVal ColumnaCondicion As String, ByVal Valor As String) As Decimal

        gridCountRows = 0

        Dim temp As Decimal = 0

        Try

            For Each dgv As DataGridViewRow In dg.Rows

                If dgv.Cells(ColumnaCondicion).Value = Valor Then

                    temp = temp + 1

                End If

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function


    Function gridSumColumn(ByVal dg As DataGridView, ByVal column As String, ByVal ColumnaCondicion As String, ByVal Operacion As String, ByVal Valor As String) As Decimal

        gridSumColumn = 0

        Dim temp As Decimal = 0

        Try

            For Each dgv As DataGridViewRow In dg.Rows

                If IsNumeric(dgv.Cells(column).Value) = False Then
                    GoTo siguiente
                End If

                'Igual
                Select Case Operacion

                    Case "="
                        If dgv.Cells(ColumnaCondicion).Value = Valor Then
                            temp = temp + CDec(dgv.Cells(column).Value)
                        End If

                    Case "<"
                        If dgv.Cells(ColumnaCondicion).Value < Valor Then
                            temp = temp + CDec(dgv.Cells(column).Value)
                        End If
                    Case ">"
                        If dgv.Cells(ColumnaCondicion).Value > Valor Then
                            temp = temp + CDec(dgv.Cells(column).Value)
                        End If
                    Case "<>"
                        If dgv.Cells(ColumnaCondicion).Value <> Valor Then
                            temp = temp + CDec(dgv.Cells(column).Value)
                        End If
                End Select

siguiente:

            Next

            Return temp

        Catch ex As Exception

        End Try

    End Function

    Public Function RetornarValorBoolean(ByVal valor As String) As Boolean

        RetornarValorBoolean = False

        Try
            If valor Is Nothing Then
                Return False
            End If

            If valor.Length = 0 Then
                Return False
            End If

            If valor.ToUpper <> "FALSE" Then
                If valor.ToUpper <> "TRUE" Then
                    Return False
                End If
            End If

            Return CBool(valor)

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function RetornarValorInteger(ByVal valor As String) As Integer

        RetornarValorInteger = 0

        Try
            If valor Is Nothing Then
                Return 0
            End If

            If valor.Length = 0 Then
                Return 0
            End If

            If IsNumeric(valor) = False Then
                Return 0
            End If

            Return CDec(valor)

        Catch ex As Exception
            Return 0
        End Try

    End Function

    Public Function RetornarValorString(ByVal valor As String) As String

        RetornarValorString = ""

        Try
            If valor Is Nothing Then
                Return ""
            End If

            If valor.Length = 0 Then
                Return ""
            End If

            Return valor

        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Function Cotizador(ByVal Cotizacion As String, ByVal Importe As String, ByVal IDMoneda As Integer) As String

        Cotizador = "0"

        If IsNumeric(Cotizacion) = False Then
            Exit Function
        End If

        If IsNumeric(Importe) = False Then
            Exit Function
        End If

        'Si la moneda es la principal, multiplicar por 1
        If IDMoneda = 1 Then
            Return Importe * 1
        End If

        Dim CData As New CData
        Dim oRow As DataRow = CData.GetRow("ID=" & IDMoneda, "VMoneda")

        'Verificar el operador
        'Si divide
        If RetornarValorBoolean(oRow("Divide").ToString) = True Then

            Cotizador = CStr(CDec(Importe) / CDec(Cotizacion))

        End If

        'Si multiplica
        If RetornarValorBoolean(oRow("Multiplica").ToString) = True Then

            Cotizador = CStr(CDec(Importe) * CDec(Cotizacion))

        End If

        If RetornarValorBoolean(oRow("Decimales").ToString) = False Then
            Cotizador = CStr(Math.Round(CDec(Cotizador), 0))
        End If

    End Function

    Public Function Cotizador(ByVal Cotizacion As String, ByVal Importe As String, ByVal IDMoneda1 As Integer, ByVal IDMoneda2 As Integer) As String

        Cotizador = "0"

        If IsNumeric(Cotizacion) = False Then
            Exit Function
        End If

        If IsNumeric(Importe) = False Then
            Exit Function
        End If

        Dim CData As New CData
        Dim oRow1 As DataRow = CData.GetRow("ID=" & IDMoneda1, "VMoneda")
        Dim oRow2 As DataRow = CData.GetRow("ID=" & IDMoneda2, "VMoneda")

        'Verificar el operador
        'Si divide
        If RetornarValorBoolean(oRow1("Divide").ToString) = True Then

            Cotizador = CStr(CDec(Importe) / CDec(Cotizacion))

        End If

        'Si multiplica
        If RetornarValorBoolean(oRow1("Multiplica").ToString) = True Then

            Cotizador = CStr(CDec(Importe) * CDec(Cotizacion))

        End If

        If RetornarValorBoolean(oRow2("Decimales").ToString) = False Then
            Cotizador = CStr(Math.Round(CDec(Cotizador), 0))
        End If

    End Function

    Public Function CotizacionDelDia(IDMoneda As Decimal, IDOperacion As Integer) As Decimal

        CotizacionDelDia = 1

        If IDMoneda = 1 Then
            Return 1
        End If

        CotizacionDelDia = ExecuteScalar("Select " & vgOwnerDBFunction & ".FCotizacionAlDia(" & IDMoneda & ", " & IDOperacion & ")")

    End Function
    Public Function CotizacionDelDiaFecha(IDMoneda As Decimal, IDOperacion As Integer, Fecha As String) As Integer

        CotizacionDelDiaFecha = 1

        If IDMoneda = 1 Then
            Return 1
        End If

        CotizacionDelDiaFecha = ExecuteScalar("Select IsNull(max(Cotizacion),1) from Cotizacion where IDMoneda = " & IDMoneda & " and cast(fecha as date) =" & Fecha)

    End Function

    Public Sub SelectNextControl(ByVal frm As Form, Optional Key As Keys = Keys.ControlKey)

        'Si el control es especial, no cambiar, lo hace solo
        '(ocxImpuestos, ocxCuentaContable, ocxTXTCliente, ocxTXTProveedor)
        Dim tipo As String = frm.ActiveControl.GetType.Name
        Dim ctr As Object = Nothing


        Select Case tipo
            Case "ocxImpuesto"
                Exit Sub
            Case "ocxCuentaContable"
                Exit Sub
            Case "ocxTXTProveedor"
                Exit Sub
            Case "ocxTXTProducto"
                Exit Sub
            Case "ocxTXTCliente"
                ctr = frm.ActiveControl
                If ctr.SoloLectura = False Then
                    Exit Sub
                End If
            Case "Button"
                Exit Sub
            Case "ocxSeleccionarEfectivo"
                Exit Sub
            Case "ocxConexionBaseDeDatos"
                SelectNextControl(frm.ActiveControl)
                Exit Sub
            Case "ocxCotizacion"
                If CObj(frm.ActiveControl).Saltar = False Then
                    Exit Sub
                End If
        End Select


        Select Case Key
            Case Keys.Enter
                frm.SelectNextControl(frm.ActiveControl, True, True, True, True)

            Case Keys.Right
                If tipo = "ocxCBX" Then
                    frm.SelectNextControl(frm.ActiveControl, True, True, True, True)
                End If

                If tipo = "ocxTXTString" Then
                    Dim txt As ocxTXTString = frm.ActiveControl
                    If txt.txt.SelectionStart = txt.txt.Text.Length Then
                        frm.SelectNextControl(frm.ActiveControl, True, True, True, True)
                    End If
                End If
            Case Keys.Left
                If tipo = "ocxCBX" Then
                    frm.SelectNextControl(frm.ActiveControl, False, True, True, True)
                End If

                If tipo = "ocxTXTString" Then
                    Dim txt As ocxTXTString = frm.ActiveControl
                    If txt.txt.SelectionStart = 0 Then
                        frm.SelectNextControl(frm.ActiveControl, False, True, True, True)
                    End If

                End If

            Case Keys.Down
                If tipo = "ocxTXTString" Or tipo = "ocxTXTNumeric" Or tipo = "ocxTXTDate" Then
                    frm.SelectNextControl(frm.ActiveControl, True, True, True, True)
                End If
            Case Keys.Up
                If tipo = "ocxTXTString" Or tipo = "ocxTXTNumeric" Or tipo = "ocxTXTDate" Then
                    frm.SelectNextControl(frm.ActiveControl, False, True, True, True)
                End If
        End Select


    End Sub

    Public Sub SelectNextControl(ByVal Control As Control)

        'Si el control es especial, no cambiar, lo hace solo
        '(ocxImpuestos, ocxCuentaContable, ocxTXTCliente, ocxTXTProveedor)

        Dim tipo As String = Control.GetType.ToString
        Dim ctr As Object = Nothing

        Select Case tipo
            Case "ocxImpuesto"
                Exit Sub
            Case "ocxCuentaContable"
                Exit Sub
            Case "ocxTXTProveedor"
                Exit Sub
            Case "ocxTXTProducto"
                Exit Sub
            Case "ocxTXTCliente"
                ctr = Control
                If ctr.SoloLectura = False Then
                    Exit Sub
                End If
            Case "Button"
                Exit Sub
            Case "ocxSeleccionarEfectivo"
                Exit Sub
            Case "ocxCotizacion"
                If CObj(Control).Saltar = False Then
                    Exit Sub
                End If
        End Select

        Control.SelectNextControl(Control, True, True, True, True)


    End Sub

    Public Function ObtenerDescuentoTacticoPlanilla(ByVal IDProducto As Integer, ByVal IDSucursal As Integer, Fecha As Date) As Decimal

        ObtenerDescuentoTacticoPlanilla = 0

        'Variables
        Dim CData As New CData
        'Dim IDActividad As Integer = 0
        Dim dtDetalleActividad As DataTable = CData.GetTable("VDetalleActividad").Copy

        '***PARECE QUE SOLO DETALLEACTIVIDAD HAY QUE USAR

        'Verificar que la informacion sea correcta
        If dtDetalleActividad Is Nothing Then
            Exit Function
        End If

        'Verificar si existe en el detalle
        If dtDetalleActividad.Select(" ID = " & IDProducto).Count = 0 Then
            Exit Function
        End If

        'Ahora Recorremos la tabla en busca de porcentajes validos
        Dim Porcentaje As Decimal = 0
        For Each oRow As DataRow In dtDetalleActividad.Select(" ID = " & IDProducto)
            'IDActividad = oRow("IDActividad")
            Dim Desde As Date = oRow("Desde")
            Dim Hasta As Date = oRow("Hasta")

            Dim Comparacion1 As Integer = Date.Compare(Fecha, Desde)
            Dim Comparacion2 As Integer = Date.Compare(Fecha, Hasta)

            If Comparacion1 >= 0 Then
                If Comparacion2 <= 0 Then

                    'Sumamos el porcentaje
                    Porcentaje = Porcentaje + oRow("DescuentoMaximo")

                End If
            End If

        Next

        Return Porcentaje

    End Function

    Public Function ObtenerDescuentoTacticoPlanillaExiste(ByVal IDProducto As Integer, ByVal IDSucursal As Integer) As Boolean

        ObtenerDescuentoTacticoPlanillaExiste = 0

        'Variables
        Dim CData As New CData
        Dim dtDetalleActividad As DataTable = CData.GetTable("VDetalleActividad").Copy

        'Verificar que la informacion sea correcta
        If dtDetalleActividad Is Nothing Then
            Return False
        End If

        'Verificar si existe en el detalle
        If dtDetalleActividad.Select(" IDProducto = " & IDProducto).Count = 0 Then
            Return False
        End If

        Return True

    End Function

    Public Function ObtenerDescuentoTacticoProducto(ByVal IDProducto As Integer, ByVal IDCliente As Integer, ByVal IDSucursal As Integer) As Decimal

        ObtenerDescuentoTacticoProducto = 0

        'Variables
        Dim CData As New CData
        Dim IDListaPrecio As String
        Dim Porcentaje As Decimal
        Dim dtCliente As DataTable = CData.GetTable("VCliente", " ID = " & IDCliente).Copy
        Dim dttemp As DataTable = CData.GetTable("VProductoListaPrecioExcepciones").Copy

        If dtCliente Is Nothing Then
            Return 0
        End If

        If dtCliente.Rows.Count = 0 Then
            Return 0
        End If

        IDListaPrecio = dtCliente.Rows(0)("IDListaPrecio")

        If IsNumeric(IDListaPrecio) = False Then
            Return 0
        End If


        For Each oRow As DataRow In dttemp.Select(" IDProducto=" & IDProducto & " And IDCliente=" & IDCliente & " And IDListaPrecio=" & IDListaPrecio & "  And IDTipoDescuento=1")
            Porcentaje = oRow("Porcentaje")
        Next

        Return Porcentaje

    End Function

    Public Function ObtenerOtrosDescuentoProducto(ByVal IDProducto As Integer, ByVal IDCliente As Integer, ByVal IDSucursal As Integer) As Decimal

        ObtenerOtrosDescuentoProducto = 0

        'Variables
        Dim CData As New CData
        Dim IDListaPrecio As String
        Dim Porcentaje As Decimal
        Dim dtCliente As DataTable = CData.GetTable("VCliente", " ID = " & IDCliente).Copy
        Dim dttemp As DataTable = CData.GetTable("VProductoListaPrecioExcepciones").Copy

        If dtCliente Is Nothing Then
            Return 0
        End If

        If dtCliente.Rows.Count = 0 Then
            Return 0
        End If

        IDListaPrecio = dtCliente.Rows(0)("IDListaPrecio")

        If IsNumeric(IDListaPrecio) = False Then
            Return 0
        End If


        For Each oRow As DataRow In dttemp.Select(" IDProducto=" & IDProducto & " And IDCliente=" & IDCliente & " And IDListaPrecio=" & IDListaPrecio & "  And IDTipoDescuento<>1")
            Porcentaje = Porcentaje + oRow("Porcentaje")
        Next

        Return Porcentaje

    End Function

    Public Sub MostrarPropiedades(ByVal frmPadre As Form, ByVal TituloVentana As String, ByVal Titulo As String, ByVal Consulta As String, ByVal dt As DataTable)

        Dim frm As New frmPropiedad
        frm.TituloVentana = TituloVentana
        frm.Titulo = Titulo
        frm.Consulta = Consulta
        If Consulta = "" Then
            frm.dt = dt
        End If

        frm.ShowDialog(frmPadre)

    End Sub

    Public Sub LvToExcel(ByVal lv As ListView)

        Try

            'DECLARACIONES
            Dim xlApp As Excel.Application
            Dim xlLibro As Excel.Workbook
            Dim xlHoja As Excel.Worksheet

            Dim i, f, cant, j As Integer
            xlApp = CType(CreateObject("Excel.application"), Excel.Application)
            xlLibro = CType(xlApp.Workbooks.Add, Excel.Workbook)
            xlHoja = CType(xlLibro.Worksheets(1), Excel.Worksheet)
            xlHoja.Name = "RESULTADO"
            f = 2
            cant = 0
            j = 2

            For i = 0 To lv.Columns.Count - 1
                xlHoja.Cells(f, i + 2) = lv.Columns(i).Text
                xlHoja.Cells(f, i + 2).Interior.ColorIndex = 15
                xlHoja.Cells(f, i + 2).Cells.HorizontalAlignment = 3
                xlHoja.Cells(f, i + 2).Font.Bold = True
                xlHoja.Cells(f, i + 2).Borders.LineStyle = 1
                xlHoja.Cells(f, i + 2).Columns.AutoFit()
            Next

            f = 3
            Dim item As ListViewItem
            For Each item In lv.Items
                For i = 0 To lv.Columns.Count - 1
                    xlHoja.Cells(f, i + 2) = item.SubItems(i).Text
                    xlHoja.Cells(f, i + 2).Borders.LineStyle = 1
                    xlHoja.Cells(f, i + 2).Columns.AutoFit()
                Next
                f += 1
            Next

            ''total
            'xlHoja.Cells(f, lv.Columns.Count).Interior.ColorIndex = 15
            'xlHoja.Cells(f, lv.Columns.Count).Cells.HorizontalAlignment = 3
            'xlHoja.Cells(f, lv.Columns.Count).Font.Bold = True
            'xlHoja.Cells(f, lv.Columns.Count).Borders.LineStyle = 1
            'xlHoja.Cells(f, lv.Columns.Count).Value = "Total"
            'xlHoja.Cells(f, lv.Columns.Count + 1).Value = cajasuma.Text
            'xlHoja.Cells(f, lv.Columns.Count + 12).Borders.LineStyle = 1


            'Mostrar Hoja de Trabajo
            xlHoja.Application.Visible = True
            xlHoja = Nothing

            'Prueba
            'xlHoja.PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA4
            'xlHoja.PrintOut()
            'xlHoja = Nothing

        Catch ex As Exception
            Try
                LvToOO(lv)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try
        End Try


    End Sub

    Public Sub LvToOO(ByVal lv As ListView)

        Dim COpenOffice As New COpenOffice

        Try
            COpenOffice.ConnectOpenOffice()

            Dim i, f, cant, j As Integer
            f = 2
            cant = 0
            j = 2

            Dim myDoc As Object, firstSheet As Object, aRange As Object
            Dim fields As Object, unoWrap As Object, sortDx As Object
            myDoc = COpenOffice.StarDesktop.loadComponentFromURL("private:factory/scalc", "_blank", 0, COpenOffice.dummyArray)
            firstSheet = myDoc.Sheets.getByIndex(0)

            'columnas
            Dim Columna = 2
            For i = 0 To lv.Columns.Count - 1
                If lv.Columns(i).Width > 0 Then
                    firstSheet.getCellByPosition(Columna, f).String = lv.Columns(i).Text
                    Columna += 1
                End If
            Next

            f = 3
            Columna = 2

            Dim item As ListViewItem
            For Each item In lv.Items
                For i = 0 To lv.Columns.Count - 1
                    If lv.Columns(i).Width > 0 Then
                        If IsNumeric(item.SubItems(i).Text) = True Then
                            Dim NumeroTemp As String = item.SubItems(i).Text.Replace(".", "")
                            firstSheet.getCellByPosition(Columna, f).value = NumeroTemp
                        Else
                            firstSheet.getCellByPosition(Columna, f).String = item.SubItems(i).Text.Replace(".", "")
                        End If

                        Columna += 1

                    End If
                Next

                Columna = 2
                f += 1

            Next


            'MsgBox(OOoMess107)
            aRange = firstSheet.getCellRangeByName("A1:B16")
            fields = COpenOffice.CreateUnoStruct("com.sun.star.table.TableSortField", 0)
            fields(0).Field = 1
            fields(0).IsAscending = True
            fields(0).IsCaseSensitive = True

            ' il faut prciser quel type de squence est transmis la proprit SortFields
            ' you must specify which type of sequence is transmitted to SortFields property
            unoWrap = COpenOffice.OpenOffice.Bridge_GetValueObject
            unoWrap.set("[]com.sun.star.table.TableSortField", fields)

            ' remplissage de SortDescriptor : proprits ayant des valeurs autres que dfaut
            ' filling of SortDescriptor : properties with non-default values 
            sortDx = COpenOffice.CreateProperties("ContainsHeader", True, "SortFields", unoWrap)
            aRange.sort(sortDx)

            ' MsgBox(OOoMess108)

            ' myDoc.close(True)
            COpenOffice.DisconnectOpenOffice()

        Catch ex As Exception
            MessageBox.Show("Se produjo el siguiente error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub




    Public Sub dtToExcel(ByVal dt As DataTable)

        Try

            'DECLARACIONES
            Dim xlApp As New Excel.Application
            Dim xlLibro As Excel.Workbook
            Dim xlHoja As New Excel.Worksheet

            Dim i, f, cant, j As Integer
            xlApp = CType(CreateObject("Excel.application"), Excel.Application)
            xlLibro = CType(xlApp.Workbooks.Add, Excel.Workbook)
            xlHoja = CType(xlLibro.Worksheets(1), Excel.Worksheet)
            xlHoja.Name = "RESULTADO"
            f = 2
            cant = 0
            j = 2

            For i = 0 To dt.Columns.Count - 1
                xlHoja.Cells(f, i + 2) = dt.Columns(i).ColumnName
                xlHoja.Cells(f, i + 2).Interior.ColorIndex = 15
                xlHoja.Cells(f, i + 2).Cells.HorizontalAlignment = 3
                xlHoja.Cells(f, i + 2).Font.Bold = True
                xlHoja.Cells(f, i + 2).Borders.LineStyle = 1
                xlHoja.Cells(f, i + 2).Columns.AutoFit()
            Next

            f = 3
            For Each oRow As DataRow In dt.Rows
                For i = 0 To dt.Columns.Count - 1
                    xlHoja.Cells(f, i + 2) = oRow(i).ToString
                    xlHoja.Cells(f, i + 2).Borders.LineStyle = 1
                    xlHoja.Cells(f, i + 2).Columns.AutoFit()
                Next
                f += 1
            Next

            xlHoja = Nothing

            'Mostrar Hoja de Trabajo
            xlHoja.Application.Visible = True

        Catch ex As Exception
            Try
                dtToOO(dt)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try
        End Try


    End Sub

    Function GridAExcel(ByVal ElGrid As DataGridView) As Boolean

        'Creamos las variables
        'Dim exApp As New Microsoft.Office.Interop.Excel.Application
        'Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        'Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet

        Dim exApp As New Excel.Application
        Dim exLibro As Excel.Workbook
        Dim exHoja As Excel.Worksheet

        Try
            'Añadimos el Libro al programa, y la hoja al libro
            exLibro = exApp.Workbooks.Add
            exHoja = exLibro.Worksheets.Add()

            ' ¿Cuantas columnas y cuantas filas?
            Dim NCol As Integer = ElGrid.ColumnCount
            Dim NRow As Integer = ElGrid.RowCount

            'Aqui recorremos todas las filas, y por cada fila todas las columnas y vamos escribiendo.
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(1, i) = ElGrid.Columns(i - 1).Name.ToString
                'exHoja.Cells.Item(1, i).HorizontalAlignment = 3
            Next

            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 2, Col + 1) = ElGrid.Rows(Fila).Cells(Col).Value
                Next
            Next
            'Titulo en negrita, Alineado al centro y que el tamaño de la columna se ajuste al texto
            exHoja.Rows.Item(1).Font.Bold = 1
            exHoja.Rows.Item(1).HorizontalAlignment = 3
            exHoja.Columns.AutoFit()



            'Aplicación visible
            exApp.Application.Visible = True

            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")

            Return False
        End Try

        Return True

    End Function

    Public Sub dtToExcel2(ByVal dt As DataTable, ByVal strFileName As String)     'DataTable -> Excel
        Dim workbook As Workbook = New Workbook()
        'Inicializar hoja de cálculo
        Dim sheet As Worksheet = workbook.Worksheets(0)
        sheet.InsertDataTable(dt, True, 2, 1, -1, -1)

        Dim savefile As New SaveFileDialog
        savefile.AddExtension = False
        savefile.Filter = "Excel|*.xls"
        savefile.FileName = strFileName
        savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

        If savefile.ShowDialog = DialogResult.OK Then
            workbook.SaveToFile(savefile.FileName, ExcelVersion.Version2007)
        End If

    End Sub


    Public Sub dtToExcel(ByVal ds As DataSet, ByVal strFileName As String)

        Try

            Dim _excel As Excel.Application
            Dim wBook As Excel.Workbook
            Dim wSheet As Excel.Worksheet

            _excel = CType(CreateObject("Excel.application"), Excel.Application)
            wBook = CType(_excel.Workbooks.Add, Excel.Workbook)
            wSheet = CType(wBook.Worksheets(1), Excel.Worksheet)

            For Each dttemp As DataTable In ds.Tables

                Dim dt As System.Data.DataTable = dttemp
                Dim dc As System.Data.DataColumn
                Dim dr As System.Data.DataRow
                Dim colIndex As Integer = 0
                Dim rowIndex As Integer = 0

                For Each dc In dt.Columns
                    colIndex = colIndex + 1
                    _excel.Cells(1, colIndex) = dc.ColumnName
                Next

                For Each dr In dt.Rows
                    rowIndex = rowIndex + 1
                    colIndex = 0
                    For Each dc In dt.Columns
                        colIndex = colIndex + 1
                        _excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                    Next
                Next

                wSheet.Columns.AutoFit()

                If System.IO.File.Exists(strFileName) Then
                    Try
                        System.IO.File.Delete(strFileName)
                    Catch ex As Exception

                    End Try
                End If

            Next

            Dim savefile As New SaveFileDialog
            savefile.AddExtension = False
            savefile.Filter = "Excel|*.xls"
            savefile.FileName = strFileName
            savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

            If savefile.ShowDialog = DialogResult.OK Then
                wBook.SaveAs(savefile.FileName)
            End If

            wBook.Close()
            _excel.Quit()

        Catch ex As Exception

            Try
                'dtToOO(dtTemp)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Try

    End Sub

    Public Sub dtToExcel3(ByVal ds As DataSet, ByVal strFileName As String, ByVal detallecant As Integer)

        Try

            Dim _excel As Excel.Application
            Dim wBook As Excel.Workbook
            Dim wSheet As Excel.Worksheet
            Dim contadorarchivo As Integer = 0
            Dim contadortotal As Integer = 0
            Dim savefile As New SaveFileDialog


            _excel = CType(CreateObject("Excel.application"), Excel.Application)
            wBook = CType(_excel.Workbooks.Add, Excel.Workbook)
            wSheet = CType(wBook.Worksheets(1), Excel.Worksheet)

            For Each dttemp As DataTable In ds.Tables

                Dim dt As System.Data.DataTable = dttemp
                Dim dc As System.Data.DataColumn
                Dim dr As System.Data.DataRow
                Dim colIndex As Integer = 0
                Dim rowIndex As Integer = 0
#Disable Warning BC42024 ' Variable local no usada
                Dim fecha As String
#Enable Warning BC42024 ' Variable local no usada
                Dim contadorregistro As Integer = 1
                Dim Topregistro As Integer = 5000


                For Each dc In dt.Columns
                    colIndex = colIndex + 1
                    _excel.Cells(1, colIndex) = dc.ColumnName
                Next

                For Each dr In dt.Rows
                    'Nuevo desde aca
                    If contadortotal <= detallecant Then

                        If contadorregistro <= Topregistro Then 'sc
                            contadorregistro = contadorregistro + 1
                        Else

                            savefile.AddExtension = False
                            savefile.Filter = "Excel|*.CSV"
                            If contadorregistro > Topregistro Then
                                contadorarchivo = contadorarchivo + 1
                                savefile.FileName = strFileName.Substring(0, 20) & "_" & contadorarchivo & ".CSV"
                            Else
                                savefile.FileName = strFileName
                            End If
                            savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

                            If savefile.ShowDialog = DialogResult.OK Then
                                wBook.SaveAs(savefile.FileName)
                            End If

                            wBook.Close()
                            _excel.Quit()

                            'Nueva hoja
                            _excel = CType(CreateObject("Excel.application"), Excel.Application)
                            wBook = CType(_excel.Workbooks.Add, Excel.Workbook)
                            wSheet = CType(wBook.Worksheets(1), Excel.Worksheet)

                            colIndex = 0

                            For Each dc In dt.Columns
                                colIndex = colIndex + 1
                                _excel.Cells(1, colIndex) = dc.ColumnName
                            Next
                            contadorregistro = 0
                            rowIndex = 0

                        End If
                    Else
                        GoTo salir
                    End If
                    'Nuevo hasta aca
                    rowIndex = rowIndex + 1
                    colIndex = 0
                    For Each dc In dt.Columns
                        'If dc.ColumnName = "FechaDocumento" Then
                        'fecha = Replace((dr(dc.ColumnName)), "/", "-")
                        'fecha = Convert.ToString(dr(dc.ColumnName))

                        'colIndex = colIndex + 1
                        '_excel.Cells(rowIndex + 1, colIndex) = Format(CDate(fecha), "yyyy/mm/dd")
                        '_excel.Cells(rowIndex + 1, colIndex) = Convert.ToString(fecha)
                        'Else
                        colIndex = colIndex + 1
                        _excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                        'End If
                    Next
                    contadortotal = contadortotal + 1
                Next

                wSheet.Columns.AutoFit()

                If System.IO.File.Exists(strFileName) Then
                    Try
                        System.IO.File.Delete(strFileName)
                    Catch ex As Exception

                    End Try
                End If

            Next
salir:
            'Dim savefile As New SaveFileDialog
            savefile.AddExtension = False
            savefile.Filter = "|*.CSV"
            If contadorarchivo > 0 Then
                contadorarchivo = contadorarchivo + 1
                savefile.FileName = strFileName.Substring(0, 20) & "_" & contadorarchivo & ".CSV"
            Else
                savefile.FileName = strFileName
            End If
            savefile.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.Desktop

            If savefile.ShowDialog = DialogResult.OK Then
                wBook.SaveAs(savefile.FileName)
            End If

            wBook.Close()
            _excel.Quit()

        Catch ex As Exception

            Try
                'dtToOO(dtTemp)
            Catch
                MessageBox.Show("No se encuentra ningun programa de calculo para exportar! Se requiere Microsoft Excel o Open Office para continuar", "ERP", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Try

    End Sub

    Public Sub dgvToPlanilla(dgv As DataGridView, nombre As String)

        Dim fic As String = VGCarpetaTemporal & "\" & nombre

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, False)

            Dim cadena As String = ""

            cadena = ""
            For c As Integer = 0 To dgv.Columns.Count - 1

                If c = 0 Then
                    cadena = cadena & dgv.Columns(c).HeaderText
                Else
                    cadena = cadena & vbTab & dgv.Columns(c).HeaderText
                End If

            Next

            sw.WriteLine(cadena)

            For Each oRow As DataGridViewRow In dgv.Rows
                cadena = ""
                For c As Integer = 0 To dgv.Columns.Count - 1

                    If c = 0 Then
                        cadena = cadena & oRow.Cells(c).Value.ToString
                    Else
                        cadena = cadena & vbTab & oRow.Cells(c).Value.ToString
                    End If

                Next

                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Dim savefile As New FolderBrowserDialog
        savefile.SelectedPath = My.Computer.FileSystem.SpecialDirectories.Desktop

        Try
            If savefile.ShowDialog = DialogResult.OK Then
                FileCopy(fic, savefile.SelectedPath & "\" & nombre & ".xls")
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub dtToOO(ByVal dt As DataTable)

        Dim COpenOffice As New COpenOffice

        Try
            COpenOffice.ConnectOpenOffice()

            Dim i, f, cant, j As Integer
            f = 2
            cant = 0
            j = 2

            Dim myDoc As Object, firstSheet As Object, aRange As Object
            Dim fields As Object, unoWrap As Object, sortDx As Object
            myDoc = COpenOffice.StarDesktop.loadComponentFromURL("private:factory/scalc", "_blank", 0, COpenOffice.dummyArray)
            firstSheet = myDoc.Sheets.getByIndex(0)

            'columnas
            Dim Columna = 2
            For i = 0 To dt.Columns.Count - 1
                firstSheet.getCellByPosition(Columna, f).String = dt.Columns(i).ColumnName
                Columna += 1
            Next

            f = 3
            Columna = 2

            For Each oRow As DataRow In dt.Rows
                For i = 0 To dt.Columns.Count - 1
                    If IsNumeric(oRow(i).ToString) = True Then
                        Dim NumeroTemp As String = oRow(i).ToString.Replace(".", "")
                        firstSheet.getCellByPosition(Columna, f).value = NumeroTemp
                    Else
                        firstSheet.getCellByPosition(Columna, f).String = oRow(i).ToString.Replace(".", "")
                    End If

                    Columna += 1

                Next

                Columna = 2
                f += 1

            Next


            'MsgBox(OOoMess107)
            aRange = firstSheet.getCellRangeByName("A1:B16")
            fields = COpenOffice.CreateUnoStruct("com.sun.star.table.TableSortField", 0)
            fields(0).Field = 1
            fields(0).IsAscending = True
            fields(0).IsCaseSensitive = True

            ' il faut prciser quel type de squence est transmis la proprit SortFields
            ' you must specify which type of sequence is transmitted to SortFields property
            unoWrap = COpenOffice.OpenOffice.Bridge_GetValueObject
            unoWrap.set("[]com.sun.star.table.TableSortField", fields)

            ' remplissage de SortDescriptor : proprits ayant des valeurs autres que dfaut
            ' filling of SortDescriptor : properties with non-default values 
            sortDx = COpenOffice.CreateProperties("ContainsHeader", True, "SortFields", unoWrap)
            aRange.sort(sortDx)

            ' MsgBox(OOoMess108)

            ' myDoc.close(True)
            COpenOffice.DisconnectOpenOffice()

        Catch ex As Exception
            MessageBox.Show("Se produjo el siguiente error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Public Function NumeroALetra(ByVal value As Double) As String

        value = CDec(value)

        Select Case value
            Case 0 : NumeroALetra = "CERO"
            Case 1 : NumeroALetra = "UN"
            Case 2 : NumeroALetra = "DOS"
            Case 3 : NumeroALetra = "TRES"
            Case 4 : NumeroALetra = "CUATRO"
            Case 5 : NumeroALetra = "CINCO"
            Case 6 : NumeroALetra = "SEIS"
            Case 7 : NumeroALetra = "SIETE"
            Case 8 : NumeroALetra = "OCHO"
            Case 9 : NumeroALetra = "NUEVE"
            Case 10 : NumeroALetra = "DIEZ"
            Case 11 : NumeroALetra = "ONCE"
            Case 12 : NumeroALetra = "DOCE"
            Case 13 : NumeroALetra = "TRECE"
            Case 14 : NumeroALetra = "CATORCE"
            Case 15 : NumeroALetra = "QUINCE"
            Case Is < 20 : NumeroALetra = "DIECI" & NumeroALetra(value - 10)
            Case 20 : NumeroALetra = "VEINTE"
            Case Is < 30 : NumeroALetra = "VEINTI" & NumeroALetra(value - 20)
            Case 30 : NumeroALetra = "TREINTA"
            Case 40 : NumeroALetra = "CUARENTA"
            Case 50 : NumeroALetra = "CINCUENTA"
            Case 60 : NumeroALetra = "SESENTA"
            Case 70 : NumeroALetra = "SETENTA"
            Case 80 : NumeroALetra = "OCHENTA"
            Case 90 : NumeroALetra = "NOVENTA"
            Case Is < 100 : NumeroALetra = NumeroALetra(Int(value \ 10) * 10) & " Y " & NumeroALetra(value Mod 10)
            Case 100 : NumeroALetra = "CIEN"
            Case Is < 200 : NumeroALetra = "CIENTO " & NumeroALetra(value - 100)
            Case 200, 300, 400, 600, 800 : NumeroALetra = NumeroALetra(Int(value \ 100)) & "CIENTOS"
            Case 500 : NumeroALetra = "QUINIENTOS"
            Case 700 : NumeroALetra = "SETECIENTOS"
            Case 900 : NumeroALetra = "NOVECIENTOS"
            Case Is < 1000 : NumeroALetra = NumeroALetra(Int(value \ 100) * 100) & " " & NumeroALetra(value Mod 100)
            Case 1000 : NumeroALetra = "MIL"
            Case Is < 2000 : NumeroALetra = "MIL " & NumeroALetra(value Mod 1000)
            Case Is < 1000000 : NumeroALetra = NumeroALetra(Int(value \ 1000)) & " MIL"
                If value Mod 1000 Then NumeroALetra = NumeroALetra & " " & NumeroALetra(value Mod 1000)
            Case 1000000 : NumeroALetra = "UN MILLON"
            Case Is < 2000000 : NumeroALetra = "UN MILLON " & NumeroALetra(value Mod 1000000)
            Case Is < 1000000000000.0# : NumeroALetra = NumeroALetra(Int(value / 1000000)) & " MILLONES "
                If (value - Int(value / 1000000) * 1000000) Then NumeroALetra = NumeroALetra & " " & NumeroALetra(value - Int(value / 1000000) * 1000000)
            Case 1000000000000.0# : NumeroALetra = "UN BILLON"
            Case Is < 2000000000000.0# : NumeroALetra = "UN BILLON " & NumeroALetra(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
            Case Else : NumeroALetra = NumeroALetra(Int(value / 1000000000000.0#)) & " BILLONES"
                If (value - Int(value / 1000000000000.0#) * 1000000000000.0#) Then NumeroALetra = NumeroALetra & " " & NumeroALetra(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
        End Select
    End Function

    Public Function NumeroALetra2(ByVal numero As String) As String
        Dim palabras, entero, dec, flag As String

        entero = ""
        dec = ""

        Dim num, x, y As Integer

        flag = "N"

        If Mid(numero, 1, 1) = "-" Then
            numero = Mid(numero, 2, numero.ToString.Length - 1).ToString
            palabras = "menos "
        End If

        For x = 1 To numero.ToString.Length
            If Mid(numero, 1, 1) = "0" Then
                numero = Trim(Mid(numero, 2, numero.ToString.Length).ToString)
                If Trim(numero.ToString.Length) = 0 Then palabras = ""
            Else
                Exit For
            End If
        Next

        For y = 1 To Len(numero)
            'If Mid(numero, y, 1) = "." Then
            If Mid(numero, y, 1) = "," Then
                flag = "S"
            Else
                If flag = "N" Then
                    entero = entero + Mid(numero, y, 1)
                Else
                    dec = dec + Mid(numero, y, 1)
                End If
            End If
        Next y

        If Len(dec) = 1 Then dec = dec & "0"

        flag = "N"

        If Val(numero) <= 999999999 Then
            For y = Len(entero) To 1 Step -1
                num = Len(entero) - (y - 1)
                Select Case y
                    Case 3, 6, 9

                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If Mid(entero, num + 1, 1) = "0" And Mid(entero, num + 2, 1) = "0" Then
#Disable Warning BC42104 ' Se usa la variable antes de que se le haya asignado un valor
                                    palabras = palabras & "CIEN "
#Enable Warning BC42104 ' Se usa la variable antes de que se le haya asignado un valor
                                Else
                                    palabras = palabras & "CIENTO "
                                End If
                            Case "2"
                                palabras = palabras & "DOSCIENTOS "
                            Case "3"
                                palabras = palabras & "TRESCIENTOS "
                            Case "4"
                                palabras = palabras & "CUATROCIENTOS "
                            Case "5"
                                palabras = palabras & "QUINIENTOS "
                            Case "6"
                                palabras = palabras & "SEISCIENTOS "
                            Case "7"
                                palabras = palabras & "SETECIENTOS "
                            Case "8"
                                palabras = palabras & "OCHOCIENTOS "
                            Case "9"
                                palabras = palabras & "NOVECIENTOS "
                        End Select
                    Case 2, 5, 8

                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    flag = "S"
                                    palabras = palabras & "DIEZ "
                                End If
                                If Mid(entero, num + 1, 1) = "1" Then
                                    flag = "S"
                                    palabras = palabras & "ONCE "
                                End If
                                If Mid(entero, num + 1, 1) = "2" Then
                                    flag = "S"
                                    palabras = palabras & "DOCE "
                                End If
                                If Mid(entero, num + 1, 1) = "3" Then
                                    flag = "S"
                                    palabras = palabras & "TRECE "
                                End If
                                If Mid(entero, num + 1, 1) = "4" Then
                                    flag = "S"
                                    palabras = palabras & "CATORCE "
                                End If
                                If Mid(entero, num + 1, 1) = "5" Then
                                    flag = "S"
                                    palabras = palabras & "QUINCE "
                                End If
                                If Mid(entero, num + 1, 1) > "5" Then
                                    flag = "N"
                                    palabras = palabras & "DIECI"
                                End If
                            Case "2"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "VEINTE "
                                    flag = "S"
                                Else
                                    palabras = palabras & "VEINTI"
                                    flag = "N"
                                End If
                            Case "3"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "TREINTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "TREINTA Y "
                                    flag = "N"
                                End If
                            Case "4"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "CUARENTA"
                                    flag = "S"
                                Else
                                    palabras = palabras & "CUARENTA Y "
                                    flag = "N"
                                End If
                            Case "5"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "CINCUENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "CINCUENTA Y "
                                    flag = "N"
                                End If
                            Case "6"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "SESENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "SESENTA Y "
                                    flag = "N"
                                End If
                            Case "7"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "SETENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "SETENTA Y "
                                    flag = "N"
                                End If
                            Case "8"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "OCHENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "OCHENTA Y "
                                    flag = "N"
                                End If
                            Case "9"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "NOVENTA "
                                    flag = "S"
                                Else
                                    palabras = palabras & "NOVENTA Y "
                                    flag = "N"
                                End If
                        End Select
                    Case 1, 4, 7

                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If flag = "N" Then
                                    If y = 1 Then
                                        palabras = palabras & "UNO "
                                    Else
                                        palabras = palabras & "UN "
                                    End If
                                End If
                            Case "2"
                                If flag = "N" Then palabras = palabras & "DOS "
                            Case "3"
                                If flag = "N" Then palabras = palabras & "TRES "
                            Case "4"
                                If flag = "N" Then palabras = palabras & "CUATRO "
                            Case "5"
                                If flag = "N" Then palabras = palabras & "CINCO "
                            Case "6"
                                If flag = "N" Then palabras = palabras & "SEIS "
                            Case "7"
                                If flag = "N" Then palabras = palabras & "SIETE "
                            Case "8"
                                If flag = "N" Then palabras = palabras & "OCHO "
                            Case "9"
                                If flag = "N" Then palabras = palabras & "NUEVE "
                        End Select
                End Select


                If y = 4 Then
                    If numero = 1000 Then palabras = "MIL"
                    If numero < 2000 Then palabras = "MIL " & palabras(numero Mod 1000)
                    If 1000000 Then palabras = palabras & "MIL "
                End If

                If y = 7 Then
                    If Len(entero) = 7 And Mid(entero, 1, 1) = "1" Then
                        palabras = palabras & "MILLON "
                    Else
                        palabras = palabras & "MILLONES "
                    End If
                End If
            Next y


            If dec <> "" Then
                NumeroALetra2 = palabras & "con " & dec
            Else
                NumeroALetra2 = palabras
            End If
        Else
            NumeroALetra2 = ""
        End If

    End Function

    Function ValidarDigitoVerificador(ByVal RUC As String, Optional ByVal MostrarMensaje As Boolean = True, Optional ByVal Validar As Boolean = False) As Boolean

        ValidarDigitoVerificador = False

        'Validar el RUC
        'Si no tiene guion (-) Retornar TRUE
        If RUC.Contains("-") = False Then
            If IsNumeric(RUC) = True And Validar = False Then
                Return True
            Else
                Return False
            End If
        End If

        If RUC.Contains(" ") = True And Validar = True Then
            Return False
        End If


        Dim basemax As Integer = 11
        Dim codigo As Long
        Dim DigitoVerificador As String = ""
        Dim numero_al As String = ""
        Dim numero As String = ""

        numero = RUC.Split("-")(0)
        DigitoVerificador = RUC.Split("-")(1)

        Dim i
        For i = 1 To Len(numero)
            Dim c
            c = Mid$(numero, i, 1)
            codigo = Asc(UCase(c))
            If Not (codigo >= 48 And codigo <= 57) Then
                numero_al = numero_al & codigo
            Else
                numero_al = numero_al & c
            End If
        Next

        Dim k : Dim total
        k = 2
        total = 0

        For i = Len(numero_al) To 1 Step -1
            If (k > basemax) Then k = 2
            Dim numero_aux
            numero_aux = Val(Mid(numero_al, i, 1))
            total = total + (numero_aux * k)
            k = k + 1
        Next


        Dim resto : Dim digito
        resto = total Mod 11
        If (resto > 1) Then
            digito = 11 - resto
        Else
            digito = 0
        End If

        If digito.ToString = DigitoVerificador Then
            Return True
        Else
            If MostrarMensaje = True Then
                MessageBox.Show("El digito verificador no es correcto! Deberia ser: " & digito.ToString, "Digito Verificador", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If
        End If

    End Function

    Public Function ControlarNroDocumento(ByVal NroDocumento As String) As Boolean

        If NroDocumento Is Nothing Then
            MessageBox.Show("El nro no es correcto!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If NroDocumento.Length < 9 Then
            MessageBox.Show("El nro de Factura debe tener como minimo 9 digitos!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If NroDocumento.Contains(" ") = True Then
            MessageBox.Show("El numero de documento no debe contener espacios!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If NroDocumento.Contains("-") = False Then
            MessageBox.Show("El numero de documento no esta separado por guiones!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        Dim Documento() As String = NroDocumento.Split("-")

        If Documento.GetLength(0) <> 3 Then
            MessageBox.Show("El nro do decumento no tiene 2 guiones!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If IsNumeric(Documento(0)) = False Then
            MessageBox.Show("La referencia a la sucursal no es valida!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If Documento(0).Length <> 3 Then
            MessageBox.Show("La referencia a la sucursal debe tener 3 digitos!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If IsNumeric(Documento(1)) = False Then
            MessageBox.Show("La referencia al punto de expedicion no es valida!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If Documento(1).Length <> 3 Then
            MessageBox.Show("La referencia al punto de expedicion debe tener 3 digitos!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If IsNumeric(Documento(2)) = False Then
            MessageBox.Show("El nro de documento no es valida!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If Documento(2).Substring(0, 1) = "0" Then
            MessageBox.Show("El nro de documento debe ser sin ceros a la izquierda!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        For I As Integer = 0 To Documento(2).Length - 1
            Dim caracter = Documento(2).Substring(I, 1)
            Select Case caracter
                Case "-"
                    MessageBox.Show("El nro de documento debe ser numerico solo, sin simbolos y sin ceros a la izquierda!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return False
                Case ","
                    MessageBox.Show("El nro de documento debe ser numerico solo, sin simbolos y sin ceros a la izquierda!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return False
                Case "."
                    MessageBox.Show("El nro de documento debe ser numerico solo, sin simbolos y sin ceros a la izquierda!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return False
            End Select
        Next


        If NroDocumento.Length > 15 Then
            MessageBox.Show("El nro no puede ser mayor a 15 digitos!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        Return True

    End Function

    Public Function ControlarTimbrado(ByVal Timbrado As String) As Boolean

        If Timbrado Is Nothing Then
            Return False
        End If

        If IsNumeric(Timbrado) = False Then
            MessageBox.Show("El timbrado tiene q ser solo numerico!", "Formato incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If Timbrado.Length < 8 Then
            MessageBox.Show("El Timbrado debe tener como minimo 8 digitos!", "Formato Incorrecto", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        Return True

    End Function

    Public Shared Function CheckForInternetConnection(Optional ByVal vMostrarError As Boolean = False) As Boolean

        CheckForInternetConnection = False

        If My.Computer.Network.IsAvailable() Then
            Try
                If My.Computer.Network.Ping("www.google.com", 1000) Then
                    CheckForInternetConnection = True
                Else
                    CheckForInternetConnection = False
                End If
            Catch exint As System.Net.NetworkInformation.PingException
                If vMostrarError = True Then
                    MsgBox("Servicio de Internet No Responde ")
                End If
            Finally
            End Try
        Else
            If vMostrarError = True Then
                MsgBox("No Hay Conexión de Red")
            End If
            CheckForInternetConnection = False
        End If

        Return CheckForInternetConnection

    End Function

    Function ControlFechaOperacion(frmPadre As Form, ByVal IDOperacion As Integer, Asunto As String, Mensaje As String, Comprobante As String, FechaComprobante As Date, Importe As String) As Boolean

        ControlFechaOperacion = False

        'Verificar primeramente si el sistema esta configurado para bloquear las operaciones fuera de fecha
        If vgConfiguraciones("BloquearDocumentos") = False Then
            Return True
        End If

        'Verificar si la fecha esta fuera de rango
        Dim Dias As Integer = VGFechaHoraSistema.Subtract(FechaComprobante).Days
        If Dias < vgConfiguraciones("DiasBloqueo") Then
            Return True
        End If

        'Emitir mensaje de solicitud
        If MessageBox.Show("La fecha del documento esta fuera del rango permitido! Debe solicitar una activacion a un usuario permitido. Desea hacerlo ahora?", "Fuera de Rango", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
            Return False
        End If

        'Seleccionar usuario
        Dim frm As New frmSeleccionUsuarioHabilitacion
        frm.IDOperacion = IDOperacion
        FGMostrarFormulario(frmPadre, frm, "Seleccion de Usuario", FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)
        If frm.Seleccionado = False Then
            Return False
        End If

        'Registrar Solicitud
        Dim SQL As String = "Exec SpRegistroHabilitacion "
        ConcatenarParametro(SQL, "@Asunto", Asunto, False)
        ConcatenarParametro(SQL, "@Mensaje", Asunto, False)

        'Obtener Codigo de Activacion

        'Enviar correo electronico

        'Validar Codigo

        'Salir

        ControlFechaOperacion = True

    End Function

    Function EnviarEmail(Para As String, Asunto As String, Mensaje As String) As Boolean

        Dim _Message As New System.Net.Mail.MailMessage()
        Dim _SMTP As New System.Net.Mail.SmtpClient

        'CONFIGURACIÓN DEL STMP
        _SMTP.Credentials = New System.Net.NetworkCredential(vgConfiguraciones("MailCuenta").ToString, vgConfiguraciones("MailContraseña").ToString)
        _SMTP.Host = vgConfiguraciones("MailServidor").ToString
        _SMTP.Port = vgConfiguraciones("MailPuerto").ToString
        _SMTP.EnableSsl = vgConfiguraciones("MailSSL").ToString

        ' CONFIGURACION DEL MENSAJE
        _Message.[To].Add(Para) 'Cuenta de Correo al que se le quiere enviar el e-mail
        _Message.From = New System.Net.Mail.MailAddress(Para, Para, System.Text.Encoding.UTF8) 'Quien lo envía
        _Message.Subject = Asunto  'Sujeto del e-mail
        _Message.SubjectEncoding = System.Text.Encoding.UTF8 'Codificacion
        _Message.Body = Mensaje 'contenido del mail
        _Message.BodyEncoding = System.Text.Encoding.UTF8
        _Message.Priority = System.Net.Mail.MailPriority.Normal
        _Message.IsBodyHtml = False

        'ENVIO
        Try
            _SMTP.Send(_Message)
            MessageBox.Show("Mensaje enviado correctamene", "Exito!", MessageBoxButtons.OK)
        Catch ex As System.Net.Mail.SmtpException
            MessageBox.Show(ex.ToString, "Error!", MessageBoxButtons.OK)
        End Try

#Disable Warning BC42353 ' La función no devuelve un valor en todas las rutas de código
    End Function
#Enable Warning BC42353 ' La función no devuelve un valor en todas las rutas de código

    Public Sub DataGridColumnasNumericas(DG As DataGridView, ByRef ColumnasNumericas() As String, Optional Decimales As Boolean = False)

        Dim CantidadDecimal As Integer = 0

        If Decimales Then
            CantidadDecimal = 2
        End If


        For c As Integer = 0 To ColumnasNumericas.Length - 1
            If DG.RowCount > 0 Then
                DG.Columns(ColumnasNumericas(c)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                DG.Columns(ColumnasNumericas(c)).DefaultCellStyle.Format = "N" & CantidadDecimal.ToString
            End If

        Next

    End Sub

    Public Sub DataGridColumnasCentradas(DG As DataGridView, ByRef ColumnasCentradas() As String)
        For c As Integer = 0 To ColumnasCentradas.Length - 1
            DG.Columns(ColumnasCentradas(c)).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        Next
    End Sub

    Public Sub DataGridColumnasVisibles(DG As DataGridView, ByRef ColumnasVisible() As String)
        For Each c As DataGridViewColumn In DG.Columns
            If ColumnasVisible.Contains(c.Name) = True Then
                c.Visible = True
            Else
                c.Visible = False
            End If
        Next
    End Sub

    Public Sub SetIDMoneda(cbx As ocxCBX, txt As ocxTXTNumeric)

        Try

            'Verificar si es que la moneda admite decimales
            Dim IDMoneda As Integer
            Dim CData As New CData
            Dim CantidadDecimal As Integer = 0

            IDMoneda = cbx.GetValue

            Dim dt As DataTable = CData.GetTable("VMoneda", " ID=" & IDMoneda)
            If dt.Rows.Count = 0 Then
                Exit Try
            End If

            txt.Decimales = dt.Rows(0)("Decimales")

            If txt.Decimales = True Then
                CantidadDecimal = 2
            End If


        Catch ex As Exception

        End Try

    End Sub

#End Region

End Class

Public Class CDetalleImpuesto

    Dim CSistema As New CSistema
    Dim CData As New CData

    'ENUMERACION
    Enum ENUMTipoImpuesto
        IVA10 = 1
        IVA5 = 2
        EXENTO = 3
    End Enum

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
            SetIDMoneda()
        End Set
    End Property

    Private DecimalesValue As Boolean
    Public Property Decimales() As Boolean
        Get
            Return DecimalesValue
        End Get
        Set(ByVal value As Boolean)
            DecimalesValue = value
        End Set
    End Property

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Public Property TipoImpuesto As ENUMTipoImpuesto

    Sub Inicializar()

        'CARGAR ESTRUCTURA DEL DETALLE IMPUESTO
        'dt = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleImpuesto").Clone
        dt = CData.GetStructure("VDetalleImpuesto", "Select Top(0) * From VDetalleImpuesto")

        If IDMoneda = 0 Then
            IDMoneda = 1
        End If

    End Sub

    Public Sub SetIDMoneda()

        Try

            'Verificar si es que la moneda admite decimales
            Decimales = False

            Dim dt As DataTable = CData.GetTable("VMoneda", " ID=" & IDMoneda)
            If dt.Rows.Count = 0 Then
                Exit Try
            End If

            Decimales = dt.Rows(0)("Decimales")

        Catch ex As Exception

        End Try

    End Sub

    Sub EstablecerImpuestosDetalle(ByVal dtDetalle As DataTable, Optional ByVal ConDescuento As Boolean = True, Optional ByVal Decimales As Boolean = False)

        If dt Is Nothing Then
            Inicializar()
        End If

        dt.Rows.Clear()

        For Each oRow As DataRow In dtDetalle.Rows

            'Verificar si existe el impuesto en el detalle
            If dt.Select(" IDImpuesto=" & oRow("IDImpuesto").ToString).Length = 0 Then

                Dim NewRow As DataRow = dt.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("Impuesto") = oRow("Impuesto").ToString
                NewRow("IDImpuesto") = oRow("IDImpuesto").ToString
                NewRow("Total") = CSistema.FormatoMoneda(oRow("Total"), Decimales)
                NewRow("TotalImpuesto") = CSistema.FormatoMoneda(oRow("TotalImpuesto"), Decimales)
                NewRow("TotalDiscriminado") = CSistema.FormatoMoneda(oRow("TotalDiscriminado"), Decimales)

                'Debug.Print(oRow("ID").ToString & " - " & oRow("IDImpuesto").ToString & " - " & CInt(oRow("Total")).ToString & " - " & CInt(oRow("Total")).ToString)

                If ConDescuento = True Then
                    NewRow("TotalSinImpuesto") = CSistema.FormatoMoneda(oRow("TotalSinImpuesto"), Decimales)
                    NewRow("TotalSinDescuento") = CSistema.FormatoMoneda(oRow("TotalSinDescuento"), Decimales)
                    NewRow("TotalDescuento") = CSistema.FormatoMoneda(oRow("TotalDescuento"), Decimales)
                Else
                    NewRow("TotalSinImpuesto") = 0
                    NewRow("TotalSinDescuento") = 0
                    NewRow("TotalDescuento") = 0
                End If

                dt.Rows.Add(NewRow)

            Else

                For Each CurrentRow As DataRow In dt.Select(" IDImpuesto=" & oRow("IDImpuesto").ToString)

                    Dim Total As Decimal = CDec(oRow("Total").ToString)
                    Dim TotalImpuesto As Decimal = 0
                    Dim TotalDiscriminado As Decimal = 0

                    CSistema.CalcularIVA(oRow("IDImpuesto"), oRow("Total").ToString, Decimales, False, TotalDiscriminado, TotalImpuesto)
                    Dim TotalSinImpuesto As Decimal = Total - TotalImpuesto
                    Dim Descuento As Decimal = 0
                    Dim TotalSinDescuento As Decimal = 0.0

                    If ConDescuento = True Then
                        Descuento = CDec(oRow("TotalDescuento").ToString)
                        TotalSinDescuento = Total - Descuento
                    End If

                    CurrentRow("Total") = CDec(CurrentRow("Total")) + Total
                    Debug.Print(oRow("ID").ToString & " - " & oRow("IDImpuesto").ToString & " - " & CDec(oRow("Total")).ToString & " - " & CDec(CurrentRow("Total")).ToString)

                    CurrentRow("TotalImpuesto") = CDec(CurrentRow("TotalImpuesto")) + TotalImpuesto
                    CurrentRow("TotalDiscriminado") = CDec(CurrentRow("TotalDiscriminado")) + TotalDiscriminado
                    CurrentRow("TotalSinImpuesto") = CDec(CurrentRow("TotalSinImpuesto")) + TotalSinImpuesto
                    CurrentRow("TotalSinDescuento") = CDec(CurrentRow("TotalSinDescuento")) + TotalSinDescuento
                    CurrentRow("TotalDescuento") = CDec(CurrentRow("TotalDescuento")) + Descuento

                Next

            End If

        Next

    End Sub

    Function Guardar(ByVal IDTransaccion As Integer) As Boolean

        Guardar = True

        For Each oRow As DataRow In dt.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDescuento", CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString, Decimales), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleImpuesto", False, False, MensajeRetorno) = False Then
                Return False
            End If

        Next

        Return True

    End Function

    Function Insert(ByVal IDTransaccion As Integer) As String

        Insert = ""

        For Each oRow As DataRow In dt.Rows

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "IDTransaccion", IDTransaccion)
            CSistema.SetSQLParameter(param, "IDImpuesto", oRow("IDImpuesto").ToString)
            CSistema.SetSQLParameter(param, "Total", CSistema.FormatoMonedaBaseDatos(oRow("Total").ToString, Decimales))
            CSistema.SetSQLParameter(param, "TotalImpuesto", CSistema.FormatoMonedaBaseDatos(oRow("TotalImpuesto").ToString, Decimales))
            CSistema.SetSQLParameter(param, "TotalDiscriminado", CSistema.FormatoMonedaBaseDatos(oRow("TotalDiscriminado").ToString, Decimales))
            CSistema.SetSQLParameter(param, "TotalDescuento", CSistema.FormatoMonedaBaseDatos(oRow("TotalDescuento").ToString, Decimales))

            Insert = Insert & CSistema.InsertSQL(param, "DetalleImpuesto")


        Next

    End Function

    Public Sub EstablecerImporte(Total As Decimal, vTipoImpuesto As ENUMTipoImpuesto, Optional Sumar As Boolean = False)

        Dim IDImpuesto As Integer
        Dim Impuesto As String = ""
        Dim TotalImpuesto As Decimal = 0
        Dim TotalDiscriminado As Decimal = 0

        Select Case vTipoImpuesto
            Case ENUMTipoImpuesto.IVA10
                IDImpuesto = CData.GetRow("Referencia='10%'", "VImpuesto")("ID")
                Impuesto = CData.GetRow("Referencia='10%'", "VImpuesto")("Descripcion")
                TotalDiscriminado = Total / CData.GetRow("Referencia='10%'", "VImpuesto")("FactorDiscriminado")
                TotalImpuesto = Total / CData.GetRow("Referencia='10%'", "VImpuesto")("FactorImpuesto")
            Case ENUMTipoImpuesto.IVA5
                IDImpuesto = CData.GetRow("Referencia='5%'", "VImpuesto")("ID")
                Impuesto = CData.GetRow("Referencia='10%'", "VImpuesto")("Descripcion")
                TotalDiscriminado = Total / CData.GetRow("Referencia='10%'", "VImpuesto")("FactorDiscriminado")
                TotalImpuesto = Total / CData.GetRow("Referencia='10%'", "VImpuesto")("FactorImpuesto")
            Case ENUMTipoImpuesto.EXENTO
                Impuesto = CData.GetRow("Referencia='EX'", "VImpuesto")("Descripcion")
                IDImpuesto = CData.GetRow("Referencia='EX'", "VImpuesto")("ID")
        End Select

        If dt.Select("IDImpuesto=" & IDImpuesto).Count = 0 Then
            Dim NewRow As DataRow = dt.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("Impuesto") = Impuesto
            NewRow("IDImpuesto") = IDImpuesto
            NewRow("Total") = Total
            NewRow("TotalImpuesto") = TotalImpuesto
            NewRow("TotalDiscriminado") = TotalDiscriminado

            dt.Rows.Add(NewRow)

        Else
            For Each oRow As DataRow In dt.Select("IDImpuesto=" & IDImpuesto)
                oRow("IDTransaccion") = 0
                oRow("Total") = Total
                oRow("TotalImpuesto") = TotalImpuesto
                oRow("TotalDiscriminado") = TotalDiscriminado
            Next
        End If


    End Sub

End Class

Public Class CFormaPago

    Dim CSistema As New CSistema

    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Sub Inicializar()

        'CARGAR ESTRUCTURA DEL DETALLE IMPUESTO
        dt = CSistema.ExecuteToDataTable("Select Top(0) * From VDetalleImpuesto").Clone

    End Sub

    Sub EstablecerImpuestosDetalle(ByVal dtDetalle As DataTable)

        dt.Rows.Clear()

        For Each oRow As DataRow In dtDetalle.Rows

            'Verificar si existe el impuesto en el detalle
            If dt.Select(" IDImpuesto=" & oRow("IDImpuesto").ToString).Length = 0 Then

                Dim NewRow As DataRow = dt.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("Impuesto") = oRow("Impuesto").ToString
                NewRow("IDImpuesto") = oRow("IDImpuesto").ToString
                NewRow("Total") = oRow("Total").ToString
                NewRow("TotalImpuesto") = oRow("TotalImpuesto").ToString
                NewRow("TotalDiscriminado") = oRow("TotalDiscriminado").ToString

                dt.Rows.Add(NewRow)

            Else

                For Each CurrentRow As DataRow In dt.Select(" IDImpuesto=" & oRow("IDImpuesto").ToString)

                    Dim Total As Decimal = Math.Round(CDec(oRow("Total").ToString), 0)
                    Dim TotalImpuesto As Decimal = CDec(oRow("TotalImpuesto").ToString)
                    Dim TotalDiscriminado As Decimal = CDec(oRow("TotalDiscriminado").ToString)

                    CurrentRow("Total") = CDec(CurrentRow("Total")) + Total
                    CurrentRow("TotalImpuesto") = CDec(CurrentRow("TotalImpuesto")) + TotalImpuesto
                    CurrentRow("TotalDiscriminado") = CDec(CurrentRow("TotalDiscriminado")) + TotalDiscriminado

                Next

            End If

        Next

    End Sub

    Function Guardar(ByVal IDTransaccion As Integer) As Boolean

        Guardar = True

        For Each oRow As DataRow In dt.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDImpuesto", oRow("IDImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Total", oRow("Total").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalImpuesto", oRow("TotalImpuesto").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TotalDiscriminado", oRow("TotalDiscriminado").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Operacion", "INS", ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleImpuesto", False, False, MensajeRetorno) = False Then
                Return False
            End If

        Next

        Return True

    End Function

End Class

Public Class CAsientoContable

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEADES
    Private dtAsientoValue As DataTable
    Public Property dtAsiento() As DataTable
        Get
            Return dtAsientoValue
        End Get
        Set(ByVal value As DataTable)
            dtAsientoValue = value
        End Set
    End Property

    Private dtDetalleAsientoValue As DataTable
    Public Property dtDetalleAsiento() As DataTable
        Get
            Return dtDetalleAsientoValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleAsientoValue = value
        End Set
    End Property

    Private dtTipoCuentaFijaValue As DataTable
    Public Property dtTipoCuentaFija() As DataTable
        Get
            Return dtTipoCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtTipoCuentaFijaValue = value
        End Set
    End Property

    Private dtValoresValue As DataTable
    Public Property dtValores() As DataTable
        Get
            Return dtValoresValue
        End Get
        Set(ByVal value As DataTable)
            dtValoresValue = value
        End Set
    End Property

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDetalleImpuestoValue As DataTable
    Public Property dtDetalleImpuesto() As DataTable
        Get
            Return dtDetalleImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleImpuestoValue = value
        End Set
    End Property

    Private NuevoValue As Boolean
    Public Property Nuevo() As Boolean
        Get
            Return NuevoValue
        End Get
        Set(ByVal value As Boolean)
            NuevoValue = value
        End Set
    End Property

    Private GeneradoValue As Boolean
    Public Property Generado() As Boolean
        Get
            Return GeneradoValue
        End Get
        Set(ByVal value As Boolean)
            GeneradoValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    Private IDTipoOperacionValue As Integer
    Public Property IDTipoOperacion() As Integer
        Get
            Return IDTipoOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTipoOperacionValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal
        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private DescuentoValue As Decimal
    Public Property Descuento() As Decimal
        Get
            Return DescuentoValue
        End Get
        Set(ByVal value As Decimal)
            DescuentoValue = value
        End Set
    End Property

    Private IDClienteProveedorValue As Integer
    Public Property IDClienteProveedor() As Integer
        Get
            Return IDClienteProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDClienteProveedorValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private EsClienteValue As Boolean
    Public Property EsCliente() As Boolean
        Get
            Return EsClienteValue
        End Get
        Set(ByVal value As Boolean)
            EsClienteValue = value
        End Set
    End Property

    Private EsVentaValue As Boolean
    Public Property EsVenta() As Boolean
        Get
            Return EsVentaValue
        End Get
        Set(ByVal value As Boolean)
            EsVentaValue = value
        End Set
    End Property

    Private EsContadoValue As Boolean
    Public Property EsContado() As Boolean
        Get
            Return EsContadoValue
        End Get
        Set(ByVal value As Boolean)
            EsContadoValue = value
        End Set
    End Property

    Private EsCreditoValue As Boolean
    Public Property EsCredito() As Boolean
        Get
            Return EsCreditoValue
        End Get
        Set(ByVal value As Boolean)
            EsCreditoValue = value
        End Set
    End Property

    Private EsProveedorValue As Boolean
    Public Property EsProveedor() As Boolean
        Get
            Return EsProveedorValue
        End Get
        Set(ByVal value As Boolean)
            EsProveedorValue = value
        End Set
    End Property

    'EVENTOS

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Inicializar Asiento
        dtAsiento = CData.GetStructure("VAsiento", "Select Top(0) * From VAsiento")
        dtAsiento.Columns.Add("IDDeposito")

        'Inicializar DetalleAsiento
        dtDetalleAsiento = CData.GetStructure("VDetalleAsiento", "Select Top(0) * From VDetalleAsiento")
        dtDetalleAsiento.Columns.Add("Fijo")

        'Inicializar Detalle tipo de Cuenta
        dtTipoCuentaFija = CData.CreateTable("VTipoCuentaFija", "Select * From VTipoCuentaFija")

        'Cuenta fija de la operacion
        dtCuentaFija = CData.CreateTable("VCuentasFijas", "Select * From VCuentasFijas V")

        If IDTipoOperacion > 0 Then
            dtCuentaFija = CSistema.FiltrarDataTable(dtCuentaFija, " IDOperacion=" & IDOperacion & " And IDTipoOperacion=" & IDTipoOperacion & " ")
        Else
            dtCuentaFija = CSistema.FiltrarDataTable(dtCuentaFija, " IDOperacion=" & IDOperacion & " ")
        End If

    End Sub

    Public Sub CambiarCuentaFija(ByVal vIDTipoOperacion As Integer)

        IDTipoOperacion = vIDTipoOperacion
        dtCuentaFija = CSistema.ExecuteToDataTable("Select * From VCuentasFijas V Where IDOperacion=" & IDOperacion & " And IDTipoOperacion=" & IDTipoOperacion)

    End Sub

    Sub Limpiar()

        If Not dtAsiento Is Nothing Then
            dtAsiento.Rows.Clear()
        End If

        If Not dtDetalleAsiento Is Nothing Then
            dtDetalleAsiento.Rows.Clear()
        End If

        If Not dtDetalleImpuesto Is Nothing Then
            dtDetalleImpuesto.Rows.Clear()
        End If

        If Not dtDetalleProductos Is Nothing Then
            dtDetalleProductos.Rows.Clear()
        End If

        If Not dtValores Is Nothing Then
            dtValores.Rows.Clear()
        End If

        Me.Descuento = 0
        Me.Total = False
        Me.EsCliente = False
        Me.EsProveedor = False
        Me.Generado = False
        Me.Nuevo = False
        Me.IDOperacion = 0

    End Sub

    Sub CargarOperacion(ByVal IDTransaccion As Integer)

        'Cargamos la operacion
        dtAsiento = CSistema.ExecuteToDataTable("Select * From VAsiento Where IDTransaccion=" & IDTransaccion)
        dtDetalleAsiento = CSistema.ExecuteToDataTable("Select * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion & " Order By ID")

        'Listar Detalle

        'Totales

    End Sub

    Sub ListarDetalle(ByVal lv As ListView)

        If dtDetalleAsiento Is Nothing Then
            Exit Sub
        End If


        For Each oRow As DataRow In dtDetalleAsiento.Rows
            Dim item As New ListViewItem
            item = lv.Items.Add(oRow("Codigo").ToString)
            item.SubItems.Add(oRow("Descripcion").ToString)
            item.SubItems.Add(CSistema.FormatoNumero(oRow("Debito").ToString))
            item.SubItems.Add(CSistema.FormatoNumero(oRow("Credito").ToString))
        Next

    End Sub

    Public Sub GenerarAsiento()

        ' Variables
        Dim Importe As Decimal

        If Generado = True Then

            'Eliminamos del detalle los que son fijos
            Dim Cantidad As Integer = 0
            Dim deleteRow(-1) As DataRow
            For Each r As DataRow In dtDetalleAsiento.Rows
                If CBool(r("Fijo")) = True Then
                    ReDim Preserve deleteRow(Cantidad)
                    deleteRow(Cantidad) = r
                    Cantidad = Cantidad + 1
                End If
            Next

            For i As Integer = 0 To deleteRow.GetLength(0) - 1
                dtDetalleAsiento.Rows.Remove(deleteRow(i))
            Next


        End If

        'Recorrer las cuentas configuradas
        For Each oRow As DataRow In dtCuentaFija.Rows

            'Añadimos al detalle del asiento
            Importe = 0

            'Cuenta contable
            Select Case oRow("Tipo").ToString

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)

                Case "TOTAL"
                    CargarCuentaTotal(oRow)

                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)

            End Select

        Next

        Generado = True

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then

                    Dim dtTemp As New DataTable

                    If EsProveedor = True Then
                        dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Producto P Join VCuentaContable CC On P.IDCuentaContableCompra=CC.ID Where P.ID=" & DetalleRow("IDProducto").ToString)
                    End If

                    If EsCliente = True Then

                        Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                        Dim ProductoRow As DataRow = dtProducto.Rows(0)

                        'dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Producto P Join VCuentaContable CC On P.IDCuentaContableVenta=CC.ID Where P.ID=" & DetalleRow("IDProducto").ToString)
                        If IsNumeric(ProductoRow("IDCuentaContableVenta")) Then
                            dtTemp = CData.GetTable("VCuentaContable", " IDCuentaContable=" & ProductoRow("IDCuentaContableVenta")).Copy
                        End If


                    End If

                    If Not dtTemp Is Nothing Then

                        If dtTemp.Rows.Count > 0 Then
                            Dim rowTemp As DataRow = dtTemp.Rows(0)

                            NewRow("Codigo") = rowTemp("Codigo")
                            NewRow("Cuenta") = rowTemp("Cuenta")
                            NewRow("Descripcion") = rowTemp("CuentaContable")
                            NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                            Establecido = True

                        End If
                    End If
                End If

                'Verificar la configuracion si pide buscar en Cliente/Proveedor
                If CBool(oRow("BuscarEnClienteProveedor").ToString) = True Then

                    If Establecido = False Then
                        Establecido = True
                    End If

                End If

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Cuenta").ToString
                    NewRow("Cuenta") = oRow("CuentaContable").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto(oRow, DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                Else
                    dtDetalleAsiento.Rows.Remove(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Cuenta").ToString
            NewRow("Cuenta") = oRow("CuentaContable").ToString
            NewRow("Alias") = oRow("Codigo").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If


            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Si hay que buscar en el producto
            If CBool(oRow("BuscarEnProducto").ToString) = True Then

            End If

            'Verificar si es de tipo venta.
            'Si la venta es contado y el oRow es credito pasar por alto y viceversa
            If CBool(oRow("EsVenta")) = EsVenta Then

                If EsContado = True Then
                    If CBool(oRow("Contado").ToString) = False Then
                        Exit Sub
                    End If
                End If

                If EsCredito = True Then
                    If CBool(oRow("Credito").ToString) = False Then
                        Exit Sub
                    End If
                End If

            End If

            'Verificar la configuracion si pide buscar en Cliente/Proveedor
            If CBool(oRow("BuscarEnClienteProveedor").ToString) = True Then

                Dim dtTemp As New DataTable

                If EsProveedor = True Then
                    dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Proveedor P Join VCuentaContable CC On P.IDCuentaContableCompra=CC.ID Where P.ID=" & IDClienteProveedor)
                End If

                If EsCliente = True Then

                    Dim dtcliente As DataTable = CData.GetTable("VCliente", " ID = " & IDClienteProveedor).Copy

                    Dim ClienteRow As DataRow = dtcliente.Rows(0)
                    'dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Cliente C Join VCuentaContable CC On C.IDCuentaContable=CC.ID Where C.ID=" & IDClienteProveedor)
                    If IsNumeric(ClienteRow("IDCuentaContable")) = True Then
                        dtTemp = CData.GetTable("VCuentaContable", " IDCuentaContable=" & ClienteRow("IDCuentaContable")).Copy
                    End If

                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        oRow("Codigo") = rowTemp("Codigo")
                        oRow("Cuenta") = rowTemp("Cuenta")
                        oRow("CuentaContable") = rowTemp("CuentaContable")
                        oRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                    End If

                End If

            End If

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si hay que buscar en Cliente/Proveedor

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("CuentaContable").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Function Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        Guardar = False

        Dim param(-1) As SqlClient.SqlParameter

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", dtAsiento.Rows(0)("Numero").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCiudad", dtAsiento.Rows(0)("IDCiudad").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(dtAsiento.Rows(0)("Fecha").ToString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", dtAsiento.Rows(0)("IDMoneda").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", dtAsiento.Rows(0)("Cotizacion").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", dtAsiento.Rows(0)("IDTipoComprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", dtAsiento.Rows(0)("NroComprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Detalle", dtAsiento.Rows(0)("Detalle").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Debito", dtAsiento.Rows(0)("Debito").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Credito", dtAsiento.Rows(0)("Credito").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Saldo", dtAsiento.Rows(0)("Saldo").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", dtAsiento.Rows(0)("Total").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCentroCosto", dtAsiento.Rows(0)("IDCentroCosto").ToString, ParameterDirection.Input)

        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAsiento", False, False, MensajeRetorno) = False Then

            'Eliminar el Registro si es que se registro
            Return False

        End If

        'Insertamos el Detalle
        If InsertarDetalle(ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

            'Eliminar el Registro si es que se registro
            Return False

        End If


    End Function

    Public Function InsertAsiento(ByVal IDTransaccion As Integer) As String

        GenerarAsiento()

        InsertAsiento = ""

        Dim param As New DataTable

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "IDTransaccion", IDTransaccion)
        CSistema.SetSQLParameter(param, "Numero", dtAsiento.Rows(0)("Numero").ToString)
        CSistema.SetSQLParameter(param, "IDSucursal", dtAsiento.Rows(0)("IDSucursal").ToString)
        CSistema.SetSQLParameter(param, "Fecha", CSistema.FormatoFechaBaseDatos(dtAsiento.Rows(0)("Fecha").ToString, True, False))
        CSistema.SetSQLParameter(param, "IDMoneda", dtAsiento.Rows(0)("IDMoneda").ToString)
        CSistema.SetSQLParameter(param, "Cotizacion", dtAsiento.Rows(0)("Cotizacion").ToString)
        CSistema.SetSQLParameter(param, "IDTipoComprobante", dtAsiento.Rows(0)("IDTipoComprobante").ToString)
        CSistema.SetSQLParameter(param, "NroComprobante", dtAsiento.Rows(0)("NroComprobante").ToString)
        CSistema.SetSQLParameter(param, "Detalle", dtAsiento.Rows(0)("Detalle").ToString)
        CSistema.SetSQLParameter(param, "Debito", dtAsiento.Rows(0)("Debito").ToString)
        CSistema.SetSQLParameter(param, "Credito", dtAsiento.Rows(0)("Credito").ToString)
        CSistema.SetSQLParameter(param, "Saldo", dtAsiento.Rows(0)("Saldo").ToString)
        CSistema.SetSQLParameter(param, "Total", dtAsiento.Rows(0)("Total").ToString)
        CSistema.SetSQLParameter(param, "IDCentroCosto", dtAsiento.Rows(0)("IDCentroCosto").ToString)

        InsertAsiento = CSistema.InsertSQL(param, "Asiento")

    End Function

    Function InsertarDetalle(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        For Each oRow As DataRow In dtDetalleAsiento.Rows

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCuentaContable", oRow("IDCuentaContable").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Debito", oRow("Debito").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Credito", oRow("Credito").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", oRow("Importe").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDSucursal", oRow("IDSucursal").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TipoComprobante", oRow("TipoComprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroComprobante", oRow("NroComprobante").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleAsiento", False, False, MensajeRetorno) = False Then
                Return False
            End If

        Next

    End Function

    Function InsertDetalle(ByVal IDTransaccion As Integer) As String

        InsertDetalle = ""

        For Each oRow As DataRow In dtDetalleAsiento.Rows

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "IDTransaccion", IDTransaccion)
            CSistema.SetSQLParameter(param, "IDCuentaContable", oRow("IDCuentaContable").ToString)
            CSistema.SetSQLParameter(param, "ID", oRow("ID").ToString)
            CSistema.SetSQLParameter(param, "Debito", CSistema.FormatoMonedaBaseDatos(oRow("Debito").ToString))
            CSistema.SetSQLParameter(param, "Credito", CSistema.FormatoMonedaBaseDatos(oRow("Credito").ToString))
            CSistema.SetSQLParameter(param, "Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString))
            CSistema.SetSQLParameter(param, "Observacion", oRow("Observacion").ToString)

            CSistema.SetSQLParameter(param, "IDSucursal", oRow("IDSucursal").ToString)
            CSistema.SetSQLParameter(param, "TipoComprobante", oRow("TipoComprobante").ToString)
            CSistema.SetSQLParameter(param, "NroComprobante", oRow("NroComprobante").ToString)

            InsertDetalle = InsertDetalle & CSistema.InsertSQL(param, "DetalleAsiento") & vbCrLf

        Next

    End Function

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtDetalleImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow(Campo).ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try

            If Detalle = False Then
                ObtenerValorTotal = Total
                Exit Try
            End If

            If condicion("IncluirImpuesto") = True Then
                vTotal = CSistema.dtSumColumn(dtDetalleProductos, "Total")
            Else
                vTotal = CSistema.dtSumColumn(dtDetalleProductos, "TotalDiscriminado")
            End If

            If condicion("IncluirDescuento") = False Then
                Dim Descuento As Decimal
                Descuento = CSistema.dtSumColumn(dtDetalleProductos, "TotalDescuento")
                vTotal = vTotal - Descuento
            End If

            ObtenerValorTotal = vTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorDescuento(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorDescuento = 0

        Try

            If Detalle = False Then
                ObtenerValorDescuento = Descuento
                Exit Try
            End If

            Descuento = CSistema.dtSumColumn(dtDetalleProductos, "TotalDescuento")

        Catch ex As Exception

        End Try

    End Function

    Public Sub New(ByVal vIDOperacion As Integer, Optional ByVal vIDTipoOperacion As Integer = -1)
        IDOperacion = vIDOperacion
        IDTipoOperacion = vIDTipoOperacion
        Inicializar()
    End Sub

    Public Sub New()
        Inicializar()
    End Sub

End Class

Public Class CAsiento

    'CLASES
    Public CSistema As New CSistema
    Public CData As New CData

    'PROPIEADES
    Private dtAsientoValue As DataTable
    Public Property dtAsiento() As DataTable
        Get
            Return dtAsientoValue
        End Get
        Set(ByVal value As DataTable)
            dtAsientoValue = value
        End Set
    End Property

    'SC: 16/02/2022 - Nuevo
    Private dtDetalleCIerreValue As DataTable
    Public Property dtDetalleCierre() As DataTable
        Get
            Return dtDetalleCIerreValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleCIerreValue = value
        End Set
    End Property


    Private dtDetalleAsientoValue As DataTable
    Public Property dtDetalleAsiento() As DataTable
        Get
            Return dtDetalleAsientoValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleAsientoValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private GeneradoValue As Boolean
    Public Property Generado() As Boolean
        Get
            Return GeneradoValue
        End Get
        Set(ByVal value As Boolean)
            GeneradoValue = value
        End Set
    End Property

    Private SaldoValue As Decimal
    Public Property Saldo() As Decimal
        Get
            Return SaldoValue
        End Get
        Set(ByVal value As Decimal)
            SaldoValue = value
        End Set
    End Property

    Private TotalValue As Decimal
    Public Property Total() As Decimal
        Get
            Return TotalValue
        End Get
        Set(ByVal value As Decimal)
            TotalValue = value
        End Set
    End Property

    Private TotalGastoAdicionalValue As Decimal
    Public Property TotalGastoAdicional() As Decimal
        Get
            Return TotalGastoAdicionalValue
        End Get
        Set(ByVal value As Decimal)
            TotalGastoAdicionalValue = value
        End Set
    End Property

    Private NoAgruparValue As Boolean
    Public Property NoAgrupar() As Boolean
        Get
            Return NoAgruparValue
        End Get
        Set(ByVal value As Boolean)
            NoAgruparValue = value
        End Set
    End Property

    Private dtDetalleAperturaValue As DataTable
    Public Property dtDetalleApertura() As DataTable
        Get
            Return dtDetalleAperturaValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleAperturaValue = value
        End Set
    End Property

    Private BloquearValue As Boolean
    Public Property Bloquear() As Boolean
        Get
            Return BloquearValue
        End Get
        Set(ByVal value As Boolean)
            BloquearValue = value
        End Set
    End Property

    Public Property NroCaja As Integer

    Public Property CajaHabilitada As Boolean

    Public Property IDCentroCosto As Integer
    'FA 18/11/2022 - Plan cuenta
    Public Property IDUnidadNegocio As Integer
    Public Property IDDepartamento As Integer

    Sub Inicializar()

        'Variables
        Generado = False
        NoAgrupar = False
        Bloquear = False

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Inicializar Asiento
        dtAsiento = CData.GetStructure("VAsiento", "Select Top(0) * From VAsiento").Clone
        dtAsiento.Columns.Add("IDDeposito")

        'Inicializar DetalleAsiento
        dtDetalleAsiento = CData.GetStructure("VDetalleAsiento", "Select Top(0) * From VDetalleAsiento").Clone
        dtDetalleAsiento.Columns.Add("Fijo")

        'Inicializar DetalleCierre
        dtDetalleCierre = CData.GetStructure("VPlanCuentaSaldo", "Select Top(0) * From VPlanCuentaSaldo").Clone
        dtDetalleCierre.Columns.Add("Fijo")

        dtDetalleApertura = CData.GetStructure("VPlanCuentaSaldo", "Select Top(0) * from VDetalleAsiento DA join Asiento A on Da.IDTransaccion=A.IDTransaccion").Clone
        dtDetalleApertura.Columns.Add("Fijo")

    End Sub

    Sub Limpiar()

        'Variables
        Generado = False
        Bloquear = False

        If Not dtAsiento Is Nothing Then
            dtAsiento.Rows.Clear()
        End If

        If Not dtDetalleAsiento Is Nothing Then
            dtDetalleAsiento.Rows.Clear()
        End If

    End Sub

    Sub ListarDetalle(ByVal lv As ListView)

        If dtDetalleAsiento Is Nothing Then
            Exit Sub
        End If

        lv.Items.Clear()

        'Calcular Moneda
        CalcularMoneda()

        AgruparDetalle()

        CData.OrderDataTable(dtDetalleAsiento, "Orden")
        For Each i As DataRow In dtDetalleAsiento.Rows
            Dim item As New ListViewItem
            item = lv.Items.Add(i("Codigo").ToString)
            item.SubItems.Add(i("Descripcion").ToString)
            item.SubItems.Add(CSistema.FormatoNumero(i("Debito").ToString))
            item.SubItems.Add(CSistema.FormatoNumero(i("Credito").ToString))
        Next

    End Sub
    'SC: 16/02/2022 - Nuevo
    Sub ListarDetalleCierre(ByVal dgv As DataGridView)

        If dtDetalleCierre Is Nothing Then
            Exit Sub
        End If

        'Limpiamos todo el detalle
        dgv.Rows.Clear()

        'Calcular Moneda
        CalcularMoneda()

        'Volver a Ordenar
        AgruparDetalleCierre()

        For Each i As DataRow In dtDetalleCierre.Rows

            Dim Registro(3) As String
            Registro(0) = i("Codigo").ToString
            Registro(1) = i("Descripcion").ToString
            Registro(2) = CSistema.FormatoNumero(i("Debito").ToString)
            Registro(3) = CSistema.FormatoMoneda(i("Credito").ToString)

            Try
                dgv.Rows.Add(Registro)
            Catch ex As Exception

            End Try
        Next

    End Sub

    'SC: 16/02/2022 -Nuevo
    Sub AgruparDetalleCierre()

        If NoAgrupar = True Then
            Exit Sub
        End If

        Dim dttemp As DataTable = dtDetalleCierre.Clone

        For Each oRow As DataRow In dtDetalleCierre.Rows

            Dim IDUnidadNegocio As Integer
            Dim IDCentroCosto As Integer
            If IsDBNull(oRow("IDUnidadNegocio")) Then
                IDUnidadNegocio = 0
                IDCentroCosto = 0
            Else
                IDUnidadNegocio = oRow("IDUnidadNegocio")
                IDCentroCosto = oRow("IDCentroCosto")
            End If

            Dim Existe As Integer = dttemp.Select(" Codigo = '" & oRow("Codigo").ToString & "' And Observacion='" & oRow("Observacion") & "' And IDUnidadNegocio= " & IDUnidadNegocio & " And IDCentroCosto= " & IDCentroCosto & "").Count

            'Agregar a dttemp
            If Existe > 0 Then

                Dim TempRow As DataRow = dttemp.Select(" Codigo = '" & oRow("Codigo").ToString & "' And IDUnidadNegocio= " & IDUnidadNegocio & " And IDCentroCosto= " & IDCentroCosto & "")(0)

                If IsNumeric(oRow("Debito")) = False Then
                    oRow("Debito") = 0
                End If

                If IsNumeric(oRow("Credito")) = False Then
                    oRow("Credito") = 0
                End If

                TempRow("Debito") = TempRow("Debito") + oRow("Debito")
                TempRow("Credito") = TempRow("Credito") + oRow("Credito")

            End If

            'Insertar en dttemp
            If Existe = 0 Then

                Dim TempRow As DataRow = dttemp.NewRow
                TempRow.ItemArray = oRow.ItemArray
                dttemp.Rows.Add(TempRow)

            End If

        Next

        dtDetalleCierre.Rows.Clear()
        dtDetalleCierre = dttemp

    End Sub

    Sub ListarDetalle(ByVal dgv As DataGridView)

        If dtDetalleAsiento Is Nothing Then
            Exit Sub
        End If

        'Limpiamos todo el detalle
        dgv.Rows.Clear()

        'Calcular Moneda
        CalcularMoneda()

        'FA 14/03/2023
        'Volver a Ordenar
        'AgruparDetalle()

        CData.OrderDataTable(dtDetalleAsiento, "Orden")

        For Each i As DataRow In dtDetalleAsiento.Rows

            Dim Registro(5) As String
            Registro(0) = i("Codigo").ToString
            Registro(1) = i("Descripcion").ToString
            Registro(2) = i("Observacion").ToString & "-" & i("UnidadNegocioDetalle") & "-" & i("Departamento") & "-" & i("Seccion")
            Registro(3) = i("NroComprobante").ToString
            Registro(4) = CSistema.FormatoNumero(i("Debito").ToString)
            Registro(5) = CSistema.FormatoMoneda(i("Credito").ToString)

            Try
                dgv.Rows.Add(Registro)
            Catch ex As Exception

            End Try
        Next

    End Sub

    Sub AgruparDetalle()

        If NoAgrupar = True Then
            Exit Sub
        End If

        Dim dttemp As DataTable = dtDetalleAsiento.Clone

        For Each oRow As DataRow In dtDetalleAsiento.Rows

            Dim IDUnidadNegocio As Integer
            Dim IDCentroCosto As Integer
            If IsDBNull(oRow("IDUnidadNegocio")) Then
                IDUnidadNegocio = 0
                IDCentroCosto = 0
            Else
                IDUnidadNegocio = oRow("IDUnidadNegocio")
                IDCentroCosto = oRow("IDCentroCosto")
            End If

            Dim Existe As Integer = dttemp.Select(" Codigo = '" & oRow("Codigo").ToString & "' And Observacion='" & oRow("Observacion") & "' And IDUnidadNegocio= " & IDUnidadNegocio & " And IDCentroCosto= " & IDCentroCosto & "").Count

            'Agregar a dttemp
            If Existe > 0 Then

                Dim TempRow As DataRow = dttemp.Select(" Codigo = '" & oRow("Codigo").ToString & "' And IDUnidadNegocio= " & IDUnidadNegocio & " And IDCentroCosto= " & IDCentroCosto & "")(0)

                If IsNumeric(oRow("Debito")) = False Then
                    oRow("Debito") = 0
                End If

                If IsNumeric(oRow("Credito")) = False Then
                    oRow("Credito") = 0
                End If

                TempRow("Debito") = TempRow("Debito") + oRow("Debito")
                TempRow("Credito") = TempRow("Credito") + oRow("Credito")

            End If

            'Insertar en dttemp
            If Existe = 0 Then

                Dim TempRow As DataRow = dttemp.NewRow
                TempRow.ItemArray = oRow.ItemArray
                dttemp.Rows.Add(TempRow)

            End If

        Next

        dtDetalleAsiento.Rows.Clear()
        dtDetalleAsiento = dttemp

    End Sub

    'SC: 17/02/2022 - Nuevo
    Sub ListarDetalleApertura(ByVal dgv As DataGridView)

        If dtDetalleApertura Is Nothing Then
            Exit Sub
        End If

        'Limpiamos todo el detalle
        dgv.Rows.Clear()

        'Calcular Moneda
        CalcularMoneda()

        'Volver a Ordenar
        AgruparDetalleApertura()

        For Each i As DataRow In dtDetalleApertura.Rows

            Dim Registro(3) As String
            Registro(0) = i("Codigo").ToString
            Registro(1) = i("Descripcion").ToString
            Registro(2) = CSistema.FormatoNumero(i("Debito").ToString)
            Registro(3) = CSistema.FormatoMoneda(i("Credito").ToString)

            Try
                dgv.Rows.Add(Registro)
            Catch ex As Exception

            End Try
        Next

    End Sub

    'SC: 17/02/2022 - Nuevo
    Sub AgruparDetalleApertura()

        If NoAgrupar = True Then
            Exit Sub
        End If

        Dim dttemp As DataTable = dtDetalleApertura.Clone

        For Each oRow As DataRow In dtDetalleApertura.Rows

            Dim IDUnidadNegocio As Integer
            Dim IDCentroCosto As Integer
            If IsDBNull(oRow("IDUnidadNegocio")) Then
                IDUnidadNegocio = 0
                IDCentroCosto = 0
            Else
                IDUnidadNegocio = oRow("IDUnidadNegocio")
                IDCentroCosto = oRow("IDCentroCosto")
            End If

            Dim Existe As Integer = dttemp.Select(" Codigo = '" & oRow("Codigo").ToString & "' And Observacion='" & oRow("Observacion") & "' And IDUnidadNegocio= " & IDUnidadNegocio & " And IDCentroCosto= " & IDCentroCosto & "").Count

            'Agregar a dttemp
            If Existe > 0 Then

                Dim TempRow As DataRow = dttemp.Select(" Codigo = '" & oRow("Codigo").ToString & "' And IDUnidadNegocio= " & IDUnidadNegocio & " And IDCentroCosto= " & IDCentroCosto & "")(0)

                If IsNumeric(oRow("Debito")) = False Then
                    oRow("Debito") = 0
                End If

                If IsNumeric(oRow("Credito")) = False Then
                    oRow("Credito") = 0
                End If

                TempRow("Debito") = TempRow("Debito") + oRow("Debito")
                TempRow("Credito") = TempRow("Credito") + oRow("Credito")

            End If

            'Insertar en dttemp
            If Existe = 0 Then

                Dim TempRow As DataRow = dttemp.NewRow
                TempRow.ItemArray = oRow.ItemArray
                dttemp.Rows.Add(TempRow)

            End If

        Next

        dtDetalleApertura.Rows.Clear()
        dtDetalleApertura = dttemp

    End Sub

    Sub CalcularMoneda()

        'Obtenemos la cotizacion
        Dim Cotizacion As Decimal
        Dim IDMoneda As Integer

        If dtAsiento Is Nothing Then
            Exit Sub
        End If

        If dtAsiento.Rows.Count = 0 Then
            Exit Sub
        End If

        IDMoneda = dtAsiento.Rows(0)("IDMoneda")
        Cotizacion = dtAsiento.Rows(0)("Cotizacion")

        'Si la moneda es primaria, salir
        If IDMoneda = 1 Then
            Exit Sub
        End If

        'Si la cotizacion es 0 o 1, salir
        If Cotizacion <= 1 Then
            Exit Sub
        End If

        Dim TotalTemp As Decimal = 0

        'Resolver este tema, no funciona asi
        Exit Sub

        For Each oRow As DataRow In dtDetalleAsiento.Rows

            If oRow("Debito") > 0 Then
                oRow("Debito") = oRow("Importe") * Cotizacion
            End If

            If oRow("Credito") > 0 Then
                oRow("Credito") = oRow("Importe") * Cotizacion
            End If

            TotalTemp = TotalTemp + (oRow("Importe") * Cotizacion)

        Next

        Total = TotalTemp

    End Sub

    Public Function Guardar(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        Guardar = False

        Dim param(-1) As SqlClient.SqlParameter

        'Validar
        If dtAsiento Is Nothing Then
            Return True
        End If

        If dtAsiento.Rows.Count = 0 Then
            Return True
        End If

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Numero", dtAsiento.Rows(0)("Numero").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCiudad", dtAsiento.Rows(0)("IDCiudad").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSucursal", dtAsiento.Rows(0)("IDSucursal").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Fecha", CSistema.FormatoFechaBaseDatos(dtAsiento.Rows(0)("Fecha").ToString, True, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDMoneda", dtAsiento.Rows(0)("IDMoneda").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Cotizacion", CSistema.FormatoMonedaBaseDatos(dtAsiento.Rows(0)("Cotizacion").ToString, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDTipoComprobante", dtAsiento.Rows(0)("IDTipoComprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@NroComprobante", dtAsiento.Rows(0)("NroComprobante").ToString, ParameterDirection.Input)
        'CSistema.SetSQLParameter(param, "@NroComprobante", dtAsiento.Rows(0)("Comprobante").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Detalle", dtAsiento.Rows(0)("Detalle").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Debito", CSistema.FormatoMonedaBaseDatos(dtAsiento.Rows(0)("Debito").ToString, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Credito", CSistema.FormatoMonedaBaseDatos(dtAsiento.Rows(0)("Credito").ToString, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Saldo", CSistema.FormatoMonedaBaseDatos(dtAsiento.Rows(0)("Saldo").ToString, False), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Total", CSistema.FormatoMonedaBaseDatos(dtAsiento.Rows(0)("Total").ToString, True), ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Bloquear", Bloquear, ParameterDirection.Input)
        'FA 02/12/2022 - Plan de cuenta
        CSistema.SetSQLParameter(param, "@GastosMultiples", dtAsiento.Rows(0)("GastosMultiples").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDUnidadNegocio", dtAsiento.Rows(0)("IDUnidadNegocio").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDCentroCosto", dtAsiento.Rows(0)("IDCentroCosto").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDDepartamento", dtAsiento.Rows(0)("IDDepartamento").ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@IDSeccion", dtAsiento.Rows(0)("IDSeccion").ToString, ParameterDirection.Input)

        CSistema.SetSQLParameter(param, "@IDUsuario", vgIDUsuario, ParameterDirection.Input)

        'Caja
        If NroCaja = 0 Then
            Dim CCaja As New CCaja
            CCaja.IDSucursal = dtAsiento.Rows(0)("IDSucursal")
            CCaja.ObtenerUltimoNumero()
            CSistema.SetSQLParameter(param, "@IDTransaccionCaja", CCaja.IDTransaccion, ParameterDirection.Input)
        Else
            Dim CCaja As New CCaja
            CCaja.IDSucursal = dtAsiento.Rows(0)("IDSucursal")
            CCaja.Obtener(NroCaja)
            CSistema.SetSQLParameter(param, "@IDTransaccionCaja", CCaja.IDTransaccion, ParameterDirection.Input)
        End If



        'Informacion de Salida
        CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
        CSistema.SetSQLParameter(param, "@Procesado", "False", ParameterDirection.Output)

        Dim MensajeRetorno As String = ""

        'Insertar Registro
        If CSistema.ExecuteStoreProcedure(param, "SpAsiento", False, False, MensajeRetorno) = False Then
            MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False

        End If

        'Insertamos el Detalle
        If InsertarDetalle(ERP.CSistema.NUMOperacionesRegistro.INS) = False Then

            'Eliminar el Registro si es que se registro
            Return False

        End If

        Return True

    End Function

    Function InsertarDetalle(ByVal Operacion As CSistema.NUMOperacionesRegistro) As Boolean

        InsertarDetalle = True

        dtDetalleAsiento.DefaultView.Sort = "Orden"

        For Each oRow As DataRowView In dtDetalleAsiento.DefaultView

            Dim param(-1) As SqlClient.SqlParameter

            CSistema.SetSQLParameter(param, "@IDTransaccion", IDTransaccion, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCuentaContable", oRow("IDCuentaContable").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@ID", oRow("ID").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Debito", CSistema.FormatoMonedaBaseDatos(oRow("Debito").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Credito", CSistema.FormatoMonedaBaseDatos(oRow("Credito").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@Observacion", oRow("Observacion").ToString, ParameterDirection.Input)

            CSistema.SetSQLParameter(param, "@IDSucursal", oRow("IDSucursal").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@TipoComprobante", oRow("TipoComprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@NroComprobante", oRow("NroComprobante").ToString, ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDUnidadNegocio", CSistema.RetornarValorInteger(oRow("IDUnidadNegocio").ToString), ParameterDirection.Input)
            CSistema.SetSQLParameter(param, "@IDCentroCosto", CSistema.RetornarValorInteger(oRow("IDCentroCosto").ToString), ParameterDirection.Input)

            'FA 30/11/2022 - Plan de cuenta
            If dtAsiento.Rows(0)("GastosMultiples").ToString = True Then
                CSistema.SetSQLParameter(param, "@IDUnidadNegocioDetalle", oRow("IDUnidadNegocioDetalle").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDCentroCostoDetalle", oRow("IDCentroCostoDetalle").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", oRow("IDDepartamentoEmpresa").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDSeccionDetalle", oRow("IDSeccionDetalle").ToString, ParameterDirection.Input)
            Else
                CSistema.SetSQLParameter(param, "@IDUnidadNegocioDetalle", dtAsiento.Rows(0)("IDUnidadNegocio").ToString, ParameterDirection.Input)
                'CSistema.SetSQLParameter(param, "@IDCentroCostoDetalle", dtAsiento.Rows(0)("IDCentroCosto").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDCentroCostoDetalle", oRow("IDCentroCosto").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDDepartamentoEmpresa", dtAsiento.Rows(0)("IDDepartamento").ToString, ParameterDirection.Input)
                CSistema.SetSQLParameter(param, "@IDSeccionDetalle", dtAsiento.Rows(0)("IDSeccion").ToString, ParameterDirection.Input)
            End If
            CSistema.SetSQLParameter(param, "@Operacion", Operacion.ToString, ParameterDirection.Input)

            'Informacion de Salida
            CSistema.SetSQLParameter(param, "@Mensaje", "", ParameterDirection.Output, 200)
            CSistema.SetSQLParameter(param, "@Procesado", "", ParameterDirection.Output)

            'Insertar el detalle
            Dim MensajeRetorno As String = ""

            If CSistema.ExecuteStoreProcedure(param, "SpDetalleAsiento", False, False, MensajeRetorno) = False Then
                MessageBox.Show(MensajeRetorno, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If

            'If oRow("IDCentroCosto").ToString <> 0 Then
            '    CentroCosto()
            'End If

        Next

    End Function

    Sub CentroCosto()
        For Each oRow As DataRow In dtDetalleAsiento.Select("IDCentroCosto is not null ")
            Dim sql As String = "Update Asiento Set IDCentroCosto = " & oRow("IDCentroCostoDetalle") & "Where IDTransaccion=" & IDTransaccion

            If CSistema.ExecuteNonQuery(sql) = 0 Then
                'Return False
            End If
        Next
    End Sub

    Sub EliminarDetalleFijos()

        If Generado = True Then

            'Eliminamos del detalle los que son fijos
            Dim Cantidad As Integer = 0
            Dim deleteRow(-1) As DataRow
            For Each r As DataRow In dtDetalleAsiento.Rows
                If CBool(r("Fijo")) = True Then
                    ReDim Preserve deleteRow(Cantidad)
                    deleteRow(Cantidad) = r
                    Cantidad = Cantidad + 1
                End If
            Next

            For i As Integer = 0 To deleteRow.GetLength(0) - 1
                dtDetalleAsiento.Rows.Remove(deleteRow(i))
            Next

        End If

    End Sub

    Function ObtenerSaldo() As Decimal

        Dim Credito As Decimal
        Dim Debito As Decimal

        For Each oRow As DataRow In dtDetalleAsiento.Rows
            Credito = Credito + CSistema.FormatoNumero(CDec(oRow("Credito").ToString), False)
            Debito = Debito + CSistema.FormatoNumero(CDec(oRow("Debito").ToString), False)
        Next

        Saldo = Debito - Credito

        'Si tiene esta condicion guarda con diferencias dbordon 18/01/2017
        'If Saldo <= 1 And Saldo >= -1 Then
        'Saldo = 0
        'End If

        Return Saldo

    End Function

    Function ObtenerTotal() As Decimal

        Dim Credito As Decimal
        Dim Debito As Decimal

        For Each oRow As DataRow In dtDetalleAsiento.Rows
            Credito = Credito + CDec(oRow("Credito").ToString)
            Debito = Debito + CDec(oRow("Debito").ToString)
        Next

        If Credito > Debito Then
            Total = Credito
        Else
            Total = Debito
        End If

        Return Total

    End Function

    Public Function InsertAsiento(ByVal IDTransaccion As Integer) As String

        InsertAsiento = ""

        Dim param As New DataTable

        'SetSQLParameter, ayuda a generar y configurar los parametros.
        'Simplemente describir el Nombre del Campo, el valor y el tipo (Estas informaciones ver en la Base de Datos)

        CSistema.SetSQLParameter(param, "IDTransaccion", IDTransaccion)
        CSistema.SetSQLParameter(param, "Numero", dtAsiento.Rows(0)("Numero").ToString)
        CSistema.SetSQLParameter(param, "IDSucursal", dtAsiento.Rows(0)("IDSucursal").ToString)
        CSistema.SetSQLParameter(param, "Fecha", CSistema.FormatoFechaBaseDatos(dtAsiento.Rows(0)("Fecha").ToString, True, False))
        CSistema.SetSQLParameter(param, "IDMoneda", dtAsiento.Rows(0)("IDMoneda").ToString)
        CSistema.SetSQLParameter(param, "Cotizacion", dtAsiento.Rows(0)("Cotizacion").ToString)
        CSistema.SetSQLParameter(param, "IDTipoComprobante", dtAsiento.Rows(0)("IDTipoComprobante").ToString)
        CSistema.SetSQLParameter(param, "NroComprobante", dtAsiento.Rows(0)("NroComprobante").ToString)
        CSistema.SetSQLParameter(param, "Detalle", dtAsiento.Rows(0)("Detalle").ToString)
        CSistema.SetSQLParameter(param, "Debito", dtAsiento.Rows(0)("Debito").ToString)
        CSistema.SetSQLParameter(param, "Credito", dtAsiento.Rows(0)("Credito").ToString)
        CSistema.SetSQLParameter(param, "Saldo", dtAsiento.Rows(0)("Saldo").ToString)
        CSistema.SetSQLParameter(param, "Total", dtAsiento.Rows(0)("Total").ToString)
        CSistema.SetSQLParameter(param, "IDCentroCosto", dtAsiento.Rows(0)("IDCentroCosto").ToString)

        InsertAsiento = CSistema.InsertSQL(param, "Asiento")

    End Function

    Function InsertDetalle(ByVal IDTransaccion As Integer) As String

        InsertDetalle = ""

        For Each oRow As DataRow In dtDetalleAsiento.Rows

            Dim param As New DataTable

            CSistema.SetSQLParameter(param, "IDTransaccion", IDTransaccion)
            CSistema.SetSQLParameter(param, "IDCuentaContable", oRow("IDCuentaContable").ToString)
            CSistema.SetSQLParameter(param, "ID", oRow("ID").ToString)
            CSistema.SetSQLParameter(param, "Debito", CSistema.FormatoMonedaBaseDatos(oRow("Debito").ToString))
            CSistema.SetSQLParameter(param, "Credito", CSistema.FormatoMonedaBaseDatos(oRow("Credito").ToString))
            CSistema.SetSQLParameter(param, "Importe", CSistema.FormatoMonedaBaseDatos(oRow("Importe").ToString))
            CSistema.SetSQLParameter(param, "Observacion", oRow("Observacion").ToString)
            CSistema.SetSQLParameter(param, "IDUnidadNegocio", oRow("IDUnidadNegocio").ToString)
            CSistema.SetSQLParameter(param, "IDCentroCosto", oRow("IDCentroCosto").ToString)

            InsertDetalle = InsertDetalle & CSistema.InsertSQL(param, "DetalleAsiento") & vbCrLf

        Next

    End Function

    Sub CargarOperacion(ByVal IDTransaccion As Integer)

        'Cargamos la operacion
        dtAsiento = CSistema.ExecuteToDataTable("Select * From VAsiento Where IDTransaccion=" & IDTransaccion)
        dtAsiento.Columns.Add("IDDeposito")

        dtDetalleAsiento = CSistema.ExecuteToDataTable("Select * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion & " Order By ID")
        dtDetalleAsiento.Columns.Add("Fijo")

        'Listar Detalle

        'Totales

    End Sub

End Class

Public Class CAsientoAplicacionAnticipoProveedores
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property


    Public Property dtDocumento As DataTable

    Sub InicializarAsiento()
        'Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Inicializar()

    End Sub



    Sub Generar(dtEgresos As DataTable)

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            IDSucursal = oRow("IDSucursal").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0
        'Dim dtProveedores As DataTable
        Dim codigo As String = ""
        Dim codigonew As Boolean = False
        Dim Debito As Integer = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        For Each oRow As DataRow In dtEgresos.Select("Seleccionado = 'True' ")
            Try

                codigo = oRow("DACodigo").ToString
                'InsertarDetalle la primera vez que recorre dtEgresos
                If codigonew = False And dtDetalleAsiento.Rows.Count < 2 Then
                    NewRow("IDTransaccion") = 0
                    NewRow("ID") = dtDetalleAsiento.Rows.Count
                    NewRow("IDCuentaContable") = oRow("DAIDCuentaContable").ToString
                    NewRow("Codigo") = oRow("DACodigo").ToString
                    NewRow("Credito") = oRow("DADebito").ToString
                    NewRow("Debito") = oRow("Importe").ToString
                    NewRow("Importe") = oRow("Importe").ToString
                    NewRow("Descripcion") = oRow("DADescripcion").ToString
                    NewRow("Observacion") = oRow("DAObservacion").ToString
                    NewRow("Alias") = oRow("DAAlias").ToString
                    codigonew = True
                    dtDetalleAsiento.Rows.Add(NewRow)
                Else
                    'Si ya existe la misma cuentacontable acumula
                    If NewRow("Codigo") = codigo Then
                        Debito = CDec(NewRow("Debito"))
                        Debito = Debito + CDec(oRow("Importe").ToString)
                        NewRow("Debito") = Debito
                        codigonew = True
                    Else
                        'Sino es la misma cuentacontable, resguarda el registro nuevo
                        NewRow("IDTransaccion") = 0
                        NewRow("ID") = dtDetalleAsiento.Rows.Count
                        NewRow("IDCuentaContable") = oRow("DAIDCuentaContable").ToString
                        NewRow("Codigo") = oRow("DACodigo").ToString
                        NewRow("Credito") = oRow("Importe").ToString
                        NewRow("Debito") = oRow("DADebito").ToString
                        NewRow("Importe") = oRow("Importe").ToString
                        NewRow("Descripcion") = oRow("DADescripcion").ToString
                        NewRow("Observacion") = oRow("DAObservacion").ToString
                        NewRow("Alias") = oRow("DAAlias").ToString
                        codigonew = True
                        dtDetalleAsiento.Rows.Add(NewRow)

                    End If
                End If
            Catch ex As Exception

            End Try
        Next
        Generado = True
    End Sub

End Class


Public Class CAsientoContableCobranzaCredito

    'PROPIEDADES
    Private CAsientoValue As CAsiento
    Public Property CAsiento() As CAsiento
        Get
            Return CAsientoValue
        End Get
        Set(ByVal value As CAsiento)
            CAsientoValue = value
        End Set
    End Property

    Private dtCuentaFijaCobranzaCreditoValue As DataTable
    Public Property dtCuentaFijaCobranzaCredito() As DataTable
        Get
            Return dtCuentaFijaCobranzaCreditoValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaCobranzaCreditoValue = value
        End Set
    End Property

    Private dtCuentaFijaFormaPagoEfectivoValue As DataTable
    Public Property dtCuentaFijaFormaPagoEfectivo() As DataTable
        Get
            Return dtCuentaFijaFormaPagoEfectivoValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaFormaPagoEfectivoValue = value
        End Set
    End Property

    Private dtCuentaFijaFormaPagoChequeValue As DataTable
    Public Property dtCuentaFijaFormaPagoCheque() As DataTable
        Get
            Return dtCuentaFijaFormaPagoChequeValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaFormaPagoChequeValue = value
        End Set
    End Property

    Private dtCuentaFijaFormaPagoDocumentoValue As DataTable
    Public Property dtCuentaFijaFormaPagoDocumento() As DataTable
        Get
            Return dtCuentaFijaFormaPagoDocumentoValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaFormaPagoDocumentoValue = value
        End Set
    End Property
    Private dtCuentaFijaFormaPagoTarjetaValue As DataTable
    Public Property dtCuentaFijaFormaPagoTarjeta() As DataTable
        Get
            Return dtCuentaFijaFormaPagoTarjetaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaFormaPagoTarjetaValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        'Funciones
        CAsiento = New CAsiento
        CAsiento.Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Inicializar Cuentas Fijas Efectivo
        dtCuentaFijaCobranzaCredito = CAsiento.CSistema.ExecuteToDataTable("Select * From VCuentaFijaCobranzaCredito").Copy

        'Inicializar Cuentas Fijas Efectivo
        dtCuentaFijaFormaPagoEfectivo = CAsiento.CSistema.ExecuteToDataTable("Select * From VCuentaFijaFormaPagoEfectivo").Copy

        'Inicializar Cuentas Fijas Cheque
        dtCuentaFijaFormaPagoCheque = CAsiento.CSistema.ExecuteToDataTable("Select * From VCuentaFijaFormaPagoCheque").Copy

        'Inicializar Cuentas Fijas Efectivo
        dtCuentaFijaFormaPagoDocumento = CAsiento.CSistema.ExecuteToDataTable("Select * From VCuentaFijaFormaPagoDocumento").Copy

    End Sub

    Sub Limpiar()

        CAsiento.Limpiar()

    End Sub

    Public Function Validar(ByRef ctrError As ErrorProvider, ByVal ctr As Control, ByRef tsslEstado As ToolStripStatusLabel, ByVal alignment As ErrorIconAlignment) As Boolean

        Dim CError As New CError
        Dim Mensaje As String

        Validar = False

        'Validar que la cabecera del asiento este creado
        If CAsiento.dtAsiento Is Nothing Then
            Mensaje = "El asiento no fue creado correctamente!"
            CError.MostrarError(Mensaje, ctrError, ctr, tsslEstado, alignment)
            Exit Function
        End If

        'Validar que la cabecera del asiento tenga registros
        If CAsiento.dtAsiento.Rows.Count = 0 Then
            Mensaje = "El asiento no fue creado correctamente!"
            CError.MostrarError(Mensaje, ctrError, ctr, tsslEstado, alignment)
            Exit Function
        End If

        'Validar que el detalle del asiento este creado
        If CAsiento.dtDetalleAsiento Is Nothing Then
            Mensaje = "El asiento no tiene ningun detalle!"
            CError.MostrarError(Mensaje, ctrError, ctr, tsslEstado, alignment)
            Exit Function
        End If

        'Validar que el detalle del asiento tenga registros
        If CAsiento.dtDetalleAsiento.Rows.Count = 0 Then
            Mensaje = "El asiento no tiene ningun detalle!"
            CError.MostrarError(Mensaje, ctrError, ctr, tsslEstado, alignment)
            Exit Function
        End If

        'Validar el saldo
        Dim Credito As Decimal = 0
        Dim Debito As Decimal = 0

        For Each oRow As DataRow In CAsiento.dtDetalleAsiento.Rows
            Credito = Credito + oRow("Credito")
            Debito = Debito + oRow("Debito")
        Next

        If Credito = 0 Or Debito = 0 Then
            Mensaje = "Los valores del detalle del asiento no son correctos!"
            CError.MostrarError(Mensaje, ctrError, ctr, tsslEstado, alignment)
            Exit Function
        End If

        Dim Saldo As Decimal = Credito - Debito

        If Saldo <> 0 Then
            Mensaje = "El saldo del asiento no es correcto!"
            CError.MostrarError(Mensaje, ctrError, ctr, tsslEstado, alignment)
            Exit Function
        End If

        Return True

    End Function

    Sub InsertarEfectivo(ByVal Importe As Decimal, ByVal IDTipoComprobante As Integer, ByVal IDMoneda As Integer)

        Dim IDCuenta As Integer = 0

        For Each oRow As DataRow In dtCuentaFijaFormaPagoEfectivo.Select("IDTipoComprobante=" & IDTipoComprobante & " And IDMoneda=" & IDMoneda)

            IDCuenta = oRow("IDCuentaContable").ToString

            'Si no existe, agregamos
            Dim Existe As Integer = CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta).Count

            If Existe = 0 Then

                Dim NewRow As DataRow = CAsiento.dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("Orden") = CInt(oRow("Orden").ToString)
                NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                NewRow("Codigo") = oRow("Codigo").ToString
                NewRow("Descripcion") = oRow("Cuenta").ToString
                NewRow("Cuenta") = oRow("CuentaContable").ToString
                NewRow("Alias") = oRow("Codigo").ToString

                'Montos
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    CAsiento.dtDetalleAsiento.Rows.Add(NewRow)
                Else
                    CAsiento.dtDetalleAsiento.Rows.Remove(NewRow)
                End If

            Else

                For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                    'Montos
                    If oRow("Debe").ToString = True Then
                        UpdateRow("Credito") = 0
                        UpdateRow("Debito") = CDec(UpdateRow("Debito")) + Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        UpdateRow("Credito") = CDec(UpdateRow("Credito")) + Importe
                        UpdateRow("Debito") = 0
                    End If

                    'Orden
                    If CInt(UpdateRow("Orden")) > CInt(oRow("Orden").ToString) Then
                        UpdateRow("Orden") = CInt(oRow("Orden").ToString)
                    End If


                    UpdateRow("Importe") = CDec(UpdateRow("Importe")) + Importe

                Next

            End If


        Next

    End Sub

    Sub EliminarEfectivo(ByVal Importe As Decimal, ByVal IDTipoComprobante As Integer, ByVal IDMoneda As Integer)

        Dim IDCuenta As Integer = 0

        For Each oRow As DataRow In dtCuentaFijaFormaPagoEfectivo.Select("IDTipoComprobante=" & IDTipoComprobante & " And IDMoneda=" & IDMoneda)

            IDCuenta = oRow("IDCuentaContable").ToString

            For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                'Montos
                If oRow("Debe").ToString = True Then
                    UpdateRow("Credito") = 0
                    UpdateRow("Debito") = CDec(UpdateRow("Debito")) - Importe
                End If

                If oRow("Haber").ToString = True Then
                    UpdateRow("Credito") = CDec(UpdateRow("Credito")) - Importe
                    UpdateRow("Debito") = 0
                End If

                UpdateRow("Importe") = CDec(UpdateRow("Importe")) - Importe

                If CDec(UpdateRow("Importe").ToString) = 0 Then
                    CAsiento.dtDetalleAsiento.Rows.Remove(UpdateRow)
                End If

            Next

        Next

    End Sub

    Sub InsertarTarjeta(ByVal Importe As Decimal, ByVal IDTipoComprobante As Integer, ByVal IDMoneda As Integer)

        Dim IDCuenta As Integer = 0

        For Each oRow As DataRow In dtCuentaFijaFormaPagoTarjeta.Select("IDTipoComprobante=" & IDTipoComprobante & " And IDMoneda=" & IDMoneda)

            IDCuenta = oRow("IDCuentaContable").ToString

            'Si no existe, agregamos
            Dim Existe As Integer = CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta).Count

            If Existe = 0 Then

                Dim NewRow As DataRow = CAsiento.dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("Orden") = CInt(oRow("Orden").ToString)
                NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                NewRow("Codigo") = oRow("Codigo").ToString
                NewRow("Descripcion") = oRow("Cuenta").ToString
                NewRow("Cuenta") = oRow("CuentaContable").ToString
                NewRow("Alias") = oRow("Codigo").ToString

                'Montos
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    CAsiento.dtDetalleAsiento.Rows.Add(NewRow)
                Else
                    CAsiento.dtDetalleAsiento.Rows.Remove(NewRow)
                End If

            Else

                For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                    'Montos
                    If oRow("Debe").ToString = True Then
                        UpdateRow("Credito") = 0
                        UpdateRow("Debito") = CDec(UpdateRow("Debito")) + Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        UpdateRow("Credito") = CDec(UpdateRow("Credito")) + Importe
                        UpdateRow("Debito") = 0
                    End If

                    'Orden
                    If CInt(UpdateRow("Orden")) > CInt(oRow("Orden").ToString) Then
                        UpdateRow("Orden") = CInt(oRow("Orden").ToString)
                    End If


                    UpdateRow("Importe") = CDec(UpdateRow("Importe")) + Importe

                Next

            End If


        Next

    End Sub

    Sub EliminarTarjeta(ByVal Importe As Decimal, ByVal IDTipoComprobante As Integer, ByVal IDMoneda As Integer)

        Dim IDCuenta As Integer = 0

        For Each oRow As DataRow In dtCuentaFijaFormaPagoTarjeta.Select("IDTipoComprobante=" & IDTipoComprobante & " And IDMoneda=" & IDMoneda)

            IDCuenta = oRow("IDCuentaContable").ToString

            For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                'Montos
                If oRow("Debe").ToString = True Then
                    UpdateRow("Credito") = 0
                    UpdateRow("Debito") = CDec(UpdateRow("Debito")) - Importe
                End If

                If oRow("Haber").ToString = True Then
                    UpdateRow("Credito") = CDec(UpdateRow("Credito")) - Importe
                    UpdateRow("Debito") = 0
                End If

                UpdateRow("Importe") = CDec(UpdateRow("Importe")) - Importe

                If CDec(UpdateRow("Importe").ToString) = 0 Then
                    CAsiento.dtDetalleAsiento.Rows.Remove(UpdateRow)
                End If

            Next

        Next

    End Sub

    Sub InsertarCheque(ByVal Importe As Decimal, ByVal Diferido As Boolean, ByVal IDMoneda As Integer)

        Dim IDCuenta As Integer = 0

        For Each oRow As DataRow In dtCuentaFijaFormaPagoCheque.Select("Diferido='" & Diferido.ToString & "' And IDMoneda=" & IDMoneda)

            IDCuenta = oRow("IDCuentaContable").ToString

            'Si no existe, agregamos
            Dim Existe As Integer = CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta).Count

            If Existe = 0 Then

                Dim NewRow As DataRow = CAsiento.dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("Orden") = CInt(oRow("Orden").ToString)
                NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                NewRow("Codigo") = oRow("Codigo").ToString
                NewRow("Descripcion") = oRow("Cuenta").ToString
                NewRow("Cuenta") = oRow("CuentaContable").ToString
                NewRow("Alias") = oRow("Codigo").ToString

                'Montos
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    CAsiento.dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Else

                For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                    'Montos
                    If oRow("Debe").ToString = True Then
                        UpdateRow("Credito") = 0
                        UpdateRow("Debito") = CDec(UpdateRow("Debito")) + Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        UpdateRow("Credito") = CDec(UpdateRow("Credito")) + Importe
                        UpdateRow("Debito") = 0
                    End If

                    'Orden
                    If CInt(UpdateRow("Orden")) > CInt(oRow("Orden").ToString) Then
                        UpdateRow("Orden") = CInt(oRow("Orden").ToString)
                    End If

                    UpdateRow("Importe") = CDec(UpdateRow("Importe")) + Importe

                Next

            End If



        Next

    End Sub

    Sub EliminarCheque(ByVal Importe As Decimal, ByVal Diferido As Boolean, ByVal IDMoneda As Integer)

        Dim IDCuenta As Integer = 0

        For Each oRow As DataRow In dtCuentaFijaFormaPagoCheque.Select("Diferido='" & Diferido.ToString & "' And IDMoneda=" & IDMoneda)

            IDCuenta = oRow("IDCuentaContable").ToString

            For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                'Montos
                If oRow("Debe").ToString = True Then
                    UpdateRow("Credito") = 0
                    UpdateRow("Debito") = CDec(UpdateRow("Debito")) - Importe
                End If

                If oRow("Haber").ToString = True Then
                    UpdateRow("Credito") = CDec(UpdateRow("Credito")) - Importe
                    UpdateRow("Debito") = 0
                End If

                UpdateRow("Importe") = CDec(UpdateRow("Importe")) - Importe

                If CDec(UpdateRow("Importe").ToString) = 0 Then
                    CAsiento.dtDetalleAsiento.Rows.Remove(UpdateRow)
                End If

            Next

        Next

    End Sub

    Sub InsertarDocumento(ByVal Importe As Decimal, ByVal IDTipoComprobante As Integer, ByVal IDMoneda As Integer)

        Dim IDCuenta As Integer = 0

        For Each oRow As DataRow In dtCuentaFijaFormaPagoDocumento.Select("IDTipoComprobante=" & IDTipoComprobante & " And IDMoneda=" & IDMoneda)

            IDCuenta = oRow("IDCuentaContable").ToString

            'Si no existe, agregamos
            Dim Existe As Integer = CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta).Count

            If Existe = 0 Then

                Dim NewRow As DataRow = CAsiento.dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("Orden") = CInt(oRow("Orden").ToString)
                NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                NewRow("Codigo") = oRow("Codigo").ToString
                NewRow("Descripcion") = oRow("Cuenta").ToString
                NewRow("Cuenta") = oRow("CuentaContable").ToString
                NewRow("Alias") = oRow("Codigo").ToString

                'Montos
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    CAsiento.dtDetalleAsiento.Rows.Add(NewRow)
                Else
                    CAsiento.dtDetalleAsiento.Rows.Remove(NewRow)
                End If

            Else

                For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                    'Montos
                    If oRow("Debe").ToString = True Then
                        UpdateRow("Credito") = 0
                        UpdateRow("Debito") = CDec(UpdateRow("Debito")) + Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        UpdateRow("Credito") = CDec(UpdateRow("Credito")) + Importe
                        UpdateRow("Debito") = 0
                    End If

                    'Orden
                    If CInt(UpdateRow("Orden")) > CInt(oRow("Orden").ToString) Then
                        UpdateRow("Orden") = CInt(oRow("Orden").ToString)
                    End If

                    UpdateRow("Importe") = CDec(UpdateRow("Importe")) + Importe

                Next

            End If


        Next

    End Sub

    Sub EliminarDocumento(ByVal Importe As Decimal, ByVal IDTipoComprobante As Integer, ByVal IDMoneda As Integer)

        Dim IDCuenta As Integer = 0

        For Each oRow As DataRow In dtCuentaFijaFormaPagoDocumento.Select("IDTipoComprobante=" & IDTipoComprobante & " And IDMoneda=" & IDMoneda)

            IDCuenta = oRow("IDCuentaContable").ToString

            For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                'Montos
                If oRow("Debe").ToString = True Then
                    UpdateRow("Credito") = 0
                    UpdateRow("Debito") = CDec(UpdateRow("Debito")) - Importe
                End If

                If oRow("Haber").ToString = True Then
                    UpdateRow("Credito") = CDec(UpdateRow("Credito")) - Importe
                    UpdateRow("Debito") = 0
                End If

                UpdateRow("Importe") = CDec(UpdateRow("Importe")) - Importe

                If CDec(UpdateRow("Importe").ToString) = 0 Then
                    CAsiento.dtDetalleAsiento.Rows.Remove(UpdateRow)
                End If

            Next

        Next

    End Sub

    Sub InsertarVenta(ByVal Importe As Decimal, ByVal IDTipoComprobante As Integer, ByVal IDMoneda As Integer, ByVal Credito As Boolean)

        Dim IDCuenta As Integer = 0
        Dim Orden As Integer = 0

        Dim Tipo As String = "CONTADO"

        If Credito = True Then
            Tipo = "CREDITO"
        End If

        For Each oRow As DataRow In dtCuentaFijaCobranzaCredito.Select("IDTipoComprobante=" & IDTipoComprobante & " And IDMoneda=" & IDMoneda & " And Tipo='" & Tipo & "'")

            IDCuenta = oRow("IDCuentaContable").ToString
            Orden = oRow("Orden").ToString

            'Si no existe, agregamos
            Dim Existe As Integer = CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta).Count

            If Existe = 0 Then

                Dim NewRow As DataRow = CAsiento.dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("Orden") = CInt(oRow("Orden").ToString)
                NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                NewRow("Codigo") = oRow("Codigo").ToString
                NewRow("Descripcion") = oRow("Cuenta").ToString
                NewRow("Cuenta") = oRow("CuentaContable").ToString
                NewRow("Alias") = oRow("Codigo").ToString

                'Montos
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = CAsiento.dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    CAsiento.dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Else
                For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                    'Montos
                    If oRow("Debe").ToString = True Then
                        UpdateRow("Credito") = 0
                        UpdateRow("Debito") = CDec(UpdateRow("Debito")) + Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        UpdateRow("Credito") = CDec(UpdateRow("Credito")) + Importe
                        UpdateRow("Debito") = 0
                    End If

                    'Orden
                    If CInt(UpdateRow("Orden")) > CInt(oRow("Orden").ToString) Then
                        UpdateRow("Orden") = CInt(oRow("Orden").ToString)
                    End If

                    UpdateRow("Importe") = CDec(UpdateRow("Importe")) + Importe

                Next

            End If


        Next

    End Sub

    Sub EliminarVenta(ByVal Importe As Decimal, ByVal IDTipoComprobante As Integer, ByVal IDMoneda As Integer, ByVal Credito As Boolean)

        Dim IDCuenta As Integer = 0
        Dim Orden As Integer = 0

        Dim Tipo As String = "CONTADO"

        If Credito = True Then
            Tipo = "CREDITO"
        End If

        For Each oRow As DataRow In dtCuentaFijaCobranzaCredito.Select("IDTipoComprobante=" & IDTipoComprobante & " And IDMoneda=" & IDMoneda & " And Tipo='" & Tipo & "'")

            IDCuenta = oRow("IDCuentaContable").ToString

            For Each UpdateRow As DataRow In CAsiento.dtDetalleAsiento.Select("IDCuentaContable=" & IDCuenta)

                'Montos
                If oRow("Debe").ToString = True Then
                    UpdateRow("Credito") = 0
                    UpdateRow("Debito") = CDec(UpdateRow("Debito")) - Importe
                End If

                If oRow("Haber").ToString = True Then
                    UpdateRow("Credito") = CDec(UpdateRow("Credito")) - Importe
                    UpdateRow("Debito") = 0
                End If

                UpdateRow("Importe") = CDec(UpdateRow("Importe")) - Importe

                If CDec(UpdateRow("Importe").ToString) = 0 Then
                    CAsiento.dtDetalleAsiento.Rows.Remove(UpdateRow)
                End If

            Next

        Next

    End Sub

End Class

Public Class CAsientoContableOrdenPago
    Inherits CAsiento

    'PROPIEDADES
    Private CAsientoValue As CAsiento
    Public Property CAsiento() As CAsiento
        Get
            Return CAsientoValue
        End Get
        Set(ByVal value As CAsiento)
            CAsientoValue = value
        End Set
    End Property

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    'FUNCIONES
    Sub InicializarAsiento()

        Inicializar()

        'Inicializar Cuentas Fijas Efectivo
        dtCuentaFija = CSistema.ExecuteToDataTable("Select * From VCuentaFijaOrdenPago").Copy


    End Sub

    Sub InsertarCheque(ByVal Importe As Decimal, ByVal CuentaBancaria As DataRow)

        For Each oRow As DataRow In dtCuentaFija.Select("TipoCheque='True'")

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Asignamos la cuenta predefinida de la Cuenta Bancaria, si es que existe.
            If IsNumeric(CuentaBancaria("IDCuentaContable").ToString) = True Then
                NewRow("IDCuentaContable") = CuentaBancaria("IDCuentaContable").ToString
                NewRow("Codigo") = CuentaBancaria("Codigo CC").ToString
                NewRow("Descripcion") = CuentaBancaria("CuentaContable").ToString
                NewRow("Cuenta") = CuentaBancaria("CuentaContable").ToString
                NewRow("Alias") = CuentaBancaria("Alias").ToString
            Else
                NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                NewRow("Codigo") = oRow("Codigo").ToString
                NewRow("Descripcion") = oRow("Cuenta").ToString
                NewRow("Cuenta") = oRow("CuentaContable").ToString
                NewRow("Alias") = oRow("Codigo").ToString
            End If

            'Montos
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Asignamos el importe
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Next

    End Sub

    Sub EliminarCheque()

    End Sub

    Sub InsertarEfectivo(ByVal Importe As Decimal)

        For Each oRow As DataRow In dtCuentaFija.Select("TipoEfectivo='True'")

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Asignamos la cuenta predefinida de la Cuenta Bancaria, si es que existe.
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Cuenta").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString

            'Montos
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Asignamos el importe
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Next

    End Sub

    Sub InsertarEgresos(ByVal Egreso As DataRow)

        For Each oRow As DataRow In dtCuentaFija.Select("TipoProveedor='True'")

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            Dim Importe As Decimal = Egreso("Importe").ToString

            NewRow("IDTransaccion") = 0

            'Asignamos la cuenta predefinida del proveedor, si es que existe.
            If IsNumeric(Egreso("IDCuentaContable").ToString) = True Then
                NewRow("IDCuentaContable") = Egreso("IDCuentaContable").ToString
                NewRow("Codigo") = Egreso("CodigoCuenta").ToString
                NewRow("Descripcion") = Egreso("CuentaContable").ToString
                NewRow("Cuenta") = Egreso("CuentaContable").ToString
                NewRow("Alias") = Egreso("Alias").ToString
            Else
                NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                NewRow("Codigo") = oRow("Codigo").ToString
                NewRow("Descripcion") = oRow("Cuenta").ToString
                NewRow("Cuenta") = oRow("CuentaContable").ToString
                NewRow("Alias") = oRow("Codigo").ToString
            End If

            'Montos
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Asignamos el importe
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Next

    End Sub

End Class

Public Class CAsientoGasto
    Inherits CAsiento

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private CotizacionValue As Decimal
    Public Property Cotizacion() As Decimal
        Get
            Return CotizacionValue
        End Get
        Set(ByVal value As Decimal)
            CotizacionValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private CajaChicaValue As Boolean
    Public Property CajaChica() As Boolean
        Get
            Return CajaChicaValue
        End Get
        Set(ByVal value As Boolean)
            CajaChicaValue = value
        End Set
    End Property
    Private IDGrupoValue As Integer
    Public Property IDGrupo() As Integer
        Get
            Return IDGrupoValue
        End Get
        Set(ByVal value As Integer)
            IDGrupoValue = value
        End Set
    End Property

    Private CCaRecibirValue As String
    Public Property CCaRecibir() As String
        Get
            Return CCaRecibirValue
        End Get
        Set(ByVal value As String)
            CCaRecibirValue = value
        End Set
    End Property

    Private ImporteGastoValue As Decimal
    Public Property ImporteGasto() As Decimal
        Get
            Return ImporteGastoValue
        End Get
        Set(ByVal value As Decimal)
            ImporteGastoValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFGasto").Copy

    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            Cotizacion = oRow("Cotizacion").ToString
            IDSucursal = oRow("IDSucursal").ToString
            CajaChica = oRow("CajaChica").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        'For Each oRow As DataRow In dtCuentaFija.Select("IDSucursal=" & IDSucursal & " And CajaChica='" & CajaChica & "'")

        '    Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

        '    Select Case Tipo


        '    End Select

        'Next

        For Each oRow As DataRow In dtCuentaFija.Select("IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda & " And FondoFijo='" & CajaChica & "'")

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo
                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
            End Select
            If CCaRecibir <> "" Then
                CargarGastoAsociadoAlAcuerdo(oRow)
            End If
        Next


        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)

            'Si la moneda no es GS
            If IDMoneda <> 1 Then
                Importe = Importe * Cotizacion
            End If

            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Verificar la configuracion si pide buscar en Proveedor
            If CBool(oRow("BuscarEnProveedor").ToString) = True Then

                'Obtener los datos del proveedor
                Dim dtTemp As New DataTable
                dtTemp = CData.GetTable("VProveedor").Copy
                dtTemp = CData.FiltrarDataTable(dtTemp, " ID = " & IDProveedor)

                'Validamos si es que se encontro
                If Not dtTemp Is Nothing Then
                    If dtTemp.Rows.Count > 0 Then

                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        'verificamos si existe un codigo valido
                        If IsNumeric(rowTemp("CuentaContableCompra")) = True Then
                            oRow("Codigo") = rowTemp("CuentaContableCompra")
                            oRow("Denominacion") = rowTemp("CuentaCompra")
                            oRow("Cuenta") = rowTemp("CuentaCompra")
                            oRow("IDCuentaContable") = rowTemp("IDCuentaContableCompra")
                        End If

                    End If
                End If
            End If

            'Verificar la configuracion si pide buscar en Grupo
            'Obtener los datos del Grupo
            If CajaChica = True Then
                Dim dtTemp2 As New DataTable
                dtTemp2 = CData.GetTable("vGrupo").Copy
                dtTemp2 = CData.FiltrarDataTable(dtTemp2, " ID = " & IDGrupo)

                'Validamos si es que se encontro
                If Not dtTemp2 Is Nothing Then
                    If dtTemp2.Rows.Count > 0 Then

                        Dim rowTemp As DataRow = dtTemp2.Rows(0)

                        'verificamos si existe un codigo valido
                        If IsNumeric(rowTemp("Codigo")) = True Then
                            oRow("Codigo") = rowTemp("Codigo")
                            oRow("Denominacion") = rowTemp("DescripcionCC")
                            oRow("Cuenta") = rowTemp("DescripcionCC")
                            oRow("IDCuentaContable") = rowTemp("IDCuentaContable")
                        End If

                    End If
                End If
            End If


            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)

            'Si la moneda no es GS
            If IDMoneda <> 1 Then
                Importe = Importe * Cotizacion
            End If

            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarGastoAsociadoAlAcuerdo(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try
            'Verificar la configuracion si pide buscar en Proveedor
            If CCaRecibir <> "" Then

                'Obtener los datos del proveedor
                Dim dtTemp As New DataTable
                dtTemp = CData.GetTable("VCuentaContable").Copy
                dtTemp = CData.FiltrarDataTable(dtTemp, " Codigo = '" & CCaRecibir & "'")

                'Validamos si es que se encontro
                If Not dtTemp Is Nothing Then
                    If dtTemp.Rows.Count > 0 Then

                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        'verificamos si existe un codigo valido
                        If IsNumeric(rowTemp("Codigo")) = True Then
                            oRow("Codigo") = rowTemp("Codigo")
                            oRow("Denominacion") = rowTemp("Descripcion")
                            oRow("Cuenta") = rowTemp("Codigo")
                            oRow("IDCuentaContable") = rowTemp("ID")
                        End If

                    End If
                End If
            End If
            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorGasto(oRow("IDImpuesto").ToString, oRow)

            'Si la moneda no es GS
            If IDMoneda <> 1 Then
                Importe = Importe * Cotizacion
            End If

            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub



    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorGasto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorGasto = 0

        Try
            ObtenerValorGasto = CSistema.dtSumColumn(dtImpuesto, "TotalDiscriminado", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function


    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtImpuesto.Rows
                vTotal = vTotal + CDec(oRow(condicion("Campo").ToString).ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function


End Class

Public Class CAsientoVale
    Inherits CAsiento

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFVale").Copy

    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If
        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Select("IDSucursal=" & IDSucursal)
            CargarCuentaTotal(oRow)
        Next

        Generado = True

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = Total

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

End Class

Public Class CAsientoVenta
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private CreditoValue As Boolean
    Public Property Credito() As Boolean
        Get
            Return CreditoValue
        End Get
        Set(ByVal value As Boolean)
            CreditoValue = value
        End Set
    End Property

    Private ContadoValue As Boolean
    Public Property Contado() As Boolean
        Get
            Return ContadoValue
        End Get
        Set(ByVal value As Boolean)
            ContadoValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFVenta").Copy


    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0


        If Credito <> oRow("Credito") Then
            If Contado <> oRow("Contado") Then
                Exit Sub
            End If
        End If

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        If Credito <> oRow("Credito") Then
            If Contado <> oRow("Contado") Then
                Exit Sub
            End If
        End If

        Try

            'Verificar la configuracion si pide buscar en Cliente/Proveedor
            If CBool(oRow("BuscarEnCliente").ToString) = True Then

                Dim dtTemp As New DataTable

                dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Cliente P Join VCuentaContable CC On P.IDCuentaContable=CC.ID Where P.ID=" & IDCliente)

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        oRow("Codigo") = rowTemp("Codigo")
                        oRow("Descripcion") = rowTemp("CuentaContable")
                        oRow("Cuenta") = rowTemp("Cuenta")
                        oRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                    End If

                End If

            End If

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si hay que buscar en Cliente/Proveedor

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        If Credito <> oRow("Credito") Then
            If Contado <> oRow("Contado") Then
                Exit Sub
            End If
        End If

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaVenta")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaVenta")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        If Credito <> oRow("Credito") Then
            If Contado <> oRow("Contado") Then
                Exit Sub
            End If
        End If

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtImpuesto.Rows
                vTotal = vTotal + CDec(oRow(condicion("Campo").ToString).ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

Public Class CAsientoVenta2
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private CreditoValue As Boolean
    Public Property Credito() As Boolean
        Get
            Return CreditoValue
        End Get
        Set(ByVal value As Boolean)
            CreditoValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFVenta").Copy


    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        If dtCuentaFija Is Nothing Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtCuentaFija.Rows

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
                Case "DESCUENTO"
                    CargarCuentaDescuento(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0


        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Verificar la configuracion si pide buscar en Cliente/Proveedor
            If CBool(oRow("BuscarEnCliente").ToString) = True Then

                Dim dtTemp As New DataTable

                dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Cliente P Join VCuentaContable CC On P.IDCuentaContable=CC.ID Where P.ID=" & IDCliente)

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        oRow("Codigo") = rowTemp("Codigo")
                        oRow("Descripcion") = rowTemp("CuentaContable")
                        oRow("Cuenta") = rowTemp("Cuenta")
                        oRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                    End If

                End If

            End If

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si hay que buscar en Cliente/Proveedor

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaVenta")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaVenta")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtImpuesto.Rows
                vTotal = vTotal + CDec(oRow(condicion("Campo").ToString).ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

Public Class CAsientoNotaDebito
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFNotaDebito").Copy


    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Rows

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
                Case "DESCUENTO"
                    CargarCuentaDescuento(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Verificar la configuracion si pide buscar en Cliente/Proveedor
            If CBool(oRow("BuscarEnCliente").ToString) = True Then

                Dim dtTemp As New DataTable

                dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Proveedor P Join VCuentaContable CC On P.IDCuentaContableCompra=CC.ID Where P.ID=" & IDCliente)

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        oRow("Codigo") = rowTemp("Codigo")
                        oRow("Descripcion") = rowTemp("Denominacion")
                        oRow("Cuenta") = rowTemp("Cuenta")
                        oRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                    End If

                End If

            End If

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si hay que buscar en Cliente/Proveedor

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaVenta")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaVenta")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtImpuesto.Rows
                vTotal = vTotal + CDec(oRow(condicion("Campo").ToString).ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

Public Class CAsientoNotaCredito
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDClienteValue As Integer
    Public Property IDCliente() As Integer
        Get
            Return IDClienteValue
        End Get
        Set(ByVal value As Integer)
            IDClienteValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFNotaCredito").Copy


    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Rows

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
                Case "DESCUENTO"
                    CargarCuentaDescuento(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Verificar la configuracion si pide buscar en Cliente/Proveedor
            If CBool(oRow("BuscarEnCliente").ToString) = True Then

                Dim dtTemp As New DataTable

                dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Proveedor P Join VCuentaContable CC On P.IDCuentaContableCompra=CC.ID Where P.ID=" & IDCliente)

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        oRow("Codigo") = rowTemp("Codigo")
                        oRow("Descripcion") = rowTemp("Denominacion")
                        oRow("Cuenta") = rowTemp("Cuenta")
                        oRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                    End If

                End If

            End If

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si hay que buscar en Cliente/Proveedor

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaVenta")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaVenta")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtImpuesto.Rows
                vTotal = vTotal + CDec(oRow(condicion("Campo").ToString).ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

Public Class CAsientoCompra
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private CotizacionValue As Integer
    Public Property Cotizacion() As Integer
        Get
            Return CotizacionValue
        End Get
        Set(ByVal value As Integer)
            CotizacionValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFCompra").Copy


    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Select(" IDSucursal= " & IDSucursal & " And IDMoneda=" & IDMoneda & " ")

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow) * Cotizacion
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Verificar la configuracion si pide buscar en Cliente/Proveedor
            If CBool(oRow("BuscarEnProveedor").ToString) = True Then

                Dim dtTemp As New DataTable

                dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'Denominacion'=CC.Descripcion, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Proveedor P Join VCuentaContable CC On P.CuentaContableCompra=CC.Codigo Where P.ID=" & IDProveedor)

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        oRow("Codigo") = rowTemp("Codigo")
                        oRow("Denominacion") = rowTemp("Denominacion")
                        oRow("Cuenta") = rowTemp("Cuenta")
                        oRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                    End If

                End If

            End If

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si hay que buscar en Cliente/Proveedor

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True) * Cotizacion
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaCompra")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaCompra")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString) * Cotizacion
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtImpuesto.Rows
                vTotal = vTotal + CDec(oRow(condicion("Campo").ToString).ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

Public Class CAsientoNotaCreditoProveedor
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFNotaCreditoProveedor").Copy


    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Rows

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
                Case "DESCUENTO"
                    CargarCuentaDescuento(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Verificar la configuracion si pide buscar en Cliente/Proveedor
            If CBool(oRow("BuscarEnProveedor").ToString) = True Then

                Dim dtTemp As New DataTable

                dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'Denominacion'=CC.Descripcion, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Proveedor P Join VCuentaContable CC On P.IDCuentaContableCompra=CC.ID Where P.ID=" & IDProveedor)

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        oRow("Codigo") = rowTemp("Codigo")
                        oRow("Denominacion") = rowTemp("Denominacion")
                        oRow("Cuenta") = rowTemp("Cuenta")
                        oRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                    End If

                End If

            End If

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si hay que buscar en Cliente/Proveedor

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaVenta")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaVenta")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtImpuesto.Rows
                vTotal = vTotal + CDec(oRow(condicion("Campo").ToString).ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

Public Class CAsientoNotaDebitoProveedor
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFNotaDebitoProveedor").Copy


    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Rows

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
                Case "DESCUENTO"
                    CargarCuentaDescuento(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Verificar la configuracion si pide buscar en Cliente/Proveedor
            If CBool(oRow("BuscarEnProveedor").ToString) = True Then

                Dim dtTemp As New DataTable

                dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'Denominacion'=CC.Descripcion, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Proveedor P Join VCuentaContable CC On P.IDCuentaContableCompra=CC.ID Where P.ID=" & IDProveedor)

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        oRow("Codigo") = rowTemp("Codigo")
                        oRow("Denominacion") = rowTemp("Denominacion")
                        oRow("Cuenta") = rowTemp("Cuenta")
                        oRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                    End If

                End If

            End If

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            'Si hay que buscar en Cliente/Proveedor

            'Si no se encontro nada, se usa el predeterminado
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaVenta")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaVenta")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtImpuesto.Rows
                vTotal = vTotal + CDec(oRow(condicion("Campo").ToString).ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

Public Class CAsientoMovimiento
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDTipoOperacionValue As Integer
    Public Property IDTipoOperacion() As Integer
        Get
            Return IDTipoOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTipoOperacionValue = value
        End Set
    End Property
    Private IDOperacionvalue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionvalue
        End Get
        Set(ByVal value As Integer)
            IDOperacionvalue = value
        End Set
    End Property

    Private IDTipoComprobanteValue As Integer
    Public Property IDTipoComprobante() As Integer
        Get
            Return IDTipoComprobanteValue
        End Get
        Set(ByVal value As Integer)
            IDTipoComprobanteValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        'dtCuentaFija = CData.GetTable("VCFMovimiento").Copy
        dtCuentaFija = CData.GetTable("VCFMovimiento", " IDOperacion = " & IDOperacion)


    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Select(" IDTipoComprobante =" & IDTipoComprobante & " and IDOperacion = " & IDOperacion & " And IDTipoOperacion=" & IDTipoOperacion & " And IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda & " ")

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
                Case "DESCUENTO"
                    CargarCuentaDescuento(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaVenta")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaVenta")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Rows
                vTotal = vTotal + CDec(oRow("Total").ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

Public Class CAsientoMacheoFacturaTicket
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDTipoOperacionValue As Integer
    Public Property IDTipoOperacion() As Integer
        Get
            Return IDTipoOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTipoOperacionValue = value
        End Set
    End Property
    Private IDOperacionvalue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionvalue
        End Get
        Set(ByVal value As Integer)
            IDOperacionvalue = value
        End Set
    End Property

    Private IDTipoComprobanteValue As Integer
    Public Property IDTipoComprobante() As Integer
        Get
            Return IDTipoComprobanteValue
        End Get
        Set(ByVal value As Integer)
            IDTipoComprobanteValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        'dtCuentaFija = CData.GetTable("VCFMovimiento").Copy
        dtCuentaFija = CData.GetTable("VCFMacheoFacturaTicket", " IDOperacion = " & IDOperacion)


    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Select(" IDTipoComprobante =" & IDTipoComprobante & " and IDOperacion = " & IDOperacion & " And IDTipoOperacion=" & IDTipoOperacion & " And IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda & " ")

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
                Case "DESCUENTO"
                    CargarCuentaDescuento(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If CBool(oRow("BuscarEnProducto").ToString) = True Then


                    Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                    Dim ProductoRow As DataRow = dtProducto.Rows(0)

                    If IsNumeric(ProductoRow("CodigoCuentaVenta")) Then
                        dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaVenta")).Copy
                    End If


                End If

                If Not dtTemp Is Nothing Then

                    If dtTemp.Rows.Count > 0 Then
                        Dim rowTemp As DataRow = dtTemp.Rows(0)

                        NewRow("Codigo") = rowTemp("Codigo")
                        NewRow("Cuenta") = rowTemp("Cuenta")
                        NewRow("Descripcion") = rowTemp("Descripcion")
                        NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                        Establecido = True

                    End If
                End If


                NewRow("Orden") = oRow("Orden")

                If Establecido = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If


                'Montos
                Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Rows
                vTotal = vTotal + CDec(oRow("Total").ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class

'dbs
Public Class CAsientoDescarga
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtImpuestoValue As DataTable
    Public Property dtImpuesto() As DataTable
        Get
            Return dtImpuestoValue
        End Get
        Set(ByVal value As DataTable)
            dtImpuestoValue = value
        End Set
    End Property

    Private dtDetalleProductosValue As DataTable
    Public Property dtDetalleProductos() As DataTable
        Get
            Return dtDetalleProductosValue
        End Get
        Set(ByVal value As DataTable)
            dtDetalleProductosValue = value
        End Set
    End Property

    Private dtDescuentoValue As DataTable
    Public Property dtDescuento() As DataTable
        Get
            Return dtDescuentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDescuentoValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDTipoOperacionValue As Integer
    Public Property IDTipoOperacion() As Integer
        Get
            Return IDTipoOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDTipoOperacionValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFMovimiento").Copy


    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Select(" IDTipoOperacion=" & IDTipoOperacion & " And IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda & " ")

            Dim Tipo As String = oRow("TIPOCUENTAFIJA").ToString.ToUpper

            Select Case Tipo

                Case "IMPUESTO"
                    CargarCuentaImpuesto(oRow)
                Case "TOTAL"
                    CargarCuentaTotal(oRow)
                Case "PRODUCTO"
                    CargarCuentaProducto(oRow)
                    CargarCuentaProductoCompra(oRow)
                Case "DESCUENTO"
                    CargarCuentaDescuento(oRow)
            End Select

        Next

        Generado = True

    End Sub

    Sub CargarCuentaImpuesto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorImpuesto(oRow("IDImpuesto").ToString, oRow)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            Else
                dtDetalleAsiento.Rows.Remove(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub CargarCuentaTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0

            NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
            NewRow("Codigo") = oRow("Codigo").ToString
            NewRow("Descripcion") = oRow("Denominacion").ToString
            NewRow("Cuenta") = oRow("Cuenta").ToString
            NewRow("Alias") = oRow("Codigo").ToString
            NewRow("Orden") = oRow("Orden").ToString

            'Montos
            Importe = ObtenerValorTotal(oRow, True)
            If oRow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If oRow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaProducto(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If oRow("Orden").ToString = "1" Then
                    If CBool(oRow("BuscarEnProducto").ToString) = True Then


                        Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                        Dim ProductoRow As DataRow = dtProducto.Rows(0)

                        If IsNumeric(ProductoRow("CodigoCuentaCompra")) Then
                            dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaCompra")).Copy
                        End If


                    End If

                    If Not dtTemp Is Nothing Then

                        If dtTemp.Rows.Count > 0 Then
                            Dim rowTemp As DataRow = dtTemp.Rows(0)

                            NewRow("Codigo") = rowTemp("Codigo")
                            NewRow("Cuenta") = rowTemp("Cuenta")
                            NewRow("Descripcion") = rowTemp("Descripcion")
                            NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                            Establecido = True

                        End If
                    End If


                    NewRow("Orden") = oRow("Orden")

                    If Establecido = False Then
                        NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                        NewRow("Codigo") = oRow("Codigo").ToString
                        NewRow("Descripcion") = oRow("Denominacion").ToString
                        NewRow("Cuenta") = oRow("Cuenta").ToString
                        NewRow("Alias") = oRow("Codigo").ToString
                    End If


                    'Montos
                    Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                    If oRow("Debe").ToString = True Then
                        NewRow("Credito") = 0
                        NewRow("Debito") = Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        NewRow("Credito") = Importe
                        NewRow("Debito") = 0
                    End If

                    'Otros
                    NewRow("ID") = dtDetalleAsiento.Rows.Count
                    NewRow("Observacion") = ""

                    NewRow("Fijo") = True

                    'Solo asignamos si el importe es mayor a 0
                    If Importe > 0 Then
                        NewRow("Importe") = Importe
                        dtDetalleAsiento.Rows.Add(NewRow)
                    End If
                End If
            Next

        Catch ex As Exception

        End Try

    End Sub
    Private Sub CargarCuentaProductoCompra(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDetalleProductos.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim Establecido As Boolean = False
                Dim dtTemp As New DataTable

                NewRow("IDTransaccion") = 0

                'Verificar la configuracion si pide buscar en el producto
                If oRow("Orden").ToString = "2" Then
                    If CBool(oRow("BuscarEnProducto").ToString) = True Then


                        Dim dtProducto As DataTable = CData.GetTable("VProducto", " ID = " & DetalleRow("IDProducto")).Copy
                        Dim ProductoRow As DataRow = dtProducto.Rows(0)

                        If IsNumeric(ProductoRow("CodigoCuentaCompra")) Then
                            dtTemp = CData.GetTable("VCuentaContable", " Codigo=" & ProductoRow("CodigoCuentaCompra")).Copy
                        End If


                    End If

                    If Not dtTemp Is Nothing Then

                        If dtTemp.Rows.Count > 0 Then
                            Dim rowTemp As DataRow = dtTemp.Rows(0)

                            NewRow("Codigo") = rowTemp("Codigo")
                            NewRow("Cuenta") = rowTemp("Cuenta")
                            NewRow("Descripcion") = rowTemp("Descripcion")
                            NewRow("IDCuentaContable") = rowTemp("IDCuentaContable")

                            Establecido = True

                        End If
                    End If


                    NewRow("Orden") = oRow("Orden")

                    If Establecido = False Then
                        NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                        NewRow("Codigo") = oRow("Codigo").ToString
                        NewRow("Descripcion") = oRow("Denominacion").ToString
                        NewRow("Cuenta") = oRow("Cuenta").ToString
                        NewRow("Alias") = oRow("Codigo").ToString
                    End If


                    'Montos
                    Importe = ObtenerValorProducto2(DetalleRow("IDProducto").ToString, DetalleRow("ID").ToString, oRow("Campo").ToString)
                    If oRow("Debe").ToString = True Then
                        NewRow("Credito") = 0
                        NewRow("Debito") = Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        NewRow("Credito") = Importe
                        NewRow("Debito") = 0
                    End If

                    'Otros
                    NewRow("ID") = dtDetalleAsiento.Rows.Count
                    NewRow("Observacion") = ""

                    NewRow("Fijo") = True

                    'Solo asignamos si el importe es mayor a 0
                    If Importe > 0 Then
                        NewRow("Importe") = Importe
                        dtDetalleAsiento.Rows.Add(NewRow)
                    End If
                End If
            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CargarCuentaDescuento(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0

        Try

            'Recorrer el detalle
            For Each DetalleRow As DataRow In dtDescuento.Rows

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow
                Dim dtTemp As New DataTable
                Dim Establecido As Boolean = False

                'Buscar cuenta en Descuento
                dtTemp = CData.GetTable("VTipoDescuento").Copy

                For Each DescuentoRow As DataRow In dtTemp.Rows

                    If DescuentoRow("Descripcion").ToString.ToUpper = DetalleRow("Tipo").ToString.ToUpper Then

                        If IsNumeric(DescuentoRow("CodigoCuentaVenta")) = False Then
                            Establecido = False
                            Exit For
                        End If

                        NewRow("Codigo") = DescuentoRow("CodigoCuentaVenta")
                        NewRow("Cuenta") = DescuentoRow("CuentaVenta")
                        NewRow("Descripcion") = DescuentoRow("DenominacionCuentaVenta")
                        NewRow("IDCuentaContable") = DescuentoRow("IDCuentaContableVenta")
                        NewRow("Orden") = oRow("Orden").ToString

                        Establecido = True
                        Exit For

                    End If

                Next

                If Establecido = False Then
                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    NewRow("Orden") = oRow("Orden").ToString
                End If

                'Montos
                Importe = DetalleRow(oRow("Campo").ToString).ToString

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""

                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function ObtenerValorImpuesto(ByVal IDImpuesto As String, ByVal condicion As DataRow) As Decimal

        ObtenerValorImpuesto = 0

        Try
            ObtenerValorImpuesto = CSistema.dtSumColumn(dtImpuesto, "TotalImpuesto", IDImpuesto, "IDImpuesto")

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorTotal(ByVal condicion As DataRow, Optional ByVal Detalle As Boolean = False) As Decimal

        ObtenerValorTotal = 0
        Dim vTotal As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Rows
                vTotal = vTotal + CDec(oRow("Total").ToString)
            Next

        Catch ex As Exception

        End Try

        ObtenerValorTotal = vTotal

    End Function

    Private Function ObtenerValorProducto(ByVal condicion As DataRow, ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto = 0
        Dim SubTotal As Decimal = 0
        Dim SubTotalImpuesto As Decimal = 0
        Dim SubTotalDescuento As Decimal = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                SubTotal = SubTotal + CDec(oRow("Total").ToString)
                SubTotalImpuesto = SubTotalImpuesto + CDec(oRow("TotalImpuesto").ToString)
                SubTotalDescuento = SubTotalDescuento + CDec(oRow("TotalDescuento").ToString)
            Next

            If CBool(condicion("IncluirImpuesto").ToString) = False Then
                SubTotal = SubTotal - SubTotalImpuesto
            End If

            If CBool(condicion("IncluirDescuento").ToString) = False Then
                SubTotal = SubTotal - SubTotalDescuento
            End If

            Return SubTotal

        Catch ex As Exception

        End Try

    End Function

    Private Function ObtenerValorProducto2(ByVal IDProducto As Integer, ByVal ID As Integer, ByVal Campo As String) As Decimal

        ObtenerValorProducto2 = 0

        Try
            For Each oRow As DataRow In dtDetalleProductos.Select(" IDProducto=" & IDProducto & " And ID=" & ID)
                ObtenerValorProducto2 = oRow(Campo)
            Next

        Catch ex As Exception

        End Try

    End Function

End Class
Public Class CAsientoAjuste
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private TotalSobranteValue As Decimal
    Public Property TotalSobrante() As Decimal
        Get
            Return TotalSobranteValue
        End Get
        Set(ByVal value As Decimal)
            TotalSobranteValue = value
        End Set
    End Property

    Private TotalFaltanteValue As Decimal
    Public Property TotalFaltante() As Decimal
        Get
            Return TotalFaltanteValue
        End Get
        Set(ByVal value As Decimal)
            TotalFaltanteValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFAjuste").Copy


    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        For Each oRow As DataRow In dtCuentaFija.Rows

            Dim Tipo As String = oRow("Tipo")

            Try

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0
                NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                NewRow("Codigo") = oRow("Codigo").ToString
                NewRow("Descripcion") = oRow("Denominacion").ToString
                NewRow("Cuenta") = oRow("Cuenta").ToString
                NewRow("Alias") = oRow("Codigo").ToString
                NewRow("Orden") = oRow("Orden").ToString

                'Montos
                Importe = 0
                If Tipo.ToUpper = "FALTANTE" Then
                    Importe = TotalFaltante
                End If

                If Tipo.ToUpper = "SOBRANTE" Then
                    Importe = TotalSobrante
                End If

                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                Else
                    dtDetalleAsiento.Rows.Remove(NewRow)
                End If

            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

End Class

Public Class CAsientoChequeRechazados
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFChequeRechazado").Copy

    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            IDSucursal = oRow("IDSucursal".ToString)
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        'Cargar el fijo
        For Each oRow As DataRow In dtCuentaFija.Select(" IDSucursal = " & IDSucursal)

            Try

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0

                If oRow("BuscarEncuentaBancaria") = True Then
                    CargarCuentaBancaria(NewRow)
                Else
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If

                NewRow("Orden") = oRow("Orden").ToString

                'Montos
                Importe = Total
                If oRow("Debe").ToString = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber").ToString = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If Importe > 0 Then
                    NewRow("Importe") = Importe
                    dtDetalleAsiento.Rows.Add(NewRow)
                Else
                    dtDetalleAsiento.Rows.Remove(NewRow)
                End If

            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

    Sub CargarCuentaBancaria(ByRef oRow As DataRow)

        Dim CuentaBancariaRow As DataRow = CData.GetRow("ID=" & IDCuentaBancaria, "VCuentaBancaria")
        Dim CuentaContableRow As DataRow = CData.GetRow("Codigo='" & CuentaBancariaRow("Codigo CC") & "'", "VCuentaContable")

        oRow("IDCuentaContable") = CuentaContableRow("IDCuentaContable").ToString
        oRow("Codigo") = CuentaContableRow("Codigo").ToString
        oRow("Descripcion") = CuentaContableRow("Descripcion").ToString
        oRow("Cuenta") = CuentaContableRow("Cuenta").ToString
        oRow("Alias") = CuentaContableRow("Codigo").ToString

    End Sub

End Class

Public Class CAsientoDepositoBancario
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private dtEfectivoValue As DataTable
    Public Property dtEfectivo() As DataTable
        Get
            Return dtEfectivoValue
        End Get
        Set(ByVal value As DataTable)
            dtEfectivoValue = value
        End Set
    End Property

    Private dtChequeValue As DataTable
    Public Property dtCheque() As DataTable
        Get
            Return dtChequeValue
        End Get
        Set(ByVal value As DataTable)
            dtChequeValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private CotizacionValue As Integer
    Public Property Cotizacion() As Integer
        Get
            Return CotizacionValue
        End Get
        Set(ByVal value As Integer)
            CotizacionValue = value
        End Set
    End Property

    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Private TotalRestarValue As Decimal
    Public Property TotalRestar() As Decimal
        Get
            Return TotalRestarValue
        End Get
        Set(ByVal value As Decimal)
            TotalRestarValue = value
        End Set
    End Property

    Private TotalDiferenciaCambioValue As Decimal
    Public Property TotalDiferenciaCambio() As Decimal
        Get
            Return TotalDiferenciaCambioValue
        End Get
        Set(ByVal value As Decimal)
            TotalDiferenciaCambioValue = value
        End Set
    End Property

    Private TotalDocumentosValue As Decimal
    Public Property TotalDocumentos() As Decimal
        Get
            Return TotalDocumentosValue
        End Get
        Set(ByVal value As Decimal)
            TotalDocumentosValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFDepositoBancario").Copy

    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0
        Dim TipoCuenta As Boolean

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        'Cargar el fijo
        TipoCuenta = CSistema.ExecuteScalar("Select Case When IDTipoCuentaBancaria=1 Then 'False' Else 'True' End From VCuentaBancaria Where ID=" & IDCuentaBancaria)
        Dim dttemp As DataTable = CData.FiltrarDataTable(dtCuentaFija.Copy, " IDSucursal = " & IDSucursal & " And IDMoneda=" & IDMoneda & " And TipoCuenta=" & TipoCuenta)
        For Each oRow As DataRow In dttemp.Rows

            Try

                If oRow("DEBE") = True Then
                    CargarTotal(oRow)
                End If

                If oRow("HABER") = True Then

                    If oRow("Efectivo") = True Then
                        CargarEfectivo(oRow)
                    End If

                    If oRow("ChequeAlDia") = True Then
                        CargarChequeAlDia(oRow)
                    End If

                    If oRow("ChequeDiferido") = True Then
                        CargarChequeDiferido(oRow)
                    End If

                    If oRow("ChequeRechazado") = True Then
                        CargarChequeRechazado(oRow)
                    End If

                    If oRow("DiferenciaCambio") = True Then
                        CargarDiferenciaCambio(oRow)
                    End If

                    If oRow("Documento") = True Then
                        CargarDocumentos(oRow)
                    End If


                End If

            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

    Sub CargarTotal(ByVal oRow As DataRow)

        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
        NewRow("Codigo") = oRow("Codigo").ToString
        NewRow("Descripcion") = oRow("Denominacion").ToString
        NewRow("Cuenta") = oRow("Cuenta").ToString
        NewRow("Alias") = oRow("Codigo").ToString

        If oRow("BuscarEncuentaBancaria") = True Then
            CargarCuentaBancaria(NewRow)
        End If

        NewRow("Orden") = oRow("Orden").ToString

        'Montos
        Importe = Total

        'Verificar la moneda
        If IDMoneda <> 1 Then
            Importe = Importe * Cotizacion
        End If

        If oRow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If oRow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        Else
            dtDetalleAsiento.Rows.Remove(NewRow)
        End If

    End Sub

    Sub CargarChequeAlDia(ByVal orow As DataRow)

        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Codigo").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Montos
        Dim dttemp As DataTable = CData.FiltrarDataTable(dtCheque, "Diferido='False' And Sel='True' And Rechazado='False'").Copy

        Importe = CSistema.dtSumColumn(dttemp, "Importe")

        'Verificar la moneda
        If IDMoneda <> 1 Then
            Importe = Importe * CSistema.dtSumColumn(dttemp, "Cotizacion")
        End If

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarChequeDiferido(ByVal orow As DataRow)

        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Codigo").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Montos
        Dim dttemp As DataTable = CData.FiltrarDataTable(dtCheque, "Diferido='True' And Sel='True' And Rechazado='False'").Copy

        Importe = CSistema.dtSumColumn(dttemp, "Importe")

        'Verificar la moneda
        If IDMoneda <> 1 Then
            Importe = Importe * CSistema.dtSumColumn(dttemp, "Cotizacion")
        End If

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarChequeRechazado(ByVal orow As DataRow)

        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Codigo").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Montos
        Dim dttemp As DataTable = CData.FiltrarDataTable(dtCheque, "Sel='True' And Rechazado='True'").Copy

        Importe = CSistema.dtSumColumn(dttemp, "Importe")

        'Verificar la moneda
        If IDMoneda <> 1 Then
            Importe = Importe * orow("Cotizacion").ToString
        End If

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If
    End Sub

    Sub CargarEfectivo(ByVal orow As DataRow)

        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Codigo").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Montos
        Dim dttemp As DataTable = CData.FiltrarDataTable(dtEfectivo, "Sel='True'").Copy

        'Verificar la moneda
        If IDMoneda <> 1 Then
            For Each ImporteRow As DataRow In dttemp.Rows
                Importe = Importe + (ImporteRow("ImporteMoneda") * ImporteRow("Cotizacion").ToString)
            Next
        Else
            Importe = CSistema.dtSumColumn(dttemp, "ImporteMoneda")
        End If

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe - TotalRestar
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarCuentaBancaria(ByRef oRow As DataRow)

        Dim CuentaBancariaRow As DataRow = CData.GetRow("ID=" & IDCuentaBancaria, "VCuentaBancaria")
        Dim CuentaContableRow As DataRow = CData.GetRow("Codigo='" & CuentaBancariaRow("Codigo CC") & "'", "VCuentaContable")

        oRow("IDCuentaContable") = CuentaContableRow("IDCuentaContable").ToString
        oRow("Codigo") = CuentaContableRow("Codigo").ToString
        oRow("Descripcion") = CuentaContableRow("Descripcion").ToString
        oRow("Cuenta") = CuentaContableRow("Cuenta").ToString
        oRow("Alias") = CuentaContableRow("Alias").ToString

    End Sub

    Sub CargarDiferenciaCambio(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalDiferenciaCambio

        If Importe = 0 Then
            Exit Sub
        End If

        If Importe < 0 Then
            Importe = Importe * -1
        End If

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If TotalDiferenciaCambio > 0 Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If TotalDiferenciaCambio < 0 Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub
    Sub CargarDocumentos(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalDocumentos

        If Importe = 0 Then
            Exit Sub
        End If

        If Importe < 0 Then
            Importe = Importe * -1
        End If

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Verificar la moneda
        If IDMoneda <> 1 Then
            Importe = Importe * Cotizacion
        End If

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If


        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

End Class

Public Class CAsientoDebitoCreditoBancario
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Private CreditoValue As Boolean
    Public Property Credito() As Boolean
        Get
            Return CreditoValue
        End Get
        Set(ByVal value As Boolean)
            CreditoValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFDebitoCreditoBancario").Copy

    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each MonedaRow As DataRow In dtAsiento.Rows
            IDMoneda = MonedaRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        Dim Filtro As String = " IDSucursal = " & IDSucursal & " And IDMoneda = " & IDMoneda
        If Credito = True Then

        End If

        For Each oRow As DataRow In dtCuentaFija.Select(Filtro)

            Try

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0

                If oRow("BuscarEncuentaBancaria") = True Then
                    CargarCuentaBancaria(NewRow)
                Else
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                End If

                NewRow("Orden") = oRow("Orden").ToString

                'Montos
                Importe = Total

                'Verificar la moneda
                If IDMoneda <> 1 Then
                    Importe = Importe * dtAsiento.Rows(0)("Cotizacion")

                End If

                If Credito = False And oRow("Debe") = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If Credito = True And oRow("Debe") = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("Importe") = Importe
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If IsNumeric(NewRow("Credito")) = True And IsNumeric(NewRow("Debito")) = True Then
                    If Importe > 0 Then
                        dtDetalleAsiento.Rows.Add(NewRow)
                    End If
                End If


            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

    Sub CargarCuentaBancaria(ByRef oRow As DataRow)

        Dim CuentaBancariaRow As DataRow = CData.GetRow("ID=" & IDCuentaBancaria, "VCuentaBancaria")
        Dim CuentaContableRow As DataRow = CData.GetRow("Codigo='" & CuentaBancariaRow("Codigo CC") & "'", "VCuentaContable")

        oRow("IDCuentaContable") = CuentaContableRow("IDCuentaContable").ToString
        oRow("Codigo") = CuentaContableRow("Codigo").ToString
        oRow("Descripcion") = CuentaContableRow("Descripcion").ToString
        oRow("Cuenta") = CuentaContableRow("Cuenta").ToString
        oRow("Alias") = CuentaContableRow("Alias").ToString

    End Sub

End Class

Public Class CAsientoTicketBascula
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDTipoProductoValue As Integer
    Public Property IDTipoProducto() As Integer
        Get
            Return IDTipoProductoValue
        End Get
        Set(ByVal value As Integer)
            IDTipoProductoValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFTicketBascula").Copy

    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each MonedaRow As DataRow In dtAsiento.Rows
            IDMoneda = MonedaRow("IDMoneda").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        Dim Filtro As String = " IDSucursal = " & IDSucursal & " And IDMoneda = " & IDMoneda & " and IDTipoProducto = " & IDTipoProducto

        For Each oRow As DataRow In dtCuentaFija.Select(Filtro)

            Try

                Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                NewRow("IDTransaccion") = 0

                If oRow("Flete") = True Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    'Montos
                    Importe = TotalGastoAdicional
                ElseIf oRow("BuscarEnTipoProducto") = True And oRow("Flete") = False Then
                    CargarTipoProducto(NewRow)
                    'Montos
                    Importe = Total
                ElseIf oRow("BuscarEnTipoProducto") = False And oRow("Flete") = False Then
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Codigo").ToString
                    'Montos
                    Importe = Total
                End If

                NewRow("Orden") = oRow("Orden").ToString
                'Verificar la moneda
                If IDMoneda <> 1 Then
                    Importe = Importe * dtAsiento.Rows(0)("Cotizacion")
                End If

                If oRow("Debe") = True Then
                    NewRow("Credito") = 0
                    NewRow("Debito") = Importe
                End If

                If oRow("Haber") = True Then
                    NewRow("Credito") = Importe
                    NewRow("Debito") = 0
                End If

                'Otros
                NewRow("Importe") = Importe
                NewRow("ID") = dtDetalleAsiento.Rows.Count
                NewRow("Observacion") = ""
                NewRow("Fijo") = True

                'Solo asignamos si el importe es mayor a 0
                If IsNumeric(NewRow("Credito")) = True And IsNumeric(NewRow("Debito")) = True Then
                    If Importe > 0 Then
                        dtDetalleAsiento.Rows.Add(NewRow)
                    End If
                End If


            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

    Sub CargarTipoProducto(ByRef oRow As DataRow)

        Dim TipoProductoRow As DataRow = CData.GetRow("ID=" & IDTipoProducto, "VTipoProducto")
        Dim CuentaContableRow As DataRow = CData.GetRow("Codigo='" & TipoProductoRow("CuentaContableCosto") & "'", "VCuentaContable")

        oRow("IDCuentaContable") = CuentaContableRow("IDCuentaContable").ToString
        oRow("Codigo") = CuentaContableRow("Codigo").ToString
        oRow("Descripcion") = CuentaContableRow("Descripcion").ToString
        oRow("Cuenta") = CuentaContableRow("Cuenta").ToString
        oRow("Alias") = CuentaContableRow("Alias").ToString

    End Sub

End Class

Public Class CAsientoDescuentoCheque
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Private GastoBancarioValue As Decimal
    Public Property GastoBancario() As Decimal
        Get
            Return GastoBancarioValue
        End Get
        Set(ByVal value As Decimal)
            GastoBancarioValue = value
        End Set
    End Property

    Private TotalAcreditadoValue As Decimal
    Public Property TotalAcreditado() As Decimal
        Get
            Return TotalAcreditadoValue
        End Get
        Set(ByVal value As Decimal)
            TotalAcreditadoValue = value
        End Set
    End Property

    Private dtChequeValue As DataTable
    Public Property dtCheque() As DataTable
        Get
            Return dtChequeValue
        End Get
        Set(ByVal value As DataTable)
            dtChequeValue = value
        End Set
    End Property

    Private CambioChequeValue As Decimal
    Public Property CambioCheque() As Decimal
        Get
            Return CambioChequeValue
        End Get
        Set(ByVal value As Decimal)
            CambioChequeValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFDescuentoCheque").Copy

    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            IDSucursal = oRow("IDSucursal").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        'Cargar el fijo
        For Each oRow As DataRow In dtCuentaFija.Select("IDSucursal=" & IDSucursal)

            Try

                Dim Tipo As String = oRow("Tipo").ToString

                Select Case Tipo

                    Case "CUENTA BANCARIA"
                        CargarCuentaBancaria(oRow)
                    Case "GASTO BANCARIO"
                        CargarGastoBancario(oRow)
                    Case "CHEQUE"
                        CargarCheque(oRow, oRow("IDSucursalCheque").ToString)
                End Select

            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

    Sub CargarCuentaBancaria(ByVal orow As DataRow)

        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        CargarCuentaCuentaBancaria(orow)

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Montos
        Importe = TotalAcreditado

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarGastoBancario(ByVal orow As DataRow)

        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Codigo").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Montos
        Importe = GastoBancario

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarCuentaCuentaBancaria(ByRef oRow As DataRow)

        Dim CuentaBancariaRow As DataRow = CData.GetRow("ID=" & IDCuentaBancaria, "VCuentaBancaria")

        If CuentaBancariaRow Is Nothing Then
            Exit Sub
        End If

        Dim CuentaContableRow As DataRow = CData.GetRow("Codigo='" & CuentaBancariaRow("CodigoCuentaContable") & "'", "VCuentaContable")

        If CuentaContableRow Is Nothing Then
            Exit Sub
        End If

        oRow("IDCuentaContable") = CuentaContableRow("IDCuentaContable").ToString
        oRow("Codigo") = CuentaContableRow("Codigo").ToString
        oRow("Descripcion") = CuentaContableRow("Descripcion").ToString
        oRow("Cuenta") = CuentaContableRow("Cuenta").ToString
        oRow("Alias") = CuentaContableRow("Alias").ToString

    End Sub

    Sub CargarCheque(ByVal orow As DataRow, ByVal IDSucursalCheque As Integer)

        For Each chequeRow As DataRow In dtCheque.Select("Sel='True' and IDSucursal = " & IDSucursalCheque)

            Dim Importe As Decimal = 0
            Dim NewRow As DataRow = dtDetalleAsiento.NewRow

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
            NewRow("Codigo") = orow("Codigo").ToString
            NewRow("Descripcion") = orow("Denominacion").ToString
            NewRow("Cuenta") = orow("Cuenta").ToString
            NewRow("Alias") = orow("Alias").ToString
            NewRow("Orden") = orow("Orden").ToString

            'Montos
            If IDMoneda = 1 Then
                Importe = chequeRow("Importe").ToString
            Else
                Importe = chequeRow("Importe").ToString * CambioCheque
            End If
            If orow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If orow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If
        Next

    End Sub

End Class

Public Class CAsientoVencimientoChequeDescontado
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDSucursalChequeValue As Integer
    Public Property IDSucursalCheque() As Integer
        Get
            Return IDSucursalChequeValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalChequeValue = value
        End Set
    End Property

    Private CotizacionValue As Integer
    Public Property Cotizacion() As Integer
        Get
            Return CotizacionValue
        End Get
        Set(ByVal value As Integer)
            CotizacionValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFVencimientoChequeDescontado").Copy

    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            IDSucursal = oRow("IDSucursal").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        'Cargar el fijo
        For Each oRow As DataRow In dtCuentaFija.Select("IDSucursal=" & IDSucursal)
            Try
                Dim Tipo As String = oRow("Tipo").ToString
                Select Case Tipo
                    Case "DIFERIDO"
                        If oRow("IDSucursalChequeDiferido").ToString = IDSucursalCheque Then
                            CargarDiferido(oRow)
                        End If
                    Case "DESCONTADO"
                        If oRow("IDSucursalChequeDescontado").ToString = IDSucursalCheque Then
                            CargarDescontado(oRow)
                        End If
                End Select

            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

    Sub CargarDescontado(ByVal orow As DataRow)
        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Montos
        Importe = Total

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarDiferido(ByVal orow As DataRow)
        Dim Importe As Decimal = 0
        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        'Montos
        Importe = Total

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If
    End Sub

End Class

Public Class CAsientoOrdenPago
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Private CodigoCuentaBancariaValue As Integer
    Public Property CodigoCuentaBancaria() As Integer
        Get
            Return CodigoCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            CodigoCuentaBancariaValue = value
        End Set
    End Property

    Private TotalEfectivoValue As Decimal
    Public Property TotalEfectivo() As Decimal
        Get
            Return TotalEfectivoValue
        End Get
        Set(ByVal value As Decimal)
            TotalEfectivoValue = value
        End Set
    End Property

    Private TotalChequeValue As Decimal
    Public Property TotalCheque() As Decimal
        Get
            Return TotalChequeValue
        End Get
        Set(ByVal value As Decimal)
            TotalChequeValue = value
        End Set
    End Property

    Private TotalDocumentoValue As Decimal
    Public Property TotalDocumento() As Decimal
        Get
            Return TotalDocumentoValue
        End Get
        Set(ByVal value As Decimal)
            TotalDocumentoValue = value
        End Set
    End Property

    Private TotalRetencionValue As Decimal
    Public Property TotalRetencion() As Decimal
        Get
            Return TotalRetencionValue
        End Get
        Set(ByVal value As Decimal)
            TotalRetencionValue = value
        End Set
    End Property

    Private TotalDiferenciaCambioValue As Decimal
    Public Property TotalDiferenciaCambio() As Decimal
        Get
            Return TotalDiferenciaCambioValue
        End Get
        Set(ByVal value As Decimal)
            TotalDiferenciaCambioValue = value
        End Set
    End Property
    Private DiferidoValue As Boolean
    Public Property Diferido() As Boolean
        Get
            Return DiferidoValue
        End Get
        Set(ByVal value As Boolean)
            DiferidoValue = value
        End Set
    End Property

    Public Property dtDocumento As DataTable

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFOrdenPago").Copy

    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            IDSucursal = oRow("IDSucursal").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        Dim dt As DataTable
        dt = CSistema.ExecuteToDataTable("Select ID,CodigoCuentaContable From VCuentaBancaria")

        For Each oRow As DataRow In dt.Select("ID=" & IDCuentaBancaria)
            CodigoCuentaBancaria = CSistema.RetornarValorInteger(oRow("CodigoCuentaContable").ToString)
        Next

        'Cargar el fijo
        For Each oRow As DataRow In dtCuentaFija.Select("IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda & " and SoloDiferido ='" & Diferido & "'")
            Try

                Dim Tipo As String = oRow("Tipo")

                Select Case Tipo

                    Case "PROVEEDOR"
                        If TotalEfectivo = 0 And TotalCheque <> 0 Then
                            CargarProveedor(oRow)
                        End If
                        If TotalDocumento <> 0 Then
                            CargarProveedorDocumento(oRow)
                        End If
                    Case "CHEQUE"
                        'If oRow("Codigo") = CodigoCuentaBancaria Then
                        If Diferido = False Then
                            CargarCheque(oRow)
                        End If
                        'End If
                    Case "CHEQUE DIFERIDO"
                        If Diferido = True Then
                            CargarChequeDiferido(oRow)
                        End If
                    Case "EFECTIVO"
                        CargarEfectivo(oRow)
                    Case "RETENCION"
                        CargarRetencion(oRow)
                    Case "DIF. CAMBIO"
                        CargarDiferenciaCambio(oRow)
                    Case "DOCUMENTO"
                        CargarDocumento(oRow)
                End Select

            Catch ex As Exception

            End Try

        Next


        Generado = True

    End Sub

    Sub CargarProveedor(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = Total

        'Verificar la configuracion si pide buscar en Cliente/Proveedor
        If CBool(orow("BuscarEnProveedores").ToString) = True Then

            Dim dtTemp As New DataTable

            dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'Denominacion'=CC.Descripcion, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Proveedor P Join VCuentaContable CC On P.CuentaContableCompra=CC.Codigo Where P.ID=" & IDProveedor)

            If Not dtTemp Is Nothing Then

                If dtTemp.Rows.Count > 0 Then
                    Dim rowTemp As DataRow = dtTemp.Rows(0)

                    orow("IDCuentaContable") = rowTemp("IDCuentaContable")
                    orow("Codigo") = rowTemp("Codigo")
                    orow("Denominacion") = rowTemp("Denominacion")
                    orow("Cuenta") = rowTemp("Cuenta")

                End If

            End If

        End If

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarProveedorDocumento(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = Total

        'Verificar la configuracion si pide buscar en Cliente/Proveedor
        'If CBool(orow("BuscarEnProveedores").ToString) = True Then

        Dim dtTemp As New DataTable

        dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 'Denominacion'=CC.Descripcion, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias From Proveedor P Join VCuentaContable CC On P.CuentaContableCompra=CC.Codigo Where P.ID=" & IDProveedor)

        If Not dtTemp Is Nothing Then

            If dtTemp.Rows.Count > 0 Then
                Dim rowTemp As DataRow = dtTemp.Rows(0)

                orow("IDCuentaContable") = rowTemp("IDCuentaContable")
                orow("Codigo") = rowTemp("Codigo")
                orow("Denominacion") = rowTemp("Denominacion")
                orow("Cuenta") = rowTemp("Cuenta")

            End If

        End If

        'End If

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarCheque(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalCheque

        CargarCuentaCuentaBancaria(orow)

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString
        'Siempre debe ir a credito la cuenta de banco
        'If orow("Debe").ToString = True Then
        '    NewRow("Credito") = 0
        '    NewRow("Debito") = Importe
        'End If

        'If orow("Haber").ToString = True Then
        NewRow("Credito") = Importe
        NewRow("Debito") = 0
        ' End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarChequeDiferido(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalCheque

        'CargarCuentaCuentaBancaria(orow)

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarEfectivo(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalEfectivo

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarRetencion(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalRetencion

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarDiferenciaCambio(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalDiferenciaCambio

        If Importe = 0 Then
            Exit Sub
        End If

        If Importe < 0 Then
            Importe = Importe * -1
        End If

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If TotalDiferenciaCambio > 0 Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If TotalDiferenciaCambio < 0 Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarCuentaCuentaBancaria(ByRef oRow As DataRow)

        Dim CuentaBancariaRow As DataRow = CData.GetRow("ID=" & IDCuentaBancaria, "VCuentaBancaria")

        If CuentaBancariaRow Is Nothing Then
            Exit Sub
        End If

        Dim CuentaContableRow As DataRow = CData.GetRow("Codigo='" & CuentaBancariaRow("Codigo CC") & "'", "VCuentaContable")

        If CuentaContableRow Is Nothing Then
            Exit Sub
        End If

        If CuentaContableRow("Codigo").ToString = "" Then
            Exit Sub
        End If

        oRow("IDCuentaContable") = CuentaContableRow("IDCuentaContable").ToString
        oRow("Codigo") = CuentaContableRow("Codigo").ToString
        oRow("CuentaContable") = CuentaContableRow("Codigo").ToString
        oRow("Descripcion") = CuentaContableRow("Descripcion").ToString
        oRow("Denominacion") = CuentaContableRow("Descripcion").ToString
        oRow("Cuenta") = CuentaContableRow("Cuenta").ToString
        oRow("Alias") = CuentaContableRow("Alias").ToString

    End Sub

    Sub CargarDocumento(ByRef orow As DataRow)

        For Each DocumentoRow As DataRow In dtDocumento.Select(" IDTipoComprobante=" & orow("IDTipoComprobante").ToString & " And IDMoneda=" & orow("IDMoneda").ToString)

            Dim NewRow As DataRow = dtDetalleAsiento.NewRow
            Dim Importe As Decimal = DocumentoRow("Importe")

            NewRow("IDTransaccion") = 0
            NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
            NewRow("Codigo") = orow("Codigo").ToString
            NewRow("Descripcion") = orow("Denominacion").ToString
            NewRow("Cuenta") = orow("Cuenta").ToString
            NewRow("Alias") = orow("Alias").ToString
            NewRow("Orden") = orow("Orden").ToString

            If orow("Debe").ToString = True Then
                NewRow("Credito") = 0
                NewRow("Debito") = Importe
            End If

            If orow("Haber").ToString = True Then
                NewRow("Credito") = Importe
                NewRow("Debito") = 0
            End If

            'Otros
            NewRow("ID") = dtDetalleAsiento.Rows.Count
            NewRow("Observacion") = ""
            NewRow("Fijo") = True

            'Solo asignamos si el importe es mayor a 0
            If Importe > 0 Then
                NewRow("Importe") = Importe
                dtDetalleAsiento.Rows.Add(NewRow)
            End If

        Next

    End Sub

End Class

Public Class CAsientoEntregaChequeOP
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDProveedorValue As Integer
    Public Property IDProveedor() As Integer
        Get
            Return IDProveedorValue
        End Get
        Set(ByVal value As Integer)
            IDProveedorValue = value
        End Set
    End Property

    Private TotalChequeValue As Decimal
    Public Property TotalCheque() As Decimal
        Get
            Return TotalChequeValue
        End Get
        Set(ByVal value As Decimal)
            TotalChequeValue = value
        End Set
    End Property

    Private TotalProveedorValue As Decimal
    Public Property TotalProveedor() As Decimal
        Get
            Return TotalProveedorValue
        End Get
        Set(ByVal value As Decimal)
            TotalProveedorValue = value
        End Set
    End Property

    Private CuentaContableProveedorValue As String
    Public Property CuentaContableProveedor() As String
        Get
            Return CuentaContableProveedorValue
        End Get
        Set(ByVal value As String)
            CuentaContableProveedorValue = value
        End Set
    End Property

    Private DiferenciaCambioValue As Decimal
    Public Property DiferenciaCambio() As Decimal
        Get
            Return DiferenciaCambioValue
        End Get
        Set(ByVal value As Decimal)
            DiferenciaCambioValue = value
        End Set
    End Property

    Private DiferidoValue As Boolean
    Public Property Diferido() As Boolean
        Get
            Return DiferidoValue
        End Get
        Set(ByVal value As Boolean)
            DiferidoValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("VCFEntregaChequeOP").Copy

    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            IDSucursal = oRow("IDSucursal").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        'Cargar el fijo
        For Each oRow As DataRow In dtCuentaFija.Select("IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda & " and ChequeDiferido =  '" & Diferido & "'")
            Try

                Dim Tipo As String = oRow("Tipo")
                Select Case Tipo

                    Case "PROVEEDOR"
                        If Diferido = False Then
                            If CuentaContableProveedor = oRow("Codigo").ToString Then
                                CargarProveedor(oRow)
                            End If
                        End If
                        If Diferido = True Then
                            CargarProveedorChequeDiferido(oRow)
                        End If
                    Case "CHEQUE"

                        CargarCheque(oRow)

                End Select

            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

    Sub CargarProveedor(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalProveedor

        'Verificar la configuracion si pide buscar en Cliente/Proveedor
        If CBool(orow("BuscarEnProveedores").ToString) = True Then

            Dim dtTemp As New DataTable

            dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, 
                                                'Codigo'=CC.Codigo, 'Cuenta'=CC.Cuenta, 
                                                'Denominacion'=CC.Descripcion, 
                                                'CuentaContable'=CC.Descripcion, 
                                                'Alias'=CC.Alias 
                                                From Proveedor P 
                                                Join VCuentaContable CC On P.CuentaContableCompra=CC.Codigo 
                                                Where P.ID=" & IDProveedor)

            If Not dtTemp Is Nothing Then

                If dtTemp.Rows.Count > 0 Then
                    Dim rowTemp As DataRow = dtTemp.Rows(0)

                    orow("IDCuentaContable") = rowTemp("IDCuentaContable")
                    orow("Codigo") = rowTemp("Codigo")
                    orow("Denominacion") = rowTemp("Denominacion")
                    orow("Cuenta") = rowTemp("Cuenta")

                End If

            End If

        End If

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarProveedorChequeDiferido(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalProveedor

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarCheque(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalCheque

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub


End Class
Public Class CAsientoVencimientoChequeEmitido
    Inherits CAsiento

    Private dtCuentaFijaValue As DataTable
    Public Property dtCuentaFija() As DataTable
        Get
            Return dtCuentaFijaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaValue = value
        End Set
    End Property

    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Private TotalChequeValue As Decimal
    Public Property TotalCheque() As Decimal
        Get
            Return TotalChequeValue
        End Get
        Set(ByVal value As Decimal)
            TotalChequeValue = value
        End Set
    End Property

    Private CuentaContableBancoValue As String
    Public Property CuentaContableBanco() As String
        Get
            Return CuentaContableBancoValue
        End Get
        Set(ByVal value As String)
            CuentaContableBancoValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        dtCuentaFija = CData.GetTable("vcfvencimientoChequeOp").Copy

    End Sub

    Sub Generar()

        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
            IDSucursal = oRow("IDSucursal").ToString
        Next

        ' Variables
        Dim Importe As Decimal = 0

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        'Cargar el fijo
        For Each oRow As DataRow In dtCuentaFija.Select("IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda)
            Try

                Dim Tipo As String = oRow("Tipo")
                Select Case Tipo

                    Case "BANCO"
                        CargarBanco(oRow)
                    Case "CHEQUE"
                        CargarCheque(oRow)
                End Select

            Catch ex As Exception

            End Try

        Next

        Generado = True

    End Sub

    Sub CargarBanco(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalCheque

        'Verificar la configuracion si pide buscar en Cliente/Proveedor
        If CBool(orow("BuscarEnBanco").ToString) = True Then

            Dim dtTemp As New DataTable

            dtTemp = CSistema.ExecuteToDataTable("Select 'IDCuentaContable'=CC.ID, CC.Codigo, 'Cuenta'=CC.Cuenta, 'Denominacion'=CC.Descripcion, 'CuentaContable'=CC.Descripcion, 'Alias'=CC.Alias from VCuentaBancaria CB Join vCuentaContable CC on CC.Codigo = CB.CodigoCuentaContable where CB.ID = " & IDCuentaBancaria)
            If Not dtTemp Is Nothing Then

                If dtTemp.Rows.Count > 0 Then
                    Dim rowTemp As DataRow = dtTemp.Rows(0)

                    orow("IDCuentaContable") = rowTemp("IDCuentaContable")
                    orow("Codigo") = rowTemp("Codigo")
                    orow("Denominacion") = rowTemp("Denominacion")
                    orow("Cuenta") = rowTemp("Cuenta")

                End If

            End If

        End If

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub

    Sub CargarCheque(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalCheque

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If orow("Debe").ToString = True Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If orow("Haber").ToString = True Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub


End Class



Public Class CAsientoCobranza
    Inherits CAsiento

    Private dtCuentaFijaVentaValue As DataTable
    Public Property dtCuentaFijaVenta() As DataTable
        Get
            Return dtCuentaFijaVentaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaVentaValue = value
        End Set
    End Property

    Private dtCuentaFijaChequeValue As DataTable
    Public Property dtCuentaFijaCheque() As DataTable
        Get
            Return dtCuentaFijaChequeValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaChequeValue = value
        End Set
    End Property

    Private dtCuentaFijaEfectivoValue As DataTable
    Public Property dtCuentaFijaEfectivo() As DataTable
        Get
            Return dtCuentaFijaEfectivoValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaEfectivoValue = value
        End Set
    End Property

    Private dtCuentaFijaDocumentoValue As DataTable
    Public Property dtCuentaFijaDocumento() As DataTable
        Get
            Return dtCuentaFijaDocumentoValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaDocumentoValue = value
        End Set
    End Property
    Private dtCuentaFijaTarjetaValue As DataTable
    Public Property dtCuentaFijaTarjeta() As DataTable
        Get
            Return dtCuentaFijaTarjetaValue
        End Get
        Set(ByVal value As DataTable)
            dtCuentaFijaTarjetaValue = value
        End Set
    End Property

    Private dtVentaValue As DataTable
    Public Property dtVenta() As DataTable
        Get
            Return dtVentaValue
        End Get
        Set(ByVal value As DataTable)
            dtVentaValue = value
        End Set
    End Property

    Private dtChequeValue As DataTable
    Public Property dtCheque() As DataTable
        Get
            Return dtChequeValue
        End Get
        Set(ByVal value As DataTable)
            dtChequeValue = value
        End Set
    End Property

    Private dtEfectivoValue As DataTable
    Public Property dtEfectivo() As DataTable
        Get
            Return dtEfectivoValue
        End Get
        Set(ByVal value As DataTable)
            dtEfectivoValue = value
        End Set
    End Property

    Private dtDocumentoValue As DataTable
    Public Property dtDocumento() As DataTable
        Get
            Return dtDocumentoValue
        End Get
        Set(ByVal value As DataTable)
            dtDocumentoValue = value
        End Set
    End Property
    Private dtTarjetaValue As DataTable
    Public Property dtTarjeta() As DataTable
        Get
            Return dtTarjetaValue
        End Get
        Set(ByVal value As DataTable)
            dtTarjetaValue = value
        End Set
    End Property
    Private IDSucursalValue As Integer
    Public Property IDSucursal() As Integer
        Get
            Return IDSucursalValue
        End Get
        Set(ByVal value As Integer)
            IDSucursalValue = value
        End Set
    End Property

    Private IDMonedaValue As Integer
    Public Property IDMoneda() As Integer
        Get
            Return IDMonedaValue
        End Get
        Set(ByVal value As Integer)
            IDMonedaValue = value
        End Set
    End Property

    Private IDCuentaBancariaValue As Integer
    Public Property IDCuentaBancaria() As Integer
        Get
            Return IDCuentaBancariaValue
        End Get
        Set(ByVal value As Integer)
            IDCuentaBancariaValue = value
        End Set
    End Property

    Private TotalDiferenciaCambioValue As Decimal
    Public Property TotalDiferenciaCambio() As Decimal
        Get
            Return TotalDiferenciaCambioValue
        End Get
        Set(ByVal value As Decimal)
            TotalDiferenciaCambioValue = value
        End Set
    End Property

    Sub InicializarAsiento()

        Inicializar()
        'dtCuentaFijaVenta = CData.GetTable("VCFCobranzaVenta", "IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda).Copy
        'dtCuentaFijaCheque = CData.GetTable("VCFCobranzaCheque", "IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda).Copy
        'dtCuentaFijaEfectivo = CData.GetTable("VCFCobranzaEfectivo", "IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda).Copy
        'dtCuentaFijaDocumento = CData.GetTable("VCFCobranzaDocumento", "IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda).Copy
        'dtCuentaFijaTarjeta = CData.GetTable("VCFCobranzaTarjeta", "IDSucursal=" & IDSucursal & " And IDMoneda=" & IDMoneda).Copy

    End Sub

    Sub Generar()
        If Bloquear = True Then
            Exit Sub
        End If

        If Generado = True Then
            Exit Sub
        Else
            Generado = True
        End If

        'Establecer la moneda
        For Each oRow As DataRow In dtAsiento.Rows
            IDMoneda = oRow("IDMoneda").ToString
        Next

        'Eliminar las cuentas que son fijas para recargar.
        EliminarDetalleFijos()

        'Cargar las Ventas
        CargarVentas()

        'Cargar Cheques
        CargarCheques()

        'Cargar Efectivos
        CargarEfectivo()

        'Cargar Documentos
        CargarDocumento()

        CargarDiferenciaCambio()

        Generado = True

    End Sub

    Sub CargarVentas()

        'Cargar el fijo Ventas
        For Each VentaRow As DataRow In dtVenta.Rows

            Try

                'Tipo
                Dim Where As String = ""

                If VentaRow("Credito") = True Then
                    Where = " Credito = 'True' "
                Else
                    Where = " Credito = 'False' "
                End If

                For Each oRow As DataRow In dtCuentaFijaVenta.Select(Where)
                    If CBool(oRow("DiferenciaCambio")) = False Then
                        Dim Importe As Decimal = VentaRow("Importe")

                        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                        NewRow("IDTransaccion") = 0
                        NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                        NewRow("Codigo") = oRow("Codigo").ToString
                        NewRow("Descripcion") = oRow("Denominacion").ToString
                        NewRow("Cuenta") = oRow("Cuenta").ToString
                        NewRow("Alias") = oRow("Alias").ToString
                        NewRow("Orden") = oRow("Orden").ToString

                        If oRow("Debe").ToString = True Then
                            NewRow("Credito") = 0
                            NewRow("Debito") = Importe
                        End If

                        If oRow("Haber").ToString = True Then
                            NewRow("Credito") = Importe
                            NewRow("Debito") = 0
                        End If

                        'Otros
                        NewRow("ID") = dtDetalleAsiento.Rows.Count
                        NewRow("Observacion") = ""
                        NewRow("Fijo") = True

                        'Solo asignamos si el importe es mayor a 0
                        If Importe > 0 Then
                            NewRow("Importe") = Importe
                            dtDetalleAsiento.Rows.Add(NewRow)
                        End If
                    End If

                Next

            Catch ex As Exception

            End Try

        Next
    End Sub
    Sub CargarDiferenciaCambio()

        'Cargar el fijo Ventas
        For Each VentaRow As DataRow In dtVenta.Rows

            Try
                For Each oRow As DataRow In dtCuentaFijaVenta.Select(" DiferenciaCambio = 1")
                    If CBool(oRow("DiferenciaCambio")) = True Then
                        Dim Importe As Decimal = TotalDiferenciaCambio
                        If Importe = 0 Then
                            Exit Sub
                        End If
                        If Importe < 0 Then
                            Importe = Importe * -1
                        End If

                        Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                        NewRow("IDTransaccion") = 0
                        NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                        NewRow("Codigo") = oRow("Codigo").ToString
                        NewRow("Descripcion") = oRow("Denominacion").ToString
                        NewRow("Cuenta") = oRow("Cuenta").ToString
                        NewRow("Alias") = oRow("Alias").ToString
                        NewRow("Orden") = oRow("Orden").ToString

                        If TotalDiferenciaCambio > 0 Then
                            NewRow("Credito") = 0
                            NewRow("Debito") = Importe
                        End If

                        If TotalDiferenciaCambio < 0 Then
                            NewRow("Credito") = Importe
                            NewRow("Debito") = 0
                        End If

                        'Otros
                        NewRow("ID") = dtDetalleAsiento.Rows.Count
                        NewRow("Observacion") = ""
                        NewRow("Fijo") = True

                        'Solo asignamos si el importe es mayor a 0
                        If Importe > 0 Then
                            NewRow("Importe") = Importe
                            dtDetalleAsiento.Rows.Add(NewRow)
                        End If
                    End If

                Next

            Catch ex As Exception

            End Try

        Next
    End Sub

    Sub CargarCheques()

        Try
            'Cargar el fijo Cheque
            For Each ChequeRow As DataRow In dtCheque.Rows


                'Tipo
                Dim Where As String = ""

                If ChequeRow("Diferido") = True Then
                    Where = " Diferido = 'True' "
                Else
                    Where = " Diferido = 'False' "
                End If

                For Each oRow As DataRow In dtCuentaFijaCheque.Select(Where)

                    Dim Importe As Decimal = ChequeRow("Importe")

                    Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Alias").ToString
                    NewRow("Orden") = oRow("Orden").ToString

                    If oRow("Debe").ToString = True Then
                        NewRow("Credito") = 0
                        NewRow("Debito") = Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        NewRow("Credito") = Importe
                        NewRow("Debito") = 0
                    End If

                    'Otros
                    NewRow("ID") = dtDetalleAsiento.Rows.Count
                    NewRow("Observacion") = ""
                    NewRow("Fijo") = True

                    'Solo asignamos si el importe es mayor a 0
                    If Importe > 0 Then
                        NewRow("Importe") = Importe
                        dtDetalleAsiento.Rows.Add(NewRow)
                    End If

                Next
            Next
        Catch ex As Exception

        End Try



    End Sub

    Sub CargarEfectivo()

        'Cargar el fijo Cheque
        For Each EfectivoRow As DataRow In dtEfectivo.Rows

            Try

                For Each oRow As DataRow In dtCuentaFijaEfectivo.Select(" IDTipoComprobante = " & EfectivoRow("IDTipoComprobante"))

                    Dim Importe As Decimal = EfectivoRow("Importe")

                    Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Alias").ToString
                    NewRow("Orden") = oRow("Orden").ToString

                    If oRow("Debe").ToString = True Then
                        NewRow("Credito") = 0
                        NewRow("Debito") = Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        NewRow("Credito") = Importe
                        NewRow("Debito") = 0
                    End If

                    'Otros
                    NewRow("ID") = dtDetalleAsiento.Rows.Count
                    NewRow("Observacion") = ""
                    NewRow("Fijo") = True

                    'Solo asignamos si el importe es mayor a 0
                    If Importe > 0 Then
                        NewRow("Importe") = Importe
                        dtDetalleAsiento.Rows.Add(NewRow)
                    End If

                Next

            Catch ex As Exception

            End Try

        Next

    End Sub

    Sub CargarDocumento()

        'Cargar el fijo Documento
        For Each DocumentoRow As DataRow In dtDocumento.Rows

            Try

                For Each oRow As DataRow In dtCuentaFijaDocumento.Select(" IDTipoComprobante = " & DocumentoRow("IDTipoComprobante"))

                    Dim Importe As Decimal = DocumentoRow("Importe")

                    Dim NewRow As DataRow = dtDetalleAsiento.NewRow

                    NewRow("IDTransaccion") = 0
                    NewRow("IDCuentaContable") = oRow("IDCuentaContable").ToString
                    NewRow("Codigo") = oRow("Codigo").ToString
                    NewRow("Descripcion") = oRow("Denominacion").ToString
                    NewRow("Cuenta") = oRow("Cuenta").ToString
                    NewRow("Alias") = oRow("Alias").ToString
                    NewRow("Orden") = oRow("Orden").ToString

                    If oRow("Debe").ToString = True Then
                        NewRow("Credito") = 0
                        NewRow("Debito") = Importe
                    End If

                    If oRow("Haber").ToString = True Then
                        NewRow("Credito") = Importe
                        NewRow("Debito") = 0
                    End If

                    'Otros
                    NewRow("ID") = dtDetalleAsiento.Rows.Count
                    NewRow("Observacion") = ""
                    NewRow("Fijo") = True

                    'Solo asignamos si el importe es mayor a 0
                    If Importe > 0 Then
                        NewRow("Importe") = Importe
                        dtDetalleAsiento.Rows.Add(NewRow)
                    End If

                Next

            Catch ex As Exception

            End Try

        Next

    End Sub

    Sub CargarDiferenciaCambio(ByRef orow As DataRow)

        Dim NewRow As DataRow = dtDetalleAsiento.NewRow
        Dim Importe As Decimal = TotalDiferenciaCambio

        If Importe = 0 Then
            Exit Sub
        End If

        If Importe < 0 Then
            Importe = Importe * -1
        End If

        NewRow("IDTransaccion") = 0
        NewRow("IDCuentaContable") = orow("IDCuentaContable").ToString
        NewRow("Codigo") = orow("Codigo").ToString
        NewRow("Descripcion") = orow("Denominacion").ToString
        NewRow("Cuenta") = orow("Cuenta").ToString
        NewRow("Alias") = orow("Alias").ToString
        NewRow("Orden") = orow("Orden").ToString

        If TotalDiferenciaCambio > 0 Then
            NewRow("Credito") = 0
            NewRow("Debito") = Importe
        End If

        If TotalDiferenciaCambio < 0 Then
            NewRow("Credito") = Importe
            NewRow("Debito") = 0
        End If

        'Otros
        NewRow("ID") = dtDetalleAsiento.Rows.Count
        NewRow("Observacion") = ""
        NewRow("Fijo") = True

        'Solo asignamos si el importe es mayor a 0
        If Importe > 0 Then
            NewRow("Importe") = Importe
            dtDetalleAsiento.Rows.Add(NewRow)
        End If

    End Sub


End Class

Public Class CSeguridad

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Private newCPUID As String
    Public Property CPUID() As String
        Get
            Return newCPUID
        End Get
        Set(ByVal value As String)
            newCPUID = value
        End Set
    End Property

    Private newHDDID As String
    Public Property HDDID() As String
        Get
            Return newHDDID
        End Get
        Set(ByVal value As String)
            newHDDID = value
        End Set
    End Property

    Private newNETID As String
    Public Property NETID() As String
        Get
            Return newNETID
        End Get
        Set(ByVal value As String)
            newNETID = value
        End Set
    End Property

    Private newNAME As String
    Public Property NAME() As String
        Get
            Return newNAME
        End Get
        Set(ByVal value As String)
            newNAME = value
        End Set
    End Property

    Private newCODIGO As String
    Public Property CODIGO() As String
        Get
            Return newCODIGO
        End Get
        Set(ByVal value As String)
            newCODIGO = value
        End Set
    End Property

    'FUNCIONES
    Public Sub GetHDSerial()

        Try
            Dim fso As New FileSystemObject
            Dim selected_drive As Drive = fso.GetDrive(Application.ExecutablePath.Substring(0, 3))
            Dim disk As New ManagementObject("Win32_LogicalDisk.DeviceID=""" + selected_drive.DriveLetter + ":""")
            Dim diskPropertyA As PropertyData = disk.Properties("VolumeSerialNumber")
            HDDID = diskPropertyA.Value.ToString()
        Catch
        End Try

    End Sub

    Public Sub GetCPUId()

        Try
            Dim cpuInfo As String = String.Empty
            Dim temp As String = String.Empty
            Dim mc As ManagementClass = New ManagementClass("Win32_Processor")
            Dim moc As ManagementObjectCollection = mc.GetInstances()
            For Each mo As ManagementObject In moc
                If cpuInfo = String.Empty Then
                    cpuInfo = mo.Properties("ProcessorId").Value.ToString()
                End If
            Next
            CPUID = cpuInfo
        Catch
        End Try

    End Sub

    Public Sub GetNETID()
        Try
            Dim objMOS As ManagementObjectSearcher
            Dim objMOC As Management.ManagementObjectCollection
            Dim objMO As Management.ManagementObject

            objMOS = New ManagementObjectSearcher("Select * From Win32_NetworkAdapter")
            objMOC = objMOS.Get

            For Each objMO In objMOC
                NETID = " " & NETID & " " & objMO("MACAddress")
            Next

            NETID = NETID.Trim
        Catch
        End Try


    End Sub

    Public Sub GetNAME()
        NAME = My.Computer.Name
    End Sub

    Public Sub New()

        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "CSistema/new 16845" & Date.Now.ToString() & vbCrLf)
        CadenaConexion = ""

        'GetHDSerial()
        'GetCPUId()
        'GetNAME()
        System.IO.File.AppendAllText("c:\tmp\registroEventos.txt", "CSistema/new 16845" & Date.Now.ToString() & vbCrLf)
        'CODIGO = NAME & " " & CPUID & " " & HDDID

    End Sub

    Public Sub New(ByVal vConexion As String)

        CadenaConexion = vConexion

        GetHDSerial()
        GetCPUId()
        GetNAME()

        CODIGO = NAME & " " & CPUID & " " & HDDID

    End Sub

    Public Function CargarTerminal() As Boolean

        CargarTerminal = False

    End Function

    Public Function Login(ByVal Usuario As String, ByVal Password As String) As Boolean

        Login = False

        'Cosultar
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Exec SpAcceso @Usuario='" & Usuario & "', @Password='" & Password & "', @IDTerminal='" & vgIDTerminal & "' ", CadenaConexion)

    End Function

    Public Sub RegistrarSuceso(ByVal Tipo As CSistema.NUMTipoOperaciones, ByVal Tabla As String)

        Dim param(-1) As SqlClient.SqlParameter

        CSistema.SetSQLParameter(param, "@Operacion", Tipo.ToString, ParameterDirection.Input)
        CSistema.SetSQLParameter(param, "@Tabla", Tabla, ParameterDirection.Input)

        'Establecer comportamientos dependiendo de que si el proceso fue valido o no.
        CSistema.ExecuteStoreProcedure(param, "SpLogSuceso", False, False, "", "", True, CadenaConexion)

    End Sub

    Public Sub OcultarControlesDenegados(ByVal frm As Form)

        If vgIDPerfil = 1 Then
            Exit Sub
        End If

        Dim Filtro As String = " IDPerfil=" & vgIDPerfil & " And Tag='" & frm.Name & "' And Control='True' "

        If Not CData.GetTable("VAccesoEspecificoPerfil") Is Nothing Then
            Dim dt As DataTable = CData.GetTable("VAccesoEspecificoPerfil").Copy
            dt = CData.FiltrarDataTable(dt, Filtro)

            For Each oRow As DataRow In dt.Rows
                OcultarControles(frm, oRow("NombreControl").ToString, oRow("Enabled").ToString, oRow("Visible").ToString, oRow("Habilitado").ToString)
            Next

        End If

    End Sub



    Private Sub OcultarControles(ByVal ctrs As Control, ByVal Control As String, ByVal Enable As Boolean, ByVal Visible As Boolean, ByVal Valor As Boolean)

        For Each ctr As Control In ctrs.Controls

            If ctr.Name = "OcxSucursalesClientes1" Then
                Debug.Print("aqui)")
            End If

            If ctr.Name = Control Then

                If Enable = True Then
                    ctr.Enabled = Valor
                End If

                If Visible = True Then
                    If Valor = False Then
                        ctr.Dispose()
                    End If
                End If

                Exit For
            Else
                If ctr.Controls.Count > 0 Then
                    OcultarControles(ctr, Control, Enable, Visible, Valor)
                End If
            End If
        Next

    End Sub


End Class


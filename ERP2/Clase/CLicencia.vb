﻿
Public Class CLicencia

    'CLASES
    Dim CArchivoInicio As New CArchivoInicio
    Dim CDecode As New ERP2ED.CDecode
    Dim CEn As New CEn

    'PROPIEDADES
    Private dtInformacionValue As DataTable
    Public Property dtInformacion() As DataTable
        Get
            Return dtInformacionValue
        End Get
        Set(ByVal value As DataTable)
            dtInformacionValue = value
        End Set
    End Property

    Public Function Existe() As Boolean

        Dim LicenciaPath As String = vgLicenciaPath

        If LicenciaPath.Trim.Length = 0 Then
            MessageBox.Show("La licencia no fue encontrada!!!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If My.Computer.FileSystem.FileExists(LicenciaPath) = False Then
            MessageBox.Show("La licencia no fue encontrada!!!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        Return True

    End Function

    Public Sub InfoLicencia()

        Dim sContent As String = vbNullString
        Dim CDecode As New ERP2ED.CDecode
        sContent = My.Computer.FileSystem.ReadAllText(vgLicenciaPath)

        CargarInformacion(sContent)

        Dim oRow As DataRow = dtInformacion.Rows(0)


        vgLicenciaOtorgadoRazonSocial = oRow("RAZON SOCIAL")
        vgLicenciaOtorgadoRUC = oRow("RUC")
        vgLicenciaOtorgadoDireccion = oRow("DIRECCION")
        vgLicenciaOtorgadoTelefono = oRow("TELEFONO")
        vgLicenciaOtorgadoWEB = oRow("WEB")
        vgLicenciaOtorgadoEmail = oRow("EMAIL")
        vgLicenciaOtorgadoFechaInicio = oRow("FECHA INICIO")
        vgLicenciaOtorgadoFechaFin = oRow("FECHA FIN")
        vgLicenciaOtorgadoDepositos = oRow("CANTIDAD DEPOSITOS")
        vgLicenciaOtorgadoTerminales = oRow("CANTIDAD TERMINALES")
        vgLicenciaOtorgadoAdmin = oRow("ADMIN")
        vgLicenciaOtorgadoPassword = oRow("PASSWORD")

        'HACER SUCURSALES
        vgLicenciaOtorgadoSucursales = "10"

        'Generar Cadena
        vgLicenciaOtorgado = "Licencia otorgada a: " & vgLicenciaOtorgadoRazonSocial
        If vgLicenciaOtorgadoRUC <> "" Then
            vgLicenciaOtorgado = vgLicenciaOtorgado & " - RUC: " & vgLicenciaOtorgadoRUC
        End If


    End Sub

    Public Sub InfoLicencia2()


seguir:
        Dim sContent As String = vbNullString
        sContent = My.Computer.FileSystem.ReadAllText(vgLicenciaPath)
        sContent = CEn.Decrypt(sContent, vgKey)
        If sContent.Length = 0 Then
            MessageBox.Show("La licencia no es correcta!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            CArchivoInicio.IniWrite(VGArchivoINI, "LICENCIA", "PATH", "")
            frmLicenciaPath.ShowDialog()
            GoTo seguir
        End If

        Dim Tablas() As String = sContent.Split("^")

        'Validar
        If Tablas.GetLength(0) <> 4 Then
            MessageBox.Show("La licencia no es correcta!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            CArchivoInicio.IniWrite(VGArchivoINI, "LICENCIA", "PATH", "")
            frmLicenciaPath.ShowDialog()
            GoTo seguir
        End If

        If Tablas(0) <> "SAIN" Then
            MessageBox.Show("La licencia no es correcta!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            CArchivoInicio.IniWrite(VGArchivoINI, "LICENCIA", "PATH", "")
            frmLicenciaPath.ShowDialog()
            GoTo seguir
        End If

        For c As Integer = 0 To Tablas.GetLength(0) - 1
            Tablas(c) = Tablas(c).Replace("[", "")
            Tablas(c) = Tablas(c).Replace("]", "")
        Next

        'Cargar las tablas
        For t As Integer = 1 To Tablas.GetLength(0) - 1

            Dim dt As New DataTable(t.ToString)
            Dim Registros() As String = Tablas(t).Split("|")

            For i As Integer = 0 To Registros.GetLength(0) - 1
                Dim Campos() As String = Registros(i).Split("&")

                'Cargar los nombres de campos si i = 0
                If i = 0 Then
                    For c As Integer = 0 To Campos.GetLength(0) - 1
                        dt.Columns.Add(Campos(c).Split("=")(0))
                    Next
                End If

                Dim NewRow As DataRow = dt.NewRow

                For c As Integer = 0 To Campos.GetLength(0) - 1
                    NewRow(Campos(c).Split("=")(0)) = Campos(c).Split("=")(1)
                Next

                dt.Rows.Add(NewRow)

            Next

            vgSAIN.Tables.Add(dt)

        Next

        'Renonmbrar tablas
        vgSAIN.Tables(0).TableName = "Empresa"
        vgSAIN.Tables(1).TableName = "Menu"
        vgSAIN.Tables(2).TableName = "Conexion"

        'Cargar Info de Empresa
        For Each oRow As DataRow In vgSAIN.Tables("Empresa").Rows
            vgLicenciaOtorgadoRazonSocial = oRow("RazonSocial").ToString
            vgLicenciaOtorgadoRUC = oRow("RUC").ToString
            vgLicenciaOtorgadoDireccion = oRow("Direccion").ToString
            vgLicenciaOtorgadoTelefono = oRow("Telefono").ToString
            vgLicenciaOtorgadoEmail = oRow("Email").ToString
            vgLicenciaOtorgadoWEB = oRow("Web").ToString
            vgLicenciaOtorgadoFechaInicio = oRow("Desde").ToString
            vgLicenciaOtorgadoFechaFin = oRow("Hasta").ToString
            vgLicenciaOtorgadoEmpresas = oRow("Empresas").ToString
            vgLicenciaOtorgadoSucursales = oRow("Sucursales").ToString
            vgLicenciaOtorgadoTerminales = oRow("Terminales").ToString
            vgLicenciaOtorgadoAdmin = oRow("Usuario").ToString
            vgLicenciaOtorgadoPassword = oRow("Password").ToString
        Next

        'Generar Cadena
        vgLicenciaOtorgado = "Licencia otorgada a: " & vgLicenciaOtorgadoRazonSocial
        If vgLicenciaOtorgadoRUC <> "" Then
            vgLicenciaOtorgado = vgLicenciaOtorgado & " - RUC: " & vgLicenciaOtorgadoRUC
        End If

        'Cargar Info de la Conexion
        'Verificar si se selecciono una conexion
        Dim TipoConexion As String = CArchivoInicio.IniGet(VGArchivoINI, "CONEXION", "TIPO", "")

        If TipoConexion = "" Then

            'Si hay una sola conexion, seleccionar
            If vgSAIN.Tables("Conexion").Rows.Count = 1 Then
                TipoConexion = vgSAIN.Tables("Conexion").Rows(0)("Descripcion").ToString
            Else
                Dim frm As New frmSeleccionConexion
                frm.dt = vgSAIN.Tables("Conexion")
                frm.ShowDialog()
                If frm.Procesado = False Then
                    MessageBox.Show("No se selecciono ninguna conexion! No se puede continuar", "Conexion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    End
                End If

                TipoConexion = frm.Conexion

            End If

        End If

        For Each oRow As DataRow In vgSAIN.Tables("Conexion").Select("Descripcion='" & TipoConexion & "' ")
            vgNombreServidor = oRow("Servidor").ToString
            vgNombreBaseDatos = oRow("BaseDatos").ToString
            vgNombreUsuarioBD = oRow("Usuario").ToString
            vgPasswordBD = oRow("Password").ToString
        Next

        'Verificar que se haya seleccionado correctamente
        If vgNombreServidor = "" Then
            CArchivoInicio.IniWrite(VGArchivoINI, "CONEXION", "TIPO", "")
            MessageBox.Show("No se selecciono correctamente la conexion! No se puede continuar", "Conexion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End
        End If

        FGEstablecerConexion()

    End Sub

    Public Function TerminalHabilitada(ByVal CInfoHardware As CInfoHardware) As Boolean

        Dim sContent As String = vbNullString
        sContent = My.Computer.FileSystem.ReadAllText(vgLicenciaPath)
        CargarInformacion(sContent)


        Dim oRow As DataRow = dtInformacion.Rows(0)

        'Obtenemos las terminales cargadas en la licencia
        Dim Terminales() As String = oRow("Terminales").ToString.Split("#")

        'Buscamos nuestra terminal en la licencia
        For i As Integer = 0 To Terminales.GetLength(0) - 1
            If Terminales(i) = CInfoHardware.CODIGO Then
                Return True
            End If

        Next

        Return False

    End Function

    Private Sub CargarInformacion(ByVal Codigo As String)

        Try
            'Desencriptar
            CDecode.InClearText = Codigo
            CDecode.Decrypt()

            vgLicencia = CDecode.CryptedText

            Dim Identificacion As String = CDecode.CryptedText

            dtInformacion = New DataTable

            'Creamos la columan
            Dim Campos() As String = Identificacion.Split("&")
            For i As Integer = 0 To Campos.GetLength(0) - 1
                Dim Fracmento() As String = Campos(i).Split("=")
                dtInformacion.Columns.Add(Fracmento(0))
            Next

            'Cargamos valores
            Dim NewRow As DataRow = dtInformacion.NewRow
            For i As Integer = 0 To Campos.GetLength(0) - 1
                Dim Fracmento() As String = Campos(i).Split("=")
                NewRow(Fracmento(0)) = Fracmento(1)
            Next


            dtInformacion.Rows.Add(NewRow)
        Catch ex As Exception

            MessageBox.Show("La licencia no es correcta!!!", "Licencia no encontrada.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            frmLicenciaPath.ShowDialog()

            If Existe() = False Then
                MessageBox.Show("El sistema no encuentra la licencia!!! Favor intentelo de nuevo.", "Licencia no encontrada.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End
            End If

            CargarInformacion(Codigo)

        End Try


    End Sub

    Public Function FechaCaducada() As Boolean


        Dim oRow As DataRow = dtInformacion.Rows(0)

        'Validar fecha de inicio
        If IsDate(oRow("FECHA INICIO").ToString) = True Then
            If CDate(oRow("FECHA INICIO").ToString) > Date.Now Then
                MessageBox.Show("La fecha no concuerda con la estipulada en la licencia!!!", "Fecha incorrecta", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If
        End If

        'Validar fecha de fin
        If IsDate(oRow("FECHA FIN").ToString) = True Then
            If CDate(oRow("FECHA FIN").ToString) < Date.Now Then
                MessageBox.Show("La licencia a caducado! Solicite una nueva a su proveedor", "Licencia caducada", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If

        End If

        Return True


    End Function

    Public Function FechaCaducada2() As Boolean


        Dim oRow As DataRow = vgSAIN.Tables("Empresa").Rows(0)

        If oRow("FECHA") = True Then
            'Validar fecha de inicio
            If IsDate(oRow("DESDE").ToString) = True Then
                If CDate(oRow("DESDE").ToString) > Date.Now Then
                    MessageBox.Show("La fecha no concuerda con la estipulada en la licencia!!!", "Fecha incorrecta", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return False
                End If
            End If

            'Validar fecha de fin
            If IsDate(oRow("HASTA").ToString) = True Then
                If CDate(oRow("HASTA").ToString) < Date.Now Then
                    MessageBox.Show("La licencia a caducado! Solicite una nueva a su proveedor", "Licencia caducada", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return False
                End If

            End If
        End If

        Return True


    End Function

End Class

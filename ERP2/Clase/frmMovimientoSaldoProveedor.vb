﻿Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows
Imports System.Drawing
Imports System.IO

Namespace Reporte

    Public Class CReporte
        'Timeout aumentada para algunas conexiones. Establecer a la propiedad SelectCommand TimeOut del SqlAdapter. Ejemplo en Sub ExistenciaValorizadaResumen //JGR 20140519
        Public Const CONNECTION_TIMEOUT_120 As Integer = 120

        'CLASES
        Protected CSistema As New CSistema
        Protected CData As New CData
        Protected CArchivoInicio As New CArchivoInicio

        'PROPIEDADES

        'FUNCIONES
        Protected Sub EstablecerConexion(ByVal report As ReportDocument)

            Dim CRTLI As CrystalDecisions.Shared.TableLogOnInfo
            For Each CRTable In report.Database.Tables
                CRTLI = CRTable.LogOnInfo
                With CRTLI.ConnectionInfo
                    .ServerName = vgNombreServidor
                    .UserID = vgNombreUsuarioBD
                    .Password = vgPasswordBD
                    .DatabaseName = vgNombreBaseDatos
                    .Type = CrystalDecisions.Shared.ConnectionInfoType.Query
                    .IntegratedSecurity = False
                End With
                CRTable.ApplyLogOnInfo(CRTLI)
            Next CRTable

        End Sub

       
        ''' <summary>
        ''' Compara la revisión del archivo local con la de la base de datos para saber si debe actualizar el archivo .rpt local
        ''' </summary>
        ''' <param name="report">Objeto reporte</param>
        ''' <param name="Name">Nombre físico del archivo de informe .rpt</param>
        ''' <remarks></remarks>
        Protected Sub LoadReport(ByRef report As ReportDocument, ByVal Name As String)
            Try
                'Verificar que exista la carpeta de reportes
                If FileIO.FileSystem.DirectoryExists(VGCarpetaReporte) = False Then
                    FileIO.FileSystem.CreateDirectory(VGCarpetaReporte)
                End If

                Dim revisionLocal As Integer = 0
                If IO.File.Exists(VGCarpetaReporte & Name & ".rpt") Then
                    report.FileName = VGCarpetaReporte & Name & ".rpt"
                    If report.SummaryInfo.ReportComments IsNot Nothing Then
                        revisionLocal = CType(report.SummaryInfo.ReportComments, Integer)
                    End If
                End If

                Dim revisionBD As Integer = 0
                'Dim rptNombre As String = System.IO.Path.GetFileNameWithoutExtension(Name)
                revisionBD = CType(CSistema.ExecuteScalar("SELECT ISNULL(MAX(version),0) FROM Informe WHERE nombre = '" & Name & "'"), Integer)
                If revisionBD > 0 AndAlso revisionLocal < revisionBD Then
                    'Get File data from dataset row.
                    Dim FileData As Byte() = Nothing
                    FileData = DirectCast(CSistema.ExecuteScalar("SELECT TOP 1 archivo FROM Informe WHERE nombre = '" & _
                                                      Name & "' ORDER BY version DESC"), Byte())
                    If FileData IsNot Nothing Then
                        'Write file data to selected file.
                        Using fs As New FileStream(VGCarpetaReporte & Name & ".rpt", FileMode.Create)
                            fs.Write(FileData, 0, FileData.Length)
                            fs.Close()
                        End Using
                        report.FileName = VGCarpetaReporte & Name & ".rpt"

                    End If

                End If
            Catch ex As Exception
                Throw
            End Try

            'Dim ArchivoLocal As String = VGCarpetaReporte & Name & ".rpt"
            'Dim ArchivoTemporal As String = VGCarpetaReporteTemporal & Name & ".rpt"

            ''Verificar que exista la carpeta de reportes
            'If FileIO.FileSystem.DirectoryExists(VGCarpetaReporte) = False Then
            '    FileIO.FileSystem.CreateDirectory(VGCarpetaReporte)
            'End If

            ''Verificar que exista el archivo
            'If FileIO.FileSystem.FileExists(ArchivoLocal) = False Then

            '    'Buscamos en la carpeta temporal de informes
            '    Dim NuevoPath As String = ""
            '    If ExisteReporte(VGCarpetaReporteTemporal, Name, NuevoPath) Then

            '        'Copiar reporte
            '        FileIO.FileSystem.CopyFile(NuevoPath, ArchivoLocal, True)

            '    Else

            '        MessageBox.Show("El sistema no encontro el reporte", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)

            '        'Bucamos el archivo
            '        Dim f As New FolderBrowserDialog
            '        f.Description = "Directorio de Reportes SAIN"
            '        f.SelectedPath = VGCarpetaReporteTemporal
            '        f.ShowNewFolderButton = False

            '        If f.ShowDialog() = DialogResult.Cancel Then
            '            MessageBox.Show("No es posible emitir el informe", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            '            Exit Sub
            '        End If

            '        VGCarpetaReporteTemporal = f.SelectedPath & "\"
            '        ArchivoTemporal = VGCarpetaReporteTemporal & Name & ".rpt"

            '        If ExisteReporte(VGCarpetaReporteTemporal, Name, NuevoPath) = True Then
            '            FileIO.FileSystem.CopyFile(NuevoPath, ArchivoLocal)
            '            CArchivoInicio.IniWrite(VGArchivoINI, "INFORMES", "PATH", VGCarpetaReporteTemporal)
            '        End If

            '    End If

            '    If FileIO.FileSystem.FileExists(ArchivoLocal) = False Then
            '        MessageBox.Show("El sistema no encontro el reporte. Vuelva a intentarlo", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            '        Exit Sub
            '    End If

            'Else

            '    Try
            '        'Verificar si es que no hay un nuevo reporte
            '        Dim Archivo1 As Date = System.IO.File.GetLastWriteTime(ArchivoLocal).Date
            '        Dim Archivo2 As Date = System.IO.File.GetLastWriteTime(ArchivoTemporal).Date

            '        If Archivo1 < Archivo2 Then
            '            FileIO.FileSystem.CopyFile(ArchivoTemporal, ArchivoLocal, True)
            '        End If
            '    Catch ex As Exception

            '    End Try

            'End If

            'rpt.FileName = ArchivoLocal

        End Sub

        Private Function ExisteReporte(ByVal dir As String, ByVal rpt As String, ByRef NuevoPath As String) As Boolean

            ExisteReporte = False

            'Archivos dentro de la carpeta
            For Each F As String In Directory.GetFiles(dir)

                If System.IO.Path.GetExtension(F) = ".rpt" Then
                    Dim Archivo As String = dir & rpt & ".rpt"
                    If F.ToUpper = Archivo.ToUpper Then
                        NuevoPath = F
                        Return True
                    End If
                End If
            Next

            'Buscar otras carpetas
            For Each d As String In Directory.GetDirectories(dir)

                If Directory.GetDirectories(d).Count > 0 Then
                    If ExisteReporte(d, rpt, NuevoPath) = True Then
                        Return True
                    End If
                End If

            Next

        End Function

        Protected Sub EstablecerTitulos(ByRef rpt As ReportClass, ByVal Titulo As String, ByVal SubTitulo As String)

            Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
            txtField = Nothing

            For Each a As Section In rpt.ReportDefinition.Sections
                For i As Integer = 0 To a.ReportObjects.Count - 1

                    Try
                        Debug.Print(a.ReportObjects.Item(i).GetType.Name)

                        If a.ReportObjects.Item(i).GetType.Name = "TextObject" Then
                            txtField = CType(a.ReportObjects.Item(i), CrystalDecisions.CrystalReports.Engine.TextObject)

                            Select Case a.ReportObjects.Item(i).Name
                                Case "txtResponsable"
                                    txtField.Text = vgUsuario & " - " & VGFechaHoraSistema.ToShortDateString & " " & VGFechaHoraSistema.ToLongTimeString
                                Case "txtSistema"
                                    txtField.Text = VGSoftwareInfo
                                Case "txtTitulo"
                                    txtField.Text = Titulo.ToUpper
                                Case "txtTipoInforme"
                                    txtField.Text = SubTitulo
                            End Select

                        End If

                    Catch ex As Exception

                    End Try


                Next
            Next

        End Sub

        ''' <summary>
        ''' Establece el texto para un campo de texto específico dentro del informe
        ''' </summary>
        ''' <param name="rpt">Objeto Informe</param>
        ''' <param name="nombre">Nombre del campo de texto (name)</param>
        ''' <param name="texto">Texto a mostrar</param>
        ''' <remarks></remarks>
        Protected Sub EstablecerTextoCampo(ByRef rpt As ReportClass, nombre As String, ByVal texto As String)

            Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
            txtField = Nothing

            For Each a As Section In rpt.ReportDefinition.Sections
                For i As Integer = 0 To a.ReportObjects.Count - 1

                    Try
                        Debug.Print(a.ReportObjects.Item(i).GetType.Name)

                        If a.ReportObjects.Item(i).GetType.Name = "TextObject" Then
                            txtField = CType(a.ReportObjects.Item(i), CrystalDecisions.CrystalReports.Engine.TextObject)

                            Select Case a.ReportObjects.Item(i).Name
                                Case nombre
                                    txtField.Text = texto
                                    Exit Sub
                            End Select

                        End If

                    Catch
                        Exit For
                    End Try


                Next
            Next

        End Sub

        Protected Sub MostrarReporte(ByVal rpt As ReportClass, ByVal Titulo As String, Optional ByVal Parametros As ParameterFields = Nothing, Optional ByVal ds As DataSet = Nothing)

            Dim frmReporte As New frmReporte

            'Configuracion del Reporte
            frmReporte.CrystalReportViewer1.ReportSource = rpt
            frmReporte.dsTemp = ds
            frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None

            If rpt.DataDefinition.Groups.Count > 0 Then
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = True
            Else
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
            End If

            frmReporte.CrystalReportViewer1.AutoSize = True
            frmReporte.StartPosition = FormStartPosition.CenterScreen

            frmReporte.Location = New Point(10, 10)
            frmReporte.Size = New Size(frmPrincipal2.ClientSize.Width - 10, frmPrincipal2.ClientSize.Height - 10)
            frmReporte.WindowState = FormWindowState.Maximized
            frmReporte.Icon = frmPrincipal2.Icon

            If Not Parametros Is Nothing Then
                frmReporte.CrystalReportViewer1.ParameterFieldInfo = Parametros
            End If

            If Titulo = "" Then
                frmReporte.Text = VGSoftwareInfo
            Else
                frmReporte.Text = VGSoftwareNombre & " :: " & Titulo
            End If

            'frmReporte.ShowDialog()
            frmReporte.Show()

        End Sub

        Sub ArmarSubTitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox, ByVal txtDesde As ocxTXTDate, ByVal txtHasta As ocxTXTDate)

            SubTitulo = "Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " "

            'Establecemos los filtros
            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next

        End Sub

        Sub ArmarSubtitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox)

            'Establecemos los filtros
            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next
        End Sub

        Sub ArmarSubTitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox, ByVal txtDesde As ocxTXTDate, ByVal txtHasta As ocxTXTDate, ByVal cbxTipo As ocxCBX, ByVal ChkResumido As ocxCHK)

            Dim InformeResumido As String
            If ChkResumido.Valor = False Then
                InformeResumido = "DETALLADO"
            Else
                InformeResumido = "RESUMIDO POR DIA"
            End If

            'Establecemos los filtros
            SubTitulo = "Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " - Tipo: " & cbxTipo.Texto & " - Informe: " & InformeResumido & ""

            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next

        End Sub



        Sub ArmarSubTitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox, ByVal nudRanking As NumericUpDown, ByVal cbxVentas As ocxCBX, ByVal cbxMoneda As ocxCBX, ByVal cbxOrdenadoPor As ocxCBX, ByVal cbxEnForma As ocxCBX, ByVal txtDesde As ocxTXTDate, ByVal txtHasta As ocxTXTDate)

            SubTitulo = "Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " - Ranking: " & nudRanking.Value.ToString & " - " & cbxVentas.Texto & " - " & cbxMoneda.Texto & " - Ordenado por: " & cbxOrdenadoPor.Texto & "/" & cbxEnForma.Texto & ""

            'Establecemos los filtros
            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next

        End Sub

        Sub ArmarSubTitulo(ByRef SubTitulo As String, ByVal gbxFiltro As GroupBox, ByVal nudRanking As NumericUpDown, ByVal cbxMoneda As ocxCBX, ByVal cbxOrdenadoPor As ocxCBX, ByVal cbxEnForma As ocxCBX, ByVal txtDesde As ocxTXTDate, ByVal txtHasta As ocxTXTDate)

            SubTitulo = "Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " - Ranking: " & nudRanking.Value.ToString & " - " & cbxMoneda.Texto & " - Ordenado por: " & cbxOrdenadoPor.Texto & "/" & cbxEnForma.Texto & ""

            'Establecemos los filtros
            For Each ctr As Object In gbxFiltro.Controls

                If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                    ctr.EstablecerSubTitulo(SubTitulo)
                End If

            Next

        End Sub

        Public Sub GetPapersizeID(ByVal Reporte As CrystalDecisions.CrystalReports.Engine.ReportClass, ByVal PrinterName As String, ByVal PaperSizeName As String)

            Dim doctoprint As New System.Drawing.Printing.PrintDocument()
            Dim ppname As String = ""

            doctoprint.PrinterSettings.PrinterName = PrinterName

            If vgIDPaperSize >= 0 Then
                Reporte.PrintOptions.PaperSize = DirectCast(doctoprint.PrinterSettings.PaperSizes(vgIDPaperSize).RawKind, CrystalDecisions.Shared.PaperSize)
            Else
                For i As Integer = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1

                    Dim rawKind As Integer = 0
                    ppname = PaperSizeName

                    If doctoprint.PrinterSettings.PaperSizes(i).PaperName = ppname Then
                        Reporte.PrintOptions.PaperSize = DirectCast(doctoprint.PrinterSettings.PaperSizes(i).RawKind, CrystalDecisions.Shared.PaperSize)
                        vgIDPaperSize = i
                        Exit For
                    End If

                Next
            End If

        End Sub

    End Class

    Public Class CReporteVentas
        Inherits CReporte

        Dim DetalleVentaCampos As String = "IDTransaccion, IDProducto, Producto, ReferenciaProveedor, Proveedor, Descripcion, Linea, SubLinea, SubLinea2, TipoProducto, Marca, PesoUnitario, PorcentajeDescuento, UnidadMedida, Referencia, ControlarExistencia, Proveedor, Deposito, Cantidad, PrecioUnitario, PrecioNeto, Peso, IDCliente, Cliente, DireccionAlternativa, IDSucursalCliente, ReferenciaCliente, MesNumero, NroComprobante, Comprobante, Fecha, Condicion, Vendedor, Total, Bruto, TotalImpuesto, TotalDiscriminado, TotalDescuento, Descuento, TotalDescuentoDiscriminado, TotalPrecioNeto, TotalCosto, TotalCostoImpuesto, CantidadCaja, Cajas, TotalBruto, TotalSinDescuento, TotalSinImpuesto, TotalNeto, Utilidad, TipoCliente "

        Private Sub DescontarDevolucionesVenta(ByRef DS As DataSet, ByVal Where As String)

            Try

                'Obtenemos la tabla matriz
                Dim dt As DataTable = DS.Tables("VVenta")

                Dim CamposACambiar() As String = {"Total", "TotalImpuesto", "TotalDiscriminado", "TotalDescuento", "TotalBruto"}

                'Obtenemos las devoluciones
                Dim dtDevoluciones As DataTable = CSistema.ExecuteToDataTable("Select * from VVentaDevoluciones " & Where)

                'Recorremos la tabla de NC y restamos las devoluciones
                For Each DevolucionRow As DataRow In dtDevoluciones.Rows

                    Dim IDTransaccion As Integer = DevolucionRow("IDTransaccion")

                    Dim Filtro As String = " IDTransaccion=" & IDTransaccion

                    For Each VentaRow As DataRow In dt.Select(Filtro)
                        For c As Integer = 0 To CamposACambiar.GetLength(0) - 1
                            VentaRow(CamposACambiar(c)) = VentaRow(CamposACambiar(c)) - DevolucionRow(CamposACambiar(c))
                        Next
                    Next

                Next

            Catch ex As Exception
                MessageBox.Show("No se pudo aplicar las devoluciones! El sistema produjo un error. " & ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Sub

        Private Sub DescontarDevolucionesDetalle(ByRef DS As DataSet, ByVal Where As String, Optional ByVal vCampos() As String = Nothing)

            Try

                'Obtenemos la tabla matriz
                Dim dt As DataTable = DS.Tables("VDetalleVenta")

                Dim CamposACambiar() As String = {"Cantidad", "Total", "Bruto", "TotalImpuesto", "TotalDiscriminado", "TotalDescuento", "Descuento", "TotalPrecioNeto", "TotalCosto", "TotalCostoImpuesto", "TotalCostoDiscriminado", "CantidadCaja", "Cajas", "TotalBruto", "TotalSinDescuento", "TotalSinImpuesto", "TotalNeto", "TotalNetoConDescuentoNeto"}

                If vCampos IsNot Nothing Then
                    CamposACambiar = vCampos
                End If

                'Obtenemos las devoluciones
                Dim dtDevoluciones As DataTable = CSistema.ExecuteToDataTable("Select IDTransaccion, IDProducto, Cantidad, Total, Bruto, TotalImpuesto, TotalDiscriminado, TotalDescuento, Descuento, TotalPrecioNeto, TotalCosto, TotalCostoImpuesto, TotalCostoDiscriminado, CantidadCaja, Cajas, TotalBruto, TotalSinDescuento, TotalSinImpuesto, TotalNeto, TotalNetoConDescuentoNeto from VDetalleVentaDevoluciones " & Where & " Order By IDTransaccion, IDProducto")

                Debug.Print(CSistema.dtSumColumn(dtDevoluciones, "TotalDiscriminado"))
                'Recorremos la tabla de NC y restamos las devoluciones
                For Each DevolucionRow As DataRow In dtDevoluciones.Rows

                    Dim IDTransaccion As Integer = DevolucionRow("IDTransaccion")
                    Dim IDProducto As Integer = DevolucionRow("IDProducto")

                    Dim Filtro As String = " IDTransaccion=" & IDTransaccion & " And IDProducto=" & IDProducto

                    If dt.Select(Filtro).Count = 1 Then
                        For Each VentaRow As DataRow In dt.Select(Filtro)
                            For c As Integer = 0 To CamposACambiar.GetLength(0) - 1
                                VentaRow(CamposACambiar(c)) = VentaRow(CamposACambiar(c)) - DevolucionRow(CamposACambiar(c))
                            Next
                        Next
                    Else

                        'Pasamos el registro de devolucion a un registro temporal, en el cual acumularemos los saldos
                        Dim SaldoRow As DataRow = dtDevoluciones.NewRow
                        SaldoRow.ItemArray = DevolucionRow.ItemArray

                        'Recorremos el detalle de la venta
                        Dim v() As DataRow = dt.Select(Filtro)
                        For Each VentaRow As DataRow In v

                            'Recorremos campo por campo definido en la variable "CamposACambiar"
                            For c As Integer = 0 To CamposACambiar.GetLength(0) - 1

                                'Si el saldo acumulado ya es 0, no hace falta descontar nada,
                                'ya que en teoria ya no hay saldo en devoluciones
                                If SaldoRow(CamposACambiar(c)) <= 0 Then
                                    GoTo siguiente
                                End If

                                'Calculo interno, para saber la diferencia
                                Dim SaldoTemp As Decimal = SaldoRow(CamposACambiar(c)) - VentaRow(CamposACambiar(c))

                                'Si saldo es mayor a 0, entonces restar el saldo acumulado
                                'y se entiende que la venta es igual a 0
                                If SaldoTemp > 0 Then
                                    SaldoRow(CamposACambiar(c)) = SaldoRow(CamposACambiar(c)) - VentaRow(CamposACambiar(c))
                                    VentaRow(CamposACambiar(c)) = 0
                                Else
                                    'Es necesario guardar temporalmente el saldo acumulado en una variable "SaldoTemp"
                                    SaldoTemp = SaldoRow(CamposACambiar(c))
                                    'Actualizamos el saldo acumulado
                                    SaldoRow(CamposACambiar(c)) = SaldoRow(CamposACambiar(c)) - VentaRow(CamposACambiar(c))
                                    'Actualizamos la venta, con el saldo temporal guardado
                                    VentaRow(CamposACambiar(c)) = VentaRow(CamposACambiar(c)) - SaldoTemp
                                End If
siguiente:

                            Next
                        Next

                    End If
                Next

                'Por ultimo, sacamos todos los que quedaron en 0 en el detalle de venta
                dt = CData.FiltrarDataTable(dt, " Cantidad>0")
                DS.Tables.Remove("VDetalleVenta")
                DS.Tables.Add(dt)


            Catch ex As Exception
                MessageBox.Show("No se pudo aplicar las devoluciones! El sistema produjo un error. " & ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End Try

        End Sub

        Private Sub SepararSucursalClienteDetalle(ByRef DS As DataSet)

            'Obtenemos la tabla matriz
            Dim dt As DataTable = DS.Tables("VDetalleVenta")

            For Each oRow As DataRow In dt.Rows
                oRow("Cliente") = oRow("DireccionAlternativa") & "(" & oRow("ReferenciaCliente") & ")"
            Next

        End Sub

        Private Sub SepararSucursalClienteVenta(ByRef DS As DataSet)

            'Obtenemos la tabla matriz
            Dim dt As DataTable = DS.Tables("VVenta")

            For Each oRow As DataRow In dt.Rows
                oRow("Cliente") = oRow("DireccionAlternativa")
            Next

        End Sub

        Sub ListadoFacturasEmitidas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("Comando_1")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("Comando_1"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidas")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar el Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturasEmitidasDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & Where & " " & WhereDetalle, conn)
            Dim aTable3 As New SqlDataAdapter("Select D.* From VVenta V Join VDetalleImpuestoDesglosadoGravado D On V.IDTransaccion=D.IDTransaccion " & Where, conn)
            Dim aTable4 As New SqlDataAdapter("Select D.* From VVenta V Join VDetalleImpuestoDesglosado D On V.IDTransaccion=D.IDTransaccion " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturasEmitidasDetallada")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasProductoCliente(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True)

            'Aqui

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle & " Order By TipoProducto", conn)

            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()
                'aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasProductoCliente")

                'Aplicar Descuentos
                'If DescontarDevoluciones = True Then
                '    'DescontarDevolucionesVenta(DSDatos, Where)
                '    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                'End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                'aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ListadoVentasTotalesProducto(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String, ByVal DescontarDevoluciones As Boolean, ByVal Reporte As String, Optional ByVal SepararSucursalCliente As Boolean = False, Optional ByVal WhereDetalleNCR As String = "")

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleVenta"
            If DescontarDevoluciones = True Then
                vTabla = "VDetalleVentaConDevolucion"
            End If

            Dim aTable1 As New SqlDataAdapter("Select " & Top & " " & DetalleVentaCampos & " From " & vTabla & " " & WhereDetalle & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select " & Top & "  ReferenciaCliente, DireccionAlternativa, NroNCR, FechaNCR, Comprobante, FechaEmision, 'TotalBrutoNCR'=Sum(TotalBrutoNCR), 'TotalDescuentoDiscriminadoNCR'=Sum(TotalDescuentoDiscriminadoNCR), 'TotalDiscriminadoNCR'=Sum(TotalDiscriminadoNCR) From " & vTabla & " " & WhereDetalleNCR & " GROUP BY NroNCR, ReferenciaCliente, DireccionAlternativa, FechaNCR, Comprobante, FechaEmision Order By FechaNCR", conn)

            Try

                aTable1.SelectCommand.CommandTimeout = 120
                aTable2.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VDetalleVenta")
                DSDatos.Tables.Add("VDetalleVentaConDevolucion")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                If DescontarDevoluciones = True Then
                    aTable2.Fill(DSDatos.Tables("VDetalleVentaConDevolucion"))
                End If
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Reporte)

                ' Cargamos subreporte en el Crystal
                If Reporte = "rptVentasTotalesProducto" Then
                    Report.Subreports(0).SetDataSource(DSDatos.Tables("VDetalleVentaConDevolucion"))
                    Report.Subreports(0).SetDatabaseLogon(vgNombreUsuarioBD, vgPasswordBD)
                    Report.OpenSubreport(0)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Dim temp As New DataTable
                temp = (DSDatos.Tables(vTabla))


                'Calcular el total de NCR que no son del mes que afectan a facturas del mes indicado
                If Reporte = "rptVentasTotalesProducto" Then
                    Dim TotalDevolucion As Decimal = CSistema.dtSumColumn(temp, "TotalDiscriminadoNCR")
                    Dim TotalDiscriminado As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VDetalleVenta"), "TotalSinImpuesto")
                    Dim TotalDescuentoSinIVA As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VDetalleVenta"), "TotalDescuentoDiscriminado")
                    Dim Resultado As Decimal = TotalDevolucion + TotalDescuentoSinIVA + TotalDiscriminado

                    Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                    txtField = Nothing
                    txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalDescuento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                    txtField.Text = CSistema.FormatoNumero(TotalDevolucion.ToString, False)

                    txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtVentas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                    txtField.Text = CSistema.FormatoNumero(Resultado.ToString, False)

                    txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtDescuento"), CrystalDecisions.CrystalReports.Engine.TextObject)
                    txtField.Text = CSistema.FormatoNumero(TotalDescuentoSinIVA.ToString, False)

                    txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtNeto"), CrystalDecisions.CrystalReports.Engine.TextObject)
                    txtField.Text = CSistema.FormatoNumero(TotalDiscriminado.ToString, False)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesTipoProducto(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVenta " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()
                'aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesTipoProducto")

                'Aplicar Descuentos
                'If DescontarDevoluciones = True Then
                '    DescontarDevolucionesVenta(DSDatos, Where)
                '    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                'End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                'aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Reporte As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                Debug.Print(CSistema.dtSumColumn(DSDatos.Tables("VDetalleVenta"), "TotalDiscriminado"))


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, Reporte)

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesCliente")

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesTipoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesTipoCliente")

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaTotalesProductoCliente")

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDetalleVentasProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal DescontarDevoluciones As Boolean = True, Optional ByVal SepararSucursalCliente As Boolean = False)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleVenta " & WhereDetalle & "  " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDetalleVentaProductoCliente")

                'Descuento de Devoluciones
                If DescontarDevoluciones = True Then
                    DescontarDevolucionesDetalle(DSDatos, WhereDetalle)
                End If

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre) From VVentaTotalAnualCliente " & Where & " group by Cliente " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasAnualesTotalesCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalTipoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoCliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualTipoCliente " & Where & " group by TipoCliente" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualTipoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualTipoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasAnualesTotalesTipoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalClienteProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,Producto,Referencia,ReferenciaProducto,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualProductoCliente " & Where & " group by Cliente,Producto,Referencia,ReferenciaProducto" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualProductoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualProductoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoTotalAnualClienteProducto")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualTotalProductoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String, ByVal Del As String, ByVal Al As String, ByVal Moneda As String, ByVal EsCantidad As Boolean, ByVal DescontarDevoluciones As Boolean, ByVal SepararSucursalCliente As Boolean, ByVal SeleccionInforme As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleVenta"

            If DescontarDevoluciones = True Then
                vTabla = "VDetalleVentaConDevolucion"
            End If

            Dim aTable1 As New SqlDataAdapter("Select " & Top & " IDTransaccion, IDProducto, Producto, Cliente, DireccionAlternativa, ReferenciaCliente, MesNumero, Cantidad, Total, Bruto, TotalImpuesto, TotalDiscriminado, TotalDescuento, Descuento, TotalPrecioNeto, TotalCosto, TotalCostoImpuesto, TotalCostoDiscriminado, CantidadCaja, Cajas, TotalBruto, TotalSinDescuento, TotalSinImpuesto, TotalNeto, TotalNetoConDescuentoNeto, TipoCliente, IDListaPrecio From " & vTabla & " " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDetalleVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, SeleccionInforme)

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)
                If SepararSucursalCliente = True Then
                    SepararSucursalClienteDetalle(DSDatos)
                End If

                Report.SetDataSource(DSDatos)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim Cantidad As ParameterField = New ParameterField()
                Dim CantidadValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Cantidad.ParameterFieldName = "EsCantidad"
                CantidadValue.Value = EsCantidad

                Cantidad.CurrentValues.Add(CantidadValue)

                Parametros.Add(Cantidad)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub



        Sub ListadoVentasAnualTotalProductoClienteCantidad(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Venta As String, ByVal Del As String, ByVal Al As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,Producto,Referencia,ReferenciaProducto,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre)  From VVentaTotalAnualProductoClienteCantidad " & Where & " group by Cliente,Producto,Referencia,ReferenciaProducto" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVentaTotalAnualProductoCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaTotalAnualProductoCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoTotalAnualProductoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasAnualCantidadCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String, ByVal Venta As String, ByVal Moneda As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Enero'=SUM(Enero),'Febrero'=sum(Febrero),'Marzo'=SUM(Marzo),'Abril'=SUM(Abril),'Mayo'=SUM(Mayo),'Junio'=SUM(Junio),'Julio'=SUM(Julio),'Agosto'=SUM(Agosto),'Setiembre'=SUM(Setiembre),'Octubre'=SUM(Octubre),'Noviembre'=SUM(Noviembre),'Diciembre'=SUM(Diciembre) From VVentaCantidadAnualCliente " & Where & " group by Cliente " & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            Try

                DSDatos.Tables.Add("VVentaCantidadAnualCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentaCantidadAnualCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaAnualCantidadCliente")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Tipo Venta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtVenta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Venta

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoVentaMesTotal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Año As Integer)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select Mes,'Total'=Sum(Total) From VMesTotal Where Year(FechaEmision) = " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VMesTotal")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMesTotal"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoVentaMesTotal")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Año
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAño"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Año

               'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoRankingProductoRentable(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VProductoUtilidad Where " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VProductoUtilidad")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProductoUtilidad"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoProductoRentable")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Del
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                'Al
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

                Sub GraficoRankingClienteVentas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente,'Total' =(sum(Total)) From VVenta Where " & Where & " And Anulado = 'False' Group By Cliente Order By Total Desc", conn)

            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoRankingClientesVenta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub GraficoRankingProductoMasVendido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Producto,'Cantidad'=Sum(Cantidad) From VProductoMasVendido  Where " & Where & " Group By Producto Order By Cantidad Desc", conn)

            Try

                DSDatos.Tables.Add("VProductoMasVendido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProductoMasVendido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptGraficoProductoMasVendido")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Del
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                'Al
                txtField = CType(Report.ReportDefinition.Sections(5).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirFactura(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VVenta " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleVenta " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVenta")
                DSDatos.Tables.Add("VDetalleVenta")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                aTable2.Fill(DSDatos.Tables("VDetalleVenta"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                EstablecerConexion(Report)
                Report.SetDataSource(DSDatos)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total

                ' Asignamos el tamaño del papel
                GetPapersizeID(Report, Impresora, "Facturacion")
                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                ''Configuracion del Reporte
                'frmReporte.CrystalReportViewer1.ReportSource = Report
                'frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                'frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                'frmReporte.CrystalReportViewer1.AutoSize = True
                '
                'frmReporte.StartPosition = FormStartPosition.CenterScreen

                'frmReporte.Show()

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListadoVentasAnuladas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " *  From VVenta " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VVenta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVenta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentaAnulada")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DMSMinuta(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDMSMinuta " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDMSMinuta")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDMSMinuta"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDMSMinuta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVentasTotalesClienteProveedor(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String, ByVal DescontarDevoluciones As Boolean)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim vTabla As String = "VDetalleVenta"

            If DescontarDevoluciones = True Then
                vTabla = "VDetalleVentaConDevolucion"
            End If

            Dim sql As New SqlDataAdapter("Select " & Top & " ReferenciaCliente, Cliente, DireccionAlternativa, ReferenciaProveedor, Proveedor, COUNT(Producto) as Cantidad FROM(Select ReferenciaCliente, Cliente, DireccionAlternativa, ReferenciaProveedor, Proveedor, Producto From " & vTabla & " " & WhereDetalle & " GROUP By ReferenciaCliente, Cliente, DireccionAlternativa, ReferenciaProveedor, Proveedor, Producto) as T GROUP By ReferenciaCliente, Cliente, DireccionAlternativa, ReferenciaProveedor, Proveedor " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDetalleVenta")

                conn.Open()

                sql.Fill(DSDatos.Tables("VDetalleVenta"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVentasTotalesClienteProveedor")

                'Agrupar por Direccion Alternativa (Sucursal de Cliente)

                SepararSucursalClienteDetalle(DSDatos)

                Report.SetDataSource(DSDatos)

                Dim temp As New DataTable
                temp = (DSDatos.Tables("VDetalleVenta"))

                'Sumar el total SKU, CANTIDAD DE PRODUCTOS/CLIENTE
                Dim Suma As Decimal = CSistema.dtSumColumn(temp, "Cantidad")

                'Contar diferentes clientes
                Dim SumaClientes As Decimal = CSistema.dtCountDistinct(temp, "Cliente")

                Dim Promedio As Decimal = Suma / SumaClientes

                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                'sku promedio
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtTotalSKU"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(Suma.ToString, False)

                'Promedio SKU
                txtField = CType(Report.ReportDefinition.Sections("Section4").ReportObjects("txtPromedio"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(Promedio.ToString, True)

                Report.SetDataSource(DSDatos)

                'Informacion de Responsable
                EstablecerTitulos(Report, "", vgUsuario)

                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCompras
        Inherits CReporte

        Sub ListadoFacturasComprasEmitidasDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleCompra C " & Where & " And " & WhereDetalle & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select " & Top & " * From VCompra C " & Where & " " & OrderBy, conn)
            Dim aTable3 As New SqlDataAdapter("Select " & Top & " * From VCompra C Join VDetalleImpuestoDesglosado D On C.IDTransaccion=D.IDtransaccion " & Where & " " & OrderBy, conn)
            Dim aTable4 As New SqlDataAdapter("Select " & Top & " * From VCompra C Join VDetalleImpuestoDesglosadoGravado D On C.IDTransaccion=D.IDtransaccion " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleCompra")
                DSDatos.Tables.Add("VCompra")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleCompra"))
                aTable2.Fill(DSDatos.Tables("VCompra"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturaCompraDetallada")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


        Sub ListadoFacturasComprasEmitidas(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleCompra C " & Where & " And " & WhereDetalle & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleCompra")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleCompra"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoFacturaCompraResumida")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoFacturasComprasporProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * from  VDetalleCompra C " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleCompra")


                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleCompra"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumenComprasProductoporProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteNotaCredito
        Inherits CReporte

                Sub ImprimirNotaCredito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaCredito")

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

                Sub ImprimirNotaCreditoAplicar(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoNotaCreditoAplicar")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaCreditoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCredito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotacredito ", conn)

            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add(" VDetalleNotaCredito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables(" VDetalleNotaCredito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpNotaCreditoDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                'Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImpresionNotaCredito(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VNotaCredito " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaCredito " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)
            Dim aTable5 As New SqlDataAdapter("Select * From VDetalleImpuesto " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCredito")
                DSDatos.Tables.Add("VDetalleNotaCredito")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuesto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCredito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaCredito"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable5.Fill(DSDatos.Tables("VDetalleImpuesto"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

    End Class

    Public Class CReporteNotaDebito
        Inherits CReporte

        Sub ImprimirNotaDebito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebito")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaDebitoAplicar(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VNotaDebito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebitoAplicar")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                frmReporte.Show()

                'aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirNotaDebitoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            ' Dim aTable1 As New SqlDataAdapter("Select  * From VNotaCredito ", conn)
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebito " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaDebito ", conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos


            Try

                DSDatos.Tables.Add("VNotaDebito")
                DSDatos.Tables.Add(" VDetalleNotaDebito")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))
                aTable2.Fill(DSDatos.Tables(" VDetalleNotaDebito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoNotaDebitoDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                'Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                 'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImpresionNotaDebito(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef PathReporte As String, ByVal Impresora As String, ByVal NumeroALetra As String, ByVal Total As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VNotaDebito " & Where & "  ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleNotaDebito " & Where & "  Order By ID", conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosado " & Where & "  ", conn)
            Dim aTable4 As New SqlDataAdapter("Select * From VDetalleImpuestoDesglosadoGravado " & Where & "  ", conn)
            Dim aTable5 As New SqlDataAdapter("Select * From VDetalleImpuesto " & Where & "  ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebito")
                DSDatos.Tables.Add("VDetalleNotaDebito")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosado")
                DSDatos.Tables.Add("VDetalleImpuestoDesglosadoGravado")
                DSDatos.Tables.Add("VDetalleImpuesto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebito"))
                aTable2.Fill(DSDatos.Tables("VDetalleNotaDebito"))
                aTable3.Fill(DSDatos.Tables("VDetalleImpuestoDesglosado"))
                aTable4.Fill(DSDatos.Tables("VDetalleImpuestoDesglosadoGravado"))
                aTable5.Fill(DSDatos.Tables("VDetalleImpuesto"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = NumeroALetra & ".-"
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Total

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

    End Class

    Public Class CReporteStock
        Inherits CReporte

        Sub ExistenciaValorizadaDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, EsInformeActual As Boolean, Optional ByVal FechaHasta As Date = Nothing)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim strSQL As String
            If EsInformeActual Then
                Where = Where & " And E.Existencia > 0 "
                strSQL = "Select " & Top & " * From VExistenciaDeposito E" & Where & " " & OrderBy
            Else
                Where = Where & " And E.Existencia > 0 And E.CostoPromedio > 0"
                strSQL = "Select " & Top & _
                " E.IDSucursal,E.IDDeposito,E.Deposito,E.IDLinea,E.Linea" & _
      ",E.IDTipoProducto,E.TipoProducto,E.IDMarca,E.Marca,E.IDProducto,E.Referencia" & _
      ",E.Producto,ISNULL(AVG(E.CostoPromedio) ,0) CostoPromedio" & _
            ", dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" & _
      FechaHasta.AddDays(1) & "', '') AS Existencia, " & _
        "dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" & _
      FechaHasta.AddDays(1) & "', '') * AVG(E.CostoPromedio) TotalValorizadoCostoPromedio" & _
                " From VExistenciaDeposito E " & _
                " JOIN VExistenciaMovimientoCalculado VEMC on E.IDProducto=VEMC.IDProducto and E.IDDeposito=VEMC.IDDeposito " & _
                " AND VEMC.Fecha >= '1-1-1900' AND VEMC.Fecha <= '" & FechaHasta & "' " & _
                Where & _
                " GROUP BY E.IDSucursal,E.IDDeposito,E.Deposito,E.IDLinea,E.Linea,E.IDTipoProducto,E.TipoProducto," & _
                " E.IDMarca,E.Marca,E.IDProducto,E.Referencia,E.Producto " & OrderBy

            End If
            Dim aTable1 As New SqlDataAdapter(strSQL, conn)
            aTable1.SelectCommand.CommandTimeout = CONNECTION_TIMEOUT_120 'IMPORTANTE
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaValorizadaDetallado")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaValorizadaResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal EsInformeActual As Boolean, Optional ByVal FechaHasta As Date = Nothing)

            ' Creamos los componentes para obtener los datos.

            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoProducto,'Existencia'=SUM(Existencia),'Costo'=SUM(Costo),'CostoPromedio'=sum(CostoPromedio) From VExistenciaDeposito " & Where & " group by TipoProducto " & OrderBy, conn)
            Where = Where & " And E.Existencia > 0 "
            Dim str As String
            If EsInformeActual Then
                str = "SELECT " & Top & " TipoProducto, SUM(Existencia) Existencia, " & _
                    "SUM(TotalValorizadoCostoPromedio) TotalValorizadoCostoPromedio " & _
                    "FROM( Select E.TipoProducto, SUM(E.Existencia) as Existencia, " & _
                    "SUM(E.Existencia) * dbo.FCostoProducto(E.IDProducto) as TotalValorizadoCostoPromedio From VExistenciaDeposito E " & Where & " GROUP BY E.TipoProducto, E.IDProducto " & _
                    ") as T GROUP BY TipoProducto " & OrderBy
            Else
                str = "SELECT " & Top & _
                    " TipoProducto, SUM(Existencia) Existencia," & _
                        "SUM(TotalValorizadoCostoPromedio) AS TotalValorizadoCostoPromedio" & _
                        " FROM (" & _
                    "Select " & _
               " E.TipoProducto," & _
           "dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" & _
     FechaHasta.AddDays(1) & "', '') AS Existencia, " & _
     "dbo.FCostoProducto(E.IDProducto) * dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" & _
     FechaHasta.AddDays(1) & "', '') AS TotalValorizadoCostoPromedio " & _
               " From VExistenciaDeposito E " & _
               " JOIN VExistenciaMovimientoCalculado VEMC on E.IDProducto=VEMC.IDProducto and E.IDDeposito=VEMC.IDDeposito " & _
               " AND VEMC.Fecha >= '1-1-1900' AND VEMC.Fecha <= '" & FechaHasta & "' " & _
               Where & _
               " GROUP BY E.IDTipoProducto,E.TipoProducto,E.IDProducto, E.IDDeposito " & _
                " ) as T GROUP BY TipoProducto " & OrderBy

            End If
            Dim aTable1 As New SqlDataAdapter(str, conn)
            aTable1.SelectCommand.CommandTimeout = CONNECTION_TIMEOUT_120 'IMPORTANTE
            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaValorizadaResumen")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoProductoResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Producto As String, ByVal Subtitulo As String, Optional ByVal IDProducto As String = "0")

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            'Dim strSQL As String = "Select " & Top & "  Producto, Movimiento, Referencia,'Entrada'=sum(Entrada),'Salida'=sum(Salida) From VExtractoMovimientoProducto " & Where & " group by Referencia,Producto,Movimiento " & OrderBy
            Dim strSQL As String = String.Format("SELECT (SELECT TOP 1 Referencia FROM Producto WHERE ID={0}) Referencia, (SELECT TOP 1 Descripcion FROM Producto WHERE ID={1}) Producto, A.Movimiento, ISNULL(B.Entrada, 0) Entrada, ISNULL(B.Salida, 0) Salida FROM( SELECT Movimiento, 0 AS Entrada, 0 AS Salida FROM dbo.VExtractoMovimientoProducto GROUP BY Movimiento) AS A LEFT OUTER JOIN (Select   Producto, Referencia, Movimiento, 'Entrada'=sum(Entrada),'Salida'=sum(Salida) From VExtractoMovimientoProducto {2} group by Referencia,Producto,Movimiento) AS B ON A.Movimiento = B.Movimiento", _
                                    IDProducto, IDProducto, Where)
            Dim aTable2 As New SqlDataAdapter(strSQL, conn)
            strSQL = "Select " & Top & " T.Movimiento,'Salida'=IsNull((Select SUM(Salida) From VExtractoMovimientoProducto E " & Where & " And E.Movimiento=T.Movimiento ),0),'Entrada'=IsNull((Select SUM(Entrada) From VExtractoMovimientoProducto E " & Where & " And E.Movimiento=T.Movimiento),0) From VTipoExtractoMovimientoProducto T " & OrderBy
            Dim aTable1 As New SqlDataAdapter(strSQL, conn)

            Try

                DSDatos.Tables.Add("VExtractoMovimientoProducto")

                conn.Open()
                aTable2.Fill(DSDatos.Tables("VExtractoMovimientoProducto"))
                conn.Close()

                Dim Entradas As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VExtractoMovimientoProducto"), "Entrada")
                Dim Salidas As Decimal = CSistema.dtSumColumn(DSDatos.Tables("VExtractoMovimientoProducto"), "Salida")
                Dim ExitenciaFinal As Decimal = (Existencia + Entradas) - Salidas

                        ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoProductoResumen")
                Report.SetDataSource(DSDatos)

                        ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                        'Existencia Inicial
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtExistenciaInicial"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(Existencia)

                        'Existencia Final
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExistenciaFinal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoNumero(ExitenciaFinal)

                        'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                        'Establecer conexion
                EstablecerConexion(Report)

                        'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
                    End Try

        End Sub

        Sub MovimientoProductoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Existencia As String, ByVal Del As String, ByVal Al As String, ByVal Subtitulo As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * from VExtractoMovimientoProductoDetalle " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VExtractoMovimientoProductoDetalle")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExtractoMovimientoProductoDetalle"))
                conn.Close()

                Dim dt As DataTable = DSDatos.Tables("VExtractoMovimientoProductoDetalle")
                Dim i As Integer = 0
                Dim SaldoAnterior As Decimal = 0

                For Each oRow As DataRow In dt.Rows

                    If i = 0 Then
                        SaldoAnterior = Existencia
                    End If

                    oRow("Saldo") = (SaldoAnterior + oRow("Entrada")) - oRow("Salida")
                    SaldoAnterior = oRow("Saldo")

                    i = 1

                Next

                'Calculo para los totales
                Dim Compras As Decimal
                Dim Entrada As Decimal
                Dim Venta As Decimal
                Dim salida As Decimal

                For Each oRow As DataRow In dt.Rows
                    'Suma Compras + Entradas
                    If oRow("Tipo") = "COMPRA" Then
                        Compras = Compras + oRow("Entrada")
                    End If

                    If oRow("Tipo") = "ENTRADA" Then
                        Entrada = Entrada + oRow("Entrada")
                    End If

                    'Suma Venta + salida
                    If oRow("Tipo") = "VENTA" Then
                        Venta = Venta + oRow("Salida")
                    End If
                    If oRow("Tipo") = "SALIDA" Then
                        salida = salida + oRow("Salida")
                    End If

                Next

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoProductoDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSubtitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Subtitulo

                ''Existencia Anterior
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtExistenciaAnterior"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Existencia)

                ''Existencia Final
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExitFinal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(SaldoAnterior)

                ''Existencia Anterior
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtExInicial"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Existencia)

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalcompras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(CInt(Math.Round(Compras, 0)))

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalEntradas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(CInt(Math.Round(Entrada, 0)))

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalVentas"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(CInt(Math.Round(Venta, 0)))

                ''TotalCompra
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtTotalSalida"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(CInt(Math.Round(salida, 0)))

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub PlanillaTomaInventarioFisico(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VProducto " & Where & " " & OrderBy, conn)
            Try

                DSDatos.Tables.Add("VProducto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProducto"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaTomaInventario")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub PlanillaTomaInventarioFisicoPorDeposito(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoFiltro As String, ByVal Filtro As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExistenciaDeposito " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaTomaInventarioPorDeposito")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''TipoFiltro
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = TipoFiltro

                ''Filtro
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Filtro

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaBajoMinimoSobreMaximo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExistenciaDeposito " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VExistenciaDeposito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaDeposito"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaBajoStockMinimo")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ExistenciaMovimientoCalculado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal FechaDesde As Date = Nothing, Optional ByVal FechaHasta As Date = Nothing)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            ''Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VExistenciaMovimientoCalculado " & Where & " " & OrderBy, conn)
            Dim strSQL As String = "Select " & Top & _
                " E.IDSucursal,E.IDDeposito,E.Deposito,E.IDLinea,E.Linea" & _
      ",E.IDTipoProducto,E.TipoProducto,E.IDMarca,E.Marca,E.IDProducto,E.Referencia" & _
      ",E.Producto,ISNULL(AVG(E.Existencia) ,0) Existencia,ISNULL(SUM(VEMC.Compras) ,0) Compras," & _
    "ISNULL(SUM(VEMC.Entradas) ,0) Entradas,ISNULL(SUM(VEMC.Salidas) ,0) Salidas," & _
    "ISNULL(SUM(VEMC.Ventas) ,0) Ventas" & _
      ", dbo.FExtractoMovimientoProductoDeposito(E.IDProducto, E.IDDeposito, '" & _
      FechaDesde & "', '" & FechaHasta & "') AS ExistenciaAnterior" & _
                " From VExistenciaDeposito E " & _
                " LEFT JOIN VExistenciaMovimientoCalculado VEMC on E.IDProducto=VEMC.IDProducto and E.IDDeposito=VEMC.IDDeposito " & _
                " AND VEMC.Fecha >= '" & FechaDesde & "' AND VEMC.Fecha <= '" & FechaHasta & "' " & _
                Where & _
                " GROUP BY E.IDSucursal,E.IDDeposito,E.Deposito,E.IDLinea,E.Linea,E.IDTipoProducto,E.TipoProducto," & _
                " E.IDMarca,E.Marca,E.IDProducto,E.Referencia,E.Producto" & OrderBy

            Dim aTable1 As New SqlDataAdapter(strSQL, conn)

            Try

                DSDatos.Tables.Add("VExistenciaMovimientoCalculado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VExistenciaMovimientoCalculado"))
                conn.Close()


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExistenciaMovimientoCalculado")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                Dim fAnterior As Date = Date.Parse(FechaDesde)
                fAnterior = fAnterior.AddDays(-1)
                'Establecer campos
                EstablecerTextoCampo(Report, "txtCampo1", "Ex. al " & String.Format(fAnterior, "dd/MM/yyyy"))
                EstablecerTextoCampo(Report, "txtCampo2", "Ex. al " & FechaHasta)
                EstablecerTextoCampo(Report, "txtCampo3", "Ex. al " & String.Format(Date.Today, "dd/MM/yyyy"))
                EstablecerTextoCampo(Report, "txtCampo4", "Mov. del " & FechaDesde & " al " & FechaHasta)


                'Establecer conexion
                EstablecerConexion(Report)

              

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VProducto " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProducto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProducto"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoProducto")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPrecioProducto(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereListaPrecio As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal EsPrecio As Boolean, ByVal VerUnidad As Boolean, ByVal Observacion As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VProducto " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VProductoListaPrecio " & WhereListaPrecio, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProducto")
                DSDatos.Tables.Add("VProductoListaPrecio")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProducto"))
                aTable2.Fill(DSDatos.Tables("VProductoListaPrecio"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPrecioProducto")
                Report.SetDataSource(DSDatos)

                'Parametros
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim Unidad As ParameterField = New ParameterField()
                Dim Precio As ParameterField = New ParameterField()
                Dim UnidadValue As ParameterDiscreteValue = New ParameterDiscreteValue()
                Dim PrecioValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Unidad.ParameterFieldName = "ParametroUnidad"
                UnidadValue.Value = VerUnidad

                Precio.ParameterFieldName = "ParametroPrecio"
                PrecioValue.Value = EsPrecio

                Unidad.CurrentValues.Add(UnidadValue)
                Precio.CurrentValues.Add(PrecioValue)

                Parametros.Add(Unidad)
                Parametros.Add(Precio)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub AjusteControlInventario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VControlInventario Where IDTransaccion =" & Where, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion =" & Where, conn)
          
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAjusteControlInventario")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoMovimientoProducto(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VMovimiento " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoMovimientoProducto")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoMovimientoProductoDetallado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VMovimiento " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleMovimiento " & WhereDetalle & " ", conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")
                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))
                aTable2.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoMovimientoProductoDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCargaMercaderia(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim strSQL As String = "Select " & Top & " * From VCargaMercaderia " & Where & " " & OrderBy
            Dim aTable1 As New SqlDataAdapter(strSQL, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCargaMercaderia")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCargaMercaderiaDetallado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCargaMercaderia " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleCargaMercaderia " & WhereDetalle & " ", conn)

            Try

                DSDatos.Tables.Add("VCargaMercaderia")
                DSDatos.Tables.Add("VDetalleCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))
                aTable2.Fill(DSDatos.Tables("VDetalleCargaMercaderia"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCargaMercaderiaDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub CargaMercaderia(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCargaMercaderia Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleCargaMercaderia Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCargaMercaderia")
                DSDatos.Tables.Add("VDetalleCargaMercaderia")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCargaMercaderia"))
                aTable2.Fill(DSDatos.Tables("VDetalleCargaMercaderia"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCargaMercaderia")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub PlanillaInventario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaInventario")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub TomaInventario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptTomaInventario")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComparativoInventarioEquipo(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComparativoInventarioEquipo")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComparativoInventarioSistema(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal strfiltro As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            Dim aTable1 As New SqlDataAdapter("Select  * From VControlInventario Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VExistenciaDepositoInventario Where IDTransaccion=" & IDTransaccion & strfiltro, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VControlInventario")
                DSDatos.Tables.Add("VExistenciaDepositoInventario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VControlInventario"))
                aTable2.Fill(DSDatos.Tables("VExistenciaDepositoInventario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComparativoInventarioSistema")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoStock(ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VMovimiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VDetalleMovimiento Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VMovimiento")
                DSDatos.Tables.Add("VDetalleMovimiento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VMovimiento"))
                aTable2.Fill(DSDatos.Tables("VDetalleMovimiento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientos")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteLotes
        Inherits CReporte

        Sub ListadoLotesEmitidos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteEmitido")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoLotesEmitidosDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VVentaLoteDistribucion " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteEmitidoDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDevolucionLotes(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal Del As String, ByVal Al As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleLoteDistribucion " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoLoteDetalleDevolucion")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Rango de Fechas
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Del

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Al

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoComprobantesSinLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VVentasSinlotes " & Where & " and IDTransaccion not in(select IDTransaccion from VLoteDistribucion) " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VVentasSinlotes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVentasSinlotes"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoComprobanteSinLote")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDetalleComprobantesSinLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " and IDTransaccion not in(select IDTransaccion from VLoteDistribucion)" & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VVentaLoteDistribucion " & WhereDetalle, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDetalleCompranteSinLote")
                Report.SetDataSource(DSDatos)

                 'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub HojaRuta(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " Order By Comprobante", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VVentaLoteDistribucion " & WhereDetalle & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VVentaLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptHojaRuta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub CargaPlanilla(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VLoteDistribucion " & Where & " " & OrderBy, conn)
            'Dim aTable1 As New SqlDataAdapter("Select * From VVenta ", conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleLoteDistribucion " & WhereDetalle, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLoteDistribucion")
                DSDatos.Tables.Add("VDetalleLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLoteDistribucion"))
                aTable2.Fill(DSDatos.Tables("VDetalleLoteDistribucion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptCargaPlanilla")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDevolucionLote(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VDevolucionLote " & Where, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleDevolucionLote" & Where, conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDevolucionLote")
                DSDatos.Tables.Add("VDetalleDevolucionLote")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDevolucionLote"))
                aTable2.Fill(DSDatos.Tables("VDetalleDevolucionLote"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDevolucionLotes")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoRendicionCobranza(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VRendicionLote " & Where, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRendicionLote")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRendicionLote"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptRendicionCobranza")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ComposiciondeLotesparaDistribucion(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From vVentaLoteDistribucion " & Where & " " & OrderBy, conn)



            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("vVentaLoteDistribucion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("vVentaLoteDistribucion"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComposicionLotesparaDistribucion")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCheque
        Inherits CReporte

        Sub ImprimirCheque(ByRef frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal IDCuentaBancaria As Integer, ByVal Diferido As Boolean, ByVal Impresora As String, ByVal Cadena1 As String, ByVal Cadena2 As String, ByVal OP As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCheque")
                conn.Open()

                aTable1.Fill(DSDatos.Tables("VCheque"))
                conn.Close()

                Dim PathReporte As String = ""
                Dim dtCuentaContable As DataTable = CData.GetTable("VCuentaBancaria", " ID= " & IDCuentaBancaria).Copy

                If dtCuentaContable Is Nothing Then
                    'Mostrar error
                End If

                If dtCuentaContable.Rows.Count = 0 Then
                    'Mostrar error
                End If

                Dim oRow As DataRow = dtCuentaContable.Rows(0)

                If Diferido = True Then
                    PathReporte = oRow("FormatoDiferido").ToString
                Else
                    PathReporte = oRow("FormatoAlDia").ToString
                End If

                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    'Mostrar error
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtImporte1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Cadena1

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtImporte2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Cadena2

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOP"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = OP

                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListarChequeCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Tipo As String, ByVal Estado As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From Vchequecliente " & Where & "" & Tipo & "" & Estado & "" & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequeCliente")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequeCliente"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoCheque")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirListadoCanje(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VPagoChequeCliente " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePagoChequeCliente " & WhereDetalle, conn)
            Dim aTable3 As New SqlDataAdapter("Select * From VFormaPago ", conn)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Debug.Print(aTable2.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VPagoChequeCliente")
                DSDatos.Tables.Add("VDetallePagoChequeCliente")
                DSDatos.Tables.Add("VFormaPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPagoChequeCliente"))
                aTable2.Fill(DSDatos.Tables("VDetallePagoChequeCliente"))
                aTable3.Fill(DSDatos.Tables("VFormaPago"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListadoCanjes")
                Report.SetDataSource(DSDatos)

               'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListarChequesDiferidosDepositados(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Condicion As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VChequesDiferidosDepositados " & Where & Condicion & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequesDiferidosDepositados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequesDiferidosDepositados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpChequesDiferidosDepositados")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListarChequeClienteDiferido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Condicion As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VchequeClienteVenta " & Where & " And Diferido='True' And Cartera='True'" & Condicion & OrderBy, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VChequeClienteVenta")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VChequeClienteVenta"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptIngresoChequesDiferidos")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub SaldoChequesDiferidos(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewChequeClienteDiferidoMovimiento2 " & Where)
            dt.TableName = "SpViewChequeClienteDiferidoMovimiento2;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpSaldoChequesDiferido")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Condicion
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                ' ''Sucursal
                'txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSucursal"), CrystalDecisions.CrystalReports.Engine.TextObject)
                'txtField.Text = Sucursal

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DescuentoChequeDiferido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Responsable As String, ByVal Titulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VDetalleDescuentoCheque " & Where, conn)

            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDescuentoCheque")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDescuentoCheque"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDescuentoCheque")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirEfectivizacion(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal Titulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VEfectivizacion Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetalleEfectivizacion Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VEfectivizacion")
                DSDatos.Tables.Add("VDetalleEfectivizacion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VEfectivizacion"))
                aTable2.Fill(DSDatos.Tables("VDetalleEfectivizacion"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptEfectivizacion")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCuentasACobrar
        Inherits CReporte

        Sub InventarioDocumentoPendiente(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendiente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteVencimientoCliente(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteVencimientoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteCobradorFechaVencimiento(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteCobradorFechaVencimiento")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteVendedorFechaVencimiento(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteVendedorFechaVencimiento")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendientePorComprobante(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientesLote  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientesLote")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesLote"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendientePorComprobante")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub InventarioDocumentoPendienteDireccionAlternativa(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VInventarioDocumentosPendientes  " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                aTable1.SelectCommand.CommandTimeout = 120

                DSDatos.Tables.Add("VInventarioDocumentosPendientes")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientes"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendienteDireccionAlternativa")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldo(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal Fecha3 As String, ByVal Fecha4 As String, ByVal Fecha5 As String, ByVal Fecha6 As String, ByVal Fecha7 As String, ByVal Fecha8 As String, ByVal Fecha9 As String, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Dia7 As String, ByVal Dia8 As String, ByVal Dia9 As String, ByVal Refencia As String, ByVal Moneda As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDistribucionSaldo  " & Where & "  " & Cond & "" & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDistribucionSaldo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDistribucionSaldo"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldo")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6
                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha7

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha8

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha9

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia7

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia8

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9

                'Dia
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txt10"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenMovimientoSaldo(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal TipoInforme As String, Optional ByVal ASC As Boolean = True)

            'Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldoCobro " & Where)

            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            'Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumendeMovimientoSaldo")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenMovimientoSaldoDetallado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal TipoInforme As String, Optional ByVal ASC As Boolean = True)

            'Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumeMovimientosSaldoCobroDetallado " & Where)

            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            'Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpResumenMovimientoSaldoDetallado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Fecha
                ''Sistema
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                'Informacion de Software
                EstablecerTitulos(Report, "", TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldoCobro(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Fecha1 As String, ByVal Fecha2 As String, ByVal Fecha3 As String, ByVal Fecha4 As String, ByVal Fecha5 As String, ByVal Fecha6 As String, ByVal Fecha7 As String, ByVal Fecha8 As String, ByVal Fecha9 As String, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Dia7 As String, ByVal Dia8 As String, ByVal Dia9 As String, ByVal Referencia As String, ByVal Moneda As String, Optional ByVal ASC As Boolean = True, Optional ByVal IDSucursal As Integer = 0)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            'Dim aTable1 As New SqlDataAdapter("Execute SPDistribucionPago " & Where, conn)

            Dim Scrip As String

            If IDSucursal = 0 Then
                Scrip = " SpViewDistribucionSaldoCobro "
            Else
                Scrip = " SpViewDistribucionSaldoCobroSucursal "
                Where = Where & "," & IDSucursal
            End If

            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute " & Scrip & Where)

            If dt Is Nothing Then
                Exit Sub
            End If

            'El nombre no se debe cambiar aunque sea otro SP.
            dt.TableName = "SpViewDistribucionSaldoCobro;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldo")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Tipo Informe
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6
                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha7

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha8

                ''Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha9

                'Dia
                Dia1 = Dia1 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                'Dia
                Dia2 = Dia2 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                'Dia
                Dia3 = Dia3 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                'Dia
                Dia4 = Dia4 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                'Dia
                Dia5 = Dia5 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                'Dia
                Dia6 = Dia6 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                'Dia
                Dia7 = Dia7 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia7"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia7

                'Dia
                Dia8 = Dia8 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia8"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia8

                'Dia
                Dia9 = Dia9 + " Días"
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia9"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia9

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub


    End Class

    Public Class CReporteCuentasAPagar
        Inherits CReporte

        Sub InventarioDocumentoPendientePago(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Cond As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " [Cod.], Comprobante, Condicion, Numero, Origen, Emision, FechaVencimiento, Plazo, Moneda, Total, Saldo, Proveedor, Referencia From VInventarioDocumentosPendientesPago  " & Where & "  " & Cond & "" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VInventarioDocumentosPendientesPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VInventarioDocumentosPendientesPago"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptInventarioDocumentoPendientePago")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DistribucionSaldosPago(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Fecha1 As Date, ByVal Fecha2 As Date, ByVal Fecha3 As Date, ByVal Fecha4 As Date, ByVal Fecha5 As Date, ByVal Fecha6 As Date, ByVal Dia1 As String, ByVal Dia2 As String, ByVal Dia3 As String, ByVal Dia4 As String, ByVal Dia5 As String, ByVal Dia6 As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, Optional ByVal ASC As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewDistribucionSaldoPago " & Where)

            'El nombre no se debe cambiar aunque sea otro SP.
            dt.TableName = "SpViewDistribucionSaldoPago;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, ASC)

            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDistribucionSaldoPago")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Dia1 y Fecha1
                Dia1 = Dia1 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia1

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha1"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                'Dia2 y Fecha2
                Dia2 = Dia2 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia2

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha2"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                'Dia3 y Fecha3
                Dia3 = Dia3 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia3

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha3"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha3

                'Dia4 y Fecha4
                Dia4 = Dia4 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia4

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha4"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha4

                'Dia5 y Fecha5
                Dia5 = Dia5 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia5

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha5"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha5

                'Dia6 y Fecha6
                Dia6 = Dia6 + "Días"

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDia6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Dia6

                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha6"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha6

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

         Sub DocumentoPagado(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDocumentoPagado" & Where & "" & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDocumentoPagado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDocumentoPagado"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDocumentoPagado")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''TipoInforme
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTipoInforme"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = TipoInforme


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoSaldoPagoResumen(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal WhereDetalle As String, ByVal Fecha As Date, ByVal Moneda As String, ByVal OrdernadoPor As String, ByVal OrderBy As String, ByVal Top As String, ByVal Orden As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldosPago " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, Orden)

            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoSaldoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Vencido
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda

                'Ordenado por 
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenado"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = OrdernadoPor

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub MovimientoSaldoPagoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal Responsable As String, ByVal WhereDetalle As String, ByVal Fecha1 As Date, ByVal Fecha2 As Date, ByVal Moneda As String, ByVal OrdernadoPor As String, ByVal OrderBy As String, ByVal Top As String, ByVal Orden As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewResumenMovimientosSaldosPagoDetalle " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, OrderBy, Orden)

            Try


                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptMovimientoSaldoProveedorDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Título
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Fecha Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDel"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha1

                'Fecha Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtAl"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha2

                'Moneda
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMoneda"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Moneda

                'Ordenado por 
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = OrdernadoPor

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteFondoFijo
        Inherits CReporte

        Sub Rendicion(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal Responsable As String, ByVal Reponer As Integer, ByVal Saldo As Integer, ByVal Tope As Integer, ByVal Fecha As Date)

            ' Creamos los componentes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                conn.Open()
                dt.TableName = "VDetalleFondoFijo"
                DSDatos.Tables.Add(dt)
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComprobanteReponer")
                Report.SetDataSource(DSDatos)

                ' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Titulo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTitulo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Titulo

                'Reponer
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtReponer"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Reponer)

                'Saldo
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtSaldo"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Saldo)

                'Tope
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtTope"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = CSistema.FormatoMoneda(Tope)


                'Fecha
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtFecha"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Fecha

               'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoVales(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VVale " & Where, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VVale")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VVale"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoVale")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteBanco
        Inherits CReporte

        Sub ExtractoMovimientoBancario(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal WhereDetalle As String, ByVal Ordenado As String, Optional ByVal ASC As Boolean = False)

            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewExtractoMovimientoBancario2 " & Where)
            dt.TableName = "SpViewExtractoMovimientoBancario2;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            CData.OrderDataTable(dt, Ordenado, Asc)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoBancario")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub SaldoBanco()

            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewSaldoBancario")
            dt.TableName = "SpViewSaldoBancario;1"

            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptSaldoBanco")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDepositoBancario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDepositoBancario " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDepositoBancario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDepositoBancario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDepositoBancario")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDepositoBancarioDetalle(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDepositoBancarioDetalle " & Where & " Order By Fecha Asc " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDepositoBancarioDetalle")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDepositoBancarioDetalle"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDepositoBancarioDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()
            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoDebitoCreditoBancario(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDebitoCreditoBancario " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDebitoCreditoBancario")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDebitoCreditoBancario"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoDebitoCreditoBancario")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CCliente
        Inherits CReporte

        Sub ExtractoMovimientoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewExtractoMovimientoCliente " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                dt.TableName = "SpViewExtractoMovimientoCliente;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoCliente")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCliente(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCliente " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCliente")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Fecha Inicio
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Orden

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoClienteNoCompra(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VCliente " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try
                Debug.Print(aTable1.SelectCommand.CommandText)

                DSDatos.Tables.Add("VCliente")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCliente"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoClienteNoCompra")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CProveedor
        Inherits CReporte

        Sub ExtractoMovimientoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewExtractoMovimientoProveedor " & Where)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            Try

                dt.TableName = "SpViewExtractoMovimientoProveedor;1"
                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptExtractoMovimientoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub

        Sub ListadoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal Titulo As String, ByVal OrderBy As String, ByVal Orden As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VProveedor " & Where & " " & OrderBy, conn)
            Debug.Print(aTable1.SelectCommand.CommandText)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VProveedor")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VProveedor"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoProveedor")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Fecha Inicio
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtOrdenadoPor"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Orden

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteAsiento
        Inherits CReporte

        Sub AsientoDiario(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal Responsable As String, ByVal VerDiferencia As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptAsientoDiario")
                Report.SetDataSource(DSDatos)

                'Parametros
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim Diferencia As ParameterField = New ParameterField()
                Dim DiferenciaValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                Diferencia.ParameterFieldName = "Diferencia"
                DiferenciaValue.Value = VerDiferencia
                Diferencia.CurrentValues.Add(DiferenciaValue)

                Parametros.Add(Diferencia)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DocumentoSinAsiento(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Titulo As String, ByVal SubTitulo As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VDocumentosSinAsientos " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDocumentosSinAsientos")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDocumentosSinAsientos"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDocumentoSinAsiento")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReportePedido
        Inherits CReporte

        Sub ImprimirPedido(ByRef frmReporte As frmReporte, ByRef Where As String, ByRef Impresora As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VPedido " & Where & "  ", conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPedido")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))

                conn.Close()
                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos

                'If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                '    MsgBox("El archivo no existe!")
                '    Exit Sub
                'End If

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpPedidos")
                Report.SetDataSource(DSDatos)

                'Dim Report As New ReportClass
                'Report.FileName = rptPedidos
                'Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)



                'Configuracion del Reporte
                frmReporte.CrystalReportViewer1.ReportSource = Report
                frmReporte.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                frmReporte.CrystalReportViewer1.ShowGroupTreeButton = False
                frmReporte.CrystalReportViewer1.AutoSize = True

                frmReporte.StartPosition = FormStartPosition.CenterScreen
                EstablecerConexion(Report)

                frmReporte.Show()


            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try
        End Sub
    End Class

    Public Class CReporteOrdenPago
        Inherits CReporte

        Sub ImprimirOrdenPago(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal Responsable As String, ByVal MontoLetra As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VOrdenPago Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VCheque Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable3 As New SqlDataAdapter("Select  * From VOrdenPagoEgreso Where IDTransaccionOrdenPago=" & IDTransaccion, conn)
            Dim aTable4 As New SqlDataAdapter("Select  * From VDetalleAsiento Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable5 As New SqlDataAdapter("Select SUM(Importe)as Importe from VOrdenPagoEfectivo Where IDTransaccionOrdenPago=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenPago")
                DSDatos.Tables.Add("VCheque")
                DSDatos.Tables.Add("VOrdenPagoEgreso")
                DSDatos.Tables.Add("VDetalleAsiento")
                DSDatos.Tables.Add("VOrdenPagoEfectivo")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPago"))
                aTable2.Fill(DSDatos.Tables("VCheque"))
                aTable3.Fill(DSDatos.Tables("VOrdenPagoEgreso"))
                aTable4.Fill(DSDatos.Tables("VDetalleAsiento"))
                aTable5.Fill(DSDatos.Tables("VOrdenPagoEfectivo"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptOrdenPago")
                Report.SetDataSource(DSDatos)

                ''Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Monto Letra
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = MontoLetra

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()
                aTable3.Dispose()
                aTable4.Dispose()
                aTable5.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirRetencionIVA(ByVal frmReporte As frmReporte, ByVal IDTransaccion As Integer, ByVal IDTransaccionOrdenPago As Integer, ByVal Responsable As String, ByVal MontoLetra As String, ByRef PathReporte As String, ByVal Impresora As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VRetencionIVA Where IDTransaccion=" & IDTransaccion, conn)
            Dim aTable2 As New SqlDataAdapter("Select  * From VComprobanteRetencion Where IDTransaccion=" & IDTransaccion, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VRetencionIVA")
                DSDatos.Tables.Add("VComprobanteRetencion")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VRetencionIVA"))
                aTable2.Fill(DSDatos.Tables("VComprobanteRetencion"))
                conn.Close()

                ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
                If FileIO.FileSystem.FileExists(VGCarpetaAplicacion & "\" & PathReporte) = False Then
                    MsgBox("El archivo no existe!")
                    Exit Sub
                End If

                Dim Report As New ReportClass
                Report.FileName = VGCarpetaAplicacion & "\" & PathReporte
                Report.SetDataSource(DSDatos)
                EstablecerConexion(Report)

                '' Total Letras 
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing
                txtField = CType(Report.ReportDefinition.Sections(4).ReportObjects("txtMontoLetras"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = MontoLetra


                Report.PrintOptions.PrinterName = Impresora
                Report.PrintToPrinter(1, False, 1, 1)


                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoOrdenPago(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VOrdenPago " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VOrdenPago")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VOrdenPago"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoOrdenPago")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteContabilidad
        Inherits CReporte

        Sub ImprimirPlanCuenta(ByVal frmReporte As frmReporte, ByVal Responsable As String, ByVal IDPlanCuenta As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VCuentaContableReporte Where IDPlanCuenta=" & IDPlanCuenta & " Order By Codigo", conn)

            Try

                DSDatos.Tables.Add("VCuentaContable")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCuentaContable"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanCuenta")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroIVA(ByVal frmReporte As frmReporte, ByVal WhereDetalle As String, ByVal Where As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal OrderBy As String, ByVal Top As String, Optional ByVal Resumido As Boolean = False)

            Dim SQL As String = ""

            If Resumido = False Then
                SQL = "Select " & Top & " * From VLibroIVA " & WhereDetalle & Where & " " & OrderBy
            End If

            If Resumido = True Then
                SQL = "Select " & Top & " * From VLibroIVA " & WhereDetalle & Where & " " & OrderBy
            End If

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter(SQL, conn)



            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VLibroIVA")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VLibroIVA"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If Resumido = False Then
                    LoadReport(Report, "rptLibroIVA3")
                Else
                    LoadReport(Report, "rptLibroIVAAgrupado")
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroMayor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Resumido As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayor " & Where)
            dt.TableName = "SpViewLibroMayor;1"
            dt = CData.FiltrarDataTable(dt, WhereDetalle)
            DSDatos.Tables.Add(dt)

            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If Resumido = False Then
                    LoadReport(Report, "rptLibroMayor")
                Else
                    LoadReport(Report, "rptLibroMayorResumido")
                End If

                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirLibroMayorSucursal(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal IDSucursal As Integer, ByVal Resumido As Boolean)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpViewLibroMayorSucursal " & Where & "," & IDSucursal)
            dt = CData.FiltrarDataTable(dt, WhereDetalle)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                dt.TableName = "SpViewLibroMayor;1"

                DSDatos.Tables.Add(dt)

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                If Resumido = False Then
                    LoadReport(Report, "rptLibroMayor")
                Else
                    LoadReport(Report, "rptLibroMayorResumido")
                End If
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceGeneral(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String, Optional ByVal vMostrarCodigo As Boolean = True, Optional ByVal Multicolumna As Boolean = False)

            Try

                Dim Report As New ReportClass
                If Multicolumna = False Then
                    LoadReport(Report, "rptBalanceGeneral")
                Else
                    LoadReport(Report, "rptEstadoResultadoMulticolumna")
                End If

                Report.SetDataSource(dt)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Parametro para definir si el informe EsCantidad o importe
                Dim Parametros As ParameterFields = New ParameterFields()
                Dim MostrarCodigo As ParameterField = New ParameterField()
                Dim MostrarCodigoValue As ParameterDiscreteValue = New ParameterDiscreteValue()

                MostrarCodigo.ParameterFieldName = "MostrarCodigo"
                MostrarCodigoValue.Value = vMostrarCodigo

                MostrarCodigo.CurrentValues.Add(MostrarCodigoValue)

                Parametros.Add(MostrarCodigo)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Parametros)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceComprobacion4(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceComprobacion4")
                Report.SetDataSource(dt)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirBalanceComprobacion3(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptBalanceComprobacion3")
                Report.SetDataSource(dt)

               'Informacion de Software
                EstablecerTitulos(Report, Titulo, SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ImprimirComprobanteNoEmitido(ByVal frmReporte As frmReporte, ByVal dt As DataTable, ByVal Titulo As String, ByVal SubTitulo As String, ByVal Responsable As String)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComprobantesNoEmitidos")
                Report.SetDataSource(dt)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CPedido
        Inherits CReporte
        Sub ListadoPedido(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VPedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rtpListaPedido")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPedidoDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select  * From VPedido " & Where & " " & OrderBy, conn)
            Dim aTable2 As New SqlDataAdapter("Select * From VDetallePedido " & Where, conn)

            Try

                DSDatos.Tables.Add("VPedido")
                DSDatos.Tables.Add("VDetallePedido")
                conn.Open()
                aTable1.Fill(DSDatos.Tables("VPedido"))
                aTable2.Fill(DSDatos.Tables("VDetallePedido"))
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoPedidoDetalle")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()
                aTable2.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteNotaCreditoProveedor
        Inherits CReporte

        Sub ImprimirNotaCreditoProveedor(ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Responsable As String, ByVal Top As String)

            Dim frmReporte As New frmReporte

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaCreditoProveedor " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaCreditoProveedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaCreditoProveedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaCreditoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteNotaDebitoProveedor
        Inherits CReporte

        Sub ImprimirNotaDebitoProveedor(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal WhereDetalle As String, ByVal Titulo As String, ByVal TipoInforme As String, ByVal OrderBy As String, ByVal Top As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VNotaDebitoProveedor " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VNotaDebitoProveedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VNotaDebitoProveedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoNotaDebitoProveedor")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteCobranza
        Inherits CReporte

        Sub ListadoCobranzaCredito(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal WhereDetalle As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCobranzaCredito " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCobranzaCredito")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCobranzaCredito"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCobranzaCredito")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

               ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoCobranzaContado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Desde As String, ByVal Hasta As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VCobranzaContado " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VCobranzaContado")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VCobranzaContado"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoCobranzaContado")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                ''Desde
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtDesde"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Desde

                ''Hasta
                txtField = CType(Report.ReportDefinition.Sections(2).ReportObjects("txtHasta"), CrystalDecisions.CrystalReports.Engine.TextObject)
                txtField.Text = Hasta

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleDocumentosCobrados(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleDocumentosCobrados " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDocumentosCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDocumentosCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleDocumentoCobrado")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleDocumentosCobradosFecha(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From VDetalleDocumentosCobrados " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VDetalleDocumentosCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleDocumentosCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleDocumentoCobradoFecha")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenCobranzaDiaria(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal WhereDetalle As String, Optional ByVal ASC As Boolean = True)

            ' Creamos los componenetes para obtener los datos.
            Dim DSDatos As New DataSet
            Dim dt2 As DataTable = CSistema.ExecuteToDataTable("Select Fecha,TipoComprobante,'Cantidad'=SUM(Cantidad),'Valores'=SUM(Valores)From VResumenCobranzaDiariaFormaPago2 " & Where & " Group By Fecha,TipoComprobante")

            dt2.TableName = "VResumenCobranzaDiariaFormaPago2"

            DSDatos.Tables.Add(dt2)


            Try

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumenCobranzaDiaria2")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleValoresCobrados(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleValoresCobrados " & Where & " " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VDetalleValoresCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleValoresCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleValoresCobrados")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub DetalleValoresCobradosPorComprobante(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal TipoInforme As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleValoresCobrados " & Where & " " & OrderBy, conn)
           
            Try

                DSDatos.Tables.Add("VDetalleValoresCobrados")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleValoresCobrados"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptDetalleValoresCobradosPorComprobante")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ResumenCobranzaContado(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal dt As DataTable)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim Contador As Integer = 0


            For Each oRow As DataRow In dt.Rows
                Contador = Contador + 1
                If Contador = dt.Rows.Count Then
                    dt.Rows.Remove(oRow)
                    Exit For
                End If
            Next

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                conn.Open()
                dt.TableName = "VResumenCobranzaContado2"
                DSDatos.Tables.Add(dt)
                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptResumenCobranzaContado")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo)

                'aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteGasto
        Inherits CReporte

        Sub ListadoGasto(ByVal frmReporte As frmReporte, ByVal Titulo As String, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal TipoInforme As String, ByVal Tabla As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " * From " & Tabla & " " & Where & " " & OrderBy, conn)

            ' Utilizamos un TryCatch para capturar un error y asi no pueda afectar a la Base de datos
            Try

                DSDatos.Tables.Add("VGasto")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VGasto"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptListadoGasto")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, Titulo, Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub
    End Class

    Public Class CReporteComisionVendedor
        Inherits CReporte

        Sub ListadoComision(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Titulo As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " Cliente, TipoProducto, IDTipoProducto, Producto, Vendedor, IDVendedor, CantidadVenta, CantidadDevuelta, ImporteVenta, ImporteDevuelto, Monto From VComisionVendedor" & Where & OrderBy, conn)


            Try

                DSDatos.Tables.Add("VComisionVendedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VComisionVendedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComisionVendedor")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing

                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoComisionDetalle(ByVal frmReporte As frmReporte, ByVal Where As String, ByVal OrderBy As String, ByVal Top As String, ByVal Responsable As String, ByVal Titulo As String, ByVal TipoInforme As String)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select " & Top & " TipoProducto,Cliente,ReferenciaCliente,Producto,Vendedor,IDVendedor,'CantidadVenta'=Sum(CantidadVenta),'CantidadDevuelta'=SUM(CantidadDevuelta),'ImporteVenta'=SUM(ImporteVenta),'ImporteDevuelto'=sum(ImporteDevuelto),'Monto'=sum(Monto)From VComisionVendedor" & Where & " Group By TipoProducto,Cliente,ReferenciaCliente,Producto,Vendedor,IDVendedor " & OrderBy, conn)

            Try

                DSDatos.Tables.Add("VComisionVendedor")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VComisionVendedor"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptComisionVendedorDetalle")
                Report.SetDataSource(DSDatos)

                '' Informacion del Sistema
                Dim txtField As CrystalDecisions.CrystalReports.Engine.TextObject
                txtField = Nothing


                'Informacion de Software
                EstablecerTitulos(Report, Titulo, TipoInforme)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

    Public Class CReporteDescuentoTactico
        Inherits CReporte

        Sub ListadoProductoDetallado(ByVal Detallado As Boolean, ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select * From VDetalleActividadDescuento Where IDTransaccion=" & IDTransaccion & " Order By FechaEmision ", conn)

            Try

                DSDatos.Tables.Add("VDetalleActividadDescuento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleActividadDescuento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaDescuentoTactico")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoProducto(ByVal Detallado As Boolean, ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select DA.* From VDetallePlanillaDescuentoTactico P Join VDetalleActividad DA On P.ID=DA.IDActividad Where IDTransaccion=" & IDTransaccion & " Order By Codigo, Producto ", conn)

            Try

                DSDatos.Tables.Add("VDetalleActividadDescuento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleActividadDescuento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaDescuentoTacticoListaProducto")
                Report.SetDataSource(DSDatos)

                'Informacion de Software
                EstablecerTitulos(Report, "", "")

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "")

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

        Sub ListadoPosmorten(ByVal Detallado As Boolean, ByVal IDTransaccion As Integer)

            ' Creamos los componenetes para obtener los datos.
            Dim conn As New SqlConnection(VGCadenaConexion)
            Dim DSDatos As New DataSet
            Dim aTable1 As New SqlDataAdapter("Select Codigo , [Cod.] , Venta,ReferenciaCliente, Cliente, Referencia , Producto , Cantidad , [Desc %] , [Desc Uni] , Descuento, Total From VDetalleActividadDescuento Where IDTransaccion=" & IDTransaccion & "  Order by Codigo  ", conn)
            Debug.Print(aTable1.SelectCommand.CommandText)
            Try

                DSDatos.Tables.Add("VDetalleActividadDescuento")

                conn.Open()
                aTable1.Fill(DSDatos.Tables("VDetalleActividadDescuento"))

                conn.Close()

                ' Cargamos los datos recolectados en un Crystal Report y lo cargamos al CrystalView
                Dim Report As New ReportClass
                LoadReport(Report, "rptPlanillaPosmorten")
                Report.SetDataSource(DSDatos)

                Dim SubTitulo As String = "LISTA DE PRODUCTOS"
                'Informacion de Software
                EstablecerTitulos(Report, "", SubTitulo)

                'Establecer conexion
                EstablecerConexion(Report)

                'Mostrar Informe
                MostrarReporte(Report, "", Nothing, DSDatos)

                aTable1.Dispose()

            Catch ex As Exception
                MsgBox("Mensaje : " & ex.Message)
            End Try

        End Sub

    End Class

End Namespace

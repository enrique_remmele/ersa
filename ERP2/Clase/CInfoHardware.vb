﻿Imports System.Management
Imports Scripting

Public Class CInfoHardware

    'Clases

    'Propiedades
    Private newCPUID As String
    Public Property CPUID() As String
        Get
            Return newCPUID
        End Get
        Set(ByVal value As String)
            newCPUID = value
        End Set
    End Property

    Private newHDDID As String
    Public Property HDDID() As String
        Get
            Return newHDDID
        End Get
        Set(ByVal value As String)
            newHDDID = value
        End Set
    End Property

    Private newNETID As String
    Public Property NETID() As String
        Get
            Return newNETID
        End Get
        Set(ByVal value As String)
            newNETID = value
        End Set
    End Property

    Private newNAME As String
    Public Property NAME() As String
        Get
            Return newNAME
        End Get
        Set(ByVal value As String)
            newNAME = value
        End Set
    End Property

    Private newCODIGO As String
    Public Property CODIGO() As String
        Get
            Return newCODIGO
        End Get
        Set(ByVal value As String)
            newCODIGO = value
        End Set
    End Property

    'Funciones
    Public Sub GetHDSerial()
        Try
            Dim fso As New FileSystemObject
            Dim selected_drive As Drive = fso.GetDrive(Application.ExecutablePath.Substring(0, 3))
            Dim disk As New ManagementObject("Win32_LogicalDisk.DeviceID=""" + selected_drive.DriveLetter + ":""")
            Dim diskPropertyA As PropertyData = disk.Properties("VolumeSerialNumber")

            For Each a As PropertyData In disk.Properties
                If a.Value IsNot Nothing Then
                    Debug.Print(a.Name & ": " & a.Value.ToString)
                Else
                    Debug.Print(a.Name & ": ?")
                End If
            Next

            HDDID = diskPropertyA.Value.ToString()

            GetMotherBoard()
        Catch 
        End Try


    End Sub

    Public Sub GetCPUId()
        Try
            Dim cpuInfo As String = String.Empty
            Dim temp As String = String.Empty
            Dim mc As ManagementClass = _
                New ManagementClass("Win32_Processor")
            Dim moc As ManagementObjectCollection = mc.GetInstances()
            For Each mo As ManagementObject In moc
                If cpuInfo = String.Empty Then
                    cpuInfo = mo.Properties("ProcessorId").Value.ToString()
                End If

                For Each a As PropertyData In mo.Properties
                    If a.Value IsNot Nothing Then
                        Debug.Print(a.Name & ": " & a.Value.ToString)
                    Else
                        Debug.Print(a.Name & ": ?")
                    End If
                Next

            Next

            CPUID = cpuInfo
        Catch 
        End Try


    End Sub

    Public Sub GetMotherBoard()
        Try
            Dim mc As ManagementClass = New ManagementClass("Win32_BaseBoard")
            Dim moc As ManagementObjectCollection = mc.GetInstances()
            For Each mo As ManagementObject In moc
                For Each a As PropertyData In mo.Properties
                    If a.Value IsNot Nothing Then
                        Debug.Print(a.Name & ": " & a.Value.ToString)
                    Else
                        Debug.Print(a.Name & ": ?")
                    End If
                Next
            Next
        Catch
        End Try


    End Sub

    Public Sub GetNETID()
        Try
            Dim objMOS As ManagementObjectSearcher
            Dim objMOC As Management.ManagementObjectCollection
            Dim objMO As Management.ManagementObject

            objMOS = New ManagementObjectSearcher("Select * From Win32_NetworkAdapter")
            objMOC = objMOS.Get

            For Each objMO In objMOC
                NETID = " " & NETID & " " & objMO("MACAddress")
            Next

            'NETID = FGEncripta(NETID.Trim)
        Catch 
        End Try


    End Sub

    Public Sub GetNAME()
        'NAME = FGEncripta(My.Computer.Name)
        NAME = My.Computer.Name
    End Sub

    Public Sub New()

        GetHDSerial()
        'GetNETID()
        GetCPUId()
        GetNAME()

        'CODIGO = NAME & " " & CPUID & " " & HDDID & " " & NETID
        CODIGO = NAME & " " & CPUID & " " & HDDID

    End Sub

    Public Function TerminalHabilitadaBD(Optional ByVal CadenaConexion As String = "") As Boolean

        Dim CSistema As New CSistema
        Dim sql As String = "Select TOP 1 ID, IDDeposito,IDSucursal, Deposito, Sucursal, CodigoSucursal, Descripcion, Activado From VTerminal Where CodigoActivacion='" & CODIGO & "' And Activado='True' "
        Dim dt As DataTable = CSistema.ExecuteToDataTable(sql, CadenaConexion)

        'Consultar a la Base de Datos si la PC esta activa
        If dt Is Nothing Then
            MessageBox.Show("Este equipo no esta habilitado en la base de datos para utilizar el sistema!", "Inhabilitado", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("Este equipo no esta habilitado en la base de datos para utilizar el sistema!", "Inhabilitado", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If CBool(dt.Rows(0)("Activado")) = False Then
            MessageBox.Show("Este equipo no esta habilitado en la base de datos para utilizar el sistema!", "Inhabilitado", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End
        End If

        'Obtener configuraciones de equipo
        Dim oRow As DataRow = dt.Rows(0)

        vgIDTerminal = oRow("ID")
        vgIDDeposito = oRow("IDDeposito")
        vgIDSucursal = oRow("IDSucursal")

        vgTerminal = oRow("Descripcion")
        vgDeposito = oRow("Deposito")
        vgSucursal = oRow("Sucursal")

        vgSucursalCodigo = oRow("CodigoSucursal")

        Return True

    End Function

    Private Sub Propiedades()

        Dim vector() As String = "Win32_1394Controller Win32_1394ControllerDevice Win32_AccountSID Win32_ActionCheck Win32_ActiveRoute Win32_AllocatedResource Win32_ApplicationCommandLine Win32_ApplicationService Win32_AssociatedBattery Win32_AssociatedProcessorMemory Win32_AutochkSetting Win32_BaseBoard Win32_Battery Win32_Binary Win32_BindImageAction Win32_BIOS Win32_BootConfiguration Win32_Bus  Win32_CacheMemory Win32_CDROMDrive Win32_CheckCheck Win32_CIMLogicalDeviceCIMDataFile Win32_ClassicCOMApplicationClasses Win32_ClassicCOMClass Win32_ClassicCOMClassSetting Win32_ClassicCOMClassSettings Win32_ClassInforAction Win32_ClientApplicationSetting Win32_CodecFile Win32_COMApplicationSettings Win32_COMClassAutoEmulator Win32_ComClassEmulator Win32_CommandLineAccess Win32_ComponentCategory Win32_ComputerSystem Win32_ComputerSystemProcessor Win32_ComputerSystemProduct Win32_ComputerSystemWindowsProductActivationSetting Win32_Condition Win32_ConnectionShare Win32_ControllerHastHub Win32_CreateFolderAction Win32_CurrentProbe Win32_DCOMApplicatio".Split(" ")

        For i As Integer = 0 To vector.GetLength(0) - 1

            Try
                Debug.Print(vbCrLf)
                Debug.Print(vbCrLf)
                Debug.Print(vector(i))
                Dim mc As ManagementClass = New ManagementClass(vector(i))
                Dim moc As ManagementObjectCollection = mc.GetInstances()
                For Each mo As ManagementObject In moc
                    For Each a As PropertyData In mo.Properties
                        If a.Value IsNot Nothing Then
                            Debug.Print(a.Name & ": " & a.Value.ToString)
                        Else
                            Debug.Print(a.Name & ": ?")
                        End If
                    Next
                Next
            Catch ex As Exception

            End Try
           
        Next

        

    End Sub

End Class

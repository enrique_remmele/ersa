﻿Public Class CCaja

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Public Property Seleccionado As Boolean
    Public Property IDSucursal As Integer
    Public Property IDTransaccion As Integer
    Public Property Numero As Integer
    Public Property Fecha As Date
    Public Property Habilitado As Boolean
    Public Property Estado As String

    'FUNCIONES
    Sub Nuevo()

    End Sub

    Private Sub SetValue(dt As DataTable)

        Dim Resultado As DataRow = dt.Rows(0)

        Me.Numero = Resultado("Numero")
        Me.IDTransaccion = Resultado("IDTransaccion")
        Me.Habilitado = Resultado("Habilitado")
        Me.Fecha = Resultado("Fecha")
        Me.Estado = Resultado("EstadoCaja")

        Seleccionado = True

    End Sub

    Sub Obtener(Numero)

        Seleccionado = False

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Top(1) * From VCaja Where IDSucursal=" & IDSucursal & " And Numero=" & Numero & " Order By Numero Desc")

        If dt Is Nothing Then
            MessageBox.Show("No se puedo obtener el numero de caja!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No se puedo obtener el numero de caja!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        SetValue(dt)


    End Sub

    Sub ObtenerUltimoNumero()

        Seleccionado = False

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Select Top(1) * From VCaja Where IDSucursal=" & IDSucursal & " Order By Numero Desc")

        If dt Is Nothing Then
            MessageBox.Show("No se puedo obtener el numero de caja!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No se puedo obtener el numero de caja!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        SetValue(dt)

    End Sub

    Public Sub New()

        Seleccionado = False
        IDSucursal = 0
        IDTransaccion = 0
        Numero = 0
        Fecha = Nothing
        Habilitado = False
        Estado = ""

    End Sub

End Class

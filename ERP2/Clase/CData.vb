﻿Imports System.Data.SqlClient
Imports System.Threading

Public Class CData

    'CLASES
    Dim CSistema As New CSistema

    'EVENTOS
    Public Event DescargandoDato(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ErrorEnDescarga(ByVal sender As Object, ByVal e As EventArgs, ByVal Mensaje As String)
    Public Event DescargaTotal(ByVal sender As Object, ByVal e As EventArgs)
    Public Event Inicializado(ByVal sender As Object, ByVal e As EventArgs, ByVal Mensaje As String)

    'PROPIEDADES
    Private ObjetosValue As Integer
    Public Property Objetos() As Integer
        Get
            Return ObjetosValue
        End Get
        Set(ByVal value As Integer)
            ObjetosValue = value
        End Set
    End Property

    Private ObjetoActualValue As String
    Public Property ObjetoActual() As String
        Get
            Return ObjetoActualValue
        End Get
        Set(ByVal value As String)
            ObjetoActualValue = value
        End Set
    End Property

    Private IndexValue As Integer
    Public Property Index() As Integer
        Get
            Return IndexValue
        End Get
        Set(ByVal value As Integer)
            IndexValue = value
        End Set
    End Property

    Private dtTablasValue As DataTable
    Public Property dtTablas() As DataTable
        Get
            Return dtTablasValue
        End Get
        Set(ByVal value As DataTable)
            dtTablasValue = value
        End Set
    End Property

    'VARIABLES

    Sub Inicializar()

        'Eliminamos todos los datos
        EliminarDAtos()

        'Cargamos las tablas principales
        CargarTabla()

        Objetos = dtTablas.Rows.Count

        RaiseEvent Inicializado(New Object, New EventArgs, "Conectado...")
        Thread.Sleep(1000)

    End Sub

    Private Sub CargarTabla()

        dtTablas = CSistema.ExecuteToDataTable("Select * From TablasPrincipales Order By Descripcion").Copy

    End Sub

    Sub EliminarDAtos()

        vgData.Tables.Clear()

    End Sub

    Sub CargarTablas()

        If dtTablas Is Nothing Then
            Exit Sub
        End If

        'Recorremos la tabla para cargar en el DataSet
        For Each oRow As DataRow In dtTablas.Rows

            Dim Tabla As String = oRow("Tabla").ToString
            Dim OrderBy As String = oRow("OrderBy").ToString

            If OrderBy <> "" Then
                OrderBy = " Order By " & OrderBy
            End If

            ObjetoActual = "Descargando " & oRow("Descripcion").ToString & "..."
            RaiseEvent DescargandoDato(New Object, New EventArgs)

            'Thread.Sleep(500)
            Dim SQL As String = ""

            If CBool(oRow("Sp")) = False Then
                SQL = "Select * From " & Tabla & " " & OrderBy
            Else
                SQL = "Exec " & Tabla & " @IDUsuario = " & vgIDUsuario & ", @IDSucursal = " & vgIDSucursal & ", @IDDeposito = " & vgIDDeposito & ", @IDTerminal = " & vgIDTerminal & " "
            End If

            Dim ad As New SqlDataAdapter(SQL, VGCadenaConexion)

            Try
                ad.Fill(vgData, oRow("NombreTabla").ToString)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                RaiseEvent ErrorEnDescarga(New Object, New EventArgs, "Error: " & ex.Message)
                Thread.Sleep(2000)
            End Try

            Index = Index + 1

        Next

    End Sub

    Function GetTable(ByVal Tabla As String, Optional ByVal Filtro As String = "") As DataTable

        GetTable = Nothing
        registroArchivo("GET-TABLE", Tabla, Tabla & "- " & Filtro)
        Try
            ' cb

            If Tabla = "VProducto" Then
                Debug.Print("aqui")
            End If

            If Tabla = "ProductoUnico" Then
                'Tabla = "VCliente"
                GetTable = CreateTable(Tabla, "select * from fn_recuperaProducto(" & Filtro & ")", 10)
            ElseIf Tabla = "ClienteUnico" Then
                'Tabla = "VCliente"
                GetTable = CreateTable(Tabla, "select * from fn_recuperaCliente(" & Filtro & ")", 10)
            Else
                If Tabla = "VTipoDescuento" Then
                    Debug.Print("aqui")
                End If

                If vgData.Tables(Tabla) Is Nothing Then
                    GetTable = CreateTable(Tabla, "Select * From " & Tabla, 10)
                End If

                If Filtro = "" Then
                    GetTable = vgData.Tables(Tabla)
                    GetTable.DefaultView.RowFilter = ""
                Else
                    GetTable = CSistema.FiltrarDataTable(vgData.Tables(Tabla), Filtro)
                End If

                If GetTable Is Nothing Then
                    If Filtro = "" Then
                        GetTable = CreateTable(Tabla, "Select * From " & Tabla, 10)
                    Else
                        GetTable = CreateTable(Tabla, "Select * From " & Tabla & " Where " & Filtro, 10)
                    End If

                End If
            End If

        Catch ex As Exception

        End Try
        registroArchivo("GET-TABLE", Tabla, Tabla & "- " & Filtro)
    End Function

    Function GetStructure(ByVal Tabla As String, ByVal SQL As String) As DataTable

        GetStructure = Nothing

        'Verificamos si existe la estructura en memoria
        If vgData.Tables.IndexOf(Tabla) >= 0 Then
            Return vgData.Tables(Tabla).Clone
        End If

        'Si no existe, entonces creamos
        Dim ad As New SqlDataAdapter(SQL, VGCadenaConexion)
        Try
            ad.Fill(vgData, Tabla)
            GetStructure = vgData.Tables(Tabla).Clone
        Catch ex As Exception

        End Try
        
    End Function

    Function CreateTable(ByVal Tabla As String, ByVal SQL As String, Optional ByVal Timeout As Integer = 30) As DataTable

        CreateTable = Nothing

        'Verificamos si existe la estructura en memoria
        If vgData.Tables.IndexOf(Tabla) >= 0 Then
            Return vgData.Tables(Tabla)
        End If

        'Si no existe, entonces creamos
        Dim ad As New SqlDataAdapter(SQL, VGCadenaConexion)
        ad.SelectCommand.CommandTimeout = Timeout
        ad.Fill(vgData, Tabla)

        CreateTable = vgData.Tables(Tabla)

    End Function

    Sub ResetTable(ByVal Tabla As String, Optional ByVal SQL As String = "")

        If Tabla = "" Then
            Exit Sub
        End If

        'Eliminamos primeramente la tabla
        If vgData.Tables.IndexOf(Tabla) >= 0 Then
            vgData.Tables.Remove(Tabla)
        End If

        If SQL = "" Then
            SQL = "Select * From " & Tabla
        End If

        Dim ad As New SqlDataAdapter(Sql, VGCadenaConexion)
        ad.Fill(vgData, Tabla)

    End Sub

    Public Sub OrderDataTable(ByRef dt As DataTable, ByVal OrderBy As String, Optional ByVal Asc As Boolean = True)

        If dt Is Nothing Then
            Exit Sub
        End If


        If OrderBy = "" Then
            Exit Sub
        End If

        Dim Tipo As String
        If Asc = True Then
            Tipo = " ASC "
        Else
            Tipo = " DESC "
        End If

        Dim dtv As DataView = dt.DefaultView

        dtv.Sort = OrderBy & Tipo
        dt = dtv.ToTable().Copy

    End Sub

    Sub Insertar(ByVal ID As Integer, ByVal Tabla As String)
        Try
            'Insertar
            Dim oRow As DataRow = CSistema.ExecuteToDataTable("Select * From " & Tabla & " Where ID = " & ID, "", 10)(0)

            If oRow Is Nothing Then
                Exit Sub
            End If

            Dim NewRow As DataRow = vgData.Tables(Tabla).NewRow
            For i As Integer = 0 To vgData.Tables(Tabla).Columns.Count - 1
                Dim NombreColumna As String = vgData.Tables(Tabla).Columns(i).ColumnName
                NewRow(NombreColumna) = oRow(NombreColumna)
            Next

            vgData.Tables(Tabla).Rows.Add(NewRow)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error al insertar producto. Contacte con el administrador", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
       

    End Sub

    Sub InsertarFiltro(ByVal Condicion As String, ByVal Tabla As String)

        Try
            'Insertar
            For Each oRow As DataRow In CSistema.ExecuteToDataTable("Select * From " & Tabla & " Where " & Condicion).Rows

                Dim NewRow As DataRow = vgData.Tables(Tabla).NewRow
                For i As Integer = 0 To vgData.Tables(Tabla).Columns.Count - 1
                    Dim NombreColumna As String = vgData.Tables(Tabla).Columns(i).ColumnName
                    NewRow(NombreColumna) = oRow(NombreColumna)

                Next

                vgData.Tables(Tabla).Rows.Add(NewRow)

            Next
        Catch ex As Exception

        End Try


    End Sub

    Sub Actualizar(ByVal ID As Integer, ByVal Tabla As String)

        'Primero eliminamos
        Eliminar(ID, Tabla)

        'luego insertamos
        Insertar(ID, Tabla)

    End Sub

    Sub Actualizar(ByVal Condicion As String, ByVal Tabla As String)

        'Primero eliminamos
        For Each oRow As DataRow In vgData.Tables(Tabla).Select(Condicion)
            vgData.Tables(Tabla).Rows.Remove(oRow)
        Next

        'luego insertamos
        For Each oRow As DataRow In CSistema.ExecuteToDataTable("Select * From " & Tabla & " Where " & Condicion & " ").Rows
            Dim NewRow As DataRow = vgData.Tables(Tabla).NewRow
            NewRow.ItemArray = oRow.ItemArray
            vgData.Tables(Tabla).Rows.Add(NewRow)
        Next

    End Sub

    Sub Eliminar(ByVal ID As Integer, ByVal Tabla As String)

        'Eliminar
        For Each oRow As DataRow In vgData.Tables(Tabla).Select(" ID = " & ID)
            vgData.Tables(Tabla).Rows.Remove(oRow)
        Next

    End Sub

    Sub EliminarFiltro(ByVal Condicion As String, ByVal Tabla As String)

        'Eliminar
        Try
            For Each oRow As DataRow In vgData.Tables(Tabla).Select(Condicion)
                vgData.Tables(Tabla).Rows.Remove(oRow)
            Next
        Catch ex As Exception

        End Try

    End Sub

    Sub EliminarRegistros(ByVal Condicion As String, ByVal Tabla As DataTable)

        'Eliminar
        Try
            Dim RegistrosEliminar(-1) As DataRow

            For Each oRow As DataRow In Tabla.Select(Condicion)
                ReDim Preserve RegistrosEliminar(RegistrosEliminar.Length)
                RegistrosEliminar(RegistrosEliminar.Length - 1) = oRow
            Next

            For i As Integer = 0 To RegistrosEliminar.Length - 1
                Tabla.Rows.Remove(RegistrosEliminar(i))
            Next

        Catch ex As Exception

        End Try

    End Sub

    Sub ActualizarRegistroABM(ByVal Operacion As CSistema.NUMOperacionesABM, ByVal ID As Integer, ByVal Tabla As String)

        Select Case Operacion

            Case ERP.CSistema.NUMOperacionesABM.INS
                Insertar(ID, Tabla)

            Case ERP.CSistema.NUMOperacionesABM.UPD

                'Primero eliminamos
                Eliminar(ID, Tabla)

                'luego insertamos
                Insertar(ID, Tabla)

            Case ERP.CSistema.NUMOperacionesABM.DEL
                Eliminar(ID, Tabla)

        End Select

    End Sub

    ''' <summary>
    ''' Devuelve un DataTable Filtrado
    ''' </summary>
    ''' <param name="dt">DataTable con informacion que se quiere filtrar</param>
    ''' <param name="Where">Los parametros de filtro</param>
    ''' <returns>DataTable</returns>
    ''' <remarks></remarks>
    Function FiltrarDataTable(ByVal dt As DataTable, ByVal Where As String) As DataTable

        FiltrarDataTable = Nothing

        If dt Is Nothing Then
            Return Nothing
        End If

        Try

            Dim dtv As DataView = dt.DefaultView
            dtv.RowFilter = Where
            FiltrarDataTable = dtv.ToTable

            Return FiltrarDataTable

        Catch ex As Exception
            CSistema.CargarError(ex, "CSistema", "FiltrarDataTable", "", dt.TableName)
            ''' <summary>
            ''' Registra un error del sistema y lo guarda en el directorio de ERROR
            ''' </summary>
            ''' <param name="Mensaje">Objeto que tiene la informacion del error.</param>
            ''' <param name="Origen">Desde donde se genero. Clase, Form, Modulo, etc.</param>
            ''' <param name="Bloque">Funcion o Procedimiento en donde se genero el error.</param>
            ''' <param name="Parametros">Opcional: Se puede dar informacion de los parametros que posiblemente que generaron el error.</param>
            ''' <param name="DatosExtras">Opcional: Se puede dar alguna informacion extra.</param>
            ''' <remarks></remarks>
        End Try


    End Function

    ''' <summary>
    ''' Devuelve el primer registro encontrado segun la consulta y la tabla
    ''' </summary>
    ''' <param name="Consulta"></param>
    ''' <param name="Tabla"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetRow(ByVal Consulta As String, ByVal Tabla As String) As DataRow

        GetRow = Nothing

        Try
            If vgData.Tables.IndexOf(Tabla) < 0 Then

                'Crear si no existe
                GetTable(Tabla)

                'Volvemos a consultar
                If vgData.Tables.IndexOf(Tabla) < 0 Then
                    Return Nothing
                End If
            End If

            GetRow = GetTable(Tabla).NewRow

            For Each oRow As DataRow In GetTable(Tabla).Select(Consulta)
                GetRow = oRow
                Exit For
            Next

        Catch ex As Exception

        End Try
        

    End Function

    ''' <summary>
    ''' Devuelve el primer registro encontrado segun la consulta y la tabla
    ''' </summary>
    ''' <param name="Consulta"></param>
    ''' <param name="Tabla"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetRow(ByVal Consulta As String, ByVal Tabla As DataTable) As DataRow

        GetRow = Nothing

        Try

            If Tabla Is Nothing Then
                Exit Function
            End If

            GetRow = Tabla.NewRow

            For Each oRow As DataRow In Tabla.Select(Consulta)
                GetRow = oRow
                Exit For
            Next

        Catch ex As Exception

        End Try


    End Function

End Class

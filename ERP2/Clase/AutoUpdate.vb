﻿Imports System.Net
Imports System.IO
''' <summary>
''' clase de actualizacion
''' </summary>
''' <remarks></remarks>
Public Class AutoUpdate
    'prueba

    Private Const Key As String = "&**#@!" ' any unique sequence of characters
    ' the file with the update information
    Private Const sfile As String = "update.txt"
    Private Const autoUpdaterFileName As String = "AutoUpdate.exe"
    Private Shared esFTP As Boolean

    ''' <summary>
    ''' Clase de actualizacion del sistema
    ''' </summary>
    ''' <param name="CommandLine">Argumentos que seran pasados al sistema una vez reabierto</param>
    ''' <param name="RemotePath">direccion de descarga del manifiesto y los archivos</param>
    ''' <param name="User">usuario de descarga</param>
    ''' <param name="Password">password de descarga</param>
    ''' <param name="VersionRevision">version del sistema local</param>
    ''' <param name="AvisaQueNoHayActualizacion">fuerza a que la funcion muestre una ventana cuando no existen actualizaciones</param>
    ''' <param name="ExibeMensajeError">fuerza a mostrar una ventana cuando ocurre algun error de conexion o descarga</param>
    ''' <returns>True: si existe una actualizacion y el usuario acepto la descarga; False: si no existe actualizacion o el usuario rechazo la descarga</returns>
    ''' <remarks></remarks>
    Public Shared Function AutoUpdate(ByRef CommandLine As String, ByVal RemotePath As String, ByVal User As String, ByVal Password As String, ByVal VersionRevision As String, ByVal AvisaQueNoHayActualizacion As Boolean, ByVal ExibeMensajeError As Boolean) As Boolean

        Dim RemoteUri As String = IIf(RemotePath.EndsWith("/"), RemotePath, RemotePath & "/")

        CommandLine = Replace(Microsoft.VisualBasic.Command(), Key, "")
        ' Verify if was called by the autoupdate
        If InStr(Microsoft.VisualBasic.Command(), Key) > 0 Then
            Try
                ' try to delete the AutoUpdate program, 
                ' since it is not needed anymore
                System.IO.File.Delete(String.Format("{0}\{1}", Application.StartupPath, autoUpdaterFileName))
            Catch ex As Exception
            End Try
            ' return false means that no update is needed
            Return False
        Else
            ' was called by the user
            Dim ret As Boolean = False ' Default - no update needed
            Try
                'Prueba la conexion a la direccion URL
                'Fuente: http://msdn.microsoft.com/en-us/library/system.net.httpwebrequest.timeout.aspx

                Dim myFTPWebResponse As FtpWebResponse
                Dim myHttpWebRequest As HttpWebRequest
                Dim responseStream As Stream

                esFTP = RemotePath.StartsWith("ftp") ''si es una direccion ftp

                If esFTP Then
                    Dim myFTPWebRequest As FtpWebRequest = CType(FtpWebRequest.Create(RemoteUri & sfile), FtpWebRequest)

                    myFTPWebRequest.KeepAlive = True
                    myFTPWebRequest.Method = WebRequestMethods.Ftp.DownloadFile
                    myFTPWebRequest.Credentials = New NetworkCredential(User, Password)

                    ' A HttpWebResponse object is created and is GetResponse Property of the HttpWebRequest associated with it 
                    myFTPWebResponse = CType(myFTPWebRequest.GetResponse(), FtpWebResponse)

                    responseStream = myFTPWebResponse.GetResponseStream()

                Else
                    ' Create a new 'HttpWebRequest' Object to the mentioned URL.
                    myHttpWebRequest = CType(WebRequest.Create(RemoteUri & sfile), HttpWebRequest)
                    'Espere por la cantidad de milisegundos del timeout
                    myHttpWebRequest.Timeout = 15000

                    ' A HttpWebResponse object is created and is GetResponse Property of the HttpWebRequest associated with it 
                    Dim myHttpWebResponse As HttpWebResponse = CType(myHttpWebRequest.GetResponse(), HttpWebResponse)

                    responseStream = myHttpWebResponse.GetResponseStream()
                End If

                'Descarga el update.txt

                Dim reader As New StreamReader(responseStream)

                Dim Contents As String = reader.ReadToEnd()
                reader.Close()

                ' if something was read
                If Contents <> "" Then
                    ' Break the contents 
                    Dim arr1 As String() = Split(Contents, "|")
                    Dim newVersion As String = arr1(0)
                    Dim A As New AutoUpdate
                    If A.CompararVersiones(newVersion, VersionRevision) Then
                        'If MessageBox.Show("Una nueva versión de " & _
                        '                   Application.ProductName & " (" & _
                        '                   arr1(0).ToString & ") esta disponible." & vbCrLf & _
                        '                   "¿Desea descargarlo? Si acepta la aplicación se reiniciará. ", "Actualización", MessageBoxButtons.YesNo, _
                        '                    MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification) _
                        '                    = DialogResult.Yes Then

                        ' assembly the parameter to be passed to the auto 
                        ' update program
                        ' x(1) is the files that need to be 
                        ' updated separated by "?"
                        Dim arg As String = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}", _
                                                          Application.ExecutablePath, _
                                                          RemoteUri, _
                                                          arr1(1), Key, Application.StartupPath, _
                                                          Application.ProductName, arr1(0), _
                                                          User, Password, _
                                                          Microsoft.VisualBasic.Command())
                        ' Download the auto update program to the application 
                        ' path, so you always have the last version runing

                        If esFTP Then
                            DownloadFTP(Application.StartupPath, autoUpdaterFileName, RemoteUri, User, Password)
                        Else
                            'Descarga el update.txt
                            Using myWebClient As New System.Net.WebClient()
                                'the webclient
                                myWebClient.DownloadFile(String.Format("{0}\{1}", RemotePath, autoUpdaterFileName), String.Format("{0}\{1}", Application.StartupPath, autoUpdaterFileName))
                            End Using
                        End If


                        ' Call the auto update program with all the parameters

                        'Dim Proceso As Process
                        'Proceso = Process.Start( _
                        '    Application.StartupPath & "\autoupdater.exe", arg)
                        'Proceso.WaitForExit()

                        System.Diagnostics.Process.Start( _
                            String.Format("{0}\{1}", Application.StartupPath, autoUpdaterFileName), arg)

                        Application.Exit()

                        ' return true - auto update in progress
                        ret = True
                        'End If --Fin de If que consulta si desea actualizar para que haga automatico ERSA
                    Else
                        If AvisaQueNoHayActualizacion Then
                            MessageBox.Show("Ya está utilizando la última versión disponible (" & VGSoftwareVersion & ")", _
                       "Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                End If
            Catch ex As Net.WebException
            'Error Web (acceso remoto inaccesible)
            ret = False
            If ExibeMensajeError Then
                MessageBox.Show("Servidor de actualización inaccesible." & vbCr & _
                                      "Compruebe el estado de su conexión o que la cuenta sea válida.", _
                                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception

            '' if there is an error return true, 
            '' what means that the application
            '' should be closed
            'ret = True
            ret = False
            If ExibeMensajeError Then
                ' something went wrong... 
                MessageBox.Show(ex.Message, _
                         "Error al actualizar.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        End Try
            Return ret
        End If
    End Function

    Function CompararVersiones(VersionDescarga As String, VersionLocal As String) As Boolean

        CompararVersiones = False

        'Validamos
        If VersionDescarga.Contains(".") = False Or VersionLocal.Contains(".") = False Then
            GoTo ErrorVersion
        End If

        Dim Descarga() As String = VersionDescarga.Split(".")
        Dim Local() As String = VersionLocal.Split(".")
        Dim Partes As Integer = Descarga.Length

        If Local.Length < Descarga.Length Then
            Partes = Local.Length
        End If

        If Partes = 0 Then
            GoTo ErrorVersion
        End If

        'Comparamos de derecha a izquierda
        For c As Integer = 0 To Partes - 1
            If Convert.ToInt16(Descarga(c)) > Convert.ToInt16(Local(c)) Then
                Return True
            End If
        Next

        'Si llegamos hasta aqui, no hay nuevas actualizaciones
        Return False

ErrorVersion:
        MessageBox.Show("No se ha podido comprobar si existe una nueva version en el sistema! Favor comunicar a sistemas.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
        CompararVersiones = False

    End Function

    Private Shared Sub DownloadFTP(ByVal filePath As String, ByVal fileName As String, ByVal RemotePath As String, ByVal User As String, ByVal Password As String)

        Dim reqFTP As FtpWebRequest = Nothing
        Dim ftpStream As Stream = Nothing
        Try
            Dim outputStream As New FileStream(String.Format("{0}\{1}", filePath, fileName), FileMode.Create)
            reqFTP = DirectCast(FtpWebRequest.Create(New Uri(RemotePath + fileName)), FtpWebRequest) ''must start with ftp://
            reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
            reqFTP.UseBinary = True
            reqFTP.Credentials = New NetworkCredential(User, Password)
            Dim response As FtpWebResponse = DirectCast(reqFTP.GetResponse(), FtpWebResponse)
            ftpStream = response.GetResponseStream()
            Dim cl As Long = response.ContentLength
            Const bufferSize As Integer = 2048
            Dim readCount As Integer
            Dim buffer As Byte() = New Byte(bufferSize - 1) {}

            readCount = ftpStream.Read(buffer, 0, bufferSize)
            While readCount > 0
                outputStream.Write(buffer, 0, readCount)
                readCount = ftpStream.Read(buffer, 0, bufferSize)
            End While

            ftpStream.Close()
            outputStream.Close()
            response.Close()
        Catch ex As Exception
            If ftpStream IsNot Nothing Then
                ftpStream.Close()
                ftpStream.Dispose()
            End If
            Throw New Exception(ex.Message)
        End Try
    End Sub


End Class

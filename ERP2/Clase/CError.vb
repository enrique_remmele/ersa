﻿Imports System.IO

Public Class CError

    'Propiedades
    Private PathLogValue As String
    ''' <summary>
    ''' Obtiene o Establece la direccion fisica en donde se almacenara el registro del log
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PathLog() As String
        Get
            Return PathLogValue
        End Get
        Set(ByVal value As String)
            PathLogValue = value
        End Set
    End Property

    Private DirectoryLogValue As String
    ''' <summary>
    ''' Obtiene o establece el nombre de la carpeta en donde se guardara el archivo de error
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DirectoryLog() As String
        Get
            Return DirectoryLogValue
        End Get
        Set(ByVal value As String)
            DirectoryLogValue = value
        End Set
    End Property

    Private FileNameLogValue As String
    ''' <summary>
    ''' Obtiene o establece el nombre del archivo de error
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FileName() As String
        Get
            Return FileNameLogValue
        End Get
        Set(ByVal value As String)
            FileNameLogValue = value
        End Set
    End Property

    ''' <summary>
    ''' Retorna el mes en texto segun el numero de mes pasado por parametro
    ''' </summary>
    ''' <param name="Month"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ReturnTextMonth(ByVal Month As Integer) As String

        ReturnTextMonth = ""

        Select Case Month
            Case 1
                Return "Enero"
            Case 2
                Return "Febrero"
            Case 3
                Return "Marzo"
            Case 4
                Return "Abril"
            Case 5
                Return "Mayo"
            Case 6
                Return "Junio"
            Case 7
                Return "Julio"
            Case 8
                Return "Agosto"
            Case 9
                Return "Setiembre"
            Case 10
                Return "Octubre"
            Case 11
                Return "Noviembre"
            Case 12
                Return "Diciembre"
        End Select
    End Function

    ''' <summary>
    ''' Inicializacion por defecto
    ''' </summary>
    ''' <remarks></remarks>
    Sub New()
        PathLog = VGCarpetaError
        DirectoryLog = Date.Now.Year & " " & ReturnTextMonth(Date.Now.Month) & "\"
        FileName = Date.Now.ToLongDateString & ".txt"
    End Sub

    ''' <summary>
    ''' True si existe la carpeta, false en caso contrario
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsExistFolder(ByVal vPathLog As String) As Boolean

        IsExistFolder = False

        Try
            If Directory.Exists(vPathLog) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try

    End Function

    ''' <summary>
    ''' True si existe el archivo, false en caso contrario
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsExistFile(ByVal vPathFileName As String) As Boolean

        IsExistFile = False

        Try
            If File.Exists(vPathFileName) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try

    End Function

    ''' <summary>
    ''' Crea una carpeta en el directorio pasado por parametro
    ''' </summary>
    ''' <param name="vPathLog"></param>
    ''' <remarks></remarks>
    Private Sub CrearCarpeta(ByVal vPathLog As String)

        Try
            Directory.CreateDirectory(vPathLog)

        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' Crea un archivo txt en el directorio pasado por parametro
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CrearArchivo(ByVal vPathFileName)

        Dim obj_FSO As Object
        Dim Archivo As Object

        Try
            obj_FSO = CreateObject("Scripting.FileSystemObject")

            'Creamos un archivo con el método CreateTextFile
            Archivo = obj_FSO.CreateTextFile(vPathFileName, False)

            'Escribimos lineas
            Dim Titulo As String = "LOG de Errores - " & VGSoftwareNombre & " :: Fecha: " & Date.Now.ToLongDateString & " " & Date.Now.ToShortTimeString
            Archivo.WriteLine(Titulo)
            Dim Guion As String = ""
            For i As Integer = 0 To Titulo.Length - 1
                Guion = Guion + "-"
            Next
            Archivo.WriteLine(Guion)

            'Cerramos el fichero
            Archivo.Close()

            obj_FSO = Nothing
            Archivo = Nothing

        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' Registra un error del sistema y lo guarda en el directorio de ERROR
    ''' </summary>
    ''' <param name="Mensaje">Objeto que tiene la informacion del error.</param>
    ''' <param name="Origen">Desde donde se genero. Clase, Form, Modulo, etc.</param>
    ''' <param name="Bloque">Funcion o Procedimiento en donde se genero el error.</param>
    ''' <param name="Parametros">Opcional: Se puede dar informacion de los parametros que posiblemente que generaron el error.</param>
    ''' <param name="DatosExtras">Opcional: Se puede dar alguna informacion extra.</param>
    ''' <remarks></remarks>
    Public Sub CargarError(ByVal Mensaje As Exception, ByVal Origen As String, ByVal Bloque As String, Optional ByVal Parametros As String = "", Optional ByVal DatosExtras As String = "", Optional ByVal MostarError As Boolean = True)

        Try
            Dim Time As String = Date.Now.ToShortDateString & " " & Date.Now.ToLongTimeString

            'Verificar si existe el path correspondiente
            If IsExistFolder(PathLog) = False Then
                CrearCarpeta(PathLog)
            End If

            'Verificar si existe el directorio correspondiente
            Dim tempDirectoryLog As String = Date.Now.Year & " " & ReturnTextMonth(Date.Now.Month) & "\"
            If IsExistFolder(PathLog & tempDirectoryLog) = False Then
                CrearCarpeta(PathLog & tempDirectoryLog)
                DirectoryLog = tempDirectoryLog
            End If

            'Verificar si existe el archivo correspondiente
            Dim tempFileName As String = Date.Now.ToLongDateString & ".txt"
            If IsExistFile(PathLog & tempDirectoryLog & tempFileName) = False Then
                CrearArchivo(PathLog & tempDirectoryLog & tempFileName)
                FileName = tempFileName
            End If

            'Cargar Linea
            Dim sw As New System.IO.StreamWriter(PathLog & DirectoryLog & FileName, True)
            sw.WriteLine("")
            sw.WriteLine("---------------------------------------------")
            sw.WriteLine("Hora: " & Time)
            sw.WriteLine("Error: " & Mensaje.Message)
            sw.WriteLine("Source: " & Origen)
            sw.WriteLine("Funcion: " & Bloque)
            sw.WriteLine("Informacion adicional: " & DatosExtras)
            sw.WriteLine("Parametros: " & Parametros)
            sw.Close()

            If MostarError = True Then
                MessageBox.Show(Mensaje.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Catch ex As Exception
            If MostarError = True Then
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try

    End Sub

    Sub MostrarError(ByVal Mensaje As String, ByRef ctrError As ErrorProvider, ByVal ctr As Control, ByRef tsslEstado As ToolStripStatusLabel, Optional ByVal alignment As ErrorIconAlignment = ErrorIconAlignment.TopRight)

        ctrError.SetError(ctr, Mensaje)
        ctrError.SetIconAlignment(ctr, alignment)
        tsslEstado.Text = Mensaje
        Exit Sub

    End Sub
    ''' <summary>
    ''' Registra un error del sistema y lo guarda en el directorio de ERROR
    ''' </summary>
    ''' <param name="Mensaje">Mensaje del error.</param>
    ''' <param name="Origen">Desde donde se genero. Clase, Form, Modulo, etc.</param>
    ''' <param name="Bloque">Funcion o Procedimiento en donde se genero el error.</param>
    ''' <param name="Parametros">Opcional: Se puede dar informacion de los parametros que posiblemente que generaron el error.</param>
    ''' <param name="DatosExtras">Opcional: Se puede dar alguna informacion extra.</param>
    ''' <remarks></remarks>
    Public Sub CargarRegistro(Mensaje As String)

        Try
            Dim Time As String = Date.Now.ToShortDateString & " " & Date.Now.ToLongTimeString

            'Verificar si existe el path correspondiente
            If IsExistFolder(PathLog) = False Then
                CrearCarpeta(VGCarpetaError)
                PathLog = VGCarpetaError
            End If

            'Verificar si existe el directorio correspondiente
            Dim tempDirectoryLog As String = Date.Now.Year & " " & ReturnTextMonth(Date.Now.Month) & "\"
            If IsExistFolder(PathLog & tempDirectoryLog) = False Then
                CrearCarpeta(PathLog & tempDirectoryLog)
                DirectoryLog = tempDirectoryLog
            End If

            'Verificar si existe el archivo correspondiente
            Dim tempFileName As String = Date.Now.ToLongDateString & ".reg"
            If IsExistFile(PathLog & tempDirectoryLog & tempFileName) = False Then
                CrearArchivo(PathLog & tempDirectoryLog & tempFileName)
                FileName = tempFileName
            End If

            'Cargar Linea
            Dim sw As New System.IO.StreamWriter(PathLog & DirectoryLog & FileName, True)
            sw.WriteLine("")
            sw.WriteLine("---------------------------------------------")
            sw.WriteLine("Hora: " & Time)
            sw.WriteLine("Error: " & Mensaje)

            sw.Close()

            'MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub


End Class

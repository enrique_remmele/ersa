﻿Imports System.Windows.Forms
Imports System.Drawing.Printing

Public Class CImpresion

    'CLASES
    Dim CSistema As New CSistema

    'PROPIEDADES
    Property PPD As PrintPreviewDialog
    Property PrintPreviewControl1 As PrintPreviewControl
    Property Zoom As Decimal
    Property IDFormularioImpresion As Integer
    Property Test As Boolean
    Property DibujarCuadricula As Boolean
    Property PageSettings As PageSettings
    Property IDTransaccion As Integer
    Property Impresora As String

    'VARIABLES
    Private WithEvents PrintDocument As PrintDocument
    Private dtFormularioImpresion As DataTable
    Private dtDetalleFormularioImpresion As DataTable
    Private dtFormularioImpresionCopia As DataTable
    Private dtCabecera As DataTable
    Private dtDetalle As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Controles
        PPD = New PrintPreviewDialog
        PrintDocument = New PrintDocument
        PageSettings = New PageSettings

        EstablecerTablas()

    End Sub

    Public Sub ImprimirTest()

        PrintDocument.DefaultPageSettings.PaperSize = PageSettings.PaperSize
        PrintDocument.DefaultPageSettings.Landscape = PageSettings.Landscape
        PrintDocument.DefaultPageSettings.Margins = PageSettings.Margins
        PrintDocument.PrinterSettings.PrinterName = Impresora

        PPD.Document = PrintDocument
        PrintPreviewControl1.Document = PPD.Document
        PrintPreviewControl1.InvalidatePreview()
        PrintPreviewControl1.Zoom = Zoom

        PrintPreviewControl1.Show()

    End Sub

    Public Sub Imprimir()

        'Validar
        If dtFormularioImpresion.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim PS As New PageSettings
        Dim CData As New CData
        Dim oRow As DataRow = dtFormularioImpresion.Rows(0)
        PS.PaperSize = New System.Drawing.Printing.PaperSize(oRow("TamañoPapel").ToString, oRow("TamañoPapelX").ToString, oRow("TamañoPapelY").ToString)
        PS.Margins = New System.Drawing.Printing.Margins(oRow("MargenIzquierdo").ToString, oRow("MargenDerecho").ToString, oRow("MargenArriba").ToString, oRow("MargenAbajo").ToString)
        PS.Landscape = oRow("Horizontal").ToString

        PrintDocument.DefaultPageSettings.PaperSize = PS.PaperSize
        PrintDocument.DefaultPageSettings.Landscape = PS.Landscape
        PrintDocument.DefaultPageSettings.Margins = PS.Margins

        'Impresora
        If Impresora Is Nothing Then
            Impresora = CData.GetRow("IDTerminal=" & vgIDTerminal & " And IDFormularioImpresion=" & IDFormularioImpresion, "VFormularioImpresionTerminal")("Impresora")
        End If

        If Impresora = "" Then
            Impresora = CData.GetRow("IDTerminal=" & vgIDTerminal & " And IDFormularioImpresion=" & IDFormularioImpresion, "VFormularioImpresionTerminal")("Impresora")
        End If

        If Impresora = "" Then
            MessageBox.Show("No se selecciono ninguna impresora para este tipo de documento!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If


        PrintDocument.PrinterSettings.PrinterName = Impresora

        AddHandler PrintDocument.PrintPage, AddressOf PrintDocument_PrintPage
        PrintDocument.Print()

    End Sub

    Sub Cuadricula(ByVal e As System.Drawing.Printing.PrintPageEventArgs)

        Try
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim I As Integer = 0
            Dim J As Integer = 0

            Dim Fuente As New FontFamily("Arial")
            Dim Formato As Font = New Font("Arial", 8, FontStyle.Regular)
            Dim Formato2 As Font = New Font("Arial", 8, FontStyle.Regular)
            Dim EstiloFuente As FontStyle = FontStyle.Regular
            Formato = New Font(Fuente.Name, 5, EstiloFuente, GraphicsUnit.Pixel)
            Formato2 = New Font(Fuente.Name, 6, EstiloFuente, GraphicsUnit.Pixel)

            'Cuadricula Horizontal
            'Matriz
            For I = 0 To 50
                X = 10
                e.Graphics.DrawString((Y).ToString, Formato2, Brushes.Gray, X, Y)
                For J = 0 To 800
                    X = X + 2
                    'Vector
                    e.Graphics.DrawString(".", Formato, Brushes.Gray, X, Y)
                Next
                Y = Y + 30
            Next

            X = 0
            Y = 0
            I = 0
            J = 0

            'Cuadricula Vertical
            'Matriz
            'Matriz
            For I = 0 To 50
                Y = 10
                e.Graphics.DrawString((X).ToString, Formato2, Brushes.Gray, X, Y)
                For J = 0 To 800
                    Y = Y + 2
                    'Vector
                    e.Graphics.DrawString(".", Formato, Brushes.Gray, X, Y)
                Next
                X = X + 30
            Next
        Catch ex As Exception

        End Try

    End Sub

    Public Function ConvInchToMm(valor As Decimal) As Decimal

        Return (valor) * 10

    End Function

    Public Function ConvToMmInch(valor As Decimal) As Decimal

        valor = (valor) / 3.8888
        ConvToMmInch = CInt(valor)

    End Function

    Private Sub EstablecerTablas()

        dtFormularioImpresion = CSistema.ExecuteToDataTable("Select * From VFormularioImpresion Where IDFormularioImpresion=" & IDFormularioImpresion)
        dtDetalleFormularioImpresion = CSistema.ExecuteToDataTable("Select * From VDetalleFormularioImpresion Where IDFormularioImpresion=" & IDFormularioImpresion)
        dtFormularioImpresionCopia = CSistema.ExecuteToDataTable("Select * From FormularioImpresionCopia Where IDFormularioImpresion=" & IDFormularioImpresion)

        If Test = False Then
            Dim Vista As String = dtFormularioImpresion.Rows(0)("Vista").ToString
            dtCabecera = CSistema.ExecuteToDataTable("Select Top(1) * From " & Vista & " Where IDTransaccion= " & IDTransaccion & " ")

            If CSistema.RetornarValorBoolean(dtFormularioImpresion.Rows(0)("Detalle").ToString) = True Then
                Dim VistaDetalle As String = dtFormularioImpresion.Rows(0)("VistaDetalle").ToString
                dtDetalle = CSistema.ExecuteToDataTable("Select  * From " & VistaDetalle & " Where IDTransaccion= " & IDTransaccion & " ")
            End If

        End If

    End Sub

    Private Sub PrintDocument_PrintPage(sender As Object, e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument.PrintPage

        'Dibujar Cuadricula
        If DibujarCuadricula Then
            Cuadricula(e)
        End If

        'Cabecera
        For Each oRow As DataRow In dtDetalleFormularioImpresion.Select(" Detalle = 'False' ")
            ImprimirCampo(e, oRow, False)
        Next

        'Detalle
        '        For Each oRow As DataRow In dtDetalleFormularioImpresion.Select(" Detalle = 'True' ")
        '            If dtDetalle Is Nothing Then
        '                ImprimirCampo(e, oRow, True)
        '            Else
        '                Dim Indice As Integer = 0
        '                Dim Interlineado As Integer = 0
        '                Dim LineasImpresas As Integer = 1
        '                For Each DetalleRow As DataRow In dtDetalle.Rows
        '                    ImprimirCampo(e, oRow, True, Interlineado, Indice, LineasImpresas)
        '                    Indice = Indice + 1
        '                    Interlineado = Interlineado + (15 * LineasImpresas)
        '                Next
        '            End If

        'seguir:

        '        Next

        'Detalle

        If dtDetalle Is Nothing Then
            For Each oRow As DataRow In dtDetalleFormularioImpresion.Select(" Detalle = 'True' ")
                ImprimirCampo(e, oRow, True)
            Next
        Else
            Dim Indice As Integer = 0
            Dim Interlineado As Integer = 0
            Dim InterlineadoOrigen As Integer = 0
            Dim LineasImpresas As Integer = 1

            For Each DetalleRow As DataRow In dtDetalle.Rows

                Interlineado = 0

                Debug.Print("")

                For Each oRow As DataRow In dtDetalleFormularioImpresion.Select(" Detalle = 'True' ")
                    ImprimirCampo(e, oRow, True, InterlineadoOrigen, Indice, LineasImpresas)
                    Debug.Print("[Coord X, Y]: " & oRow("PosicionX").ToString & ", " & oRow("PosicionY").ToString)
                    Debug.Print("[Interlineado Origen]: " & InterlineadoOrigen)
                    Debug.Print("[Interlineado]: " & Interlineado)

                    'Si las lineas impresas aumentaron, entonces aumentar el interlineado
                    If LineasImpresas > 1 Then
                        Interlineado = (15 * (LineasImpresas - 1))
                    End If

                    LineasImpresas = 0

                Next

                InterlineadoOrigen = InterlineadoOrigen + Interlineado + 15
                Indice = Indice + 1

            Next

        End If

    End Sub

    Private Sub ImprimirCampo(e As System.Drawing.Printing.PrintPageEventArgs, Camporow As DataRow, Detalle As Boolean, Optional ByRef Interlineado As Integer = 0, Optional Indice As Integer = 0, Optional ByRef LineasImpresas As Integer = 0)

        'Alineacion
        'Izquierda, Derecha, Centrada
        Dim Tipo As New StringFormat
        Select Case Camporow("Alineacion").ToString
            Case "Izquierda"
                Tipo.Alignment = StringAlignment.Near
            Case "Derecha"
                Tipo.Alignment = StringAlignment.Far
            Case "Centrada"
                Tipo.Alignment = StringAlignment.Center
        End Select

        Dim PosicionX As Integer = Camporow("PosicionX")
        Dim PosicionY As Integer = Camporow("PosicionY")

        If Detalle = True Then
            If Indice > 0 Then
                PosicionY = PosicionY + Interlineado
            End If
        End If

        If Camporow("PuedeCrecer") = False Then
            'e.Graphics.DrawString(EstablecerValor(Camporow, Detalle, Indice), EstablecerFormato(Camporow), Brushes.Black, PosicionX, PosicionY, Tipo)
            Imprimir(e, EstablecerValor(Camporow, Detalle, Indice), EstablecerFormato(Camporow), Brushes.Black, PosicionX, PosicionY, Tipo, Camporow)
        Else
            Dim Palabras() As String = EstablecerValor(Camporow, Detalle, Indice).Split(" ")
            Dim CantidadLineas As Integer = CInt(Camporow("Lineas").ToString)
            Dim Lineas(CantidadLineas - 1) As String
            Dim LineaActual As Integer = 0

            'Inicializar matriz
            For c As Integer = 0 To Lineas.Length - 1
                Lineas(c) = ""
            Next

            For c As Integer = 0 To Palabras.Length - 1
                If (Palabras(c).Length + Lineas(LineaActual).Length + 1) < Camporow("Largo") Then
                    Lineas(LineaActual) = Lineas(LineaActual) & " " & Palabras(c)
                Else

                    LineaActual = LineaActual + 1
                    ReDim Preserve Lineas(LineaActual)
                    Lineas(LineaActual) = Palabras(c)
                End If
            Next

            'Imprimir
            For c As Integer = 0 To Lineas.Length - 1
                If Lineas(c).Length > 0 Then
                    'e.Graphics.DrawString(Lineas(c), EstablecerFormato(Camporow), Brushes.Black, PosicionX, PosicionY, Tipo)

                    If c = 0 And Camporow("MargenPrimeraLinea") > 0 Then
                        Imprimir(e, Lineas(c), EstablecerFormato(Camporow), Brushes.Black, PosicionX + Camporow("MargenPrimeraLinea"), PosicionY, Tipo, Camporow)
                    Else
                        Imprimir(e, Lineas(c), EstablecerFormato(Camporow), Brushes.Black, PosicionX, PosicionY, Tipo, Camporow)
                    End If

                    PosicionY = PosicionY + Camporow("Interlineado")
                    LineasImpresas = LineasImpresas + 1
                End If

            Next

        End If


    End Sub

    Private Sub Imprimir(e As System.Drawing.Printing.PrintPageEventArgs, valor As String, Estilo As System.Drawing.Font, Tipo As System.Drawing.Brush, PosicionX As Integer, PosicionY As Integer, Formato As System.Drawing.StringFormat, CampoRow As DataRow)

        e.Graphics.DrawString(valor, Estilo, Tipo, PosicionX, PosicionY, Formato)
        Debug.Print("[Campo, valor:] " & CampoRow("Campo") & ", " & valor)

        'Imprimir Copias
        If dtFormularioImpresionCopia Is Nothing Then
            Exit Sub
        End If

        Dim PosicionXOrigen As Integer = PosicionX
        Dim PosicionYOrigen As Integer = PosicionY

        For Each oRow As DataRow In dtFormularioImpresionCopia.Rows
            PosicionX = PosicionXOrigen + oRow("PosicionX")
            PosicionY = PosicionYOrigen + oRow("PosicionY")
            e.Graphics.DrawString(valor, Estilo, Tipo, PosicionX, PosicionY, Formato)
        Next

    End Sub

    Private Function EstablecerFormato(CampoRow As DataRow) As Font

        EstablecerFormato = New Font("Arial", 8, FontStyle.Regular)

        Try

            Dim FS As FontStyle = FontStyle.Regular
            Select Case CampoRow("Formato").ToString
                Case "Normal"
                    FS = FontStyle.Regular
                Case "Negrita"
                    FS = FontStyle.Bold
                Case "Cursiva"
                    FS = FontStyle.Italic
            End Select

            Dim Retorno As Font = New Font(CampoRow("TipoLetra").ToString, CampoRow("TamañoLetra"), FS)
            Return Retorno

        Catch ex As Exception
            Exit Function
        End Try

    End Function

    Private Function EstablecerValor(CampoRow As DataRow, Detalle As Boolean, Optional Indice As Integer = 0) As String

        EstablecerValor = "xxxx"

        If Test Then

            EstablecerValor = CampoRow("Valor").ToString

            If EstablecerValor = "" Then
                EstablecerValor = CampoRow("Campo").ToString
            End If

        Else
            If Detalle = False Then
                EstablecerValor = dtCabecera.Rows(0)(CampoRow("NombreCampo").ToString).ToString
            Else
                EstablecerValor = dtDetalle.Rows(Indice)(CampoRow("NombreCampo").ToString).ToString
            End If

        End If

        'Numerico
        If CampoRow("Numerico") = True Then

            If IsNumeric(EstablecerValor) = False Then
                Exit Function
            End If

            EstablecerValor = CSistema.FormatoNumero(EstablecerValor)

        End If

        'Largo
        If CampoRow("Largo") > 0 Then
            If CampoRow("PuedeCrecer") = False Then
                If EstablecerValor.Length > CampoRow("Largo") Then
                    EstablecerValor = EstablecerValor.Substring(0, CampoRow("Largo"))
                End If
            End If
        End If

        Debug.Print("[Campo, valor:] " & CampoRow("Campo") & ", " & EstablecerValor)

    End Function


End Class

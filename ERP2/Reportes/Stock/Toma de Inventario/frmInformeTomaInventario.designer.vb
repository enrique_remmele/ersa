﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInformeTomaInventario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkCategoria = New ERP.ocxCHK()
        Me.cbxCategoria = New ERP.ocxCBX()
        Me.chkMarca = New ERP.ocxCHK()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkActivo = New ERP.ocxCHK()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkCategoria)
        Me.gbxFiltro.Controls.Add(Me.cbxCategoria)
        Me.gbxFiltro.Controls.Add(Me.chkMarca)
        Me.gbxFiltro.Controls.Add(Me.cbxMarca)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 11)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(348, 156)
        Me.gbxFiltro.TabIndex = 5
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkCategoria
        '
        Me.chkCategoria.BackColor = System.Drawing.Color.Transparent
        Me.chkCategoria.Color = System.Drawing.Color.Empty
        Me.chkCategoria.Location = New System.Drawing.Point(11, 128)
        Me.chkCategoria.Name = "chkCategoria"
        Me.chkCategoria.Size = New System.Drawing.Size(114, 21)
        Me.chkCategoria.SoloLectura = False
        Me.chkCategoria.TabIndex = 26
        Me.chkCategoria.Texto = "Categoria"
        Me.chkCategoria.Valor = False
        '
        'cbxCategoria
        '
        Me.cbxCategoria.CampoWhere = "IDCategoria"
        Me.cbxCategoria.CargarUnaSolaVez = False
        Me.cbxCategoria.DataDisplayMember = "Descripcion"
        Me.cbxCategoria.DataFilter = Nothing
        Me.cbxCategoria.DataOrderBy = "Descripcion"
        Me.cbxCategoria.DataSource = "Categoria"
        Me.cbxCategoria.DataValueMember = "ID"
        Me.cbxCategoria.Enabled = False
        Me.cbxCategoria.FormABM = Nothing
        Me.cbxCategoria.Indicaciones = Nothing
        Me.cbxCategoria.Location = New System.Drawing.Point(130, 128)
        Me.cbxCategoria.Name = "cbxCategoria"
        Me.cbxCategoria.SeleccionObligatoria = False
        Me.cbxCategoria.Size = New System.Drawing.Size(213, 21)
        Me.cbxCategoria.SoloLectura = False
        Me.cbxCategoria.TabIndex = 27
        Me.cbxCategoria.Texto = ""
        '
        'chkMarca
        '
        Me.chkMarca.BackColor = System.Drawing.Color.Transparent
        Me.chkMarca.Color = System.Drawing.Color.Empty
        Me.chkMarca.Location = New System.Drawing.Point(11, 65)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Size = New System.Drawing.Size(114, 21)
        Me.chkMarca.SoloLectura = False
        Me.chkMarca.TabIndex = 24
        Me.chkMarca.Texto = "Marca:"
        Me.chkMarca.Valor = False
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = "IDMarca"
        Me.cbxMarca.CargarUnaSolaVez = False
        Me.cbxMarca.DataDisplayMember = "Descripcion"
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = "Descripcion"
        Me.cbxMarca.DataSource = "VMarca"
        Me.cbxMarca.DataValueMember = "ID"
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = Nothing
        Me.cbxMarca.Location = New System.Drawing.Point(130, 65)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(213, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 25
        Me.cbxMarca.Texto = "ADES"
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(11, 44)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(114, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 22
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "VLinea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(130, 44)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(213, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 23
        Me.cbxLinea.Texto = "BEBIDAS"
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(11, 23)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 18
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(130, 23)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 19
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(11, 105)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(114, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 10
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(130, 107)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(213, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 11
        Me.cbxDeposito.Texto = "AVERIADOS"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(11, 85)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 8
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(130, 86)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 9
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkActivo)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 173)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(348, 120)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkActivo
        '
        Me.chkActivo.BackColor = System.Drawing.Color.Transparent
        Me.chkActivo.Color = System.Drawing.Color.Empty
        Me.chkActivo.Location = New System.Drawing.Point(145, 80)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(114, 21)
        Me.chkActivo.SoloLectura = False
        Me.chkActivo.TabIndex = 26
        Me.chkActivo.Texto = "Sólo Activos:"
        Me.chkActivo.Valor = False
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(165, 32)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(9, 32)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(14, 16)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(9, 81)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 7
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(14, 65)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 6
        Me.lblRanking.Text = "Ranking:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(246, 317)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 9
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(73, 317)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 8
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmInformeTomaInventario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(377, 356)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmInformeTomaInventario"
        Me.Text = "Toma de Inventario Físico"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkActivo As ERP.ocxCHK
    Friend WithEvents chkCategoria As ERP.ocxCHK
    Friend WithEvents cbxCategoria As ERP.ocxCBX
    Friend WithEvents chkMarca As ERP.ocxCHK
    Friend WithEvents cbxMarca As ERP.ocxCBX
End Class

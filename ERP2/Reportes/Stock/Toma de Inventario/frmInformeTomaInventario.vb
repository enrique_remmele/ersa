﻿Imports ERP.Reporte
Public Class frmInformeTomaInventario

    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim TipoFiltro As String = ""
    Dim Filtro As String = ""

    'VARIABLES
    Dim Titulo As String = "PLANILLA DE TOMA DE INVENTARIO FISICO"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        cbxCategoria.SeleccionMultiple = True
        cbxDeposito.SeleccionMultiple = True
        cbxLinea.SeleccionMultiple = True
        cbxMarca.SeleccionMultiple = True
        cbxTipoProducto.SeleccionMultiple = True
        cbxSucursal.SeleccionMultiple = True


        chkActivo.chk.Checked = True
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por producto
        If chkTipoProducto.chk.Checked = True Then
            TipoFiltro = "Prod."
            Filtro = cbxTipoProducto.cbx.Text
        End If

        'Filtrar por linea
        If chkLinea.chk.Checked = True Then
            TipoFiltro = "Linea:"
            Filtro = cbxLinea.cbx.Text
        End If

        'Filtrar por marca
        If chkMarca.chk.Checked = True Then
            TipoFiltro = "Marca:"
            Filtro = cbxMarca.cbx.Text
        End If

        'Filtrar por sucursal
        If chkSucursal.chk.Checked = True Then
            TipoFiltro = "Marca:"
            Filtro = cbxSucursal.cbx.Text
        End If
        If chkTipoProducto.chk.Checked = False And chkLinea.chk.Checked = False And chkMarca.chk.Checked = False And chkSucursal.chk.Checked = False Then
            TipoFiltro = ""
            Filtro = ""
        End If

        If chkActivo.chk.Checked = True Then
            Where = " Where Estado=1"
        Else
            Where = " Where Estado=0"
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro)
        CReporte.PlanillaTomaInventarioFisico(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VProducto", "Select Top(0) Marca, SubLinea, SubLinea2, Linea From VProducto ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmExistenciaValorizada_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkLinea_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    Private Sub chkMarca_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCategoria_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCategoria.Load

    End Sub

    Private Sub chkCategoria_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCategoria.PropertyChanged
        cbxCategoria.Enabled = value
    End Sub
End Class
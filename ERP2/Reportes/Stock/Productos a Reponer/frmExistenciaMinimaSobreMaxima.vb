﻿Imports ERP.Reporte
Public Class frmExistenciaMinimaSobreMaxima

    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "EXISTENCIA BAJO STOCK MINIMO"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Existencia
        cbxExistencia.Items.Add("Bajo mínimo del producto")
        cbxExistencia.Items.Add("Bajo critico del producto")
        cbxExistencia.Items.Add("Negativa")
        cbxExistencia.Items.Add("Productos sin existencia")
        cbxExistencia.Items.Add("Bajo una cantidad ingresada")
        cbxExistencia.Items.Add("Sobre una cantidad ingresada")

        cbxCategoria.SeleccionMultiple = True
        cbxDeposito.SeleccionMultiple = True
        cbxLinea.SeleccionMultiple = True
        cbxMarca.SeleccionMultiple = True
        cbxProducto.SeleccionMultiple = True
        cbxSucursal.SeleccionMultiple = True


        chkActivo.chk.Checked = True
        txtCantidad.Enabled = False
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkActivo.chk.Checked = True Then
            Where = " Where Estado=1"
        Else
            Where = " Where Estado=0"
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CambiarOrdenacion()

        Select Case cbxExistencia.SelectedIndex

            Case 0
                Where = Where + " and Existencia < ExistenciaMinima "
                TipoInforme = cbxExistencia.Text
                CReporte.ExistenciaBajoMinimoSobreMaximo(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
            Case 1
                Where = Where + " and Existencia < ExistenciaCritica "
                TipoInforme = cbxExistencia.Text
                CReporte.ExistenciaBajoMinimoSobreMaximo(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
            Case 2
                Where = Where + " and Existencia < 0 "
                TipoInforme = cbxExistencia.Text
                CReporte.ExistenciaBajoMinimoSobreMaximo(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
            Case 3
                Where = Where + " and Existencia = 0 "
                TipoInforme = cbxExistencia.Text
                CReporte.ExistenciaBajoMinimoSobreMaximo(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
            Case 4
                Where = Where + " and Existencia < " & txtCantidad.txt.Text
                TipoInforme = cbxExistencia.Text
                CReporte.ExistenciaBajoMinimoSobreMaximo(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
            Case 5
                Where = Where + " and Existencia > " & txtCantidad.txt.Text
                TipoInforme = cbxExistencia.Text
                CReporte.ExistenciaBajoMinimoSobreMaximo(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        End Select

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VProducto", "Select Top(0) Producto, Linea, Marca, Deposito From VProducto ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmExistenciaValorizada_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxProducto.Enabled = value
    End Sub

    Private Sub chkLinea_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    Private Sub chkMarca_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkCategoria_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCategoria.PropertyChanged
        cbxCategoria.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxExistencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxExistencia.SelectedIndexChanged
        Select Case cbxExistencia.SelectedIndex
            Case Is = 0
                txtCantidad.Enabled = False
            Case Is = 1
                txtCantidad.Enabled = False
            Case Is = 2
                txtCantidad.Enabled = False
            Case Is = 3
                txtCantidad.Enabled = False
            Case Is = 4
                txtCantidad.Enabled = True
            Case Is = 5
                txtCantidad.Enabled = True
        End Select
    End Sub

    Private Sub chkCategoria_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCategoria.Load

    End Sub

   
End Class
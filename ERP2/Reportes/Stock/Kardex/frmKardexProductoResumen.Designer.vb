﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmKardexProductoResumen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkExcluirSubLinea2 = New ERP.ocxCHK()
        Me.chkExcluirSubLinea = New ERP.ocxCHK()
        Me.chkExcluirLinea = New ERP.ocxCHK()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkExcluirTipoProducto = New ERP.ocxCHK()
        Me.chkSubLinea = New ERP.ocxCHK()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.chkSubLinea2 = New ERP.ocxCHK()
        Me.cbxSubLinea2 = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.DatoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.gbxFiltro.SuspendLayout()
        CType(Me.DatoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(112, 134)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(312, 70)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 9
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(11, 133)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(95, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 8
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(11, 47)
        Me.chkLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(95, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 2
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "VLinea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.dtSeleccionado = Nothing
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(112, 48)
        Me.cbxLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionMultiple = False
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(312, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 3
        Me.cbxLinea.Texto = "BEBIDAS"
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(11, 23)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(95, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 0
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "E.IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(112, 23)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(312, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 1
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkExcluirSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirLinea)
        Me.gbxFiltro.Controls.Add(Me.Label3)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Location = New System.Drawing.Point(10, 14)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(468, 220)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkExcluirSubLinea2
        '
        Me.chkExcluirSubLinea2.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirSubLinea2.Color = System.Drawing.Color.Empty
        Me.chkExcluirSubLinea2.Location = New System.Drawing.Point(436, 98)
        Me.chkExcluirSubLinea2.Name = "chkExcluirSubLinea2"
        Me.chkExcluirSubLinea2.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirSubLinea2.SoloLectura = False
        Me.chkExcluirSubLinea2.TabIndex = 43
        Me.chkExcluirSubLinea2.Texto = ""
        Me.chkExcluirSubLinea2.Valor = False
        '
        'chkExcluirSubLinea
        '
        Me.chkExcluirSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirSubLinea.Color = System.Drawing.Color.Empty
        Me.chkExcluirSubLinea.Location = New System.Drawing.Point(436, 73)
        Me.chkExcluirSubLinea.Name = "chkExcluirSubLinea"
        Me.chkExcluirSubLinea.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirSubLinea.SoloLectura = False
        Me.chkExcluirSubLinea.TabIndex = 42
        Me.chkExcluirSubLinea.Texto = ""
        Me.chkExcluirSubLinea.Valor = False
        '
        'chkExcluirLinea
        '
        Me.chkExcluirLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirLinea.Color = System.Drawing.Color.Empty
        Me.chkExcluirLinea.Location = New System.Drawing.Point(436, 47)
        Me.chkExcluirLinea.Name = "chkExcluirLinea"
        Me.chkExcluirLinea.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirLinea.SoloLectura = False
        Me.chkExcluirLinea.TabIndex = 41
        Me.chkExcluirLinea.Texto = ""
        Me.chkExcluirLinea.Valor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(429, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 12)
        Me.Label3.TabIndex = 39
        Me.Label3.Text = "Excluir"
        '
        'chkExcluirTipoProducto
        '
        Me.chkExcluirTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoProducto.Location = New System.Drawing.Point(436, 23)
        Me.chkExcluirTipoProducto.Name = "chkExcluirTipoProducto"
        Me.chkExcluirTipoProducto.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoProducto.SoloLectura = False
        Me.chkExcluirTipoProducto.TabIndex = 40
        Me.chkExcluirTipoProducto.Texto = ""
        Me.chkExcluirTipoProducto.Valor = False
        '
        'chkSubLinea
        '
        Me.chkSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea.Color = System.Drawing.Color.Empty
        Me.chkSubLinea.Location = New System.Drawing.Point(11, 73)
        Me.chkSubLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSubLinea.Name = "chkSubLinea"
        Me.chkSubLinea.Size = New System.Drawing.Size(95, 21)
        Me.chkSubLinea.SoloLectura = False
        Me.chkSubLinea.TabIndex = 4
        Me.chkSubLinea.Texto = "SubLinea:"
        Me.chkSubLinea.Valor = False
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = "IDSubLinea"
        Me.cbxSubLinea.CargarUnaSolaVez = False
        Me.cbxSubLinea.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = "Descripcion"
        Me.cbxSubLinea.DataSource = "VSubLinea"
        Me.cbxSubLinea.DataValueMember = "ID"
        Me.cbxSubLinea.dtSeleccionado = Nothing
        Me.cbxSubLinea.Enabled = False
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = Nothing
        Me.cbxSubLinea.Location = New System.Drawing.Point(112, 73)
        Me.cbxSubLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionMultiple = False
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(312, 21)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 5
        Me.cbxSubLinea.Texto = "BEBIDAS"
        '
        'chkSubLinea2
        '
        Me.chkSubLinea2.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea2.Color = System.Drawing.Color.Empty
        Me.chkSubLinea2.Location = New System.Drawing.Point(11, 98)
        Me.chkSubLinea2.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSubLinea2.Name = "chkSubLinea2"
        Me.chkSubLinea2.Size = New System.Drawing.Size(95, 21)
        Me.chkSubLinea2.SoloLectura = False
        Me.chkSubLinea2.TabIndex = 6
        Me.chkSubLinea2.Texto = "SubLinea2:"
        Me.chkSubLinea2.Valor = False
        '
        'cbxSubLinea2
        '
        Me.cbxSubLinea2.CampoWhere = "IDSubLinea2"
        Me.cbxSubLinea2.CargarUnaSolaVez = False
        Me.cbxSubLinea2.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea2.DataFilter = Nothing
        Me.cbxSubLinea2.DataOrderBy = "Descripcion"
        Me.cbxSubLinea2.DataSource = "VSubLinea2"
        Me.cbxSubLinea2.DataValueMember = "ID"
        Me.cbxSubLinea2.dtSeleccionado = Nothing
        Me.cbxSubLinea2.Enabled = False
        Me.cbxSubLinea2.FormABM = Nothing
        Me.cbxSubLinea2.Indicaciones = Nothing
        Me.cbxSubLinea2.Location = New System.Drawing.Point(112, 98)
        Me.cbxSubLinea2.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSubLinea2.Name = "cbxSubLinea2"
        Me.cbxSubLinea2.SeleccionMultiple = False
        Me.cbxSubLinea2.SeleccionObligatoria = False
        Me.cbxSubLinea2.Size = New System.Drawing.Size(312, 21)
        Me.cbxSubLinea2.SoloLectura = False
        Me.cbxSubLinea2.TabIndex = 7
        Me.cbxSubLinea2.Texto = "BEBIDAS"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(618, 238)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(86, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(15, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "al"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 5, 14, 20, 44, 687)
        Me.txtHasta.Location = New System.Drawing.Point(107, 41)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 1
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(7, 22)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(40, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Fecha:"
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 5, 14, 20, 44, 687)
        Me.txtDesde.Location = New System.Drawing.Point(6, 41)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 0
        '
        'DatoBindingSource
        '
        Me.DatoBindingSource.DataSource = GetType(ERP.Dato)
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(512, 238)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(100, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(484, 11)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(241, 223)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'frmKardexProductoResumen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(735, 268)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmKardexProductoResumen"
        Me.Text = "frmKardexProductoResumen"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        CType(Me.DatoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents chkProducto As ocxCHK
    Friend WithEvents chkLinea As ocxCHK
    Friend WithEvents cbxLinea As ocxCBX
    Friend WithEvents chkTipoProducto As ocxCHK
    Friend WithEvents cbxTipoProducto As ocxCBX
    Friend WithEvents gbxFiltro As GroupBox
    Friend WithEvents btnCerrar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents lblPeriodo As Label
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents DatoBindingSource As BindingSource
    Friend WithEvents btnInforme As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents chkSubLinea As ocxCHK
    Friend WithEvents cbxSubLinea As ocxCBX
    Friend WithEvents chkSubLinea2 As ocxCHK
    Friend WithEvents cbxSubLinea2 As ocxCBX
    Friend WithEvents chkExcluirSubLinea2 As ocxCHK
    Friend WithEvents chkExcluirSubLinea As ocxCHK
    Friend WithEvents chkExcluirLinea As ocxCHK
    Friend WithEvents Label3 As Label
    Friend WithEvents chkExcluirTipoProducto As ocxCHK
End Class

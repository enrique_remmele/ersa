﻿Imports ERP.Reporte
Public Class frmKardexProductoDetalle
    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String
    Dim TipoInforme As String

    Private listaOrdenadoPor As List(Of Dato)

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        txtProducto.Conectar()

    End Sub

    Sub Listar()

        Dim Where As String = " Where Fecha between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False).ToString & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False).ToString & "' "
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Titulo = "KARDEX Detallado "
        TipoInforme = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Establecemos los filtros

        If txtProducto.Registro IsNot Nothing Then
            Where = Where & " and IDProducto = " & txtProducto.Registro("ID").ToString
        Else
            MessageBox.Show("Debe Seleccionar un producto!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

        OrderBy = " Order by " & txtDesde.txt.Text

        'Ranking
        CReporte.ArmarSubtitulo(TipoInforme, gbxFiltro)

        TipoInforme = TipoInforme & " Desde: " & txtDesde.txt.Text & " Hasta: " & txtHasta.txt.Text

        CReporte.ListadoKardexProductoDetalle(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador)

    End Sub

    Private Sub frmExistenciaValorizada_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(sender As Object, e As EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()

    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmKardexProductoResumen
    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String
    Dim TipoInforme As String

    Private listaOrdenadoPor As List(Of Dato)

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        txtProducto.Conectar()
        txtProducto.Enabled = False

    End Sub

    Sub Listar()

        Dim Where As String = " @Fecha1 = '" & CSistema.FormatoFechaBaseDatos(CDate(txtDesde.txt.Text), True, False).ToString & "' , @Fecha2 = '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False).ToString & "'"
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Titulo = "KARDEX RESUMEN"
        TipoInforme = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Establecemos los filtros
        If chkProducto.chk.Checked Then
            If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
                Where = Where & " ,@IDProducto = " & txtProducto.Registro("ID").ToString
            End If
        End If

        If chkTipoProducto.chk.Checked Then


            If chkExcluirTipoProducto.chk.Checked Then
                Where = Where & " ,@IDTipoProductoExcluir = " & cbxTipoProducto.GetValue.ToString
            Else
                Where = Where & " ,@IDTipoProducto = " & cbxTipoProducto.GetValue.ToString
            End If

        End If

        If chkLinea.chk.Checked Then
            If chkExcluirLinea.chk.Checked Then
                Where = Where & " ,@IDLineaExcluir = " & cbxLinea.GetValue.ToString
            Else
                Where = Where & " ,@IDLinea = " & cbxLinea.GetValue.ToString
            End If

        End If

        If chkSubLinea.chk.Checked Then
            If chkExcluirSubLinea.chk.Checked Then
                Where = Where & " ,@IDSubLineaExcluir = " & cbxSubLinea.GetValue.ToString
            Else
                Where = Where & " ,@IDSubLinea = " & cbxSubLinea.GetValue.ToString
            End If
        End If

        If chkSubLinea2.chk.Checked Then
            If chkExcluirSubLinea2.chk.Checked Then
                Where = Where & " ,@IDSubLinea2Excluir = " & cbxSubLinea2.GetValue.ToString
            Else
                Where = Where & " ,@IDSubLinea2 = " & cbxSubLinea2.GetValue.ToString
            End If

        End If

        'Ranking
        CReporte.ArmarSubtitulo(TipoInforme, gbxFiltro)

        TipoInforme = TipoInforme & " Desde: " & txtDesde.txt.Text & " Hasta: " & txtHasta.txt.Text

        CReporte.ListadoKardexProductoResumen(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador)

    End Sub



    Private Sub frmExistenciaValorizada_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(sender As Object, e As EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkLinea_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    Private Sub chkSubLinea_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSubLinea.PropertyChanged
        cbxSubLinea.Enabled = value
    End Sub

    Private Sub chkSubLinea2_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSubLinea2.PropertyChanged
        cbxSubLinea2.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class
﻿Imports ERP.Reporte

Public Class frmListadoOperacionProducto

    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE OPERACIONES POR PRODUCTO"
    Dim TipoInforme As String

    Private listOrdenadoPor As List(Of Dato)


    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()

        cbxOrdenadoPor.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        cbxOrdenadoPor.Items.Add("OPERACION")
        cbxOrdenadoPor.Items.Add("PRODUCTO")
        txtProducto.Enabled = False
        txtProducto.Conectar()

        'Tipos de Menu
        cbxMenu.cbx.Items.Add("Movimiento de Stock")
        cbxMenu.cbx.Items.Add("Descarga de Stock")
        cbxMenu.cbx.Items.Add("Consumo de Combustible")
        cbxMenu.cbx.Items.Add("Materia Prima Balanceado")
        cbxMenu.cbx.Items.Add("Descarga de Compras")
        cbxMenu.cbx.SelectedIndex = 0

        'Seleccion Multiple
        cbxTipoProducto.SeleccionMultiple = True
        cbxTipoComprobante.SeleccionMultiple = True

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim Filtros As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

siguiente:

        Next

        If Not chkAnulado.Valor Then
            Where = Where & " And Anulado = 0"
        End If
        WhereDetalle = Where

        'Establecemos el Orden
        Select Case cbxOrdenadoPor.Text
            Case "FECHA"
                OrderBy = " Order By Fecha " & cbxEnForma.cbx.Text
                Filtros = " Ordenado por Fecha " & IIf(cbxEnForma.cbx.Text <> "", IIf(cbxEnForma.cbx.Text = "ASC", "de forma ascendente.", "de forma descendente").ToLower, "")
            Case "OPERACION"
                OrderBy = " Order By Operacion " & cbxEnForma.cbx.Text
                Filtros = " Ordenado por Operacion " & IIf(cbxEnForma.cbx.Text <> "", IIf(cbxEnForma.cbx.Text = "ASC", "de forma ascendente.", "de forma descendente").ToLower, "")
            Case "PRODUCTO"
                OrderBy = " Order By Producto " & cbxEnForma.cbx.Text
                Filtros = " Ordenado por Producto " & IIf(cbxEnForma.cbx.Text <> "", IIf(cbxEnForma.cbx.Text = "ASC", "de forma ascendente.", "de forma descendente").ToLower, "")
        End Select



        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
            Where = IIf(String.IsNullOrEmpty(Where), " where IDProducto = " + txtProducto.Registro("Id").ToString, Where + " and IDProducto = " + txtProducto.Registro("Id").ToString)
        End If

        Dim vMovimiento As String = ""
        If chkMovimientosStock.Valor = True Then
            If cbxMenu.cbx.SelectedIndex = 0 Then
                vMovimiento = " and MovimientoStock = 'True' "
            ElseIf cbxMenu.cbx.SelectedIndex = 1 Then
                vMovimiento = " and DescargaStock = 'True' "
            ElseIf cbxMenu.cbx.SelectedIndex = 2 Then
                vMovimiento = " and MovimientoCombustible = 'True' "
            ElseIf cbxMenu.cbx.SelectedIndex = 3 Then
                vMovimiento = " and MovimientoMateriaPrima = 'True' "
            ElseIf cbxMenu.cbx.SelectedIndex = 4 Then
                vMovimiento = " and DescargaCompra = 'True' "
            End If
        End If
        Where = Where & vMovimiento

        CReporte.ListadoOperacionProducto(frm, Titulo, Filtros, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        'cbxOrdenadoPor.cbx.Items.Clear()


        'dt = CData.GetStructure("VMovimiento ", "Select Top(0)Fecha,Numero From VMovimiento")


        'If dt Is Nothing Then
        '    Exit Sub
        'End If

        'For i As Integer = 0 To dt.Columns.Count - 1
        '    cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        'Next

        'listOrdenadoPor = New List(Of Dato)
        'Dim obj As Dato
        'obj = New Dato
        'obj.idString = "FechaTransaccion, Numero, TipoComprobante, NroComprobante"
        'obj.descripcion = "Fecha"
        'listOrdenadoPor.Add(obj)
        'obj = New Dato
        'obj.idString = "TipoComprobante ASC, Fecha"
        'obj.descripcion = "Tipo de comprobante"
        'listOrdenadoPor.Add(obj)
        'obj = New Dato
        'obj.idString = "Numero"
        'obj.descripcion = "Numero"
        'listOrdenadoPor.Add(obj)
        'OrdenadoPorBindingSource.DataSource = listOrdenadoPor


    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkTipoOperacion_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoOperacion.PropertyChanged
        cbxTipoOperacion.Enabled = value
    End Sub
    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkDepSalida_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepSalida.PropertyChanged
        cbxDepSalida.Enabled = value
    End Sub

    Private Sub chkDepEntrada_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepEntrada.PropertyChanged
        cbxDepositoEntrada.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkMovimientosStock_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMovimientosStock.PropertyChanged
        cbxMenu.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub
End Class




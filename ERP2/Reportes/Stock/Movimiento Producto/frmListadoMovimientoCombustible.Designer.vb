﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoMovimientoCombustible
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkCamion = New ERP.ocxCHK()
        Me.chkChofer = New ERP.ocxCHK()
        Me.cbxCamion = New ERP.ocxCBX()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxDepositoEntrada = New ERP.ocxCBX()
        Me.chkDepEntrada = New ERP.ocxCHK()
        Me.cbxDepSalida = New ERP.ocxCBX()
        Me.chkDepSalida = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.chkProducto = New ERP.ocxCHK()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(439, 239)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(150, 23)
        Me.btnInforme.TabIndex = 6
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(595, 239)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxTipoInforme)
        Me.GroupBox2.Controls.Add(Me.lblInforme)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(439, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(259, 211)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.dtSeleccionado = Nothing
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(9, 83)
        Me.cbxTipoInforme.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionMultiple = False
        Me.cbxTipoInforme.SeleccionObligatoria = True
        Me.cbxTipoInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 4
        Me.cbxTipoInforme.Texto = ""
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(6, 67)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(45, 13)
        Me.lblInforme.TabIndex = 3
        Me.lblInforme.Text = "Informe:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 3, 9, 15, 15, 138)
        Me.txtHasta.Location = New System.Drawing.Point(89, 35)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 3, 9, 15, 15, 154)
        Me.txtDesde.Location = New System.Drawing.Point(9, 35)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkCamion)
        Me.gbxFiltro.Controls.Add(Me.chkChofer)
        Me.gbxFiltro.Controls.Add(Me.cbxCamion)
        Me.gbxFiltro.Controls.Add(Me.cbxChofer)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxDepositoEntrada)
        Me.gbxFiltro.Controls.Add(Me.chkDepEntrada)
        Me.gbxFiltro.Controls.Add(Me.cbxDepSalida)
        Me.gbxFiltro.Controls.Add(Me.chkDepSalida)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Location = New System.Drawing.Point(10, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(423, 211)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkCamion
        '
        Me.chkCamion.BackColor = System.Drawing.Color.Transparent
        Me.chkCamion.Color = System.Drawing.Color.Empty
        Me.chkCamion.Location = New System.Drawing.Point(6, 178)
        Me.chkCamion.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCamion.Name = "chkCamion"
        Me.chkCamion.Size = New System.Drawing.Size(128, 24)
        Me.chkCamion.SoloLectura = False
        Me.chkCamion.TabIndex = 27
        Me.chkCamion.Texto = "Camion:"
        Me.chkCamion.Valor = False
        '
        'chkChofer
        '
        Me.chkChofer.BackColor = System.Drawing.Color.Transparent
        Me.chkChofer.Color = System.Drawing.Color.Empty
        Me.chkChofer.Location = New System.Drawing.Point(6, 154)
        Me.chkChofer.Margin = New System.Windows.Forms.Padding(4)
        Me.chkChofer.Name = "chkChofer"
        Me.chkChofer.Size = New System.Drawing.Size(128, 24)
        Me.chkChofer.SoloLectura = False
        Me.chkChofer.TabIndex = 26
        Me.chkChofer.Texto = "Chofer:"
        Me.chkChofer.Valor = False
        '
        'cbxCamion
        '
        Me.cbxCamion.CampoWhere = "IDCamion"
        Me.cbxCamion.CargarUnaSolaVez = False
        Me.cbxCamion.DataDisplayMember = "Descripcion"
        Me.cbxCamion.DataFilter = Nothing
        Me.cbxCamion.DataOrderBy = Nothing
        Me.cbxCamion.DataSource = "Camion"
        Me.cbxCamion.DataValueMember = "ID"
        Me.cbxCamion.dtSeleccionado = Nothing
        Me.cbxCamion.Enabled = False
        Me.cbxCamion.FormABM = Nothing
        Me.cbxCamion.Indicaciones = Nothing
        Me.cbxCamion.Location = New System.Drawing.Point(140, 181)
        Me.cbxCamion.Name = "cbxCamion"
        Me.cbxCamion.SeleccionMultiple = False
        Me.cbxCamion.SeleccionObligatoria = True
        Me.cbxCamion.Size = New System.Drawing.Size(269, 21)
        Me.cbxCamion.SoloLectura = False
        Me.cbxCamion.TabIndex = 25
        Me.cbxCamion.Texto = ""
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = "IDChofer"
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = Nothing
        Me.cbxChofer.DataSource = "Chofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.dtSeleccionado = Nothing
        Me.cbxChofer.Enabled = False
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(140, 154)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionMultiple = False
        Me.cbxChofer.SeleccionObligatoria = True
        Me.cbxChofer.Size = New System.Drawing.Size(269, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 23
        Me.cbxChofer.Texto = ""
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(140, 20)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(269, 20)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 13
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = ""
        Me.cbxSucursal.DataSource = "Sucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(140, 46)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(269, 23)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 11
        Me.cbxSucursal.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(6, 45)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(128, 23)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 10
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxDepositoEntrada
        '
        Me.cbxDepositoEntrada.CampoWhere = "IDDepositoEntrada"
        Me.cbxDepositoEntrada.CargarUnaSolaVez = False
        Me.cbxDepositoEntrada.DataDisplayMember = "Suc-Dep"
        Me.cbxDepositoEntrada.DataFilter = Nothing
        Me.cbxDepositoEntrada.DataOrderBy = ""
        Me.cbxDepositoEntrada.DataSource = "VDeposito"
        Me.cbxDepositoEntrada.DataValueMember = "ID"
        Me.cbxDepositoEntrada.dtSeleccionado = Nothing
        Me.cbxDepositoEntrada.Enabled = False
        Me.cbxDepositoEntrada.FormABM = Nothing
        Me.cbxDepositoEntrada.Indicaciones = Nothing
        Me.cbxDepositoEntrada.Location = New System.Drawing.Point(140, 127)
        Me.cbxDepositoEntrada.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepositoEntrada.Name = "cbxDepositoEntrada"
        Me.cbxDepositoEntrada.SeleccionMultiple = False
        Me.cbxDepositoEntrada.SeleccionObligatoria = False
        Me.cbxDepositoEntrada.Size = New System.Drawing.Size(269, 24)
        Me.cbxDepositoEntrada.SoloLectura = False
        Me.cbxDepositoEntrada.TabIndex = 9
        Me.cbxDepositoEntrada.Texto = ""
        '
        'chkDepEntrada
        '
        Me.chkDepEntrada.BackColor = System.Drawing.Color.Transparent
        Me.chkDepEntrada.Color = System.Drawing.Color.Empty
        Me.chkDepEntrada.Location = New System.Drawing.Point(6, 127)
        Me.chkDepEntrada.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDepEntrada.Name = "chkDepEntrada"
        Me.chkDepEntrada.Size = New System.Drawing.Size(128, 24)
        Me.chkDepEntrada.SoloLectura = False
        Me.chkDepEntrada.TabIndex = 8
        Me.chkDepEntrada.Texto = "Dep. Entrada:"
        Me.chkDepEntrada.Valor = False
        '
        'cbxDepSalida
        '
        Me.cbxDepSalida.CampoWhere = "IDDepositoSalida"
        Me.cbxDepSalida.CargarUnaSolaVez = False
        Me.cbxDepSalida.DataDisplayMember = "Suc-Dep"
        Me.cbxDepSalida.DataFilter = Nothing
        Me.cbxDepSalida.DataOrderBy = ""
        Me.cbxDepSalida.DataSource = "VDeposito"
        Me.cbxDepSalida.DataValueMember = "ID"
        Me.cbxDepSalida.dtSeleccionado = Nothing
        Me.cbxDepSalida.Enabled = False
        Me.cbxDepSalida.FormABM = Nothing
        Me.cbxDepSalida.Indicaciones = Nothing
        Me.cbxDepSalida.Location = New System.Drawing.Point(140, 100)
        Me.cbxDepSalida.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepSalida.Name = "cbxDepSalida"
        Me.cbxDepSalida.SeleccionMultiple = False
        Me.cbxDepSalida.SeleccionObligatoria = False
        Me.cbxDepSalida.Size = New System.Drawing.Size(269, 21)
        Me.cbxDepSalida.SoloLectura = False
        Me.cbxDepSalida.TabIndex = 7
        Me.cbxDepSalida.Texto = ""
        '
        'chkDepSalida
        '
        Me.chkDepSalida.BackColor = System.Drawing.Color.Transparent
        Me.chkDepSalida.Color = System.Drawing.Color.Empty
        Me.chkDepSalida.Location = New System.Drawing.Point(6, 100)
        Me.chkDepSalida.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDepSalida.Name = "chkDepSalida"
        Me.chkDepSalida.Size = New System.Drawing.Size(128, 21)
        Me.chkDepSalida.SoloLectura = False
        Me.chkDepSalida.TabIndex = 6
        Me.chkDepSalida.Texto = "Dep. Salida:"
        Me.chkDepSalida.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = ""
        Me.cbxTipoComprobante.DataSource = "TipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(140, 73)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(269, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 5
        Me.cbxTipoComprobante.Texto = ""
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(6, 73)
        Me.chkTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(128, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 4
        Me.chkTipoComprobante.Texto = "Tipo de comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(6, 19)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(75, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 0
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'frmListadoMovimientoCombustible
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(709, 273)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoMovimientoCombustible"
        Me.Text = "frmListadoMovimientoCombustible"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents chkSucursal As ocxCHK
    Friend WithEvents cbxDepositoEntrada As ocxCBX
    Friend WithEvents chkDepEntrada As ocxCHK
    Friend WithEvents cbxDepSalida As ocxCBX
    Friend WithEvents lblPeriodo As Label
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents chkDepSalida As ocxCHK
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents chkTipoComprobante As ocxCHK
    Friend WithEvents btnInforme As Button
    Friend WithEvents chkProducto As ocxCHK
    Friend WithEvents cbxTipoInforme As ocxCBX
    Friend WithEvents btnCerrar As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lblInforme As Label
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents gbxFiltro As GroupBox
    Friend WithEvents chkCamion As ocxCHK
    Friend WithEvents chkChofer As ocxCHK
    Friend WithEvents cbxCamion As ocxCBX
    Friend WithEvents cbxChofer As ocxCBX
End Class

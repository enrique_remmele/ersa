﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoDevolucionesSinNC
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtNumeroSolicitudCliente = New System.Windows.Forms.TextBox()
        Me.chkNumeroSolicitudCliente = New ERP.ocxCHK()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.ckhUsuario = New ERP.ocxCHK()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkAnulados = New ERP.ocxCHK()
        Me.rdbIncluir = New System.Windows.Forms.RadioButton()
        Me.rdbSoloAnulados = New System.Windows.Forms.RadioButton()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtNumeroSolicitudCliente
        '
        Me.txtNumeroSolicitudCliente.Enabled = False
        Me.txtNumeroSolicitudCliente.Location = New System.Drawing.Point(113, 134)
        Me.txtNumeroSolicitudCliente.Name = "txtNumeroSolicitudCliente"
        Me.txtNumeroSolicitudCliente.Size = New System.Drawing.Size(113, 20)
        Me.txtNumeroSolicitudCliente.TabIndex = 63
        '
        'chkNumeroSolicitudCliente
        '
        Me.chkNumeroSolicitudCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkNumeroSolicitudCliente.Color = System.Drawing.Color.Empty
        Me.chkNumeroSolicitudCliente.Location = New System.Drawing.Point(9, 134)
        Me.chkNumeroSolicitudCliente.Name = "chkNumeroSolicitudCliente"
        Me.chkNumeroSolicitudCliente.Size = New System.Drawing.Size(97, 21)
        Me.chkNumeroSolicitudCliente.SoloLectura = False
        Me.chkNumeroSolicitudCliente.TabIndex = 62
        Me.chkNumeroSolicitudCliente.Texto = "Solicitud Cliente:"
        Me.chkNumeroSolicitudCliente.Valor = False
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(9, 106)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(88, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 40
        Me.chkTipoCliente.Texto = "Tipo Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "vTipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(112, 106)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(329, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 38
        Me.cbxTipoCliente.Texto = "SUPERMERCADO"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(112, 190)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(329, 65)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 36
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(9, 190)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(88, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 37
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(9, 50)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(91, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 32
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(112, 50)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(329, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 33
        Me.cbxTipoProducto.Texto = ""
        '
        'ckhUsuario
        '
        Me.ckhUsuario.BackColor = System.Drawing.Color.Transparent
        Me.ckhUsuario.Color = System.Drawing.Color.Empty
        Me.ckhUsuario.Location = New System.Drawing.Point(9, 78)
        Me.ckhUsuario.Name = "ckhUsuario"
        Me.ckhUsuario.Size = New System.Drawing.Size(75, 21)
        Me.ckhUsuario.SoloLectura = False
        Me.ckhUsuario.TabIndex = 30
        Me.ckhUsuario.Texto = "Usuario:"
        Me.ckhUsuario.Valor = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "vUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(112, 78)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(329, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 31
        Me.cbxUsuario.Texto = "CAST"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(9, 283)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(423, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 19
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 22)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(75, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 20
        Me.chkSucursal.Texto = "Deposito:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDDeposito"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "SucDeposito"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "SucDeposito"
        Me.cbxSucursal.DataSource = "VDeposito"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(112, 22)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(329, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 21
        Me.cbxSucursal.Texto = "CENTRAL"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 262)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(77, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 18
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(605, 312)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 34
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHasta.Location = New System.Drawing.Point(92, 44)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(84, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesde.Location = New System.Drawing.Point(12, 44)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(465, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(203, 294)
        Me.GroupBox2.TabIndex = 32
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Fecha:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(462, 312)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 33
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkAnulados)
        Me.gbxFiltro.Controls.Add(Me.rdbIncluir)
        Me.gbxFiltro.Controls.Add(Me.rdbSoloAnulados)
        Me.gbxFiltro.Controls.Add(Me.txtNumeroSolicitudCliente)
        Me.gbxFiltro.Controls.Add(Me.chkNumeroSolicitudCliente)
        Me.gbxFiltro.Controls.Add(Me.chkTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.ckhUsuario)
        Me.gbxFiltro.Controls.Add(Me.cbxUsuario)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(456, 335)
        Me.gbxFiltro.TabIndex = 31
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkAnulados
        '
        Me.chkAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkAnulados.Color = System.Drawing.Color.Empty
        Me.chkAnulados.Location = New System.Drawing.Point(9, 162)
        Me.chkAnulados.Name = "chkAnulados"
        Me.chkAnulados.Size = New System.Drawing.Size(114, 21)
        Me.chkAnulados.SoloLectura = False
        Me.chkAnulados.TabIndex = 66
        Me.chkAnulados.Texto = "Anulados:"
        Me.chkAnulados.Valor = False
        '
        'rdbIncluir
        '
        Me.rdbIncluir.AutoSize = True
        Me.rdbIncluir.Location = New System.Drawing.Point(141, 164)
        Me.rdbIncluir.Name = "rdbIncluir"
        Me.rdbIncluir.Size = New System.Drawing.Size(53, 17)
        Me.rdbIncluir.TabIndex = 64
        Me.rdbIncluir.TabStop = True
        Me.rdbIncluir.Text = "Incluir"
        Me.rdbIncluir.UseVisualStyleBackColor = True
        '
        'rdbSoloAnulados
        '
        Me.rdbSoloAnulados.AutoSize = True
        Me.rdbSoloAnulados.Location = New System.Drawing.Point(202, 164)
        Me.rdbSoloAnulados.Name = "rdbSoloAnulados"
        Me.rdbSoloAnulados.Size = New System.Drawing.Size(93, 17)
        Me.rdbSoloAnulados.TabIndex = 65
        Me.rdbSoloAnulados.TabStop = True
        Me.rdbSoloAnulados.Text = "Solo Anulados"
        Me.rdbSoloAnulados.UseVisualStyleBackColor = True
        '
        'frmListadoDevolucionesSinNC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(677, 347)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoDevolucionesSinNC"
        Me.Text = "frmListadoDevolucionesSinNC"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents txtNumeroSolicitudCliente As TextBox
    Friend WithEvents chkNumeroSolicitudCliente As ocxCHK
    Friend WithEvents chkTipoCliente As ocxCHK
    Friend WithEvents cbxTipoCliente As ocxCBX
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents chkProducto As ocxCHK
    Friend WithEvents chkTipoProducto As ocxCHK
    Friend WithEvents cbxTipoProducto As ocxCBX
    Friend WithEvents ckhUsuario As ocxCHK
    Friend WithEvents cbxUsuario As ocxCBX
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents chkSucursal As ocxCHK
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents chkCliente As ocxCHK
    Friend WithEvents btnCerrar As Button
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnInforme As Button
    Friend WithEvents gbxFiltro As GroupBox
    Friend WithEvents chkAnulados As ocxCHK
    Friend WithEvents rdbIncluir As RadioButton
    Friend WithEvents rdbSoloAnulados As RadioButton
    Friend WithEvents Label1 As Label
End Class

﻿Imports ERP.Reporte
Public Class frmListadoDevolucionesSinNC
    'CLASES
    Dim CReporte As New CReporteNotaCredito
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE DEVOLUCIONES DE CLIENTES SIN NC"
    Dim TipoInforme As String
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Radio Button
        rdbIncluir.Checked = True
        rdbIncluir.Enabled = False
        rdbSoloAnulados.Enabled = False

        CargarInformacion()
        txtCliente.Conectar()

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxSucursal.Conectar()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, rdbIncluir)
        CSistema.CargaControl(vControles, rdbSoloAnulados)



    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1 "
        Dim OrderBy As String = ""
        TipoInforme = ""


        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If txtDesde.txt.Text = "" Or txtHasta.txt.Text = "" Then
            MessageBox.Show("Seleccione correctamente el rango de fechas", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtDesde.Focus()
            Exit Sub
        End If


        Where = Where & " and Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        If chkAnulados.Valor = False Then
            Where = Where & " And Anulado = 'False' "
        Else
            If rdbSoloAnulados.Checked Then
                Where = Where & " And Anulado = 'True' "
            End If
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next


        If chkNumeroSolicitudCliente.chk.Checked Then
            Where = Where & " and NumeroSolicitudCliente = " & txtNumeroSolicitudCliente.Text.Trim
        End If

        If chkCliente.chk.Checked = True Then

            If txtCliente.txtID.txt.Text = "" Then
                Dim mensaje As String = "Ingrese primero algún cliente!"
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                Where = Where & " And (IDCliente = " & txtCliente.Registro("ID").ToString & ") "
            End If

        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        CReporte.ListadoDevolucionSinNC(frm, Where, "", Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy)
    End Sub


    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub OcxAnulados_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkAnulados.PropertyChanged
        rdbIncluir.Enabled = value
        rdbSoloAnulados.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkNumeroSolicitudCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkNumeroSolicitudCliente.PropertyChanged
        txtNumeroSolicitudCliente.Enabled = value
    End Sub

End Class
﻿Imports ERP.Reporte
Public Class frmListadoMovimientoCombustible
    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE COMPROBANTES DE CONSUMO DE COMBUSTIBLE"
    Dim TipoInforme As String

    Private listOrdenadoPor As List(Of Dato)


    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO-CAMION")
        cbxTipoInforme.cbx.Items.Add("DETALLADO-CHOFER")
        cbxTipoInforme.cbx.SelectedIndex = 0

        txtProducto.Consulta = "Select ID, Descripcion, Ref From VProducto Where ConsumoCombustible = 1"

        txtProducto.Conectar()

    End Sub

    Sub Listar()

        Dim Where As String = " "
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

siguiente:

        Next

        Where = Where & " And Anulado = 0"
        WhereDetalle = Where

        Select Case cbxTipoInforme.cbx.SelectedIndex
            'Case 0
            '    'cbxProducto.EstablecerCondicion(Where)
            '    If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
            '        Where = IIf(String.IsNullOrEmpty(Where), " where IDProducto = " + txtProducto.Registro("Id").ToString, Where + " and IDProducto = " + txtProducto.Registro("Id").ToString)
            '    End If
            '    Where = Where
            '    CReporte.ListadoMovimientoCombustible(frm, Titulo, cbxTipoInforme.txt.Text, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)
            Case 0
                Titulo = Titulo & " DETALLADO"
                If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
                    WhereDetalle = IIf(String.IsNullOrEmpty(WhereDetalle), " where IDProducto = " + txtProducto.Registro("Id").ToString, WhereDetalle + " and IDProducto = " + txtProducto.Registro("Id").ToString)
                End If
                Where = Where
                CReporte.ListadoMovimientoCombustibleDetallado(frm, Titulo, cbxTipoInforme.txt.Text, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, "Detallado")
            Case 1
                Titulo = Titulo & " DETALLADO POR CAMION"
                If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
                    WhereDetalle = IIf(String.IsNullOrEmpty(WhereDetalle), " where IDProducto = " + txtProducto.Registro("Id").ToString, WhereDetalle + " and IDProducto = " + txtProducto.Registro("Id").ToString)
                End If
                Where = Where
                CReporte.ListadoMovimientoCombustibleDetallado(frm, Titulo, cbxTipoInforme.txt.Text, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, "Camion")
            Case 2
                Titulo = Titulo & " DETALLADO POR CHOFER"
                If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
                    WhereDetalle = IIf(String.IsNullOrEmpty(WhereDetalle), " where IDProducto = " + txtProducto.Registro("Id").ToString, WhereDetalle + " and IDProducto = " + txtProducto.Registro("Id").ToString)
                End If
                Where = Where
                CReporte.ListadoMovimientoCombustibleDetallado(frm, Titulo, cbxTipoInforme.txt.Text, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, "Chofer")

        End Select

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

    End Sub

    Sub SeleccionarTipoInforme()
        Select Case cbxTipoInforme.cbx.SelectedIndex
            Case 0
                chkProducto.chk.Checked = False
                txtProducto.Enabled = False
        End Select
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        SeleccionarTipoInforme()
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkDepSalida_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepSalida.PropertyChanged
        cbxDepSalida.Enabled = value
    End Sub

    Private Sub chkDepEntrada_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepEntrada.PropertyChanged
        cbxDepositoEntrada.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkChofer_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub

    Private Sub chkCamion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCamion.PropertyChanged
        cbxCamion.Enabled = value
    End Sub
End Class
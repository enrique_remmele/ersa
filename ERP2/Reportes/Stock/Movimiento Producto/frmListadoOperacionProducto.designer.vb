﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoOperacionProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxOrdenadoPor = New System.Windows.Forms.ComboBox()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkAnulado = New ERP.ocxCHK()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.chkMovimientosStock = New ERP.ocxCHK()
        Me.cbxMenu = New ERP.ocxCBX()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxDepositoEntrada = New ERP.ocxCBX()
        Me.chkDepEntrada = New ERP.ocxCHK()
        Me.cbxDepSalida = New ERP.ocxCBX()
        Me.chkDepSalida = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoOperacion = New ERP.ocxCBX()
        Me.chkTipoOperacion = New ERP.ocxCHK()
        Me.chkProducto = New ERP.ocxCHK()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMovimientosStock)
        Me.gbxFiltro.Controls.Add(Me.cbxMenu)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxDepositoEntrada)
        Me.gbxFiltro.Controls.Add(Me.chkDepEntrada)
        Me.gbxFiltro.Controls.Add(Me.cbxDepSalida)
        Me.gbxFiltro.Controls.Add(Me.chkDepSalida)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoOperacion)
        Me.gbxFiltro.Controls.Add(Me.chkTipoOperacion)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(423, 259)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(597, 248)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.chkAnulado)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(441, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(259, 230)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.DisplayMember = "idString"
        Me.cbxOrdenadoPor.FormattingEnabled = True
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(9, 76)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.TabIndex = 6
        Me.cbxOrdenadoPor.ValueMember = "idString"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 60)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 5
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(9, 116)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 9
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 100)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 8
        Me.lblRanking.Text = "Ranking:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(491, 248)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(100, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkAnulado
        '
        Me.chkAnulado.BackColor = System.Drawing.Color.Transparent
        Me.chkAnulado.Color = System.Drawing.Color.Empty
        Me.chkAnulado.Location = New System.Drawing.Point(9, 154)
        Me.chkAnulado.Margin = New System.Windows.Forms.Padding(4)
        Me.chkAnulado.Name = "chkAnulado"
        Me.chkAnulado.Size = New System.Drawing.Size(128, 21)
        Me.chkAnulado.SoloLectura = False
        Me.chkAnulado.TabIndex = 12
        Me.chkAnulado.Texto = "Incluir anulados"
        Me.chkAnulado.Valor = False
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(172, 76)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(66, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 7
        Me.cbxEnForma.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 3, 9, 15, 15, 138)
        Me.txtHasta.Location = New System.Drawing.Point(89, 35)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 3, 9, 15, 15, 154)
        Me.txtDesde.Location = New System.Drawing.Point(9, 35)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = ""
        Me.cbxTipoProducto.DataSource = "TipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(140, 47)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(269, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 18
        Me.cbxTipoProducto.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(6, 47)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(128, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 17
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'chkMovimientosStock
        '
        Me.chkMovimientosStock.BackColor = System.Drawing.Color.Transparent
        Me.chkMovimientosStock.Color = System.Drawing.Color.Empty
        Me.chkMovimientosStock.Location = New System.Drawing.Point(5, 215)
        Me.chkMovimientosStock.Margin = New System.Windows.Forms.Padding(4)
        Me.chkMovimientosStock.Name = "chkMovimientosStock"
        Me.chkMovimientosStock.Size = New System.Drawing.Size(128, 21)
        Me.chkMovimientosStock.SoloLectura = False
        Me.chkMovimientosStock.TabIndex = 16
        Me.chkMovimientosStock.Texto = "Menu"
        Me.chkMovimientosStock.Valor = False
        '
        'cbxMenu
        '
        Me.cbxMenu.CampoWhere = Nothing
        Me.cbxMenu.CargarUnaSolaVez = False
        Me.cbxMenu.DataDisplayMember = Nothing
        Me.cbxMenu.DataFilter = Nothing
        Me.cbxMenu.DataOrderBy = Nothing
        Me.cbxMenu.DataSource = Nothing
        Me.cbxMenu.DataValueMember = Nothing
        Me.cbxMenu.dtSeleccionado = Nothing
        Me.cbxMenu.Enabled = False
        Me.cbxMenu.FormABM = Nothing
        Me.cbxMenu.Indicaciones = Nothing
        Me.cbxMenu.Location = New System.Drawing.Point(139, 209)
        Me.cbxMenu.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMenu.Name = "cbxMenu"
        Me.cbxMenu.SeleccionMultiple = False
        Me.cbxMenu.SeleccionObligatoria = True
        Me.cbxMenu.Size = New System.Drawing.Size(211, 21)
        Me.cbxMenu.SoloLectura = False
        Me.cbxMenu.TabIndex = 15
        Me.cbxMenu.Texto = ""
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(140, 20)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(269, 21)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 14
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = ""
        Me.cbxSucursal.DataSource = "Sucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(139, 182)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(269, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 11
        Me.cbxSucursal.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(5, 187)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(128, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 10
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxDepositoEntrada
        '
        Me.cbxDepositoEntrada.CampoWhere = "IDDepositoEntrada"
        Me.cbxDepositoEntrada.CargarUnaSolaVez = False
        Me.cbxDepositoEntrada.DataDisplayMember = "Suc-Dep"
        Me.cbxDepositoEntrada.DataFilter = Nothing
        Me.cbxDepositoEntrada.DataOrderBy = ""
        Me.cbxDepositoEntrada.DataSource = "VDeposito"
        Me.cbxDepositoEntrada.DataValueMember = "ID"
        Me.cbxDepositoEntrada.dtSeleccionado = Nothing
        Me.cbxDepositoEntrada.Enabled = False
        Me.cbxDepositoEntrada.FormABM = Nothing
        Me.cbxDepositoEntrada.Indicaciones = Nothing
        Me.cbxDepositoEntrada.Location = New System.Drawing.Point(139, 155)
        Me.cbxDepositoEntrada.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepositoEntrada.Name = "cbxDepositoEntrada"
        Me.cbxDepositoEntrada.SeleccionMultiple = False
        Me.cbxDepositoEntrada.SeleccionObligatoria = False
        Me.cbxDepositoEntrada.Size = New System.Drawing.Size(269, 21)
        Me.cbxDepositoEntrada.SoloLectura = False
        Me.cbxDepositoEntrada.TabIndex = 9
        Me.cbxDepositoEntrada.Texto = ""
        '
        'chkDepEntrada
        '
        Me.chkDepEntrada.BackColor = System.Drawing.Color.Transparent
        Me.chkDepEntrada.Color = System.Drawing.Color.Empty
        Me.chkDepEntrada.Location = New System.Drawing.Point(5, 159)
        Me.chkDepEntrada.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDepEntrada.Name = "chkDepEntrada"
        Me.chkDepEntrada.Size = New System.Drawing.Size(128, 21)
        Me.chkDepEntrada.SoloLectura = False
        Me.chkDepEntrada.TabIndex = 8
        Me.chkDepEntrada.Texto = "Dep. Entrada:"
        Me.chkDepEntrada.Valor = False
        '
        'cbxDepSalida
        '
        Me.cbxDepSalida.CampoWhere = "IDDepositoSalida"
        Me.cbxDepSalida.CargarUnaSolaVez = False
        Me.cbxDepSalida.DataDisplayMember = "Suc-Dep"
        Me.cbxDepSalida.DataFilter = Nothing
        Me.cbxDepSalida.DataOrderBy = ""
        Me.cbxDepSalida.DataSource = "VDeposito"
        Me.cbxDepSalida.DataValueMember = "ID"
        Me.cbxDepSalida.dtSeleccionado = Nothing
        Me.cbxDepSalida.Enabled = False
        Me.cbxDepSalida.FormABM = Nothing
        Me.cbxDepSalida.Indicaciones = Nothing
        Me.cbxDepSalida.Location = New System.Drawing.Point(139, 128)
        Me.cbxDepSalida.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDepSalida.Name = "cbxDepSalida"
        Me.cbxDepSalida.SeleccionMultiple = False
        Me.cbxDepSalida.SeleccionObligatoria = False
        Me.cbxDepSalida.Size = New System.Drawing.Size(269, 21)
        Me.cbxDepSalida.SoloLectura = False
        Me.cbxDepSalida.TabIndex = 7
        Me.cbxDepSalida.Texto = ""
        '
        'chkDepSalida
        '
        Me.chkDepSalida.BackColor = System.Drawing.Color.Transparent
        Me.chkDepSalida.Color = System.Drawing.Color.Empty
        Me.chkDepSalida.Location = New System.Drawing.Point(5, 131)
        Me.chkDepSalida.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDepSalida.Name = "chkDepSalida"
        Me.chkDepSalida.Size = New System.Drawing.Size(128, 21)
        Me.chkDepSalida.SoloLectura = False
        Me.chkDepSalida.TabIndex = 6
        Me.chkDepSalida.Texto = "Dep. Salida:"
        Me.chkDepSalida.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = ""
        Me.cbxTipoComprobante.DataSource = "TipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(139, 101)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(269, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 5
        Me.cbxTipoComprobante.Texto = ""
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(5, 103)
        Me.chkTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(128, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 4
        Me.chkTipoComprobante.Texto = "Tipo de comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoOperacion
        '
        Me.cbxTipoOperacion.CampoWhere = "IDTipoOperacion"
        Me.cbxTipoOperacion.CargarUnaSolaVez = False
        Me.cbxTipoOperacion.DataDisplayMember = "Descripcion"
        Me.cbxTipoOperacion.DataFilter = Nothing
        Me.cbxTipoOperacion.DataOrderBy = ""
        Me.cbxTipoOperacion.DataSource = "TipoOperacion"
        Me.cbxTipoOperacion.DataValueMember = "ID"
        Me.cbxTipoOperacion.dtSeleccionado = Nothing
        Me.cbxTipoOperacion.Enabled = False
        Me.cbxTipoOperacion.FormABM = Nothing
        Me.cbxTipoOperacion.Indicaciones = Nothing
        Me.cbxTipoOperacion.Location = New System.Drawing.Point(139, 74)
        Me.cbxTipoOperacion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoOperacion.Name = "cbxTipoOperacion"
        Me.cbxTipoOperacion.SeleccionMultiple = False
        Me.cbxTipoOperacion.SeleccionObligatoria = False
        Me.cbxTipoOperacion.Size = New System.Drawing.Size(269, 21)
        Me.cbxTipoOperacion.SoloLectura = False
        Me.cbxTipoOperacion.TabIndex = 3
        Me.cbxTipoOperacion.Texto = ""
        '
        'chkTipoOperacion
        '
        Me.chkTipoOperacion.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoOperacion.Color = System.Drawing.Color.Empty
        Me.chkTipoOperacion.Location = New System.Drawing.Point(5, 75)
        Me.chkTipoOperacion.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoOperacion.Name = "chkTipoOperacion"
        Me.chkTipoOperacion.Size = New System.Drawing.Size(128, 21)
        Me.chkTipoOperacion.SoloLectura = False
        Me.chkTipoOperacion.TabIndex = 2
        Me.chkTipoOperacion.Texto = "Tipo de operación:"
        Me.chkTipoOperacion.Valor = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(6, 19)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(75, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 0
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'frmListadoOperacionProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(709, 310)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoOperacionProducto"
        Me.Text = "Listado de operaciones por producto "
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents cbxOrdenadoPor As System.Windows.Forms.ComboBox
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxTipoOperacion As ERP.ocxCBX
    Friend WithEvents chkTipoOperacion As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxDepositoEntrada As ERP.ocxCBX
    Friend WithEvents chkDepEntrada As ERP.ocxCHK
    Friend WithEvents cbxDepSalida As ERP.ocxCBX
    Friend WithEvents chkDepSalida As ERP.ocxCHK
    Friend WithEvents chkAnulado As ERP.ocxCHK
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents chkMovimientosStock As ERP.ocxCHK
    Friend WithEvents cbxMenu As ERP.ocxCBX
    Friend WithEvents cbxTipoProducto As ocxCBX
    Friend WithEvents chkTipoProducto As ocxCHK
End Class

﻿Imports ERP.Reporte

Public Class frmListadoMovimientoProducto

    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE COMPROBANTES DE STOCK"
    Dim TipoInforme As String

    Private listOrdenadoPor As List(Of Dato)


    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO PRODUCTO/FECHA")
        cbxTipoInforme.cbx.SelectedIndex = 0

        'Tipos de Menu
        cbxMenu.cbx.Items.Add("Movimiento de Stock")
        cbxMenu.cbx.Items.Add("Descarga de Stock")
        cbxMenu.cbx.Items.Add("Consumo de Combustible")
        cbxMenu.cbx.Items.Add("Materia Prima Balanceado")
        cbxMenu.cbx.Items.Add("Descarga de Compras")
        cbxMenu.cbx.SelectedIndex = 0

        cbxOrdenadoPor.SelectedIndex = 0

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        CSistema.SqlToComboBox(cbxTipoOperacion, "select distinct descripcion,descripcion from TipoOperacion")
        cbxTipoOperacion.DataDisplayMember = "Descripcion"
        cbxTipoOperacion.DataValueMember = "Descripcion"

        'Seleccion Multiple
        cbxTipoProducto.SeleccionMultiple = True
        cbxTipoComprobante.SeleccionMultiple = True


        txtProducto.Conectar()

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If
            If ctr.name = "cbxTipoOperacion" Then
                GoTo siguiente
            End If
            If ctr.name = "cbxTipoProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

siguiente:

        Next

        If chkTipoOperacion.Valor Then
            Where = Where & " And Operacion = '" & cbxTipoOperacion.cbx.Text & "' "
        End If

        If Not chkAnulado.Valor Then
            Where = Where & " And Anulado = 0"
        End If
        WhereDetalle = Where

        If chkTipoProducto.Valor Then
            cbxTipoProducto.EstablecerCondicion(WhereDetalle)
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        Dim vMovimiento As String = ""
        If chkMovimientosStock.Valor = True Then
            If cbxMenu.cbx.SelectedIndex = 0 Then
                vMovimiento = " and MovimientoStock = 'True' "
            ElseIf cbxMenu.cbx.SelectedIndex = 1 Then
                vMovimiento = " and DescargaStock = 'True' "
            ElseIf cbxMenu.cbx.SelectedIndex = 2 Then
                vMovimiento = " and MovimientoCombustible = 'True' "
            ElseIf cbxMenu.cbx.SelectedIndex = 3 Then
                vMovimiento = " and MovimientoMateriaPrima = 'True' "
            ElseIf cbxMenu.cbx.SelectedIndex = 4 Then
                vMovimiento = " and DescargaCompra = 'True' "
            End If
        End If

        Select Case cbxTipoInforme.cbx.SelectedIndex
            Case 0
                'cbxProducto.EstablecerCondicion(Where)
                If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
                    Where = IIf(String.IsNullOrEmpty(Where), " where IDProducto = " + txtProducto.Registro("Id").ToString, Where + " and IDProducto = " + txtProducto.Registro("Id").ToString)
                End If
                Where = Where & vMovimiento
                CReporte.ListadoMovimientoProducto(frm, Titulo, cbxTipoInforme.txt.Text, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)
            Case 1
                Titulo = Titulo & " DETALLADO"
                If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
                    WhereDetalle = IIf(String.IsNullOrEmpty(WhereDetalle), " where IDProducto = " + txtProducto.Registro("Id").ToString, WhereDetalle + " and IDProducto = " + txtProducto.Registro("Id").ToString)
                End If
                Where = Where & vMovimiento
                CReporte.ListadoMovimientoProductoDetallado(frm, Titulo, cbxTipoInforme.txt.Text, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)
            Case 2
                Titulo = Titulo & " DETALLADO PRODUCTO/FECHA"
                If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
                    WhereDetalle = IIf(String.IsNullOrEmpty(WhereDetalle), " where IDProducto = " + txtProducto.Registro("Id").ToString, WhereDetalle + " and IDProducto = " + txtProducto.Registro("Id").ToString)
                End If
                Where = Where & vMovimiento
                CReporte.ListadoMovimientoDetalladoProductoFecha(frm, Titulo, cbxTipoInforme.txt.Text, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)

        End Select

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        'cbxOrdenadoPor.cbx.Items.Clear()


        'dt = CData.GetStructure("VMovimiento ", "Select Top(0)Fecha,Numero From VMovimiento")


        'If dt Is Nothing Then
        '    Exit Sub
        'End If

        'For i As Integer = 0 To dt.Columns.Count - 1
        '    cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        'Next

        'listOrdenadoPor = New List(Of Dato)
        'Dim obj As Dato
        'obj = New Dato
        'obj.idString = "FechaTransaccion, Numero, TipoComprobante, NroComprobante"
        'obj.descripcion = "Fecha"
        'listOrdenadoPor.Add(obj)
        'obj = New Dato
        'obj.idString = "TipoComprobante ASC, Fecha"
        'obj.descripcion = "Tipo de comprobante"
        'listOrdenadoPor.Add(obj)
        'obj = New Dato
        'obj.idString = "Numero"
        'obj.descripcion = "Numero"
        'listOrdenadoPor.Add(obj)
        'OrdenadoPorBindingSource.DataSource = listOrdenadoPor


    End Sub

    Sub SeleccionarTipoInforme()
        Select Case cbxTipoInforme.cbx.SelectedIndex
            Case 0
                chkProducto.chk.Checked = False
                txtProducto.Enabled = False
        End Select
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        SeleccionarTipoInforme()
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkTipoOperacion_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoOperacion.PropertyChanged
        cbxTipoOperacion.Enabled = value
    End Sub
    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkDepSalida_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepSalida.PropertyChanged
        cbxDepSalida.Enabled = value
    End Sub

    Private Sub chkDepEntrada_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepEntrada.PropertyChanged
        cbxDepositoEntrada.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkMovimientosStock_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMovimientosStock.PropertyChanged
        cbxMenu.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub
End Class




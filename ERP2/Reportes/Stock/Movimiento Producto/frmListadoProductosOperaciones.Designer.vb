﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoProductosOperaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxOrdenadoPor = New System.Windows.Forms.ComboBox()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkAnulado = New ERP.ocxCHK()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.chkDepEntrada = New ERP.ocxCHK()
        Me.chkDepSalida = New ERP.ocxCHK()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.chkTipoOperacion = New ERP.ocxCHK()
        Me.chkProducto = New ERP.ocxCHK()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(793, 281)
        Me.btnCerrar.Margin = New System.Windows.Forms.Padding(4)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(133, 28)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblInforme)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(585, 13)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(345, 261)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.DisplayMember = "idString"
        Me.cbxOrdenadoPor.FormattingEnabled = True
        Me.cbxOrdenadoPor.Items.AddRange(New Object() {"Fecha"})
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(12, 140)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(208, 24)
        Me.cbxOrdenadoPor.TabIndex = 6
        Me.cbxOrdenadoPor.ValueMember = "idString"
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(8, 71)
        Me.lblInforme.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(59, 17)
        Me.lblInforme.TabIndex = 3
        Me.lblInforme.Text = "Informe:"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(8, 121)
        Me.lblOrdenado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(76, 17)
        Me.lblOrdenado.TabIndex = 5
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(12, 190)
        Me.nudRanking.Margin = New System.Windows.Forms.Padding(4)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(99, 22)
        Me.nudRanking.TabIndex = 9
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(8, 23)
        Me.lblPeriodo.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(61, 17)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(8, 170)
        Me.lblRanking.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(64, 17)
        Me.lblRanking.TabIndex = 8
        Me.lblRanking.Text = "Ranking:"
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 3, 9, 15, 15, 138)
        Me.txtHasta.Location = New System.Drawing.Point(119, 43)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(5)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(99, 25)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 3, 9, 15, 15, 154)
        Me.txtDesde.Location = New System.Drawing.Point(12, 43)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(5)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(99, 25)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(652, 281)
        Me.btnInforme.Margin = New System.Windows.Forms.Padding(4)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(133, 28)
        Me.btnInforme.TabIndex = 6
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkAnulado)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkDepEntrada)
        Me.gbxFiltro.Controls.Add(Me.chkDepSalida)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkTipoOperacion)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Location = New System.Drawing.Point(13, 13)
        Me.gbxFiltro.Margin = New System.Windows.Forms.Padding(4)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Padding = New System.Windows.Forms.Padding(4)
        Me.gbxFiltro.Size = New System.Drawing.Size(564, 261)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkAnulado
        '
        Me.chkAnulado.BackColor = System.Drawing.Color.Transparent
        Me.chkAnulado.Color = System.Drawing.Color.Empty
        Me.chkAnulado.Location = New System.Drawing.Point(8, 226)
        Me.chkAnulado.Margin = New System.Windows.Forms.Padding(5)
        Me.chkAnulado.Name = "chkAnulado"
        Me.chkAnulado.Size = New System.Drawing.Size(171, 26)
        Me.chkAnulado.SoloLectura = False
        Me.chkAnulado.TabIndex = 12
        Me.chkAnulado.Texto = "Incluir anulados"
        Me.chkAnulado.Valor = False
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(8, 190)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(5)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(171, 26)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 10
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'chkDepEntrada
        '
        Me.chkDepEntrada.BackColor = System.Drawing.Color.Transparent
        Me.chkDepEntrada.Color = System.Drawing.Color.Empty
        Me.chkDepEntrada.Location = New System.Drawing.Point(8, 156)
        Me.chkDepEntrada.Margin = New System.Windows.Forms.Padding(5)
        Me.chkDepEntrada.Name = "chkDepEntrada"
        Me.chkDepEntrada.Size = New System.Drawing.Size(171, 26)
        Me.chkDepEntrada.SoloLectura = False
        Me.chkDepEntrada.TabIndex = 8
        Me.chkDepEntrada.Texto = "Dep. Entrada:"
        Me.chkDepEntrada.Valor = False
        '
        'chkDepSalida
        '
        Me.chkDepSalida.BackColor = System.Drawing.Color.Transparent
        Me.chkDepSalida.Color = System.Drawing.Color.Empty
        Me.chkDepSalida.Location = New System.Drawing.Point(8, 123)
        Me.chkDepSalida.Margin = New System.Windows.Forms.Padding(5)
        Me.chkDepSalida.Name = "chkDepSalida"
        Me.chkDepSalida.Size = New System.Drawing.Size(171, 26)
        Me.chkDepSalida.SoloLectura = False
        Me.chkDepSalida.TabIndex = 6
        Me.chkDepSalida.Texto = "Dep. Salida:"
        Me.chkDepSalida.Valor = False
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(8, 90)
        Me.chkTipoComprobante.Margin = New System.Windows.Forms.Padding(5)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(171, 26)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 4
        Me.chkTipoComprobante.Texto = "Tipo de comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'chkTipoOperacion
        '
        Me.chkTipoOperacion.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoOperacion.Color = System.Drawing.Color.Empty
        Me.chkTipoOperacion.Location = New System.Drawing.Point(8, 57)
        Me.chkTipoOperacion.Margin = New System.Windows.Forms.Padding(5)
        Me.chkTipoOperacion.Name = "chkTipoOperacion"
        Me.chkTipoOperacion.Size = New System.Drawing.Size(171, 26)
        Me.chkTipoOperacion.SoloLectura = False
        Me.chkTipoOperacion.TabIndex = 2
        Me.chkTipoOperacion.Texto = "Tipo de operación:"
        Me.chkTipoOperacion.Valor = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(8, 23)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(5)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(100, 26)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 0
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'frmListadoProductosOperaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(940, 317)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoProductosOperaciones"
        Me.Text = "Listado de productos de stock"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxOrdenadoPor As System.Windows.Forms.ComboBox
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkAnulado As ERP.ocxCHK
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents chkDepEntrada As ERP.ocxCHK
    Friend WithEvents chkDepSalida As ERP.ocxCHK
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents chkTipoOperacion As ERP.ocxCHK
    Friend WithEvents chkProducto As ERP.ocxCHK
End Class

﻿Imports ERP.Reporte
Public Class frmMovimientoProducto

    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "EXTRACTO DE MOVIMIENTOS DE PRODUCTO"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        txtProducto.Conectar()
        CargarInformacion()
        CambiarOrdenacion()

        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        chkConCosto.Visible = vgVerCosto

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)
        'Movimiento
        CSistema.SqlToComboBox(cbxMovimiento, "Select Distinct Movimiento, Movimiento from VExtractoMovimientoProducto")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Ventas
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        'Tipo de Venta
        cbxTipoInforme.cbx.SelectedIndex = 0


    End Sub

    Sub Listar()

        If txtProducto.Seleccionado = False Then
            MessageBox.Show("Selecione un Producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim Where As String = ""
        Dim SubTitulo As String = ""
        Dim Producto As String = ""
        Dim Existencia As Decimal = 0
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim OrdenadoFechaOperacion As Boolean = False

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If cbxOrdenadoPor.cbx.SelectedIndex = 0 Then
            Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
        Else
            Where = " Where FechaOperacion Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
            OrdenadoFechaOperacion = True
        End If


        'Calcular Existencia Inicial
        If chkDeposito.Valor = False And chkSucursal.Valor = False Then
            Existencia = CSistema.ExecuteScalar("Select 'Cantidad'=dbo.FExtractoMovimientoProducto(" & txtProducto.Registro("ID") & ",'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "', '" & OrdenadoFechaOperacion & "')", "", 1000)
        End If

        If chkSucursal.Valor = True And chkDeposito.Valor = False And Existencia = 0 Then
            Existencia = CSistema.ExecuteScalar("Select 'Cantidad'=dbo.FExtractoMovimientoProductoSucursal(" & txtProducto.Registro("ID") & "," & cbxSucursal.GetValue & ",'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "', '" & OrdenadoFechaOperacion & "')", "", 1000)
        End If

        If chkDeposito.Valor = True Then
            Existencia = CSistema.ExecuteScalar("Select 'Cantidad'=dbo.FExtractoMovimientoProductoDeposito(" & txtProducto.Registro("ID") & "," & cbxDeposito.GetValue & ",'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "', '" & OrdenadoFechaOperacion & "')", "", 1000)
        End If

        Where = Where & " And IDProducto=" & txtProducto.Registro("ID") & " "

        'Producto
        Producto = txtProducto.Registro("Descripcion").ToString & "  (" & txtProducto.Registro("Ref").ToString & ")"

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then

                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        If chkMovimiento.Valor = True Then
            If Where = "" Then
                Where = "Where Movimiento= " & "'" & cbxMovimiento.cbx.Text & "'"
            Else
                Where = Where & " And Movimiento= " & "'" & cbxMovimiento.cbx.Text & "'"
            End If

        End If

        'Establecemos el Orden
        'OrderBy = " Order By Fecha"

        If cbxOrdenadoPor.txt.Text <> "" Then
            If cbxTipoInforme.cbx.SelectedIndex = 0 Then
                OrderBy = "Order by " & cbxOrdenadoPor.txt.Text
            Else
                If OrdenadoFechaOperacion = True Then
                    OrderBy = "Order by IDTransaccion"
                Else
                    OrderBy = "Order by Fecha, Hora"
                End If
            End If

        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        ArmarSubTitulo(SubTitulo)
        If chkConCosto.chk.Checked Then
            TipoInforme = "Detallados con Costo"
            CReporte.MovimientoProductoDetalleCosteado(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo)
        Else

            Select Case cbxTipoInforme.cbx.SelectedIndex

                Case 1
                    TipoInforme = cbxTipoInforme.cbx.Text
                    CReporte.MovimientoProductoDetalle(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo, OrdenadoFechaOperacion)
                Case 0
                    TipoInforme = cbxTipoInforme.cbx.Text
                    CReporte.MovimientoProductoResumen(frm, Where, Titulo, TipoInforme & " - " & SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, Producto, SubTitulo, txtProducto.Registro("ID"))

            End Select
        End If
    End Sub

    Sub CambiarOrdenacion()

        cbxOrdenadoPor.cbx.Items.Clear()

        Select Case cbxTipoInforme.cbx.SelectedIndex
            Case 0
                cbxOrdenadoPor.cbx.Items.Add("Movimiento")
            Case 1
                cbxOrdenadoPor.cbx.Items.Add("Fec. de Documento")
                cbxOrdenadoPor.cbx.Items.Add("Fec. Operacion")
        End Select

        cbxOrdenadoPor.cbx.SelectedIndex = 0

    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)
        SubTitulo = "Del " & txtDesde.GetValue.ToShortDateString & " Al " & txtHasta.GetValue.ToShortDateString
        For Each ctr As Object In gbxFiltro.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubtitulo(SubTitulo)
            End If
        Next
    End Sub

    Private Sub frmExistenciaValorizada_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        cbxMovimiento.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmMovimientoProducto_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkMovimiento_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMovimiento.PropertyChanged
        cbxMovimiento.Enabled = value
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            txtDesde.Focus()
        End If
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        CambiarOrdenacion()
    End Sub

    Private Sub chkConCosto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkConCosto.PropertyChanged
        cbxTipoInforme.Enabled = Not value
    End Sub
End Class
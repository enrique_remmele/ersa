﻿Imports ERP.Reporte
Public Class frmListadoGastoAcuerdo
    'CLASES
    Dim CReporte As New CReporteTicketBascula
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES


    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        txtProducto.Conectar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        txtHastaFactura.SetValue(Date.Today)
        txtDesdeFactura.SetValue(Date.Today)
        txtHastaFactura.SetValue(Date.Today)
        txtDesdeOP.SetValue(Date.Today)

        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "

        CSistema.SqlToComboBox(cbxMoneda, "Select ID, Descripcion from moneda ")


    End Sub

    Sub Listar()

        Dim Titulo As String = "Extracto de Pagos por Acuerdo"

        Dim Where As String = "Where 1 = 1 "
        Dim SubTitulo As String = ""
        Dim Producto As String = ""
        Dim Existencia As Decimal = 0
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim OrdenadoFechaOperacion As Boolean = False

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkFechaFactura.Valor Then

            Where = Where & " and FechaGasto between '" & CSistema.FormatoFechaBaseDatos(txtDesdeFactura.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaFactura.txt.Text, True, False) & "' "
            SubTitulo = " Facturas desde " & txtDesdeFactura.txt.Text & " al " & txtHastaFactura.txt.Text

        End If

        If chkFechaOP.Valor Then

            Where = Where & " and FechaOP between '" & CSistema.FormatoFechaBaseDatos(txtDesdeOP.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaOP.txt.Text, True, False) & "' "
            SubTitulo = " Pagos desde " & txtDesdeOP.txt.Text & " al " & txtHastaOP.txt.Text

        End If

        SubTitulo = IIf(SubTitulo = "", "", "")

        If txtProducto.Seleccionado = True And txtProducto.txt.txt.Text <> "" And chkProducto.Valor = True Then
            Where = Where & " and IDProducto=" & txtProducto.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prod: " & txtProducto.txt.txt.Text
        End If
        If txtProveedor.Seleccionado = True And txtProveedor.txtRazonSocial.txt.Text <> "" And chkProveedor.Valor = True Then
            Where = Where & " and IDProveedor=" & txtProveedor.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prov: " & txtProveedor.txtRazonSocial.txt.Text
        End If

        If chkZafra.Valor Then
            Where = Where & " and zafra = " & txtZafra.Text
        End If

        If chkAcuerdo.Valor Then
            Where = Where & " and acuerdo = " & txtNroAcuerdo.Text
        End If

        If chkMoneda.Valor Then
            Where = Where & " and IDMonedaAcuerdo = " & cbxMoneda.cbx.SelectedValue
        End If

        CReporte.ExtractoPagoFacturaAcuerdo(frm, Where, Titulo, vgUsuarioIdentificador, SubTitulo)


    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)
        '   SubTitulo = " Al " & txtHasta.GetValue.ToShortDateString
        For Each ctr As Object In gbxFiltro.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubtitulo(SubTitulo)
            End If
        Next
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmInformeAcuerdo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmInformeAcuerdo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkFechaFactura_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaFactura.PropertyChanged
        txtDesdeFactura.Enabled = value
        txtHastaFactura.Enabled = value

    End Sub

    Private Sub chkPeriodoMacheo_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaOP.PropertyChanged
        txtDesdeOP.Enabled = value
        txtHastaOP.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkAcuerdo_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkAcuerdo.PropertyChanged
        txtNroAcuerdo.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
    End Sub

    Private Sub chkZafra_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkZafra.PropertyChanged
        txtZafra.Enabled = value
    End Sub
End Class
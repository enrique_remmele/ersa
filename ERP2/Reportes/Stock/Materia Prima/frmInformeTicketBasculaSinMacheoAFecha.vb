﻿Imports ERP.Reporte
Public Class frmInformeTicketBasculaSinMacheoAFecha
    'CLASES
    Dim CReporte As New CReporteTicketBascula
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "LISTADO DE TICKETS DE BASCULA SIN MACHEAR A FECHA"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        txtProducto.Conectar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        txtHasta.SetValue(Date.Today)

        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "


    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim SubTitulo As String = ""
        Dim Producto As String = ""
        Dim Existencia As Decimal = 0
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim OrdenadoFechaOperacion As Boolean = False

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = "'" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        SubTitulo = txtHasta.txt.Text
        If txtProducto.Seleccionado = True And txtProducto.txt.txt.Text <> "" Then
            Where = Where & ", @IDProducto=" & txtProducto.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prod: " & txtProducto.txt.txt.Text
        End If
        If txtProveedor.Seleccionado = True And txtProveedor.txtRazonSocial.txt.Text <> "" Then
            Where = Where & ", @IDProveedor=" & txtProveedor.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prov: " & txtProveedor.txtRazonSocial.txt.Text
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls
            If ctr.GetType.Name.ToUpper = "OCXCBX" Then

                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'ArmarSubTitulo(SubTitulo)
        TipoInforme = ""
        CReporte.ListadoTicketBasculaSinMachearAFecha(frm, Where, Titulo, TipoInforme & " - " & SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtHasta.txt.Text, SubTitulo)


    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)
        SubTitulo = " Al " & txtHasta.GetValue.ToShortDateString
        For Each ctr As Object In gbxFiltro.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubtitulo(SubTitulo)
            End If
        Next
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            txtHasta.Focus()
        End If
    End Sub

    Private Sub frmInformeAcuerdo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmInformeAcuerdo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

End Class
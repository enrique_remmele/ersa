﻿Imports ERP.Reporte
Public Class frmInformeTicketBasculaSinCostovb
    'CLASES
    Dim CReporte As New CReporteTicketBascula
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "LISTADO DE TICKETS DE BASCULA"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        txtProducto.Conectar()
        CargarInformacion()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
        cbxEstado.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)
        'Movimiento
        CSistema.SqlToComboBox(cbxMoneda, "Select Distinct ID, Descripcion from VMoneda")

        'Movimiento
        CSistema.SqlToComboBox(cbxDeposito, "Select Distinct ID, SucDeposito from VDeposito")

        'Chofer
        CSistema.SqlToComboBox(cbxChofer.cbx, "Select ID, Nombres From ChoferProveedor Order By 2")
        'Chapa
        CSistema.SqlToComboBox(cbxChapa.cbx, "Select ID, Patente From CamionProveedor Order By 2")
        'camion
        CSistema.SqlToComboBox(cbxCamion.cbx, "Select ID, Descripcion From CamionProveedor Order By 2")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'Ordenado por
        cbxOrdenadoPor.cbx.Items.Add("NroAcuerdo")
        cbxOrdenadoPor.cbx.Items.Add("Fecha")
        cbxOrdenadoPor.cbx.Items.Add("Proveedor")
        cbxOrdenadoPor.cbx.Items.Add("Producto")
        cbxOrdenadoPor.cbx.Items.Add("Deposito")
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Estado
        cbxEstado.cbx.Items.Add("TODOS")
        cbxEstado.cbx.Items.Add("APLICADOS")
        cbxEstado.cbx.Items.Add("NO APLICADOS")
        cbxEstado.cbx.DropDownStyle = ComboBoxStyle.DropDownList


        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "


    End Sub

    Sub Listar()

        'If txtProducto.Seleccionado = False Then
        '    MessageBox.Show("Selecione un Producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        '    Exit Sub
        'End If

        Dim Where As String = ""
        Dim SubTitulo As String = ""
        Dim Producto As String = ""
        Dim Existencia As Decimal = 0
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim OrdenadoFechaOperacion As Boolean = False

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where cast(Fecha as date) Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        SubTitulo = txtDesde.txt.Text & " - " & txtHasta.txt.Text
        If txtProducto.Seleccionado = True And txtProducto.txt.txt.Text <> "" Then
            Where = Where & " And IDProducto=" & txtProducto.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prod: " & txtProducto.txt.txt.Text
        End If
        If txtProveedor.Seleccionado = True And txtProveedor.txtRazonSocial.txt.Text <> "" Then
            Where = Where & " And IDProveedor=" & txtProveedor.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prov: " & txtProveedor.txtRazonSocial.txt.Text
        End If

        If chkAcuerdo.Valor = True Then
            Where = Where & " And NroAcuerdo=" & txtNroAcuerdo.ObtenerValor & " "
            SubTitulo = SubTitulo & "  -  Acuerdo: " & txtNroAcuerdo.ObtenerValor
        End If

        If chkChapa.Valor = True Then
            Where = Where & " And IDCamion=" & cbxChapa.GetValue & " "
            SubTitulo = SubTitulo & "  -  Camion: " & cbxCamion.Texto & " - " & cbxChapa.Texto
        End If

        If chkChofer.Valor = True Then
            Where = Where & " And IDChofer=" & cbxChofer.GetValue & " "
            SubTitulo = SubTitulo & "  -  Chofer: " & cbxChofer.Texto
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls
            If ctr.GetType.Name.ToUpper = "OCXCBX" Then

                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        If chkMoneda.Valor = True Then
            If Where = "" Then
                Where = "Where IDMoneda = " & "'" & cbxMoneda.GetValue & "'"
            Else
                Where = Where & " And IDMoneda = " & "'" & cbxMoneda.GetValue & "'"
            End If
            SubTitulo = SubTitulo & "  -  Moneda: " & cbxMoneda.Texto
        End If

        If chkDeposito.Valor = True Then
            If Where = "" Then
                Where = "Where IDDeposito = " & "'" & cbxDeposito.GetValue & "'"
            Else
                Where = Where & " And IDDeposito = " & "'" & cbxDeposito.GetValue & "'"
            End If
            SubTitulo = SubTitulo & "  -  Deposito: " & cbxDeposito.Texto
        End If

        'Establecemos el Orden
        'OrderBy = " Order By Fecha"

        If cbxOrdenadoPor.txt.Text <> "" Then
            OrderBy = "Order by " & cbxOrdenadoPor.txt.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        If cbxEstado.cbx.SelectedIndex = 1 Then
            Where = Where & " and IDTransaccion in (select IDTransaccionTicket from detallemacheo)"
            SubTitulo = SubTitulo & "TICKETS APLICADOS"
        ElseIf cbxEstado.cbx.SelectedIndex = 2 Then
            Where = Where & " and IDTransaccion not in (select IDTransaccionTicket from detallemacheo)"
            SubTitulo = SubTitulo & "TICKETS NO APLICADOS"
        End If

        'ArmarSubTitulo(SubTitulo)
        TipoInforme = ""
        CReporte.ListadoTicketBascula(frm, Where, Titulo, TipoInforme & " - " & SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo, True)


    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)
        SubTitulo = "Del " & txtDesde.GetValue.ToShortDateString & " Al " & txtHasta.GetValue.ToShortDateString
        For Each ctr As Object In gbxFiltro.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubtitulo(SubTitulo)
            End If
        Next
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            txtDesde.Focus()
        End If
    End Sub

    Private Sub frmInformeAcuerdo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    

    Private Sub chkZafra_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkAcuerdo.PropertyChanged
        txtNroAcuerdo.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkChapa_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkChapa.PropertyChanged
        cbxChapa.Enabled = value
    End Sub

    Private Sub chkChofer_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub

    Private Sub cbxChapa_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxChapa.PropertyChanged
        cbxCamion.cbx.DataSource = Nothing
        If cbxChapa.Validar = False Then
            Exit Sub
        End If
        CSistema.SqlToComboBox(cbxCamion.cbx, CData.GetTable("vCamionProveedor", " ID = " & cbxChapa.GetValue), "ID", "Descripcion")
        cbxChofer.Texto = CType(CSistema.ExecuteScalar("Select top(1) Nombres from ChoferProveedor where IDCamion = " & cbxChapa.GetValue), String)
    End Sub

    Private Sub frmInformeTicketBasculaSinCostovb_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
End Class
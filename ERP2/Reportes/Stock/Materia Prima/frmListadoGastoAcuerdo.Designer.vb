﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoGastoAcuerdo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtZafra = New System.Windows.Forms.TextBox()
        Me.txtNroAcuerdo = New System.Windows.Forms.TextBox()
        Me.chkZafra = New ERP.ocxCHK()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.chkAcuerdo = New ERP.ocxCHK()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkFechaOP = New ERP.ocxCHK()
        Me.txtHastaOP = New ERP.ocxTXTDate()
        Me.txtDesdeOP = New ERP.ocxTXTDate()
        Me.chkFechaFactura = New ERP.ocxCHK()
        Me.txtHastaFactura = New ERP.ocxTXTDate()
        Me.txtDesdeFactura = New ERP.ocxTXTDate()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(318, 219)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(162, 23)
        Me.btnInforme.TabIndex = 14
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(565, 219)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 15
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtNroAcuerdo)
        Me.gbxFiltro.Controls.Add(Me.txtZafra)
        Me.gbxFiltro.Controls.Add(Me.chkZafra)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkAcuerdo)
        Me.gbxFiltro.Controls.Add(Me.txtProveedor)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(468, 201)
        Me.gbxFiltro.TabIndex = 12
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaOP)
        Me.GroupBox2.Controls.Add(Me.txtHastaOP)
        Me.GroupBox2.Controls.Add(Me.txtDesdeOP)
        Me.GroupBox2.Controls.Add(Me.chkFechaFactura)
        Me.GroupBox2.Controls.Add(Me.txtHastaFactura)
        Me.GroupBox2.Controls.Add(Me.txtDesdeFactura)
        Me.GroupBox2.Controls.Add(Me.cbxMoneda)
        Me.GroupBox2.Controls.Add(Me.chkMoneda)
        Me.GroupBox2.Location = New System.Drawing.Point(484, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(182, 201)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtZafra
        '
        Me.txtZafra.Enabled = False
        Me.txtZafra.Location = New System.Drawing.Point(249, 19)
        Me.txtZafra.Name = "txtZafra"
        Me.txtZafra.Size = New System.Drawing.Size(59, 20)
        Me.txtZafra.TabIndex = 17
        '
        'txtNroAcuerdo
        '
        Me.txtNroAcuerdo.Enabled = False
        Me.txtNroAcuerdo.Location = New System.Drawing.Point(91, 20)
        Me.txtNroAcuerdo.Name = "txtNroAcuerdo"
        Me.txtNroAcuerdo.Size = New System.Drawing.Size(59, 20)
        Me.txtNroAcuerdo.TabIndex = 18
        '
        'chkZafra
        '
        Me.chkZafra.BackColor = System.Drawing.Color.Transparent
        Me.chkZafra.Color = System.Drawing.Color.Empty
        Me.chkZafra.Location = New System.Drawing.Point(189, 19)
        Me.chkZafra.Name = "chkZafra"
        Me.chkZafra.Size = New System.Drawing.Size(54, 21)
        Me.chkZafra.SoloLectura = False
        Me.chkZafra.TabIndex = 16
        Me.chkZafra.Texto = "Zafra:"
        Me.chkZafra.Valor = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(6, 125)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(65, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 14
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(6, 69)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(65, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 13
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'chkAcuerdo
        '
        Me.chkAcuerdo.BackColor = System.Drawing.Color.Transparent
        Me.chkAcuerdo.Color = System.Drawing.Color.Empty
        Me.chkAcuerdo.Location = New System.Drawing.Point(6, 19)
        Me.chkAcuerdo.Name = "chkAcuerdo"
        Me.chkAcuerdo.Size = New System.Drawing.Size(65, 21)
        Me.chkAcuerdo.SoloLectura = False
        Me.chkAcuerdo.TabIndex = 12
        Me.chkAcuerdo.Texto = "Acuerdo:"
        Me.chkAcuerdo.Valor = False
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.Enabled = False
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(91, 69)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(343, 52)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 9
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(91, 125)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(343, 47)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 7
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkFechaOP
        '
        Me.chkFechaOP.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaOP.Color = System.Drawing.Color.Empty
        Me.chkFechaOP.Location = New System.Drawing.Point(9, 74)
        Me.chkFechaOP.Name = "chkFechaOP"
        Me.chkFechaOP.Size = New System.Drawing.Size(153, 21)
        Me.chkFechaOP.SoloLectura = False
        Me.chkFechaOP.TabIndex = 49
        Me.chkFechaOP.Texto = "Periodo Orden Pago:"
        Me.chkFechaOP.Valor = False
        '
        'txtHastaOP
        '
        Me.txtHastaOP.AñoFecha = 0
        Me.txtHastaOP.Color = System.Drawing.Color.Empty
        Me.txtHastaOP.Enabled = False
        Me.txtHastaOP.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtHastaOP.Location = New System.Drawing.Point(93, 101)
        Me.txtHastaOP.MesFecha = 0
        Me.txtHastaOP.Name = "txtHastaOP"
        Me.txtHastaOP.PermitirNulo = False
        Me.txtHastaOP.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaOP.SoloLectura = False
        Me.txtHastaOP.TabIndex = 48
        '
        'txtDesdeOP
        '
        Me.txtDesdeOP.AñoFecha = 0
        Me.txtDesdeOP.Color = System.Drawing.Color.Empty
        Me.txtDesdeOP.Enabled = False
        Me.txtDesdeOP.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtDesdeOP.Location = New System.Drawing.Point(9, 101)
        Me.txtDesdeOP.MesFecha = 0
        Me.txtDesdeOP.Name = "txtDesdeOP"
        Me.txtDesdeOP.PermitirNulo = False
        Me.txtDesdeOP.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeOP.SoloLectura = False
        Me.txtDesdeOP.TabIndex = 47
        '
        'chkFechaFactura
        '
        Me.chkFechaFactura.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaFactura.Color = System.Drawing.Color.Empty
        Me.chkFechaFactura.Location = New System.Drawing.Point(9, 17)
        Me.chkFechaFactura.Name = "chkFechaFactura"
        Me.chkFechaFactura.Size = New System.Drawing.Size(153, 21)
        Me.chkFechaFactura.SoloLectura = False
        Me.chkFechaFactura.TabIndex = 46
        Me.chkFechaFactura.Texto = "Periodo Factura:"
        Me.chkFechaFactura.Valor = False
        '
        'txtHastaFactura
        '
        Me.txtHastaFactura.AñoFecha = 0
        Me.txtHastaFactura.Color = System.Drawing.Color.Empty
        Me.txtHastaFactura.Enabled = False
        Me.txtHastaFactura.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtHastaFactura.Location = New System.Drawing.Point(93, 44)
        Me.txtHastaFactura.MesFecha = 0
        Me.txtHastaFactura.Name = "txtHastaFactura"
        Me.txtHastaFactura.PermitirNulo = False
        Me.txtHastaFactura.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaFactura.SoloLectura = False
        Me.txtHastaFactura.TabIndex = 2
        '
        'txtDesdeFactura
        '
        Me.txtDesdeFactura.AñoFecha = 0
        Me.txtDesdeFactura.Color = System.Drawing.Color.Empty
        Me.txtDesdeFactura.Enabled = False
        Me.txtDesdeFactura.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtDesdeFactura.Location = New System.Drawing.Point(9, 44)
        Me.txtDesdeFactura.MesFecha = 0
        Me.txtDesdeFactura.Name = "txtDesdeFactura"
        Me.txtDesdeFactura.PermitirNulo = False
        Me.txtDesdeFactura.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeFactura.SoloLectura = False
        Me.txtDesdeFactura.TabIndex = 1
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = ""
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = ""
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = ""
        Me.cbxMoneda.DataSource = ""
        Me.cbxMoneda.DataValueMember = ""
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(9, 151)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(158, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 1
        Me.cbxMoneda.Texto = ""
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(8, 130)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(65, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 0
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'frmListadoGastoAcuerdo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(677, 254)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmListadoGastoAcuerdo"
        Me.Text = "frmListadoGastoAcuerdo"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnInforme As Button
    Friend WithEvents chkAcuerdo As ocxCHK
    Friend WithEvents btnCerrar As Button
    Friend WithEvents gbxFiltro As GroupBox
    Public WithEvents txtProveedor As ocxTXTProveedor
    Friend WithEvents chkMoneda As ocxCHK
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents cbxMoneda As ocxCBX
    Friend WithEvents txtHastaFactura As ocxTXTDate
    Friend WithEvents txtDesdeFactura As ocxTXTDate
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents chkFechaOP As ocxCHK
    Friend WithEvents txtHastaOP As ocxTXTDate
    Friend WithEvents txtDesdeOP As ocxTXTDate
    Friend WithEvents chkFechaFactura As ocxCHK
    Friend WithEvents chkProducto As ocxCHK
    Friend WithEvents chkProveedor As ocxCHK
    Friend WithEvents chkZafra As ocxCHK
    Friend WithEvents txtNroAcuerdo As TextBox
    Friend WithEvents txtZafra As TextBox
End Class

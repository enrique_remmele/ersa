﻿Imports ERP.Reporte
Public Class frmInformeAcuerdo
    'CLASES
    Dim CReporte As New CReporteAcuerdo
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "LISTADO DE ACUERDOS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        txtProducto.Conectar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)
        'Movimiento
        CSistema.SqlToComboBox(cbxMoneda, "Select Distinct ID, Descripcion from VMoneda")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.SelectedIndex = 0
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Ordenado por
        cbxOrdenadoPor.cbx.Items.Add("Producto")
        cbxOrdenadoPor.cbx.Items.Add("NroAcuerdo")
        cbxOrdenadoPor.cbx.Items.Add("Fecha")
        cbxOrdenadoPor.cbx.Items.Add("Proveedor")
        cbxOrdenadoPor.cbx.Items.Add("Zafra")
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Estado
        cbxEstado.cbx.Items.Add("VIGENTES")
        cbxEstado.cbx.Items.Add("CUMPLIDOS")
        cbxEstado.cbx.Items.Add("TODOS")
        cbxEstado.cbx.SelectedIndex = 0
        cbxEstado.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "


    End Sub

    Sub Listar()

        'If txtProducto.Seleccionado = False Then
        '    MessageBox.Show("Selecione un Producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        '    Exit Sub
        'End If

        Dim Where As String = ""
        Dim SubTitulo As String = ""
        Dim Producto As String = ""
        Dim Existencia As Decimal = 0
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim OrdenadoFechaOperacion As Boolean = False

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        SubTitulo = txtDesde.txt.Text & " - " & txtHasta.txt.Text
        If txtProducto.Seleccionado = True And txtProducto.txt.txt.Text <> "" Then
            Where = Where & " And IDProducto=" & txtProducto.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prod: " & txtProducto.txt.txt.Text
        End If
        If txtProveedor.Seleccionado = True And txtProveedor.txtRazonSocial.txt.Text <> "" Then
            Where = Where & " And IDProveedor=" & txtProveedor.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prov: " & txtProveedor.txtRazonSocial.txt.Text
        End If

        If chkZafra.Valor = True Then
            Where = Where & " And Zafra=" & txtZafra.ObtenerValor & " "
            SubTitulo = SubTitulo & "  -  Zafra: " & txtZafra.ObtenerValor
        End If

        If cbxEstado.txt.Text = "CUMPLIDOS" Then
            Where = Where & " and Anulado = 1 "
        End If

        If cbxEstado.txt.Text = "VIGENTES" Then
            Where = Where & " and Anulado = 0 "
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls
            If ctr.GetType.Name.ToUpper = "OCXCBX" Then

                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        If chkMoneda.Valor = True Then
            If Where = "" Then
                Where = "Where IDMoneda = " & "'" & cbxMoneda.GetValue & "'"
            Else
                Where = Where & " And IDMoneda = " & "'" & cbxMoneda.GetValue & "'"
            End If
            SubTitulo = SubTitulo & "  -  Moneda: " & cbxMoneda.Texto
        End If

        'Establecemos el Orden
        'OrderBy = " Order By Fecha"

        If cbxOrdenadoPor.txt.Text <> "" Then
            OrderBy = "Order by " & cbxOrdenadoPor.txt.Text
            If cbxEnForma.txt.Text <> "" Then
                OrderBy = OrderBy & " " & cbxEnForma.txt.Text
            End If
        End If

        'ArmarSubTitulo(SubTitulo)
        TipoInforme = ""
        CReporte.Acuerdo(frm, Where, Titulo, TipoInforme & " - " & SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo)


    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)
        SubTitulo = "Del " & txtDesde.GetValue.ToShortDateString & " Al " & txtHasta.GetValue.ToShortDateString
        For Each ctr As Object In gbxFiltro.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubtitulo(SubTitulo)
            End If
        Next
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            txtDesde.Focus()
        End If
    End Sub

    Private Sub frmInformeAcuerdo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmInformeAcuerdo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkZafra_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkZafra.PropertyChanged
        txtZafra.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

End Class
﻿Imports ERP.Reporte

Public Class frmListadoCargaMercaderia

    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "PICKING"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()
        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.SelectedIndex = 0

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0



    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where CONVERT(DATE, Fecha) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.SelectedValue <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.SelectedValue
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        Select Case cbxTipoInforme.cbx.SelectedIndex
            Case 0
                CReporte.ListadoCargaMercaderia(frm, Titulo, cbxTipoInforme.txt.Text, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)
            Case 1
                'cbxProducto.EstablecerCondicion(WhereDetalle)
                CReporte.ListadoCargaMercaderiaDetallado(frm, Titulo, cbxTipoInforme.txt.Text, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)
        End Select


    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        'Dim dt As DataTable = Nothing

        'cbxOrdenadoPor.cbx.Items.Clear()


        'dt = CData.GetStructure("VCargaMercaderia ", "Select Top(0)Fecha,Numero,Sucursal From VCargaMercaderia")


        'If dt Is Nothing Then
        '    Exit Sub
        'End If

        'For i As Integer = 0 To dt.Columns.Count - 1
        '    cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        'Next
        Dim listOrdenadoPor As New List(Of Dato)
        Dim obj As Dato = New Dato
        obj.descripcion = "Fecha"
        obj.idString = "Fecha, IDTransaccion, Comprobante, Lote"
        listOrdenadoPor.Add(obj)
        obj = New Dato
        obj.descripcion = "Número"
        obj.idString = "Numero"
        listOrdenadoPor.Add(obj)
        obj = New Dato
        obj.descripcion = "Lote"
        obj.idString = "Lote"
        listOrdenadoPor.Add(obj)
        obj = New Dato
        obj.descripcion = "Usuario"
        obj.idString = "Usuario"
        listOrdenadoPor.Add(obj)
        obj = New Dato
        obj.descripcion = "Distribuidor"
        obj.idString = "Distribuidor"
        listOrdenadoPor.Add(obj)
        obj = New Dato
        obj.descripcion = "Vehículo"
        obj.idString = "Vehiculo"
        listOrdenadoPor.Add(obj)
        obj = New Dato
        obj.descripcion = "Chofer"
        obj.idString = "Chofer"
        listOrdenadoPor.Add(obj)
        obj = New Dato
        obj.descripcion = "Zona"
        obj.idString = "Zona"
        listOrdenadoPor.Add(obj)

        DatoBindingSource.DataSource = listOrdenadoPor

    End Sub

    Sub SeleccionarTipoInforme()
        'Select Case cbxTipoInforme.cbx.SelectedIndex
        '    Case 0
        '        chkProducto.chk.Checked = False
        '        cbxProducto.Enabled = False
        'End Select
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        SeleccionarTipoInforme()
    End Sub

    'Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
    '    cbxProducto.Enabled = value
    'End Sub

    Private Sub chkSucursal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSucursal.Load

    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkZona_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkZona.Load

    End Sub

    Private Sub chkZona_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkZona.PropertyChanged
        cbxZona.Enabled = value
    End Sub

    Private Sub chkChofer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkChofer.Load

    End Sub

    Private Sub chkChofer_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub

    Private Sub chkDistribuidor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDistribuidor.Load

    End Sub

    Private Sub chkDistribuidor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDistribuidor.PropertyChanged
        cbxDistribuidor.Enabled = value
    End Sub

    Private Sub cbxZona_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxZona.Load

    End Sub
End Class




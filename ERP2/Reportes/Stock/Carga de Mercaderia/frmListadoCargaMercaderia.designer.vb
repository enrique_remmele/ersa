﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoCargaMercaderia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkDistribuidor = New ERP.ocxCHK()
        Me.cbxDistribuidor = New ERP.ocxCBX()
        Me.chkChofer = New ERP.ocxCHK()
        Me.chkZona = New ERP.ocxCHK()
        Me.cbxZona = New ERP.ocxCBX()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxOrdenadoPor = New System.Windows.Forms.ComboBox()
        Me.DatoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DatoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkDistribuidor)
        Me.gbxFiltro.Controls.Add(Me.cbxDistribuidor)
        Me.gbxFiltro.Controls.Add(Me.chkChofer)
        Me.gbxFiltro.Controls.Add(Me.chkZona)
        Me.gbxFiltro.Controls.Add(Me.cbxZona)
        Me.gbxFiltro.Controls.Add(Me.cbxChofer)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(354, 193)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkDistribuidor
        '
        Me.chkDistribuidor.BackColor = System.Drawing.Color.Transparent
        Me.chkDistribuidor.Color = System.Drawing.Color.Empty
        Me.chkDistribuidor.Location = New System.Drawing.Point(6, 88)
        Me.chkDistribuidor.Name = "chkDistribuidor"
        Me.chkDistribuidor.Size = New System.Drawing.Size(114, 21)
        Me.chkDistribuidor.SoloLectura = False
        Me.chkDistribuidor.TabIndex = 16
        Me.chkDistribuidor.Texto = "Distribuidor:"
        Me.chkDistribuidor.Valor = False
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.CampoWhere = "IDDistribuidor"
        Me.cbxDistribuidor.CargarUnaSolaVez = False
        Me.cbxDistribuidor.DataDisplayMember = "Nombres"
        Me.cbxDistribuidor.DataFilter = Nothing
        Me.cbxDistribuidor.DataOrderBy = Nothing
        Me.cbxDistribuidor.DataSource = "Distribuidor"
        Me.cbxDistribuidor.DataValueMember = "ID"
        Me.cbxDistribuidor.Enabled = False
        Me.cbxDistribuidor.FormABM = Nothing
        Me.cbxDistribuidor.Indicaciones = Nothing
        Me.cbxDistribuidor.Location = New System.Drawing.Point(125, 87)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.SeleccionObligatoria = False
        Me.cbxDistribuidor.Size = New System.Drawing.Size(213, 21)
        Me.cbxDistribuidor.SoloLectura = False
        Me.cbxDistribuidor.TabIndex = 17
        Me.cbxDistribuidor.Texto = ""
        '
        'chkChofer
        '
        Me.chkChofer.BackColor = System.Drawing.Color.Transparent
        Me.chkChofer.Color = System.Drawing.Color.Empty
        Me.chkChofer.Location = New System.Drawing.Point(6, 115)
        Me.chkChofer.Name = "chkChofer"
        Me.chkChofer.Size = New System.Drawing.Size(114, 21)
        Me.chkChofer.SoloLectura = False
        Me.chkChofer.TabIndex = 14
        Me.chkChofer.Texto = "Chofer:"
        Me.chkChofer.Valor = False
        '
        'chkZona
        '
        Me.chkZona.BackColor = System.Drawing.Color.Transparent
        Me.chkZona.Color = System.Drawing.Color.Empty
        Me.chkZona.Location = New System.Drawing.Point(6, 61)
        Me.chkZona.Name = "chkZona"
        Me.chkZona.Size = New System.Drawing.Size(114, 21)
        Me.chkZona.SoloLectura = False
        Me.chkZona.TabIndex = 12
        Me.chkZona.Texto = "Zona:"
        Me.chkZona.Valor = False
        '
        'cbxZona
        '
        Me.cbxZona.CampoWhere = "IDZonaVenta"
        Me.cbxZona.CargarUnaSolaVez = False
        Me.cbxZona.DataDisplayMember = "Descripcion"
        Me.cbxZona.DataFilter = Nothing
        Me.cbxZona.DataOrderBy = "Descripcion"
        Me.cbxZona.DataSource = "ZonaVenta"
        Me.cbxZona.DataValueMember = "ID"
        Me.cbxZona.Enabled = False
        Me.cbxZona.FormABM = Nothing
        Me.cbxZona.Indicaciones = Nothing
        Me.cbxZona.Location = New System.Drawing.Point(125, 60)
        Me.cbxZona.Name = "cbxZona"
        Me.cbxZona.SeleccionObligatoria = False
        Me.cbxZona.Size = New System.Drawing.Size(213, 21)
        Me.cbxZona.SoloLectura = False
        Me.cbxZona.TabIndex = 13
        Me.cbxZona.Texto = ""
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = "IDChofer"
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = Nothing
        Me.cbxChofer.DataSource = "Chofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.Enabled = False
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(125, 114)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionObligatoria = False
        Me.cbxChofer.Size = New System.Drawing.Size(213, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 15
        Me.cbxChofer.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(6, 34)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 10
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(125, 33)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 11
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(507, 202)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.cbxTipoInforme)
        Me.GroupBox2.Controls.Add(Me.lblInforme)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(363, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 193)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.DataSource = Me.DatoBindingSource
        Me.cbxOrdenadoPor.DisplayMember = "descripcion"
        Me.cbxOrdenadoPor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxOrdenadoPor.FormattingEnabled = True
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 120)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.TabIndex = 12
        Me.cbxOrdenadoPor.ValueMember = "idString"
        '
        'DatoBindingSource
        '
        Me.DatoBindingSource.DataSource = GetType(ERP.Dato)
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(6, 76)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionObligatoria = True
        Me.cbxTipoInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 11
        Me.cbxTipoInforme.Texto = ""
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(5, 60)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(45, 13)
        Me.lblInforme.TabIndex = 10
        Me.lblInforme.Text = "Informe:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(169, 120)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(67, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 104)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(6, 160)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 7
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 144)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 6
        Me.lblRanking.Text = "Ranking:"
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 3, 9, 45, 39, 128)
        Me.txtHasta.Location = New System.Drawing.Point(89, 35)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 3, 9, 45, 39, 128)
        Me.txtDesde.Location = New System.Drawing.Point(6, 35)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(401, 202)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(100, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmListadoCargaMercaderia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(615, 232)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoCargaMercaderia"
        Me.Text = "Listado Carga de Mercaderías"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DatoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxTipoInforme As ERP.ocxCBX
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents cbxOrdenadoPor As System.Windows.Forms.ComboBox
    Friend WithEvents DatoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkZona As ERP.ocxCHK
    Friend WithEvents cbxZona As ERP.ocxCBX
    Friend WithEvents chkChofer As ERP.ocxCHK
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents chkDistribuidor As ERP.ocxCHK
    Friend WithEvents cbxDistribuidor As ERP.ocxCBX
End Class

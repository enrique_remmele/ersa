﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmExistenciaValorizada
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkSubLinea = New ERP.ocxCHK()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.txtCuentaContable = New ERP.ocxTXTCuentaContable()
        Me.chkCuentaContable = New ERP.ocxCHK()
        Me.chkExcluirDeposito = New ERP.ocxCHK()
        Me.chkExcluirSucursal = New ERP.ocxCHK()
        Me.chkExcluirSubLinea = New ERP.ocxCHK()
        Me.chkExcluirLinea = New ERP.ocxCHK()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkExcluirTipoProducto = New ERP.ocxCHK()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkExcluirProductosSinCostoPromedio = New ERP.ocxCHK()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.cbxOrdenadoPor = New System.Windows.Forms.ComboBox()
        Me.OrdenadoPorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.chkActivo = New ERP.ocxCHK()
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.OrdenadoPorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaContable)
        Me.gbxFiltro.Controls.Add(Me.chkCuentaContable)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirLinea)
        Me.gbxFiltro.Controls.Add(Me.Label3)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 11)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(485, 279)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkSubLinea
        '
        Me.chkSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea.Color = System.Drawing.Color.Empty
        Me.chkSubLinea.Location = New System.Drawing.Point(11, 79)
        Me.chkSubLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSubLinea.Name = "chkSubLinea"
        Me.chkSubLinea.Size = New System.Drawing.Size(101, 21)
        Me.chkSubLinea.SoloLectura = False
        Me.chkSubLinea.TabIndex = 41
        Me.chkSubLinea.Texto = "SubLinea:"
        Me.chkSubLinea.Valor = False
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = "E.IDSubLinea"
        Me.cbxSubLinea.CargarUnaSolaVez = False
        Me.cbxSubLinea.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = "Descripcion"
        Me.cbxSubLinea.DataSource = "VSubLinea"
        Me.cbxSubLinea.DataValueMember = "ID"
        Me.cbxSubLinea.dtSeleccionado = Nothing
        Me.cbxSubLinea.Enabled = False
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea.Location = New System.Drawing.Point(114, 79)
        Me.cbxSubLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionMultiple = False
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(327, 21)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 42
        Me.cbxSubLinea.Texto = ""
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.AlturaMaxima = 0
        Me.txtCuentaContable.Consulta = Nothing
        Me.txtCuentaContable.Enabled = False
        Me.txtCuentaContable.IDCentroCosto = 0
        Me.txtCuentaContable.IDUnidadNegocio = 0
        Me.txtCuentaContable.ListarTodas = False
        Me.txtCuentaContable.Location = New System.Drawing.Point(114, 172)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Registro = Nothing
        Me.txtCuentaContable.Resolucion173 = False
        Me.txtCuentaContable.Seleccionado = False
        Me.txtCuentaContable.Size = New System.Drawing.Size(326, 47)
        Me.txtCuentaContable.SoloLectura = False
        Me.txtCuentaContable.TabIndex = 40
        Me.txtCuentaContable.Texto = Nothing
        Me.txtCuentaContable.whereFiltro = Nothing
        '
        'chkCuentaContable
        '
        Me.chkCuentaContable.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaContable.Color = System.Drawing.Color.Empty
        Me.chkCuentaContable.Location = New System.Drawing.Point(11, 172)
        Me.chkCuentaContable.Name = "chkCuentaContable"
        Me.chkCuentaContable.Size = New System.Drawing.Size(97, 21)
        Me.chkCuentaContable.SoloLectura = False
        Me.chkCuentaContable.TabIndex = 39
        Me.chkCuentaContable.Texto = "Cta. Mercaderia:"
        Me.chkCuentaContable.Valor = False
        '
        'chkExcluirDeposito
        '
        Me.chkExcluirDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirDeposito.Color = System.Drawing.Color.Empty
        Me.chkExcluirDeposito.Location = New System.Drawing.Point(452, 135)
        Me.chkExcluirDeposito.Name = "chkExcluirDeposito"
        Me.chkExcluirDeposito.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirDeposito.SoloLectura = False
        Me.chkExcluirDeposito.TabIndex = 36
        Me.chkExcluirDeposito.Texto = ""
        Me.chkExcluirDeposito.Valor = False
        '
        'chkExcluirSucursal
        '
        Me.chkExcluirSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirSucursal.Color = System.Drawing.Color.Empty
        Me.chkExcluirSucursal.Location = New System.Drawing.Point(452, 107)
        Me.chkExcluirSucursal.Name = "chkExcluirSucursal"
        Me.chkExcluirSucursal.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirSucursal.SoloLectura = False
        Me.chkExcluirSucursal.TabIndex = 35
        Me.chkExcluirSucursal.Texto = ""
        Me.chkExcluirSucursal.Valor = False
        '
        'chkExcluirSubLinea
        '
        Me.chkExcluirSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirSubLinea.Color = System.Drawing.Color.Empty
        Me.chkExcluirSubLinea.Location = New System.Drawing.Point(452, 79)
        Me.chkExcluirSubLinea.Name = "chkExcluirSubLinea"
        Me.chkExcluirSubLinea.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirSubLinea.SoloLectura = False
        Me.chkExcluirSubLinea.TabIndex = 34
        Me.chkExcluirSubLinea.Texto = ""
        Me.chkExcluirSubLinea.Valor = False
        '
        'chkExcluirLinea
        '
        Me.chkExcluirLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirLinea.Color = System.Drawing.Color.Empty
        Me.chkExcluirLinea.Location = New System.Drawing.Point(452, 51)
        Me.chkExcluirLinea.Name = "chkExcluirLinea"
        Me.chkExcluirLinea.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirLinea.SoloLectura = False
        Me.chkExcluirLinea.TabIndex = 33
        Me.chkExcluirLinea.Texto = ""
        Me.chkExcluirLinea.Valor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(446, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 12)
        Me.Label3.TabIndex = 31
        Me.Label3.Text = "Excluir"
        '
        'chkExcluirTipoProducto
        '
        Me.chkExcluirTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoProducto.Location = New System.Drawing.Point(452, 23)
        Me.chkExcluirTipoProducto.Name = "chkExcluirTipoProducto"
        Me.chkExcluirTipoProducto.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoProducto.SoloLectura = False
        Me.chkExcluirTipoProducto.TabIndex = 32
        Me.chkExcluirTipoProducto.Texto = ""
        Me.chkExcluirTipoProducto.Valor = False
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(114, 219)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(327, 53)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 17
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(11, 219)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(97, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 30
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(11, 51)
        Me.chkLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(97, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 2
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "E.IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "VLinea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.dtSeleccionado = Nothing
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(114, 51)
        Me.cbxLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionMultiple = False
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(327, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 3
        Me.cbxLinea.Texto = "BEBIDAS"
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(11, 23)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(97, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 0
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "E.IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(114, 23)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(327, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 1
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(11, 135)
        Me.chkDeposito.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(97, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 8
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "E.IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(114, 135)
        Me.cbxDeposito.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(327, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 9
        Me.cbxDeposito.Texto = "ASU - VENTAS01"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(11, 107)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(97, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "E.IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(114, 107)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(327, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkExcluirProductosSinCostoPromedio)
        Me.GroupBox2.Controls.Add(Me.RadioButton2)
        Me.GroupBox2.Controls.Add(Me.RadioButton1)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.chkActivo)
        Me.GroupBox2.Controls.Add(Me.cbxTipoInforme)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Location = New System.Drawing.Point(503, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(262, 240)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkExcluirProductosSinCostoPromedio
        '
        Me.chkExcluirProductosSinCostoPromedio.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirProductosSinCostoPromedio.Color = System.Drawing.Color.Empty
        Me.chkExcluirProductosSinCostoPromedio.Location = New System.Drawing.Point(6, 177)
        Me.chkExcluirProductosSinCostoPromedio.Margin = New System.Windows.Forms.Padding(4)
        Me.chkExcluirProductosSinCostoPromedio.Name = "chkExcluirProductosSinCostoPromedio"
        Me.chkExcluirProductosSinCostoPromedio.Size = New System.Drawing.Size(202, 21)
        Me.chkExcluirProductosSinCostoPromedio.SoloLectura = False
        Me.chkExcluirProductosSinCostoPromedio.TabIndex = 14
        Me.chkExcluirProductosSinCostoPromedio.Texto = "Excluir productos sin costo promedio"
        Me.chkExcluirProductosSinCostoPromedio.Valor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(130, 20)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(33, 17)
        Me.RadioButton2.TabIndex = 13
        Me.RadioButton2.Text = "al"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(57, 20)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(55, 17)
        Me.RadioButton1.TabIndex = 12
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Actual"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(2013, 6, 5, 14, 20, 44, 687)
        Me.txtHasta.Location = New System.Drawing.Point(169, 20)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 11
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(11, 22)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(40, 13)
        Me.lblPeriodo.TabIndex = 9
        Me.lblPeriodo.Text = "Fecha:"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.DataSource = Me.OrdenadoPorBindingSource
        Me.cbxOrdenadoPor.DisplayMember = "descripcion"
        Me.cbxOrdenadoPor.FormattingEnabled = True
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 123)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.TabIndex = 8
        Me.cbxOrdenadoPor.ValueMember = "idString"
        '
        'OrdenadoPorBindingSource
        '
        Me.OrdenadoPorBindingSource.DataSource = GetType(ERP.Dato)
        '
        'chkActivo
        '
        Me.chkActivo.BackColor = System.Drawing.Color.Transparent
        Me.chkActivo.Color = System.Drawing.Color.Empty
        Me.chkActivo.Enabled = False
        Me.chkActivo.Location = New System.Drawing.Point(6, 150)
        Me.chkActivo.Margin = New System.Windows.Forms.Padding(4)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(114, 21)
        Me.chkActivo.SoloLectura = False
        Me.chkActivo.TabIndex = 7
        Me.chkActivo.Texto = "Sólo Activos"
        Me.chkActivo.Valor = True
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.dtSeleccionado = Nothing
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(6, 65)
        Me.cbxTipoInforme.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionMultiple = False
        Me.cbxTipoInforme.SeleccionObligatoria = False
        Me.cbxTipoInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 1
        Me.cbxTipoInforme.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Tipo Informe:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(169, 123)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(74, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 4
        Me.cbxEnForma.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(11, 107)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 2
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(169, 65)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 6
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(174, 49)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 5
        Me.lblRanking.Text = "Ranking:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(663, 282)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(557, 282)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(100, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmExistenciaValorizada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(775, 320)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmExistenciaValorizada"
        Me.Text = "frmExistenciaValorizada"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.OrdenadoPorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents cbxTipoInforme As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkActivo As ERP.ocxCHK
    Friend WithEvents cbxOrdenadoPor As System.Windows.Forms.ComboBox
    Friend WithEvents OrdenadoPorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents chkExcluirProductosSinCostoPromedio As ERP.ocxCHK
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents chkExcluirDeposito As ocxCHK
    Friend WithEvents chkExcluirSucursal As ocxCHK
    Friend WithEvents chkExcluirSubLinea As ocxCHK
    Friend WithEvents chkExcluirLinea As ocxCHK
    Friend WithEvents Label3 As Label
    Friend WithEvents chkExcluirTipoProducto As ocxCHK
    Friend WithEvents txtCuentaContable As ocxTXTCuentaContable
    Friend WithEvents chkCuentaContable As ocxCHK
    Friend WithEvents chkSubLinea As ocxCHK
    Friend WithEvents cbxSubLinea As ocxCBX
End Class

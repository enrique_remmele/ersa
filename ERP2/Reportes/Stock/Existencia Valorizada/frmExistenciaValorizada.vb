﻿Imports ERP.Reporte

Public Class frmExistenciaValorizada

    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES

    Private listaOrdenadoPor As List(Of Dato)

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        txtHasta.SetValue(Date.Today)

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.DropDownStyle = ComboBoxStyle.DropDownList

        CambiarOrdenacion()

        'Ventas
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO CC")
        cbxTipoInforme.cbx.Items.Add("DETALLADO VS KARDEX")

        txtProducto.Conectar()
        txtProducto.Enabled = False
        txtCuentaContable.Conectar()
        txtCuentaContable.Enabled = False

        cbxDeposito.SeleccionMultiple = True
        cbxSucursal.SeleccionMultiple = True
        cbxTipoProducto.SeleccionMultiple = True
        cbxLinea.SeleccionMultiple = True
        cbxSubLinea.SeleccionMultiple = True

        txtProducto.SeleccionMultiple = True
        'Tipo de Venta
        cbxTipoInforme.cbx.SelectedIndex = 0

        chkActivo.chk.Checked = True
    End Sub

    Sub Listar()
        Dim Titulo As String = "EXISTENCIA VALORIZADA"
        Dim TipoInforme As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkActivo.chk.Checked = True Then
            'Filtrar por Fecha
            Where = " Where E.Estado=1 "
            If chkExcluirProductosSinCostoPromedio.chk.Checked Then
                Where = Where & "AND E.CostoPromedio > 0 "
            End If
        Else
            If chkExcluirProductosSinCostoPromedio.chk.Checked Then
                Where = " WHERE E.CostoPromedio > 0 "
            End If
        End If

        EstablecerCondicion(Where)

        If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
            Where = IIf(String.IsNullOrEmpty(Where), " where (E.IDProducto) = " + txtProducto.Registro("Id").ToString, Where + " and (E.IDProducto) = " + txtProducto.Registro("Id").ToString)
        End If

        If txtCuentaContable.Registro IsNot Nothing And txtCuentaContable.Enabled Then
            Where = IIf(String.IsNullOrEmpty(Where), " where (CuentaContable) like '" + txtCuentaContable.Registro("codigo").ToString & "%'", Where + " and (cuentacontable) like '" + txtCuentaContable.Registro("codigo").ToString) & "%'"
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro)

        If RadioButton2.Checked Then
            TipoInforme = TipoInforme & " - Al " & txtHasta.txt.Text
        End If

        'Establecemos el Orden
        Dim objOrderBy As Dato = OrdenadoPorBindingSource.Current

        OrderBy = "Order by " & objOrderBy.idString

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        Select Case cbxTipoInforme.cbx.SelectedIndex

            Case 0
                If RadioButton1.Checked Then
                    CReporte.ExistenciaValorizadaDetalleAsync(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)
                    ''CReporte.ExistenciaValorizadaResumen(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)
                Else
                    ListarInformeResumido()
                End If

            Case 1
                If RadioButton1.Checked Then
                    CReporte.ExistenciaValorizadaDetalle(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)
                Else
                    ListarInformeDetallado()
                End If

            Case 2
                ListarInformeDetalladoCuentaContable()
                'CReporte.ExistenciaValorizadaDetalleCuentaContable(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)
            Case 3
                ListarInformeDetalladoComparativoKardex()
        End Select
        desBloquearEstadoOperacion(VGCadenaConexion, "existeVal")
    End Sub

    Sub ListarInformeResumido()

        Dim Titulo As String = "EXISTENCIA VALORIZADA"
        Dim TipoInforme As String = ""
        Dim Where As String = " "
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        EstablecerCondicionSP(Where)

        CReporte.ArmarSubtitulo(TipoInforme, gbxFiltro)

        If RadioButton2.Checked Then
            TipoInforme = TipoInforme & " - Al " & txtHasta.txt.Text
        End If

        ''org    CReporte.ExistenciaValorizadaResumen(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)
        CReporte.ExistenciaValorizadaDetalleAsync(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)
    End Sub

    Sub ListarInformeDetallado()

        Dim Titulo As String = "EXISTENCIA VALORIZADA"
        Dim TipoInforme As String = ""
        Dim Where As String = " "
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        EstablecerCondicionSP(Where)

        CReporte.ArmarSubtitulo(TipoInforme, gbxFiltro)

        If RadioButton2.Checked Then
            TipoInforme = TipoInforme & " - Al " & txtHasta.txt.Text
        End If
        ''org
        ''CReporte.ExistenciaValorizadaDetalle(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)


        CReporte.ExistenciaValorizadaDetalleAsync(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)



    End Sub

    Sub ListarInformeDetalladoComparativoKardex()

        Dim Titulo As String = "EXISTENCIA VALORIZADA OPERATIVA VS KARDEX"
        Dim TipoInforme As String = ""
        Dim Where As String = " "
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        EstablecerCondicionSP(Where)

        CReporte.ArmarSubtitulo(TipoInforme, gbxFiltro)

        If RadioButton2.Checked Then
            TipoInforme = TipoInforme & " - Al " & txtHasta.txt.Text
        End If

        CReporte.ExistenciaValorizadaComparativoKardex(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)

    End Sub

    Sub ListarInformeDetalladoCuentaContable()

        Dim Titulo As String = "EXISTENCIA VALORIZADA POR CUENTA CONTABLE"
        Dim TipoInforme As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        EstablecerCondicionSP(Where)

        CReporte.ArmarSubtitulo(TipoInforme, gbxFiltro)

        If RadioButton2.Checked Then
            TipoInforme = TipoInforme & " - Al " & txtHasta.txt.Text
        End If

        CReporte.ExistenciaValorizadaDetalleCuentaContable(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, RadioButton1.Checked, txtHasta.GetValue)

    End Sub

    Sub EstablecerCondicionSP(ByRef Where As String)
        Where = " @Fecha = '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "

        If chkTipoProducto.chk.Checked Then
            Where = Where & " ,@IDTipoProducto = " & cbxTipoProducto.cbx.SelectedValue
        End If

        If chkLinea.chk.Checked Then
            Where = Where & " ,@IDLinea = " & cbxLinea.cbx.SelectedValue
        End If

        If chkSubLinea.chk.Checked Then
            Where = Where & " ,@IDSubLinea = " & cbxSubLinea.cbx.SelectedValue
        End If
        If chkSucursal.chk.Checked Then
            Where = Where & " ,@IDSucursal = " & cbxSucursal.cbx.SelectedValue
        End If
        If chkDeposito.chk.Checked Then
            Where = Where & " ,@IDDeposito = " & cbxDeposito.cbx.SelectedValue
        End If

        If chkProducto.chk.Checked Then
            Where = Where & " ,@IDProducto = " & txtProducto.Registro("ID")
        End If

        If chkCuentaContable.chk.Checked Then
            Where = Where & " ,@CodigoCuentaContable = '" & txtCuentaContable.Registro("codigo") & "'"
        End If

    End Sub

    Sub EstablecerCondicion(ByRef Where As String)

        'Tipo de Comprobante
        cbxTipoProducto.EstablecerCondicion(Where, chkExcluirTipoProducto.Valor)

        'Sucursal
        cbxSucursal.EstablecerCondicion(Where, chkExcluirSucursal.Valor)

        'Tipo de Producto
        cbxTipoProducto.EstablecerCondicion(Where, chkExcluirTipoProducto.Valor)

        'Linea
        cbxLinea.EstablecerCondicion(Where, chkExcluirLinea.Valor)

        'Marca
        cbxSubLinea.EstablecerCondicion(Where, chkExcluirSubLinea.Valor)

        'Deposito
        cbxDeposito.EstablecerCondicion(Where, chkExcluirDeposito.Valor)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por

        listaOrdenadoPor = New List(Of Dato)
        Dim obj As Dato
       
        Select Case cbxTipoInforme.cbx.Text
            Case "RESUMIDO"
                obj = New Dato
                obj.descripcion = "Tipo de Producto"
                obj.idString = "TipoProducto"
                listaOrdenadoPor.Add(obj)
                obj = New Dato
                obj.descripcion = "Existencia"
                obj.idString = "Existencia"
                listaOrdenadoPor.Add(obj)
                obj = New Dato
                obj.descripcion = "Costo Promedio"
                obj.idString = "CostoPromedio"
                listaOrdenadoPor.Add(obj)
                obj = New Dato
                obj.descripcion = "Total Valorizado"
                obj.idString = "TotalValorizadoCostoPromedio"
                listaOrdenadoPor.Add(obj)
            Case "DETALLADO"
                obj = New Dato
                obj.descripcion = "Producto"
                obj.idString = "E.TipoProducto, E.Linea, E.Producto"
                listaOrdenadoPor.Add(obj)
                obj = New Dato
                obj.descripcion = "Tipo de Producto"
                obj.idString = "E.TipoProducto, E.Linea, E.Producto"
                listaOrdenadoPor.Add(obj)
                obj = New Dato
                obj.descripcion = "Existencia"
                obj.idString = "E.TipoProducto, E.Linea, E.Existencia"
                listaOrdenadoPor.Add(obj)
                obj = New Dato
                obj.descripcion = "Costo Promedio"
                obj.idString = "E.TipoProducto, E.Linea, E.CostoPromedio"
                listaOrdenadoPor.Add(obj)
                obj = New Dato
                obj.descripcion = "Total Valorizado"
                obj.idString = "E.TipoProducto, E.Linea, E.TotalValorizadoCostoPromedio"
                listaOrdenadoPor.Add(obj)

            Case Else
                obj = New Dato
                obj.descripcion = "Cuenta Contable"
                obj.idString = "CuentaContable"
                listaOrdenadoPor.Add(obj)
        End Select
       
        OrdenadoPorBindingSource.DataSource = listaOrdenadoPor
        cbxOrdenadoPor.DataSource = OrdenadoPorBindingSource
        cbxEnForma.cbx.SelectedIndex = 0
        obj = Nothing

    End Sub

    Private Sub frmExistenciaValorizada_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click

        If bloquearEstadoOperacion(VGCadenaConexion, "existeVal") = "OK" Then
            Listar()
        Else
            Dim result As DialogResult = MessageBox.Show("Los procesos y tablas necesarias para este informe estan ocupados con un pedido anterior. " & vbCrLf & "Si continua su pedido debera esperar que los procesos anteriores terminen. " & vbCrLf & vbCrLf & "Esto puede causar errores de conexion a base de datos o tiempo de espera agotado. " & vbCrLf & vbCrLf & "¿Deseas continuar?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If result = DialogResult.Yes Then
                Listar()
            End If
        End If

    End Sub

    Private Sub chkLinea_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        CambiarOrdenacion()
        If cbxTipoInforme.cbx.Text = "DETALLADO CC" Then
            chkCuentaContable.Enabled = True
        Else
            chkCuentaContable.chk.Checked = False
            chkCuentaContable.Enabled = False

            If RadioButton2.Checked Then
                chkCuentaContable.chk.Checked = False
                chkCuentaContable.Enabled = False
                chkActivo.chk.Checked = True
                chkActivo.chk.Enabled = False
                chkExcluirProductosSinCostoPromedio.chk.Checked = False
                chkExcluirProductosSinCostoPromedio.Enabled = False
                chkExcluirDeposito.chk.Checked = False
                chkExcluirDeposito.Enabled = False
                chkExcluirLinea.chk.Checked = False
                chkExcluirLinea.Enabled = False
                chkExcluirSubLinea.chk.Checked = False
                chkExcluirSubLinea.Enabled = False
                chkExcluirSucursal.chk.Checked = False
                chkExcluirSucursal.Enabled = False
                chkExcluirTipoProducto.chk.Checked = False
                chkExcluirTipoProducto.Enabled = False
            Else
                chkActivo.chk.Enabled = True
                chkExcluirProductosSinCostoPromedio.Enabled = True
                chkExcluirDeposito.Enabled = True
                chkExcluirLinea.Enabled = True
                chkExcluirSubLinea.Enabled = True
                chkExcluirSucursal.Enabled = True
                chkExcluirTipoProducto.Enabled = True
            End If
        End If
    End Sub

    Private Sub nudRanking_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudRanking.ValueChanged
        Try
            'Para que al poner un ranking automaticamente sugiera ordenar por TotalValorizado en forma descendente //JGR 16/04/2014
            Me.cbxOrdenadoPor.SelectedIndex = Me.cbxOrdenadoPor.Items.Count - 1
            Me.cbxEnForma.cbx.SelectedIndex = 1
        Catch
        End Try
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RadioButton2.CheckedChanged
        txtHasta.Enabled = RadioButton2.Checked
        If cbxTipoInforme.cbx.SelectedIndex = 2 Then
            If cbxTipoInforme.cbx.Text <> "DETALLADO CC" Then
                chkCuentaContable.chk.Checked = False
                chkCuentaContable.Enabled = False
            End If
            chkActivo.chk.Checked = True
            chkActivo.chk.Enabled = False
            chkExcluirProductosSinCostoPromedio.chk.Checked = False
            chkExcluirProductosSinCostoPromedio.Enabled = False
            chkExcluirDeposito.chk.Checked = False
            chkExcluirDeposito.Enabled = False
            chkExcluirLinea.chk.Checked = False
            chkExcluirLinea.Enabled = False
            chkExcluirSubLinea.chk.Checked = False
            chkExcluirSubLinea.Enabled = False
            chkExcluirSucursal.chk.Checked = False
            chkExcluirSucursal.Enabled = False
            chkExcluirTipoProducto.chk.Checked = False
            chkExcluirTipoProducto.Enabled = False
        Else
            If cbxTipoInforme.cbx.Text = "DETALLADO CC" Then
                chkCuentaContable.Enabled = True
            End If
            chkActivo.chk.Enabled = True
            chkExcluirProductosSinCostoPromedio.Enabled = True
            chkExcluirDeposito.Enabled = True
            chkExcluirLinea.Enabled = True
            chkExcluirSubLinea.Enabled = True
            chkExcluirSucursal.Enabled = True
            chkExcluirTipoProducto.Enabled = True
        End If
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkCuentaContable_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCuentaContable.PropertyChanged
        txtCuentaContable.Enabled = value
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExistenciaMovimientoCalculado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxOrdenadoPor = New System.Windows.Forms.ComboBox()
        Me.chkResumenProducto = New ERP.ocxCHK()
        Me.chkExcluirAnulado = New ERP.ocxCHK()
        Me.chkIncluirPeso = New ERP.ocxCHK()
        Me.chkSinExistencia = New ERP.ocxCHK()
        Me.chkSinMovimientos = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.chkActivo = New ERP.ocxCHK()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkCategoria = New ERP.ocxCHK()
        Me.cbxCategoria = New ERP.ocxCBX()
        Me.chkMarca = New ERP.ocxCHK()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.chkResumenProducto)
        Me.GroupBox2.Controls.Add(Me.chkExcluirAnulado)
        Me.GroupBox2.Controls.Add(Me.chkIncluirPeso)
        Me.GroupBox2.Controls.Add(Me.chkSinExistencia)
        Me.GroupBox2.Controls.Add(Me.chkSinMovimientos)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.chkActivo)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Location = New System.Drawing.Point(459, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(268, 276)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cbxOrdenadoPor.FormattingEnabled = True
        Me.cbxOrdenadoPor.Items.AddRange(New Object() {"Producto", "Referecia", "Existencia Actual", "Deposito"})
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(5, 87)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(150, 21)
        Me.cbxOrdenadoPor.TabIndex = 16
        '
        'chkResumenProducto
        '
        Me.chkResumenProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkResumenProducto.Color = System.Drawing.Color.Empty
        Me.chkResumenProducto.Location = New System.Drawing.Point(5, 145)
        Me.chkResumenProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkResumenProducto.Name = "chkResumenProducto"
        Me.chkResumenProducto.Size = New System.Drawing.Size(150, 21)
        Me.chkResumenProducto.SoloLectura = False
        Me.chkResumenProducto.TabIndex = 15
        Me.chkResumenProducto.Texto = "Resumido Por Producto"
        Me.chkResumenProducto.Valor = False
        '
        'chkExcluirAnulado
        '
        Me.chkExcluirAnulado.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirAnulado.Color = System.Drawing.Color.Empty
        Me.chkExcluirAnulado.Location = New System.Drawing.Point(5, 248)
        Me.chkExcluirAnulado.Margin = New System.Windows.Forms.Padding(4)
        Me.chkExcluirAnulado.Name = "chkExcluirAnulado"
        Me.chkExcluirAnulado.Size = New System.Drawing.Size(229, 21)
        Me.chkExcluirAnulado.SoloLectura = False
        Me.chkExcluirAnulado.TabIndex = 14
        Me.chkExcluirAnulado.Texto = "Excluir Anulados a Hoy"
        Me.chkExcluirAnulado.Valor = False
        '
        'chkIncluirPeso
        '
        Me.chkIncluirPeso.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirPeso.Color = System.Drawing.Color.Empty
        Me.chkIncluirPeso.Location = New System.Drawing.Point(5, 219)
        Me.chkIncluirPeso.Margin = New System.Windows.Forms.Padding(4)
        Me.chkIncluirPeso.Name = "chkIncluirPeso"
        Me.chkIncluirPeso.Size = New System.Drawing.Size(229, 21)
        Me.chkIncluirPeso.SoloLectura = False
        Me.chkIncluirPeso.TabIndex = 13
        Me.chkIncluirPeso.Texto = "Mostrar Peso"
        Me.chkIncluirPeso.Valor = False
        '
        'chkSinExistencia
        '
        Me.chkSinExistencia.BackColor = System.Drawing.Color.Transparent
        Me.chkSinExistencia.Color = System.Drawing.Color.Empty
        Me.chkSinExistencia.Location = New System.Drawing.Point(5, 194)
        Me.chkSinExistencia.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSinExistencia.Name = "chkSinExistencia"
        Me.chkSinExistencia.Size = New System.Drawing.Size(229, 21)
        Me.chkSinExistencia.SoloLectura = False
        Me.chkSinExistencia.TabIndex = 12
        Me.chkSinExistencia.Texto = "Ver Productos sin existencia"
        Me.chkSinExistencia.Valor = False
        '
        'chkSinMovimientos
        '
        Me.chkSinMovimientos.BackColor = System.Drawing.Color.Transparent
        Me.chkSinMovimientos.Color = System.Drawing.Color.Empty
        Me.chkSinMovimientos.Location = New System.Drawing.Point(5, 170)
        Me.chkSinMovimientos.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSinMovimientos.Name = "chkSinMovimientos"
        Me.chkSinMovimientos.Size = New System.Drawing.Size(209, 21)
        Me.chkSinMovimientos.SoloLectura = False
        Me.chkSinMovimientos.TabIndex = 11
        Me.chkSinMovimientos.Texto = "Ver Productos sin movimientos"
        Me.chkSinMovimientos.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(86, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(15, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "al"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 5, 14, 20, 44, 687)
        Me.txtHasta.Location = New System.Drawing.Point(107, 41)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 3
        '
        'chkActivo
        '
        Me.chkActivo.BackColor = System.Drawing.Color.Transparent
        Me.chkActivo.Color = System.Drawing.Color.Empty
        Me.chkActivo.Location = New System.Drawing.Point(5, 120)
        Me.chkActivo.Margin = New System.Windows.Forms.Padding(4)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(114, 21)
        Me.chkActivo.SoloLectura = False
        Me.chkActivo.TabIndex = 9
        Me.chkActivo.Texto = "Sólo Activos"
        Me.chkActivo.Valor = False
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(7, 22)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(40, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Fecha:"
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 5, 14, 20, 44, 687)
        Me.txtDesde.Location = New System.Drawing.Point(6, 41)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(162, 86)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 6
        Me.cbxEnForma.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(3, 70)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 4
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(620, 295)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(514, 295)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(100, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkCategoria)
        Me.gbxFiltro.Controls.Add(Me.cbxCategoria)
        Me.gbxFiltro.Controls.Add(Me.chkMarca)
        Me.gbxFiltro.Controls.Add(Me.cbxMarca)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 15)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(441, 274)
        Me.gbxFiltro.TabIndex = 1
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(112, 171)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(312, 70)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 16
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(11, 149)
        Me.chkProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(95, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 14
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "E.IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = "RazonSocialReferencia"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = "RazonSocialReferencia"
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(112, 149)
        Me.cbxProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(312, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 15
        Me.cbxProveedor.Texto = "19 DE MARZO S.A."
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(11, 170)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(95, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 12
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkCategoria
        '
        Me.chkCategoria.BackColor = System.Drawing.Color.Transparent
        Me.chkCategoria.Color = System.Drawing.Color.Empty
        Me.chkCategoria.Location = New System.Drawing.Point(11, 128)
        Me.chkCategoria.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCategoria.Name = "chkCategoria"
        Me.chkCategoria.Size = New System.Drawing.Size(95, 21)
        Me.chkCategoria.SoloLectura = False
        Me.chkCategoria.TabIndex = 10
        Me.chkCategoria.Texto = "Categoria:"
        Me.chkCategoria.Valor = False
        '
        'cbxCategoria
        '
        Me.cbxCategoria.CampoWhere = "E.IDCategoria"
        Me.cbxCategoria.CargarUnaSolaVez = False
        Me.cbxCategoria.DataDisplayMember = "Descripcion"
        Me.cbxCategoria.DataFilter = Nothing
        Me.cbxCategoria.DataOrderBy = "Descripcion"
        Me.cbxCategoria.DataSource = "Categoria"
        Me.cbxCategoria.DataValueMember = "ID"
        Me.cbxCategoria.dtSeleccionado = Nothing
        Me.cbxCategoria.Enabled = False
        Me.cbxCategoria.FormABM = Nothing
        Me.cbxCategoria.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxCategoria.Location = New System.Drawing.Point(112, 128)
        Me.cbxCategoria.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCategoria.Name = "cbxCategoria"
        Me.cbxCategoria.SeleccionMultiple = False
        Me.cbxCategoria.SeleccionObligatoria = False
        Me.cbxCategoria.Size = New System.Drawing.Size(312, 21)
        Me.cbxCategoria.SoloLectura = False
        Me.cbxCategoria.TabIndex = 11
        Me.cbxCategoria.Texto = ""
        '
        'chkMarca
        '
        Me.chkMarca.BackColor = System.Drawing.Color.Transparent
        Me.chkMarca.Color = System.Drawing.Color.Empty
        Me.chkMarca.Location = New System.Drawing.Point(11, 65)
        Me.chkMarca.Margin = New System.Windows.Forms.Padding(4)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Size = New System.Drawing.Size(95, 21)
        Me.chkMarca.SoloLectura = False
        Me.chkMarca.TabIndex = 4
        Me.chkMarca.Texto = "Marca:"
        Me.chkMarca.Valor = False
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = "E.IDMarca"
        Me.cbxMarca.CargarUnaSolaVez = False
        Me.cbxMarca.DataDisplayMember = "Descripcion"
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = "Descripcion"
        Me.cbxMarca.DataSource = "VMarca"
        Me.cbxMarca.DataValueMember = "ID"
        Me.cbxMarca.dtSeleccionado = Nothing
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = Nothing
        Me.cbxMarca.Location = New System.Drawing.Point(112, 65)
        Me.cbxMarca.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionMultiple = False
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(312, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 5
        Me.cbxMarca.Texto = "ADES"
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(11, 44)
        Me.chkLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(95, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 2
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "E.IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "VLinea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.dtSeleccionado = Nothing
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(112, 44)
        Me.cbxLinea.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionMultiple = False
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(312, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 3
        Me.cbxLinea.Texto = "BEBIDAS"
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(11, 23)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(95, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 0
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "E.IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(112, 23)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(312, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 1
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(11, 107)
        Me.chkDeposito.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(95, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 8
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "E.IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(112, 107)
        Me.cbxDeposito.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(312, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 9
        Me.cbxDeposito.Texto = "AVERIADOS"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(11, 86)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(95, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "E.IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(112, 86)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(312, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'frmExistenciaMovimientoCalculado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(735, 328)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmExistenciaMovimientoCalculado"
        Me.Text = "Movimiento Producto"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkMarca As ERP.ocxCHK
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkActivo As ERP.ocxCHK
    Friend WithEvents chkCategoria As ERP.ocxCHK
    Friend WithEvents cbxCategoria As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents chkSinExistencia As ERP.ocxCHK
    Friend WithEvents chkSinMovimientos As ERP.ocxCHK
    Friend WithEvents chkExcluirAnulado As ocxCHK
    Friend WithEvents chkIncluirPeso As ocxCHK
    Friend WithEvents chkResumenProducto As ocxCHK
    Friend WithEvents cbxOrdenadoPor As ComboBox
End Class

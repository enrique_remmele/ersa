﻿Imports ERP.Reporte
Public Class frmExistenciaMovimientoCalculado
    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String
    Dim TipoInforme As String = ""

    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True

        CargarInformacion()
        chkSinExistencia.Valor = True
        chkSinMovimientos.Valor = True

        cbxOrdenadoPor.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.DropDownStyle = ComboBoxStyle.DropDownList

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        'Habilitamos la propiedad de seleccion multiple a los cbx que queremos
        cbxDeposito.SeleccionMultiple = True
        cbxSucursal.SeleccionMultiple = True
        cbxProveedor.SeleccionMultiple = True
        cbxTipoProducto.SeleccionMultiple = True



        chkActivo.chk.Checked = True
        txtProducto.SeleccionMultiple = True
        txtProducto.Conectar()
        'txtProducto.Conectar("Select 'Sel'=Convert(bit, 'False'),  * from VProducto")
        'txtProducto.SeleccionMultiple = True
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereResumenProducto As String = ""
        Dim Condicion As String = ""
        Dim OrderBy As String = ""
        Dim Having As String = " ) a Where 1 = 1"
        'Prueba realizada para Existencia
        'Dim Having As String = ""
        Titulo = ""
        TipoInforme = ""

        'Mostrar productos sin existencia
        If chkSinExistencia.Valor = False Then
            Having = Having & " And a.ExistenciaAnterior+a.Compras+a.Entradas-a.Salidas-a.Ventas <> 0 "
            'Having = Having & " and (ISNULL(SUM(VEMC.Compras) ,0) + ISNULL(SUM(VEMC.Entradas) ,0) - ISNULL(SUM(VEMC.Salidas) ,0) - ISNULL(SUM(VEMC.Ventas) ,0) ) <>0 "
        End If

        'mostrar productos sin movimientos
        If chkSinMovimientos.Valor = False Then
            Having = Having & " and Compras + Entradas + Salidas + Ventas <> 0 "
            'Having = Having & " and (ISNULL(SUM(VEMC.Compras) ,0) + ISNULL(SUM(VEMC.Entradas) ,0) + ISNULL(SUM(VEMC.Salidas) ,0) + ISNULL(SUM(VEMC.Ventas) ,0) ) <>0 "
        End If

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Titulo = "RESUMEN DE EXISTENCIA"

        If chkActivo.chk.Checked = True Then
            Where = " Where E.Estado=1"
            Titulo = Titulo & "  DEL  " & txtDesde.GetValueString & "  AL  " & txtHasta.GetValueString
        Else
            Titulo = Titulo & "  DEL  " & txtDesde.GetValueString & " AL " & txtHasta.GetValueString
        End If


        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

            If ctr.Name = "cbxSucursal" Then
                ctr.EstablecerCondicion(WhereResumenProducto)
            End If
            If ctr.Name = "cbxDeposito" Then
                ctr.EstablecerCondicion(WhereResumenProducto)
            End If

siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.Text
            If cbxEnForma.cbx.Text <> "" Then
                OrderBy = OrderBy & " " & cbxEnForma.Text
            End If

        End If


        'Ranking

        If chkProducto.Valor = True Then
            If Not txtProducto.dtProductosSeleccionados Is Nothing Then

                If txtProducto.dtProductosSeleccionados.Rows.Count > 0 Then

                    Dim IDProductoIN As String = " E.IDProducto In (0"


                    For Each dRow As DataRow In txtProducto.dtProductosSeleccionados.Rows
                        IDProductoIN = IDProductoIN & ", " & dRow("ID")
                    Next
                    IDProductoIN = IDProductoIN & ")"

                    Where = IIf(String.IsNullOrEmpty(Where), " where " & IDProductoIN, Where & "And " & IDProductoIN)

                    GoTo Seguir

                End If

            End If

            If txtProducto.Registro IsNot Nothing And txtProducto.Enabled Then
                Where = IIf(String.IsNullOrEmpty(Where), " where (E.IDProducto) = " + txtProducto.Registro("Id").ToString, Where + " and (E.IDProducto) = " + txtProducto.Registro("Id").ToString)
            End If

        End If
Seguir:
        If chkExcluirAnulado.chk.Checked Then
            Where = IIf(String.IsNullOrEmpty(Where), " Where VEMC.Anulado = 'False' ", Where + " and VEMC.Anulado = 'False' ")
            'WhereResumenProducto = WhereResumenProducto + " and Anulado = 0 "
        End If

        If WhereResumenProducto.Length > 1 Then
            WhereResumenProducto = WhereResumenProducto.Replace("Where", "And")
            WhereResumenProducto = WhereResumenProducto.Replace("E.", "")
        End If


        OrderBy = Having & OrderBy

        TipoInforme = ""
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro)
        CReporte.ExistenciaMovimientoCalculado(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, chkIncluirPeso.chk.Checked, txtDesde.GetValue, txtHasta.GetValue, chkResumenProducto.chk.Checked, WhereResumenProducto)
        'CReporte.ExistenciaMovimientoCalculado1(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, chkIncluirPeso.chk.Checked, txtDesde.GetValue, txtHasta.GetValue, chkResumenProducto.chk.Checked, WhereResumenProducto)


    End Sub

    Sub EstablecerSubTitulo()

        Dim Filtro As String = ""
        If chkTipoProducto.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por:" & cbxTipoProducto.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxTipoProducto.cbx.Text
            End If
        End If

        If chkLinea.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por:" & cbxLinea.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxLinea.cbx.Text
            End If
        End If

        If chkMarca.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por:" & cbxMarca.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxMarca.cbx.Text
            End If
        End If

        If chkSucursal.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por:" & cbxSucursal.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxSucursal.cbx.Text
            End If
        End If

        If chkDeposito.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por:" & cbxDeposito.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxDeposito.cbx.Text()
            End If
        End If


        If Filtro <> "" Then
            TipoInforme = TipoInforme & " " & Filtro
        End If


    End Sub

    Private Sub frmExistenciaMovimientoCalculado_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmExistenciaValorizada_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        If bloquearEstadoOperacion(VGCadenaConexion, "existeCalc") = "OK" Then
            Listar()
        Else
            Dim result As DialogResult = MessageBox.Show("Los procesos y tablas necesarias para este informe estan ocupados con un pedido anterior. " & vbCrLf & "Si continua su pedido debera esperar que los procesos anteriores terminen. " & vbCrLf & vbCrLf & "Esto puede causar errores de conexion a base de datos o tiempo de espera agotado. " & vbCrLf & vbCrLf & "¿Deseas continuar?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If result = DialogResult.Yes Then
                Listar()
            End If
        End If

    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        cbxDeposito.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkLinea_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    Private Sub chkMarca_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkCategoria_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCategoria.PropertyChanged
        cbxCategoria.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged1(sender As Object, e As System.EventArgs, value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
    End Sub

    Private Sub txtProducto_ItemSeleccionado(sender As System.Object, e As System.EventArgs) Handles txtProducto.ItemSeleccionado

    End Sub

    Private Sub chkExcluirAnulado_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkExcluirAnulado.PropertyChanged
        If value = True Then
            chkResumenProducto.chk.Checked = True
            chkResumenProducto.chk.Enabled = False
        Else
            chkResumenProducto.chk.Enabled = True
        End If
    End Sub
End Class
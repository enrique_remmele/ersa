﻿Imports ERP.Reporte
Public Class frmListadoOrdenDePedido
    'CLASES
    Dim CReporte As New CReporteOrdenDePedido
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "Listado Orden de Pedido"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        txtProducto.SeleccionMultiple = True
        txtProducto.Conectar()

        CargarInformacion()

        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
        txtDesdeEntrega.PrimerDiaMes()
        txtHastaEntrega.Hoy()

        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        cbxOrdenadoPor.cbx.Items.Clear()
        cbxOrdenadoPor.cbx.Items.Add("Fecha")
        cbxOrdenadoPor.cbx.Items.Add("Producto")
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        
        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Ventas
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        'Tipo de Venta
        cbxTipoInforme.cbx.SelectedIndex = 0

        'Seleccion Multiple
        cbxDeposito.SeleccionMultiple = True
        cbxSucursal.SeleccionMultiple = True
        



    End Sub

    Sub Listar()

        'If txtProducto.Seleccionado = False Then
        '    MessageBox.Show("Selecione un Producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        '    Exit Sub
        'End If

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim SubTitulo As String = ""
        Dim Producto As String = ""
        Dim Existencia As Decimal = 0
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim OrdenadoFechaOperacion As Boolean = False

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        Where = " Where cast(Fecha as date) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
        WhereDetalle = " Where cast(Fecha as date) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"

        'Where = " Where cast(FechaFacturar as date) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
        'WhereDetalle = " Where cast(FechaFacturar as date) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"



        'Producto
        'Producto = txtProducto.Registro("Descripcion").ToString & "  (" & txtProducto.Registro("Ref").ToString & ")"

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls



            If ctr.GetType.Name.ToUpper = "OCXCBX" Then

                'Menos los controles que no quiero, Ejemplo Producto
                If ctr.name = "cbxTipoProducto" Then
                    ctr.EstablecerCondicion(WhereDetalle)
                    GoTo siguiente
                End If

                ctr.EstablecerCondicion(Where)
                ctr.EstablecerCondicion(WhereDetalle)
            End If
siguiente:

        Next

        If Not txtProducto.dtProductosSeleccionados Is Nothing Then

            If txtProducto.dtProductosSeleccionados.Rows.Count > 0 Then

                Dim IDProductoIN As String = " IDProducto In (0"


                For Each dRow As DataRow In txtProducto.dtProductosSeleccionados.Rows
                    IDProductoIN = IDProductoIN & ", " & dRow("ID")
                Next
                IDProductoIN = IDProductoIN & ")"

                WhereDetalle = IIf(String.IsNullOrEmpty(Where), " where " & IDProductoIN, WhereDetalle & "And " & IDProductoIN)

                GoTo Seguir

            End If

        End If

        If txtProducto.Seleccionado = True And txtProducto.txt.Texto <> "" Then
            WhereDetalle = WhereDetalle & " and IDProducto = " & txtProducto.Registro("ID").ToString
        End If
        If chkFechaEntrega.Valor = True Then
            WhereDetalle = WhereDetalle & " and Cast(FechaEntrega as date) Between '" & txtDesdeEntrega.GetValueString & "' And '" & txtHastaEntrega.GetValueString & "'"
        End If
        If chkNoVeMolino.Valor = True Then
            Where = Where & " and IDSucursal <> 4" ' no ver los pedidos del molino
        End If

Seguir:

        If cbxOrdenadoPor.txt.Text <> "" Then
            If cbxOrdenadoPor.cbx.SelectedIndex = 0 Then
                OrderBy = " Order by " & cbxOrdenadoPor.Texto
            End If
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        ArmarSubTitulo(SubTitulo)


        Select Case cbxTipoInforme.cbx.SelectedIndex

            Case 0
                TipoInforme = cbxTipoInforme.cbx.Text
                CReporte.ListadoOrdenDePedido(frm, Where, WhereDetalle, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo, True)
            Case 1
                TipoInforme = cbxTipoInforme.cbx.Text
                CReporte.ListadoOrdenDePedido(frm, Where, WhereDetalle, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo, False)
            Case 2
                TipoInforme = cbxTipoInforme.cbx.Text
                CReporte.ListadoDePedidoPorProducto(frm, Where, WhereDetalle, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo)

        End Select

    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)
        SubTitulo = "Del " & txtDesde.GetValue.ToShortDateString & " Al " & txtHasta.GetValue.ToShortDateString
        For Each ctr As Object In gbxFiltro.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubtitulo(SubTitulo)
            End If
        Next
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmMovimientoProducto_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            txtDesde.Focus()
        End If
    End Sub

    Private Sub frmListadoOrdenDePedido_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkFechaEntrega_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaEntrega.PropertyChanged
        txtDesdeEntrega.Enabled = value
        txtHastaEntrega.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub
End Class
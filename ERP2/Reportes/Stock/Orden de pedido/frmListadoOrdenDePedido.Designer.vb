﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoOrdenDePedido
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkFechaEntrega = New ERP.ocxCHK()
        Me.txtHastaEntrega = New ERP.ocxTXTDate()
        Me.txtDesdeEntrega = New ERP.ocxTXTDate()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkNoVeMolino = New ERP.ocxCHK()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(11, 25)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(75, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 2
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(111, 25)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(294, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaEntrega)
        Me.GroupBox2.Controls.Add(Me.txtHastaEntrega)
        Me.GroupBox2.Controls.Add(Me.txtDesdeEntrega)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.cbxTipoInforme)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Location = New System.Drawing.Point(437, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(268, 253)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkFechaEntrega
        '
        Me.chkFechaEntrega.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaEntrega.Color = System.Drawing.Color.Empty
        Me.chkFechaEntrega.Location = New System.Drawing.Point(9, 62)
        Me.chkFechaEntrega.Name = "chkFechaEntrega"
        Me.chkFechaEntrega.Size = New System.Drawing.Size(158, 21)
        Me.chkFechaEntrega.SoloLectura = False
        Me.chkFechaEntrega.TabIndex = 8
        Me.chkFechaEntrega.Texto = "Fecha de Entrega"
        Me.chkFechaEntrega.Valor = False
        '
        'txtHastaEntrega
        '
        Me.txtHastaEntrega.AñoFecha = 0
        Me.txtHastaEntrega.Color = System.Drawing.Color.Empty
        Me.txtHastaEntrega.Enabled = False
        Me.txtHastaEntrega.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtHastaEntrega.Location = New System.Drawing.Point(93, 85)
        Me.txtHastaEntrega.MesFecha = 0
        Me.txtHastaEntrega.Name = "txtHastaEntrega"
        Me.txtHastaEntrega.PermitirNulo = False
        Me.txtHastaEntrega.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaEntrega.SoloLectura = False
        Me.txtHastaEntrega.TabIndex = 11
        '
        'txtDesdeEntrega
        '
        Me.txtDesdeEntrega.AñoFecha = 0
        Me.txtDesdeEntrega.Color = System.Drawing.Color.Empty
        Me.txtDesdeEntrega.Enabled = False
        Me.txtDesdeEntrega.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtDesdeEntrega.Location = New System.Drawing.Point(9, 85)
        Me.txtDesdeEntrega.MesFecha = 0
        Me.txtDesdeEntrega.Name = "txtDesdeEntrega"
        Me.txtDesdeEntrega.PermitirNulo = False
        Me.txtDesdeEntrega.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeEntrega.SoloLectura = False
        Me.txtDesdeEntrega.TabIndex = 10
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(89, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo de Carga"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtHasta.Location = New System.Drawing.Point(93, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtDesde.Location = New System.Drawing.Point(9, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.dtSeleccionado = Nothing
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(9, 129)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionMultiple = False
        Me.cbxTipoInforme.SeleccionObligatoria = False
        Me.cbxTipoInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 4
        Me.cbxTipoInforme.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 113)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Tipo Informe:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(171, 169)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(66, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 7
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(8, 169)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 6
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 153)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 5
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(9, 209)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 9
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 193)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 8
        Me.lblRanking.Text = "Ranking:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(605, 292)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(111, 52)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(294, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 5
        Me.cbxDeposito.Texto = "CENTRAL"
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(11, 52)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(75, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 4
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkNoVeMolino)
        Me.gbxFiltro.Controls.Add(Me.Label3)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(420, 253)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkNoVeMolino
        '
        Me.chkNoVeMolino.BackColor = System.Drawing.Color.Transparent
        Me.chkNoVeMolino.Color = System.Drawing.Color.Empty
        Me.chkNoVeMolino.Location = New System.Drawing.Point(111, 190)
        Me.chkNoVeMolino.Name = "chkNoVeMolino"
        Me.chkNoVeMolino.Size = New System.Drawing.Size(153, 21)
        Me.chkNoVeMolino.SoloLectura = False
        Me.chkNoVeMolino.TabIndex = 12
        Me.chkNoVeMolino.Texto = "No ver Pedido de Molino"
        Me.chkNoVeMolino.Valor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Producto:"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(111, 136)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(296, 47)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 7
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(499, 292)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(100, 23)
        Me.btnInforme.TabIndex = 6
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(11, 79)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(95, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 13
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(111, 79)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(294, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 14
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'frmListadoOrdenDePedido
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(722, 327)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoOrdenDePedido"
        Me.Text = "frmListadoOrdenDePedido"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents cbxTipoInforme As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkFechaEntrega As ERP.ocxCHK
    Friend WithEvents txtHastaEntrega As ERP.ocxTXTDate
    Friend WithEvents txtDesdeEntrega As ERP.ocxTXTDate
    Friend WithEvents chkNoVeMolino As ERP.ocxCHK
    Friend WithEvents chkTipoProducto As ocxCHK
    Friend WithEvents cbxTipoProducto As ocxCBX
End Class

﻿Imports ERP.Reporte

Public Class frmListadoRetencionRecibida

    'CLASES
    Dim CReporte As New CReporteCobranza
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = ""
    Dim Subtitulo As String = ""
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtCliente.Conectar()
        chkFechaEmision.chk.Checked = True
        'Funciones
        CargarInformacion()
        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()
        txtCobranzaDesde.PrimerDiaAño()
        txtCobranzaHasta.Hoy()
        chkImprimirCabecera.Valor = True
    End Sub

    Sub CargarInformacion()

        'Orden
        cbxOrdenadoPor.cbx.Items.Add("FechaRetencion")
        cbxOrdenadoPor.cbx.Items.Add("Cliente")
        cbxOrdenadoPor.cbx.Items.Add("Venta")
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.SelectedIndex = 0

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkFechaEmision.chk.Checked = True And chkFechaCobranza.chk.Checked = False Then
            Where = " Where RR.FechaRetencion Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        ElseIf chkFechaCobranza.chk.Checked = True And chkFechaEmision.chk.Checked = False Then
            Where = " Where CC.FechaEmision Between '" & txtCobranzaDesde.GetValueString & "' And '" & txtCobranzaHasta.GetValueString & "' "
        ElseIf chkFechaCobranza.chk.Checked = True And chkFechaEmision.chk.Checked = True Then
            Where = " Where RR.FechaRetencion Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' and CC.FechaEmision Between '" & txtCobranzaDesde.GetValueString & "' And '" & txtCobranzaHasta.GetValueString & "' "
        Else
            Where = ""
        End If

        If chkCliente.chk.Checked = True Then
            If txtCliente.Seleccionado = False Then
                MessageBox.Show("Seleccione correctamente un cliente!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            Where = Where & " And RR.IDCliente = " & txtCliente.Registro("ID") & "  "
        End If

        If chkSucursal.chk.Checked = True Then
            Where = Where & " And RR.IDSucursal = " & cbxSucursal.cbx.SelectedValue & " "
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If


        'Titulo
        Titulo = "LISTADO DE RETENCIONES RECIBIDAS"
        Subtitulo = "Fecha desde: " & txtDesde.txt.Text & " hasta " & txtHasta.txt.Text & ""
        CReporte.ListadoRetencionRecibida(frm, Titulo, Subtitulo, Where, OrderBy, chkImprimirCabecera.Valor)

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoCobranzaCredito_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
   
End Class
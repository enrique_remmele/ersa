﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoRetencionRecibida
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.txtCobranzaHasta = New ERP.ocxTXTDate()
        Me.txtCobranzaDesde = New ERP.ocxTXTDate()
        Me.chkImprimirCabecera = New ERP.ocxCHK()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkFechaEmision = New ERP.ocxCHK()
        Me.chkFechaCobranza = New ERP.ocxCHK()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(870, 207)
        Me.btnCerrar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(84, 28)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaCobranza)
        Me.GroupBox2.Controls.Add(Me.chkFechaEmision)
        Me.GroupBox2.Controls.Add(Me.txtCobranzaHasta)
        Me.GroupBox2.Controls.Add(Me.txtCobranzaDesde)
        Me.GroupBox2.Controls.Add(Me.chkImprimirCabecera)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(585, 15)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(374, 184)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(9, 103)
        Me.lblOrdenado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(76, 17)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(636, 207)
        Me.btnInforme.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(167, 28)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(16, 15)
        Me.gbxFiltro.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxFiltro.Size = New System.Drawing.Size(561, 184)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'txtCobranzaHasta
        '
        Me.txtCobranzaHasta.Color = System.Drawing.Color.Empty
        Me.txtCobranzaHasta.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtCobranzaHasta.Location = New System.Drawing.Point(270, 59)
        Me.txtCobranzaHasta.Margin = New System.Windows.Forms.Padding(5)
        Me.txtCobranzaHasta.Name = "txtCobranzaHasta"
        Me.txtCobranzaHasta.PermitirNulo = False
        Me.txtCobranzaHasta.Size = New System.Drawing.Size(99, 25)
        Me.txtCobranzaHasta.SoloLectura = False
        Me.txtCobranzaHasta.TabIndex = 13
        '
        'txtCobranzaDesde
        '
        Me.txtCobranzaDesde.Color = System.Drawing.Color.Empty
        Me.txtCobranzaDesde.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtCobranzaDesde.Location = New System.Drawing.Point(163, 59)
        Me.txtCobranzaDesde.Margin = New System.Windows.Forms.Padding(5)
        Me.txtCobranzaDesde.Name = "txtCobranzaDesde"
        Me.txtCobranzaDesde.PermitirNulo = False
        Me.txtCobranzaDesde.Size = New System.Drawing.Size(99, 25)
        Me.txtCobranzaDesde.SoloLectura = False
        Me.txtCobranzaDesde.TabIndex = 12
        '
        'chkImprimirCabecera
        '
        Me.chkImprimirCabecera.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimirCabecera.Color = System.Drawing.Color.Empty
        Me.chkImprimirCabecera.Location = New System.Drawing.Point(11, 154)
        Me.chkImprimirCabecera.Margin = New System.Windows.Forms.Padding(5)
        Me.chkImprimirCabecera.Name = "chkImprimirCabecera"
        Me.chkImprimirCabecera.Size = New System.Drawing.Size(307, 26)
        Me.chkImprimirCabecera.SoloLectura = False
        Me.chkImprimirCabecera.TabIndex = 10
        Me.chkImprimirCabecera.Texto = "Imprimir Cabecera"
        Me.chkImprimirCabecera.Valor = False
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(220, 124)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(5)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(97, 26)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(9, 124)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(5)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(209, 26)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtHasta.Location = New System.Drawing.Point(270, 24)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(5)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(99, 25)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtDesde.Location = New System.Drawing.Point(163, 24)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(5)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(99, 25)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 56
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(15, 118)
        Me.txtCliente.Margin = New System.Windows.Forms.Padding(5)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(540, 30)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 3
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(15, 97)
        Me.chkCliente.Margin = New System.Windows.Forms.Padding(5)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(91, 26)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 2
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(15, 49)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(5)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(91, 20)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(113, 47)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(5)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(284, 26)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'chkFechaEmision
        '
        Me.chkFechaEmision.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaEmision.Color = System.Drawing.Color.Empty
        Me.chkFechaEmision.Location = New System.Drawing.Point(5, 23)
        Me.chkFechaEmision.Margin = New System.Windows.Forms.Padding(5)
        Me.chkFechaEmision.Name = "chkFechaEmision"
        Me.chkFechaEmision.Size = New System.Drawing.Size(151, 26)
        Me.chkFechaEmision.SoloLectura = False
        Me.chkFechaEmision.TabIndex = 18
        Me.chkFechaEmision.Texto = "Fecha de Emisión:"
        Me.chkFechaEmision.Valor = False
        '
        'chkFechaCobranza
        '
        Me.chkFechaCobranza.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaCobranza.Color = System.Drawing.Color.Empty
        Me.chkFechaCobranza.Location = New System.Drawing.Point(5, 59)
        Me.chkFechaCobranza.Margin = New System.Windows.Forms.Padding(5)
        Me.chkFechaCobranza.Name = "chkFechaCobranza"
        Me.chkFechaCobranza.Size = New System.Drawing.Size(151, 26)
        Me.chkFechaCobranza.SoloLectura = False
        Me.chkFechaCobranza.TabIndex = 19
        Me.chkFechaCobranza.Texto = "Fecha de Cobranza:"
        Me.chkFechaCobranza.Valor = False
        '
        'frmListadoRetencionRecibida
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(972, 258)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmListadoRetencionRecibida"
        Me.Text = "frmListadoRetencionRecibida"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkImprimirCabecera As ERP.ocxCHK
    Friend WithEvents txtCobranzaHasta As ERP.ocxTXTDate
    Friend WithEvents txtCobranzaDesde As ERP.ocxTXTDate
    Friend WithEvents chkFechaCobranza As ERP.ocxCHK
    Friend WithEvents chkFechaEmision As ERP.ocxCHK
End Class

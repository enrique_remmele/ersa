﻿Imports ERP.Reporte
Imports System.Data.SqlClient

Public Class frmListadoCobranzaCredito

    'CLASES
    Dim CReporte As New CReporteCobranza
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = ""
    Dim Subtitulo As String = ""
    Dim TipoInforme As String
    Dim DescripcionTipoComprobante As String = ""
    Dim vdtCuentaBancaria As DataTable
    Dim Recibo As String = ""
    Dim Cobrador As String = ""
    Dim Cliente As String = ""
    Dim Sucursal As String = ""

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()

        CambiarTipoComprobante()

        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
        txtDesdeAplicacion.PrimerDiaAño()
        txtHastaAplicacion.Hoy()
    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = " where AnticipoCliente = 'True'"
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where FechaEmision Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        WhereDetalle = WhereDetalle & " and cast(Fecha as date) Between '" & CSistema.FormatoFechaBaseDatos(txtDesdeAplicacion.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHastaAplicacion.txt.Text, True, False) & "'"

        If chkRecibo.chk.Checked = True Then
            Where = Where & " And Comprobante Between '" & txtReciboDesde.Text & "' And  '" & txtReciboHasta.Text & "'"
            WhereDetalle = WhereDetalle & " And Comprobante Between '" & txtReciboDesde.Text & "' And  '" & txtReciboHasta.Text & "'"
        End If

        If chkCliente.chk.Checked = True Then
            Where = Where & " And IDCliente = '" & cbxCliente.cbx.SelectedValue & "'  "
            WhereDetalle = WhereDetalle & " And IDCliente = '" & cbxCliente.cbx.SelectedValue & "'  "
        End If

        If chkCobrador.chk.Checked = True Then
            Where = Where & " And IDCobrador = '" & cbxCobrador.cbx.SelectedValue & "' "
            WhereDetalle = WhereDetalle & " And IDCobrador = '" & cbxCobrador.cbx.SelectedValue & "' "
        End If

        If chkSucursal.chk.Checked = True Then
            Where = Where & " And IDSucursal = " & cbxSucursal.cbx.SelectedValue & " "
            WhereDetalle = WhereDetalle & " And IDSucursal = " & cbxSucursal.cbx.SelectedValue & " "
        End If

        If chkTipoComprobante.chk.Checked = True Then
            If cbxTipoComprobante.cbx.Text <> "" Then
                Where = Where & "And TipoComprobante = '" & cbxTipoComprobante.cbx.Text & "' "
                WhereDetalle = WhereDetalle & "And TipoComprobante = '" & cbxTipoComprobante.cbx.Text & "' "
            End If
        End If

siguiente:

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If


        'Titulo
        If chkTipoComprobante.chk.Checked = False Then
            Titulo = "LISTADO DE COBRANZAS POR RECIBO"
        Else
            If cbxTipoComprobante.cbx.Text = "REC" Then
                Titulo = "LISTADO DE COBRANZAS POR RECIBO "
            Else
                Titulo = "LISTADO DE COBRANZAS " & cbxTipoComprobante.cbx.Text & ""
            End If
        End If

        'Subtitulo
        If chkRecibo.chk.Checked = True Then
            If txtReciboDesde.Text = txtReciboHasta.Text Then
                Recibo = " - Recibo N°: " & txtReciboDesde.Text & ""
            Else
                Recibo = " - Recibo desde N° " & txtReciboDesde.Text & " hasta N° " & txtReciboHasta.Text & ""
            End If
        End If

        If chkCobrador.chk.Checked = True Then
            Cobrador = " - Cobrador: " & cbxCobrador.cbx.Text & ""
        End If

        If chkCliente.chk.Checked = True Then
            Cliente = " - Cliente: " & cbxCliente.cbx.Text & ""
        End If

        If chkSucursal.chk.Checked = True Then
            Sucursal = " - Sucursal: " & cbxSucursal.cbx.Text & ""
        End If

        Subtitulo = "Fecha desde: " & txtDesde.txt.Text & " hasta " & txtHasta.txt.Text & "" & Recibo & "" & Cliente & "" & Cobrador & "" & Sucursal & ""

        If chkVerAnulados.Valor = False Then
            Where = Where & " and Anulado = 'False'"
        Else
            If chkSoloAnulados.Valor Then
                Where = Where & " and Anulado = 'True'"
                Subtitulo = Subtitulo & " ANULADOS"
            End If
        End If

        If chkAnticipoCliente.Valor = False Then
            CReporte.ListadoCobranzaCredito(frm, Titulo, Subtitulo, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)
        Else
            CReporte.ListadoAnticipoCliente(frm, Titulo, Subtitulo, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, chkAnticipoConSaldo.Valor)
        End If
    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VCobranzaCredito ", "Select Top(0) Comprobante, Cliente, FechaEmision From VCobranzaCredito ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next
        cbxOrdenadoPor.cbx.SelectedIndex = 0
    End Sub

    Sub CambiarTipoComprobante()

        'Tipo comprobante

        Dim cmd As String = "Select Distinct TipoComprobante From VCobranzaCredito"
        Dim conn As New SqlConnection(VGCadenaConexion)
        Dim da As New SqlDataAdapter(cmd, conn)
        Dim dt As DataTable = New DataTable("TipoComprobante")
        da.Fill(dt)
        With cbxTipoComprobante.cbx
            .DataSource = dt
            .DisplayMember = "TipoComprobante"
        End With
        cbxTipoComprobante.cbx.SelectedIndex = 0

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoCobranzaCredito_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub chkCobrador_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCobrador.PropertyChanged
        cbxCobrador.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkRecibo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkRecibo.PropertyChanged
        txtReciboDesde.Enabled = value
        txtReciboHasta.Enabled = value
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkVerAnulados_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkVerAnulados.PropertyChanged
        chkSoloAnulados.Enabled = value
    End Sub

    Private Sub chkAnticipoCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkAnticipoCliente.PropertyChanged
        chkAnticipoConSaldo.Enabled = value
        If chkAnticipoCliente.chk.Checked = True Then
            cbxOrdenadoPor.Enabled = False
            cbxEnForma.Enabled = False
        Else
            cbxOrdenadoPor.Enabled = True
            cbxEnForma.Enabled = True
        End If
    End Sub
End Class




﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoCobranzaCredito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkCobrador = New ERP.ocxCHK()
        Me.cbxCobrador = New ERP.ocxCBX()
        Me.chkRecibo = New ERP.ocxCHK()
        Me.txtReciboHasta = New System.Windows.Forms.TextBox()
        Me.txtReciboDesde = New System.Windows.Forms.TextBox()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxCliente = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkSoloAnulados = New ERP.ocxCHK()
        Me.chkAnticipoConSaldo = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHastaAplicacion = New ERP.ocxTXTDate()
        Me.txtDesdeAplicacion = New ERP.ocxTXTDate()
        Me.chkAnticipoCliente = New ERP.ocxCHK()
        Me.chkVerAnulados = New ERP.ocxCHK()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkCobrador)
        Me.gbxFiltro.Controls.Add(Me.cbxCobrador)
        Me.gbxFiltro.Controls.Add(Me.chkRecibo)
        Me.gbxFiltro.Controls.Add(Me.txtReciboHasta)
        Me.gbxFiltro.Controls.Add(Me.txtReciboDesde)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(8, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(311, 253)
        Me.gbxFiltro.TabIndex = 3
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkCobrador
        '
        Me.chkCobrador.BackColor = System.Drawing.Color.Transparent
        Me.chkCobrador.Color = System.Drawing.Color.Empty
        Me.chkCobrador.Location = New System.Drawing.Point(13, 88)
        Me.chkCobrador.Name = "chkCobrador"
        Me.chkCobrador.Size = New System.Drawing.Size(68, 21)
        Me.chkCobrador.SoloLectura = False
        Me.chkCobrador.TabIndex = 3
        Me.chkCobrador.Texto = "Cobrador:"
        Me.chkCobrador.Valor = False
        '
        'cbxCobrador
        '
        Me.cbxCobrador.CampoWhere = "IDCobrador"
        Me.cbxCobrador.CargarUnaSolaVez = False
        Me.cbxCobrador.DataDisplayMember = "Nombres"
        Me.cbxCobrador.DataFilter = Nothing
        Me.cbxCobrador.DataOrderBy = ""
        Me.cbxCobrador.DataSource = "Cobrador"
        Me.cbxCobrador.DataValueMember = "ID"
        Me.cbxCobrador.dtSeleccionado = Nothing
        Me.cbxCobrador.Enabled = False
        Me.cbxCobrador.FormABM = Nothing
        Me.cbxCobrador.Indicaciones = Nothing
        Me.cbxCobrador.Location = New System.Drawing.Point(85, 87)
        Me.cbxCobrador.Name = "cbxCobrador"
        Me.cbxCobrador.SeleccionMultiple = False
        Me.cbxCobrador.SeleccionObligatoria = True
        Me.cbxCobrador.Size = New System.Drawing.Size(213, 21)
        Me.cbxCobrador.SoloLectura = False
        Me.cbxCobrador.TabIndex = 4
        Me.cbxCobrador.Texto = ""
        '
        'chkRecibo
        '
        Me.chkRecibo.BackColor = System.Drawing.Color.Transparent
        Me.chkRecibo.Color = System.Drawing.Color.Empty
        Me.chkRecibo.Location = New System.Drawing.Point(13, 46)
        Me.chkRecibo.Name = "chkRecibo"
        Me.chkRecibo.Size = New System.Drawing.Size(68, 21)
        Me.chkRecibo.SoloLectura = False
        Me.chkRecibo.TabIndex = 0
        Me.chkRecibo.Texto = "N° Recibo:"
        Me.chkRecibo.Valor = False
        '
        'txtReciboHasta
        '
        Me.txtReciboHasta.Enabled = False
        Me.txtReciboHasta.Location = New System.Drawing.Point(198, 47)
        Me.txtReciboHasta.Name = "txtReciboHasta"
        Me.txtReciboHasta.Size = New System.Drawing.Size(100, 20)
        Me.txtReciboHasta.TabIndex = 2
        '
        'txtReciboDesde
        '
        Me.txtReciboDesde.Enabled = False
        Me.txtReciboDesde.Location = New System.Drawing.Point(85, 47)
        Me.txtReciboDesde.Name = "txtReciboDesde"
        Me.txtReciboDesde.Size = New System.Drawing.Size(100, 20)
        Me.txtReciboDesde.TabIndex = 1
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(13, 115)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(68, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 5
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxCliente
        '
        Me.cbxCliente.CampoWhere = "IDCliente"
        Me.cbxCliente.CargarUnaSolaVez = False
        Me.cbxCliente.DataDisplayMember = "RazonSocial"
        Me.cbxCliente.DataFilter = Nothing
        Me.cbxCliente.DataOrderBy = ""
        Me.cbxCliente.DataSource = "vCliente"
        Me.cbxCliente.DataValueMember = "ID"
        Me.cbxCliente.dtSeleccionado = Nothing
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormABM = Nothing
        Me.cbxCliente.Indicaciones = Nothing
        Me.cbxCliente.Location = New System.Drawing.Point(85, 114)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.SeleccionMultiple = False
        Me.cbxCliente.SeleccionObligatoria = True
        Me.cbxCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxCliente.SoloLectura = False
        Me.cbxCliente.TabIndex = 6
        Me.cbxCliente.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(13, 142)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(68, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 7
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(85, 142)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 8
        Me.cbxSucursal.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(511, 262)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 2
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkSoloAnulados)
        Me.GroupBox2.Controls.Add(Me.chkAnticipoConSaldo)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtHastaAplicacion)
        Me.GroupBox2.Controls.Add(Me.txtDesdeAplicacion)
        Me.GroupBox2.Controls.Add(Me.chkAnticipoCliente)
        Me.GroupBox2.Controls.Add(Me.chkVerAnulados)
        Me.GroupBox2.Controls.Add(Me.chkTipoComprobante)
        Me.GroupBox2.Controls.Add(Me.cbxTipoComprobante)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(326, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 253)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkSoloAnulados
        '
        Me.chkSoloAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkSoloAnulados.Color = System.Drawing.Color.Empty
        Me.chkSoloAnulados.Enabled = False
        Me.chkSoloAnulados.Location = New System.Drawing.Point(125, 231)
        Me.chkSoloAnulados.Name = "chkSoloAnulados"
        Me.chkSoloAnulados.Size = New System.Drawing.Size(113, 16)
        Me.chkSoloAnulados.SoloLectura = False
        Me.chkSoloAnulados.TabIndex = 14
        Me.chkSoloAnulados.Texto = "Solo Anulados"
        Me.chkSoloAnulados.Valor = False
        '
        'chkAnticipoConSaldo
        '
        Me.chkAnticipoConSaldo.BackColor = System.Drawing.Color.Transparent
        Me.chkAnticipoConSaldo.Color = System.Drawing.Color.Empty
        Me.chkAnticipoConSaldo.Enabled = False
        Me.chkAnticipoConSaldo.Location = New System.Drawing.Point(125, 211)
        Me.chkAnticipoConSaldo.Name = "chkAnticipoConSaldo"
        Me.chkAnticipoConSaldo.Size = New System.Drawing.Size(113, 16)
        Me.chkAnticipoConSaldo.SoloLectura = False
        Me.chkAnticipoConSaldo.TabIndex = 12
        Me.chkAnticipoConSaldo.Texto = "Solo con Saldo"
        Me.chkAnticipoConSaldo.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Rango de Aplicacion"
        '
        'txtHastaAplicacion
        '
        Me.txtHastaAplicacion.AñoFecha = 0
        Me.txtHastaAplicacion.Color = System.Drawing.Color.Empty
        Me.txtHastaAplicacion.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtHastaAplicacion.Location = New System.Drawing.Point(90, 88)
        Me.txtHastaAplicacion.MesFecha = 0
        Me.txtHastaAplicacion.Name = "txtHastaAplicacion"
        Me.txtHastaAplicacion.PermitirNulo = False
        Me.txtHastaAplicacion.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaAplicacion.SoloLectura = False
        Me.txtHastaAplicacion.TabIndex = 5
        '
        'txtDesdeAplicacion
        '
        Me.txtDesdeAplicacion.AñoFecha = 0
        Me.txtDesdeAplicacion.Color = System.Drawing.Color.Empty
        Me.txtDesdeAplicacion.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtDesdeAplicacion.Location = New System.Drawing.Point(7, 88)
        Me.txtDesdeAplicacion.MesFecha = 0
        Me.txtDesdeAplicacion.Name = "txtDesdeAplicacion"
        Me.txtDesdeAplicacion.PermitirNulo = False
        Me.txtDesdeAplicacion.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeAplicacion.SoloLectura = False
        Me.txtDesdeAplicacion.TabIndex = 4
        '
        'chkAnticipoCliente
        '
        Me.chkAnticipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkAnticipoCliente.Color = System.Drawing.Color.Empty
        Me.chkAnticipoCliente.Location = New System.Drawing.Point(10, 211)
        Me.chkAnticipoCliente.Name = "chkAnticipoCliente"
        Me.chkAnticipoCliente.Size = New System.Drawing.Size(113, 16)
        Me.chkAnticipoCliente.SoloLectura = False
        Me.chkAnticipoCliente.TabIndex = 11
        Me.chkAnticipoCliente.Texto = "Anticipo de Clientes"
        Me.chkAnticipoCliente.Valor = False
        '
        'chkVerAnulados
        '
        Me.chkVerAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkVerAnulados.Color = System.Drawing.Color.Empty
        Me.chkVerAnulados.Location = New System.Drawing.Point(10, 231)
        Me.chkVerAnulados.Name = "chkVerAnulados"
        Me.chkVerAnulados.Size = New System.Drawing.Size(113, 16)
        Me.chkVerAnulados.SoloLectura = False
        Me.chkVerAnulados.TabIndex = 13
        Me.chkVerAnulados.Texto = "Ver Anulados"
        Me.chkVerAnulados.Valor = False
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(7, 112)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(113, 16)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 6
        Me.chkTipoComprobante.Texto = "Tipo Comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = ""
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = ""
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = ""
        Me.cbxTipoComprobante.DataValueMember = ""
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(7, 130)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = True
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 7
        Me.cbxTipoComprobante.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(165, 175)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 10
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(7, 175)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 9
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(7, 158)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 8
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(7, 20)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtHasta.Location = New System.Drawing.Point(90, 39)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtDesde.Location = New System.Drawing.Point(7, 39)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(326, 262)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmListadoCobranzaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(578, 291)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoCobranzaCredito"
        Me.Text = "Listado Cobranza Crédito"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxCliente As ERP.ocxCBX
    Friend WithEvents chkRecibo As ERP.ocxCHK
    Friend WithEvents txtReciboHasta As System.Windows.Forms.TextBox
    Friend WithEvents txtReciboDesde As System.Windows.Forms.TextBox
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkCobrador As ERP.ocxCHK
    Friend WithEvents cbxCobrador As ERP.ocxCBX
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents chkVerAnulados As ERP.ocxCHK
    Friend WithEvents chkAnticipoCliente As ERP.ocxCHK
    Friend WithEvents txtHastaAplicacion As ERP.ocxTXTDate
    Friend WithEvents txtDesdeAplicacion As ERP.ocxTXTDate
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkSoloAnulados As ocxCHK
    Friend WithEvents chkAnticipoConSaldo As ocxCHK
End Class

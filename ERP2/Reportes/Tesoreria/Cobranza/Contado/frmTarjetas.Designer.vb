﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTarjetas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucusal = New ERP.ocxCBX()
        Me.chkTipo = New ERP.ocxCHK()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkNoDepositados = New ERP.ocxCHK()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(21, 27)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucusal)
        Me.gbxFiltro.Controls.Add(Me.chkTipo)
        Me.gbxFiltro.Controls.Add(Me.cbxTipo)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 1)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(295, 77)
        Me.gbxFiltro.TabIndex = 5
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(20, 42)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(71, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucusal
        '
        Me.cbxSucusal.CampoWhere = "IDSucursal"
        Me.cbxSucusal.CargarUnaSolaVez = False
        Me.cbxSucusal.DataDisplayMember = "Descripcion"
        Me.cbxSucusal.DataFilter = Nothing
        Me.cbxSucusal.DataOrderBy = "Descripcion"
        Me.cbxSucusal.DataSource = "VSucursal"
        Me.cbxSucusal.DataValueMember = "ID"
        Me.cbxSucusal.Enabled = False
        Me.cbxSucusal.FormABM = Nothing
        Me.cbxSucusal.Indicaciones = Nothing
        Me.cbxSucusal.Location = New System.Drawing.Point(91, 42)
        Me.cbxSucusal.Name = "cbxSucusal"
        Me.cbxSucusal.SeleccionObligatoria = False
        Me.cbxSucusal.Size = New System.Drawing.Size(173, 21)
        Me.cbxSucusal.SoloLectura = False
        Me.cbxSucusal.TabIndex = 5
        Me.cbxSucusal.Texto = ""
        '
        'chkTipo
        '
        Me.chkTipo.BackColor = System.Drawing.Color.Transparent
        Me.chkTipo.Color = System.Drawing.Color.Empty
        Me.chkTipo.Location = New System.Drawing.Point(20, 19)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(71, 21)
        Me.chkTipo.SoloLectura = False
        Me.chkTipo.TabIndex = 2
        Me.chkTipo.Texto = "Tipo:"
        Me.chkTipo.Valor = False
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = "IDTipoOperacion"
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = "Descripcion"
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = "Descripcion"
        Me.cbxTipo.DataSource = "VTipoComprobante"
        Me.cbxTipo.DataValueMember = "ID"
        Me.cbxTipo.Enabled = False
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(91, 19)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(173, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 206)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(310, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(233, 175)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 8
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(138, 175)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(89, 23)
        Me.btnInforme.TabIndex = 7
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkNoDepositados)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 81)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(293, 88)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 2, 18, 13, 3, 29, 337)
        Me.txtHasta.Location = New System.Drawing.Point(99, 49)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 18, 13, 3, 29, 337)
        Me.txtDesde.Location = New System.Drawing.Point(19, 49)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkNoDepositados
        '
        Me.chkNoDepositados.BackColor = System.Drawing.Color.Transparent
        Me.chkNoDepositados.Color = System.Drawing.Color.Empty
        Me.chkNoDepositados.Location = New System.Drawing.Point(191, 49)
        Me.chkNoDepositados.Name = "chkNoDepositados"
        Me.chkNoDepositados.Size = New System.Drawing.Size(96, 21)
        Me.chkNoDepositados.SoloLectura = False
        Me.chkNoDepositados.TabIndex = 6
        Me.chkNoDepositados.Texto = "No depositados"
        Me.chkNoDepositados.Valor = False
        '
        'frmTarjetas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(310, 228)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmTarjetas"
        Me.Text = "frmTarjetas"
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucusal As ERP.ocxCBX
    Friend WithEvents chkTipo As ERP.ocxCHK
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents chkNoDepositados As ERP.ocxCHK
End Class

﻿Imports ERP.Reporte

Public Class frmPlanillaCobranza

    'CLASES
    Dim CReporte As New CReporteCobranza
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "DETALLE DE DOCUMENTOS COBRADOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtCliente.Conectar()

        CargarInformacion()

        CambiarOrdenacion()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Condición
        cbxCondicion.cbx.Items.Add("Contado + Crédito")
        cbxCondicion.cbx.Items.Add("Contado")
        cbxCondicion.cbx.Items.Add("Crédito")

        'Informe
        cbxInforme.cbx.Items.Add("Comprobantes Cobrados")        
        cbxInforme.cbx.Items.Add("Resumen Diario")
        cbxInforme.cbx.Items.Add("Valores Ingresados")
        cbxInforme.cbx.Items.Add("Resumen Diario por Periodo")

        cbxInforme.cbx.SelectedIndex = 0
        cbxCondicion.cbx.SelectedIndex = 0

        CSistema.SqlToComboBox(cbxTipoComprobanteCobranza.cbx, "Select ID, Descripcion from TipoComprobante where IDOperacion in(19,20)")


        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim WhereAnticipo As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim ASC As Boolean
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Informe
        Select Case cbxInforme.cbx.SelectedIndex

            Case 0

                Where = "Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

                If chkCliente.chk.Checked = True Then
                    Where = Where & " And IDCliente = " & txtCliente.txtID.ObtenerValor
                End If

                For Each ctr As Object In gbxFiltro.Controls

                    If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                        ctr.EstablecerCondicion(Where)
                    End If

                Next


                'Establecemos el Orden
                If cbxOrdenadoPor.cbx.Text <> "" Then
                    OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
                End If

                If OrderBy = "" Then
                    OrderBy = " Order By Comprobante"
                Else
                    If OrderBy.Contains("Comprobante") = False Then
                        OrderBy = OrderBy & ", Comprobante"
                    End If
                End If

                If cbxEnForma.cbx.Text <> "" Then
                    OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
                End If

                'Ranking
                If nudRanking.Value > 0 Then
                    Top = "Top (" & nudRanking.Value & ")"
                End If

                'Lote
                If chkPlanillas.chk.Checked = True Then
                    Where = Where & " And Planilla Between '" & txtPlanillaDesde.ObtenerValor & "' And '" & txtPlanillaHasta.ObtenerValor & "' "
                End If

                'Lote
                If chkRecibo.Valor = True Then
                    Where = Where & " And Recibo Between '" & txtReciboDesde.ObtenerValor & "' And '" & txtReciboHasta.ObtenerValor & "'"
                End If

                Titulo = "DETALLE DE DOCUMENTOS COBRADOS"

                TipoInforme = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text & " - Por comprobante "
                If chkSucursal.Valor = True Then
                    TipoInforme = TipoInforme & "- De " & cbxSucursal.cbx.Text
                End If

                'Condición
                Select Case cbxCondicion.cbx.SelectedIndex
                    Case 1
                        Where = Where & " And Credito = 0"
                    Case 2
                        Where = Where & " And Credito = 1"
                End Select

                'Cliente
                If chkCliente.Valor = True Then
                    Where = Where & " And IDCliente = " & txtCliente.txtID.ObtenerValor
                End If

                If chkPorFecha.Valor = True Then
                    CReporte.DetalleDocumentosCobradosFecha(frm, Titulo, TipoInforme, Where, OrderBy, Top, vgUsuarioIdentificador)
                Else
                    If chkAgrupadoPorVendedor.Valor = True Then
                        CReporte.DetalleDocumentosCobradosVendedor(frm, Titulo, TipoInforme, Where, OrderBy, Top, vgUsuarioIdentificador)
                    Else
                        CReporte.DetalleDocumentosCobrados(frm, Titulo, TipoInforme, Where, OrderBy, Top, vgUsuarioIdentificador)
                    End If
                End If

            Case 1

                Where = " Where (Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "') "

                For Each ctr As Object In gbxFiltro.Controls

                    If ctr.GetType.Name.ToUpper = "OCXCBX" And ctr.name <> "cbxTipoProducto" Then
                        ctr.EstablecerCondicion(Where)
                    End If

                Next

                'Lote
                If chkPlanillas.Valor = True Then
                    Where = Where & " And Planilla Between '" & txtPlanillaDesde.txt.Text & "' And '" & txtPlanillaHasta.txt.Text & "'"
                End If

                'Lote
                If chkRecibo.Valor = True Then
                    Where = Where & " And NroComprobante Between " & txtReciboDesde.ObtenerValor & " And " & txtReciboHasta.ObtenerValor
                End If

                'Cliente
                If chkCliente.Valor = True Then
                    Where = Where & " And IDCliente = " & txtCliente.txtID.ObtenerValor
                End If

                If chkCobrador.Valor = True Then
                    TipoInforme = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text & " - Cobrador: " & cbxCobrador.txt.Text
                Else
                    TipoInforme = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text
                End If

                If chkSucursal.Valor = True Then
                    TipoInforme = TipoInforme & " - Suc:" & cbxSucursal.cbx.Text
                End If

                Titulo = "RESUMEN DE COBRANZA DIARIA"

                CReporte.ResumenCobranzaDiaria(frm, Titulo, TipoInforme, Where, OrderBy, Top, vgUsuarioIdentificador, WhereDetalle, ASC)


            Case 2

                Where = "Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
                WhereAnticipo = " FecCobranza Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
                If chkUsuario.Valor = True Then
                    WhereAnticipo = WhereAnticipo & " and IDUsuario =" & cbxUsuario.GetValue
                Else
                    WhereAnticipo = WhereAnticipo & " and IDUsuario is null"
                End If

                For Each ctr As Object In gbxFiltro.Controls

                    If ctr.GetType.Name.ToUpper = "OCXCBX" And ctr.name <> "cbxTipoProducto" Then
                        ctr.EstablecerCondicion(Where)
                    End If

                Next

                'Establecemos el Orden
                If cbxOrdenadoPor.cbx.Text <> "" Then
                    OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
                End If

                If cbxEnForma.cbx.Text <> "" Then
                    OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
                End If

                'Ranking
                If nudRanking.Value > 0 Then
                    Top = "Top (" & nudRanking.Value & ")"
                End If

                'Lote
                If chkPlanillas.chk.Checked = True Then
                    Where = Where & " And Planilla Between " & txtPlanillaDesde.ObtenerValor & " And " & txtPlanillaHasta.ObtenerValor & " "
                End If

                'Lote
                If chkRecibo.Valor = True Then
                    Where = Where & " And Recibo Between " & txtReciboDesde.ObtenerValor & " And " & txtReciboHasta.ObtenerValor
                End If

                'Cliente
                If chkCliente.Valor = True Then
                    Where = Where & " And IDCliente = " & txtCliente.txtID.ObtenerValor
                End If

                Titulo = "DETALLE DE VALORES COBRADOS"
                TipoInforme = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text
                If chkSucursal.Valor = True Then
                    TipoInforme = TipoInforme & "- De " & cbxSucursal.cbx.Text
                End If

                If chkVerAnticipo.Valor = True Then
                    Where = Where & " and AnticipoCliente = 'True'"
                End If

                If chkPorComprobante.Valor = True Then
                    CReporte.DetalleValoresCobradosPorComprobante(frm, Titulo, TipoInforme, Where, OrderBy, Top, vgUsuarioIdentificador)
                Else
                    CReporte.DetalleValoresCobrados(frm, Titulo, TipoInforme, Where, OrderBy, Top, vgUsuarioIdentificador)
                End If

                'Reporte de anticipo de clientes
                'If chkVerAnticipo.Valor = True Then
                'CReporte.AnticipoClientes(frm, "Anticipo de clientes", TipoInforme, WhereAnticipo, OrderBy, Top, vgUsuarioIdentificador)
                'End If

            Case 3

                Where = " Where (Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "') "

                For Each ctr As Object In gbxFiltro.Controls

                    If ctr.GetType.Name.ToUpper = "OCXCBX" And ctr.name <> "cbxTipoProducto" Then
                        ctr.EstablecerCondicion(Where)
                    End If

                Next

                'Lote
                If chkPlanillas.Valor = True Then
                    Where = Where & " And Planilla Between '" & txtPlanillaDesde.txt.Text & "' And '" & txtPlanillaHasta.txt.Text & "'"
                End If

                'Lote
                If chkRecibo.Valor = True Then
                    Where = Where & " And NroComprobante Between " & txtReciboDesde.ObtenerValor & " And " & txtReciboHasta.ObtenerValor
                End If

                'Cliente
                If chkCliente.Valor = True Then
                    Where = Where & " And IDCliente = " & txtCliente.txtID.ObtenerValor
                End If

                If chkCobrador.Valor = True Then
                    TipoInforme = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text & " - Cobrador: " & cbxCobrador.txt.Text
                Else
                    TipoInforme = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text
                End If

                If chkSucursal.Valor = True Then
                    TipoInforme = TipoInforme & " - Suc:" & cbxSucursal.cbx.Text
                End If

                Titulo = "COMPROBANTE DE COBRANZA POR PERIODO"

                CReporte.ComprobanteCobranzaPeriodo(frm, Titulo, TipoInforme, Where, OrderBy, Top, vgUsuarioIdentificador, WhereDetalle, ASC)

        End Select

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VDetalleDocumentosCobrados ", "Select Top(0) Comprobante,Sucursal,Cliente,Planilla,Cobranza,Recibo,Fecha From VDetalleDocumentosCobrados ")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Sub SeleccionarInforme()
        Select Case cbxInforme.cbx.SelectedIndex
            Case Is = 0
                cbxCondicion.Enabled = True
                cbxOrdenadoPor.Enabled = True
                cbxEnForma.Enabled = True
                chkPorComprobante.Enabled = False
                chkPorFecha.Enabled = True
                nudRanking.Enabled = True
                chkAgrupadoPorVendedor.Visible = True
            Case Is = 1
                cbxCondicion.Enabled = False
                cbxOrdenadoPor.Enabled = False
                cbxEnForma.Enabled = False
                chkPorComprobante.Enabled = False
                chkPorFecha.Enabled = False
                nudRanking.Enabled = False
                chkAgrupadoPorVendedor.Visible = False
            Case Is = 2
                cbxCondicion.Enabled = False
                cbxOrdenadoPor.Enabled = True
                cbxEnForma.Enabled = True
                chkPorComprobante.Enabled = True
                chkPorFecha.Enabled = False
                nudRanking.Enabled = False
                chkAgrupadoPorVendedor.Visible = False
        End Select
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmDetalleDocumentosCobrados_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkPlanillas_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkPlanillas.PropertyChanged
        txtPlanillaDesde.Enabled = value
        txtPlanillaHasta.Enabled = value
        txtPlanillaDesde.txt.Focus()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkCobrador_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCobrador.PropertyChanged
        cbxCobrador.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkOrigen_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobanteCobranza.PropertyChanged
        cbxTipoComprobanteCobranza.Enabled = value
    End Sub

    Private Sub txtPlanillaDesde_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPlanillaDesde.TeclaPrecionada
        txtPlanillaHasta.txt.Text = txtPlanillaDesde.ObtenerValor
    End Sub

    Private Sub txtReciboDesde_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReciboDesde.TeclaPrecionada
        txtReciboHasta.txt.Text = txtReciboDesde.ObtenerValor
    End Sub

    Private Sub chkRecibo_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkRecibo.PropertyChanged
        txtReciboDesde.Enabled = value
        txtReciboHasta.Enabled = value
        txtReciboDesde.txt.Focus()
    End Sub

    Private Sub cbxInforme_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxInforme.KeyDown
        SeleccionarInforme()
    End Sub

    Private Sub cbxInforme_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxInforme.KeyUp
        SeleccionarInforme()
    End Sub

    Private Sub cbxInforme_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxInforme.PropertyChanged
        SeleccionarInforme()
    End Sub

    Private Sub cbxInforme_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxInforme.TeclaPrecionada
        SeleccionarInforme()
    End Sub

    Private Sub chkUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkVerAnticipo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVerAnticipo.PropertyChanged
        chkUsuario.Valor = value
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub frmPlanillaCobranza_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        'FA 20/06/2023
        Me.Dispose()
        LiberarMemoria()
    End Sub
End Class




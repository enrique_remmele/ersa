﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanillaCobranza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkAgrupadoPorVendedor = New ERP.ocxCHK()
        Me.chkVerAnticipo = New ERP.ocxCHK()
        Me.chkPorFecha = New ERP.ocxCHK()
        Me.chkPorComprobante = New ERP.ocxCHK()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.cbxInforme = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkUsuario = New ERP.ocxCHK()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.txtReciboHasta = New ERP.ocxTXTNumeric()
        Me.txtReciboDesde = New ERP.ocxTXTNumeric()
        Me.chkRecibo = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkCliente = New ERP.ocxCHK()
        Me.txtPlanillaHasta = New ERP.ocxTXTNumeric()
        Me.txtPlanillaDesde = New ERP.ocxTXTNumeric()
        Me.chkPlanillas = New ERP.ocxCHK()
        Me.chkTipoComprobanteCobranza = New ERP.ocxCHK()
        Me.cbxTipoComprobanteCobranza = New ERP.ocxCBX()
        Me.chkCobrador = New ERP.ocxCHK()
        Me.cbxCobrador = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkUsuario)
        Me.gbxFiltro.Controls.Add(Me.cbxUsuario)
        Me.gbxFiltro.Controls.Add(Me.txtReciboHasta)
        Me.gbxFiltro.Controls.Add(Me.txtReciboDesde)
        Me.gbxFiltro.Controls.Add(Me.chkRecibo)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.txtPlanillaHasta)
        Me.gbxFiltro.Controls.Add(Me.txtPlanillaDesde)
        Me.gbxFiltro.Controls.Add(Me.chkPlanillas)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobanteCobranza)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobanteCobranza)
        Me.gbxFiltro.Controls.Add(Me.chkCobrador)
        Me.gbxFiltro.Controls.Add(Me.cbxCobrador)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(437, 378)
        Me.gbxFiltro.TabIndex = 2
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(426, 397)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkAgrupadoPorVendedor)
        Me.GroupBox2.Controls.Add(Me.chkVerAnticipo)
        Me.GroupBox2.Controls.Add(Me.chkPorFecha)
        Me.GroupBox2.Controls.Add(Me.chkPorComprobante)
        Me.GroupBox2.Controls.Add(Me.cbxCondicion)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxInforme)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(455, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 378)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 106)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Condición:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Informe:"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 149)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 7
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(7, 206)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 11
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 190)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 10
        Me.lblRanking.Text = "Ranking:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(230, 399)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 433)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(708, 22)
        Me.StatusStrip1.TabIndex = 4
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'chkAgrupadoPorVendedor
        '
        Me.chkAgrupadoPorVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkAgrupadoPorVendedor.Color = System.Drawing.Color.Empty
        Me.chkAgrupadoPorVendedor.Location = New System.Drawing.Point(5, 324)
        Me.chkAgrupadoPorVendedor.Margin = New System.Windows.Forms.Padding(4)
        Me.chkAgrupadoPorVendedor.Name = "chkAgrupadoPorVendedor"
        Me.chkAgrupadoPorVendedor.Size = New System.Drawing.Size(158, 21)
        Me.chkAgrupadoPorVendedor.SoloLectura = False
        Me.chkAgrupadoPorVendedor.TabIndex = 15
        Me.chkAgrupadoPorVendedor.Texto = "Agrupado Por Vendedor"
        Me.chkAgrupadoPorVendedor.Valor = False
        Me.chkAgrupadoPorVendedor.Visible = False
        '
        'chkVerAnticipo
        '
        Me.chkVerAnticipo.BackColor = System.Drawing.Color.Transparent
        Me.chkVerAnticipo.Color = System.Drawing.Color.Empty
        Me.chkVerAnticipo.Location = New System.Drawing.Point(5, 295)
        Me.chkVerAnticipo.Margin = New System.Windows.Forms.Padding(4)
        Me.chkVerAnticipo.Name = "chkVerAnticipo"
        Me.chkVerAnticipo.Size = New System.Drawing.Size(158, 21)
        Me.chkVerAnticipo.SoloLectura = False
        Me.chkVerAnticipo.TabIndex = 14
        Me.chkVerAnticipo.Texto = "Ver Anticipo"
        Me.chkVerAnticipo.Valor = False
        '
        'chkPorFecha
        '
        Me.chkPorFecha.BackColor = System.Drawing.Color.Transparent
        Me.chkPorFecha.Color = System.Drawing.Color.Empty
        Me.chkPorFecha.Location = New System.Drawing.Point(6, 269)
        Me.chkPorFecha.Margin = New System.Windows.Forms.Padding(4)
        Me.chkPorFecha.Name = "chkPorFecha"
        Me.chkPorFecha.Size = New System.Drawing.Size(158, 21)
        Me.chkPorFecha.SoloLectura = False
        Me.chkPorFecha.TabIndex = 13
        Me.chkPorFecha.Texto = "Por Fecha"
        Me.chkPorFecha.Valor = False
        '
        'chkPorComprobante
        '
        Me.chkPorComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkPorComprobante.Color = System.Drawing.Color.Empty
        Me.chkPorComprobante.Location = New System.Drawing.Point(6, 242)
        Me.chkPorComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.chkPorComprobante.Name = "chkPorComprobante"
        Me.chkPorComprobante.Size = New System.Drawing.Size(158, 21)
        Me.chkPorComprobante.SoloLectura = False
        Me.chkPorComprobante.TabIndex = 12
        Me.chkPorComprobante.Texto = "Por Comprobante"
        Me.chkPorComprobante.Valor = False
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(7, 122)
        Me.cbxCondicion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = True
        Me.cbxCondicion.Size = New System.Drawing.Size(157, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 6
        Me.cbxCondicion.Texto = ""
        '
        'cbxInforme
        '
        Me.cbxInforme.CampoWhere = Nothing
        Me.cbxInforme.CargarUnaSolaVez = False
        Me.cbxInforme.DataDisplayMember = Nothing
        Me.cbxInforme.DataFilter = Nothing
        Me.cbxInforme.DataOrderBy = Nothing
        Me.cbxInforme.DataSource = Nothing
        Me.cbxInforme.DataValueMember = Nothing
        Me.cbxInforme.dtSeleccionado = Nothing
        Me.cbxInforme.FormABM = Nothing
        Me.cbxInforme.Indicaciones = Nothing
        Me.cbxInforme.Location = New System.Drawing.Point(6, 80)
        Me.cbxInforme.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxInforme.Name = "cbxInforme"
        Me.cbxInforme.SeleccionMultiple = False
        Me.cbxInforme.SeleccionObligatoria = True
        Me.cbxInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxInforme.SoloLectura = False
        Me.cbxInforme.TabIndex = 4
        Me.cbxInforme.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 166)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 9
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 166)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 8
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 7, 5, 11, 35, 47, 816)
        Me.txtHasta.Location = New System.Drawing.Point(89, 38)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 7, 5, 11, 35, 47, 816)
        Me.txtDesde.Location = New System.Drawing.Point(6, 38)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(8, 260)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(122, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 25
        Me.chkTipoProducto.Texto = "Tipo Producto"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "vTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(138, 260)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(276, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 26
        Me.cbxTipoProducto.Texto = ""
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(8, 204)
        Me.chkMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(114, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 24
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(137, 202)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(213, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 20
        Me.cbxMoneda.Texto = ""
        '
        'chkUsuario
        '
        Me.chkUsuario.BackColor = System.Drawing.Color.Transparent
        Me.chkUsuario.Color = System.Drawing.Color.Empty
        Me.chkUsuario.Location = New System.Drawing.Point(8, 231)
        Me.chkUsuario.Margin = New System.Windows.Forms.Padding(4)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(89, 21)
        Me.chkUsuario.SoloLectura = False
        Me.chkUsuario.TabIndex = 18
        Me.chkUsuario.Texto = "Usuario:"
        Me.chkUsuario.Valor = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "VUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(138, 231)
        Me.cbxUsuario.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(276, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 19
        Me.cbxUsuario.Texto = ""
        '
        'txtReciboHasta
        '
        Me.txtReciboHasta.Color = System.Drawing.Color.Empty
        Me.txtReciboHasta.Decimales = True
        Me.txtReciboHasta.Enabled = False
        Me.txtReciboHasta.Indicaciones = Nothing
        Me.txtReciboHasta.Location = New System.Drawing.Point(248, 45)
        Me.txtReciboHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtReciboHasta.Name = "txtReciboHasta"
        Me.txtReciboHasta.Size = New System.Drawing.Size(102, 20)
        Me.txtReciboHasta.SoloLectura = False
        Me.txtReciboHasta.TabIndex = 5
        Me.txtReciboHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtReciboHasta.Texto = "0"
        '
        'txtReciboDesde
        '
        Me.txtReciboDesde.Color = System.Drawing.Color.Empty
        Me.txtReciboDesde.Decimales = True
        Me.txtReciboDesde.Enabled = False
        Me.txtReciboDesde.Indicaciones = Nothing
        Me.txtReciboDesde.Location = New System.Drawing.Point(137, 45)
        Me.txtReciboDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtReciboDesde.Name = "txtReciboDesde"
        Me.txtReciboDesde.Size = New System.Drawing.Size(105, 20)
        Me.txtReciboDesde.SoloLectura = False
        Me.txtReciboDesde.TabIndex = 4
        Me.txtReciboDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtReciboDesde.Texto = "0"
        '
        'chkRecibo
        '
        Me.chkRecibo.BackColor = System.Drawing.Color.Transparent
        Me.chkRecibo.Color = System.Drawing.Color.Empty
        Me.chkRecibo.Location = New System.Drawing.Point(7, 46)
        Me.chkRecibo.Margin = New System.Windows.Forms.Padding(4)
        Me.chkRecibo.Name = "chkRecibo"
        Me.chkRecibo.Size = New System.Drawing.Size(82, 21)
        Me.chkRecibo.SoloLectura = False
        Me.chkRecibo.TabIndex = 3
        Me.chkRecibo.Texto = "Recibo:"
        Me.chkRecibo.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 100
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(72, 295)
        Me.txtCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(353, 73)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 17
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 295)
        Me.chkCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(57, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 16
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'txtPlanillaHasta
        '
        Me.txtPlanillaHasta.Color = System.Drawing.Color.Empty
        Me.txtPlanillaHasta.Decimales = True
        Me.txtPlanillaHasta.Enabled = False
        Me.txtPlanillaHasta.Indicaciones = Nothing
        Me.txtPlanillaHasta.Location = New System.Drawing.Point(249, 18)
        Me.txtPlanillaHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPlanillaHasta.Name = "txtPlanillaHasta"
        Me.txtPlanillaHasta.Size = New System.Drawing.Size(102, 20)
        Me.txtPlanillaHasta.SoloLectura = False
        Me.txtPlanillaHasta.TabIndex = 2
        Me.txtPlanillaHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlanillaHasta.Texto = "0"
        '
        'txtPlanillaDesde
        '
        Me.txtPlanillaDesde.Color = System.Drawing.Color.Empty
        Me.txtPlanillaDesde.Decimales = True
        Me.txtPlanillaDesde.Enabled = False
        Me.txtPlanillaDesde.Indicaciones = Nothing
        Me.txtPlanillaDesde.Location = New System.Drawing.Point(138, 18)
        Me.txtPlanillaDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtPlanillaDesde.Name = "txtPlanillaDesde"
        Me.txtPlanillaDesde.Size = New System.Drawing.Size(105, 20)
        Me.txtPlanillaDesde.SoloLectura = False
        Me.txtPlanillaDesde.TabIndex = 1
        Me.txtPlanillaDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPlanillaDesde.Texto = "0"
        '
        'chkPlanillas
        '
        Me.chkPlanillas.BackColor = System.Drawing.Color.Transparent
        Me.chkPlanillas.Color = System.Drawing.Color.Empty
        Me.chkPlanillas.Location = New System.Drawing.Point(7, 19)
        Me.chkPlanillas.Margin = New System.Windows.Forms.Padding(4)
        Me.chkPlanillas.Name = "chkPlanillas"
        Me.chkPlanillas.Size = New System.Drawing.Size(82, 21)
        Me.chkPlanillas.SoloLectura = False
        Me.chkPlanillas.TabIndex = 0
        Me.chkPlanillas.Texto = "Planillas:"
        Me.chkPlanillas.Valor = False
        '
        'chkTipoComprobanteCobranza
        '
        Me.chkTipoComprobanteCobranza.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobanteCobranza.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobanteCobranza.Location = New System.Drawing.Point(8, 177)
        Me.chkTipoComprobanteCobranza.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoComprobanteCobranza.Name = "chkTipoComprobanteCobranza"
        Me.chkTipoComprobanteCobranza.Size = New System.Drawing.Size(131, 21)
        Me.chkTipoComprobanteCobranza.SoloLectura = False
        Me.chkTipoComprobanteCobranza.TabIndex = 14
        Me.chkTipoComprobanteCobranza.Texto = "Comprobante Cobranza:"
        Me.chkTipoComprobanteCobranza.Valor = False
        '
        'cbxTipoComprobanteCobranza
        '
        Me.cbxTipoComprobanteCobranza.CampoWhere = "IDTipoComprobanteCobranza"
        Me.cbxTipoComprobanteCobranza.CargarUnaSolaVez = False
        Me.cbxTipoComprobanteCobranza.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobanteCobranza.DataFilter = ""
        Me.cbxTipoComprobanteCobranza.DataOrderBy = Nothing
        Me.cbxTipoComprobanteCobranza.DataSource = ""
        Me.cbxTipoComprobanteCobranza.DataValueMember = "ID"
        Me.cbxTipoComprobanteCobranza.dtSeleccionado = Nothing
        Me.cbxTipoComprobanteCobranza.Enabled = False
        Me.cbxTipoComprobanteCobranza.FormABM = Nothing
        Me.cbxTipoComprobanteCobranza.Indicaciones = Nothing
        Me.cbxTipoComprobanteCobranza.Location = New System.Drawing.Point(138, 175)
        Me.cbxTipoComprobanteCobranza.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobanteCobranza.Name = "cbxTipoComprobanteCobranza"
        Me.cbxTipoComprobanteCobranza.SeleccionMultiple = False
        Me.cbxTipoComprobanteCobranza.SeleccionObligatoria = False
        Me.cbxTipoComprobanteCobranza.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoComprobanteCobranza.SoloLectura = False
        Me.cbxTipoComprobanteCobranza.TabIndex = 15
        Me.cbxTipoComprobanteCobranza.Texto = ""
        '
        'chkCobrador
        '
        Me.chkCobrador.BackColor = System.Drawing.Color.Transparent
        Me.chkCobrador.Color = System.Drawing.Color.Empty
        Me.chkCobrador.Location = New System.Drawing.Point(7, 125)
        Me.chkCobrador.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCobrador.Name = "chkCobrador"
        Me.chkCobrador.Size = New System.Drawing.Size(79, 21)
        Me.chkCobrador.SoloLectura = False
        Me.chkCobrador.TabIndex = 10
        Me.chkCobrador.Texto = "Cobrador:"
        Me.chkCobrador.Valor = False
        '
        'cbxCobrador
        '
        Me.cbxCobrador.CampoWhere = "IDCobrador"
        Me.cbxCobrador.CargarUnaSolaVez = False
        Me.cbxCobrador.DataDisplayMember = "Nombres"
        Me.cbxCobrador.DataFilter = Nothing
        Me.cbxCobrador.DataOrderBy = "Nombres"
        Me.cbxCobrador.DataSource = "Cobrador"
        Me.cbxCobrador.DataValueMember = "ID"
        Me.cbxCobrador.dtSeleccionado = Nothing
        Me.cbxCobrador.Enabled = False
        Me.cbxCobrador.FormABM = Nothing
        Me.cbxCobrador.Indicaciones = Nothing
        Me.cbxCobrador.Location = New System.Drawing.Point(138, 123)
        Me.cbxCobrador.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCobrador.Name = "cbxCobrador"
        Me.cbxCobrador.SeleccionMultiple = False
        Me.cbxCobrador.SeleccionObligatoria = False
        Me.cbxCobrador.Size = New System.Drawing.Size(213, 21)
        Me.cbxCobrador.SoloLectura = False
        Me.cbxCobrador.TabIndex = 11
        Me.cbxCobrador.Texto = ""
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(7, 99)
        Me.chkVendedor.Margin = New System.Windows.Forms.Padding(4)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(79, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 8
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "Vendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(138, 97)
        Me.cbxVendedor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(213, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 9
        Me.cbxVendedor.Texto = ""
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(7, 74)
        Me.chkTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(116, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 6
        Me.chkTipoComprobante.Texto = "Tipo Comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = "TipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(138, 72)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 7
        Me.cbxTipoComprobante.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(8, 152)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(79, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 12
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursalOperacion"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(138, 149)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 13
        Me.cbxSucursal.Texto = ""
        '
        'frmPlanillaCobranza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(708, 455)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmPlanillaCobranza"
        Me.Text = "Planilla de Cobranzas"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxInforme As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkTipoComprobanteCobranza As ERP.ocxCHK
    Friend WithEvents cbxTipoComprobanteCobranza As ERP.ocxCBX
    Friend WithEvents chkCobrador As ERP.ocxCHK
    Friend WithEvents cbxCobrador As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents txtPlanillaHasta As ERP.ocxTXTNumeric
    Friend WithEvents txtPlanillaDesde As ERP.ocxTXTNumeric
    Friend WithEvents chkPlanillas As ERP.ocxCHK
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents txtReciboHasta As ERP.ocxTXTNumeric
    Friend WithEvents txtReciboDesde As ERP.ocxTXTNumeric
    Friend WithEvents chkRecibo As ERP.ocxCHK
    Friend WithEvents chkPorComprobante As ERP.ocxCHK
    Friend WithEvents chkPorFecha As ERP.ocxCHK
    Friend WithEvents chkUsuario As ERP.ocxCHK
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents chkVerAnticipo As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkAgrupadoPorVendedor As ocxCHK
End Class

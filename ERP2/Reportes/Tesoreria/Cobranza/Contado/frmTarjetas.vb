﻿Imports ERP.Reporte
Public Class frmTarjetas
    'CLASES
    Dim CReporte As New CCliente
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "INVENTARIO DE TARJETAS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()
        Try
            'Moneda
            CSistema.SqlToComboBox(cbxTipo.cbx, "Select ID, Descripcion From TipoComprobante where ID in (21,48) Order By Descripcion Asc", VGCadenaConexion)
            cbxTipo.cbx.SelectedIndex = 1
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As Boolean = True
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        If Not IsDate(txtDesde.txt.Text) Or Not IsDate(txtHasta.txt.Text) Then
            Dim mensaje As String = "La fecha no es correcta!"
            ctrError.SetError(txtDesde, mensaje)
            ctrError.SetIconAlignment(txtDesde, ErrorIconAlignment.TopRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        
        Where = " where Fecha between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"

        If cbxTipo.Enabled = True Then
            Where = Where & " and IDTipoComprobante = " & cbxTipo.GetValue
        End If

        If cbxSucusal.Enabled = True Then
            Where = Where & " and IDSucursal = " & cbxSucusal.GetValue
        End If

        If chkNoDepositados.Valor = True Then
            'Where = Where & " and IDTransaccion not in (Select IDTransaccionTarjeta from DetalleDepositoBancario where IDTransaccionTarjeta is not null)"
            Where = Where & " and Cancelado = 0 "
        End If

        OrderBy = " Order by Fecha "
        TipoInforme = ""
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        CReporte.Tarjetas(frm, Titulo, Where, OrderBy, TipoInforme)


    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmExtractoMovimientoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipo.PropertyChanged
        cbxTipo.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucusal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

End Class
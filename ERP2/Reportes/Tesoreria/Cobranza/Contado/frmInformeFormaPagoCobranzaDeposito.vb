﻿Imports ERP.Reporte
Public Class frmInformeFormaPagoCobranzaDeposito
    'CLASES
    Dim CReporte As New CCliente
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "FORMA DE PAGO (COBRANZAS - DEPOSITOS - RECHAZOS)"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
        txtDesdeCobranza.PrimerDiaMes()
        txtHastaCobranza.Hoy()
        CargarInformacion()

    End Sub

    Sub CargarInformacion()
        'Ordenado por
        CSistema.SqlToComboBox(cbxFormaPago.cbx, "select distinct FormaPago,FormaPago from vFormaPago")
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
        txtCliente.frm = Me
    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1 "
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As Boolean = True
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        If Not IsDate(txtDesde.txt.Text) Or Not IsDate(txtHasta.txt.Text) Then
            Dim mensaje As String = "La fecha no es correcta!"
            ctrError.SetError(txtDesde, mensaje)
            ctrError.SetIconAlignment(txtDesde, ErrorIconAlignment.TopRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If


        If chkFechaFP.Valor = True Then
            Where = Where & " and cast(FechaFormaPago as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        End If

        If chkFechaCobranza.Valor = True Then
            Where = Where & " and cast(FechaCobranza as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeCobranza.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaCobranza.txt.Text, True, False) & "'"
        End If

        If chkNoDepositados.Valor = True Then
            Where = Where & " and Deposito = '- -'"
        End If
        If chkFormaPago.Valor = True Then
            Where = Where & " and FormaPago = '" & cbxFormaPago.cbx.Text & "' "
        End If
        If chkMoneda.Valor = True Then
            Where = Where & " and IDMoneda = " & cbxMoneda.GetValue
        End If

        If chkComprobante.Valor = True Then
            Where = Where & " and Comprobante like '%" & txtComprobante.Texto & "%' "
        End If

        If chkCliente.Valor = True And txtCliente.txtReferencia.Texto <> "" Then
            Where = Where & " and IDCliente = " & txtCliente.Registro("ID").ToString
        End If

        If chkSucursal.Valor = True Then
            Where = Where & " and IDSucursal = " & cbxSucursal.GetValue
        End If

        OrderBy = " Order by FechaFormaPago "
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)

        CReporte.FormaPagoCobranzaDepositoRechazdo(frm, Titulo, Where, OrderBy, TipoInforme)


    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmExtractoMovimientoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub


    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkFormaPago.PropertyChanged
        cbxFormaPago.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub


    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkComprobante_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkComprobante.PropertyChanged
        txtComprobante.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged_1(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkFechaFP_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaFP.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkFechaCobranza_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaCobranza.PropertyChanged
        txtDesdeCobranza.Enabled = value
        txtHastaCobranza.Enabled = value
    End Sub
End Class
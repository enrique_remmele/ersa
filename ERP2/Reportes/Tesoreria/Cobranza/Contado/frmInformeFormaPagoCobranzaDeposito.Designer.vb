﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInformeFormaPagoCobranzaDeposito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.chkComprobante = New ERP.ocxCHK()
        Me.cbxFormaPago = New ERP.ocxCBX()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkFormaPago = New ERP.ocxCHK()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkFechaCobranza = New ERP.ocxCHK()
        Me.chkFechaFP = New ERP.ocxCHK()
        Me.txtHastaCobranza = New ERP.ocxTXTDate()
        Me.txtDesdeCobranza = New ERP.ocxTXTDate()
        Me.chkNoDepositados = New ERP.ocxCHK()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.txtComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxFormaPago)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkFormaPago)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 1)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(472, 221)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(13, 101)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(71, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(124, 101)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(217, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(13, 129)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(86, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 8
        Me.chkCliente.Texto = "Cliente"
        Me.chkCliente.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(13, 156)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(422, 28)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 9
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Enabled = False
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(124, 72)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(102, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 5
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'chkComprobante
        '
        Me.chkComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkComprobante.Color = System.Drawing.Color.Empty
        Me.chkComprobante.Location = New System.Drawing.Point(13, 72)
        Me.chkComprobante.Name = "chkComprobante"
        Me.chkComprobante.Size = New System.Drawing.Size(86, 21)
        Me.chkComprobante.SoloLectura = False
        Me.chkComprobante.TabIndex = 4
        Me.chkComprobante.Texto = "Comprobante:"
        Me.chkComprobante.Valor = False
        '
        'cbxFormaPago
        '
        Me.cbxFormaPago.CampoWhere = Nothing
        Me.cbxFormaPago.CargarUnaSolaVez = False
        Me.cbxFormaPago.DataDisplayMember = Nothing
        Me.cbxFormaPago.DataFilter = Nothing
        Me.cbxFormaPago.DataOrderBy = Nothing
        Me.cbxFormaPago.DataSource = Nothing
        Me.cbxFormaPago.DataValueMember = Nothing
        Me.cbxFormaPago.dtSeleccionado = Nothing
        Me.cbxFormaPago.Enabled = False
        Me.cbxFormaPago.FormABM = Nothing
        Me.cbxFormaPago.Indicaciones = Nothing
        Me.cbxFormaPago.Location = New System.Drawing.Point(124, 16)
        Me.cbxFormaPago.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxFormaPago.Name = "cbxFormaPago"
        Me.cbxFormaPago.SeleccionMultiple = False
        Me.cbxFormaPago.SeleccionObligatoria = True
        Me.cbxFormaPago.Size = New System.Drawing.Size(157, 21)
        Me.cbxFormaPago.SoloLectura = False
        Me.cbxFormaPago.TabIndex = 1
        Me.cbxFormaPago.Texto = ""
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(13, 44)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(71, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 2
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(124, 44)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(173, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 3
        Me.cbxMoneda.Texto = ""
        '
        'chkFormaPago
        '
        Me.chkFormaPago.BackColor = System.Drawing.Color.Transparent
        Me.chkFormaPago.Color = System.Drawing.Color.Empty
        Me.chkFormaPago.Location = New System.Drawing.Point(13, 16)
        Me.chkFormaPago.Name = "chkFormaPago"
        Me.chkFormaPago.Size = New System.Drawing.Size(71, 21)
        Me.chkFormaPago.SoloLectura = False
        Me.chkFormaPago.TabIndex = 0
        Me.chkFormaPago.Texto = "Tipo:"
        Me.chkFormaPago.Valor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaCobranza)
        Me.GroupBox2.Controls.Add(Me.chkFechaFP)
        Me.GroupBox2.Controls.Add(Me.txtHastaCobranza)
        Me.GroupBox2.Controls.Add(Me.txtDesdeCobranza)
        Me.GroupBox2.Controls.Add(Me.chkNoDepositados)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 230)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(470, 103)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkFechaCobranza
        '
        Me.chkFechaCobranza.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaCobranza.Color = System.Drawing.Color.Empty
        Me.chkFechaCobranza.Location = New System.Drawing.Point(194, 23)
        Me.chkFechaCobranza.Name = "chkFechaCobranza"
        Me.chkFechaCobranza.Size = New System.Drawing.Size(154, 21)
        Me.chkFechaCobranza.SoloLectura = False
        Me.chkFechaCobranza.TabIndex = 3
        Me.chkFechaCobranza.Texto = "Fecha de Cobranza"
        Me.chkFechaCobranza.Valor = True
        '
        'chkFechaFP
        '
        Me.chkFechaFP.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaFP.Color = System.Drawing.Color.Empty
        Me.chkFechaFP.Location = New System.Drawing.Point(19, 23)
        Me.chkFechaFP.Name = "chkFechaFP"
        Me.chkFechaFP.Size = New System.Drawing.Size(154, 21)
        Me.chkFechaFP.SoloLectura = False
        Me.chkFechaFP.TabIndex = 0
        Me.chkFechaFP.Texto = "Fecha de Forma de Pago"
        Me.chkFechaFP.Valor = False
        '
        'txtHastaCobranza
        '
        Me.txtHastaCobranza.Color = System.Drawing.Color.Empty
        Me.txtHastaCobranza.Fecha = New Date(2013, 2, 18, 13, 3, 29, 337)
        Me.txtHastaCobranza.Location = New System.Drawing.Point(274, 49)
        Me.txtHastaCobranza.Name = "txtHastaCobranza"
        Me.txtHastaCobranza.PermitirNulo = False
        Me.txtHastaCobranza.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaCobranza.SoloLectura = False
        Me.txtHastaCobranza.TabIndex = 5
        '
        'txtDesdeCobranza
        '
        Me.txtDesdeCobranza.Color = System.Drawing.Color.Empty
        Me.txtDesdeCobranza.Fecha = New Date(2013, 2, 18, 13, 3, 29, 337)
        Me.txtDesdeCobranza.Location = New System.Drawing.Point(194, 49)
        Me.txtDesdeCobranza.Name = "txtDesdeCobranza"
        Me.txtDesdeCobranza.PermitirNulo = False
        Me.txtDesdeCobranza.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeCobranza.SoloLectura = False
        Me.txtDesdeCobranza.TabIndex = 4
        '
        'chkNoDepositados
        '
        Me.chkNoDepositados.BackColor = System.Drawing.Color.Transparent
        Me.chkNoDepositados.Color = System.Drawing.Color.Empty
        Me.chkNoDepositados.Location = New System.Drawing.Point(19, 76)
        Me.chkNoDepositados.Name = "chkNoDepositados"
        Me.chkNoDepositados.Size = New System.Drawing.Size(96, 21)
        Me.chkNoDepositados.SoloLectura = False
        Me.chkNoDepositados.TabIndex = 6
        Me.chkNoDepositados.Texto = "No depositados"
        Me.chkNoDepositados.Valor = False
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(2013, 2, 18, 13, 3, 29, 337)
        Me.txtHasta.Location = New System.Drawing.Point(99, 49)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Enabled = False
        Me.txtDesde.Fecha = New Date(2013, 2, 18, 13, 3, 29, 337)
        Me.txtDesde.Location = New System.Drawing.Point(19, 49)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(284, 339)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(89, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(379, 339)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 379)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(493, 22)
        Me.StatusStrip1.TabIndex = 19
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmInformeFormaPagoCobranzaDeposito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(493, 401)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "frmInformeFormaPagoCobranzaDeposito"
        Me.Text = "frmInformeFormaPagoCobranzaDeposito"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkNoDepositados As ERP.ocxCHK
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents chkFormaPago As ERP.ocxCHK
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents cbxFormaPago As ERP.ocxCBX
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents chkComprobante As ERP.ocxCHK
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtHastaCobranza As ERP.ocxTXTDate
    Friend WithEvents txtDesdeCobranza As ERP.ocxTXTDate
    Friend WithEvents chkFechaFP As ERP.ocxCHK
    Friend WithEvents chkFechaCobranza As ERP.ocxCHK
End Class

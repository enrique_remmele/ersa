﻿Imports ERP.Reporte
Public Class frmListadoValeGasto
    'CLASES
    Dim CReporte As New CReporteFondoFijo
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE VALES"

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()

        CargarInformacion()


    End Sub

    Sub CargarInformacion()

        cbxTipo.cbx.Items.Add("TODOS")
        cbxTipo.cbx.Items.Add("VALES A RENDIR")
        cbxTipo.cbx.Items.Add("VALES A RENDIR PENDIENTES")
        cbxTipo.cbx.Items.Add("VALES SIN RENDIR")

        cbxTipo.cbx.SelectedIndex = 0
        'Grupo
        CSistema.SqlToComboBox(cbxGrupo.cbx, CData.GetTable("vGrupoUsuario", "IDUsuario=" & vgIDUsuario), "IDGrupo", "Grupo")

    End Sub

    Sub Listar()

        Dim TipoInforme As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And Anulado='False' "

        If chkGrupo.Valor = True Then
            Where = Where & " and Idgrupo = " & cbxGrupo.GetValue
        End If

        Select Case cbxTipo.cbx.SelectedIndex
            Case 0
                Titulo = "LISTADO DE TODOS LOS VALES"
            Case 1
                Titulo = "LISTADO DE VALES A RENDIR"
                Where = Where & " And ARendir='True'"
            Case 2
                Titulo = "LISTADO DE VALES A RENDIR PENDIENTES"
                Where = Where & " And ARendir='True' And Cancelado='False'"
            Case 3
                Titulo = "LISTADO DE VALES SIN RENDICION"
                Where = Where & " And ARendir='False'"
        End Select

        TipoInforme = "PERIODO: " & txtDesde.GetValue & " - " & txtHasta.GetValue
        CReporte.ListadoValesGasto(frm, Titulo, TipoInforme, Where, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)

    End Sub


    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoVale_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkGrupo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkGrupo.PropertyChanged
        cbxGrupo.Enabled = value
    End Sub
End Class
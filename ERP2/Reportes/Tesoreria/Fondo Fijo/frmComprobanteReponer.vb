﻿Imports ERP.Reporte

Public Class frmComprobanteReponer

    'CLASES
    Dim CSistema As New CSistema
    Public CData As New CData
    Dim CReporte As New CReporteFondoFijo
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim vNuevo As Boolean
    Dim vNuevoPE As Boolean
    Dim vControles() As Control
    Dim dtFondoFijo As DataTable
    Dim dtDetalleFondoFijo As DataTable
    Dim dtDetalleFondoFijoContabilidad As DataTable
    Dim dtGrupo As DataTable
    Dim Consulta As String
    Dim Titulo As String = "LISTADO DE COMPROBANTES A REPONER"

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private NuevoIDTransaccionValue As Integer

    Public Property NuevoIDTransaccion() As Integer
        Get
            Return NuevoIDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            NuevoIDTransaccionValue = value
        End Set
    End Property

    Private IDOperacionValue As Integer
    Public Property IDOperacion() As Integer
        Get
            Return IDOperacionValue
        End Get
        Set(ByVal value As Integer)
            IDOperacionValue = value
        End Set
    End Property

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones
        CargarInformacion()

        'Focus
        dgw.Focus()

    End Sub

    Sub CargarInformacion()

        'Fondo Fijo
        dtFondoFijo = CData.GetTable("VFondoFijo")

        'Sucursal
        CSistema.SqlToComboBox(cbxSucursal.cbx, " Select ID, Codigo From VSucursal Order By 2 ")

        'Grupo
        'CSistema.SqlToComboBox(cbxGrupo.cbx, " Select ID, Descripcion From Grupo Order By 2 ")

        'cbxGrupo.cbx.SelectedIndex = 1


    End Sub

    Sub ListarComprobantes(Optional ByVal ID As Integer = 0)

        dgw.Rows.Clear()

        'Cargar en la Lista
        For Each oRow As DataRow In dtDetalleFondoFijo.Rows

            Dim Registro(10) As String
            Registro(0) = oRow("Sel").ToString
            Registro(1) = oRow("Numero").ToString
            Registro(2) = oRow("Tipo").ToString
            Registro(3) = oRow("NroComprobante").ToString
            Registro(4) = oRow("Fecha").ToString
            Registro(5) = oRow("RazonSocial").ToString
            Registro(6) = CSistema.FormatoMoneda(oRow("Total").ToString)
            Registro(7) = oRow("Sucursal").ToString
            Registro(8) = oRow("Grupo").ToString
            Registro(9) = oRow("IDTransaccionGasto").ToString
            Registro(10) = oRow("IDTransaccionVale").ToString

            dgw.Rows.Add(Registro)

        Next
        For Each item As DataGridViewRow In dgw.Rows
            item.Cells(1).ReadOnly = True
            item.Cells(2).ReadOnly = True
            item.Cells(3).ReadOnly = True
            item.Cells(4).ReadOnly = True
            item.Cells(5).ReadOnly = True
            item.Cells(6).ReadOnly = True
            item.Cells(7).ReadOnly = True
        Next


    End Sub

    Sub CargarComprobantes()

        'Cargar Tabla
        dtDetalleFondoFijo = CSistema.ExecuteToDataTable("Select 'Sel'='True',Numero,Tipo,TipoComprobante,Fecha, NroComprobante,Observacion,RazonSocial, Total, IDTransaccionGasto, IDTransaccionVale, Sucursal, Grupo From VDetalleFondoFijo Where IDSucursal=" & cbxSucursal.GetValue & " And IDGrupo=" & cbxGrupo.GetValue & " And Cancelado='False' Order By Numero")

        dtDetalleFondoFijoContabilidad = CSistema.ExecuteToDataTable("Select 'Sel'='True',Numero,Tipo,TipoComprobante,Fecha, NroComprobante,Observacion,RazonSocial, Total, IDTransaccionGasto, IDTransaccionVale, Sucursal, Grupo, Codigo, Descripcion, ObservacionAsiento, Debito, NroTimbrado, FechaVencimientoTimbrado From VDetalleFondoFijoContabilidad Where IDSucursal=" & cbxSucursal.GetValue & " And IDGrupo=" & cbxGrupo.GetValue & " And Cancelado='False' Order By Numero")

        'Listar
        ListarComprobantes()

        CalcularTotales()

        btnImprimir.Focus()

    End Sub

    Sub InicializarControles()

        txtReponer.txt.Clear()
        txtSaldo.txt.Clear()

    End Sub

    Sub ObtenerInformacionFondoFijo()

        txtTope.SetValue(0)

        'Validar
        'Sucursal
        If cbxSucursal.Validar() = False Then
            Exit Sub
        End If

        If cbxGrupo.Validar = False Then
            Exit Sub
        End If

        If Not dtFondoFijo Is Nothing Then
            For Each oRow As DataRow In dtFondoFijo.Select(" IDSucursal=" & cbxSucursal.cbx.SelectedValue & " And IDGrupo=" & cbxGrupo.cbx.SelectedValue & " ")
                txtTope.txt.Text = oRow("Tope").ToString
                txtSaldo.txt.Text = Int(txtTope.txt.Text) - Int(txtReponer.txt.Text)
                Exit For
            Next
        End If

    End Sub

    Sub CalcularTotales()

        Dim TotalComprobantes As Decimal = 0
        Dim Saldo As Decimal = 0
        Dim CantidadComprobantes As Decimal = 0

        For Each item As DataGridViewRow In dgw.Rows
            If item.Cells("colSel").Value = True Then
                TotalComprobantes = TotalComprobantes + CDbl(item.Cells("colTotal").Value)
                CantidadComprobantes = CantidadComprobantes + 1
                Saldo = txtTope.txt.Text - CDbl(TotalComprobantes)
            End If
        Next
        txtReponer.SetValue(TotalComprobantes)
        txtSaldo.SetValue(Saldo)
        txtCantidadComprobantes.SetValue(CantidadComprobantes)

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If


        Dim dt As DataTable = dtDetalleFondoFijo.Clone

        For Each item As DataGridViewRow In dgw.Rows

            If item.Cells(0).Value = True Then
                Dim IDTransaccionGasto As String = item.Cells("colIDTransaccionGasto").Value
                Dim IDTransaccionVale As String = item.Cells("colIDTransaccionVale").Value

                For Each oRow As DataRow In dtDetalleFondoFijo.Select(" IDTransaccionGasto=" & IDTransaccionGasto & " And IDTransaccionVale=" & IDTransaccionVale)
                    dt.Rows.Add(oRow.ItemArray)
                Next
            End If

        Next


        CReporte.Rendicion(frm, dt, Titulo, vgUsuarioIdentificador, txtReponer.txt.Text, txtSaldo.txt.Text, txtTope.txt.Text, txtFecha.GetValue)

    End Sub

    Sub ListarInformeContable()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If dgw.Rows.Count = 0 Then
            Exit Sub
        End If


        Dim dt As DataTable = dtDetalleFondoFijoContabilidad.Clone
        Dim DescripcionCuentaCredito As String = ""
        For Each item As DataGridViewRow In dgw.Rows

            If item.Cells(0).Value = True Then
                Dim IDTransaccionGasto As String = item.Cells("colIDTransaccionGasto").Value
                Dim IDTransaccionVale As String = item.Cells("colIDTransaccionVale").Value

                For Each oRow As DataRow In dtDetalleFondoFijoContabilidad.Select(" IDTransaccionGasto=" & IDTransaccionGasto & " And IDTransaccionVale=" & IDTransaccionVale)
                    dt.Rows.Add(oRow.ItemArray)
                Next

                If DescripcionCuentaCredito = "" Then
                    DescripcionCuentaCredito = CSistema.ExecuteScalar("Select Top(1) Descripcion From VDetalleAsiento Where IDTransaccion=" & IDTransaccionGasto & " And Credito>0")
                End If

            End If

        Next

        CReporte.RendicionContabilidad(frm, dt, Titulo, vgUsuarioIdentificador, txtReponer.txt.Text, txtSaldo.txt.Text, txtTope.txt.Text, txtFecha.GetValue, DescripcionCuentaCredito)

    End Sub

    Sub Obtener()
        Dim dt As DataTable = dtDetalleFondoFijo.Clone

        For Each item As DataGridViewRow In dgw.Rows

            If item.Cells(0).Value = True Then
                Dim IDTransaccionGasto As String = item.Cells("colIDTransaccionGasto").Value
                Dim IDTransaccionVale As String = item.Cells("colIDTransaccionVale").Value

                For Each oRow As DataRow In dtDetalleFondoFijo.Select(" IDTransaccionGasto=" & IDTransaccionGasto & " And IDTransaccionVale=" & IDTransaccionVale)
                    dt.Rows.Add(oRow.ItemArray)
                Next
            End If

        Next
    End Sub

    Private Sub frmRendicionFondoFijo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbxSucursal_Leave(sender As Object, e As System.EventArgs) Handles cbxSucursal.Leave

        cbxGrupo.cbx.DataSource = Nothing

        If IsNumeric(cbxSucursal.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If cbxSucursal.cbx.Text.Trim = "" Then
            Exit Sub
        End If

        'Sucursales
        CSistema.SqlToComboBox(cbxGrupo.cbx, "Select IDGrupo, Grupo From VFondoFijo Where IDSucursal=" & cbxSucursal.cbx.SelectedValue)

        ObtenerInformacionFondoFijo()

    End Sub

    Private Sub frmRendicionFondoFijo_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub btnCargarComprobantes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargarComprobantes.Click
        CargarComprobantes()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Listar()
    End Sub

    Private Sub dgw_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgw.CellContentClick

        If dgw.SelectedCells.Count = 0 Then
            Exit Sub
        End If

        Dim RowIndex As Integer = dgw.SelectedCells.Item(0).RowIndex

        If e.ColumnIndex = dgw.Columns.Item("colSel").Index Then

            For Each oRow As DataGridViewRow In dgw.Rows

                If oRow.Index = RowIndex Then

                    If oRow.Cells("colSel").Value = False Then
                        oRow.Cells("colSel").Value = True
                        oRow.Cells("colSel").ReadOnly = False
                    Else
                        oRow.Cells("colSel").ReadOnly = True
                        oRow.Cells("colSel").Value = False
                    End If

                    Exit For

                End If

            Next

            CalcularTotales()

            dgw.Update()

        End If

    End Sub

    Private Sub cbxGrupo_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxGrupo.TeclaPrecionada
        ObtenerInformacionFondoFijo()
    End Sub

    Private Sub cbxSucursal_TeclaPrecionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxSucursal.TeclaPrecionada
        ObtenerInformacionFondoFijo()
    End Sub

    Private Sub cbxGrupo_Leave(sender As System.Object, e As System.EventArgs) Handles cbxGrupo.Leave
        ObtenerInformacionFondoFijo()
    End Sub

    Private Sub btnInformeContable_Click(sender As System.Object, e As System.EventArgs) Handles btnInformeContable.Click
        ListarInformeContable()
    End Sub
End Class
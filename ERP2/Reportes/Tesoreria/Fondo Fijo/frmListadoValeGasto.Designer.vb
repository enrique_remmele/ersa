﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoValeGasto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTipoInforme = New System.Windows.Forms.Label()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkGrupo = New ERP.ocxCHK()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.SuspendLayout()
        '
        'lblTipoInforme
        '
        Me.lblTipoInforme.AutoSize = True
        Me.lblTipoInforme.Location = New System.Drawing.Point(14, 11)
        Me.lblTipoInforme.Name = "lblTipoInforme"
        Me.lblTipoInforme.Size = New System.Drawing.Size(84, 13)
        Me.lblTipoInforme.TabIndex = 7
        Me.lblTipoInforme.Text = "Tipo de Informe:"
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(14, 27)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionObligatoria = True
        Me.cbxTipo.Size = New System.Drawing.Size(226, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 8
        Me.cbxTipo.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 133)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Periodo:"
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 21, 10, 3, 18, 309)
        Me.txtHasta.Location = New System.Drawing.Point(97, 149)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 11
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 21, 10, 3, 18, 309)
        Me.txtDesde.Location = New System.Drawing.Point(14, 149)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 10
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(177, 191)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 13
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(14, 191)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 12
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkGrupo
        '
        Me.chkGrupo.BackColor = System.Drawing.Color.Transparent
        Me.chkGrupo.Color = System.Drawing.Color.Empty
        Me.chkGrupo.Location = New System.Drawing.Point(16, 67)
        Me.chkGrupo.Name = "chkGrupo"
        Me.chkGrupo.Size = New System.Drawing.Size(89, 21)
        Me.chkGrupo.SoloLectura = False
        Me.chkGrupo.TabIndex = 14
        Me.chkGrupo.Texto = "Grupo:"
        Me.chkGrupo.Valor = False
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = "IDGrupo"
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = "Descripcion"
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = "Descripcion"
        Me.cbxGrupo.DataSource = Nothing
        Me.cbxGrupo.DataValueMember = "ID"
        Me.cbxGrupo.Enabled = False
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(17, 88)
        Me.cbxGrupo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionObligatoria = False
        Me.cbxGrupo.Size = New System.Drawing.Size(276, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 15
        Me.cbxGrupo.Texto = ""
        '
        'frmListadoValeGasto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(304, 226)
        Me.Controls.Add(Me.chkGrupo)
        Me.Controls.Add(Me.cbxGrupo)
        Me.Controls.Add(Me.lblTipoInforme)
        Me.Controls.Add(Me.cbxTipo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtHasta)
        Me.Controls.Add(Me.txtDesde)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoValeGasto"
        Me.Tag = "frmListadoValeGasto"
        Me.Text = "frmListadoValeGasto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTipoInforme As System.Windows.Forms.Label
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkGrupo As ERP.ocxCHK
    Friend WithEvents cbxGrupo As ERP.ocxCBX
End Class

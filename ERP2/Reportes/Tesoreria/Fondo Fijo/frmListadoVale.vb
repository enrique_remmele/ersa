﻿Imports ERP.Reporte

Public Class frmListadoVale

    'CLASES
    Dim CReporte As New CReporteFondoFijo
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE VALES"

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()

        CargarInformacion()


    End Sub

    Sub CargarInformacion()

        cbxTipo.cbx.Items.Add("TODOS")
        cbxTipo.cbx.Items.Add("VALES A RENDIR")
        cbxTipo.cbx.Items.Add("VALES A RENDIR PENDIENTES")
        cbxTipo.cbx.Items.Add("VALES SIN RENDIR")

        cbxTipo.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim TipoInforme As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And Anulado='False' "

        '29062021 - SM - Agregar nuevo filtro de Sucursal
        If chkSucursal.chk.Checked = True Then
            Where = Where & " And IDSucursal=" & cbxSucursal.GetValue
        End If

        Select Case cbxTipo.cbx.SelectedIndex
            Case 0
                Titulo = "LISTADO DE TODOS LOS VALES"
            Case 1
                Titulo = "LISTADO DE VALES A RENDIR"
                Where = Where & " And ARendir='True'"
            Case 2
                Titulo = "LISTADO DE VALES A RENDIR PENDIENTES"
                Where = Where & " And ARendir='True' And Cancelado='False'"
            Case 3
                Titulo = "LISTADO DE VALES SIN RENDICION"
                Where = Where & " And ARendir='False'"
        End Select

        '29062021 - SM - Agregar nuevo filtro de Sucursal
        If chkSucursal.Valor = True Then
            TipoInforme = "PERIODO: " & txtDesde.GetValue & " - " & txtHasta.GetValue & "  -  SUCURSAL: " & cbxSucursal.cbx.Text
        Else
            TipoInforme = "PERIODO: " & txtDesde.GetValue & " - " & txtHasta.GetValue
        End If

        CReporte.ListadoVales(frm, Titulo, TipoInforme, Where, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)

    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoVale_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

End Class




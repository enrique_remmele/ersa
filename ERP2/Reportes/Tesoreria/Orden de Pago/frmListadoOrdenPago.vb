﻿Imports ERP.Reporte

Public Class frmListadoOrdenPago

    'CLASES
    Dim CReporte As New CReporteOrdenPago
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE ORDENES DE PAGOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()
        CambiarOrdenacion()

        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipo de OP
        cbxTipoOP.cbx.Items.Add("PAGOS A PROVEEDORES")
        cbxTipoOP.cbx.Items.Add("ANTICIPO")
        cbxTipoOP.cbx.Items.Add("EGRESO A RENDIR")
        cbxTipoOP.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoOP.cbx.SelectedIndex = 0

        'Tipo de saldo
        cbxTipoSaldo.cbx.Items.Add("CON SALDO")
        cbxTipoSaldo.cbx.Items.Add("SIN SALDO")
        cbxTipoSaldo.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoSaldo.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.name.ToString = "cbxUnidadNegocio" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        If chkUnidadNegocio.chk.Checked Then
            Where = Where & " And IDTransaccion in (Select Distinct IDTransaccion from vDetalleAsiento where IDUnidadNegocioActualCuenta = " & cbxUnidadNegocio.cbx.SelectedValue & ") "
        End If

        'Se agrega un filtro por Tipo de Proveedor 22032023
        If chkTipoProveedor.chk.Checked Then
            Where = Where & " and IDTipoProveedor = " & cbxTipoProveedor.cbx.SelectedValue
        End If


        TipoInforme = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text

        'Establecemos el filtro TipoOP
        If cbxTipoOP.cbx.Text <> "" And chkTipoOP.Valor Then
            Select Case cbxTipoOP.cbx.Text
                Case "PAGOS A PROVEEDORES"
                    Where += " AND Tipo = 'Pago Proveedor' "
                    TipoInforme += " - Tipo OP: Pago Proveedor"
                Case "ANTICIPO"
                    Where += " AND Tipo = 'Anticipo Proveedor' "
                    TipoInforme += " - Tipo OP: Anticipo Proveedor"
                Case "EGRESO A RENDIR"
                    Where += " AND Tipo = 'Egreso a Rendir' "
                    TipoInforme += " - Tipo OP: Egreso a Rendir"
            End Select
        End If

        'Establecemos el filtro TipoSaldo
        If cbxTipoSaldo.cbx.Text <> "" And chkTipoSaldo.Valor Then
            Select Case cbxTipoSaldo.cbx.Text
                Case "CON SALDO"
                    Where += " AND Saldo > 0 "
                    TipoInforme += " - Tipo Saldo: Con Saldo"
                Case "SIN SALDO"
                    Where += " AND Saldo <= 0  "
                    TipoInforme += " - Tipo Saldo: Sin Saldo"
            End Select

        End If

        'Establecemos el filtro para no traer anulados
        Where += " AND Anulado = 'FALSE' "

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        If chkSucursal.Valor = True Then
            TipoInforme = TipoInforme & " - " & cbxSucursal.cbx.Text
        End If

        'Tipo de Proveedor 22032023
        If chkTipoProveedor.Valor = True Then
            TipoInforme = TipoInforme & " - " & cbxTipoProveedor.cbx.Text
        End If

        CReporte.ListadoOrdenPago(frm, Titulo, Where, OrderBy, Top, TipoInforme)


    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VOrdenPago ", "Select Top(0) Sucursal,Proveedor,Fecha,NroComprobante From VOrdenPago")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoOrdenPago_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCuenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
    End Sub

    Private Sub chkTipoOP_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoOP.PropertyChanged
        cbxTipoOP.Enabled = value
    End Sub

    Private Sub cbxTipoSaldo_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxTipoSaldo.PropertyChanged

    End Sub

    Private Sub chkTipoSaldo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoSaldo.PropertyChanged
        cbxTipoSaldo.Enabled = value
    End Sub

    Private Sub chkUnidadNegocio_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkUnidadNegocio.PropertyChanged
        cbxUnidadNegocio.Enabled = value
    End Sub

    Private Sub chkTipoProveedor_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProveedor.PropertyChanged
        cbxTipoProveedor.Enabled = value
    End Sub
End Class




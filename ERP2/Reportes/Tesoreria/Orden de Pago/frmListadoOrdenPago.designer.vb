﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoOrdenPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.cbxTipoProveedor = New ERP.ocxCBX()
        Me.chkTipoProveedor = New ERP.ocxCHK()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.chkUnidadNegocio = New ERP.ocxCHK()
        Me.cbxTipoSaldo = New ERP.ocxCBX()
        Me.chkTipoSaldo = New ERP.ocxCHK()
        Me.cbxTipoOP = New ERP.ocxCBX()
        Me.chkTipoOP = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.chkUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoSaldo)
        Me.gbxFiltro.Controls.Add(Me.chkTipoSaldo)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoOP)
        Me.gbxFiltro.Controls.Add(Me.chkTipoOP)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(533, 212)
        Me.gbxFiltro.TabIndex = 2
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(737, 230)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 4
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(556, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 212)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 62)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(556, 230)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(138, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 79)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 79)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 46, 4, 962)
        Me.txtHasta.Location = New System.Drawing.Point(89, 38)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 46, 4, 962)
        Me.txtDesde.Location = New System.Drawing.Point(6, 38)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'cbxTipoProveedor
        '
        Me.cbxTipoProveedor.CampoWhere = ""
        Me.cbxTipoProveedor.CargarUnaSolaVez = False
        Me.cbxTipoProveedor.DataDisplayMember = "Descripcion"
        Me.cbxTipoProveedor.DataFilter = Nothing
        Me.cbxTipoProveedor.DataOrderBy = Nothing
        Me.cbxTipoProveedor.DataSource = "TipoProveedor"
        Me.cbxTipoProveedor.DataValueMember = "ID"
        Me.cbxTipoProveedor.dtSeleccionado = Nothing
        Me.cbxTipoProveedor.Enabled = False
        Me.cbxTipoProveedor.FormABM = Nothing
        Me.cbxTipoProveedor.Indicaciones = Nothing
        Me.cbxTipoProveedor.Location = New System.Drawing.Point(153, 182)
        Me.cbxTipoProveedor.Name = "cbxTipoProveedor"
        Me.cbxTipoProveedor.SeleccionMultiple = False
        Me.cbxTipoProveedor.SeleccionObligatoria = False
        Me.cbxTipoProveedor.Size = New System.Drawing.Size(354, 23)
        Me.cbxTipoProveedor.SoloLectura = False
        Me.cbxTipoProveedor.TabIndex = 11
        Me.cbxTipoProveedor.Texto = ""
        '
        'chkTipoProveedor
        '
        Me.chkTipoProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProveedor.Color = System.Drawing.Color.Empty
        Me.chkTipoProveedor.Location = New System.Drawing.Point(15, 184)
        Me.chkTipoProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProveedor.Name = "chkTipoProveedor"
        Me.chkTipoProveedor.Size = New System.Drawing.Size(97, 21)
        Me.chkTipoProveedor.SoloLectura = False
        Me.chkTipoProveedor.TabIndex = 10
        Me.chkTipoProveedor.Texto = "Tipo Proveedor:"
        Me.chkTipoProveedor.Valor = False
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(15, 67)
        Me.chkProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(74, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 2
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = "TipoSaldo"
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = "Descripcion"
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = "UnidadNegocio"
        Me.cbxUnidadNegocio.DataValueMember = "ID"
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.Enabled = False
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(153, 153)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(354, 23)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 9
        Me.cbxUnidadNegocio.Texto = ""
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(15, 155)
        Me.chkUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(97, 21)
        Me.chkUnidadNegocio.SoloLectura = False
        Me.chkUnidadNegocio.TabIndex = 8
        Me.chkUnidadNegocio.Texto = "Unid. Negocio:"
        Me.chkUnidadNegocio.Valor = False
        '
        'cbxTipoSaldo
        '
        Me.cbxTipoSaldo.CampoWhere = "TipoSaldo"
        Me.cbxTipoSaldo.CargarUnaSolaVez = False
        Me.cbxTipoSaldo.DataDisplayMember = "Text"
        Me.cbxTipoSaldo.DataFilter = Nothing
        Me.cbxTipoSaldo.DataOrderBy = Nothing
        Me.cbxTipoSaldo.DataSource = Nothing
        Me.cbxTipoSaldo.DataValueMember = "Value"
        Me.cbxTipoSaldo.dtSeleccionado = Nothing
        Me.cbxTipoSaldo.Enabled = False
        Me.cbxTipoSaldo.FormABM = Nothing
        Me.cbxTipoSaldo.Indicaciones = Nothing
        Me.cbxTipoSaldo.Location = New System.Drawing.Point(153, 124)
        Me.cbxTipoSaldo.Name = "cbxTipoSaldo"
        Me.cbxTipoSaldo.SeleccionMultiple = False
        Me.cbxTipoSaldo.SeleccionObligatoria = False
        Me.cbxTipoSaldo.Size = New System.Drawing.Size(354, 23)
        Me.cbxTipoSaldo.SoloLectura = False
        Me.cbxTipoSaldo.TabIndex = 7
        Me.cbxTipoSaldo.Texto = ""
        '
        'chkTipoSaldo
        '
        Me.chkTipoSaldo.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoSaldo.Color = System.Drawing.Color.Empty
        Me.chkTipoSaldo.Location = New System.Drawing.Point(15, 126)
        Me.chkTipoSaldo.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoSaldo.Name = "chkTipoSaldo"
        Me.chkTipoSaldo.Size = New System.Drawing.Size(74, 21)
        Me.chkTipoSaldo.SoloLectura = False
        Me.chkTipoSaldo.TabIndex = 6
        Me.chkTipoSaldo.Texto = "Tipo saldo:"
        Me.chkTipoSaldo.Valor = False
        '
        'cbxTipoOP
        '
        Me.cbxTipoOP.CampoWhere = ""
        Me.cbxTipoOP.CargarUnaSolaVez = False
        Me.cbxTipoOP.DataDisplayMember = ""
        Me.cbxTipoOP.DataFilter = Nothing
        Me.cbxTipoOP.DataOrderBy = Nothing
        Me.cbxTipoOP.DataSource = Nothing
        Me.cbxTipoOP.DataValueMember = ""
        Me.cbxTipoOP.dtSeleccionado = Nothing
        Me.cbxTipoOP.Enabled = False
        Me.cbxTipoOP.FormABM = Nothing
        Me.cbxTipoOP.Indicaciones = Nothing
        Me.cbxTipoOP.Location = New System.Drawing.Point(153, 95)
        Me.cbxTipoOP.Name = "cbxTipoOP"
        Me.cbxTipoOP.SeleccionMultiple = False
        Me.cbxTipoOP.SeleccionObligatoria = False
        Me.cbxTipoOP.Size = New System.Drawing.Size(354, 23)
        Me.cbxTipoOP.SoloLectura = False
        Me.cbxTipoOP.TabIndex = 5
        Me.cbxTipoOP.Texto = ""
        '
        'chkTipoOP
        '
        Me.chkTipoOP.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoOP.Color = System.Drawing.Color.Empty
        Me.chkTipoOP.Location = New System.Drawing.Point(15, 97)
        Me.chkTipoOP.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoOP.Name = "chkTipoOP"
        Me.chkTipoOP.Size = New System.Drawing.Size(74, 21)
        Me.chkTipoOP.SoloLectura = False
        Me.chkTipoOP.TabIndex = 4
        Me.chkTipoOP.Texto = "Tipo OP:"
        Me.chkTipoOP.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = "RazonSocial"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(153, 67)
        Me.cbxProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = True
        Me.cbxProveedor.Size = New System.Drawing.Size(354, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 3
        Me.cbxProveedor.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(15, 38)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(68, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(153, 38)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(354, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'frmListadoOrdenPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(812, 275)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoOrdenPago"
        Me.Text = "Listado Ordenes de Pagos"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents cbxTipoOP As ERP.ocxCBX
    Friend WithEvents chkTipoOP As ERP.ocxCHK
    Friend WithEvents cbxTipoSaldo As ERP.ocxCBX
    Friend WithEvents chkTipoSaldo As ERP.ocxCHK
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents chkUnidadNegocio As ocxCHK
    Friend WithEvents cbxTipoProveedor As ocxCBX
    Friend WithEvents chkTipoProveedor As ocxCHK
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoDepositoBancario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkValores = New ERP.ocxCHK()
        Me.cbxValores = New ERP.ocxCBX()
        Me.chkCuenta = New ERP.ocxCHK()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.chkCarga = New ERP.ocxCHK()
        Me.chkOperacion = New ERP.ocxCHK()
        Me.txtHastaCarga = New ERP.ocxTXTDate()
        Me.txtDesdeCarga = New ERP.ocxTXTDate()
        Me.cbxInforme = New ERP.ocxCBX()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(300, 274)
        Me.gbxFiltro.TabIndex = 2
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(382, 283)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkCarga)
        Me.GroupBox2.Controls.Add(Me.chkOperacion)
        Me.GroupBox2.Controls.Add(Me.txtHastaCarga)
        Me.GroupBox2.Controls.Add(Me.txtDesdeCarga)
        Me.GroupBox2.Controls.Add(Me.cbxInforme)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxMoneda)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(309, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(260, 274)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 142)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Informe:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 100)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Moneda:"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(8, 186)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 7
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(6, 247)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 11
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 15)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 231)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 10
        Me.lblRanking.Text = "Ranking:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(135, 283)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkValores
        '
        Me.chkValores.BackColor = System.Drawing.Color.Transparent
        Me.chkValores.Color = System.Drawing.Color.Empty
        Me.chkValores.Location = New System.Drawing.Point(12, 108)
        Me.chkValores.Name = "chkValores"
        Me.chkValores.Size = New System.Drawing.Size(68, 21)
        Me.chkValores.SoloLectura = False
        Me.chkValores.TabIndex = 5
        Me.chkValores.Texto = "Valores:"
        Me.chkValores.Valor = False
        '
        'cbxValores
        '
        Me.cbxValores.CampoWhere = ""
        Me.cbxValores.CargarUnaSolaVez = False
        Me.cbxValores.DataDisplayMember = ""
        Me.cbxValores.DataFilter = Nothing
        Me.cbxValores.DataOrderBy = Nothing
        Me.cbxValores.DataSource = ""
        Me.cbxValores.DataValueMember = ""
        Me.cbxValores.Enabled = False
        Me.cbxValores.FormABM = Nothing
        Me.cbxValores.Indicaciones = Nothing
        Me.cbxValores.Location = New System.Drawing.Point(85, 109)
        Me.cbxValores.Name = "cbxValores"
        Me.cbxValores.SeleccionObligatoria = True
        Me.cbxValores.Size = New System.Drawing.Size(199, 21)
        Me.cbxValores.SoloLectura = False
        Me.cbxValores.TabIndex = 6
        Me.cbxValores.Texto = ""
        '
        'chkCuenta
        '
        Me.chkCuenta.BackColor = System.Drawing.Color.Transparent
        Me.chkCuenta.Color = System.Drawing.Color.Empty
        Me.chkCuenta.Location = New System.Drawing.Point(12, 71)
        Me.chkCuenta.Name = "chkCuenta"
        Me.chkCuenta.Size = New System.Drawing.Size(68, 21)
        Me.chkCuenta.SoloLectura = False
        Me.chkCuenta.TabIndex = 3
        Me.chkCuenta.Texto = "Cuenta:"
        Me.chkCuenta.Valor = False
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = "IDCuentaBancaria"
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = "Descripcion"
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = "vCuentaBancaria"
        Me.cbxCuenta.DataValueMember = "ID"
        Me.cbxCuenta.Enabled = False
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(86, 72)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionObligatoria = True
        Me.cbxCuenta.Size = New System.Drawing.Size(199, 21)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 4
        Me.cbxCuenta.Texto = ""
        '
        'chkCarga
        '
        Me.chkCarga.BackColor = System.Drawing.Color.Transparent
        Me.chkCarga.Color = System.Drawing.Color.Empty
        Me.chkCarga.Location = New System.Drawing.Point(5, 61)
        Me.chkCarga.Name = "chkCarga"
        Me.chkCarga.Size = New System.Drawing.Size(87, 21)
        Me.chkCarga.SoloLectura = False
        Me.chkCarga.TabIndex = 19
        Me.chkCarga.Texto = "Carga"
        Me.chkCarga.Valor = False
        '
        'chkOperacion
        '
        Me.chkOperacion.BackColor = System.Drawing.Color.Transparent
        Me.chkOperacion.Color = System.Drawing.Color.Empty
        Me.chkOperacion.Location = New System.Drawing.Point(5, 34)
        Me.chkOperacion.Name = "chkOperacion"
        Me.chkOperacion.Size = New System.Drawing.Size(87, 21)
        Me.chkOperacion.SoloLectura = False
        Me.chkOperacion.TabIndex = 18
        Me.chkOperacion.Texto = "Operacion"
        Me.chkOperacion.Valor = True
        '
        'txtHastaCarga
        '
        Me.txtHastaCarga.Color = System.Drawing.Color.Empty
        Me.txtHastaCarga.Fecha = New Date(2013, 5, 31, 16, 45, 5, 358)
        Me.txtHastaCarga.Location = New System.Drawing.Point(180, 60)
        Me.txtHastaCarga.Name = "txtHastaCarga"
        Me.txtHastaCarga.PermitirNulo = False
        Me.txtHastaCarga.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaCarga.SoloLectura = False
        Me.txtHastaCarga.TabIndex = 13
        '
        'txtDesdeCarga
        '
        Me.txtDesdeCarga.Color = System.Drawing.Color.Empty
        Me.txtDesdeCarga.Fecha = New Date(2013, 5, 31, 16, 45, 5, 358)
        Me.txtDesdeCarga.Location = New System.Drawing.Point(97, 60)
        Me.txtDesdeCarga.Name = "txtDesdeCarga"
        Me.txtDesdeCarga.PermitirNulo = False
        Me.txtDesdeCarga.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeCarga.SoloLectura = False
        Me.txtDesdeCarga.TabIndex = 12
        '
        'cbxInforme
        '
        Me.cbxInforme.CampoWhere = Nothing
        Me.cbxInforme.CargarUnaSolaVez = False
        Me.cbxInforme.DataDisplayMember = Nothing
        Me.cbxInforme.DataFilter = Nothing
        Me.cbxInforme.DataOrderBy = Nothing
        Me.cbxInforme.DataSource = Nothing
        Me.cbxInforme.DataValueMember = Nothing
        Me.cbxInforme.FormABM = Nothing
        Me.cbxInforme.Indicaciones = Nothing
        Me.cbxInforme.Location = New System.Drawing.Point(6, 158)
        Me.cbxInforme.Name = "cbxInforme"
        Me.cbxInforme.SeleccionObligatoria = True
        Me.cbxInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxInforme.SoloLectura = False
        Me.cbxInforme.TabIndex = 6
        Me.cbxInforme.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "Moneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = "Moneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(6, 116)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(157, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 4
        Me.cbxMoneda.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(165, 203)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 9
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 203)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 8
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 45, 5, 358)
        Me.txtHasta.Location = New System.Drawing.Point(180, 34)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 45, 5, 358)
        Me.txtDesde.Location = New System.Drawing.Point(97, 34)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 32)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(68, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(83, 33)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(199, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'frmListadoDepositoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 317)
        Me.Controls.Add(Me.chkValores)
        Me.Controls.Add(Me.cbxValores)
        Me.Controls.Add(Me.chkCuenta)
        Me.Controls.Add(Me.cbxCuenta)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoDepositoBancario"
        Me.Text = "Listado Depósito Bancario"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkCuenta As ERP.ocxCHK
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents cbxInforme As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkValores As ERP.ocxCHK
    Friend WithEvents cbxValores As ERP.ocxCBX
    Friend WithEvents txtHastaCarga As ERP.ocxTXTDate
    Friend WithEvents txtDesdeCarga As ERP.ocxTXTDate
    Friend WithEvents chkCarga As ERP.ocxCHK
    Friend WithEvents chkOperacion As ERP.ocxCHK
End Class

﻿Imports ERP.Reporte

Public Class frmListadoDepositoBancario

    'CLASES
    Dim CReporte As New CReporteBanco
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim dtBanco As DataTable

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE DEPOSITOS BANCARIOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()
        txtDesde.Ayer()
        txtDesdeCarga.Ayer()
        txtHasta.Hoy()
        txtHastaCarga.Hoy()

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        'Informe
        cbxInforme.cbx.Items.Add("Resumido")
        cbxInforme.cbx.Items.Add("Detallado")
        cbxInforme.cbx.SelectedIndex = 0

        'Valores
        cbxValores.cbx.Items.Add("Efectivo")
        cbxValores.cbx.Items.Add("Cheques al Día")
        cbxValores.cbx.Items.Add("Cheques Diferidos")
        cbxValores.cbx.Items.Add("Cheques devueltos - Redep.")
        cbxValores.cbx.Items.Add("Cheques en Custodia")

        dtBanco = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria")

        cbxMoneda.cbx.SelectedIndex = 0


    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim Banco As String = ""
        Dim Titulo As String = ""
        Dim TipoInforme As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where 1 = 1 "
        If chkOperacion.Valor = True Then
            Where = Where & " and Fecha Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "
        End If
        If chkCarga.Valor = True Then
            Where = Where & " And cast(fechatransaccion as date) Between '" & CSistema.FormatoFechaBaseDatos(txtDesdeCarga.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHastaCarga.txt.Text, True, False) & "' "
        End If

        Where = Where & " And IDMoneda=" & cbxMoneda.GetValue

        Titulo = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text

        If chkCuenta.Valor = True Then
            Where = Where & " And IDCuentaBancaria = " & cbxCuenta.GetValue
            For Each oRow As DataRow In dtBanco.Select("ID=" & cbxCuenta.GetValue)
                Banco = oRow("Referencia").ToString
            Next
            TipoInforme = Banco & " - " & cbxCuenta.cbx.Text & " - " & cbxMoneda.cbx.Text
        End If

        If chkSucursal.Valor = True Then
            Titulo = Titulo & " - " & cbxSucursal.cbx.Text
            Where = Where & " And IDSucursal=" & cbxSucursal.GetValue
        End If

        If chkValores.Valor = True Then

            Select Case cbxValores.cbx.SelectedIndex
                Case Is = 0
                    Where = Where & " And Tipo='EFE'"
                Case Is = 1
                    Where = Where & " And Tipo='CHQ'"
                Case Is = 2
                    Where = Where & " And Tipo='DIF'"
                Case Is = 3
                    Where = Where & " And Rechazado='True'"
                Case Is = 4
                    Where = Where & " And Cartera='True'"
            End Select

        End If


        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            'OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
            OrderBy = "Order by " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If



        'Seleccionar Informe
        Select Case cbxInforme.cbx.SelectedIndex
            Case Is = 0
                Titulo = "DEPOSITOS BANCARIOS"
                TipoInforme = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString
                CReporte.ListadoDepositoBancario(frm, Titulo, Where, OrderBy, Top, vgUsuarioIdentificador, TipoInforme)
            Case Is = 1
                CReporte.ListadoDepositoBancarioDetalle(frm, Titulo, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, TipoInforme)
        End Select



    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VDepositoBancario ", "Select Top(0) Numero,Fecha,Sucursal,NroComprobante From VDepositoBancario")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next
        cbxOrdenadoPor.cbx.SelectedIndex = 0
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoDepositoBancario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCuenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCuenta.PropertyChanged
        cbxCuenta.Enabled = value
    End Sub

    Private Sub chkValores_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkValores.PropertyChanged
        If cbxInforme.cbx.SelectedIndex = 1 Then
            cbxValores.Enabled = value
        End If
    End Sub
End Class




﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoChequesEmitidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtFechaSinCobro = New ERP.ocxTXTDate()
        Me.chkNoCobradosAl = New ERP.ocxCHK()
        Me.txtFechaCobroChequeHasta = New ERP.ocxTXTDate()
        Me.chkFechaCobroCheque = New ERP.ocxCHK()
        Me.txtFechaCobroChequeDesde = New ERP.ocxTXTDate()
        Me.txtVencimientoHasta = New ERP.ocxTXTDate()
        Me.txtVencimientoDesde = New ERP.ocxTXTDate()
        Me.txtFechaSinEntrega = New ERP.ocxTXTDate()
        Me.chkNoEntregadosAl = New ERP.ocxCHK()
        Me.txtFechaEntregaHasta = New ERP.ocxTXTDate()
        Me.chkFechaEntrega = New ERP.ocxCHK()
        Me.txtFechaEntregaDesde = New ERP.ocxTXTDate()
        Me.chkVencimiento = New ERP.ocxCHK()
        Me.chkEmision = New ERP.ocxCHK()
        Me.cbxAgrupado = New ERP.ocxCBX()
        Me.txtFechaOPHasta = New ERP.ocxTXTDate()
        Me.chkFechaPago = New ERP.ocxCHK()
        Me.txtFechaOPDesde = New ERP.ocxTXTDate()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.lblBanco = New System.Windows.Forms.Label()
        Me.txtMoneda = New ERP.ocxTXTString()
        Me.txtBanco = New ERP.ocxTXTString()
        Me.chkCuentaBancaria = New ERP.ocxCHK()
        Me.cbxCuentaBancaria = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkVerOpAnuladas = New ERP.ocxCHK()
        Me.chkTipo = New ERP.ocxCHK()
        Me.cbxtipo = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtFechaSinCobro)
        Me.GroupBox2.Controls.Add(Me.chkNoCobradosAl)
        Me.GroupBox2.Controls.Add(Me.txtFechaCobroChequeHasta)
        Me.GroupBox2.Controls.Add(Me.chkFechaCobroCheque)
        Me.GroupBox2.Controls.Add(Me.txtFechaCobroChequeDesde)
        Me.GroupBox2.Controls.Add(Me.txtVencimientoHasta)
        Me.GroupBox2.Controls.Add(Me.txtVencimientoDesde)
        Me.GroupBox2.Controls.Add(Me.txtFechaSinEntrega)
        Me.GroupBox2.Controls.Add(Me.chkNoEntregadosAl)
        Me.GroupBox2.Controls.Add(Me.txtFechaEntregaHasta)
        Me.GroupBox2.Controls.Add(Me.chkFechaEntrega)
        Me.GroupBox2.Controls.Add(Me.txtFechaEntregaDesde)
        Me.GroupBox2.Controls.Add(Me.chkVencimiento)
        Me.GroupBox2.Controls.Add(Me.chkEmision)
        Me.GroupBox2.Controls.Add(Me.cbxAgrupado)
        Me.GroupBox2.Controls.Add(Me.txtFechaOPHasta)
        Me.GroupBox2.Controls.Add(Me.chkFechaPago)
        Me.GroupBox2.Controls.Add(Me.txtFechaOPDesde)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(366, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(362, 284)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtFechaSinCobro
        '
        Me.txtFechaSinCobro.AñoFecha = 0
        Me.txtFechaSinCobro.Color = System.Drawing.Color.Empty
        Me.txtFechaSinCobro.Enabled = False
        Me.txtFechaSinCobro.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaSinCobro.Location = New System.Drawing.Point(194, 182)
        Me.txtFechaSinCobro.MesFecha = 0
        Me.txtFechaSinCobro.Name = "txtFechaSinCobro"
        Me.txtFechaSinCobro.PermitirNulo = False
        Me.txtFechaSinCobro.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaSinCobro.SoloLectura = False
        Me.txtFechaSinCobro.TabIndex = 31
        '
        'chkNoCobradosAl
        '
        Me.chkNoCobradosAl.BackColor = System.Drawing.Color.Transparent
        Me.chkNoCobradosAl.Color = System.Drawing.Color.Empty
        Me.chkNoCobradosAl.Location = New System.Drawing.Point(52, 181)
        Me.chkNoCobradosAl.Name = "chkNoCobradosAl"
        Me.chkNoCobradosAl.Size = New System.Drawing.Size(136, 21)
        Me.chkNoCobradosAl.SoloLectura = False
        Me.chkNoCobradosAl.TabIndex = 30
        Me.chkNoCobradosAl.Texto = "No Cobrados a Fecha:"
        Me.chkNoCobradosAl.Valor = False
        '
        'txtFechaCobroChequeHasta
        '
        Me.txtFechaCobroChequeHasta.AñoFecha = 0
        Me.txtFechaCobroChequeHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaCobroChequeHasta.Enabled = False
        Me.txtFechaCobroChequeHasta.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaCobroChequeHasta.Location = New System.Drawing.Point(276, 125)
        Me.txtFechaCobroChequeHasta.MesFecha = 0
        Me.txtFechaCobroChequeHasta.Name = "txtFechaCobroChequeHasta"
        Me.txtFechaCobroChequeHasta.PermitirNulo = False
        Me.txtFechaCobroChequeHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaCobroChequeHasta.SoloLectura = False
        Me.txtFechaCobroChequeHasta.TabIndex = 29
        '
        'chkFechaCobroCheque
        '
        Me.chkFechaCobroCheque.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaCobroCheque.Color = System.Drawing.Color.Empty
        Me.chkFechaCobroCheque.Location = New System.Drawing.Point(52, 124)
        Me.chkFechaCobroCheque.Name = "chkFechaCobroCheque"
        Me.chkFechaCobroCheque.Size = New System.Drawing.Size(136, 21)
        Me.chkFechaCobroCheque.SoloLectura = False
        Me.chkFechaCobroCheque.TabIndex = 27
        Me.chkFechaCobroCheque.Texto = "Fecha Cobro Cheque:"
        Me.chkFechaCobroCheque.Valor = False
        '
        'txtFechaCobroChequeDesde
        '
        Me.txtFechaCobroChequeDesde.AñoFecha = 0
        Me.txtFechaCobroChequeDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaCobroChequeDesde.Enabled = False
        Me.txtFechaCobroChequeDesde.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaCobroChequeDesde.Location = New System.Drawing.Point(194, 125)
        Me.txtFechaCobroChequeDesde.MesFecha = 0
        Me.txtFechaCobroChequeDesde.Name = "txtFechaCobroChequeDesde"
        Me.txtFechaCobroChequeDesde.PermitirNulo = False
        Me.txtFechaCobroChequeDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaCobroChequeDesde.SoloLectura = False
        Me.txtFechaCobroChequeDesde.TabIndex = 28
        '
        'txtVencimientoHasta
        '
        Me.txtVencimientoHasta.AñoFecha = 0
        Me.txtVencimientoHasta.Color = System.Drawing.Color.Empty
        Me.txtVencimientoHasta.Enabled = False
        Me.txtVencimientoHasta.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtVencimientoHasta.Location = New System.Drawing.Point(276, 49)
        Me.txtVencimientoHasta.MesFecha = 0
        Me.txtVencimientoHasta.Name = "txtVencimientoHasta"
        Me.txtVencimientoHasta.PermitirNulo = False
        Me.txtVencimientoHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtVencimientoHasta.SoloLectura = False
        Me.txtVencimientoHasta.TabIndex = 26
        '
        'txtVencimientoDesde
        '
        Me.txtVencimientoDesde.AñoFecha = 0
        Me.txtVencimientoDesde.Color = System.Drawing.Color.Empty
        Me.txtVencimientoDesde.Enabled = False
        Me.txtVencimientoDesde.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtVencimientoDesde.Location = New System.Drawing.Point(194, 49)
        Me.txtVencimientoDesde.MesFecha = 0
        Me.txtVencimientoDesde.Name = "txtVencimientoDesde"
        Me.txtVencimientoDesde.PermitirNulo = False
        Me.txtVencimientoDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtVencimientoDesde.SoloLectura = False
        Me.txtVencimientoDesde.TabIndex = 25
        '
        'txtFechaSinEntrega
        '
        Me.txtFechaSinEntrega.AñoFecha = 0
        Me.txtFechaSinEntrega.Color = System.Drawing.Color.Empty
        Me.txtFechaSinEntrega.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaSinEntrega.Location = New System.Drawing.Point(194, 155)
        Me.txtFechaSinEntrega.MesFecha = 0
        Me.txtFechaSinEntrega.Name = "txtFechaSinEntrega"
        Me.txtFechaSinEntrega.PermitirNulo = False
        Me.txtFechaSinEntrega.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaSinEntrega.SoloLectura = False
        Me.txtFechaSinEntrega.TabIndex = 24
        '
        'chkNoEntregadosAl
        '
        Me.chkNoEntregadosAl.BackColor = System.Drawing.Color.Transparent
        Me.chkNoEntregadosAl.Color = System.Drawing.Color.Empty
        Me.chkNoEntregadosAl.Location = New System.Drawing.Point(52, 154)
        Me.chkNoEntregadosAl.Name = "chkNoEntregadosAl"
        Me.chkNoEntregadosAl.Size = New System.Drawing.Size(136, 21)
        Me.chkNoEntregadosAl.SoloLectura = False
        Me.chkNoEntregadosAl.TabIndex = 23
        Me.chkNoEntregadosAl.Texto = "No entregados a Fecha:"
        Me.chkNoEntregadosAl.Valor = True
        '
        'txtFechaEntregaHasta
        '
        Me.txtFechaEntregaHasta.AñoFecha = 0
        Me.txtFechaEntregaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaEntregaHasta.Enabled = False
        Me.txtFechaEntregaHasta.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaEntregaHasta.Location = New System.Drawing.Point(276, 99)
        Me.txtFechaEntregaHasta.MesFecha = 0
        Me.txtFechaEntregaHasta.Name = "txtFechaEntregaHasta"
        Me.txtFechaEntregaHasta.PermitirNulo = False
        Me.txtFechaEntregaHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaEntregaHasta.SoloLectura = False
        Me.txtFechaEntregaHasta.TabIndex = 22
        '
        'chkFechaEntrega
        '
        Me.chkFechaEntrega.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaEntrega.Color = System.Drawing.Color.Empty
        Me.chkFechaEntrega.Location = New System.Drawing.Point(52, 98)
        Me.chkFechaEntrega.Name = "chkFechaEntrega"
        Me.chkFechaEntrega.Size = New System.Drawing.Size(136, 21)
        Me.chkFechaEntrega.SoloLectura = False
        Me.chkFechaEntrega.TabIndex = 20
        Me.chkFechaEntrega.Texto = "Fecha Entrega Cheque:"
        Me.chkFechaEntrega.Valor = False
        '
        'txtFechaEntregaDesde
        '
        Me.txtFechaEntregaDesde.AñoFecha = 0
        Me.txtFechaEntregaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaEntregaDesde.Enabled = False
        Me.txtFechaEntregaDesde.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaEntregaDesde.Location = New System.Drawing.Point(194, 99)
        Me.txtFechaEntregaDesde.MesFecha = 0
        Me.txtFechaEntregaDesde.Name = "txtFechaEntregaDesde"
        Me.txtFechaEntregaDesde.PermitirNulo = False
        Me.txtFechaEntregaDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaEntregaDesde.SoloLectura = False
        Me.txtFechaEntregaDesde.TabIndex = 21
        '
        'chkVencimiento
        '
        Me.chkVencimiento.BackColor = System.Drawing.Color.Transparent
        Me.chkVencimiento.Color = System.Drawing.Color.Empty
        Me.chkVencimiento.Location = New System.Drawing.Point(52, 48)
        Me.chkVencimiento.Name = "chkVencimiento"
        Me.chkVencimiento.Size = New System.Drawing.Size(118, 21)
        Me.chkVencimiento.SoloLectura = False
        Me.chkVencimiento.TabIndex = 19
        Me.chkVencimiento.Texto = "Fecha Vencimiento:"
        Me.chkVencimiento.Valor = False
        '
        'chkEmision
        '
        Me.chkEmision.BackColor = System.Drawing.Color.Transparent
        Me.chkEmision.Color = System.Drawing.Color.Empty
        Me.chkEmision.Location = New System.Drawing.Point(52, 23)
        Me.chkEmision.Name = "chkEmision"
        Me.chkEmision.Size = New System.Drawing.Size(106, 21)
        Me.chkEmision.SoloLectura = False
        Me.chkEmision.TabIndex = 18
        Me.chkEmision.Texto = "Fecha Emision:"
        Me.chkEmision.Valor = True
        '
        'cbxAgrupado
        '
        Me.cbxAgrupado.CampoWhere = Nothing
        Me.cbxAgrupado.CargarUnaSolaVez = False
        Me.cbxAgrupado.DataDisplayMember = Nothing
        Me.cbxAgrupado.DataFilter = Nothing
        Me.cbxAgrupado.DataOrderBy = Nothing
        Me.cbxAgrupado.DataSource = Nothing
        Me.cbxAgrupado.DataValueMember = Nothing
        Me.cbxAgrupado.dtSeleccionado = Nothing
        Me.cbxAgrupado.FormABM = Nothing
        Me.cbxAgrupado.Indicaciones = Nothing
        Me.cbxAgrupado.Location = New System.Drawing.Point(114, 214)
        Me.cbxAgrupado.Name = "cbxAgrupado"
        Me.cbxAgrupado.SeleccionMultiple = False
        Me.cbxAgrupado.SeleccionObligatoria = False
        Me.cbxAgrupado.Size = New System.Drawing.Size(157, 21)
        Me.cbxAgrupado.SoloLectura = False
        Me.cbxAgrupado.TabIndex = 16
        Me.cbxAgrupado.Texto = ""
        '
        'txtFechaOPHasta
        '
        Me.txtFechaOPHasta.AñoFecha = 0
        Me.txtFechaOPHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaOPHasta.Enabled = False
        Me.txtFechaOPHasta.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaOPHasta.Location = New System.Drawing.Point(276, 74)
        Me.txtFechaOPHasta.MesFecha = 0
        Me.txtFechaOPHasta.Name = "txtFechaOPHasta"
        Me.txtFechaOPHasta.PermitirNulo = False
        Me.txtFechaOPHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaOPHasta.SoloLectura = False
        Me.txtFechaOPHasta.TabIndex = 5
        '
        'chkFechaPago
        '
        Me.chkFechaPago.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaPago.Color = System.Drawing.Color.Empty
        Me.chkFechaPago.Location = New System.Drawing.Point(52, 73)
        Me.chkFechaPago.Name = "chkFechaPago"
        Me.chkFechaPago.Size = New System.Drawing.Size(119, 21)
        Me.chkFechaPago.SoloLectura = False
        Me.chkFechaPago.TabIndex = 3
        Me.chkFechaPago.Texto = "Fecha Orden Pago:"
        Me.chkFechaPago.Valor = False
        '
        'txtFechaOPDesde
        '
        Me.txtFechaOPDesde.AñoFecha = 0
        Me.txtFechaOPDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaOPDesde.Enabled = False
        Me.txtFechaOPDesde.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaOPDesde.Location = New System.Drawing.Point(194, 74)
        Me.txtFechaOPDesde.MesFecha = 0
        Me.txtFechaOPDesde.Name = "txtFechaOPDesde"
        Me.txtFechaOPDesde.PermitirNulo = False
        Me.txtFechaOPDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaOPDesde.SoloLectura = False
        Me.txtFechaOPDesde.TabIndex = 4
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(220, 242)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(49, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 12
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(114, 242)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(106, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 11
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(7, 242)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 10
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(7, 24)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtHasta.Location = New System.Drawing.Point(276, 24)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtDesde.Location = New System.Drawing.Point(194, 24)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(366, 302)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(185, 23)
        Me.btnInforme.TabIndex = 6
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.lblBanco)
        Me.gbxFiltro.Controls.Add(Me.txtMoneda)
        Me.gbxFiltro.Controls.Add(Me.txtBanco)
        Me.gbxFiltro.Controls.Add(Me.chkCuentaBancaria)
        Me.gbxFiltro.Controls.Add(Me.cbxCuentaBancaria)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkVerOpAnuladas)
        Me.gbxFiltro.Controls.Add(Me.chkTipo)
        Me.gbxFiltro.Controls.Add(Me.cbxtipo)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(354, 256)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = " "
        '
        'lblBanco
        '
        Me.lblBanco.AutoSize = True
        Me.lblBanco.Location = New System.Drawing.Point(24, 50)
        Me.lblBanco.Name = "lblBanco"
        Me.lblBanco.Size = New System.Drawing.Size(41, 13)
        Me.lblBanco.TabIndex = 18
        Me.lblBanco.Text = "Banco:"
        '
        'txtMoneda
        '
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Color = System.Drawing.Color.Empty
        Me.txtMoneda.Indicaciones = Nothing
        Me.txtMoneda.Location = New System.Drawing.Point(280, 46)
        Me.txtMoneda.Multilinea = False
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(53, 21)
        Me.txtMoneda.SoloLectura = True
        Me.txtMoneda.TabIndex = 15
        Me.txtMoneda.TabStop = False
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtMoneda.Texto = ""
        '
        'txtBanco
        '
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.Color = System.Drawing.Color.Empty
        Me.txtBanco.Indicaciones = Nothing
        Me.txtBanco.Location = New System.Drawing.Point(71, 46)
        Me.txtBanco.Multilinea = False
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(209, 21)
        Me.txtBanco.SoloLectura = True
        Me.txtBanco.TabIndex = 14
        Me.txtBanco.TabStop = False
        Me.txtBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtBanco.Texto = ""
        '
        'chkCuentaBancaria
        '
        Me.chkCuentaBancaria.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaBancaria.Color = System.Drawing.Color.Empty
        Me.chkCuentaBancaria.Location = New System.Drawing.Point(9, 19)
        Me.chkCuentaBancaria.Name = "chkCuentaBancaria"
        Me.chkCuentaBancaria.Size = New System.Drawing.Size(113, 21)
        Me.chkCuentaBancaria.SoloLectura = False
        Me.chkCuentaBancaria.TabIndex = 12
        Me.chkCuentaBancaria.Texto = "Cuenta Bancaria:"
        Me.chkCuentaBancaria.Valor = False
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.CampoWhere = "IDCuentaBancaria"
        Me.cbxCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxCuentaBancaria.DataDisplayMember = "CuentaBancaria"
        Me.cbxCuentaBancaria.DataFilter = Nothing
        Me.cbxCuentaBancaria.DataOrderBy = Nothing
        Me.cbxCuentaBancaria.DataSource = "VCuentaBancaria"
        Me.cbxCuentaBancaria.DataValueMember = "ID"
        Me.cbxCuentaBancaria.dtSeleccionado = Nothing
        Me.cbxCuentaBancaria.Enabled = False
        Me.cbxCuentaBancaria.FormABM = Nothing
        Me.cbxCuentaBancaria.Indicaciones = Nothing
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(129, 19)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.SeleccionMultiple = False
        Me.cbxCuentaBancaria.SeleccionObligatoria = False
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(213, 21)
        Me.cbxCuentaBancaria.SoloLectura = False
        Me.cbxCuentaBancaria.TabIndex = 11
        Me.cbxCuentaBancaria.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(8, 107)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(73, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 8
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(128, 107)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 9
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkVerOpAnuladas
        '
        Me.chkVerOpAnuladas.BackColor = System.Drawing.Color.Transparent
        Me.chkVerOpAnuladas.Color = System.Drawing.Color.Empty
        Me.chkVerOpAnuladas.Location = New System.Drawing.Point(9, 209)
        Me.chkVerOpAnuladas.Name = "chkVerOpAnuladas"
        Me.chkVerOpAnuladas.Size = New System.Drawing.Size(176, 21)
        Me.chkVerOpAnuladas.SoloLectura = False
        Me.chkVerOpAnuladas.TabIndex = 6
        Me.chkVerOpAnuladas.Texto = "Ver OPs Anuladas"
        Me.chkVerOpAnuladas.Valor = False
        '
        'chkTipo
        '
        Me.chkTipo.BackColor = System.Drawing.Color.Transparent
        Me.chkTipo.Color = System.Drawing.Color.Empty
        Me.chkTipo.Location = New System.Drawing.Point(9, 80)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(52, 21)
        Me.chkTipo.SoloLectura = False
        Me.chkTipo.TabIndex = 4
        Me.chkTipo.Texto = "Tipo:"
        Me.chkTipo.Valor = False
        '
        'cbxtipo
        '
        Me.cbxtipo.CampoWhere = ""
        Me.cbxtipo.CargarUnaSolaVez = False
        Me.cbxtipo.DataDisplayMember = "Tipo"
        Me.cbxtipo.DataFilter = Nothing
        Me.cbxtipo.DataOrderBy = Nothing
        Me.cbxtipo.DataSource = ""
        Me.cbxtipo.DataValueMember = ""
        Me.cbxtipo.dtSeleccionado = Nothing
        Me.cbxtipo.Enabled = False
        Me.cbxtipo.FormABM = Nothing
        Me.cbxtipo.Indicaciones = Nothing
        Me.cbxtipo.Location = New System.Drawing.Point(129, 80)
        Me.cbxtipo.Name = "cbxtipo"
        Me.cbxtipo.SeleccionMultiple = False
        Me.cbxtipo.SeleccionObligatoria = False
        Me.cbxtipo.Size = New System.Drawing.Size(213, 21)
        Me.cbxtipo.SoloLectura = False
        Me.cbxtipo.TabIndex = 5
        Me.cbxtipo.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(665, 302)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 216)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Agrupado por:"
        '
        'frmListadoChequesEmitidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(738, 337)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoChequesEmitidos"
        Me.Tag = "frmListadoChequesEmitidos"
        Me.Text = "frmListadoChequesEmitidos"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFechaOPHasta As ERP.ocxTXTDate
    Friend WithEvents chkFechaPago As ERP.ocxCHK
    Friend WithEvents txtFechaOPDesde As ERP.ocxTXTDate
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkTipo As ERP.ocxCHK
    Friend WithEvents cbxtipo As ERP.ocxCBX
    Friend WithEvents cbxAgrupado As ERP.ocxCBX
    Friend WithEvents chkVerOpAnuladas As ERP.ocxCHK
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxCuentaBancaria As ERP.ocxCBX
    Friend WithEvents chkCuentaBancaria As ERP.ocxCHK
    Friend WithEvents txtMoneda As ERP.ocxTXTString
    Friend WithEvents txtBanco As ERP.ocxTXTString
    Friend WithEvents lblBanco As System.Windows.Forms.Label
    Friend WithEvents chkVencimiento As ERP.ocxCHK
    Friend WithEvents chkEmision As ERP.ocxCHK
    Friend WithEvents txtFechaSinEntrega As ocxTXTDate
    Friend WithEvents chkNoEntregadosAl As ocxCHK
    Friend WithEvents txtFechaEntregaHasta As ocxTXTDate
    Friend WithEvents chkFechaEntrega As ocxCHK
    Friend WithEvents txtFechaEntregaDesde As ocxTXTDate
    Friend WithEvents txtVencimientoHasta As ocxTXTDate
    Friend WithEvents txtVencimientoDesde As ocxTXTDate
    Friend WithEvents txtFechaCobroChequeHasta As ocxTXTDate
    Friend WithEvents chkFechaCobroCheque As ocxCHK
    Friend WithEvents txtFechaCobroChequeDesde As ocxTXTDate
    Friend WithEvents txtFechaSinCobro As ocxTXTDate
    Friend WithEvents chkNoCobradosAl As ocxCHK
    Friend WithEvents Label1 As Label
End Class

﻿Imports ERP.Reporte
Public Class frmListadoChequesEmitidos
    'CLASES
    Dim CReporte As New CReporteChequeEmitido
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim dtCuentaBancaria As New DataTable

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE CHEQUES EMITIDOS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True


        'Funciones
        CargarInformacion()
        CambiarOrdenacion()

        txtDesde.Hoy()
        txtHasta.Hoy()
        txtFechaSinEntrega.Hoy()

    End Sub
    Sub CargarInformacion()

        'Cuenta Bancaria
        dtCuentaBancaria = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria").Copy
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, dtCuentaBancaria, "ID", "CuentaBancaria")
        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        ' cargar cbxtipo
        'cbxtipo.cbx.Items.Add("ENTREGADOS")
        'cbxtipo.cbx.Items.Add("NO ENTREGADOS")
        cbxtipo.cbx.Items.Add("AL DIA")
        cbxtipo.cbx.Items.Add("DIFERIDOS")
        cbxtipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxtipo.cbx.SelectedIndex = 0


        'Agrupado
        cbxAgrupado.cbx.Items.Add("Proveedor")
        cbxAgrupado.cbx.Items.Add("Cta/Cheque/Diferido")
        cbxAgrupado.cbx.Items.Add("Cuenta Contable")
        cbxAgrupado.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxAgrupado.cbx.SelectedIndex = 0

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("vOrdenPagoCheque", "Select Top(0) Proveedor, Banco, CuentaBancaria,NroCheque, FechaPago, FechaVencimiento From vOrdenPagoCheque ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next
        cbxOrdenadoPor.cbx.SelectedIndex = 0
    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1"
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = "Order by "
        Dim Top As String = ""
        Dim Tipo As String = ""
        Dim TipoInforme As String = ""
        Dim Estado As String = ""
        Dim OrderByGrupo As String = ""
        Dim OrderBy2 As String = ""
        Dim OrderBy3 As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        If chkEmision.Valor = True Then
            Where = Where & " And FechaChequeFiltro Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False) & "' "
            TipoInforme = TipoInforme & " Emitidos del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text
        End If
        If chkVencimiento.Valor = True Then
            Where = Where & " And Diferido = 'True' and FechaVencimiento Between '" & CSistema.FormatoFechaBaseDatos(txtVencimientoDesde.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtVencimientoHasta.GetValue, True, False) & "' "
            TipoInforme = TipoInforme & " Vencidos del " & txtVencimientoDesde.txt.Text & " al " & txtVencimientoHasta.txt.Text
        End If
        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        'OrderBy = EstablecerOrderBy()

        'If chkTipo.chk.Checked = True Then
        '    If cbxtipo.cbx.SelectedIndex = 0 Then 'entregados
        '        Tipo = " And RetiradoPor is not null "
        '    ElseIf cbxtipo.cbx.SelectedIndex = 1 Then 'no entregados
        '        Tipo = " And RetiradoPor is null "
        '    ElseIf cbxtipo.cbx.SelectedIndex = 2 Then 'diferidos
        '        Tipo = " And diferido = 'True' "
        '    End If
        'Else
        '    Tipo = ""
        'End If

        If chkFechaEntrega.Valor = True Then
            Where = Where & " And FechaEntrega Between '" & CSistema.FormatoFechaBaseDatos(txtFechaEntregaDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtFechaEntregaHasta.GetValue, True, False) & "' "
        End If

        If chkNoEntregadosAl.Valor Then
            Where = Where & " And isnull(FechaEntrega, '" & CSistema.FormatoFechaBaseDatos(txtFechaSinEntrega.GetValue.AddDays(1), True, False) & "') > '" & CSistema.FormatoFechaBaseDatos(txtFechaSinEntrega.GetValue, True, False) & "' "
            TipoInforme = TipoInforme & " - No Entregados al " & txtFechaSinEntrega.txt.Text

        End If
        If chkNoCobradosAl.Valor Then
            Where = Where & " And isnull(FechaCobroProveedor, '" & CSistema.FormatoFechaBaseDatos(txtFechaCobroChequeDesde.GetValue.AddDays(1), True, False) & "') > '" & CSistema.FormatoFechaBaseDatos(txtFechaCobroChequeDesde.GetValue, True, False) & "' "
            TipoInforme = TipoInforme & " - No Cobrados al " & txtFechaCobroChequeDesde.txt.Text
        End If

        If chkFechaPago.Valor = True Then
            Where = Where & " And FechaPago Between '" & CSistema.FormatoFechaBaseDatos(txtFechaOPDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtFechaOPHasta.GetValue, True, False) & "' "
        End If

        If chkFechaCobroCheque.Valor = True Then
            Where = Where & " And FechaCobroProveedor Between '" & CSistema.FormatoFechaBaseDatos(txtFechaCobroChequeDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtFechaCobroChequeHasta.GetValue, True, False) & "' "
        End If

        Dim vAgrupado As String = cbxAgrupado.cbx.Text


        'If cbxAgrupado.cbx.SelectedIndex = 0 Then
        '    vAgrupado = "P"
        'Else
        '    vAgrupado = "B"
        'End If

        'Ordenado
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = OrderBy & cbxOrdenadoPor.cbx.Text
        Else
            OrderBy = ""
        End If


        'En forma
        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        Else
            OrderBy = " "
        End If

        Dim vchkVerOpAnuladas As String = ""
        If chkVerOpAnuladas.Valor = False Then
            vchkVerOpAnuladas = "and Anulado = 'False'"
        End If

        Where = Where & vchkVerOpAnuladas

        If chkTipo.chk.Checked = True Then
            If cbxtipo.cbx.SelectedIndex = 0 Then 'al dia
                Tipo = " And diferido = 'False' "
                TipoInforme = TipoInforme & " - " & cbxtipo.cbx.Text
            ElseIf cbxtipo.cbx.SelectedIndex = 1 Then 'diferidos
                Tipo = " And diferido = 'True' "
                TipoInforme = TipoInforme & " - " & cbxtipo.cbx.Text
            End If
        Else
            Tipo = ""
        End If


        '        TipoInforme = "Del " & txtDesde.GetValue & " al " & txtHasta.GetValue & " - Tipo: " & cbxtipo.cbx.Text & ""
        Dim vFiltros As String = ""
        CReporte.ArmarSubtitulo(vFiltros, gbxFiltro)
        CReporte.ListarChequeEmitido(frm, Where, WhereDetalle, Tipo, Estado, OrderBy, Top, vgUsuarioIdentificador, TipoInforme & " - " & vFiltros, vAgrupado)

    End Sub

    Sub ObtenerCuenta()
        If dtCuentaBancaria.Columns.Count > 0 Then
            If cbxCuentaBancaria.cbx.SelectedValue Is Nothing Then
                Exit Sub
            End If

            txtBanco.txt.Clear()
            txtMoneda.txt.Clear()

            For Each oRow As DataRow In dtCuentaBancaria.Select(" ID=" & cbxCuentaBancaria.cbx.SelectedValue)

                txtBanco.txt.Text = oRow("Banco").ToString
                txtMoneda.txt.Text = oRow("Mon").ToString

            Next
        End If
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub


    Private Sub chkTipo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipo.PropertyChanged
        cbxtipo.Enabled = value
    End Sub


    Private Sub chkFechaPago_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaPago.PropertyChanged
        txtFechaOPDesde.Enabled = value
        txtFechaOPHasta.Enabled = value
    End Sub

    Private Sub frmListadoChequesEmitidos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub


    Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCuentaBancaria_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)
        cbxCuentaBancaria.Enabled = value
    End Sub

    Private Sub chkCuentaBancaria_PropertyChanged_1(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCuentaBancaria.PropertyChanged
        cbxCuentaBancaria.Enabled = value
    End Sub

    Private Sub cbxCuentaBancaria_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxCuentaBancaria.PropertyChanged
        ObtenerCuenta()
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Close()
    End Sub

    Private Sub chkAgrupadoPor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)
        cbxAgrupado.Enabled = value
    End Sub


    Private Sub chkEmision_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkEmision.PropertyChanged
        If chkVencimiento.Valor = False And value = False Then
            chkVencimiento.Valor = True
        End If

        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkVencimiento_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVencimiento.PropertyChanged
        If chkEmision.Valor = False And value = False Then
            chkEmision.Valor = True
        End If

        txtVencimientoDesde.Enabled = value
        txtVencimientoHasta.Enabled = value
    End Sub

    Private Sub chkFechaEntrega_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaEntrega.PropertyChanged
        txtFechaEntregaDesde.Enabled = value
        txtFechaEntregaHasta.Enabled = value
    End Sub

    Private Sub chkNoEntregadosAl_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkNoEntregadosAl.PropertyChanged
        txtFechaSinEntrega.Enabled = value
    End Sub

    Private Sub txtHasta_Load(sender As Object, e As EventArgs) Handles txtHasta.Load

    End Sub

    Private Sub chkFechaCobroCheque_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaCobroCheque.PropertyChanged
        txtFechaCobroChequeDesde.Enabled = value
        txtFechaCobroChequeHasta.Enabled = value

    End Sub

    Private Sub chkNoCobradosAl_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkNoCobradosAl.PropertyChanged
        txtFechaSinCobro.Enabled = value
    End Sub


End Class
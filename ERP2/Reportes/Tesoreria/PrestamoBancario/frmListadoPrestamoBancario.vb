﻿Imports ERP.Reporte

Public Class frmListadoPrestamoBancario

    'CLASES
    Dim CReporte As New CReporteBanco
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim dtBanco As DataTable

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE PRESTAMOS BANCARIOS DETALLADO"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
        txtDesdeDetalle.PrimerDiaMes()
        txtHastDetalle.Hoy()

        CargarInformacion()

        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Informe
        cbxInforme.cbx.Items.Add("Resumido")
        cbxInforme.cbx.Items.Add("Detallado")
        cbxInforme.cbx.Items.Add("Detallado Con Pagos Parciales")
        cbxInforme.cbx.Items.Add("Vencimiento de Cuotas")
        cbxInforme.cbx.Items.Add("Deuda Total del Periodos")
        cbxInforme.cbx.SelectedIndex = 0

        dtBanco = CSistema.ExecuteToDataTable("Select * From VCuentaBancaria")

        cbxMoneda.cbx.SelectedIndex = 0

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VDepositoBancario ", "Select Top(0) Fecha,NroComprobante From VPrestamoBancario")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim Banco As String = ""
        Dim Titulo As String = ""
        Dim TipoInforme As String = ""
        Dim frm As New frmReporte
        Dim Informe As String = ""

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = "Where 1 =1 "

        Where += " AND Anulado = 0 "

        If chkMoneda.Valor = True Then
            Where = Where & " And IDMoneda=" & cbxMoneda.GetValue
            TipoInforme = TipoInforme & cbxMoneda.Texto & " "
        End If


        Titulo = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text
        TipoInforme = "Fecha: " & txtDesde.txt.Text & " - " & txtHasta.txt.Text & "  "
        If chkBanco.Valor = True Then
            Where = Where & " and IDBanco = " & cbxBanco.GetValue
            TipoInforme = TipoInforme & "Banco: " & cbxBanco.Texto
        End If

        If chkCuenta.Valor = True Then
            Where = Where & " And IDCuentaBancaria = " & cbxCuenta.GetValue
            For Each oRow As DataRow In dtBanco.Select("ID=" & cbxCuenta.GetValue)
                Banco = oRow("Referencia").ToString
            Next
            If chkBanco.Valor = True Then
                TipoInforme = " - " & cbxCuenta.cbx.Text & " - " & cbxMoneda.cbx.Text
            Else
                TipoInforme = Banco & " - " & cbxCuenta.cbx.Text & " - " & cbxMoneda.cbx.Text
            End If
        End If

        If chkSucursal.Valor = True Then
            Titulo = Titulo & " - " & cbxSucursal.cbx.Text
            Where = Where & " And IDSucursal=" & cbxSucursal.GetValue
        End If


        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        'Informe
        Informe = cbxInforme.Texto

        If chkSoloActivos.Valor = True Then
            'Where = Where & " and Idtransaccion in (select IDTransaccion from  DetallePrestamoBancario Where(IDtransaccion = VDetallePrestamoBancario.IDTransaccion) and Cancelado = 0)"
            Where = Where & " and (Select sum(saldo) from VDetallePrestamoBancario V where V.IDTransaccion = VDetallePrestamoBancario.IDTransaccion ) <> 0"
        End If

        If chkGarantia.Valor = True Then
            Where = Where & " and TipoGarantia = '" & cbxTipoGarantia.Text & "'"
        End If

        If chkVerCancelados.Valor = False Then
            Where = Where & " and Saldo > 0"
        Else
        End If

        Where = Where & " and cast(Fecha as date) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        If chkVencimientoCuota.Valor = True Then
            Where = Where & " and cast(FechaVencimiento as date) Between '" & txtDesdeDetalle.GetValueString & "' And '" & txtHastDetalle.GetValueString & "' "
        End If
        'Seleccionar Informe
        Select Case cbxInforme.cbx.SelectedIndex
            Case Is = 0
                'Titulo = "PRESTAMOS BANCARIOS"
                'TipoInforme = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString
                CReporte.ListadoPrestamoBancarioDetalle(frm, Titulo, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, TipoInforme, Informe)
            Case Is = 1
                CReporte.ListadoPrestamoBancarioDetalle(frm, Titulo, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, TipoInforme, Informe)
            Case Is = 3
                CReporte.ListadoPrestamoBancarioDetalle(frm, Titulo, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, TipoInforme, "Cuotas")
            Case Is = 4
                CReporte.ListadoPrestamoBancarioDetalle(frm, Titulo, Where, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text, TipoInforme, "DeudaTotal")
        End Select



    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoDepositoBancario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCuenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCuenta.PropertyChanged
        cbxCuenta.Enabled = value
    End Sub

    Private Sub chkBanco_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkBanco.PropertyChanged
        cbxBanco.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkGarantia_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkGarantia.PropertyChanged
        cbxTipoGarantia.Enabled = value
    End Sub

    Private Sub chkVencimientoCuota_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVencimientoCuota.PropertyChanged
        txtDesdeDetalle.Enabled = value
        txtHastDetalle.Enabled = value
    End Sub
End Class
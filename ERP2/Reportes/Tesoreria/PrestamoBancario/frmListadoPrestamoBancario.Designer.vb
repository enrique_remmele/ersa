﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoPrestamoBancario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkVencimientoCuota = New ERP.ocxCHK()
        Me.txtHastDetalle = New ERP.ocxTXTDate()
        Me.txtDesdeDetalle = New ERP.ocxTXTDate()
        Me.chkVerCancelados = New ERP.ocxCHK()
        Me.cbxInforme = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkGarantia = New ERP.ocxCHK()
        Me.cbxTipoGarantia = New System.Windows.Forms.ComboBox()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.chkBanco = New ERP.ocxCHK()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkSoloActivos = New ERP.ocxCHK()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCuenta = New ERP.ocxCHK()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Informe:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(311, 283)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 9
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(492, 283)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 15
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkVencimientoCuota)
        Me.GroupBox2.Controls.Add(Me.txtHastDetalle)
        Me.GroupBox2.Controls.Add(Me.txtDesdeDetalle)
        Me.GroupBox2.Controls.Add(Me.chkVerCancelados)
        Me.GroupBox2.Controls.Add(Me.cbxInforme)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(311, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 270)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkVencimientoCuota
        '
        Me.chkVencimientoCuota.BackColor = System.Drawing.Color.Transparent
        Me.chkVencimientoCuota.Color = System.Drawing.Color.Empty
        Me.chkVencimientoCuota.Location = New System.Drawing.Point(6, 59)
        Me.chkVencimientoCuota.Name = "chkVencimientoCuota"
        Me.chkVencimientoCuota.Size = New System.Drawing.Size(199, 21)
        Me.chkVencimientoCuota.SoloLectura = False
        Me.chkVencimientoCuota.TabIndex = 40
        Me.chkVencimientoCuota.Texto = "Vencimiento de Cuotas"
        Me.chkVencimientoCuota.Valor = False
        '
        'txtHastDetalle
        '
        Me.txtHastDetalle.AñoFecha = 0
        Me.txtHastDetalle.Color = System.Drawing.Color.Empty
        Me.txtHastDetalle.Enabled = False
        Me.txtHastDetalle.Fecha = New Date(2013, 5, 31, 16, 45, 5, 358)
        Me.txtHastDetalle.Location = New System.Drawing.Point(89, 85)
        Me.txtHastDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHastDetalle.MesFecha = 0
        Me.txtHastDetalle.Name = "txtHastDetalle"
        Me.txtHastDetalle.PermitirNulo = False
        Me.txtHastDetalle.Size = New System.Drawing.Size(74, 20)
        Me.txtHastDetalle.SoloLectura = False
        Me.txtHastDetalle.TabIndex = 39
        '
        'txtDesdeDetalle
        '
        Me.txtDesdeDetalle.AñoFecha = 0
        Me.txtDesdeDetalle.Color = System.Drawing.Color.Empty
        Me.txtDesdeDetalle.Enabled = False
        Me.txtDesdeDetalle.Fecha = New Date(2013, 5, 31, 16, 45, 5, 358)
        Me.txtDesdeDetalle.Location = New System.Drawing.Point(6, 85)
        Me.txtDesdeDetalle.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesdeDetalle.MesFecha = 0
        Me.txtDesdeDetalle.Name = "txtDesdeDetalle"
        Me.txtDesdeDetalle.PermitirNulo = False
        Me.txtDesdeDetalle.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeDetalle.SoloLectura = False
        Me.txtDesdeDetalle.TabIndex = 38
        '
        'chkVerCancelados
        '
        Me.chkVerCancelados.BackColor = System.Drawing.Color.Transparent
        Me.chkVerCancelados.Color = System.Drawing.Color.Empty
        Me.chkVerCancelados.Location = New System.Drawing.Point(8, 246)
        Me.chkVerCancelados.Margin = New System.Windows.Forms.Padding(4)
        Me.chkVerCancelados.Name = "chkVerCancelados"
        Me.chkVerCancelados.Size = New System.Drawing.Size(217, 21)
        Me.chkVerCancelados.SoloLectura = False
        Me.chkVerCancelados.TabIndex = 36
        Me.chkVerCancelados.Texto = "Ver cuotas sin saldo"
        Me.chkVerCancelados.Valor = True
        '
        'cbxInforme
        '
        Me.cbxInforme.CampoWhere = Nothing
        Me.cbxInforme.CargarUnaSolaVez = False
        Me.cbxInforme.DataDisplayMember = Nothing
        Me.cbxInforme.DataFilter = Nothing
        Me.cbxInforme.DataOrderBy = Nothing
        Me.cbxInforme.DataSource = Nothing
        Me.cbxInforme.DataValueMember = Nothing
        Me.cbxInforme.dtSeleccionado = Nothing
        Me.cbxInforme.FormABM = Nothing
        Me.cbxInforme.Indicaciones = Nothing
        Me.cbxInforme.Location = New System.Drawing.Point(5, 128)
        Me.cbxInforme.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxInforme.Name = "cbxInforme"
        Me.cbxInforme.SeleccionMultiple = False
        Me.cbxInforme.SeleccionObligatoria = True
        Me.cbxInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxInforme.SoloLectura = False
        Me.cbxInforme.TabIndex = 6
        Me.cbxInforme.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(165, 173)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 9
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(5, 173)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 8
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(7, 156)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 7
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(5, 217)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 11
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 15)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(105, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo de Prestamo"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(5, 201)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 10
        Me.lblRanking.Text = "Ranking:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 45, 5, 358)
        Me.txtHasta.Location = New System.Drawing.Point(89, 34)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 45, 5, 358)
        Me.txtDesde.Location = New System.Drawing.Point(6, 34)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkGarantia)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoGarantia)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkBanco)
        Me.gbxFiltro.Controls.Add(Me.cbxBanco)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkSoloActivos)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCuenta)
        Me.gbxFiltro.Controls.Add(Me.cbxCuenta)
        Me.gbxFiltro.Location = New System.Drawing.Point(5, 7)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(300, 270)
        Me.gbxFiltro.TabIndex = 10
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkGarantia
        '
        Me.chkGarantia.BackColor = System.Drawing.Color.Transparent
        Me.chkGarantia.Color = System.Drawing.Color.Empty
        Me.chkGarantia.Location = New System.Drawing.Point(9, 198)
        Me.chkGarantia.Margin = New System.Windows.Forms.Padding(4)
        Me.chkGarantia.Name = "chkGarantia"
        Me.chkGarantia.Size = New System.Drawing.Size(68, 21)
        Me.chkGarantia.SoloLectura = False
        Me.chkGarantia.TabIndex = 35
        Me.chkGarantia.Texto = "Garantia:"
        Me.chkGarantia.Valor = False
        '
        'cbxTipoGarantia
        '
        Me.cbxTipoGarantia.Enabled = False
        Me.cbxTipoGarantia.FormattingEnabled = True
        Me.cbxTipoGarantia.Items.AddRange(New Object() {"A SOLA FIRMA", "HIPOTECARIA", "PRENDARIA", "GARANTIA DE TERCEROS", "WARRANTS", "REVOLVING", "CASH COLLATERAL"})
        Me.cbxTipoGarantia.Location = New System.Drawing.Point(85, 196)
        Me.cbxTipoGarantia.Margin = New System.Windows.Forms.Padding(2)
        Me.cbxTipoGarantia.Name = "cbxTipoGarantia"
        Me.cbxTipoGarantia.Size = New System.Drawing.Size(193, 21)
        Me.cbxTipoGarantia.TabIndex = 34
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(9, 133)
        Me.chkMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(68, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 16
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'chkBanco
        '
        Me.chkBanco.BackColor = System.Drawing.Color.Transparent
        Me.chkBanco.Color = System.Drawing.Color.Empty
        Me.chkBanco.Location = New System.Drawing.Point(9, 67)
        Me.chkBanco.Margin = New System.Windows.Forms.Padding(4)
        Me.chkBanco.Name = "chkBanco"
        Me.chkBanco.Size = New System.Drawing.Size(68, 21)
        Me.chkBanco.SoloLectura = False
        Me.chkBanco.TabIndex = 14
        Me.chkBanco.Texto = "Banco:"
        Me.chkBanco.Valor = False
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = "IDBanco"
        Me.cbxBanco.CargarUnaSolaVez = False
        Me.cbxBanco.DataDisplayMember = "Descripcion"
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = "vBanco"
        Me.cbxBanco.DataValueMember = "ID"
        Me.cbxBanco.dtSeleccionado = Nothing
        Me.cbxBanco.Enabled = False
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(85, 67)
        Me.cbxBanco.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionMultiple = False
        Me.cbxBanco.SeleccionObligatoria = True
        Me.cbxBanco.Size = New System.Drawing.Size(199, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 15
        Me.cbxBanco.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "Moneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = "Moneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(85, 133)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(199, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 4
        Me.cbxMoneda.Texto = ""
        '
        'chkSoloActivos
        '
        Me.chkSoloActivos.BackColor = System.Drawing.Color.Transparent
        Me.chkSoloActivos.Color = System.Drawing.Color.Empty
        Me.chkSoloActivos.Location = New System.Drawing.Point(9, 169)
        Me.chkSoloActivos.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSoloActivos.Name = "chkSoloActivos"
        Me.chkSoloActivos.Size = New System.Drawing.Size(95, 21)
        Me.chkSoloActivos.SoloLectura = False
        Me.chkSoloActivos.TabIndex = 13
        Me.chkSoloActivos.Texto = "Solo Activos"
        Me.chkSoloActivos.Valor = False
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 32)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(68, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(85, 32)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(199, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'chkCuenta
        '
        Me.chkCuenta.BackColor = System.Drawing.Color.Transparent
        Me.chkCuenta.Color = System.Drawing.Color.Empty
        Me.chkCuenta.Location = New System.Drawing.Point(9, 102)
        Me.chkCuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCuenta.Name = "chkCuenta"
        Me.chkCuenta.Size = New System.Drawing.Size(68, 21)
        Me.chkCuenta.SoloLectura = False
        Me.chkCuenta.TabIndex = 11
        Me.chkCuenta.Texto = "Cuenta:"
        Me.chkCuenta.Valor = False
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = "IDCuentaBancaria"
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = "Descripcion"
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = "vCuentaBancaria"
        Me.cbxCuenta.DataValueMember = "ID"
        Me.cbxCuenta.dtSeleccionado = Nothing
        Me.cbxCuenta.Enabled = False
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(85, 102)
        Me.cbxCuenta.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionMultiple = False
        Me.cbxCuenta.SeleccionObligatoria = True
        Me.cbxCuenta.Size = New System.Drawing.Size(199, 21)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 12
        Me.cbxCuenta.Texto = ""
        '
        'frmListadoPrestamoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(561, 320)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmListadoPrestamoBancario"
        Me.Tag = "frmListadoPrestamoBancario"
        Me.Text = "frmListadoPrestamoBancario"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxInforme As ERP.ocxCBX
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents chkCuenta As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSoloActivos As ERP.ocxCHK
    Friend WithEvents chkBanco As ERP.ocxCHK
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents chkGarantia As ERP.ocxCHK
    Friend WithEvents cbxTipoGarantia As System.Windows.Forms.ComboBox
    Friend WithEvents chkVerCancelados As ERP.ocxCHK
    Friend WithEvents txtHastDetalle As ERP.ocxTXTDate
    Friend WithEvents txtDesdeDetalle As ERP.ocxTXTDate
    Friend WithEvents chkVencimientoCuota As ERP.ocxCHK
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoDebitoCreditoBancario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.OcxCHK1 = New ERP.ocxCHK()
        Me.chkNroOperacion = New ERP.ocxCHK()
        Me.txtID = New ERP.ocxTXTNumeric()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkCarga = New ERP.ocxCHK()
        Me.chkOperacion = New ERP.ocxCHK()
        Me.txtHastaCarga = New ERP.ocxTXTDate()
        Me.txtDesdeCarga = New ERP.ocxTXTDate()
        Me.chkInformeCC = New ERP.ocxCHK()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkCuenta = New ERP.ocxCHK()
        Me.cbxCuenta = New ERP.ocxCBX()
        Me.chkUnidadNegocio = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.OcxCHK1)
        Me.gbxFiltro.Controls.Add(Me.chkNroOperacion)
        Me.gbxFiltro.Controls.Add(Me.txtID)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(368, 220)
        Me.gbxFiltro.TabIndex = 2
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'OcxCHK1
        '
        Me.OcxCHK1.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK1.Color = System.Drawing.Color.Empty
        Me.OcxCHK1.Location = New System.Drawing.Point(150, 75)
        Me.OcxCHK1.Name = "OcxCHK1"
        Me.OcxCHK1.Size = New System.Drawing.Size(68, 21)
        Me.OcxCHK1.SoloLectura = False
        Me.OcxCHK1.TabIndex = 7
        Me.OcxCHK1.Texto = "Sucursal:"
        Me.OcxCHK1.Valor = False
        '
        'chkNroOperacion
        '
        Me.chkNroOperacion.BackColor = System.Drawing.Color.Transparent
        Me.chkNroOperacion.Color = System.Drawing.Color.Empty
        Me.chkNroOperacion.Location = New System.Drawing.Point(9, 105)
        Me.chkNroOperacion.Name = "chkNroOperacion"
        Me.chkNroOperacion.Size = New System.Drawing.Size(89, 21)
        Me.chkNroOperacion.SoloLectura = False
        Me.chkNroOperacion.TabIndex = 6
        Me.chkNroOperacion.Texto = "Nro.Operacion:"
        Me.chkNroOperacion.Valor = False
        '
        'txtID
        '
        Me.txtID.Color = System.Drawing.Color.Empty
        Me.txtID.Decimales = False
        Me.txtID.Indicaciones = "Intruduzca el codigo y presione ENTER para obtener la información"
        Me.txtID.Location = New System.Drawing.Point(104, 102)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(74, 21)
        Me.txtID.SoloLectura = False
        Me.txtID.TabIndex = 3
        Me.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtID.Texto = "0"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 37)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(68, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(83, 38)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(280, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(370, 242)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 5
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkCarga)
        Me.GroupBox2.Controls.Add(Me.chkOperacion)
        Me.GroupBox2.Controls.Add(Me.txtHastaCarga)
        Me.GroupBox2.Controls.Add(Me.txtDesdeCarga)
        Me.GroupBox2.Controls.Add(Me.chkInformeCC)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(377, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(270, 220)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkCarga
        '
        Me.chkCarga.BackColor = System.Drawing.Color.Transparent
        Me.chkCarga.Color = System.Drawing.Color.Empty
        Me.chkCarga.Location = New System.Drawing.Point(11, 62)
        Me.chkCarga.Name = "chkCarga"
        Me.chkCarga.Size = New System.Drawing.Size(87, 21)
        Me.chkCarga.SoloLectura = False
        Me.chkCarga.TabIndex = 17
        Me.chkCarga.Texto = "Carga"
        Me.chkCarga.Valor = False
        '
        'chkOperacion
        '
        Me.chkOperacion.BackColor = System.Drawing.Color.Transparent
        Me.chkOperacion.Color = System.Drawing.Color.Empty
        Me.chkOperacion.Location = New System.Drawing.Point(11, 35)
        Me.chkOperacion.Name = "chkOperacion"
        Me.chkOperacion.Size = New System.Drawing.Size(87, 21)
        Me.chkOperacion.SoloLectura = False
        Me.chkOperacion.TabIndex = 16
        Me.chkOperacion.Texto = "Operacion"
        Me.chkOperacion.Valor = True
        '
        'txtHastaCarga
        '
        Me.txtHastaCarga.AñoFecha = 0
        Me.txtHastaCarga.Color = System.Drawing.Color.Empty
        Me.txtHastaCarga.Fecha = New Date(2013, 5, 31, 16, 44, 31, 471)
        Me.txtHastaCarga.Location = New System.Drawing.Point(187, 62)
        Me.txtHastaCarga.MesFecha = 0
        Me.txtHastaCarga.Name = "txtHastaCarga"
        Me.txtHastaCarga.PermitirNulo = False
        Me.txtHastaCarga.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaCarga.SoloLectura = False
        Me.txtHastaCarga.TabIndex = 15
        '
        'txtDesdeCarga
        '
        Me.txtDesdeCarga.AñoFecha = 0
        Me.txtDesdeCarga.Color = System.Drawing.Color.Empty
        Me.txtDesdeCarga.Fecha = New Date(2013, 5, 31, 16, 44, 31, 471)
        Me.txtDesdeCarga.Location = New System.Drawing.Point(104, 62)
        Me.txtDesdeCarga.MesFecha = 0
        Me.txtDesdeCarga.Name = "txtDesdeCarga"
        Me.txtDesdeCarga.PermitirNulo = False
        Me.txtDesdeCarga.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeCarga.SoloLectura = False
        Me.txtDesdeCarga.TabIndex = 14
        '
        'chkInformeCC
        '
        Me.chkInformeCC.BackColor = System.Drawing.Color.Transparent
        Me.chkInformeCC.Color = System.Drawing.Color.Empty
        Me.chkInformeCC.Location = New System.Drawing.Point(99, 177)
        Me.chkInformeCC.Name = "chkInformeCC"
        Me.chkInformeCC.Size = New System.Drawing.Size(110, 21)
        Me.chkInformeCC.SoloLectura = False
        Me.chkInformeCC.TabIndex = 13
        Me.chkInformeCC.Texto = "Informe Contable"
        Me.chkInformeCC.Valor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 199)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Vencimiento:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 133)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 133)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 116)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(6, 176)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 7
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(8, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 160)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 6
        Me.lblRanking.Text = "Ranking:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 44, 31, 471)
        Me.txtHasta.Location = New System.Drawing.Point(187, 36)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 44, 31, 471)
        Me.txtDesde.Location = New System.Drawing.Point(104, 36)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(134, 242)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkCuenta
        '
        Me.chkCuenta.BackColor = System.Drawing.Color.Transparent
        Me.chkCuenta.Color = System.Drawing.Color.Empty
        Me.chkCuenta.Location = New System.Drawing.Point(12, 76)
        Me.chkCuenta.Name = "chkCuenta"
        Me.chkCuenta.Size = New System.Drawing.Size(68, 21)
        Me.chkCuenta.SoloLectura = False
        Me.chkCuenta.TabIndex = 3
        Me.chkCuenta.Texto = "Cuenta:"
        Me.chkCuenta.Valor = False
        '
        'cbxCuenta
        '
        Me.cbxCuenta.CampoWhere = "IDCuentaBancaria"
        Me.cbxCuenta.CargarUnaSolaVez = False
        Me.cbxCuenta.DataDisplayMember = "Descripcion"
        Me.cbxCuenta.DataFilter = Nothing
        Me.cbxCuenta.DataOrderBy = Nothing
        Me.cbxCuenta.DataSource = "vCuentaBancaria"
        Me.cbxCuenta.DataValueMember = "ID"
        Me.cbxCuenta.dtSeleccionado = Nothing
        Me.cbxCuenta.Enabled = False
        Me.cbxCuenta.FormABM = Nothing
        Me.cbxCuenta.Indicaciones = Nothing
        Me.cbxCuenta.Location = New System.Drawing.Point(86, 77)
        Me.cbxCuenta.Name = "cbxCuenta"
        Me.cbxCuenta.SeleccionMultiple = False
        Me.cbxCuenta.SeleccionObligatoria = True
        Me.cbxCuenta.Size = New System.Drawing.Size(280, 21)
        Me.cbxCuenta.SoloLectura = False
        Me.cbxCuenta.TabIndex = 4
        Me.cbxCuenta.Texto = ""
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(9, 133)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(116, 21)
        Me.chkUnidadNegocio.SoloLectura = False
        Me.chkUnidadNegocio.TabIndex = 6
        Me.chkUnidadNegocio.Texto = "Unidad de Negocio:"
        Me.chkUnidadNegocio.Valor = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = ""
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = ""
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = ""
        Me.cbxUnidadNegocio.DataValueMember = ""
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.Enabled = False
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(131, 134)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = True
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(232, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 7
        Me.cbxUnidadNegocio.Texto = ""
        '
        'frmListadoDebitoCreditoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 277)
        Me.Controls.Add(Me.chkCuenta)
        Me.Controls.Add(Me.cbxCuenta)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoDebitoCreditoBancario"
        Me.Text = "Listado Débitos y Créditos Bancarios"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkCuenta As ERP.ocxCHK
    Friend WithEvents cbxCuenta As ERP.ocxCBX
    Friend WithEvents chkNroOperacion As ERP.ocxCHK
    Friend WithEvents txtID As ERP.ocxTXTNumeric
    Friend WithEvents chkInformeCC As ERP.ocxCHK
    Friend WithEvents OcxCHK1 As ERP.ocxCHK
    Friend WithEvents txtHastaCarga As ERP.ocxTXTDate
    Friend WithEvents txtDesdeCarga As ERP.ocxTXTDate
    Friend WithEvents chkCarga As ERP.ocxCHK
    Friend WithEvents chkOperacion As ERP.ocxCHK
    Friend WithEvents chkUnidadNegocio As ocxCHK
    Friend WithEvents cbxUnidadNegocio As ocxCBX
End Class

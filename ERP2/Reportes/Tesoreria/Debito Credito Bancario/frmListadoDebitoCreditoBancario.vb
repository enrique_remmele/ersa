﻿Imports ERP.Reporte

Public Class frmListadoDebitoCreditoBancario

    'CLASES
    Dim CReporte As New CReporteBanco
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE DEBITOS Y CREDITOS BANCARIOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()
        txtID.Enabled = False
        txtDesde.Ayer()
        txtHasta.Hoy()
        txtDesdeCarga.Ayer()
        txtHastaCarga.Hoy()

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, "Select ID,Descripcion From vUnidadNegocio Where Estado = 1 Order by Descripcion")

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        Where = " Where 1 = 1 "
        If chkOperacion.Valor = True Then
            Where = Where & " and Fecha Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "
        End If
        If chkCarga.Valor = True Then
            Where = Where & " And cast(fechatransaccion as date) Between '" & CSistema.FormatoFechaBaseDatos(txtDesdeCarga.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHastaCarga.txt.Text, True, False) & "' "
        End If
        If txtID.txt.Text <> "" And txtID.txt.Text <> 0 And chkNroOperacion.Valor = True Then
            Where = Where & " and numero = " & txtID.ObtenerValor
        End If



        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        If chkCuenta.Valor = True Then
            Where = Where & " and IDCuentaBancaria=" & cbxCuenta.GetValue
        End If

        If chkUnidadNegocio.chk.Checked = True Then
            Where = Where & " And IDUnidadNegocio = " & cbxUnidadNegocio.cbx.SelectedValue
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        TipoInforme = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString

        If chkSucursal.Valor = True Then
            TipoInforme = TipoInforme & " - Suc:" & cbxSucursal.cbx.Text
        End If

        If chkCuenta.Valor = True Then
            TipoInforme = TipoInforme & " - Cuenta:" & cbxCuenta.cbx.Text
        End If

        CReporte.ListadoDebitoCreditoBancario(frm, Titulo, Where, OrderBy, Top, TipoInforme, chkInformeCC.Valor)


    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VDepositoBancario ", "Select Top(0) Sucursal,Fecha,NroComprobante,Cuenta From VDepositoBancario")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

        cbxOrdenadoPor.cbx.SelectedIndex = 1

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoDebitoCreditoBancario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCuenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCuenta.PropertyChanged
        cbxCuenta.Enabled = value
    End Sub

    Private Sub chkNroOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkNroOperacion.PropertyChanged
        txtID.Enabled = value
    End Sub

    Private Sub chkUnidadNegocio_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkUnidadNegocio.PropertyChanged
        cbxUnidadNegocio.Enabled = value
        cbxUnidadNegocio.cbx.SelectedIndex = 0
    End Sub
End Class




﻿Imports ERP.Reporte
Public Class frmInformeFinancieroIngresos

    'CLASES
    Dim CReporte As New CInformeFinanciero
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Public Property Precargar As Boolean
    Public Property IDProveedor As Integer
    Public Property Desde As Date
    Public Property Hasta As Date

    'VARIABLES
    Dim Titulo As String = "INGRESOS TOTALES "
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True

        'Fechas
        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()

        'Funciones
        CargarInformacion()
        txtDesde.Ayer()
        txtHasta.Ayer()
    End Sub

    Sub CargarInformacion()

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim Where2 As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As Boolean = True
        Dim Comprobante As String = ""
        Dim SP As String = ""
        Dim IDTransaccionEgreso As String = ""
        Dim IDProveedor As String = ""
        Dim TipoComprobante As String = ""

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = "'" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "','" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "

        Titulo = "INFORME INGRESOS TOTALES"
        TipoInforme = "Fecha desde " & txtDesde.txt.Text & " hasta " & txtHasta.txt.Text

        If chkSucursal.chk.Checked = False Then
            Where = Where & ",0"
            CReporte.InformeFinancieroIngresos(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador)
        Else
            Where = Where & "," & cbxSucursal.GetValue
            CReporte.InformeFinancieroIngresos(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador)
        End If

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmExtractoMovimientoProveedor_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

End Class





﻿Imports System.IO
'Imports ERP.Reporte
Public Class frmInformeControlTesoreria
    'CLASES
    'Dim CReporte As New CInformeFinanciero
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim vControles() As Control

    'PROPIEDADES
    Public Property Precargar As Boolean
    Public Property IDProveedor As Integer
    Public Property Desde As Date
    Public Property Hasta As Date

    'VARIABLES
    Dim Titulo As String = "INFORME DE CONTROL DE TESORERIA "
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True

        'Funciones
        CargarInformacion()
        txtDesde.Ayer()
        txtFechaDesde.Hoy()
        txtFechaHasta.Hoy()
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        CSistema.CargaControl(vControles, txtvalortotaldescontado)
        CSistema.CargaControl(vControles, txtvalortotaldiferido)
        CSistema.CargaControl(vControles, txtventacontadoasu)
        CSistema.CargaControl(vControles, txtventacontadoaquay)
        CSistema.CargaControl(vControles, txtventacontadocord)
        CSistema.CargaControl(vControles, txtventacontadoconc)
        CSistema.CargaControl(vControles, txtventacontadocde)
        CSistema.CargaControl(vControles, txtventacontadoMolino)

        CSistema.CargaControl(vControles, txtcobradoteso)
        CSistema.CargaControl(vControles, txtcobradocyc)
        CSistema.CargaControl(vControles, txtcobradoaquay)
        CSistema.CargaControl(vControles, txtcobradocord)
        CSistema.CargaControl(vControles, txtcobradoconc)
        CSistema.CargaControl(vControles, txtcobradocde)
        CSistema.CargaControl(vControles, txtcobradomolino)

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereSR As String = ""
        Dim Where2 As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        'Dim frm As New frmReporte
        Dim Orden As Boolean = True
        Dim Comprobante As String = ""
        Dim SP As String = ""
        Dim IDTransaccionEgreso As String = ""
        Dim IDProveedor As String = ""
        Dim TipoComprobante As String = ""

        'frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = "'" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "','" & CSistema.FormatoFechaBaseDatos(txtFechaDesde.txt.Text, True, False) & "','" & CSistema.FormatoFechaBaseDatos(txtFechaHasta.txt.Text, True, False) & "'"
        'WhereSR = "'" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "'"
        'Titulo = "INFORME CONTROL DE TESORERIA"
        'TipoInforme = "Resumen de Recaudaciones - Fecha " & txtDesde.txt.Text

        'If chkMoneda.chk.Checked = True Then
        '    WhereDetalle = " IDMoneda = " & cbxMoneda.GetValue
        '    CReporte.InformeControlTesoreria(frm, Where, WhereSR, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador)
        'Else
        '    WhereDetalle = ""
        '    CReporte.InformeControlTesoreria(frm, Where, WhereSR, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador)
        'End If

        'If chkValCom.chk.Checked = True Then
        '    CReporte.InformeControlTesoreriaValoresyComposicion(frm, Where, WhereSR, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador)
        'End If

        '-----------------------------------------------------------------------------------------------------------------------------------------------'
        ' Muestra el reporte en un visor
        '' Dim reportViewer As New ReportPrintTool(reporte)

        ''reportViewer.PreviewForm.MdiParent = Me ' Opcional, si quieres mostrarlo como una ventana hija
        '' reportViewer.PreviewForm.StartPosition = FormStartPosition.CenterScreen ' Posicionamiento de la ventana
        ''reportViewer.PreviewForm.WindowState = FormWindowState.Normal ' Estado de la ventana (Normal, Maximizada, Minimizada)

        ''Dim previewForm As Form = reportViewer.PreviewForm

        ''previewForm.MinimizeBox = True
        'previewForm.WindowState = FormWindowState.Normal
        'previewForm.ShowInTaskbar = True
        'previewForm.StartPosition = FormStartPosition.CenterScreen
        ' Permitir que la ventana se minimice
        'reportViewer.PreviewForm.MinimizeBox = True ' Habilita el botón de minimizar
        'reportViewer.PreviewForm.ShowInTaskbar = True ' Muestra el reporte en la barra de tareas

        'reportViewer.ShowPreviewDialog()


        ''copiado de CREPORTE

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute  SpViewInformeControlTesoreria1 " & " " & Where,, 300)

        Dim reporte As New TESORERIA()

        Dim carpetaDestino As String = "C:\ReporteTemporal"
        'Dim rutaPDF As String = Path.Combine(carpetaDestino, "ReporteTESORERIA.pdf")
        Dim rutaPDF As String = Path.Combine(carpetaDestino, "ReporteTESORERIA" & Replace(txtDesde.txt.Text, "/", "-") & ".pdf")

        ' Verifica si la carpeta existe, si no, la crea
        If Not Directory.Exists(carpetaDestino) Then
            Directory.CreateDirectory(carpetaDestino)
        End If

        ' Exporta el reporte a PDF en la carpeta especificada
        reporte.ExportToPdf(rutaPDF)

        ' Verifica si el archivo se generó correctamente
        If File.Exists(rutaPDF) Then
            ' Abre el archivo PDF en el navegador predeterminado o en la aplicación predeterminada
            Process.Start(New ProcessStartInfo(rutaPDF) With {.UseShellExecute = True})
        Else
            MessageBox.Show("El archivo PDF no se pudo generar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If


    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmExtractoMovimientoProveedor_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub btnRecaudaciones_Click(sender As Object, e As EventArgs) Handles btnRecaudaciones.Click
        'Recaudaciones()
    End Sub

    Private Sub btnIngreso_Click(sender As Object, e As EventArgs) Handles btnIngreso.Click
        Ingresos()
    End Sub

    Sub Ingresos()
        'Dim Total As String
        Dim Totalasu As String = ""
        Dim Totalaquay As String = ""
        Dim Totalcord As String = ""
        Dim Totalconc As String = ""
        Dim Totalcde As String = ""
        Dim Totalmolino As String = ""
        Dim Totalanuladoasu As String = ""
        Dim Totalanuladoaquay As String = ""
        Dim Totalanuladocord As String = ""
        Dim Totalanuladoconc As String = ""
        Dim Totalanuladocde As String = ""
        Dim Totalanuladomolino As String = ""
        Dim TotalNCasu As String = ""
        Dim TotalNCaquay As String = ""
        Dim TotalNCcord As String = ""
        Dim TotalNCconc As String = ""
        Dim TotalNCcde As String = ""
        Dim TotalNCmolino As String = ""
        Dim cobradoteso As String = ""
        Dim cobradocyc As String = ""
        Dim cobradoaquay As String = ""
        Dim cobradocord As String = ""
        Dim cobradoconc As String = ""
        Dim cobradocde As String = ""
        Dim cobradomolino As String = ""
        Dim Netoasu As String = ""
        Dim Netoaquay As String = ""
        Dim Netocord As String = ""
        Dim Netoconc As String = ""
        Dim Netocde As String = ""
        Dim Netomolino As String = ""
        Dim Valoresdescontado As String = ""
        Dim Valoresdiferido As String = ""

        Totalasu = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'ASU' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 0 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalaquay = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'AQUAY' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 0  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalcord = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'CORD' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 0  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalconc = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'CON' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 0  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalcde = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'CDE' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 0  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalmolino = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'MOL' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 0  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalanuladoasu = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'ASU' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 1 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalanuladoaquay = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'AQUAY' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 1  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalanuladocord = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'CORD' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 1  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalanuladoconc = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'CON' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 1  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalanuladocde = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'CDE' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 1  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Totalanuladomolino = CSistema.ExecuteScalar("Select isnull(sum(total),0) from vventa where Credito = 0 And Sucursal = 'MOL' and IDFormaPagoFactura in (0, 1, 2, 6, 9) and anulado = 1  and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        TotalNCasu = CSistema.ExecuteScalar("Select isnull(sum(importe),0) from VNotaCreditoVentaAplicacionInforme Where Anulado = 'False' and Sucursal = 'ASU' and SubMotivoNotaCredito = 'POR ANULACION' and Condicion = 'CONT' And FechaNC = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        TotalNCaquay = CSistema.ExecuteScalar("Select isnull(sum(importe),0) from VNotaCreditoVentaAplicacionInforme Where Anulado = 'False' and Sucursal = 'AQUAY' and SubMotivoNotaCredito = 'POR ANULACION' and Condicion = 'CONT' And FechaNC = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        TotalNCcord = CSistema.ExecuteScalar("Select isnull(sum(importe),0) from VNotaCreditoVentaAplicacionInforme Where Anulado = 'False' and Sucursal = 'CORD' and SubMotivoNotaCredito = 'POR ANULACION' and Condicion = 'CONT' And FechaNC = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        TotalNCconc = CSistema.ExecuteScalar("Select isnull(sum(importe),0) from VNotaCreditoVentaAplicacionInforme Where Anulado = 'False' and Sucursal = 'CON' and SubMotivoNotaCredito = 'POR ANULACION' and Condicion = 'CONT' And FechaNC = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        TotalNCcde = CSistema.ExecuteScalar("Select isnull(sum(importe),0) from VNotaCreditoVentaAplicacionInforme Where Anulado = 'False' and Sucursal = 'CDE' and SubMotivoNotaCredito = 'POR ANULACION' and Condicion = 'CONT' And FechaNC = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        TotalNCmolino = CSistema.ExecuteScalar("Select isnull(sum(importe),0) from VNotaCreditoVentaAplicacionInforme Where Anulado = 'False' and Sucursal = 'MOL' and SubMotivoNotaCredito = 'POR ANULACION' and Condicion = 'CONT' And FechaNC = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        txtventacontadoasu.txt.Text = CSistema.FormatoNumero(Totalasu)
        txtventacontadoaquay.txt.Text = CSistema.FormatoNumero(Totalaquay)
        txtventacontadocord.txt.Text = CSistema.FormatoNumero(Totalcord)
        txtventacontadoconc.txt.Text = CSistema.FormatoNumero(Totalconc)
        txtventacontadocde.txt.Text = CSistema.FormatoNumero(Totalcde)
        txtventacontadoMolino.txt.Text = CSistema.FormatoNumero(Totalmolino)
        txtventaanuladaasu.txt.Text = CSistema.FormatoNumero(Totalanuladoasu)
        txtventaanuladaaquay.txt.Text = CSistema.FormatoNumero(Totalanuladoaquay)
        txtventaanuladacord.txt.Text = CSistema.FormatoNumero(Totalanuladocord)
        txtventaanuladaconc.txt.Text = CSistema.FormatoNumero(Totalanuladoconc)
        txtventaanuladacde.txt.Text = CSistema.FormatoNumero(Totalanuladocde)
        txtventaanuladamolino.txt.Text = CSistema.FormatoNumero(Totalanuladomolino)
        txtNCasu.txt.Text = CSistema.FormatoNumero(TotalNCasu)
        txtNCaquay.txt.Text = CSistema.FormatoNumero(TotalNCaquay)
        txtNCcord.txt.Text = CSistema.FormatoNumero(TotalNCcord)
        txtNCconc.txt.Text = CSistema.FormatoNumero(TotalNCconc)
        txtNCcde.txt.Text = CSistema.FormatoNumero(TotalNCcde)
        txtNCmolino.txt.Text = CSistema.FormatoNumero(TotalNCmolino)

        cobradoteso = CSistema.ExecuteScalar("select isnull(sum(ImporteTotalValor),0) from VDetalleValoresCobrados where IDUsuario in (192, 265, 466) and IDSucursal = 1 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        cobradocyc = CSistema.ExecuteScalar("select isnull(sum(ImporteTotalValor),0) from VDetalleValoresCobrados where IDUsuario in (405,412) and IDSucursal = 1 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        cobradoaquay = CSistema.ExecuteScalar("select isnull(sum(ImporteTotalValor),0) from VDetalleValoresCobrados where IDSucursal = 5 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        cobradocord = CSistema.ExecuteScalar("select isnull(sum(ImporteTotalValor),0) from VDetalleValoresCobrados where IDSucursal = 9 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        cobradoconc = CSistema.ExecuteScalar("select isnull(sum(ImporteTotalValor),0) from VDetalleValoresCobrados where IDSucursal = 3 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        cobradocde = CSistema.ExecuteScalar("select isnull(sum(ImporteTotalValor),0) from VDetalleValoresCobrados where IDSucursal = 2 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        cobradomolino = CSistema.ExecuteScalar("select isnull(sum(ImporteTotalValor),0) from VDetalleValoresCobrados where IDSucursal = 4 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        txtcobradoteso.txt.Text = CSistema.FormatoNumero(cobradoteso)
        txtcobradocyc.txt.Text = CSistema.FormatoNumero(cobradocyc)
        txtcobradoaquay.txt.Text = CSistema.FormatoNumero(cobradoaquay)
        txtcobradocord.txt.Text = CSistema.FormatoNumero(cobradocord)
        txtcobradoconc.txt.Text = CSistema.FormatoNumero(cobradoconc)
        txtcobradocde.txt.Text = CSistema.FormatoNumero(cobradocde)
        txtcobradomolino.txt.Text = CSistema.FormatoNumero(cobradomolino)

        Netoasu = CSistema.ExecuteScalar("select isnull(sum(TotalSinImpuesto),0) from VDetalleVentaConDevolucion3 where IdMoneda=1 and Anulado = 'False' and CancelarAutomatico = 0 And IDSucursal = 1 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Netoaquay = CSistema.ExecuteScalar("select isnull(sum(TotalSinImpuesto),0) from VDetalleVentaConDevolucion3 where IdMoneda=1 and Anulado = 'False' and CancelarAutomatico = 0 And IDSucursal = 5 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Netocord = CSistema.ExecuteScalar("select isnull(sum(TotalSinImpuesto),0) from VDetalleVentaConDevolucion3 where IdMoneda=1 and Anulado = 'False' and CancelarAutomatico = 0 And IDSucursal = 9 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Netoconc = CSistema.ExecuteScalar("select isnull(sum(TotalSinImpuesto),0) from VDetalleVentaConDevolucion3 where IdMoneda=1 and Anulado = 'False' and CancelarAutomatico = 0 And IDSucursal = 3 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Netocde = CSistema.ExecuteScalar("select isnull(sum(TotalSinImpuesto),0) from VDetalleVentaConDevolucion3 where IdMoneda=1 and Anulado = 'False' and CancelarAutomatico = 0 And IDSucursal = 2 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Netomolino = CSistema.ExecuteScalar("select isnull(sum(TotalSinImpuesto),0) from VDetalleVentaConDevolucion3 where IdMoneda=1 and Anulado = 'False' and CancelarAutomatico = 0 And IDSucursal = 4 and Fecha = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        txtnetoasu.txt.Text = CSistema.FormatoNumero(Netoasu)
        txtnetoaquay.txt.Text = CSistema.FormatoNumero(Netoaquay)
        txtnetocord.txt.Text = CSistema.FormatoNumero(Netocord)
        txtnetoconc.txt.Text = CSistema.FormatoNumero(Netoconc)
        txtnetocde.txt.Text = CSistema.FormatoNumero(Netocde)
        txtnetomolino.txt.Text = CSistema.FormatoNumero(Netomolino)

        Valoresdescontado = CSistema.ExecuteScalar("Select isnull(sum(Total),0) from VChequeCliente where Descontado = 1 and Diferido = 1 and FechaVencimiento = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        Valoresdiferido = CSistema.ExecuteScalar("Select isnull(sum(Total),0) from VChequeCliente where  Descontado = 0 and Diferido = 1 and FechaVencimiento = '" & CSistema.FormatoFechaBaseDatos(txtfecha.txt.Text, True, False) & "'")
        txtvalortotaldescontado.txt.Text = CSistema.FormatoNumero(Valoresdescontado)
        txtvalortotaldiferido.txt.Text = CSistema.FormatoNumero(Valoresdiferido)

        '20240103' 
    End Sub

End Class
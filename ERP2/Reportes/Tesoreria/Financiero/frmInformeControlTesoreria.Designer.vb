﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInformeControlTesoreria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFechaHasta = New ERP.ocxTXTDate()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFechaDesde = New ERP.ocxTXTDate()
        Me.btnIngreso = New System.Windows.Forms.Button()
        Me.btnRecaudaciones = New System.Windows.Forms.Button()
        Me.lblVentaCont = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblNetoSinIVA = New System.Windows.Forms.Label()
        Me.lblvalores = New System.Windows.Forms.Label()
        Me.lblCobrado = New System.Windows.Forms.Label()
        Me.lblDiferido = New System.Windows.Forms.Label()
        Me.lblDescontado = New System.Windows.Forms.Label()
        Me.txtcobradoteso = New ERP.ocxTXTString()
        Me.txtvalortotaldiferido = New ERP.ocxTXTString()
        Me.txtvalortotaldescontado = New ERP.ocxTXTString()
        Me.txtcobradomolino = New ERP.ocxTXTString()
        Me.txtcobradocde = New ERP.ocxTXTString()
        Me.txtcobradoconc = New ERP.ocxTXTString()
        Me.txtcobradocord = New ERP.ocxTXTString()
        Me.txtcobradoaquay = New ERP.ocxTXTString()
        Me.txtcobradocyc = New ERP.ocxTXTString()
        Me.txtnetomolino = New ERP.ocxTXTString()
        Me.txtnetocde = New ERP.ocxTXTString()
        Me.txtnetoconc = New ERP.ocxTXTString()
        Me.txtnetocord = New ERP.ocxTXTString()
        Me.txtnetoaquay = New ERP.ocxTXTString()
        Me.txtventacontadoMolino = New ERP.ocxTXTString()
        Me.txtventacontadocde = New ERP.ocxTXTString()
        Me.txtventacontadoconc = New ERP.ocxTXTString()
        Me.txtventacontadocord = New ERP.ocxTXTString()
        Me.txtventacontadoaquay = New ERP.ocxTXTString()
        Me.txtnetoasu = New ERP.ocxTXTString()
        Me.txtventacontadoasu = New ERP.ocxTXTString()
        Me.txtfecha = New ERP.ocxTXTDate()
        Me.chkValCom = New ERP.ocxCHK()
        Me.lblTeso = New System.Windows.Forms.Label()
        Me.lblCyc = New System.Windows.Forms.Label()
        Me.txtventaanuladamolino = New ERP.ocxTXTString()
        Me.txtventaanuladacde = New ERP.ocxTXTString()
        Me.txtventaanuladaconc = New ERP.ocxTXTString()
        Me.txtventaanuladacord = New ERP.ocxTXTString()
        Me.txtventaanuladaaquay = New ERP.ocxTXTString()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtventaanuladaasu = New ERP.ocxTXTString()
        Me.txtNCmolino = New ERP.ocxTXTString()
        Me.txtNCcde = New ERP.ocxTXTString()
        Me.txtNCconc = New ERP.ocxTXTString()
        Me.txtNCcord = New ERP.ocxTXTString()
        Me.txtNCaquay = New ERP.ocxTXTString()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtNCasu = New ERP.ocxTXTString()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(263, 354)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 11
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(111, 353)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 10
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 223)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(93, 75)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 21)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(40, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Fecha:"
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtDesde.Location = New System.Drawing.Point(9, 37)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 158)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(329, 57)
        Me.gbxFiltro.TabIndex = 8
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(7, 19)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(86, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 2
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(98, 19)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(216, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 3
        Me.cbxMoneda.Texto = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtFechaHasta)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtFechaDesde)
        Me.GroupBox1.Location = New System.Drawing.Point(121, 223)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(220, 75)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Vencimiento de CHQ.DIFERIDO"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(93, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Hasta"
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.AñoFecha = 0
        Me.txtFechaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaHasta.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtFechaHasta.Location = New System.Drawing.Point(96, 37)
        Me.txtFechaHasta.MesFecha = 0
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.PermitirNulo = False
        Me.txtFechaHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaHasta.SoloLectura = False
        Me.txtFechaHasta.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Desde"
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.AñoFecha = 0
        Me.txtFechaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaDesde.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtFechaDesde.Location = New System.Drawing.Point(9, 37)
        Me.txtFechaDesde.MesFecha = 0
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.PermitirNulo = False
        Me.txtFechaDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaDesde.SoloLectura = False
        Me.txtFechaDesde.TabIndex = 1
        '
        'btnIngreso
        '
        Me.btnIngreso.Location = New System.Drawing.Point(12, 41)
        Me.btnIngreso.Name = "btnIngreso"
        Me.btnIngreso.Size = New System.Drawing.Size(125, 23)
        Me.btnIngreso.TabIndex = 12
        Me.btnIngreso.Text = "Ingresos"
        Me.btnIngreso.UseVisualStyleBackColor = True
        '
        'btnRecaudaciones
        '
        Me.btnRecaudaciones.Location = New System.Drawing.Point(12, 75)
        Me.btnRecaudaciones.Name = "btnRecaudaciones"
        Me.btnRecaudaciones.Size = New System.Drawing.Size(125, 23)
        Me.btnRecaudaciones.TabIndex = 13
        Me.btnRecaudaciones.Text = "Recaudaciones"
        Me.btnRecaudaciones.UseVisualStyleBackColor = True
        Me.btnRecaudaciones.Visible = False
        '
        'lblVentaCont
        '
        Me.lblVentaCont.AutoSize = True
        Me.lblVentaCont.Location = New System.Drawing.Point(424, 23)
        Me.lblVentaCont.Name = "lblVentaCont"
        Me.lblVentaCont.Size = New System.Drawing.Size(78, 13)
        Me.lblVentaCont.TabIndex = 4
        Me.lblVentaCont.Text = "Venta Contado"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(366, 46)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "ASU"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(358, 73)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 84
        Me.Label5.Text = "AQUAY"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(365, 127)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 13)
        Me.Label6.TabIndex = 86
        Me.Label6.Text = "CON"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(361, 100)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(38, 13)
        Me.Label7.TabIndex = 85
        Me.Label7.Text = "CORD"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(356, 181)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 88
        Me.Label8.Text = "MOLINO"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(366, 154)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(29, 13)
        Me.Label9.TabIndex = 87
        Me.Label9.Text = "CDE"
        '
        'lblNetoSinIVA
        '
        Me.lblNetoSinIVA.AutoSize = True
        Me.lblNetoSinIVA.Location = New System.Drawing.Point(962, 22)
        Me.lblNetoSinIVA.Name = "lblNetoSinIVA"
        Me.lblNetoSinIVA.Size = New System.Drawing.Size(68, 13)
        Me.lblNetoSinIVA.TabIndex = 89
        Me.lblNetoSinIVA.Text = "Neto Sin IVA"
        '
        'lblvalores
        '
        Me.lblvalores.AutoSize = True
        Me.lblvalores.Location = New System.Drawing.Point(260, 52)
        Me.lblvalores.Name = "lblvalores"
        Me.lblvalores.Size = New System.Drawing.Size(42, 13)
        Me.lblvalores.TabIndex = 96
        Me.lblvalores.Text = "Valores"
        '
        'lblCobrado
        '
        Me.lblCobrado.AutoSize = True
        Me.lblCobrado.Location = New System.Drawing.Point(796, 7)
        Me.lblCobrado.Name = "lblCobrado"
        Me.lblCobrado.Size = New System.Drawing.Size(72, 13)
        Me.lblCobrado.TabIndex = 98
        Me.lblCobrado.Text = "Cobrado Cont"
        '
        'lblDiferido
        '
        Me.lblDiferido.AutoSize = True
        Me.lblDiferido.Location = New System.Drawing.Point(166, 75)
        Me.lblDiferido.Name = "lblDiferido"
        Me.lblDiferido.Size = New System.Drawing.Size(62, 13)
        Me.lblDiferido.TabIndex = 107
        Me.lblDiferido.Text = "Ch. Diferido"
        '
        'lblDescontado
        '
        Me.lblDescontado.AutoSize = True
        Me.lblDescontado.Location = New System.Drawing.Point(166, 104)
        Me.lblDescontado.Name = "lblDescontado"
        Me.lblDescontado.Size = New System.Drawing.Size(65, 13)
        Me.lblDescontado.TabIndex = 106
        Me.lblDescontado.Text = "Descontado"
        Me.lblDescontado.Visible = False
        '
        'txtcobradoteso
        '
        Me.txtcobradoteso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcobradoteso.Color = System.Drawing.Color.Empty
        Me.txtcobradoteso.Indicaciones = Nothing
        Me.txtcobradoteso.Location = New System.Drawing.Point(736, 41)
        Me.txtcobradoteso.Multilinea = False
        Me.txtcobradoteso.Name = "txtcobradoteso"
        Me.txtcobradoteso.Size = New System.Drawing.Size(104, 21)
        Me.txtcobradoteso.SoloLectura = False
        Me.txtcobradoteso.TabIndex = 108
        Me.txtcobradoteso.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcobradoteso.Texto = ""
        '
        'txtvalortotaldiferido
        '
        Me.txtvalortotaldiferido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtvalortotaldiferido.Color = System.Drawing.Color.Empty
        Me.txtvalortotaldiferido.Indicaciones = Nothing
        Me.txtvalortotaldiferido.Location = New System.Drawing.Point(237, 75)
        Me.txtvalortotaldiferido.Multilinea = False
        Me.txtvalortotaldiferido.Name = "txtvalortotaldiferido"
        Me.txtvalortotaldiferido.Size = New System.Drawing.Size(104, 21)
        Me.txtvalortotaldiferido.SoloLectura = False
        Me.txtvalortotaldiferido.TabIndex = 105
        Me.txtvalortotaldiferido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtvalortotaldiferido.Texto = ""
        '
        'txtvalortotaldescontado
        '
        Me.txtvalortotaldescontado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtvalortotaldescontado.Color = System.Drawing.Color.Empty
        Me.txtvalortotaldescontado.Indicaciones = Nothing
        Me.txtvalortotaldescontado.Location = New System.Drawing.Point(237, 104)
        Me.txtvalortotaldescontado.Multilinea = False
        Me.txtvalortotaldescontado.Name = "txtvalortotaldescontado"
        Me.txtvalortotaldescontado.Size = New System.Drawing.Size(104, 21)
        Me.txtvalortotaldescontado.SoloLectura = False
        Me.txtvalortotaldescontado.TabIndex = 104
        Me.txtvalortotaldescontado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtvalortotaldescontado.Texto = ""
        Me.txtvalortotaldescontado.Visible = False
        '
        'txtcobradomolino
        '
        Me.txtcobradomolino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcobradomolino.Color = System.Drawing.Color.Empty
        Me.txtcobradomolino.Indicaciones = Nothing
        Me.txtcobradomolino.Location = New System.Drawing.Point(799, 177)
        Me.txtcobradomolino.Multilinea = False
        Me.txtcobradomolino.Name = "txtcobradomolino"
        Me.txtcobradomolino.Size = New System.Drawing.Size(104, 21)
        Me.txtcobradomolino.SoloLectura = False
        Me.txtcobradomolino.TabIndex = 103
        Me.txtcobradomolino.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcobradomolino.Texto = ""
        '
        'txtcobradocde
        '
        Me.txtcobradocde.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcobradocde.Color = System.Drawing.Color.Empty
        Me.txtcobradocde.Indicaciones = Nothing
        Me.txtcobradocde.Location = New System.Drawing.Point(799, 150)
        Me.txtcobradocde.Multilinea = False
        Me.txtcobradocde.Name = "txtcobradocde"
        Me.txtcobradocde.Size = New System.Drawing.Size(104, 21)
        Me.txtcobradocde.SoloLectura = False
        Me.txtcobradocde.TabIndex = 102
        Me.txtcobradocde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcobradocde.Texto = ""
        '
        'txtcobradoconc
        '
        Me.txtcobradoconc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcobradoconc.Color = System.Drawing.Color.Empty
        Me.txtcobradoconc.Indicaciones = Nothing
        Me.txtcobradoconc.Location = New System.Drawing.Point(799, 123)
        Me.txtcobradoconc.Multilinea = False
        Me.txtcobradoconc.Name = "txtcobradoconc"
        Me.txtcobradoconc.Size = New System.Drawing.Size(104, 21)
        Me.txtcobradoconc.SoloLectura = False
        Me.txtcobradoconc.TabIndex = 101
        Me.txtcobradoconc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcobradoconc.Texto = ""
        '
        'txtcobradocord
        '
        Me.txtcobradocord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcobradocord.Color = System.Drawing.Color.Empty
        Me.txtcobradocord.Indicaciones = Nothing
        Me.txtcobradocord.Location = New System.Drawing.Point(799, 96)
        Me.txtcobradocord.Multilinea = False
        Me.txtcobradocord.Name = "txtcobradocord"
        Me.txtcobradocord.Size = New System.Drawing.Size(104, 21)
        Me.txtcobradocord.SoloLectura = False
        Me.txtcobradocord.TabIndex = 100
        Me.txtcobradocord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcobradocord.Texto = ""
        '
        'txtcobradoaquay
        '
        Me.txtcobradoaquay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcobradoaquay.Color = System.Drawing.Color.Empty
        Me.txtcobradoaquay.Indicaciones = Nothing
        Me.txtcobradoaquay.Location = New System.Drawing.Point(799, 69)
        Me.txtcobradoaquay.Multilinea = False
        Me.txtcobradoaquay.Name = "txtcobradoaquay"
        Me.txtcobradoaquay.Size = New System.Drawing.Size(104, 21)
        Me.txtcobradoaquay.SoloLectura = False
        Me.txtcobradoaquay.TabIndex = 99
        Me.txtcobradoaquay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcobradoaquay.Texto = ""
        '
        'txtcobradocyc
        '
        Me.txtcobradocyc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcobradocyc.Color = System.Drawing.Color.Empty
        Me.txtcobradocyc.Indicaciones = Nothing
        Me.txtcobradocyc.Location = New System.Drawing.Point(846, 41)
        Me.txtcobradocyc.Multilinea = False
        Me.txtcobradocyc.Name = "txtcobradocyc"
        Me.txtcobradocyc.Size = New System.Drawing.Size(104, 21)
        Me.txtcobradocyc.SoloLectura = False
        Me.txtcobradocyc.TabIndex = 97
        Me.txtcobradocyc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtcobradocyc.Texto = ""
        '
        'txtnetomolino
        '
        Me.txtnetomolino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnetomolino.Color = System.Drawing.Color.Empty
        Me.txtnetomolino.Indicaciones = Nothing
        Me.txtnetomolino.Location = New System.Drawing.Point(956, 177)
        Me.txtnetomolino.Multilinea = False
        Me.txtnetomolino.Name = "txtnetomolino"
        Me.txtnetomolino.Size = New System.Drawing.Size(104, 21)
        Me.txtnetomolino.SoloLectura = False
        Me.txtnetomolino.TabIndex = 94
        Me.txtnetomolino.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtnetomolino.Texto = ""
        '
        'txtnetocde
        '
        Me.txtnetocde.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnetocde.Color = System.Drawing.Color.Empty
        Me.txtnetocde.Indicaciones = Nothing
        Me.txtnetocde.Location = New System.Drawing.Point(956, 150)
        Me.txtnetocde.Multilinea = False
        Me.txtnetocde.Name = "txtnetocde"
        Me.txtnetocde.Size = New System.Drawing.Size(104, 21)
        Me.txtnetocde.SoloLectura = False
        Me.txtnetocde.TabIndex = 93
        Me.txtnetocde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtnetocde.Texto = ""
        '
        'txtnetoconc
        '
        Me.txtnetoconc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnetoconc.Color = System.Drawing.Color.Empty
        Me.txtnetoconc.Indicaciones = Nothing
        Me.txtnetoconc.Location = New System.Drawing.Point(956, 123)
        Me.txtnetoconc.Multilinea = False
        Me.txtnetoconc.Name = "txtnetoconc"
        Me.txtnetoconc.Size = New System.Drawing.Size(104, 21)
        Me.txtnetoconc.SoloLectura = False
        Me.txtnetoconc.TabIndex = 92
        Me.txtnetoconc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtnetoconc.Texto = ""
        '
        'txtnetocord
        '
        Me.txtnetocord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnetocord.Color = System.Drawing.Color.Empty
        Me.txtnetocord.Indicaciones = Nothing
        Me.txtnetocord.Location = New System.Drawing.Point(956, 96)
        Me.txtnetocord.Multilinea = False
        Me.txtnetocord.Name = "txtnetocord"
        Me.txtnetocord.Size = New System.Drawing.Size(104, 21)
        Me.txtnetocord.SoloLectura = False
        Me.txtnetocord.TabIndex = 91
        Me.txtnetocord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtnetocord.Texto = ""
        '
        'txtnetoaquay
        '
        Me.txtnetoaquay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnetoaquay.Color = System.Drawing.Color.Empty
        Me.txtnetoaquay.Indicaciones = Nothing
        Me.txtnetoaquay.Location = New System.Drawing.Point(956, 69)
        Me.txtnetoaquay.Multilinea = False
        Me.txtnetoaquay.Name = "txtnetoaquay"
        Me.txtnetoaquay.Size = New System.Drawing.Size(104, 21)
        Me.txtnetoaquay.SoloLectura = False
        Me.txtnetoaquay.TabIndex = 90
        Me.txtnetoaquay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtnetoaquay.Texto = ""
        '
        'txtventacontadoMolino
        '
        Me.txtventacontadoMolino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventacontadoMolino.Color = System.Drawing.Color.Empty
        Me.txtventacontadoMolino.Indicaciones = Nothing
        Me.txtventacontadoMolino.Location = New System.Drawing.Point(411, 179)
        Me.txtventacontadoMolino.Multilinea = False
        Me.txtventacontadoMolino.Name = "txtventacontadoMolino"
        Me.txtventacontadoMolino.Size = New System.Drawing.Size(98, 21)
        Me.txtventacontadoMolino.SoloLectura = False
        Me.txtventacontadoMolino.TabIndex = 83
        Me.txtventacontadoMolino.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventacontadoMolino.Texto = ""
        '
        'txtventacontadocde
        '
        Me.txtventacontadocde.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventacontadocde.Color = System.Drawing.Color.Empty
        Me.txtventacontadocde.Indicaciones = Nothing
        Me.txtventacontadocde.Location = New System.Drawing.Point(411, 152)
        Me.txtventacontadocde.Multilinea = False
        Me.txtventacontadocde.Name = "txtventacontadocde"
        Me.txtventacontadocde.Size = New System.Drawing.Size(98, 21)
        Me.txtventacontadocde.SoloLectura = False
        Me.txtventacontadocde.TabIndex = 82
        Me.txtventacontadocde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventacontadocde.Texto = ""
        '
        'txtventacontadoconc
        '
        Me.txtventacontadoconc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventacontadoconc.Color = System.Drawing.Color.Empty
        Me.txtventacontadoconc.Indicaciones = Nothing
        Me.txtventacontadoconc.Location = New System.Drawing.Point(411, 125)
        Me.txtventacontadoconc.Multilinea = False
        Me.txtventacontadoconc.Name = "txtventacontadoconc"
        Me.txtventacontadoconc.Size = New System.Drawing.Size(98, 21)
        Me.txtventacontadoconc.SoloLectura = False
        Me.txtventacontadoconc.TabIndex = 81
        Me.txtventacontadoconc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventacontadoconc.Texto = ""
        '
        'txtventacontadocord
        '
        Me.txtventacontadocord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventacontadocord.Color = System.Drawing.Color.Empty
        Me.txtventacontadocord.Indicaciones = Nothing
        Me.txtventacontadocord.Location = New System.Drawing.Point(411, 98)
        Me.txtventacontadocord.Multilinea = False
        Me.txtventacontadocord.Name = "txtventacontadocord"
        Me.txtventacontadocord.Size = New System.Drawing.Size(98, 21)
        Me.txtventacontadocord.SoloLectura = False
        Me.txtventacontadocord.TabIndex = 80
        Me.txtventacontadocord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventacontadocord.Texto = ""
        '
        'txtventacontadoaquay
        '
        Me.txtventacontadoaquay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventacontadoaquay.Color = System.Drawing.Color.Empty
        Me.txtventacontadoaquay.Indicaciones = Nothing
        Me.txtventacontadoaquay.Location = New System.Drawing.Point(411, 71)
        Me.txtventacontadoaquay.Multilinea = False
        Me.txtventacontadoaquay.Name = "txtventacontadoaquay"
        Me.txtventacontadoaquay.Size = New System.Drawing.Size(98, 21)
        Me.txtventacontadoaquay.SoloLectura = False
        Me.txtventacontadoaquay.TabIndex = 79
        Me.txtventacontadoaquay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventacontadoaquay.Texto = ""
        '
        'txtnetoasu
        '
        Me.txtnetoasu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtnetoasu.Color = System.Drawing.Color.Empty
        Me.txtnetoasu.Indicaciones = Nothing
        Me.txtnetoasu.Location = New System.Drawing.Point(956, 41)
        Me.txtnetoasu.Multilinea = False
        Me.txtnetoasu.Name = "txtnetoasu"
        Me.txtnetoasu.Size = New System.Drawing.Size(104, 21)
        Me.txtnetoasu.SoloLectura = False
        Me.txtnetoasu.TabIndex = 78
        Me.txtnetoasu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtnetoasu.Texto = ""
        '
        'txtventacontadoasu
        '
        Me.txtventacontadoasu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventacontadoasu.Color = System.Drawing.Color.Empty
        Me.txtventacontadoasu.Indicaciones = Nothing
        Me.txtventacontadoasu.Location = New System.Drawing.Point(411, 44)
        Me.txtventacontadoasu.Multilinea = False
        Me.txtventacontadoasu.Name = "txtventacontadoasu"
        Me.txtventacontadoasu.Size = New System.Drawing.Size(98, 21)
        Me.txtventacontadoasu.SoloLectura = False
        Me.txtventacontadoasu.TabIndex = 77
        Me.txtventacontadoasu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventacontadoasu.Texto = ""
        '
        'txtfecha
        '
        Me.txtfecha.AñoFecha = 0
        Me.txtfecha.Color = System.Drawing.Color.Empty
        Me.txtfecha.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtfecha.Location = New System.Drawing.Point(152, 43)
        Me.txtfecha.MesFecha = 0
        Me.txtfecha.Name = "txtfecha"
        Me.txtfecha.PermitirNulo = False
        Me.txtfecha.Size = New System.Drawing.Size(74, 20)
        Me.txtfecha.SoloLectura = False
        Me.txtfecha.TabIndex = 4
        '
        'chkValCom
        '
        Me.chkValCom.BackColor = System.Drawing.Color.Transparent
        Me.chkValCom.Color = System.Drawing.Color.Empty
        Me.chkValCom.Location = New System.Drawing.Point(19, 304)
        Me.chkValCom.Name = "chkValCom"
        Me.chkValCom.Size = New System.Drawing.Size(132, 21)
        Me.chkValCom.SoloLectura = False
        Me.chkValCom.TabIndex = 4
        Me.chkValCom.Texto = "Valores y Composición"
        Me.chkValCom.Valor = False
        Me.chkValCom.Visible = False
        '
        'lblTeso
        '
        Me.lblTeso.AutoSize = True
        Me.lblTeso.Location = New System.Drawing.Point(740, 25)
        Me.lblTeso.Name = "lblTeso"
        Me.lblTeso.Size = New System.Drawing.Size(96, 13)
        Me.lblTeso.TabIndex = 109
        Me.lblTeso.Text = "Cobrado Tesorería"
        '
        'lblCyc
        '
        Me.lblCyc.AutoSize = True
        Me.lblCyc.Location = New System.Drawing.Point(855, 25)
        Me.lblCyc.Name = "lblCyc"
        Me.lblCyc.Size = New System.Drawing.Size(75, 13)
        Me.lblCyc.TabIndex = 110
        Me.lblCyc.Text = "Cobrado C y C"
        '
        'txtventaanuladamolino
        '
        Me.txtventaanuladamolino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventaanuladamolino.Color = System.Drawing.Color.Empty
        Me.txtventaanuladamolino.Indicaciones = Nothing
        Me.txtventaanuladamolino.Location = New System.Drawing.Point(516, 179)
        Me.txtventaanuladamolino.Multilinea = False
        Me.txtventaanuladamolino.Name = "txtventaanuladamolino"
        Me.txtventaanuladamolino.Size = New System.Drawing.Size(99, 21)
        Me.txtventaanuladamolino.SoloLectura = False
        Me.txtventaanuladamolino.TabIndex = 117
        Me.txtventaanuladamolino.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventaanuladamolino.Texto = ""
        '
        'txtventaanuladacde
        '
        Me.txtventaanuladacde.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventaanuladacde.Color = System.Drawing.Color.Empty
        Me.txtventaanuladacde.Indicaciones = Nothing
        Me.txtventaanuladacde.Location = New System.Drawing.Point(516, 152)
        Me.txtventaanuladacde.Multilinea = False
        Me.txtventaanuladacde.Name = "txtventaanuladacde"
        Me.txtventaanuladacde.Size = New System.Drawing.Size(99, 21)
        Me.txtventaanuladacde.SoloLectura = False
        Me.txtventaanuladacde.TabIndex = 116
        Me.txtventaanuladacde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventaanuladacde.Texto = ""
        '
        'txtventaanuladaconc
        '
        Me.txtventaanuladaconc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventaanuladaconc.Color = System.Drawing.Color.Empty
        Me.txtventaanuladaconc.Indicaciones = Nothing
        Me.txtventaanuladaconc.Location = New System.Drawing.Point(516, 125)
        Me.txtventaanuladaconc.Multilinea = False
        Me.txtventaanuladaconc.Name = "txtventaanuladaconc"
        Me.txtventaanuladaconc.Size = New System.Drawing.Size(99, 21)
        Me.txtventaanuladaconc.SoloLectura = False
        Me.txtventaanuladaconc.TabIndex = 115
        Me.txtventaanuladaconc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventaanuladaconc.Texto = ""
        '
        'txtventaanuladacord
        '
        Me.txtventaanuladacord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventaanuladacord.Color = System.Drawing.Color.Empty
        Me.txtventaanuladacord.Indicaciones = Nothing
        Me.txtventaanuladacord.Location = New System.Drawing.Point(516, 98)
        Me.txtventaanuladacord.Multilinea = False
        Me.txtventaanuladacord.Name = "txtventaanuladacord"
        Me.txtventaanuladacord.Size = New System.Drawing.Size(99, 21)
        Me.txtventaanuladacord.SoloLectura = False
        Me.txtventaanuladacord.TabIndex = 114
        Me.txtventaanuladacord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventaanuladacord.Texto = ""
        '
        'txtventaanuladaaquay
        '
        Me.txtventaanuladaaquay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventaanuladaaquay.Color = System.Drawing.Color.Empty
        Me.txtventaanuladaaquay.Indicaciones = Nothing
        Me.txtventaanuladaaquay.Location = New System.Drawing.Point(516, 71)
        Me.txtventaanuladaaquay.Multilinea = False
        Me.txtventaanuladaaquay.Name = "txtventaanuladaaquay"
        Me.txtventaanuladaaquay.Size = New System.Drawing.Size(99, 21)
        Me.txtventaanuladaaquay.SoloLectura = False
        Me.txtventaanuladaaquay.TabIndex = 113
        Me.txtventaanuladaaquay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventaanuladaaquay.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(529, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 13)
        Me.Label3.TabIndex = 111
        Me.Label3.Text = "Venta Anulada"
        '
        'txtventaanuladaasu
        '
        Me.txtventaanuladaasu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtventaanuladaasu.Color = System.Drawing.Color.Empty
        Me.txtventaanuladaasu.Indicaciones = Nothing
        Me.txtventaanuladaasu.Location = New System.Drawing.Point(516, 44)
        Me.txtventaanuladaasu.Multilinea = False
        Me.txtventaanuladaasu.Name = "txtventaanuladaasu"
        Me.txtventaanuladaasu.Size = New System.Drawing.Size(99, 21)
        Me.txtventaanuladaasu.SoloLectura = False
        Me.txtventaanuladaasu.TabIndex = 112
        Me.txtventaanuladaasu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtventaanuladaasu.Texto = ""
        '
        'txtNCmolino
        '
        Me.txtNCmolino.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNCmolino.Color = System.Drawing.Color.Empty
        Me.txtNCmolino.Indicaciones = Nothing
        Me.txtNCmolino.Location = New System.Drawing.Point(621, 178)
        Me.txtNCmolino.Multilinea = False
        Me.txtNCmolino.Name = "txtNCmolino"
        Me.txtNCmolino.Size = New System.Drawing.Size(99, 21)
        Me.txtNCmolino.SoloLectura = False
        Me.txtNCmolino.TabIndex = 124
        Me.txtNCmolino.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNCmolino.Texto = ""
        '
        'txtNCcde
        '
        Me.txtNCcde.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNCcde.Color = System.Drawing.Color.Empty
        Me.txtNCcde.Indicaciones = Nothing
        Me.txtNCcde.Location = New System.Drawing.Point(621, 151)
        Me.txtNCcde.Multilinea = False
        Me.txtNCcde.Name = "txtNCcde"
        Me.txtNCcde.Size = New System.Drawing.Size(99, 21)
        Me.txtNCcde.SoloLectura = False
        Me.txtNCcde.TabIndex = 123
        Me.txtNCcde.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNCcde.Texto = ""
        '
        'txtNCconc
        '
        Me.txtNCconc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNCconc.Color = System.Drawing.Color.Empty
        Me.txtNCconc.Indicaciones = Nothing
        Me.txtNCconc.Location = New System.Drawing.Point(621, 124)
        Me.txtNCconc.Multilinea = False
        Me.txtNCconc.Name = "txtNCconc"
        Me.txtNCconc.Size = New System.Drawing.Size(99, 21)
        Me.txtNCconc.SoloLectura = False
        Me.txtNCconc.TabIndex = 122
        Me.txtNCconc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNCconc.Texto = ""
        '
        'txtNCcord
        '
        Me.txtNCcord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNCcord.Color = System.Drawing.Color.Empty
        Me.txtNCcord.Indicaciones = Nothing
        Me.txtNCcord.Location = New System.Drawing.Point(621, 97)
        Me.txtNCcord.Multilinea = False
        Me.txtNCcord.Name = "txtNCcord"
        Me.txtNCcord.Size = New System.Drawing.Size(99, 21)
        Me.txtNCcord.SoloLectura = False
        Me.txtNCcord.TabIndex = 121
        Me.txtNCcord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNCcord.Texto = ""
        '
        'txtNCaquay
        '
        Me.txtNCaquay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNCaquay.Color = System.Drawing.Color.Empty
        Me.txtNCaquay.Indicaciones = Nothing
        Me.txtNCaquay.Location = New System.Drawing.Point(621, 70)
        Me.txtNCaquay.Multilinea = False
        Me.txtNCaquay.Name = "txtNCaquay"
        Me.txtNCaquay.Size = New System.Drawing.Size(99, 21)
        Me.txtNCaquay.SoloLectura = False
        Me.txtNCaquay.TabIndex = 120
        Me.txtNCaquay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNCaquay.Texto = ""
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(628, 23)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(86, 13)
        Me.Label10.TabIndex = 118
        Me.Label10.Text = "Notas de Crédito"
        '
        'txtNCasu
        '
        Me.txtNCasu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNCasu.Color = System.Drawing.Color.Empty
        Me.txtNCasu.Indicaciones = Nothing
        Me.txtNCasu.Location = New System.Drawing.Point(621, 43)
        Me.txtNCasu.Multilinea = False
        Me.txtNCasu.Name = "txtNCasu"
        Me.txtNCasu.Size = New System.Drawing.Size(99, 21)
        Me.txtNCasu.SoloLectura = False
        Me.txtNCasu.TabIndex = 119
        Me.txtNCasu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNCasu.Texto = ""
        '
        'frmInformeControlTesoreria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1106, 403)
        Me.Controls.Add(Me.txtNCmolino)
        Me.Controls.Add(Me.txtNCcde)
        Me.Controls.Add(Me.txtNCconc)
        Me.Controls.Add(Me.txtNCcord)
        Me.Controls.Add(Me.txtNCaquay)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtNCasu)
        Me.Controls.Add(Me.txtventaanuladamolino)
        Me.Controls.Add(Me.txtventaanuladacde)
        Me.Controls.Add(Me.txtventaanuladaconc)
        Me.Controls.Add(Me.txtventaanuladacord)
        Me.Controls.Add(Me.txtventaanuladaaquay)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtventaanuladaasu)
        Me.Controls.Add(Me.lblCyc)
        Me.Controls.Add(Me.lblTeso)
        Me.Controls.Add(Me.chkValCom)
        Me.Controls.Add(Me.txtcobradoteso)
        Me.Controls.Add(Me.lblDiferido)
        Me.Controls.Add(Me.lblDescontado)
        Me.Controls.Add(Me.txtvalortotaldiferido)
        Me.Controls.Add(Me.txtvalortotaldescontado)
        Me.Controls.Add(Me.txtcobradomolino)
        Me.Controls.Add(Me.txtcobradocde)
        Me.Controls.Add(Me.txtcobradoconc)
        Me.Controls.Add(Me.txtcobradocord)
        Me.Controls.Add(Me.txtcobradoaquay)
        Me.Controls.Add(Me.lblCobrado)
        Me.Controls.Add(Me.txtcobradocyc)
        Me.Controls.Add(Me.lblvalores)
        Me.Controls.Add(Me.txtnetomolino)
        Me.Controls.Add(Me.txtnetocde)
        Me.Controls.Add(Me.txtnetoconc)
        Me.Controls.Add(Me.txtnetocord)
        Me.Controls.Add(Me.txtnetoaquay)
        Me.Controls.Add(Me.lblNetoSinIVA)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtventacontadoMolino)
        Me.Controls.Add(Me.txtventacontadocde)
        Me.Controls.Add(Me.txtventacontadoconc)
        Me.Controls.Add(Me.txtventacontadocord)
        Me.Controls.Add(Me.txtventacontadoaquay)
        Me.Controls.Add(Me.txtnetoasu)
        Me.Controls.Add(Me.lblVentaCont)
        Me.Controls.Add(Me.txtventacontadoasu)
        Me.Controls.Add(Me.txtfecha)
        Me.Controls.Add(Me.btnRecaudaciones)
        Me.Controls.Add(Me.btnIngreso)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmInformeControlTesoreria"
        Me.Text = "frmInformeControlTesoreria"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Public WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Public WithEvents txtFechaHasta As ERP.ocxTXTDate
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents txtFechaDesde As ERP.ocxTXTDate
    Friend WithEvents btnIngreso As Button
    Friend WithEvents btnRecaudaciones As Button
    Public WithEvents txtfecha As ocxTXTDate
    Friend WithEvents txtventacontadoasu As ocxTXTString
    Friend WithEvents lblVentaCont As Label
    Friend WithEvents txtnetoasu As ocxTXTString
    Friend WithEvents txtventacontadoaquay As ocxTXTString
    Friend WithEvents txtventacontadocord As ocxTXTString
    Friend WithEvents txtventacontadoconc As ocxTXTString
    Friend WithEvents txtventacontadocde As ocxTXTString
    Friend WithEvents txtventacontadoMolino As ocxTXTString
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents lblNetoSinIVA As Label
    Friend WithEvents txtnetoaquay As ocxTXTString
    Friend WithEvents txtnetoconc As ocxTXTString
    Friend WithEvents txtnetocord As ocxTXTString
    Friend WithEvents txtnetomolino As ocxTXTString
    Friend WithEvents txtnetocde As ocxTXTString
    Friend WithEvents txtvalores As ocxTXTString
    Friend WithEvents lblvalores As Label
    Friend WithEvents txtcobradomolino As ocxTXTString
    Friend WithEvents txtcobradocde As ocxTXTString
    Friend WithEvents txtcobradoconc As ocxTXTString
    Friend WithEvents txtcobradocord As ocxTXTString
    Friend WithEvents txtcobradoaquay As ocxTXTString
    Friend WithEvents lblCobrado As Label
    Friend WithEvents txtcobradocyc As ocxTXTString
    Friend WithEvents txtvalortotaldescontado As ocxTXTString
    Friend WithEvents txtvalortotaldiferido As ocxTXTString
    Friend WithEvents lblDiferido As Label
    Friend WithEvents lblDescontado As Label
    Friend WithEvents txtcobradoteso As ocxTXTString
    Friend WithEvents chkMoneda As ocxCHK
    Friend WithEvents chkValCom As ocxCHK
    Friend WithEvents lblTeso As Label
    Friend WithEvents lblCyc As Label
    Friend WithEvents txtventaanuladamolino As ocxTXTString
    Friend WithEvents txtventaanuladacde As ocxTXTString
    Friend WithEvents txtventaanuladaconc As ocxTXTString
    Friend WithEvents txtventaanuladacord As ocxTXTString
    Friend WithEvents txtventaanuladaaquay As ocxTXTString
    Friend WithEvents Label3 As Label
    Friend WithEvents txtventaanuladaasu As ocxTXTString
    Friend WithEvents txtNCmolino As ocxTXTString
    Friend WithEvents txtNCcde As ocxTXTString
    Friend WithEvents txtNCconc As ocxTXTString
    Friend WithEvents txtNCcord As ocxTXTString
    Friend WithEvents txtNCaquay As ocxTXTString
    Friend WithEvents Label10 As Label
    Friend WithEvents txtNCasu As ocxTXTString
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoRecibosGenerados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblAnulado = New System.Windows.Forms.Label()
        Me.cbxTipoListado = New ERP.ocxCBX()
        Me.chkEstado = New ERP.ocxCHK()
        Me.cbxEstado = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkRecibo = New ERP.ocxCHK()
        Me.txtReciboHasta = New System.Windows.Forms.TextBox()
        Me.txtReciboDesde = New System.Windows.Forms.TextBox()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(593, 227)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblAnulado)
        Me.GroupBox2.Controls.Add(Me.cbxTipoListado)
        Me.GroupBox2.Controls.Add(Me.chkEstado)
        Me.GroupBox2.Controls.Add(Me.cbxEstado)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(408, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 209)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblAnulado
        '
        Me.lblAnulado.AutoSize = True
        Me.lblAnulado.Location = New System.Drawing.Point(8, 162)
        Me.lblAnulado.Name = "lblAnulado"
        Me.lblAnulado.Size = New System.Drawing.Size(83, 13)
        Me.lblAnulado.TabIndex = 14
        Me.lblAnulado.Text = "Tipo de Listado:"
        '
        'cbxTipoListado
        '
        Me.cbxTipoListado.CampoWhere = Nothing
        Me.cbxTipoListado.CargarUnaSolaVez = False
        Me.cbxTipoListado.DataDisplayMember = Nothing
        Me.cbxTipoListado.DataFilter = Nothing
        Me.cbxTipoListado.DataOrderBy = Nothing
        Me.cbxTipoListado.DataSource = Nothing
        Me.cbxTipoListado.DataValueMember = Nothing
        Me.cbxTipoListado.dtSeleccionado = Nothing
        Me.cbxTipoListado.FormABM = Nothing
        Me.cbxTipoListado.Indicaciones = Nothing
        Me.cbxTipoListado.Location = New System.Drawing.Point(11, 179)
        Me.cbxTipoListado.Name = "cbxTipoListado"
        Me.cbxTipoListado.SeleccionMultiple = False
        Me.cbxTipoListado.SeleccionObligatoria = True
        Me.cbxTipoListado.Size = New System.Drawing.Size(227, 20)
        Me.cbxTipoListado.SoloLectura = False
        Me.cbxTipoListado.TabIndex = 13
        Me.cbxTipoListado.Texto = ""
        '
        'chkEstado
        '
        Me.chkEstado.BackColor = System.Drawing.Color.Transparent
        Me.chkEstado.Color = System.Drawing.Color.Empty
        Me.chkEstado.Location = New System.Drawing.Point(7, 69)
        Me.chkEstado.Name = "chkEstado"
        Me.chkEstado.Size = New System.Drawing.Size(113, 16)
        Me.chkEstado.SoloLectura = False
        Me.chkEstado.TabIndex = 11
        Me.chkEstado.Texto = "Estado:"
        Me.chkEstado.Valor = False
        '
        'cbxEstado
        '
        Me.cbxEstado.CampoWhere = ""
        Me.cbxEstado.CargarUnaSolaVez = False
        Me.cbxEstado.DataDisplayMember = ""
        Me.cbxEstado.DataFilter = Nothing
        Me.cbxEstado.DataOrderBy = Nothing
        Me.cbxEstado.DataSource = ""
        Me.cbxEstado.DataValueMember = ""
        Me.cbxEstado.dtSeleccionado = Nothing
        Me.cbxEstado.Enabled = False
        Me.cbxEstado.FormABM = Nothing
        Me.cbxEstado.Indicaciones = Nothing
        Me.cbxEstado.Location = New System.Drawing.Point(7, 87)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.SeleccionMultiple = False
        Me.cbxEstado.SeleccionObligatoria = True
        Me.cbxEstado.Size = New System.Drawing.Size(231, 21)
        Me.cbxEstado.SoloLectura = False
        Me.cbxEstado.TabIndex = 9
        Me.cbxEstado.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(165, 132)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(7, 132)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(7, 115)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(7, 20)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtHasta.Location = New System.Drawing.Point(90, 39)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 42, 32, 311)
        Me.txtDesde.Location = New System.Drawing.Point(7, 39)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkRecibo)
        Me.gbxFiltro.Controls.Add(Me.txtReciboHasta)
        Me.gbxFiltro.Controls.Add(Me.txtReciboDesde)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(390, 209)
        Me.gbxFiltro.TabIndex = 6
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(15, 127)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(369, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 21
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(13, 107)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(99, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 20
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkRecibo
        '
        Me.chkRecibo.BackColor = System.Drawing.Color.Transparent
        Me.chkRecibo.Color = System.Drawing.Color.Empty
        Me.chkRecibo.Location = New System.Drawing.Point(13, 46)
        Me.chkRecibo.Name = "chkRecibo"
        Me.chkRecibo.Size = New System.Drawing.Size(68, 21)
        Me.chkRecibo.SoloLectura = False
        Me.chkRecibo.TabIndex = 8
        Me.chkRecibo.Texto = "N° Recibo:"
        Me.chkRecibo.Valor = False
        '
        'txtReciboHasta
        '
        Me.txtReciboHasta.Enabled = False
        Me.txtReciboHasta.Location = New System.Drawing.Point(198, 47)
        Me.txtReciboHasta.Name = "txtReciboHasta"
        Me.txtReciboHasta.Size = New System.Drawing.Size(100, 20)
        Me.txtReciboHasta.TabIndex = 7
        '
        'txtReciboDesde
        '
        Me.txtReciboDesde.Enabled = False
        Me.txtReciboDesde.Location = New System.Drawing.Point(85, 47)
        Me.txtReciboDesde.Name = "txtReciboDesde"
        Me.txtReciboDesde.Size = New System.Drawing.Size(100, 20)
        Me.txtReciboDesde.TabIndex = 6
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(13, 76)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(68, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(85, 76)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.Texto = ""
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(408, 227)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 5
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmListadoRecibosGenerados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(729, 279)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoRecibosGenerados"
        Me.Text = "frmListadoRecibosGenerados"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkEstado As ERP.ocxCHK
    Friend WithEvents cbxEstado As ERP.ocxCBX
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkRecibo As ERP.ocxCHK
    Friend WithEvents txtReciboHasta As System.Windows.Forms.TextBox
    Friend WithEvents txtReciboDesde As System.Windows.Forms.TextBox
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents lblAnulado As System.Windows.Forms.Label
    Friend WithEvents cbxTipoListado As ERP.ocxCBX
End Class

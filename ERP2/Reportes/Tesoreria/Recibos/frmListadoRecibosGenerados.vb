﻿Imports ERP.Reporte
Imports System.Data.SqlClient
Public Class frmListadoRecibosGenerados
    'CLASES
    Dim CReporte As New CReporteReciboVenta
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES

    Dim vdtCuentaBancaria As DataTable
    Dim Recibo As String = ""
    Dim Cobrador As String = ""
    Dim Cliente As String = ""
    Dim Sucursal As String = ""

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()


        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'TipoListado
        cbxTipoListado.cbx.Items.Add("LISTAR TODOS")
        cbxTipoListado.cbx.Items.Add("SOLO ANULADOS")
        cbxTipoListado.cbx.Items.Add("SOLO ACTIVOS")
        cbxTipoListado.cbx.SelectedIndex = 0

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0


        cbxEstado.cbx.Items.Add("ANULADO")
        cbxEstado.cbx.Items.Add("COBRADO")
        cbxEstado.cbx.Items.Add("COBRADO CON COMPROBANTE DISTINTO")
        cbxEstado.cbx.Items.Add("PENDIENTE DE RENDICION")
        cbxEstado.cbx.SelectedIndex = 0

        'cbxOrdenadoPor.cbx.Items.Add("Sucursal")
        'cbxOrdenadoPor.cbx.Items.Add("Cliente")

        txtCliente.Conectar()
    End Sub

    Sub Listar()

        Dim Titulo As String = ""
        Dim Subtitulo As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where cast(Fecha as date) Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"


        If chkRecibo.chk.Checked = True Then
            Where = Where & " And NroRecibo Between '" & txtReciboDesde.Text & "' And  '" & txtReciboHasta.Text & "'"
        End If

        'Filtrar por Cliente
        If chkCliente.chk.Checked = True Then
            If txtCliente.Seleccionado = True Then
                Where = Where + " And IDCliente = " & txtCliente.Registro("ID").ToString
                WhereDetalle = WhereDetalle + " And IDCliente = " & txtCliente.Registro("ID").ToString
            Else

            End If
        End If


        If chkSucursal.chk.Checked = True Then
            Where = Where & " And IDSucursal = " & cbxSucursal.cbx.SelectedValue & " "
            WhereDetalle = WhereDetalle & " And CC.IDSucursal = " & cbxSucursal.cbx.SelectedValue & " "
        End If

siguiente:

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If


        'Tipo de Listado es Listar todos
        If cbxTipoListado.cbx.Text = "LISTAR TODOS" Then
            Where = Where & " "
        End If

        'Tipo de Listado es Listar solo Anulados
        If cbxTipoListado.cbx.Text = "SOLO ANULADOS" Then
            Where = Where & " And Anulado = 'True'"
        End If

        'Tipo de Listado es Listar sin Anulados
        If cbxTipoListado.cbx.Text = "SOLO ACTIVOS" Then
            Where = Where & " And Anulado = 'False'"
        End If

        If chkEstado.Valor Then
            If cbxEstado.cbx.Text <> "" Then
                Where = Where & " And Estado = '" & cbxEstado.cbx.Text & "' "
            End If
        End If

        Titulo = "LISTADO DE RECIBOS GENERADOS"

        'Subtitulo
        If chkRecibo.chk.Checked = True Then
            If txtReciboDesde.Text = txtReciboHasta.Text Then
                Recibo = " - Recibo N°: " & txtReciboDesde.Text & ""
            Else
                Recibo = " - Recibo desde N° " & txtReciboDesde.Text & " hasta N° " & txtReciboHasta.Text & ""
            End If
        End If

        If chkSucursal.chk.Checked = True Then
            Sucursal = " - Sucursal: " & cbxSucursal.cbx.Text & ""
        End If

        Subtitulo = "Fecha desde: " & txtDesde.txt.Text & " hasta " & txtHasta.txt.Text & "" & Recibo & "" & Cliente & "" & Cobrador & "" & Sucursal & ""
        CReporte.ListadoRecibosGenerados(frm, Titulo, Subtitulo, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, txtDesde.txt.Text, txtHasta.txt.Text)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VReciboVenta ", "Select Top(0) NroRecibo,Fecha, Cliente From VReciboVenta ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next
        cbxOrdenadoPor.cbx.SelectedIndex = 0
    End Sub

    

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoCobranzaCredito_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkRecibo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkRecibo.PropertyChanged
        txtReciboDesde.Enabled = value
        txtReciboHasta.Enabled = value
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkEstado.PropertyChanged
        cbxEstado.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged_1(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub frmListadoRecibosGenerados_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        LiberarMemoria()
    End Sub
End Class




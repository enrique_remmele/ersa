﻿Imports ERP.Reporte
Public Class frmResumendeMovimientoSaldo

    'CLASES
    Dim CReporte As New CReporteCuentasACobrar
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'VARIABLES
    Dim Titulo As String = "RESUMEN DE MOVIMIENTOS Y SALDOS"
    Dim TipoInforme As String
    Dim ID As Integer

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CambiarOrdenacion()
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        cbxMoneda.cbx.SelectedIndex = 1

        txtCliente.Conectar()

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim ASC As Boolean
        Dim Top As String = ""
        Dim Fecha1 As String = ""
        Dim Fecha2 As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkCliente.chk.Checked = True Then
            WhereDetalle = "IDCliente = " & txtCliente.txtID.ObtenerValor
        End If
        If WhereDetalle = "" Then
            WhereDetalle = " IDMoneda = " & cbxMoneda.GetValue
        Else
            WhereDetalle = WhereDetalle & "And IDMoneda = " & cbxMoneda.GetValue
        End If


        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion2(WhereDetalle)
            End If

        Next


        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            If cbxEnForma.cbx.SelectedIndex = 0 Then
                Asc = True
            Else
                Asc = False
            End If
        End If

        'Fecha
        Fecha1 = txtDesde.GetValueString
        Fecha2 = txtHasta.GetValueString

        Select Case cbxTipoInforme.cbx.SelectedIndex

            Case 0
                TipoInforme = "Por Nombre o RazónSocial - En " & cbxMoneda.cbx.Text
                Where = "'" & txtDesde.GetValueString & "'"
                CReporte.ResumenMovimientoSaldo(frm, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, Fecha1, Fecha2, TipoInforme, ASC)
            Case 1
                TipoInforme = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString & " - Por Nombre o razónsocial - En " & cbxMoneda.cbx.Text
                Where = "'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "'"
                CReporte.ResumenMovimientoSaldoDetallado(frm, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador, Fecha1, Fecha2, TipoInforme, ASC)

        End Select



    End Sub

    Sub CambiarOrdenacion()

         'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VCliente ", "Select	Top(0) RazonSocial,Referencia,TipoCliente,Vendedor,Cobrador From VCliente")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmResumendeMovimientoSaldo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        CambiarOrdenacion()
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub chkEstadoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkEstadoCliente.PropertyChanged
        cbxEstadoCliente.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkCobrador_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCobrador.PropertyChanged
        cbxCobrador.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxMoneda.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class


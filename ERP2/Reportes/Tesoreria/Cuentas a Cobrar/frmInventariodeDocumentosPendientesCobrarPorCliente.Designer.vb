﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmInventariodeDocumentosPendientesCobrarPorCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkClienteSucursal = New ERP.ocxCHK()
        Me.cbxClienteSucursal = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbxCondiciones = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxDocumento = New ERP.ocxCBX()
        Me.cbxVencimiento = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxCondiciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkClienteSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxClienteSucursal)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(6, 4)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(512, 200)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(7, 49)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 38
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(125, 45)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(358, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 39
        Me.cbxTipoProducto.Texto = ""
        '
        'chkClienteSucursal
        '
        Me.chkClienteSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkClienteSucursal.Color = System.Drawing.Color.Empty
        Me.chkClienteSucursal.Location = New System.Drawing.Point(6, 119)
        Me.chkClienteSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkClienteSucursal.Name = "chkClienteSucursal"
        Me.chkClienteSucursal.Size = New System.Drawing.Size(103, 21)
        Me.chkClienteSucursal.SoloLectura = False
        Me.chkClienteSucursal.TabIndex = 37
        Me.chkClienteSucursal.Texto = "Cliente Sucursal:"
        Me.chkClienteSucursal.Valor = False
        '
        'cbxClienteSucursal
        '
        Me.cbxClienteSucursal.CampoWhere = ""
        Me.cbxClienteSucursal.CargarUnaSolaVez = False
        Me.cbxClienteSucursal.DataDisplayMember = ""
        Me.cbxClienteSucursal.DataFilter = Nothing
        Me.cbxClienteSucursal.DataOrderBy = ""
        Me.cbxClienteSucursal.DataSource = ""
        Me.cbxClienteSucursal.DataValueMember = "ID"
        Me.cbxClienteSucursal.dtSeleccionado = Nothing
        Me.cbxClienteSucursal.Enabled = False
        Me.cbxClienteSucursal.FormABM = Nothing
        Me.cbxClienteSucursal.Indicaciones = Nothing
        Me.cbxClienteSucursal.Location = New System.Drawing.Point(125, 120)
        Me.cbxClienteSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxClienteSucursal.Name = "cbxClienteSucursal"
        Me.cbxClienteSucursal.SeleccionMultiple = False
        Me.cbxClienteSucursal.SeleccionObligatoria = False
        Me.cbxClienteSucursal.Size = New System.Drawing.Size(358, 21)
        Me.cbxClienteSucursal.SoloLectura = False
        Me.cbxClienteSucursal.TabIndex = 36
        Me.cbxClienteSucursal.Texto = ""
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 150
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(125, 81)
        Me.txtCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(358, 42)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 35
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(6, 79)
        Me.chkCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(53, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 34
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = ""
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(125, 18)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(358, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 13
        Me.cbxSucursal.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(7, 20)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 12
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 250)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(815, 22)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'gbxCondiciones
        '
        Me.gbxCondiciones.Controls.Add(Me.Label1)
        Me.gbxCondiciones.Controls.Add(Me.cbxDocumento)
        Me.gbxCondiciones.Controls.Add(Me.cbxVencimiento)
        Me.gbxCondiciones.Controls.Add(Me.Label2)
        Me.gbxCondiciones.Controls.Add(Me.cbxEnForma)
        Me.gbxCondiciones.Controls.Add(Me.cbxOrdenadoPor)
        Me.gbxCondiciones.Controls.Add(Me.lblOrdenado)
        Me.gbxCondiciones.Controls.Add(Me.lblPeriodo)
        Me.gbxCondiciones.Controls.Add(Me.txtHasta)
        Me.gbxCondiciones.Controls.Add(Me.txtDesde)
        Me.gbxCondiciones.Location = New System.Drawing.Point(536, 4)
        Me.gbxCondiciones.Name = "gbxCondiciones"
        Me.gbxCondiciones.Size = New System.Drawing.Size(267, 200)
        Me.gbxCondiciones.TabIndex = 8
        Me.gbxCondiciones.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 144)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Condición:"
        '
        'cbxDocumento
        '
        Me.cbxDocumento.CampoWhere = Nothing
        Me.cbxDocumento.CargarUnaSolaVez = False
        Me.cbxDocumento.DataDisplayMember = Nothing
        Me.cbxDocumento.DataFilter = Nothing
        Me.cbxDocumento.DataOrderBy = Nothing
        Me.cbxDocumento.DataSource = Nothing
        Me.cbxDocumento.DataValueMember = Nothing
        Me.cbxDocumento.dtSeleccionado = Nothing
        Me.cbxDocumento.FormABM = Nothing
        Me.cbxDocumento.Indicaciones = Nothing
        Me.cbxDocumento.Location = New System.Drawing.Point(14, 160)
        Me.cbxDocumento.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDocumento.Name = "cbxDocumento"
        Me.cbxDocumento.SeleccionMultiple = False
        Me.cbxDocumento.SeleccionObligatoria = False
        Me.cbxDocumento.Size = New System.Drawing.Size(231, 21)
        Me.cbxDocumento.SoloLectura = False
        Me.cbxDocumento.TabIndex = 19
        Me.cbxDocumento.Texto = ""
        '
        'cbxVencimiento
        '
        Me.cbxVencimiento.FormattingEnabled = True
        Me.cbxVencimiento.Location = New System.Drawing.Point(15, 29)
        Me.cbxVencimiento.Name = "cbxVencimiento"
        Me.cbxVencimiento.Size = New System.Drawing.Size(230, 21)
        Me.cbxVencimiento.TabIndex = 11
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Vencimiento:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(172, 121)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 17
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(15, 121)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 16
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(15, 104)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 15
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(16, 56)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 12
        Me.lblPeriodo.Text = "Periodo:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 26, 8, 35, 7, 504)
        Me.txtHasta.Location = New System.Drawing.Point(98, 71)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 14
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 26, 8, 35, 7, 504)
        Me.txtDesde.Location = New System.Drawing.Point(15, 71)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 13
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(740, 210)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 10
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(564, 210)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 9
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmInventariodeDocumentosPendientesCobrarPorCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(815, 272)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxCondiciones)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmInventariodeDocumentosPendientesCobrarPorCliente"
        Me.Tag = "frmInventariodeDocumentosPendientesCobrarPorCliente"
        Me.Text = "frmInventariodeDocumentosPendientesCobrarPorCliente"
        Me.gbxFiltro.ResumeLayout(False)
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxCondiciones.ResumeLayout(False)
        Me.gbxCondiciones.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gbxFiltro As GroupBox
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents chkSucursal As ocxCHK
    Friend WithEvents chkClienteSucursal As ocxCHK
    Friend WithEvents cbxClienteSucursal As ocxCBX
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents chkCliente As ocxCHK
    Friend WithEvents ErrorProvider1 As ErrorProvider
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents gbxCondiciones As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxDocumento As ocxCBX
    Friend WithEvents cbxVencimiento As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cbxEnForma As ocxCBX
    Friend WithEvents cbxOrdenadoPor As ocxCBX
    Friend WithEvents lblOrdenado As Label
    Friend WithEvents lblPeriodo As Label
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents btnCerrar As Button
    Friend WithEvents btnInforme As Button
    Friend WithEvents chkTipoProducto As ocxCHK
    Friend WithEvents cbxTipoProducto As ocxCBX
End Class

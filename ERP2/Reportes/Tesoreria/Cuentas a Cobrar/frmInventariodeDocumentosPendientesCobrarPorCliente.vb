﻿Imports ERP.Reporte
Public Class frmInventariodeDocumentosPendientesCobrarPorCliente
    'CLASES
    Dim CReporte As New CReporteCuentasACobrar
    Dim CReporte2 As New CReporteCobranza
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        Me.AcceptButton = New Button
        txtCliente.Conectar()

        CambiarOrdenacion()
        CargarInformacion()

        txtDesde.txt.Focus()
        txtDesde.txt.SelectAll()

        HabilitarPeriodo()

        AsignarFecha()
        txtCliente.Enabled = False
        cbxClienteSucursal.txt.Text = ''
        cbxClienteSucursal.Enabled = False
    End Sub

    Sub CargarInformacion()

        'Contado - Credito
        cbxDocumento.cbx.Items.Add("CONTADO + CREDITO")
        cbxDocumento.cbx.Items.Add("CONTADO")
        cbxDocumento.cbx.Items.Add("CREDITO")
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.SelectedIndex = 0
        'cbxDocumento.cbx.Text = "CONTADO+CREDITO"

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        'Fecha Vencimiento
        cbxVencimiento.Items.Add("TODOS A HOY")
        cbxVencimiento.Items.Add("VENCIDOS A HOY")
        cbxVencimiento.Items.Add("A VENCER EN EL PERIODO")
        cbxVencimiento.DropDownStyle = ComboBoxStyle.DropDownList
        cbxVencimiento.DropDownStyle = ComboBoxStyle.DropDownList
        cbxVencimiento.SelectedIndex = 0

        cbxOrdenadoPor.cbx.SelectedIndex = 5

        'Configuracion
        cbxOrdenadoPor.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENADO", "")
        cbxDocumento.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DOCUMENTO", "")
        cbxEnForma.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ENFORMA", "")

    End Sub

    Sub GuardarInformacion()

        'Configuracion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENADO", cbxOrdenadoPor.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DOCUMENTO", cbxDocumento.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ENFORMA", cbxEnForma.cbx.Text)

    End Sub

    Sub Listar()

        Dim Titulo As String = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR"
        Dim TipoInforme As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = " Where 1 = 1"
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim TipoProducto = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        Dim TipoMoneda As String = ""
        Dim dtCCFF As New DataTable
        Dim CuentasContables As String = " "
        Dim Mensaje As String = ""
        Dim TotalAnticipo As Decimal = 0


        If chkTipoProducto.Valor = True Then
            TipoInforme = TipoInforme & "Tipo Producto: " & cbxTipoProducto.cbx.Text
        End If


        If cbxVencimiento.Text = "" Then
            Mensaje = "Seleccione un vencimiento!"
            ErrorProvider1.SetError(cbxVencimiento, Mensaje)
            ErrorProvider1.SetIconAlignment(cbxVencimiento, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = Mensaje
            Exit Sub
        End If

        Where = ""

        Select Case cbxVencimiento.SelectedIndex
            Case 0
                Where = "Where Anulado='False' And Cancelado='False'"
            Case 1
                Where = " Where Anulado= 'False' And Cancelado= 'False' And [FechaVencimiento] <= GETDATE () "
            Case 2
                Where = " Where Anulado= 'False' And FechaVencimiento Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
                TipoInforme = TipoInforme & " Fecha Desde " & txtDesde.txt.Text & " Hasta " & txtHasta.txt.Text & ""
        End Select

        If chkCliente.chk.Checked = True Then

            If txtCliente.txtID.txt.Text = "" Then
                Mensaje = "Ingrese primero algún cliente!"
                ctrError.SetError(txtCliente, Mensaje)
                ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopRight)
                tsslEstado.Text = Mensaje
                Exit Sub
            Else
                Where = Where & " And IDCliente = " & txtCliente.Registro("ID").ToString
                If WhereDetalle = "" Then
                    WhereDetalle = " Where IDCliente =" & txtCliente.Registro("ID").ToString
                Else
                    WhereDetalle = WhereDetalle & " and IDCliente =" & txtCliente.Registro("ID").ToString
                End If

            End If
            TotalAnticipo = CSistema.ExecuteScalar("Select ISNULL(Sum(Monto),0) from SaldoTotalAnticipos where idcliente = " & txtCliente.Registro("ID").ToString)
        Else
            TotalAnticipo = CSistema.ExecuteScalar("Select ISNULL(Sum(Monto),0) from SaldoTotalAnticipos")
        End If


        'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
        If chkClienteSucursal.chk.Checked = True Then
            If cbxClienteSucursal.cbx.Text <> "" Then
                Where = Where & " And IDSucursalDelCliente =" & cbxClienteSucursal.cbx.SelectedValue
                If WhereDetalle = "" Then
                    WhereDetalle = " Where IDSucursalDelCliente =" & cbxClienteSucursal.cbx.SelectedValue
                Else
                    WhereDetalle = WhereDetalle & " and IDSucursalDelCliente =" & cbxClienteSucursal.cbx.SelectedValue
                End If
            End If
        End If



        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.name = "cbxTipoProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

siguiente:

        Next

        For Each ctr As Object In gbxCondiciones.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        Select Case cbxDocumento.cbx.SelectedIndex

            Case 0
                Where = Where & ""
            Case 1
                Where = Where & " And " & " Condicion = 'CONT'"
            Case 2
                Where = Where & " And " & " Condicion = 'CRED'"
        End Select

        'Where = Where & " and Saldo <> 0"
        If chkTipoProducto.Valor = True Then
            TipoProducto = " and IDTipoProducto =" & cbxTipoProducto.cbx.SelectedValue
        End If


        If chkTipoProducto.Valor = True Then
            'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
            Where = Where & TipoProducto
        End If
        OrderBy = "Order by Cliente, IDSucursal, FechaVencimiento " & cbxEnForma.cbx.Text
        Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR"
        'TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
        'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
        If cbxClienteSucursal.cbx.Text = "" Then
            'Igual
            TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
        Else
            'Nuevo
            TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text & " - Suc. Cliente: " & cbxClienteSucursal.cbx.Text
        End If
        CReporte.InventarioDocumentoPendientePorCliente(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, TotalAnticipo, vgUsuarioIdentificador)

        'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
        chkCliente.Valor = False
        txtCliente.Clear()

        chkClienteSucursal.Valor = False
        cbxClienteSucursal.Enabled = False
        cbxClienteSucursal.cbx.Text = ""


    End Sub

    Sub HabilitarPeriodo()
        Select Case cbxVencimiento.SelectedIndex
            Case 0
                txtDesde.Enabled = False
                txtHasta.Enabled = False
            Case 1
                txtDesde.Enabled = False
                txtHasta.Enabled = False
            Case 2
                txtDesde.Enabled = True
                txtHasta.Enabled = True
        End Select
    End Sub

    Sub AsignarFecha()
        If cbxVencimiento.SelectedIndex = 2 Then
            txtDesde.PrimerDiaAño()
            txtHasta.Hoy()
        Else
            txtDesde.txt.Text = ""
            txtHasta.txt.Text = ""
        End If
    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VInventarioDocumentosPendientes ", "Select Top(0) 'FechaVencimiento/Cliente'='','Cobrador/FechaVencimiento'='','Vendedor/FechaVencimiento'='','NroComprobante'='','Lote'='','Cliente/FechaVencimiento'='' From VInventarioDocumentosPendientes")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next
        cbxOrdenadoPor.cbx.SelectedIndex = 0

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmInventariodeDocumentosPendientesCobrar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmInventariodeDocumentosPendientesCobrar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmInventariodeDocumentosPendientesCobrar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CambiarOrdenacion()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkDocumentos_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxDocumento.Enabled = value
    End Sub

    Private Sub cbxVencimiento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxVencimiento.SelectedIndexChanged
        AsignarFecha()

        HabilitarPeriodo()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxDocumento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxDocumento.Load

    End Sub

    'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
    Private Sub chkClienteSucursal_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkClienteSucursal.PropertyChanged
        cbxClienteSucursal.Enabled = value
        If chkClienteSucursal.chk.Checked = True Then
            CSistema.SqlToComboBox(cbxClienteSucursal.cbx, "select ID, Sucursal from vClienteSucursal where IDSucursal=" & cbxSucursal.cbx.SelectedValue & "And IDCliente =" & txtCliente.Registro("ID").ToString & "Order by 2")
        End If
    End Sub


End Class
﻿Imports ERP.Reporte

Public Class frmInventariodeDocumentosPendientesCobrar

    'CLASES
    Dim CReporte As New CReporteCuentasACobrar
    Dim CReporte2 As New CReporteCobranza
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True

        Me.AcceptButton = New Button
        txtCliente.Conectar()
        txtCuentaContableDesde.Conectar()
        txtCuentaContableHasta.Conectar()
        CambiarOrdenacion()
        CargarInformacion()

        txtDesde.txt.Focus()
        txtDesde.txt.SelectAll()

        HabilitarPeriodo()

        AsignarFecha()

    End Sub

    Sub CargarInformacion()

        'Contado - Credito
        cbxDocumento.cbx.Items.Add("CONTADO + CREDITO")
        cbxDocumento.cbx.Items.Add("CONTADO")
        cbxDocumento.cbx.Items.Add("CREDITO")
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.SelectedIndex = 0
        'cbxDocumento.cbx.Text = "CONTADO+CREDITO"

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        'Fecha Vencimiento
        cbxVencimiento.Items.Add("TODOS A HOY")
        cbxVencimiento.Items.Add("VENCIDOS A HOY")
        cbxVencimiento.Items.Add("A VENCER EN EL PERIODO")
        cbxVencimiento.DropDownStyle = ComboBoxStyle.DropDownList
        cbxVencimiento.DropDownStyle = ComboBoxStyle.DropDownList
        cbxVencimiento.SelectedIndex = 0

        cbxMoneda.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.SelectedIndex = 5

        'Configuracion
        cbxOrdenadoPor.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENADO", "")
        cbxDocumento.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "DOCUMENTO", "")
        cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")
        cbxEnForma.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ENFORMA", "")
        nudRanking.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "RANKING", "")

        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

    End Sub

    Sub GuardarInformacion()

        'Configuracion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENADO", cbxOrdenadoPor.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "DOCUMENTO", cbxDocumento.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ENFORMA", cbxEnForma.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RANKING", nudRanking.Text)

    End Sub

    Sub Listar()

        Dim Titulo As String = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR"
        Dim TipoInforme As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = " Where 1 = 1"
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim TipoProducto = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        Dim TipoMoneda As String = ""
        Dim dtCCFF As New DataTable
        Dim CuentasContables As String = " "
        Dim Mensaje As String = ""
        Dim TotalAnticipo As Decimal = 0

        If chkCC.Valor = True Then
            If txtCuentaContableDesde.txtCodigo.Texto = "" Then
                Mensaje = "Ingrese Cuenta Contable Desde!"
                ctrError.SetError(txtCuentaContableDesde, Mensaje)
                ctrError.SetIconAlignment(txtCuentaContableDesde, ErrorIconAlignment.TopRight)
                tsslEstado.Text = Mensaje
                Exit Sub
            End If
            If txtCuentaContableHasta.txtCodigo.Texto = "" Then
                Mensaje = "Ingrese Cuenta Contable Hasta!"
                ctrError.SetError(txtCuentaContableHasta, Mensaje)
                ctrError.SetIconAlignment(txtCuentaContableHasta, ErrorIconAlignment.TopRight)
                tsslEstado.Text = Mensaje
                Exit Sub
            End If
        End If

        'Buscar las Cuentas contables
        If chkCC.Valor = True And txtCuentaContableDesde.txtCodigo.Texto <> "" And txtCuentaContableHasta.txtCodigo.Texto <> "" Then 'Si hay filtro de Cuenta Contable
            CuentasContables = " and (CuentaContable = '0' "
            dtCCFF = CSistema.ExecuteToDataTable("select Codigo from CuentaContable where codigo between '" & txtCuentaContableDesde.txtCodigo.Texto & "' and '" & txtCuentaContableHasta.txtCodigo.Texto & "' and Imputable = 1 and descripcion like '%deudores por venta%'").Copy
            For Each oRow As DataRow In dtCCFF.Rows
                Dim param As New DataTable
                CuentasContables = CuentasContables & " or CuentaContable ='" & oRow("Codigo").ToString & "'    "
            Next
            CuentasContables = CuentasContables & ")"
            If txtCuentaContableDesde.txtCodigo.Texto <> "" Then
                TipoInforme = TipoInforme & "CC: " & txtCuentaContableDesde.txtCodigo.Texto & " - " & txtCuentaContableHasta.txtCodigo.Texto & " - "
            End If
        End If
        'Fin de cuentas contables

        If chkTipoProducto.Valor = True Then
            TipoInforme = TipoInforme & "Tipo Producto: " & cbxTipoProducto.cbx.Text
        End If


        If cbxVencimiento.Text = "" Then
            Mensaje = "Seleccione un vencimiento!"
            ErrorProvider1.SetError(cbxVencimiento, mensaje)
            ErrorProvider1.SetIconAlignment(cbxVencimiento, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        Where = ""

        Select Case cbxVencimiento.SelectedIndex
            Case 0
                Where = "Where Anulado='False' And Cancelado='False'"
            Case 1
                Where = " Where Anulado= 'False' And Cancelado= 'False' And [FechaVencimiento] <= GETDATE () "
            Case 2
                Where = " Where Anulado= 'False' And FechaVencimiento Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
                TipoInforme = TipoInforme & " Fecha Desde " & txtDesde.txt.Text & " Hasta " & txtHasta.txt.Text & ""
        End Select

        If chkCliente.chk.Checked = True Then

            If txtCliente.txtID.txt.Text = "" Then
                Mensaje = "Ingrese primero algún cliente!"
                ctrError.SetError(txtCliente, mensaje)
                ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                Where = Where & " And IDCliente = " & txtCliente.Registro("ID").ToString
                If WhereDetalle = "" Then
                    WhereDetalle = " Where IDCliente =" & txtCliente.Registro("ID").ToString
                Else
                    WhereDetalle = WhereDetalle & " and IDCliente =" & txtCliente.Registro("ID").ToString
                End If

            End If
            TotalAnticipo = CSistema.ExecuteScalar("Select ISNULL(Sum(Monto),0) from SaldoTotalAnticipos where idcliente = " & txtCliente.Registro("ID").ToString)
        Else
            TotalAnticipo = CSistema.ExecuteScalar("Select ISNULL(Sum(Monto),0) from SaldoTotalAnticipos")
        End If


        'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
        If chkClienteSucursal.chk.Checked = True Then
            If cbxClienteSucursal.cbx.Text <> "" Then
                Where = Where & " And IDSucursalDelCliente =" & cbxClienteSucursal.cbx.SelectedValue
                If WhereDetalle = "" Then
                    WhereDetalle = " Where IDSucursalDelCliente =" & cbxClienteSucursal.cbx.SelectedValue
                Else
                    WhereDetalle = WhereDetalle & " and IDSucursalDelCliente =" & cbxClienteSucursal.cbx.SelectedValue
                End If
            End If
        End If

        If chkChequeDiferido.Valor = True And chkDescontados.Valor = True Then
            Where = Where & " And ChequeDiferido = 'True'"
        End If

        If chkExcluirDiferido.Valor = True Then
            Where = Where & " And ChequeDiferido = 'False'"
        End If

        If chkChequeDiferido.chk.Checked = True And chkDescontados.Valor = False Then
            Where = Where & " And ChequeDiferido = 'True' and ChequeDescontado = 'False'"
        End If

        If chkChequeDiferido.chk.Checked = False And chkDescontados.Valor = True Then
            Where = Where & " And ChequeDiferido = 'True' and ChequeDescontado = 'True'"
        End If

        If chkBoletaContraBoleta.Valor = True Then
            Where = Where & " and BoletaContraBoleta = 'True' "
        End If

        If chkJudicial.Valor = True Then
            Where = Where & " and Judicial = 'True' "
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.name = "cbxTipoProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

siguiente:

        Next

        For Each ctr As Object In GroupBox2.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        'Filro de Cuenta Contable
        If chkCC.Valor = True And txtCuentaContableDesde.txtCodigo.Texto <> "" Then
            Where = Where
        End If

        Select Case cbxDocumento.cbx.SelectedIndex

            Case 0
                Where = Where & ""
            Case 1
                Where = Where & " And " & " Condicion = 'CONT'"
            Case 2
                Where = Where & " And " & " Condicion = 'CRED'"
        End Select

        If chkClienteChequeDiferidoFacturaPendiente.chk.Checked Then
            Where = Where & " and dbo.FClienteChequeDiferidoFacturaVencidaConSaldo(IDCliente) = 1"
        End If

        'Where = Where & " and Saldo <> 0"
        If chkTipoProducto.Valor = True Then
            TipoProducto = " and IDTipoProducto =" & cbxTipoProducto.cbx.SelectedValue
        End If

        'Tipo moneda utilizada
        If cbxMoneda.Enabled Then
            TipoMoneda = " - En " & cbxMoneda.Texto
        End If
        If chkResumido.Valor = True Then 'Si quiere resumido por cliente
            If chkTipoProducto.Valor = True Then
                TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
                Where = Where & TipoProducto
            End If
            OrderBy = "Order by Cliente, IDSucursal, FechaVencimiento " & cbxEnForma.cbx.Text
            Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR"
            'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
            If cbxClienteSucursal.cbx.Text = "" Then
                'Igual
                TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
            Else
                '    'Nuevo
                TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text & " - Suc. Cliente: " & cbxClienteSucursal.cbx.Text
            End If
            CReporte.InventarioDocumentoPendiente(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, TotalAnticipo, vgUsuarioIdentificador, "rptInventarioDocumentoPendienteResumido")
            Else ' Si no quiere resumido por cliente
                Dim vFiltroCuentaContable As String
            If chkCC.Valor = False Then
                If chkInventarioDetallado.Valor = False Then
                    Select Case cbxOrdenadoPor.cbx.SelectedIndex
                        Case 0
                            If chkTipoProducto.Valor = True Then
                                'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
                                Where = Where & TipoProducto
                            End If
                            OrderBy = "Order by FechaVencimiento " & cbxEnForma.txt.Text
                            Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR (VENCIMIMIENTO / CLIENTE)"
                            'TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
                            TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text
                            CReporte.InventarioDocumentoPendienteVencimientoCliente(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador)
                        Case 1
                            If chkTipoProducto.Valor = True Then
                                'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
                                Where = Where & TipoProducto
                            End If
                            OrderBy = "Order by FechaVencimiento " & cbxEnForma.txt.Text
                            Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR (COBRADOR / FECHA VENCIMIENTO)"
                            TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text & " - Por cobrador"
                            CReporte.InventarioDocumentoPendienteCobradorFechaVencimiento(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador)
                        Case 2
                            If chkTipoProducto.Valor = True Then
                                'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
                                Where = Where & TipoProducto
                            End If
                            OrderBy = "Order by FechaVencimiento " & cbxEnForma.txt.Text
                            Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR (VENDEDOR / VENCIMIENTO)"
                            TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text & " - Por vendedor"
                            CReporte.InventarioDocumentoPendienteVendedorFechaVencimiento(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador)
                        Case 3
                            If chkTipoProducto.Valor = True Then
                                'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientesLote.IdTransaccion)"
                                Where = Where & TipoProducto
                            End If
                            OrderBy = "Order by NroComprobante ASC"
                            Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR - POR COMPROBANTE)"
                            TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text & " - Por NroComprobante"
                            CReporte.InventarioDocumentoPendientePorComprobante(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador)
                        Case 4
                            If chkTipoProducto.Valor = True Then
                                'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientesLote.IdTransaccion)"
                                Where = Where & TipoProducto
                            End If
                            OrderBy = "Order by Lote ASC"
                            Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR - POR LOTE"
                            TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text & " - Por Lote"
                            CReporte.InventarioDocumentoPendientePorComprobante(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador)
                        Case 5
                            If chkDireccionAlternativa.chk.Checked = True Then
                                If chkTipoProducto.Valor = True Then
                                    'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
                                    Where = Where & TipoProducto
                                End If
                                OrderBy = "Order by Cliente, IDSucursal, FechaVencimiento " & cbxEnForma.cbx.Text
                                Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR - CON DIRECCION ALTERNATIVA"
                                TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
                                CReporte.InventarioDocumentoPendienteDireccionAlternativa(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador)
                            Else
                                If chkTipoProducto.Valor = True Then
                                    'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
                                    cbxTipoProducto.EstablecerCondicion(Where)
                                    'Where = Where & TipoProducto
                                    Where = Where
                                End If
                                OrderBy = "Order by Cliente, IDSucursal, FechaVencimiento " & cbxEnForma.cbx.Text
                                Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR"
                                'TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
                                'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
                                If cbxClienteSucursal.cbx.Text = "" Then
                                    'Igual
                                    TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
                                Else
                                    'Nuevo
                                    TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text & " - Suc. Cliente: " & cbxClienteSucursal.cbx.Text
                            End If
                            CReporte.InventarioDocumentoPendiente(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, TotalAnticipo, vgUsuarioIdentificador)
                            End If

                    End Select
                Else 'si quiere con importe original y GS
                    If chkTipoProducto.Valor = True Then
                        'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
                        Where = Where & TipoProducto
                    End If

                    OrderBy = "Order by FechaVencimiento,Cliente " & cbxEnForma.cbx.Text
                    Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR ORIG/GS"
                    TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
                    CReporte.InventarioImporteOriginalGS(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador)

                End If
            Else 'Cuando se agrega el filtro de Cuenta Contable
                If chkTipoProducto.Valor = True Then
                    'TipoProducto = "and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VInventarioDocumentosPendientes.IdTransaccion)"
                    Where = Where & TipoProducto
                End If
                If chkCC.Valor = True And txtCuentaContableDesde.txtCodigo.Texto <> "" Then
                    vFiltroCuentaContable = " and exists(Select * from DetalleAsiento where IDTransaccion = VInventarioDocumentosPendientes.IdTransaccion " & CuentasContables & ")"
                    Where = Where & vFiltroCuentaContable
                End If
                OrderBy = "Order by Cliente, IDSucursal, FechaVencimiento " & cbxEnForma.cbx.Text
                Titulo = "INVENTARIO DE DOCUMENTOS PENDIENTES A COBRAR"
                TipoInforme = TipoInforme & " - " & cbxDocumento.cbx.Text & " - " & cbxVencimiento.Text & TipoMoneda & " - Suc: " & cbxSucursal.cbx.Text
                CReporte.InventarioDocumentoPendienteCuentaContable(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, vgUsuarioIdentificador)
            End If
        End If 'Fin de Informe detallado

        If chkMostrarAnticipo.chk.Checked Then
            CReporte2.ListadoAnticipoCliente(frm, "Listado de Anticipos", "", WhereDetalle, WhereDetalle, "", Top, vgUsuarioIdentificador, "01/01/2000", Today.Date.ToString, True)
        End If

        'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
        chkCliente.Valor = False
        txtCliente.Clear()

        chkClienteSucursal.Valor = False
        cbxClienteSucursal.Enabled = False
        cbxClienteSucursal.cbx.Text = ""


    End Sub

    Sub HabilitarPeriodo()
        Select Case cbxVencimiento.SelectedIndex
            Case 0
                txtDesde.Enabled = False
                txtHasta.Enabled = False
            Case 1
                txtDesde.Enabled = False
                txtHasta.Enabled = False
            Case 2
                txtDesde.Enabled = True
                txtHasta.Enabled = True
        End Select
    End Sub

    Sub AsignarFecha()
        If cbxVencimiento.SelectedIndex = 2 Then
            txtDesde.PrimerDiaAño()
            txtHasta.Hoy()
        Else
            txtDesde.txt.Text = ""
            txtHasta.txt.Text = ""
        End If
    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VInventarioDocumentosPendientes ", "Select Top(0) 'FechaVencimiento/Cliente'='','Cobrador/FechaVencimiento'='','Vendedor/FechaVencimiento'='','NroComprobante'='','Lote'='','Cliente/FechaVencimiento'='' From VInventarioDocumentosPendientes")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next
        cbxOrdenadoPor.cbx.SelectedIndex = 0

    End Sub
    Sub CopiarCuenta()
        If txtCuentaContableDesde.txtCodigo.Texto = "" Then
            Exit Sub
        End If
        txtCuentaContableHasta.txtCodigo.Texto = txtCuentaContableDesde.txtCodigo.Texto
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub frmInventariodeDocumentosPendientesCobrar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        'FA 20/06/2023
        LiberarMemoria()
    End Sub

    Private Sub frmInventariodeDocumentosPendientesCobrar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmInventariodeDocumentosPendientesCobrar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = chkVendedor.Valor
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CambiarOrdenacion()
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
    End Sub


    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub
    'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkEstadoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkEstadoCliente.PropertyChanged
        cbxEstadoCliente.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkDocumentos_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxDocumento.Enabled = value
    End Sub

    Private Sub cbxVencimiento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxVencimiento.SelectedIndexChanged
        AsignarFecha()

        HabilitarPeriodo()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxDocumento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxDocumento.Load

    End Sub

    Private Sub chkCobrador_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCobrador.PropertyChanged
        cbxCobrador.Enabled = value
    End Sub

    Private Sub chkVendedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVendedor.Load

    End Sub

    Private Sub chkChofer_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
        cbxTipoProducto.cbx.Text = ""
    End Sub

    Private Sub chkMoneda_PropertyChanged_1(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkCC_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCC.PropertyChanged
        txtCuentaContableDesde.Enabled = value
        chkCCHasta.Valor = value
        txtCuentaContableHasta.Enabled = value
    End Sub

    Private Sub chkCCHasta_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCCHasta.PropertyChanged
        txtCuentaContableHasta.Enabled = value
    End Sub

    Private Sub txtCuentaContableDesde_Leave(sender As System.Object, e As System.EventArgs) Handles txtCuentaContableDesde.Leave
        CopiarCuenta()
    End Sub

    Private Sub chkSucursalCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursalCliente.PropertyChanged
        cbxSucursalCliente.Enabled = value
    End Sub
    'FACEVAL 05-10-2021 FILTRO DE EXCLUIR CHEQUES DIFERIDO
    Private Sub chkExcluirDiferido_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkExcluirDiferido.PropertyChanged
        If chkExcluirDiferido.Valor = True Then
            chkChequeDiferido.Enabled = False
            chkDescontados.Enabled = False
            chkClienteChequeDiferidoFacturaPendiente.Enabled = False
        Else
            chkChequeDiferido.Enabled = True
            chkDescontados.Enabled = True
            chkClienteChequeDiferidoFacturaPendiente.Enabled = True
        End If

    End Sub

    Private Sub chkClienteChequeDiferidoFacturaPendiente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkClienteChequeDiferidoFacturaPendiente.PropertyChanged
        If chkClienteChequeDiferidoFacturaPendiente.Valor = True Then
            chkExcluirDiferido.Enabled = False
        ElseIf chkDescontados.Valor = True Then
            chkExcluirDiferido.Enabled = False
        ElseIf chkChequeDiferido.Valor = True Then
            chkExcluirDiferido.Enabled = False
        Else
            chkExcluirDiferido.Enabled = True
        End If
    End Sub

    Private Sub chkDescontados_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkDescontados.PropertyChanged
        If chkDescontados.Valor = True Then
            chkExcluirDiferido.Enabled = False
        ElseIf chkDescontados.Valor = True Then
            chkExcluirDiferido.Enabled = False
        ElseIf chkClienteChequeDiferidoFacturaPendiente.Valor = True Then
            chkExcluirDiferido.Enabled = False
        Else
            chkExcluirDiferido.Enabled = True
        End If
    End Sub

    Private Sub chkChequeDiferido_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkChequeDiferido.PropertyChanged
        If chkChequeDiferido.Valor = True Then
            chkExcluirDiferido.Enabled = False
        ElseIf chkClienteChequeDiferidoFacturaPendiente.Valor = True Then
            chkExcluirDiferido.Enabled = False
        ElseIf chkDescontados.Valor = True Then
            chkExcluirDiferido.Enabled = False
        Else
            chkExcluirDiferido.Enabled = True
        End If
    End Sub

    'SC: 18/11/2021 Se Agrega para nuevo filtro de Cliente Sucursal
    Private Sub chkClienteSucursal_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkClienteSucursal.PropertyChanged
        cbxClienteSucursal.Enabled = value
        If chkClienteSucursal.chk.Checked = True Then
            CSistema.SqlToComboBox(cbxClienteSucursal.cbx, "select ID, Sucursal from vClienteSucursal where IDCliente =" & txtCliente.Registro("ID").ToString & "Order by 2")
        End If
    End Sub



    'FIN 05-10-2021
End Class




﻿Imports ERP.Reporte
Public Class frmDistribuciondeSaldoDocumentoaCobrar
    'CLASES
    Dim CReporte As New CReporteCuentasACobrar
    Dim CSistema As New CSistema
    Dim CData As New CData


    Dim Where As String = ""

    'PROPIEDADES

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    Private CampoWhereValue As String
    Public Property CampoWhere() As String
        Get
            Return CampoWhereValue
        End Get
        Set(ByVal value As String)
            CampoWhereValue = value
        End Set
    End Property


    'VARIABLES
    Dim Titulo As String = "DISTRIBUCION DE SALDO DE DOCUMENTOS A COBRAR"
    Dim TipoInforme As String = ""
    Dim ID As Integer

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()

        cbxCondicion.cbx.Focus()
        CalculaDiasFecha()
        txtCliente.Conectar()

        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Cbx Condicion
        cbxCondicion.cbx.Items.Add("A VENCER")
        cbxCondicion.cbx.Items.Add("VENCIDOS")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxCondicion.cbx.SelectedIndex = 0


        cbxDocumento.cbx.Items.Add("Contado y Crédito")
        cbxDocumento.cbx.Items.Add("Crédito")
        cbxDocumento.cbx.Items.Add("Contado")
        cbxDocumento.cbx.SelectedIndex = 0

        'Cargar TXt Dias
        txtDia1.txt.Text = "10"
        txtDia2.txt.Text = "15"
        txtDia3.txt.Text = "20"
        txtDia4.txt.Text = "30"
        txtDia5.txt.Text = "60"
        txtDia6.txt.Text = "90"
        txtDia7.txt.Text = "120"
        txtDia8.txt.Text = "180"
        txtDia9.txt.Text = "365"

        CalculaDiasFecha()

        txtReferencia.Hoy()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        cbxMoneda.cbx.SelectedIndex = 1
    End Sub

    Sub Listar()

        Dim Campo As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim ASC As Boolean
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        TipoInforme = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar 
        Dim Condicion As Boolean
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            Condicion = 0
        Else
            Condicion = 1
        End If

        Where = "'" + txtFecha1.GetValueString + "','" + txtFecha2.GetValueString + "','" + txtFecha3.GetValueString + "','" + txtFecha4.GetValueString + "','" + txtFecha5.GetValueString + "','" + txtFecha6.GetValueString + "','" + txtFecha7.GetValueString + "','" + txtFecha8.GetValueString + "','" + txtFecha9.GetValueString + "','" + txtReferencia.GetValueString + "'," + Condicion.ToString


        Select Case cbxDocumento.cbx.SelectedIndex
            Case Is = 0
                Where = Where & ",'True', 'True'"
            Case Is = 1
                Where = Where & ",'True', 'False'"
            Case Is = 2
                Where = Where & ",'False', 'True'"
        End Select

        For Each ctr As Object In GroupBox3.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion2(WhereDetalle)
            End If

        Next

        If (chkCliente.Valor = True) And (txtCliente.txtReferencia.Texto <> "") Then
            If WhereDetalle = "" Then
                WhereDetalle = WhereDetalle & " IDCliente = " & txtCliente.Registro("ID").ToString
                TipoInforme = TipoInforme & " - Cliente: " & txtCliente.Registro("RazonSocial").ToString
            End If
            If WhereDetalle <> "" Then
                WhereDetalle = WhereDetalle & " and IDCliente = " & txtCliente.Registro("ID").ToString
                TipoInforme = TipoInforme & " - Cliente: " & txtCliente.Registro("RazonSocial").ToString
            End If
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = cbxOrdenadoPor.cbx.Text
            TipoInforme = TipoInforme & " - Ordenado por  " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            If cbxEnForma.cbx.SelectedIndex = 0 Then
                ASC = True
                TipoInforme = TipoInforme & " - En Forma Ascendente"
            Else
                ASC = False
                TipoInforme = TipoInforme & " - En forma Descendente"
            End If
        End If

        'Titulos
        If chkMoneda.Valor = True Then
            TipoInforme = TipoInforme & " - Moneda: " & cbxMoneda.Texto
        End If

        If chkTipoProducto.Valor = True Then
            TipoInforme = TipoInforme & " - Tipo Producto: " & cbxTipoProducto.Texto
        End If

        'Si se selecciono por susucrsal
        Dim IDSucursal As Integer = 0
        If chkSucursal.Valor = True Then
            IDSucursal = cbxSucursal.GetValue
            TipoInforme = TipoInforme & " - Sucursal: " & cbxSucursal.Texto
        End If

        If chkSucursalCliente.Valor = True Then
            TipoInforme = TipoInforme & " - Sucursal del Cliente: " & cbxSucursalCliente.Texto
        End If

        If chkVerJudiciales.Valor = False Then
            If WhereDetalle = "" Then
                WhereDetalle = WhereDetalle & " Judicial = 'False'"
            Else
                WhereDetalle = WhereDetalle & " and Judicial = 'False'"
            End If
            TipoInforme = TipoInforme & " - Sin Judiciales"
        Else
            TipoInforme = TipoInforme & " - Judiciales"
        End If
        CReporte.ArmarSubtitulo(TipoInforme, GroupBox5)
        CReporte.DistribucionSaldoCobro(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top, Cond, vgUsuarioIdentificador, txtFecha1.GetValueString, txtFecha2.GetValueString, txtFecha3.GetValueString, txtFecha4.GetValueString, txtFecha5.GetValueString, txtFecha6.GetValueString, txtFecha7.GetValueString, txtFecha8.GetValueString, txtFecha9.GetValueString, txtDia1.ObtenerValor, txtDia2.ObtenerValor, txtDia3.ObtenerValor, txtDia4.ObtenerValor, txtDia5.ObtenerValor, txtDia6.ObtenerValor, txtDia7.ObtenerValor, txtDia8.ObtenerValor, txtDia9.ObtenerValor, txtReferencia.GetValueString, cbxMoneda.GetValue, ASC, IDSucursal)

    End Sub


    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VVenta", "Select Top(0) Cliente,'Total'='','Cobrador'='','Vendedor'='','Sucursal'='' From VVenta ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Function CalcularFecha(ByVal Referencia As Date, ByRef Dias As Integer) As Date

        CalcularFecha = Date.Now

        Dim Retorno As Date

        ' A Vencer
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            Retorno = Referencia.AddDays(Dias)
        End If

        'Vencido
        If cbxCondicion.cbx.SelectedIndex = 1 Then
            Retorno = Referencia.AddDays(Dias * -1)
        End If

        Return Retorno

    End Function

    'Sub CalculaDiasFecha()

    '    If (IsDate(txtReferencia.GetValueString)) = True Then

    '        'Dia1
    '        txtFecha1.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia1.ObtenerValor)

    '        'Dia2
    '        txtFecha2.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia2.ObtenerValor)
    '        'Dia3

    '        txtFecha3.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia3.ObtenerValor)
    '        'Dia4

    '        txtFecha4.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia4.ObtenerValor)
    '        'Dia5

    '        txtFecha5.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia5.ObtenerValor)

    '        'Dia6
    '        txtFecha6.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia6.ObtenerValor)

    '        'Dia7
    '        txtFecha7.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia7.ObtenerValor)

    '        'Dia8
    '        txtFecha8.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia8.ObtenerValor)

    '        'Dia9
    '        txtFecha9.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia9.ObtenerValor)

    '    Else

    '        Exit Sub

    '    End If
    'End Sub

    Sub CalculaDiasFecha()
        Dim Fecha As String = txtReferencia.GetValueString
        'If IsDate(Fecha) Then
        If IsDate(txtReferencia.txt.Text) Then
            'Dia1
            txtFecha1.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia1.ObtenerValor.ToString)
            'Dia2
            txtFecha2.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia2.ObtenerValor.ToString)
            'Dia3
            txtFecha3.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia3.ObtenerValor.ToString)
            'Dia4
            txtFecha4.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia4.ObtenerValor.ToString)
            'Dia5
            txtFecha5.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia5.ObtenerValor.ToString)
            'Dia6
            txtFecha6.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia6.ObtenerValor.ToString)
            'Dia7
            txtFecha7.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia7.ObtenerValor.ToString)
            'Dia8
            txtFecha8.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia8.ObtenerValor.ToString)
            'Dia9
            txtFecha9.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia9.ObtenerValor.ToString)


        End If

    End Sub
    Function CalcularFecha2(ByVal Referencia As Date, ByRef Dias As Integer) As Date

        'CalcularFecha = Date.Now

        Dim Retorno As Date

        ' A Vencer
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            Retorno = Referencia.AddDays(Dias)
        End If

        'Vencido
        If cbxCondicion.cbx.SelectedIndex = 1 Then
            Retorno = Referencia.AddDays(Dias * -1)
        End If

        Return Retorno

    End Function
    Private Sub frmDistribuciondeSaldoDocumentoaCobrar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmDistribuciondeSaldoDocumentoaCobrar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtDia1_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia1.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia2_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia2.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia3_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia3.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia4_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia4.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia5_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia5.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia6_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia6.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia7_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia7.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia8_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia8.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia9_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia9.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub cbxCondicion_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxCondicion.PropertyChanged
        CalculaDiasFecha()
    End Sub

    Private Sub cbxCondicion_TeclaPresionada(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCondicion.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtReferencia_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.TeclaPrecionada

        CalculaDiasFecha()

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkDocumentos_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxDocumento.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub

    Private Sub chkEstadoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkEstadoCliente.PropertyChanged
        cbxEstadoCliente.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCobrador_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCobrador.PropertyChanged
        cbxCobrador.Enabled = value
    End Sub

    Private Sub cbxCondicion_TeclaPresionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxCondicion.TeclaPrecionada

    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkSucursalCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursalCliente.PropertyChanged
        cbxSucursalCliente.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub frmDistribuciondeSaldoDocumentoaCobrar_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        'FA 20/06/2023
        Me.Dispose()
    End Sub
End Class
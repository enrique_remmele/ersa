﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDistribuciondeSaldoDocumentoaCobrar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkSucursalCliente = New ERP.ocxCHK()
        Me.cbxSucursalCliente = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.cbxCobrador = New ERP.ocxCBX()
        Me.chkCobrador = New ERP.ocxCHK()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkEstadoCliente = New ERP.ocxCHK()
        Me.cbxEstadoCliente = New ERP.ocxCBX()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.chkZonaVenta = New ERP.ocxCHK()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtDia2 = New ERP.ocxTXTNumeric()
        Me.txtDia3 = New ERP.ocxTXTNumeric()
        Me.txtDia4 = New ERP.ocxTXTNumeric()
        Me.txtDia9 = New ERP.ocxTXTNumeric()
        Me.txtDia8 = New ERP.ocxTXTNumeric()
        Me.txtDia7 = New ERP.ocxTXTNumeric()
        Me.txtDia6 = New ERP.ocxTXTNumeric()
        Me.txtDia5 = New ERP.ocxTXTNumeric()
        Me.txtDia1 = New ERP.ocxTXTNumeric()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtFecha4 = New ERP.ocxTXTDate()
        Me.txtFecha9 = New ERP.ocxTXTDate()
        Me.txtFecha3 = New ERP.ocxTXTDate()
        Me.txtFecha1 = New ERP.ocxTXTDate()
        Me.txtFecha2 = New ERP.ocxTXTDate()
        Me.txtFecha8 = New ERP.ocxTXTDate()
        Me.txtFecha7 = New ERP.ocxTXTDate()
        Me.txtFecha5 = New ERP.ocxTXTDate()
        Me.txtFecha6 = New ERP.ocxTXTDate()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.chkVerJudiciales = New ERP.ocxCHK()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtReferencia = New ERP.ocxTXTDate()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxDocumento = New ERP.ocxCBX()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(696, 411)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 0
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(482, 411)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 3
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(613, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(146, 231)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Plazos (Diaz/Fecha)"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtCliente)
        Me.GroupBox3.Controls.Add(Me.chkCliente)
        Me.GroupBox3.Controls.Add(Me.chkSucursalCliente)
        Me.GroupBox3.Controls.Add(Me.cbxSucursalCliente)
        Me.GroupBox3.Controls.Add(Me.chkTipoProducto)
        Me.GroupBox3.Controls.Add(Me.cbxTipoProducto)
        Me.GroupBox3.Controls.Add(Me.cbxCobrador)
        Me.GroupBox3.Controls.Add(Me.chkCobrador)
        Me.GroupBox3.Controls.Add(Me.chkVendedor)
        Me.GroupBox3.Controls.Add(Me.chkMoneda)
        Me.GroupBox3.Controls.Add(Me.cbxMoneda)
        Me.GroupBox3.Controls.Add(Me.chkEstadoCliente)
        Me.GroupBox3.Controls.Add(Me.cbxEstadoCliente)
        Me.GroupBox3.Controls.Add(Me.chkTipoCliente)
        Me.GroupBox3.Controls.Add(Me.cbxTipoCliente)
        Me.GroupBox3.Controls.Add(Me.chkZonaVenta)
        Me.GroupBox3.Controls.Add(Me.cbxZonaVenta)
        Me.GroupBox3.Controls.Add(Me.chkSucursal)
        Me.GroupBox3.Controls.Add(Me.cbxSucursal)
        Me.GroupBox3.Controls.Add(Me.chkCiudad)
        Me.GroupBox3.Controls.Add(Me.cbxCiudad)
        Me.GroupBox3.Controls.Add(Me.cbxVendedor)
        Me.GroupBox3.Location = New System.Drawing.Point(10, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(348, 397)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Filtros"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 150
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(12, 317)
        Me.txtCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(324, 42)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 23
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(12, 294)
        Me.chkCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(53, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 22
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkSucursalCliente
        '
        Me.chkSucursalCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalCliente.Color = System.Drawing.Color.Empty
        Me.chkSucursalCliente.Location = New System.Drawing.Point(12, 266)
        Me.chkSucursalCliente.Name = "chkSucursalCliente"
        Me.chkSucursalCliente.Size = New System.Drawing.Size(96, 21)
        Me.chkSucursalCliente.SoloLectura = False
        Me.chkSucursalCliente.TabIndex = 19
        Me.chkSucursalCliente.Texto = "Sucursal Cliente:"
        Me.chkSucursalCliente.Valor = False
        '
        'cbxSucursalCliente
        '
        Me.cbxSucursalCliente.CampoWhere = "IDSucursalFiltro"
        Me.cbxSucursalCliente.CargarUnaSolaVez = False
        Me.cbxSucursalCliente.DataDisplayMember = "Descripcion"
        Me.cbxSucursalCliente.DataFilter = Nothing
        Me.cbxSucursalCliente.DataOrderBy = Nothing
        Me.cbxSucursalCliente.DataSource = "VSucursal"
        Me.cbxSucursalCliente.DataValueMember = "ID"
        Me.cbxSucursalCliente.dtSeleccionado = Nothing
        Me.cbxSucursalCliente.Enabled = False
        Me.cbxSucursalCliente.FormABM = Nothing
        Me.cbxSucursalCliente.Indicaciones = Nothing
        Me.cbxSucursalCliente.Location = New System.Drawing.Point(115, 266)
        Me.cbxSucursalCliente.Name = "cbxSucursalCliente"
        Me.cbxSucursalCliente.SeleccionMultiple = False
        Me.cbxSucursalCliente.SeleccionObligatoria = False
        Me.cbxSucursalCliente.Size = New System.Drawing.Size(221, 21)
        Me.cbxSucursalCliente.SoloLectura = False
        Me.cbxSucursalCliente.TabIndex = 20
        Me.cbxSucursalCliente.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(12, 239)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(96, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 17
        Me.chkTipoProducto.Texto = "Tipo producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "vTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(115, 239)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(221, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 18
        Me.cbxTipoProducto.Texto = ""
        '
        'cbxCobrador
        '
        Me.cbxCobrador.CampoWhere = "IDCobrador"
        Me.cbxCobrador.CargarUnaSolaVez = False
        Me.cbxCobrador.DataDisplayMember = "Nombres"
        Me.cbxCobrador.DataFilter = Nothing
        Me.cbxCobrador.DataOrderBy = "Nombres"
        Me.cbxCobrador.DataSource = "Cobrador"
        Me.cbxCobrador.DataValueMember = "ID"
        Me.cbxCobrador.dtSeleccionado = Nothing
        Me.cbxCobrador.Enabled = False
        Me.cbxCobrador.FormABM = Nothing
        Me.cbxCobrador.Indicaciones = Nothing
        Me.cbxCobrador.Location = New System.Drawing.Point(115, 43)
        Me.cbxCobrador.Name = "cbxCobrador"
        Me.cbxCobrador.SeleccionMultiple = False
        Me.cbxCobrador.SeleccionObligatoria = False
        Me.cbxCobrador.Size = New System.Drawing.Size(221, 21)
        Me.cbxCobrador.SoloLectura = False
        Me.cbxCobrador.TabIndex = 16
        Me.cbxCobrador.Texto = ""
        '
        'chkCobrador
        '
        Me.chkCobrador.BackColor = System.Drawing.Color.Transparent
        Me.chkCobrador.Color = System.Drawing.Color.Empty
        Me.chkCobrador.Location = New System.Drawing.Point(12, 43)
        Me.chkCobrador.Name = "chkCobrador"
        Me.chkCobrador.Size = New System.Drawing.Size(96, 21)
        Me.chkCobrador.SoloLectura = False
        Me.chkCobrador.TabIndex = 2
        Me.chkCobrador.Texto = "Cobrador:"
        Me.chkCobrador.Valor = False
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(12, 22)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(96, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 0
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(12, 212)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(96, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 14
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "Moneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(115, 212)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(221, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 15
        Me.cbxMoneda.Texto = ""
        '
        'chkEstadoCliente
        '
        Me.chkEstadoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkEstadoCliente.Color = System.Drawing.Color.Empty
        Me.chkEstadoCliente.Location = New System.Drawing.Point(12, 99)
        Me.chkEstadoCliente.Name = "chkEstadoCliente"
        Me.chkEstadoCliente.Size = New System.Drawing.Size(96, 21)
        Me.chkEstadoCliente.SoloLectura = False
        Me.chkEstadoCliente.TabIndex = 6
        Me.chkEstadoCliente.Texto = "Estado Cliente:"
        Me.chkEstadoCliente.Valor = False
        '
        'cbxEstadoCliente
        '
        Me.cbxEstadoCliente.CampoWhere = "IDEstado"
        Me.cbxEstadoCliente.CargarUnaSolaVez = False
        Me.cbxEstadoCliente.DataDisplayMember = "Descripcion"
        Me.cbxEstadoCliente.DataFilter = Nothing
        Me.cbxEstadoCliente.DataOrderBy = "Descripcion"
        Me.cbxEstadoCliente.DataSource = "EstadoCliente"
        Me.cbxEstadoCliente.DataValueMember = "ID"
        Me.cbxEstadoCliente.dtSeleccionado = Nothing
        Me.cbxEstadoCliente.Enabled = False
        Me.cbxEstadoCliente.FormABM = Nothing
        Me.cbxEstadoCliente.Indicaciones = Nothing
        Me.cbxEstadoCliente.Location = New System.Drawing.Point(115, 98)
        Me.cbxEstadoCliente.Name = "cbxEstadoCliente"
        Me.cbxEstadoCliente.SeleccionMultiple = False
        Me.cbxEstadoCliente.SeleccionObligatoria = False
        Me.cbxEstadoCliente.Size = New System.Drawing.Size(221, 21)
        Me.cbxEstadoCliente.SoloLectura = False
        Me.cbxEstadoCliente.TabIndex = 7
        Me.cbxEstadoCliente.Texto = ""
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(12, 77)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(96, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 4
        Me.chkTipoCliente.Texto = "Tipo de Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "TipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(115, 77)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(221, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 5
        Me.cbxTipoCliente.Texto = ""
        '
        'chkZonaVenta
        '
        Me.chkZonaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkZonaVenta.Color = System.Drawing.Color.Empty
        Me.chkZonaVenta.Location = New System.Drawing.Point(12, 173)
        Me.chkZonaVenta.Name = "chkZonaVenta"
        Me.chkZonaVenta.Size = New System.Drawing.Size(96, 21)
        Me.chkZonaVenta.SoloLectura = False
        Me.chkZonaVenta.TabIndex = 12
        Me.chkZonaVenta.Texto = "Zona de Venta:"
        Me.chkZonaVenta.Valor = False
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = "IDZonaVenta"
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = Nothing
        Me.cbxZonaVenta.DataSource = "ZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.dtSeleccionado = Nothing
        Me.cbxZonaVenta.Enabled = False
        Me.cbxZonaVenta.FormABM = Nothing
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(115, 173)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionMultiple = False
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(221, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 13
        Me.cbxZonaVenta.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(12, 152)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(96, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 10
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(115, 152)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(221, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 11
        Me.cbxSucursal.Texto = ""
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(12, 131)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(96, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 8
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudad"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Ciudad"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VSucursal"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(115, 131)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(221, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 9
        Me.cbxCiudad.Texto = ""
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = "frmVendedor"
        Me.cbxVendedor.Indicaciones = "Seleccion de Vendedor"
        Me.cbxVendedor.Location = New System.Drawing.Point(115, 22)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(221, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 1
        Me.cbxVendedor.Texto = ""
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtDia2)
        Me.GroupBox4.Controls.Add(Me.txtDia3)
        Me.GroupBox4.Controls.Add(Me.txtDia4)
        Me.GroupBox4.Controls.Add(Me.txtDia9)
        Me.GroupBox4.Controls.Add(Me.txtDia8)
        Me.GroupBox4.Controls.Add(Me.txtDia7)
        Me.GroupBox4.Controls.Add(Me.txtDia6)
        Me.GroupBox4.Controls.Add(Me.txtDia5)
        Me.GroupBox4.Controls.Add(Me.txtDia1)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.txtFecha4)
        Me.GroupBox4.Controls.Add(Me.txtFecha9)
        Me.GroupBox4.Controls.Add(Me.txtFecha3)
        Me.GroupBox4.Controls.Add(Me.txtFecha1)
        Me.GroupBox4.Controls.Add(Me.txtFecha2)
        Me.GroupBox4.Controls.Add(Me.txtFecha8)
        Me.GroupBox4.Controls.Add(Me.txtFecha7)
        Me.GroupBox4.Controls.Add(Me.txtFecha5)
        Me.GroupBox4.Controls.Add(Me.txtFecha6)
        Me.GroupBox4.Location = New System.Drawing.Point(613, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(146, 397)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        '
        'txtDia2
        '
        Me.txtDia2.Color = System.Drawing.Color.Empty
        Me.txtDia2.Decimales = True
        Me.txtDia2.Indicaciones = Nothing
        Me.txtDia2.Location = New System.Drawing.Point(15, 59)
        Me.txtDia2.Name = "txtDia2"
        Me.txtDia2.Size = New System.Drawing.Size(40, 20)
        Me.txtDia2.SoloLectura = False
        Me.txtDia2.TabIndex = 3
        Me.txtDia2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia2.Texto = "0"
        '
        'txtDia3
        '
        Me.txtDia3.Color = System.Drawing.Color.Empty
        Me.txtDia3.Decimales = True
        Me.txtDia3.Indicaciones = Nothing
        Me.txtDia3.Location = New System.Drawing.Point(15, 79)
        Me.txtDia3.Name = "txtDia3"
        Me.txtDia3.Size = New System.Drawing.Size(40, 20)
        Me.txtDia3.SoloLectura = False
        Me.txtDia3.TabIndex = 5
        Me.txtDia3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia3.Texto = "0"
        '
        'txtDia4
        '
        Me.txtDia4.Color = System.Drawing.Color.Empty
        Me.txtDia4.Decimales = True
        Me.txtDia4.Indicaciones = Nothing
        Me.txtDia4.Location = New System.Drawing.Point(15, 99)
        Me.txtDia4.Name = "txtDia4"
        Me.txtDia4.Size = New System.Drawing.Size(40, 20)
        Me.txtDia4.SoloLectura = False
        Me.txtDia4.TabIndex = 7
        Me.txtDia4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia4.Texto = "0"
        '
        'txtDia9
        '
        Me.txtDia9.Color = System.Drawing.Color.Empty
        Me.txtDia9.Decimales = True
        Me.txtDia9.Indicaciones = Nothing
        Me.txtDia9.Location = New System.Drawing.Point(15, 199)
        Me.txtDia9.Name = "txtDia9"
        Me.txtDia9.Size = New System.Drawing.Size(40, 20)
        Me.txtDia9.SoloLectura = False
        Me.txtDia9.TabIndex = 17
        Me.txtDia9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia9.Texto = "0"
        '
        'txtDia8
        '
        Me.txtDia8.Color = System.Drawing.Color.Empty
        Me.txtDia8.Decimales = True
        Me.txtDia8.Indicaciones = Nothing
        Me.txtDia8.Location = New System.Drawing.Point(15, 179)
        Me.txtDia8.Name = "txtDia8"
        Me.txtDia8.Size = New System.Drawing.Size(40, 20)
        Me.txtDia8.SoloLectura = False
        Me.txtDia8.TabIndex = 15
        Me.txtDia8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia8.Texto = "0"
        '
        'txtDia7
        '
        Me.txtDia7.Color = System.Drawing.Color.Empty
        Me.txtDia7.Decimales = True
        Me.txtDia7.Indicaciones = Nothing
        Me.txtDia7.Location = New System.Drawing.Point(15, 159)
        Me.txtDia7.Name = "txtDia7"
        Me.txtDia7.Size = New System.Drawing.Size(40, 20)
        Me.txtDia7.SoloLectura = False
        Me.txtDia7.TabIndex = 13
        Me.txtDia7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia7.Texto = "0"
        '
        'txtDia6
        '
        Me.txtDia6.Color = System.Drawing.Color.Empty
        Me.txtDia6.Decimales = True
        Me.txtDia6.Indicaciones = Nothing
        Me.txtDia6.Location = New System.Drawing.Point(15, 139)
        Me.txtDia6.Name = "txtDia6"
        Me.txtDia6.Size = New System.Drawing.Size(40, 20)
        Me.txtDia6.SoloLectura = False
        Me.txtDia6.TabIndex = 11
        Me.txtDia6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia6.Texto = "0"
        '
        'txtDia5
        '
        Me.txtDia5.Color = System.Drawing.Color.Empty
        Me.txtDia5.Decimales = True
        Me.txtDia5.Indicaciones = Nothing
        Me.txtDia5.Location = New System.Drawing.Point(15, 119)
        Me.txtDia5.Name = "txtDia5"
        Me.txtDia5.Size = New System.Drawing.Size(40, 20)
        Me.txtDia5.SoloLectura = False
        Me.txtDia5.TabIndex = 9
        Me.txtDia5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia5.Texto = "0"
        '
        'txtDia1
        '
        Me.txtDia1.Color = System.Drawing.Color.Empty
        Me.txtDia1.Decimales = True
        Me.txtDia1.Indicaciones = Nothing
        Me.txtDia1.Location = New System.Drawing.Point(15, 39)
        Me.txtDia1.Name = "txtDia1"
        Me.txtDia1.Size = New System.Drawing.Size(40, 20)
        Me.txtDia1.SoloLectura = False
        Me.txtDia1.TabIndex = 1
        Me.txtDia1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia1.Texto = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Plazos (Diaz/Fecha)"
        '
        'txtFecha4
        '
        Me.txtFecha4.AñoFecha = 0
        Me.txtFecha4.Color = System.Drawing.Color.Empty
        Me.txtFecha4.Enabled = False
        Me.txtFecha4.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha4.Location = New System.Drawing.Point(57, 99)
        Me.txtFecha4.MesFecha = 0
        Me.txtFecha4.Name = "txtFecha4"
        Me.txtFecha4.PermitirNulo = False
        Me.txtFecha4.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha4.SoloLectura = True
        Me.txtFecha4.TabIndex = 8
        Me.txtFecha4.TabStop = False
        '
        'txtFecha9
        '
        Me.txtFecha9.AñoFecha = 0
        Me.txtFecha9.Color = System.Drawing.Color.Empty
        Me.txtFecha9.Enabled = False
        Me.txtFecha9.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha9.Location = New System.Drawing.Point(57, 199)
        Me.txtFecha9.MesFecha = 0
        Me.txtFecha9.Name = "txtFecha9"
        Me.txtFecha9.PermitirNulo = False
        Me.txtFecha9.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha9.SoloLectura = True
        Me.txtFecha9.TabIndex = 18
        Me.txtFecha9.TabStop = False
        '
        'txtFecha3
        '
        Me.txtFecha3.AñoFecha = 0
        Me.txtFecha3.Color = System.Drawing.Color.Empty
        Me.txtFecha3.Enabled = False
        Me.txtFecha3.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha3.Location = New System.Drawing.Point(57, 79)
        Me.txtFecha3.MesFecha = 0
        Me.txtFecha3.Name = "txtFecha3"
        Me.txtFecha3.PermitirNulo = False
        Me.txtFecha3.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha3.SoloLectura = True
        Me.txtFecha3.TabIndex = 6
        Me.txtFecha3.TabStop = False
        '
        'txtFecha1
        '
        Me.txtFecha1.AñoFecha = 0
        Me.txtFecha1.Color = System.Drawing.Color.Empty
        Me.txtFecha1.Enabled = False
        Me.txtFecha1.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha1.Location = New System.Drawing.Point(57, 39)
        Me.txtFecha1.MesFecha = 0
        Me.txtFecha1.Name = "txtFecha1"
        Me.txtFecha1.PermitirNulo = False
        Me.txtFecha1.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha1.SoloLectura = True
        Me.txtFecha1.TabIndex = 2
        Me.txtFecha1.TabStop = False
        '
        'txtFecha2
        '
        Me.txtFecha2.AñoFecha = 0
        Me.txtFecha2.Color = System.Drawing.Color.Empty
        Me.txtFecha2.Enabled = False
        Me.txtFecha2.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha2.Location = New System.Drawing.Point(57, 59)
        Me.txtFecha2.MesFecha = 0
        Me.txtFecha2.Name = "txtFecha2"
        Me.txtFecha2.PermitirNulo = False
        Me.txtFecha2.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha2.SoloLectura = True
        Me.txtFecha2.TabIndex = 4
        Me.txtFecha2.TabStop = False
        '
        'txtFecha8
        '
        Me.txtFecha8.AñoFecha = 0
        Me.txtFecha8.Color = System.Drawing.Color.Empty
        Me.txtFecha8.Enabled = False
        Me.txtFecha8.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha8.Location = New System.Drawing.Point(57, 179)
        Me.txtFecha8.MesFecha = 0
        Me.txtFecha8.Name = "txtFecha8"
        Me.txtFecha8.PermitirNulo = False
        Me.txtFecha8.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha8.SoloLectura = True
        Me.txtFecha8.TabIndex = 16
        Me.txtFecha8.TabStop = False
        '
        'txtFecha7
        '
        Me.txtFecha7.AñoFecha = 0
        Me.txtFecha7.Color = System.Drawing.Color.Empty
        Me.txtFecha7.Enabled = False
        Me.txtFecha7.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha7.Location = New System.Drawing.Point(57, 159)
        Me.txtFecha7.MesFecha = 0
        Me.txtFecha7.Name = "txtFecha7"
        Me.txtFecha7.PermitirNulo = False
        Me.txtFecha7.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha7.SoloLectura = True
        Me.txtFecha7.TabIndex = 14
        Me.txtFecha7.TabStop = False
        '
        'txtFecha5
        '
        Me.txtFecha5.AñoFecha = 0
        Me.txtFecha5.Color = System.Drawing.Color.Empty
        Me.txtFecha5.Enabled = False
        Me.txtFecha5.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha5.Location = New System.Drawing.Point(57, 119)
        Me.txtFecha5.MesFecha = 0
        Me.txtFecha5.Name = "txtFecha5"
        Me.txtFecha5.PermitirNulo = False
        Me.txtFecha5.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha5.SoloLectura = True
        Me.txtFecha5.TabIndex = 10
        Me.txtFecha5.TabStop = False
        '
        'txtFecha6
        '
        Me.txtFecha6.AñoFecha = 0
        Me.txtFecha6.Color = System.Drawing.Color.Empty
        Me.txtFecha6.Enabled = False
        Me.txtFecha6.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtFecha6.Location = New System.Drawing.Point(57, 139)
        Me.txtFecha6.MesFecha = 0
        Me.txtFecha6.Name = "txtFecha6"
        Me.txtFecha6.PermitirNulo = False
        Me.txtFecha6.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha6.SoloLectura = True
        Me.txtFecha6.TabIndex = 12
        Me.txtFecha6.TabStop = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.chkVerJudiciales)
        Me.GroupBox5.Controls.Add(Me.Label2)
        Me.GroupBox5.Controls.Add(Me.cbxCondicion)
        Me.GroupBox5.Controls.Add(Me.Label4)
        Me.GroupBox5.Controls.Add(Me.txtReferencia)
        Me.GroupBox5.Controls.Add(Me.cbxEnForma)
        Me.GroupBox5.Controls.Add(Me.cbxDocumento)
        Me.GroupBox5.Controls.Add(Me.nudRanking)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox5.Controls.Add(Me.Label5)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Location = New System.Drawing.Point(361, 4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(246, 397)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Opciones"
        '
        'chkVerJudiciales
        '
        Me.chkVerJudiciales.BackColor = System.Drawing.Color.Transparent
        Me.chkVerJudiciales.Color = System.Drawing.Color.Empty
        Me.chkVerJudiciales.Location = New System.Drawing.Point(8, 248)
        Me.chkVerJudiciales.Name = "chkVerJudiciales"
        Me.chkVerJudiciales.Size = New System.Drawing.Size(189, 21)
        Me.chkVerJudiciales.SoloLectura = False
        Me.chkVerJudiciales.TabIndex = 19
        Me.chkVerJudiciales.Texto = "Ver Judiciales"
        Me.chkVerJudiciales.Valor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Documentos:"
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(7, 31)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = True
        Me.cbxCondicion.Size = New System.Drawing.Size(160, 23)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 1
        Me.cbxCondicion.Texto = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Referencia:"
        '
        'txtReferencia
        '
        Me.txtReferencia.AñoFecha = 0
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Fecha = New Date(2013, 7, 2, 8, 28, 43, 541)
        Me.txtReferencia.Location = New System.Drawing.Point(7, 77)
        Me.txtReferencia.MesFecha = 0
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.PermitirNulo = False
        Me.txtReferencia.Size = New System.Drawing.Size(74, 20)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 9
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(166, 173)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 4
        Me.cbxEnForma.Texto = ""
        '
        'cbxDocumento
        '
        Me.cbxDocumento.CampoWhere = ""
        Me.cbxDocumento.CargarUnaSolaVez = False
        Me.cbxDocumento.DataDisplayMember = ""
        Me.cbxDocumento.DataFilter = Nothing
        Me.cbxDocumento.DataOrderBy = ""
        Me.cbxDocumento.DataSource = ""
        Me.cbxDocumento.DataValueMember = ""
        Me.cbxDocumento.dtSeleccionado = Nothing
        Me.cbxDocumento.FormABM = "frmTipoComprobante"
        Me.cbxDocumento.Indicaciones = "Seleccion de Vendedor"
        Me.cbxDocumento.Location = New System.Drawing.Point(7, 131)
        Me.cbxDocumento.Name = "cbxDocumento"
        Me.cbxDocumento.SeleccionMultiple = False
        Me.cbxDocumento.SeleccionObligatoria = True
        Me.cbxDocumento.Size = New System.Drawing.Size(230, 21)
        Me.cbxDocumento.SoloLectura = False
        Me.cbxDocumento.TabIndex = 7
        Me.cbxDocumento.Texto = ""
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(7, 213)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 197)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 13)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Ranking:"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(7, 173)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(158, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 3
        Me.cbxOrdenadoPor.Texto = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 156)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Ordenado:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(57, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Condicion:"
        '
        'frmDistribuciondeSaldoDocumentoaCobrar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(768, 447)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmDistribuciondeSaldoDocumentoaCobrar"
        Me.Text = "frmDistribuciondeSaldoDocumentoaCobrar"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxDocumento As ERP.ocxCBX
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents chkEstadoCliente As ERP.ocxCHK
    Friend WithEvents cbxEstadoCliente As ERP.ocxCBX
    Friend WithEvents chkTipoCliente As ERP.ocxCHK
    Friend WithEvents cbxTipoCliente As ERP.ocxCBX
    Friend WithEvents chkZonaVenta As ERP.ocxCHK
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDia3 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia9 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia8 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia7 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia6 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia5 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia1 As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtFecha4 As ERP.ocxTXTDate
    Friend WithEvents txtFecha9 As ERP.ocxTXTDate
    Friend WithEvents txtFecha3 As ERP.ocxTXTDate
    Friend WithEvents txtFecha1 As ERP.ocxTXTDate
    Friend WithEvents txtFecha2 As ERP.ocxTXTDate
    Friend WithEvents txtFecha7 As ERP.ocxTXTDate
    Friend WithEvents txtFecha5 As ERP.ocxTXTDate
    Friend WithEvents txtFecha6 As ERP.ocxTXTDate
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As ERP.ocxTXTDate
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents chkCobrador As ERP.ocxCHK
    Friend WithEvents txtDia2 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia4 As ERP.ocxTXTNumeric
    Friend WithEvents txtFecha8 As ERP.ocxTXTDate
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxCobrador As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkVerJudiciales As ERP.ocxCHK
    Friend WithEvents chkSucursalCliente As ERP.ocxCHK
    Friend WithEvents cbxSucursalCliente As ERP.ocxCBX
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkCliente As ERP.ocxCHK
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInventariodeDocumentosPendientesCobrar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkClienteSucursal = New ERP.ocxCHK()
        Me.cbxClienteSucursal = New ERP.ocxCBX()
        Me.cbxSucursalCliente = New ERP.ocxCBX()
        Me.chkSucursalCliente = New ERP.ocxCHK()
        Me.chkCCHasta = New ERP.ocxCHK()
        Me.txtCuentaContableHasta = New ERP.ocxTXTCuentaContable()
        Me.chkCC = New ERP.ocxCHK()
        Me.txtCuentaContableDesde = New ERP.ocxTXTCuentaContable()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.chkChofer = New ERP.ocxCHK()
        Me.cbxCobrador = New ERP.ocxCBX()
        Me.chkCobrador = New ERP.ocxCHK()
        Me.cbxEstadoCliente = New ERP.ocxCBX()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.chkEstadoCliente = New ERP.ocxCHK()
        Me.chkZonaVenta = New ERP.ocxCHK()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.chkExcluirDiferido = New ERP.ocxCHK()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkJudicial = New ERP.ocxCHK()
        Me.chkBoletaContraBoleta = New ERP.ocxCHK()
        Me.chkClienteChequeDiferidoFacturaPendiente = New ERP.ocxCHK()
        Me.chkMostrarAnticipo = New ERP.ocxCHK()
        Me.chkResumido = New ERP.ocxCHK()
        Me.chkDescontados = New ERP.ocxCHK()
        Me.chkInventarioDetallado = New ERP.ocxCHK()
        Me.chkDireccionAlternativa = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxDocumento = New ERP.ocxCBX()
        Me.cbxVencimiento = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.chkChequeDiferido = New ERP.ocxCHK()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkClienteSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxClienteSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursalCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursalCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCCHasta)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaContableHasta)
        Me.gbxFiltro.Controls.Add(Me.chkCC)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaContableDesde)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.chkTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxChofer)
        Me.gbxFiltro.Controls.Add(Me.chkChofer)
        Me.gbxFiltro.Controls.Add(Me.cbxCobrador)
        Me.gbxFiltro.Controls.Add(Me.chkCobrador)
        Me.gbxFiltro.Controls.Add(Me.cbxEstadoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkEstadoCliente)
        Me.gbxFiltro.Controls.Add(Me.chkZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(446, 493)
        Me.gbxFiltro.TabIndex = 1
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkClienteSucursal
        '
        Me.chkClienteSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkClienteSucursal.Color = System.Drawing.Color.Empty
        Me.chkClienteSucursal.Location = New System.Drawing.Point(11, 457)
        Me.chkClienteSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkClienteSucursal.Name = "chkClienteSucursal"
        Me.chkClienteSucursal.Size = New System.Drawing.Size(103, 21)
        Me.chkClienteSucursal.SoloLectura = False
        Me.chkClienteSucursal.TabIndex = 33
        Me.chkClienteSucursal.Texto = "Cliente Sucursal:"
        Me.chkClienteSucursal.Valor = False
        '
        'cbxClienteSucursal
        '
        Me.cbxClienteSucursal.CampoWhere = ""
        Me.cbxClienteSucursal.CargarUnaSolaVez = False
        Me.cbxClienteSucursal.DataDisplayMember = ""
        Me.cbxClienteSucursal.DataFilter = Nothing
        Me.cbxClienteSucursal.DataOrderBy = ""
        Me.cbxClienteSucursal.DataSource = ""
        Me.cbxClienteSucursal.DataValueMember = "ID"
        Me.cbxClienteSucursal.dtSeleccionado = Nothing
        Me.cbxClienteSucursal.Enabled = False
        Me.cbxClienteSucursal.FormABM = Nothing
        Me.cbxClienteSucursal.Indicaciones = Nothing
        Me.cbxClienteSucursal.Location = New System.Drawing.Point(122, 458)
        Me.cbxClienteSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxClienteSucursal.Name = "cbxClienteSucursal"
        Me.cbxClienteSucursal.SeleccionMultiple = False
        Me.cbxClienteSucursal.SeleccionObligatoria = False
        Me.cbxClienteSucursal.Size = New System.Drawing.Size(317, 21)
        Me.cbxClienteSucursal.SoloLectura = False
        Me.cbxClienteSucursal.TabIndex = 32
        Me.cbxClienteSucursal.Texto = ""
        '
        'cbxSucursalCliente
        '
        Me.cbxSucursalCliente.CampoWhere = "IDSucursalFiltro"
        Me.cbxSucursalCliente.CargarUnaSolaVez = False
        Me.cbxSucursalCliente.DataDisplayMember = "Descripcion"
        Me.cbxSucursalCliente.DataFilter = Nothing
        Me.cbxSucursalCliente.DataOrderBy = ""
        Me.cbxSucursalCliente.DataSource = "VSucursal"
        Me.cbxSucursalCliente.DataValueMember = "ID"
        Me.cbxSucursalCliente.dtSeleccionado = Nothing
        Me.cbxSucursalCliente.Enabled = False
        Me.cbxSucursalCliente.FormABM = Nothing
        Me.cbxSucursalCliente.Indicaciones = Nothing
        Me.cbxSucursalCliente.Location = New System.Drawing.Point(139, 312)
        Me.cbxSucursalCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursalCliente.Name = "cbxSucursalCliente"
        Me.cbxSucursalCliente.SeleccionMultiple = False
        Me.cbxSucursalCliente.SeleccionObligatoria = False
        Me.cbxSucursalCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursalCliente.SoloLectura = False
        Me.cbxSucursalCliente.TabIndex = 25
        Me.cbxSucursalCliente.Texto = ""
        '
        'chkSucursalCliente
        '
        Me.chkSucursalCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalCliente.Color = System.Drawing.Color.Empty
        Me.chkSucursalCliente.Location = New System.Drawing.Point(11, 314)
        Me.chkSucursalCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursalCliente.Name = "chkSucursalCliente"
        Me.chkSucursalCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursalCliente.SoloLectura = False
        Me.chkSucursalCliente.TabIndex = 24
        Me.chkSucursalCliente.Texto = "Sucursal del Cliente:"
        Me.chkSucursalCliente.Valor = False
        '
        'chkCCHasta
        '
        Me.chkCCHasta.BackColor = System.Drawing.Color.Transparent
        Me.chkCCHasta.Color = System.Drawing.Color.Empty
        Me.chkCCHasta.Location = New System.Drawing.Point(11, 388)
        Me.chkCCHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCCHasta.Name = "chkCCHasta"
        Me.chkCCHasta.Size = New System.Drawing.Size(73, 21)
        Me.chkCCHasta.SoloLectura = True
        Me.chkCCHasta.TabIndex = 28
        Me.chkCCHasta.Texto = "CC Hasta:"
        Me.chkCCHasta.Valor = False
        '
        'txtCuentaContableHasta
        '
        Me.txtCuentaContableHasta.AlturaMaxima = 205
        Me.txtCuentaContableHasta.Consulta = Nothing
        Me.txtCuentaContableHasta.Enabled = False
        Me.txtCuentaContableHasta.IDCentroCosto = 0
        Me.txtCuentaContableHasta.IDUnidadNegocio = 0
        Me.txtCuentaContableHasta.ListarTodas = False
        Me.txtCuentaContableHasta.Location = New System.Drawing.Point(84, 388)
        Me.txtCuentaContableHasta.Name = "txtCuentaContableHasta"
        Me.txtCuentaContableHasta.Registro = Nothing
        Me.txtCuentaContableHasta.Resolucion173 = False
        Me.txtCuentaContableHasta.Seleccionado = False
        Me.txtCuentaContableHasta.Size = New System.Drawing.Size(358, 27)
        Me.txtCuentaContableHasta.SoloLectura = False
        Me.txtCuentaContableHasta.TabIndex = 29
        Me.txtCuentaContableHasta.Texto = Nothing
        Me.txtCuentaContableHasta.whereFiltro = Nothing
        '
        'chkCC
        '
        Me.chkCC.BackColor = System.Drawing.Color.Transparent
        Me.chkCC.Color = System.Drawing.Color.Empty
        Me.chkCC.Location = New System.Drawing.Point(11, 359)
        Me.chkCC.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCC.Name = "chkCC"
        Me.chkCC.Size = New System.Drawing.Size(73, 21)
        Me.chkCC.SoloLectura = False
        Me.chkCC.TabIndex = 26
        Me.chkCC.Texto = "CC Desde:"
        Me.chkCC.Valor = False
        '
        'txtCuentaContableDesde
        '
        Me.txtCuentaContableDesde.AlturaMaxima = 205
        Me.txtCuentaContableDesde.Consulta = Nothing
        Me.txtCuentaContableDesde.Enabled = False
        Me.txtCuentaContableDesde.IDCentroCosto = 0
        Me.txtCuentaContableDesde.IDUnidadNegocio = 0
        Me.txtCuentaContableDesde.ListarTodas = False
        Me.txtCuentaContableDesde.Location = New System.Drawing.Point(84, 359)
        Me.txtCuentaContableDesde.Name = "txtCuentaContableDesde"
        Me.txtCuentaContableDesde.Registro = Nothing
        Me.txtCuentaContableDesde.Resolucion173 = False
        Me.txtCuentaContableDesde.Seleccionado = False
        Me.txtCuentaContableDesde.Size = New System.Drawing.Size(358, 27)
        Me.txtCuentaContableDesde.SoloLectura = False
        Me.txtCuentaContableDesde.TabIndex = 27
        Me.txtCuentaContableDesde.Texto = Nothing
        Me.txtCuentaContableDesde.whereFiltro = Nothing
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(10, 289)
        Me.chkMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(114, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 22
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(11, 264)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 20
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(139, 260)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 21
        Me.cbxTipoProducto.Texto = ""
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 150
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(84, 420)
        Me.txtCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(358, 42)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 31
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(139, 286)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(213, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 23
        Me.cbxMoneda.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(11, 417)
        Me.chkCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(53, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 30
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "TipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(139, 215)
        Me.cbxTipoCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 17
        Me.cbxTipoCliente.Texto = ""
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(11, 217)
        Me.chkTipoCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 16
        Me.chkTipoCliente.Texto = "Tipo de Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = "IDChofer"
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "Chofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.dtSeleccionado = Nothing
        Me.cbxChofer.Enabled = False
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(139, 99)
        Me.cbxChofer.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionMultiple = False
        Me.cbxChofer.SeleccionObligatoria = False
        Me.cbxChofer.Size = New System.Drawing.Size(213, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 7
        Me.cbxChofer.Texto = ""
        '
        'chkChofer
        '
        Me.chkChofer.BackColor = System.Drawing.Color.Transparent
        Me.chkChofer.Color = System.Drawing.Color.Empty
        Me.chkChofer.Location = New System.Drawing.Point(11, 101)
        Me.chkChofer.Margin = New System.Windows.Forms.Padding(4)
        Me.chkChofer.Name = "chkChofer"
        Me.chkChofer.Size = New System.Drawing.Size(114, 21)
        Me.chkChofer.SoloLectura = False
        Me.chkChofer.TabIndex = 6
        Me.chkChofer.Texto = "Chofer:"
        Me.chkChofer.Valor = False
        '
        'cbxCobrador
        '
        Me.cbxCobrador.CampoWhere = "IDCobrador"
        Me.cbxCobrador.CargarUnaSolaVez = False
        Me.cbxCobrador.DataDisplayMember = "Nombres"
        Me.cbxCobrador.DataFilter = Nothing
        Me.cbxCobrador.DataOrderBy = "Nombres"
        Me.cbxCobrador.DataSource = "VCobrador"
        Me.cbxCobrador.DataValueMember = "ID"
        Me.cbxCobrador.dtSeleccionado = Nothing
        Me.cbxCobrador.Enabled = False
        Me.cbxCobrador.FormABM = Nothing
        Me.cbxCobrador.Indicaciones = Nothing
        Me.cbxCobrador.Location = New System.Drawing.Point(139, 76)
        Me.cbxCobrador.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCobrador.Name = "cbxCobrador"
        Me.cbxCobrador.SeleccionMultiple = False
        Me.cbxCobrador.SeleccionObligatoria = False
        Me.cbxCobrador.Size = New System.Drawing.Size(213, 21)
        Me.cbxCobrador.SoloLectura = False
        Me.cbxCobrador.TabIndex = 5
        Me.cbxCobrador.Texto = ""
        '
        'chkCobrador
        '
        Me.chkCobrador.BackColor = System.Drawing.Color.Transparent
        Me.chkCobrador.Color = System.Drawing.Color.Empty
        Me.chkCobrador.Location = New System.Drawing.Point(11, 76)
        Me.chkCobrador.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCobrador.Name = "chkCobrador"
        Me.chkCobrador.Size = New System.Drawing.Size(114, 21)
        Me.chkCobrador.SoloLectura = False
        Me.chkCobrador.TabIndex = 4
        Me.chkCobrador.Texto = "Cobrador:"
        Me.chkCobrador.Valor = False
        '
        'cbxEstadoCliente
        '
        Me.cbxEstadoCliente.CampoWhere = "IDEstado"
        Me.cbxEstadoCliente.CargarUnaSolaVez = False
        Me.cbxEstadoCliente.DataDisplayMember = "Descripcion"
        Me.cbxEstadoCliente.DataFilter = Nothing
        Me.cbxEstadoCliente.DataOrderBy = "Descripcion"
        Me.cbxEstadoCliente.DataSource = "EstadoCliente"
        Me.cbxEstadoCliente.DataValueMember = "ID"
        Me.cbxEstadoCliente.dtSeleccionado = Nothing
        Me.cbxEstadoCliente.Enabled = False
        Me.cbxEstadoCliente.FormABM = Nothing
        Me.cbxEstadoCliente.Indicaciones = Nothing
        Me.cbxEstadoCliente.Location = New System.Drawing.Point(139, 237)
        Me.cbxEstadoCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEstadoCliente.Name = "cbxEstadoCliente"
        Me.cbxEstadoCliente.SeleccionMultiple = False
        Me.cbxEstadoCliente.SeleccionObligatoria = False
        Me.cbxEstadoCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxEstadoCliente.SoloLectura = False
        Me.cbxEstadoCliente.TabIndex = 19
        Me.cbxEstadoCliente.Texto = ""
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = "IDZonaVenta"
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = "Descripcion"
        Me.cbxZonaVenta.DataSource = "ZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.dtSeleccionado = Nothing
        Me.cbxZonaVenta.Enabled = False
        Me.cbxZonaVenta.FormABM = Nothing
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(139, 192)
        Me.cbxZonaVenta.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionMultiple = False
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(213, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 15
        Me.cbxZonaVenta.Texto = ""
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "Deposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "Deposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(139, 168)
        Me.cbxDeposito.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(213, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 13
        Me.cbxDeposito.Texto = ""
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = ""
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(139, 145)
        Me.cbxSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 11
        Me.cbxSucursal.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudad"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Ciudad"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = ""
        Me.cbxCiudad.DataSource = "VSucursal"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(139, 122)
        Me.cbxCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(213, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 9
        Me.cbxCiudad.Texto = ""
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(139, 53)
        Me.cbxVendedor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(213, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 3
        Me.cbxVendedor.Texto = ""
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = "Descripcion"
        Me.cbxTipoComprobante.DataSource = "VTipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(139, 30)
        Me.cbxTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = ""
        '
        'chkEstadoCliente
        '
        Me.chkEstadoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkEstadoCliente.Color = System.Drawing.Color.Empty
        Me.chkEstadoCliente.Location = New System.Drawing.Point(11, 240)
        Me.chkEstadoCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkEstadoCliente.Name = "chkEstadoCliente"
        Me.chkEstadoCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkEstadoCliente.SoloLectura = False
        Me.chkEstadoCliente.TabIndex = 18
        Me.chkEstadoCliente.Texto = "Estado Cliente:"
        Me.chkEstadoCliente.Valor = False
        '
        'chkZonaVenta
        '
        Me.chkZonaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkZonaVenta.Color = System.Drawing.Color.Empty
        Me.chkZonaVenta.Location = New System.Drawing.Point(11, 192)
        Me.chkZonaVenta.Margin = New System.Windows.Forms.Padding(4)
        Me.chkZonaVenta.Name = "chkZonaVenta"
        Me.chkZonaVenta.Size = New System.Drawing.Size(114, 21)
        Me.chkZonaVenta.SoloLectura = False
        Me.chkZonaVenta.TabIndex = 14
        Me.chkZonaVenta.Texto = "Zona de Venta:"
        Me.chkZonaVenta.Valor = False
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(11, 170)
        Me.chkDeposito.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(114, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 12
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(11, 147)
        Me.chkSucursal.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 10
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(11, 125)
        Me.chkCiudad.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(114, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 8
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(11, 53)
        Me.chkVendedor.Margin = New System.Windows.Forms.Padding(4)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(114, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 2
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(11, 30)
        Me.chkTipoComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 0
        Me.chkTipoComprobante.Texto = "Tipo Comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'chkExcluirDiferido
        '
        Me.chkExcluirDiferido.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirDiferido.Color = System.Drawing.Color.Empty
        Me.chkExcluirDiferido.Location = New System.Drawing.Point(5, 388)
        Me.chkExcluirDiferido.Margin = New System.Windows.Forms.Padding(4)
        Me.chkExcluirDiferido.Name = "chkExcluirDiferido"
        Me.chkExcluirDiferido.Size = New System.Drawing.Size(135, 21)
        Me.chkExcluirDiferido.SoloLectura = False
        Me.chkExcluirDiferido.TabIndex = 18
        Me.chkExcluirDiferido.Texto = "Excluir Cheque Diferido"
        Me.chkExcluirDiferido.Valor = False
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(616, 502)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 4
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkExcluirDiferido)
        Me.GroupBox2.Controls.Add(Me.chkJudicial)
        Me.GroupBox2.Controls.Add(Me.chkBoletaContraBoleta)
        Me.GroupBox2.Controls.Add(Me.chkClienteChequeDiferidoFacturaPendiente)
        Me.GroupBox2.Controls.Add(Me.chkMostrarAnticipo)
        Me.GroupBox2.Controls.Add(Me.chkResumido)
        Me.GroupBox2.Controls.Add(Me.chkDescontados)
        Me.GroupBox2.Controls.Add(Me.chkInventarioDetallado)
        Me.GroupBox2.Controls.Add(Me.chkDireccionAlternativa)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxDocumento)
        Me.GroupBox2.Controls.Add(Me.cbxVencimiento)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.chkChequeDiferido)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(455, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(239, 493)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkJudicial
        '
        Me.chkJudicial.BackColor = System.Drawing.Color.Transparent
        Me.chkJudicial.Color = System.Drawing.Color.Empty
        Me.chkJudicial.Location = New System.Drawing.Point(5, 365)
        Me.chkJudicial.Margin = New System.Windows.Forms.Padding(4)
        Me.chkJudicial.Name = "chkJudicial"
        Me.chkJudicial.Size = New System.Drawing.Size(126, 21)
        Me.chkJudicial.SoloLectura = False
        Me.chkJudicial.TabIndex = 17
        Me.chkJudicial.Texto = "Clientes en Judicial"
        Me.chkJudicial.Valor = False
        '
        'chkBoletaContraBoleta
        '
        Me.chkBoletaContraBoleta.BackColor = System.Drawing.Color.Transparent
        Me.chkBoletaContraBoleta.Color = System.Drawing.Color.Empty
        Me.chkBoletaContraBoleta.Location = New System.Drawing.Point(5, 342)
        Me.chkBoletaContraBoleta.Margin = New System.Windows.Forms.Padding(4)
        Me.chkBoletaContraBoleta.Name = "chkBoletaContraBoleta"
        Me.chkBoletaContraBoleta.Size = New System.Drawing.Size(126, 21)
        Me.chkBoletaContraBoleta.SoloLectura = False
        Me.chkBoletaContraBoleta.TabIndex = 16
        Me.chkBoletaContraBoleta.Texto = "Boleta contra Boleta"
        Me.chkBoletaContraBoleta.Valor = False
        '
        'chkClienteChequeDiferidoFacturaPendiente
        '
        Me.chkClienteChequeDiferidoFacturaPendiente.BackColor = System.Drawing.Color.Transparent
        Me.chkClienteChequeDiferidoFacturaPendiente.Color = System.Drawing.Color.Empty
        Me.chkClienteChequeDiferidoFacturaPendiente.Location = New System.Drawing.Point(5, 434)
        Me.chkClienteChequeDiferidoFacturaPendiente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkClienteChequeDiferidoFacturaPendiente.Name = "chkClienteChequeDiferidoFacturaPendiente"
        Me.chkClienteChequeDiferidoFacturaPendiente.Size = New System.Drawing.Size(234, 21)
        Me.chkClienteChequeDiferidoFacturaPendiente.SoloLectura = False
        Me.chkClienteChequeDiferidoFacturaPendiente.TabIndex = 20
        Me.chkClienteChequeDiferidoFacturaPendiente.Texto = "Clientes CHQ DIF con Fact. Pendiente"
        Me.chkClienteChequeDiferidoFacturaPendiente.Valor = False
        '
        'chkMostrarAnticipo
        '
        Me.chkMostrarAnticipo.BackColor = System.Drawing.Color.Transparent
        Me.chkMostrarAnticipo.Color = System.Drawing.Color.Empty
        Me.chkMostrarAnticipo.Location = New System.Drawing.Point(5, 319)
        Me.chkMostrarAnticipo.Margin = New System.Windows.Forms.Padding(4)
        Me.chkMostrarAnticipo.Name = "chkMostrarAnticipo"
        Me.chkMostrarAnticipo.Size = New System.Drawing.Size(150, 21)
        Me.chkMostrarAnticipo.SoloLectura = False
        Me.chkMostrarAnticipo.TabIndex = 15
        Me.chkMostrarAnticipo.Texto = "Mostrar Reporte Anticipo"
        Me.chkMostrarAnticipo.Valor = False
        '
        'chkResumido
        '
        Me.chkResumido.BackColor = System.Drawing.Color.Transparent
        Me.chkResumido.Color = System.Drawing.Color.Empty
        Me.chkResumido.Location = New System.Drawing.Point(5, 296)
        Me.chkResumido.Margin = New System.Windows.Forms.Padding(4)
        Me.chkResumido.Name = "chkResumido"
        Me.chkResumido.Size = New System.Drawing.Size(135, 21)
        Me.chkResumido.SoloLectura = False
        Me.chkResumido.TabIndex = 14
        Me.chkResumido.Texto = "Resumido por Cliente"
        Me.chkResumido.Valor = False
        '
        'chkDescontados
        '
        Me.chkDescontados.BackColor = System.Drawing.Color.Transparent
        Me.chkDescontados.Color = System.Drawing.Color.Empty
        Me.chkDescontados.Location = New System.Drawing.Point(5, 411)
        Me.chkDescontados.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDescontados.Name = "chkDescontados"
        Me.chkDescontados.Size = New System.Drawing.Size(182, 21)
        Me.chkDescontados.SoloLectura = False
        Me.chkDescontados.TabIndex = 19
        Me.chkDescontados.Texto = "Ver Cheque Descontado"
        Me.chkDescontados.Valor = False
        '
        'chkInventarioDetallado
        '
        Me.chkInventarioDetallado.BackColor = System.Drawing.Color.Transparent
        Me.chkInventarioDetallado.Color = System.Drawing.Color.Empty
        Me.chkInventarioDetallado.Location = New System.Drawing.Point(5, 273)
        Me.chkInventarioDetallado.Margin = New System.Windows.Forms.Padding(4)
        Me.chkInventarioDetallado.Name = "chkInventarioDetallado"
        Me.chkInventarioDetallado.Size = New System.Drawing.Size(172, 21)
        Me.chkInventarioDetallado.SoloLectura = False
        Me.chkInventarioDetallado.TabIndex = 13
        Me.chkInventarioDetallado.Texto = "Mostrar Importe Original y GS"
        Me.chkInventarioDetallado.Valor = False
        '
        'chkDireccionAlternativa
        '
        Me.chkDireccionAlternativa.BackColor = System.Drawing.Color.Transparent
        Me.chkDireccionAlternativa.Color = System.Drawing.Color.Empty
        Me.chkDireccionAlternativa.Location = New System.Drawing.Point(5, 250)
        Me.chkDireccionAlternativa.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDireccionAlternativa.Name = "chkDireccionAlternativa"
        Me.chkDireccionAlternativa.Size = New System.Drawing.Size(172, 21)
        Me.chkDireccionAlternativa.SoloLectura = False
        Me.chkDireccionAlternativa.TabIndex = 12
        Me.chkDireccionAlternativa.Texto = "Con direcciones alternativas"
        Me.chkDireccionAlternativa.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 162)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Condición:"
        '
        'cbxDocumento
        '
        Me.cbxDocumento.CampoWhere = Nothing
        Me.cbxDocumento.CargarUnaSolaVez = False
        Me.cbxDocumento.DataDisplayMember = Nothing
        Me.cbxDocumento.DataFilter = Nothing
        Me.cbxDocumento.DataOrderBy = Nothing
        Me.cbxDocumento.DataSource = Nothing
        Me.cbxDocumento.DataValueMember = Nothing
        Me.cbxDocumento.dtSeleccionado = Nothing
        Me.cbxDocumento.FormABM = Nothing
        Me.cbxDocumento.Indicaciones = Nothing
        Me.cbxDocumento.Location = New System.Drawing.Point(4, 178)
        Me.cbxDocumento.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDocumento.Name = "cbxDocumento"
        Me.cbxDocumento.SeleccionMultiple = False
        Me.cbxDocumento.SeleccionObligatoria = False
        Me.cbxDocumento.Size = New System.Drawing.Size(231, 21)
        Me.cbxDocumento.SoloLectura = False
        Me.cbxDocumento.TabIndex = 9
        Me.cbxDocumento.Texto = ""
        '
        'cbxVencimiento
        '
        Me.cbxVencimiento.FormattingEnabled = True
        Me.cbxVencimiento.Location = New System.Drawing.Point(5, 47)
        Me.cbxVencimiento.Name = "cbxVencimiento"
        Me.cbxVencimiento.Size = New System.Drawing.Size(230, 21)
        Me.cbxVencimiento.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Vencimiento:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(162, 139)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 7
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(5, 139)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 6
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(5, 122)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 5
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(5, 223)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 11
        '
        'chkChequeDiferido
        '
        Me.chkChequeDiferido.BackColor = System.Drawing.Color.Transparent
        Me.chkChequeDiferido.Color = System.Drawing.Color.Empty
        Me.chkChequeDiferido.Location = New System.Drawing.Point(5, 457)
        Me.chkChequeDiferido.Margin = New System.Windows.Forms.Padding(4)
        Me.chkChequeDiferido.Name = "chkChequeDiferido"
        Me.chkChequeDiferido.Size = New System.Drawing.Size(114, 21)
        Me.chkChequeDiferido.SoloLectura = False
        Me.chkChequeDiferido.TabIndex = 21
        Me.chkChequeDiferido.Texto = "Cheque Diferido"
        Me.chkChequeDiferido.Valor = False
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 74)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 2
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 206)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 10
        Me.lblRanking.Text = "Ranking:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 26, 8, 35, 7, 504)
        Me.txtHasta.Location = New System.Drawing.Point(88, 89)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 4
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 26, 8, 35, 7, 504)
        Me.txtDesde.Location = New System.Drawing.Point(5, 89)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 3
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(440, 502)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 3
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 526)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(706, 22)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'frmInventariodeDocumentosPendientesCobrar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 548)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmInventariodeDocumentosPendientesCobrar"
        Me.Text = "frmInventariodeDocumentosPendientes"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkZonaVenta As ERP.ocxCHK
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkEstadoCliente As ERP.ocxCHK
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxVencimiento As System.Windows.Forms.ComboBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxEstadoCliente As ERP.ocxCBX
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents cbxDocumento As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkTipoCliente As ERP.ocxCHK
    Friend WithEvents cbxTipoCliente As ERP.ocxCBX
    Friend WithEvents chkDireccionAlternativa As ERP.ocxCHK
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents cbxCobrador As ERP.ocxCBX
    Friend WithEvents chkCobrador As ERP.ocxCHK
    Friend WithEvents chkChofer As ERP.ocxCHK
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents chkChequeDiferido As ERP.ocxCHK
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents chkInventarioDetallado As ERP.ocxCHK
    Friend WithEvents chkCC As ERP.ocxCHK
    Friend WithEvents txtCuentaContableDesde As ERP.ocxTXTCuentaContable
    Friend WithEvents chkCCHasta As ERP.ocxCHK
    Friend WithEvents txtCuentaContableHasta As ERP.ocxTXTCuentaContable
    Friend WithEvents chkDescontados As ERP.ocxCHK
    Friend WithEvents cbxSucursalCliente As ERP.ocxCBX
    Friend WithEvents chkSucursalCliente As ERP.ocxCHK
    Friend WithEvents chkResumido As ERP.ocxCHK
    Friend WithEvents chkMostrarAnticipo As ERP.ocxCHK
    Friend WithEvents chkClienteChequeDiferidoFacturaPendiente As ocxCHK
    Friend WithEvents chkBoletaContraBoleta As ocxCHK
    Friend WithEvents chkJudicial As ocxCHK
    Friend WithEvents chkExcluirDiferido As ocxCHK
    Friend WithEvents cbxClienteSucursal As ocxCBX
    Friend WithEvents chkClienteSucursal As ocxCHK
End Class

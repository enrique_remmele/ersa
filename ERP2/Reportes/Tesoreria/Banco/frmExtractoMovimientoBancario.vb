﻿Imports ERP.Reporte
Public Class frmExtractoMovimientoBancario


    'CLASES
    Dim CReporte As New CReporteBanco
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'Variables
    Dim vNuevo As Boolean
    Dim ID As Integer
    Dim vControles() As Control
    Dim dtCuentaBancaria As New DataTable
    Dim dtConciliacion As New DataTable

    'FUNCIONES

    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones

        'Fechas
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Titulo As String = "EXTRACTO DE MOVIMIENTOS BANCARIOS"
        Dim TipoInforme As String = ""
        Dim ASC As Boolean = False

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = "'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "', " & cbxCuentaBancaria.GetValue & ", '" & chkChequeDiferido.Checked & "'"

        WhereDetalle = "IDCuentaBancaria=" & cbxCuentaBancaria.GetValue

        TipoInforme = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString

        CReporte.ExtractoMovimientoBancario(frm, Where, OrderBy, Top, vgUsuarioIdentificador, Titulo, TipoInforme, WhereDetalle)

    End Sub

    Private Sub frmExtractoMovimientoBancario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmConciliaciondeMovimientoBancaria_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

  
End Class


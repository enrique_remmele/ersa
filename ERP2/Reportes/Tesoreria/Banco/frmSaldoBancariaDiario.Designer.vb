﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSaldoBancariaDiario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkCuentaBancaria = New ERP.ocxCHK()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.cbxCuentaBancaria = New ERP.ocxCBX()
        Me.txtFecha = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxTipoReporte = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkCuentaBancaria)
        Me.gbxFiltro.Controls.Add(Me.lblPeriodo)
        Me.gbxFiltro.Controls.Add(Me.cbxCuentaBancaria)
        Me.gbxFiltro.Controls.Add(Me.txtFecha)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(356, 72)
        Me.gbxFiltro.TabIndex = 3
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkCuentaBancaria
        '
        Me.chkCuentaBancaria.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaBancaria.Color = System.Drawing.Color.Empty
        Me.chkCuentaBancaria.Location = New System.Drawing.Point(9, 16)
        Me.chkCuentaBancaria.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCuentaBancaria.Name = "chkCuentaBancaria"
        Me.chkCuentaBancaria.Size = New System.Drawing.Size(107, 21)
        Me.chkCuentaBancaria.SoloLectura = False
        Me.chkCuentaBancaria.TabIndex = 6
        Me.chkCuentaBancaria.Texto = "Cuenta Bancaria:"
        Me.chkCuentaBancaria.Valor = True
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(9, 50)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(40, 13)
        Me.lblPeriodo.TabIndex = 4
        Me.lblPeriodo.Text = "Fecha:"
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.CampoWhere = "IDCuentaBancaria"
        Me.cbxCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxCuentaBancaria.DataDisplayMember = "Descripcion"
        Me.cbxCuentaBancaria.DataFilter = Nothing
        Me.cbxCuentaBancaria.DataOrderBy = Nothing
        Me.cbxCuentaBancaria.DataSource = "VCuentaBancaria"
        Me.cbxCuentaBancaria.DataValueMember = "ID"
        Me.cbxCuentaBancaria.FormABM = Nothing
        Me.cbxCuentaBancaria.Indicaciones = Nothing
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(121, 16)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.SeleccionObligatoria = False
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(229, 21)
        Me.cbxCuentaBancaria.SoloLectura = False
        Me.cbxCuentaBancaria.TabIndex = 3
        Me.cbxCuentaBancaria.Texto = ""
        '
        'txtFecha
        '
        Me.txtFecha.Color = System.Drawing.Color.Empty
        Me.txtFecha.Fecha = New Date(2013, 2, 25, 9, 26, 49, 512)
        Me.txtFecha.Location = New System.Drawing.Point(121, 43)
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.PermitirNulo = False
        Me.txtFecha.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha.SoloLectura = False
        Me.txtFecha.TabIndex = 5
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(167, 166)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 4
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(305, 166)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 5
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 99)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Tipo de Reporte:"
        '
        'cbxTipoReporte
        '
        Me.cbxTipoReporte.CampoWhere = Nothing
        Me.cbxTipoReporte.CargarUnaSolaVez = False
        Me.cbxTipoReporte.DataDisplayMember = Nothing
        Me.cbxTipoReporte.DataFilter = Nothing
        Me.cbxTipoReporte.DataOrderBy = Nothing
        Me.cbxTipoReporte.DataSource = Nothing
        Me.cbxTipoReporte.DataValueMember = Nothing
        Me.cbxTipoReporte.FormABM = Nothing
        Me.cbxTipoReporte.Indicaciones = Nothing
        Me.cbxTipoReporte.Location = New System.Drawing.Point(133, 91)
        Me.cbxTipoReporte.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoReporte.Name = "cbxTipoReporte"
        Me.cbxTipoReporte.SeleccionObligatoria = False
        Me.cbxTipoReporte.Size = New System.Drawing.Size(208, 21)
        Me.cbxTipoReporte.SoloLectura = False
        Me.cbxTipoReporte.TabIndex = 6
        Me.cbxTipoReporte.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 128)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Tipo:"
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(133, 120)
        Me.cbxTipo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(208, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 8
        Me.cbxTipo.Texto = ""
        '
        'frmSaldoBancariaDiario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(380, 201)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbxTipo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxTipoReporte)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmSaldoBancariaDiario"
        Me.Text = "frmSaldoBancariaDiario"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents cbxCuentaBancaria As ERP.ocxCBX
    Friend WithEvents txtFecha As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkCuentaBancaria As ERP.ocxCHK
    Friend WithEvents cbxTipoReporte As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxTipo As ERP.ocxCBX
End Class

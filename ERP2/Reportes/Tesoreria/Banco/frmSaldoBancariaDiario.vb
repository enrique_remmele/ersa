﻿Imports ERP.Reporte
Public Class frmSaldoBancariaDiario



    'CLASES
    Dim CReporte As New CReporteBanco
    Dim CSistema As New CSistema
    Dim CArchivoInicio As New CArchivoInicio
    Dim CData As New CData

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'Variables
    Dim vNuevo As Boolean
    Dim ID As Integer
    Dim vControles() As Control
    Dim dtCuentaBancaria As New DataTable
    Dim dtConciliacion As New DataTable

    'FUNCIONES

    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones

        'Fechas
        txtFecha.Ayer()
        cbxTipoReporte.cbx.Items.Add("RESUMIDO")
        cbxTipoReporte.cbx.Items.Add("DETALLADO")
        cbxTipoReporte.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoReporte.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoReporte.cbx.SelectedIndex = 0

        cbxTipo.cbx.Items.Add("DEPOSITOS A CONFIRMAR")
        cbxTipo.cbx.Items.Add("CHEQUES NO PAGADOS")
        cbxTipo.cbx.Items.Add("NO CONCILIADO")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipo.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Titulo As String = "CUENTA BANCARIA SALDO DIARIO"
        Dim TipoInforme As String = ""
        Dim ASC As Boolean = False
        Dim Todos As Boolean = Not chkCuentaBancaria.Valor

        frm.MdiParent = My.Application.ApplicationContext.MainForm


        WhereDetalle = ""

        TipoInforme = "Del " & txtFecha.txt.Text & ""
        If chkCuentaBancaria.Valor = True Then
            TipoInforme = TipoInforme & " Cuenta Bancaria = " & cbxCuentaBancaria.Texto
        End If
        If cbxTipoReporte.cbx.SelectedIndex = 0 Then 'RESUMIDO
            Where = "@fecha='" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "'"
            Where = Where & ",@IDCuentaBancaria=" & cbxCuentaBancaria.GetValue
            Where = Where & ",@Todos='" & Todos & "'"
            CReporte.CuentaBancariaSaldoDiario(frm, Where, OrderBy, Top, vgUsuarioIdentificador, Titulo, TipoInforme, WhereDetalle)
        ElseIf cbxTipoReporte.cbx.SelectedIndex = 1 Then 'DETALLADO
            Where = " Where Fecha <= '" & CSistema.FormatoFechaBaseDatos(txtFecha.txt.Text, True, False) & "'"
            If chkCuentaBancaria.Valor = True Then
                Where = Where & " and IDCuentaBancaria = " & cbxCuentaBancaria.GetValue
            End If
            'Tipos de saldo a ver en reporte
            If cbxTipo.cbx.SelectedIndex = 0 Then
                Where = Where & " and (DepositoAconfirmar <> 0 or Conciliado = 'True')  "
            ElseIf cbxTipo.cbx.SelectedIndex = 1 Then
                Where = Where & " and (ChequesNoPagados <> 0 or Conciliado = 'True') "
            ElseIf cbxTipo.cbx.SelectedIndex = 2 Then
                Where = Where
            End If
            CReporte.CuentaBancariaSaldoDiarioDetallado(frm, Where, OrderBy, Top, vgUsuarioIdentificador, Titulo, TipoInforme, WhereDetalle)
        End If


    End Sub

    Private Sub frmExtractoMovimientoBancario_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmConciliaciondeMovimientoBancaria_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub


End Class
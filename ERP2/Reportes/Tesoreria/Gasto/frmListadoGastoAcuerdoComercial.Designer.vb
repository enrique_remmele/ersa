﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoGastoAcuerdoComercial
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkFechaDocumento = New ERP.ocxCHK()
        Me.chkFechaOperacion = New ERP.ocxCHK()
        Me.txtHastaDocu = New ERP.ocxTXTDate()
        Me.txtDesdeDocu = New ERP.ocxTXTDate()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.txtPalabraClave1 = New System.Windows.Forms.TextBox()
        Me.txtPalabraClave2 = New System.Windows.Forms.TextBox()
        Me.txtPalabraClave3 = New System.Windows.Forms.TextBox()
        Me.txtPalabraClave4 = New System.Windows.Forms.TextBox()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkPalabraClave = New ERP.ocxCHK()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 123)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(650, 187)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaDocumento)
        Me.GroupBox2.Controls.Add(Me.chkFechaOperacion)
        Me.GroupBox2.Controls.Add(Me.txtHastaDocu)
        Me.GroupBox2.Controls.Add(Me.txtDesdeDocu)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(519, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(194, 175)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkFechaDocumento
        '
        Me.chkFechaDocumento.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaDocumento.Color = System.Drawing.Color.Empty
        Me.chkFechaDocumento.Location = New System.Drawing.Point(5, 65)
        Me.chkFechaDocumento.Name = "chkFechaDocumento"
        Me.chkFechaDocumento.Size = New System.Drawing.Size(99, 24)
        Me.chkFechaDocumento.SoloLectura = False
        Me.chkFechaDocumento.TabIndex = 17
        Me.chkFechaDocumento.Texto = "Fecha Acuerdo:"
        Me.chkFechaDocumento.Valor = False
        '
        'chkFechaOperacion
        '
        Me.chkFechaOperacion.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaOperacion.Color = System.Drawing.Color.Empty
        Me.chkFechaOperacion.Location = New System.Drawing.Point(6, 13)
        Me.chkFechaOperacion.Name = "chkFechaOperacion"
        Me.chkFechaOperacion.Size = New System.Drawing.Size(119, 24)
        Me.chkFechaOperacion.SoloLectura = False
        Me.chkFechaOperacion.TabIndex = 16
        Me.chkFechaOperacion.Texto = "Fecha Gasto:"
        Me.chkFechaOperacion.Valor = False
        '
        'txtHastaDocu
        '
        Me.txtHastaDocu.AñoFecha = 0
        Me.txtHastaDocu.Color = System.Drawing.Color.Empty
        Me.txtHastaDocu.Enabled = False
        Me.txtHastaDocu.Fecha = New Date(2013, 5, 31, 16, 35, 16, 575)
        Me.txtHastaDocu.Location = New System.Drawing.Point(90, 91)
        Me.txtHastaDocu.MesFecha = 0
        Me.txtHastaDocu.Name = "txtHastaDocu"
        Me.txtHastaDocu.PermitirNulo = False
        Me.txtHastaDocu.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaDocu.SoloLectura = False
        Me.txtHastaDocu.TabIndex = 15
        '
        'txtDesdeDocu
        '
        Me.txtDesdeDocu.AñoFecha = 0
        Me.txtDesdeDocu.Color = System.Drawing.Color.Empty
        Me.txtDesdeDocu.Enabled = False
        Me.txtDesdeDocu.Fecha = New Date(2013, 5, 31, 16, 35, 16, 575)
        Me.txtDesdeDocu.Location = New System.Drawing.Point(7, 91)
        Me.txtDesdeDocu.MesFecha = 0
        Me.txtDesdeDocu.Name = "txtDesdeDocu"
        Me.txtDesdeDocu.PermitirNulo = False
        Me.txtDesdeDocu.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeDocu.SoloLectura = False
        Me.txtDesdeDocu.TabIndex = 14
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(121, 140)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(61, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 140)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(110, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 35, 16, 575)
        Me.txtHasta.Location = New System.Drawing.Point(89, 39)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Enabled = False
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 35, 16, 575)
        Me.txtDesde.Location = New System.Drawing.Point(6, 39)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(519, 187)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 6
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'txtPalabraClave1
        '
        Me.txtPalabraClave1.Enabled = False
        Me.txtPalabraClave1.Location = New System.Drawing.Point(118, 91)
        Me.txtPalabraClave1.Name = "txtPalabraClave1"
        Me.txtPalabraClave1.Size = New System.Drawing.Size(74, 20)
        Me.txtPalabraClave1.TabIndex = 25
        '
        'txtPalabraClave2
        '
        Me.txtPalabraClave2.Enabled = False
        Me.txtPalabraClave2.Location = New System.Drawing.Point(208, 91)
        Me.txtPalabraClave2.Name = "txtPalabraClave2"
        Me.txtPalabraClave2.Size = New System.Drawing.Size(74, 20)
        Me.txtPalabraClave2.TabIndex = 26
        '
        'txtPalabraClave3
        '
        Me.txtPalabraClave3.Enabled = False
        Me.txtPalabraClave3.Location = New System.Drawing.Point(298, 91)
        Me.txtPalabraClave3.Name = "txtPalabraClave3"
        Me.txtPalabraClave3.Size = New System.Drawing.Size(74, 20)
        Me.txtPalabraClave3.TabIndex = 27
        '
        'txtPalabraClave4
        '
        Me.txtPalabraClave4.Enabled = False
        Me.txtPalabraClave4.Location = New System.Drawing.Point(388, 91)
        Me.txtPalabraClave4.Name = "txtPalabraClave4"
        Me.txtPalabraClave4.Size = New System.Drawing.Size(74, 20)
        Me.txtPalabraClave4.TabIndex = 28
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtPalabraClave4)
        Me.gbxFiltro.Controls.Add(Me.txtPalabraClave3)
        Me.gbxFiltro.Controls.Add(Me.txtPalabraClave2)
        Me.gbxFiltro.Controls.Add(Me.txtPalabraClave1)
        Me.gbxFiltro.Controls.Add(Me.chkPalabraClave)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Location = New System.Drawing.Point(19, 6)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(494, 175)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkPalabraClave
        '
        Me.chkPalabraClave.BackColor = System.Drawing.Color.Transparent
        Me.chkPalabraClave.Color = System.Drawing.Color.Empty
        Me.chkPalabraClave.Location = New System.Drawing.Point(8, 91)
        Me.chkPalabraClave.Margin = New System.Windows.Forms.Padding(4)
        Me.chkPalabraClave.Name = "chkPalabraClave"
        Me.chkPalabraClave.Size = New System.Drawing.Size(97, 21)
        Me.chkPalabraClave.SoloLectura = False
        Me.chkPalabraClave.TabIndex = 24
        Me.chkPalabraClave.Texto = "Palabra Clave:"
        Me.chkPalabraClave.Valor = False
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(8, 53)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(89, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 4
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = "RazonSocial"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(118, 53)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(345, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 5
        Me.cbxProveedor.Texto = ""
        '
        'frmListadoGastoAcuerdoComercial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(732, 222)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoGastoAcuerdoComercial"
        Me.Text = "frmListadoGastoAcuerdoComercial"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbxEnForma As ocxCBX
    Friend WithEvents cbxOrdenadoPor As ocxCBX
    Friend WithEvents lblOrdenado As Label
    Friend WithEvents txtHastaDocu As ocxTXTDate
    Friend WithEvents txtDesdeDocu As ocxTXTDate
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents btnCerrar As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents chkFechaDocumento As ocxCHK
    Friend WithEvents chkFechaOperacion As ocxCHK
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents btnInforme As Button
    Friend WithEvents cbxProveedor As ocxCBX
    Friend WithEvents chkProveedor As ocxCHK
    Friend WithEvents chkPalabraClave As ocxCHK
    Friend WithEvents txtPalabraClave1 As TextBox
    Friend WithEvents txtPalabraClave2 As TextBox
    Friend WithEvents txtPalabraClave3 As TextBox
    Friend WithEvents txtPalabraClave4 As TextBox
    Friend WithEvents gbxFiltro As GroupBox
End Class

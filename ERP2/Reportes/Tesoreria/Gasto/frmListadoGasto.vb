﻿Imports ERP.Reporte

Public Class frmListadoGasto

    'CLASES
    Dim CReporte As New CReporteGasto
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE GASTOS"
    Dim TipoInforme As String
    Dim Seccion As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()
        CambiarOrdenacion()

        txtDesde.Hoy()
        txtDesdeDocu.Hoy()
        txtHasta.Hoy()
        txtHastaDocu.Hoy()

        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Orden en formato
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Grupo
        '  CSistema.SqlToComboBox(cbxGrupo.cbx, CData.GetTable("vGrupoUsuario", "IDUsuario=" & vgIDUsuario), "IDGrupo", "Grupo")

        'Tipo de Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("VTipoComprobante", "IDOperacion=" & CSistema.ObtenerIDOperacion(frmGastos.Name, "GASTO", "GAS")), "ID", "Codigo")

        'FA 18/04/2023 Prueba
        'Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' ")
        'CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
        'cbxDepartamento.cbx.Text = ""
        cbxDepartamento.Enabled = False

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'TIPO IVA
        cbxTipoIVA.cbx.Items.Add("DIRECTO")
        cbxTipoIVA.cbx.Items.Add("INDISTINTO")
        cbxTipoIVA.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoIVA.cbx.SelectedIndex = 0

        'Configuraciones
        'Deposito
        chkDeposito.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString)
        cbxDeposito.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDeposito").ToString)

        'Distribucion
        chkCamion.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString)
        cbxCamion.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString)
        chkChofer.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString)
        cbxChofer.Visible = CSistema.RetornarValorBoolean(vgConfiguraciones("GastoHabilitarDistribucion").ToString)

    End Sub

    Sub Listar()

        Dim Where As String = " where 1 = 1 "
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim Tabla As String = "VGasto"

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkFechaDocumento.Valor = True Then
            Where = Where & " and Fecha Between '" & txtDesdeDocu.GetValueString & "' And '" & txtHastaDocu.GetValueString & "' "
        End If

        If chkFechaOperacion.Valor = True Then
            Where = Where & " and convert(date,FechaTransaccion) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        End If

        'Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        'Where = " Where convert(date,FechaTransaccion) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Condicion
            If ctr.Name = "cbxCondicion" Then
                GoTo siguiente
            End If

            'UnidadNegocio
            If ctr.Name = "cbxUnidadNegocio" Then
                GoTo siguiente
            End If

            'Tipo IVA
            If ctr.Name = "cbxTipoIVA" Then
                GoTo siguiente
            End If

            'Departamento
            If ctr.Name = "cbxDepartamento" Then
                GoTo siguiente
            End If

            'Seccion
            If ctr.Name = "cbxSeccion" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

            'If chkInformeCC.Valor = True Then
            '    If txtNumeroOP.txt.Text <> "" then
            '        Where = Where & " And NumeroOrdenPago = " & txtNumeroOP.GetValue & " "
            '    End If
            'End If

siguiente:

        Next

        If chkUnidadNegocio.chk.Checked Then
            Where = Where & " And IDTransaccion in (Select Distinct IDTransaccion from vDetalleAsiento where IDUnidadNegocioDetalle = " & cbxUnidadNegocio.cbx.SelectedValue & ") "
        End If

        'SM - 01072021 - Filtro de Departamento Empresa
        If chkDepartamento.Valor = True Then
            'Where = Where & " And IDDepartamentoEmpresa= " & cbxDepartamento.cbx.SelectedValue
            Where = Where & " And DepartamentoEmpresa like'%" & cbxDepartamento.Text & "%'"
        End If

        'SM - 01072021 - Filtro de Departamento Empresa
        If chkSeccion.Valor = True Then
            Where = Where & " And IDSeccion= " & cbxSeccion.cbx.SelectedValue
        End If

        'Condicion
        If cbxCondicion.Enabled = True Then
            If cbxCondicion.cbx.SelectedIndex = 0 Then
                Where = Where & " And Credito='False' "
            End If

            If cbxCondicion.cbx.SelectedIndex = 1 Then
                Where = Where & " And Credito='True' "
            End If

        End If

        'Tipo de IVA
        If cbxTipoIVA.Enabled = True Then

            If cbxTipoIVA.cbx.SelectedIndex = 0 Then
                Where = Where & " And Directo='True' "
            End If

            If cbxTipoIVA.cbx.SelectedIndex = 1 Then
                Where = Where & " And Directo='False' "
            End If

        End If

        'RRHH
        If chkIncluirRRHH.Valor = False Then
            Where = Where & " And RRHH='False' "
        Else
            If chkSoloRRHH.Valor = True Then
                Where = Where & " And RRHH='True' "
            End If
        End If

        'Tipo
        If fpnTipoGasto.Enabled = True Then

            'FondoFijo
            If rdbFondoFijo.Checked = True Then
                Tabla = " VGastoFondoFijo"
            End If

            'Gasto
            If rdbGasto.Checked = True Then
                Tabla = " VGastoTipoComprobante "
            End If

        End If

        If chkPalabraClave.Valor Then
            Where = Where & "And (Observacion like '%" & txtPalabraClave1.Text & "%' And Observacion like '%" & txtPalabraClave2.Text & "%' And Observacion like '%" & txtPalabraClave3.Text & "%' And Observacion like '%" & txtPalabraClave4.Text & "%') "
        End If

        'Libro IVA
        If fpnLibroIVA.Enabled = True Then
            Where = Where & " And IncluirLibro='" & rdbIncluido.Checked & "' "
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        TipoInforme = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString

        If chkSucursal.Valor = True Then
            TipoInforme = TipoInforme & " - Suc:" & cbxSucursal.cbx.Text
        End If

        If chkDepartamento.Valor = True Then
            TipoInforme = TipoInforme & " - Departamento:" & cbxDepartamento.Text
        End If

        If chkSeccion.Valor = True Then
            TipoInforme = TipoInforme & " - Seccion:" & cbxSeccion.cbx.Text
        End If

        If chkInformeCC.Valor = True Then
            If txtNumeroOP.txt.Text <> "" Then
                Where = Where & " and Idtransaccion in (select idtransaccion from vOrdenPagoEgreso where numero = " & txtNumeroOP.GetValue & ")"
            End If
        End If

        Dim Desde As Date
        Dim Hasta As Date

        If chkFechaOperacion.chk.Checked Then
            Desde = txtDesde.txt.Text
            Hasta = txtHasta.txt.Text
        End If

        If chkFechaDocumento.chk.Checked Then
            Desde = txtDesdeDocu.txt.Text
            Hasta = txtHastaDocu.txt.Text
        End If

        CReporte.ListadoGasto(frm, Titulo, Where, OrderBy, Top, TipoInforme, Tabla, chkInformeCC.Valor, Desde, Hasta)


    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VGasto ", "Select Top(0) Fecha, Proveedor, NroComprobante, TipoComprobante, Total, Numero  From VGasto")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoGasto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
    End Sub

    Private Sub chkGrupo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkGrupo.PropertyChanged
        cbxGrupo.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkCondicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCondicion.PropertyChanged
        cbxCondicion.Enabled = value
    End Sub

    Private Sub chkImpuesto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkImpuesto.PropertyChanged
        cbxTipoIVA.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkCamion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCamion.PropertyChanged
        cbxCamion.Enabled = value
    End Sub

    Private Sub chkChofer_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub

    Private Sub chkLibroIVA_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkLibroIVA.PropertyChanged
        fpnLibroIVA.Enabled = value
    End Sub

    Private Sub chkTipo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipo.PropertyChanged
        fpnTipoGasto.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCuenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        cbxProveedor.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged_1(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkFechaOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaOperacion.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkFechaDocumento_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaDocumento.PropertyChanged
        txtDesdeDocu.Enabled = value
        txtHastaDocu.Enabled = value
    End Sub

  
    Private Sub chkIncluirRRHH_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkIncluirRRHH.PropertyChanged
        chkSoloRRHH.Enabled = value
    End Sub

    Private Sub chkUnidadNegocio_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkUnidadNegocio.PropertyChanged
        cbxUnidadNegocio.Enabled = value
    End Sub

    Private Sub chkPalabraClave_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkPalabraClave.PropertyChanged
        txtPalabraClave1.Enabled = value
        txtPalabraClave2.Enabled = value
        txtPalabraClave3.Enabled = value
        txtPalabraClave4.Enabled = value
    End Sub

    Private Sub chkDepartamento_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkDepartamento.PropertyChanged
        cbxDepartamento.Enabled = value
    End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs)
        'FA 18/04/2023 Prueba
        'Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamento.cbx.SelectedValue)
        'If Seccion = True Then
        '    Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamento.cbx.SelectedValue)
        '    CSistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
        '    cbxSeccion.cbx.Text = ""
        '    chkSeccion.Enabled = True
        'Else
        '    cbxSeccion.Enabled = False
        '    chkSeccion.Enabled = False
        'End If
    End Sub

    Private Sub chkSeccion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSeccion.PropertyChanged
        cbxSeccion.Enabled = value
    End Sub
End Class




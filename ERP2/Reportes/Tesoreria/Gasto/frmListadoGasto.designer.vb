﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoGasto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.cbxSeccion = New ERP.ocxCBX()
        Me.chkSeccion = New ERP.ocxCHK()
        Me.chkDepartamento = New ERP.ocxCHK()
        Me.txtPalabraClave4 = New System.Windows.Forms.TextBox()
        Me.txtPalabraClave3 = New System.Windows.Forms.TextBox()
        Me.txtPalabraClave2 = New System.Windows.Forms.TextBox()
        Me.txtPalabraClave1 = New System.Windows.Forms.TextBox()
        Me.chkPalabraClave = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.chkUnidadNegocio = New ERP.ocxCHK()
        Me.chkUsuario = New ERP.ocxCHK()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.chkChofer = New ERP.ocxCHK()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.chkCamion = New ERP.ocxCHK()
        Me.cbxCamion = New ERP.ocxCBX()
        Me.chkImpuesto = New ERP.ocxCHK()
        Me.cbxTipoIVA = New ERP.ocxCBX()
        Me.chkCondicion = New ERP.ocxCHK()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.chkGrupo = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkSoloRRHH = New ERP.ocxCHK()
        Me.chkIncluirRRHH = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkFechaDocumento = New ERP.ocxCHK()
        Me.txtNumeroOP = New ERP.ocxTXTString()
        Me.chkFechaOperacion = New ERP.ocxCHK()
        Me.txtHastaDocu = New ERP.ocxTXTDate()
        Me.txtDesdeDocu = New ERP.ocxTXTDate()
        Me.chkInformeCC = New ERP.ocxCHK()
        Me.fpnLibroIVA = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbIncluido = New System.Windows.Forms.RadioButton()
        Me.rdbNoIncluido = New System.Windows.Forms.RadioButton()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.fpnTipoGasto = New System.Windows.Forms.FlowLayoutPanel()
        Me.rdbGasto = New System.Windows.Forms.RadioButton()
        Me.rdbFondoFijo = New System.Windows.Forms.RadioButton()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.chkTipo = New ERP.ocxCHK()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkLibroIVA = New ERP.ocxCHK()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.cbxDepartamento = New System.Windows.Forms.ComboBox()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.fpnLibroIVA.SuspendLayout()
        Me.fpnTipoGasto.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.cbxDepartamento)
        Me.gbxFiltro.Controls.Add(Me.cbxSeccion)
        Me.gbxFiltro.Controls.Add(Me.chkSeccion)
        Me.gbxFiltro.Controls.Add(Me.chkDepartamento)
        Me.gbxFiltro.Controls.Add(Me.txtPalabraClave4)
        Me.gbxFiltro.Controls.Add(Me.txtPalabraClave3)
        Me.gbxFiltro.Controls.Add(Me.txtPalabraClave2)
        Me.gbxFiltro.Controls.Add(Me.txtPalabraClave1)
        Me.gbxFiltro.Controls.Add(Me.chkPalabraClave)
        Me.gbxFiltro.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.chkUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.chkUsuario)
        Me.gbxFiltro.Controls.Add(Me.cbxUsuario)
        Me.gbxFiltro.Controls.Add(Me.chkChofer)
        Me.gbxFiltro.Controls.Add(Me.cbxChofer)
        Me.gbxFiltro.Controls.Add(Me.chkCamion)
        Me.gbxFiltro.Controls.Add(Me.cbxCamion)
        Me.gbxFiltro.Controls.Add(Me.chkImpuesto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoIVA)
        Me.gbxFiltro.Controls.Add(Me.chkCondicion)
        Me.gbxFiltro.Controls.Add(Me.cbxCondicion)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkGrupo)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxGrupo)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(494, 414)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = "ID"
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = ""
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = Nothing
        Me.cbxSeccion.DataSource = ""
        Me.cbxSeccion.DataValueMember = ""
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.Enabled = False
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(118, 355)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(345, 21)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 32
        Me.cbxSeccion.Texto = ""
        '
        'chkSeccion
        '
        Me.chkSeccion.BackColor = System.Drawing.Color.Transparent
        Me.chkSeccion.Color = System.Drawing.Color.Empty
        Me.chkSeccion.Location = New System.Drawing.Point(7, 356)
        Me.chkSeccion.Margin = New System.Windows.Forms.Padding(4)
        Me.chkSeccion.Name = "chkSeccion"
        Me.chkSeccion.Size = New System.Drawing.Size(97, 21)
        Me.chkSeccion.SoloLectura = False
        Me.chkSeccion.TabIndex = 31
        Me.chkSeccion.Texto = "Seccion:"
        Me.chkSeccion.Valor = False
        '
        'chkDepartamento
        '
        Me.chkDepartamento.BackColor = System.Drawing.Color.Transparent
        Me.chkDepartamento.Color = System.Drawing.Color.Empty
        Me.chkDepartamento.Location = New System.Drawing.Point(7, 327)
        Me.chkDepartamento.Margin = New System.Windows.Forms.Padding(4)
        Me.chkDepartamento.Name = "chkDepartamento"
        Me.chkDepartamento.Size = New System.Drawing.Size(97, 21)
        Me.chkDepartamento.SoloLectura = False
        Me.chkDepartamento.TabIndex = 29
        Me.chkDepartamento.Texto = "Departamento:"
        Me.chkDepartamento.Valor = False
        '
        'txtPalabraClave4
        '
        Me.txtPalabraClave4.Enabled = False
        Me.txtPalabraClave4.Location = New System.Drawing.Point(387, 386)
        Me.txtPalabraClave4.Name = "txtPalabraClave4"
        Me.txtPalabraClave4.Size = New System.Drawing.Size(74, 20)
        Me.txtPalabraClave4.TabIndex = 28
        '
        'txtPalabraClave3
        '
        Me.txtPalabraClave3.Enabled = False
        Me.txtPalabraClave3.Location = New System.Drawing.Point(297, 386)
        Me.txtPalabraClave3.Name = "txtPalabraClave3"
        Me.txtPalabraClave3.Size = New System.Drawing.Size(74, 20)
        Me.txtPalabraClave3.TabIndex = 27
        '
        'txtPalabraClave2
        '
        Me.txtPalabraClave2.Enabled = False
        Me.txtPalabraClave2.Location = New System.Drawing.Point(207, 386)
        Me.txtPalabraClave2.Name = "txtPalabraClave2"
        Me.txtPalabraClave2.Size = New System.Drawing.Size(74, 20)
        Me.txtPalabraClave2.TabIndex = 26
        '
        'txtPalabraClave1
        '
        Me.txtPalabraClave1.Enabled = False
        Me.txtPalabraClave1.Location = New System.Drawing.Point(117, 386)
        Me.txtPalabraClave1.Name = "txtPalabraClave1"
        Me.txtPalabraClave1.Size = New System.Drawing.Size(74, 20)
        Me.txtPalabraClave1.TabIndex = 25
        '
        'chkPalabraClave
        '
        Me.chkPalabraClave.BackColor = System.Drawing.Color.Transparent
        Me.chkPalabraClave.Color = System.Drawing.Color.Empty
        Me.chkPalabraClave.Location = New System.Drawing.Point(7, 386)
        Me.chkPalabraClave.Margin = New System.Windows.Forms.Padding(4)
        Me.chkPalabraClave.Name = "chkPalabraClave"
        Me.chkPalabraClave.Size = New System.Drawing.Size(97, 21)
        Me.chkPalabraClave.SoloLectura = False
        Me.chkPalabraClave.TabIndex = 24
        Me.chkPalabraClave.Texto = "Palabra Clave:"
        Me.chkPalabraClave.Valor = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = "TipoSaldo"
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = "Descripcion"
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = "UnidadNegocio"
        Me.cbxUnidadNegocio.DataValueMember = "ID"
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.Enabled = False
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(118, 301)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(345, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 23
        Me.cbxUnidadNegocio.Texto = ""
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(8, 301)
        Me.chkUnidadNegocio.Margin = New System.Windows.Forms.Padding(4)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(97, 21)
        Me.chkUnidadNegocio.SoloLectura = False
        Me.chkUnidadNegocio.TabIndex = 22
        Me.chkUnidadNegocio.Texto = "Unid. Negocio:"
        Me.chkUnidadNegocio.Valor = False
        '
        'chkUsuario
        '
        Me.chkUsuario.BackColor = System.Drawing.Color.Transparent
        Me.chkUsuario.Color = System.Drawing.Color.Empty
        Me.chkUsuario.Location = New System.Drawing.Point(8, 176)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(89, 21)
        Me.chkUsuario.SoloLectura = False
        Me.chkUsuario.TabIndex = 12
        Me.chkUsuario.Texto = "Usuario:"
        Me.chkUsuario.Valor = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "VUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(118, 176)
        Me.cbxUsuario.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(345, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 13
        Me.cbxUsuario.Texto = ""
        '
        'chkChofer
        '
        Me.chkChofer.BackColor = System.Drawing.Color.Transparent
        Me.chkChofer.Color = System.Drawing.Color.Empty
        Me.chkChofer.Location = New System.Drawing.Point(8, 276)
        Me.chkChofer.Name = "chkChofer"
        Me.chkChofer.Size = New System.Drawing.Size(89, 21)
        Me.chkChofer.SoloLectura = False
        Me.chkChofer.TabIndex = 20
        Me.chkChofer.Texto = "Chofer:"
        Me.chkChofer.Valor = False
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = "IDChofer"
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "VChofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.dtSeleccionado = Nothing
        Me.cbxChofer.Enabled = False
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(118, 276)
        Me.cbxChofer.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionMultiple = False
        Me.cbxChofer.SeleccionObligatoria = False
        Me.cbxChofer.Size = New System.Drawing.Size(345, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 21
        Me.cbxChofer.Texto = ""
        '
        'chkCamion
        '
        Me.chkCamion.BackColor = System.Drawing.Color.Transparent
        Me.chkCamion.Color = System.Drawing.Color.Empty
        Me.chkCamion.Location = New System.Drawing.Point(8, 251)
        Me.chkCamion.Name = "chkCamion"
        Me.chkCamion.Size = New System.Drawing.Size(89, 21)
        Me.chkCamion.SoloLectura = False
        Me.chkCamion.TabIndex = 18
        Me.chkCamion.Texto = "Camion:"
        Me.chkCamion.Valor = False
        '
        'cbxCamion
        '
        Me.cbxCamion.CampoWhere = "IDCamion"
        Me.cbxCamion.CargarUnaSolaVez = False
        Me.cbxCamion.DataDisplayMember = "Descripcion"
        Me.cbxCamion.DataFilter = Nothing
        Me.cbxCamion.DataOrderBy = "Descripcion"
        Me.cbxCamion.DataSource = "VCamion"
        Me.cbxCamion.DataValueMember = "ID"
        Me.cbxCamion.dtSeleccionado = Nothing
        Me.cbxCamion.Enabled = False
        Me.cbxCamion.FormABM = Nothing
        Me.cbxCamion.Indicaciones = Nothing
        Me.cbxCamion.Location = New System.Drawing.Point(118, 251)
        Me.cbxCamion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCamion.Name = "cbxCamion"
        Me.cbxCamion.SeleccionMultiple = False
        Me.cbxCamion.SeleccionObligatoria = False
        Me.cbxCamion.Size = New System.Drawing.Size(345, 21)
        Me.cbxCamion.SoloLectura = False
        Me.cbxCamion.TabIndex = 19
        Me.cbxCamion.Texto = ""
        '
        'chkImpuesto
        '
        Me.chkImpuesto.BackColor = System.Drawing.Color.Transparent
        Me.chkImpuesto.Color = System.Drawing.Color.Empty
        Me.chkImpuesto.Location = New System.Drawing.Point(8, 226)
        Me.chkImpuesto.Name = "chkImpuesto"
        Me.chkImpuesto.Size = New System.Drawing.Size(89, 21)
        Me.chkImpuesto.SoloLectura = False
        Me.chkImpuesto.TabIndex = 16
        Me.chkImpuesto.Texto = "Impuesto:"
        Me.chkImpuesto.Valor = False
        '
        'cbxTipoIVA
        '
        Me.cbxTipoIVA.CampoWhere = Nothing
        Me.cbxTipoIVA.CargarUnaSolaVez = False
        Me.cbxTipoIVA.DataDisplayMember = Nothing
        Me.cbxTipoIVA.DataFilter = Nothing
        Me.cbxTipoIVA.DataOrderBy = Nothing
        Me.cbxTipoIVA.DataSource = Nothing
        Me.cbxTipoIVA.DataValueMember = Nothing
        Me.cbxTipoIVA.dtSeleccionado = Nothing
        Me.cbxTipoIVA.Enabled = False
        Me.cbxTipoIVA.FormABM = Nothing
        Me.cbxTipoIVA.Indicaciones = Nothing
        Me.cbxTipoIVA.Location = New System.Drawing.Point(118, 226)
        Me.cbxTipoIVA.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxTipoIVA.Name = "cbxTipoIVA"
        Me.cbxTipoIVA.SeleccionMultiple = False
        Me.cbxTipoIVA.SeleccionObligatoria = False
        Me.cbxTipoIVA.Size = New System.Drawing.Size(345, 21)
        Me.cbxTipoIVA.SoloLectura = False
        Me.cbxTipoIVA.TabIndex = 17
        Me.cbxTipoIVA.Texto = ""
        '
        'chkCondicion
        '
        Me.chkCondicion.BackColor = System.Drawing.Color.Transparent
        Me.chkCondicion.Color = System.Drawing.Color.Empty
        Me.chkCondicion.Location = New System.Drawing.Point(8, 201)
        Me.chkCondicion.Name = "chkCondicion"
        Me.chkCondicion.Size = New System.Drawing.Size(89, 21)
        Me.chkCondicion.SoloLectura = False
        Me.chkCondicion.TabIndex = 14
        Me.chkCondicion.Texto = "Condicion:"
        Me.chkCondicion.Valor = False
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.Enabled = False
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(118, 201)
        Me.cbxCondicion.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = False
        Me.cbxCondicion.Size = New System.Drawing.Size(345, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 15
        Me.cbxCondicion.Texto = ""
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(8, 151)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(89, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 10
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'chkGrupo
        '
        Me.chkGrupo.BackColor = System.Drawing.Color.Transparent
        Me.chkGrupo.Color = System.Drawing.Color.Empty
        Me.chkGrupo.Location = New System.Drawing.Point(8, 126)
        Me.chkGrupo.Name = "chkGrupo"
        Me.chkGrupo.Size = New System.Drawing.Size(89, 21)
        Me.chkGrupo.SoloLectura = False
        Me.chkGrupo.TabIndex = 8
        Me.chkGrupo.Texto = "Grupo:"
        Me.chkGrupo.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(118, 151)
        Me.cbxMoneda.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(345, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 11
        Me.cbxMoneda.Texto = ""
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = "IDGrupo"
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = "Descripcion"
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = "Descripcion"
        Me.cbxGrupo.DataSource = "VGrupo"
        Me.cbxGrupo.DataValueMember = "ID"
        Me.cbxGrupo.dtSeleccionado = Nothing
        Me.cbxGrupo.Enabled = False
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(118, 126)
        Me.cbxGrupo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionMultiple = False
        Me.cbxGrupo.SeleccionObligatoria = False
        Me.cbxGrupo.Size = New System.Drawing.Size(345, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 9
        Me.cbxGrupo.Texto = ""
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(8, 101)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(89, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 6
        Me.chkTipoComprobante.Texto = "Tipo de Comp.:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = "Descripcion"
        Me.cbxTipoComprobante.DataSource = ""
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(118, 101)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(345, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 7
        Me.cbxTipoComprobante.Texto = ""
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(8, 51)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(89, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 2
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(118, 51)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(345, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 3
        Me.cbxDeposito.Texto = ""
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(8, 76)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(89, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 4
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = "RazonSocial"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(118, 76)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(345, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 5
        Me.cbxProveedor.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(8, 26)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(89, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(118, 26)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(345, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(633, 396)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkSoloRRHH)
        Me.GroupBox2.Controls.Add(Me.chkIncluirRRHH)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.chkFechaDocumento)
        Me.GroupBox2.Controls.Add(Me.txtNumeroOP)
        Me.GroupBox2.Controls.Add(Me.chkFechaOperacion)
        Me.GroupBox2.Controls.Add(Me.txtHastaDocu)
        Me.GroupBox2.Controls.Add(Me.txtDesdeDocu)
        Me.GroupBox2.Controls.Add(Me.chkInformeCC)
        Me.GroupBox2.Controls.Add(Me.fpnLibroIVA)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.fpnTipoGasto)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.chkTipo)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.chkLibroIVA)
        Me.GroupBox2.Location = New System.Drawing.Point(503, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(194, 387)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkSoloRRHH
        '
        Me.chkSoloRRHH.BackColor = System.Drawing.Color.Transparent
        Me.chkSoloRRHH.Color = System.Drawing.Color.Empty
        Me.chkSoloRRHH.Enabled = False
        Me.chkSoloRRHH.Location = New System.Drawing.Point(99, 360)
        Me.chkSoloRRHH.Name = "chkSoloRRHH"
        Me.chkSoloRRHH.Size = New System.Drawing.Size(95, 21)
        Me.chkSoloRRHH.SoloLectura = False
        Me.chkSoloRRHH.TabIndex = 26
        Me.chkSoloRRHH.Texto = "Solo RRHH"
        Me.chkSoloRRHH.Valor = False
        '
        'chkIncluirRRHH
        '
        Me.chkIncluirRRHH.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirRRHH.Color = System.Drawing.Color.Empty
        Me.chkIncluirRRHH.Location = New System.Drawing.Point(5, 360)
        Me.chkIncluirRRHH.Name = "chkIncluirRRHH"
        Me.chkIncluirRRHH.Size = New System.Drawing.Size(95, 21)
        Me.chkIncluirRRHH.SoloLectura = False
        Me.chkIncluirRRHH.TabIndex = 25
        Me.chkIncluirRRHH.Texto = "Incluir RRHH"
        Me.chkIncluirRRHH.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 334)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(25, 13)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "OP:"
        '
        'chkFechaDocumento
        '
        Me.chkFechaDocumento.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaDocumento.Color = System.Drawing.Color.Empty
        Me.chkFechaDocumento.Location = New System.Drawing.Point(5, 65)
        Me.chkFechaDocumento.Name = "chkFechaDocumento"
        Me.chkFechaDocumento.Size = New System.Drawing.Size(119, 24)
        Me.chkFechaDocumento.SoloLectura = False
        Me.chkFechaDocumento.TabIndex = 17
        Me.chkFechaDocumento.Texto = "Fecha Documento:"
        Me.chkFechaDocumento.Valor = False
        '
        'txtNumeroOP
        '
        Me.txtNumeroOP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroOP.Color = System.Drawing.Color.Empty
        Me.txtNumeroOP.Indicaciones = Nothing
        Me.txtNumeroOP.Location = New System.Drawing.Point(34, 330)
        Me.txtNumeroOP.Margin = New System.Windows.Forms.Padding(3, 4, 3, 3)
        Me.txtNumeroOP.Multilinea = False
        Me.txtNumeroOP.Name = "txtNumeroOP"
        Me.txtNumeroOP.Size = New System.Drawing.Size(68, 20)
        Me.txtNumeroOP.SoloLectura = False
        Me.txtNumeroOP.TabIndex = 23
        Me.txtNumeroOP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtNumeroOP.Texto = ""
        '
        'chkFechaOperacion
        '
        Me.chkFechaOperacion.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaOperacion.Color = System.Drawing.Color.Empty
        Me.chkFechaOperacion.Location = New System.Drawing.Point(6, 13)
        Me.chkFechaOperacion.Name = "chkFechaOperacion"
        Me.chkFechaOperacion.Size = New System.Drawing.Size(119, 24)
        Me.chkFechaOperacion.SoloLectura = False
        Me.chkFechaOperacion.TabIndex = 16
        Me.chkFechaOperacion.Texto = "Fecha Operacion:"
        Me.chkFechaOperacion.Valor = False
        '
        'txtHastaDocu
        '
        Me.txtHastaDocu.AñoFecha = 0
        Me.txtHastaDocu.Color = System.Drawing.Color.Empty
        Me.txtHastaDocu.Enabled = False
        Me.txtHastaDocu.Fecha = New Date(2013, 5, 31, 16, 35, 16, 575)
        Me.txtHastaDocu.Location = New System.Drawing.Point(90, 91)
        Me.txtHastaDocu.MesFecha = 0
        Me.txtHastaDocu.Name = "txtHastaDocu"
        Me.txtHastaDocu.PermitirNulo = False
        Me.txtHastaDocu.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaDocu.SoloLectura = False
        Me.txtHastaDocu.TabIndex = 15
        '
        'txtDesdeDocu
        '
        Me.txtDesdeDocu.AñoFecha = 0
        Me.txtDesdeDocu.Color = System.Drawing.Color.Empty
        Me.txtDesdeDocu.Enabled = False
        Me.txtDesdeDocu.Fecha = New Date(2013, 5, 31, 16, 35, 16, 575)
        Me.txtDesdeDocu.Location = New System.Drawing.Point(7, 91)
        Me.txtDesdeDocu.MesFecha = 0
        Me.txtDesdeDocu.Name = "txtDesdeDocu"
        Me.txtDesdeDocu.PermitirNulo = False
        Me.txtDesdeDocu.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeDocu.SoloLectura = False
        Me.txtDesdeDocu.TabIndex = 14
        '
        'chkInformeCC
        '
        Me.chkInformeCC.BackColor = System.Drawing.Color.Transparent
        Me.chkInformeCC.Color = System.Drawing.Color.Empty
        Me.chkInformeCC.Location = New System.Drawing.Point(6, 310)
        Me.chkInformeCC.Name = "chkInformeCC"
        Me.chkInformeCC.Size = New System.Drawing.Size(110, 21)
        Me.chkInformeCC.SoloLectura = False
        Me.chkInformeCC.TabIndex = 12
        Me.chkInformeCC.Texto = "Informe Contable"
        Me.chkInformeCC.Valor = False
        '
        'fpnLibroIVA
        '
        Me.fpnLibroIVA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.fpnLibroIVA.Controls.Add(Me.rdbIncluido)
        Me.fpnLibroIVA.Controls.Add(Me.rdbNoIncluido)
        Me.fpnLibroIVA.Enabled = False
        Me.fpnLibroIVA.Location = New System.Drawing.Point(6, 227)
        Me.fpnLibroIVA.Name = "fpnLibroIVA"
        Me.fpnLibroIVA.Size = New System.Drawing.Size(182, 28)
        Me.fpnLibroIVA.TabIndex = 9
        '
        'rdbIncluido
        '
        Me.rdbIncluido.AutoSize = True
        Me.rdbIncluido.Checked = True
        Me.rdbIncluido.Location = New System.Drawing.Point(3, 3)
        Me.rdbIncluido.Name = "rdbIncluido"
        Me.rdbIncluido.Size = New System.Drawing.Size(67, 17)
        Me.rdbIncluido.TabIndex = 0
        Me.rdbIncluido.TabStop = True
        Me.rdbIncluido.Text = "Incluidos"
        Me.rdbIncluido.UseVisualStyleBackColor = True
        '
        'rdbNoIncluido
        '
        Me.rdbNoIncluido.AutoSize = True
        Me.rdbNoIncluido.Location = New System.Drawing.Point(76, 3)
        Me.rdbNoIncluido.Name = "rdbNoIncluido"
        Me.rdbNoIncluido.Size = New System.Drawing.Size(84, 17)
        Me.rdbNoIncluido.TabIndex = 1
        Me.rdbNoIncluido.Text = "No Incluidos"
        Me.rdbNoIncluido.UseVisualStyleBackColor = True
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(121, 140)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(61, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'fpnTipoGasto
        '
        Me.fpnTipoGasto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.fpnTipoGasto.Controls.Add(Me.rdbGasto)
        Me.fpnTipoGasto.Controls.Add(Me.rdbFondoFijo)
        Me.fpnTipoGasto.Enabled = False
        Me.fpnTipoGasto.Location = New System.Drawing.Point(6, 276)
        Me.fpnTipoGasto.Name = "fpnTipoGasto"
        Me.fpnTipoGasto.Size = New System.Drawing.Size(182, 28)
        Me.fpnTipoGasto.TabIndex = 11
        '
        'rdbGasto
        '
        Me.rdbGasto.AutoSize = True
        Me.rdbGasto.Checked = True
        Me.rdbGasto.Location = New System.Drawing.Point(3, 3)
        Me.rdbGasto.Name = "rdbGasto"
        Me.rdbGasto.Size = New System.Drawing.Size(58, 17)
        Me.rdbGasto.TabIndex = 0
        Me.rdbGasto.TabStop = True
        Me.rdbGasto.Text = "Gastos"
        Me.rdbGasto.UseVisualStyleBackColor = True
        '
        'rdbFondoFijo
        '
        Me.rdbFondoFijo.AutoSize = True
        Me.rdbFondoFijo.Location = New System.Drawing.Point(67, 3)
        Me.rdbFondoFijo.Name = "rdbFondoFijo"
        Me.rdbFondoFijo.Size = New System.Drawing.Size(74, 17)
        Me.rdbFondoFijo.TabIndex = 1
        Me.rdbFondoFijo.Text = "Fondo Fijo"
        Me.rdbFondoFijo.UseVisualStyleBackColor = True
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 140)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(110, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 123)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(6, 181)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 7
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 165)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 6
        Me.lblRanking.Text = "Ranking:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(2013, 5, 31, 16, 35, 16, 575)
        Me.txtHasta.Location = New System.Drawing.Point(89, 39)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'chkTipo
        '
        Me.chkTipo.BackColor = System.Drawing.Color.Transparent
        Me.chkTipo.Color = System.Drawing.Color.Empty
        Me.chkTipo.Location = New System.Drawing.Point(6, 257)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(54, 21)
        Me.chkTipo.SoloLectura = False
        Me.chkTipo.TabIndex = 10
        Me.chkTipo.Texto = "Tipo:"
        Me.chkTipo.Valor = False
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Enabled = False
        Me.txtDesde.Fecha = New Date(2013, 5, 31, 16, 35, 16, 575)
        Me.txtDesde.Location = New System.Drawing.Point(6, 39)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkLibroIVA
        '
        Me.chkLibroIVA.BackColor = System.Drawing.Color.Transparent
        Me.chkLibroIVA.Color = System.Drawing.Color.Empty
        Me.chkLibroIVA.Location = New System.Drawing.Point(6, 208)
        Me.chkLibroIVA.Name = "chkLibroIVA"
        Me.chkLibroIVA.Size = New System.Drawing.Size(77, 21)
        Me.chkLibroIVA.SoloLectura = False
        Me.chkLibroIVA.TabIndex = 8
        Me.chkLibroIVA.Texto = "Libro IVA:"
        Me.chkLibroIVA.Valor = False
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(502, 396)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.FormattingEnabled = True
        Me.cbxDepartamento.Items.AddRange(New Object() {"ADMINISTRACION AQUAY", "ADMINISTRACION CENTRAL", "ADMINISTRACION MOLINO", "ALMACEN GENERAL", "ASESORIA LEGAL", "AUDITORIA INTERNA", "DESARROLLO Y CALIDAD", "DIRECTORIO", "DPTO.COMERCIAL AQUAY", "DPTO.COMERCIAL HARINA DE TRIGO", "DPTO.COMERCIAL NUTRICION ANIMAL", "LABORATORIO DE PLANTA", "LOGISTICA Y DISTRIBUCION", "MANTENIMIENTO", "MARKETING", "PLANEACION Y CONTROL DE PRODUCCION", "PRODUCCION MOLINO DE TRIGO", "PRODUCCION NUTRICION ANIMAL", "PRODUCTOS TERMINADOS", "SEGURIDAD INDUSTRIAL", "SILO", "TALLER AQUAY"})
        Me.cbxDepartamento.Location = New System.Drawing.Point(118, 327)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.Size = New System.Drawing.Size(345, 21)
        Me.cbxDepartamento.TabIndex = 33
        '
        'frmListadoGasto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(732, 429)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoGasto"
        Me.Text = "Listado Ordenes de Pago"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.fpnLibroIVA.ResumeLayout(False)
        Me.fpnLibroIVA.PerformLayout()
        Me.fpnTipoGasto.ResumeLayout(False)
        Me.fpnTipoGasto.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents chkGrupo As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents chkImpuesto As ERP.ocxCHK
    Friend WithEvents cbxTipoIVA As ERP.ocxCBX
    Friend WithEvents chkCondicion As ERP.ocxCHK
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents chkLibroIVA As ERP.ocxCHK
    Friend WithEvents rdbNoIncluido As System.Windows.Forms.RadioButton
    Friend WithEvents rdbIncluido As System.Windows.Forms.RadioButton
    Friend WithEvents chkChofer As ERP.ocxCHK
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents chkCamion As ERP.ocxCHK
    Friend WithEvents cbxCamion As ERP.ocxCBX
    Friend WithEvents fpnLibroIVA As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents fpnTipoGasto As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents rdbGasto As System.Windows.Forms.RadioButton
    Friend WithEvents rdbFondoFijo As System.Windows.Forms.RadioButton
    Friend WithEvents chkTipo As ERP.ocxCHK
    Friend WithEvents chkUsuario As ERP.ocxCHK
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents chkInformeCC As ERP.ocxCHK
    Friend WithEvents chkFechaDocumento As ERP.ocxCHK
    Friend WithEvents chkFechaOperacion As ERP.ocxCHK
    Friend WithEvents txtHastaDocu As ERP.ocxTXTDate
    Friend WithEvents txtDesdeDocu As ERP.ocxTXTDate
    Friend WithEvents txtNumeroOP As ERP.ocxTXTString
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkSoloRRHH As ERP.ocxCHK
    Friend WithEvents chkIncluirRRHH As ERP.ocxCHK
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents chkUnidadNegocio As ocxCHK
    Friend WithEvents txtPalabraClave4 As TextBox
    Friend WithEvents txtPalabraClave3 As TextBox
    Friend WithEvents txtPalabraClave2 As TextBox
    Friend WithEvents txtPalabraClave1 As TextBox
    Friend WithEvents chkPalabraClave As ocxCHK
    Friend WithEvents chkDepartamento As ocxCHK
    Friend WithEvents cbxSeccion As ocxCBX
    Friend WithEvents chkSeccion As ocxCHK
    Friend WithEvents cbxDepartamento As ComboBox
End Class

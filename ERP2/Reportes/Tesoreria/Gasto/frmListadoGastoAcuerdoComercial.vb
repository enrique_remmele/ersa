﻿    Imports ERP.Reporte

Public Class frmListadoGastoAcuerdoComercial

    'CLASES
    Dim CReporte As New CReporteGasto
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE GASTOS / ACUERDOS COMERCIALES"
    Dim TipoInforme As String
    Dim Seccion As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()
        CambiarOrdenacion()

        txtDesde.Hoy()
        txtDesdeDocu.Hoy()
        txtHasta.Hoy()
        txtHastaDocu.Hoy()

        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Orden en formato
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Grupo
        '  CSistema.SqlToComboBox(cbxGrupo.cbx, CData.GetTable("vGrupoUsuario", "IDUsuario=" & vgIDUsuario), "IDGrupo", "Grupo")

        'FA 18/04/2023 Prueba
        'Dim dtDepartamentos As DataTable = CData.GetTable("VDepartamentoEmpresa", " Estado = 'True' ")
        'CSistema.SqlToComboBox(cbxDepartamento.cbx, dtDepartamentos, "ID", "Descripcion")
        'cbxDepartamento.cbx.Text = ""
        'cbxDepartamento.Enabled = False

        'CONDICION
        'cbxCondicion.cbx.Items.Add("CONTADO")
        'cbxCondicion.cbx.Items.Add("CREDITO")
        'cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Sub Listar()

        Dim Where As String = " where 1 = 1 "
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim Tabla As String = "VGastoAcuerdoComercialInforme"

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkFechaDocumento.Valor = True Then
            Where = Where & " and FechaAcuerdo Between '" & txtDesdeDocu.GetValueString & "' And '" & txtHastaDocu.GetValueString & "' "
        End If

        If chkFechaOperacion.Valor = True Then
            Where = Where & " and convert(date,FechaGasto) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        End If

        'Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        'Where = " Where convert(date,FechaTransaccion) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Condicion
            If ctr.Name = "cbxCondicion" Then
                GoTo siguiente
            End If

            'UnidadNegocio
            If ctr.Name = "cbxUnidadNegocio" Then
                GoTo siguiente
            End If

            'Tipo IVA
            If ctr.Name = "cbxTipoIVA" Then
                GoTo siguiente
            End If

            'Departamento
            If ctr.Name = "cbxDepartamento" Then
                GoTo siguiente
            End If

            'Seccion
            If ctr.Name = "cbxSeccion" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If

            'If chkInformeCC.Valor = True Then
            '    If txtNumeroOP.txt.Text <> "" then
            '        Where = Where & " And NumeroOrdenPago = " & txtNumeroOP.GetValue & " "
            '    End If
            'End If

siguiente:

        Next

        'If chkUnidadNegocio.chk.Checked Then
        '    Where = Where & " And IDTransaccion in (Select Distinct IDTransaccion from vDetalleAsiento where IDUnidadNegocioDetalle = " & cbxUnidadNegocio.cbx.SelectedValue & ") "
        'End If

        'SM - 01072021 - Filtro de Departamento Empresa
        'If chkDepartamento.Valor = True Then
        '    'Where = Where & " And IDDepartamentoEmpresa= " & cbxDepartamento.cbx.SelectedValue
        '    Where = Where & " And DepartamentoEmpresa like'%" & cbxDepartamento.Text & "%'"
        'End If

        'SM - 01072021 - Filtro de Departamento Empresa
        'If chkSeccion.Valor = True Then
        '    Where = Where & " And IDSeccion= " & cbxSeccion.cbx.SelectedValue
        'End If

        'Condicion
        'If cbxCondicion.Enabled = True Then
        '    If cbxCondicion.cbx.SelectedIndex = 0 Then
        '        Where = Where & " And Credito='False' "
        '    End If

        '    If cbxCondicion.cbx.SelectedIndex = 1 Then
        '        Where = Where & " And Credito='True' "
        '    End If

        'End If

        If chkPalabraClave.Valor Then
            Where = Where & "And ([Observacion Gasto] like '%" & txtPalabraClave1.Text & "%' And [Observacion Gasto] like '%" & txtPalabraClave2.Text & "%' And [Observacion Gasto] like '%" & txtPalabraClave3.Text & "%' And [Observacion Gasto] like '%" & txtPalabraClave4.Text & "%') "
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        TipoInforme = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString

        'If chkSucursal.Valor = True Then
        '    TipoInforme = TipoInforme & " - Suc:" & cbxSucursal.cbx.Text
        'End If

        'If chkDepartamento.Valor = True Then
        '    TipoInforme = TipoInforme & " - Departamento:" & cbxDepartamento.Text
        'End If

        'If chkSeccion.Valor = True Then
        '    TipoInforme = TipoInforme & " - Seccion:" & cbxSeccion.cbx.Text
        'End If

        Dim Desde As Date
        Dim Hasta As Date

        If chkFechaOperacion.chk.Checked Then
            Desde = txtDesde.txt.Text
            Hasta = txtHasta.txt.Text
        End If

        If chkFechaDocumento.chk.Checked Then
            Desde = txtDesdeDocu.txt.Text
            Hasta = txtHastaDocu.txt.Text
        End If

        CReporte.ListadoGastoAcuerdoComercial(frm, Titulo, Where, OrderBy, Top, TipoInforme, Tabla, Desde, Hasta)


    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VGastoAcuerdoComercialInforme ", "Select Top(0) RazonSocial, FechaGasto, FechaAcuerdo From VGastoAcuerdoComercialInforme")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoGasto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    'Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
    '    cbxSucursal.Enabled = value
    'End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
    End Sub

    'Private Sub chkCondicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
    '    cbxCondicion.Enabled = value
    'End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCuenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        cbxProveedor.Enabled = value
    End Sub

    'Private Sub chkUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)
    '    cbxUsuario.Enabled = value
    'End Sub

    Private Sub chkFechaOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaOperacion.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkFechaDocumento_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaDocumento.PropertyChanged
        txtDesdeDocu.Enabled = value
        txtHastaDocu.Enabled = value
    End Sub

    'Private Sub chkUnidadNegocio_PropertyChanged(sender As Object, e As EventArgs, value As Boolean)
    '    cbxUnidadNegocio.Enabled = value
    'End Sub

    Private Sub chkPalabraClave_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkPalabraClave.PropertyChanged
        txtPalabraClave1.Enabled = value
        txtPalabraClave2.Enabled = value
        txtPalabraClave3.Enabled = value
        txtPalabraClave4.Enabled = value
    End Sub

    'Private Sub chkDepartamento_PropertyChanged(sender As Object, e As EventArgs, value As Boolean)
    '    cbxDepartamento.Enabled = value
    'End Sub

    Private Sub cbxDepartamento_PropertyChanged(sender As Object, e As EventArgs)
        'FA 18/04/2023 Prueba
        'Seccion = CSistema.ExecuteScalar("Select Seccion From vDepartamentoEmpresa where ID = " & cbxDepartamento.cbx.SelectedValue)
        'If Seccion = True Then
        '    Dim dtSeccion As DataTable = CData.GetTable("vSeccion", " IDDepartamentoEmpresa=" & cbxDepartamento.cbx.SelectedValue)
        '    CSistema.SqlToComboBox(cbxSeccion.cbx, dtSeccion, "ID", "Descripcion")
        '    cbxSeccion.cbx.Text = ""
        '    chkSeccion.Enabled = True
        'Else
        '    cbxSeccion.Enabled = False
        '    chkSeccion.Enabled = False
        'End If
    End Sub

    'Private Sub chkSeccion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean)
    '    cbxSeccion.Enabled = value
    'End Sub
End Class
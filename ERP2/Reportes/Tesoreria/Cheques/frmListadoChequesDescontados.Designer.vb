﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoChequesDescontados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtVencimientoHasta = New System.Windows.Forms.DateTimePicker()
        Me.txtVencimientoDesde = New System.Windows.Forms.DateTimePicker()
        Me.txtHasta = New System.Windows.Forms.DateTimePicker()
        Me.txtDesde = New System.Windows.Forms.DateTimePicker()
        Me.chkPeriodoVencimiento = New ERP.ocxCHK()
        Me.chkPeriodoEmisionCheque = New ERP.ocxCHK()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.txtNroDescuento = New System.Windows.Forms.TextBox()
        Me.chkNroDescuento = New ERP.ocxCHK()
        Me.chkBanco = New ERP.ocxCHK()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxCliente = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(354, 140)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(96, 23)
        Me.btnInforme.TabIndex = 5
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtVencimientoHasta)
        Me.GroupBox2.Controls.Add(Me.txtVencimientoDesde)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.chkPeriodoVencimiento)
        Me.GroupBox2.Controls.Add(Me.chkPeriodoEmisionCheque)
        Me.GroupBox2.Location = New System.Drawing.Point(354, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(210, 122)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtVencimientoHasta
        '
        Me.txtVencimientoHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtVencimientoHasta.Location = New System.Drawing.Point(91, 89)
        Me.txtVencimientoHasta.Name = "txtVencimientoHasta"
        Me.txtVencimientoHasta.Size = New System.Drawing.Size(79, 20)
        Me.txtVencimientoHasta.TabIndex = 14
        '
        'txtVencimientoDesde
        '
        Me.txtVencimientoDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtVencimientoDesde.Location = New System.Drawing.Point(6, 89)
        Me.txtVencimientoDesde.Name = "txtVencimientoDesde"
        Me.txtVencimientoDesde.Size = New System.Drawing.Size(79, 20)
        Me.txtVencimientoDesde.TabIndex = 13
        '
        'txtHasta
        '
        Me.txtHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtHasta.Location = New System.Drawing.Point(91, 36)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.Size = New System.Drawing.Size(79, 20)
        Me.txtHasta.TabIndex = 12
        '
        'txtDesde
        '
        Me.txtDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtDesde.Location = New System.Drawing.Point(6, 36)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.Size = New System.Drawing.Size(79, 20)
        Me.txtDesde.TabIndex = 11
        '
        'chkPeriodoVencimiento
        '
        Me.chkPeriodoVencimiento.BackColor = System.Drawing.Color.Transparent
        Me.chkPeriodoVencimiento.Color = System.Drawing.Color.Empty
        Me.chkPeriodoVencimiento.Location = New System.Drawing.Point(6, 63)
        Me.chkPeriodoVencimiento.Name = "chkPeriodoVencimiento"
        Me.chkPeriodoVencimiento.Size = New System.Drawing.Size(198, 21)
        Me.chkPeriodoVencimiento.SoloLectura = False
        Me.chkPeriodoVencimiento.TabIndex = 10
        Me.chkPeriodoVencimiento.Texto = "Periodo de Vencimiento de Cheque:"
        Me.chkPeriodoVencimiento.Valor = False
        '
        'chkPeriodoEmisionCheque
        '
        Me.chkPeriodoEmisionCheque.BackColor = System.Drawing.Color.Transparent
        Me.chkPeriodoEmisionCheque.Color = System.Drawing.Color.Empty
        Me.chkPeriodoEmisionCheque.Location = New System.Drawing.Point(6, 12)
        Me.chkPeriodoEmisionCheque.Name = "chkPeriodoEmisionCheque"
        Me.chkPeriodoEmisionCheque.Size = New System.Drawing.Size(179, 21)
        Me.chkPeriodoEmisionCheque.SoloLectura = False
        Me.chkPeriodoEmisionCheque.TabIndex = 9
        Me.chkPeriodoEmisionCheque.Texto = "Periodo de Emision de Cheque:"
        Me.chkPeriodoEmisionCheque.Valor = False
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtNroDescuento)
        Me.gbxFiltro.Controls.Add(Me.chkNroDescuento)
        Me.gbxFiltro.Controls.Add(Me.chkBanco)
        Me.gbxFiltro.Controls.Add(Me.cbxBanco)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxCliente)
        Me.gbxFiltro.Location = New System.Drawing.Point(0, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(348, 151)
        Me.gbxFiltro.TabIndex = 6
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'txtNroDescuento
        '
        Me.txtNroDescuento.Enabled = False
        Me.txtNroDescuento.Location = New System.Drawing.Point(129, 112)
        Me.txtNroDescuento.Name = "txtNroDescuento"
        Me.txtNroDescuento.Size = New System.Drawing.Size(113, 20)
        Me.txtNroDescuento.TabIndex = 42
        '
        'chkNroDescuento
        '
        Me.chkNroDescuento.BackColor = System.Drawing.Color.Transparent
        Me.chkNroDescuento.Color = System.Drawing.Color.Empty
        Me.chkNroDescuento.Location = New System.Drawing.Point(9, 111)
        Me.chkNroDescuento.Name = "chkNroDescuento"
        Me.chkNroDescuento.Size = New System.Drawing.Size(114, 21)
        Me.chkNroDescuento.SoloLectura = False
        Me.chkNroDescuento.TabIndex = 6
        Me.chkNroDescuento.Texto = "Número Descuento:"
        Me.chkNroDescuento.Valor = False
        '
        'chkBanco
        '
        Me.chkBanco.BackColor = System.Drawing.Color.Transparent
        Me.chkBanco.Color = System.Drawing.Color.Empty
        Me.chkBanco.Location = New System.Drawing.Point(9, 70)
        Me.chkBanco.Name = "chkBanco"
        Me.chkBanco.Size = New System.Drawing.Size(114, 21)
        Me.chkBanco.SoloLectura = False
        Me.chkBanco.TabIndex = 4
        Me.chkBanco.Texto = "Banco Descuento:"
        Me.chkBanco.Valor = False
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = "IDBancoDescuento"
        Me.cbxBanco.CargarUnaSolaVez = False
        Me.cbxBanco.DataDisplayMember = "Descripcion"
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = "VBanco"
        Me.cbxBanco.DataValueMember = "ID"
        Me.cbxBanco.dtSeleccionado = Nothing
        Me.cbxBanco.Enabled = False
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(128, 70)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionMultiple = False
        Me.cbxBanco.SeleccionObligatoria = False
        Me.cbxBanco.Size = New System.Drawing.Size(213, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 5
        Me.cbxBanco.Texto = "ITAU"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 28)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 2
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxCliente
        '
        Me.cbxCliente.CampoWhere = "IDCliente"
        Me.cbxCliente.CargarUnaSolaVez = False
        Me.cbxCliente.DataDisplayMember = "RazonSocial"
        Me.cbxCliente.DataFilter = Nothing
        Me.cbxCliente.DataOrderBy = "RazonSocial"
        Me.cbxCliente.DataSource = ""
        Me.cbxCliente.DataValueMember = "ID"
        Me.cbxCliente.dtSeleccionado = Nothing
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormABM = Nothing
        Me.cbxCliente.Indicaciones = Nothing
        Me.cbxCliente.Location = New System.Drawing.Point(128, 28)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.SeleccionMultiple = False
        Me.cbxCliente.SeleccionObligatoria = False
        Me.cbxCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxCliente.SoloLectura = False
        Me.cbxCliente.TabIndex = 3
        Me.cbxCliente.Texto = "ZUNILDA FLOR CHAMORRO"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(478, 140)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(86, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'frmListadoChequesDescontados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(579, 181)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoChequesDescontados"
        Me.Text = "frmListadoChequesDescontados"
        Me.GroupBox2.ResumeLayout(False)
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkBanco As ERP.ocxCHK
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents cbxCliente As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkPeriodoEmisionCheque As ERP.ocxCHK
    Friend WithEvents chkPeriodoVencimiento As ERP.ocxCHK
    Friend WithEvents txtVencimientoHasta As DateTimePicker
    Friend WithEvents txtVencimientoDesde As DateTimePicker
    Friend WithEvents txtHasta As DateTimePicker
    Friend WithEvents txtDesde As DateTimePicker
    Friend WithEvents chkNroDescuento As ocxCHK
    Friend WithEvents txtNroDescuento As TextBox
End Class

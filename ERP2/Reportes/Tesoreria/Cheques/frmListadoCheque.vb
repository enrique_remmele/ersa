﻿Imports ERP.Reporte

Public Class frmListadoCheque

    'CLASES
    Dim CReporte As New CReporteCheque
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE CHEQUES CLIENTES"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtCliente.Conectar()

        'Funciones
        CargarInformacion()
        CambiarOrdenacion()
        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()
        txtFechaCobranzaDesde.Hoy()
        txtFechaCobranzaHasta.Hoy()
        txtDesdeVencimiento.Hoy()
        txtHastaVencimiento.Hoy()

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        ' cargar cbxtipo
        cbxtipo.cbx.Items.Add("AL DIA")
        cbxtipo.cbx.Items.Add("DIFERIDO")
        cbxtipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'cargar cbxestado
        cbxEstado.cbx.Items.Add("DEPOSITADO")
        cbxEstado.cbx.Items.Add("CARTERA")
        cbxEstado.cbx.Items.Add("RECHAZADO")
        cbxEstado.cbx.Items.Add("DESCONTADO")
        cbxEstado.cbx.Items.Add("RECHAZADO CANCELADO")
        cbxEstado.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'cargar condición
        cbxCondicion.cbx.Items.Add("Contado+Crédito")
        cbxCondicion.cbx.Items.Add("Contado")
        cbxCondicion.cbx.Items.Add("Crédito")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxCondicion.cbx.SelectedIndex = 0

        'Agrupado
        cbxAgrupado.cbx.Items.Add("Cliente")
        cbxAgrupado.cbx.Items.Add("Banco")
        cbxAgrupado.cbx.Items.Add("Estado")
        cbxAgrupado.cbx.Items.Add("Moneda")
        cbxAgrupado.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxAgrupado.cbx.SelectedIndex = 0

        'Moneda
        cbxMoneda.cbx.Text = "GUARANIES"
    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1"
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = " Order By Numero"
        Dim Top As String = ""
        Dim Tipo As String = ""
        Dim TipoInforme As String = ""
        Dim Estado As String = ""
        Dim SubTitulo As String = ""


        If chkFechaCheque.chk.Checked = True Then
            If IsDate(txtDesde.txt.Text) And IsDate(txtHasta.txt.Text) Then
                GoTo Seguir
            End If
        End If

        If chkFechaCobranza.chk.Checked = True Then
            If IsDate(txtFechaCobranzaDesde.txt.Text) And IsDate(txtFechaCobranzaHasta.txt.Text) Then
                GoTo Seguir
            End If
        End If

        If chkFechaVencimiento.chk.Checked = True Then
            If IsDate(txtDesdeVencimiento.txt.Text) And IsDate(txtHastaVencimiento.txt.Text) Then
                GoTo Seguir
            End If
        End If

        If chkFechaRechazo.chk.Checked = True Then
            If IsDate(txtDesdeRechazo.txt.Text) And IsDate(txtHastaRechazo.txt.Text) Then
                GoTo Seguir
            End If

        End If

        MessageBox.Show("Seleccione un rango de fecha", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Exit Sub
Seguir:

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        If chkFechaCheque.chk.Checked Then
            Where = Where & " and Fecha Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False) & "'"
            SubTitulo = "Del " & txtDesde.GetValue & " al " & txtHasta.GetValue & "  "
        End If

        If chkFechaCobranza.Valor = True Then
            Where = Where & " And FecCobranza Between '" & CSistema.FormatoFechaBaseDatos(txtFechaCobranzaDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtFechaCobranzaHasta.GetValue, True, False) & "'"
            SubTitulo = SubTitulo & "Cobranza del " & " " & txtFechaCobranzaDesde.GetValue & " al " & txtFechaCobranzaHasta.GetValue & "  "
        End If

        If chkFechaVencimiento.Valor = True Then
            Where = Where & " And FechaVencimiento Between '" & CSistema.FormatoFechaBaseDatos(txtDesdeVencimiento.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaVencimiento.GetValue, True, False) & "'"
            SubTitulo = SubTitulo & "Vecimiento del " & " " & txtDesdeVencimiento.GetValue & " al " & txtHastaVencimiento.GetValue & "  "
        End If

        If chkFechaRechazo.Valor = True Then
            Where = Where & " And FecRechazo Between '" & CSistema.FormatoFechaBaseDatos(txtDesdeRechazo.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaRechazo.GetValue, True, False) & "'"
            SubTitulo = SubTitulo & "Rechazo del " & " " & txtDesdeRechazo.GetValue & " al " & txtHastaRechazo.GetValue & "  "
        End If


        'Filtrar moneda y/o comprobante
        If cbxMoneda.Enabled Then
            Where = Where & " and IDMoneda=" & cbxMoneda.GetValue & " "
        End If
        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Cliente
        If chkCliente.Valor = True Then
            Where = Where & " And IDCliente=" & txtCliente.Registro("ID") & " "
        End If

        'Establecemos el Orden
        OrderBy = EstablecerOrderBy()

        If chkTipo.chk.Checked = True Then
            If cbxtipo.cbx.Text <> "" Then
                Tipo = " And " & " Tipo= '" & cbxtipo.cbx.Text & "'"
            End If

        End If
        If chkEstado.chk.Checked = True Then
            If cbxEstado.cbx.Text <> "" Then
                Estado = " And " & " Estado ='" & cbxEstado.cbx.Text & "'"
            End If
        End If

        Select Case cbxCondicion.cbx.SelectedIndex
            Case 1
                Where = Where & "And Condicion='False'"
            Case 2
                Where = Where & "And Condicion='True'"
        End Select

        'TipoInforme = "Del " & txtDesde.GetValue & " al " & txtHasta.GetValue

        If chkEstado.Valor = True Then
            TipoInforme = TipoInforme & " Estado = " & cbxEstado.Texto
        End If
        If cbxEstado.Texto = "RECHAZADO" Then
            TipoInforme = TipoInforme & " CON SALDO"
        End If
        Dim vFiltros As String = ""
        CReporte.ArmarSubTitulo(vFiltros, gbxFiltro)
        If chkFormatoPlanillaParaDescuento.chk.Checked Then
            CReporte.ListarChequeDiferidoParaDescuento(frm, Where, WhereDetalle, Tipo, Estado, OrderBy, Top, vgUsuarioIdentificador, TipoInforme & " - " & vFiltros, chkAgrupado.Valor, cbxAgrupado.cbx.Text)

        Else
            CReporte.ListarChequeCliente(frm, Where, WhereDetalle, Tipo, Estado, OrderBy, Top, vgUsuarioIdentificador, SubTitulo & vFiltros, chkAgrupado.Valor, cbxAgrupado.cbx.Text)

        End If
       
    End Sub

    Function EstablecerOrderBy() As String

        Dim OrderBy As String = ""
        Dim OrderByGrupo As String = ""
        Dim OrderBy2 As String = ""
        Dim OrderBy3 As String = ""

        'Si esta agrupado
        If chkAgrupado.chk.Checked Then
            OrderByGrupo = "Order By " & cbxAgrupado.cbx.Text & " "
        End If

        'Ordenado
        Select Case cbxOrdenadoPor.cbx.SelectedIndex
            Case Is >= 0
                OrderBy2 = ", " & cbxOrdenadoPor.cbx.Text
                OrderBy3 = "Order By " & cbxOrdenadoPor.cbx.Text
            Case Else
                Return OrderByGrupo
        End Select

        'En forma
        Select Case cbxEnForma.cbx.SelectedIndex
            Case Is >= 0
                OrderBy2 = OrderBy2
                OrderBy3 = OrderBy3
        End Select


        'Armar Order By
        If chkAgrupado.Valor = False Then
            OrderBy = OrderBy3
        Else
            OrderBy = OrderByGrupo & OrderBy2
        End If

        Return OrderBy

    End Function


    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VChequeCliente", "Select Top(0) Numero, Fecha, NroCheque, Banco, Cliente, Tipo, Estado, Importe From VChequeCliente ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoCheque_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxBanco_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkBanco.PropertyChanged
        cbxBanco.Enabled = value
    End Sub

    Private Sub chkTipo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipo.PropertyChanged
        cbxtipo.Enabled = value

    End Sub

    Private Sub cbxEstado_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkEstado.PropertyChanged
        cbxEstado.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkFechaCobranza_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkFechaCobranza.PropertyChanged
        txtFechaCobranzaDesde.Enabled = value
        txtFechaCobranzaHasta.Enabled = value
    End Sub

    Private Sub chkAgrupado_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkAgrupado.PropertyChanged
        cbxAgrupado.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub cbxtipo_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxtipo.PropertyChanged
        If cbxtipo.cbx.Text = "DIFERIDO" Then
            chkFormatoPlanillaParaDescuento.Enabled = True
        Else
            chkFormatoPlanillaParaDescuento.Enabled = False
        End If
    End Sub

    Private Sub chkFechaCheque_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaCheque.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkFechaVencimiento_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaVencimiento.PropertyChanged
        txtDesdeVencimiento.Enabled = value
        txtHastaVencimiento.Enabled = value
    End Sub

    Private Sub chkFechaRechazo_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaRechazo.PropertyChanged
        txtDesdeRechazo.Enabled = value
        txtHastaRechazo.Enabled = value
    End Sub
End Class



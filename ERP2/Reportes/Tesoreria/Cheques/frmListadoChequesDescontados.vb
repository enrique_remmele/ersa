﻿Imports ERP.Reporte
Public Class frmListadoChequesDescontados

    'CLASES
    Dim CReporte As New CReporteCheque
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE VENCIMIENTO DE CHEQUES DESCONTADOS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()


    End Sub

    Sub CargarInformacion()
        CSistema.SqlToComboBox(cbxCliente, "Select ID, Razonsocial from Cliente where IDEstado = 1 order by RazonSocial")

    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1"
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Estado As String = ""
        Dim SubTitulo As String = ""


        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkPeriodoEmisionCheque.chk.Checked Then
            Where = Where & " And FechaEmisionCheque Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.Text, True, False) & "' "
        End If

        If chkPeriodoVencimiento.chk.Checked Then
            Where = Where & " And Vencimiento Between '" & CSistema.FormatoFechaBaseDatos(txtVencimientoDesde.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtVencimientoHasta.Text, True, False) & "' "
        End If

        If chkNroDescuento.chk.Checked = True And txtNroDescuento.Text <> "" Then
            Where = Where & " And NroOperacionDescuento=" & txtNroDescuento.Text & " "
            SubTitulo = SubTitulo & "  -  NroOperacionDescuento: " & txtNroDescuento.Text
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Filtros para traer informes por Cliente y/o Banco
        cbxBanco.EstablecerCondicion(Where)
        cbxCliente.EstablecerCondicion(Where)

        CReporte.ListarChequesDiferidosDescontados(frm, Where, vgUsuarioIdentificador, SubTitulo)

    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmChequesDiferidosDepositados_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxBanco_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkBanco.PropertyChanged
        cbxBanco.Enabled = value
    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub


    Private Sub chkPeriodoEmisionCheque_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkPeriodoEmisionCheque.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub


    Private Sub chkPeriodoVencimiento_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkPeriodoVencimiento.PropertyChanged
        txtVencimientoDesde.Enabled = value
        txtVencimientoHasta.Enabled = value
    End Sub

    Private Sub chkNroDescuento_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkNroDescuento.PropertyChanged
        txtNroDescuento.Enabled = value
    End Sub

End Class
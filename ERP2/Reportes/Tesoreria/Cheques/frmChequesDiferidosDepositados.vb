﻿Imports ERP.Reporte
Public Class frmChequesDiferidosDepositados

    'CLASES
    Dim CReporte As New CReporteCheque
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE CHEQUES DIFERIDOS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()



    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'CONDICION
        cbxCondicion.cbx.Items.Add("CONT")
        cbxCondicion.cbx.Items.Add("CRED")

        'cargar cbxestado
        cbxEstado.cbx.Items.Add("DEPOSITADO")
        cbxEstado.cbx.Items.Add("CARTERA")
        cbxEstado.cbx.Items.Add("RECHAZADO")
        cbxEstado.cbx.Items.Add("DESCONTADO")
        cbxEstado.cbx.DropDownStyle = ComboBoxStyle.DropDownList


    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Condicion As String = ""
        Dim Estado As String = ""
        Dim SubTitulo As String = ""


        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        If chkCondicion.chk.Checked = True Then
            If cbxCondicion.cbx.Text <> "" Then
                Condicion = " And " & " [Cond.] = '" & cbxCondicion.cbx.Text & "'"
            End If

        End If

        If chkEstado.chk.Checked = True Then
            If cbxEstado.cbx.Text <> "" Then
                Estado = " And " & " Estado ='" & cbxEstado.cbx.Text & "'"
            End If

        End If

        cbxBanco.EstablecerCondicion(WhereDetalle)
        cbxCliente.EstablecerCondicion(WhereDetalle)

        SubTitulo = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString

        CReporte.ListarChequesDiferidosDepositados(frm, Where, WhereDetalle, Condicion, OrderBy, Top, vgUsuarioIdentificador, SubTitulo)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VChequesDiferidosDepositados", "Select Top(0) Banco, Cliente, Fecha,[Cheque Nro],Importe From VChequesDiferidosDepositados ")



        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmChequesDiferidosDepositados_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxBanco_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkBanco.PropertyChanged
        cbxBanco.Enabled = value
    End Sub


    Private Sub chkCondicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCondicion.PropertyChanged
        cbxCondicion.Enabled = value
    End Sub

    Private Sub chkEstado_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkEstado.PropertyChanged
        cbxEstado.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class






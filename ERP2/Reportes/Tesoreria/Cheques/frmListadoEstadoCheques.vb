﻿Imports ERP.Reporte
Public Class frmListadoEstadoCheques

    'CLASES
    Dim CReporte As New CReporteChequeras
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE ESTADO DE CHEQUES"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Botones
        'txtFechaDesde.Enabled = False
        'txtFechaHasta.Enabled = False
        txtDesde.Enabled = True
        txtHasta.Enabled = True

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        CSistema.SqlToComboBox(cbxBanco.cbx, "select distinct BA.ID, BA.Descripcion from Banco BA right join Chequera CH on BA.ID = CH.IDBanco")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        cbxOrdenadoPor.cbx.Items.Add("FECHA")
        cbxOrdenadoPor.cbx.Items.Add("NRO DE CHEQUE")
        cbxOrdenadoPor.cbx.Items.Add("ESTADO")
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Estado del cheque
        cbxEstadoCheque.cbx.Items.Add("ANULADO")
        cbxEstadoCheque.cbx.Items.Add("UTILIZADO")
        cbxEstadoCheque.cbx.Items.Add("EN BLANCO")

        'Estado de la chequera
        cbxEstadoChequera.cbx.Items.Add("ACTIVO")
        cbxEstadoChequera.cbx.Items.Add("INACTIVO")



    End Sub

    Sub Listar()

        tsslEstado.Text = ""
        'If chkNroCheque.chk.Checked = True Then
        If VerificarDatos() = False Then
                Dim mensaje As String = "Los numero no se encuentran en la chequera"
                CSistema.MostrarError(mensaje, ctrError, txtDesde, tsslEstado, ErrorIconAlignment.MiddleRight)
                Exit Sub
            End If
        'End If

        Dim Where As String
        Dim EstadoCheque As String = ""
        Dim EstadoChequera As String = ""
        Dim OrderBy As String = ""
        Dim EnForma As String = ""
        Dim Top As String = ""
        Dim Subtitulo As String = "Banco: " & cbxBanco.cbx.Text & " Cuenta bancaria: " & cbxCuentaBancaria.cbx.Text & IIf(cbxEstadoCheque.Enabled, " Estado cheque: " & cbxEstadoCheque.cbx.Text, "") & IIf(cbxEstadoChequera.Enabled, " Estado Chequera:" & cbxEstadoChequera.cbx.Text, "")
        Dim Titulo As String = "Listado de estado de cheques"

        'Estado del cheque
        Select Case cbxEstadoCheque.cbx.Text
            Case "ANULADO"
                EstadoCheque = "'Anulado'"
            Case "UTILIZADO"
                EstadoCheque = "'Utilizado'"
            Case "EN BLANCO"
                EstadoCheque = "'En blanco'"
        End Select

        'Estado de la chequera
        Select Case cbxEstadoChequera.cbx.Text
            Case "ACTIVO"
                EstadoChequera = "'1'"
            Case "INACTIVO"
                EstadoChequera = "'0'"
        End Select

        'Orden
        Select Case cbxOrdenadoPor.cbx.Text
            Case "FECHA"
                OrderBy = "'Fecha'"
            Case "NRO DE CHEQUE"
                OrderBy = "'NroCheque'"
            Case "ESTADO"
                OrderBy = "'Estado'"
            Case ""
                OrderBy = "null"
        End Select

        'Orden en forma
        Select Case cbxEnForma.cbx.Text
            Case "ASC"
                EnForma = "'ASC'"
            Case "DESC"
                EnForma = "'DESC'"
            Case ""
                EnForma = "null"
        End Select

        Where = "'" & cbxBanco.cbx.SelectedValue & "','" & cbxCuentaBancaria.cbx.SelectedValue & "','" & Replace(txtDesde.txt.Text, ".", "") & "','" & Replace(txtHasta.txt.Text, ".", "") & "', " & IIf(cbxEstadoCheque.Enabled, EstadoCheque, "null") & ", " & IIf(cbxEstadoChequera.Enabled, EstadoChequera, "null") & ", " & OrderBy & ", " & EnForma & ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        CReporte.ListarEstadoCheques(frm, Where, "", vgUsuarioIdentificador, Titulo, Subtitulo, "NroCheque", True)

    End Sub

    Function VerificarDatos() As Boolean

        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("select * from chequera where '" & Replace(txtDesde.txt.Text, ".", "") & "' between NroDesde and NroHasta and '" & Replace(txtHasta.txt.Text, ".", "") & "' between NroDesde and NroHasta and IDCuentaBancaria = " & cbxCuentaBancaria.GetValue)
        If dt.Rows.Count > 0 Then
            Return True
        End If
        Return False

    End Function

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmChequesDiferidosDepositados_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCondicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkEstadoCheque.PropertyChanged
        cbxEstadoCheque.Enabled = value
    End Sub

    Private Sub chkEstado_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkEstadoChequera.PropertyChanged
        cbxEstadoChequera.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub CargarCuentasSegunBanco()

        'Cargamos las cuentas bancarias del cheque seleccionado
        cbxCuentaBancaria.cbx.Text = ""
        CSistema.SqlToComboBox(cbxCuentaBancaria.cbx, "Select ID, CuentaBancaria From CuentaBancaria Where IDBanco=" & cbxBanco.cbx.SelectedValue & " Order By Cuentabancaria Asc")

        ''Cargamos el menor y el mayor numero de cheques encontrados para el banco seleccionado
        'Dim dt As New DataTable
        'dt = CSistema.ExecuteToDataTable("select 'Desde' = isnull(min(NroDesde),0), 'Hasta' = isnull(max(NroHasta),0) From vchequera where IDBanco = '" & cbxBanco.cbx.SelectedValue & "'")
        ''Solo procesar si es que se encontro la fila asociada
        'If dt.Rows.Count > 0 Then
        '    'Cargamos la fila "0" en un nuevo objeto DATAROW
        '    Dim oRow As DataRow
        '    oRow = dt.Rows(0)
        '    'Asignamos los valores
        '    txtDesde.txt.Text = oRow("Desde").ToString
        '    txtHasta.txt.Text = oRow("Hasta").ToString
        'End If

    End Sub

    Private Sub chkCuenta_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)

        cbxCuentaBancaria.Enabled = value
        If value = True Then
            CargarDesdeHastaSegunCuenta()
        Else
            CargarCuentasSegunBanco()
        End If

    End Sub

    Private Sub cbxCuentaBancaria_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxCuentaBancaria.PropertyChanged

        If cbxCuentaBancaria.Enabled = False Or IsNumeric(cbxCuentaBancaria.cbx.SelectedValue) = False Then
            Exit Sub
        End If

        If chkNroCheque.chk.Checked = True Then
            CargarDesdeHastaSegunCuenta()
        End If


    End Sub

    Private Sub cbxCuentaBancaria_TeclaPrecionada(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxCuentaBancaria.TeclaPrecionada

        If cbxCuentaBancaria.Enabled = False Or IsNumeric(cbxCuentaBancaria.cbx.SelectedValue) = False Then
            Exit Sub
        End If
        CargarDesdeHastaSegunCuenta()

    End Sub

    Private Sub CargarDesdeHastaSegunCuenta()

        'Cargamos el menor y el mayor numero de cheques encontrados para la cuenta seleccionada
        Dim dt As New DataTable
        dt = CSistema.ExecuteToDataTable("select top 1 'Desde' = isnull(NroDesde,0), 'Hasta' = isnull(NroHasta,0) From vchequera where Estado = '1' and IDBanco = '" & cbxBanco.cbx.SelectedValue & "' and IdCuentaBancaria = '" & cbxCuentaBancaria.cbx.SelectedValue & "'")
        'Solo procesar si es que se encontro la fila asociada
        If dt.Rows.Count > 0 Then
            'Cargamos la fila "0" en un nuevo objeto DATAROW
            Dim oRow As DataRow
            oRow = dt.Rows(0)
            'Asignamos los valores
            txtDesde.txt.Text = oRow("Desde").ToString
            txtHasta.txt.Text = oRow("Hasta").ToString
        End If

    End Sub

    Private Sub cbxBanco_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxBanco.PropertyChanged

        If cbxBanco.Enabled = False Or IsNumeric(cbxBanco.cbx.SelectedValue) = False Then
            Exit Sub
        End If
        CargarCuentasSegunBanco()

    End Sub

    Private Sub cbxBanco_TeclaPrecionada_1(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles cbxBanco.TeclaPrecionada

        If cbxBanco.Enabled = False Or IsNumeric(cbxBanco.cbx.SelectedValue) = False Then
            Exit Sub
        End If
        CargarCuentasSegunBanco()

    End Sub

    'Private Sub chkFechaCheque_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaCheque.PropertyChanged

    '    If chkFechaCheque.chk.Checked = True Then
    '        txtFechaDesde.Enabled = True
    '        txtFechaHasta.Enabled = True
    '    Else
    '        txtFechaDesde.Clear()
    '        txtFechaHasta.Clear()
    '        txtFechaDesde.Enabled = False
    '        txtFechaHasta.Enabled = False

    '    End If

    'End Sub

    'Private Sub chkNroCheque_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkNroCheque.PropertyChanged

    '    If chkNroCheque.chk.Checked = True Then
    '        CargarDesdeHastaSegunCuenta()
    '        txtDesde.Enabled = True
    '        txtHasta.Enabled = True
    '    Else
    '        txtDesde.txt.Clear()
    '        txtHasta.txt.Clear()
    '        txtDesde.Enabled = False
    '        txtHasta.Enabled = False

    '    End If
    'End Sub

End Class






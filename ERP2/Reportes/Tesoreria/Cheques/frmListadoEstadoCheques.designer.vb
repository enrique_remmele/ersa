﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoEstadoCheques
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtHasta = New ERP.ocxTXTNumeric()
        Me.chkNroCheque = New ERP.ocxCHK()
        Me.txtDesde = New ERP.ocxTXTNumeric()
        Me.chkFechaCheque = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.txtFechaHasta = New ERP.ocxTXTDate()
        Me.txtFechaDesde = New ERP.ocxTXTDate()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxCuentaBancaria = New ERP.ocxCBX()
        Me.chkEstadoChequera = New ERP.ocxCHK()
        Me.cbxEstadoChequera = New ERP.ocxCBX()
        Me.chkEstadoCheque = New ERP.ocxCHK()
        Me.cbxEstadoCheque = New ERP.ocxCBX()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(7, 147)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        Me.lblOrdenado.Visible = False
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(400, 204)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.chkNroCheque)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.chkFechaCheque)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.txtFechaHasta)
        Me.GroupBox2.Controls.Add(Me.txtFechaDesde)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Location = New System.Drawing.Point(356, 1)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 196)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Decimales = True
        Me.txtHasta.Indicaciones = Nothing
        Me.txtHasta.Location = New System.Drawing.Point(105, 50)
        Me.txtHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.Size = New System.Drawing.Size(91, 25)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 20
        Me.txtHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtHasta.Texto = "0"
        '
        'chkNroCheque
        '
        Me.chkNroCheque.BackColor = System.Drawing.Color.Transparent
        Me.chkNroCheque.Color = System.Drawing.Color.Empty
        Me.chkNroCheque.Location = New System.Drawing.Point(6, 14)
        Me.chkNroCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.chkNroCheque.Name = "chkNroCheque"
        Me.chkNroCheque.Size = New System.Drawing.Size(82, 21)
        Me.chkNroCheque.SoloLectura = False
        Me.chkNroCheque.TabIndex = 21
        Me.chkNroCheque.Texto = "Nro. Cheque:"
        Me.chkNroCheque.Valor = False
        Me.chkNroCheque.Visible = False
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Decimales = True
        Me.txtDesde.Indicaciones = Nothing
        Me.txtDesde.Location = New System.Drawing.Point(6, 52)
        Me.txtDesde.Margin = New System.Windows.Forms.Padding(4)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.Size = New System.Drawing.Size(91, 23)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 19
        Me.txtDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDesde.Texto = "0"
        '
        'chkFechaCheque
        '
        Me.chkFechaCheque.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaCheque.Color = System.Drawing.Color.Empty
        Me.chkFechaCheque.Location = New System.Drawing.Point(4, 83)
        Me.chkFechaCheque.Name = "chkFechaCheque"
        Me.chkFechaCheque.Size = New System.Drawing.Size(136, 21)
        Me.chkFechaCheque.SoloLectura = False
        Me.chkFechaCheque.TabIndex = 21
        Me.chkFechaCheque.Texto = "Fecha Cheque"
        Me.chkFechaCheque.Valor = False
        Me.chkFechaCheque.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(102, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Hasta:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(3, 35)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(41, 13)
        Me.lblPeriodo.TabIndex = 17
        Me.lblPeriodo.Text = "Desde:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(164, 165)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        Me.cbxEnForma.Visible = False
        '
        'txtFechaHasta
        '
        Me.txtFechaHasta.AñoFecha = 0
        Me.txtFechaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaHasta.Enabled = False
        Me.txtFechaHasta.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaHasta.Location = New System.Drawing.Point(90, 107)
        Me.txtFechaHasta.MesFecha = 0
        Me.txtFechaHasta.Name = "txtFechaHasta"
        Me.txtFechaHasta.PermitirNulo = False
        Me.txtFechaHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaHasta.SoloLectura = False
        Me.txtFechaHasta.TabIndex = 20
        Me.txtFechaHasta.Visible = False
        '
        'txtFechaDesde
        '
        Me.txtFechaDesde.AñoFecha = 0
        Me.txtFechaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaDesde.Enabled = False
        Me.txtFechaDesde.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaDesde.Location = New System.Drawing.Point(7, 107)
        Me.txtFechaDesde.MesFecha = 0
        Me.txtFechaDesde.Name = "txtFechaDesde"
        Me.txtFechaDesde.PermitirNulo = False
        Me.txtFechaDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaDesde.SoloLectura = False
        Me.txtFechaDesde.TabIndex = 19
        Me.txtFechaDesde.Visible = False
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(7, 165)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        Me.cbxOrdenadoPor.Visible = False
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.Label3)
        Me.gbxFiltro.Controls.Add(Me.Label2)
        Me.gbxFiltro.Controls.Add(Me.cbxCuentaBancaria)
        Me.gbxFiltro.Controls.Add(Me.chkEstadoChequera)
        Me.gbxFiltro.Controls.Add(Me.cbxEstadoChequera)
        Me.gbxFiltro.Controls.Add(Me.chkEstadoCheque)
        Me.gbxFiltro.Controls.Add(Me.cbxEstadoCheque)
        Me.gbxFiltro.Controls.Add(Me.cbxBanco)
        Me.gbxFiltro.Location = New System.Drawing.Point(2, 2)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(348, 126)
        Me.gbxFiltro.TabIndex = 2
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Banco:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Cuenta Bancaria:"
        '
        'cbxCuentaBancaria
        '
        Me.cbxCuentaBancaria.CampoWhere = Nothing
        Me.cbxCuentaBancaria.CargarUnaSolaVez = False
        Me.cbxCuentaBancaria.DataDisplayMember = Nothing
        Me.cbxCuentaBancaria.DataFilter = Nothing
        Me.cbxCuentaBancaria.DataOrderBy = Nothing
        Me.cbxCuentaBancaria.DataSource = Nothing
        Me.cbxCuentaBancaria.DataValueMember = Nothing
        Me.cbxCuentaBancaria.dtSeleccionado = Nothing
        Me.cbxCuentaBancaria.FormABM = Nothing
        Me.cbxCuentaBancaria.Indicaciones = Nothing
        Me.cbxCuentaBancaria.Location = New System.Drawing.Point(128, 45)
        Me.cbxCuentaBancaria.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxCuentaBancaria.Name = "cbxCuentaBancaria"
        Me.cbxCuentaBancaria.SeleccionMultiple = False
        Me.cbxCuentaBancaria.SeleccionObligatoria = False
        Me.cbxCuentaBancaria.Size = New System.Drawing.Size(214, 21)
        Me.cbxCuentaBancaria.SoloLectura = False
        Me.cbxCuentaBancaria.TabIndex = 13
        Me.cbxCuentaBancaria.Texto = ""
        '
        'chkEstadoChequera
        '
        Me.chkEstadoChequera.BackColor = System.Drawing.Color.Transparent
        Me.chkEstadoChequera.Color = System.Drawing.Color.Empty
        Me.chkEstadoChequera.Location = New System.Drawing.Point(8, 94)
        Me.chkEstadoChequera.Margin = New System.Windows.Forms.Padding(4)
        Me.chkEstadoChequera.Name = "chkEstadoChequera"
        Me.chkEstadoChequera.Size = New System.Drawing.Size(114, 21)
        Me.chkEstadoChequera.SoloLectura = False
        Me.chkEstadoChequera.TabIndex = 10
        Me.chkEstadoChequera.Texto = "Estado chequera:"
        Me.chkEstadoChequera.Valor = False
        '
        'cbxEstadoChequera
        '
        Me.cbxEstadoChequera.CampoWhere = "IDEstado"
        Me.cbxEstadoChequera.CargarUnaSolaVez = False
        Me.cbxEstadoChequera.DataDisplayMember = "Estado"
        Me.cbxEstadoChequera.DataFilter = Nothing
        Me.cbxEstadoChequera.DataOrderBy = ""
        Me.cbxEstadoChequera.DataSource = ""
        Me.cbxEstadoChequera.DataValueMember = ""
        Me.cbxEstadoChequera.dtSeleccionado = Nothing
        Me.cbxEstadoChequera.Enabled = False
        Me.cbxEstadoChequera.FormABM = Nothing
        Me.cbxEstadoChequera.Indicaciones = Nothing
        Me.cbxEstadoChequera.Location = New System.Drawing.Point(128, 92)
        Me.cbxEstadoChequera.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEstadoChequera.Name = "cbxEstadoChequera"
        Me.cbxEstadoChequera.SeleccionMultiple = False
        Me.cbxEstadoChequera.SeleccionObligatoria = False
        Me.cbxEstadoChequera.Size = New System.Drawing.Size(213, 21)
        Me.cbxEstadoChequera.SoloLectura = False
        Me.cbxEstadoChequera.TabIndex = 11
        Me.cbxEstadoChequera.Texto = "ACTIVO"
        '
        'chkEstadoCheque
        '
        Me.chkEstadoCheque.BackColor = System.Drawing.Color.Transparent
        Me.chkEstadoCheque.Color = System.Drawing.Color.Empty
        Me.chkEstadoCheque.Location = New System.Drawing.Point(8, 68)
        Me.chkEstadoCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.chkEstadoCheque.Name = "chkEstadoCheque"
        Me.chkEstadoCheque.Size = New System.Drawing.Size(114, 21)
        Me.chkEstadoCheque.SoloLectura = False
        Me.chkEstadoCheque.TabIndex = 6
        Me.chkEstadoCheque.Texto = "Estado cheque:"
        Me.chkEstadoCheque.Valor = False
        '
        'cbxEstadoCheque
        '
        Me.cbxEstadoCheque.CampoWhere = "IDEstado"
        Me.cbxEstadoCheque.CargarUnaSolaVez = False
        Me.cbxEstadoCheque.DataDisplayMember = "Estado"
        Me.cbxEstadoCheque.DataFilter = Nothing
        Me.cbxEstadoCheque.DataOrderBy = ""
        Me.cbxEstadoCheque.DataSource = ""
        Me.cbxEstadoCheque.DataValueMember = ""
        Me.cbxEstadoCheque.dtSeleccionado = Nothing
        Me.cbxEstadoCheque.Enabled = False
        Me.cbxEstadoCheque.FormABM = Nothing
        Me.cbxEstadoCheque.Indicaciones = Nothing
        Me.cbxEstadoCheque.Location = New System.Drawing.Point(128, 68)
        Me.cbxEstadoCheque.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxEstadoCheque.Name = "cbxEstadoCheque"
        Me.cbxEstadoCheque.SeleccionMultiple = False
        Me.cbxEstadoCheque.SeleccionObligatoria = False
        Me.cbxEstadoCheque.Size = New System.Drawing.Size(213, 21)
        Me.cbxEstadoCheque.SoloLectura = False
        Me.cbxEstadoCheque.TabIndex = 7
        Me.cbxEstadoCheque.Texto = "ANULADO"
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = ""
        Me.cbxBanco.CargarUnaSolaVez = False
        Me.cbxBanco.DataDisplayMember = ""
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = ""
        Me.cbxBanco.DataValueMember = ""
        Me.cbxBanco.dtSeleccionado = Nothing
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(129, 21)
        Me.cbxBanco.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionMultiple = False
        Me.cbxBanco.SeleccionObligatoria = False
        Me.cbxBanco.Size = New System.Drawing.Size(213, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 5
        Me.cbxBanco.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(530, 204)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 234)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 10, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(606, 22)
        Me.StatusStrip1.TabIndex = 4
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'frmListadoEstadoCheques
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(606, 256)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoEstadoCheques"
        Me.Text = "Listado de estado de cheques"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents cbxEstadoCheque As ERP.ocxCBX
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkEstadoCheque As ERP.ocxCHK
    Friend WithEvents chkEstadoChequera As ERP.ocxCHK
    Friend WithEvents cbxEstadoChequera As ERP.ocxCBX
    Friend WithEvents cbxCuentaBancaria As ERP.ocxCBX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtHasta As ocxTXTNumeric
    Friend WithEvents chkNroCheque As ocxCHK
    Friend WithEvents txtDesde As ocxTXTNumeric
    Friend WithEvents chkFechaCheque As ocxCHK
    Friend WithEvents Label1 As Label
    Friend WithEvents txtFechaHasta As ocxTXTDate
    Friend WithEvents lblPeriodo As Label
    Friend WithEvents txtFechaDesde As ocxTXTDate
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoCheque
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtHastaVencimiento = New ERP.ocxTXTDate()
        Me.chkFechaVencimiento = New ERP.ocxCHK()
        Me.txtDesdeVencimiento = New ERP.ocxTXTDate()
        Me.cbxAgrupado = New ERP.ocxCBX()
        Me.txtFechaCobranzaHasta = New ERP.ocxTXTDate()
        Me.chkFechaCobranza = New ERP.ocxCHK()
        Me.txtFechaCobranzaDesde = New ERP.ocxTXTDate()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkAgrupado = New ERP.ocxCHK()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkFormatoPlanillaParaDescuento = New ERP.ocxCHK()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkEstado = New ERP.ocxCHK()
        Me.cbxEstado = New ERP.ocxCBX()
        Me.chkTipo = New ERP.ocxCHK()
        Me.cbxtipo = New ERP.ocxCBX()
        Me.chkBanco = New ERP.ocxCHK()
        Me.cbxBanco = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.chkFechaCheque = New ERP.ocxCHK()
        Me.chkFechaRechazo = New ERP.ocxCHK()
        Me.txtHastaRechazo = New ERP.ocxTXTDate()
        Me.txtDesdeRechazo = New ERP.ocxTXTDate()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(163, 347)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(185, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 291)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 10
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaRechazo)
        Me.GroupBox2.Controls.Add(Me.txtHastaRechazo)
        Me.GroupBox2.Controls.Add(Me.txtDesdeRechazo)
        Me.GroupBox2.Controls.Add(Me.chkFechaCheque)
        Me.GroupBox2.Controls.Add(Me.txtHastaVencimiento)
        Me.GroupBox2.Controls.Add(Me.chkFechaVencimiento)
        Me.GroupBox2.Controls.Add(Me.txtDesdeVencimiento)
        Me.GroupBox2.Controls.Add(Me.cbxAgrupado)
        Me.GroupBox2.Controls.Add(Me.txtFechaCobranzaHasta)
        Me.GroupBox2.Controls.Add(Me.chkFechaCobranza)
        Me.GroupBox2.Controls.Add(Me.txtFechaCobranzaDesde)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxCondicion)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.chkAgrupado)
        Me.GroupBox2.Location = New System.Drawing.Point(361, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(177, 334)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtHastaVencimiento
        '
        Me.txtHastaVencimiento.AñoFecha = 0
        Me.txtHastaVencimiento.Color = System.Drawing.Color.Empty
        Me.txtHastaVencimiento.Enabled = False
        Me.txtHastaVencimiento.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtHastaVencimiento.Location = New System.Drawing.Point(92, 135)
        Me.txtHastaVencimiento.MesFecha = 0
        Me.txtHastaVencimiento.Name = "txtHastaVencimiento"
        Me.txtHastaVencimiento.PermitirNulo = False
        Me.txtHastaVencimiento.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaVencimiento.SoloLectura = False
        Me.txtHastaVencimiento.TabIndex = 17
        '
        'chkFechaVencimiento
        '
        Me.chkFechaVencimiento.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaVencimiento.Color = System.Drawing.Color.Empty
        Me.chkFechaVencimiento.Location = New System.Drawing.Point(6, 112)
        Me.chkFechaVencimiento.Name = "chkFechaVencimiento"
        Me.chkFechaVencimiento.Size = New System.Drawing.Size(153, 21)
        Me.chkFechaVencimiento.SoloLectura = False
        Me.chkFechaVencimiento.TabIndex = 15
        Me.chkFechaVencimiento.Texto = "Fecha de Vencimiento:"
        Me.chkFechaVencimiento.Valor = False
        '
        'txtDesdeVencimiento
        '
        Me.txtDesdeVencimiento.AñoFecha = 0
        Me.txtDesdeVencimiento.Color = System.Drawing.Color.Empty
        Me.txtDesdeVencimiento.Enabled = False
        Me.txtDesdeVencimiento.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtDesdeVencimiento.Location = New System.Drawing.Point(9, 135)
        Me.txtDesdeVencimiento.MesFecha = 0
        Me.txtDesdeVencimiento.Name = "txtDesdeVencimiento"
        Me.txtDesdeVencimiento.PermitirNulo = False
        Me.txtDesdeVencimiento.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeVencimiento.SoloLectura = False
        Me.txtDesdeVencimiento.TabIndex = 16
        '
        'cbxAgrupado
        '
        Me.cbxAgrupado.CampoWhere = Nothing
        Me.cbxAgrupado.CargarUnaSolaVez = False
        Me.cbxAgrupado.DataDisplayMember = Nothing
        Me.cbxAgrupado.DataFilter = Nothing
        Me.cbxAgrupado.DataOrderBy = Nothing
        Me.cbxAgrupado.DataSource = Nothing
        Me.cbxAgrupado.DataValueMember = Nothing
        Me.cbxAgrupado.dtSeleccionado = Nothing
        Me.cbxAgrupado.Enabled = False
        Me.cbxAgrupado.FormABM = Nothing
        Me.cbxAgrupado.Indicaciones = Nothing
        Me.cbxAgrupado.Location = New System.Drawing.Point(6, 267)
        Me.cbxAgrupado.Name = "cbxAgrupado"
        Me.cbxAgrupado.SeleccionMultiple = False
        Me.cbxAgrupado.SeleccionObligatoria = False
        Me.cbxAgrupado.Size = New System.Drawing.Size(157, 21)
        Me.cbxAgrupado.SoloLectura = False
        Me.cbxAgrupado.TabIndex = 9
        Me.cbxAgrupado.Texto = ""
        '
        'txtFechaCobranzaHasta
        '
        Me.txtFechaCobranzaHasta.AñoFecha = 0
        Me.txtFechaCobranzaHasta.Color = System.Drawing.Color.Empty
        Me.txtFechaCobranzaHasta.Enabled = False
        Me.txtFechaCobranzaHasta.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaCobranzaHasta.Location = New System.Drawing.Point(92, 85)
        Me.txtFechaCobranzaHasta.MesFecha = 0
        Me.txtFechaCobranzaHasta.Name = "txtFechaCobranzaHasta"
        Me.txtFechaCobranzaHasta.PermitirNulo = False
        Me.txtFechaCobranzaHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaCobranzaHasta.SoloLectura = False
        Me.txtFechaCobranzaHasta.TabIndex = 5
        '
        'chkFechaCobranza
        '
        Me.chkFechaCobranza.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaCobranza.Color = System.Drawing.Color.Empty
        Me.chkFechaCobranza.Location = New System.Drawing.Point(6, 63)
        Me.chkFechaCobranza.Name = "chkFechaCobranza"
        Me.chkFechaCobranza.Size = New System.Drawing.Size(106, 21)
        Me.chkFechaCobranza.SoloLectura = False
        Me.chkFechaCobranza.TabIndex = 3
        Me.chkFechaCobranza.Texto = "Fecha Cobranza:"
        Me.chkFechaCobranza.Valor = False
        '
        'txtFechaCobranzaDesde
        '
        Me.txtFechaCobranzaDesde.AñoFecha = 0
        Me.txtFechaCobranzaDesde.Color = System.Drawing.Color.Empty
        Me.txtFechaCobranzaDesde.Enabled = False
        Me.txtFechaCobranzaDesde.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtFechaCobranzaDesde.Location = New System.Drawing.Point(9, 85)
        Me.txtFechaCobranzaDesde.MesFecha = 0
        Me.txtFechaCobranzaDesde.Name = "txtFechaCobranzaDesde"
        Me.txtFechaCobranzaDesde.PermitirNulo = False
        Me.txtFechaCobranzaDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtFechaCobranzaDesde.SoloLectura = False
        Me.txtFechaCobranzaDesde.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 206)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Condición:"
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(6, 222)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = False
        Me.cbxCondicion.Size = New System.Drawing.Size(157, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 7
        Me.cbxCondicion.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(113, 307)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(49, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 12
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 307)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(106, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 11
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtHasta.Location = New System.Drawing.Point(92, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Enabled = False
        Me.txtDesde.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtDesde.Location = New System.Drawing.Point(9, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkAgrupado
        '
        Me.chkAgrupado.BackColor = System.Drawing.Color.Transparent
        Me.chkAgrupado.Color = System.Drawing.Color.Empty
        Me.chkAgrupado.Location = New System.Drawing.Point(6, 246)
        Me.chkAgrupado.Name = "chkAgrupado"
        Me.chkAgrupado.Size = New System.Drawing.Size(106, 17)
        Me.chkAgrupado.SoloLectura = False
        Me.chkAgrupado.TabIndex = 8
        Me.chkAgrupado.Texto = "Agrupar por:"
        Me.chkAgrupado.Valor = False
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkFormatoPlanillaParaDescuento)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkEstado)
        Me.gbxFiltro.Controls.Add(Me.cbxEstado)
        Me.gbxFiltro.Controls.Add(Me.chkTipo)
        Me.gbxFiltro.Controls.Add(Me.cbxtipo)
        Me.gbxFiltro.Controls.Add(Me.chkBanco)
        Me.gbxFiltro.Controls.Add(Me.cbxBanco)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(7, 7)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(348, 334)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkFormatoPlanillaParaDescuento
        '
        Me.chkFormatoPlanillaParaDescuento.BackColor = System.Drawing.Color.Transparent
        Me.chkFormatoPlanillaParaDescuento.Color = System.Drawing.Color.Empty
        Me.chkFormatoPlanillaParaDescuento.Enabled = False
        Me.chkFormatoPlanillaParaDescuento.Location = New System.Drawing.Point(10, 291)
        Me.chkFormatoPlanillaParaDescuento.Name = "chkFormatoPlanillaParaDescuento"
        Me.chkFormatoPlanillaParaDescuento.Size = New System.Drawing.Size(185, 21)
        Me.chkFormatoPlanillaParaDescuento.SoloLectura = False
        Me.chkFormatoPlanillaParaDescuento.TabIndex = 12
        Me.chkFormatoPlanillaParaDescuento.Texto = "Formato Planilla para Descuento"
        Me.chkFormatoPlanillaParaDescuento.Valor = False
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(10, 263)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(86, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 10
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(129, 263)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(108, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 11
        Me.cbxMoneda.Texto = ""
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 82
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(6, 208)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(335, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 9
        '
        'chkEstado
        '
        Me.chkEstado.BackColor = System.Drawing.Color.Transparent
        Me.chkEstado.Color = System.Drawing.Color.Empty
        Me.chkEstado.Location = New System.Drawing.Point(9, 139)
        Me.chkEstado.Name = "chkEstado"
        Me.chkEstado.Size = New System.Drawing.Size(66, 21)
        Me.chkEstado.SoloLectura = False
        Me.chkEstado.TabIndex = 6
        Me.chkEstado.Texto = "Estado:"
        Me.chkEstado.Valor = False
        '
        'cbxEstado
        '
        Me.cbxEstado.CampoWhere = "IDEstado"
        Me.cbxEstado.CargarUnaSolaVez = False
        Me.cbxEstado.DataDisplayMember = "Estado"
        Me.cbxEstado.DataFilter = Nothing
        Me.cbxEstado.DataOrderBy = ""
        Me.cbxEstado.DataSource = ""
        Me.cbxEstado.DataValueMember = ""
        Me.cbxEstado.dtSeleccionado = Nothing
        Me.cbxEstado.Enabled = False
        Me.cbxEstado.FormABM = Nothing
        Me.cbxEstado.Indicaciones = Nothing
        Me.cbxEstado.Location = New System.Drawing.Point(129, 139)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.SeleccionMultiple = False
        Me.cbxEstado.SeleccionObligatoria = False
        Me.cbxEstado.Size = New System.Drawing.Size(213, 21)
        Me.cbxEstado.SoloLectura = False
        Me.cbxEstado.TabIndex = 7
        Me.cbxEstado.Texto = "DEPOSITADO"
        '
        'chkTipo
        '
        Me.chkTipo.BackColor = System.Drawing.Color.Transparent
        Me.chkTipo.Color = System.Drawing.Color.Empty
        Me.chkTipo.Location = New System.Drawing.Point(9, 112)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(52, 21)
        Me.chkTipo.SoloLectura = False
        Me.chkTipo.TabIndex = 4
        Me.chkTipo.Texto = "Tipo:"
        Me.chkTipo.Valor = False
        '
        'cbxtipo
        '
        Me.cbxtipo.CampoWhere = ""
        Me.cbxtipo.CargarUnaSolaVez = False
        Me.cbxtipo.DataDisplayMember = "Tipo"
        Me.cbxtipo.DataFilter = Nothing
        Me.cbxtipo.DataOrderBy = Nothing
        Me.cbxtipo.DataSource = ""
        Me.cbxtipo.DataValueMember = ""
        Me.cbxtipo.dtSeleccionado = Nothing
        Me.cbxtipo.Enabled = False
        Me.cbxtipo.FormABM = Nothing
        Me.cbxtipo.Indicaciones = Nothing
        Me.cbxtipo.Location = New System.Drawing.Point(129, 112)
        Me.cbxtipo.Name = "cbxtipo"
        Me.cbxtipo.SeleccionMultiple = False
        Me.cbxtipo.SeleccionObligatoria = False
        Me.cbxtipo.Size = New System.Drawing.Size(213, 21)
        Me.cbxtipo.SoloLectura = False
        Me.cbxtipo.TabIndex = 5
        Me.cbxtipo.Texto = "AL DIA"
        '
        'chkBanco
        '
        Me.chkBanco.BackColor = System.Drawing.Color.Transparent
        Me.chkBanco.Color = System.Drawing.Color.Empty
        Me.chkBanco.Location = New System.Drawing.Point(8, 65)
        Me.chkBanco.Name = "chkBanco"
        Me.chkBanco.Size = New System.Drawing.Size(66, 21)
        Me.chkBanco.SoloLectura = False
        Me.chkBanco.TabIndex = 2
        Me.chkBanco.Texto = "Banco:"
        Me.chkBanco.Valor = False
        '
        'cbxBanco
        '
        Me.cbxBanco.CampoWhere = "IDBanco"
        Me.cbxBanco.CargarUnaSolaVez = False
        Me.cbxBanco.DataDisplayMember = "Descripcion"
        Me.cbxBanco.DataFilter = Nothing
        Me.cbxBanco.DataOrderBy = Nothing
        Me.cbxBanco.DataSource = "VBanco"
        Me.cbxBanco.DataValueMember = "ID"
        Me.cbxBanco.dtSeleccionado = Nothing
        Me.cbxBanco.Enabled = False
        Me.cbxBanco.FormABM = Nothing
        Me.cbxBanco.Indicaciones = Nothing
        Me.cbxBanco.Location = New System.Drawing.Point(128, 65)
        Me.cbxBanco.Name = "cbxBanco"
        Me.cbxBanco.SeleccionMultiple = False
        Me.cbxBanco.SeleccionObligatoria = False
        Me.cbxBanco.Size = New System.Drawing.Size(213, 21)
        Me.cbxBanco.SoloLectura = False
        Me.cbxBanco.TabIndex = 3
        Me.cbxBanco.Texto = "ITAU"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(10, 184)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(65, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 8
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(8, 40)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(73, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 0
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(128, 40)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 1
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(461, 347)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'chkFechaCheque
        '
        Me.chkFechaCheque.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaCheque.Color = System.Drawing.Color.Empty
        Me.chkFechaCheque.Location = New System.Drawing.Point(6, 14)
        Me.chkFechaCheque.Name = "chkFechaCheque"
        Me.chkFechaCheque.Size = New System.Drawing.Size(136, 21)
        Me.chkFechaCheque.SoloLectura = False
        Me.chkFechaCheque.TabIndex = 18
        Me.chkFechaCheque.Texto = "Fecha Cheque"
        Me.chkFechaCheque.Valor = False
        '
        'chkFechaRechazo
        '
        Me.chkFechaRechazo.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaRechazo.Color = System.Drawing.Color.Empty
        Me.chkFechaRechazo.Location = New System.Drawing.Point(6, 159)
        Me.chkFechaRechazo.Name = "chkFechaRechazo"
        Me.chkFechaRechazo.Size = New System.Drawing.Size(136, 21)
        Me.chkFechaRechazo.SoloLectura = False
        Me.chkFechaRechazo.TabIndex = 21
        Me.chkFechaRechazo.Texto = "Fecha Rechazo"
        Me.chkFechaRechazo.Valor = False
        '
        'txtHastaRechazo
        '
        Me.txtHastaRechazo.AñoFecha = 0
        Me.txtHastaRechazo.Color = System.Drawing.Color.Empty
        Me.txtHastaRechazo.Enabled = False
        Me.txtHastaRechazo.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtHastaRechazo.Location = New System.Drawing.Point(92, 183)
        Me.txtHastaRechazo.MesFecha = 0
        Me.txtHastaRechazo.Name = "txtHastaRechazo"
        Me.txtHastaRechazo.PermitirNulo = False
        Me.txtHastaRechazo.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaRechazo.SoloLectura = False
        Me.txtHastaRechazo.TabIndex = 20
        '
        'txtDesdeRechazo
        '
        Me.txtDesdeRechazo.AñoFecha = 0
        Me.txtDesdeRechazo.Color = System.Drawing.Color.Empty
        Me.txtDesdeRechazo.Enabled = False
        Me.txtDesdeRechazo.Fecha = New Date(2013, 1, 30, 10, 50, 32, 62)
        Me.txtDesdeRechazo.Location = New System.Drawing.Point(9, 183)
        Me.txtDesdeRechazo.MesFecha = 0
        Me.txtDesdeRechazo.Name = "txtDesdeRechazo"
        Me.txtDesdeRechazo.PermitirNulo = False
        Me.txtDesdeRechazo.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeRechazo.SoloLectura = False
        Me.txtDesdeRechazo.TabIndex = 19
        '
        'frmListadoCheque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(557, 382)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoCheque"
        Me.Text = "frmListadoCheque"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkEstado As ERP.ocxCHK
    Friend WithEvents cbxEstado As ERP.ocxCBX
    Friend WithEvents chkTipo As ERP.ocxCHK
    Friend WithEvents cbxtipo As ERP.ocxCBX
    Friend WithEvents chkBanco As ERP.ocxCHK
    Friend WithEvents cbxBanco As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents txtFechaCobranzaDesde As ERP.ocxTXTDate
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents chkFechaCobranza As ERP.ocxCHK
    Friend WithEvents txtFechaCobranzaHasta As ERP.ocxTXTDate
    Friend WithEvents cbxAgrupado As ERP.ocxCBX
    Friend WithEvents chkAgrupado As ERP.ocxCHK
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents txtHastaVencimiento As ERP.ocxTXTDate
    Friend WithEvents chkFechaVencimiento As ERP.ocxCHK
    Friend WithEvents txtDesdeVencimiento As ERP.ocxTXTDate
    Friend WithEvents chkFormatoPlanillaParaDescuento As ERP.ocxCHK
    Friend WithEvents chkFechaRechazo As ocxCHK
    Friend WithEvents txtHastaRechazo As ocxTXTDate
    Friend WithEvents txtDesdeRechazo As ocxTXTDate
    Friend WithEvents chkFechaCheque As ocxCHK
End Class

﻿Imports ERP.Reporte
Public Class frmListadoChequesRechazados
    'CLASES
    Dim CReporte As New CReporteCheque
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES

    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList


    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Tipo As String = ""
        Dim Estado As String = ""
        Dim SubTitulo As String = ""
        Dim Titulo As String = "LISTADO DE OPERACIONES DE RECHAZO DE CHEQUES"


        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Anulados
        If chkAnulados.Valor = True Then
            Where = Where & " And Anulado='True' "
            Titulo = Titulo & " - OPERACIONES ANULADAS"
        Else
            Where = Where & " And Anulado='False' "
        End If

        'Anulados
        If chkSoloConSaldo.Valor = True Then
            Where = Where & " And SaldoACuenta > 0 "
            Titulo = Titulo & " CON SALDO"
        Else
            Titulo = "LISTADO HISTORICO DE OPERACIONES DE RECHAZO DE CHEQUES"
        End If

        'Estado Cheque 20/05/2022 - Cambios solicitados
        If chkEstado.Valor = True Then
            Where = Where & " And Estado= '" & cbxEstadoCheque.Text & "'"

        End If


        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        SubTitulo = "Del " & txtDesde.txt.Text & " al " & txtHasta.txt.Text
        CReporte.ArmarSubTitulo(SubTitulo, gbxFiltro)

        cbxCliente.EstablecerCondicion(WhereDetalle)
        CReporte.ImprimirListadoChequesRechazados(frm, Where, WhereDetalle, vgUsuarioIdentificador, OrderBy, Top, SubTitulo, Titulo)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("vChequeClienteRechazado", "Select Top(0) Sucursal , Cliente , Fecha, Importe  From vChequeClienteRechazado  ")



        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    'Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
    '    cbxCiudad.Enabled = value
    'End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmListadoChequesRechazados_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkEstado_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkEstado.PropertyChanged
        cbxEstadoCheque.Enabled = value
    End Sub
End Class




﻿
Imports ERP.Reporte
Public Class frmSaldoChequesDiferidos

    'CLASES
    Dim CReporte As New CReporteCheque
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'VARIABLES
    Dim Titulo As String = "LISTADO DE CHEQUES CLIENTES"
    Dim TipoInforme As String
    Dim ID As Integer

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()



    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        ' cargar cbx mes
        cbxMes.cbx.Items.Add("ENERO")
        cbxMes.cbx.Items.Add("FEBRERO")
        cbxMes.cbx.Items.Add("MARZO")
        cbxMes.cbx.Items.Add("ABRIL")
        cbxMes.cbx.Items.Add("MAYO")
        cbxMes.cbx.Items.Add("JUNIO")
        cbxMes.cbx.Items.Add("JULIO")
        cbxMes.cbx.Items.Add("AGOSTO")
        cbxMes.cbx.Items.Add("SEPTIEMBRE")
        cbxMes.cbx.Items.Add("OCTUBRE")
        cbxMes.cbx.Items.Add("NOVIEMBRE")
        cbxMes.cbx.Items.Add("DICIEMBRE")
        cbxMes.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Condición
        cbxCondicion.cbx.Items.Add("Contado")
        cbxCondicion.cbx.Items.Add("Crédito")
        cbxCondicion.cbx.SelectedIndex = 0

        'cargar cbx año
        Dim intAño As Integer = Year(Now())
        Dim i As Integer
        For i = intAño To 2000 Step -1

            cbxAño.cbx.Items.Add(i)
        Next
        cbxAño.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxMes.cbx.Focus()

        'CARGAR LA ULTIMA CONFIGURACION
        'Mes
        cbxMes.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MES", "")
        'Año
        cbxAño.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "AÑO", "")

        cbxAño.cbx.Text = Date.Now.Year

        cbxMes.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim Top As String = ""
        Dim Tipo As String = ""
        Dim Estado As String = ""
        Dim Condicion As String = ""
        Dim ContCred As String = ""
        Dim Sucursal As String = ""
        Dim Titulo As String = ""
        Dim TipoInforme As String = ""
        Dim Ordenado As String = ""
        Dim ASC As Boolean = False

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = cbxMes.cbx.SelectedIndex + 1 & "," & cbxAño.cbx.Text

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            Ordenado = cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            If cbxEnForma.cbx.SelectedIndex = 0 Then
                ASC = True
            Else
                ASC = False
            End If
        End If

        Select Case cbxCondicion.cbx.SelectedIndex
            Case Is = 0
                WhereDetalle = "Condicion='False'"
                Titulo = "SALDO DE CHEQUES DIFERIDOS"
                If chkSucursal.Valor = True Then
                    WhereDetalle = WhereDetalle & " And IDSucursal=" & cbxSucursal.GetValue
                    TipoInforme = "COBRANZA CONTADO (" & cbxSucursal.cbx.Text & ")"
                Else
                    TipoInforme = "COBRANZA CONTADO "
                End If

            Case Is = 1
                WhereDetalle = "Condicion='True'"
                Titulo = "SALDO DE CHEQUES DIFERIDOS"
                If chkSucursal.Valor = True Then
                    WhereDetalle = WhereDetalle & " And IDSucursal=" & cbxSucursal.GetValue
                    TipoInforme = "COBRANZA CREDITO (" & cbxSucursal.cbx.Text & ")"
                Else
                    TipoInforme = "COBRANZA CREDITO "
                End If
        End Select


        CReporte.SaldoChequesDiferidos(frm, Where, WhereDetalle, vgUsuarioIdentificador, Titulo, TipoInforme, Ordenado, ASC)



    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VSaldoChequeDiferidos", "Select Top(0)  Fecha, 'Ingresos'=Ingreso,'Depositos'= Deposito, DescuentoCheque, Saldo From VSaldoChequesDiferidos ")



        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmSaldoChequesDiferidos_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'GuardarInformacion()
    End Sub

    Private Sub frmSaldoChequesDiferidos_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoCheque_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCondicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)

    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)

    End Sub

    Private Sub chkSucursal_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCondicion_PropertyChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxCondicion.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class





﻿Imports ERP.Reporte
Public Class frmRetencionEmitida
    'CLASES
    Dim CReporte As New CReporteCobranza
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = ""
    Dim Subtitulo As String = ""
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Funciones
        CargarInformacion()
        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()
        chkImprimirCabecera.Valor = True
    End Sub

    Sub CargarInformacion()
        'Orden
        cbxOrdenadoPor.cbx.Items.Add("Fecha")
        cbxOrdenadoPor.cbx.Items.Add("Proveedor")
        cbxOrdenadoPor.cbx.Items.Add("Comprobante")
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.SelectedIndex = 0

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        'Proveedor
        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono From VProveedor "
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        If chkProveedor.chk.Checked = True Then
            If txtProveedor.Seleccionado = False Then
                MessageBox.Show("Seleccione correctamente un proveedor!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            Where = Where & " And IDProveedor = " & txtProveedor.Registro("ID") & "  "
        End If

        If chkSucursal.chk.Checked = True Then
            Where = Where & " And IDSucursal = " & cbxSucursal.cbx.SelectedValue & " "
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If



        'Titulo
        Titulo = "LISTADO DE RETENCIONES EMITIDAS"
        Subtitulo = "Fecha desde: " & txtDesde.txt.Text & " hasta " & txtHasta.txt.Text & ""
        CReporte.ListadoRetencionEmitida(frm, Titulo, Subtitulo, Where, OrderBy, chkImprimirCabecera.Valor)

    End Sub

    Private Sub chkProveedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmRetencionEmitida_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub
End Class
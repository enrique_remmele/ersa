﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDistribuciondeSaldoDocumentoaPagar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkDocumentos = New ERP.ocxCHK()
        Me.cbxDocumentos = New ERP.ocxCBX()
        Me.chkTipoProveedor = New ERP.ocxCHK()
        Me.cbxTipoProveedor = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtReferencia = New ERP.ocxTXTDate()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.lblCondicion = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtDia2 = New ERP.ocxTXTNumeric()
        Me.txtDia3 = New ERP.ocxTXTNumeric()
        Me.txtDia4 = New ERP.ocxTXTNumeric()
        Me.txtDia6 = New ERP.ocxTXTNumeric()
        Me.txtDia5 = New ERP.ocxTXTNumeric()
        Me.txtDia1 = New ERP.ocxTXTNumeric()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFecha4 = New ERP.ocxTXTDate()
        Me.txtFecha3 = New ERP.ocxTXTDate()
        Me.txtFecha1 = New ERP.ocxTXTDate()
        Me.txtFecha2 = New ERP.ocxTXTDate()
        Me.txtFecha5 = New ERP.ocxTXTDate()
        Me.txtFecha6 = New ERP.ocxTXTDate()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkDocumentos)
        Me.gbxFiltro.Controls.Add(Me.cbxDocumentos)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Location = New System.Drawing.Point(10, 5)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(331, 196)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkDocumentos
        '
        Me.chkDocumentos.BackColor = System.Drawing.Color.Transparent
        Me.chkDocumentos.Color = System.Drawing.Color.Empty
        Me.chkDocumentos.Location = New System.Drawing.Point(9, 24)
        Me.chkDocumentos.Name = "chkDocumentos"
        Me.chkDocumentos.Size = New System.Drawing.Size(94, 21)
        Me.chkDocumentos.SoloLectura = False
        Me.chkDocumentos.TabIndex = 0
        Me.chkDocumentos.Texto = "Documentos:"
        Me.chkDocumentos.Valor = False
        '
        'cbxDocumentos
        '
        Me.cbxDocumentos.CampoWhere = ""
        Me.cbxDocumentos.CargarUnaSolaVez = False
        Me.cbxDocumentos.DataDisplayMember = ""
        Me.cbxDocumentos.DataFilter = Nothing
        Me.cbxDocumentos.DataOrderBy = ""
        Me.cbxDocumentos.DataSource = ""
        Me.cbxDocumentos.DataValueMember = ""
        Me.cbxDocumentos.Enabled = False
        Me.cbxDocumentos.FormABM = "frmVendedor"
        Me.cbxDocumentos.Indicaciones = "Seleccion de Vendedor"
        Me.cbxDocumentos.Location = New System.Drawing.Point(110, 24)
        Me.cbxDocumentos.Name = "cbxDocumentos"
        Me.cbxDocumentos.SeleccionObligatoria = True
        Me.cbxDocumentos.Size = New System.Drawing.Size(213, 21)
        Me.cbxDocumentos.SoloLectura = False
        Me.cbxDocumentos.TabIndex = 1
        Me.cbxDocumentos.Texto = ""
        '
        'chkTipoProveedor
        '
        Me.chkTipoProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProveedor.Color = System.Drawing.Color.Empty
        Me.chkTipoProveedor.Location = New System.Drawing.Point(9, 47)
        Me.chkTipoProveedor.Name = "chkTipoProveedor"
        Me.chkTipoProveedor.Size = New System.Drawing.Size(98, 21)
        Me.chkTipoProveedor.SoloLectura = False
        Me.chkTipoProveedor.TabIndex = 2
        Me.chkTipoProveedor.Texto = "Tipo Proveedor:"
        Me.chkTipoProveedor.Valor = False
        '
        'cbxTipoProveedor
        '
        Me.cbxTipoProveedor.CampoWhere = ""
        Me.cbxTipoProveedor.CargarUnaSolaVez = False
        Me.cbxTipoProveedor.DataDisplayMember = ""
        Me.cbxTipoProveedor.DataFilter = Nothing
        Me.cbxTipoProveedor.DataOrderBy = ""
        Me.cbxTipoProveedor.DataSource = ""
        Me.cbxTipoProveedor.DataValueMember = ""
        Me.cbxTipoProveedor.Enabled = False
        Me.cbxTipoProveedor.FormABM = Nothing
        Me.cbxTipoProveedor.Indicaciones = Nothing
        Me.cbxTipoProveedor.Location = New System.Drawing.Point(110, 47)
        Me.cbxTipoProveedor.Name = "cbxTipoProveedor"
        Me.cbxTipoProveedor.SeleccionObligatoria = True
        Me.cbxTipoProveedor.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoProveedor.SoloLectura = False
        Me.cbxTipoProveedor.TabIndex = 3
        Me.cbxTipoProveedor.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 70)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(69, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(110, 70)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = True
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.Texto = ""
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(9, 93)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(69, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 6
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "Suc-Dep"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "Suc-Dep"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(110, 93)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionObligatoria = True
        Me.cbxDeposito.Size = New System.Drawing.Size(213, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 7
        Me.cbxDeposito.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(654, 207)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 4
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxCondicion)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxMoneda)
        Me.GroupBox2.Controls.Add(Me.txtReferencia)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.lblCondicion)
        Me.GroupBox2.Location = New System.Drawing.Point(346, 5)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(219, 196)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = Nothing
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = Nothing
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = Nothing
        Me.cbxCondicion.DataSource = Nothing
        Me.cbxCondicion.DataValueMember = Nothing
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(17, 34)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionObligatoria = True
        Me.cbxCondicion.Size = New System.Drawing.Size(192, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 1
        Me.cbxCondicion.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 147)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Moneda:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 62)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Referencia:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "Moneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(17, 162)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = True
        Me.cbxMoneda.Size = New System.Drawing.Size(192, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 8
        Me.cbxMoneda.Texto = ""
        '
        'txtReferencia
        '
        Me.txtReferencia.Color = System.Drawing.Color.Empty
        Me.txtReferencia.Fecha = New Date(2013, 7, 2, 11, 17, 59, 946)
        Me.txtReferencia.Location = New System.Drawing.Point(17, 78)
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.PermitirNulo = False
        Me.txtReferencia.Size = New System.Drawing.Size(74, 20)
        Me.txtReferencia.SoloLectura = False
        Me.txtReferencia.TabIndex = 3
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(153, 121)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = True
        Me.cbxEnForma.Size = New System.Drawing.Size(56, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 6
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(17, 121)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = True
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(136, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 5
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(17, 105)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 4
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'lblCondicion
        '
        Me.lblCondicion.AutoSize = True
        Me.lblCondicion.Location = New System.Drawing.Point(17, 18)
        Me.lblCondicion.Name = "lblCondicion"
        Me.lblCondicion.Size = New System.Drawing.Size(57, 13)
        Me.lblCondicion.TabIndex = 0
        Me.lblCondicion.Text = "Condicion:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(346, 207)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 3
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDia2)
        Me.GroupBox1.Controls.Add(Me.txtDia3)
        Me.GroupBox1.Controls.Add(Me.txtDia4)
        Me.GroupBox1.Controls.Add(Me.txtDia6)
        Me.GroupBox1.Controls.Add(Me.txtDia5)
        Me.GroupBox1.Controls.Add(Me.txtDia1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtFecha4)
        Me.GroupBox1.Controls.Add(Me.txtFecha3)
        Me.GroupBox1.Controls.Add(Me.txtFecha1)
        Me.GroupBox1.Controls.Add(Me.txtFecha2)
        Me.GroupBox1.Controls.Add(Me.txtFecha5)
        Me.GroupBox1.Controls.Add(Me.txtFecha6)
        Me.GroupBox1.Location = New System.Drawing.Point(571, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(146, 196)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'txtDia2
        '
        Me.txtDia2.Color = System.Drawing.Color.Empty
        Me.txtDia2.Decimales = True
        Me.txtDia2.Indicaciones = Nothing
        Me.txtDia2.Location = New System.Drawing.Point(15, 59)
        Me.txtDia2.Name = "txtDia2"
        Me.txtDia2.Size = New System.Drawing.Size(40, 20)
        Me.txtDia2.SoloLectura = False
        Me.txtDia2.TabIndex = 3
        Me.txtDia2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia2.Texto = "0"
        '
        'txtDia3
        '
        Me.txtDia3.Color = System.Drawing.Color.Empty
        Me.txtDia3.Decimales = True
        Me.txtDia3.Indicaciones = Nothing
        Me.txtDia3.Location = New System.Drawing.Point(15, 80)
        Me.txtDia3.Name = "txtDia3"
        Me.txtDia3.Size = New System.Drawing.Size(40, 20)
        Me.txtDia3.SoloLectura = False
        Me.txtDia3.TabIndex = 5
        Me.txtDia3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia3.Texto = "0"
        '
        'txtDia4
        '
        Me.txtDia4.Color = System.Drawing.Color.Empty
        Me.txtDia4.Decimales = True
        Me.txtDia4.Indicaciones = Nothing
        Me.txtDia4.Location = New System.Drawing.Point(15, 101)
        Me.txtDia4.Name = "txtDia4"
        Me.txtDia4.Size = New System.Drawing.Size(40, 20)
        Me.txtDia4.SoloLectura = False
        Me.txtDia4.TabIndex = 7
        Me.txtDia4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia4.Texto = "0"
        '
        'txtDia6
        '
        Me.txtDia6.Color = System.Drawing.Color.Empty
        Me.txtDia6.Decimales = True
        Me.txtDia6.Indicaciones = Nothing
        Me.txtDia6.Location = New System.Drawing.Point(15, 143)
        Me.txtDia6.Name = "txtDia6"
        Me.txtDia6.Size = New System.Drawing.Size(40, 20)
        Me.txtDia6.SoloLectura = False
        Me.txtDia6.TabIndex = 11
        Me.txtDia6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia6.Texto = "0"
        '
        'txtDia5
        '
        Me.txtDia5.Color = System.Drawing.Color.Empty
        Me.txtDia5.Decimales = True
        Me.txtDia5.Indicaciones = Nothing
        Me.txtDia5.Location = New System.Drawing.Point(15, 122)
        Me.txtDia5.Name = "txtDia5"
        Me.txtDia5.Size = New System.Drawing.Size(40, 20)
        Me.txtDia5.SoloLectura = False
        Me.txtDia5.TabIndex = 9
        Me.txtDia5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia5.Texto = "0"
        '
        'txtDia1
        '
        Me.txtDia1.Color = System.Drawing.Color.Empty
        Me.txtDia1.Decimales = True
        Me.txtDia1.Indicaciones = Nothing
        Me.txtDia1.Location = New System.Drawing.Point(15, 38)
        Me.txtDia1.Name = "txtDia1"
        Me.txtDia1.Size = New System.Drawing.Size(40, 20)
        Me.txtDia1.SoloLectura = False
        Me.txtDia1.TabIndex = 1
        Me.txtDia1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDia1.Texto = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Plazos (Diaz/Fecha)"
        '
        'txtFecha4
        '
        Me.txtFecha4.Color = System.Drawing.Color.Empty
        Me.txtFecha4.Enabled = False
        Me.txtFecha4.Fecha = New Date(2013, 7, 2, 11, 17, 59, 946)
        Me.txtFecha4.Location = New System.Drawing.Point(57, 101)
        Me.txtFecha4.Name = "txtFecha4"
        Me.txtFecha4.PermitirNulo = False
        Me.txtFecha4.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha4.SoloLectura = True
        Me.txtFecha4.TabIndex = 8
        Me.txtFecha4.TabStop = False
        '
        'txtFecha3
        '
        Me.txtFecha3.Color = System.Drawing.Color.Empty
        Me.txtFecha3.Enabled = False
        Me.txtFecha3.Fecha = New Date(2013, 7, 2, 11, 17, 59, 946)
        Me.txtFecha3.Location = New System.Drawing.Point(57, 80)
        Me.txtFecha3.Name = "txtFecha3"
        Me.txtFecha3.PermitirNulo = False
        Me.txtFecha3.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha3.SoloLectura = True
        Me.txtFecha3.TabIndex = 6
        Me.txtFecha3.TabStop = False
        '
        'txtFecha1
        '
        Me.txtFecha1.Color = System.Drawing.Color.Empty
        Me.txtFecha1.Fecha = New Date(2013, 7, 2, 11, 17, 59, 946)
        Me.txtFecha1.Location = New System.Drawing.Point(57, 38)
        Me.txtFecha1.Name = "txtFecha1"
        Me.txtFecha1.PermitirNulo = False
        Me.txtFecha1.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha1.SoloLectura = True
        Me.txtFecha1.TabIndex = 2
        Me.txtFecha1.TabStop = False
        '
        'txtFecha2
        '
        Me.txtFecha2.Color = System.Drawing.Color.Empty
        Me.txtFecha2.Enabled = False
        Me.txtFecha2.Fecha = New Date(2013, 7, 2, 11, 17, 59, 946)
        Me.txtFecha2.Location = New System.Drawing.Point(57, 59)
        Me.txtFecha2.Name = "txtFecha2"
        Me.txtFecha2.PermitirNulo = False
        Me.txtFecha2.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha2.SoloLectura = True
        Me.txtFecha2.TabIndex = 4
        Me.txtFecha2.TabStop = False
        '
        'txtFecha5
        '
        Me.txtFecha5.Color = System.Drawing.Color.Empty
        Me.txtFecha5.Enabled = False
        Me.txtFecha5.Fecha = New Date(2013, 7, 2, 11, 17, 59, 946)
        Me.txtFecha5.Location = New System.Drawing.Point(57, 122)
        Me.txtFecha5.Name = "txtFecha5"
        Me.txtFecha5.PermitirNulo = False
        Me.txtFecha5.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha5.SoloLectura = True
        Me.txtFecha5.TabIndex = 10
        Me.txtFecha5.TabStop = False
        '
        'txtFecha6
        '
        Me.txtFecha6.Color = System.Drawing.Color.Empty
        Me.txtFecha6.Enabled = False
        Me.txtFecha6.Fecha = New Date(2013, 7, 2, 11, 17, 59, 946)
        Me.txtFecha6.Location = New System.Drawing.Point(57, 143)
        Me.txtFecha6.Name = "txtFecha6"
        Me.txtFecha6.PermitirNulo = False
        Me.txtFecha6.Size = New System.Drawing.Size(74, 20)
        Me.txtFecha6.SoloLectura = True
        Me.txtFecha6.TabIndex = 12
        Me.txtFecha6.TabStop = False
        '
        'frmDistribuciondeSaldoDocumentoaPagar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(730, 237)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmDistribuciondeSaldoDocumentoaPagar"
        Me.Text = "DistribuciondeSaldoDocumentoaPagar"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents chkTipoProveedor As ERP.ocxCHK
    Friend WithEvents cbxTipoProveedor As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkDocumentos As ERP.ocxCHK
    Friend WithEvents cbxDocumentos As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents lblCondicion As System.Windows.Forms.Label
    Friend WithEvents txtFecha1 As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFecha4 As ERP.ocxTXTDate
    Friend WithEvents txtFecha3 As ERP.ocxTXTDate
    Friend WithEvents txtFecha2 As ERP.ocxTXTDate
    Friend WithEvents txtFecha5 As ERP.ocxTXTDate
    Friend WithEvents txtFecha6 As ERP.ocxTXTDate
    Friend WithEvents txtDia2 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia3 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia4 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia6 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia5 As ERP.ocxTXTNumeric
    Friend WithEvents txtDia1 As ERP.ocxTXTNumeric
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As ERP.ocxTXTDate
    Friend WithEvents cbxCondicion As ERP.ocxCBX
End Class

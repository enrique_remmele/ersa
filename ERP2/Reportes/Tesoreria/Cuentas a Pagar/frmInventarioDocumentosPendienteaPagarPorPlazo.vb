﻿Imports ERP.Reporte

Public Class frmInventarioDocumentosPendienteaPagarPorPlazo
    'CLASES
    Dim CReporte As New CReporteCuentasAPagar
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    'PROPIEDADES

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        HabilitarPeriodo()

        AsignarFecha()

    End Sub

    Sub CargarInformacion()


        'Proveedor
        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono From VProveedor "

        'Cuentas Contables
        txtCuentaContableDesde.Conectar()
        txtCuentaContableHasta.Conectar()

        'Contado - Credito
        cbxDocumento.cbx.Items.Add("CONTADO+CREDITO")
        cbxDocumento.cbx.Items.Add("CONTADO")
        cbxDocumento.cbx.Items.Add("CREDITO")
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.Text = "CONTADO+CREDITO"

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        'Fecha Vencimiento
        cbxVencimiento.Items.Add("TODOS A HOY")
        cbxVencimiento.Items.Add("VENCIDOS A HOY")
        cbxVencimiento.Items.Add("A VENCER EN EL PERIODO")
        cbxVencimiento.DropDownStyle = ComboBoxStyle.DropDownList
        cbxVencimiento.DropDownStyle = ComboBoxStyle.DropDownList
        cbxVencimiento.SelectedIndex = 0

        'Moneda
        cbxMoneda.cbx.SelectedIndex = 0

        'Ordenado
        cbxOrdenadoPor.cbx.Items.Add("COMPROBANTE")
        cbxOrdenadoPor.cbx.Items.Add("PROVEEDOR")
        cbxOrdenadoPor.cbx.Items.Add("SUCURSAL")
        cbxOrdenadoPor.cbx.Items.Add("FECHAVENCIMIENTO")
        cbxOrdenadoPor.cbx.Items.Add("SALDO")
        cbxVencimiento.DropDownStyle = ComboBoxStyle.DropDownList
        cbxVencimiento.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.SelectedItem = 0

        'Configuracion
        cbxVencimiento.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "VENCIMIENTO", "")
        cbxOrdenadoPor.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENADO", "")
        cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")
        cbxEnForma.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ENFORMA", "")
        nudRanking.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "RANKING", "")

        cbxMoneda.txt.Clear()
        cbxMoneda.cbx.Text = ""
    End Sub

    Sub GuardarInformacion()

        'Configuracion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "VENCIMIENTO", cbxVencimiento.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENADO", cbxOrdenadoPor.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ENFORMA", cbxEnForma.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RANKING", nudRanking.Text)


    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim SubTitulo As String = ""
        Dim Titulo As String = "INVENTARIO GENERAL DE DOCUMENTO A PAGAR POR PLAZO"
        Dim TipoInforme As String = ""

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Dim dtCCFF As New DataTable
        Dim CuentasContables As String = " "
        Dim Mensaje As String = ""


        If chkCC.Valor = True Then
            If txtCuentaContableDesde.txtCodigo.Texto = "" Then
                Mensaje = "Ingrese Cuenta Contable Desde!"
                ctrError.SetError(txtCuentaContableDesde, Mensaje)
                ctrError.SetIconAlignment(txtCuentaContableDesde, ErrorIconAlignment.TopRight)
                tsslEstado.Text = Mensaje
                Exit Sub
            End If
            If txtCuentaContableHasta.txtCodigo.Texto = "" Then
                Mensaje = "Ingrese Cuenta Contable Hasta!"
                ctrError.SetError(txtCuentaContableHasta, Mensaje)
                ctrError.SetIconAlignment(txtCuentaContableHasta, ErrorIconAlignment.TopRight)
                tsslEstado.Text = Mensaje
                Exit Sub
            End If
        End If

        'Buscar las Cuentas contables
        If chkCC.Valor = True And txtCuentaContableDesde.txtCodigo.Texto <> "" And txtCuentaContableHasta.txtCodigo.Texto <> "" Then 'Si hay filtro de Cuenta Contable
            CuentasContables = " and (CuentaContable = '0' "
            dtCCFF = CSistema.ExecuteToDataTable("select Codigo from CuentaContable where codigo between '" & txtCuentaContableDesde.txtCodigo.Texto & "' and '" & txtCuentaContableHasta.txtCodigo.Texto & "' and Imputable = 1 and descripcion like '%proveedo%'").Copy
            For Each oRow As DataRow In dtCCFF.Rows
                Dim param As New DataTable
                CuentasContables = CuentasContables & " or CuentaContable ='" & oRow("Codigo").ToString & "'    "
            Next
            CuentasContables = CuentasContables & ")"
            If txtCuentaContableDesde.txtCodigo.Texto <> "" Then
                TipoInforme = TipoInforme & "CC: " & txtCuentaContableDesde.txtCodigo.Texto & " - " & txtCuentaContableHasta.txtCodigo.Texto & " - "
            End If
        End If
        'Fin de cuentas contables

        Where = " Where 1=1"
        Titulo = "INVENTARIO GENERAL DE DOCUMENTO A PAGAR POR PLAZO"
        Select Case cbxVencimiento.SelectedIndex

            Case 0
                If cbxVencimiento.Text <> "" Then
                    Where = Where & " And FechaEmision<= GETDATE() And Anulado='False' And Cancelado='False' "
                End If

            Case 1
                If cbxVencimiento.Text <> "" Then
                    Where = Where & "And FechaVencimiento<= GETDATE () And Anulado='False' And Cancelado= 'False' "
                End If

            Case 2
                If cbxVencimiento.Text <> "" Then
                    Where = Where & "And FechaVencimiento Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And Anulado='False' And Cancelado= 'False'"
                End If

        End Select

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            If cbxOrdenadoPor.cbx.SelectedIndex = 3 Then
                OrderBy = " Order by fechaVencimiento"
            Else
                OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text & ",  FechaVencimiento"
            End If
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        Select Case cbxDocumento.cbx.SelectedIndex
            Case 0
                If cbxDocumento.cbx.Text <> "" Then
                    Cond = ""
                End If
            Case 1
                If cbxDocumento.cbx.Text <> "" Then
                    Cond = " And " & " Condicion = 'Cont'"
                End If
            Case 2
                If cbxDocumento.cbx.Text <> "" Then
                    Cond = " And " & " Condicion = 'Cred'"
                End If
        End Select

        If chkProveedor.Valor = True Then
            Where = Where & " And IDProveedor = " & txtProveedor.Registro("ID").ToString
        End If

        'Para Limpiar Valor de Moneda - Sonia -04032021
        If chkMoneda.Valor = False Then
            cbxMoneda.GetValue()
            cbxMoneda.cbx.Text = ""

        End If

        CReporte.ArmarSubTitulo(SubTitulo, gbxFiltro, nudRanking, cbxMoneda, cbxOrdenadoPor, cbxEnForma, txtDesde, txtHasta)

        If chkProveedor.Valor = True Then
            SubTitulo = SubTitulo & " " & txtProveedor.Registro("Referencia").ToString & "-" & txtProveedor.Registro("RazonSocial").ToString
        End If
        If chkResumidoProveedor.Valor = False Then
            If chkCC.Valor = False Then
                If chkInventarioDetallado.Valor = False Then
                    CReporte.InventarioDocumentoPendientePagoPorPlazo(frm, Where, WhereDetalle, OrderBy, Top, Cond, vgUsuarioIdentificador, Titulo, SubTitulo)
                Else
                    CReporte.InventarioPagarOriginalGs(frm, Where, WhereDetalle, OrderBy, Top, Cond, vgUsuarioIdentificador, Titulo, SubTitulo)
                End If
            Else
                Dim vFiltroCuentaContable As String = ""
                If chkCC.Valor = True And txtCuentaContableDesde.txtCodigo.Texto <> "" Then
                    vFiltroCuentaContable = " and exists(Select * from DetalleAsiento where IDTransaccion = VInventarioDocumentosPendientesPorPlazo.IdTransaccion " & CuentasContables & ")"
                    Where = Where & vFiltroCuentaContable
                End If
                SubTitulo = SubTitulo & " CC: " & txtCuentaContableDesde.txtCodigo.Texto & "-" & txtCuentaContableHasta.txtCodigo.Texto
                CReporte.InventarioPagarOriginalGsCuentaContable(frm, Where, WhereDetalle, OrderBy, Top, Cond, vgUsuarioIdentificador, Titulo, SubTitulo)
            End If
        Else ' Resumido por proveedor
            CReporte.InventarioDocumentoPendientePagoPorPlazo(frm, Where, WhereDetalle, OrderBy, Top, Cond, vgUsuarioIdentificador, Titulo, SubTitulo, "rptInventarioDocumentoPendientePagoPorPlazoResumido")
        End If
        cbxMoneda.txt.Text = ""


    End Sub

    Sub HabilitarPeriodo()
        Select Case cbxVencimiento.SelectedIndex
            Case 0
                txtDesde.Enabled = False
                txtHasta.Enabled = False
            Case 1
                txtDesde.Enabled = False
                txtHasta.Enabled = False
            Case 2
                txtDesde.Enabled = True
                txtHasta.Enabled = True
        End Select
    End Sub

    Sub AsignarFecha()
        If cbxVencimiento.SelectedIndex = 2 Then
            txtDesde.PrimerDiaAño()
            txtHasta.Hoy()
        Else
            txtDesde.txt.Text = ""
            txtHasta.txt.Text = ""
        End If
    End Sub

    Sub CopiarCuenta()
        If txtCuentaContableDesde.txtCodigo.Texto = "" Then
            Exit Sub
        End If
        txtCuentaContableHasta.txtCodigo.Texto = txtCuentaContableDesde.txtCodigo.Texto
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = chkTipoComprobante.Valor
    End Sub

    Private Sub frmInventariodeDocumentosPendientesAPagar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmInventariodeDocumentosPendientesAPagar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ' cbxDeposito.DataFilter = " IDSucursal = " & cbxSucursal.GetValue
    End Sub

    Private Sub chkMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkDocumentos_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxDocumento.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxVencimiento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxVencimiento.SelectedIndexChanged
        AsignarFecha()

        HabilitarPeriodo()
    End Sub

    Private Sub chkMoneda_PropertyChanged_1(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkTipoProveedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProveedor.PropertyChanged
        cbxTipoProveedor.Enabled = value
    End Sub

    Private Sub chkCC_Leave(sender As System.Object, e As System.EventArgs) Handles chkCC.Leave
        CopiarCuenta()
    End Sub

    Private Sub chkCC_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCC.PropertyChanged
        txtCuentaContableDesde.Enabled = value
        chkCCHasta.Valor = value
        txtCuentaContableHasta.Enabled = value
    End Sub

    Private Sub txtCuentaContableDesde_Leave(sender As System.Object, e As System.EventArgs) Handles txtCuentaContableDesde.Leave
        CopiarCuenta()
    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmDistribuciondeSaldoDocumentoaPagar
    'CLASES
    Dim CReporte As New CReporteCuentasAPagar
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio


    'PROPIEDADES

    Private IDTransaccionValue As Integer
    Public Property IDTransaccion() As Integer
        Get
            Return IDTransaccionValue
        End Get
        Set(ByVal value As Integer)
            IDTransaccionValue = value
        End Set
    End Property

    'VARIABLES

    Dim ID As Integer

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        txtReferencia.Hoy()

        'Funciones
        CambiarOrdenacion()
        CargarInformacion()


    End Sub

    Sub CargarInformacion()
        'Cbx Condicion
        cbxCondicion.cbx.Items.Add("A VENCER")
        cbxCondicion.cbx.Items.Add("VENCIDOS")

        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxCondicion.cbx.SelectedIndex = 0
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")

        'Cargar TextBox Dias
        txtDia1.txt.Text = "7"
        txtDia2.txt.Text = "20"
        txtDia3.txt.Text = "30"
        txtDia4.txt.Text = "60"
        txtDia5.txt.Text = "90"
        txtDia6.txt.Text = "120"
        CalculaDiasFecha()

        txtReferencia.txt.Text = Date.Now

        cbxMoneda.cbx.SelectedIndex = 0
        cbxCondicion.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

        CSistema.SqlToComboBox(cbxDocumentos.cbx, "Select Distinct ID,Descripcion From TipoComprobante")
        CSistema.SqlToComboBox(cbxTipoProveedor.cbx, "Select Distinct ID,Descripcion From TipoProveedor")

        cbxOrdenadoPor.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENADO", "")
        cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")
        cbxEnForma.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ENFORMA", "")

    End Sub

    Sub GuardarInformacion()

        'Configuracion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENADO", cbxOrdenadoPor.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ENFORMA", cbxEnForma.cbx.Text)

    End Sub

    Sub Listar()
        Dim Titulo As String = "DISTRIBUCION DE SALDOS DE DOCUMENTOS A PAGAR"
        Dim TipoInforme As String = ""
        Dim Campo As String = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim ASC As Boolean = False
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar 
        Dim Condicion As Boolean
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            Condicion = 0
        Else
            Condicion = 1
        End If

        'Where = "'" + txtFecha1.txt.Text + "','" + txtFecha2.txt.Text + "','" + txtFecha3.txt.Text + "','" + txtFecha4.txt.Text + "','" + txtFecha5.txt.Text + "','" + txtFecha6.txt.Text + "','" + txtReferencia.GetValueString + "'," + Condicion.ToString
        Where = "'" + CSistema.FormatoFechaBaseDatos(txtFecha1.txt.Text, True, False) + "','" + CSistema.FormatoFechaBaseDatos(txtFecha2.txt.Text, True, False) + "','" + CSistema.FormatoFechaBaseDatos(txtFecha3.txt.Text, True, False) + "','" + CSistema.FormatoFechaBaseDatos(txtFecha4.txt.Text, True, False) + "','" + CSistema.FormatoFechaBaseDatos(txtFecha5.txt.Text, True, False) + "','" + CSistema.FormatoFechaBaseDatos(txtFecha6.txt.Text, True, False) + "','" + CSistema.FormatoFechaBaseDatos(txtReferencia.GetValue, True, False) + "'," + Condicion.ToString
        Where = Where & ", " & cbxMoneda.GetValue
        WhereDetalle = " IDMoneda=" & cbxMoneda.GetValue

        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion2(WhereDetalle)
            End If

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            If cbxEnForma.cbx.SelectedIndex = 0 Then
                ASC = "True"
            Else
                ASC = "False"
            End If
        End If

        TipoInforme = cbxCondicion.cbx.Text & " AL " & txtReferencia.GetValueString & " - En " & cbxMoneda.txt.Text
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro)

        'CReporte.DistribucionSaldosPago(frm, Where, WhereDetalle, OrderBy, txtFecha1.GetValueString, txtFecha2.GetValueString, txtFecha3.GetValueString, txtFecha4.GetValueString, txtFecha5.GetValueString, txtFecha6.GetValueString, txtDia1.txt.Text, txtDia2.txt.Text, txtDia3.txt.Text, txtDia4.txt.Text, txtDia5.txt.Text, txtDia6.txt.Text, Titulo, TipoInforme, vgUsuarioIdentificador, ASC)
        CReporte.DistribucionSaldosPago(frm, Where, WhereDetalle, OrderBy, txtFecha1.GetValue, txtFecha2.GetValue, txtFecha3.GetValue, txtFecha4.GetValue, txtFecha5.GetValue, txtFecha6.GetValue, txtDia1.txt.Text, txtDia2.txt.Text, txtDia3.txt.Text, txtDia4.txt.Text, txtDia5.txt.Text, txtDia6.txt.Text, Titulo, TipoInforme, vgUsuarioIdentificador, ASC)
    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VCompra ", "Select Top(0) 'Total'='','Sucursal'='','Proveedor'='' From VCompra")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Function CalcularFecha(ByVal Referencia As Date, ByRef Dias As Integer) As Date

        CalcularFecha = Date.Now

        Dim Retorno As Date

        ' A Vencer
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            Retorno = Referencia.AddDays(Dias)
        End If

        'Vencido
        If cbxCondicion.cbx.SelectedIndex = 1 Then
            Retorno = Referencia.AddDays(Dias * -1)
        End If

        Return Retorno

    End Function
    Function CalcularFecha2(ByVal Referencia As Date, ByRef Dias As Integer) As Date

        'CalcularFecha = Date.Now

        Dim Retorno As Date

        ' A Vencer
        If cbxCondicion.cbx.SelectedIndex = 0 Then
            Retorno = Referencia.AddDays(Dias)
        End If

        'Vencido
        If cbxCondicion.cbx.SelectedIndex = 1 Then
            Retorno = Referencia.AddDays(Dias * -1)
        End If

        Return Retorno

    End Function

    Sub CalculaDiasFecha()
        Dim Fecha As String = txtReferencia.GetValueString
        'If IsDate(Fecha) Then
        If IsDate(txtReferencia.txt.Text) Then
            'Dia1
            'txtFecha1.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia1.ObtenerValor.ToString)
            txtFecha1.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia1.ObtenerValor.ToString)

            'Dia2
            'txtFecha2.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia2.ObtenerValor.ToString)
            txtFecha2.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia2.ObtenerValor.ToString)
            'Dia3

            'txtFecha3.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia3.ObtenerValor.ToString)
            txtFecha3.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia3.ObtenerValor.ToString)
            'Dia4

            'txtFecha4.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia4.ObtenerValor.ToString)
            txtFecha4.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia4.ObtenerValor.ToString)
            'Dia5

            'txtFecha5.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia5.ObtenerValor.ToString)
            txtFecha5.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia5.ObtenerValor.ToString)

            'Dia6
            'txtFecha6.txt.Text = CalcularFecha(txtReferencia.GetValueString, txtDia6.ObtenerValor.ToString)
            txtFecha6.txt.Text = CalcularFecha2(txtReferencia.GetValue, txtDia6.ObtenerValor.ToString)


        End If

    End Sub

    Private Sub frmDistribuciondeSaldoDocumentoaPagar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Enter Then
            CSistema.SelectNextControl(Me, e.KeyCode)
        End If
    End Sub

    Private Sub frmDistribuciondeSaldoDocumentoaCobrar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub txtDia1_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia1.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia2_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia2.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia3_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia3.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia4_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia4.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia5_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia5.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia6_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDia6.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia7_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia8_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        CalculaDiasFecha()
    End Sub

    Private Sub txtDia9_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        CalculaDiasFecha()
    End Sub

    Private Sub cbxCondicion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CalculaDiasFecha()
    End Sub

    Private Sub txtReferencia_TeclaPrecionada(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.TeclaPrecionada
        CalculaDiasFecha()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipoProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoProveedor.PropertyChanged
        cbxTipoProveedor.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub cbxCondicion_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxCondicion.PropertyChanged
        CalculaDiasFecha()
    End Sub

    Private Sub cbxCondicion_TeclaPresionada(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxCondicion.TeclaPrecionada
        CalculaDiasFecha()
    End Sub


    Private Sub chkMoneda_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkDocumentos_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDocumentos.PropertyChanged
        cbxDocumentos.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub frmDistribuciondeSaldoDocumentoaPagar_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        GuardarInformacion()
    End Sub

End Class
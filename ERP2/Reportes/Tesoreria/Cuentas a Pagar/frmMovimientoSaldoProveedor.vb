﻿Imports ERP.Reporte

Public Class frmMovimientoSaldoProveedor

    'CLASES
    Dim CReporte As New CReporteCuentasAPagar
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        txtProveedor.Conectar()

        'Funciones
        CambiarOrdenacion()
        CargarInformacion()

        cbxTipo.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipo Movimiento
        cbxTipo.cbx.Items.Add("Resumen")
        cbxTipo.cbx.Items.Add("Detalle")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Moneda
        cbxMoneda.cbx.Text = "GUARANIES"

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim Where2 As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim SubTitulo As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As Boolean = True
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = "'" & txtHasta.GetValueString & "'"
        Where2 = "'" & txtDesde.GetValueString & "', '" & txtHasta.GetValueString & "' "

        WhereDetalle = " IDMoneda=" & cbxMoneda.GetValue & " "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion2(WhereDetalle)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        Select Case cbxOrdenadoPor.cbx.SelectedIndex
            Case Is = 0
                OrderBy = "Proveedor"
            Case Is = 1
                OrderBy = "TipoProveedor"
            Case Is = 2
                OrderBy = "Saldo"
        End Select

        Select Case cbxEnForma.cbx.SelectedIndex
            Case Is = 1
                Orden = False
        End Select

        'SubTitulo
        SubTitulo = "Ordenado por: " & cbxOrdenadoPor.Texto & " - Moneda: " & cbxMoneda.Texto

        CReporte.ArmarSubTitulo(SubTitulo, gbxFiltro)

        Select Case cbxTipo.cbx.SelectedIndex
            Case Is = 0
                Titulo = "SALDO DE PROVEEDORES AL " & txtDesde.GetValue.ToShortDateString
                CReporte.MovimientoSaldoPagoResumen(frm, Where, Titulo, vgUsuarioIdentificador, WhereDetalle, txtHasta.GetValue, cbxMoneda.cbx.Text, cbxOrdenadoPor.cbx.Text, OrderBy, Top, Orden, SubTitulo)
            Case Is = 1
                Titulo = "RESUMEN DE MOVIMIENTOS Y SALDOS DE PROVEEDORES"
                CReporte.MovimientoSaldoPagoDetalle(frm, Where2, Titulo, vgUsuarioIdentificador, WhereDetalle, txtDesde.GetValue, txtHasta.GetValue, cbxMoneda.cbx.Text, cbxOrdenadoPor.cbx.Text, OrderBy, Top, Orden, SubTitulo)
        End Select

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("SpViewResumenMovimientosSaldosPago", "select top(0) 'Por Razón Social'= Proveedor,'Por TipoProveedor'=TipoProveedor,'Saldo'= 0 From VCompra")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmMovimientoSaldoProveedor_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkTipoProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoProveedor.PropertyChanged
        cbxTipoProveedor.Enabled = value
    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        txtProveedor.Enabled = value
    End Sub


    Private Sub chkProveedor_PropertyChanged_1(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
    End Sub
End Class




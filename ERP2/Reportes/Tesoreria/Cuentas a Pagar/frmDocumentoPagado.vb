﻿Imports ERP.Reporte

Public Class frmDocumentoPagado



    'CLASES
    Dim CReporte As New CReporteCuentasAPagar
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "DOCUMENTOS PAGADOS POR PROVEEDOR"
    Dim TipoInforme As String

    Dim vdtCuentaBancaria As DataTable

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()
        CambiarOrdenacion()

        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipo Comprobante
        CSistema.SqlToComboBox(cbxTipoComprobante.cbx, CData.GetTable("Vtipocomprobante", " Operacion = 'PAGOS Y COBROS CON DOCUMENTOS' and Proveedor = 1"), "ID", "Descripcion")

        'Tipo Proveedor
        CSistema.SqlToComboBox(cbxTipoProveedor.cbx, CData.GetTable("TipoProveedor"), "ID", "Descripcion")

        'Proveedor
        CSistema.SqlToComboBox(cbxProveedor.cbx, CData.GetTable("VProveedor"), "ID", "RazonSocial")


        'Cuentas Bancarias
        vdtCuentaBancaria = CData.GetTable("VCuentaBancaria").Copy
        CSistema.SqlToComboBox(cbxCuenta.cbx, vdtCuentaBancaria, "ID", "Cuenta")
        

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim SubTitulo As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where FechaPago Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        If chkTipoComprobante.chk.Checked = True And cbxTipoComprobante.cbx.Text <> "" Then
            Where = Where & " And IDTipocomprobantepago =" & cbxTipoComprobante.cbx.SelectedValue & " "
            SubTitulo = SubTitulo & "  -  Tipo de Comprobante: " & cbxTipoComprobante.cbx.Text
        Else
            Where = Where
        End If

        If chkTipoProveedor.chk.Checked = True And cbxTipoProveedor.cbx.Text <> "" Then
            Where = Where & " And IDTipoProveedor =" & cbxTipoProveedor.cbx.SelectedValue & " "
            SubTitulo = SubTitulo & "  -  Tipo de Proveedor: " & cbxTipoProveedor.cbx.Text
        Else
            Where = Where
        End If

        If chkProveedor.chk.Checked = True And cbxProveedor.cbx.Text <> "" Then
            Where = Where & " And IDProveedor =" & cbxProveedor.cbx.SelectedValue & " "
            SubTitulo = SubTitulo & "  -  Proveedor: " & cbxProveedor.cbx.Text
        Else
            Where = Where
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        TipoInforme = "Entre el " & txtDesde.GetValueString & " y el " & txtHasta.GetValueString & " - " & SubTitulo

        CReporte.DocumentoPagado(frm, Where, Titulo, vgUsuarioIdentificador, OrderBy, Top, TipoInforme)


    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VDocumentoPagado ", "Select Top(0) Proveedor,Documento,Fecha,Importe From VDocumentoPagado")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub frmDocumentoPagado_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkTipoProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoProveedor.PropertyChanged
        cbxTipoProveedor.Enabled = value
    End Sub

    Private Sub cbxCuenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        For Each oRow As DataRow In vdtCuentaBancaria.Select(" ID = " & cbxCuenta.GetValue)
            txtBanco.txt.Text = oRow("Referencia")
        Next
    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkBanco_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkBanco.PropertyChanged
        cbxCuenta.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
    End Sub

   
End Class




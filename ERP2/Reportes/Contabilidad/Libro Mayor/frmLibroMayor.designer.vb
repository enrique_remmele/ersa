﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroMayor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtNumeroInicial = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.chkFormatoImpresion = New ERP.ocxCHK()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.rdbHorizontal = New System.Windows.Forms.RadioButton()
        Me.rdbVertical = New System.Windows.Forms.RadioButton()
        Me.chkImprimirCabecera = New ERP.ocxCHK()
        Me.chkResumido = New ERP.ocxCHK()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbxFormato = New ERP.ocxCBX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkCentroCosto = New ERP.ocxCHK()
        Me.cbxCentroCosto = New ERP.ocxCBX()
        Me.chkUnidadNegocio = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.chkMasDetalles = New ERP.ocxCHK()
        Me.rdbContabilidad = New System.Windows.Forms.RadioButton()
        Me.rdbAdministracion = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCuentaFinal = New ERP.ocxTXTCuentaContable()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCuentaInicial = New ERP.ocxTXTCuentaContable()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.chkDepartamento = New ERP.ocxCHK()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.chkSeccion = New ERP.ocxCHK()
        Me.cbxSeccion = New ERP.ocxCBX()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(9, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(231, 466)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(125, 23)
        Me.btnListar.TabIndex = 3
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtNumeroInicial)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.chkFormatoImpresion)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.rdbHorizontal)
        Me.GroupBox2.Controls.Add(Me.rdbVertical)
        Me.GroupBox2.Controls.Add(Me.chkImprimirCabecera)
        Me.GroupBox2.Controls.Add(Me.chkResumido)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.cbxFormato)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.cbxTipo)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(364, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 456)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtNumeroInicial
        '
        Me.txtNumeroInicial.Enabled = False
        Me.txtNumeroInicial.Location = New System.Drawing.Point(59, 237)
        Me.txtNumeroInicial.Multiline = True
        Me.txtNumeroInicial.Name = "txtNumeroInicial"
        Me.txtNumeroInicial.Size = New System.Drawing.Size(70, 21)
        Me.txtNumeroInicial.TabIndex = 25
        Me.txtNumeroInicial.Text = "1"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(6, 237)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 21)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Rubrica:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkFormatoImpresion
        '
        Me.chkFormatoImpresion.BackColor = System.Drawing.Color.Transparent
        Me.chkFormatoImpresion.Color = System.Drawing.Color.Empty
        Me.chkFormatoImpresion.Location = New System.Drawing.Point(14, 210)
        Me.chkFormatoImpresion.Name = "chkFormatoImpresion"
        Me.chkFormatoImpresion.Size = New System.Drawing.Size(115, 21)
        Me.chkFormatoImpresion.SoloLectura = False
        Me.chkFormatoImpresion.TabIndex = 26
        Me.chkFormatoImpresion.Texto = "Formato Impresion"
        Me.chkFormatoImpresion.Valor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 268)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Orientacion de la hoja:"
        '
        'rdbHorizontal
        '
        Me.rdbHorizontal.AutoSize = True
        Me.rdbHorizontal.Location = New System.Drawing.Point(120, 285)
        Me.rdbHorizontal.Name = "rdbHorizontal"
        Me.rdbHorizontal.Size = New System.Drawing.Size(72, 17)
        Me.rdbHorizontal.TabIndex = 12
        Me.rdbHorizontal.TabStop = True
        Me.rdbHorizontal.Text = "Horizontal"
        Me.rdbHorizontal.UseVisualStyleBackColor = True
        '
        'rdbVertical
        '
        Me.rdbVertical.AutoSize = True
        Me.rdbVertical.Checked = True
        Me.rdbVertical.Location = New System.Drawing.Point(14, 285)
        Me.rdbVertical.Name = "rdbVertical"
        Me.rdbVertical.Size = New System.Drawing.Size(60, 17)
        Me.rdbVertical.TabIndex = 11
        Me.rdbVertical.TabStop = True
        Me.rdbVertical.Text = "Vertical"
        Me.rdbVertical.UseVisualStyleBackColor = True
        '
        'chkImprimirCabecera
        '
        Me.chkImprimirCabecera.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimirCabecera.Color = System.Drawing.Color.Empty
        Me.chkImprimirCabecera.Location = New System.Drawing.Point(14, 183)
        Me.chkImprimirCabecera.Name = "chkImprimirCabecera"
        Me.chkImprimirCabecera.Size = New System.Drawing.Size(230, 21)
        Me.chkImprimirCabecera.SoloLectura = False
        Me.chkImprimirCabecera.TabIndex = 10
        Me.chkImprimirCabecera.Texto = "Imprimir Cabecera"
        Me.chkImprimirCabecera.Valor = False
        '
        'chkResumido
        '
        Me.chkResumido.BackColor = System.Drawing.Color.Transparent
        Me.chkResumido.Color = System.Drawing.Color.Empty
        Me.chkResumido.Location = New System.Drawing.Point(15, 156)
        Me.chkResumido.Name = "chkResumido"
        Me.chkResumido.Size = New System.Drawing.Size(207, 21)
        Me.chkResumido.SoloLectura = False
        Me.chkResumido.TabIndex = 5
        Me.chkResumido.Texto = "Informe Resumido"
        Me.chkResumido.Valor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Formato:"
        '
        'cbxFormato
        '
        Me.cbxFormato.CampoWhere = Nothing
        Me.cbxFormato.CargarUnaSolaVez = False
        Me.cbxFormato.DataDisplayMember = Nothing
        Me.cbxFormato.DataFilter = Nothing
        Me.cbxFormato.DataOrderBy = Nothing
        Me.cbxFormato.DataSource = Nothing
        Me.cbxFormato.DataValueMember = Nothing
        Me.cbxFormato.dtSeleccionado = Nothing
        Me.cbxFormato.FormABM = Nothing
        Me.cbxFormato.Indicaciones = Nothing
        Me.cbxFormato.Location = New System.Drawing.Point(9, 129)
        Me.cbxFormato.Name = "cbxFormato"
        Me.cbxFormato.SeleccionMultiple = False
        Me.cbxFormato.SeleccionObligatoria = True
        Me.cbxFormato.Size = New System.Drawing.Size(213, 21)
        Me.cbxFormato.SoloLectura = False
        Me.cbxFormato.TabIndex = 4
        Me.cbxFormato.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tipo:"
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.dtSeleccionado = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(9, 84)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionMultiple = False
        Me.cbxTipo.SeleccionObligatoria = True
        Me.cbxTipo.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 17, 16, 39, 0, 258)
        Me.txtHasta.Location = New System.Drawing.Point(89, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 17, 16, 39, 0, 258)
        Me.txtDesde.Location = New System.Drawing.Point(9, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSeccion)
        Me.gbxFiltro.Controls.Add(Me.cbxSeccion)
        Me.gbxFiltro.Controls.Add(Me.chkDepartamento)
        Me.gbxFiltro.Controls.Add(Me.cbxDepartamento)
        Me.gbxFiltro.Controls.Add(Me.chkCentroCosto)
        Me.gbxFiltro.Controls.Add(Me.cbxCentroCosto)
        Me.gbxFiltro.Controls.Add(Me.chkUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.chkMasDetalles)
        Me.gbxFiltro.Controls.Add(Me.rdbContabilidad)
        Me.gbxFiltro.Controls.Add(Me.rdbAdministracion)
        Me.gbxFiltro.Controls.Add(Me.Label2)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaFinal)
        Me.gbxFiltro.Controls.Add(Me.Label1)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaInicial)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(6, 4)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(350, 456)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkCentroCosto
        '
        Me.chkCentroCosto.BackColor = System.Drawing.Color.Transparent
        Me.chkCentroCosto.Color = System.Drawing.Color.Empty
        Me.chkCentroCosto.Location = New System.Drawing.Point(16, 219)
        Me.chkCentroCosto.Name = "chkCentroCosto"
        Me.chkCentroCosto.Size = New System.Drawing.Size(115, 21)
        Me.chkCentroCosto.SoloLectura = False
        Me.chkCentroCosto.TabIndex = 17
        Me.chkCentroCosto.Texto = "Centro de Costo:"
        Me.chkCentroCosto.Valor = False
        '
        'cbxCentroCosto
        '
        Me.cbxCentroCosto.CampoWhere = "IDCentroCosto"
        Me.cbxCentroCosto.CargarUnaSolaVez = False
        Me.cbxCentroCosto.DataDisplayMember = "Descripcion"
        Me.cbxCentroCosto.DataFilter = Nothing
        Me.cbxCentroCosto.DataOrderBy = Nothing
        Me.cbxCentroCosto.DataSource = "vCentroCosto"
        Me.cbxCentroCosto.DataValueMember = "ID"
        Me.cbxCentroCosto.dtSeleccionado = Nothing
        Me.cbxCentroCosto.Enabled = False
        Me.cbxCentroCosto.FormABM = Nothing
        Me.cbxCentroCosto.Indicaciones = Nothing
        Me.cbxCentroCosto.Location = New System.Drawing.Point(137, 219)
        Me.cbxCentroCosto.Name = "cbxCentroCosto"
        Me.cbxCentroCosto.SeleccionMultiple = False
        Me.cbxCentroCosto.SeleccionObligatoria = False
        Me.cbxCentroCosto.Size = New System.Drawing.Size(198, 21)
        Me.cbxCentroCosto.SoloLectura = False
        Me.cbxCentroCosto.TabIndex = 18
        Me.cbxCentroCosto.Texto = ""
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(16, 191)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(115, 21)
        Me.chkUnidadNegocio.SoloLectura = False
        Me.chkUnidadNegocio.TabIndex = 15
        Me.chkUnidadNegocio.Texto = "Unidad de negocio:"
        Me.chkUnidadNegocio.Valor = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = "IDUnidadNegocio"
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = "Descripcion"
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = "VUnidadNegocio"
        Me.cbxUnidadNegocio.DataValueMember = "ID"
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.Enabled = False
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(137, 191)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(198, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 16
        Me.cbxUnidadNegocio.Texto = ""
        '
        'chkMasDetalles
        '
        Me.chkMasDetalles.BackColor = System.Drawing.Color.Transparent
        Me.chkMasDetalles.Color = System.Drawing.Color.Empty
        Me.chkMasDetalles.Location = New System.Drawing.Point(16, 431)
        Me.chkMasDetalles.Name = "chkMasDetalles"
        Me.chkMasDetalles.Size = New System.Drawing.Size(256, 21)
        Me.chkMasDetalles.SoloLectura = False
        Me.chkMasDetalles.TabIndex = 14
        Me.chkMasDetalles.Texto = "Mostrar Usuario y Fecha de Transaccion"
        Me.chkMasDetalles.Valor = False
        '
        'rdbContabilidad
        '
        Me.rdbContabilidad.AutoSize = True
        Me.rdbContabilidad.Location = New System.Drawing.Point(16, 408)
        Me.rdbContabilidad.Name = "rdbContabilidad"
        Me.rdbContabilidad.Size = New System.Drawing.Size(83, 17)
        Me.rdbContabilidad.TabIndex = 9
        Me.rdbContabilidad.Text = "Contabilidad"
        Me.rdbContabilidad.UseVisualStyleBackColor = True
        Me.rdbContabilidad.Visible = False
        '
        'rdbAdministracion
        '
        Me.rdbAdministracion.AutoSize = True
        Me.rdbAdministracion.Checked = True
        Me.rdbAdministracion.Location = New System.Drawing.Point(17, 382)
        Me.rdbAdministracion.Name = "rdbAdministracion"
        Me.rdbAdministracion.Size = New System.Drawing.Size(93, 17)
        Me.rdbAdministracion.TabIndex = 8
        Me.rdbAdministracion.TabStop = True
        Me.rdbAdministracion.Text = "Administracion"
        Me.rdbAdministracion.UseVisualStyleBackColor = True
        Me.rdbAdministracion.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Cuenta Final:"
        '
        'txtCuentaFinal
        '
        Me.txtCuentaFinal.AlturaMaxima = 205
        Me.txtCuentaFinal.Consulta = Nothing
        Me.txtCuentaFinal.IDCentroCosto = 0
        Me.txtCuentaFinal.IDUnidadNegocio = 0
        Me.txtCuentaFinal.ListarTodas = False
        Me.txtCuentaFinal.Location = New System.Drawing.Point(17, 84)
        Me.txtCuentaFinal.Name = "txtCuentaFinal"
        Me.txtCuentaFinal.Registro = Nothing
        Me.txtCuentaFinal.Resolucion173 = False
        Me.txtCuentaFinal.Seleccionado = False
        Me.txtCuentaFinal.Size = New System.Drawing.Size(319, 27)
        Me.txtCuentaFinal.SoloLectura = False
        Me.txtCuentaFinal.TabIndex = 5
        Me.txtCuentaFinal.Texto = Nothing
        Me.txtCuentaFinal.whereFiltro = Nothing
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cuenta Inicial:"
        '
        'txtCuentaInicial
        '
        Me.txtCuentaInicial.AlturaMaxima = 205
        Me.txtCuentaInicial.Consulta = Nothing
        Me.txtCuentaInicial.IDCentroCosto = 0
        Me.txtCuentaInicial.IDUnidadNegocio = 0
        Me.txtCuentaInicial.ListarTodas = False
        Me.txtCuentaInicial.Location = New System.Drawing.Point(17, 34)
        Me.txtCuentaInicial.Name = "txtCuentaInicial"
        Me.txtCuentaInicial.Registro = Nothing
        Me.txtCuentaInicial.Resolucion173 = False
        Me.txtCuentaInicial.Seleccionado = False
        Me.txtCuentaInicial.Size = New System.Drawing.Size(319, 27)
        Me.txtCuentaInicial.SoloLectura = False
        Me.txtCuentaInicial.TabIndex = 1
        Me.txtCuentaInicial.Texto = Nothing
        Me.txtCuentaInicial.whereFiltro = Nothing
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(16, 160)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(115, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 0
        Me.chkTipoComprobante.Texto = "TipoComprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = "Descripcion"
        Me.cbxTipoComprobante.DataSource = "TipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(137, 160)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(198, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(16, 126)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(69, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(137, 126)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(198, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(545, 466)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 4
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 495)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(616, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(6, 466)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(125, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Analitico"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'chkDepartamento
        '
        Me.chkDepartamento.BackColor = System.Drawing.Color.Transparent
        Me.chkDepartamento.Color = System.Drawing.Color.Empty
        Me.chkDepartamento.Location = New System.Drawing.Point(16, 249)
        Me.chkDepartamento.Name = "chkDepartamento"
        Me.chkDepartamento.Size = New System.Drawing.Size(115, 21)
        Me.chkDepartamento.SoloLectura = False
        Me.chkDepartamento.TabIndex = 19
        Me.chkDepartamento.Texto = "Departamento:"
        Me.chkDepartamento.Valor = False
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = "IDDepartamento"
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = "Descripcion"
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = Nothing
        Me.cbxDepartamento.DataSource = "vDepartamentoEmpresa"
        Me.cbxDepartamento.DataValueMember = "ID"
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.Enabled = False
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(137, 249)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(198, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 20
        Me.cbxDepartamento.Texto = ""
        '
        'chkSeccion
        '
        Me.chkSeccion.BackColor = System.Drawing.Color.Transparent
        Me.chkSeccion.Color = System.Drawing.Color.Empty
        Me.chkSeccion.Location = New System.Drawing.Point(16, 280)
        Me.chkSeccion.Name = "chkSeccion"
        Me.chkSeccion.Size = New System.Drawing.Size(115, 21)
        Me.chkSeccion.SoloLectura = False
        Me.chkSeccion.TabIndex = 21
        Me.chkSeccion.Texto = "Seccion:"
        Me.chkSeccion.Valor = False
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = "IDSeccion"
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = "Descripcion"
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = Nothing
        Me.cbxSeccion.DataSource = "vSeccion"
        Me.cbxSeccion.DataValueMember = "ID"
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.Enabled = False
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(137, 280)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(198, 21)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 22
        Me.cbxSeccion.Texto = ""
        '
        'frmLibroMayor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(616, 517)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnListar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmLibroMayor"
        Me.Text = "LibroMayor"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCuentaFinal As ERP.ocxTXTCuentaContable
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCuentaInicial As ERP.ocxTXTCuentaContable
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbxFormato As ERP.ocxCBX
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents chkResumido As ERP.ocxCHK
    Friend WithEvents chkImprimirCabecera As ERP.ocxCHK
    Friend WithEvents rdbContabilidad As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAdministracion As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents rdbHorizontal As System.Windows.Forms.RadioButton
    Friend WithEvents rdbVertical As System.Windows.Forms.RadioButton
    Friend WithEvents chkMasDetalles As ERP.ocxCHK
    Friend WithEvents chkUnidadNegocio As ERP.ocxCHK
    Friend WithEvents cbxUnidadNegocio As ERP.ocxCBX
    Friend WithEvents chkCentroCosto As ERP.ocxCHK
    Friend WithEvents cbxCentroCosto As ERP.ocxCBX
    Friend WithEvents txtNumeroInicial As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents chkFormatoImpresion As ocxCHK
    Friend WithEvents chkSeccion As ocxCHK
    Friend WithEvents cbxSeccion As ocxCBX
    Friend WithEvents chkDepartamento As ocxCHK
    Friend WithEvents cbxDepartamento As ocxCBX
End Class

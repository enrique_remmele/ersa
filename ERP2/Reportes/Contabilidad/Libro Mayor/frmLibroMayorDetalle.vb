﻿Imports ERP.Reporte

Public Class frmLibroMayorDetalle

    'CLASES
    Private CSistema As New CSistema
    Private CData As New CData
    Dim CReporte As New CReporteContabilidad

    'VARIABLES
    Dim Subtitulo As String

    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Conectar Cuentas
        txtCuentaFinal.Conectar()
        txtCuentaInicial.Conectar()

        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()

    End Sub

    Sub Listar()
        Subtitulo = ""
        Dim frm As New frmReporte

        If txtCuentaInicial.txtCodigo.GetValue = "" Then
            Dim mensaje As String = "Seleccion una cuenta Inicial!"
            ErrorProvider1.SetError(txtCuentaInicial, mensaje)
            ErrorProvider1.SetIconAlignment(txtCuentaInicial, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If txtCuentaFinal.txtCodigo.GetValue = "" Then
            Dim mensaje As String = "Seleccion una cuenta Final!"
            ErrorProvider1.SetError(txtCuentaFinal, mensaje)
            ErrorProvider1.SetIconAlignment(txtCuentaFinal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Dim Titulo As String = "LIBRO MAYOR"

        CReporte.ArmarSubTitulo(Subtitulo, gbxFiltro, txtDesde, txtHasta)

        Try

            CReporte.ImprimirLibroMayorDetalle(frm, Where(False), WhereDetalle, Titulo, Subtitulo, vgUsuarioIdentificador)

        Catch ex As Exception

        End Try

    End Sub

    Function Where(ByVal Pantalla As Boolean) As String

        Where = "'" & txtCuentaInicial.txtCodigo.txt.Text & "','" & txtCuentaFinal.txtCodigo.txt.Text & "','" & CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False) & "','" & CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False) & "','" & Pantalla & "' "

    End Function

    Function WhereDetalle() As String

        WhereDetalle = ""

        If chkSucursal.chk.Checked = True Then
            WhereDetalle = "IDSucursal = " & cbxSucursal.GetValue
        End If

        If chkTipoComprobante.chk.Checked = True Then
            If WhereDetalle = "" Then
                WhereDetalle = "IDTipoComprobante= " & cbxTipoComprobante.GetValue
            Else
                WhereDetalle = WhereDetalle & " And IDTipoComprobante = " & cbxTipoComprobante.GetValue
            End If
        End If


    End Function

    Sub Analitico()


        Dim frm As New frmLibroIVAAnalitico
        Dim Titulo As String = "LIBRO MAYOR - " & "Del " & txtDesde.GetValue.ToShortDateString & " al " & txtHasta.GetValue.ToShortDateString

        frm.Titulo = Titulo
        frm.dtCuenta = CSistema.ExecuteToDataTable("Execute SpViewLibroMayorDetalle " & Where(True), "", 1000)
        frm.FechaDesde = txtDesde.GetValueString
        frm.FechaHasta = txtHasta.GetValueString

        If chkSucursal.Valor = True Then
            frm.IDSucursal = cbxSucursal.GetValue
            frm.Sucursal = cbxSucursal.Texto
        End If

        If chkTipoComprobante.Valor = True Then
            frm.IDTipoComprobante = cbxTipoComprobante.GetValue
            frm.TipoComprobante = cbxTipoComprobante.Texto
        End If

        frm.dtAsiento = CData.FiltrarDataTable(frm.dtAsiento, WhereDetalle)

        FGMostrarFormulario(Me, frm, "Libo Mayor", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False, False)

    End Sub

    Sub CopiarCuenta()
        If txtCuentaInicial.txtCodigo.Texto = "" Then
            Exit Sub
        End If
        txtCuentaFinal.txtCodigo.Texto = txtCuentaInicial.txtCodigo.Texto
    End Sub

    Private Sub frmETL_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmETL_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Analitico()
    End Sub

    Private Sub txtCuentaInicial_Leave(sender As Object, e As System.EventArgs) Handles txtCuentaInicial.Leave
        CopiarCuenta()
    End Sub

End Class


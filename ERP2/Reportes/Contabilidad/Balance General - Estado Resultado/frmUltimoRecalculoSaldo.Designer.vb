﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUltimoRecalculoSaldo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblHora = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnSI = New System.Windows.Forms.Button()
        Me.btnNO = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.Label1.Location = New System.Drawing.Point(27, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(273, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Ultimo Recalculo Realizado el:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.Label2.Location = New System.Drawing.Point(229, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 25)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "a las"
        '
        'lblHora
        '
        Me.lblHora.AutoSize = True
        Me.lblHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHora.Location = New System.Drawing.Point(313, 63)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(167, 25)
        Me.lblHora.TabIndex = 2
        Me.lblHora.Text = "Hora Ultimo Rec"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(27, 63)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(181, 25)
        Me.lblFecha.TabIndex = 3
        Me.lblFecha.Text = "Fecha Ultimo Rec"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.Label5.Location = New System.Drawing.Point(125, 117)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(246, 25)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Desea Volver a recalcular?"
        '
        'btnSI
        '
        Me.btnSI.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnSI.Location = New System.Drawing.Point(89, 174)
        Me.btnSI.Name = "btnSI"
        Me.btnSI.Size = New System.Drawing.Size(92, 38)
        Me.btnSI.TabIndex = 5
        Me.btnSI.Text = "SI"
        Me.btnSI.UseVisualStyleBackColor = True
        '
        'btnNO
        '
        Me.btnNO.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.btnNO.Location = New System.Drawing.Point(332, 174)
        Me.btnNO.Name = "btnNO"
        Me.btnNO.Size = New System.Drawing.Size(92, 38)
        Me.btnNO.TabIndex = 6
        Me.btnNO.Text = "NO"
        Me.btnNO.UseVisualStyleBackColor = True
        '
        'frmUltimoRecalculoSaldo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(544, 221)
        Me.Controls.Add(Me.btnNO)
        Me.Controls.Add(Me.btnSI)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblHora)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmUltimoRecalculoSaldo"
        Me.Text = "frmUltimoRecalculoSaldo"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblHora As Label
    Friend WithEvents lblFecha As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents btnSI As Button
    Friend WithEvents btnNO As Button
End Class

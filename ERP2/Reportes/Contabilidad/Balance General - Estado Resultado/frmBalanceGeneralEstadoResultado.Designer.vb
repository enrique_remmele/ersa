﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmBalanceGeneralEstadoResultado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxOpciones = New System.Windows.Forms.GroupBox()
        Me.txtNumeroInicial = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkRegenerar = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.txtFactor = New ERP.ocxTXTNumeric()
        Me.chkIncluirSinSaldo = New ERP.ocxCHK()
        Me.chkIncluirCodigo = New ERP.ocxCHK()
        Me.chkUnidadNegocio = New ERP.ocxCHK()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxMesHasta = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nudAño = New System.Windows.Forms.NumericUpDown()
        Me.lblMes = New System.Windows.Forms.Label()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.lblFactor = New System.Windows.Forms.Label()
        Me.cbxInforme = New System.Windows.Forms.ComboBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.gbxOpciones.SuspendLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(305, 314)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'gbxOpciones
        '
        Me.gbxOpciones.Controls.Add(Me.txtNumeroInicial)
        Me.gbxOpciones.Controls.Add(Me.Label3)
        Me.gbxOpciones.Controls.Add(Me.chkRegenerar)
        Me.gbxOpciones.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxOpciones.Controls.Add(Me.txtFactor)
        Me.gbxOpciones.Controls.Add(Me.chkIncluirSinSaldo)
        Me.gbxOpciones.Controls.Add(Me.chkIncluirCodigo)
        Me.gbxOpciones.Controls.Add(Me.chkUnidadNegocio)
        Me.gbxOpciones.Controls.Add(Me.Label2)
        Me.gbxOpciones.Controls.Add(Me.cbxMesHasta)
        Me.gbxOpciones.Controls.Add(Me.Label1)
        Me.gbxOpciones.Controls.Add(Me.nudAño)
        Me.gbxOpciones.Controls.Add(Me.lblMes)
        Me.gbxOpciones.Controls.Add(Me.cbxMes)
        Me.gbxOpciones.Controls.Add(Me.lblInforme)
        Me.gbxOpciones.Controls.Add(Me.lblFactor)
        Me.gbxOpciones.Controls.Add(Me.cbxInforme)
        Me.gbxOpciones.Location = New System.Drawing.Point(15, 11)
        Me.gbxOpciones.Name = "gbxOpciones"
        Me.gbxOpciones.Size = New System.Drawing.Size(365, 279)
        Me.gbxOpciones.TabIndex = 0
        Me.gbxOpciones.TabStop = False
        Me.gbxOpciones.Text = "Opciones"
        '
        'txtNumeroInicial
        '
        Me.txtNumeroInicial.Location = New System.Drawing.Point(53, 239)
        Me.txtNumeroInicial.Name = "txtNumeroInicial"
        Me.txtNumeroInicial.Size = New System.Drawing.Size(70, 20)
        Me.txtNumeroInicial.TabIndex = 23
        Me.txtNumeroInicial.Text = "1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(0, 238)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 13)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Rubrica:"
        '
        'chkRegenerar
        '
        Me.chkRegenerar.BackColor = System.Drawing.Color.Transparent
        Me.chkRegenerar.Color = System.Drawing.Color.Empty
        Me.chkRegenerar.Location = New System.Drawing.Point(0, 184)
        Me.chkRegenerar.Name = "chkRegenerar"
        Me.chkRegenerar.Size = New System.Drawing.Size(137, 21)
        Me.chkRegenerar.SoloLectura = False
        Me.chkRegenerar.TabIndex = 14
        Me.chkRegenerar.Texto = "Regenerar Saldos "
        Me.chkRegenerar.Valor = False
        Me.chkRegenerar.Visible = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = Nothing
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = "Descripcion"
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = "vUnidadNegocio"
        Me.cbxUnidadNegocio.DataValueMember = "ID"
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.Enabled = False
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(99, 149)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = False
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(252, 23)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 9
        Me.cbxUnidadNegocio.Texto = ""
        '
        'txtFactor
        '
        Me.txtFactor.Color = System.Drawing.Color.Empty
        Me.txtFactor.Decimales = True
        Me.txtFactor.Indicaciones = Nothing
        Me.txtFactor.Location = New System.Drawing.Point(53, 211)
        Me.txtFactor.Name = "txtFactor"
        Me.txtFactor.Size = New System.Drawing.Size(70, 21)
        Me.txtFactor.SoloLectura = False
        Me.txtFactor.TabIndex = 11
        Me.txtFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFactor.Texto = "0"
        '
        'chkIncluirSinSaldo
        '
        Me.chkIncluirSinSaldo.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirSinSaldo.Color = System.Drawing.Color.Empty
        Me.chkIncluirSinSaldo.Location = New System.Drawing.Point(189, 211)
        Me.chkIncluirSinSaldo.Name = "chkIncluirSinSaldo"
        Me.chkIncluirSinSaldo.Size = New System.Drawing.Size(137, 21)
        Me.chkIncluirSinSaldo.SoloLectura = False
        Me.chkIncluirSinSaldo.TabIndex = 13
        Me.chkIncluirSinSaldo.Texto = "Incluir cuentas sin saldo"
        Me.chkIncluirSinSaldo.Valor = False
        '
        'chkIncluirCodigo
        '
        Me.chkIncluirCodigo.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirCodigo.Color = System.Drawing.Color.Empty
        Me.chkIncluirCodigo.Location = New System.Drawing.Point(189, 184)
        Me.chkIncluirCodigo.Name = "chkIncluirCodigo"
        Me.chkIncluirCodigo.Size = New System.Drawing.Size(124, 21)
        Me.chkIncluirCodigo.SoloLectura = False
        Me.chkIncluirCodigo.TabIndex = 12
        Me.chkIncluirCodigo.Texto = "Incluir Codigo"
        Me.chkIncluirCodigo.Valor = False
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(0, 149)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(90, 21)
        Me.chkUnidadNegocio.SoloLectura = False
        Me.chkUnidadNegocio.TabIndex = 8
        Me.chkUnidadNegocio.Texto = "Unid. Negocio:"
        Me.chkUnidadNegocio.Valor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(0, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Mes Hasta:"
        '
        'cbxMesHasta
        '
        Me.cbxMesHasta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxMesHasta.Enabled = False
        Me.cbxMesHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMesHasta.FormattingEnabled = True
        Me.cbxMesHasta.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"})
        Me.cbxMesHasta.Location = New System.Drawing.Point(99, 113)
        Me.cbxMesHasta.Name = "cbxMesHasta"
        Me.cbxMesHasta.Size = New System.Drawing.Size(165, 26)
        Me.cbxMesHasta.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(0, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Mes Desde:"
        '
        'nudAño
        '
        Me.nudAño.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAño.Location = New System.Drawing.Point(100, 49)
        Me.nudAño.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.nudAño.Name = "nudAño"
        Me.nudAño.Size = New System.Drawing.Size(69, 26)
        Me.nudAño.TabIndex = 3
        Me.nudAño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudAño.ThousandsSeparator = True
        Me.nudAño.Value = New Decimal(New Integer() {2013, 0, 0, 0})
        '
        'lblMes
        '
        Me.lblMes.AutoSize = True
        Me.lblMes.Location = New System.Drawing.Point(0, 56)
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Size = New System.Drawing.Size(29, 13)
        Me.lblMes.TabIndex = 2
        Me.lblMes.Text = "Año:"
        '
        'cbxMes
        '
        Me.cbxMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxMes.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"})
        Me.cbxMes.Location = New System.Drawing.Point(100, 81)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(165, 26)
        Me.cbxMes.TabIndex = 5
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(0, 25)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(45, 13)
        Me.lblInforme.TabIndex = 0
        Me.lblInforme.Text = "Informe:"
        '
        'lblFactor
        '
        Me.lblFactor.AutoSize = True
        Me.lblFactor.Location = New System.Drawing.Point(0, 213)
        Me.lblFactor.Name = "lblFactor"
        Me.lblFactor.Size = New System.Drawing.Size(40, 13)
        Me.lblFactor.TabIndex = 10
        Me.lblFactor.Text = "Factor:"
        '
        'cbxInforme
        '
        Me.cbxInforme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxInforme.FormattingEnabled = True
        Me.cbxInforme.Items.AddRange(New Object() {"BALANCE GENERAL", "ESTADO DE RESULTADO", "ESTADO DE RESULTADO MULTICOLUMNA", "LIBRO INVENTARIO", "BALANCE GENERAL Y ESTADO DE RESULTADO"})
        Me.cbxInforme.Location = New System.Drawing.Point(100, 21)
        Me.cbxInforme.Name = "cbxInforme"
        Me.cbxInforme.Size = New System.Drawing.Size(252, 21)
        Me.cbxInforme.TabIndex = 1
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(188, 314)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(97, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'frmBalanceGeneralEstadoResultado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(395, 367)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gbxOpciones)
        Me.Controls.Add(Me.btnAceptar)
        Me.Name = "frmBalanceGeneralEstadoResultado"
        Me.Text = "frmBalanceGeneralEstadoResultado"
        Me.gbxOpciones.ResumeLayout(False)
        Me.gbxOpciones.PerformLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents gbxOpciones As System.Windows.Forms.GroupBox
    Friend WithEvents nudAño As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMes As System.Windows.Forms.Label
    Friend WithEvents cbxMes As System.Windows.Forms.ComboBox
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents lblFactor As System.Windows.Forms.Label
    Friend WithEvents cbxInforme As System.Windows.Forms.ComboBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxMesHasta As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkUnidadNegocio As ERP.ocxCHK
    Friend WithEvents chkIncluirCodigo As ERP.ocxCHK
    Friend WithEvents chkIncluirSinSaldo As ERP.ocxCHK
    Friend WithEvents cbxUnidadNegocio As ERP.ocxCBX
    Friend WithEvents txtFactor As ERP.ocxTXTNumeric
    Friend WithEvents chkRegenerar As ocxCHK
    Friend WithEvents Label3 As Label
    Friend WithEvents txtNumeroInicial As TextBox
End Class

﻿Imports ERP.Reporte

Public Class frmBalanceGeneralEstadoResultado

    'CLASES
    Dim CSistema As New CSistema
    Dim CReporte As New CReporteContabilidad

    'VARIABLES
    Dim dtPlanCuenta As DataTable
    Dim dtPlanCuentaSaldos As DataTable
    Dim TipoInforme As String

    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Año
        nudAño.Minimum = Date.Now.Year - 10
        nudAño.Maximum = Date.Now.Year + 10
        nudAño.Value = Date.Now.Year

        'Mes
        cbxMes.SelectedIndex = Date.Now.Month - 1

        cbxInforme.SelectedIndex = 0

    End Sub

    Sub RecalcularSaldos()

        Dim Procesados As String = ""
        Dim Tabla As String = "SpRecalcularPlanCuentaSaldo"


        For i As Integer = 1 To 12

            Dim SQL As String = "Exec " & Tabla & " @Año='" & nudAño.Value & "', @Mes='" & i & "' "
            Dim dttemp As DataTable = CSistema.ExecuteToDataTable(SQL, "", 1000)

            If dttemp Is Nothing Then
                MessageBox.Show("Error en la consulta...", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            If dttemp.Rows.Count = 0 Then
                MessageBox.Show("Error en la consulta...", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            Dim Resultado As DataRow = dttemp.Rows(0)

            If Resultado("Procesado") = False Then
                MessageBox.Show(Resultado("Mensaje"), "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            Dim Registros As Integer = Resultado("Registros")

        Next

        'MessageBox.Show("Saldos Recalculados", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)



    End Sub

    Sub Cargar()

        'Validar que no se ejecute el kardex
        If CBool(CSistema.ExecuteScalar("Select top(1) BloquearBalance from KardexBloquearOperaciones")) Then
            MessageBox.Show("El sistema esta regenerando Kardex y Asientos contables. Favor vuelva a intentar mas tarde", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        'Se quita el regenerar saldos por pedido del directorio(Karin)
        'If chkRegenerar.Valor Then
        '    RecalcularSaldos()
        'End If

        Select Case cbxInforme.SelectedIndex
            Case 0
                'Comentado para prueba de ordenamiento 
                dtPlanCuenta = CSistema.ExecuteToDataTable("Select Codigo, Descripcion, CodigoPadre, Categoria, Imputable, SaldoMes, SaldoAcumulado, MostrarCodigo From VBalanceGeneral Where PlanCuentaTitular='True' Order By Codigo ")
                'dtPlanCuenta = CSistema.ExecuteToDataTable("Select Codigo, Descripcion, CodigoPadre, Categoria, Imputable, SaldoMes, SaldoAcumulado, MostrarCodigo,Codigo1 From VBalanceGeneral Where PlanCuentaTitular='True' Order By Codigo ")
            Case 1
                'Comentado para prueba de ordenamiento 
                dtPlanCuenta = CSistema.ExecuteToDataTable("Select Codigo, Descripcion, CodigoPadre, Categoria, Imputable, SaldoMes, SaldoAcumulado, MostrarCodigo From VEstadoResultado Where PlanCuentaTitular='True' Order By Codigo ")
                'dtPlanCuenta = CSistema.ExecuteToDataTable("Select Codigo, Descripcion, CodigoPadre, Categoria, Imputable, SaldoMes, SaldoAcumulado, MostrarCodigo,Codigo1 From VEstadoResultado Where PlanCuentaTitular='True' Order By Codigo ")
            Case 2
                dtPlanCuenta = CSistema.ExecuteToDataTable("Select Codigo, Descripcion, CodigoPadre, Categoria, Imputable, [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], Acumulado, MostrarCodigo, Año, Mes From VEstadoResultadoMulticolumna Where PlanCuentaTitular='True' Order By Codigo ")

            Case 3
                dtPlanCuenta = CSistema.ExecuteToDataTable("Select Codigo, Descripcion, CodigoPadre, Categoria, Imputable, SaldoMes, SaldoAcumulado, MostrarCodigo, Seccion From VLibroInventario2 Where PlanCuentaTitular='True' Order By Codigo ")

            Case 4
                dtPlanCuenta = CSistema.ExecuteToDataTable("Select Codigo, Descripcion, CodigoPadre, Categoria, Imputable, SaldoMes, SaldoAcumulado, MostrarCodigo From VBalanceGeneralyEstadoResultado Where PlanCuentaTitular='True' Order By Codigo ")

        End Select

        Dim SQL As String = "Exec StoreProcedure "
        CSistema.ConcatenarParametro(SQL, "@Año", nudAño.Value)
        CSistema.ConcatenarParametro(SQL, "@Mes", cbxMes.SelectedIndex + 1)

        If chkUnidadNegocio.Valor = True Then
            CSistema.ConcatenarParametro(SQL, "@IDUnidadNegocio", cbxUnidadNegocio.GetValue)
        End If

        CSistema.ConcatenarParametro(SQL, "@Factor", 1)

        Select Case cbxInforme.SelectedIndex
            Case 0
                'Comentado para prueba de ordenamiento 
                SQL = SQL.Replace("StoreProcedure", "SpViewBalanceGeneral")
                'SQL = SQL.Replace("StoreProcedure", "SpViewBalanceGeneralPrueba")
                dtPlanCuentaSaldos = CSistema.ExecuteToDataTable(SQL)
                EstablecerValores()
                Suprimir(dtPlanCuenta, "SaldoAcumulado")
            Case 1
                'Comentado para prueba de ordenamiento 
                SQL = SQL.Replace("StoreProcedure", "SpViewEstadoResultado")
                'SQL = SQL.Replace("StoreProcedure", "SpViewEstadoResultadoPrueba")
                dtPlanCuentaSaldos = CSistema.ExecuteToDataTable(SQL)
                EstablecerValores()
                Suprimir(dtPlanCuenta, "SaldoAcumulado")

            Case 2
                SQL = SQL.Replace("StoreProcedure", "SpViewEstadoResultadoMulticolumna")
                CSistema.ConcatenarParametro(SQL, "@MesHasta", cbxMesHasta.SelectedIndex + 1)
                dtPlanCuentaSaldos = CSistema.ExecuteToDataTable(SQL)
                EstablecerValoresMultiColumna()
                Suprimir(dtPlanCuenta, "Acumulado")


            Case 3
                SQL = SQL.Replace("StoreProcedure", "SpViewLibroInventario")
                dtPlanCuentaSaldos = CSistema.ExecuteToDataTable(SQL)
                EstablecerValores()
                Suprimir(dtPlanCuenta, "SaldoAcumulado")
                FormatoImpresion(dtPlanCuenta)

            Case 4
                'SQL = SQL.Replace("StoreProcedure", "SpViewBalanceGeneralyEstadoResultado1")
                SQL = SQL.Replace("StoreProcedure", "SpViewBalanceGeneralyEstadoResultado")
                dtPlanCuentaSaldos = CSistema.ExecuteToDataTable(SQL)
                EstablecerValores()
                Suprimir(dtPlanCuenta, "SaldoAcumulado")

        End Select

        For Each oRow As DataRow In dtPlanCuenta.Rows
            If chkIncluirCodigo.Valor = False Then
                oRow("MostrarCodigo") = "0"
            Else
                oRow("MostrarCodigo") = "1"
            End If
        Next

        Dim Subtitulo As String
        If cbxInforme.SelectedIndex = 2 Then
            Subtitulo = "Año: " & nudAño.Value & " Mes desde: " & cbxMes.Text & " hasta: " & cbxMesHasta.Text
        Else
            Subtitulo = "Año: " & nudAño.Value & " Mes: " & cbxMes.Text
        End If

        If chkUnidadNegocio.Valor = True Then
            Subtitulo = Subtitulo & " - " & cbxUnidadNegocio.cbx.Text
        End If

        If cbxInforme.SelectedIndex = 0 Then
            CReporte.ImprimirBalanceGeneral(frmReporte, dtPlanCuenta, "BALANCE GENERAL", Subtitulo, vgUsuarioIdentificador)
        End If

        If cbxInforme.SelectedIndex = 1 Then
            CReporte.ImprimirBalanceGeneral(frmReporte, dtPlanCuenta, "ESTADO DE RESULTADO", Subtitulo, vgUsuarioIdentificador)
        End If

        If cbxInforme.SelectedIndex = 2 Then
            CReporte.ImprimirBalanceGeneral(frmReporte, dtPlanCuenta, "ESTADO DE RESULTADO MULTICOLUMNA", Subtitulo, vgUsuarioIdentificador, False, True)
        End If

        If cbxInforme.SelectedIndex = 3 Then
            Dim NumeroInicial As Integer = IIf(IsNumeric(txtNumeroInicial.Text), CInt(txtNumeroInicial.Text), 0)
            CReporte.ImprimirLibroInventario(frmReporte, dtPlanCuenta, NumeroInicial - 1)
        End If

        If cbxInforme.SelectedIndex = 4 Then
            CReporte.ImprimirBalanceGeneralyEstadoResultado(frmReporte, dtPlanCuenta, "BALANCE GENERAL Y ESTADO DE RESULTADO", Subtitulo, vgUsuarioIdentificador)
        End If

    End Sub

    Sub Suprimir(ByVal dt As DataTable, ByVal campo As String)

        If chkIncluirSinSaldo.Valor = True Then
            Exit Sub
        End If


        Dim rowsToRemove() As DataRow = dt.Select("" & campo & " = '0' ")

        If rowsToRemove IsNot Nothing AndAlso rowsToRemove.Length > 0 Then
            For i As Integer = 0 To rowsToRemove.Length - 1
                rowsToRemove(i).Delete()
            Next
        End If

        dt.AcceptChanges()

    End Sub

    Sub FormatoImpresion(ByVal dt As DataTable)

        Dim rowsToRemove() As DataRow = dt.Select(" Imputable = 'True' ")

        If rowsToRemove IsNot Nothing AndAlso rowsToRemove.Length > 0 Then
            For i As Integer = 0 To rowsToRemove.Length - 1
                If (rowsToRemove(i).Item("Seccion") <> "I N G R E S O S") And rowsToRemove(i).Item("Seccion") <> "E G R E S O S" Then
                    rowsToRemove(i).Delete()
                End If

            Next
        End If

        dt.AcceptChanges()

    End Sub

    Sub EstablecerValores()

        MigrarValores()

        For Each oRow As DataRow In dtPlanCuenta.Select(" Categoria = '1' ")

            Dim SaldoMes As Decimal = oRow("SaldoMes")
            Dim SaldoAcumulado As Decimal = oRow("SaldoAcumulado")

            oRow("SaldoMes") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "SaldoMes"))
            oRow("SaldoAcumulado") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "SaldoAcumulado"))

        Next

    End Sub

    Sub EstablecerValoresMultiColumna()

        MigrarValores(True)

        For Each oRow As DataRow In dtPlanCuenta.Select(" Categoria = '1' ")
            'If dtPlanCuenta.Select("Codigo = 541030407") Then
            oRow("1") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "1"))
                oRow("2") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "2"))
                oRow("3") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "3"))
                oRow("4") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "4"))
                oRow("5") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "5"))
                oRow("6") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "6"))
                oRow("7") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "7"))
                oRow("8") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "8"))
                oRow("9") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "9"))
                oRow("10") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "10"))
                oRow("11") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "11"))
                oRow("12") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "12"))
                oRow("Acumulado") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "Acumulado"))
            'End If
        Next

        'Agregar Resultado
        Dim ResultadoRow As DataRow = dtPlanCuenta.NewRow
        ResultadoRow("Categoria") = 0

        For i As Integer = 0 To 12

            Dim Ingresos As Decimal = 0
            Dim Egresos As Decimal = 0

            'Acumulado si es 0
            If i = 0 Then
                Ingresos = dtPlanCuenta.Select(" Categoria=1 And Codigo='4' ")(0)("Acumulado")
                Egresos = dtPlanCuenta.Select(" Categoria=1 And Codigo='5' ")(0)("Acumulado")
                ResultadoRow("Acumulado") = Ingresos - Egresos
            Else
                Ingresos = dtPlanCuenta.Select(" Categoria=1 And Codigo='4' ")(0)("" & i.ToString & "")
                Egresos = dtPlanCuenta.Select(" Categoria=1 And Codigo='5' ")(0)("" & i.ToString & "")
                ResultadoRow("" & i.ToString & "") = Ingresos - Egresos
            End If

        Next

        dtPlanCuenta.Rows.Add(ResultadoRow)

    End Sub

    Sub MigrarValores(Optional MultiColumna As Boolean = False)

        If MultiColumna = False Then
            For Each oRow As DataRow In dtPlanCuenta.Rows

                oRow("SaldoMes") = "0"
                oRow("SaldoAcumulado") = "0"

                For Each orow2 As DataRow In dtPlanCuentaSaldos.Select(" Codigo = '" & oRow("Codigo") & "' ")
                    oRow("SaldoMes") = orow2("MovimientoMes")
                    oRow("SaldoAcumulado") = orow2("SaldoActual")
                Next

            Next
        End If

        If MultiColumna = True Then
            For Each oRow As DataRow In dtPlanCuenta.Rows

                oRow("1") = "0"
                oRow("2") = "0"
                oRow("3") = "0"
                oRow("4") = "0"
                oRow("5") = "0"
                oRow("6") = "0"
                oRow("7") = "0"
                oRow("8") = "0"
                oRow("9") = "0"
                oRow("10") = "0"
                oRow("11") = "0"
                oRow("12") = "0"
                oRow("Acumulado") = "0"

                For Each orow2 As DataRow In dtPlanCuentaSaldos.Select(" Codigo = '" & oRow("Codigo") & "' ")
                    oRow("1") = orow2("1")
                    oRow("2") = orow2("2")
                    oRow("3") = orow2("3")
                    oRow("4") = orow2("4")
                    oRow("5") = orow2("5")
                    oRow("6") = orow2("6")
                    oRow("7") = orow2("7")
                    oRow("8") = orow2("8")
                    oRow("9") = orow2("9")
                    oRow("10") = orow2("10")
                    oRow("11") = orow2("11")
                    oRow("12") = orow2("12")
                    oRow("Acumulado") = orow2("Acumulado")
                Next

            Next
        End If

    End Sub

    Sub Valores(Codigo As String, Saldo As Decimal, Optional Mes As Integer = 0)

        Dim Filtro As String = " Codigo = '" & Codigo & "' "
        If Mes > 0 Then
            Filtro = Filtro & " And Mes = " & Mes
        End If

        For Each oRow As DataRow In dtPlanCuenta.Select(Filtro)

            If IsNumeric(oRow("SaldoMes")) = False Then
                oRow("SaldoMes") = 0
            End If

            oRow("SaldoMes") = CDec(oRow("SaldoMes")) + Saldo

            If oRow("CodigoPadre") <> "---" Then
                Valores(oRow("CodigoPadre"), Saldo, Mes)
            End If

        Next

    End Sub

    Function ObtenerValor(ByVal Codigo As String, ByVal Campo As String) As Decimal

        ObtenerValor = 0

        'Verificar si tiene
        Dim Saldo As Decimal = 0

        For Each oRow As DataRow In dtPlanCuenta.Select(" CodigoPadre = '" & Codigo & "' ")

            Saldo = CDec(ObtenerValor(oRow("Codigo"), Campo))

            If IsNumeric(oRow(Campo)) = False Then
                oRow(Campo) = 0
            End If

            Saldo = Saldo + oRow(Campo)
            oRow(Campo) = CSistema.FormatoMoneda(Saldo)

            ObtenerValor = ObtenerValor + Saldo

        Next

    End Function

    Private Sub frmBalanceGeneralEstadoResultado_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmBalanceGeneralEstadoResultado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Cargar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Cargar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkUnidadNegocio.PropertyChanged
        If chkUnidadNegocio.chk.Checked = True Then
            cbxUnidadNegocio.Enabled = True
        Else
            cbxUnidadNegocio.Enabled = False
        End If
    End Sub

    Private Sub cbxInforme_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxInforme.SelectedIndexChanged
        Select Case cbxInforme.SelectedIndex
            Case 0
                cbxMesHasta.Text = ""
                cbxMesHasta.Enabled = False
                cbxMes.Enabled = True
                chkIncluirCodigo.Enabled = True
                cbxMes.SelectedIndex = Date.Now.Month - 1
                txtNumeroInicial.Enabled = False
            Case 1
                cbxMesHasta.Text = ""
                cbxMesHasta.Enabled = False
                cbxMes.Enabled = True
                chkIncluirCodigo.Enabled = True
                cbxMes.SelectedIndex = Date.Now.Month - 1
                txtNumeroInicial.Enabled = False
            Case 2
                cbxMes.SelectedIndex = 0
                cbxMesHasta.SelectedIndex = Date.Now.Month - 1
                cbxMesHasta.Enabled = True
                cbxMes.Enabled = True
                chkIncluirCodigo.Enabled = False
                txtNumeroInicial.Enabled = False
            Case 3
                cbxMes.SelectedIndex = 11
                cbxMesHasta.SelectedIndex = 11
                cbxMes.Enabled = False
                cbxMesHasta.Enabled = False
                chkIncluirCodigo.Enabled = False
                txtNumeroInicial.Enabled = True
        End Select
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBalanceComprobacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblMes = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.cbxInforme = New System.Windows.Forms.ComboBox()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.lblFactor = New System.Windows.Forms.Label()
        Me.lblCuentaInicial = New System.Windows.Forms.Label()
        Me.lblCuentaFinal = New System.Windows.Forms.Label()
        Me.gbxOpciones = New System.Windows.Forms.GroupBox()
        Me.nudAño = New System.Windows.Forms.NumericUpDown()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.chkIncluirCodigo = New ERP.ocxCHK()
        Me.chkIncluirSinSaldo = New ERP.ocxCHK()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.txtCuentaFinal = New ERP.ocxTXTCuentaContable()
        Me.txtCuentaInicial = New ERP.ocxTXTCuentaContable()
        Me.txtFactor = New ERP.ocxTXTNumeric()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.gbxOpciones.SuspendLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMes
        '
        Me.lblMes.AutoSize = True
        Me.lblMes.Location = New System.Drawing.Point(26, 34)
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Size = New System.Drawing.Size(54, 13)
        Me.lblMes.TabIndex = 0
        Me.lblMes.Text = "Año/Mes:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(266, 283)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(97, 23)
        Me.btnAceptar.TabIndex = 1
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'cbxMes
        '
        Me.cbxMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"})
        Me.cbxMes.Location = New System.Drawing.Point(175, 30)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(125, 21)
        Me.cbxMes.TabIndex = 2
        '
        'cbxInforme
        '
        Me.cbxInforme.FormattingEnabled = True
        Me.cbxInforme.Location = New System.Drawing.Point(111, 55)
        Me.cbxInforme.Name = "cbxInforme"
        Me.cbxInforme.Size = New System.Drawing.Size(189, 21)
        Me.cbxInforme.TabIndex = 4
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(26, 59)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(45, 13)
        Me.lblInforme.TabIndex = 3
        Me.lblInforme.Text = "Informe:"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(26, 113)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 7
        Me.lblMoneda.Text = "Moneda:"
        '
        'lblFactor
        '
        Me.lblFactor.AutoSize = True
        Me.lblFactor.Location = New System.Drawing.Point(26, 140)
        Me.lblFactor.Name = "lblFactor"
        Me.lblFactor.Size = New System.Drawing.Size(40, 13)
        Me.lblFactor.TabIndex = 9
        Me.lblFactor.Text = "Factor:"
        '
        'lblCuentaInicial
        '
        Me.lblCuentaInicial.AutoSize = True
        Me.lblCuentaInicial.Location = New System.Drawing.Point(26, 182)
        Me.lblCuentaInicial.Name = "lblCuentaInicial"
        Me.lblCuentaInicial.Size = New System.Drawing.Size(73, 13)
        Me.lblCuentaInicial.TabIndex = 13
        Me.lblCuentaInicial.Text = "Cuenta inicial:"
        '
        'lblCuentaFinal
        '
        Me.lblCuentaFinal.AutoSize = True
        Me.lblCuentaFinal.Location = New System.Drawing.Point(26, 215)
        Me.lblCuentaFinal.Name = "lblCuentaFinal"
        Me.lblCuentaFinal.Size = New System.Drawing.Size(66, 13)
        Me.lblCuentaFinal.TabIndex = 15
        Me.lblCuentaFinal.Text = "Cuenta final:"
        '
        'gbxOpciones
        '
        Me.gbxOpciones.Controls.Add(Me.nudAño)
        Me.gbxOpciones.Controls.Add(Me.lblMes)
        Me.gbxOpciones.Controls.Add(Me.chkIncluirCodigo)
        Me.gbxOpciones.Controls.Add(Me.cbxMes)
        Me.gbxOpciones.Controls.Add(Me.chkIncluirSinSaldo)
        Me.gbxOpciones.Controls.Add(Me.chkSucursal)
        Me.gbxOpciones.Controls.Add(Me.lblCuentaFinal)
        Me.gbxOpciones.Controls.Add(Me.cbxSucursal)
        Me.gbxOpciones.Controls.Add(Me.txtCuentaFinal)
        Me.gbxOpciones.Controls.Add(Me.txtCuentaInicial)
        Me.gbxOpciones.Controls.Add(Me.lblCuentaInicial)
        Me.gbxOpciones.Controls.Add(Me.lblInforme)
        Me.gbxOpciones.Controls.Add(Me.lblFactor)
        Me.gbxOpciones.Controls.Add(Me.cbxInforme)
        Me.gbxOpciones.Controls.Add(Me.txtFactor)
        Me.gbxOpciones.Controls.Add(Me.cbxMoneda)
        Me.gbxOpciones.Controls.Add(Me.lblMoneda)
        Me.gbxOpciones.Location = New System.Drawing.Point(12, 12)
        Me.gbxOpciones.Name = "gbxOpciones"
        Me.gbxOpciones.Size = New System.Drawing.Size(471, 251)
        Me.gbxOpciones.TabIndex = 0
        Me.gbxOpciones.TabStop = False
        Me.gbxOpciones.Text = "Opciones"
        '
        'nudAño
        '
        Me.nudAño.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAño.Location = New System.Drawing.Point(111, 30)
        Me.nudAño.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.nudAño.Name = "nudAño"
        Me.nudAño.Size = New System.Drawing.Size(58, 20)
        Me.nudAño.TabIndex = 1
        Me.nudAño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudAño.ThousandsSeparator = True
        Me.nudAño.Value = New Decimal(New Integer() {2013, 0, 0, 0})
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(383, 283)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'chkIncluirCodigo
        '
        Me.chkIncluirCodigo.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirCodigo.Color = System.Drawing.Color.Empty
        Me.chkIncluirCodigo.Location = New System.Drawing.Point(210, 136)
        Me.chkIncluirCodigo.Name = "chkIncluirCodigo"
        Me.chkIncluirCodigo.Size = New System.Drawing.Size(152, 21)
        Me.chkIncluirCodigo.SoloLectura = False
        Me.chkIncluirCodigo.TabIndex = 12
        Me.chkIncluirCodigo.Texto = "Incluir codigos"
        Me.chkIncluirCodigo.Valor = True
        '
        'chkIncluirSinSaldo
        '
        Me.chkIncluirSinSaldo.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirSinSaldo.Color = System.Drawing.Color.Empty
        Me.chkIncluirSinSaldo.Location = New System.Drawing.Point(210, 109)
        Me.chkIncluirSinSaldo.Name = "chkIncluirSinSaldo"
        Me.chkIncluirSinSaldo.Size = New System.Drawing.Size(152, 21)
        Me.chkIncluirSinSaldo.SoloLectura = False
        Me.chkIncluirSinSaldo.TabIndex = 11
        Me.chkIncluirSinSaldo.Texto = "Incluir cuentas sin saldos"
        Me.chkIncluirSinSaldo.Valor = False
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(26, 82)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(74, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 5
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(111, 82)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(189, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 6
        Me.cbxSucursal.Texto = "ASUNCION - CENTRAL"
        '
        'txtCuentaFinal
        '
        Me.txtCuentaFinal.AlturaMaxima = 0
        Me.txtCuentaFinal.Consulta = Nothing
        Me.txtCuentaFinal.ListarTodas = False
        Me.txtCuentaFinal.Location = New System.Drawing.Point(111, 208)
        Me.txtCuentaFinal.Name = "txtCuentaFinal"
        Me.txtCuentaFinal.Registro = Nothing
        Me.txtCuentaFinal.Resolucion173 = False
        Me.txtCuentaFinal.Seleccionado = False
        Me.txtCuentaFinal.Size = New System.Drawing.Size(349, 27)
        Me.txtCuentaFinal.SoloLectura = False
        Me.txtCuentaFinal.TabIndex = 16
        Me.txtCuentaFinal.Texto = Nothing
        '
        'txtCuentaInicial
        '
        Me.txtCuentaInicial.AlturaMaxima = 0
        Me.txtCuentaInicial.Consulta = Nothing
        Me.txtCuentaInicial.ListarTodas = False
        Me.txtCuentaInicial.Location = New System.Drawing.Point(111, 175)
        Me.txtCuentaInicial.Name = "txtCuentaInicial"
        Me.txtCuentaInicial.Registro = Nothing
        Me.txtCuentaInicial.Resolucion173 = False
        Me.txtCuentaInicial.Seleccionado = False
        Me.txtCuentaInicial.Size = New System.Drawing.Size(349, 27)
        Me.txtCuentaInicial.SoloLectura = False
        Me.txtCuentaInicial.TabIndex = 14
        Me.txtCuentaInicial.Texto = "d"
        '
        'txtFactor
        '
        Me.txtFactor.Color = System.Drawing.Color.Empty
        Me.txtFactor.Decimales = True
        Me.txtFactor.Indicaciones = Nothing
        Me.txtFactor.Location = New System.Drawing.Point(111, 136)
        Me.txtFactor.Name = "txtFactor"
        Me.txtFactor.Size = New System.Drawing.Size(69, 20)
        Me.txtFactor.SoloLectura = False
        Me.txtFactor.TabIndex = 10
        Me.txtFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFactor.Texto = "1"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(111, 109)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(69, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 8
        Me.cbxMoneda.Texto = ""
        '
        'frmBalanceComprobacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(491, 325)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gbxOpciones)
        Me.Controls.Add(Me.btnAceptar)
        Me.Name = "frmBalanceComprobacion"
        Me.Text = "frmBalanceComprobacion"
        Me.gbxOpciones.ResumeLayout(False)
        Me.gbxOpciones.PerformLayout()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblMes As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents cbxMes As System.Windows.Forms.ComboBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtCuentaInicial As ERP.ocxTXTCuentaContable
    Friend WithEvents cbxInforme As System.Windows.Forms.ComboBox
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents txtFactor As ERP.ocxTXTNumeric
    Friend WithEvents lblFactor As System.Windows.Forms.Label
    Friend WithEvents lblCuentaInicial As System.Windows.Forms.Label
    Friend WithEvents lblCuentaFinal As System.Windows.Forms.Label
    Friend WithEvents txtCuentaFinal As ERP.ocxTXTCuentaContable
    Friend WithEvents chkIncluirSinSaldo As ERP.ocxCHK
    Friend WithEvents chkIncluirCodigo As ERP.ocxCHK
    Friend WithEvents gbxOpciones As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents nudAño As System.Windows.Forms.NumericUpDown
End Class

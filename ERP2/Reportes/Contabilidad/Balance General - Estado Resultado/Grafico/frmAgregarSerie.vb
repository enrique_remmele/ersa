﻿Public Class frmAgregarSerie

    Dim CSistema As New CSistema
    Public Property IDSerie As Integer
    Public Property dtSerie As DataTable
    Public Property Modificar As Boolean


    Sub Inicializar()
        txtCuentaContable.Conectar()

        Listar()
    End Sub

    Sub Listar()

        If dtSerie Is Nothing Then
            Dim dtAux As New DataTable
            dtAux.Columns.Add("ID")
            dtAux.Columns.Add("Codigo")
            dtAux.Columns.Add("Descripcion")
            dtAux.Columns.Add("Año")
            dtSerie = dtAux.Clone
            txtAño.ReadOnly = False
        Else
            txtAño.ReadOnly = True
        End If

        CSistema.dtToGrid(DataGridView1, dtSerie)
        DataGridView1.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

    End Sub

    Sub CargarCuentaContable()
        Dim newRow As DataRow = dtSerie.NewRow

        newRow("ID") = txtCuentaContable.Registro("ID")
        newRow("Codigo") = txtCuentaContable.Registro("Codigo")
        newRow("Descripcion") = txtCuentaContable.Registro("Descripcion")
        newRow("Año") = txtAño.Value

        dtSerie.Rows.Add(newRow)
        Listar()

    End Sub

    Sub EliminarProducto()

        'Validar
        If DataGridView1.SelectedRows.Count = 0 Then
            MessageBox.Show("Seleccione correctamente el registro a eliminar!")
            Exit Sub
        End If

        Dim Cuenta As String = DataGridView1.SelectedRows(0).Cells("Codigo").Value
        Dim dtAux As DataTable = dtSerie.Clone()

        For Each Row As DataRow In dtSerie.Rows
            If Row("Codigo") = Cuenta Then
                GoTo Seguir
            End If
            Dim NewRow As DataRow = dtAux.NewRow()
            For I = 0 To dtSerie.Columns.Count - 1
                NewRow(I) = Row(I)
            Next
            dtAux.Rows.Add(NewRow)
Seguir:

        Next

        dtSerie.Clear()
        dtSerie = dtAux.Copy()

        'Volver a enumerar los ID
        Dim Index As Integer = 0
        For Each oRow As DataRow In dtSerie.Rows
            oRow("ID") = Index
            Index = Index + 1
        Next

        Listar()

    End Sub

    Private Sub frmAgregarSerie_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If txtCuentaContable.Seleccionado Then
            CargarCuentaContable()
        End If
    End Sub

    Private Sub DataGridView1_KeyUp(sender As Object, e As KeyEventArgs) Handles DataGridView1.KeyUp
        If e.KeyCode = Keys.Delete Then
            EliminarProducto()
        End If
    End Sub
End Class
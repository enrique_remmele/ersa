﻿Public Class frmGraficoContable

    Dim CSistema As New CSistema
    Dim dsSeries As New DataSet

    Sub Inicializar()
        txtCuentaContable.Conectar()
        Listar()
    End Sub

    Sub Listar()
        If OcxCHK1.chk.Checked Then
            If Not txtCuentaContable.Seleccionado Then
                MessageBox.Show("Debe seleccionar una cuenta contable", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            Chart1.DataSource = CSistema.ExecuteToDataTable("Select 
                                                        pc1.M, 
                                                        'Saldo'=Sum(pc1.Saldo)+isnull((Select Sum(pc2.Saldo)from VPlanCuentaSaldo pc2 where pc2.año = pc1.Año and pc2.Codigo = pc1.Codigo and pc2.Mes <pc1.Mes) ,0)
                                                        from vPlancuentaSaldo pc1 
                                                        where pc1.año = " & txtAño.Value & "
                                                        and pc1.Codigo = '" & txtCuentaContable.Registro("Codigo").ToString & "' 
                                                        Group by pc1.M
                                                        ,pc1.Mes
                                                        ,pc1.Año
                                                        ,pc1.Codigo
                                                        order by pc1.Mes")
            Chart1.Series("Series1").XValueMember = "M"
            Chart1.Series("Series1").YValueMembers = "Saldo"
            'Chart1.Series(0).Label = "#VALY{N0}"
            Chart1.ChartAreas(0).AxisY.LabelStyle.Format = "N0"

            Chart1.Series("Series1").Color = Color.Blue
            Chart1.Series("Series1").LegendText = txtCuentaContable.Registro("Codigo").ToString
        End If
    End Sub


    Sub Guardar()

    End Sub

    Sub AgregarSerie(Optional ByVal Modificar As Boolean = False)

        Dim frm As New frmAgregarSerie
        frm.Modificar = Modificar
        If Modificar Then
            frm.IDSerie = dgvSeries.Rows(dgvSeries.SelectedRows(0).Index).Cells("ID").Value


        End If
        frm.ShowDialog()



    End Sub

    Private Sub frmGraficoContable_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Guardar()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAgregarSerie.Click
        AgregarSerie()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnModificarSerie.Click
        If dgvSeries.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar un registro", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        AgregarSerie(True)
    End Sub

    Private Sub OcxCHK1_Load(sender As Object, e As EventArgs) Handles OcxCHK1.Load

    End Sub

    Private Sub OcxCHK1_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles OcxCHK1.PropertyChanged
        txtCuentaContable.Enabled = value
        btnRefrescar.Enabled = value
        txtAño.Enabled = value
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        Listar()

    End Sub

End Class
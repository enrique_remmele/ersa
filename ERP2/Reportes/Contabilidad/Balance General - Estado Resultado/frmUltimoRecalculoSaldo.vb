﻿Public Class frmUltimoRecalculoSaldo

    Dim CSistema As New CSistema


    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()
        lblFecha.Text = CSistema.ExecuteScalar("")
        lblHora.Text = CSistema.ExecuteScalar("")

    End Sub

    Sub Recalcular()

    End Sub


    Private Sub frmUltimoRecalculoSaldo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnSI_Click(sender As Object, e As EventArgs) Handles btnSI.Click
        Recalcular()
    End Sub

    Private Sub btnNO_Click(sender As Object, e As EventArgs) Handles btnNO.Click
        Me.Close()
    End Sub
End Class
﻿Public Class frmBalanceComprobacion

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CReporte As New ERP.Reporte.CReporteContabilidad

    'VARIABLES
    Dim dtPlanCuenta As DataTable
    Dim dtPlanCuentaSaldos As DataTable

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Mes
        cbxMes.SelectedIndex = Date.Now.Month - 1

        'Funciones
        CargarInformacion()

        'Foco
        nudAño.Focus()

    End Sub

    Sub CargarInformacion()

        'Año
        nudAño.Minimum = Date.Now.Year - 10
        nudAño.Maximum = Date.Now.Year + 10
        nudAño.Value = Date.Now.Year

        'Cuentas
        txtCuentaFinal.ListarTodas = True
        txtCuentaInicial.ListarTodas = True
        txtCuentaFinal.Conectar()
        txtCuentaInicial.Conectar()

        txtCuentaInicial.SetValue("1")
        txtCuentaFinal.SetMaxValue()

        'Informe
        cbxInforme.Items.Add("3 COLUMNAS")
        cbxInforme.Items.Add("4 COLUMNAS")
        cbxInforme.DropDownStyle = ComboBoxStyle.DropDownList

        'Moneda
        CSistema.SqlToComboBox(cbxMoneda.cbx, CData.GetTable("VMoneda"), "ID", "Referencia")

    End Sub

    Sub Cargar()

        If cbxInforme.SelectedIndex = -1 Then
            Exit Sub
        End If

        dtPlanCuenta = CSistema.ExecuteToDataTable("Select Codigo, 'Denominacion'=Descripcion, CodigoPadre, Categoria, Imputable From VBalanceComprobacion Where PlanCuentaTitular='True' Order By Codigo ")
        dtPlanCuenta.Columns.Add("SaldoAnterior")
        dtPlanCuenta.Columns.Add("Debito")
        dtPlanCuenta.Columns.Add("Credito")
        dtPlanCuenta.Columns.Add("SaldoActual")
        dtPlanCuenta.Columns.Add("MovimientoMes")
        dtPlanCuenta.Columns.Add("MostrarCodigo")

        Dim SQL As String = "Exec StoreProcedure "
        CSistema.ConcatenarParametro(SQL, "@Año", nudAño.Value)
        CSistema.ConcatenarParametro(SQL, "@Mes", cbxMes.SelectedIndex + 1)
        CSistema.ConcatenarParametro(SQL, "@Factor", txtFactor.ObtenerValor)
        CSistema.ConcatenarParametro(SQL, "@CuentaInicial", txtCuentaInicial.Registro("Codigo"))
        CSistema.ConcatenarParametro(SQL, "@CuentaFinal", txtCuentaFinal.Registro("Codigo"))
        If chkSucursal.Valor = True Then
            CSistema.ConcatenarParametro(SQL, "@IDSucursal", cbxSucursal.GetValue)
        End If

        Select Case cbxInforme.SelectedIndex
            Case 0
                If chkSucursal.Valor = False Then
                    SQL = SQL.Replace("StoreProcedure", "SpViewBalanceComprobacion3")
                Else
                    SQL = SQL.Replace("StoreProcedure", "SpViewBalanceComprobacionSucursal3")
                End If

                dtPlanCuentaSaldos = CSistema.ExecuteToDataTable(SQL)
                EstablecerValores3()
                Suprimir(dtPlanCuenta, "SaldoActual")

            Case 1
                If chkSucursal.Valor = False Then
                    SQL = SQL.Replace("StoreProcedure", "SpViewBalanceComprobacion4")
                Else
                    SQL = SQL.Replace("StoreProcedure", "SpViewBalanceComprobacionSucursal4")
                End If
                dtPlanCuentaSaldos = CSistema.ExecuteToDataTable(SQL)
                EstablecerValores4()
                Suprimir(dtPlanCuenta, "SaldoActual")
        End Select

        For Each oRow As DataRow In dtPlanCuenta.Rows
            If chkIncluirCodigo.Valor = False Then
                oRow("MostrarCodigo") = "0"
            Else
                oRow("MostrarCodigo") = "1"
            End If
        Next

        Dim SubTitulo As String = "PERIODO: " & cbxMes.Text & " " & nudAño.Value & " - " & cbxMoneda.Texto.ToUpper

        If cbxInforme.SelectedIndex = 1 Then
            CReporte.ImprimirBalanceComprobacion4(frmReporte, dtPlanCuenta, "BALANCE DE COMPROBACION", SubTitulo, vgUsuarioIdentificador)
        End If

        If cbxInforme.SelectedIndex = 0 Then
            CReporte.ImprimirBalanceComprobacion3(frmReporte, dtPlanCuenta, "BALANCE DE COMPROBACION", SubTitulo, vgUsuarioIdentificador)
        End If


    End Sub

    Sub MigrarValores4()

        For Each oRow As DataRow In dtPlanCuenta.Rows
            oRow("SaldoAnterior") = "0"
            oRow("Debito") = "0"
            oRow("Credito") = "0"
            oRow("SaldoActual") = "0"
            For Each orow2 As DataRow In dtPlanCuentaSaldos.Select(" Codigo = '" & oRow("Codigo") & "'")
                oRow("SaldoAnterior") = orow2("SaldoAnterior")
                oRow("Debito") = orow2("Debito")
                oRow("Credito") = orow2("Credito")
                oRow("SaldoActual") = orow2("SaldoActual")
            Next
        Next

    End Sub

    Sub MigrarValores3()

        For Each oRow As DataRow In dtPlanCuenta.Rows
            oRow("SaldoAnterior") = "0"
            oRow("MovimientoMes") = "0"
            oRow("SaldoActual") = "0"
            For Each orow2 As DataRow In dtPlanCuentaSaldos.Select(" Codigo = '" & oRow("Codigo") & "'")
                oRow("SaldoAnterior") = orow2("SaldoAnterior")
                oRow("MovimientoMes") = orow2("MovimientoMes")
                oRow("SaldoActual") = orow2("SaldoActual")
            Next
        Next

    End Sub

    Sub EstablecerValores3()

        MigrarValores3()

        For Each oRow As DataRow In dtPlanCuenta.Select(" Categoria = '1' ")
            oRow("SaldoAnterior") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "SaldoAnterior"))
            oRow("MovimientoMes") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "MovimientoMes"))
            oRow("SaldoActual") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "SaldoActual"))
        Next

    End Sub

    Sub EstablecerValores4()

        MigrarValores4()

        For Each oRow As DataRow In dtPlanCuenta.Select(" Categoria = '1' ")
            oRow("SaldoAnterior") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "SaldoAnterior"))
            oRow("Debito") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "Debito"))
            oRow("Credito") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "Credito"))
            oRow("SaldoActual") = CSistema.FormatoMoneda(ObtenerValor(oRow("Codigo"), "SaldoActual"))
        Next

    End Sub

    Function ObtenerValor(Codigo As String, Valor As String) As Decimal

        ObtenerValor = 0

        'Verificar si tiene
        Dim Saldo As Decimal = 0

        For Each oRow As DataRow In dtPlanCuenta.Select(" CodigoPadre = '" & Codigo & "' ")

            Saldo = CDec(ObtenerValor(oRow("Codigo"), Valor))

            If IsNumeric(oRow(Valor)) = False Then
                oRow(Valor) = 0
            End If

            Saldo = Saldo + oRow(Valor)
            oRow(Valor) = CSistema.FormatoMoneda(Saldo)

            ObtenerValor = ObtenerValor + Saldo

        Next

    End Function

    Sub Suprimir(ByVal dt As DataTable, ByVal campo As String)

        If chkIncluirSinSaldo.Valor = True Then
            Exit Sub
        End If

        Dim rowsToRemove() As DataRow = dt.Select("" & campo & " = '0' ")

        If rowsToRemove IsNot Nothing AndAlso rowsToRemove.Length > 0 Then
            For i As Integer = 0 To rowsToRemove.Length - 1
                rowsToRemove(i).Delete()
            Next
        End If

        dt.AcceptChanges()

    End Sub

    Private Sub frmBalanceComprobacion_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown, Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmBalanceComprobacion_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        Cargar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub txtCuentaInicial_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuentaInicial.ItemSeleccionado
        txtCuentaFinal.Focus()
    End Sub

    Private Sub txtCuentaFinal_ItemSeleccionado(sender As System.Object, e As System.EventArgs) Handles txtCuentaFinal.ItemSeleccionado
        btnAceptar.Focus()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub
End Class
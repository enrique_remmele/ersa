﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBalanceResolucion49
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nudAño = New System.Windows.Forms.NumericUpDown()
        Me.lblMes = New System.Windows.Forms.Label()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.gbxOpciones = New System.Windows.Forms.GroupBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxOpciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(0, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Mes:"
        '
        'nudAño
        '
        Me.nudAño.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAño.Location = New System.Drawing.Point(100, 23)
        Me.nudAño.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.nudAño.Name = "nudAño"
        Me.nudAño.Size = New System.Drawing.Size(69, 26)
        Me.nudAño.TabIndex = 3
        Me.nudAño.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudAño.ThousandsSeparator = True
        Me.nudAño.Value = New Decimal(New Integer() {2023, 0, 0, 0})
        '
        'lblMes
        '
        Me.lblMes.AutoSize = True
        Me.lblMes.Location = New System.Drawing.Point(0, 30)
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Size = New System.Drawing.Size(29, 13)
        Me.lblMes.TabIndex = 2
        Me.lblMes.Text = "Año:"
        '
        'cbxMes
        '
        Me.cbxMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxMes.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Items.AddRange(New Object() {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"})
        Me.cbxMes.Location = New System.Drawing.Point(100, 55)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(165, 26)
        Me.cbxMes.TabIndex = 5
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(300, 145)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'gbxOpciones
        '
        Me.gbxOpciones.Controls.Add(Me.Label1)
        Me.gbxOpciones.Controls.Add(Me.nudAño)
        Me.gbxOpciones.Controls.Add(Me.lblMes)
        Me.gbxOpciones.Controls.Add(Me.cbxMes)
        Me.gbxOpciones.Location = New System.Drawing.Point(15, 20)
        Me.gbxOpciones.Name = "gbxOpciones"
        Me.gbxOpciones.Size = New System.Drawing.Size(365, 109)
        Me.gbxOpciones.TabIndex = 3
        Me.gbxOpciones.TabStop = False
        Me.gbxOpciones.Text = "Opciones"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(183, 145)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(97, 23)
        Me.btnAceptar.TabIndex = 4
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'frmBalanceResolucion49
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(395, 189)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gbxOpciones)
        Me.Controls.Add(Me.btnAceptar)
        Me.Name = "frmBalanceResolucion49"
        Me.Text = "frmBalanceResolucion49"
        CType(Me.nudAño, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxOpciones.ResumeLayout(False)
        Me.gbxOpciones.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents nudAño As NumericUpDown
    Friend WithEvents lblMes As Label
    Friend WithEvents cbxMes As ComboBox
    Friend WithEvents btnCancelar As Button
    Friend WithEvents gbxOpciones As GroupBox
    Friend WithEvents btnAceptar As Button
End Class

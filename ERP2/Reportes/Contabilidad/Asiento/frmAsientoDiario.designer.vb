﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAsientoDiario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxCliente = New System.Windows.Forms.GroupBox()
        Me.chkAplicacionVentas = New ERP.ocxCHK()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.chkCobranzaCreditoRes = New ERP.ocxCHK()
        Me.chkCobranzaCredito = New ERP.ocxCHK()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chkCobranzaContadoRes = New ERP.ocxCHK()
        Me.chkCobranzaContado = New ERP.ocxCHK()
        Me.chkNotaDebitoCliente = New ERP.ocxCHK()
        Me.chkNotaCreditoCliente = New ERP.ocxCHK()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkVentaRes = New ERP.ocxCHK()
        Me.chkVenta = New ERP.ocxCHK()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gbxProveedores = New System.Windows.Forms.GroupBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.chkGastoContadoRes = New ERP.ocxCHK()
        Me.OcxCHK1 = New ERP.ocxCHK()
        Me.OcxCHK9 = New ERP.ocxCHK()
        Me.OcxCHK8 = New ERP.ocxCHK()
        Me.OcxCHK7 = New ERP.ocxCHK()
        Me.OcxCHK6 = New ERP.ocxCHK()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.gbxBancos = New System.Windows.Forms.GroupBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.OcxCHK2 = New ERP.ocxCHK()
        Me.OcxCHK5 = New ERP.ocxCHK()
        Me.OcxCHK4 = New ERP.ocxCHK()
        Me.OcxCHK3 = New ERP.ocxCHK()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.chkDepositoRes = New ERP.ocxCHK()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.chkUsuario = New ERP.ocxCHK()
        Me.OcxTXTCuentaContable1 = New ERP.ocxTXTCuentaContable()
        Me.chkCuentaContable = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxCentroCosto = New ERP.ocxCBX()
        Me.chkCentroCosto = New ERP.ocxCHK()
        Me.rdbContabilidad = New System.Windows.Forms.RadioButton()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.rdbAdministracion = New System.Windows.Forms.RadioButton()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxStock = New System.Windows.Forms.GroupBox()
        Me.OcxCHK15 = New ERP.ocxCHK()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.OcxCHK14 = New ERP.ocxCHK()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.OcxCHK13 = New ERP.ocxCHK()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.OcxCHK12 = New ERP.ocxCHK()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.OcxCHK11 = New ERP.ocxCHK()
        Me.OcxCHK10 = New ERP.ocxCHK()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.btnContabilidad = New System.Windows.Forms.Button()
        Me.chkOtros = New ERP.ocxCHK()
        Me.chkSeleccionarTodo = New ERP.ocxCHK()
        Me.chkAsientosConDiferencia = New ERP.ocxCHK()
        Me.gbxCliente.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxProveedores.SuspendLayout()
        Me.gbxBancos.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.gbxStock.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(190, 581)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(90, 23)
        Me.btnListar.TabIndex = 9
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(21, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(292, 581)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 10
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'gbxCliente
        '
        Me.gbxCliente.Controls.Add(Me.chkAplicacionVentas)
        Me.gbxCliente.Controls.Add(Me.Label31)
        Me.gbxCliente.Controls.Add(Me.Label13)
        Me.gbxCliente.Controls.Add(Me.chkCobranzaCreditoRes)
        Me.gbxCliente.Controls.Add(Me.chkCobranzaCredito)
        Me.gbxCliente.Controls.Add(Me.Label8)
        Me.gbxCliente.Controls.Add(Me.chkCobranzaContadoRes)
        Me.gbxCliente.Controls.Add(Me.chkCobranzaContado)
        Me.gbxCliente.Controls.Add(Me.chkNotaDebitoCliente)
        Me.gbxCliente.Controls.Add(Me.chkNotaCreditoCliente)
        Me.gbxCliente.Controls.Add(Me.Label3)
        Me.gbxCliente.Controls.Add(Me.chkVentaRes)
        Me.gbxCliente.Controls.Add(Me.chkVenta)
        Me.gbxCliente.Controls.Add(Me.Label19)
        Me.gbxCliente.Controls.Add(Me.Label17)
        Me.gbxCliente.Controls.Add(Me.Label5)
        Me.gbxCliente.Controls.Add(Me.Label4)
        Me.gbxCliente.Controls.Add(Me.Label2)
        Me.gbxCliente.Controls.Add(Me.Label1)
        Me.gbxCliente.Location = New System.Drawing.Point(8, 215)
        Me.gbxCliente.Name = "gbxCliente"
        Me.gbxCliente.Size = New System.Drawing.Size(182, 160)
        Me.gbxCliente.TabIndex = 3
        Me.gbxCliente.TabStop = False
        '
        'chkAplicacionVentas
        '
        Me.chkAplicacionVentas.BackColor = System.Drawing.Color.Transparent
        Me.chkAplicacionVentas.Color = System.Drawing.Color.Empty
        Me.chkAplicacionVentas.Location = New System.Drawing.Point(111, 132)
        Me.chkAplicacionVentas.Name = "chkAplicacionVentas"
        Me.chkAplicacionVentas.Size = New System.Drawing.Size(13, 12)
        Me.chkAplicacionVentas.SoloLectura = False
        Me.chkAplicacionVentas.TabIndex = 18
        Me.chkAplicacionVentas.Tag = "APLICACION VENTAS DETALLADO"
        Me.chkAplicacionVentas.Texto = ""
        Me.chkAplicacionVentas.Valor = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(17, 134)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(92, 13)
        Me.Label31.TabIndex = 17
        Me.Label31.Text = "Aplicacion Ventas"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(144, 115)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(26, 13)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Res"
        '
        'chkCobranzaCreditoRes
        '
        Me.chkCobranzaCreditoRes.BackColor = System.Drawing.Color.Transparent
        Me.chkCobranzaCreditoRes.Color = System.Drawing.Color.Empty
        Me.chkCobranzaCreditoRes.Location = New System.Drawing.Point(129, 115)
        Me.chkCobranzaCreditoRes.Name = "chkCobranzaCreditoRes"
        Me.chkCobranzaCreditoRes.Size = New System.Drawing.Size(13, 12)
        Me.chkCobranzaCreditoRes.SoloLectura = False
        Me.chkCobranzaCreditoRes.TabIndex = 15
        Me.chkCobranzaCreditoRes.Tag = "COBRANZA CREDITO RESUMIDO"
        Me.chkCobranzaCreditoRes.Texto = ""
        Me.chkCobranzaCreditoRes.Valor = False
        '
        'chkCobranzaCredito
        '
        Me.chkCobranzaCredito.BackColor = System.Drawing.Color.Transparent
        Me.chkCobranzaCredito.Color = System.Drawing.Color.Empty
        Me.chkCobranzaCredito.Location = New System.Drawing.Point(111, 115)
        Me.chkCobranzaCredito.Name = "chkCobranzaCredito"
        Me.chkCobranzaCredito.Size = New System.Drawing.Size(13, 12)
        Me.chkCobranzaCredito.SoloLectura = False
        Me.chkCobranzaCredito.TabIndex = 14
        Me.chkCobranzaCredito.Tag = "COBRANZA CREDITO DETALLADO"
        Me.chkCobranzaCredito.Texto = ""
        Me.chkCobranzaCredito.Valor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(144, 95)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(26, 13)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Res"
        '
        'chkCobranzaContadoRes
        '
        Me.chkCobranzaContadoRes.BackColor = System.Drawing.Color.Transparent
        Me.chkCobranzaContadoRes.Color = System.Drawing.Color.Empty
        Me.chkCobranzaContadoRes.Location = New System.Drawing.Point(129, 95)
        Me.chkCobranzaContadoRes.Name = "chkCobranzaContadoRes"
        Me.chkCobranzaContadoRes.Size = New System.Drawing.Size(13, 12)
        Me.chkCobranzaContadoRes.SoloLectura = False
        Me.chkCobranzaContadoRes.TabIndex = 12
        Me.chkCobranzaContadoRes.Tag = "COBRANZA CONTADO RESUMIDO"
        Me.chkCobranzaContadoRes.Texto = ""
        Me.chkCobranzaContadoRes.Valor = False
        '
        'chkCobranzaContado
        '
        Me.chkCobranzaContado.BackColor = System.Drawing.Color.Transparent
        Me.chkCobranzaContado.Color = System.Drawing.Color.Empty
        Me.chkCobranzaContado.Location = New System.Drawing.Point(111, 95)
        Me.chkCobranzaContado.Name = "chkCobranzaContado"
        Me.chkCobranzaContado.Size = New System.Drawing.Size(13, 12)
        Me.chkCobranzaContado.SoloLectura = False
        Me.chkCobranzaContado.TabIndex = 10
        Me.chkCobranzaContado.Tag = "COBRANZA CONTADO DETALLADO"
        Me.chkCobranzaContado.Texto = ""
        Me.chkCobranzaContado.Valor = False
        '
        'chkNotaDebitoCliente
        '
        Me.chkNotaDebitoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkNotaDebitoCliente.Color = System.Drawing.Color.Empty
        Me.chkNotaDebitoCliente.Location = New System.Drawing.Point(111, 74)
        Me.chkNotaDebitoCliente.Name = "chkNotaDebitoCliente"
        Me.chkNotaDebitoCliente.Size = New System.Drawing.Size(13, 12)
        Me.chkNotaDebitoCliente.SoloLectura = False
        Me.chkNotaDebitoCliente.TabIndex = 8
        Me.chkNotaDebitoCliente.Tag = "NOTA CREDITO CLIENTE"
        Me.chkNotaDebitoCliente.Texto = ""
        Me.chkNotaDebitoCliente.Valor = False
        '
        'chkNotaCreditoCliente
        '
        Me.chkNotaCreditoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkNotaCreditoCliente.Color = System.Drawing.Color.Empty
        Me.chkNotaCreditoCliente.Location = New System.Drawing.Point(111, 53)
        Me.chkNotaCreditoCliente.Name = "chkNotaCreditoCliente"
        Me.chkNotaCreditoCliente.Size = New System.Drawing.Size(13, 12)
        Me.chkNotaCreditoCliente.SoloLectura = False
        Me.chkNotaCreditoCliente.TabIndex = 6
        Me.chkNotaCreditoCliente.Tag = "NOTA DEBITO CLIENTE"
        Me.chkNotaCreditoCliente.Texto = ""
        Me.chkNotaCreditoCliente.Valor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(144, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(26, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Res"
        '
        'chkVentaRes
        '
        Me.chkVentaRes.BackColor = System.Drawing.Color.Transparent
        Me.chkVentaRes.Color = System.Drawing.Color.Empty
        Me.chkVentaRes.Location = New System.Drawing.Point(129, 34)
        Me.chkVentaRes.Name = "chkVentaRes"
        Me.chkVentaRes.Size = New System.Drawing.Size(13, 12)
        Me.chkVentaRes.SoloLectura = False
        Me.chkVentaRes.TabIndex = 3
        Me.chkVentaRes.Tag = "VENTA RESUMIDA"
        Me.chkVentaRes.Texto = ""
        Me.chkVentaRes.Valor = False
        '
        'chkVenta
        '
        Me.chkVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkVenta.Color = System.Drawing.Color.Empty
        Me.chkVenta.Location = New System.Drawing.Point(111, 34)
        Me.chkVenta.Name = "chkVenta"
        Me.chkVenta.Size = New System.Drawing.Size(13, 12)
        Me.chkVenta.SoloLectura = False
        Me.chkVenta.TabIndex = 2
        Me.chkVenta.Tag = "VENTA DETALLADA"
        Me.chkVenta.Texto = ""
        Me.chkVenta.Valor = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(19, 115)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(88, 13)
        Me.Label19.TabIndex = 13
        Me.Label19.Text = "Cobranza Crédito"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(41, 74)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(66, 13)
        Me.Label17.TabIndex = 7
        Me.Label17.Text = "Nota Crédito"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 95)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Cobranza Contado"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(43, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Nota Débito"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(67, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Ventas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(49, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "CLIENTES"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 605)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(362, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ErrorProvider1.ContainerControl = Me
        '
        'gbxProveedores
        '
        Me.gbxProveedores.Controls.Add(Me.Label25)
        Me.gbxProveedores.Controls.Add(Me.Label22)
        Me.gbxProveedores.Controls.Add(Me.chkGastoContadoRes)
        Me.gbxProveedores.Controls.Add(Me.OcxCHK1)
        Me.gbxProveedores.Controls.Add(Me.OcxCHK9)
        Me.gbxProveedores.Controls.Add(Me.OcxCHK8)
        Me.gbxProveedores.Controls.Add(Me.OcxCHK7)
        Me.gbxProveedores.Controls.Add(Me.OcxCHK6)
        Me.gbxProveedores.Controls.Add(Me.Label18)
        Me.gbxProveedores.Controls.Add(Me.Label6)
        Me.gbxProveedores.Controls.Add(Me.Label7)
        Me.gbxProveedores.Controls.Add(Me.Label9)
        Me.gbxProveedores.Controls.Add(Me.Label10)
        Me.gbxProveedores.Location = New System.Drawing.Point(196, 215)
        Me.gbxProveedores.Name = "gbxProveedores"
        Me.gbxProveedores.Size = New System.Drawing.Size(159, 160)
        Me.gbxProveedores.TabIndex = 4
        Me.gbxProveedores.TabStop = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(125, 53)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(26, 13)
        Me.Label25.TabIndex = 18
        Me.Label25.Text = "Res"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(15, 74)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(71, 13)
        Me.Label22.TabIndex = 10
        Me.Label22.Text = "Gasto Crédito"
        '
        'chkGastoContadoRes
        '
        Me.chkGastoContadoRes.BackColor = System.Drawing.Color.Transparent
        Me.chkGastoContadoRes.Color = System.Drawing.Color.Empty
        Me.chkGastoContadoRes.Location = New System.Drawing.Point(110, 53)
        Me.chkGastoContadoRes.Name = "chkGastoContadoRes"
        Me.chkGastoContadoRes.Size = New System.Drawing.Size(13, 12)
        Me.chkGastoContadoRes.SoloLectura = False
        Me.chkGastoContadoRes.TabIndex = 17
        Me.chkGastoContadoRes.Tag = "VENTA RESUMIDA"
        Me.chkGastoContadoRes.Texto = ""
        Me.chkGastoContadoRes.Valor = False
        '
        'OcxCHK1
        '
        Me.OcxCHK1.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK1.Color = System.Drawing.Color.Empty
        Me.OcxCHK1.Location = New System.Drawing.Point(93, 74)
        Me.OcxCHK1.Name = "OcxCHK1"
        Me.OcxCHK1.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK1.SoloLectura = False
        Me.OcxCHK1.TabIndex = 9
        Me.OcxCHK1.Tag = "GASTO CREDITO"
        Me.OcxCHK1.Texto = ""
        Me.OcxCHK1.Valor = False
        '
        'OcxCHK9
        '
        Me.OcxCHK9.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK9.Color = System.Drawing.Color.Empty
        Me.OcxCHK9.Location = New System.Drawing.Point(93, 116)
        Me.OcxCHK9.Name = "OcxCHK9"
        Me.OcxCHK9.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK9.SoloLectura = False
        Me.OcxCHK9.TabIndex = 8
        Me.OcxCHK9.Tag = "NOTA CREDITO PROVEEDOR"
        Me.OcxCHK9.Texto = ""
        Me.OcxCHK9.Valor = False
        '
        'OcxCHK8
        '
        Me.OcxCHK8.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK8.Color = System.Drawing.Color.Empty
        Me.OcxCHK8.Location = New System.Drawing.Point(93, 95)
        Me.OcxCHK8.Name = "OcxCHK8"
        Me.OcxCHK8.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK8.SoloLectura = False
        Me.OcxCHK8.TabIndex = 6
        Me.OcxCHK8.Tag = "NOTA DEBITO PROVEEDOR"
        Me.OcxCHK8.Texto = ""
        Me.OcxCHK8.Valor = False
        '
        'OcxCHK7
        '
        Me.OcxCHK7.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK7.Color = System.Drawing.Color.Empty
        Me.OcxCHK7.Location = New System.Drawing.Point(93, 53)
        Me.OcxCHK7.Name = "OcxCHK7"
        Me.OcxCHK7.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK7.SoloLectura = False
        Me.OcxCHK7.TabIndex = 4
        Me.OcxCHK7.Tag = "GASTO CONTADO"
        Me.OcxCHK7.Texto = ""
        Me.OcxCHK7.Valor = False
        '
        'OcxCHK6
        '
        Me.OcxCHK6.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK6.Color = System.Drawing.Color.Empty
        Me.OcxCHK6.Location = New System.Drawing.Point(93, 34)
        Me.OcxCHK6.Name = "OcxCHK6"
        Me.OcxCHK6.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK6.SoloLectura = False
        Me.OcxCHK6.TabIndex = 2
        Me.OcxCHK6.Tag = "COMPRA"
        Me.OcxCHK6.Texto = ""
        Me.OcxCHK6.Valor = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(20, 116)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(66, 13)
        Me.Label18.TabIndex = 7
        Me.Label18.Text = "Nota Crédito"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(22, 95)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Nota Débito"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 53)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Gasto Contado"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(38, 34)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Compras"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(33, 18)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(89, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "PROVEEDORES"
        '
        'gbxBancos
        '
        Me.gbxBancos.Controls.Add(Me.Label26)
        Me.gbxBancos.Controls.Add(Me.OcxCHK2)
        Me.gbxBancos.Controls.Add(Me.OcxCHK5)
        Me.gbxBancos.Controls.Add(Me.OcxCHK4)
        Me.gbxBancos.Controls.Add(Me.OcxCHK3)
        Me.gbxBancos.Controls.Add(Me.Label20)
        Me.gbxBancos.Controls.Add(Me.chkDepositoRes)
        Me.gbxBancos.Controls.Add(Me.chkDeposito)
        Me.gbxBancos.Controls.Add(Me.Label16)
        Me.gbxBancos.Controls.Add(Me.Label11)
        Me.gbxBancos.Controls.Add(Me.Label12)
        Me.gbxBancos.Controls.Add(Me.Label14)
        Me.gbxBancos.Controls.Add(Me.Label15)
        Me.gbxBancos.Location = New System.Drawing.Point(8, 378)
        Me.gbxBancos.Name = "gbxBancos"
        Me.gbxBancos.Size = New System.Drawing.Size(182, 161)
        Me.gbxBancos.TabIndex = 5
        Me.gbxBancos.TabStop = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(1, 113)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(104, 13)
        Me.Label26.TabIndex = 12
        Me.Label26.Text = "Entrega de Cheques"
        '
        'OcxCHK2
        '
        Me.OcxCHK2.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK2.Color = System.Drawing.Color.Empty
        Me.OcxCHK2.Location = New System.Drawing.Point(111, 114)
        Me.OcxCHK2.Name = "OcxCHK2"
        Me.OcxCHK2.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK2.SoloLectura = False
        Me.OcxCHK2.TabIndex = 11
        Me.OcxCHK2.Tag = "ENTREGA DE CHEQUE"
        Me.OcxCHK2.Texto = ""
        Me.OcxCHK2.Valor = False
        '
        'OcxCHK5
        '
        Me.OcxCHK5.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK5.Color = System.Drawing.Color.Empty
        Me.OcxCHK5.Location = New System.Drawing.Point(111, 94)
        Me.OcxCHK5.Name = "OcxCHK5"
        Me.OcxCHK5.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK5.SoloLectura = False
        Me.OcxCHK5.TabIndex = 10
        Me.OcxCHK5.Tag = "ORDEN PAGO"
        Me.OcxCHK5.Texto = ""
        Me.OcxCHK5.Valor = False
        '
        'OcxCHK4
        '
        Me.OcxCHK4.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK4.Color = System.Drawing.Color.Empty
        Me.OcxCHK4.Location = New System.Drawing.Point(111, 73)
        Me.OcxCHK4.Name = "OcxCHK4"
        Me.OcxCHK4.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK4.SoloLectura = False
        Me.OcxCHK4.TabIndex = 8
        Me.OcxCHK4.Tag = "DEBITO/CREDITO BANCARIO"
        Me.OcxCHK4.Texto = ""
        Me.OcxCHK4.Valor = False
        '
        'OcxCHK3
        '
        Me.OcxCHK3.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK3.Color = System.Drawing.Color.Empty
        Me.OcxCHK3.Location = New System.Drawing.Point(111, 52)
        Me.OcxCHK3.Name = "OcxCHK3"
        Me.OcxCHK3.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK3.SoloLectura = False
        Me.OcxCHK3.TabIndex = 6
        Me.OcxCHK3.Tag = "DESCUENTO CHEQUE"
        Me.OcxCHK3.Texto = ""
        Me.OcxCHK3.Valor = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(144, 33)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(26, 13)
        Me.Label20.TabIndex = 4
        Me.Label20.Text = "Res"
        '
        'chkDepositoRes
        '
        Me.chkDepositoRes.BackColor = System.Drawing.Color.Transparent
        Me.chkDepositoRes.Color = System.Drawing.Color.Empty
        Me.chkDepositoRes.Location = New System.Drawing.Point(129, 33)
        Me.chkDepositoRes.Name = "chkDepositoRes"
        Me.chkDepositoRes.Size = New System.Drawing.Size(13, 12)
        Me.chkDepositoRes.SoloLectura = False
        Me.chkDepositoRes.TabIndex = 3
        Me.chkDepositoRes.Tag = "DEPOSITO RESUMIDO"
        Me.chkDepositoRes.Texto = ""
        Me.chkDepositoRes.Valor = False
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(111, 33)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(13, 12)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 2
        Me.chkDeposito.Tag = "DEPOSITO DETALLADO"
        Me.chkDeposito.Texto = ""
        Me.chkDeposito.Valor = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(24, 94)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(79, 13)
        Me.Label16.TabIndex = 9
        Me.Label16.Text = "Orden de Pago"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(11, 73)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(92, 13)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Débitos / Créditos"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(28, 52)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(75, 13)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Desc. Cheque"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(49, 33)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(54, 13)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Depósitos"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(68, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(51, 13)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "BANCOS"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cbxUsuario)
        Me.GroupBox4.Controls.Add(Me.chkUsuario)
        Me.GroupBox4.Controls.Add(Me.OcxTXTCuentaContable1)
        Me.GroupBox4.Controls.Add(Me.chkCuentaContable)
        Me.GroupBox4.Controls.Add(Me.cbxTipoComprobante)
        Me.GroupBox4.Controls.Add(Me.chkTipoComprobante)
        Me.GroupBox4.Controls.Add(Me.cbxCentroCosto)
        Me.GroupBox4.Controls.Add(Me.chkCentroCosto)
        Me.GroupBox4.Controls.Add(Me.rdbContabilidad)
        Me.GroupBox4.Controls.Add(Me.cbxSucursal)
        Me.GroupBox4.Controls.Add(Me.chkSucursal)
        Me.GroupBox4.Controls.Add(Me.rdbAdministracion)
        Me.GroupBox4.Controls.Add(Me.lblPeriodo)
        Me.GroupBox4.Controls.Add(Me.txtHasta)
        Me.GroupBox4.Controls.Add(Me.txtDesde)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(347, 186)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = Nothing
        Me.cbxUsuario.DataSource = "VUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(71, 159)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(251, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 14
        Me.cbxUsuario.Texto = ""
        '
        'chkUsuario
        '
        Me.chkUsuario.BackColor = System.Drawing.Color.Transparent
        Me.chkUsuario.Color = System.Drawing.Color.Empty
        Me.chkUsuario.Location = New System.Drawing.Point(11, 160)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(54, 19)
        Me.chkUsuario.SoloLectura = False
        Me.chkUsuario.TabIndex = 13
        Me.chkUsuario.Texto = "Usuario"
        Me.chkUsuario.Valor = False
        '
        'OcxTXTCuentaContable1
        '
        Me.OcxTXTCuentaContable1.AlturaMaxima = 0
        Me.OcxTXTCuentaContable1.Consulta = Nothing
        Me.OcxTXTCuentaContable1.Enabled = False
        Me.OcxTXTCuentaContable1.IDCentroCosto = 0
        Me.OcxTXTCuentaContable1.IDUnidadNegocio = 0
        Me.OcxTXTCuentaContable1.ListarTodas = False
        Me.OcxTXTCuentaContable1.Location = New System.Drawing.Point(70, 127)
        Me.OcxTXTCuentaContable1.Name = "OcxTXTCuentaContable1"
        Me.OcxTXTCuentaContable1.Registro = Nothing
        Me.OcxTXTCuentaContable1.Resolucion173 = False
        Me.OcxTXTCuentaContable1.Seleccionado = False
        Me.OcxTXTCuentaContable1.Size = New System.Drawing.Size(254, 59)
        Me.OcxTXTCuentaContable1.SoloLectura = False
        Me.OcxTXTCuentaContable1.TabIndex = 12
        Me.OcxTXTCuentaContable1.Texto = Nothing
        Me.OcxTXTCuentaContable1.whereFiltro = Nothing
        '
        'chkCuentaContable
        '
        Me.chkCuentaContable.BackColor = System.Drawing.Color.Transparent
        Me.chkCuentaContable.Color = System.Drawing.Color.Empty
        Me.chkCuentaContable.Location = New System.Drawing.Point(13, 130)
        Me.chkCuentaContable.Name = "chkCuentaContable"
        Me.chkCuentaContable.Size = New System.Drawing.Size(54, 19)
        Me.chkCuentaContable.SoloLectura = False
        Me.chkCuentaContable.TabIndex = 11
        Me.chkCuentaContable.Texto = "C.C."
        Me.chkCuentaContable.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = Nothing
        Me.cbxTipoComprobante.DataSource = "VTipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(73, 99)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(251, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 10
        Me.cbxTipoComprobante.Texto = ""
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(13, 100)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(54, 19)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 9
        Me.chkTipoComprobante.Texto = "Comp."
        Me.chkTipoComprobante.Valor = False
        '
        'cbxCentroCosto
        '
        Me.cbxCentroCosto.CampoWhere = "IDCentroCosto"
        Me.cbxCentroCosto.CargarUnaSolaVez = False
        Me.cbxCentroCosto.DataDisplayMember = "Descripcion"
        Me.cbxCentroCosto.DataFilter = Nothing
        Me.cbxCentroCosto.DataOrderBy = Nothing
        Me.cbxCentroCosto.DataSource = "VCentroCosto"
        Me.cbxCentroCosto.DataValueMember = "ID"
        Me.cbxCentroCosto.dtSeleccionado = Nothing
        Me.cbxCentroCosto.Enabled = False
        Me.cbxCentroCosto.FormABM = Nothing
        Me.cbxCentroCosto.Indicaciones = Nothing
        Me.cbxCentroCosto.Location = New System.Drawing.Point(73, 70)
        Me.cbxCentroCosto.Name = "cbxCentroCosto"
        Me.cbxCentroCosto.SeleccionMultiple = False
        Me.cbxCentroCosto.SeleccionObligatoria = False
        Me.cbxCentroCosto.Size = New System.Drawing.Size(251, 21)
        Me.cbxCentroCosto.SoloLectura = False
        Me.cbxCentroCosto.TabIndex = 8
        Me.cbxCentroCosto.Texto = ""
        '
        'chkCentroCosto
        '
        Me.chkCentroCosto.BackColor = System.Drawing.Color.Transparent
        Me.chkCentroCosto.Color = System.Drawing.Color.Empty
        Me.chkCentroCosto.Location = New System.Drawing.Point(13, 71)
        Me.chkCentroCosto.Name = "chkCentroCosto"
        Me.chkCentroCosto.Size = New System.Drawing.Size(54, 19)
        Me.chkCentroCosto.SoloLectura = False
        Me.chkCentroCosto.TabIndex = 7
        Me.chkCentroCosto.Texto = "C.Costo"
        Me.chkCentroCosto.Valor = False
        '
        'rdbContabilidad
        '
        Me.rdbContabilidad.AutoSize = True
        Me.rdbContabilidad.Location = New System.Drawing.Point(241, 41)
        Me.rdbContabilidad.Name = "rdbContabilidad"
        Me.rdbContabilidad.Size = New System.Drawing.Size(83, 17)
        Me.rdbContabilidad.TabIndex = 6
        Me.rdbContabilidad.Text = "Contabilidad"
        Me.rdbContabilidad.UseVisualStyleBackColor = True
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(73, 39)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(154, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(13, 40)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(54, 19)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Texto = "Origen"
        Me.chkSucursal.Valor = False
        '
        'rdbAdministracion
        '
        Me.rdbAdministracion.AutoSize = True
        Me.rdbAdministracion.Checked = True
        Me.rdbAdministracion.Location = New System.Drawing.Point(241, 20)
        Me.rdbAdministracion.Name = "rdbAdministracion"
        Me.rdbAdministracion.Size = New System.Drawing.Size(93, 17)
        Me.rdbAdministracion.TabIndex = 3
        Me.rdbAdministracion.TabStop = True
        Me.rdbAdministracion.Text = "Administracion"
        Me.rdbAdministracion.UseVisualStyleBackColor = True
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 4, 12, 8, 11, 12, 345)
        Me.txtHasta.Location = New System.Drawing.Point(153, 15)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 4, 12, 8, 11, 12, 345)
        Me.txtDesde.Location = New System.Drawing.Point(73, 15)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'gbxStock
        '
        Me.gbxStock.Controls.Add(Me.OcxCHK15)
        Me.gbxStock.Controls.Add(Me.Label30)
        Me.gbxStock.Controls.Add(Me.OcxCHK14)
        Me.gbxStock.Controls.Add(Me.Label29)
        Me.gbxStock.Controls.Add(Me.OcxCHK13)
        Me.gbxStock.Controls.Add(Me.Label28)
        Me.gbxStock.Controls.Add(Me.OcxCHK12)
        Me.gbxStock.Controls.Add(Me.Label27)
        Me.gbxStock.Controls.Add(Me.OcxCHK11)
        Me.gbxStock.Controls.Add(Me.OcxCHK10)
        Me.gbxStock.Controls.Add(Me.Label21)
        Me.gbxStock.Controls.Add(Me.Label23)
        Me.gbxStock.Controls.Add(Me.Label24)
        Me.gbxStock.Location = New System.Drawing.Point(196, 378)
        Me.gbxStock.Name = "gbxStock"
        Me.gbxStock.Size = New System.Drawing.Size(159, 161)
        Me.gbxStock.TabIndex = 6
        Me.gbxStock.TabStop = False
        '
        'OcxCHK15
        '
        Me.OcxCHK15.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK15.Color = System.Drawing.Color.Empty
        Me.OcxCHK15.Location = New System.Drawing.Point(106, 133)
        Me.OcxCHK15.Name = "OcxCHK15"
        Me.OcxCHK15.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK15.SoloLectura = False
        Me.OcxCHK15.TabIndex = 12
        Me.OcxCHK15.Tag = "DESCARGACOMPRA"
        Me.OcxCHK15.Texto = ""
        Me.OcxCHK15.Valor = False
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(0, 132)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(97, 13)
        Me.Label30.TabIndex = 11
        Me.Label30.Text = "Descarga Compras"
        '
        'OcxCHK14
        '
        Me.OcxCHK14.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK14.Color = System.Drawing.Color.Empty
        Me.OcxCHK14.Location = New System.Drawing.Point(106, 115)
        Me.OcxCHK14.Name = "OcxCHK14"
        Me.OcxCHK14.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK14.SoloLectura = False
        Me.OcxCHK14.TabIndex = 10
        Me.OcxCHK14.Tag = "MATERIAPRIMA"
        Me.OcxCHK14.Texto = ""
        Me.OcxCHK14.Valor = False
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(0, 114)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(96, 13)
        Me.Label29.TabIndex = 9
        Me.Label29.Text = "Mat. Prima Balanc."
        '
        'OcxCHK13
        '
        Me.OcxCHK13.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK13.Color = System.Drawing.Color.Empty
        Me.OcxCHK13.Location = New System.Drawing.Point(106, 94)
        Me.OcxCHK13.Name = "OcxCHK13"
        Me.OcxCHK13.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK13.SoloLectura = False
        Me.OcxCHK13.TabIndex = 8
        Me.OcxCHK13.Tag = "COMBUSTIBLE"
        Me.OcxCHK13.Texto = ""
        Me.OcxCHK13.Valor = False
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(0, 93)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(91, 13)
        Me.Label28.TabIndex = 7
        Me.Label28.Text = "Cons.Combustible"
        '
        'OcxCHK12
        '
        Me.OcxCHK12.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK12.Color = System.Drawing.Color.Empty
        Me.OcxCHK12.Location = New System.Drawing.Point(106, 72)
        Me.OcxCHK12.Name = "OcxCHK12"
        Me.OcxCHK12.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK12.SoloLectura = False
        Me.OcxCHK12.TabIndex = 6
        Me.OcxCHK12.Tag = "DESCARGASTOCK"
        Me.OcxCHK12.Texto = ""
        Me.OcxCHK12.Valor = False
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(7, 71)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(84, 13)
        Me.Label27.TabIndex = 5
        Me.Label27.Text = "Descarga Stock"
        '
        'OcxCHK11
        '
        Me.OcxCHK11.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK11.Color = System.Drawing.Color.Empty
        Me.OcxCHK11.Location = New System.Drawing.Point(106, 52)
        Me.OcxCHK11.Name = "OcxCHK11"
        Me.OcxCHK11.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK11.SoloLectura = False
        Me.OcxCHK11.TabIndex = 4
        Me.OcxCHK11.Tag = "DESCARGASTOCK"
        Me.OcxCHK11.Texto = ""
        Me.OcxCHK11.Valor = False
        Me.OcxCHK11.Visible = False
        '
        'OcxCHK10
        '
        Me.OcxCHK10.BackColor = System.Drawing.Color.Transparent
        Me.OcxCHK10.Color = System.Drawing.Color.Empty
        Me.OcxCHK10.Location = New System.Drawing.Point(106, 34)
        Me.OcxCHK10.Name = "OcxCHK10"
        Me.OcxCHK10.Size = New System.Drawing.Size(13, 12)
        Me.OcxCHK10.SoloLectura = False
        Me.OcxCHK10.TabIndex = 2
        Me.OcxCHK10.Tag = "MOVIMIENTO"
        Me.OcxCHK10.Texto = ""
        Me.OcxCHK10.Valor = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(55, 52)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(36, 13)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Ajuste"
        Me.Label21.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(30, 33)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(61, 13)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "Movimiento"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(57, 16)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(43, 13)
        Me.Label24.TabIndex = 0
        Me.Label24.Text = "STOCK"
        '
        'btnContabilidad
        '
        Me.btnContabilidad.Location = New System.Drawing.Point(8, 581)
        Me.btnContabilidad.Name = "btnContabilidad"
        Me.btnContabilidad.Size = New System.Drawing.Size(118, 23)
        Me.btnContabilidad.TabIndex = 8
        Me.btnContabilidad.Text = "Pasar a Contabilidad"
        Me.btnContabilidad.UseVisualStyleBackColor = True
        '
        'chkOtros
        '
        Me.chkOtros.BackColor = System.Drawing.Color.Transparent
        Me.chkOtros.Color = System.Drawing.Color.Empty
        Me.chkOtros.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Italic Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOtros.Location = New System.Drawing.Point(19, 548)
        Me.chkOtros.Name = "chkOtros"
        Me.chkOtros.Size = New System.Drawing.Size(336, 17)
        Me.chkOtros.SoloLectura = False
        Me.chkOtros.TabIndex = 7
        Me.chkOtros.Tag = "OTROS"
        Me.chkOtros.Texto = "* Otros asientos sin operaciones relacionadas."
        Me.chkOtros.Valor = False
        '
        'chkSeleccionarTodo
        '
        Me.chkSeleccionarTodo.BackColor = System.Drawing.Color.Transparent
        Me.chkSeleccionarTodo.Color = System.Drawing.Color.Empty
        Me.chkSeleccionarTodo.Location = New System.Drawing.Point(21, 194)
        Me.chkSeleccionarTodo.Name = "chkSeleccionarTodo"
        Me.chkSeleccionarTodo.Size = New System.Drawing.Size(104, 16)
        Me.chkSeleccionarTodo.SoloLectura = False
        Me.chkSeleccionarTodo.TabIndex = 1
        Me.chkSeleccionarTodo.Texto = "Seleccionar todo"
        Me.chkSeleccionarTodo.Valor = False
        '
        'chkAsientosConDiferencia
        '
        Me.chkAsientosConDiferencia.BackColor = System.Drawing.Color.Transparent
        Me.chkAsientosConDiferencia.Color = System.Drawing.Color.Empty
        Me.chkAsientosConDiferencia.Location = New System.Drawing.Point(131, 194)
        Me.chkAsientosConDiferencia.Name = "chkAsientosConDiferencia"
        Me.chkAsientosConDiferencia.Size = New System.Drawing.Size(142, 16)
        Me.chkAsientosConDiferencia.SoloLectura = False
        Me.chkAsientosConDiferencia.TabIndex = 2
        Me.chkAsientosConDiferencia.Texto = "Asientos con diferencias"
        Me.chkAsientosConDiferencia.Valor = False
        '
        'frmAsientoDiario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(362, 627)
        Me.Controls.Add(Me.chkOtros)
        Me.Controls.Add(Me.chkSeleccionarTodo)
        Me.Controls.Add(Me.chkAsientosConDiferencia)
        Me.Controls.Add(Me.btnContabilidad)
        Me.Controls.Add(Me.gbxStock)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.gbxBancos)
        Me.Controls.Add(Me.btnListar)
        Me.Controls.Add(Me.gbxProveedores)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxCliente)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmAsientoDiario"
        Me.Text = "frmAsientoDiario"
        Me.gbxCliente.ResumeLayout(False)
        Me.gbxCliente.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxProveedores.ResumeLayout(False)
        Me.gbxProveedores.PerformLayout()
        Me.gbxBancos.ResumeLayout(False)
        Me.gbxBancos.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.gbxStock.ResumeLayout(False)
        Me.gbxStock.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents gbxCliente As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents gbxProveedores As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents gbxBancos As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents gbxStock As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents rdbContabilidad As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAdministracion As System.Windows.Forms.RadioButton
    Friend WithEvents chkVentaRes As ERP.ocxCHK
    Friend WithEvents chkVenta As ERP.ocxCHK
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents chkCobranzaCreditoRes As ERP.ocxCHK
    Friend WithEvents chkCobranzaCredito As ERP.ocxCHK
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkCobranzaContadoRes As ERP.ocxCHK
    Friend WithEvents chkCobranzaContado As ERP.ocxCHK
    Friend WithEvents chkNotaDebitoCliente As ERP.ocxCHK
    Friend WithEvents chkNotaCreditoCliente As ERP.ocxCHK
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents OcxCHK5 As ERP.ocxCHK
    Friend WithEvents OcxCHK4 As ERP.ocxCHK
    Friend WithEvents OcxCHK3 As ERP.ocxCHK
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents chkDepositoRes As ERP.ocxCHK
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents OcxCHK9 As ERP.ocxCHK
    Friend WithEvents OcxCHK8 As ERP.ocxCHK
    Friend WithEvents OcxCHK7 As ERP.ocxCHK
    Friend WithEvents OcxCHK6 As ERP.ocxCHK
    Friend WithEvents OcxCHK11 As ERP.ocxCHK
    Friend WithEvents OcxCHK10 As ERP.ocxCHK
    Friend WithEvents btnContabilidad As System.Windows.Forms.Button
    Friend WithEvents chkAsientosConDiferencia As ERP.ocxCHK
    Friend WithEvents chkSeleccionarTodo As ERP.ocxCHK
    Friend WithEvents chkOtros As ERP.ocxCHK
    Friend WithEvents OcxCHK1 As ERP.ocxCHK
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents chkGastoContadoRes As ERP.ocxCHK
    Friend WithEvents cbxCentroCosto As ERP.ocxCBX
    Friend WithEvents chkCentroCosto As ERP.ocxCHK
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents OcxCHK2 As ERP.ocxCHK
    Friend WithEvents OcxCHK12 As ERP.ocxCHK
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents OcxCHK13 As ERP.ocxCHK
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents OcxCHK15 As ERP.ocxCHK
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents OcxCHK14 As ERP.ocxCHK
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As ocxCBX
    Friend WithEvents chkTipoComprobante As ocxCHK
    Friend WithEvents chkCuentaContable As ocxCHK
    Friend WithEvents OcxTXTCuentaContable1 As ocxTXTCuentaContable
    Friend WithEvents cbxUsuario As ocxCBX
    Friend WithEvents chkUsuario As ocxCHK
    Friend WithEvents chkAplicacionVentas As ocxCHK
    Friend WithEvents Label31 As Label
End Class

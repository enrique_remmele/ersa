﻿Imports System.Threading

Public Class frmPasarAContabilidad

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES
    Private dtValue As DataTable
    Public Property dt() As DataTable
        Get
            Return dtValue
        End Get
        Set(ByVal value As DataTable)
            dtValue = value
        End Set
    End Property

    Private desdeValue As Date
    Public Property desde() As Date
        Get
            Return desdeValue
        End Get
        Set(ByVal value As Date)
            desdeValue = value
        End Set
    End Property

    Private hastaValue As Date
    Public Property hasta() As Date
        Get
            Return hastaValue
        End Get
        Set(ByVal value As Date)
            hastaValue = value
        End Set
    End Property

    'VARIABLES
    Dim vActualizar As Thread
    Dim vProcesando As Boolean

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        frmPasarAContabilidad.CheckForIllegalCrossThreadCalls = False

        'Label
        Me.lblProgreso.BackColor = Color.Transparent

        'Botones
        btnIniciar.Enabled = True
        btnCancelar.Enabled = False
        btnCerrar.Enabled = True

    End Sub

    Sub Iniciar()

        vProcesando = True

        CData.OrderDataTable(dt, "Orden")

        'Ordenar la tabla
        vActualizar = New Thread(AddressOf Actualizar)
        vActualizar.Start()

        btnIniciar.Enabled = False
        btnCancelar.Enabled = True
        btnCerrar.Enabled = False

        Me.Close()

    End Sub

    Sub Actualizar()


        'Eliminamos primeramente
        Eliminar()

        Dim i As Integer = 0
        Dim Porcentaje As Decimal = 0
        For Each orow As DataRow In dt.Rows
            If vProcesando = True Then
                Porcentaje = CInt(((i + 1) / dt.Rows.Count) * 100)
                lblProgreso.Text = "Copiando " & (i + 1).ToString & " de " & dt.Rows.Count & " - " & Porcentaje & "% "
                pbProgreso.Value = Porcentaje

                Procesar(orow)

                i = i + 1
            Else
                Exit For
            End If
        Next

        vProcesando = False
        btnIniciar.Enabled = True
        btnCancelar.Enabled = False
        btnCerrar.Enabled = True
        MessageBox.Show("Proceso finalizado", "Pase Contable", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()

    End Sub

    Sub Procesar(ByVal oRow As DataRow)

        Dim Mensaje As String = "Se produjo un error! Desea continuar?"
        Dim SQL As String = " Exec SpPaseContable "

        CSistema.ConcatenarParametro(SQL, "@Codigo", oRow("Codigo").ToString)
        CSistema.ConcatenarParametro(SQL, "@Fecha", CSistema.FormatoFechaBaseDatos(oRow("Fecha").ToString, True, False))
        CSistema.ConcatenarParametro(SQL, "@IDSucursal", oRow("IDSucursal").ToString)
        CSistema.ConcatenarParametro(SQL, "@IDCentroCosto", oRow("IDCentroCosto").ToString)
        CSistema.ConcatenarParametro(SQL, "@Debito", CSistema.FormatoMonedaBaseDatos(oRow("Debito").ToString))
        CSistema.ConcatenarParametro(SQL, "@Credito", CSistema.FormatoMonedaBaseDatos(oRow("Credito").ToString))
        CSistema.ConcatenarParametro(SQL, "@Observacion", oRow("Observacion").ToString)
        CSistema.ConcatenarParametro(SQL, "@Operacion", oRow("Operacion").ToString)
        CSistema.ConcatenarParametro(SQL, "@IDTipoComprobante", oRow("IDTipoComprobante").ToString)
        CSistema.ConcatenarParametro(SQL, "@NroComprobante", oRow("NroComprobante").ToString)
        CSistema.ConcatenarParametro(SQL, "@Tipo", oRow("Tipo").ToString)
        CSistema.ConcatenarParametro(SQL, "@IDTransaccion", oRow("IDTransaccion").ToString)
        CSistema.ConcatenarParametro(SQL, "@Resumido", oRow("Resumido").ToString)
        CSistema.ConcatenarParametro(SQL, "@IDUsuario", vgIDUsuario)

        Dim Primero As Boolean = False

        If oRow("Orden") = 0 Then
            Primero = True
        End If

        CSistema.ConcatenarParametro(SQL, "@Primero", Primero)

        Dim dtTemp As DataTable = CSistema.ExecuteToDataTable(SQL)
        'Dim dtTemp As New DataTable

        Debug.Print(SQL)

        'Validar
        If dtTemp Is Nothing Then
            GoTo Salir
        End If

        If dtTemp.Rows.Count = 0 Then
            GoTo Salir
        End If

        Dim Resultado As DataRow = dtTemp.Rows(0)

        If Resultado("Procesado") = True Then
            GoTo Siguiente
        End If

        Mensaje = Resultado("Mensaje")

        GoTo Salir

Salir:
        If MessageBox.Show(Mensaje, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) = DialogResult.No Then
            vProcesando = False
        End If

        Exit Sub

Siguiente:

    End Sub

    Sub Eliminar()

        Dim Tipos(-1) As String

        For Each orow As DataRow In dt.Rows
            If Tipos.Contains(orow("Tipo").ToString) = False Then
                ReDim Preserve Tipos(Tipos.GetLength(0))
                Tipos(Tipos.GetLength(0) - 1) = orow("Tipo")
            End If
        Next

        Dim porcentaje As Decimal

        For i As Integer = 0 To Tipos.GetLength(0) - 1

            Dim SQL As String = "Exec SpContabilidad "
            CSistema.ConcatenarParametro(SQL, "@Desde", CSistema.FormatoFechaDesdeHastaBaseDatos(desde, True, False))
            CSistema.ConcatenarParametro(SQL, "@Hasta", CSistema.FormatoFechaDesdeHastaBaseDatos(hasta, False, True))
            CSistema.ConcatenarParametro(SQL, "@Tipo", Tipos(i))

            Dim dtTemp As DataTable = CSistema.ExecuteToDataTable(SQL)
       
            'Validar
            If dtTemp Is Nothing Then
                GoTo Salir
            End If

            If dtTemp.Rows.Count = 0 Then
                GoTo Salir
            End If

            Dim Resultado As DataRow = dtTemp.Rows(0)

            If Resultado("Procesado") = False Then
                GoTo salir
            End If

            porcentaje = CInt((i + 1 / Tipos.GetLength(0)) * 100)

            Try
                lblProgreso.Text = "Eliminando " & Tipos(i) & "  - " & porcentaje & " %"
                pbProgreso.Value = porcentaje
            Catch ex As Exception

            End Try

            GoTo siguiente


salir:
            If MessageBox.Show("Desea continuar?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Stop) = DialogResult.No Then
                vProcesando = False
                Exit Sub
            End If

siguiente:

            Thread.Sleep(500)

        Next
        



    End Sub

    Sub Cancelar()

        vProcesando = False
        Thread.Sleep(1000)

        btnIniciar.Enabled = True
        btnCancelar.Enabled = False
        btnCerrar.Enabled = True

    End Sub

    Sub Cerrar()

        vProcesando = False
        Thread.Sleep(1000)
        Me.Close()

    End Sub

    Private Sub frmPasarAContabilidad_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If vProcesando = True Then
            e.Cancel = True
        End If

    End Sub

    Private Sub frmPasarAContabilidad_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmPasarAContabilidad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnIniciar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIniciar.Click
        Iniciar()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Cerrar()
    End Sub

End Class
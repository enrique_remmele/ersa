﻿Imports ERP.Reporte
Public Class frmAsientoDiario
    'CLASES
    Dim CReporte As New CReporteAsiento
    Dim CSistema As New CSistema

    'VARIABLES
    Dim Titulo As String = ""
    Dim vControles() As Control
    Dim vdtAsientos As DataTable
    Dim Where As String = ""
    Dim Ban As Boolean = False
    Dim Ban2 As Boolean = False

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        CargarInformacion()

        txtDesde.Hoy()
        txtHasta.Hoy()
        cbxTipoComprobante.SeleccionMultiple = True
        OcxTXTCuentaContable1.Conectar()


    End Sub

    Sub CargarInformacion()
        ReDim vControles(-1)
        CSistema.CargaControl(vControles, chkNotaDebitoCliente)
        CSistema.CargaControl(vControles, chkCobranzaContado)
        CSistema.CargaControl(vControles, chkCobranzaContadoRes)
    End Sub

    Sub InicializarControles()
        CSistema.InicializaControles(vControles)
    End Sub

    Sub Listar(ByVal informe As Boolean)

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"

        'Venta
        If rdbContabilidad.Checked = True Then
            chkVenta.Tag = "VENTA"
        Else
            If chkVentaRes.Valor = False Then
                chkVenta.Tag = "VENTA DETALLADA"
            Else
                chkVenta.Tag = "VENTA RESUMIDA"
            End If
        End If


        'Cobranza Contado
        If rdbContabilidad.Checked = True Then
            chkCobranzaContado.Tag = "COBRANZA"
        Else
            If chkCobranzaContadoRes.Valor = False Then
                chkCobranzaContado.Tag = "COBRANZA CONTADO DETALLADO"
            Else
                chkCobranzaContado.Tag = "COBRANZA CONTADO RESUMIDO"
            End If
        End If


        'Cobranza Crédito
        If rdbContabilidad.Checked = True Then
            chkCobranzaCredito.Tag = "COBRANZA"
        Else
            If chkCobranzaCreditoRes.Valor = False Then
                chkCobranzaCredito.Tag = "COBRANZA CREDITO DETALLADO"
            Else
                chkCobranzaCredito.Tag = "COBRANZA CREDITO RESUMIDO"
            End If
        End If


        'Depósito Bancario
        If rdbContabilidad.Checked = True Then
            chkDeposito.Tag = "DEPOSITO"
        Else
            If chkDepositoRes.Valor = False Then
                chkDeposito.Tag = "DEPOSITO DETALLADO"
            Else
                chkDeposito.Tag = "DEPOSITO RESUMIDO"
            End If
        End If

        ' Anticipo Aplicacion
        If rdbContabilidad.Checked = True Then
            chkAplicacionVentas.Tag = "APLICACION VENTAS"
            ' Else
            'If chkDepositoRes.Valor = False Then
            '    chkDeposito.Tag = "DEPOSITO DETALLADO"
            'Else
            '    chkDeposito.Tag = "DEPOSITO RESUMIDO"
            'End If
        End If

        EstablecerCondicionTipoDocumento()

        Titulo = "ASIENTOS DIARIOS DEL " & txtDesde.txt.Text & " AL " & txtHasta.txt.Text

        'Establecemos los filtros
        For Each ctr As Object In GroupBox4.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Asientos con diferencias

        'Tipo
        Dim CG As String = ""

        If rdbContabilidad.Checked = True Then
            CG = "CG"
        End If

        Dim SQL As String
        Dim ConSaldo As String = ""
        Dim AndCC As String = ""
        If chkAsientosConDiferencia.Valor = True Then
            'ConSaldo = ConSaldo & " and IDTransaccion in (select A.IDTransaccion 
            '                                            from Asiento A 
            '                                            left outer join DetalleAsiento DA on A.IDTransaccion = DA.IDTRansaccion 
            '                                            where (isnull(DA.CuentaContable,'')= '' or abs(A.Saldo) >1) 
            '                                            and A.IDTransaccion = VDetalleAsientoContable2.IDTransaccion 
            '                                            and A.Anulado = 0)"

            ConSaldo = ConSaldo & " and IDTransaccion in (select A.IDTransaccion 
                                                        from Asiento A 
                                                        left outer join DetalleAsiento DA on A.IDTransaccion = DA.IDTRansaccion 
                                                        where A.IDTransaccion = VDetalleAsientoContable2.IDTransaccion 
                                                        and A.Anulado = 0
							                            group by A.IDTransaccion
							                            having abs(sum(DA.Credito)-sum(DA.Debito))>1)"
        End If

        If chkCuentaContable.Valor Then
            If OcxTXTCuentaContable1.Seleccionado Then
                AndCC = " And IDTransaccion in (select distinct idtransaccion from vdetalleasiento Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' and codigo = '" & OcxTXTCuentaContable1.Registro("Codigo") & "')"
            End If
        End If



        SQL = "Select  * From VDetalleAsientoContable2" & CG & " " & Where & ConSaldo & AndCC & " Order By Fecha, Sucursal, Moneda, Operacion, Codigo, Orden  "
        vdtAsientos = CSistema.ExecuteToDataTable(SQL)
        vdtAsientos.TableName = "VDetalleAsientoContable"

        If informe = True Then
            CReporte.AsientoDiario(frm, vdtAsientos, Titulo, vgUsuarioIdentificador, chkAsientosConDiferencia.Valor, chkGastoContadoRes.Valor)
        Else
            PasarAContabilidad()
        End If

    End Sub

    Sub PasarAContabilidad()

        Dim frm As New frmPasarAContabilidad
        frm.dt = vdtAsientos
        frm.desde = txtDesde.GetValue
        frm.hasta = txtHasta.GetValue

        FGMostrarFormulario(Me, frm, "Pase Contable", Windows.Forms.FormBorderStyle.FixedToolWindow, FormStartPosition.CenterScreen, True, False)

    End Sub

    Function EstablecerCondicionTipoDocumento() As String

        EstablecerCondicionTipoDocumento = ""

        For Each ctr1 As Control In Me.Controls

            If ctr1.GetType.Name.ToUpper = "GROUPBOX" Then
                For Each ctr As Object In ctr1.Controls
                    If ctr.GetType.Name.ToUpper = "OCXCHK" Then
                        If ctr.valor Then
                            If Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'" Then
                                Where = Where & " And ( Tipo = '" & ctr.tag & "' "
                            Else
                                Where = Where & " Or Tipo = '" & ctr.tag & "' "
                            End If
                        End If
                    End If
                Next
            End If
        Next

        'Otros asientos
        If chkOtros.Valor = True Then
            If Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'" Then
                Where = Where & " And ( Tipo = '" & chkOtros.Tag & "' "
            Else
                Where = Where & " Or Tipo = '" & chkOtros.Tag & "' "
            End If
        End If

        If Where <> " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'" Then
            Where = Where & " ) "
        End If

    End Function

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar(True)
    End Sub

    Private Sub frmAsientoDiario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown, Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxSucursal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkVenta_CheckedChanged() Handles chkVenta.PropertyChanged
        If chkVenta.Valor = True Then
            chkVenta.Tag = "VENTA RESUMIDA"
            chkVentaRes.Valor = True
        Else
            chkVenta.Tag = "VENTA DETALLADA"
            chkVentaRes.Valor = False
        End If
    End Sub

    Private Sub chkVentaRes_CheckedChanged() Handles chkVentaRes.PropertyChanged
        If chkVenta.Valor = False Then
            chkVentaRes.Valor = False
        End If
    End Sub

    Private Sub chkCobranza_CheckedChanged() Handles chkCobranzaContado.PropertyChanged
        If chkCobranzaContado.Valor = True Then
            chkCobranzaContado.Tag = "COBRANZA CONTADO RESUMIDO"
            chkCobranzaContadoRes.Valor = True
        Else
            chkCobranzaContado.Tag = "COBRANZA CONTADO DETALLADO"
            chkCobranzaContadoRes.Valor = False
        End If
    End Sub

    Private Sub chkCobranzaRes_CheckedChanged() Handles chkCobranzaContadoRes.PropertyChanged
        If chkCobranzaContado.Valor = False Then
            chkCobranzaContadoRes.Valor = False
        End If
    End Sub

    Private Sub chkDeposito_PropertyChanged() Handles chkDeposito.PropertyChanged
        If chkDeposito.Valor = True Then
            chkDepositoRes.Valor = True
            Ban = True
        Else
            chkDepositoRes.Valor = False
            Ban = False
        End If
    End Sub

    Private Sub chkDepositoRes_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepositoRes.PropertyChanged
        If chkDeposito.Valor = False Then
            chkDepositoRes.Valor = False
        End If
    End Sub

    Private Sub chkCobranzaCredito_CheckedChanged() Handles chkCobranzaCredito.PropertyChanged
        If chkCobranzaCredito.Valor = True Then
            chkCobranzaCredito.Tag = "COBRANZA CREDITO RESUMIDO"
            chkCobranzaCreditoRes.Valor = True
        Else
            chkCobranzaCredito.Tag = "COBRANZA CREDITO DETALLADO"
            chkCobranzaCreditoRes.Valor = False
        End If
    End Sub

    Private Sub chkCobranzaCreditoRes_CheckedChanged() Handles chkCobranzaCreditoRes.PropertyChanged
        If chkCobranzaCredito.Valor = False Then
            chkCobranzaCreditoRes.Valor = False
        End If
    End Sub

    Private Sub chkDeposito_CheckedChanged() Handles chkDeposito.PropertyChanged
        If chkDeposito.Valor = True Then
            chkDeposito.Tag = "DEPOSITO RESUMIDO"
            chkDepositoRes.Valor = True
        Else
            chkDeposito.Tag = "DEPOSITO DETALLADO"
            chkDepositoRes.Valor = False
        End If
    End Sub

    Private Sub chkAplicacionVentas_CheckedChanged() Handles chkAplicacionVentas.PropertyChanged
        If chkAplicacionVentas.Valor = True Then
            chkAplicacionVentas.Tag = "APLICACION VENTAS RESUMIDO"
            'chkDepositoRes.Valor = True
        Else
            chkAplicacionVentas.Tag = "APLICACION VENTAS DETALLADO"
            'chkDepositoRes.Valor = False
        End If
    End Sub

    Private Sub chkDepositoRes_CheckedChanged() Handles chkDepositoRes.PropertyChanged
        If chkDeposito.Valor = False Then
            chkDepositoRes.Valor = False
        End If
    End Sub

    Private Sub chkSucursal_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub rdbContabilidad_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbContabilidad.CheckedChanged
        btnContabilidad.Enabled = rdbAdministracion.Checked
    End Sub

    Private Sub rdbAdministracion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAdministracion.CheckedChanged
        btnContabilidad.Enabled = rdbAdministracion.Checked
    End Sub

    Private Sub chkSeleccionarTodo_LinkClicked(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal valor As Boolean) Handles chkSeleccionarTodo.PropertyChanged

        For Each c As Object In gbxCliente.Controls
            If c.GetType.Name = "ocxCHK" Then
                c.valor = valor
            End If
        Next

        For Each c As Object In gbxProveedores.Controls
            If c.GetType.Name = "ocxCHK" Then
                c.valor = valor
            End If
        Next

        For Each c As Object In gbxBancos.Controls
            If c.GetType.Name = "ocxCHK" Then
                c.valor = valor
            End If
        Next

        For Each c As Object In gbxStock.Controls
            If c.GetType.Name = "ocxCHK" Then
                c.valor = valor
            End If
        Next

        'Otros
        chkOtros.Valor = valor
        chkVentaRes.Valor = False
        chkCobranzaContadoRes.Valor = False
        chkCobranzaCreditoRes.Valor = False
        chkDepositoRes.Valor = False
        chkGastoContadoRes.Valor = False

    End Sub

    Private Sub btnContabilidad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContabilidad.Click
        Listar(False)
    End Sub


    Private Sub OcxCHK7_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles OcxCHK7.PropertyChanged
        If OcxCHK7.Valor = False Then
            chkGastoContadoRes.Valor = False
        Else
            chkGastoContadoRes.Valor = True
        End If
    End Sub

    Private Sub chkCentroCosto_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkCentroCosto.PropertyChanged
        cbxCentroCosto.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkDeposito.PropertyChanged

    End Sub

    Private Sub chkVenta_CheckedChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVenta.PropertyChanged

    End Sub

    Private Sub chkVentaRes_CheckedChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVentaRes.PropertyChanged

    End Sub

    Private Sub chkCobranza_CheckedChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCobranzaContado.PropertyChanged

    End Sub

    Private Sub chkCobranzaRes_CheckedChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCobranzaContadoRes.PropertyChanged

    End Sub

    Private Sub chkCobranzaCredito_CheckedChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCobranzaCredito.PropertyChanged

    End Sub

    Private Sub chkCobranzaCreditoRes_CheckedChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCobranzaCreditoRes.PropertyChanged

    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkCuentaContable_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCuentaContable.PropertyChanged
        OcxTXTCuentaContable1.Enabled = value
    End Sub

    Private Sub chkUsuario_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub


End Class
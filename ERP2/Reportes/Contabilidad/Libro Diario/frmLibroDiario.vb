﻿Imports ERP.Reporte

Public Class frmLibroDiario

    'CLASES
    Dim CReporte As New CReporteDiario
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "Libro Diario"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Fecha
        txtHasta.SetValue(Date.Today)
        chkImprimirCabecera.Valor = True

    End Sub

    Sub Listar()

        Dim Where As String = " Where 1=1 "
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Subtitulo As String = ""
        Dim NumeroInicial As Integer = 1

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        If PorFecha.Checked Then
            Where = Where & " and Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
        End If

        If PorAsiento.Checked Then
            Where = Where & " and Numero Between " & txtAsientoDesde.ObtenerValor & " and " & txtAsientoHasta.ObtenerValor
        End If

        If chkPorOperacion.Checked Then
            Where = Where & " and IDOperacion = " & cbxOperacion.cbx.SelectedValue
        End If

        If chkFormatoImpresion.Valor Then
            If IsNumeric(txtNumeroInicial.Texto) Then
                NumeroInicial = CInt(txtNumeroInicial.Texto)
            End If
        End If

        'Establecemos el Orden
        OrderBy = " Order By Fecha asc"
        'If PorFecha.Checked Then
        '    OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text & " " & cbxEnForma.cbx.Text & ", Fecha"
        'End If

        CReporte.ArmarSubTitulo(Subtitulo, GroupBox1, txtDesde, txtHasta)
        'If rdbAdministracion.Checked = True Then
        CReporte.LibroDiario(frm, Where, Titulo, Subtitulo, vgUsuarioIdentificador, OrderBy, chkImprimirCabecera.Valor, chkFormatoImpresion.Valor, NumeroInicial - 1, chkAgruparOperacion.Valor)
        'Else
        '    CReporte.LibroDiarioCG(frm, Where, Titulo, Subtitulo, vgUsuarioIdentificador, chkImprimirCabecera.Valor)
        'End If


    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub btnListar_Click(sender As System.Object, e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub PorFecha_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles PorFecha.CheckedChanged
        If PorFecha.Checked = True Then
            txtDesde.Enabled = True
            txtHasta.Enabled = True
            btnListar.Enabled = True
        Else
            txtDesde.Enabled = False
            txtHasta.Enabled = False
            btnListar.Enabled = False
        End If
    End Sub

    Private Sub frmLibroDiario_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub PorAsiento_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles PorAsiento.CheckedChanged
        If PorAsiento.Checked = True Then
            txtAsientoDesde.Enabled = True
            txtAsientoHasta.Enabled = True
            btnListar.Enabled = True
        Else
            txtAsientoDesde.Enabled = False
            txtAsientoHasta.Enabled = False
            btnListar.Enabled = False
        End If
    End Sub

    Private Sub chkFormatoImpresion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean)
        txtNumeroInicial.Enabled = value
    End Sub

    Private Sub chkPorOperacion_CheckedChanged(sender As Object, e As EventArgs) Handles chkPorOperacion.CheckedChanged
        cbxOperacion.Enabled = chkPorOperacion.Checked

    End Sub
End Class
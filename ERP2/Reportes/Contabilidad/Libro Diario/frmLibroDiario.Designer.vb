﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroDiario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.PorFecha = New System.Windows.Forms.CheckBox()
        Me.PorAsiento = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkPorOperacion = New System.Windows.Forms.CheckBox()
        Me.cbxOperacion = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtAsientoDesde = New ERP.ocxTXTNumeric()
        Me.txtAsientoHasta = New ERP.ocxTXTNumeric()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkAgruparOperacion = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNumeroInicial = New ERP.ocxTXTNumeric()
        Me.chkFormatoImpresion = New ERP.ocxCHK()
        Me.chkImprimirCabecera = New ERP.ocxCHK()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(339, 178)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(153, 23)
        Me.btnListar.TabIndex = 10
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(498, 178)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(87, 23)
        Me.btnCerrar.TabIndex = 11
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'PorFecha
        '
        Me.PorFecha.AutoSize = True
        Me.PorFecha.Checked = True
        Me.PorFecha.CheckState = System.Windows.Forms.CheckState.Checked
        Me.PorFecha.Location = New System.Drawing.Point(13, 22)
        Me.PorFecha.Name = "PorFecha"
        Me.PorFecha.Size = New System.Drawing.Size(75, 17)
        Me.PorFecha.TabIndex = 12
        Me.PorFecha.Text = "Por Fecha"
        Me.PorFecha.UseVisualStyleBackColor = True
        '
        'PorAsiento
        '
        Me.PorAsiento.AutoSize = True
        Me.PorAsiento.Location = New System.Drawing.Point(13, 55)
        Me.PorAsiento.Name = "PorAsiento"
        Me.PorAsiento.Size = New System.Drawing.Size(80, 17)
        Me.PorAsiento.TabIndex = 13
        Me.PorAsiento.Text = "Por Asiento"
        Me.PorAsiento.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkPorOperacion)
        Me.GroupBox1.Controls.Add(Me.cbxOperacion)
        Me.GroupBox1.Controls.Add(Me.txtHasta)
        Me.GroupBox1.Controls.Add(Me.PorAsiento)
        Me.GroupBox1.Controls.Add(Me.txtDesde)
        Me.GroupBox1.Controls.Add(Me.PorFecha)
        Me.GroupBox1.Controls.Add(Me.txtAsientoDesde)
        Me.GroupBox1.Controls.Add(Me.txtAsientoHasta)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(388, 149)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filtros"
        '
        'chkPorOperacion
        '
        Me.chkPorOperacion.AutoSize = True
        Me.chkPorOperacion.Location = New System.Drawing.Point(13, 98)
        Me.chkPorOperacion.Name = "chkPorOperacion"
        Me.chkPorOperacion.Size = New System.Drawing.Size(94, 17)
        Me.chkPorOperacion.TabIndex = 15
        Me.chkPorOperacion.Text = "Por Operacion"
        Me.chkPorOperacion.UseVisualStyleBackColor = True
        '
        'cbxOperacion
        '
        Me.cbxOperacion.CampoWhere = "IDOperacion"
        Me.cbxOperacion.CargarUnaSolaVez = False
        Me.cbxOperacion.DataDisplayMember = "Descripcion"
        Me.cbxOperacion.DataFilter = Nothing
        Me.cbxOperacion.DataOrderBy = Nothing
        Me.cbxOperacion.DataSource = "VOperacion"
        Me.cbxOperacion.DataValueMember = "ID"
        Me.cbxOperacion.dtSeleccionado = Nothing
        Me.cbxOperacion.Enabled = False
        Me.cbxOperacion.FormABM = Nothing
        Me.cbxOperacion.Indicaciones = Nothing
        Me.cbxOperacion.Location = New System.Drawing.Point(123, 96)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.SeleccionMultiple = False
        Me.cbxOperacion.SeleccionObligatoria = False
        Me.cbxOperacion.Size = New System.Drawing.Size(259, 23)
        Me.cbxOperacion.SoloLectura = False
        Me.cbxOperacion.TabIndex = 14
        Me.cbxOperacion.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 4, 12, 8, 11, 12, 345)
        Me.txtHasta.Location = New System.Drawing.Point(203, 19)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 5
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 4, 12, 8, 11, 12, 345)
        Me.txtDesde.Location = New System.Drawing.Point(123, 19)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 4
        '
        'txtAsientoDesde
        '
        Me.txtAsientoDesde.Color = System.Drawing.Color.Empty
        Me.txtAsientoDesde.Decimales = True
        Me.txtAsientoDesde.Indicaciones = Nothing
        Me.txtAsientoDesde.Location = New System.Drawing.Point(123, 51)
        Me.txtAsientoDesde.Name = "txtAsientoDesde"
        Me.txtAsientoDesde.Size = New System.Drawing.Size(74, 22)
        Me.txtAsientoDesde.SoloLectura = False
        Me.txtAsientoDesde.TabIndex = 7
        Me.txtAsientoDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAsientoDesde.Texto = "0"
        '
        'txtAsientoHasta
        '
        Me.txtAsientoHasta.Color = System.Drawing.Color.Empty
        Me.txtAsientoHasta.Decimales = True
        Me.txtAsientoHasta.Indicaciones = Nothing
        Me.txtAsientoHasta.Location = New System.Drawing.Point(203, 51)
        Me.txtAsientoHasta.Name = "txtAsientoHasta"
        Me.txtAsientoHasta.Size = New System.Drawing.Size(74, 22)
        Me.txtAsientoHasta.SoloLectura = False
        Me.txtAsientoHasta.TabIndex = 8
        Me.txtAsientoHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAsientoHasta.Texto = "0"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkAgruparOperacion)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtNumeroInicial)
        Me.GroupBox2.Controls.Add(Me.chkFormatoImpresion)
        Me.GroupBox2.Controls.Add(Me.chkImprimirCabecera)
        Me.GroupBox2.Location = New System.Drawing.Point(406, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(179, 149)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkAgruparOperacion
        '
        Me.chkAgruparOperacion.BackColor = System.Drawing.Color.Transparent
        Me.chkAgruparOperacion.Color = System.Drawing.Color.Empty
        Me.chkAgruparOperacion.Location = New System.Drawing.Point(12, 48)
        Me.chkAgruparOperacion.Name = "chkAgruparOperacion"
        Me.chkAgruparOperacion.Size = New System.Drawing.Size(130, 21)
        Me.chkAgruparOperacion.SoloLectura = False
        Me.chkAgruparOperacion.TabIndex = 24
        Me.chkAgruparOperacion.Texto = "Agrupar por Operacion"
        Me.chkAgruparOperacion.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 119)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Rubrica Desde:"
        '
        'txtNumeroInicial
        '
        Me.txtNumeroInicial.Color = System.Drawing.Color.Empty
        Me.txtNumeroInicial.Decimales = True
        Me.txtNumeroInicial.Enabled = False
        Me.txtNumeroInicial.Indicaciones = Nothing
        Me.txtNumeroInicial.Location = New System.Drawing.Point(96, 116)
        Me.txtNumeroInicial.Name = "txtNumeroInicial"
        Me.txtNumeroInicial.Size = New System.Drawing.Size(74, 22)
        Me.txtNumeroInicial.SoloLectura = False
        Me.txtNumeroInicial.TabIndex = 22
        Me.txtNumeroInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroInicial.Texto = "0"
        '
        'chkFormatoImpresion
        '
        Me.chkFormatoImpresion.BackColor = System.Drawing.Color.Transparent
        Me.chkFormatoImpresion.Color = System.Drawing.Color.Empty
        Me.chkFormatoImpresion.Location = New System.Drawing.Point(12, 75)
        Me.chkFormatoImpresion.Name = "chkFormatoImpresion"
        Me.chkFormatoImpresion.Size = New System.Drawing.Size(115, 21)
        Me.chkFormatoImpresion.SoloLectura = False
        Me.chkFormatoImpresion.TabIndex = 21
        Me.chkFormatoImpresion.Texto = "Formato Impresion"
        Me.chkFormatoImpresion.Valor = False
        '
        'chkImprimirCabecera
        '
        Me.chkImprimirCabecera.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimirCabecera.Color = System.Drawing.Color.Empty
        Me.chkImprimirCabecera.Location = New System.Drawing.Point(12, 22)
        Me.chkImprimirCabecera.Name = "chkImprimirCabecera"
        Me.chkImprimirCabecera.Size = New System.Drawing.Size(106, 21)
        Me.chkImprimirCabecera.SoloLectura = False
        Me.chkImprimirCabecera.TabIndex = 20
        Me.chkImprimirCabecera.Texto = "Imprimir Cabecera"
        Me.chkImprimirCabecera.Valor = False
        '
        'frmLibroDiario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(595, 213)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnListar)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmLibroDiario"
        Me.Text = "Libro Diario"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtAsientoHasta As ERP.ocxTXTNumeric
    Friend WithEvents txtAsientoDesde As ERP.ocxTXTNumeric
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents PorFecha As System.Windows.Forms.CheckBox
    Friend WithEvents PorAsiento As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNumeroInicial As ocxTXTNumeric
    Friend WithEvents chkFormatoImpresion As ocxCHK
    Friend WithEvents chkImprimirCabecera As ocxCHK
    Friend WithEvents chkPorOperacion As CheckBox
    Friend WithEvents cbxOperacion As ocxCBX
    Friend WithEvents chkAgruparOperacion As ocxCHK
End Class

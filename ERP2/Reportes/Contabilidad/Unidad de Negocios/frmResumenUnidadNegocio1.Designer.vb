﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResumenUnidadNegocio1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFiltros = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.dgvTotales = New System.Windows.Forms.DataGridView()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.chkDepartamento = New ERP.ocxCHK()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.chkCentroCosto = New ERP.ocxCHK()
        Me.cbxCentroCosto = New ERP.ocxCBX()
        Me.chkUnidadNegocio = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkTipo = New ERP.ocxCHK()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.chkSeccion = New ERP.ocxCHK()
        Me.cbxSeccion = New ERP.ocxCBX()
        CType(Me.dgvTotales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblFiltros
        '
        Me.lblFiltros.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFiltros.Location = New System.Drawing.Point(12, 9)
        Me.lblFiltros.Name = "lblFiltros"
        Me.lblFiltros.Size = New System.Drawing.Size(60, 18)
        Me.lblFiltros.TabIndex = 0
        Me.lblFiltros.Text = "Filtros:"
        Me.lblFiltros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Desde:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(44, 14)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Hasta:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(12, 458)
        Me.btnListar.Margin = New System.Windows.Forms.Padding(3, 3, 50, 3)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(141, 29)
        Me.btnListar.TabIndex = 15
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'btnExportar
        '
        Me.btnExportar.Enabled = False
        Me.btnExportar.Location = New System.Drawing.Point(12, 493)
        Me.btnExportar.Margin = New System.Windows.Forms.Padding(3, 3, 50, 3)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(141, 26)
        Me.btnExportar.TabIndex = 16
        Me.btnExportar.Text = "Exportar"
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'dgvTotales
        '
        Me.dgvTotales.AllowUserToAddRows = False
        Me.dgvTotales.AllowUserToDeleteRows = False
        Me.dgvTotales.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvTotales.Location = New System.Drawing.Point(272, 533)
        Me.dgvTotales.Name = "dgvTotales"
        Me.dgvTotales.ReadOnly = True
        Me.dgvTotales.Size = New System.Drawing.Size(1209, 105)
        Me.dgvTotales.TabIndex = 18
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLista.Location = New System.Drawing.Point(272, 1)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.Size = New System.Drawing.Size(1209, 533)
        Me.dgvLista.TabIndex = 17
        '
        'chkDepartamento
        '
        Me.chkDepartamento.BackColor = System.Drawing.Color.Transparent
        Me.chkDepartamento.Color = System.Drawing.Color.Empty
        Me.chkDepartamento.Location = New System.Drawing.Point(12, 243)
        Me.chkDepartamento.Name = "chkDepartamento"
        Me.chkDepartamento.Size = New System.Drawing.Size(139, 17)
        Me.chkDepartamento.SoloLectura = False
        Me.chkDepartamento.TabIndex = 9
        Me.chkDepartamento.Texto = "Departamento:"
        Me.chkDepartamento.Valor = False
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = ""
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = ""
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = ""
        Me.cbxDepartamento.DataSource = ""
        Me.cbxDepartamento.DataValueMember = ""
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.Enabled = False
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(12, 266)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(254, 24)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 10
        Me.cbxDepartamento.Texto = ""
        '
        'chkCentroCosto
        '
        Me.chkCentroCosto.BackColor = System.Drawing.Color.Transparent
        Me.chkCentroCosto.Color = System.Drawing.Color.Empty
        Me.chkCentroCosto.Location = New System.Drawing.Point(12, 137)
        Me.chkCentroCosto.Name = "chkCentroCosto"
        Me.chkCentroCosto.Size = New System.Drawing.Size(139, 17)
        Me.chkCentroCosto.SoloLectura = False
        Me.chkCentroCosto.TabIndex = 7
        Me.chkCentroCosto.Texto = "Centro de Costo:"
        Me.chkCentroCosto.Valor = False
        '
        'cbxCentroCosto
        '
        Me.cbxCentroCosto.CampoWhere = ""
        Me.cbxCentroCosto.CargarUnaSolaVez = False
        Me.cbxCentroCosto.DataDisplayMember = ""
        Me.cbxCentroCosto.DataFilter = Nothing
        Me.cbxCentroCosto.DataOrderBy = ""
        Me.cbxCentroCosto.DataSource = ""
        Me.cbxCentroCosto.DataValueMember = ""
        Me.cbxCentroCosto.dtSeleccionado = Nothing
        Me.cbxCentroCosto.Enabled = False
        Me.cbxCentroCosto.FormABM = Nothing
        Me.cbxCentroCosto.Indicaciones = Nothing
        Me.cbxCentroCosto.Location = New System.Drawing.Point(12, 160)
        Me.cbxCentroCosto.Name = "cbxCentroCosto"
        Me.cbxCentroCosto.SeleccionMultiple = False
        Me.cbxCentroCosto.SeleccionObligatoria = False
        Me.cbxCentroCosto.Size = New System.Drawing.Size(254, 24)
        Me.cbxCentroCosto.SoloLectura = False
        Me.cbxCentroCosto.TabIndex = 8
        Me.cbxCentroCosto.Texto = ""
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(12, 84)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(139, 17)
        Me.chkUnidadNegocio.SoloLectura = False
        Me.chkUnidadNegocio.TabIndex = 5
        Me.chkUnidadNegocio.Texto = "Unidad de Negocio:"
        Me.chkUnidadNegocio.Valor = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = ""
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = ""
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = ""
        Me.cbxUnidadNegocio.DataSource = ""
        Me.cbxUnidadNegocio.DataValueMember = ""
        Me.cbxUnidadNegocio.dtSeleccionado = Nothing
        Me.cbxUnidadNegocio.Enabled = False
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(12, 107)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionMultiple = True
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(254, 24)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 6
        Me.cbxUnidadNegocio.Texto = ""
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 17, 16, 39, 0, 258)
        Me.txtDesde.Location = New System.Drawing.Point(59, 32)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 2
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 17, 16, 39, 0, 258)
        Me.txtHasta.Location = New System.Drawing.Point(59, 58)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 4
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(12, 190)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(139, 17)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 11
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = ""
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = ""
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = ""
        Me.cbxSucursal.DataSource = ""
        Me.cbxSucursal.DataValueMember = ""
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(12, 213)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(254, 24)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 12
        Me.cbxSucursal.Texto = ""
        '
        'chkTipo
        '
        Me.chkTipo.BackColor = System.Drawing.Color.Transparent
        Me.chkTipo.Color = System.Drawing.Color.Empty
        Me.chkTipo.Location = New System.Drawing.Point(12, 402)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(139, 17)
        Me.chkTipo.SoloLectura = False
        Me.chkTipo.TabIndex = 13
        Me.chkTipo.Texto = "Tipo:"
        Me.chkTipo.Valor = False
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.dtSeleccionado = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(12, 425)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionMultiple = False
        Me.cbxTipo.SeleccionObligatoria = False
        Me.cbxTipo.Size = New System.Drawing.Size(157, 27)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 14
        Me.cbxTipo.Texto = ""
        '
        'chkSeccion
        '
        Me.chkSeccion.BackColor = System.Drawing.Color.Transparent
        Me.chkSeccion.Color = System.Drawing.Color.Empty
        Me.chkSeccion.Location = New System.Drawing.Point(12, 296)
        Me.chkSeccion.Name = "chkSeccion"
        Me.chkSeccion.Size = New System.Drawing.Size(60, 17)
        Me.chkSeccion.SoloLectura = False
        Me.chkSeccion.TabIndex = 19
        Me.chkSeccion.Texto = "Seccion:"
        Me.chkSeccion.Valor = False
        '
        'cbxSeccion
        '
        Me.cbxSeccion.CampoWhere = ""
        Me.cbxSeccion.CargarUnaSolaVez = False
        Me.cbxSeccion.DataDisplayMember = ""
        Me.cbxSeccion.DataFilter = Nothing
        Me.cbxSeccion.DataOrderBy = ""
        Me.cbxSeccion.DataSource = ""
        Me.cbxSeccion.DataValueMember = ""
        Me.cbxSeccion.dtSeleccionado = Nothing
        Me.cbxSeccion.Enabled = False
        Me.cbxSeccion.FormABM = Nothing
        Me.cbxSeccion.Indicaciones = Nothing
        Me.cbxSeccion.Location = New System.Drawing.Point(12, 319)
        Me.cbxSeccion.Name = "cbxSeccion"
        Me.cbxSeccion.SeleccionMultiple = False
        Me.cbxSeccion.SeleccionObligatoria = False
        Me.cbxSeccion.Size = New System.Drawing.Size(254, 24)
        Me.cbxSeccion.SoloLectura = False
        Me.cbxSeccion.TabIndex = 20
        Me.cbxSeccion.Texto = ""
        '
        'frmResumenUnidadNegocio1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1482, 640)
        Me.Controls.Add(Me.chkSeccion)
        Me.Controls.Add(Me.cbxSeccion)
        Me.Controls.Add(Me.dgvTotales)
        Me.Controls.Add(Me.dgvLista)
        Me.Controls.Add(Me.chkDepartamento)
        Me.Controls.Add(Me.cbxDepartamento)
        Me.Controls.Add(Me.chkCentroCosto)
        Me.Controls.Add(Me.cbxCentroCosto)
        Me.Controls.Add(Me.chkUnidadNegocio)
        Me.Controls.Add(Me.cbxUnidadNegocio)
        Me.Controls.Add(Me.lblFiltros)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDesde)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtHasta)
        Me.Controls.Add(Me.chkSucursal)
        Me.Controls.Add(Me.cbxSucursal)
        Me.Controls.Add(Me.chkTipo)
        Me.Controls.Add(Me.cbxTipo)
        Me.Controls.Add(Me.btnListar)
        Me.Controls.Add(Me.btnExportar)
        Me.Name = "frmResumenUnidadNegocio1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "frmResumenUnidadNegocio1"
        Me.Text = "frmResumenUnidadNegocio1"
        CType(Me.dgvTotales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblFiltros As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents Label2 As Label
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents chkSucursal As ocxCHK
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents chkTipo As ocxCHK
    Friend WithEvents cbxTipo As ocxCBX
    Friend WithEvents btnListar As Button
    Friend WithEvents btnExportar As Button
    Friend WithEvents chkUnidadNegocio As ocxCHK
    Friend WithEvents cbxUnidadNegocio As ocxCBX
    Friend WithEvents chkCentroCosto As ocxCHK
    Friend WithEvents cbxCentroCosto As ocxCBX
    Friend WithEvents chkDepartamento As ocxCHK
    Friend WithEvents cbxDepartamento As ocxCBX
    Friend WithEvents dgvTotales As DataGridView
    Friend WithEvents dgvLista As DataGridView
    Friend WithEvents chkSeccion As ocxCHK
    Friend WithEvents cbxSeccion As ocxCBX
End Class

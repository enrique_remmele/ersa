﻿Public Class frmResumenUnidadNegocio
    'Clases
    Dim CSistema As New CSistema

    'Funciones
    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        cbxSucursal.Conectar()

        'Ordenado por
        cbxTipo.cbx.Items.Add("Todos")
        cbxTipo.cbx.Items.Add("Balance General")
        cbxTipo.cbx.Items.Add("Estado de Resultado")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipo.cbx.SelectedIndex = 0
    End Sub

    Sub Listar()
        Dim vTipo As Integer = 0
        If chkTipo.Valor = True Then
            If cbxTipo.cbx.SelectedIndex = 0 Then
                vTipo = 0
            ElseIf cbxTipo.cbx.SelectedIndex = 1 Then
                vTipo = 1
            ElseIf cbxTipo.cbx.SelectedIndex = 2 Then
                vTipo = 2
            End If
        End If

        Dim sql As String = "Exec SpViewResumenUnidadNegocio @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "',@Tipo=" & vTipo & " "

        'Filtros

        If chkSucursal.chk.Checked Then
            sql = sql & ", @IDSucursal=" & cbxSucursal.GetValue
        End If

        'Solo los que tienen diferencias
        Dim dt As DataTable = CSistema.ExecuteToDataTable(sql)

        '
        CSistema.SqlToDataGrid(dgvLista, sql)

        'Formato
        If dgvLista.ColumnCount = 0 Then
            Exit Sub
        End If

        For i = 5 To dgvLista.ColumnCount - 1
            dgvLista.Columns(i).HeaderText = CSistema.ExecuteScalar("Select descripcion from unidadnegocio where id = " & dgvLista.Columns(i).Name)
            dgvLista.Columns(i).Name = CSistema.ExecuteScalar("Select descripcion from unidadnegocio where id = " & dgvLista.Columns(i).Name)
            If dgvLista.Columns(i).HeaderText = "" Then
                dgvLista.Columns(i).Name = "Sin Unid.Neg."
                dgvLista.Columns(i).HeaderText = "Sin Unid.Neg."
            End If
            CSistema.DataGridColumnasNumericas(dgvLista, {dgvLista.Columns(i).Name})
            dgvLista.Columns(i).Width = dgvLista.Columns(i).MinimumWidth()
        Next

        dgvLista.Columns("IDSucursal").Visible = False

        btnExportar.Enabled = True

        ListarTotales()

    End Sub

    Sub ListarTotales()

        dgvTotales.Columns.Clear()
        dgvTotales.Rows.Clear()


        For i = 4 To dgvLista.ColumnCount - 1
            dgvTotales.Columns.Add(dgvLista.Columns(i).Name, dgvLista.Columns(i).HeaderText)
        Next

        dgvTotales.Rows.Add()

        For e = 4 To dgvLista.ColumnCount - 1
            Dim suma As Decimal = 0

            For i = 0 To dgvLista.Rows.Count - 1
                If IsNumeric(dgvLista.Rows(i).Cells(e).Value) Then
                    suma = suma + dgvLista.Rows(i).Cells(e).Value
                End If
            Next
            dgvTotales.Rows(0).Cells(e - 4).Value = suma
            suma = 0
            CSistema.DataGridColumnasNumericas(dgvTotales, {dgvTotales.Columns(e - 4).Name})
        Next

    End Sub
  
    Private Sub frmControlExistencia_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me)
    End Sub

    Private Sub frmControlExistencia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
        cbxSucursal.cbx.SelectedIndex = 0
    End Sub

   
    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        CSistema.GridAExcel(dgvLista)
    End Sub

    Private Sub cbxTipo_Load(sender As Object, e As EventArgs) Handles cbxTipo.Load

    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroMayorUNCC
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnListar = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.chkImprimirCabecera = New ERP.ocxCHK()
        Me.chkResumido = New ERP.ocxCHK()
        Me.chkHojaRubricada = New ERP.ocxCHK()
        Me.chkListar = New ERP.ocxCHK()
        Me.chkIncluir = New ERP.ocxCHK()
        Me.cbxFormato = New ERP.ocxCBX()
        Me.cbxTipo = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkCentroCosto = New ERP.ocxCHK()
        Me.cbxCentroCosto = New ERP.ocxCBX()
        Me.chkUnidadNegocio = New ERP.ocxCHK()
        Me.cbxUnidadNegocio = New ERP.ocxCBX()
        Me.txtCuentaFinal = New ERP.ocxTXTCuentaContable()
        Me.txtCuentaInicial = New ERP.ocxTXTCuentaContable()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkImprimirCabecera)
        Me.GroupBox2.Controls.Add(Me.chkResumido)
        Me.GroupBox2.Controls.Add(Me.chkHojaRubricada)
        Me.GroupBox2.Controls.Add(Me.chkListar)
        Me.GroupBox2.Controls.Add(Me.chkIncluir)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.cbxFormato)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.cbxTipo)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(364, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 301)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Formato:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tipo:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(9, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkCentroCosto)
        Me.gbxFiltro.Controls.Add(Me.cbxCentroCosto)
        Me.gbxFiltro.Controls.Add(Me.chkUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.cbxUnidadNegocio)
        Me.gbxFiltro.Controls.Add(Me.Label2)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaFinal)
        Me.gbxFiltro.Controls.Add(Me.Label1)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaInicial)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(6, 2)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(350, 301)
        Me.gbxFiltro.TabIndex = 6
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Cuenta Final:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cuenta Inicial:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(545, 309)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 10
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 339)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(629, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(231, 309)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(125, 23)
        Me.btnListar.TabIndex = 9
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(6, 309)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(125, 23)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Analitico"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'chkImprimirCabecera
        '
        Me.chkImprimirCabecera.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimirCabecera.Color = System.Drawing.Color.Empty
        Me.chkImprimirCabecera.Location = New System.Drawing.Point(15, 262)
        Me.chkImprimirCabecera.Name = "chkImprimirCabecera"
        Me.chkImprimirCabecera.Size = New System.Drawing.Size(230, 21)
        Me.chkImprimirCabecera.SoloLectura = False
        Me.chkImprimirCabecera.TabIndex = 10
        Me.chkImprimirCabecera.Texto = "Imprimir Cabecera"
        Me.chkImprimirCabecera.Valor = False
        '
        'chkResumido
        '
        Me.chkResumido.BackColor = System.Drawing.Color.Transparent
        Me.chkResumido.Color = System.Drawing.Color.Empty
        Me.chkResumido.Location = New System.Drawing.Point(15, 156)
        Me.chkResumido.Name = "chkResumido"
        Me.chkResumido.Size = New System.Drawing.Size(207, 21)
        Me.chkResumido.SoloLectura = False
        Me.chkResumido.TabIndex = 5
        Me.chkResumido.Texto = "Informe Resumido"
        Me.chkResumido.Valor = False
        '
        'chkHojaRubricada
        '
        Me.chkHojaRubricada.BackColor = System.Drawing.Color.Transparent
        Me.chkHojaRubricada.Color = System.Drawing.Color.Empty
        Me.chkHojaRubricada.Location = New System.Drawing.Point(15, 237)
        Me.chkHojaRubricada.Name = "chkHojaRubricada"
        Me.chkHojaRubricada.Size = New System.Drawing.Size(204, 21)
        Me.chkHojaRubricada.SoloLectura = False
        Me.chkHojaRubricada.TabIndex = 8
        Me.chkHojaRubricada.Texto = "Hojas Rubricadas"
        Me.chkHojaRubricada.Valor = False
        '
        'chkListar
        '
        Me.chkListar.BackColor = System.Drawing.Color.Transparent
        Me.chkListar.Color = System.Drawing.Color.Empty
        Me.chkListar.Location = New System.Drawing.Point(15, 210)
        Me.chkListar.Name = "chkListar"
        Me.chkListar.Size = New System.Drawing.Size(204, 21)
        Me.chkListar.SoloLectura = False
        Me.chkListar.TabIndex = 7
        Me.chkListar.Texto = "Listar sólo los conceptos"
        Me.chkListar.Valor = False
        '
        'chkIncluir
        '
        Me.chkIncluir.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluir.Color = System.Drawing.Color.Empty
        Me.chkIncluir.Location = New System.Drawing.Point(15, 183)
        Me.chkIncluir.Name = "chkIncluir"
        Me.chkIncluir.Size = New System.Drawing.Size(207, 21)
        Me.chkIncluir.SoloLectura = False
        Me.chkIncluir.TabIndex = 6
        Me.chkIncluir.Texto = "Incluir cuentas sin saldo/movimientos"
        Me.chkIncluir.Valor = False
        '
        'cbxFormato
        '
        Me.cbxFormato.CampoWhere = Nothing
        Me.cbxFormato.CargarUnaSolaVez = False
        Me.cbxFormato.DataDisplayMember = Nothing
        Me.cbxFormato.DataFilter = Nothing
        Me.cbxFormato.DataOrderBy = Nothing
        Me.cbxFormato.DataSource = Nothing
        Me.cbxFormato.DataValueMember = Nothing
        Me.cbxFormato.FormABM = Nothing
        Me.cbxFormato.Indicaciones = Nothing
        Me.cbxFormato.Location = New System.Drawing.Point(9, 129)
        Me.cbxFormato.Name = "cbxFormato"
        Me.cbxFormato.SeleccionObligatoria = True
        Me.cbxFormato.Size = New System.Drawing.Size(213, 21)
        Me.cbxFormato.SoloLectura = False
        Me.cbxFormato.TabIndex = 4
        Me.cbxFormato.Texto = ""
        '
        'cbxTipo
        '
        Me.cbxTipo.CampoWhere = Nothing
        Me.cbxTipo.CargarUnaSolaVez = False
        Me.cbxTipo.DataDisplayMember = Nothing
        Me.cbxTipo.DataFilter = Nothing
        Me.cbxTipo.DataOrderBy = Nothing
        Me.cbxTipo.DataSource = Nothing
        Me.cbxTipo.DataValueMember = Nothing
        Me.cbxTipo.FormABM = Nothing
        Me.cbxTipo.Indicaciones = Nothing
        Me.cbxTipo.Location = New System.Drawing.Point(9, 84)
        Me.cbxTipo.Name = "cbxTipo"
        Me.cbxTipo.SeleccionObligatoria = True
        Me.cbxTipo.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipo.SoloLectura = False
        Me.cbxTipo.TabIndex = 3
        Me.cbxTipo.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 17, 16, 39, 0, 258)
        Me.txtHasta.Location = New System.Drawing.Point(89, 38)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 17, 16, 39, 0, 258)
        Me.txtDesde.Location = New System.Drawing.Point(9, 38)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkCentroCosto
        '
        Me.chkCentroCosto.BackColor = System.Drawing.Color.Transparent
        Me.chkCentroCosto.Color = System.Drawing.Color.Empty
        Me.chkCentroCosto.Location = New System.Drawing.Point(16, 219)
        Me.chkCentroCosto.Name = "chkCentroCosto"
        Me.chkCentroCosto.Size = New System.Drawing.Size(115, 21)
        Me.chkCentroCosto.SoloLectura = False
        Me.chkCentroCosto.TabIndex = 17
        Me.chkCentroCosto.Texto = "Centro de Costo:"
        Me.chkCentroCosto.Valor = False
        '
        'cbxCentroCosto
        '
        Me.cbxCentroCosto.CampoWhere = "IDCentroCosto"
        Me.cbxCentroCosto.CargarUnaSolaVez = False
        Me.cbxCentroCosto.DataDisplayMember = "Detalle"
        Me.cbxCentroCosto.DataFilter = Nothing
        Me.cbxCentroCosto.DataOrderBy = Nothing
        Me.cbxCentroCosto.DataSource = "vCentroCosto"
        Me.cbxCentroCosto.DataValueMember = "ID"
        Me.cbxCentroCosto.Enabled = False
        Me.cbxCentroCosto.FormABM = Nothing
        Me.cbxCentroCosto.Indicaciones = Nothing
        Me.cbxCentroCosto.Location = New System.Drawing.Point(137, 219)
        Me.cbxCentroCosto.Name = "cbxCentroCosto"
        Me.cbxCentroCosto.SeleccionObligatoria = False
        Me.cbxCentroCosto.Size = New System.Drawing.Size(198, 21)
        Me.cbxCentroCosto.SoloLectura = False
        Me.cbxCentroCosto.TabIndex = 18
        Me.cbxCentroCosto.Texto = ""
        '
        'chkUnidadNegocio
        '
        Me.chkUnidadNegocio.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidadNegocio.Color = System.Drawing.Color.Empty
        Me.chkUnidadNegocio.Location = New System.Drawing.Point(16, 191)
        Me.chkUnidadNegocio.Name = "chkUnidadNegocio"
        Me.chkUnidadNegocio.Size = New System.Drawing.Size(115, 21)
        Me.chkUnidadNegocio.SoloLectura = False
        Me.chkUnidadNegocio.TabIndex = 15
        Me.chkUnidadNegocio.Texto = "Unidad de negocio:"
        Me.chkUnidadNegocio.Valor = False
        '
        'cbxUnidadNegocio
        '
        Me.cbxUnidadNegocio.CampoWhere = "IDUnidadNegocio"
        Me.cbxUnidadNegocio.CargarUnaSolaVez = False
        Me.cbxUnidadNegocio.DataDisplayMember = "Descripcion"
        Me.cbxUnidadNegocio.DataFilter = Nothing
        Me.cbxUnidadNegocio.DataOrderBy = Nothing
        Me.cbxUnidadNegocio.DataSource = "VUnidadNegocio"
        Me.cbxUnidadNegocio.DataValueMember = "ID"
        Me.cbxUnidadNegocio.Enabled = False
        Me.cbxUnidadNegocio.FormABM = Nothing
        Me.cbxUnidadNegocio.Indicaciones = Nothing
        Me.cbxUnidadNegocio.Location = New System.Drawing.Point(137, 191)
        Me.cbxUnidadNegocio.Name = "cbxUnidadNegocio"
        Me.cbxUnidadNegocio.SeleccionObligatoria = False
        Me.cbxUnidadNegocio.Size = New System.Drawing.Size(198, 21)
        Me.cbxUnidadNegocio.SoloLectura = False
        Me.cbxUnidadNegocio.TabIndex = 16
        Me.cbxUnidadNegocio.Texto = ""
        '
        'txtCuentaFinal
        '
        Me.txtCuentaFinal.AlturaMaxima = 205
        Me.txtCuentaFinal.Consulta = Nothing
        Me.txtCuentaFinal.IDCentroCosto = 0
        Me.txtCuentaFinal.IDUnidadNegocio = 0
        Me.txtCuentaFinal.ListarTodas = False
        Me.txtCuentaFinal.Location = New System.Drawing.Point(17, 84)
        Me.txtCuentaFinal.Name = "txtCuentaFinal"
        Me.txtCuentaFinal.Registro = Nothing
        Me.txtCuentaFinal.Resolucion173 = False
        Me.txtCuentaFinal.Seleccionado = False
        Me.txtCuentaFinal.Size = New System.Drawing.Size(319, 27)
        Me.txtCuentaFinal.SoloLectura = False
        Me.txtCuentaFinal.TabIndex = 5
        Me.txtCuentaFinal.Texto = Nothing
        Me.txtCuentaFinal.whereFiltro = Nothing
        '
        'txtCuentaInicial
        '
        Me.txtCuentaInicial.AlturaMaxima = 205
        Me.txtCuentaInicial.Consulta = Nothing
        Me.txtCuentaInicial.IDCentroCosto = 0
        Me.txtCuentaInicial.IDUnidadNegocio = 0
        Me.txtCuentaInicial.ListarTodas = False
        Me.txtCuentaInicial.Location = New System.Drawing.Point(17, 34)
        Me.txtCuentaInicial.Name = "txtCuentaInicial"
        Me.txtCuentaInicial.Registro = Nothing
        Me.txtCuentaInicial.Resolucion173 = False
        Me.txtCuentaInicial.Seleccionado = False
        Me.txtCuentaInicial.Size = New System.Drawing.Size(319, 27)
        Me.txtCuentaInicial.SoloLectura = False
        Me.txtCuentaInicial.TabIndex = 1
        Me.txtCuentaInicial.Texto = Nothing
        Me.txtCuentaInicial.whereFiltro = Nothing
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(16, 160)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(115, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 0
        Me.chkTipoComprobante.Texto = "TipoComprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = Nothing
        Me.cbxTipoComprobante.DataOrderBy = "Descripcion"
        Me.cbxTipoComprobante.DataSource = "TipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(137, 160)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(198, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(16, 126)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(69, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(137, 126)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(198, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = ""
        '
        'frmLibroMayorUNCC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(629, 361)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnListar)
        Me.Controls.Add(Me.Button1)
        Me.Name = "frmLibroMayorUNCC"
        Me.Text = "frmLibroMayorUNCC"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkImprimirCabecera As ERP.ocxCHK
    Friend WithEvents chkResumido As ERP.ocxCHK
    Friend WithEvents chkHojaRubricada As ERP.ocxCHK
    Friend WithEvents chkListar As ERP.ocxCHK
    Friend WithEvents chkIncluir As ERP.ocxCHK
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbxFormato As ERP.ocxCBX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbxTipo As ERP.ocxCBX
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkCentroCosto As ERP.ocxCHK
    Friend WithEvents cbxCentroCosto As ERP.ocxCBX
    Friend WithEvents chkUnidadNegocio As ERP.ocxCHK
    Friend WithEvents cbxUnidadNegocio As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCuentaFinal As ERP.ocxTXTCuentaContable
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCuentaInicial As ERP.ocxTXTCuentaContable
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnListar As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class

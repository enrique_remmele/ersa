﻿Public Class frmResumenUnidadNegocio1
    'Clases
    Dim CSistema As New CSistema

    'Funciones
    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Controles
        cbxSucursal.Conectar()

        'Ordenado por
        'cbxTipo.cbx.Items.Add("Todos")
        'cbxTipo.cbx.Items.Add("Balance General")
        cbxTipo.cbx.Items.Add("Estado de Resultado")
        cbxTipo.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipo.cbx.SelectedIndex = 0

        CSistema.SqlToComboBox(cbxUnidadNegocio.cbx, "Select ID,Descripcion From vUnidadNegocio Where Estado = 1 Order by Descripcion")
        CSistema.SqlToComboBox(cbxCentroCosto.cbx, "Select ID,Descripcion From vCentroCosto Where Estado = 1 Order by Descripcion")
        CSistema.SqlToComboBox(cbxSucursal.cbx, "Select ID,Descripcion From VSucursal Where Estado = 1 Order by Descripcion")
        CSistema.SqlToComboBox(cbxDepartamento.cbx, "Select ID,Descripcion From vdepartamentoempresa Where Estado = 'True' Order by Descripcion")
        CSistema.SqlToComboBox(cbxSeccion.cbx, "Select ID,Seccion From vSeccion Where Estado = 'True' Order by Seccion")


    End Sub

    Sub Listar()
        Dim vTipo As Integer = 0
        'If chkTipo.Valor = True Then
        '    If cbxTipo.cbx.SelectedIndex = 0 Then
        '        vTipo = 0
        '    ElseIf cbxTipo.cbx.SelectedIndex = 1 Then
        '        vTipo = 1
        '    ElseIf cbxTipo.cbx.SelectedIndex = 2 Then
        vTipo = 2
        '    End If
        'End If
        Dim sql As String

        If chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUN @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "', @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDSucursal=NULL ,@Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = False And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocio1 @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "',@IDUnidadNegocio=NULL, @IDSucursal=NULL,@Tipo=" & vTipo
        End If

        If chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = True And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNSuc @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "', @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDSucursal=" & cbxSucursal.GetValue & ",@Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = False And chkSucursal.chk.Checked = True And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroSuc @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' , @IDUnidadNegocio= NULL, @IDSucursal=" & cbxSucursal.GetValue & ", @Tipo=" & vTipo
        End If

        If chkUnidadNegocio.chk.Checked = False And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = True And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroCC @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' , @IDUnidadNegocio= NULL, @IDSucursal=NULL, @IDCentroCosto= " & cbxCentroCosto.cbx.SelectedValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = True And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNCC @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "', @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDCentroCosto=" & cbxCentroCosto.GetValue & ",@Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = True And chkCentroCosto.chk.Checked = True And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNCCSUC @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "', @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDCentroCosto=" & cbxCentroCosto.GetValue & ", @IDSucursal=" & cbxSucursal.GetValue & " ,@Tipo=" & vTipo
        End If

        If chkUnidadNegocio.chk.Checked = False And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = True And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroDEP @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' , @IDDepartamento= " & cbxDepartamento.cbx.SelectedValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = True And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNDEP @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "', @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & " , @IDDepartamento= " & cbxDepartamento.cbx.SelectedValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = True And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = True And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNSUCDEP @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "', @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDSucursal=" & cbxSucursal.GetValue & " , @IDDepartamento= " & cbxDepartamento.cbx.SelectedValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = True And chkDepartamento.chk.Checked = True And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNCCDEP @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "', @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDCentroCosto=" & cbxCentroCosto.GetValue & " , @IDDepartamento= " & cbxDepartamento.cbx.SelectedValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = True And chkCentroCosto.chk.Checked = True And chkDepartamento.chk.Checked = True And chkSeccion.chk.Checked = False Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNSUCCCDEP @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "', @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDSucursal=" & cbxSucursal.GetValue & ", @IDCentroCosto=" & cbxCentroCosto.GetValue & " , @IDDepartamento= " & cbxDepartamento.cbx.SelectedValue & ", @Tipo=" & vTipo
        End If

        If chkUnidadNegocio.chk.Checked = False And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = True Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroSEC @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' , @IDSeccion= " & cbxSeccion.cbx.SelectedValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = False And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = True Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNSEC @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' , @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & " , @IDSeccion= " & cbxSeccion.cbx.SelectedValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = True And chkCentroCosto.chk.Checked = False And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = True Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNSECSUC @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' , @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & " , @IDSeccion= " & cbxSeccion.cbx.SelectedValue & ", @IDSucursal=" & cbxSucursal.GetValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = True And chkCentroCosto.chk.Checked = True And chkDepartamento.chk.Checked = False And chkSeccion.chk.Checked = True Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNCCSECSUC @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' , @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDCentroCosto=" & cbxCentroCosto.GetValue & " , @IDSeccion= " & cbxSeccion.cbx.SelectedValue & ", @IDSucursal=" & cbxSucursal.GetValue & ", @Tipo=" & vTipo
        ElseIf chkUnidadNegocio.chk.Checked = True And chkSucursal.chk.Checked = True And chkCentroCosto.chk.Checked = True And chkDepartamento.chk.Checked = True And chkSeccion.chk.Checked = True Then
            sql = "Exec SpViewResumenUnidadNegocioFiltroUNCCDEPSECSUC @Fecha1='" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "', @Fecha2='" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' , @IDUnidadNegocio= " & cbxUnidadNegocio.GetValue & ", @IDCentroCosto=" & cbxCentroCosto.GetValue & " , @IDDepartamento= " & cbxDepartamento.cbx.SelectedValue & " , @IDSeccion= " & cbxSeccion.cbx.SelectedValue & ", @IDSucursal=" & cbxSucursal.GetValue & ", @Tipo=" & vTipo
        End If

        'Solo los que tienen diferencias
        Dim dt As DataTable = CSistema.ExecuteToDataTable(sql)

        'Formato
        If dt.Rows.Count = 0 Then
            If MessageBox.Show("No se encontraron registros", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                Exit Sub
            End If
        End If

        dgvLista.Columns.Clear()
        'dgvLista.Rows.Clear()

        dgvTotales.Columns.Clear()
        'dgvTotales.Rows.Clear()

        CSistema.SqlToDataGrid(dgvLista, sql)

        'For i = 5 To dgvLista.ColumnCount - 1
        For i = 3 To dgvLista.ColumnCount - 1
            'dgvLista.Columns(i).HeaderText = CSistema.ExecuteScalar("Select descripcion from unidadnegocio where id = " & dgvLista.Columns(i).Name)
            'dgvLista.Columns(i).Name = CSistema.ExecuteScalar("Select descripcion from unidadnegocio where id = " & dgvLista.Columns(i).Name)
            'If dgvLista.Columns(i).HeaderText = "" Then
            '    dgvLista.Columns(i).Name = "Sin Unid.Neg."
            '    dgvLista.Columns(i).HeaderText = "Sin Unid.Neg."
            'End If
            CSistema.DataGridColumnasNumericas(dgvLista, {dgvLista.Columns(i).Name})
            'dgvLista.Columns().Width = dgvLista.Columns(i).MinimumWidth()
        Next

        'dgvLista.Columns("IDSucursal").Visible = False

        btnExportar.Enabled = True

        ListarTotales()

    End Sub

    Sub ListarTotales()

        dgvTotales.Columns.Clear()
        dgvTotales.Rows.Clear()

        'For i = 4 To dgvLista.ColumnCount - 1
        For i = 3 To dgvLista.ColumnCount - 1
            dgvTotales.Columns.Add(dgvLista.Columns(i).Name, dgvLista.Columns(i).HeaderText)
        Next

        dgvTotales.Rows.Add()
        'For e = 4 To dgvLista.ColumnCount - 1
        For e = 3 To dgvLista.ColumnCount - 1
            Dim suma As Decimal = 0

            For i = 0 To dgvLista.Rows.Count - 1
                If IsNumeric(dgvLista.Rows(i).Cells(e).Value) Then
                    suma = suma + dgvLista.Rows(i).Cells(e).Value
                End If
            Next
            'dgvTotales.Rows(0).Cells(e - 4).Value = suma
            dgvTotales.Rows(0).Cells(e - 3).Value = suma
            suma = 0
            'CSistema.DataGridColumnasNumericas(dgvTotales, {dgvTotales.Columns(e - 4).Name})
            CSistema.DataGridColumnasNumericas(dgvTotales, {dgvTotales.Columns(e - 3).Name})
        Next

    End Sub

    Private Sub frmControlExistencia_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me)
    End Sub

    Private Sub frmControlExistencia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
        cbxSucursal.cbx.SelectedIndex = 0
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click
        CSistema.GridAExcel(dgvLista)
    End Sub

    Private Sub chkUnidadNegocio_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkUnidadNegocio.PropertyChanged
        cbxUnidadNegocio.Enabled = value
        cbxUnidadNegocio.cbx.SelectedIndex = 0
    End Sub

    Private Sub chkCentroCosto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCentroCosto.PropertyChanged
        cbxCentroCosto.Enabled = value
        cbxCentroCosto.cbx.SelectedIndex = 0
    End Sub

    Private Sub chkDepartamento_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkDepartamento.PropertyChanged
        cbxDepartamento.Enabled = value
        cbxDepartamento.cbx.SelectedIndex = 0
    End Sub

    Private Sub chkSeccion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSeccion.PropertyChanged
        cbxSeccion.Enabled = value
        cbxSeccion.cbx.SelectedIndex = 0
    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmLibroMayorUNCC
    'CLASES
    Private CSistema As New CSistema
    Private CData As New CData
    Dim CReporte As New CReporteContabilidad

    'VARIABLES
    Dim Subtitulo As String

    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Conectar Cuentas
        txtCuentaFinal.Conectar()
        txtCuentaInicial.Conectar()

        'Tipo
        cbxTipo.cbx.Items.Add("Todos")
        cbxTipo.cbx.Items.Add("Débito")
        cbxTipo.cbx.Items.Add("Crédito")
        cbxTipo.cbx.SelectedIndex = 0

        'En Forma
        cbxFormato.cbx.Items.Add("Verificación")

        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()
        chkImprimirCabecera.Valor = True

    End Sub

    Sub Listar()

        Dim frm As New frmReporte

        If txtCuentaInicial.txtCodigo.GetValue = "" Then
            Dim mensaje As String = "Seleccion una cuenta Inicial!"
            ErrorProvider1.SetError(txtCuentaInicial, mensaje)
            ErrorProvider1.SetIconAlignment(txtCuentaInicial, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If txtCuentaFinal.txtCodigo.GetValue = "" Then
            Dim mensaje As String = "Seleccion una cuenta Final!"
            ErrorProvider1.SetError(txtCuentaFinal, mensaje)
            ErrorProvider1.SetIconAlignment(txtCuentaFinal, ErrorIconAlignment.MiddleRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Dim Titulo As String = "LIBRO MAYOR"

        CReporte.ArmarSubTitulo(Subtitulo, gbxFiltro, txtDesde, txtHasta, cbxTipo, chkResumido)

        Try
            CReporte.ImprimirLibroMayorUNCC(frm, Where(False), WhereDetalle, Titulo, Subtitulo, vgUsuarioIdentificador, chkResumido.Valor, chkImprimirCabecera.Valor)


        Catch ex As Exception

        End Try

    End Sub

    Function Where(ByVal Pantalla As Boolean) As String

        Where = "'" & txtCuentaInicial.txtCodigo.txt.Text & "','" & txtCuentaFinal.txtCodigo.txt.Text & "','" & CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False) & "','" & CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False) & "','" & Pantalla & "' "

    End Function

    Function WhereDetalle() As String

        WhereDetalle = ""

        If chkSucursal.Valor = True Then
            WhereDetalle = "IDSucursal = " & cbxSucursal.GetValue
        End If

        If chkCentroCosto.Valor = True Then
            If WhereDetalle = "" Then
                WhereDetalle = WhereDetalle & "IDCentroCosto = " & cbxCentroCosto.GetValue
            Else
                WhereDetalle = WhereDetalle & " and IDCentroCosto = " & cbxCentroCosto.GetValue
            End If
        End If

        If chkUnidadNegocio.Valor = True Then
            If WhereDetalle = "" Then
                WhereDetalle = WhereDetalle & "IDUnidadNegocio = " & cbxUnidadNegocio.GetValue
            Else
                WhereDetalle = WhereDetalle & " and IDUnidadNegocio = " & cbxUnidadNegocio.GetValue
            End If
        End If

        If chkTipoComprobante.Valor = True Then
            If WhereDetalle = "" Then
                WhereDetalle = WhereDetalle & "IDTipoComprobante= " & cbxTipoComprobante.GetValue
            Else
                WhereDetalle = WhereDetalle & " And IDTipoComprobante = " & cbxTipoComprobante.GetValue
            End If
        End If

            Select Case cbxTipo.cbx.SelectedIndex

                Case 0
                WhereDetalle = WhereDetalle
                Case 1
                    If WhereDetalle = "" Then
                        WhereDetalle = "Debito > 0"
                    Else
                        WhereDetalle = WhereDetalle & " And Debito > 0"
                    End If

                Case 2
                    If WhereDetalle = "" Then
                        WhereDetalle = " Credito > 0"
                    Else
                        WhereDetalle = WhereDetalle & " And Credito > 0"
                    End If

            End Select

    End Function

    Sub Analitico()

        Dim frm As New frmLibroIVAAnalitico
        Dim Titulo As String = "LIBRO MAYOR - " & "Del " & txtDesde.GetValue.ToShortDateString & " al " & txtHasta.GetValue.ToShortDateString

        frm.Titulo = Titulo
        frm.dtCuenta = CSistema.ExecuteToDataTable("Execute SpViewLibroMayor" & Where(True), "", 1000)
        frm.FechaDesde = txtDesde.GetValueString
        frm.FechaHasta = txtHasta.GetValueString

        If chkSucursal.Valor = True Then
            frm.IDSucursal = cbxSucursal.GetValue
            frm.Sucursal = cbxSucursal.Texto
        End If

        If chkTipoComprobante.Valor = True Then
            frm.IDTipoComprobante = cbxTipoComprobante.GetValue
            frm.TipoComprobante = cbxTipoComprobante.Texto
        End If

        frm.TipoCuenta = cbxTipo.cbx.SelectedIndex
        frm.dtAsiento = CData.FiltrarDataTable(frm.dtAsiento, WhereDetalle)

        FGMostrarFormulario(Me, frm, "Libo Mayor", Windows.Forms.FormBorderStyle.Sizable, FormStartPosition.CenterScreen, False, False)

    End Sub

    Sub CopiarCuenta()
        If txtCuentaInicial.txtCodigo.Texto = "" Then
            Exit Sub
        End If
        txtCuentaFinal.txtCodigo.Texto = txtCuentaInicial.txtCodigo.Texto
    End Sub

    Private Sub frmETL_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmETL_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListar.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Analitico()
    End Sub

    Private Sub txtCuentaInicial_Leave(sender As Object, e As System.EventArgs) Handles txtCuentaInicial.Leave
        CopiarCuenta()
    End Sub

    Private Sub chkUnidadNegocio_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkUnidadNegocio.PropertyChanged
        cbxUnidadNegocio.Enabled = value
    End Sub

    Private Sub chkCentroCosto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCentroCosto.PropertyChanged
        cbxCentroCosto.Enabled = value
    End Sub
End Class
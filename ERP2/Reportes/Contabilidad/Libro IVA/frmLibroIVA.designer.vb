﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroIVA
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.rbRetencion = New System.Windows.Forms.RadioButton()
        Me.rbCompra = New System.Windows.Forms.RadioButton()
        Me.rbVentas = New System.Windows.Forms.RadioButton()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNumeroInicial = New System.Windows.Forms.TextBox()
        Me.chkNotaDebitoPro = New ERP.ocxCHK()
        Me.chkNotaCreditoPro = New ERP.ocxCHK()
        Me.chkVenta = New ERP.ocxCHK()
        Me.chkNotaCreditoCli = New ERP.ocxCHK()
        Me.chkCompra = New ERP.ocxCHK()
        Me.chkNotaDebitoCli = New ERP.ocxCHK()
        Me.chkFormatoImpresion = New ERP.ocxCHK()
        Me.chkImprimirCabecera = New ERP.ocxCHK()
        Me.chkInformeResumido = New ERP.ocxCHK()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxCliente = New ERP.ocxCBX()
        Me.chkEstado = New ERP.ocxCHK()
        Me.cbxEstado = New ERP.ocxCBX()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxGrupo = New ERP.ocxCBX()
        Me.ocxGrupo = New ERP.ocxCHK()
        Me.chkCondicion = New ERP.ocxCHK()
        Me.chkTipoIVA = New ERP.ocxCHK()
        Me.cbxTipoDocumento = New ERP.ocxCBX()
        Me.cbxTipoIVA = New ERP.ocxCBX()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.chkTipoDocumento = New ERP.ocxCHK()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxCliente)
        Me.gbxFiltro.Controls.Add(Me.chkEstado)
        Me.gbxFiltro.Controls.Add(Me.cbxEstado)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxGrupo)
        Me.gbxFiltro.Controls.Add(Me.ocxGrupo)
        Me.gbxFiltro.Controls.Add(Me.chkCondicion)
        Me.gbxFiltro.Controls.Add(Me.chkTipoIVA)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoDocumento)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoIVA)
        Me.gbxFiltro.Controls.Add(Me.cbxCondicion)
        Me.gbxFiltro.Controls.Add(Me.chkTipoDocumento)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(178, 8)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(316, 281)
        Me.gbxFiltro.TabIndex = 1
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'rbRetencion
        '
        Me.rbRetencion.AutoSize = True
        Me.rbRetencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbRetencion.Location = New System.Drawing.Point(7, 186)
        Me.rbRetencion.Name = "rbRetencion"
        Me.rbRetencion.Size = New System.Drawing.Size(115, 17)
        Me.rbRetencion.TabIndex = 8
        Me.rbRetencion.TabStop = True
        Me.rbRetencion.Text = "Libro Retencion"
        Me.rbRetencion.UseVisualStyleBackColor = True
        '
        'rbCompra
        '
        Me.rbCompra.AutoSize = True
        Me.rbCompra.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCompra.Location = New System.Drawing.Point(7, 92)
        Me.rbCompra.Name = "rbCompra"
        Me.rbCompra.Size = New System.Drawing.Size(99, 17)
        Me.rbCompra.TabIndex = 4
        Me.rbCompra.TabStop = True
        Me.rbCompra.Text = "Libro Compra"
        Me.rbCompra.UseVisualStyleBackColor = True
        '
        'rbVentas
        '
        Me.rbVentas.AutoSize = True
        Me.rbVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbVentas.Location = New System.Drawing.Point(7, 9)
        Me.rbVentas.Name = "rbVentas"
        Me.rbVentas.Size = New System.Drawing.Size(90, 17)
        Me.rbVentas.TabIndex = 0
        Me.rbVentas.TabStop = True
        Me.rbVentas.Text = "Libro Venta"
        Me.rbVentas.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(672, 304)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 4
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtNumeroInicial)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.chkFormatoImpresion)
        Me.GroupBox2.Controls.Add(Me.chkImprimirCabecera)
        Me.GroupBox2.Controls.Add(Me.chkInformeResumido)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(499, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 281)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 66)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(491, 304)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(163, 23)
        Me.btnInforme.TabIndex = 3
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.rbVentas)
        Me.Panel1.Controls.Add(Me.chkNotaDebitoPro)
        Me.Panel1.Controls.Add(Me.rbRetencion)
        Me.Panel1.Controls.Add(Me.rbCompra)
        Me.Panel1.Controls.Add(Me.chkNotaCreditoPro)
        Me.Panel1.Controls.Add(Me.chkVenta)
        Me.Panel1.Controls.Add(Me.chkNotaCreditoCli)
        Me.Panel1.Controls.Add(Me.chkCompra)
        Me.Panel1.Controls.Add(Me.chkNotaDebitoCli)
        Me.Panel1.Location = New System.Drawing.Point(3, 13)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(170, 276)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(2, 196)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Rubrica Desde:"
        '
        'txtNumeroInicial
        '
        Me.txtNumeroInicial.Enabled = False
        Me.txtNumeroInicial.Location = New System.Drawing.Point(89, 196)
        Me.txtNumeroInicial.Name = "txtNumeroInicial"
        Me.txtNumeroInicial.Size = New System.Drawing.Size(100, 20)
        Me.txtNumeroInicial.TabIndex = 22
        '
        'chkNotaDebitoPro
        '
        Me.chkNotaDebitoPro.BackColor = System.Drawing.Color.Transparent
        Me.chkNotaDebitoPro.Color = System.Drawing.Color.Empty
        Me.chkNotaDebitoPro.Location = New System.Drawing.Point(30, 157)
        Me.chkNotaDebitoPro.Name = "chkNotaDebitoPro"
        Me.chkNotaDebitoPro.Size = New System.Drawing.Size(132, 21)
        Me.chkNotaDebitoPro.SoloLectura = False
        Me.chkNotaDebitoPro.TabIndex = 7
        Me.chkNotaDebitoPro.Texto = "Nota Debito Recibido"
        Me.chkNotaDebitoPro.Valor = False
        '
        'chkNotaCreditoPro
        '
        Me.chkNotaCreditoPro.BackColor = System.Drawing.Color.Transparent
        Me.chkNotaCreditoPro.Color = System.Drawing.Color.Empty
        Me.chkNotaCreditoPro.Location = New System.Drawing.Point(30, 136)
        Me.chkNotaCreditoPro.Name = "chkNotaCreditoPro"
        Me.chkNotaCreditoPro.Size = New System.Drawing.Size(138, 21)
        Me.chkNotaCreditoPro.SoloLectura = False
        Me.chkNotaCreditoPro.TabIndex = 6
        Me.chkNotaCreditoPro.Texto = "Nota Credito Recibido"
        Me.chkNotaCreditoPro.Valor = False
        '
        'chkVenta
        '
        Me.chkVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkVenta.Color = System.Drawing.Color.Empty
        Me.chkVenta.Location = New System.Drawing.Point(30, 30)
        Me.chkVenta.Name = "chkVenta"
        Me.chkVenta.Size = New System.Drawing.Size(104, 21)
        Me.chkVenta.SoloLectura = False
        Me.chkVenta.TabIndex = 1
        Me.chkVenta.Texto = "Factura de Venta"
        Me.chkVenta.Valor = False
        '
        'chkNotaCreditoCli
        '
        Me.chkNotaCreditoCli.BackColor = System.Drawing.Color.Transparent
        Me.chkNotaCreditoCli.Color = System.Drawing.Color.Empty
        Me.chkNotaCreditoCli.Location = New System.Drawing.Point(30, 49)
        Me.chkNotaCreditoCli.Name = "chkNotaCreditoCli"
        Me.chkNotaCreditoCli.Size = New System.Drawing.Size(120, 21)
        Me.chkNotaCreditoCli.SoloLectura = False
        Me.chkNotaCreditoCli.TabIndex = 2
        Me.chkNotaCreditoCli.Texto = "Nota Credito Emitido"
        Me.chkNotaCreditoCli.Valor = False
        '
        'chkCompra
        '
        Me.chkCompra.BackColor = System.Drawing.Color.Transparent
        Me.chkCompra.Color = System.Drawing.Color.Empty
        Me.chkCompra.Location = New System.Drawing.Point(30, 115)
        Me.chkCompra.Name = "chkCompra"
        Me.chkCompra.Size = New System.Drawing.Size(114, 21)
        Me.chkCompra.SoloLectura = False
        Me.chkCompra.TabIndex = 5
        Me.chkCompra.Texto = "Factura de Compra"
        Me.chkCompra.Valor = False
        '
        'chkNotaDebitoCli
        '
        Me.chkNotaDebitoCli.BackColor = System.Drawing.Color.Transparent
        Me.chkNotaDebitoCli.Color = System.Drawing.Color.Empty
        Me.chkNotaDebitoCli.Location = New System.Drawing.Point(30, 68)
        Me.chkNotaDebitoCli.Name = "chkNotaDebitoCli"
        Me.chkNotaDebitoCli.Size = New System.Drawing.Size(119, 21)
        Me.chkNotaDebitoCli.SoloLectura = False
        Me.chkNotaDebitoCli.TabIndex = 3
        Me.chkNotaDebitoCli.Texto = "Nota Debito Emitido"
        Me.chkNotaDebitoCli.Valor = False
        '
        'chkFormatoImpresion
        '
        Me.chkFormatoImpresion.BackColor = System.Drawing.Color.Transparent
        Me.chkFormatoImpresion.Color = System.Drawing.Color.Empty
        Me.chkFormatoImpresion.Location = New System.Drawing.Point(6, 166)
        Me.chkFormatoImpresion.Name = "chkFormatoImpresion"
        Me.chkFormatoImpresion.Size = New System.Drawing.Size(222, 21)
        Me.chkFormatoImpresion.SoloLectura = False
        Me.chkFormatoImpresion.TabIndex = 18
        Me.chkFormatoImpresion.Texto = "Formato Impresion"
        Me.chkFormatoImpresion.Valor = False
        '
        'chkImprimirCabecera
        '
        Me.chkImprimirCabecera.BackColor = System.Drawing.Color.Transparent
        Me.chkImprimirCabecera.Color = System.Drawing.Color.Empty
        Me.chkImprimirCabecera.Location = New System.Drawing.Point(6, 139)
        Me.chkImprimirCabecera.Name = "chkImprimirCabecera"
        Me.chkImprimirCabecera.Size = New System.Drawing.Size(230, 21)
        Me.chkImprimirCabecera.SoloLectura = False
        Me.chkImprimirCabecera.TabIndex = 9
        Me.chkImprimirCabecera.Texto = "Imprimir Cabecera"
        Me.chkImprimirCabecera.Valor = False
        '
        'chkInformeResumido
        '
        Me.chkInformeResumido.BackColor = System.Drawing.Color.Transparent
        Me.chkInformeResumido.Color = System.Drawing.Color.Empty
        Me.chkInformeResumido.Location = New System.Drawing.Point(6, 115)
        Me.chkInformeResumido.Name = "chkInformeResumido"
        Me.chkInformeResumido.Size = New System.Drawing.Size(230, 21)
        Me.chkInformeResumido.SoloLectura = False
        Me.chkInformeResumido.TabIndex = 8
        Me.chkInformeResumido.Texto = "Resumido"
        Me.chkInformeResumido.Valor = False
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 83)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 83)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 21, 15, 26, 50, 447)
        Me.txtHasta.Location = New System.Drawing.Point(89, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 21, 15, 26, 50, 447)
        Me.txtDesde.Location = New System.Drawing.Point(6, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Enabled = False
        Me.chkProveedor.Location = New System.Drawing.Point(9, 206)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(81, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 18
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = "RazonSocialReferencia"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = Nothing
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(113, 206)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(195, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 17
        Me.cbxProveedor.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Enabled = False
        Me.chkCliente.Location = New System.Drawing.Point(9, 183)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(67, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 16
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxCliente
        '
        Me.cbxCliente.CampoWhere = "IDCliente"
        Me.cbxCliente.CargarUnaSolaVez = False
        Me.cbxCliente.DataDisplayMember = "RazonSocialReferencia"
        Me.cbxCliente.DataFilter = Nothing
        Me.cbxCliente.DataOrderBy = Nothing
        Me.cbxCliente.DataSource = "VCliente"
        Me.cbxCliente.DataValueMember = "ID"
        Me.cbxCliente.dtSeleccionado = Nothing
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormABM = Nothing
        Me.cbxCliente.Indicaciones = Nothing
        Me.cbxCliente.Location = New System.Drawing.Point(113, 183)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.SeleccionMultiple = False
        Me.cbxCliente.SeleccionObligatoria = False
        Me.cbxCliente.Size = New System.Drawing.Size(195, 21)
        Me.cbxCliente.SoloLectura = False
        Me.cbxCliente.TabIndex = 15
        Me.cbxCliente.Texto = ""
        '
        'chkEstado
        '
        Me.chkEstado.BackColor = System.Drawing.Color.Transparent
        Me.chkEstado.Color = System.Drawing.Color.Empty
        Me.chkEstado.Location = New System.Drawing.Point(9, 251)
        Me.chkEstado.Name = "chkEstado"
        Me.chkEstado.Size = New System.Drawing.Size(67, 21)
        Me.chkEstado.SoloLectura = False
        Me.chkEstado.TabIndex = 14
        Me.chkEstado.Texto = "Estado:"
        Me.chkEstado.Valor = False
        '
        'cbxEstado
        '
        Me.cbxEstado.CampoWhere = Nothing
        Me.cbxEstado.CargarUnaSolaVez = False
        Me.cbxEstado.DataDisplayMember = Nothing
        Me.cbxEstado.DataFilter = Nothing
        Me.cbxEstado.DataOrderBy = Nothing
        Me.cbxEstado.DataSource = Nothing
        Me.cbxEstado.DataValueMember = Nothing
        Me.cbxEstado.dtSeleccionado = Nothing
        Me.cbxEstado.Enabled = False
        Me.cbxEstado.FormABM = Nothing
        Me.cbxEstado.Indicaciones = Nothing
        Me.cbxEstado.Location = New System.Drawing.Point(113, 251)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.SeleccionMultiple = False
        Me.cbxEstado.SeleccionObligatoria = False
        Me.cbxEstado.Size = New System.Drawing.Size(195, 21)
        Me.cbxEstado.SoloLectura = False
        Me.cbxEstado.TabIndex = 11
        Me.cbxEstado.Texto = ""
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(9, 160)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(67, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 12
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(113, 160)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(195, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 13
        Me.cbxMoneda.Texto = "GUARANIES"
        '
        'cbxGrupo
        '
        Me.cbxGrupo.CampoWhere = "IDTipoComprobante"
        Me.cbxGrupo.CargarUnaSolaVez = False
        Me.cbxGrupo.DataDisplayMember = "Descripcion"
        Me.cbxGrupo.DataFilter = Nothing
        Me.cbxGrupo.DataOrderBy = Nothing
        Me.cbxGrupo.DataSource = "VTipoComprobante"
        Me.cbxGrupo.DataValueMember = "ID"
        Me.cbxGrupo.dtSeleccionado = Nothing
        Me.cbxGrupo.Enabled = False
        Me.cbxGrupo.FormABM = Nothing
        Me.cbxGrupo.Indicaciones = Nothing
        Me.cbxGrupo.Location = New System.Drawing.Point(113, 91)
        Me.cbxGrupo.Name = "cbxGrupo"
        Me.cbxGrupo.SeleccionMultiple = False
        Me.cbxGrupo.SeleccionObligatoria = False
        Me.cbxGrupo.Size = New System.Drawing.Size(195, 21)
        Me.cbxGrupo.SoloLectura = False
        Me.cbxGrupo.TabIndex = 11
        Me.cbxGrupo.Texto = "GRUPO"
        '
        'ocxGrupo
        '
        Me.ocxGrupo.BackColor = System.Drawing.Color.Transparent
        Me.ocxGrupo.Color = System.Drawing.Color.Empty
        Me.ocxGrupo.Location = New System.Drawing.Point(9, 91)
        Me.ocxGrupo.Name = "ocxGrupo"
        Me.ocxGrupo.Size = New System.Drawing.Size(104, 21)
        Me.ocxGrupo.SoloLectura = False
        Me.ocxGrupo.TabIndex = 10
        Me.ocxGrupo.Texto = "Grupo:"
        Me.ocxGrupo.Valor = False
        '
        'chkCondicion
        '
        Me.chkCondicion.BackColor = System.Drawing.Color.Transparent
        Me.chkCondicion.Color = System.Drawing.Color.Empty
        Me.chkCondicion.Location = New System.Drawing.Point(9, 22)
        Me.chkCondicion.Name = "chkCondicion"
        Me.chkCondicion.Size = New System.Drawing.Size(104, 21)
        Me.chkCondicion.SoloLectura = False
        Me.chkCondicion.TabIndex = 0
        Me.chkCondicion.Texto = "Condicion:"
        Me.chkCondicion.Valor = False
        '
        'chkTipoIVA
        '
        Me.chkTipoIVA.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoIVA.Color = System.Drawing.Color.Empty
        Me.chkTipoIVA.Location = New System.Drawing.Point(9, 45)
        Me.chkTipoIVA.Name = "chkTipoIVA"
        Me.chkTipoIVA.Size = New System.Drawing.Size(104, 21)
        Me.chkTipoIVA.SoloLectura = False
        Me.chkTipoIVA.TabIndex = 2
        Me.chkTipoIVA.Texto = "Tipo IVA:"
        Me.chkTipoIVA.Valor = False
        '
        'cbxTipoDocumento
        '
        Me.cbxTipoDocumento.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoDocumento.CargarUnaSolaVez = False
        Me.cbxTipoDocumento.DataDisplayMember = "Descripcion"
        Me.cbxTipoDocumento.DataFilter = Nothing
        Me.cbxTipoDocumento.DataOrderBy = Nothing
        Me.cbxTipoDocumento.DataSource = "VTipoComprobante"
        Me.cbxTipoDocumento.DataValueMember = "ID"
        Me.cbxTipoDocumento.dtSeleccionado = Nothing
        Me.cbxTipoDocumento.Enabled = False
        Me.cbxTipoDocumento.FormABM = Nothing
        Me.cbxTipoDocumento.Indicaciones = Nothing
        Me.cbxTipoDocumento.Location = New System.Drawing.Point(113, 68)
        Me.cbxTipoDocumento.Name = "cbxTipoDocumento"
        Me.cbxTipoDocumento.SeleccionMultiple = False
        Me.cbxTipoDocumento.SeleccionObligatoria = False
        Me.cbxTipoDocumento.Size = New System.Drawing.Size(195, 21)
        Me.cbxTipoDocumento.SoloLectura = False
        Me.cbxTipoDocumento.TabIndex = 5
        Me.cbxTipoDocumento.Texto = "REMISIONES EMITIDAS"
        '
        'cbxTipoIVA
        '
        Me.cbxTipoIVA.CampoWhere = ""
        Me.cbxTipoIVA.CargarUnaSolaVez = False
        Me.cbxTipoIVA.DataDisplayMember = ""
        Me.cbxTipoIVA.DataFilter = Nothing
        Me.cbxTipoIVA.DataOrderBy = ""
        Me.cbxTipoIVA.DataSource = ""
        Me.cbxTipoIVA.DataValueMember = ""
        Me.cbxTipoIVA.dtSeleccionado = Nothing
        Me.cbxTipoIVA.Enabled = False
        Me.cbxTipoIVA.FormABM = Nothing
        Me.cbxTipoIVA.Indicaciones = Nothing
        Me.cbxTipoIVA.Location = New System.Drawing.Point(113, 45)
        Me.cbxTipoIVA.Name = "cbxTipoIVA"
        Me.cbxTipoIVA.SeleccionMultiple = False
        Me.cbxTipoIVA.SeleccionObligatoria = False
        Me.cbxTipoIVA.Size = New System.Drawing.Size(195, 21)
        Me.cbxTipoIVA.SoloLectura = False
        Me.cbxTipoIVA.TabIndex = 3
        Me.cbxTipoIVA.Texto = "DIRECTO"
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = ""
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = ""
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = ""
        Me.cbxCondicion.DataSource = ""
        Me.cbxCondicion.DataValueMember = ""
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.Enabled = False
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(113, 22)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = False
        Me.cbxCondicion.Size = New System.Drawing.Size(195, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 1
        Me.cbxCondicion.Texto = "CONTADO+CREDITO"
        '
        'chkTipoDocumento
        '
        Me.chkTipoDocumento.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoDocumento.Color = System.Drawing.Color.Empty
        Me.chkTipoDocumento.Location = New System.Drawing.Point(9, 68)
        Me.chkTipoDocumento.Name = "chkTipoDocumento"
        Me.chkTipoDocumento.Size = New System.Drawing.Size(104, 21)
        Me.chkTipoDocumento.SoloLectura = False
        Me.chkTipoDocumento.TabIndex = 4
        Me.chkTipoDocumento.Texto = "Tipo Documento:"
        Me.chkTipoDocumento.Valor = False
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(9, 137)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(67, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 8
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudad"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Descripcion"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = "Descripcion"
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(113, 137)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(195, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 9
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 114)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(69, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(113, 114)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(195, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'frmLibroIVA
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(747, 339)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmLibroIVA"
        Me.Text = "Libro IVA"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkTipoDocumento As ERP.ocxCHK
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents cbxTipoIVA As ERP.ocxCBX
    Friend WithEvents cbxTipoDocumento As ERP.ocxCBX
    Friend WithEvents chkNotaDebitoPro As ERP.ocxCHK
    Friend WithEvents chkNotaCreditoPro As ERP.ocxCHK
    Friend WithEvents chkCompra As ERP.ocxCHK
    Friend WithEvents chkNotaDebitoCli As ERP.ocxCHK
    Friend WithEvents chkNotaCreditoCli As ERP.ocxCHK
    Friend WithEvents chkVenta As ERP.ocxCHK
    Friend WithEvents rbRetencion As System.Windows.Forms.RadioButton
    Friend WithEvents rbCompra As System.Windows.Forms.RadioButton
    Friend WithEvents rbVentas As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkCondicion As ERP.ocxCHK
    Friend WithEvents chkTipoIVA As ERP.ocxCHK
    Friend WithEvents chkInformeResumido As ERP.ocxCHK
    Friend WithEvents chkImprimirCabecera As ERP.ocxCHK
    Friend WithEvents cbxGrupo As ERP.ocxCBX
    Friend WithEvents ocxGrupo As ERP.ocxCHK
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents chkEstado As ERP.ocxCHK
    Friend WithEvents cbxEstado As ERP.ocxCBX
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents cbxCliente As ERP.ocxCBX
    Friend WithEvents chkFormatoImpresion As ocxCHK
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNumeroInicial As TextBox
End Class

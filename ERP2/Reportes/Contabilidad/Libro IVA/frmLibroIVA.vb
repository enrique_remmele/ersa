﻿Imports ERP.Reporte

Public Class frmLibroIVA

    'CLASES
    Dim CReporte As New CReporteContabilidad
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = ""
    Dim TipoInforme As String = ""
    Dim Where As String = ""
    Dim WhereDetalle As String = ""
    Dim Where2 As String = ""
    Dim vdtCuentaBancaria As DataTable

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

        CambiarOrdenacion()
        cbxEstado.Texto = "TODOS"
    End Sub

    Sub CargarInformacion()

        'Condición
        cbxCondicion.cbx.Items.Add("CONTADO+CREDITO")
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")

        'Tipo IVA
        cbxTipoIVA.cbx.Items.Add("AMBOS")
        cbxTipoIVA.cbx.Items.Add("DIRECTO")
        cbxTipoIVA.cbx.Items.Add("INDISTINTO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipo IVA
        cbxEstado.cbx.Items.Add("TODOS")
        cbxEstado.cbx.Items.Add("ACTIVOS")
        cbxEstado.cbx.Items.Add("ANULADOS")

        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
        chkImprimirCabecera.Valor = True

    End Sub

    Sub Listar()

        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Inicializar condiciones
        Where = ""
        Where2 = ""
        WhereDetalle = ""

        'Tituto
        If rbVentas.Checked = True Then
            Titulo = "LIBRO IVA VENTA"
        End If

        If rbCompra.Checked = True Then
            Titulo = "LIBRO IVA COMPRA"
        End If

        If rbRetencion.Checked = True Then
            Titulo = "LIBRO IVA RETENCION"
        End If

        Where = " Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "


        'Tipo Informe
        TipoInforme = Nothing
        'Fecha
        TipoInforme = TipoInforme & "Fecha desde " & txtDesde.txt.Text & " Hasta " & txtHasta.txt.Text & " "

        'Tipo IVA
        If chkTipoIVA.chk.Checked = True Then
            TipoInforme = TipoInforme & "- Tipo I.V.A. " & cbxTipoIVA.Texto & " "
        End If

        'Tipo Documento
        If chkTipoDocumento.chk.Checked = True Then
            TipoInforme = TipoInforme & "- Tipo Documento " & cbxTipoDocumento.Texto & " "
        End If

        'Condicion
        If chkCondicion.chk.Checked = True Then
            TipoInforme = TipoInforme & "- Condición " & cbxCondicion.Texto & " "
        End If

        'Sucursal
        If chkSucursal.chk.Checked = True Then
            TipoInforme = TipoInforme & "- Sucursal " & cbxSucursal.Texto & " "
        End If

        'Ciudad
        If chkCiudad.chk.Checked = True Then
            TipoInforme = TipoInforme & "- Ciudad " & cbxCiudad.Texto & " "
        End If

        'Moneda
        If chkMoneda.chk.Checked = True Then
            TipoInforme = TipoInforme & "- Moneda " & cbxMoneda.Texto & " "
        End If

        'Ordenado
        If cbxCondicion.Texto = "" Then
            TipoInforme = TipoInforme & "- Ordenado por " & cbxOrdenadoPor.Texto & " - " & cbxEnForma.Texto & ""
        End If

        'Resumido
        If chkInformeResumido.chk.Checked = True Then
            TipoInforme = TipoInforme & "- RESUMIDO"
        End If

        'Concición de Venta
        If cbxCondicion.txt.Text = "CREDITO" And cbxCondicion.Enabled = True Then
            Where2 = " Where Credito='True' "
        End If

        If cbxCondicion.txt.Text = "CONTADO" And cbxCondicion.Enabled = True Then
            Where2 = " Where Credito='False' "
        End If

        'Tipo IVA de Compra
        If cbxTipoIVA.txt.Text = "DIRECTO" And cbxTipoIVA.Enabled = True Then
            If Where2 = "" Then
                Where2 = " Where Directo='True' "
            Else
                Where2 = Where2 & " And Directo='True' "
            End If
        End If

        If cbxTipoIVA.txt.Text = "INDISTINTO" And cbxTipoIVA.Enabled = True Then
            If Where2 = "" Then
                Where2 = " Where Directo='False' "
            Else
                Where2 = Where2 & " And Directo='False' "
            End If
        End If

        WhereDetalle = WhereDetalle & Where2
        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(WhereDetalle)
            End If
siguiente:

        Next

        'Establecemos el Orden
        OrderBy = " Order By Fecha, Comprobante"
        If cbxOrdenadoPor.cbx.Text <> "" And cbxOrdenadoPor.cbx.Text <> "Fecha" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text & " " & cbxEnForma.cbx.Text & ", Fecha"
        End If


        Where = Where & EstablecerCondicionDocumento()

        If WhereDetalle = "" Then
            Where = "Where " & Where
        Else
            Where = "And " & Where
        End If

        'CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        If chkEstado.Valor = True Then
            If cbxEstado.cbx.SelectedIndex = 1 Then 'activos
                Where = Where & " and RazonSocial <> '-----Comprobante Anulado-----' "
                TipoInforme = TipoInforme = " ANULADOS"
            ElseIf cbxEstado.cbx.SelectedIndex = 2 Then ' anulados
                Where = Where & " and RazonSocial = '-----Comprobante Anulado-----' "
            End If
        End If
        Dim NumeroInicial As Integer = 1

        If chkFormatoImpresion.Valor Then
            If IsNumeric(txtNumeroInicial.Text) Then
                NumeroInicial = CInt(txtNumeroInicial.Text)
            End If
        End If

        CReporte.ImprimirLibroIVA(frm, WhereDetalle, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, chkInformeResumido.Valor, chkImprimirCabecera.Valor, NumeroInicial - 1)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VLibroIVA ", "Select Top(0) Fecha, Comprobante, [Cod.], RazonSocial, RUC, Total, Sucursal, Ciudad, Tipo, Moneda From VLibroIVA ")


        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Function EstablecerCondicionDocumento() As String

        EstablecerCondicionDocumento = ""

        Dim WhereLocal As String

        WhereLocal = " And ("

        If chkVenta.Valor = True Then

            If WhereLocal = " And (" Then
                WhereLocal = WhereLocal & " Tipo= 'Venta' "
            Else
                WhereLocal = WhereLocal & " Or Tipo= 'Venta'"

            End If
        End If

        If chkNotaCreditoCli.Valor = True Then
            If WhereLocal = " And (" Then
                WhereLocal = WhereLocal & " Tipo= 'Nota Credito Emitido' "
            Else
                WhereLocal = WhereLocal & " Or tipo= 'Nota Credito Emitido'"
            End If
        End If

        If chkNotaDebitoCli.Valor = True Then
            If WhereLocal = " And (" Then
                WhereLocal = WhereLocal & "  Tipo= 'Nota Debito Emitido' "
            Else
                WhereLocal = WhereLocal & " Or Tipo= 'Nota Debito Emitido'"
            End If
        End If

        If chkCompra.Valor = True Then
            If WhereLocal = " And (" Then
                WhereLocal = WhereLocal & "  Tipo= 'Compra' "
            Else
                WhereLocal = WhereLocal & " Or Tipo= 'Compra'"
            End If
        End If

        If chkNotaCreditoPro.Valor = True Then
            If WhereLocal = " And (" Then
                WhereLocal = WhereLocal & "  Tipo= 'Nota Credito Recibido' "
            Else
                WhereLocal = WhereLocal & " Or Tipo= 'Nota Credito Recibido'"
            End If
        End If

        If chkNotaDebitoPro.Valor = True Then
            If WhereLocal = " And (" Then
                WhereLocal = WhereLocal & "  Tipo= 'Nota Debito Recibido' "
            Else
                WhereLocal = WhereLocal & " Or Tipo= 'Nota Debito Recibido'"
            End If
        End If

        If rbRetencion.Checked = True Then
            If WhereLocal = " And (" Then
                WhereLocal = WhereLocal & "  Tipo= 'Retencion' "
            Else
                WhereLocal = WhereLocal & " Or Tipo= 'Retencion'"
            End If
        End If



        WhereLocal = WhereLocal & ") "

        Return WhereLocal

    End Function

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxCondicion.Enabled = value
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkTipoDocumento_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoDocumento.PropertyChanged
        cbxTipoDocumento.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chklbDocumentos_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)

        'If e.CurrentValue = CheckState.Checked Then
        '    Select Case chklbDocumentos.SelectedIndex
        '        Case 0
        '            cbxTipoIVA.Enabled = False
        '        Case 6
        '            cbxCondicion.Enabled = False
        '    End Select
        'Else
        '    Select Case chklbDocumentos.SelectedIndex
        '        Case 0
        '            cbxTipoIVA.Enabled = True
        '        Case 6
        '            cbxCondicion.Enabled = True
        '    End Select
        'End If
    End Sub

    Private Sub rbVentas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbVentas.CheckedChanged
        chkVenta.Valor = rbVentas.Checked
        chkNotaCreditoCli.Valor = rbVentas.Checked
        chkNotaDebitoCli.Valor = rbVentas.Checked
        chkCliente.Enabled = rbVentas.Checked
    End Sub

    Private Sub rbCompra_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCompra.CheckedChanged
        chkCompra.Valor = rbCompra.Checked
        chkNotaCreditoPro.Valor = rbCompra.Checked
        chkNotaDebitoPro.Valor = rbCompra.Checked
        chkProveedor.Enabled = rbCompra.Checked
    End Sub

    'Private Sub chkTipoIVA_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTipoIVA.Load

    'End Sub

    Private Sub chkTipoIVA_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoIVA.PropertyChanged
        cbxTipoIVA.Enabled = value
    End Sub

    Private Sub chkCondicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCondicion.PropertyChanged
        cbxCondicion.Enabled = value
    End Sub

    Private Sub rbRetencion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbRetencion.CheckedChanged

    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkEstado_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkEstado.PropertyChanged
        cbxEstado.Enabled = value
    End Sub

    
    Private Sub chkCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged

        Select Case chkCliente.chk.Checked
            Case True
                cbxCliente.Enabled = True
            Case False
                cbxCliente.Enabled = False

        End Select
    End Sub

    Private Sub chkProveedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProveedor.PropertyChanged
        Select Case chkProveedor.chk.Checked
            Case True
                cbxProveedor.Enabled = True
            Case False
                cbxProveedor.Enabled = False

        End Select
    End Sub

    Private Sub chkFormatoImpresion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFormatoImpresion.PropertyChanged

        If value Then
            chkInformeResumido.Valor = Not value
        End If

        chkInformeResumido.Enabled = Not value
        txtNumeroInicial.Enabled = value

    End Sub
End Class




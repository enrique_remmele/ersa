﻿Imports ERP.Reporte
Public Class frmControlIntegridad
    Dim CReporte As New CReporteContabilidad
    Dim CSistema As New CSistema

    'VARIABLES
    Dim vControles() As Control  'SC:10/02/2022 - Nuevo

    Sub Inicializar()

        'Controles
        CSistema.InicializaControles(Me) 'SC:10/02/2022 - Nuevo

        CSistema.SqlToComboBox(cbxMes, CSistema.RetornardtMeses(), "ID", "Descripcion")
        'SC:10/02/2022 - Se comenta la linea de abajo por el cambio de vista para configurar cuenta que se van a validar con el Control de Integridad
        'txtCuentaContable.Conectar("Select * from VCuentaContable where codigo in (select distinct CuentaContableCompra from producto where ControlarExistencia = 1)")

        'SC:10/02/2022 - Nuevo
        txtCuentaContable.Conectar("Select * from VCuentaContable where codigo in (select distinct codigo from vIntegridadCuentaContable where estado = 1)")
        chkDetalle.chk.Checked = True
        'SC:10/02/2022 - Nuevo
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        ReDim vControles(-1)

        CSistema.CargaControl(vControles, txtCuentaContable)

        txtCuentaContable.Conectar()

    End Sub

    Sub Listar()
        Dim SubTitulo As String = ""
        Dim frm As New frmReporte
        If txtCuentaContable.Seleccionado = False Then
            MessageBox.Show("Debe seleccionar una cuenta contable", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If cbxMes.Text = "" Then
            MessageBox.Show("Debe seleccionar un Mes", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If chkDetalle.chk.Checked Then
            Dim dtDetalle As DataTable = CSistema.ExecuteToDataTable("exec SpMovimientosOperativosContraContablesDetalle @vCodigo='" & txtCuentaContable.Registro("Codigo") & "',@vMes=" & cbxMes.SelectedValue & ",@vAño=" & txtAño.Value & ", @SoloMostrarDiferencias=1", "", 500)
            Dim form As New frmDetalleDiferenciasControlIntegridad
            form.dt = dtDetalle
            form.ShowDialog()
        Else
            SubTitulo = "Cuenta Contable: " & txtCuentaContable.Registro("Cuenta") & " - Mes: " & cbxMes.Text & " Año:" & txtAño.Value.ToString
            CReporte.ControlIntegridad(frm, vgUsuarioIdentificador, SubTitulo, txtCuentaContable.txtCodigo.Texto, txtAño.Value, cbxMes.SelectedValue)

        End If

    End Sub

    Sub InicializarControles()


        txtCuentaContable.Registro("Cuenta") = ""


        'Error
        ctrError.Clear()

        'Foco
        txtCuentaContable.Registro("Cuenta").Focus()


    End Sub
    Private Sub frmControlIntegridad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnListar_Click(sender As Object, e As EventArgs) Handles btnListar.Click
        Listar()
    End Sub
End Class
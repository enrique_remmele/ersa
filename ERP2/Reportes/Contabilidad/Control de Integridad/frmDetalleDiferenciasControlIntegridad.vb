﻿Public Class frmDetalleDiferenciasControlIntegridad
    Dim CSistema As New CSistema
    Public Property dt As DataTable

    Sub Inicializar()

        CSistema.dtToGrid(DataGridView1, dt)
        CSistema.DataGridColumnasVisibles(DataGridView1, {"Fecha", "Operacion", "OperacionComprobante", "TipoComprobante", "Entradas", "Salidas", "Debito", "Credito", "Diferencia Entrada", "Diferencia Salida"})
        CSistema.DataGridColumnasNumericas(DataGridView1, {"Entradas", "Salidas", "Debito", "Credito", "Diferencia Entrada", "Diferencia Salida"})
        DataGridView1.Columns("OperacionComprobante").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        txtCantidadComprobante.SetValue(DataGridView1.Rows.Count)
        TxtTotalDiferenciaEntrada.SetValue(CSistema.gridSumColumn(DataGridView1, "Diferencia Entrada"))
        TxtTotalDiferenciaSalida.SetValue(CSistema.gridSumColumn(DataGridView1, "Diferencia Salida"))
    End Sub

    Sub VerDetalle()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Sub
        End If

        Dim IDTransaccion As Integer
        IDTransaccion = DataGridView1.SelectedRows(0).Cells("IDTransaccion").Value

        Dim IDSucursalOperacion As Integer
        IDSucursalOperacion = DataGridView1.SelectedRows(0).Cells("IDSucursal").Value

        Select Case DataGridView1.SelectedRows(0).Cells("Operacion").Value

            Case "MOVIMIENTOS"
                Dim frmMovimientoStock As New frmMovimientoStock
                frmMovimientoStock.IDTransaccion = IDTransaccion
                frmMovimientoStock.show()
                frmMovimientoStock.CargarOperacion(IDTransaccion)

            Case "DESCARGASTOCK"
                Dim frmDescargaStock As New frmDescargaStock
                frmDescargaStock.IDTransaccion = IDTransaccion
                frmDescargaStock.Show()
                frmDescargaStock.CargarOperacion(IDTransaccion)

            Case "MOVIMIENTO MATERIA PRIMA"
                Dim frmMovimientoMateriaPrima As New frmMovimientoMateriaPrima
                frmMovimientoMateriaPrima.IDTransaccion = IDTransaccion
                frmMovimientoMateriaPrima.Show()
                frmMovimientoMateriaPrima.CargarOperacion(IDTransaccion)

            Case "MOVIMIENTO DESCARGA DE COMPRA"
                Dim frmMovimientoDescargaCompra As New frmMovimientoDescargaCompra
                frmMovimientoDescargaCompra.IDTransaccion = IDTransaccion
                frmMovimientoDescargaCompra.Show()
                frmMovimientoDescargaCompra.CargarOperacion(IDTransaccion)

            Case "CONSUMO COMBUSTIBLE"
                Dim frmConsumoCombustible As New frmConsumoCombustible
                frmConsumoCombustible.IDTransaccion = IDTransaccion
                frmConsumoCombustible.Show()
                frmConsumoCombustible.CargarOperacion(IDTransaccion)

            Case "VENTAS CLIENTES"
                Dim frmVenta As New frmVenta
                frmVenta.IDTransaccion = IDTransaccion
                frmVenta.Show()

            Case "NOTA CREDITO"
                Dim frmNotaCreditoCliente As New frmNotaCreditoCliente
                frmNotaCreditoCliente.IDTransaccion = IDTransaccion
                frmNotaCreditoCliente.Show()
                frmNotaCreditoCliente.CargarOperacion(IDTransaccion)

            Case "NOTA CREDITO PROVEEDOR"
                Dim frmNotaCreditoProveedor As New frmNotaCreditoProveedor
                frmNotaCreditoProveedor.IDTransaccion = IDTransaccion
                frmNotaCreditoProveedor.Show()
                frmNotaCreditoProveedor.CargarOperacion(IDTransaccion)

            Case "TICKET DE BASCULA"
                Dim frmTicketBascula As New frmTicketBascula
                frmTicketBascula.IDTransaccion = IDTransaccion
                frmTicketBascula.Show()
                frmTicketBascula.CargarOperacion(IDTransaccion)

            Case "COMPRA DE MERCADERIA"
                Dim frmCompra As New frmCompra
                frmCompra.IDTransaccion = IDTransaccion
                frmCompra.Show()

        End Select

    End Sub

    Sub VerAsiento()

        If DataGridView1.SelectedRows.Count = 0 Then
            Dim mensaje As String = "Seleccione un registro!"
            MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Sub
        End If

        Dim Tipo As String
        Dim Comprobante As String
        Dim IDTransaccion As Integer

        IDTransaccion = DataGridView1.SelectedRows(0).Cells("IDTransaccion").Value
        Comprobante = DataGridView1.SelectedRows(0).Cells("OperacionComprobante").Value
        Tipo = DataGridView1.SelectedRows(0).Cells("Operacion").Value

        Dim frm As New frmVisualizarAsiento
        frm.WindowState = FormWindowState.Normal
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedToolWindow
        frm.Text = Tipo & ": " & Comprobante
        frm.IDTransaccion = IDTransaccion

        'Mostramos
        frm.Show(Me)

    End Sub

    Sub Exportar()

        If Not dt Is Nothing Then
            If dt.Rows.Count > 0 Then
                CSistema.dtToExcel2(dt, "Diferencias")
            End If
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnVerDetalle.Click
        VerDetalle()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        VerAsiento()
    End Sub

    Private Sub frmDetalleDiferenciasControlIntegridad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Exportar()

    End Sub
End Class
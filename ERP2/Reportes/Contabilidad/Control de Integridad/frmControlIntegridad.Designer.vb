﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmControlIntegridad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbxMes = New System.Windows.Forms.ComboBox()
        Me.txtAño = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnListar = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.chkDetalle = New ERP.ocxCHK()
        Me.txtCuentaContable = New ERP.ocxTXTCuentaContable()
        CType(Me.txtAño, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbxMes
        '
        Me.cbxMes.FormattingEnabled = True
        Me.cbxMes.Location = New System.Drawing.Point(107, 77)
        Me.cbxMes.Name = "cbxMes"
        Me.cbxMes.Size = New System.Drawing.Size(121, 21)
        Me.cbxMes.TabIndex = 1
        '
        'txtAño
        '
        Me.txtAño.Location = New System.Drawing.Point(107, 104)
        Me.txtAño.Maximum = New Decimal(New Integer() {3000, 0, 0, 0})
        Me.txtAño.Minimum = New Decimal(New Integer() {2018, 0, 0, 0})
        Me.txtAño.Name = "txtAño"
        Me.txtAño.Size = New System.Drawing.Size(120, 20)
        Me.txtAño.TabIndex = 2
        Me.txtAño.Value = New Decimal(New Integer() {2020, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Cuenta Contable:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 81)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Mes:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 108)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Año:"
        '
        'btnListar
        '
        Me.btnListar.Location = New System.Drawing.Point(396, 137)
        Me.btnListar.Name = "btnListar"
        Me.btnListar.Size = New System.Drawing.Size(167, 23)
        Me.btnListar.TabIndex = 6
        Me.btnListar.Text = "Listar"
        Me.btnListar.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 163)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(611, 22)
        Me.StatusStrip1.TabIndex = 114
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'chkDetalle
        '
        Me.chkDetalle.BackColor = System.Drawing.Color.Transparent
        Me.chkDetalle.Color = System.Drawing.Color.Empty
        Me.chkDetalle.Location = New System.Drawing.Point(284, 77)
        Me.chkDetalle.Name = "chkDetalle"
        Me.chkDetalle.Size = New System.Drawing.Size(129, 21)
        Me.chkDetalle.SoloLectura = False
        Me.chkDetalle.TabIndex = 7
        Me.chkDetalle.Texto = "Detalle de diferencias"
        Me.chkDetalle.Valor = False
        Me.chkDetalle.Visible = False
        '
        'txtCuentaContable
        '
        Me.txtCuentaContable.AlturaMaxima = 0
        Me.txtCuentaContable.Consulta = Nothing
        Me.txtCuentaContable.IDCentroCosto = 0
        Me.txtCuentaContable.IDUnidadNegocio = 0
        Me.txtCuentaContable.ListarTodas = False
        Me.txtCuentaContable.Location = New System.Drawing.Point(107, 22)
        Me.txtCuentaContable.Name = "txtCuentaContable"
        Me.txtCuentaContable.Registro = Nothing
        Me.txtCuentaContable.Resolucion173 = False
        Me.txtCuentaContable.Seleccionado = False
        Me.txtCuentaContable.Size = New System.Drawing.Size(492, 49)
        Me.txtCuentaContable.SoloLectura = False
        Me.txtCuentaContable.TabIndex = 0
        Me.txtCuentaContable.Texto = Nothing
        Me.txtCuentaContable.whereFiltro = Nothing
        '
        'frmControlIntegridad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(611, 185)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.chkDetalle)
        Me.Controls.Add(Me.btnListar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtAño)
        Me.Controls.Add(Me.cbxMes)
        Me.Controls.Add(Me.txtCuentaContable)
        Me.Name = "frmControlIntegridad"
        Me.Text = "frmControlIntegridad"
        CType(Me.txtAño, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtCuentaContable As ocxTXTCuentaContable
    Friend WithEvents cbxMes As ComboBox
    Friend WithEvents txtAño As NumericUpDown
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnListar As Button
    Friend WithEvents chkDetalle As ocxCHK
    Friend WithEvents ctrError As ErrorProvider
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents tsslEstado As ToolStripStatusLabel
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDetalleDiferenciasControlIntegridad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnVerDetalle = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.LblTotalDiferenciaEntrada = New System.Windows.Forms.Label()
        Me.LblTotalDiferenciaSalida = New System.Windows.Forms.Label()
        Me.LblCantidad = New System.Windows.Forms.Label()
        Me.txtCantidadComprobante = New ERP.ocxTXTNumeric()
        Me.TxtTotalDiferenciaSalida = New ERP.ocxTXTNumeric()
        Me.TxtTotalDiferenciaEntrada = New ERP.ocxTXTNumeric()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.DataGridView1, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.60177!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.39823!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1028, 452)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.LblCantidad)
        Me.Panel1.Controls.Add(Me.txtCantidadComprobante)
        Me.Panel1.Controls.Add(Me.TxtTotalDiferenciaSalida)
        Me.Panel1.Controls.Add(Me.TxtTotalDiferenciaEntrada)
        Me.Panel1.Controls.Add(Me.LblTotalDiferenciaEntrada)
        Me.Panel1.Controls.Add(Me.LblTotalDiferenciaSalida)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.btnVerDetalle)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 407)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1022, 42)
        Me.Panel1.TabIndex = 0
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(892, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(125, 23)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Exportar a Excel"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(101, 4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Ver Asiento"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btnVerDetalle
        '
        Me.btnVerDetalle.Location = New System.Drawing.Point(9, 4)
        Me.btnVerDetalle.Name = "btnVerDetalle"
        Me.btnVerDetalle.Size = New System.Drawing.Size(75, 23)
        Me.btnVerDetalle.TabIndex = 0
        Me.btnVerDetalle.Text = "Ver Detalle"
        Me.btnVerDetalle.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1022, 398)
        Me.DataGridView1.TabIndex = 1
        '
        'LblTotalDiferenciaEntrada
        '
        Me.LblTotalDiferenciaEntrada.AutoSize = True
        Me.LblTotalDiferenciaEntrada.Location = New System.Drawing.Point(396, 13)
        Me.LblTotalDiferenciaEntrada.Name = "LblTotalDiferenciaEntrada"
        Me.LblTotalDiferenciaEntrada.Size = New System.Drawing.Size(125, 13)
        Me.LblTotalDiferenciaEntrada.TabIndex = 43
        Me.LblTotalDiferenciaEntrada.Text = "Total Diferencia Entrada:"
        '
        'LblTotalDiferenciaSalida
        '
        Me.LblTotalDiferenciaSalida.AutoSize = True
        Me.LblTotalDiferenciaSalida.Location = New System.Drawing.Point(642, 13)
        Me.LblTotalDiferenciaSalida.Name = "LblTotalDiferenciaSalida"
        Me.LblTotalDiferenciaSalida.Size = New System.Drawing.Size(117, 13)
        Me.LblTotalDiferenciaSalida.TabIndex = 42
        Me.LblTotalDiferenciaSalida.Text = "Total Diferencia Salida:"
        '
        'LblCantidad
        '
        Me.LblCantidad.AutoSize = True
        Me.LblCantidad.Location = New System.Drawing.Point(281, 9)
        Me.LblCantidad.Name = "LblCantidad"
        Me.LblCantidad.Size = New System.Drawing.Size(52, 13)
        Me.LblCantidad.TabIndex = 47
        Me.LblCantidad.Text = "Cantidad:"
        '
        'txtCantidadComprobante
        '
        Me.txtCantidadComprobante.Color = System.Drawing.Color.Empty
        Me.txtCantidadComprobante.Decimales = True
        Me.txtCantidadComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidadComprobante.Indicaciones = Nothing
        Me.txtCantidadComprobante.Location = New System.Drawing.Point(334, 5)
        Me.txtCantidadComprobante.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCantidadComprobante.Name = "txtCantidadComprobante"
        Me.txtCantidadComprobante.Size = New System.Drawing.Size(45, 22)
        Me.txtCantidadComprobante.SoloLectura = True
        Me.txtCantidadComprobante.TabIndex = 46
        Me.txtCantidadComprobante.TabStop = False
        Me.txtCantidadComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadComprobante.Texto = "0"
        '
        'TxtTotalDiferenciaSalida
        '
        Me.TxtTotalDiferenciaSalida.Color = System.Drawing.Color.Empty
        Me.TxtTotalDiferenciaSalida.Decimales = True
        Me.TxtTotalDiferenciaSalida.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTotalDiferenciaSalida.Indicaciones = Nothing
        Me.TxtTotalDiferenciaSalida.Location = New System.Drawing.Point(757, 9)
        Me.TxtTotalDiferenciaSalida.Margin = New System.Windows.Forms.Padding(4)
        Me.TxtTotalDiferenciaSalida.Name = "TxtTotalDiferenciaSalida"
        Me.TxtTotalDiferenciaSalida.Size = New System.Drawing.Size(113, 22)
        Me.TxtTotalDiferenciaSalida.SoloLectura = True
        Me.TxtTotalDiferenciaSalida.TabIndex = 45
        Me.TxtTotalDiferenciaSalida.TabStop = False
        Me.TxtTotalDiferenciaSalida.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtTotalDiferenciaSalida.Texto = "0"
        '
        'TxtTotalDiferenciaEntrada
        '
        Me.TxtTotalDiferenciaEntrada.Color = System.Drawing.Color.Empty
        Me.TxtTotalDiferenciaEntrada.Decimales = True
        Me.TxtTotalDiferenciaEntrada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTotalDiferenciaEntrada.Indicaciones = Nothing
        Me.TxtTotalDiferenciaEntrada.Location = New System.Drawing.Point(520, 9)
        Me.TxtTotalDiferenciaEntrada.Margin = New System.Windows.Forms.Padding(4)
        Me.TxtTotalDiferenciaEntrada.Name = "TxtTotalDiferenciaEntrada"
        Me.TxtTotalDiferenciaEntrada.Size = New System.Drawing.Size(113, 22)
        Me.TxtTotalDiferenciaEntrada.SoloLectura = True
        Me.TxtTotalDiferenciaEntrada.TabIndex = 44
        Me.TxtTotalDiferenciaEntrada.TabStop = False
        Me.TxtTotalDiferenciaEntrada.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TxtTotalDiferenciaEntrada.Texto = "0"
        '
        'frmDetalleDiferenciasControlIntegridad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1028, 452)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "frmDetalleDiferenciasControlIntegridad"
        Me.Text = "frmDetalleDiferenciasControlIntegridad"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Button3 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents btnVerDetalle As Button
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents TxtTotalDiferenciaSalida As ocxTXTNumeric
    Friend WithEvents TxtTotalDiferenciaEntrada As ocxTXTNumeric
    Friend WithEvents LblTotalDiferenciaEntrada As Label
    Friend WithEvents LblTotalDiferenciaSalida As Label
    Friend WithEvents LblCantidad As Label
    Friend WithEvents txtCantidadComprobante As ocxTXTNumeric
End Class

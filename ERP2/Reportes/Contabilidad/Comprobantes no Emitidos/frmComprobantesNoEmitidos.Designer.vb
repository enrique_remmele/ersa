﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComprobantesNoEmitidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxParametros = New System.Windows.Forms.GroupBox()
        Me.lblFiltro = New System.Windows.Forms.Label()
        Me.rdbPorFecha = New System.Windows.Forms.RadioButton()
        Me.nudHasta = New System.Windows.Forms.NumericUpDown()
        Me.rdbPorNumeracion = New System.Windows.Forms.RadioButton()
        Me.nudDesde = New System.Windows.Forms.NumericUpDown()
        Me.lblPuntoExpedicion = New System.Windows.Forms.Label()
        Me.cbxPuntoExpedicion = New System.Windows.Forms.ComboBox()
        Me.lblTipoComprobante = New System.Windows.Forms.Label()
        Me.cbxTipoComprobante = New System.Windows.Forms.ComboBox()
        Me.lblOperacion = New System.Windows.Forms.Label()
        Me.cbxOperacion = New System.Windows.Forms.ComboBox()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxParametros.SuspendLayout()
        CType(Me.nudHasta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDesde, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbxParametros
        '
        Me.gbxParametros.Controls.Add(Me.lblFiltro)
        Me.gbxParametros.Controls.Add(Me.txtHasta)
        Me.gbxParametros.Controls.Add(Me.txtDesde)
        Me.gbxParametros.Controls.Add(Me.rdbPorFecha)
        Me.gbxParametros.Controls.Add(Me.nudHasta)
        Me.gbxParametros.Controls.Add(Me.rdbPorNumeracion)
        Me.gbxParametros.Controls.Add(Me.nudDesde)
        Me.gbxParametros.Controls.Add(Me.lblPuntoExpedicion)
        Me.gbxParametros.Controls.Add(Me.cbxPuntoExpedicion)
        Me.gbxParametros.Controls.Add(Me.lblTipoComprobante)
        Me.gbxParametros.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxParametros.Controls.Add(Me.lblOperacion)
        Me.gbxParametros.Controls.Add(Me.cbxOperacion)
        Me.gbxParametros.Location = New System.Drawing.Point(12, 12)
        Me.gbxParametros.Name = "gbxParametros"
        Me.gbxParametros.Size = New System.Drawing.Size(343, 209)
        Me.gbxParametros.TabIndex = 0
        Me.gbxParametros.TabStop = False
        Me.gbxParametros.Text = "Parametros"
        '
        'lblFiltro
        '
        Me.lblFiltro.AutoSize = True
        Me.lblFiltro.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFiltro.Location = New System.Drawing.Point(20, 119)
        Me.lblFiltro.Name = "lblFiltro"
        Me.lblFiltro.Size = New System.Drawing.Size(84, 13)
        Me.lblFiltro.TabIndex = 6
        Me.lblFiltro.Text = "Filtro de lista."
        '
        'rdbPorFecha
        '
        Me.rdbPorFecha.AutoSize = True
        Me.rdbPorFecha.Location = New System.Drawing.Point(20, 172)
        Me.rdbPorFecha.Name = "rdbPorFecha"
        Me.rdbPorFecha.Size = New System.Drawing.Size(77, 17)
        Me.rdbPorFecha.TabIndex = 10
        Me.rdbPorFecha.TabStop = True
        Me.rdbPorFecha.Text = "Por Fecha:"
        Me.rdbPorFecha.UseVisualStyleBackColor = True
        '
        'nudHasta
        '
        Me.nudHasta.Enabled = False
        Me.nudHasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.nudHasta.Location = New System.Drawing.Point(231, 143)
        Me.nudHasta.Name = "nudHasta"
        Me.nudHasta.Size = New System.Drawing.Size(103, 20)
        Me.nudHasta.TabIndex = 9
        '
        'rdbPorNumeracion
        '
        Me.rdbPorNumeracion.AutoSize = True
        Me.rdbPorNumeracion.Location = New System.Drawing.Point(20, 145)
        Me.rdbPorNumeracion.Name = "rdbPorNumeracion"
        Me.rdbPorNumeracion.Size = New System.Drawing.Size(102, 17)
        Me.rdbPorNumeracion.TabIndex = 7
        Me.rdbPorNumeracion.TabStop = True
        Me.rdbPorNumeracion.Text = "Por numeracion:"
        Me.rdbPorNumeracion.UseVisualStyleBackColor = True
        '
        'nudDesde
        '
        Me.nudDesde.Enabled = False
        Me.nudDesde.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.nudDesde.Location = New System.Drawing.Point(128, 143)
        Me.nudDesde.Name = "nudDesde"
        Me.nudDesde.Size = New System.Drawing.Size(103, 20)
        Me.nudDesde.TabIndex = 8
        '
        'lblPuntoExpedicion
        '
        Me.lblPuntoExpedicion.AutoSize = True
        Me.lblPuntoExpedicion.Location = New System.Drawing.Point(17, 90)
        Me.lblPuntoExpedicion.Name = "lblPuntoExpedicion"
        Me.lblPuntoExpedicion.Size = New System.Drawing.Size(77, 13)
        Me.lblPuntoExpedicion.TabIndex = 4
        Me.lblPuntoExpedicion.Text = "Punto de Exp.:"
        '
        'cbxPuntoExpedicion
        '
        Me.cbxPuntoExpedicion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxPuntoExpedicion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cbxPuntoExpedicion.FormattingEnabled = True
        Me.cbxPuntoExpedicion.Location = New System.Drawing.Point(128, 86)
        Me.cbxPuntoExpedicion.Name = "cbxPuntoExpedicion"
        Me.cbxPuntoExpedicion.Size = New System.Drawing.Size(206, 21)
        Me.cbxPuntoExpedicion.TabIndex = 5
        '
        'lblTipoComprobante
        '
        Me.lblTipoComprobante.AutoSize = True
        Me.lblTipoComprobante.Location = New System.Drawing.Point(17, 63)
        Me.lblTipoComprobante.Name = "lblTipoComprobante"
        Me.lblTipoComprobante.Size = New System.Drawing.Size(64, 13)
        Me.lblTipoComprobante.TabIndex = 2
        Me.lblTipoComprobante.Text = "Tipo Comp.:"
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxTipoComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cbxTipoComprobante.FormattingEnabled = True
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(128, 59)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(206, 21)
        Me.cbxTipoComprobante.TabIndex = 3
        '
        'lblOperacion
        '
        Me.lblOperacion.AutoSize = True
        Me.lblOperacion.Location = New System.Drawing.Point(17, 36)
        Me.lblOperacion.Name = "lblOperacion"
        Me.lblOperacion.Size = New System.Drawing.Size(59, 13)
        Me.lblOperacion.TabIndex = 0
        Me.lblOperacion.Text = "Operacion:"
        '
        'cbxOperacion
        '
        Me.cbxOperacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxOperacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.cbxOperacion.FormattingEnabled = True
        Me.cbxOperacion.Location = New System.Drawing.Point(128, 32)
        Me.cbxOperacion.Name = "cbxOperacion"
        Me.cbxOperacion.Size = New System.Drawing.Size(206, 21)
        Me.cbxOperacion.TabIndex = 1
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.Location = New System.Drawing.Point(271, 227)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 2
        Me.btnSalir.Text = "Cerrar"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(180, 227)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 23)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Text = "Listar"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(CType(0, Long))
        Me.txtHasta.Location = New System.Drawing.Point(231, 169)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(103, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 12
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Enabled = False
        Me.txtDesde.Fecha = New Date(CType(0, Long))
        Me.txtDesde.Location = New System.Drawing.Point(128, 169)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(103, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 11
        '
        'frmComprobantesNoEmitidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(370, 263)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnNuevo)
        Me.Controls.Add(Me.gbxParametros)
        Me.Name = "frmComprobantesNoEmitidos"
        Me.Text = "frmComprobantesNoEmitidos"
        Me.gbxParametros.ResumeLayout(False)
        Me.gbxParametros.PerformLayout()
        CType(Me.nudHasta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDesde, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxParametros As System.Windows.Forms.GroupBox
    Friend WithEvents lblTipoComprobante As System.Windows.Forms.Label
    Friend WithEvents cbxTipoComprobante As System.Windows.Forms.ComboBox
    Friend WithEvents lblOperacion As System.Windows.Forms.Label
    Friend WithEvents cbxOperacion As System.Windows.Forms.ComboBox
    Friend WithEvents nudDesde As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPuntoExpedicion As System.Windows.Forms.Label
    Friend WithEvents cbxPuntoExpedicion As System.Windows.Forms.ComboBox
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents rdbPorFecha As System.Windows.Forms.RadioButton
    Friend WithEvents nudHasta As System.Windows.Forms.NumericUpDown
    Friend WithEvents rdbPorNumeracion As System.Windows.Forms.RadioButton
    Friend WithEvents lblFiltro As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
End Class

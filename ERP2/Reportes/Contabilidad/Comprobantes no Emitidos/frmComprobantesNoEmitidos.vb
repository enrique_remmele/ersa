﻿Imports ERP.Reporte

Public Class frmComprobantesNoEmitidos

    'CLASES
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CReporte As New CReporteContabilidad

    'VARIABLES
    Dim dtTipoComprobante As DataTable
    Dim dtPuntoExpedicion As DataTable

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Controles
        rdbPorFecha.Checked = False
        rdbPorNumeracion.Checked = False

        'Funciones
        CargarInformacion()

        'Foco
        cbxOperacion.Focus()

    End Sub

    Sub CargarInformacion()

        'Operacion
        CSistema.SqlToComboBox(cbxOperacion, "Select Distinct IDOperacion, Operacion From VPuntoExpedicion ")

        'Tipo de Comprobantes
        dtTipoComprobante = CSistema.ExecuteToDataTable("Select Distinct IDTipoComprobante, TipoComprobante, IDOperacion From VPuntoExpedicion ")

        'Punto de Expedicion
        dtPuntoExpedicion = CSistema.ExecuteToDataTable("Select * From VPuntoExpedicion ")


    End Sub

    Sub Seleccionar()

        If IsNumeric(cbxOperacion.SelectedValue) = False Or IsNumeric(cbxTipoComprobante.SelectedValue) = False Or IsNumeric(cbxPuntoExpedicion.SelectedValue) = False Then
            Exit Sub
        End If

        For Each oRow As DataRow In dtPuntoExpedicion.Select(" IDTipoComprobante=" & cbxTipoComprobante.SelectedValue & " And IDOperacion=" & cbxOperacion.SelectedValue & " And ID=" & cbxPuntoExpedicion.SelectedValue & " ")

            Dim NumeracionDesde As Integer = oRow("NumeracionDesde")
            Dim NumeracionHasta As Integer = oRow("NumeracionHasta")

            nudDesde.Minimum = NumeracionDesde
            nudDesde.Maximum = NumeracionHasta
            nudDesde.Value = NumeracionDesde

            nudHasta.Minimum = NumeracionDesde
            nudHasta.Maximum = NumeracionHasta
            nudHasta.Value = oRow("ProximoNumero")

        Next

    End Sub

    Sub Listar()

        Dim SQL As String = "Exec SpViewComprobantesNoEmitidos "
        Dim Titulo As String = "COMPROBANTES NO EMITIDOS - " & cbxOperacion.Text
        Dim SubTitulo As String = ""

        If rdbPorNumeracion.Checked = True Then
            CSistema.ConcatenarParametro(SQL, "@Operacion", cbxOperacion.Text)
            CSistema.ConcatenarParametro(SQL, "@IDPuntoExpedicion", cbxPuntoExpedicion.SelectedValue)
            CSistema.ConcatenarParametro(SQL, "@PorNumeracion", "True")
            CSistema.ConcatenarParametro(SQL, "@NumeracionDesde", nudDesde.Value)
            CSistema.ConcatenarParametro(SQL, "@NumeracionHasta", nudHasta.Value)
        End If

        If rdbPorFecha.Checked = True Then
            CSistema.ConcatenarParametro(SQL, "@Operacion", cbxOperacion.Text)
            CSistema.ConcatenarParametro(SQL, "@IDPuntoExpedicion", cbxPuntoExpedicion.SelectedValue)
            CSistema.ConcatenarParametro(SQL, "@PorFecha", "True")
            CSistema.ConcatenarParametro(SQL, "@FechaDesde", txtDesde.GetValueString)
            CSistema.ConcatenarParametro(SQL, "@FechaHasta", txtHasta.GetValueString)
        End If

        Dim dt As DataTable = CSistema.ExecuteToDataTable(SQL)
        If dt Is Nothing Then
            Exit Sub
        End If

        dt.TableName = "SpViewComprobantesNoEmitidos"
        CReporte.ImprimirComprobanteNoEmitido(frmReporte, dt, Titulo, SubTitulo, vgUsuarioIdentificador)

    End Sub

    Private Sub frmComprobantesNoEmitidos_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmComprobantesNoEmitidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxOperacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxOperacion.SelectedIndexChanged

        cbxTipoComprobante.DataSource = Nothing

        If IsNumeric(cbxOperacion.SelectedValue) = False Then
            Exit Sub
        End If

        If dtPuntoExpedicion Is Nothing Then
            Exit Sub
        End If

        Dim dttemp As DataTable = CData.FiltrarDataTable(dtTipoComprobante, " IDOperacion=" & cbxOperacion.SelectedValue & " ")
        CSistema.SqlToComboBox(cbxTipoComprobante, dttemp, "IDTipoComprobante", "TipoComprobante")

    End Sub

    Private Sub cbxTipoDocumento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoComprobante.SelectedIndexChanged

        cbxPuntoExpedicion.DataSource = Nothing

        If IsNumeric(cbxTipoComprobante.SelectedValue) = False Then
            Exit Sub
        End If

        Dim dttemp As DataTable = CData.FiltrarDataTable(dtPuntoExpedicion, " IDTipoComprobante=" & cbxTipoComprobante.SelectedValue & " and IDOperacion=" & cbxOperacion.SelectedValue & " ")
        CSistema.SqlToComboBox(cbxPuntoExpedicion, dttemp, "ID", "Descripcion")

    End Sub

    Private Sub cbxPuntoExpedicion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPuntoExpedicion.SelectedIndexChanged
        Seleccionar()
    End Sub

    Private Sub rdbPorNumeracion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbPorNumeracion.CheckedChanged
        nudDesde.Enabled = rdbPorNumeracion.Checked
        nudHasta.Enabled = rdbPorNumeracion.Checked
    End Sub

    Private Sub rdbPorFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbPorFecha.CheckedChanged
        txtDesde.Enabled = rdbPorFecha.Checked
        txtHasta.Enabled = rdbPorFecha.Checked
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Listar()
    End Sub

End Class
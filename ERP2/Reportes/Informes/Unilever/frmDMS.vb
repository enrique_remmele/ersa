﻿Public Class frmDMS

    'CLASES
    Private CSistema As New CSistema
    Private CData As New CData

    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CSistema.SqlToComboBox(cbxSucursal, "Select Distinct ID,Descripcion From Sucursal")

    End Sub

    Sub Procesar()

        Dim ArchivoImportRegionUser As String = GuardarImportRegionUser()
        Dim ArchivoImportStore As String = GuardarImportStore()
        Dim ArchivoImportStock As String = GuardarImportStock()
        Dim ArchivoImportOrderInvoice As String = GuardarImportOrderInvoice()
        Dim ArchivoImportOrderItem As String = GuardarImportOrderItem()
        'Dim ArchivoImportQuota As String = GuardarImportQuota()
        Dim ArchivoImportVisit As String = GuardarImportVisit()
        Dim ArchivoImportRoute As String = GuardarImportRoute()

        MsgBox("Registro Guardado.!")

        Dim savefile As New FolderBrowserDialog
        savefile.SelectedPath = My.Computer.FileSystem.SpecialDirectories.Desktop

        If savefile.ShowDialog = DialogResult.OK Then
            FileCopy(VGCarpetaTemporal & ArchivoImportRegionUser, savefile.SelectedPath & "\" & ArchivoImportRegionUser)
            FileCopy(VGCarpetaTemporal & ArchivoImportStore, savefile.SelectedPath & "\" & ArchivoImportStore)
            FileCopy(VGCarpetaTemporal & ArchivoImportStock, savefile.SelectedPath & "\" & ArchivoImportStock)
            FileCopy(VGCarpetaTemporal & ArchivoImportOrderInvoice, savefile.SelectedPath & "\" & ArchivoImportOrderInvoice)
            FileCopy(VGCarpetaTemporal & ArchivoImportOrderItem, savefile.SelectedPath & "\" & ArchivoImportOrderItem)
            'FileCopy(VGCarpetaTemporal & ArchivoImportQuota, savefile.SelectedPath & "\" & ArchivoImportQuota)
            FileCopy(VGCarpetaTemporal & ArchivoImportVisit, savefile.SelectedPath & "\" & ArchivoImportVisit)
            FileCopy(VGCarpetaTemporal & ArchivoImportRoute, savefile.SelectedPath & "\" & ArchivoImportRoute)
        End If

    End Sub

    Function GuardarImportRegionUser() As String

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpImportRegionUser @IDSucursal=" & cbxSucursal.GetValue)
        Dim Archivo As String = ""

        Archivo = Archivo & "ImportRegionUser_"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Second.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarImportStore() As String
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpImportStore @IDSucursal=" & cbxSucursal.GetValue)
        Dim Archivo As String = ""

        Archivo = Archivo & "ImportStore_"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Second.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarImportStock() As String
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpImportStock @IDSucursal=" & cbxSucursal.GetValue & ",@Desde='" & CSistema.FormatoFechaDesdeHastaBaseDatos(txtDesde.txt.Text, True, False) & "', @Hasta='" & CSistema.FormatoFechaDesdeHastaBaseDatos(txtHasta.txt.Text, False, True) & "'")
        Dim Archivo As String = ""

        Archivo = Archivo & "ImportStock_"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Second.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarImportOrderInvoice() As String
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpImportOrderInvoice @IDSucursal=" & cbxSucursal.GetValue & ",@Desde='" & CSistema.FormatoFechaDesdeHastaBaseDatos(txtDesde.txt.Text, True, False) & "', @Hasta='" & CSistema.FormatoFechaDesdeHastaBaseDatos(txtHasta.txt.Text, False, True) & "'")
        Dim Archivo As String = ""

        Archivo = Archivo & "ImportOrderInvoice_"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Second.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarImportOrderItem() As String
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpImportOrderItem @IDSucursal=" & cbxSucursal.GetValue & ",@Desde='" & CSistema.FormatoFechaDesdeHastaBaseDatos(txtDesde.txt.Text, True, False) & "', @Hasta='" & CSistema.FormatoFechaDesdeHastaBaseDatos(txtHasta.txt.Text, False, True) & "'")
        Dim Archivo As String = ""

        'Asigna secuencia por linea de item
        Dim Secuencia As Integer = 0
        Dim NroFactura As String = ""
        For Each orow As DataRow In dt.Rows
            If NroFactura <> orow("cdOrder") Then
                Secuencia = 0
            End If
            Secuencia = Secuencia + 1
            orow("nrSequenceOrder") = Secuencia
            orow("nrSequenceInvoice") = Secuencia

            NroFactura = orow("cdOrder")
        Next



        Archivo = Archivo & "ImportOrderItemInvoiceItem_"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Second.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarImportQuota() As String
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpImportQuota @IDSucursal=" & cbxSucursal.GetValue)
        Dim Archivo As String = ""

        Archivo = Archivo & "ImportQuota_"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Second.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarImportVisit() As String

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpImportVisit @IDSucursal=" & cbxSucursal.GetValue & ",@Desde='" & txtDesde.txt.Text & "', @Hasta='" & txtHasta.txt.Text & "'")
        Dim Archivo As String = ""

        Archivo = Archivo & "ImportVisit_"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Second.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarImportRoute() As String

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpImportRoute @IDSucursal=" & cbxSucursal.GetValue & "")
        Dim Archivo As String = ""

        Archivo = Archivo & "ImportRoute_"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Second.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString & vbTab
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function


    Private Sub frmETL_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmETL_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

End Class


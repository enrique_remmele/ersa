﻿Public Class frmETL

    'CLASES
    Private CSistema As New CSistema
    Private CData As New CData

    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CSistema.SqlToComboBox(cbxSucursal, "Select Distinct ID,Descripcion From Sucursal")

    End Sub

    Sub Procesar()

        Dim ArchivoZona As String = GuardarZona()
        Dim ArchivoStock As String = GuardarStock()
        Dim ArchivoCliente As String = GuardarCliente()
        Dim ArchivoVenta As String = GuardarVenta()

        MsgBox("Registro Guardado.!")

        Dim savefile As New FolderBrowserDialog
        savefile.SelectedPath = My.Computer.FileSystem.SpecialDirectories.Desktop

        If savefile.ShowDialog = DialogResult.OK Then
            FileCopy(VGCarpetaTemporal & ArchivoZona, savefile.SelectedPath & "\" & ArchivoZona)
            FileCopy(VGCarpetaTemporal & ArchivoStock, savefile.SelectedPath & "\" & ArchivoStock)
            FileCopy(VGCarpetaTemporal & ArchivoCliente, savefile.SelectedPath & "\" & ArchivoCliente)
            FileCopy(VGCarpetaTemporal & ArchivoVenta, savefile.SelectedPath & "\" & ArchivoVenta)
        End If

    End Sub

    Function GuardarZona() As String

        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpETLVendedor @IDSucursal=" & cbxSucursal.GetValue)
        Dim CodigoDistribuidor As String = CData.GetTable("VSucursal").Select(" ID = " & cbxSucursal.GetValue)(0)("CodigoDistribuidor").ToString
        Dim Archivo As String = CodigoDistribuidor

        Archivo = Archivo & "zon"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo

    End Function

    Function GuardarStock() As String
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpETLStock @IDSucursal=" & cbxSucursal.GetValue)
        Dim CodigoDistribuidor As String = CData.GetTable("VSucursal").Select(" ID = " & cbxSucursal.GetValue)(0)("CodigoDistribuidor").ToString
        Dim Archivo As String = CodigoDistribuidor

        Archivo = Archivo & "sto"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarCliente() As String
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpETLCliente @IDSucursal=" & cbxSucursal.GetValue)
        Dim CodigoDistribuidor As String = CData.GetTable("VSucursal").Select(" ID = " & cbxSucursal.GetValue)(0)("CodigoDistribuidor").ToString
        Dim Archivo As String = CodigoDistribuidor

        Archivo = Archivo & "pop"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString
                Next
                sw.WriteLine(cadena)
                Debug.Print(cadena)

            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Function GuardarVenta() As String
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Execute SpETLVenta @IDSucursal=" & cbxSucursal.GetValue & ",@Desde='" & txtDesde.txt.Text & "', @Hasta='" & txtHasta.txt.Text & "'")
        Dim CodigoDistribuidor As String = CData.GetTable("VSucursal").Select(" ID = " & cbxSucursal.GetValue)(0)("CodigoDistribuidor").ToString
        Dim Archivo As String = CodigoDistribuidor

        Archivo = Archivo & "sal"
        Archivo = Archivo & VGFechaHoraSistema.Year
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Month.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Day.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Hour.ToString)
        Archivo = Archivo & CSistema.FormatDobleDigito(VGFechaHoraSistema.Minute.ToString)
        Archivo = Archivo & ".txt"

        Dim fic As String = VGCarpetaTemporal & "\" & Archivo

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, True)

            Dim cadena As String = ""
            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1
                    cadena = cadena & oRow(c).ToString
                Next
                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Return Archivo
    End Function

    Private Sub frmETL_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmETL_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        Procesar()
    End Sub

End Class


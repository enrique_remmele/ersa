﻿Imports ERP.Reporte

Public Class frmListaPrecioExcepciones

    'CLASES
    Dim CSistema As New CSistema
    Dim CReporte As New CReporteStock

    'VARIABLES
    Dim TipoInforme As String

    Sub Inicializar()
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.SelectedIndex = 0


        'Ordenado Por
        cbxOrdenadoPor.cbx.Items.Add("Cliente")
        cbxOrdenadoPor.cbx.Items.Add("Vendedor")
        cbxOrdenadoPor.cbx.Items.Add("Producto")
        cbxOrdenadoPor.cbx.Items.Add("Descuento")
        cbxOrdenadoPor.cbx.SelectedIndex = 0

        'Seleccion Multiple
        'cbxListaPrecio.SeleccionMultiple = True

        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

    End Sub

    Sub Listar()

        Dim Where As String = " where Desde >= '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And Hasta <= '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "
        Dim frm As New frmReporte
        Dim Orden As String = ""
        Dim OrderBy As String = ""
        Dim Titulo As String = "LISTA DE EXCEPCIONES DE PRECIO"


        'Establecer los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        Orden = " Order By " & cbxOrdenadoPor.cbx.Text

        ArmarSubTitulo(TipoInforme)

        CReporte.ListaPrecioExcepcion(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy)


    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.name = "cbxListaPrecio" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubTitulo(SubTitulo)
            End If
siguiente:

        Next

    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkVendedor_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProducto.PropertyChanged
        cbxProducto.Enabled = value
    End Sub

    Private Sub btnInforme_Click(sender As Object, e As EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListaPrecioExcepciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub frmListaPrecioExcepciones_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        'FA 20/06/2023
        LiberarMemoria()
    End Sub
End Class
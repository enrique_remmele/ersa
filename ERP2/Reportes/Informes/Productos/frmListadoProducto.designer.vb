﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkCategoria = New ERP.ocxCHK()
        Me.chkPresentacion = New ERP.ocxCHK()
        Me.chkSubLinea2 = New ERP.ocxCHK()
        Me.chkSubLinea = New ERP.ocxCHK()
        Me.cbxCategoria = New ERP.ocxCBX()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.cbxSubLinea2 = New ERP.ocxCBX()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkMarca = New ERP.ocxCHK()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxOpciones = New System.Windows.Forms.GroupBox()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.gbxFiltro.SuspendLayout()
        Me.gbxOpciones.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkCategoria)
        Me.gbxFiltro.Controls.Add(Me.chkPresentacion)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxCategoria)
        Me.gbxFiltro.Controls.Add(Me.cbxPresentacion)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMarca)
        Me.gbxFiltro.Controls.Add(Me.cbxMarca)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Location = New System.Drawing.Point(4, 4)
        Me.gbxFiltro.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxFiltro.Size = New System.Drawing.Size(516, 436)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkCategoria
        '
        Me.chkCategoria.BackColor = System.Drawing.Color.Transparent
        Me.chkCategoria.Color = System.Drawing.Color.Empty
        Me.chkCategoria.Location = New System.Drawing.Point(9, 250)
        Me.chkCategoria.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkCategoria.Name = "chkCategoria"
        Me.chkCategoria.Size = New System.Drawing.Size(135, 26)
        Me.chkCategoria.SoloLectura = False
        Me.chkCategoria.TabIndex = 12
        Me.chkCategoria.Texto = "Categoria:"
        Me.chkCategoria.Valor = False
        '
        'chkPresentacion
        '
        Me.chkPresentacion.BackColor = System.Drawing.Color.Transparent
        Me.chkPresentacion.Color = System.Drawing.Color.Empty
        Me.chkPresentacion.Location = New System.Drawing.Point(9, 217)
        Me.chkPresentacion.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkPresentacion.Name = "chkPresentacion"
        Me.chkPresentacion.Size = New System.Drawing.Size(135, 26)
        Me.chkPresentacion.SoloLectura = False
        Me.chkPresentacion.TabIndex = 10
        Me.chkPresentacion.Texto = "Presentacion:"
        Me.chkPresentacion.Valor = False
        '
        'chkSubLinea2
        '
        Me.chkSubLinea2.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea2.Color = System.Drawing.Color.Empty
        Me.chkSubLinea2.Location = New System.Drawing.Point(9, 150)
        Me.chkSubLinea2.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkSubLinea2.Name = "chkSubLinea2"
        Me.chkSubLinea2.Size = New System.Drawing.Size(135, 26)
        Me.chkSubLinea2.SoloLectura = False
        Me.chkSubLinea2.TabIndex = 6
        Me.chkSubLinea2.Texto = "SubLinea2"
        Me.chkSubLinea2.Valor = False
        '
        'chkSubLinea
        '
        Me.chkSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea.Color = System.Drawing.Color.Empty
        Me.chkSubLinea.Location = New System.Drawing.Point(9, 117)
        Me.chkSubLinea.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkSubLinea.Name = "chkSubLinea"
        Me.chkSubLinea.Size = New System.Drawing.Size(135, 26)
        Me.chkSubLinea.SoloLectura = False
        Me.chkSubLinea.TabIndex = 4
        Me.chkSubLinea.Texto = "SubLinea:"
        Me.chkSubLinea.Valor = False
        '
        'cbxCategoria
        '
        Me.cbxCategoria.CampoWhere = "IDCategoria"
        Me.cbxCategoria.CargarUnaSolaVez = False
        Me.cbxCategoria.DataDisplayMember = "Descripcion"
        Me.cbxCategoria.DataFilter = Nothing
        Me.cbxCategoria.DataOrderBy = "Descripcion"
        Me.cbxCategoria.DataSource = "Categoria"
        Me.cbxCategoria.DataValueMember = "ID"
        Me.cbxCategoria.Enabled = False
        Me.cbxCategoria.FormABM = Nothing
        Me.cbxCategoria.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxCategoria.Location = New System.Drawing.Point(149, 250)
        Me.cbxCategoria.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxCategoria.Name = "cbxCategoria"
        Me.cbxCategoria.SeleccionObligatoria = False
        Me.cbxCategoria.Size = New System.Drawing.Size(340, 26)
        Me.cbxCategoria.SoloLectura = False
        Me.cbxCategoria.TabIndex = 13
        Me.cbxCategoria.Texto = ""
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = "IDPresentacion"
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = "Descripcion"
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = "Descripcion"
        Me.cbxPresentacion.DataSource = "Presentacion"
        Me.cbxPresentacion.DataValueMember = "ID"
        Me.cbxPresentacion.Enabled = False
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPresentacion.Location = New System.Drawing.Point(149, 217)
        Me.cbxPresentacion.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(340, 26)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 11
        Me.cbxPresentacion.Texto = ""
        '
        'cbxSubLinea2
        '
        Me.cbxSubLinea2.CampoWhere = "IDSubLinea2"
        Me.cbxSubLinea2.CargarUnaSolaVez = False
        Me.cbxSubLinea2.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea2.DataFilter = Nothing
        Me.cbxSubLinea2.DataOrderBy = "Descripcion"
        Me.cbxSubLinea2.DataSource = "SubLinea2"
        Me.cbxSubLinea2.DataValueMember = "ID"
        Me.cbxSubLinea2.Enabled = False
        Me.cbxSubLinea2.FormABM = Nothing
        Me.cbxSubLinea2.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea2.Location = New System.Drawing.Point(149, 150)
        Me.cbxSubLinea2.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxSubLinea2.Name = "cbxSubLinea2"
        Me.cbxSubLinea2.SeleccionObligatoria = False
        Me.cbxSubLinea2.Size = New System.Drawing.Size(340, 26)
        Me.cbxSubLinea2.SoloLectura = False
        Me.cbxSubLinea2.TabIndex = 7
        Me.cbxSubLinea2.Texto = ""
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = "IDSubLinea"
        Me.cbxSubLinea.CargarUnaSolaVez = False
        Me.cbxSubLinea.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = "Descripcion"
        Me.cbxSubLinea.DataSource = "SubLinea"
        Me.cbxSubLinea.DataValueMember = "ID"
        Me.cbxSubLinea.Enabled = False
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea.Location = New System.Drawing.Point(149, 117)
        Me.cbxSubLinea.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(340, 26)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 5
        Me.cbxSubLinea.Texto = ""
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(9, 316)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(135, 26)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 16
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkMarca
        '
        Me.chkMarca.BackColor = System.Drawing.Color.Transparent
        Me.chkMarca.Color = System.Drawing.Color.Empty
        Me.chkMarca.Location = New System.Drawing.Point(9, 183)
        Me.chkMarca.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Size = New System.Drawing.Size(135, 26)
        Me.chkMarca.SoloLectura = False
        Me.chkMarca.TabIndex = 8
        Me.chkMarca.Texto = "Marca:"
        Me.chkMarca.Valor = False
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = "IDMarca"
        Me.cbxMarca.CargarUnaSolaVez = False
        Me.cbxMarca.DataDisplayMember = "Descripcion"
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = "Descripcion"
        Me.cbxMarca.DataSource = "Marca"
        Me.cbxMarca.DataValueMember = "ID"
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = Nothing
        Me.cbxMarca.Location = New System.Drawing.Point(149, 183)
        Me.cbxMarca.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(340, 26)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 9
        Me.cbxMarca.Texto = ""
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(9, 283)
        Me.chkProveedor.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(135, 26)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 14
        Me.chkProveedor.Texto = "Procedencia:"
        Me.chkProveedor.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = "RazonSocial"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = "RazonSocial"
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(149, 283)
        Me.cbxProveedor.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(340, 26)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 15
        Me.cbxProveedor.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(9, 49)
        Me.chkTipoProducto.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(135, 26)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 0
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "TipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(149, 49)
        Me.cbxTipoProducto.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(340, 26)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 1
        Me.cbxTipoProducto.Texto = ""
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(9, 82)
        Me.chkLinea.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(135, 26)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 2
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "Linea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(149, 82)
        Me.cbxLinea.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(340, 26)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 3
        Me.cbxLinea.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(21, 28)
        Me.lblOrdenado.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(76, 17)
        Me.lblOrdenado.TabIndex = 0
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(795, 448)
        Me.btnCerrar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(84, 28)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(533, 448)
        Me.btnInforme.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(167, 28)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxOpciones
        '
        Me.gbxOpciones.Controls.Add(Me.lblOrdenado)
        Me.gbxOpciones.Controls.Add(Me.cbxOrdenadoPor)
        Me.gbxOpciones.Controls.Add(Me.cbxEnForma)
        Me.gbxOpciones.Location = New System.Drawing.Point(528, 4)
        Me.gbxOpciones.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxOpciones.Name = "gbxOpciones"
        Me.gbxOpciones.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.gbxOpciones.Size = New System.Drawing.Size(345, 436)
        Me.gbxOpciones.TabIndex = 1
        Me.gbxOpciones.TabStop = False
        Me.gbxOpciones.Text = "Opciones"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(21, 49)
        Me.cbxOrdenadoPor.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(209, 26)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 1
        Me.cbxOrdenadoPor.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(229, 49)
        Me.cbxEnForma.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(97, 26)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 2
        Me.cbxEnForma.Texto = ""
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(149, 316)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(5)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.Size = New System.Drawing.Size(340, 111)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 17
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'frmListadoProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(903, 482)
        Me.Controls.Add(Me.gbxOpciones)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmListadoProducto"
        Me.Text = "frmListadoProducto"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxOpciones.ResumeLayout(False)
        Me.gbxOpciones.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkMarca As ERP.ocxCHK
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents gbxOpciones As System.Windows.Forms.GroupBox
    Friend WithEvents cbxSubLinea2 As ERP.ocxCBX
    Friend WithEvents cbxSubLinea As ERP.ocxCBX
    Friend WithEvents cbxPresentacion As ERP.ocxCBX
    Friend WithEvents chkCategoria As ERP.ocxCHK
    Friend WithEvents chkPresentacion As ERP.ocxCHK
    Friend WithEvents chkSubLinea2 As ERP.ocxCHK
    Friend WithEvents chkSubLinea As ERP.ocxCHK
    Friend WithEvents cbxCategoria As ERP.ocxCBX
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
End Class

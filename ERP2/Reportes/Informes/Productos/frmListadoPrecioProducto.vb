﻿Imports ERP.Reporte

Public Class frmListadoPrecioProducto

    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES

    'FUNCIONES
    Sub Inicializar()

        Me.KeyPreview = True
        Me.AcceptButton = New Button

        CargarInformacion()
        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")

        'Listas de Precios
        CSistema.SqlToComboBox(cbxListaPrecio, "Select ID, Descripcion From ListaPrecio where IDSUcursal = 1")

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereListaPrecio As String = " Where 1 = 1"
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        Dim TipoInforme As String = ""
        Dim Titulo As String = ""
        'Dim TipoInforme As String

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If rdbPrecio.Checked Then
            Titulo = "LISTA DE PRECIO"
            WhereListaPrecio = WhereListaPrecio & " and Precio > 0 "
        Else
            Titulo = "LISTA DE COSTOS"
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.name = "cbxListaPrecio" Then
                GoTo siguiente
            End If

            If ctr.name = "cbxSucursal" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        cbxProducto.EstablecerCondicion(Where)

        ArmarSubTitulo(TipoInforme)

        'Sacar los productos de Bonificacion
        If Where = "" Then
            'Where = " Where CharIndex('BON',Referencia,0)=0 And ControlarExistencia='True' "
            Where = " Where CharIndex('BON',Referencia,0)=0 "
        Else
            'Where = Where & " And CharIndex('BON',Referencia,0)=0 And ControlarExistencia='True' "
            Where = Where & " And CharIndex('BON',Referencia,0)=0 "
        End If

        'Con existencia
        If chkConExistencia.Valor = True Then
            Where = Where & " And ExistenciaGeneral>0 "
        End If

        If chkListaPrecio.Valor = True Then
            WhereListaPrecio = WhereListaPrecio & " And IDListaPrecio=" & cbxListaPrecio.GetValue
        End If

        'If chkSucursal.Valor = True Then
        WhereListaPrecio = WhereListaPrecio & " And IDSucursal=1" '& cbxSucursal.GetValue
        ' End If

        'En Ofertas
        If chkOferta.Valor = True Then
            WhereListaPrecio = WhereListaPrecio & " And TRPPorcentaje>0 "
        End If

        ' Dim Observacion As String = txtObservacion.Text
        Titulo = Titulo
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro)

        CReporte.ListadoPrecioProducto(frm, Where, WhereListaPrecio, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, rdbPrecio.Checked, chkUnidad.Valor)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Producto", "Select Top(0) Descripcion,TipoProducto,Linea,Marca From VProducto")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next


    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.name = "cbxListaPrecio" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubTitulo(SubTitulo)
            End If
siguiente:

        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoPrecioProducto_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub


    Private Sub chkProcedencia_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProcedencia.PropertyChanged
        cbxProcedencia.Enabled = value
    End Sub

    Private Sub chkMarca_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
    End Sub

    Private Sub chkTipoProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProducto.PropertyChanged
        cbxProducto.Enabled = value
    End Sub

    Private Sub chkSubLinea_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSubLinea.PropertyChanged
        cbxSubLinea.Enabled = value
    End Sub

    Private Sub chkSubLinea2_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSubLinea2.PropertyChanged
        cbxSubLinea2.Enabled = value
    End Sub

    Private Sub chkPresentacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkPresentacion.PropertyChanged
        cbxPresentacion.Enabled = value
    End Sub

    Private Sub chkCategoria_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCategoria.PropertyChanged
        cbxCategoria.Enabled = value
    End Sub

    'Private Sub chkObservacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
    '    txtObservacion.Enabled = chkObservacion.Valor
    'End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
    End Sub

    'Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)
    '    cbxSucursal.Enabled = value
    'End Sub

    Private Sub OcxCHK1_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkListaPrecio.PropertyChanged
        'If value = False Then
        '    chkSucursal.Valor = True
        '    chkSucursal.Enabled = False
        'Else
        '    chkSucursal.Enabled = True
        'End If
        cbxListaPrecio.Enabled = value


    End Sub

    Private Sub frmListadoPrecioProducto_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        'FA 20/06/2023
        LiberarMemoria()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmListadoPrecioProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.gbxOpciones = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkUnidad = New ERP.ocxCHK()
        Me.chkOferta = New ERP.ocxCHK()
        Me.chkSoloActivos = New ERP.ocxCHK()
        Me.chkConExistencia = New ERP.ocxCHK()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.rdbPrecio = New System.Windows.Forms.RadioButton()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkListaPrecio = New ERP.ocxCHK()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.chkCategoria = New ERP.ocxCHK()
        Me.chkPresentacion = New ERP.ocxCHK()
        Me.chkSubLinea2 = New ERP.ocxCHK()
        Me.chkSubLinea = New ERP.ocxCHK()
        Me.cbxCategoria = New ERP.ocxCBX()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.cbxSubLinea2 = New ERP.ocxCBX()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.chkProducto = New ERP.ocxCHK()
        Me.cbxProducto = New ERP.ocxCBX()
        Me.chkMarca = New ERP.ocxCHK()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.chkProcedencia = New ERP.ocxCHK()
        Me.cbxProcedencia = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.gbxOpciones.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxOpciones
        '
        Me.gbxOpciones.Controls.Add(Me.Label1)
        Me.gbxOpciones.Controls.Add(Me.chkUnidad)
        Me.gbxOpciones.Controls.Add(Me.chkOferta)
        Me.gbxOpciones.Controls.Add(Me.chkSoloActivos)
        Me.gbxOpciones.Controls.Add(Me.chkConExistencia)
        Me.gbxOpciones.Controls.Add(Me.RadioButton1)
        Me.gbxOpciones.Controls.Add(Me.lblOrdenado)
        Me.gbxOpciones.Controls.Add(Me.rdbPrecio)
        Me.gbxOpciones.Controls.Add(Me.cbxOrdenadoPor)
        Me.gbxOpciones.Controls.Add(Me.cbxEnForma)
        Me.gbxOpciones.Location = New System.Drawing.Point(405, 12)
        Me.gbxOpciones.Name = "gbxOpciones"
        Me.gbxOpciones.Size = New System.Drawing.Size(259, 315)
        Me.gbxOpciones.TabIndex = 1
        Me.gbxOpciones.TabStop = False
        Me.gbxOpciones.Text = "Opciones"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 211)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Importe Tipo:"
        '
        'chkUnidad
        '
        Me.chkUnidad.BackColor = System.Drawing.Color.Transparent
        Me.chkUnidad.Color = System.Drawing.Color.Empty
        Me.chkUnidad.Location = New System.Drawing.Point(14, 176)
        Me.chkUnidad.Name = "chkUnidad"
        Me.chkUnidad.Size = New System.Drawing.Size(229, 21)
        Me.chkUnidad.SoloLectura = False
        Me.chkUnidad.TabIndex = 7
        Me.chkUnidad.Texto = "Incluir Unidad"
        Me.chkUnidad.Valor = False
        '
        'chkOferta
        '
        Me.chkOferta.BackColor = System.Drawing.Color.Transparent
        Me.chkOferta.Color = System.Drawing.Color.Empty
        Me.chkOferta.Location = New System.Drawing.Point(14, 149)
        Me.chkOferta.Name = "chkOferta"
        Me.chkOferta.Size = New System.Drawing.Size(229, 21)
        Me.chkOferta.SoloLectura = False
        Me.chkOferta.TabIndex = 6
        Me.chkOferta.Texto = "Productos en Oferta"
        Me.chkOferta.Valor = False
        '
        'chkSoloActivos
        '
        Me.chkSoloActivos.BackColor = System.Drawing.Color.Transparent
        Me.chkSoloActivos.Color = System.Drawing.Color.Empty
        Me.chkSoloActivos.Location = New System.Drawing.Point(14, 67)
        Me.chkSoloActivos.Name = "chkSoloActivos"
        Me.chkSoloActivos.Size = New System.Drawing.Size(229, 21)
        Me.chkSoloActivos.SoloLectura = False
        Me.chkSoloActivos.TabIndex = 3
        Me.chkSoloActivos.Texto = "Solo Activos"
        Me.chkSoloActivos.Valor = True
        '
        'chkConExistencia
        '
        Me.chkConExistencia.BackColor = System.Drawing.Color.Transparent
        Me.chkConExistencia.Color = System.Drawing.Color.Empty
        Me.chkConExistencia.Location = New System.Drawing.Point(14, 121)
        Me.chkConExistencia.Name = "chkConExistencia"
        Me.chkConExistencia.Size = New System.Drawing.Size(229, 21)
        Me.chkConExistencia.SoloLectura = False
        Me.chkConExistencia.TabIndex = 5
        Me.chkConExistencia.Texto = "Solo con Existencia"
        Me.chkConExistencia.Valor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(150, 211)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(52, 17)
        Me.RadioButton1.TabIndex = 10
        Me.RadioButton1.Text = "Costo"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(14, 23)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 0
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'rdbPrecio
        '
        Me.rdbPrecio.AutoSize = True
        Me.rdbPrecio.Checked = True
        Me.rdbPrecio.Location = New System.Drawing.Point(89, 211)
        Me.rdbPrecio.Name = "rdbPrecio"
        Me.rdbPrecio.Size = New System.Drawing.Size(55, 17)
        Me.rdbPrecio.TabIndex = 9
        Me.rdbPrecio.TabStop = True
        Me.rdbPrecio.Text = "Precio"
        Me.rdbPrecio.UseVisualStyleBackColor = True
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(14, 40)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 1
        Me.cbxOrdenadoPor.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(167, 40)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 2
        Me.cbxEnForma.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(601, 333)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(405, 333)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.chkCategoria)
        Me.gbxFiltro.Controls.Add(Me.chkPresentacion)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxCategoria)
        Me.gbxFiltro.Controls.Add(Me.cbxPresentacion)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMarca)
        Me.gbxFiltro.Controls.Add(Me.cbxMarca)
        Me.gbxFiltro.Controls.Add(Me.chkProcedencia)
        Me.gbxFiltro.Controls.Add(Me.cbxProcedencia)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(387, 377)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkListaPrecio
        '
        Me.chkListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkListaPrecio.Location = New System.Drawing.Point(9, 40)
        Me.chkListaPrecio.Name = "chkListaPrecio"
        Me.chkListaPrecio.Size = New System.Drawing.Size(101, 21)
        Me.chkListaPrecio.SoloLectura = False
        Me.chkListaPrecio.TabIndex = 24
        Me.chkListaPrecio.Texto = "Lista de Precio:"
        Me.chkListaPrecio.Valor = True
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(8, 341)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(89, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 20
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = "RazonSocial"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = "RazonSocial"
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(113, 341)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(255, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 21
        Me.cbxProveedor.Texto = "19 DE MARZO S.A."
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = "ID"
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Descripcion"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = ""
        Me.cbxListaPrecio.DataSource = ""
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(113, 40)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(255, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 1
        Me.cbxListaPrecio.Texto = "BARES Y COPETINES"
        '
        'chkCategoria
        '
        Me.chkCategoria.BackColor = System.Drawing.Color.Transparent
        Me.chkCategoria.Color = System.Drawing.Color.Empty
        Me.chkCategoria.Location = New System.Drawing.Point(8, 260)
        Me.chkCategoria.Name = "chkCategoria"
        Me.chkCategoria.Size = New System.Drawing.Size(101, 21)
        Me.chkCategoria.SoloLectura = False
        Me.chkCategoria.TabIndex = 14
        Me.chkCategoria.Texto = "Categoria:"
        Me.chkCategoria.Valor = False
        '
        'chkPresentacion
        '
        Me.chkPresentacion.BackColor = System.Drawing.Color.Transparent
        Me.chkPresentacion.Color = System.Drawing.Color.Empty
        Me.chkPresentacion.Location = New System.Drawing.Point(8, 233)
        Me.chkPresentacion.Name = "chkPresentacion"
        Me.chkPresentacion.Size = New System.Drawing.Size(101, 21)
        Me.chkPresentacion.SoloLectura = False
        Me.chkPresentacion.TabIndex = 12
        Me.chkPresentacion.Texto = "Presentacion:"
        Me.chkPresentacion.Valor = False
        '
        'chkSubLinea2
        '
        Me.chkSubLinea2.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea2.Color = System.Drawing.Color.Empty
        Me.chkSubLinea2.Location = New System.Drawing.Point(8, 179)
        Me.chkSubLinea2.Name = "chkSubLinea2"
        Me.chkSubLinea2.Size = New System.Drawing.Size(101, 21)
        Me.chkSubLinea2.SoloLectura = False
        Me.chkSubLinea2.TabIndex = 8
        Me.chkSubLinea2.Texto = "SubLinea2"
        Me.chkSubLinea2.Valor = False
        '
        'chkSubLinea
        '
        Me.chkSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea.Color = System.Drawing.Color.Empty
        Me.chkSubLinea.Location = New System.Drawing.Point(8, 152)
        Me.chkSubLinea.Name = "chkSubLinea"
        Me.chkSubLinea.Size = New System.Drawing.Size(101, 21)
        Me.chkSubLinea.SoloLectura = False
        Me.chkSubLinea.TabIndex = 6
        Me.chkSubLinea.Texto = "SubLinea:"
        Me.chkSubLinea.Valor = False
        '
        'cbxCategoria
        '
        Me.cbxCategoria.CampoWhere = "IDCategoria"
        Me.cbxCategoria.CargarUnaSolaVez = False
        Me.cbxCategoria.DataDisplayMember = "Descripcion"
        Me.cbxCategoria.DataFilter = Nothing
        Me.cbxCategoria.DataOrderBy = "Descripcion"
        Me.cbxCategoria.DataSource = "Categoria"
        Me.cbxCategoria.DataValueMember = "ID"
        Me.cbxCategoria.dtSeleccionado = Nothing
        Me.cbxCategoria.Enabled = False
        Me.cbxCategoria.FormABM = Nothing
        Me.cbxCategoria.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxCategoria.Location = New System.Drawing.Point(113, 260)
        Me.cbxCategoria.Name = "cbxCategoria"
        Me.cbxCategoria.SeleccionMultiple = False
        Me.cbxCategoria.SeleccionObligatoria = False
        Me.cbxCategoria.Size = New System.Drawing.Size(255, 21)
        Me.cbxCategoria.SoloLectura = False
        Me.cbxCategoria.TabIndex = 15
        Me.cbxCategoria.Texto = "CARLOS"
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = "IDPresentacion"
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = "Descripcion"
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = "Descripcion"
        Me.cbxPresentacion.DataSource = "Presentacion"
        Me.cbxPresentacion.DataValueMember = "ID"
        Me.cbxPresentacion.dtSeleccionado = Nothing
        Me.cbxPresentacion.Enabled = False
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPresentacion.Location = New System.Drawing.Point(113, 233)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionMultiple = False
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(255, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 13
        Me.cbxPresentacion.Texto = "1 KG"
        '
        'cbxSubLinea2
        '
        Me.cbxSubLinea2.CampoWhere = "IDSubLinea2"
        Me.cbxSubLinea2.CargarUnaSolaVez = False
        Me.cbxSubLinea2.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea2.DataFilter = Nothing
        Me.cbxSubLinea2.DataOrderBy = "Descripcion"
        Me.cbxSubLinea2.DataSource = "SubLinea2"
        Me.cbxSubLinea2.DataValueMember = "ID"
        Me.cbxSubLinea2.dtSeleccionado = Nothing
        Me.cbxSubLinea2.Enabled = False
        Me.cbxSubLinea2.FormABM = Nothing
        Me.cbxSubLinea2.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea2.Location = New System.Drawing.Point(113, 179)
        Me.cbxSubLinea2.Name = "cbxSubLinea2"
        Me.cbxSubLinea2.SeleccionMultiple = False
        Me.cbxSubLinea2.SeleccionObligatoria = False
        Me.cbxSubLinea2.Size = New System.Drawing.Size(255, 21)
        Me.cbxSubLinea2.SoloLectura = False
        Me.cbxSubLinea2.TabIndex = 9
        Me.cbxSubLinea2.Texto = "BROWNIE"
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = "IDSubLinea"
        Me.cbxSubLinea.CargarUnaSolaVez = False
        Me.cbxSubLinea.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = "Descripcion"
        Me.cbxSubLinea.DataSource = "SubLinea"
        Me.cbxSubLinea.DataValueMember = "ID"
        Me.cbxSubLinea.dtSeleccionado = Nothing
        Me.cbxSubLinea.Enabled = False
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea.Location = New System.Drawing.Point(113, 152)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionMultiple = False
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(255, 21)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 7
        Me.cbxSubLinea.Texto = "AEROSOL"
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(8, 314)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(101, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 18
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'cbxProducto
        '
        Me.cbxProducto.CampoWhere = "ID"
        Me.cbxProducto.CargarUnaSolaVez = False
        Me.cbxProducto.DataDisplayMember = "Descripcion"
        Me.cbxProducto.DataFilter = Nothing
        Me.cbxProducto.DataOrderBy = "Descripcion"
        Me.cbxProducto.DataSource = "VProducto"
        Me.cbxProducto.DataValueMember = "ID"
        Me.cbxProducto.dtSeleccionado = Nothing
        Me.cbxProducto.Enabled = False
        Me.cbxProducto.FormABM = Nothing
        Me.cbxProducto.Indicaciones = Nothing
        Me.cbxProducto.Location = New System.Drawing.Point(113, 314)
        Me.cbxProducto.Name = "cbxProducto"
        Me.cbxProducto.SeleccionMultiple = False
        Me.cbxProducto.SeleccionObligatoria = False
        Me.cbxProducto.Size = New System.Drawing.Size(255, 21)
        Me.cbxProducto.SoloLectura = False
        Me.cbxProducto.TabIndex = 19
        Me.cbxProducto.Texto = "AC. TRIMESTRAL SET/10 (50%)"
        '
        'chkMarca
        '
        Me.chkMarca.BackColor = System.Drawing.Color.Transparent
        Me.chkMarca.Color = System.Drawing.Color.Empty
        Me.chkMarca.Location = New System.Drawing.Point(8, 206)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Size = New System.Drawing.Size(101, 21)
        Me.chkMarca.SoloLectura = False
        Me.chkMarca.TabIndex = 10
        Me.chkMarca.Texto = "Marca:"
        Me.chkMarca.Valor = False
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = "IDMarca"
        Me.cbxMarca.CargarUnaSolaVez = False
        Me.cbxMarca.DataDisplayMember = "Descripcion"
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = "Descripcion"
        Me.cbxMarca.DataSource = "Marca"
        Me.cbxMarca.DataValueMember = "ID"
        Me.cbxMarca.dtSeleccionado = Nothing
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = Nothing
        Me.cbxMarca.Location = New System.Drawing.Point(113, 206)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionMultiple = False
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(255, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 11
        Me.cbxMarca.Texto = "ADES"
        '
        'chkProcedencia
        '
        Me.chkProcedencia.BackColor = System.Drawing.Color.Transparent
        Me.chkProcedencia.Color = System.Drawing.Color.Empty
        Me.chkProcedencia.Location = New System.Drawing.Point(8, 287)
        Me.chkProcedencia.Name = "chkProcedencia"
        Me.chkProcedencia.Size = New System.Drawing.Size(101, 21)
        Me.chkProcedencia.SoloLectura = False
        Me.chkProcedencia.TabIndex = 16
        Me.chkProcedencia.Texto = "Procedencia:"
        Me.chkProcedencia.Valor = False
        '
        'cbxProcedencia
        '
        Me.cbxProcedencia.CampoWhere = "IDProcedencia"
        Me.cbxProcedencia.CargarUnaSolaVez = False
        Me.cbxProcedencia.DataDisplayMember = "Procedencia"
        Me.cbxProcedencia.DataFilter = Nothing
        Me.cbxProcedencia.DataOrderBy = "Procedencia"
        Me.cbxProcedencia.DataSource = "VProducto"
        Me.cbxProcedencia.DataValueMember = "ID"
        Me.cbxProcedencia.dtSeleccionado = Nothing
        Me.cbxProcedencia.Enabled = False
        Me.cbxProcedencia.FormABM = Nothing
        Me.cbxProcedencia.Indicaciones = Nothing
        Me.cbxProcedencia.Location = New System.Drawing.Point(113, 287)
        Me.cbxProcedencia.Name = "cbxProcedencia"
        Me.cbxProcedencia.SeleccionMultiple = False
        Me.cbxProcedencia.SeleccionObligatoria = False
        Me.cbxProcedencia.Size = New System.Drawing.Size(255, 21)
        Me.cbxProcedencia.SoloLectura = False
        Me.cbxProcedencia.TabIndex = 17
        Me.cbxProcedencia.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(8, 97)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(101, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 2
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "TipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(113, 97)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(255, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 3
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(8, 124)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(101, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 4
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "Linea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.dtSeleccionado = Nothing
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(113, 124)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionMultiple = False
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(255, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 5
        Me.cbxLinea.Texto = "BEBIDAS"
        '
        'frmListadoPrecioProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(675, 443)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.gbxOpciones)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoPrecioProducto"
        Me.Text = "frmListadoPrecioProducto"
        Me.gbxOpciones.ResumeLayout(False)
        Me.gbxOpciones.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxOpciones As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents rdbPrecio As System.Windows.Forms.RadioButton
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkCategoria As ERP.ocxCHK
    Friend WithEvents chkPresentacion As ERP.ocxCHK
    Friend WithEvents chkSubLinea2 As ERP.ocxCHK
    Friend WithEvents chkSubLinea As ERP.ocxCHK
    Friend WithEvents cbxCategoria As ERP.ocxCBX
    Friend WithEvents cbxPresentacion As ERP.ocxCBX
    Friend WithEvents cbxSubLinea2 As ERP.ocxCBX
    Friend WithEvents cbxSubLinea As ERP.ocxCBX
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents cbxProducto As ERP.ocxCBX
    Friend WithEvents chkMarca As ERP.ocxCHK
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents chkProcedencia As ERP.ocxCHK
    Friend WithEvents cbxProcedencia As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents chkSoloActivos As ERP.ocxCHK
    Friend WithEvents chkConExistencia As ERP.ocxCHK
    Friend WithEvents chkOferta As ERP.ocxCHK
    Friend WithEvents chkUnidad As ERP.ocxCHK
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents chkListaPrecio As ERP.ocxCHK
End Class

﻿Imports ERP.Reporte
Public Class frmAuditoriaClientes
    'CLASES
    Dim CReporte As New CCliente
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "AUDITORIA DE CLIENTES"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()
        CargarInformacion()
    End Sub
    Sub CargarInformacion()


        'Moneda
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito, 'Limite de Credito'=LimiteCredito, 'Plazo de Credito'=PlazoCredito  From VCliente "
        txtCliente.frm = Me
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

        'Orden en forma
        cbxModificado.cbx.Items.Add("Limite de Credito")
        cbxModificado.cbx.Items.Add("Vendedor")
        cbxModificado.cbx.Items.Add("Tipo de cliente")
        cbxModificado.cbx.Items.Add("Lista de precio")
        cbxModificado.cbx.Items.Add("Plazo")
        cbxModificado.cbx.Items.Add("Condicion")
        'cbxModificado.cbx.Items.Add("Email")
        'cbxModificado.cbx.Items.Add("NroCasa")
        'cbxModificado.cbx.Items.Add("Estado")
        cbxModificado.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxModificado.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxModificado.cbx.SelectedIndex = 0
    End Sub

    Private Sub frmAuditoriaClientes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub
    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        Dim TipoInforme As String = "Desde:" & txtDesde.txt.Text & "   Hasta:" & txtHasta.txt.Text
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar por Cliente
        Dim mensaje As String = ""
        'If txtCliente.txtID.txt.Text = "" Then
        '    mensaje = "Ingrese un cliente valido."
        '    ctrError.SetError(txtCliente, mensaje)
        '    ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopRight)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'Else
        '    ctrError.SetError(txtCliente, mensaje)
        '    ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopRight)
        '    tsslEstado.Text = mensaje
        'End If

        Where = " where 1 = 1"

        If chkClientes.Valor = True Then
            If txtCliente.txtRazonSocial.Texto <> "" Then
                Where = Where + " and ID = " & txtCliente.Registro("ID").ToString
            End If
        End If

        If chkModificado.Valor = True Then
            If cbxModificado.cbx.SelectedIndex = 0 Then
                Where = Where & " and ModiLimiteCredito = 'True'"
            ElseIf cbxModificado.cbx.SelectedIndex = 1 Then
                Where = Where & " and ModiVendedor = 'True'"
            ElseIf cbxModificado.cbx.SelectedIndex = 2 Then
                Where = Where & " and ModiTipoCliente = 'True'"
            ElseIf cbxModificado.cbx.SelectedIndex = 3 Then
                Where = Where & " and ModiListaPrecio = 'True'"
            ElseIf cbxModificado.cbx.SelectedIndex = 4 Then
                Where = Where & " and ModiPlazo = 'True'"
            ElseIf cbxModificado.cbx.SelectedIndex = 5 Then
                Where = Where & " and ModiCondicion = 'True'"
                'ElseIf cbxModificado.cbx.SelectedIndex = 6 Then
                '    Where = Where & " and Email = 'True'"
                'ElseIf cbxModificado.cbx.SelectedIndex = 7 Then
                '    Where = Where & " and NroCasa = 'True'"
                'ElseIf cbxModificado.cbx.SelectedIndex = 8 Then
                '    Where = Where & " and Estado = 'True'"
            End If
            TipoInforme = TipoInforme & "  -  Modificado = " & cbxModificado.cbx.Text
        End If

        If chkSucursal.Valor = True Then
            Where = Where & " and IDSucursal = " & cbxSucursal.GetValue
            TipoInforme = TipoInforme & "  -  Suc: " & cbxSucursal.cbx.Text
        End If

        If chkUsuario.Valor = True Then
            Where = Where & " and IDUsuario = " & cbxUsuario.GetValue
            TipoInforme = TipoInforme & "  -  Usuario: " & cbxUsuario.cbx.Text
        End If

        Where = Where + " and FechaModificacion between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'"
        OrderBy = " Order by ID,FechaModificacion"

        If txtCliente.Seleccionado = True And chkClientes.Valor = True Then
            TipoInforme = TipoInforme & "   -   Cliente: " & txtCliente.txtRazonSocial.txt.Text
        End If
        CReporte.AuditoriaClientes(frm, Where, Titulo, TipoInforme, OrderBy, Orden, vgUsuarioIdentificador)
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkClientes_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkClientes.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkModificado_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkModificado.PropertyChanged
        cbxModificado.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAuditoriaExcepcionesPrecios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkClientes = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkListaPrecio = New ERP.ocxCHK()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(387, 225)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 19
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(191, 143)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(296, 72)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtHasta.Location = New System.Drawing.Point(93, 38)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtDesde.Location = New System.Drawing.Point(9, 38)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(281, 225)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(100, 23)
        Me.btnInforme.TabIndex = 18
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkClientes
        '
        Me.chkClientes.BackColor = System.Drawing.Color.Transparent
        Me.chkClientes.Color = System.Drawing.Color.Empty
        Me.chkClientes.Location = New System.Drawing.Point(12, 97)
        Me.chkClientes.Name = "chkClientes"
        Me.chkClientes.Size = New System.Drawing.Size(62, 21)
        Me.chkClientes.SoloLectura = False
        Me.chkClientes.TabIndex = 28
        Me.chkClientes.Texto = "Clientes:"
        Me.chkClientes.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(112, 94)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(375, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 27
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(12, 41)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(95, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 22
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkListaPrecio
        '
        Me.chkListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkListaPrecio.Location = New System.Drawing.Point(12, 12)
        Me.chkListaPrecio.Name = "chkListaPrecio"
        Me.chkListaPrecio.Size = New System.Drawing.Size(95, 21)
        Me.chkListaPrecio.SoloLectura = False
        Me.chkListaPrecio.TabIndex = 20
        Me.chkListaPrecio.Texto = "Lista de precio:"
        Me.chkListaPrecio.Valor = False
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = "IDListaPrecio"
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Lista"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = Nothing
        Me.cbxListaPrecio.DataSource = "VListaPrecio"
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.Enabled = False
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(112, 12)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(264, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 21
        Me.cbxListaPrecio.Texto = ""
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(112, 41)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.Size = New System.Drawing.Size(375, 47)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 16
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'frmAuditoriaExcepcionesPrecios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(518, 273)
        Me.Controls.Add(Me.chkClientes)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.chkProducto)
        Me.Controls.Add(Me.chkListaPrecio)
        Me.Controls.Add(Me.cbxListaPrecio)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.txtProducto)
        Me.Name = "frmAuditoriaExcepcionesPrecios"
        Me.Text = "frmAuditoriaExcepcionesPrecios"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents chkListaPrecio As ERP.ocxCHK
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents chkClientes As ERP.ocxCHK
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
End Class

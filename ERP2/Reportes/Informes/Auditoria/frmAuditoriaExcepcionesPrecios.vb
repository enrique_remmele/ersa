﻿Imports ERP.Reporte
Public Class frmAuditoriaExcepcionesPrecios
    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "AUDITORIA DE EXCEPCIONES DE PRECIOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()
        txtProducto.Conectar()
        txtProducto.Enabled = False
        chkProducto.Valor = False

        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito, 'Limite de Credito'=LimiteCredito, 'Plazo de Credito'=PlazoCredito  From VCliente "
        txtCliente.frm = Me
        txtCliente.Enabled = False
        chkClientes.Valor = False
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

        chkListaPrecio.Valor = False
        cbxListaPrecio.Enabled = False
    End Sub

    Sub Listar()

        Dim Where As String = " "
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        Dim TipoInforme As String = "Desde:" & txtDesde.txt.Text & " Hasta:" & txtHasta.txt.Text
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        'Establecemos los filtros 
        Where = " Where FechaModificacion between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        If chkListaPrecio.Valor = True And cbxListaPrecio.cbx.Text <> "" Then
            Where = Where & " and IDListaPrecio = " & cbxListaPrecio.GetValue
            TipoInforme = TipoInforme & "  -  Lista de Precio: " & cbxListaPrecio.txt.Text
        End If
        If chkProducto.Valor = True And txtProducto.Seleccionado = True Then
            Where = Where & " and IDProducto = " & txtProducto.Registro("Id").ToString
            TipoInforme = TipoInforme & "  -  Producto: " & txtProducto.Registro("Referencia") & " - " & txtProducto.Registro("Descripcion")
        End If
        If chkClientes.Valor = True And txtCliente.Seleccionado = True Then
            Where = Where & " and IDCliente = " & txtCliente.Registro("ID")
            TipoInforme = TipoInforme & " - Cliente: " & txtCliente.txtRazonSocial.txt.Text
        End If

        OrderBy = " Order by FechaModificacion, IDProducto, IdListaPrecio"


        CReporte.AuditoriaExcepciones(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy)

    End Sub

    Private Sub chkListaPrecio_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmAuditoriaExcepcionesPrecios_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkClientes_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkClientes.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub
End Class
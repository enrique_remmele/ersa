﻿Imports ERP.Reporte
Public Class frmAuditoriaProductos
    'CLASES
    Dim CReporte As New CReporteStock
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "AUDITORIA DE PRODUCTOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()
        txtProducto.Conectar()
        txtProducto.Enabled = True
        chkProducto.Valor = True
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
    End Sub

    Sub Listar()

        Dim Where As String = " "
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        Dim TipoInforme As String = "Desde:" & txtDesde.txt.Text & " Hasta:" & txtHasta.txt.Text
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        'Establecemos los filtros 
        Where = " Where FechaModificacion between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'"
        If chkListaPrecio.Valor = True Then
            Where = Where & " and IDListaPrecio = " & cbxListaPrecio.GetValue
        End If
        If chkProducto.Valor = True And txtProducto.Seleccionado = True Then
            Where = Where & " and IDProducto = " & txtProducto.Registro("Id").ToString
        End If

        If chkProducto.Valor = True And txtProducto.Seleccionado = True Then
            TipoInforme = TipoInforme & "  -  Producto: " & txtProducto.Registro("Referencia") & " - " & txtProducto.Registro("Descripcion")
        End If
        If chkListaPrecio.Valor = True Then
            TipoInforme = TipoInforme & "  -  Lista de Precio: " & cbxListaPrecio.txt.Text
        End If

        OrderBy = " Order by IdProducto, IdListaPrecio, FechaModificacion"


        CReporte.AuditoriaProductos(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy)

    End Sub

    Private Sub chkListaPrecio_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub frmAuditoriaProductos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class
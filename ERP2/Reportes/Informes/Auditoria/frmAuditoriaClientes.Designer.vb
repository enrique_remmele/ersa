﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAuditoriaClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkUsuario = New ERP.ocxCHK()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxModificado = New ERP.ocxCBX()
        Me.chkModificado = New ERP.ocxCHK()
        Me.chkClientes = New ERP.ocxCHK()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(208, 253)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 22
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(11, 180)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(329, 67)
        Me.GroupBox2.TabIndex = 20
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(143, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "Hasta:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Desde:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(14, 253)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 21
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 298)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(491, 22)
        Me.StatusStrip1.TabIndex = 25
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(0, 17)
        '
        'chkUsuario
        '
        Me.chkUsuario.BackColor = System.Drawing.Color.Transparent
        Me.chkUsuario.Color = System.Drawing.Color.Empty
        Me.chkUsuario.Location = New System.Drawing.Point(12, 108)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(76, 21)
        Me.chkUsuario.SoloLectura = False
        Me.chkUsuario.TabIndex = 32
        Me.chkUsuario.Texto = "Usuario:"
        Me.chkUsuario.Valor = False
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(12, 81)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(76, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 30
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(121, 81)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(219, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 31
        Me.cbxSucursal.Texto = ""
        '
        'cbxModificado
        '
        Me.cbxModificado.CampoWhere = Nothing
        Me.cbxModificado.CargarUnaSolaVez = False
        Me.cbxModificado.DataDisplayMember = Nothing
        Me.cbxModificado.DataFilter = Nothing
        Me.cbxModificado.DataOrderBy = Nothing
        Me.cbxModificado.DataSource = Nothing
        Me.cbxModificado.DataValueMember = Nothing
        Me.cbxModificado.Enabled = False
        Me.cbxModificado.FormABM = Nothing
        Me.cbxModificado.Indicaciones = Nothing
        Me.cbxModificado.Location = New System.Drawing.Point(121, 54)
        Me.cbxModificado.Name = "cbxModificado"
        Me.cbxModificado.SeleccionObligatoria = True
        Me.cbxModificado.Size = New System.Drawing.Size(219, 21)
        Me.cbxModificado.SoloLectura = False
        Me.cbxModificado.TabIndex = 28
        Me.cbxModificado.Texto = ""
        '
        'chkModificado
        '
        Me.chkModificado.BackColor = System.Drawing.Color.Transparent
        Me.chkModificado.Color = System.Drawing.Color.Empty
        Me.chkModificado.Location = New System.Drawing.Point(12, 54)
        Me.chkModificado.Name = "chkModificado"
        Me.chkModificado.Size = New System.Drawing.Size(105, 21)
        Me.chkModificado.SoloLectura = False
        Me.chkModificado.TabIndex = 27
        Me.chkModificado.Texto = "Dato Modificado:"
        Me.chkModificado.Valor = False
        '
        'chkClientes
        '
        Me.chkClientes.BackColor = System.Drawing.Color.Transparent
        Me.chkClientes.Color = System.Drawing.Color.Empty
        Me.chkClientes.Location = New System.Drawing.Point(10, 15)
        Me.chkClientes.Name = "chkClientes"
        Me.chkClientes.Size = New System.Drawing.Size(62, 21)
        Me.chkClientes.SoloLectura = False
        Me.chkClientes.TabIndex = 26
        Me.chkClientes.Texto = "Clientes:"
        Me.chkClientes.Valor = False
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 2, 19, 16, 17, 41, 382)
        Me.txtHasta.Location = New System.Drawing.Point(191, 25)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 19, 16, 17, 41, 382)
        Me.txtDesde.Location = New System.Drawing.Point(57, 25)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(78, 12)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(375, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 24
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "VUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(121, 108)
        Me.cbxUsuario.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(276, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 33
        Me.cbxUsuario.Texto = ""
        '
        'frmAuditoriaClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(491, 320)
        Me.Controls.Add(Me.cbxUsuario)
        Me.Controls.Add(Me.chkUsuario)
        Me.Controls.Add(Me.chkSucursal)
        Me.Controls.Add(Me.cbxSucursal)
        Me.Controls.Add(Me.cbxModificado)
        Me.Controls.Add(Me.chkModificado)
        Me.Controls.Add(Me.chkClientes)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmAuditoriaClientes"
        Me.Text = "frmAuditoriaClientes"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkClientes As ERP.ocxCHK
    Friend WithEvents chkModificado As ERP.ocxCHK
    Friend WithEvents cbxModificado As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkUsuario As ERP.ocxCHK
    Friend WithEvents cbxUsuario As ERP.ocxCBX
End Class

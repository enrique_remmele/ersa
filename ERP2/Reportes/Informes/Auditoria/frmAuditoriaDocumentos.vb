﻿Imports ERP.Reporte
Public Class frmAuditoriaDocumentos
    'CLASES
    Dim CReporte As New CCliente
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "AUDITORIA DE DOCUMENTOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()
        CargarInformacion()
    End Sub
    Sub CargarInformacion()
        'Moneda
        chkClientes.Valor = False
        txtCliente.Enabled = False
        chkProveedor.Valor = False
        txtProveedor.Enabled = False
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito, 'Limite de Credito'=LimiteCredito, 'Plazo de Credito'=PlazoCredito  From VCliente "
        txtCliente.frm = Me
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "

    End Sub
    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        Dim TipoInforme As String = "Desde:" & txtDesde.txt.Text & " Hasta:" & txtHasta.txt.Text
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar por Cliente
        Dim mensaje As String = ""
        'If txtCliente.txtID.txt.Text = "" Then
        '    mensaje = "Ingrese un cliente valido."
        '    ctrError.SetError(txtCliente, mensaje)
        '    ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopRight)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'Else
        '    ctrError.SetError(txtCliente, mensaje)
        '    ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopRight)
        '    tsslEstado.Text = mensaje
        'End If

        Where = " where 1 = 1"

        If chkClientes.Valor = True Then
            If txtCliente.txtRazonSocial.Texto <> "" And txtCliente.Seleccionado = True Then
                Where = Where + " and IDCliente = " & txtCliente.Registro("ID").ToString
                TipoInforme = "   -   Cliente: " & txtCliente.txtRazonSocial.txt.Text
            Else
                Where = Where & " and IDCliente <> 0"
            End If
        End If
        If chkProveedor.Valor = True Then
            If txtProveedor.txtRazonSocial.Texto <> "" And txtProveedor.Seleccionado = True Then
                Where = Where + " and IDProveedor = " & txtProveedor.Registro("ID").ToString
                TipoInforme = "   -   Proveedor: " & txtProveedor.txtRazonSocial.txt.Text
            Else
                Where = Where & " and IDProveedor <> 0"
            End If
        End If
        If chkTipoComprobante.Valor = True Then
            Where = Where & " and IDTipoComprobante = " & cbxTipoComprobante.GetValue
        End If

        Where = Where + " and FechaModificacion between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'"
        OrderBy = " Order by FechaModificacion"

        CReporte.AuditoriaDocumentos(frm, Where, Titulo, TipoInforme, OrderBy, Orden, vgUsuarioIdentificador)
    End Sub

    Private Sub frmAuditoriaDocumentos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkClientes_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkClientes.PropertyChanged
        txtCliente.Enabled = value
        chkProveedor.Enabled = Not value
    End Sub

    Private Sub chkProveedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
        chkClientes.Enabled = Not value
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class
﻿Imports ERP.Reporte

Public Class frmExtractoMovimientoProveedorTodos


    'CLASES
    Dim CReporte As New CProveedor
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Public Property Precargar As Boolean
    Public Property IDProveedor As Integer
    Public Property Desde As Date
    Public Property Hasta As Date

    'VARIABLES
    Dim Titulo As String = "EXTRACTO DE MOVIMIENTOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True

        'Fechas
        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Cuentas Contables
        txtCuentaContableDesde.Conectar()
        'txtCuentaContableHasta.Conectar()

        ''Moneda
        'cbxMoneda.cbx.Text = "GUARANIES"

        If Precargar Then
            txtDesde.SetValue(Desde)
            txtHasta.SetValue(Hasta)
        End If

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim frm As New frmReporte
        Dim SP As String = ""

        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Where = "'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "', null, '%%', " & IIf(cbxMoneda.Enabled, cbxMoneda.GetValue.ToString, "null")
        Where = "'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "', null, '%%', 0"

        'Filtrar moneda y/o comprobante
        'If cbxMoneda.Enabled Then
        '    WhereDetalle = " IDMoneda=" & cbxMoneda.GetValue & " "
        'End If

        Titulo = "EXTRACTO DE MOVIMIENTO POR PROVEEDOR"
        TipoInforme = "Proveedor: Todos - Fecha desde " & txtDesde.txt.Text & " hasta " & txtHasta.txt.Text & ""

        If chkCC.chk.Checked Then
            Where = Where & ", '" & txtCuentaContableDesde.txtCodigo.Texto & "'"
            SP = "SpViewExtractoMovimientoProveedorMonedasCC"
            CReporte.ExtractoMovimientoProveedorMonedaTodosCC(frm, SP, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador)
        Else
            SP = "SpViewExtractoMovimientoProveedorMonedas2"
            CReporte.ExtractoMovimientoProveedorMonedaTodos(frm, SP, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador)
        End If

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmExtractoMovimientoProveedor_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    'Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)
    '    cbxMoneda.Enabled = value
    'End Sub

    Private Sub OcxCHK1_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCC.PropertyChanged
        txtCuentaContableDesde.Enabled = value

    End Sub

    Private Sub chkProveedor_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
    End Sub
End Class





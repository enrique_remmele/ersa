﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExtractoMovimientoDocumentoProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkCCHasta = New ERP.ocxCHK()
        Me.txtCuentaContableHasta = New ERP.ocxTXTCuentaContable()
        Me.chkCC = New ERP.ocxCHK()
        Me.txtCuentaContableDesde = New ERP.ocxTXTCuentaContable()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.chkComprobante = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.ctrError = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsslEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtHastaAsociado = New ERP.ocxTXTDate()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkCCHasta)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaContableHasta)
        Me.gbxFiltro.Controls.Add(Me.chkCC)
        Me.gbxFiltro.Controls.Add(Me.txtCuentaContableDesde)
        Me.gbxFiltro.Controls.Add(Me.txtProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.txtComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkComprobante)
        Me.gbxFiltro.Controls.Add(Me.Label1)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(599, 179)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkCCHasta
        '
        Me.chkCCHasta.BackColor = System.Drawing.Color.Transparent
        Me.chkCCHasta.Color = System.Drawing.Color.Empty
        Me.chkCCHasta.Location = New System.Drawing.Point(5, 142)
        Me.chkCCHasta.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCCHasta.Name = "chkCCHasta"
        Me.chkCCHasta.Size = New System.Drawing.Size(73, 21)
        Me.chkCCHasta.SoloLectura = True
        Me.chkCCHasta.TabIndex = 34
        Me.chkCCHasta.Texto = "CC Hasta:"
        Me.chkCCHasta.Valor = False
        '
        'txtCuentaContableHasta
        '
        Me.txtCuentaContableHasta.AlturaMaxima = 205
        Me.txtCuentaContableHasta.Consulta = Nothing
        Me.txtCuentaContableHasta.Enabled = False
        Me.txtCuentaContableHasta.IDCentroCosto = 0
        Me.txtCuentaContableHasta.IDUnidadNegocio = 0
        Me.txtCuentaContableHasta.ListarTodas = False
        Me.txtCuentaContableHasta.Location = New System.Drawing.Point(100, 139)
        Me.txtCuentaContableHasta.Name = "txtCuentaContableHasta"
        Me.txtCuentaContableHasta.Registro = Nothing
        Me.txtCuentaContableHasta.Resolucion173 = False
        Me.txtCuentaContableHasta.Seleccionado = False
        Me.txtCuentaContableHasta.Size = New System.Drawing.Size(358, 27)
        Me.txtCuentaContableHasta.SoloLectura = False
        Me.txtCuentaContableHasta.TabIndex = 33
        Me.txtCuentaContableHasta.Texto = Nothing
        Me.txtCuentaContableHasta.whereFiltro = Nothing
        '
        'chkCC
        '
        Me.chkCC.BackColor = System.Drawing.Color.Transparent
        Me.chkCC.Color = System.Drawing.Color.Empty
        Me.chkCC.Location = New System.Drawing.Point(5, 113)
        Me.chkCC.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCC.Name = "chkCC"
        Me.chkCC.Size = New System.Drawing.Size(73, 21)
        Me.chkCC.SoloLectura = False
        Me.chkCC.TabIndex = 32
        Me.chkCC.Texto = "CC Desde:"
        Me.chkCC.Valor = False
        '
        'txtCuentaContableDesde
        '
        Me.txtCuentaContableDesde.AlturaMaxima = 205
        Me.txtCuentaContableDesde.Consulta = Nothing
        Me.txtCuentaContableDesde.Enabled = False
        Me.txtCuentaContableDesde.IDCentroCosto = 0
        Me.txtCuentaContableDesde.IDUnidadNegocio = 0
        Me.txtCuentaContableDesde.ListarTodas = False
        Me.txtCuentaContableDesde.Location = New System.Drawing.Point(100, 110)
        Me.txtCuentaContableDesde.Name = "txtCuentaContableDesde"
        Me.txtCuentaContableDesde.Registro = Nothing
        Me.txtCuentaContableDesde.Resolucion173 = False
        Me.txtCuentaContableDesde.Seleccionado = False
        Me.txtCuentaContableDesde.Size = New System.Drawing.Size(358, 27)
        Me.txtCuentaContableDesde.SoloLectura = False
        Me.txtCuentaContableDesde.TabIndex = 31
        Me.txtCuentaContableDesde.Texto = Nothing
        Me.txtCuentaContableDesde.whereFiltro = Nothing
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(71, 19)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(458, 28)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 1
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(5, 53)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(86, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 2
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Enabled = False
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(100, 83)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(102, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 6
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'chkComprobante
        '
        Me.chkComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkComprobante.Color = System.Drawing.Color.Empty
        Me.chkComprobante.Location = New System.Drawing.Point(5, 83)
        Me.chkComprobante.Name = "chkComprobante"
        Me.chkComprobante.Size = New System.Drawing.Size(86, 21)
        Me.chkComprobante.SoloLectura = False
        Me.chkComprobante.TabIndex = 4
        Me.chkComprobante.Texto = "Comprobante:"
        Me.chkComprobante.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Proveedor:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(100, 53)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(96, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 3
        Me.cbxMoneda.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtHasta.Location = New System.Drawing.Point(97, 37)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtDesde.Location = New System.Drawing.Point(9, 37)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 21)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 197)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(184, 75)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Fecha del Egreso"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(547, 232)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(439, 232)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(92, 23)
        Me.btnInforme.TabIndex = 6
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'ctrError
        '
        Me.ctrError.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink
        Me.ctrError.ContainerControl = Me
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsslEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 307)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(623, 22)
        Me.StatusStrip1.TabIndex = 8
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsslEstado
        '
        Me.tsslEstado.Name = "tsslEstado"
        Me.tsslEstado.Size = New System.Drawing.Size(45, 17)
        Me.tsslEstado.Text = "Estado:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtHastaAsociado)
        Me.GroupBox1.Location = New System.Drawing.Point(202, 197)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(114, 75)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Saldo hasta:"
        '
        'txtHastaAsociado
        '
        Me.txtHastaAsociado.Color = System.Drawing.Color.Empty
        Me.txtHastaAsociado.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtHastaAsociado.Location = New System.Drawing.Point(16, 37)
        Me.txtHastaAsociado.Name = "txtHastaAsociado"
        Me.txtHastaAsociado.PermitirNulo = False
        Me.txtHastaAsociado.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaAsociado.SoloLectura = False
        Me.txtHastaAsociado.TabIndex = 2
        '
        'frmExtractoMovimientoDocumentoProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(623, 329)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmExtractoMovimientoDocumentoProveedor"
        Me.Text = "frmExtractoMovimientoDocumentoProveedor"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ctrError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Public WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents chkComprobante As ERP.ocxCHK
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Public WithEvents txtHasta As ERP.ocxTXTDate
    Public WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkCCHasta As ERP.ocxCHK
    Friend WithEvents txtCuentaContableHasta As ERP.ocxTXTCuentaContable
    Friend WithEvents chkCC As ERP.ocxCHK
    Friend WithEvents txtCuentaContableDesde As ERP.ocxTXTCuentaContable
    Friend WithEvents ctrError As System.Windows.Forms.ErrorProvider
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsslEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Public WithEvents txtHastaAsociado As ERP.ocxTXTDate
End Class

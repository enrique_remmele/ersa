﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkPais = New ERP.ocxCHK()
        Me.cbxPais = New ERP.ocxCBX()
        Me.chkTipoProveedor = New ERP.ocxCHK()
        Me.cbxTipoProveedor = New ERP.ocxCBX()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxEnForma)
        Me.gbxFiltro.Controls.Add(Me.cbxOrdenadoPor)
        Me.gbxFiltro.Controls.Add(Me.lblOrdenado)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkPais)
        Me.gbxFiltro.Controls.Add(Me.cbxPais)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(616, 230)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 115
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.Enabled = False
        Me.txtProveedor.Location = New System.Drawing.Point(107, 183)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(434, 34)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.TabIndex = 11
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(483, 30)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 14
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(327, 30)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 13
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(324, 14)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 12
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'chkSucursal
        '
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(6, 143)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(76, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 8
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(111, 142)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(175, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 9
        Me.cbxSucursal.Texto = ""
        '
        'chkCiudad
        '
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(7, 78)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(63, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 4
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudad"
        Me.cbxCiudad.DataDisplayMember = "Descripcion"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = "Descripcion"
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(111, 80)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(175, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 5
        Me.cbxCiudad.Texto = ""
        '
        'chkPais
        '
        Me.chkPais.Color = System.Drawing.Color.Empty
        Me.chkPais.Location = New System.Drawing.Point(7, 108)
        Me.chkPais.Name = "chkPais"
        Me.chkPais.Size = New System.Drawing.Size(63, 21)
        Me.chkPais.SoloLectura = False
        Me.chkPais.TabIndex = 6
        Me.chkPais.Texto = "País:"
        Me.chkPais.Valor = False
        '
        'cbxPais
        '
        Me.cbxPais.CampoWhere = "IDPais"
        Me.cbxPais.DataDisplayMember = "Descripcion"
        Me.cbxPais.DataFilter = Nothing
        Me.cbxPais.DataOrderBy = "Descripcion"
        Me.cbxPais.DataSource = "Pais"
        Me.cbxPais.DataValueMember = "ID"
        Me.cbxPais.Enabled = False
        Me.cbxPais.FormABM = Nothing
        Me.cbxPais.Indicaciones = Nothing
        Me.cbxPais.Location = New System.Drawing.Point(111, 107)
        Me.cbxPais.Name = "cbxPais"
        Me.cbxPais.SeleccionObligatoria = False
        Me.cbxPais.Size = New System.Drawing.Size(175, 21)
        Me.cbxPais.SoloLectura = False
        Me.cbxPais.TabIndex = 7
        Me.cbxPais.Texto = ""
        '
        'chkTipoProveedor
        '
        Me.chkTipoProveedor.Color = System.Drawing.Color.Empty
        Me.chkTipoProveedor.Location = New System.Drawing.Point(6, 23)
        Me.chkTipoProveedor.Name = "chkTipoProveedor"
        Me.chkTipoProveedor.Size = New System.Drawing.Size(101, 21)
        Me.chkTipoProveedor.SoloLectura = False
        Me.chkTipoProveedor.TabIndex = 0
        Me.chkTipoProveedor.Texto = "Tipo Proveedor:"
        Me.chkTipoProveedor.Valor = False
        '
        'cbxTipoProveedor
        '
        Me.cbxTipoProveedor.CampoWhere = "IDTipoProveedor"
        Me.cbxTipoProveedor.DataDisplayMember = "Descripcion"
        Me.cbxTipoProveedor.DataFilter = Nothing
        Me.cbxTipoProveedor.DataOrderBy = "Descripcion"
        Me.cbxTipoProveedor.DataSource = "TipoProveedor"
        Me.cbxTipoProveedor.DataValueMember = "ID"
        Me.cbxTipoProveedor.Enabled = False
        Me.cbxTipoProveedor.FormABM = Nothing
        Me.cbxTipoProveedor.Indicaciones = Nothing
        Me.cbxTipoProveedor.Location = New System.Drawing.Point(112, 25)
        Me.cbxTipoProveedor.Name = "cbxTipoProveedor"
        Me.cbxTipoProveedor.SeleccionObligatoria = False
        Me.cbxTipoProveedor.Size = New System.Drawing.Size(175, 21)
        Me.cbxTipoProveedor.SoloLectura = False
        Me.cbxTipoProveedor.TabIndex = 1
        Me.cbxTipoProveedor.Texto = ""
        '
        'chkProveedor
        '
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(7, 182)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(88, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 10
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'chkMoneda
        '
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(7, 50)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(71, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 2
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(112, 52)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(175, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 3
        Me.cbxMoneda.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(362, 260)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 2
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(134, 260)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmListadoProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(635, 307)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoProveedor"
        Me.Text = "frmListadoProveedor"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents chkTipoProveedor As ERP.ocxCHK
    Friend WithEvents cbxTipoProveedor As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents chkPais As ERP.ocxCHK
    Friend WithEvents cbxPais As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
End Class

﻿Imports ERP.Reporte

Public Class frmExtractoMovimientoProveedor


    'CLASES
    Dim CReporte As New CProveedor
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Public Property Precargar As Boolean
    Public Property IDProveedor As Integer
    Public Property Desde As Date
    Public Property Hasta As Date

    'VARIABLES
    Dim Titulo As String = "EXTRACTO DE MOVIMIENTOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True

        'Fechas
        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()

        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Moneda
        cbxMoneda.cbx.Text = "GUARANIES"
        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "

        'Tipo Comprobante
        CSistema.SqlToComboBox(cbxComprobante.cbx, "Select ID , Codigo  From VTipoComprobante v where Proveedor = 'True'  And (FormName='frmCompra' Or FormName='frmGastos') ")
        cbxComprobante.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        If Precargar Then
            txtProveedor.SetValue(IDProveedor)
            txtDesde.SetValue(Desde)
            txtHasta.SetValue(Hasta)
        End If

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim Where2 As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As Boolean = True
        Dim Comprobante As String = ""
        Dim SP As String = ""
        Dim IDTransaccionEgreso As String = ""
        Dim IDProveedor As String = ""
        Dim TipoComprobante As String = ""

        frm.MdiParent = My.Application.ApplicationContext.MainForm
        If txtProveedor.Seleccionado = False Then
            MessageBox.Show("Debe seleccionar un proveedor!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        'Comprobante
        If chkComprobante.Valor = True Then
            Comprobante = "'" & txtComprobante.txt.Text & "'"
            IDProveedor = txtProveedor.Registro("ID")
            TipoComprobante = cbxComprobante.cbx.Text

            'IDTransaccionEgreso
            IDTransaccionEgreso = CSistema.ExecuteScalar("Select IsNull((Select Top(1) IDTransaccion From VCompraGasto Where NroComprobante=" & Comprobante & " And IDProveedor= " & IDProveedor & " And TipoComprobante= '" & TipoComprobante & "'), 0) ")

            'Validar
            If IDTransaccionEgreso = 0 Then
                MessageBox.Show("No se encontro el registro!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

        End If

        If chkComprobante.Valor = True Then
            Where = "'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "', " & IDTransaccionEgreso & ", '%%', " & cbxMoneda.GetValue
        Else
            Where = "'" & txtDesde.GetValueString & "','" & txtHasta.GetValueString & "', " & txtProveedor.Registro("ID").ToString & ", '%%', " & cbxMoneda.GetValue
        End If

        'Dar formato a fecha inicial
        Dim Fecha As Date = txtDesde.txt.Text
        Dim Dia As String
        Dim Mes As String
        Dim Año As String
        Dim a As String
        Dim FechaInicial As String
        Dia = Fecha.Day
        Mes = Fecha.Month
        Año = Fecha.Year
        a = Año.Substring(2, 2)
        FechaInicial = Dia & "-" & Mes & "-" & a

        'Filtrar moneda y/o comprobante
        If cbxMoneda.Enabled Then
            WhereDetalle = " IDMoneda=" & cbxMoneda.GetValue & " "
        End If

        If cbxComprobante.Enabled Then
            If WhereDetalle.Trim.Length > 0 Then
                WhereDetalle = WhereDetalle & " And IDMoneda=" & cbxMoneda.GetValue & " "
            Else
                WhereDetalle = WhereDetalle & " And Operacion Like '%" & cbxComprobante.cbx.Text & "%' And Documento Like '%" & txtComprobante.GetValue & "%' "
            End If
        End If

        If chkIncluirRRHH.chk.Checked = False Then
            If WhereDetalle.Trim.Length > 0 Then
                WhereDetalle = WhereDetalle & " And RRHH = 'False'"
            Else
                WhereDetalle = WhereDetalle & " RRHH = 'False'"
            End If

        End If

        Titulo = "EXTRACTO DE MOVIMIENTO POR PROVEEDOR"
        TipoInforme = "Proveedor: " & txtProveedor.txtRazonSocial.Texto & " (" & txtProveedor.txtReferencia.Texto & ") - Fecha desde " & txtDesde.txt.Text & " hasta " & txtHasta.txt.Text & " " & Comprobante & ""

        If chkComprobante.chk.Checked = False Then
            Comprobante = ""
            SP = "SpViewExtractoMovimientoProveedorMonedas"
            CReporte.ExtractoMovimientoProveedorMoneda(frm, SP, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador)
        Else
            Comprobante = " - Comprobante N°= " & txtComprobante.Texto & ""
            SP = "SpViewExtractoMovimientoProveedor"
            CReporte.ExtractoMovimientoProveedor(frm, SP, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador)
        End If

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkComprobante.PropertyChanged
        cbxComprobante.Enabled = value
        txtComprobante.Enabled = value
    End Sub

    Private Sub frmExtractoMovimientoProveedor_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub
End Class





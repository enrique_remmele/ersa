﻿Imports ERP.Reporte
Public Class frmRankingProveedores

    'CLASES
    Dim CReporte As New CProveedor
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "RANKING DE PROVEEDORES"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()
        txtProducto.Conectar()
    End Sub

    Sub CargarInformacion()

        'Proveedor
        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono From VProveedor "

        'Reporte
        cbxTipoReporte.cbx.Items.Add("Ranking de Proveedores")
        cbxTipoReporte.cbx.Items.Add("Compras Proveedor/Producto")
        cbxTipoReporte.cbx.Items.Add("Compras por ProductoProveedor")
        cbxTipoReporte.cbx.SelectedIndex = 1
    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1 "
        Dim WhereDetalle As String = " Where 1 = 1"
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar por Cliente
        If chkProveedor.chk.Checked = True Then
            If txtProveedor.txtRazonSocial.Texto <> "" Then
                Where = Where + " and IDProveedor = " & txtProveedor.Registro("ID").ToString
                Where = WhereDetalle + " and IDProveedor = " & txtProveedor.Registro("ID").ToString
            End If
        End If
        If chkRango.Valor = True Then
            Where = Where & " and Fecha between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' and ' " & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
            WhereDetalle = WhereDetalle & " and Fecha between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' and ' " & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        End If
        If chkProducto.Valor = True Then
            WhereDetalle = WhereDetalle & " And IDProducto=" & txtProducto.Registro("ID") & " "
        End If


        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(WhereDetalle)
            End If
siguiente:

        Next
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        Select Case cbxTipoReporte.cbx.SelectedIndex
            Case 0 'Ranking de proveedores
                Titulo = "RANKING DE PROVEEDORES"
                CReporte.RankingProveedor(frm, Where, Titulo, TipoInforme, OrderBy, Orden, vgUsuarioIdentificador)
            Case 1 'Proveedor/Producto
                Titulo = "COMPRA PROVEEDORES/PRODUCTO"
                CReporte.RankingProveedorProducto(frm, Where, WhereDetalle, Titulo, TipoInforme, OrderBy, Orden, vgUsuarioIdentificador, "rptCompraProveedorProducto")
            Case 2 'Producto/Proveedor
                Titulo = "COMPRA PRODUCTO/PROVEEDORES"
                CReporte.RankingProveedorProducto(frm, Where, WhereDetalle, Titulo, TipoInforme, OrderBy, Orden, vgUsuarioIdentificador, "rptCompraProductoProveedor")
        End Select
    End Sub


    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
    End Sub

    Private Sub chkTipoProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoProveedor.PropertyChanged
        cbxTipoProveedor.Enabled = value
    End Sub

    Private Sub frmRankingProveedores_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkRango_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkRango.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub
End Class
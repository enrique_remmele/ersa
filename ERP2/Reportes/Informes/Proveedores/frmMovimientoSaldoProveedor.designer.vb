﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExtractoMovimientoProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkIncluirRRHH = New ERP.ocxCHK()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.txtComprobante = New ERP.ocxTXTString()
        Me.cbxComprobante = New ERP.ocxCBX()
        Me.chkComprobante = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.txtComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxComprobante)
        Me.gbxFiltro.Controls.Add(Me.chkComprobante)
        Me.gbxFiltro.Controls.Add(Me.Label1)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkIncluirRRHH)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(538, 112)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkIncluirRRHH
        '
        Me.chkIncluirRRHH.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirRRHH.Color = System.Drawing.Color.Empty
        Me.chkIncluirRRHH.Location = New System.Drawing.Point(202, 56)
        Me.chkIncluirRRHH.Name = "chkIncluirRRHH"
        Me.chkIncluirRRHH.Size = New System.Drawing.Size(95, 21)
        Me.chkIncluirRRHH.SoloLectura = False
        Me.chkIncluirRRHH.TabIndex = 27
        Me.chkIncluirRRHH.Texto = "Incluir RRHH"
        Me.chkIncluirRRHH.Valor = False
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(71, 19)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(458, 28)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 1
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(6, 53)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(86, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 2
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'txtComprobante
        '
        Me.txtComprobante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComprobante.Color = System.Drawing.Color.Empty
        Me.txtComprobante.Enabled = False
        Me.txtComprobante.Indicaciones = Nothing
        Me.txtComprobante.Location = New System.Drawing.Point(202, 83)
        Me.txtComprobante.Multilinea = False
        Me.txtComprobante.Name = "txtComprobante"
        Me.txtComprobante.Size = New System.Drawing.Size(102, 21)
        Me.txtComprobante.SoloLectura = False
        Me.txtComprobante.TabIndex = 6
        Me.txtComprobante.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.txtComprobante.Texto = ""
        '
        'cbxComprobante
        '
        Me.cbxComprobante.CampoWhere = ""
        Me.cbxComprobante.CargarUnaSolaVez = False
        Me.cbxComprobante.DataDisplayMember = ""
        Me.cbxComprobante.DataFilter = Nothing
        Me.cbxComprobante.DataOrderBy = ""
        Me.cbxComprobante.DataSource = ""
        Me.cbxComprobante.DataValueMember = ""
        Me.cbxComprobante.dtSeleccionado = Nothing
        Me.cbxComprobante.Enabled = False
        Me.cbxComprobante.FormABM = Nothing
        Me.cbxComprobante.Indicaciones = Nothing
        Me.cbxComprobante.Location = New System.Drawing.Point(97, 83)
        Me.cbxComprobante.Name = "cbxComprobante"
        Me.cbxComprobante.SeleccionMultiple = False
        Me.cbxComprobante.SeleccionObligatoria = False
        Me.cbxComprobante.Size = New System.Drawing.Size(96, 21)
        Me.cbxComprobante.SoloLectura = False
        Me.cbxComprobante.TabIndex = 5
        Me.cbxComprobante.Texto = ""
        '
        'chkComprobante
        '
        Me.chkComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkComprobante.Color = System.Drawing.Color.Empty
        Me.chkComprobante.Location = New System.Drawing.Point(6, 83)
        Me.chkComprobante.Name = "chkComprobante"
        Me.chkComprobante.Size = New System.Drawing.Size(86, 21)
        Me.chkComprobante.SoloLectura = False
        Me.chkComprobante.TabIndex = 4
        Me.chkComprobante.Texto = "Comprobante:"
        Me.chkComprobante.Valor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Proveedor:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(97, 53)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(96, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 3
        Me.cbxMoneda.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(469, 157)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 120)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(184, 75)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 21)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtHasta.Location = New System.Drawing.Point(97, 37)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtDesde.Location = New System.Drawing.Point(9, 37)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(309, 157)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmExtractoMovimientoProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(544, 201)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmExtractoMovimientoProveedor"
        Me.Text = "frmExtractoMovimientoProveedor"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Public WithEvents txtHasta As ERP.ocxTXTDate
    Public WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents cbxComprobante As ERP.ocxCBX
    Friend WithEvents chkComprobante As ERP.ocxCHK
    Friend WithEvents txtComprobante As ERP.ocxTXTString
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents chkIncluirRRHH As ERP.ocxCHK

End Class

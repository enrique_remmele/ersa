﻿Imports ERP.Reporte
Public Class frmExtractoMovimientoDocumentoProveedor


    'CLASES
    Dim CReporte As New CProveedor
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Public Property Precargar As Boolean
    Public Property IDProveedor As Integer
    Public Property Desde As Date
    Public Property Hasta As Date

    'VARIABLES
    Dim Titulo As String = "EXTRACTO DE MOVIMIENTOS"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True

        'Fechas
        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()
        txtHastaAsociado.Hoy()
        'Funciones
        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Moneda
        cbxMoneda.cbx.Text = "GUARANIES"
        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "

        'Cuentas Contables
        txtCuentaContableDesde.Conectar()
        txtCuentaContableHasta.Conectar()

        If Precargar Then
            txtProveedor.SetValue(IDProveedor)
            txtDesde.SetValue(Desde)
            txtHasta.SetValue(Hasta)
        End If

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim Where2 As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As Boolean = True
        Dim Comprobante As String = ""
        Dim SP As String = ""
        Dim IDTransaccionEgreso As String = ""
        Dim IDProveedor As String = ""
        Dim TipoComprobante As String = ""

        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Dim dtCCFF As New DataTable
        Dim CuentasContables As String = " "
        Dim Mensaje As String = ""

        If chkCC.Valor = True Then
            If txtCuentaContableDesde.txtCodigo.Texto = "" Then
                Mensaje = "Ingrese Cuenta Contable Desde!"
                ctrError.SetError(txtCuentaContableDesde, Mensaje)
                ctrError.SetIconAlignment(txtCuentaContableDesde, ErrorIconAlignment.TopRight)
                tsslEstado.Text = Mensaje
                Exit Sub
            End If
            If txtCuentaContableHasta.txtCodigo.Texto = "" Then
                Mensaje = "Ingrese Cuenta Contable Hasta!"
                ctrError.SetError(txtCuentaContableHasta, Mensaje)
                ctrError.SetIconAlignment(txtCuentaContableHasta, ErrorIconAlignment.TopRight)
                tsslEstado.Text = Mensaje
                Exit Sub
            End If
        End If

        'Buscar las Cuentas contables
        If chkCC.Valor = True And txtCuentaContableDesde.txtCodigo.Texto <> "" And txtCuentaContableHasta.txtCodigo.Texto <> "" Then 'Si hay filtro de Cuenta Contable
            CuentasContables = " and (CodigoCC = '0' "
            dtCCFF = CSistema.ExecuteToDataTable("select Codigo from CuentaContable where codigo between '" & txtCuentaContableDesde.txtCodigo.Texto & "' and '" & txtCuentaContableHasta.txtCodigo.Texto & "' and Imputable = 1 and descripcion like '%proveedo%'").Copy
            For Each oRow As DataRow In dtCCFF.Rows
                Dim param As New DataTable
                CuentasContables = CuentasContables & " or CodigoCC ='" & oRow("Codigo").ToString & "'    "
            Next
            CuentasContables = CuentasContables & ")"
            If txtCuentaContableDesde.txtCodigo.Texto <> "" Then
                TipoInforme = TipoInforme & "CC: " & txtCuentaContableDesde.txtCodigo.Texto & " - " & txtCuentaContableHasta.txtCodigo.Texto & " - "
            End If
        End If
        'Fin de cuentas contables

        Where = " Where FechaEgreso between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "
        Where = Where & " and Fecha <= '" & CSistema.FormatoFechaBaseDatos(txtHastaAsociado.txt.Text, True, False) & "' "

        If txtProveedor.Seleccionado = True Then
            Where = Where & " and Referencia = '" & txtProveedor.Registro("Referencia").ToString & "'"
        End If

        If chkComprobante.Valor = True And txtComprobante.txt.Text <> "" Then
            Where = Where & " and ComprobanteGasto like '%" & txtComprobante.txt.Text & "%' "
        End If

        Titulo = "EXTRACTO DE MOVIMIENTO DOCUMENTO DE PROVEEDORES"
        TipoInforme = "Fecha desde " & txtDesde.txt.Text & " hasta " & txtHasta.txt.Text & " " & Comprobante & "" & " - Saldo al:" & txtHastaAsociado.txt.Text
        If txtProveedor.Seleccionado = True Then
            TipoInforme = TipoInforme & " - Proveedor: " & txtProveedor.txtRazonSocial.Texto & " (" & txtProveedor.txtReferencia.Texto & ")"
        End If
        If chkCC.Valor = True Then
            TipoInforme = TipoInforme & "Cuentas: " & txtCuentaContableDesde.txtCodigo.Texto & " - " & txtCuentaContableHasta.txtCodigo.Texto
        End If
        Where = Where & CuentasContables
        CReporte.ExtractoMovimientoDocumentoProveedor(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador)


    End Sub
    Sub CopiarCuenta()
        If txtCuentaContableDesde.txtCodigo.Texto = "" Then
            Exit Sub
        End If
        txtCuentaContableHasta.txtCodigo.Texto = txtCuentaContableDesde.txtCodigo.Texto
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkComprobante.PropertyChanged
        txtComprobante.Enabled = value
    End Sub

    Private Sub frmExtractoMovimientoProveedor_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkCC_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCC.PropertyChanged
        txtCuentaContableDesde.Enabled = value
        chkCCHasta.Valor = value
        txtCuentaContableHasta.Enabled = value
    End Sub

    Private Sub txtCuentaContableDesde_Leave(sender As System.Object, e As System.EventArgs) Handles txtCuentaContableDesde.Leave
        CopiarCuenta()
    End Sub
End Class
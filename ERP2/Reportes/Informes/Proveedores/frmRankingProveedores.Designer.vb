﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRankingProveedores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkProducto = New ERP.ocxCHK()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkRango = New ERP.ocxCHK()
        Me.cbxTipoReporte = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.chkTipoProveedor = New ERP.ocxCHK()
        Me.cbxTipoProveedor = New ERP.ocxCBX()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkRango)
        Me.gbxFiltro.Controls.Add(Me.GroupBox2)
        Me.gbxFiltro.Controls.Add(Me.txtProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(654, 233)
        Me.gbxFiltro.TabIndex = 3
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxTipoReporte)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(421, 44)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(227, 167)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Reporte:"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 21)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(505, 251)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 5
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(277, 251)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 4
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(7, 164)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(71, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 16
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(81, 164)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.Size = New System.Drawing.Size(327, 47)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 15
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkRango
        '
        Me.chkRango.BackColor = System.Drawing.Color.Transparent
        Me.chkRango.Color = System.Drawing.Color.Empty
        Me.chkRango.Location = New System.Drawing.Point(421, 19)
        Me.chkRango.Name = "chkRango"
        Me.chkRango.Size = New System.Drawing.Size(74, 21)
        Me.chkRango.SoloLectura = False
        Me.chkRango.TabIndex = 13
        Me.chkRango.Texto = "Por Fecha"
        Me.chkRango.Valor = False
        '
        'cbxTipoReporte
        '
        Me.cbxTipoReporte.CampoWhere = Nothing
        Me.cbxTipoReporte.CargarUnaSolaVez = False
        Me.cbxTipoReporte.DataDisplayMember = Nothing
        Me.cbxTipoReporte.DataFilter = Nothing
        Me.cbxTipoReporte.DataOrderBy = Nothing
        Me.cbxTipoReporte.DataSource = Nothing
        Me.cbxTipoReporte.DataValueMember = Nothing
        Me.cbxTipoReporte.FormABM = Nothing
        Me.cbxTipoReporte.Indicaciones = Nothing
        Me.cbxTipoReporte.Location = New System.Drawing.Point(9, 90)
        Me.cbxTipoReporte.Name = "cbxTipoReporte"
        Me.cbxTipoReporte.SeleccionObligatoria = True
        Me.cbxTipoReporte.Size = New System.Drawing.Size(212, 21)
        Me.cbxTipoReporte.SoloLectura = False
        Me.cbxTipoReporte.TabIndex = 5
        Me.cbxTipoReporte.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtHasta.Location = New System.Drawing.Point(97, 37)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 26, 9, 4, 23, 155)
        Me.txtDesde.Location = New System.Drawing.Point(9, 37)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 115
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.Enabled = False
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(7, 109)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(401, 34)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 11
        '
        'chkTipoProveedor
        '
        Me.chkTipoProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProveedor.Color = System.Drawing.Color.Empty
        Me.chkTipoProveedor.Location = New System.Drawing.Point(7, 23)
        Me.chkTipoProveedor.Name = "chkTipoProveedor"
        Me.chkTipoProveedor.Size = New System.Drawing.Size(101, 21)
        Me.chkTipoProveedor.SoloLectura = False
        Me.chkTipoProveedor.TabIndex = 0
        Me.chkTipoProveedor.Texto = "Tipo Proveedor:"
        Me.chkTipoProveedor.Valor = False
        '
        'cbxTipoProveedor
        '
        Me.cbxTipoProveedor.CampoWhere = "IDTipoProveedor"
        Me.cbxTipoProveedor.CargarUnaSolaVez = False
        Me.cbxTipoProveedor.DataDisplayMember = "Descripcion"
        Me.cbxTipoProveedor.DataFilter = Nothing
        Me.cbxTipoProveedor.DataOrderBy = "Descripcion"
        Me.cbxTipoProveedor.DataSource = "TipoProveedor"
        Me.cbxTipoProveedor.DataValueMember = "ID"
        Me.cbxTipoProveedor.Enabled = False
        Me.cbxTipoProveedor.FormABM = Nothing
        Me.cbxTipoProveedor.Indicaciones = Nothing
        Me.cbxTipoProveedor.Location = New System.Drawing.Point(112, 25)
        Me.cbxTipoProveedor.Name = "cbxTipoProveedor"
        Me.cbxTipoProveedor.SeleccionObligatoria = False
        Me.cbxTipoProveedor.Size = New System.Drawing.Size(296, 21)
        Me.cbxTipoProveedor.SoloLectura = False
        Me.cbxTipoProveedor.TabIndex = 1
        Me.cbxTipoProveedor.Texto = ""
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(7, 82)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(88, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 10
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(7, 50)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(71, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 2
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(112, 52)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(175, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 3
        Me.cbxMoneda.Texto = ""
        '
        'frmRankingProveedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(678, 286)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmRankingProveedores"
        Me.Text = "frmRankingProveedores"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtProveedor As ERP.ocxTXTProveedor
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkTipoProveedor As ERP.ocxCHK
    Friend WithEvents cbxTipoProveedor As ERP.ocxCBX
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkRango As ERP.ocxCHK
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Public WithEvents txtHasta As ERP.ocxTXTDate
    Public WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxTipoReporte As ERP.ocxCBX
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
End Class

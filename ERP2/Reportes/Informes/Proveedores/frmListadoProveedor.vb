﻿Imports ERP.Reporte

Public Class frmListadoProveedor

    'CLASES
    Dim CReporte As New CProveedor
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "INDICE GENERAL DE PROVEEDORES"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        'Proveedor
        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono From VProveedor "

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar por Cliente
        If chkProveedor.chk.Checked = True Then
            If txtProveedor.txtRazonSocial.Texto <> "" Then
                Where = "Where "
                Where = Where + "ID = " & txtProveedor.Registro("ID").ToString
            End If
        End If


        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        If cbxOrdenadoPor.cbx.Text = "RazonSocial" Then
            Orden = "Por Nombre o Razón Social"
        ElseIf cbxOrdenadoPor.cbx.Text = "TipoProveedor" Then
            Orden = "Por Tipo Cliente"
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro)
        CReporte.ListadoProveedor(frm, Where, Titulo, TipoInforme, OrderBy, Orden, vgUsuarioIdentificador)
    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Proveedor", "Select Top(0) RazonSocial,TipoProveedor From VProveedor")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next


    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub
    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub


    Private Sub chkBarrio_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkPais.PropertyChanged
        cbxPais.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
    End Sub

    Private Sub chkTipoProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoProveedor.PropertyChanged
        cbxTipoProveedor.Enabled = value
    End Sub
End Class




﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmExtractoMovimientoProveedorTodos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.chkCC = New ERP.ocxCHK()
        Me.txtCuentaContableDesde = New ERP.ocxTXTCuentaContable()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.SuspendLayout()
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(259, 152)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(120, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 7, 1, 16, 35, 19, 225)
        Me.txtHasta.Location = New System.Drawing.Point(166, 95)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 35
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 7, 1, 16, 35, 19, 225)
        Me.txtDesde.Location = New System.Drawing.Point(85, 95)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 33
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(14, 102)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 31
        Me.lblPeriodo.Text = "Periodo:"
        '
        'chkCC
        '
        Me.chkCC.BackColor = System.Drawing.Color.Transparent
        Me.chkCC.Color = System.Drawing.Color.Empty
        Me.chkCC.Location = New System.Drawing.Point(16, 53)
        Me.chkCC.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCC.Name = "chkCC"
        Me.chkCC.Size = New System.Drawing.Size(73, 21)
        Me.chkCC.SoloLectura = False
        Me.chkCC.TabIndex = 39
        Me.chkCC.Texto = "C.Contable:"
        Me.chkCC.Valor = False
        '
        'txtCuentaContableDesde
        '
        Me.txtCuentaContableDesde.AlturaMaxima = 205
        Me.txtCuentaContableDesde.Consulta = Nothing
        Me.txtCuentaContableDesde.Enabled = False
        Me.txtCuentaContableDesde.IDCentroCosto = 0
        Me.txtCuentaContableDesde.IDUnidadNegocio = 0
        Me.txtCuentaContableDesde.ListarTodas = False
        Me.txtCuentaContableDesde.Location = New System.Drawing.Point(90, 53)
        Me.txtCuentaContableDesde.Name = "txtCuentaContableDesde"
        Me.txtCuentaContableDesde.Registro = Nothing
        Me.txtCuentaContableDesde.Resolucion173 = False
        Me.txtCuentaContableDesde.Seleccionado = False
        Me.txtCuentaContableDesde.Size = New System.Drawing.Size(397, 27)
        Me.txtCuentaContableDesde.SoloLectura = False
        Me.txtCuentaContableDesde.TabIndex = 38
        Me.txtCuentaContableDesde.Texto = Nothing
        Me.txtCuentaContableDesde.whereFiltro = Nothing
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(385, 152)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 34
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 57
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.Enabled = False
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(90, 12)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(397, 27)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 41
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(16, 18)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(78, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 40
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'frmExtractoMovimientoProveedorTodos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(503, 190)
        Me.Controls.Add(Me.txtProveedor)
        Me.Controls.Add(Me.chkProveedor)
        Me.Controls.Add(Me.txtHasta)
        Me.Controls.Add(Me.txtDesde)
        Me.Controls.Add(Me.lblPeriodo)
        Me.Controls.Add(Me.chkCC)
        Me.Controls.Add(Me.txtCuentaContableDesde)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmExtractoMovimientoProveedorTodos"
        Me.Text = "frmExtractoMovimientoProveedorTodos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents lblPeriodo As Label
    Friend WithEvents chkCC As ocxCHK
    Friend WithEvents txtCuentaContableDesde As ocxTXTCuentaContable
    Friend WithEvents btnCerrar As Button
    Friend WithEvents txtProveedor As ocxTXTProveedor
    Friend WithEvents chkProveedor As ocxCHK
End Class

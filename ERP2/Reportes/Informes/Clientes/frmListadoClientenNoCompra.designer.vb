﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoClienteNoCompra
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.OcxTXTProducto1 = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkZonaVenta = New ERP.ocxCHK()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.chkDepartamento = New ERP.ocxCHK()
        Me.cbxDepartamento = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkBarrio = New ERP.ocxCHK()
        Me.cbxBarrio = New ERP.ocxCBX()
        Me.chkListaPrecio = New ERP.ocxCHK()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.chkDistribuidor = New ERP.ocxCHK()
        Me.cbxDistribuidor = New ERP.ocxCBX()
        Me.chkCobrador = New ERP.ocxCHK()
        Me.cbxCobrador = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkPromotor = New ERP.ocxCHK()
        Me.cbxPromotor = New ERP.ocxCBX()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkActivo = New ERP.ocxCHK()
        Me.cbxActivo = New ERP.ocxCBX()
        Me.chkCondicion = New ERP.ocxCHK()
        Me.cbxCondicion = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.OcxTXTProducto1)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxEnForma)
        Me.gbxFiltro.Controls.Add(Me.cbxOrdenadoPor)
        Me.gbxFiltro.Controls.Add(Me.lblOrdenado)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.cbxZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.chkDepartamento)
        Me.gbxFiltro.Controls.Add(Me.cbxDepartamento)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkBarrio)
        Me.gbxFiltro.Controls.Add(Me.cbxBarrio)
        Me.gbxFiltro.Controls.Add(Me.chkListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.cbxListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.chkDistribuidor)
        Me.gbxFiltro.Controls.Add(Me.cbxDistribuidor)
        Me.gbxFiltro.Controls.Add(Me.chkCobrador)
        Me.gbxFiltro.Controls.Add(Me.cbxCobrador)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkPromotor)
        Me.gbxFiltro.Controls.Add(Me.cbxPromotor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkActivo)
        Me.gbxFiltro.Controls.Add(Me.cbxActivo)
        Me.gbxFiltro.Controls.Add(Me.chkCondicion)
        Me.gbxFiltro.Controls.Add(Me.cbxCondicion)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(577, 307)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'OcxTXTProducto1
        '
        Me.OcxTXTProducto1.AlturaMaxima = 0
        Me.OcxTXTProducto1.ColumnasNumericas = Nothing
        Me.OcxTXTProducto1.Compra = False
        Me.OcxTXTProducto1.Consulta = Nothing
        Me.OcxTXTProducto1.ControlarExistencia = False
        Me.OcxTXTProducto1.ControlarReservas = False
        Me.OcxTXTProducto1.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxTXTProducto1.dtDescuento = Nothing
        Me.OcxTXTProducto1.dtProductosSeleccionados = Nothing
        Me.OcxTXTProducto1.Enabled = False
        Me.OcxTXTProducto1.IDCliente = 0
        Me.OcxTXTProducto1.IDClienteSucursal = 0
        Me.OcxTXTProducto1.IDDeposito = 0
        Me.OcxTXTProducto1.IDListaPrecio = 0
        Me.OcxTXTProducto1.IDMoneda = 0
        Me.OcxTXTProducto1.IDSucursal = 0
        Me.OcxTXTProducto1.Location = New System.Drawing.Point(112, 264)
        Me.OcxTXTProducto1.Name = "OcxTXTProducto1"
        Me.OcxTXTProducto1.Precios = Nothing
        Me.OcxTXTProducto1.Registro = Nothing
        Me.OcxTXTProducto1.Seleccionado = False
        Me.OcxTXTProducto1.SeleccionMultiple = False
        Me.OcxTXTProducto1.Size = New System.Drawing.Size(428, 30)
        Me.OcxTXTProducto1.SoloLectura = True
        Me.OcxTXTProducto1.TabIndex = 34
        Me.OcxTXTProducto1.TabStop = False
        Me.OcxTXTProducto1.TieneDescuento = False
        Me.OcxTXTProducto1.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.OcxTXTProducto1.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(6, 264)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(88, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 33
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(457, 193)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 32
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(300, 193)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 31
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(300, 168)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 30
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(298, 109)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(76, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 28
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(386, 108)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(175, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 29
        Me.cbxSucursal.Texto = ""
        '
        'chkZonaVenta
        '
        Me.chkZonaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkZonaVenta.Color = System.Drawing.Color.Empty
        Me.chkZonaVenta.Location = New System.Drawing.Point(298, 88)
        Me.chkZonaVenta.Name = "chkZonaVenta"
        Me.chkZonaVenta.Size = New System.Drawing.Size(76, 21)
        Me.chkZonaVenta.SoloLectura = False
        Me.chkZonaVenta.TabIndex = 26
        Me.chkZonaVenta.Texto = "ZonaVtas:"
        Me.chkZonaVenta.Valor = False
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = "IDZonaVenta"
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = "Descripcion"
        Me.cbxZonaVenta.DataSource = "ZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.dtSeleccionado = Nothing
        Me.cbxZonaVenta.Enabled = False
        Me.cbxZonaVenta.FormABM = Nothing
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(386, 87)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionMultiple = False
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(175, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 27
        Me.cbxZonaVenta.Texto = ""
        '
        'chkDepartamento
        '
        Me.chkDepartamento.BackColor = System.Drawing.Color.Transparent
        Me.chkDepartamento.Color = System.Drawing.Color.Empty
        Me.chkDepartamento.Location = New System.Drawing.Point(298, 66)
        Me.chkDepartamento.Name = "chkDepartamento"
        Me.chkDepartamento.Size = New System.Drawing.Size(63, 21)
        Me.chkDepartamento.SoloLectura = False
        Me.chkDepartamento.TabIndex = 24
        Me.chkDepartamento.Texto = "Depto:"
        Me.chkDepartamento.Valor = False
        '
        'cbxDepartamento
        '
        Me.cbxDepartamento.CampoWhere = "IDDepartamento"
        Me.cbxDepartamento.CargarUnaSolaVez = False
        Me.cbxDepartamento.DataDisplayMember = "Descripcion"
        Me.cbxDepartamento.DataFilter = Nothing
        Me.cbxDepartamento.DataOrderBy = "Descripcion"
        Me.cbxDepartamento.DataSource = "Departamento"
        Me.cbxDepartamento.DataValueMember = "ID"
        Me.cbxDepartamento.dtSeleccionado = Nothing
        Me.cbxDepartamento.Enabled = False
        Me.cbxDepartamento.FormABM = Nothing
        Me.cbxDepartamento.Indicaciones = Nothing
        Me.cbxDepartamento.Location = New System.Drawing.Point(386, 66)
        Me.cbxDepartamento.Name = "cbxDepartamento"
        Me.cbxDepartamento.SeleccionMultiple = False
        Me.cbxDepartamento.SeleccionObligatoria = False
        Me.cbxDepartamento.Size = New System.Drawing.Size(175, 21)
        Me.cbxDepartamento.SoloLectura = False
        Me.cbxDepartamento.TabIndex = 25
        Me.cbxDepartamento.Texto = ""
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(298, 45)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(63, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 22
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudad"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Descripcion"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = "Descripcion"
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(386, 45)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(175, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 23
        Me.cbxCiudad.Texto = ""
        '
        'chkBarrio
        '
        Me.chkBarrio.BackColor = System.Drawing.Color.Transparent
        Me.chkBarrio.Color = System.Drawing.Color.Empty
        Me.chkBarrio.Location = New System.Drawing.Point(298, 23)
        Me.chkBarrio.Name = "chkBarrio"
        Me.chkBarrio.Size = New System.Drawing.Size(63, 21)
        Me.chkBarrio.SoloLectura = False
        Me.chkBarrio.TabIndex = 20
        Me.chkBarrio.Texto = "Barrio:"
        Me.chkBarrio.Valor = False
        '
        'cbxBarrio
        '
        Me.cbxBarrio.CampoWhere = "IDBarrio"
        Me.cbxBarrio.CargarUnaSolaVez = False
        Me.cbxBarrio.DataDisplayMember = "Descripcion"
        Me.cbxBarrio.DataFilter = Nothing
        Me.cbxBarrio.DataOrderBy = "Descripcion"
        Me.cbxBarrio.DataSource = "Barrio"
        Me.cbxBarrio.DataValueMember = "ID"
        Me.cbxBarrio.dtSeleccionado = Nothing
        Me.cbxBarrio.Enabled = False
        Me.cbxBarrio.FormABM = Nothing
        Me.cbxBarrio.Indicaciones = Nothing
        Me.cbxBarrio.Location = New System.Drawing.Point(386, 24)
        Me.cbxBarrio.Name = "cbxBarrio"
        Me.cbxBarrio.SeleccionMultiple = False
        Me.cbxBarrio.SeleccionObligatoria = False
        Me.cbxBarrio.Size = New System.Drawing.Size(175, 21)
        Me.cbxBarrio.SoloLectura = False
        Me.cbxBarrio.TabIndex = 21
        Me.cbxBarrio.Texto = ""
        '
        'chkListaPrecio
        '
        Me.chkListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkListaPrecio.Location = New System.Drawing.Point(6, 130)
        Me.chkListaPrecio.Name = "chkListaPrecio"
        Me.chkListaPrecio.Size = New System.Drawing.Size(101, 21)
        Me.chkListaPrecio.SoloLectura = False
        Me.chkListaPrecio.TabIndex = 10
        Me.chkListaPrecio.Texto = "Lista de Precios:"
        Me.chkListaPrecio.Valor = False
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = "IDListaPrecio"
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Descripcion"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = "Descripcion"
        Me.cbxListaPrecio.DataSource = "ListaPrecio"
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.Enabled = False
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(112, 130)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(175, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 11
        Me.cbxListaPrecio.Texto = ""
        '
        'chkDistribuidor
        '
        Me.chkDistribuidor.BackColor = System.Drawing.Color.Transparent
        Me.chkDistribuidor.Color = System.Drawing.Color.Empty
        Me.chkDistribuidor.Location = New System.Drawing.Point(6, 111)
        Me.chkDistribuidor.Name = "chkDistribuidor"
        Me.chkDistribuidor.Size = New System.Drawing.Size(88, 21)
        Me.chkDistribuidor.SoloLectura = False
        Me.chkDistribuidor.TabIndex = 8
        Me.chkDistribuidor.Texto = "Distribuidor:"
        Me.chkDistribuidor.Valor = False
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.CampoWhere = "IDDistribuidor"
        Me.cbxDistribuidor.CargarUnaSolaVez = False
        Me.cbxDistribuidor.DataDisplayMember = "Nombres"
        Me.cbxDistribuidor.DataFilter = Nothing
        Me.cbxDistribuidor.DataOrderBy = "Nombres"
        Me.cbxDistribuidor.DataSource = "Distribuidor"
        Me.cbxDistribuidor.DataValueMember = "ID"
        Me.cbxDistribuidor.dtSeleccionado = Nothing
        Me.cbxDistribuidor.Enabled = False
        Me.cbxDistribuidor.FormABM = Nothing
        Me.cbxDistribuidor.Indicaciones = Nothing
        Me.cbxDistribuidor.Location = New System.Drawing.Point(112, 109)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.SeleccionMultiple = False
        Me.cbxDistribuidor.SeleccionObligatoria = False
        Me.cbxDistribuidor.Size = New System.Drawing.Size(175, 21)
        Me.cbxDistribuidor.SoloLectura = False
        Me.cbxDistribuidor.TabIndex = 9
        Me.cbxDistribuidor.Texto = ""
        '
        'chkCobrador
        '
        Me.chkCobrador.BackColor = System.Drawing.Color.Transparent
        Me.chkCobrador.Color = System.Drawing.Color.Empty
        Me.chkCobrador.Location = New System.Drawing.Point(6, 88)
        Me.chkCobrador.Name = "chkCobrador"
        Me.chkCobrador.Size = New System.Drawing.Size(71, 21)
        Me.chkCobrador.SoloLectura = False
        Me.chkCobrador.TabIndex = 6
        Me.chkCobrador.Texto = "Cobrador:"
        Me.chkCobrador.Valor = False
        '
        'cbxCobrador
        '
        Me.cbxCobrador.CampoWhere = "IDCobrador"
        Me.cbxCobrador.CargarUnaSolaVez = False
        Me.cbxCobrador.DataDisplayMember = "Nombres"
        Me.cbxCobrador.DataFilter = Nothing
        Me.cbxCobrador.DataOrderBy = "Nombres"
        Me.cbxCobrador.DataSource = "Cobrador"
        Me.cbxCobrador.DataValueMember = "ID"
        Me.cbxCobrador.dtSeleccionado = Nothing
        Me.cbxCobrador.Enabled = False
        Me.cbxCobrador.FormABM = Nothing
        Me.cbxCobrador.Indicaciones = Nothing
        Me.cbxCobrador.Location = New System.Drawing.Point(112, 88)
        Me.cbxCobrador.Name = "cbxCobrador"
        Me.cbxCobrador.SeleccionMultiple = False
        Me.cbxCobrador.SeleccionObligatoria = False
        Me.cbxCobrador.Size = New System.Drawing.Size(175, 21)
        Me.cbxCobrador.SoloLectura = False
        Me.cbxCobrador.TabIndex = 7
        Me.cbxCobrador.Texto = ""
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(6, 69)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(71, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 4
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "Vendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(112, 67)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(175, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 5
        Me.cbxVendedor.Texto = ""
        '
        'chkPromotor
        '
        Me.chkPromotor.BackColor = System.Drawing.Color.Transparent
        Me.chkPromotor.Color = System.Drawing.Color.Empty
        Me.chkPromotor.Location = New System.Drawing.Point(6, 46)
        Me.chkPromotor.Name = "chkPromotor"
        Me.chkPromotor.Size = New System.Drawing.Size(71, 21)
        Me.chkPromotor.SoloLectura = False
        Me.chkPromotor.TabIndex = 2
        Me.chkPromotor.Texto = "Promotor:"
        Me.chkPromotor.Valor = False
        '
        'cbxPromotor
        '
        Me.cbxPromotor.CampoWhere = "IDPromotor"
        Me.cbxPromotor.CargarUnaSolaVez = False
        Me.cbxPromotor.DataDisplayMember = "Nombres"
        Me.cbxPromotor.DataFilter = Nothing
        Me.cbxPromotor.DataOrderBy = "Nombres"
        Me.cbxPromotor.DataSource = "Promotor"
        Me.cbxPromotor.DataValueMember = "ID"
        Me.cbxPromotor.dtSeleccionado = Nothing
        Me.cbxPromotor.Enabled = False
        Me.cbxPromotor.FormABM = Nothing
        Me.cbxPromotor.Indicaciones = Nothing
        Me.cbxPromotor.Location = New System.Drawing.Point(112, 46)
        Me.cbxPromotor.Name = "cbxPromotor"
        Me.cbxPromotor.SeleccionMultiple = False
        Me.cbxPromotor.SeleccionObligatoria = False
        Me.cbxPromotor.Size = New System.Drawing.Size(175, 21)
        Me.cbxPromotor.SoloLectura = False
        Me.cbxPromotor.TabIndex = 3
        Me.cbxPromotor.Texto = ""
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(6, 23)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(88, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 0
        Me.chkTipoCliente.Texto = "Tipo Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "TipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(112, 25)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(175, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 1
        Me.cbxTipoCliente.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 231)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(88, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 18
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkActivo
        '
        Me.chkActivo.BackColor = System.Drawing.Color.Transparent
        Me.chkActivo.Color = System.Drawing.Color.Empty
        Me.chkActivo.Location = New System.Drawing.Point(7, 193)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(88, 21)
        Me.chkActivo.SoloLectura = False
        Me.chkActivo.TabIndex = 16
        Me.chkActivo.Texto = "Estado Cta:"
        Me.chkActivo.Valor = False
        '
        'cbxActivo
        '
        Me.cbxActivo.CampoWhere = "IDEstado"
        Me.cbxActivo.CargarUnaSolaVez = False
        Me.cbxActivo.DataDisplayMember = "Descripcion"
        Me.cbxActivo.DataFilter = Nothing
        Me.cbxActivo.DataOrderBy = "Descripcion"
        Me.cbxActivo.DataSource = "EstadoCliente"
        Me.cbxActivo.DataValueMember = "ID"
        Me.cbxActivo.dtSeleccionado = Nothing
        Me.cbxActivo.Enabled = False
        Me.cbxActivo.FormABM = Nothing
        Me.cbxActivo.Indicaciones = Nothing
        Me.cbxActivo.Location = New System.Drawing.Point(112, 193)
        Me.cbxActivo.Name = "cbxActivo"
        Me.cbxActivo.SeleccionMultiple = False
        Me.cbxActivo.SeleccionObligatoria = False
        Me.cbxActivo.Size = New System.Drawing.Size(175, 21)
        Me.cbxActivo.SoloLectura = False
        Me.cbxActivo.TabIndex = 17
        Me.cbxActivo.Texto = ""
        '
        'chkCondicion
        '
        Me.chkCondicion.BackColor = System.Drawing.Color.Transparent
        Me.chkCondicion.Color = System.Drawing.Color.Empty
        Me.chkCondicion.Location = New System.Drawing.Point(7, 172)
        Me.chkCondicion.Name = "chkCondicion"
        Me.chkCondicion.Size = New System.Drawing.Size(99, 21)
        Me.chkCondicion.SoloLectura = False
        Me.chkCondicion.TabIndex = 14
        Me.chkCondicion.Texto = "Condición Vta:"
        Me.chkCondicion.Valor = False
        '
        'cbxCondicion
        '
        Me.cbxCondicion.CampoWhere = ""
        Me.cbxCondicion.CargarUnaSolaVez = False
        Me.cbxCondicion.DataDisplayMember = ""
        Me.cbxCondicion.DataFilter = Nothing
        Me.cbxCondicion.DataOrderBy = ""
        Me.cbxCondicion.DataSource = ""
        Me.cbxCondicion.DataValueMember = ""
        Me.cbxCondicion.dtSeleccionado = Nothing
        Me.cbxCondicion.Enabled = False
        Me.cbxCondicion.FormABM = Nothing
        Me.cbxCondicion.Indicaciones = Nothing
        Me.cbxCondicion.Location = New System.Drawing.Point(112, 172)
        Me.cbxCondicion.Name = "cbxCondicion"
        Me.cbxCondicion.SeleccionMultiple = False
        Me.cbxCondicion.SeleccionObligatoria = False
        Me.cbxCondicion.Size = New System.Drawing.Size(175, 21)
        Me.cbxCondicion.SoloLectura = False
        Me.cbxCondicion.TabIndex = 15
        Me.cbxCondicion.Texto = ""
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 117
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(112, 231)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(428, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 19
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(6, 152)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(71, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 12
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "Descripcion"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(112, 151)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(175, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 13
        Me.cbxMoneda.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(763, 287)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(586, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(277, 75)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "No compran Desde:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 2, 21, 13, 1, 45, 642)
        Me.txtHasta.Location = New System.Drawing.Point(197, 26)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 21, 13, 1, 45, 642)
        Me.txtDesde.Location = New System.Drawing.Point(115, 26)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(586, 287)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmListadoClienteNoCompra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(872, 313)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoClienteNoCompra"
        Me.Text = "frmListadoClienteNoCompra"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkActivo As ERP.ocxCHK
    Friend WithEvents cbxActivo As ERP.ocxCBX
    Friend WithEvents chkCondicion As ERP.ocxCHK
    Friend WithEvents cbxCondicion As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkPromotor As ERP.ocxCHK
    Friend WithEvents cbxPromotor As ERP.ocxCBX
    Friend WithEvents chkTipoCliente As ERP.ocxCHK
    Friend WithEvents cbxTipoCliente As ERP.ocxCBX
    Friend WithEvents chkDistribuidor As ERP.ocxCHK
    Friend WithEvents cbxDistribuidor As ERP.ocxCBX
    Friend WithEvents chkCobrador As ERP.ocxCHK
    Friend WithEvents cbxCobrador As ERP.ocxCBX
    Friend WithEvents chkListaPrecio As ERP.ocxCHK
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkZonaVenta As ERP.ocxCHK
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents chkDepartamento As ERP.ocxCHK
    Friend WithEvents cbxDepartamento As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents chkBarrio As ERP.ocxCHK
    Friend WithEvents cbxBarrio As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OcxTXTProducto1 As ERP.ocxTXTProducto
    Friend WithEvents chkProducto As ERP.ocxCHK
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAniversariosyCumpleaños
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkFecha = New ERP.ocxCHK()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.Label2)
        Me.gbxFiltro.Controls.Add(Me.Label1)
        Me.gbxFiltro.Controls.Add(Me.chkFecha)
        Me.gbxFiltro.Controls.Add(Me.txtHasta)
        Me.gbxFiltro.Controls.Add(Me.txtDesde)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoCliente)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 4)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(591, 126)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(430, 183)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(257, 183)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 6
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(10, 142)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(60, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 20
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(4, 92)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(76, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 28
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(109, 92)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(175, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 29
        Me.cbxSucursal.Texto = ""
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(4, 61)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(71, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 4
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "Vendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(109, 61)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(175, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 5
        Me.cbxVendedor.Texto = ""
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(4, 30)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(88, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 0
        Me.chkTipoCliente.Texto = "Tipo Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "TipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(109, 30)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(175, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 1
        Me.cbxTipoCliente.Texto = ""
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(76, 139)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(417, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 19
        '
        'chkFecha
        '
        Me.chkFecha.BackColor = System.Drawing.Color.Transparent
        Me.chkFecha.Color = System.Drawing.Color.Empty
        Me.chkFecha.Location = New System.Drawing.Point(321, 19)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(185, 21)
        Me.chkFecha.SoloLectura = False
        Me.chkFecha.TabIndex = 30
        Me.chkFecha.Texto = "Fecha Cumpleaños/Aniversario:"
        Me.chkFecha.Valor = False
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Enabled = False
        Me.txtHasta.Fecha = New Date(2013, 2, 19, 16, 17, 41, 382)
        Me.txtHasta.Location = New System.Drawing.Point(416, 78)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 32
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Enabled = False
        Me.txtDesde.Fecha = New Date(2013, 2, 19, 16, 17, 41, 382)
        Me.txtDesde.Location = New System.Drawing.Point(416, 49)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 31
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(352, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "Desde:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(352, 82)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Hasta:"
        '
        'frmAniversariosyCumpleaños
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(514, 222)
        Me.Controls.Add(Me.chkCliente)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.txtCliente)
        Me.Name = "frmAniversariosyCumpleaños"
        Me.Text = "frmAniversariosyCumpleaños"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkTipoCliente As ERP.ocxCHK
    Friend WithEvents cbxTipoCliente As ERP.ocxCBX
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkFecha As ERP.ocxCHK
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
End Class

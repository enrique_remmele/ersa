﻿Imports ERP.Reporte

Public Class frmAniversariosyCumpleaños


    'CLASES
    Dim CReporte As New CCliente
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "ANIVERSARIOS Y CUMPLEAÑOS DE CLIENTES"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        'Moneda
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, Vendedor  From VCliente "
        txtCliente.frm = Me

      
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar por Cliente
        If chkCliente.chk.Checked = True Then
            If txtCliente.txtRazonSocial.Texto <> "" Then
                Where = "Where "
                Where = Where + "ID = " & txtCliente.Registro("ID").ToString
            End If
        End If

        'Filtrar por FechaAlta
        If chkFecha.chk.Checked = True Then
            If Where <> "" Then
                Where = Where + " and FechaFestiva between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'"
            Else
                Where = "Where "
                Where = Where + " FechaFestiva between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'"
            End If
        End If


        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        CReporte.ListadoAniversarioCliente(frm, Where, Titulo, vgUsuarioIdentificador)

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkFechaAlta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkFecha.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub
End Class
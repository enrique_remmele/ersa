﻿Imports ERP.Reporte

Public Class frmExtractoMovimientoCliente



    'CLASES
    Dim CReporte As New CCliente
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "EXTRACTO DE MOVIMIENTOS CLIENTES"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

    End Sub

    Sub CargarInformacion()
        Try
            'Moneda
            cbxMoneda.cbx.SelectedIndex = 1
            txtCliente.Conectar()
            txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
            txtCliente.frm = Me
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As Boolean = True


        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'If txtCliente.txtID.txt.Text = "" Then
        '    Dim mensaje As String = "Ingrese primero algún cliente!"
        '    ctrError.SetError(txtCliente, mensaje)
        '    ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopRight)
        '    tsslEstado.Text = mensaje
        '    Exit Sub
        'End If

        If Not IsDate(txtDesde.txt.Text) Or Not IsDate(txtHasta.txt.Text) Then
            Dim mensaje As String = "La fecha no es correcta!"
            ctrError.SetError(txtDesde, mensaje)
            ctrError.SetIconAlignment(txtDesde, ErrorIconAlignment.TopRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If txtCliente.txtID.txt.Text <> "" Then
            Where = "@Fecha1='" & txtDesde.GetValueString & "',@Fecha2='" & txtHasta.GetValueString & "',@IDCliente=" & txtCliente.Registro("ID").ToString & ", @Todos='False'"
        Else
            Where = "@Fecha1='" & txtDesde.GetValueString & "',@Fecha2='" & txtHasta.GetValueString & "',@IDCliente= 0, @Todos='True'"
        End If
        If cbxMoneda.Enabled = True Then
            WhereDetalle = "IDMoneda = " & cbxMoneda.GetValue
        End If

        If cbxSucusal.Enabled = True Then
            WhereDetalle = "IDSucursal = " & cbxSucusal.GetValue
        End If

        TipoInforme = ""
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)

        If txtCliente.txtID.txt.Text <> "" Then
            CReporte.ExtractoMovimientoCliente(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, False)
        Else
            CReporte.ExtractoMovimientoCliente(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, True)
        End If

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmExtractoMovimientoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkMoneda_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucusal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmExtractoMovimientoCliente_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        LiberarMemoria()
    End Sub
End Class




﻿Imports ERP.Reporte

Public Class frmListadoCliente

    'CLASES
    Dim CReporte As New CCliente
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "INDICE GENERAL DE CLIENTES"
    Dim TipoInforme As String


    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        'Condición de Venta
        cbxCondicion.cbx.Items.Add("Contado")
        cbxCondicion.cbx.Items.Add("Crédito")

        'Moneda
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito, 'Limite de Credito'=LimiteCredito, 'Plazo de Credito'=PlazoCredito  From VCliente "
        txtCliente.frm = Me

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")

        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar por Cliente
        If chkCliente.chk.Checked = True Then
            If txtCliente.txtRazonSocial.Texto <> "" Then
                Where = "Where "
                Where = Where + "ID = " & txtCliente.Registro("ID").ToString
            End If
        End If

        'Filtrar por FechaAlta
        If chkFechaAlta.chk.Checked = True Then
            If Where <> "" Then
                Where = Where + " and FechaAlta between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'"
            Else
                Where = "Where "
                Where = Where + " FechaAlta between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'"
            End If
        End If

        'Filtrar por Condición Venta
        If chkCondicion.chk.Checked = True Then
            If cbxCondicion.cbx.Text = "Contado" Then
                If Where <> "" Then
                    Where = Where + " and Contado = 1"
                Else
                    Where = "Where "
                    Where = Where + " Contado = 1"
                End If
            ElseIf cbxCondicion.cbx.Text = "Crédito" Then
                If Where <> "" Then
                    Where = Where + " and Credito = 1"
                Else
                    Where = "Where "
                    Where = Where + " Credito = 1"
                End If
            End If
        End If


        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecer(Where)

        If cbxOrdenadoPor.cbx.Text = "RazonSocial" Then
            Orden = "Por Nombre o Razón Social"
        ElseIf cbxOrdenadoPor.cbx.Text = "TipoCliente" Then
            Orden = "Por Tipo Cliente"
        ElseIf cbxOrdenadoPor.cbx.Text = "Promotor" Then
            Orden = "Por Promotor"
        ElseIf cbxOrdenadoPor.cbx.Text = "Vendedor" Then
            Orden = "Por Vendedor"
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        If chkExcesoLineaCredito.Valor = True Then
            If Where = "" Then
                Where = "  where SaldoCredito <0 and Credito = 1 "
            Else
                Where = Where & "  and SaldoCredito <0 and Credito = 1"
            End If
        End If

        If chkJudicial.Valor = True Then
            If Where = "" Then
                Where = "  where Judicial = 'True' "
            Else
                Where = Where & "  and Judicial = 'True' "
            End If
        End If

        CReporte.ListadoCliente(frm, Where, Titulo, OrderBy, Orden, vgUsuarioIdentificador)
    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Cliente", "Select Top(0) RazonSocial,TipoCliente,Promotor,Vendedor From VCliente")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next


    End Sub

    'Sub Establecer(ByRef where As String)

    '    'Tipo de Cliente
    '    cbxTipoCliente.EstablecerCondicion(where)

    '    'Categoria
    '    cbxCategoriaCliente.EstablecerCondicion(where, chkCategoriaCliente.Valor)

    '    'Promotor
    '    cbxPromotor.EstablecerCondicion(where, chkPromotor.Valor)

    '    'Vendedor
    '    cbxVendedor.EstablecerCondicion(where, chkVendedor.Valor)

    '    'Cobrador
    '    cbxCobrador.EstablecerCondicion(where, chkCobrador.Valor)

    '    'Distribuidor
    '    cbxDistribuidor.EstablecerCondicion(where, chkDistribuidor.Valor)

    '    'Lista de Precio
    '    cbxListaPrecio.EstablecerCondicion(where, chkListaPrecio.Valor)

    '    'Moneda
    '    cbxMoneda.EstablecerCondicion(where, chkMoneda.Valor)

    '    'Condición de venta
    '    cbxCondicion.EstablecerCondicion(where, chkCondicion.Valor)

    '    'Estado de cta
    '    cbxActivo.EstablecerCondicion(where, chkActivo.Valor)

    '    'Barrio
    '    cbxBarrio.EstablecerCondicion(where, chkBarrio.Valor)

    '    'Ciudad
    '    cbxCiudad.EstablecerCondicion(where, chkCiudad.Valor)

    '    'Departamento
    '    cbxDepartamento.EstablecerCondicion(where, chkDepartamento.Valor)

    '    'Zona de ventas
    '    cbxZonaVenta.EstablecerCondicion(where, chkZonaVenta.Valor)

    '    'Sucursal
    '    cbxSucursal.EstablecerCondicion(where, chkSucursal.Valor)


    'End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub
    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub

    Private Sub chkPromotor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkPromotor.PropertyChanged
        cbxPromotor.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkCobrador_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCobrador.PropertyChanged
        cbxCobrador.Enabled = value
    End Sub

    Private Sub chkDistribuidor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDistribuidor.PropertyChanged
        cbxDistribuidor.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub chkActivo_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkActivo.PropertyChanged
        cbxActivo.Enabled = value
    End Sub

    Private Sub chkFechaAlta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkFechaAlta.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkCondicion_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCondicion.PropertyChanged
        cbxCondicion.Enabled = value
    End Sub

    Private Sub chkBarrio_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkBarrio.PropertyChanged
        cbxBarrio.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub


    Private Sub chkDepartamento_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepartamento.PropertyChanged
        cbxDepartamento.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCategoriaCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCategoriaCliente.PropertyChanged
        cbxCategoriaCliente.Enabled = value
    End Sub
End Class




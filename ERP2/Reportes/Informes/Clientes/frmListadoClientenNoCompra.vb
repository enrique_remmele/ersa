﻿Imports ERP.Reporte

Public Class frmListadoClienteNoCompra

    'CLASES
    Dim CReporte As New CCliente
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = "LISTADO DE CLIENTES QUE NO COMPRAN DESDE"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        'Condición de Venta
        cbxCondicion.cbx.Items.Add("Contado")
        cbxCondicion.cbx.Items.Add("Crédito")

        'Moneda
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
        txtCliente.frm = Me

        OcxTXTProducto1.Conectar()

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As String = ""
        Dim SQLClienteProducto As String = ""
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        If chkProducto.Valor Then
            If OcxTXTProducto1.Registro Is Nothing Then
                MessageBox.Show("Seleccione un Producto", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            SQLClienteProducto = "Select 'UltimaCompra'= dbo.FUltimaCompraClienteProducto(VCliente.ID, " & OcxTXTProducto1.Registro("ID").ToString & "), VCliente.ID, VCliente.Referencia, VCliente.RazonSocial, VCliente.NombreFantasia, VCliente.Direccion, VCliente.Telefono, VCliente.Fax, VCliente.Vendedor, VCliente.Condicion, VCliente.LimiteCredito, VCliente.PlazoCredito From VCliente "
            Where = " Where dbo.FUltimaCompraClienteProducto(VCliente.ID," & OcxTXTProducto1.Registro("ID").ToString & ") Not between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "' "
        Else
            Where = " Where ((Select Max(V.Fecha) From VVenta V Where V.Anulado='False' And V.IDCliente=VCliente.ID) Not between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "') "
        End If



        'Filtrar por Cliente
        If chkCliente.chk.Checked = True Then
            If txtCliente.txtRazonSocial.Texto <> "" Then
                Where = Where + " and ID = " & txtCliente.Registro("ID").ToString
            End If
        End If

        'Filtrar por Condición Venta
        If chkCondicion.chk.Checked = True Then
            If cbxCondicion.cbx.Text = "Contado" Then
                Where = Where + " and Contado = 1"
            End If
        End If

        If chkCondicion.chk.Checked = True Then
            If cbxCondicion.cbx.Text = "Crédito" Then
                Where = Where + " and Credito = 1"
            End If
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        If cbxOrdenadoPor.cbx.Text = "RazonSocial" Then
            Orden = "Por Nombre o Razón Social"
        ElseIf cbxOrdenadoPor.cbx.Text = "TipoCliente" Then
            Orden = "Por Tipo Cliente"
        ElseIf cbxOrdenadoPor.cbx.Text = "Promotor" Then
            Orden = "Por Promotor"
        ElseIf cbxOrdenadoPor.cbx.Text = "Vendedor" Then
            Orden = "Por Vendedor"
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        If chkProducto.Valor Then
            CReporte.ListadoClienteNoCompraProducto(frm, SQLClienteProducto, Where, Titulo, TipoInforme, OrderBy, Orden, vgUsuarioIdentificador)
        Else
            CReporte.ListadoClienteNoCompra(frm, Where, Titulo, TipoInforme, OrderBy, Orden, vgUsuarioIdentificador)
        End If


    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Cliente", "Select Top(0) RazonSocial,TipoCliente,Promotor,Vendedor From VCliente")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub

    Private Sub chkPromotor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkPromotor.PropertyChanged
        cbxPromotor.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkCobrador_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCobrador.PropertyChanged
        cbxCobrador.Enabled = value
    End Sub

    Private Sub chkDistribuidor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDistribuidor.PropertyChanged
        cbxDistribuidor.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub chkActivo_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkActivo.PropertyChanged
        cbxActivo.Enabled = value
    End Sub

    Private Sub chkFechaAlta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkCondicion_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCondicion.PropertyChanged
        cbxCondicion.Enabled = value
    End Sub

    Private Sub chkBarrio_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkBarrio.PropertyChanged
        cbxBarrio.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkDepartamento_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDepartamento.PropertyChanged
        cbxDepartamento.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged1(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        OcxTXTProducto1.Enabled = value
        OcxTXTProducto1.SoloLectura = Not value
    End Sub
End Class




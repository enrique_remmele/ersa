﻿Imports ERP.Reporte
Public Class frmListadoFacturaCompraEmitida
    'CLASES
    Dim CReporte As New CReporteCompras
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE FACTURAS RECIBIDAS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Form
        Me.KeyPreview = True
        Me.AcceptButton = New Button

        'Funciones
        CargarInformacion()

        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
        txtFechaOperDesde.Hoy()
        txtFechaOperHasta.Hoy()
        chkFechaDocumento.Valor = True
        chkFechaOperacion.Valor = True
    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Condicion
        cbxVentas.cbx.Items.Add("Contado + Crédito")
        cbxVentas.cbx.Items.Add("Contado")
        cbxVentas.cbx.Items.Add("Crédito")
        cbxVentas.cbx.SelectedIndex = 0

        'Configuracion
        cbxTipoInforme.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "REPORTE", "")
        cbxOrdenadoPor.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENADO", "")
        cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")
        cbxEnForma.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ENFORMA", "")
        nudRanking.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "RANKING", "")

    End Sub

    Sub GuardarInformacion()
        'Configuracion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "REPORTE", cbxTipoInforme.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENADO", cbxOrdenadoPor.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ENFORMA", cbxEnForma.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RANKING", nudRanking.Text)

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = "IDProducto>0"
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " where 1 = 1 "
        If chkFechaDocumento.Valor = True Then
            Where = Where & " and C.Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        End If
        If chkFechaOperacion.Valor = True Then
            Where = Where & " and Cast(C.FechaTransaccion as date) Between '" & txtFechaOperDesde.GetValueString & "' And '" & txtFechaOperHasta.GetValueString & "' "
        End If

        'Moneda
        Where = Where & " And IdMoneda=" & cbxMoneda.GetValue

        'Tipo Comprobante
        If chkTipoComprobante.chk.Checked = True Then
            Where = Where & " And IDTipoComprobante=" & cbxTipoComprobante.GetValue
        End If

        'Sucursal
        If chkSucursal.chk.Checked = True Then
            Where = Where & " And IDSucursal=" & cbxSucursal.GetValue
        End If

        '02-07-2021 SM: Nuevo filtro
        If chkDepartamento.chk.Checked = True Then
            Where = Where & " And IDDepartamentoEmpresa= " & cbxDepartamento.cbx.SelectedValue
        End If

        'Condicion
        If cbxVentas.txt.Text = "Crédito" Then
            Where = Where & " And Condicion='CRED'"
        End If

        If cbxVentas.txt.Text = "Contado" Then
            Where = Where & " And Condicion='CONT'"
        End If

        'Deposito
        If chkDeposito.chk.Checked = True Then
            Where = Where & " And IDDeposito=" & cbxDeposito.GetValue
        End If

        'Proveedor
        If chkProveedor.chk.Checked = True Then
            Where = Where & " And IDproveedor=" & cbxProveedor.GetValue
        End If

        'Establecemos los filtros

        For Each ctr As Object In gbxFiltro.Controls
            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxTipoComprobante" Or ctr.name = "cbxSucursal" Or ctr.name = "cbxDeposito" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(WhereDetalle)
            End If

            GoTo siguiente

siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
            If cbxOrdenadoPor.cbx.Text <> "Comprobante" Then
                OrderBy = OrderBy & ", Comprobante"
            End If
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        EstablecerSubTitulo()

        Select Case cbxTipoInforme.cbx.SelectedIndex

            Case 0
                Titulo = "LISTADO DE FACTURAS COMPRA"
                CReporte.ListadoFacturasComprasEmitidas(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
            Case 1
                Titulo = "LISTADO DE FACTURAS COMPRA"
                CReporte.ListadoFacturasComprasEmitidasDetalle(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        End Select

    End Sub

    Sub EstablecerSubTitulo()

        If cbxTipoInforme.cbx.SelectedIndex = 0 Then
            TipoInforme = "RESUMIDO"
        End If

        If cbxTipoInforme.cbx.SelectedIndex = 1 Then
            TipoInforme = "DETALLADO"
        End If

        Dim Filtro As String = ""

        TipoInforme = TipoInforme & " desde fecha: " & txtDesde.GetValue.ToShortDateString & " a " & txtHasta.GetValue.ToShortDateString


        If chkTipoComprobante.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxTipoComprobante.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxTipoComprobante.cbx.Text
            End If
        End If

        If chkSucursal.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxSucursal.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxSucursal.cbx.Text
            End If
        End If
        '02-07-2021 SM - Nuevo filtro para Documento Empresa
        If chkDepartamento.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxDepartamento.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxDepartamento.cbx.Text
            End If
        End If

        If chkDeposito.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxDeposito.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxDeposito.cbx.Text
            End If
        End If

        If chkProducto.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxProducto.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxProducto.cbx.Text
            End If
        End If

        If chkProveedor.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxProveedor.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxProveedor.cbx.Text
            End If
        End If

        If chkTipoProducto.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxTipoProducto.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxTipoProducto.cbx.Text
            End If
        End If

        If chkMarca.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxMarca.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxMarca.cbx.Text
            End If
        End If

        If chkLinea.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxLinea.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxLinea.cbx.Text
            End If
        End If

        If chkSubLinea.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxSubLinea.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxSubLinea.cbx.Text
            End If
        End If

        If chkSubLinea2.Valor = True Then
            If Filtro = "" Then
                Filtro = "Filtrado por: " & cbxSubLinea2.cbx.Text
            Else
                Filtro = Filtro & " - " & cbxSubLinea2.cbx.Text
            End If
        End If

        If Filtro <> "" Then
            TipoInforme = TipoInforme & " " & Filtro
        End If

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        Select Case cbxTipoInforme.cbx.SelectedIndex

            Case 0
                dt = CData.GetStructure("VCompra", "Select Top(0) Comprobante, Proveedor, Fecha, Sucursal, Deposito, Total From VCompra ")
            Case 1
                dt = CData.GetStructure("VCompra", "Select Top(0) Comprobante, Proveedor, Fecha, Sucursal, Deposito, Total From VCompra ")
        End Select

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = chkTipoComprobante.Valor
    End Sub

    Private Sub frmListadoFacturaCompraEmitida_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmListadoFacturaCompraEmitida_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        CambiarOrdenacion()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProducto.PropertyChanged
        cbxProducto.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        cbxDeposito.DataFilter = " IDSucursal = " & cbxSucursal.GetValue
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
    End Sub

    Private Sub chkLinea_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    Private Sub chkMarca_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
    End Sub

    Private Sub chkSubLinea_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkSubLinea.PropertyChanged
        cbxSubLinea.Enabled = value
    End Sub

    Private Sub chkSubLinea2_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkSubLinea2.PropertyChanged
        cbxSubLinea2.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub


    Private Sub chkFechaDocumento_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaDocumento.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkFechaOperacion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkFechaOperacion.PropertyChanged
        txtFechaOperDesde.Enabled = value
        txtFechaOperHasta.Enabled = value
    End Sub

    Private Sub chkUsuario_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkDepartamento_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkDepartamento.PropertyChanged
        cbxDepartamento.Enabled = value
    End Sub


End Class
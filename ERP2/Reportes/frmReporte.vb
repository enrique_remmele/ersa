﻿Public Class frmReporte

    'Varialbes
    Private dsTempValue As DataSet
    Public Property dsTemp() As DataSet
        Get
            Return dsTempValue
        End Get
        Set(ByVal value As DataSet)
            dsTempValue = value
        End Set
    End Property

    Sub CargarTablas()

        If dsTemp Is Nothing Then

            'Ocultar exportar
            flpExportar.Visible = False
            TableLayoutPanel1.RowStyles(1).Height = 0
            Exit Sub

        End If

        For Each t As DataTable In dsTemp.Tables

            Dim btn As New Button
            btn.Text = t.TableName
            flpExportar.Controls.Add(btn)

            AddHandler btn.Click, AddressOf ExportarTabla

        Next

    End Sub

    Sub ExportarTabla(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dt As New DataTable

        For Each t As DataTable In dsTemp.Tables
            If t.TableName = sender.text Then

                'Cargar campos
                For c As Integer = 0 To t.Columns.Count - 1
                    dt.Columns.Add(t.Columns(c).ColumnName)
                Next

                For i As Integer = 0 To t.Rows.Count - 1
                    Dim NewRow As DataRow = dt.NewRow
                    For c As Integer = 0 To t.Columns.Count - 1
                        NewRow(t.Columns(c).ColumnName) = t.Rows(i)(c)
                    Next
                    dt.Rows.Add(NewRow)

                Next

            End If

        Next

        Dim fic As String = VGCarpetaTemporal & "\" & sender.text

        'Vendedor
        Try

            If IO.Directory.Exists(VGCarpetaTemporal) = False Then

                'Eliminar
                IO.Directory.CreateDirectory(VGCarpetaTemporal)

            End If

            If IO.File.Exists(fic) = True Then

                'Eliminar
                IO.File.Delete(fic)

            End If

            Dim sw As New System.IO.StreamWriter(fic, False)

            Dim cadena As String = ""

            cadena = ""
            For c As Integer = 0 To dt.Columns.Count - 1

                If c = 0 Then
                    cadena = cadena & dt.Columns(c).ColumnName
                Else
                    cadena = cadena & vbTab & dt.Columns(c).ColumnName
                End If

            Next
            sw.WriteLine(cadena)

            For Each oRow As DataRow In dt.Rows
                cadena = ""
                For c As Integer = 0 To dt.Columns.Count - 1

                    If c = 0 Then
                        cadena = cadena & oRow(c).ToString
                    Else
                        cadena = cadena & vbTab & oRow(c).ToString
                    End If

                Next

                sw.WriteLine(cadena)
            Next

            sw.Close()

        Catch ex As Exception

        End Try

        Dim savefile As New FolderBrowserDialog
        savefile.SelectedPath = My.Computer.FileSystem.SpecialDirectories.Desktop

        Try
            If savefile.ShowDialog = DialogResult.OK Then
                FileCopy(fic, savefile.SelectedPath & "\" & sender.text & ".xls")
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub frmReporte_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

        'Select Case e.KeyCode
        '    Case Keys.Down
        '        'e.Handled = True
        '        SendKeys.Send("{PGDN}")

        '    Case Keys.Up
        '        'e.Handled = True
        '        SendKeys.Send("{PGUP}")
        'End Select

    End Sub

    Private Sub frmReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.KeyPreview = True

        Dim report As CrystalDecisions.CrystalReports.Engine.ReportClass = CrystalReportViewer1.ReportSource
        If report.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape Then
            CrystalReportViewer1.Zoom(1)
        Else
            CrystalReportViewer1.Zoom(100)
        End If
        CrystalReportViewer1.AutoScroll = False

        CargarTablas()

    End Sub

    Private Sub CrystalReportViewer1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CrystalReportViewer1.KeyDown

        Dim report As CrystalDecisions.CrystalReports.Engine.ReportClass = CrystalReportViewer1.ReportSource


    End Sub

    Private Sub CrystalReportViewer1_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles CrystalReportViewer1.MouseWheel
        Dim Cantidad As Integer = e.Delta
        Dim Arriba As Boolean

        If Cantidad > 0 Then
            Arriba = True
        Else
            Arriba = False
        End If

        'If My.Computer.Keyboard.CtrlKeyDown = True Then
        '    If Arriba = True Then
        '        CrystalReportViewer1.Zoom(100)
        '    Else
        '        CrystalReportViewer1.Zoom(2)
        '    End If

        '    Exit Sub
        'Else
        '    If Arriba = True Then
        '        CrystalReportViewer1_KeyDown(sender, New KeyEventArgs(Keys.PageUp))
        '    Else
        '        CrystalReportViewer1_KeyDown(sender, New KeyEventArgs(Keys.PageDown))
        '    End If
        'End If


    End Sub

    Private Sub frmReporte_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        CrystalReportViewer1.Dispose()
        LiberarMemoria()

    End Sub
End Class
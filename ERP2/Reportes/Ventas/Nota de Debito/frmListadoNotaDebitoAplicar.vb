﻿Imports ERP.Reporte
Public Class frmListadoNotaDebitoAplicar

    'CLASES
    Dim CReporte As New CReporteNotaDebito
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE NOTAS CREDITO APLICAR"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

        CambiarOrdenacion()

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And Aplicar='True' And Saldo>0 "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CReporte.ImprimirNotaDebitoAplicar(frm, Where, WhereDetalle, OrderBy, Top)

        'Select Case cbxTipoInforme.cbx.SelectedIndex

        '    Case 0
        '        TipoInforme = cbxTipoInforme.cbx.Text
        '        CReporte.ListadoFacturasEmitidas(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        '    Case 1
        '        TipoInforme = cbxTipoInforme.cbx.Text
        '        cbxProducto.EstablecerCondicion(WhereDetalle)
        '        CReporte.ListadoFacturasEmitidasDetalle(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        'End Select



    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VNotaCredito", "Select Top(0) Comprobante, Cliente, Fecha, Sucursal, Deposito, Total From VNotaCredito ")



        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub


    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class






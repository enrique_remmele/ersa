﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoNotaDebitoAplicar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxCliente = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(357, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 140)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 73)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 73)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 56)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(6, 114)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 7
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 13)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 98)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 6
        Me.lblRanking.Text = "Ranking:"
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 3, 19, 11, 5, 9, 675)
        Me.txtHasta.Location = New System.Drawing.Point(89, 32)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 3, 19, 11, 5, 9, 675)
        Me.txtDesde.Location = New System.Drawing.Point(6, 32)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 63)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 20
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxCliente
        '
        Me.cbxCliente.CampoWhere = "IDCliente"
        Me.cbxCliente.CargarUnaSolaVez = False
        Me.cbxCliente.DataDisplayMember = "RazonSocial"
        Me.cbxCliente.DataFilter = Nothing
        Me.cbxCliente.DataOrderBy = "RazonSocial"
        Me.cbxCliente.DataSource = "VCliente"
        Me.cbxCliente.DataValueMember = "ID"
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormABM = Nothing
        Me.cbxCliente.Indicaciones = Nothing
        Me.cbxCliente.Location = New System.Drawing.Point(129, 63)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.SeleccionObligatoria = False
        Me.cbxCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxCliente.SoloLectura = False
        Me.cbxCliente.TabIndex = 21
        Me.cbxCliente.Texto = "ZUNILDA FLOR CHAMORRO"
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(9, 19)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(114, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 6
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 2)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(348, 91)
        Me.gbxFiltro.TabIndex = 12
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 40)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 8
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(129, 40)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 9
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudad"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Ciudad"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VSucursal"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(129, 19)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(213, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 7
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(372, 150)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 14
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(524, 150)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 15
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'frmListadoNotaDebitoAplicar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(601, 173)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoNotaDebitoAplicar"
        Me.Text = "frmListadoNotaDebitoAplicar"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents cbxCliente As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
End Class

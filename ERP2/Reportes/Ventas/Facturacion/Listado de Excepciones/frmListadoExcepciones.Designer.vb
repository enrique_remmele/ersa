﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoExcepciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkHasta = New ERP.ocxCHK()
        Me.chkDesde = New ERP.ocxCHK()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.txtHasta2 = New ERP.ocxTXTDate()
        Me.txtDesde1 = New ERP.ocxTXTDate()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkListaPrecio = New ERP.ocxCHK()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkHasta)
        Me.gbxFiltro.Controls.Add(Me.chkDesde)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.txtHasta2)
        Me.gbxFiltro.Controls.Add(Me.txtDesde1)
        Me.gbxFiltro.Controls.Add(Me.txtHasta)
        Me.gbxFiltro.Controls.Add(Me.txtDesde)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.cbxListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(442, 313)
        Me.gbxFiltro.TabIndex = 5
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(7, 161)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 38
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDListaPrecio"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "vTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(127, 161)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(278, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 39
        Me.cbxTipoProducto.Texto = ""
        '
        'chkHasta
        '
        Me.chkHasta.BackColor = System.Drawing.Color.Transparent
        Me.chkHasta.Color = System.Drawing.Color.Empty
        Me.chkHasta.Location = New System.Drawing.Point(218, 248)
        Me.chkHasta.Name = "chkHasta"
        Me.chkHasta.Size = New System.Drawing.Size(114, 21)
        Me.chkHasta.SoloLectura = False
        Me.chkHasta.TabIndex = 37
        Me.chkHasta.Texto = "Hasta:"
        Me.chkHasta.Valor = False
        '
        'chkDesde
        '
        Me.chkDesde.BackColor = System.Drawing.Color.Transparent
        Me.chkDesde.Color = System.Drawing.Color.Empty
        Me.chkDesde.Location = New System.Drawing.Point(6, 248)
        Me.chkDesde.Name = "chkDesde"
        Me.chkDesde.Size = New System.Drawing.Size(114, 21)
        Me.chkDesde.SoloLectura = False
        Me.chkDesde.TabIndex = 36
        Me.chkDesde.Texto = "Desde:"
        Me.chkDesde.Valor = True
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(8, 214)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(421, 23)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 35
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(8, 191)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(69, 17)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 34
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'txtHasta2
        '
        Me.txtHasta2.Color = System.Drawing.Color.Empty
        Me.txtHasta2.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtHasta2.Location = New System.Drawing.Point(302, 273)
        Me.txtHasta2.Name = "txtHasta2"
        Me.txtHasta2.PermitirNulo = False
        Me.txtHasta2.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta2.SoloLectura = False
        Me.txtHasta2.TabIndex = 33
        '
        'txtDesde1
        '
        Me.txtDesde1.Color = System.Drawing.Color.Empty
        Me.txtDesde1.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtDesde1.Location = New System.Drawing.Point(219, 273)
        Me.txtDesde1.Name = "txtDesde1"
        Me.txtDesde1.PermitirNulo = False
        Me.txtDesde1.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde1.SoloLectura = False
        Me.txtDesde1.TabIndex = 32
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtHasta.Location = New System.Drawing.Point(89, 273)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 30
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtDesde.Location = New System.Drawing.Point(6, 273)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 29
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(7, 102)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(421, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 27
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(7, 82)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 26
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkListaPrecio
        '
        Me.chkListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkListaPrecio.Location = New System.Drawing.Point(7, 56)
        Me.chkListaPrecio.Name = "chkListaPrecio"
        Me.chkListaPrecio.Size = New System.Drawing.Size(114, 21)
        Me.chkListaPrecio.SoloLectura = False
        Me.chkListaPrecio.TabIndex = 6
        Me.chkListaPrecio.Texto = "Lista de Precios:"
        Me.chkListaPrecio.Valor = False
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = "IDListaPrecio"
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Lista"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = "Lista"
        Me.cbxListaPrecio.DataSource = "VListaPrecio"
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.Enabled = False
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(127, 56)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(278, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 7
        Me.cbxListaPrecio.Texto = "BARES Y COPETINES"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(7, 30)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 20
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(127, 30)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(278, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 21
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(139, 331)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 9
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(19, 331)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(98, 23)
        Me.btnInforme.TabIndex = 8
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmListadoExcepciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(467, 366)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoExcepciones"
        Me.Tag = "frmListadoExcepciones"
        Me.Text = "frmListadoExcepciones"
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkListaPrecio As ERP.ocxCHK
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents txtHasta2 As ERP.ocxTXTDate
    Friend WithEvents txtDesde1 As ERP.ocxTXTDate
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents chkHasta As ERP.ocxCHK
    Friend WithEvents chkDesde As ERP.ocxCHK
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
End Class

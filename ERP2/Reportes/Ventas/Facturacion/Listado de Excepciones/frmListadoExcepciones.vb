﻿Imports ERP.Reporte
Public Class frmListadoExcepciones

    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    'VARIABLES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'funciones
        CargarInformacion()

        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

        txtDesde1.Hoy()
        txtHasta2.UltimoDiaMes()

    End Sub

    Sub CargarInformacion()
        'Cliente
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
        txtCliente.frm = Me

        'Producto
        txtProducto.Conectar()
    End Sub

    Sub Listar()

        Dim WhereDetalle As String = " where 1 = 1"
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim SubTitulo As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkDesde.Valor = True Then
            WhereDetalle = WhereDetalle & " and Desde Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
        End If

        If chkHasta.Valor = True Then
            WhereDetalle = WhereDetalle & " and Hasta Between '" & txtDesde1.GetValueString & "' And '" & txtHasta2.GetValueString & "'"
        End If

        'Producto
        If chkProducto.Valor = True Then
            If txtProducto.Seleccionado = False Then
                Exit Sub
            End If

            WhereDetalle = WhereDetalle & " And IDProducto=" & txtProducto.Registro("ID") & " "

        End If

        'Cliente
        If chkCliente.Valor = True Then
            If txtCliente.Seleccionado = False Then
                Exit Sub
            End If

            WhereDetalle = WhereDetalle & " And IDCliente=" & txtCliente.Registro("ID") & " "
            WhereDetalle = WhereDetalle & " And IDCliente=" & txtCliente.Registro("ID") & " "

        End If

        'Sucursal
        If chkSucursal.Valor = True Then
            WhereDetalle = WhereDetalle & " and IdSucursal = " & cbxSucursal.GetValue
        End If

        'Lista de precio
        If chkListaPrecio.Valor = True Then
            WhereDetalle = WhereDetalle & " and IdListaPrecio = " & cbxListaPrecio.GetValue
        End If

        'Filtros del Reporte para Subtitulo
        Dim Filtro As String = ""
        'Sucursal
        If chkSucursal.Valor = True And cbxSucursal.txt.Text <> "" Then
            Filtro = Filtro & "(Suc. - " & cbxSucursal.txt.Text & ")"
        End If
        'Lista de precio
        If chkListaPrecio.Valor = True And cbxListaPrecio.txt.Text <> "" Then
            Filtro = Filtro & "(Lista.Precio - " & cbxListaPrecio.txt.Text & ")"
        End If
        'Producto
        If chkProducto.Valor = True And txtProducto.txt.Texto <> "" Then
            Filtro = Filtro & "(Prod. - " & txtProducto.txt.Text & ")"
        End If
        'Cliente
        If chkCliente.Valor = True And txtCliente.txtReferencia.txt.Text <> "" Then
            Filtro = Filtro & "Clie. - " & txtCliente.txtReferencia.txt.Text & "-" & txtCliente.txtRazonSocial.txt.Text & ")"
        End If

        CReporte.ListadoExcepciones(frm, WhereDetalle, "LISTADO DE EXCEPCIONES", Filtro, SubTitulo)

    End Sub

    Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkDesde_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkDesde.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkHasta_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkHasta.PropertyChanged
        txtDesde1.Enabled = value
        txtHasta2.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoExcepciones_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmGraficoRankingClientesVenta


    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "GRAFICO RANKING CLIENTE VENTAS"
    Dim TipoInforme As String

    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " FechaEmision between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'"

        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CReporte.ArmarSubTitulo(TipoInforme, New GroupBox, txtDesde, txtHasta)
        CReporte.GraficoRankingClienteVentas(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, Top)


    End Sub

    Sub CargarInformacion()

    End Sub


    Private Sub frmListadoVentasAnualTotalTipoCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class
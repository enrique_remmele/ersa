﻿Imports ERP.Reporte
Public Class frmGraficoVentaMesTotal


    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "GRAFICO VENTAS MES TOTAL"
    Dim TipoInforme As String

    Sub Inicializar()
        CargarInformacion()
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If cbxAño.Text <> "" Then
            Where = cbxAño.Text & " Group By Mes,MesNumero order by MesNumero asc"
        Else
            CSistema.MostrarError("El año no puede estar vacío", ctrError, btnInforme, tsslEstado, ErrorIconAlignment.TopRight)
            Exit Sub
        End If
        CReporte.GraficoVentaMesTotal(frm, Where, Titulo, vgUsuarioIdentificador, cbxAño.Text)

    End Sub
    Sub CargarInformacion()

        'Cargar Años
        cbxAño.Items.Add("2011")
        cbxAño.Items.Add("2012")
        cbxAño.Items.Add("2013")
        cbxAño.Items.Add("2014")
        cbxAño.Items.Add("2015")
        cbxAño.Items.Add("2016")
        cbxAño.Items.Add("2017")
        cbxAño.Items.Add("2018")
        cbxAño.Items.Add("2019")
        cbxAño.Items.Add("2020")
        cbxAño.DropDownStyle = ComboBoxStyle.DropDownList


    End Sub


    Private Sub frmListadoVentasAnualTotalTipoCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class
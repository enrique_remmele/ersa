﻿Imports ERP.Reporte

Public Class frmListadoVentasPorProducto

    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    'VARIABLES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'funciones
        CargarInformacion()
        chkCancelacionAutomatica.Valor = True
        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
        chkIncluirProductosSinControl.Valor = True
    End Sub

    Sub Listar()

        Dim WhereDetalle As String = ""
        Dim WhereDetalleNCR As String = ""
        Dim WhereNCR As String = "" '=> SC:21-05-2022 nuevo filtro where solo para notas de credito
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim SubTitulo As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        WhereDetalle = " Where  FechaEmision Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
        'WhereDetalleNCR = "Where (FechaNCR < '" & txtDesde.GetValueString & "' Or FechaNCR > '" & txtHasta.GetValueString & "') And FechaEmision Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
        '=> SC:21-05-2022 nuevo filtro where solo para unir con detalle notas de credito
        WhereDetalleNCR = "Where (FechaNCR Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "') And FechaEmision Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"
        '=> SC:21-05-2022 nuevo
        WhereNCR = " Where (FechaNCR Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "')"


        If cbxVentas.txt.Text = "Crédito" Then
            WhereDetalle = WhereDetalle & " And Credito=1"
            WhereDetalleNCR = WhereDetalleNCR & " And Credito=1"
        End If

        If cbxVentas.txt.Text = "Contado" Then
            WhereDetalle = WhereDetalle & " And Credito=0"
            WhereDetalleNCR = WhereDetalleNCR & " And Credito=0"
        End If

        If Not txtProducto.dtProductosSeleccionados Is Nothing Then
            If txtProducto.dtProductosSeleccionados.Rows.Count = 0 Then
                txtProducto.dtProductosSeleccionados.Reset()
            End If
        End If

        'Producto
        If chkProducto.Valor = True Then
            If txtProducto.Seleccionado = False And (txtProducto.dtProductosSeleccionados Is Nothing) Then
                Exit Sub
            End If
            If Not txtProducto.dtProductosSeleccionados Is Nothing Then

                If txtProducto.dtProductosSeleccionados.Rows.Count > 0 Then

                    Dim IDProductoIN As String = " IDProducto In (0"


                    For Each dRow As DataRow In txtProducto.dtProductosSeleccionados.Rows
                        IDProductoIN = IDProductoIN & ", " & dRow("ID")
                    Next
                    IDProductoIN = IDProductoIN & ")"

                    WhereDetalle = IIf(String.IsNullOrEmpty(WhereDetalle), " where " & IDProductoIN, WhereDetalle & "And " & IDProductoIN)
                    WhereDetalleNCR = IIf(String.IsNullOrEmpty(WhereDetalleNCR), " where " & IDProductoIN, WhereDetalleNCR & "And " & IDProductoIN)
                    '=> SC:21-05-2022 nuevo
                    WhereNCR = IIf(String.IsNullOrEmpty(WhereNCR), " where " & IDProductoIN, WhereNCR & "And " & IDProductoIN)

                    GoTo Seguir

                End If

            End If

            WhereDetalle = WhereDetalle & " And IDProducto=" & txtProducto.Registro("ID") & " "
            WhereDetalleNCR = WhereDetalleNCR & " And IDProducto=" & txtProducto.Registro("ID") & " "
            '=> SC:21-05-2022 nuevo
            WhereNCR = WhereNCR & " And IDProducto=" & txtProducto.Registro("ID") & " "
        End If
Seguir:

        'Cliente
        If chkCliente.Valor = True Then
            If txtCliente.Seleccionado = False Then
                Exit Sub
            End If

            WhereDetalle = WhereDetalle & " And IDCliente=" & txtCliente.Registro("ID") & " "
            WhereDetalleNCR = WhereDetalleNCR & " And IDCliente=" & txtCliente.Registro("ID") & " "
            '=> SC:21-05-2022 nuevo
            WhereNCR = WhereNCR & " And IDCliente=" & txtCliente.Registro("ID") & " "
        End If

        Dim vCancelarAutomatico As String = ""
        If chkCancelacionAutomatica.Valor = False Then
            vCancelarAutomatico = vCancelarAutomatico & " and CancelarAutomatico = 0"
        End If
        'Moneda
        WhereDetalle = WhereDetalle & " And IdMoneda=" & cbxMoneda.GetValue
        WhereDetalleNCR = WhereDetalleNCR & " And IdMoneda=" & cbxMoneda.GetValue
        '=> SC:21-05-2022 nuevo
        WhereNCR = WhereNCR & " And IdMoneda=" & cbxMoneda.GetValue
        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            Select Case cbxTipoReporte.cbx.SelectedIndex
                Case 0
                    OrderBy = "Order by TipoProducto, SubLinea, Linea, Producto"
                Case 1
                    OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
                Case 2
                    OrderBy = " Order By TipoProducto"
                Case 3
                    OrderBy = " Order By Producto"
                Case 4
                    OrderBy = " Order By Producto"
                Case 5
                    OrderBy = "Order by TipoProducto, SubLinea, Linea, Producto"
            End Select
        End If

        If cbxEnForma.cbx.Text <> "" And cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        'Incluir Sin Control
        If chkIncluirProductosSinControl.Valor = False Then
            WhereDetalle = WhereDetalle & " and ControlarExistencia = 'True' "
            WhereDetalleNCR = WhereDetalleNCR & " and ControlarExistencia = 'True' "
        End If

        'Incluir Anulados
        If chkIncluirAnulados.Valor = False Then
            WhereDetalle = WhereDetalle & " and Anulado = 'False' "
            WhereDetalleNCR = WhereDetalleNCR & " and Anulado = 'False' "
            '=> SC:21-05-2022 nuevo
            WhereNCR = WhereNCR & " and Anulado = 'False' "
        End If

        CReporte.ArmarSubTitulo(SubTitulo, gbxFiltro, nudRanking, cbxVentas, cbxMoneda, cbxOrdenadoPor, cbxEnForma, txtDesde, txtHasta)

        EstablecerCondicionDetalleVenta(WhereDetalle)
        EstablecerCondicionDetalleVenta(WhereDetalleNCR)
        '=> SC:21-05-2022 nuevo
        EstablecerCondicionDetalleVenta(WhereNCR)

        'Cliente
        If chkCliente.Valor = True And cbxTipoReporte.cbx.SelectedIndex = 1 Then
            If txtCliente.Seleccionado = False Then
                Exit Sub
            End If

            WhereDetalle = WhereDetalle & " And IDCliente=" & txtCliente.Registro("ID") & " "
            WhereDetalleNCR = WhereDetalleNCR & " And IDCliente=" & txtCliente.Registro("ID") & " "
            '=> SC:21-05-2022 nuevo
            WhereNCR = WhereNCR & " And IDCliente=" & txtCliente.Registro("ID") & " "

        End If


        If chkSucursalCliente.Valor = True Then
            WhereDetalle = WhereDetalle & " and IDSucursalFiltro = " & cbxSucursalCliente.GetValue
        End If

        Select Case cbxTipoReporte.cbx.SelectedIndex
            Case 0
                WhereDetalle = WhereDetalle & vCancelarAutomatico
                'CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, "LISTADO DE VENTAS TOTALES POR PRODUCTO", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptVentasTotalesProducto", False, WhereDetalleNCR)
                '=> SC:21-05-2022 nuevo
                CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, "LISTADO DE VENTAS TOTALES POR PRODUCTO", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptVentasTotalesProducto", False, WhereDetalleNCR, WhereNCR)

            Case 1
                WhereDetalle = WhereDetalle & vCancelarAutomatico
                'CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, "LISTADO DE VENTAS TOTALES POR PRODUCTO / CLIENTE", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptListadoVentasProductoCliente", False, WhereDetalleNCR)
                '=> SC:21-05-2022 nuevo
                CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, "LISTADO DE VENTAS TOTALES POR PRODUCTO / CLIENTE", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptListadoVentasProductoCliente", False, WhereDetalleNCR, WhereNCR)

            Case 2
                WhereDetalle = WhereDetalle & vCancelarAutomatico
                'CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, "LISTADO DE VENTAS TOTALES POR " & vgConfiguraciones("ProductoClasificacion1") & "", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptListadoVentasTotalesTipoProducto", False, WhereDetalleNCR)
                '=> SC:21-05-2022 nuevo
                CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, "LISTADO DE VENTAS TOTALES POR " & vgConfiguraciones("ProductoClasificacion1") & "", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptListadoVentasTotalesTipoProducto", False, WhereDetalleNCR, WhereNCR)

            Case 3
                CReporte.PlanillaVentaDiaria(frm, WhereDetalle & " And CancelarAutomatico = 0", "PLANILLA DE VENTA DIARIA", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptPlanillaVentaDiaria", False, WhereDetalleNCR)
            Case 4
                CReporte.PlanillaVentaDiaria(frm, WhereDetalle & " And CancelarAutomatico = 1", "BONIFICACION-DONACION-MUESTRA", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptPlanillaVentaDiariaCA", False, WhereDetalleNCR)
            Case 5
                WhereDetalle = WhereDetalle & vCancelarAutomatico
                'CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, "LISTADO DE VENTAS TOTALES POR PRODUCTO", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptVentasPromedio", False, WhereDetalleNCR)
                '=> SC:21-05-2022 nuevo
                CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, "LISTADO DE VENTAS TOTALES POR PRODUCTO", SubTitulo, OrderBy, Top, chkDevoluciones.Valor, "rptVentasPromedio", False, WhereDetalleNCR, WhereNCR)

        End Select


    End Sub

    Sub EstablecerCondicionVenta(ByRef Where As String)

        'Tipo de Comprobante
        cbxTipoComprobante.EstablecerCondicion(Where, chkExcluirTipoComprobante.Valor)

        'Vendedor
        cbxVendedor.EstablecerCondicion(Where, chkExcluirVendedor.Valor)

        'Zona de Venta
        cbxZonaVenta.EstablecerCondicion(Where, chkExcluirZonaVenta.Valor)

        'Lista de Precio
        cbxListaPrecio.EstablecerCondicion(Where, chkExcluirListaPrecios.Valor)

        'Tipo Cliente
        cbxTipoCliente.EstablecerCondicion(Where, chkExcluirTipoCliente.Valor)

        'Ciudad
        cbxCiudad.EstablecerCondicion(Where, chkExcluirCiudad.Valor)

        'Sucursal
        cbxSucursal.EstablecerCondicion(Where, chkExcluirSucursal.Valor)

    End Sub

    Sub EstablecerCondicionDetalleVenta(ByRef Where As String)

        'Tipo de Comprobante
        cbxTipoComprobante.EstablecerCondicion(Where, chkExcluirTipoComprobante.Valor)

        'Vendedor
        cbxVendedor.EstablecerCondicion(Where, chkExcluirVendedor.Valor)

        'Zona de Venta
        cbxZonaVenta.EstablecerCondicion(Where, chkExcluirZonaVenta.Valor)

        'Lista de Precio
        cbxListaPrecio.EstablecerCondicion(Where, chkExcluirListaPrecios.Valor)

        'Tipo Cliente
        cbxTipoCliente.EstablecerCondicion(Where, chkExcluirTipoCliente.Valor)

        'Proveedor
        cbxProveedor.EstablecerCondicion(Where, chkExcluirProveedor.Valor)

        'Ciudad
        cbxCiudad.EstablecerCondicion(Where, chkExcluirCiudad.Valor)

        'Sucursal
        cbxSucursal.EstablecerCondicion(Where, chkExcluirSucursal.Valor)

        'Tipo de Producto
        cbxTipoProducto.EstablecerCondicion(Where, chkExcluirTipoProducto.Valor)

        'Linea
        cbxLinea.EstablecerCondicion(Where, chkExcluirLinea.Valor)

        'Marca
        cbxMarca.EstablecerCondicion(Where, chkExcluirMarca.Valor)

        'SubLinea
        cbxSubLinea.EstablecerCondicion(Where, chkExcluirSubLinea.Valor)

        'SubLinea2
        cbxSubLinea2.EstablecerCondicion(Where, chkExcluirSubLinea2.Valor)

        'Presentacion
        cbxPresentacion.EstablecerCondicion(Where, chkExcluirPresentacion.Valor)

        'Deposito
        cbxDeposito.EstablecerCondicion(Where, chkExcluirDeposito.Valor)

    End Sub

    Sub CargarInformacion()

        'Cliente
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
        txtCliente.frm = Me

        'Producto
        txtProducto.ControlarExistencia = False
        txtProducto.Conectar(True, False)

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Reporte
        cbxTipoReporte.cbx.Items.Add("Ventas Totales por Producto")
        cbxTipoReporte.cbx.Items.Add("Ventas Totales Producto/Cliente")
        cbxTipoReporte.cbx.Items.Add("Ventas Totales por " & vgConfiguraciones("ProductoClasificacion1") & "")
        cbxTipoReporte.cbx.Items.Add("Planilla de venta diaria")
        cbxTipoReporte.cbx.Items.Add("Venta - Cancelacion Automatica")
        cbxTipoReporte.cbx.Items.Add("Ventas - Precio Promedio")
        cbxTipoReporte.cbx.SelectedIndex = 3

        'Ventas
        cbxVentas.cbx.Items.Add("Contado + Crédito")
        cbxVentas.cbx.Items.Add("Contado")
        cbxVentas.cbx.Items.Add("Crédito")
        cbxVentas.cbx.SelectedIndex = 0

        'Tipo de Venta
        cbxVentas.cbx.SelectedIndex = 0

        'Configuracion
        cbxTipoReporte.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "REPORTE", "")
        cbxOrdenadoPor.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENADO", "")
        cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")
        cbxEnForma.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ENFORMA", "")
        nudRanking.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "RANKING", "")

        cbxMoneda.cbx.SelectedIndex = 0

        'Habilitamos Seleccion multiple de todos los filtros ocxcbx
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

        txtProducto.SeleccionMultiple = True
        txtProducto.Conectar()


    End Sub

    Sub Ordenacion()
        cbxOrdenadoPor.cbx.Items.Clear()
        Select Case cbxTipoReporte.cbx.SelectedIndex
            Case 0
                cbxOrdenadoPor.cbx.Items.Add(vgConfiguraciones("ProductoClasificacion1"))
                cbxOrdenadoPor.cbx.SelectedIndex = 0
            Case 1
                cbxOrdenadoPor.cbx.Items.Add("Cliente")
                cbxOrdenadoPor.cbx.Items.Add("Producto")
                cbxOrdenadoPor.cbx.SelectedIndex = 0
            Case 2
                cbxOrdenadoPor.cbx.Items.Add(vgConfiguraciones("ProductoClasificacion1"))
                cbxOrdenadoPor.cbx.SelectedIndex = 0
        End Select

    End Sub

    Sub GuardarInformacion()

        'Configuracion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "REPORTE", cbxTipoReporte.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENADO", cbxOrdenadoPor.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ENFORMA", cbxEnForma.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RANKING", nudRanking.Text)

    End Sub

    Private Sub frmListadoVentasPorProducto_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
        'FA 20/06/2023
        Me.Dispose()
    End Sub

    Private Sub frmListadoVentasPorProductoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoVentasporProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
        chkExcluirTipoComprobante.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
        chkExcluirVendedor.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
        chkExcluirCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
        chkExcluirSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
        chkExcluirDeposito.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
        chkExcluirZonaVenta.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
        chkExcluirListaPrecios.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub ckLinea_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
        chkExcluirLinea.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
        chkExcluirTipoCliente.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
        chkExcluirProveedor.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
        chkExcluirTipoProducto.Enabled = value
        cbxTipoProducto.cbx.Text = ""
    End Sub

    Private Sub chkMarca_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
        chkExcluirMarca.Enabled = value
    End Sub

    Private Sub chkSubLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSubLinea.PropertyChanged
        cbxSubLinea.Enabled = value
        chkExcluirSubLinea.Enabled = value
    End Sub

    Private Sub chkSubLinea2_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSubLinea2.PropertyChanged
        cbxSubLinea2.Enabled = value
        chkExcluirSubLinea2.Enabled = value
    End Sub

    Private Sub chkPresentacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkPresentacion.PropertyChanged
        cbxPresentacion.Enabled = value
        chkExcluirPresentacion.Enabled = value
    End Sub

    Private Sub cbxTipoReporte_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbxTipoReporte.PropertyChanged
        Ordenacion()

    End Sub

    Private Sub chkSucursalCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursalCliente.PropertyChanged
        cbxSucursalCliente.Enabled = value
    End Sub

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoVentaAnulada
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkMotivo = New ERP.ocxCHK()
        Me.cbxMotivo = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkTarjeta = New ERP.ocxCHK()
        Me.chkBonifiacion = New ERP.ocxCHK()
        Me.chkMuestra = New ERP.ocxCHK()
        Me.chkDonacion = New ERP.ocxCHK()
        Me.chkDiferido = New ERP.ocxCHK()
        Me.chkCredito = New ERP.ocxCHK()
        Me.chkContado = New ERP.ocxCHK()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxDocumento = New ERP.ocxCBX()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(595, 331)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(7, 130)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 7
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(13, 110)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 6
        Me.lblRanking.Text = "Ranking:"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(13, 69)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkMotivo)
        Me.gbxFiltro.Controls.Add(Me.cbxMotivo)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Location = New System.Drawing.Point(8, 1)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(399, 293)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkMotivo
        '
        Me.chkMotivo.BackColor = System.Drawing.Color.Transparent
        Me.chkMotivo.Color = System.Drawing.Color.Empty
        Me.chkMotivo.Location = New System.Drawing.Point(10, 144)
        Me.chkMotivo.Name = "chkMotivo"
        Me.chkMotivo.Size = New System.Drawing.Size(71, 21)
        Me.chkMotivo.SoloLectura = False
        Me.chkMotivo.TabIndex = 8
        Me.chkMotivo.Texto = "Motivo:"
        Me.chkMotivo.Valor = False
        '
        'cbxMotivo
        '
        Me.cbxMotivo.CampoWhere = "IDMotivo"
        Me.cbxMotivo.CargarUnaSolaVez = False
        Me.cbxMotivo.DataDisplayMember = "Descripcion"
        Me.cbxMotivo.DataFilter = Nothing
        Me.cbxMotivo.DataOrderBy = Nothing
        Me.cbxMotivo.DataSource = "MotivoAnulacionVenta"
        Me.cbxMotivo.DataValueMember = "ID"
        Me.cbxMotivo.dtSeleccionado = Nothing
        Me.cbxMotivo.Enabled = False
        Me.cbxMotivo.FormABM = Nothing
        Me.cbxMotivo.Indicaciones = Nothing
        Me.cbxMotivo.Location = New System.Drawing.Point(130, 144)
        Me.cbxMotivo.Name = "cbxMotivo"
        Me.cbxMotivo.SeleccionMultiple = False
        Me.cbxMotivo.SeleccionObligatoria = False
        Me.cbxMotivo.Size = New System.Drawing.Size(260, 21)
        Me.cbxMotivo.SoloLectura = False
        Me.cbxMotivo.TabIndex = 9
        Me.cbxMotivo.Texto = "CERRADO"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(10, 185)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(71, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 10
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 100
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(10, 213)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(383, 56)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 11
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(10, 102)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(130, 102)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(260, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(10, 81)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(114, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 4
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudadCliente"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Descripcion"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(130, 81)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(260, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 5
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(9, 40)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(114, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 2
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(9, 19)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 0
        Me.chkTipoComprobante.Texto = "Tipo Comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = "FormName='frmVenta'"
        Me.cbxTipoComprobante.DataOrderBy = "Descripcion"
        Me.cbxTipoComprobante.DataSource = "VTipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(129, 19)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(260, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = "BOLETA DE VENTAS"
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = "frmVendedor"
        Me.cbxVendedor.Indicaciones = "Seleccion de Vendedor"
        Me.cbxVendedor.Location = New System.Drawing.Point(129, 40)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(260, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 3
        Me.cbxVendedor.Texto = "ALDO GONZALES"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(414, 1)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 271)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkTarjeta)
        Me.GroupBox1.Controls.Add(Me.chkBonifiacion)
        Me.GroupBox1.Controls.Add(Me.chkMuestra)
        Me.GroupBox1.Controls.Add(Me.chkDonacion)
        Me.GroupBox1.Controls.Add(Me.chkDiferido)
        Me.GroupBox1.Controls.Add(Me.chkCredito)
        Me.GroupBox1.Controls.Add(Me.chkContado)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 160)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(222, 101)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Formas de Pago"
        '
        'chkTarjeta
        '
        Me.chkTarjeta.BackColor = System.Drawing.Color.Transparent
        Me.chkTarjeta.Color = System.Drawing.Color.Empty
        Me.chkTarjeta.Location = New System.Drawing.Point(9, 80)
        Me.chkTarjeta.Margin = New System.Windows.Forms.Padding(4)
        Me.chkTarjeta.Name = "chkTarjeta"
        Me.chkTarjeta.Size = New System.Drawing.Size(182, 21)
        Me.chkTarjeta.SoloLectura = False
        Me.chkTarjeta.TabIndex = 29
        Me.chkTarjeta.Texto = "TARJETA DE CREDITO/DEBITO"
        Me.chkTarjeta.Valor = False
        '
        'chkBonifiacion
        '
        Me.chkBonifiacion.BackColor = System.Drawing.Color.Transparent
        Me.chkBonifiacion.Color = System.Drawing.Color.Empty
        Me.chkBonifiacion.Location = New System.Drawing.Point(101, 59)
        Me.chkBonifiacion.Name = "chkBonifiacion"
        Me.chkBonifiacion.Size = New System.Drawing.Size(109, 21)
        Me.chkBonifiacion.SoloLectura = False
        Me.chkBonifiacion.TabIndex = 27
        Me.chkBonifiacion.Texto = "BONIFICACION"
        Me.chkBonifiacion.Valor = False
        '
        'chkMuestra
        '
        Me.chkMuestra.BackColor = System.Drawing.Color.Transparent
        Me.chkMuestra.Color = System.Drawing.Color.Empty
        Me.chkMuestra.Location = New System.Drawing.Point(101, 38)
        Me.chkMuestra.Name = "chkMuestra"
        Me.chkMuestra.Size = New System.Drawing.Size(85, 21)
        Me.chkMuestra.SoloLectura = False
        Me.chkMuestra.TabIndex = 26
        Me.chkMuestra.Texto = "MUESTRA"
        Me.chkMuestra.Valor = False
        '
        'chkDonacion
        '
        Me.chkDonacion.BackColor = System.Drawing.Color.Transparent
        Me.chkDonacion.Color = System.Drawing.Color.Empty
        Me.chkDonacion.Location = New System.Drawing.Point(101, 18)
        Me.chkDonacion.Name = "chkDonacion"
        Me.chkDonacion.Size = New System.Drawing.Size(109, 21)
        Me.chkDonacion.SoloLectura = False
        Me.chkDonacion.TabIndex = 25
        Me.chkDonacion.Texto = "DONACION"
        Me.chkDonacion.Valor = False
        '
        'chkDiferido
        '
        Me.chkDiferido.BackColor = System.Drawing.Color.Transparent
        Me.chkDiferido.Color = System.Drawing.Color.Empty
        Me.chkDiferido.Location = New System.Drawing.Point(9, 60)
        Me.chkDiferido.Name = "chkDiferido"
        Me.chkDiferido.Size = New System.Drawing.Size(71, 21)
        Me.chkDiferido.SoloLectura = False
        Me.chkDiferido.TabIndex = 24
        Me.chkDiferido.Texto = "DIFERIDO"
        Me.chkDiferido.Valor = False
        '
        'chkCredito
        '
        Me.chkCredito.BackColor = System.Drawing.Color.Transparent
        Me.chkCredito.Color = System.Drawing.Color.Empty
        Me.chkCredito.Location = New System.Drawing.Point(9, 39)
        Me.chkCredito.Name = "chkCredito"
        Me.chkCredito.Size = New System.Drawing.Size(71, 21)
        Me.chkCredito.SoloLectura = False
        Me.chkCredito.TabIndex = 23
        Me.chkCredito.Texto = "CREDITO"
        Me.chkCredito.Valor = False
        '
        'chkContado
        '
        Me.chkContado.BackColor = System.Drawing.Color.Transparent
        Me.chkContado.Color = System.Drawing.Color.Empty
        Me.chkContado.Location = New System.Drawing.Point(9, 19)
        Me.chkContado.Name = "chkContado"
        Me.chkContado.Size = New System.Drawing.Size(71, 21)
        Me.chkContado.SoloLectura = False
        Me.chkContado.TabIndex = 22
        Me.chkContado.Texto = "CONTADO"
        Me.chkContado.Valor = False
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 85)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 85)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 2, 14, 9, 22, 0, 114)
        Me.txtHasta.Location = New System.Drawing.Point(89, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 14, 9, 22, 0, 114)
        Me.txtDesde.Location = New System.Drawing.Point(6, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(434, 331)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(424, 279)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Condición:"
        '
        'cbxDocumento
        '
        Me.cbxDocumento.CampoWhere = Nothing
        Me.cbxDocumento.CargarUnaSolaVez = False
        Me.cbxDocumento.DataDisplayMember = Nothing
        Me.cbxDocumento.DataFilter = Nothing
        Me.cbxDocumento.DataOrderBy = Nothing
        Me.cbxDocumento.DataSource = Nothing
        Me.cbxDocumento.DataValueMember = Nothing
        Me.cbxDocumento.dtSeleccionado = Nothing
        Me.cbxDocumento.FormABM = Nothing
        Me.cbxDocumento.Indicaciones = Nothing
        Me.cbxDocumento.Location = New System.Drawing.Point(423, 295)
        Me.cbxDocumento.Margin = New System.Windows.Forms.Padding(4)
        Me.cbxDocumento.Name = "cbxDocumento"
        Me.cbxDocumento.SeleccionMultiple = False
        Me.cbxDocumento.SeleccionObligatoria = False
        Me.cbxDocumento.Size = New System.Drawing.Size(231, 21)
        Me.cbxDocumento.SoloLectura = False
        Me.cbxDocumento.TabIndex = 9
        Me.cbxDocumento.Texto = ""
        '
        'frmListadoVentaAnulada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(665, 384)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxDocumento)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoVentaAnulada"
        Me.Text = "frmListadoVentaAnulada"
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkMotivo As ERP.ocxCHK
    Friend WithEvents cbxMotivo As ERP.ocxCBX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkBonifiacion As ERP.ocxCHK
    Friend WithEvents chkMuestra As ERP.ocxCHK
    Friend WithEvents chkDonacion As ERP.ocxCHK
    Friend WithEvents chkDiferido As ERP.ocxCHK
    Friend WithEvents chkCredito As ERP.ocxCHK
    Friend WithEvents chkContado As ERP.ocxCHK
    Friend WithEvents chkTarjeta As ERP.ocxCHK
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxDocumento As ocxCBX
End Class

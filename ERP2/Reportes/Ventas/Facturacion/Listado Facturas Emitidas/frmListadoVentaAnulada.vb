﻿Imports ERP.Reporte

Public Class frmListadoVentaAnulada


    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim FormaPago As String

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    'VARIABLES
    Dim Titulo As String = "COMPROBANTES ANULADOS"
    Dim TipoInforme As String

    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()

        CambiarOrdenacion()

        txtCliente.Conectar()

        'Fechas
        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()
        FormaPago = ""
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
    End Sub

    Sub Listar()
        FormaPago = ""
        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        TipoInforme = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Anulado ='True' And Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"

        If chkCliente.Valor = True Then
            Where = Where & " And IDCliente = " & txtCliente.Registro("ID").ToString
        End If

        CargaFormaPago()
        'Filtrar por Forma de pago
        If FormaPago <> "" Then
            FormaPago = " and (IDFormaPagoFactura = 0" & FormaPago & ")"
            Where = Where & FormaPago
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        Select Case cbxDocumento.cbx.SelectedIndex

            Case 0
                Where = Where & ""
            Case 1
                Where = Where & " And " & " (Condicion = 'CONT')"
            Case 2
                Where = Where & " And " & " (Condicion = 'CRED')"
        End Select

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)

        CReporte.ListadoVentasAnuladas(frm, Where, Titulo, vgUsuarioIdentificador, OrderBy, Top, TipoInforme)

    End Sub
    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Contado - Credito
        cbxDocumento.cbx.Items.Add("CONTADO + CREDITO")
        cbxDocumento.cbx.Items.Add("CONTADO")
        cbxDocumento.cbx.Items.Add("CREDITO")
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.SelectedIndex = 0

    End Sub
    Sub CambiarOrdenacion()

        cbxEnForma.cbx.Text = ""
        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VVenta", "Select Top(0) Comprobante, Cliente, Fecha, Vendedor, Sucursal, Deposito, Total, MotivoAnulacion From VVenta ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmListadoVentaAnulada_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoVentasAnualesTotalesCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkMotivo_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkMotivo.PropertyChanged
        cbxMotivo.Enabled = value
    End Sub
    Private Sub CargaFormaPago()
        If chkContado.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 1"
        End If
        If chkCredito.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 2"
        End If
        If chkDiferido.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 3"
        End If
        If chkDonacion.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 4"
        End If
        If chkMuestra.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 5"
        End If
        If chkBonifiacion.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 6"
        End If
        If chkTarjeta.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 9"
        End If
    End Sub
End Class
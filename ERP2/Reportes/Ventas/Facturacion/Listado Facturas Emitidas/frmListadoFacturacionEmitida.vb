﻿Imports ERP.Reporte

Public Class frmListadoFacturacionEmitida

    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim FormaPago As String
    Dim Vista As String
    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    'VARIABLES
    Dim Titulo As String = "LISTADO DE FACTURAS EMITIDAS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        txtCliente.Conectar()
        'Fechas
        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()
        FormaPago = ""
        cbxTipoInforme.cbx.SelectedIndex = 1
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO CON NC")
        cbxTipoInforme.cbx.Items.Add("RESUMIDO A GUARANIES")
        cbxTipoInforme.cbx.Items.Add("RESUMIDO POR SUCURSAL")
        cbxTipoInforme.cbx.SelectedIndex = 0

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Contado - Credito
        cbxDocumento.cbx.Items.Add("CONTADO + CREDITO")
        cbxDocumento.cbx.Items.Add("CONTADO")
        cbxDocumento.cbx.Items.Add("CREDITO")
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxDocumento.cbx.SelectedIndex = 0

        'Moneda
        cbxMoneda.cbx.Items.Add("Todos")
        cbxMoneda.cbx.Items.Add("Guaranies")
        cbxMoneda.cbx.Items.Add("Dolares")
        cbxMoneda.cbx.SelectedIndex = 0

        'Descuento
        cbxDescuento.cbx.Items.Add("Todos")
        cbxDescuento.cbx.Items.Add("Sin Descuento")
        cbxDescuento.cbx.Items.Add("Con Descuento")
        cbxDescuento.cbx.SelectedIndex = 0

        'Seleccion Multiple
        cbxVendedor.SeleccionMultiple = True
        cbxTipoProducto.SeleccionMultiple = True

        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

        cbxTipoProducto.SeleccionMultiple = True

    End Sub

    Sub Listar()

        Dim SubTitulo As String = ""
        Dim Where As String = ""
        Dim TipoProductoCabecera = ""
        Dim TipoProductoDetalle = ""
        'SC: 11/06/2022 - Linea comentada
        Dim WhereDetalle As String = "and 1 =1 "
        'Dim WhereDetalle As String = ""
        Dim WhereNC As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        FormaPago = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where (Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "') "
        'WhereNC = " Where VVenta.Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        If chkCliente.chk.Checked = True Then

            If txtCliente.txtID.txt.Text = "" Then
                Dim mensaje As String = "Ingrese primero algún cliente!"
                ctrError.SetError(txtCliente, mensaje)
                ctrError.SetIconAlignment(txtCliente, ErrorIconAlignment.TopRight)
                tsslEstado.Text = mensaje
                Exit Sub
            Else
                Where = Where & " And (IDCliente = " & txtCliente.Registro("ID").ToString & ") "
                ' WhereNC = WhereNC & " And (VVenta.IDCliente = " & txtCliente.Registro("ID").ToString
            End If

        End If

        CargaFormaPago()

        'Filtrar por Forma de pago
        If FormaPago <> "" Then
            FormaPago = " and (IDFormaPagoFactura = 0" & FormaPago & ")"
            Where = Where & FormaPago
            'WhereNC = WhereNC & FormaPago
        End If

        If cbxTipoInforme.txt.Text = "RESUMIDO POR SUCURSAL" Then
            TipoInforme = "RESUMIDO POR SUCURSAL"
        Else
            Where = Where
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.name = "cbxUsuario" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Filtrar por tipo de producto
        If cbxTipoProducto.Texto <> "" And chkTipoProducto.Valor Then
            'TipoProductoCabecera = " and " & cbxTipoProducto.cbx.SelectedValue & " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = vventa.IdTransaccion) "
            'TipoProductoDetalle = " and IDTransaccion in (Select IDTransaccion From VVenta Where " +
            '                      " Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' " +
            '                      " and " + cbxTipoProducto.cbx.SelectedValue.ToString() + " in (Select idTipoProducto from VdetalleVenta DV  where  DV.IdTRansaccion = VVenta.IdTransaccion))"
            'SC: 11/06/2022 - Las 2 lineas siguientes se agrega
            '09032023
            TipoProductoCabecera = " and IDTipoProducto = " & cbxTipoProducto.cbx.SelectedValue
            TipoProductoDetalle = " and IDTipoProducto = " & cbxTipoProducto.cbx.SelectedValue

            

        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            If cbxOrdenadoPor.cbx.Text = "Fecha" Then
                OrderBy = " Order By FechaEmision"
            End If
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text

            If cbxEnForma.cbx.Text <> "" Then
                OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
            End If
        End If

        

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        Select Case cbxDocumento.cbx.SelectedIndex

            Case 0
                Where = Where & ""
            Case 1
                Where = Where & " And " & " (Condicion = 'CONT')"
            Case 2
                Where = Where & " And " & " (Condicion = 'CRED')"
        End Select

        Select Case cbxMoneda.cbx.SelectedIndex

            Case 0
                Where = Where & ""
            Case 1
                Where = Where & " And " & " (IDMoneda = 1)"
            Case 2
                Where = Where & " And " & " (IDMoneda = 2)"
        End Select

        Select Case cbxDescuento.cbx.SelectedIndex

            Case 0
                WhereDetalle = WhereDetalle & ""
            Case 1
                WhereDetalle = WhereDetalle & " And " & " ([Desc Uni] = 0)"
            Case 2
                WhereDetalle = WhereDetalle & " And " & " ([Desc Uni] > 0)"
        End Select

        If chkAnulados.Valor = False Then
            Where = Where & " And (Anulado = 'False') "
        Else
            If rdbSoloAnulados.Checked Then
                Where = Where & " And (Anulado = 'True') "
            End If
        End If

        CReporte.ArmarSubTitulo(SubTitulo, gbxFiltro, txtDesde, txtHasta)
        'SC: 11/06/2022 - Para eventual Subtitulo de Cliente que no se incluye en la linea anterior
        If chkCliente.chk.Checked = "True" Then
            SubTitulo = SubTitulo & " - " & txtCliente.txtRazonSocial.GetValue
        End If

        Dim vUsuario As String = ""
        If chkUsuario.Valor = True Then
            vUsuario = " and (IDUsuario = " & cbxUsuario.GetValue & ")"
        End If

        Select Case cbxTipoInforme.cbx.SelectedIndex

            Case 0
                'SC: 11/05/2022 - Seleccionando TipoProducto o Producto y algunos de unos Check
                If (cbxProducto.Texto <> "" Or cbxTipoProducto.Texto <> "") And (chkProducto.Valor Or chkTipoProducto.Valor) Then
                    cbxProducto.EstablecerCondicion2(Where)
                    WhereDetalle = Where + WhereDetalle
                    'SC: 11/06/2022 - Se agrega nuevo parametro de vista programada de acuerdo al filtro tipoproducto e idproducto
                    CReporte.ListadoFacturasEmitidasProducto(frm, Where + TipoProductoCabecera, WhereDetalle + TipoProductoDetalle, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, vUsuario, Vista)
                Else
                    CReporte.ListadoFacturasEmitidas(frm, Where + TipoProductoCabecera, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, vUsuario)
                End If
            Case 1
                cbxProducto.EstablecerCondicion2(WhereDetalle)
                WhereDetalle = Where + WhereDetalle
                'CReporte.ListadoFacturasEmitidasDetalle(frm, Where, WhereDetalle + TipoProductoDetalle, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, vUsuario)
                CReporte.ListadoFacturasEmitidasDetalle(frm, Where, WhereDetalle, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, vUsuario)


            Case 2
                cbxProducto.EstablecerCondicion2(WhereDetalle)
                WhereNC = Where.Replace("(", "(VVenta.")
                WhereDetalle = Where + WhereDetalle
                CReporte.ListadoFacturasEmitidasDetalleNotaCredito(frm, Where, WhereDetalle + TipoProductoDetalle, WhereNC, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, vUsuario)

            Case 3
                CReporte.ListadoFacturasEmitidasAGuaranies(frm, Where, Titulo, SubTitulo, vgUsuarioIdentificador, OrderBy, Top, vUsuario)
            Case 4
                CReporte.ListadoFacturasEmitidasPorSucursal(frm, Where + TipoProductoCabecera, Titulo, TipoInforme & " - " & SubTitulo, vgUsuarioIdentificador, OrderBy, Top, vUsuario)
        End Select



    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        Select Case cbxTipoInforme.cbx.SelectedIndex

            Case 0
                dt = CData.GetStructure("VVenta", "Select Top(0) Comprobante, Cliente, Fecha, Vendedor, Sucursal, Deposito, Total From VVenta ")
            Case 1
                dt = CData.GetStructure("VVenta", "Select Top(0) Comprobante, Cliente, Fecha, Vendedor, Sucursal, Deposito, Total From VVenta ")
        End Select

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next
        cbxOrdenadoPor.cbx.SelectedIndex = 0
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = chkTipoComprobante.Valor
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = chkVendedor.Valor
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        CambiarOrdenacion()
    End Sub

    Private Sub chkDistribuidor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDistribuidor.PropertyChanged
        cbxDistribuidor.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProducto.PropertyChanged
        cbxProducto.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As Object, e As System.EventArgs, value As Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
        cbxTipoProducto.cbx.Text = ""
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Private Sub CargaFormaPago()
        If chkEfectivo.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 1"
        End If
        If chkChqAlDia.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 6"
        End If
        If chChqDiferido.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 2"
        End If
        If chkDonacion.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 3"
        End If
        If chkMuestra.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 5"
        End If
        If chkBonifiacion.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 4"
        End If
        If chkTarjeta.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 9"
        End If
    End Sub

    Private Sub chkUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkCategoriaCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCategoriaCliente.PropertyChanged
        cbxCategoriaCliente.Enabled = value
    End Sub

    Private Sub chkAnulados_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkAnulados.PropertyChanged
        rdbIncluir.Enabled = value
        rdbSoloAnulados.Enabled = value
    End Sub

    'SC: 11/06/2022 - Se agrega para evaluar si usa filtro Informe Resumido y TipoProducto, deshabilita Producto y usa Vista para el efecto
    Private Sub chkTipoProducto_Leave(sender As Object, e As EventArgs) Handles chkTipoProducto.Leave
        If cbxTipoInforme.cbx.SelectedIndex = 0 Then
            If chkTipoProducto.chk.Checked = "True" Then
                chkProducto.chk.Enabled = "False"
                cbxProducto.cbx.Enabled = "False"
                Vista = "VVentayDetalle"
            Else
                chkProducto.chk.Enabled = "True"
                cbxProducto.cbx.Enabled = "True"
            End If
        End If
    End Sub

    'SC: 11/06/2022 - Se agrega para evaluar si usa filtro Informe Resumido y Producto, deshabilita TipoProducto y usa Vista para el efecto
    Private Sub chkProducto_Leave(sender As Object, e As EventArgs) Handles chkProducto.Leave
        If cbxTipoInforme.cbx.SelectedIndex = 0 Then
            If chkProducto.chk.Checked = "True" Then
                chkTipoProducto.chk.Enabled = "False"
                cbxTipoProducto.cbx.Enabled = "False"
                Vista = "VVentayDetalle1"
            Else
                chkTipoProducto.chk.Enabled = "True"
                cbxTipoProducto.cbx.Enabled = "True"
            End If
        End If
    End Sub
    'SC: 11/06/2022 - Se agrega para evaluar si usa filtro Informe Resumido, siendo asi setea usa Vista por defecto
    Private Sub cbxTipoInforme_Leave(sender As Object, e As EventArgs) Handles cbxTipoInforme.Leave
        If cbxTipoInforme.cbx.SelectedIndex = 0 Then
            Vista = "VVentayDetalle"
        End If
    End Sub

    Private Sub frmListadoFacturacionEmitida_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        LiberarMemoria()
    End Sub
End Class
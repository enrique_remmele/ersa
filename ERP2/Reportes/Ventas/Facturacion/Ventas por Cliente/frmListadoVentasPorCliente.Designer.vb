﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoVentasPorCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblReporte = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.chkExcluirCategoriaCliente = New ERP.ocxCHK()
        Me.chkCategoriaCliente = New ERP.ocxCHK()
        Me.cbxCategoriaCliente = New ERP.ocxCBX()
        Me.chkExcluirPresentacion = New ERP.ocxCHK()
        Me.chkExcluirSubLinea2 = New ERP.ocxCHK()
        Me.chkPresentacion = New ERP.ocxCHK()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.chkExcluirSubLinea = New ERP.ocxCHK()
        Me.chkSubLinea2 = New ERP.ocxCHK()
        Me.cbxSubLinea2 = New ERP.ocxCBX()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.chkSubLinea = New ERP.ocxCHK()
        Me.chkExcluirTipoCliente = New ERP.ocxCHK()
        Me.chkExcluirProveedor = New ERP.ocxCHK()
        Me.chkExcluirTipoProducto = New ERP.ocxCHK()
        Me.chkExcluirLinea = New ERP.ocxCHK()
        Me.chkExcluirMarca = New ERP.ocxCHK()
        Me.chkExcluirCiudad = New ERP.ocxCHK()
        Me.chkExcluirSucursal = New ERP.ocxCHK()
        Me.chkExcluirDeposito = New ERP.ocxCHK()
        Me.chkExcluirListaPrecios = New ERP.ocxCHK()
        Me.chkExcluirZonaVenta = New ERP.ocxCHK()
        Me.chkExcluirVendedor = New ERP.ocxCHK()
        Me.chkExcluirTipoComprobante = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkMarca = New ERP.ocxCHK()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkListaPrecio = New ERP.ocxCHK()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.chkZonaVenta = New ERP.ocxCHK()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkSucursalCliente = New ERP.ocxCHK()
        Me.chkDevoluciones = New ERP.ocxCHK()
        Me.chkIncluirAnulados = New ERP.ocxCHK()
        Me.chkIncluirProductosSinControl = New ERP.ocxCHK()
        Me.cbxTipoReporte = New ERP.ocxCBX()
        Me.cbxVentas = New ERP.ocxCBX()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(617, 457)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 1
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkExcluirCategoriaCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCategoriaCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxCategoriaCliente)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirPresentacion)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.chkPresentacion)
        Me.gbxFiltro.Controls.Add(Me.cbxPresentacion)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirLinea)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirMarca)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirListaPrecios)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirVendedor)
        Me.gbxFiltro.Controls.Add(Me.Label3)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMarca)
        Me.gbxFiltro.Controls.Add(Me.cbxMarca)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.cbxListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.chkZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.cbxZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(442, 503)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(404, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 12)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Excluir"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 268)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 13
        Me.lblRanking.Text = "Ranking:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkSucursalCliente)
        Me.GroupBox2.Controls.Add(Me.chkDevoluciones)
        Me.GroupBox2.Controls.Add(Me.chkIncluirAnulados)
        Me.GroupBox2.Controls.Add(Me.chkIncluirProductosSinControl)
        Me.GroupBox2.Controls.Add(Me.cbxTipoReporte)
        Me.GroupBox2.Controls.Add(Me.lblReporte)
        Me.GroupBox2.Controls.Add(Me.cbxVentas)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxMoneda)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(473, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(228, 429)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblReporte
        '
        Me.lblReporte.AutoSize = True
        Me.lblReporte.Location = New System.Drawing.Point(6, 65)
        Me.lblReporte.Name = "lblReporte"
        Me.lblReporte.Size = New System.Drawing.Size(48, 13)
        Me.lblReporte.TabIndex = 4
        Me.lblReporte.Text = "Reporte:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 111)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Ventas:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 156)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Moneda:"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 218)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 10
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(6, 284)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 14
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 1
        Me.lblPeriodo.Text = "Periodo:"
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(497, 457)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(98, 23)
        Me.btnInforme.TabIndex = 0
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'chkExcluirCategoriaCliente
        '
        Me.chkExcluirCategoriaCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirCategoriaCliente.Color = System.Drawing.Color.Empty
        Me.chkExcluirCategoriaCliente.Location = New System.Drawing.Point(411, 139)
        Me.chkExcluirCategoriaCliente.Name = "chkExcluirCategoriaCliente"
        Me.chkExcluirCategoriaCliente.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirCategoriaCliente.SoloLectura = False
        Me.chkExcluirCategoriaCliente.TabIndex = 17
        Me.chkExcluirCategoriaCliente.Texto = ""
        Me.chkExcluirCategoriaCliente.Valor = False
        '
        'chkCategoriaCliente
        '
        Me.chkCategoriaCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCategoriaCliente.Color = System.Drawing.Color.Empty
        Me.chkCategoriaCliente.Location = New System.Drawing.Point(7, 135)
        Me.chkCategoriaCliente.Name = "chkCategoriaCliente"
        Me.chkCategoriaCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCategoriaCliente.SoloLectura = False
        Me.chkCategoriaCliente.TabIndex = 15
        Me.chkCategoriaCliente.Texto = "Categoria:"
        Me.chkCategoriaCliente.Valor = False
        '
        'cbxCategoriaCliente
        '
        Me.cbxCategoriaCliente.CampoWhere = "IDCategoriaCliente"
        Me.cbxCategoriaCliente.CargarUnaSolaVez = False
        Me.cbxCategoriaCliente.DataDisplayMember = "Descripcion"
        Me.cbxCategoriaCliente.DataFilter = Nothing
        Me.cbxCategoriaCliente.DataOrderBy = "Descripcion"
        Me.cbxCategoriaCliente.DataSource = "VCategoriaCliente"
        Me.cbxCategoriaCliente.DataValueMember = "ID"
        Me.cbxCategoriaCliente.dtSeleccionado = Nothing
        Me.cbxCategoriaCliente.Enabled = False
        Me.cbxCategoriaCliente.FormABM = Nothing
        Me.cbxCategoriaCliente.Indicaciones = Nothing
        Me.cbxCategoriaCliente.Location = New System.Drawing.Point(127, 135)
        Me.cbxCategoriaCliente.Name = "cbxCategoriaCliente"
        Me.cbxCategoriaCliente.SeleccionMultiple = False
        Me.cbxCategoriaCliente.SeleccionObligatoria = False
        Me.cbxCategoriaCliente.Size = New System.Drawing.Size(278, 21)
        Me.cbxCategoriaCliente.SoloLectura = False
        Me.cbxCategoriaCliente.TabIndex = 16
        Me.cbxCategoriaCliente.Texto = ""
        '
        'chkExcluirPresentacion
        '
        Me.chkExcluirPresentacion.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirPresentacion.Color = System.Drawing.Color.Empty
        Me.chkExcluirPresentacion.Location = New System.Drawing.Point(411, 293)
        Me.chkExcluirPresentacion.Name = "chkExcluirPresentacion"
        Me.chkExcluirPresentacion.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirPresentacion.SoloLectura = False
        Me.chkExcluirPresentacion.TabIndex = 38
        Me.chkExcluirPresentacion.Texto = ""
        Me.chkExcluirPresentacion.Valor = False
        '
        'chkExcluirSubLinea2
        '
        Me.chkExcluirSubLinea2.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirSubLinea2.Color = System.Drawing.Color.Empty
        Me.chkExcluirSubLinea2.Location = New System.Drawing.Point(411, 274)
        Me.chkExcluirSubLinea2.Name = "chkExcluirSubLinea2"
        Me.chkExcluirSubLinea2.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirSubLinea2.SoloLectura = False
        Me.chkExcluirSubLinea2.TabIndex = 35
        Me.chkExcluirSubLinea2.Texto = ""
        Me.chkExcluirSubLinea2.Valor = False
        '
        'chkPresentacion
        '
        Me.chkPresentacion.BackColor = System.Drawing.Color.Transparent
        Me.chkPresentacion.Color = System.Drawing.Color.Empty
        Me.chkPresentacion.Location = New System.Drawing.Point(6, 290)
        Me.chkPresentacion.Name = "chkPresentacion"
        Me.chkPresentacion.Size = New System.Drawing.Size(114, 21)
        Me.chkPresentacion.SoloLectura = False
        Me.chkPresentacion.TabIndex = 36
        Me.chkPresentacion.Texto = "Presentación:"
        Me.chkPresentacion.Valor = False
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = "IDPresentacion"
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = "Descripcion"
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = "Descripcion"
        Me.cbxPresentacion.DataSource = "VPresentacion"
        Me.cbxPresentacion.DataValueMember = "ID"
        Me.cbxPresentacion.dtSeleccionado = Nothing
        Me.cbxPresentacion.Enabled = False
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPresentacion.Location = New System.Drawing.Point(126, 290)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionMultiple = False
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(279, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 37
        Me.cbxPresentacion.Texto = ""
        '
        'chkExcluirSubLinea
        '
        Me.chkExcluirSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirSubLinea.Color = System.Drawing.Color.Empty
        Me.chkExcluirSubLinea.Location = New System.Drawing.Point(411, 254)
        Me.chkExcluirSubLinea.Name = "chkExcluirSubLinea"
        Me.chkExcluirSubLinea.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirSubLinea.SoloLectura = False
        Me.chkExcluirSubLinea.TabIndex = 32
        Me.chkExcluirSubLinea.Texto = ""
        Me.chkExcluirSubLinea.Valor = False
        '
        'chkSubLinea2
        '
        Me.chkSubLinea2.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea2.Color = System.Drawing.Color.Empty
        Me.chkSubLinea2.Location = New System.Drawing.Point(6, 270)
        Me.chkSubLinea2.Name = "chkSubLinea2"
        Me.chkSubLinea2.Size = New System.Drawing.Size(114, 21)
        Me.chkSubLinea2.SoloLectura = False
        Me.chkSubLinea2.TabIndex = 33
        Me.chkSubLinea2.Texto = "Sub-Linea2:"
        Me.chkSubLinea2.Valor = False
        '
        'cbxSubLinea2
        '
        Me.cbxSubLinea2.CampoWhere = "IDSubLinea2"
        Me.cbxSubLinea2.CargarUnaSolaVez = False
        Me.cbxSubLinea2.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea2.DataFilter = Nothing
        Me.cbxSubLinea2.DataOrderBy = "Descripcion"
        Me.cbxSubLinea2.DataSource = "VSubLinea2"
        Me.cbxSubLinea2.DataValueMember = "ID"
        Me.cbxSubLinea2.dtSeleccionado = Nothing
        Me.cbxSubLinea2.Enabled = False
        Me.cbxSubLinea2.FormABM = Nothing
        Me.cbxSubLinea2.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea2.Location = New System.Drawing.Point(126, 270)
        Me.cbxSubLinea2.Name = "cbxSubLinea2"
        Me.cbxSubLinea2.SeleccionMultiple = False
        Me.cbxSubLinea2.SeleccionObligatoria = False
        Me.cbxSubLinea2.Size = New System.Drawing.Size(279, 21)
        Me.cbxSubLinea2.SoloLectura = False
        Me.cbxSubLinea2.TabIndex = 34
        Me.cbxSubLinea2.Texto = ""
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = "IDSubLinea"
        Me.cbxSubLinea.CargarUnaSolaVez = False
        Me.cbxSubLinea.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = "Descripcion"
        Me.cbxSubLinea.DataSource = "VSubLinea"
        Me.cbxSubLinea.DataValueMember = "ID"
        Me.cbxSubLinea.dtSeleccionado = Nothing
        Me.cbxSubLinea.Enabled = False
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea.Location = New System.Drawing.Point(126, 249)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionMultiple = False
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(279, 21)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 31
        Me.cbxSubLinea.Texto = ""
        '
        'chkSubLinea
        '
        Me.chkSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea.Color = System.Drawing.Color.Empty
        Me.chkSubLinea.Location = New System.Drawing.Point(6, 249)
        Me.chkSubLinea.Name = "chkSubLinea"
        Me.chkSubLinea.Size = New System.Drawing.Size(114, 21)
        Me.chkSubLinea.SoloLectura = False
        Me.chkSubLinea.TabIndex = 30
        Me.chkSubLinea.Texto = "Sub-Linea:"
        Me.chkSubLinea.Valor = False
        '
        'chkExcluirTipoCliente
        '
        Me.chkExcluirTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoCliente.Location = New System.Drawing.Point(411, 120)
        Me.chkExcluirTipoCliente.Name = "chkExcluirTipoCliente"
        Me.chkExcluirTipoCliente.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoCliente.SoloLectura = False
        Me.chkExcluirTipoCliente.TabIndex = 14
        Me.chkExcluirTipoCliente.Texto = ""
        Me.chkExcluirTipoCliente.Valor = False
        '
        'chkExcluirProveedor
        '
        Me.chkExcluirProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirProveedor.Color = System.Drawing.Color.Empty
        Me.chkExcluirProveedor.Location = New System.Drawing.Point(411, 163)
        Me.chkExcluirProveedor.Name = "chkExcluirProveedor"
        Me.chkExcluirProveedor.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirProveedor.SoloLectura = False
        Me.chkExcluirProveedor.TabIndex = 20
        Me.chkExcluirProveedor.Texto = ""
        Me.chkExcluirProveedor.Valor = False
        '
        'chkExcluirTipoProducto
        '
        Me.chkExcluirTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoProducto.Location = New System.Drawing.Point(411, 191)
        Me.chkExcluirTipoProducto.Name = "chkExcluirTipoProducto"
        Me.chkExcluirTipoProducto.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoProducto.SoloLectura = False
        Me.chkExcluirTipoProducto.TabIndex = 23
        Me.chkExcluirTipoProducto.Texto = ""
        Me.chkExcluirTipoProducto.Valor = False
        '
        'chkExcluirLinea
        '
        Me.chkExcluirLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirLinea.Color = System.Drawing.Color.Empty
        Me.chkExcluirLinea.Location = New System.Drawing.Point(411, 211)
        Me.chkExcluirLinea.Name = "chkExcluirLinea"
        Me.chkExcluirLinea.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirLinea.SoloLectura = False
        Me.chkExcluirLinea.TabIndex = 26
        Me.chkExcluirLinea.Texto = ""
        Me.chkExcluirLinea.Valor = False
        '
        'chkExcluirMarca
        '
        Me.chkExcluirMarca.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirMarca.Color = System.Drawing.Color.Empty
        Me.chkExcluirMarca.Location = New System.Drawing.Point(411, 233)
        Me.chkExcluirMarca.Name = "chkExcluirMarca"
        Me.chkExcluirMarca.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirMarca.SoloLectura = False
        Me.chkExcluirMarca.TabIndex = 29
        Me.chkExcluirMarca.Texto = ""
        Me.chkExcluirMarca.Valor = False
        '
        'chkExcluirCiudad
        '
        Me.chkExcluirCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirCiudad.Color = System.Drawing.Color.Empty
        Me.chkExcluirCiudad.Location = New System.Drawing.Point(411, 333)
        Me.chkExcluirCiudad.Name = "chkExcluirCiudad"
        Me.chkExcluirCiudad.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirCiudad.SoloLectura = False
        Me.chkExcluirCiudad.TabIndex = 41
        Me.chkExcluirCiudad.Texto = ""
        Me.chkExcluirCiudad.Valor = False
        '
        'chkExcluirSucursal
        '
        Me.chkExcluirSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirSucursal.Color = System.Drawing.Color.Empty
        Me.chkExcluirSucursal.Location = New System.Drawing.Point(411, 355)
        Me.chkExcluirSucursal.Name = "chkExcluirSucursal"
        Me.chkExcluirSucursal.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirSucursal.SoloLectura = False
        Me.chkExcluirSucursal.TabIndex = 44
        Me.chkExcluirSucursal.Texto = ""
        Me.chkExcluirSucursal.Valor = False
        '
        'chkExcluirDeposito
        '
        Me.chkExcluirDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirDeposito.Color = System.Drawing.Color.Empty
        Me.chkExcluirDeposito.Location = New System.Drawing.Point(411, 376)
        Me.chkExcluirDeposito.Name = "chkExcluirDeposito"
        Me.chkExcluirDeposito.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirDeposito.SoloLectura = False
        Me.chkExcluirDeposito.TabIndex = 47
        Me.chkExcluirDeposito.Texto = ""
        Me.chkExcluirDeposito.Valor = False
        '
        'chkExcluirListaPrecios
        '
        Me.chkExcluirListaPrecios.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirListaPrecios.Color = System.Drawing.Color.Empty
        Me.chkExcluirListaPrecios.Location = New System.Drawing.Point(411, 99)
        Me.chkExcluirListaPrecios.Name = "chkExcluirListaPrecios"
        Me.chkExcluirListaPrecios.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirListaPrecios.SoloLectura = False
        Me.chkExcluirListaPrecios.TabIndex = 11
        Me.chkExcluirListaPrecios.Texto = ""
        Me.chkExcluirListaPrecios.Valor = False
        '
        'chkExcluirZonaVenta
        '
        Me.chkExcluirZonaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirZonaVenta.Color = System.Drawing.Color.Empty
        Me.chkExcluirZonaVenta.Location = New System.Drawing.Point(411, 78)
        Me.chkExcluirZonaVenta.Name = "chkExcluirZonaVenta"
        Me.chkExcluirZonaVenta.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirZonaVenta.SoloLectura = False
        Me.chkExcluirZonaVenta.TabIndex = 8
        Me.chkExcluirZonaVenta.Texto = ""
        Me.chkExcluirZonaVenta.Valor = False
        '
        'chkExcluirVendedor
        '
        Me.chkExcluirVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirVendedor.Color = System.Drawing.Color.Empty
        Me.chkExcluirVendedor.Location = New System.Drawing.Point(411, 57)
        Me.chkExcluirVendedor.Name = "chkExcluirVendedor"
        Me.chkExcluirVendedor.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirVendedor.SoloLectura = False
        Me.chkExcluirVendedor.TabIndex = 5
        Me.chkExcluirVendedor.Texto = ""
        Me.chkExcluirVendedor.Valor = False
        '
        'chkExcluirTipoComprobante
        '
        Me.chkExcluirTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoComprobante.Location = New System.Drawing.Point(411, 36)
        Me.chkExcluirTipoComprobante.Name = "chkExcluirTipoComprobante"
        Me.chkExcluirTipoComprobante.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoComprobante.SoloLectura = False
        Me.chkExcluirTipoComprobante.TabIndex = 2
        Me.chkExcluirTipoComprobante.Texto = ""
        Me.chkExcluirTipoComprobante.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(7, 471)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(421, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 51
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(7, 158)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(114, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 18
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = "RazonSocial"
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = "RazonSocial"
        Me.cbxProveedor.DataSource = "VProveedor"
        Me.cbxProveedor.DataValueMember = "ID"
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = Nothing
        Me.cbxProveedor.Location = New System.Drawing.Point(127, 158)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(278, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 19
        Me.cbxProveedor.Texto = "19 DE MARZO S.A."
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(7, 115)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 12
        Me.chkTipoCliente.Texto = "Tipo de Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "VTipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(127, 115)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(278, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 13
        Me.cbxTipoCliente.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(7, 185)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 21
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(127, 186)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(278, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 22
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(7, 420)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(421, 23)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 49
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkMarca
        '
        Me.chkMarca.BackColor = System.Drawing.Color.Transparent
        Me.chkMarca.Color = System.Drawing.Color.Empty
        Me.chkMarca.Location = New System.Drawing.Point(7, 228)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Size = New System.Drawing.Size(114, 21)
        Me.chkMarca.SoloLectura = False
        Me.chkMarca.TabIndex = 27
        Me.chkMarca.Texto = "Marca:"
        Me.chkMarca.Valor = False
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = "IDMarca"
        Me.cbxMarca.CargarUnaSolaVez = False
        Me.cbxMarca.DataDisplayMember = "Descripcion"
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = "Descripcion"
        Me.cbxMarca.DataSource = "VMarca"
        Me.cbxMarca.DataValueMember = "ID"
        Me.cbxMarca.dtSeleccionado = Nothing
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = Nothing
        Me.cbxMarca.Location = New System.Drawing.Point(127, 228)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionMultiple = False
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(278, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 28
        Me.cbxMarca.Texto = "ADES"
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(7, 207)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(114, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 24
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "VLinea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.dtSeleccionado = Nothing
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(127, 207)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionMultiple = False
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(278, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 25
        Me.cbxLinea.Texto = "BEBIDAS"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(7, 451)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 50
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(7, 402)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 48
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkListaPrecio
        '
        Me.chkListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkListaPrecio.Location = New System.Drawing.Point(7, 94)
        Me.chkListaPrecio.Name = "chkListaPrecio"
        Me.chkListaPrecio.Size = New System.Drawing.Size(114, 21)
        Me.chkListaPrecio.SoloLectura = False
        Me.chkListaPrecio.TabIndex = 9
        Me.chkListaPrecio.Texto = "Lista de Precios:"
        Me.chkListaPrecio.Valor = False
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = "IDListaPrecio"
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Lista"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = "Lista"
        Me.cbxListaPrecio.DataSource = "VListaPrecio"
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.Enabled = False
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(127, 94)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(278, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 10
        Me.cbxListaPrecio.Texto = "BARES Y COPETINES"
        '
        'chkZonaVenta
        '
        Me.chkZonaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkZonaVenta.Color = System.Drawing.Color.Empty
        Me.chkZonaVenta.Location = New System.Drawing.Point(7, 73)
        Me.chkZonaVenta.Name = "chkZonaVenta"
        Me.chkZonaVenta.Size = New System.Drawing.Size(114, 21)
        Me.chkZonaVenta.SoloLectura = False
        Me.chkZonaVenta.TabIndex = 6
        Me.chkZonaVenta.Texto = "Zona de Venta:"
        Me.chkZonaVenta.Valor = False
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = "IDZonaVenta"
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = Nothing
        Me.cbxZonaVenta.DataSource = "ZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.dtSeleccionado = Nothing
        Me.cbxZonaVenta.Enabled = False
        Me.cbxZonaVenta.FormABM = Nothing
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(127, 73)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionMultiple = False
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(278, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 7
        Me.cbxZonaVenta.Texto = "CENTRAL NORTE 1"
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(7, 371)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(114, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 45
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(127, 371)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(278, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 46
        Me.cbxDeposito.Texto = "VENTAS01"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(7, 350)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 42
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(127, 350)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(278, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 43
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(7, 329)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(114, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 39
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudadCliente"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Descripcion"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(127, 329)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(278, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 40
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(7, 52)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(114, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 3
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(7, 31)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 0
        Me.chkTipoComprobante.Texto = "Tipo Comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = "FormName='frmVenta'"
        Me.cbxTipoComprobante.DataOrderBy = "Descripcion"
        Me.cbxTipoComprobante.DataSource = "VTipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(127, 31)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(278, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = "BOLETA DE VENTAS"
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = "frmVendedor"
        Me.cbxVendedor.Indicaciones = "Seleccion de Vendedor"
        Me.cbxVendedor.Location = New System.Drawing.Point(127, 52)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(278, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 4
        Me.cbxVendedor.Texto = "ALDO GONZALES"
        '
        'chkSucursalCliente
        '
        Me.chkSucursalCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalCliente.Color = System.Drawing.Color.Empty
        Me.chkSucursalCliente.Location = New System.Drawing.Point(6, 396)
        Me.chkSucursalCliente.Name = "chkSucursalCliente"
        Me.chkSucursalCliente.Size = New System.Drawing.Size(201, 21)
        Me.chkSucursalCliente.SoloLectura = False
        Me.chkSucursalCliente.TabIndex = 0
        Me.chkSucursalCliente.Texto = "Separar por sucursales de clientes"
        Me.chkSucursalCliente.Valor = True
        '
        'chkDevoluciones
        '
        Me.chkDevoluciones.BackColor = System.Drawing.Color.Transparent
        Me.chkDevoluciones.Color = System.Drawing.Color.Empty
        Me.chkDevoluciones.Location = New System.Drawing.Point(6, 372)
        Me.chkDevoluciones.Name = "chkDevoluciones"
        Me.chkDevoluciones.Size = New System.Drawing.Size(171, 21)
        Me.chkDevoluciones.SoloLectura = False
        Me.chkDevoluciones.TabIndex = 17
        Me.chkDevoluciones.Texto = "Descontar devoluciones"
        Me.chkDevoluciones.Valor = True
        '
        'chkIncluirAnulados
        '
        Me.chkIncluirAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirAnulados.Color = System.Drawing.Color.Empty
        Me.chkIncluirAnulados.Location = New System.Drawing.Point(6, 346)
        Me.chkIncluirAnulados.Name = "chkIncluirAnulados"
        Me.chkIncluirAnulados.Size = New System.Drawing.Size(171, 21)
        Me.chkIncluirAnulados.SoloLectura = False
        Me.chkIncluirAnulados.TabIndex = 16
        Me.chkIncluirAnulados.Texto = "Incluir Anulados"
        Me.chkIncluirAnulados.Valor = False
        '
        'chkIncluirProductosSinControl
        '
        Me.chkIncluirProductosSinControl.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirProductosSinControl.Color = System.Drawing.Color.Empty
        Me.chkIncluirProductosSinControl.Location = New System.Drawing.Point(6, 321)
        Me.chkIncluirProductosSinControl.Name = "chkIncluirProductosSinControl"
        Me.chkIncluirProductosSinControl.Size = New System.Drawing.Size(171, 21)
        Me.chkIncluirProductosSinControl.SoloLectura = False
        Me.chkIncluirProductosSinControl.TabIndex = 15
        Me.chkIncluirProductosSinControl.Texto = "Incluir productos sin control"
        Me.chkIncluirProductosSinControl.Valor = False
        '
        'cbxTipoReporte
        '
        Me.cbxTipoReporte.CampoWhere = Nothing
        Me.cbxTipoReporte.CargarUnaSolaVez = False
        Me.cbxTipoReporte.DataDisplayMember = Nothing
        Me.cbxTipoReporte.DataFilter = Nothing
        Me.cbxTipoReporte.DataOrderBy = Nothing
        Me.cbxTipoReporte.DataSource = Nothing
        Me.cbxTipoReporte.DataValueMember = Nothing
        Me.cbxTipoReporte.dtSeleccionado = Nothing
        Me.cbxTipoReporte.FormABM = Nothing
        Me.cbxTipoReporte.Indicaciones = Nothing
        Me.cbxTipoReporte.Location = New System.Drawing.Point(6, 82)
        Me.cbxTipoReporte.Name = "cbxTipoReporte"
        Me.cbxTipoReporte.SeleccionMultiple = False
        Me.cbxTipoReporte.SeleccionObligatoria = True
        Me.cbxTipoReporte.Size = New System.Drawing.Size(216, 21)
        Me.cbxTipoReporte.SoloLectura = False
        Me.cbxTipoReporte.TabIndex = 5
        Me.cbxTipoReporte.Texto = ""
        '
        'cbxVentas
        '
        Me.cbxVentas.CampoWhere = Nothing
        Me.cbxVentas.CargarUnaSolaVez = False
        Me.cbxVentas.DataDisplayMember = Nothing
        Me.cbxVentas.DataFilter = Nothing
        Me.cbxVentas.DataOrderBy = Nothing
        Me.cbxVentas.DataSource = Nothing
        Me.cbxVentas.DataValueMember = Nothing
        Me.cbxVentas.dtSeleccionado = Nothing
        Me.cbxVentas.FormABM = Nothing
        Me.cbxVentas.Indicaciones = Nothing
        Me.cbxVentas.Location = New System.Drawing.Point(6, 128)
        Me.cbxVentas.Name = "cbxVentas"
        Me.cbxVentas.SeleccionMultiple = False
        Me.cbxVentas.SeleccionObligatoria = False
        Me.cbxVentas.Size = New System.Drawing.Size(160, 21)
        Me.cbxVentas.SoloLectura = False
        Me.cbxVentas.TabIndex = 7
        Me.cbxVentas.Texto = ""
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(6, 179)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(160, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 9
        Me.cbxMoneda.Texto = "DOLARES E.E.U.U."
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(106, 235)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(56, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 12
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 235)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(98, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 11
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtHasta.Location = New System.Drawing.Point(89, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 3
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtDesde.Location = New System.Drawing.Point(6, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 2
        '
        'frmListadoVentasPorCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(715, 539)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoVentasPorCliente"
        Me.Text = "frmListadoVentasPorCliente"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkExcluirSubLinea2 As ERP.ocxCHK
    Friend WithEvents chkExcluirSubLinea As ERP.ocxCHK
    Friend WithEvents cbxSubLinea2 As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkSubLinea2 As ERP.ocxCHK
    Friend WithEvents cbxSubLinea As ERP.ocxCBX
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkSubLinea As ERP.ocxCHK
    Friend WithEvents chkExcluirTipoCliente As ERP.ocxCHK
    Friend WithEvents chkExcluirProveedor As ERP.ocxCHK
    Friend WithEvents chkExcluirTipoProducto As ERP.ocxCHK
    Friend WithEvents chkExcluirLinea As ERP.ocxCHK
    Friend WithEvents chkExcluirMarca As ERP.ocxCHK
    Friend WithEvents chkExcluirCiudad As ERP.ocxCHK
    Friend WithEvents chkExcluirSucursal As ERP.ocxCHK
    Friend WithEvents chkExcluirDeposito As ERP.ocxCHK
    Friend WithEvents chkExcluirListaPrecios As ERP.ocxCHK
    Friend WithEvents chkExcluirZonaVenta As ERP.ocxCHK
    Friend WithEvents chkExcluirVendedor As ERP.ocxCHK
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkExcluirTipoComprobante As ERP.ocxCHK
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents chkTipoCliente As ERP.ocxCHK
    Friend WithEvents cbxTipoCliente As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents chkMarca As ERP.ocxCHK
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents chkListaPrecio As ERP.ocxCHK
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents chkZonaVenta As ERP.ocxCHK
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Public WithEvents chkDevoluciones As ERP.ocxCHK
    Friend WithEvents chkIncluirAnulados As ERP.ocxCHK
    Friend WithEvents chkIncluirProductosSinControl As ERP.ocxCHK
    Friend WithEvents cbxTipoReporte As ERP.ocxCBX
    Friend WithEvents lblReporte As System.Windows.Forms.Label
    Friend WithEvents cbxVentas As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkSucursalCliente As ERP.ocxCHK
    Friend WithEvents chkExcluirPresentacion As ERP.ocxCHK
    Friend WithEvents chkPresentacion As ERP.ocxCHK
    Friend WithEvents cbxPresentacion As ERP.ocxCBX
    Friend WithEvents chkExcluirCategoriaCliente As ocxCHK
    Friend WithEvents chkCategoriaCliente As ocxCHK
    Friend WithEvents cbxCategoriaCliente As ocxCBX
End Class

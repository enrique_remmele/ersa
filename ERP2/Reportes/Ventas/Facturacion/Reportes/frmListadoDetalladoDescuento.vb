﻿Imports ERP.Reporte
Public Class frmListadoDetalladoDescuento
    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    'VARIABLES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'funciones
        CargarInformacion()

        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

    End Sub

    Sub CargarInformacion()
        'Cliente
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
        txtCliente.frm = Me

        'Producto
        txtProducto.Conectar()
    End Sub

    Sub Listar()

        Dim WhereDetalle As String = " "
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim SubTitulo As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm
        WhereDetalle = WhereDetalle & " Where cast(FechaEmision as date) Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"

        'Producto
        If chkProducto.Valor = True Then
            If txtProducto.Seleccionado = False Then
                Exit Sub
            End If

            WhereDetalle = WhereDetalle & " And IDProducto=" & txtProducto.Registro("ID") & " "

        End If

        'Cliente
        If chkCliente.Valor = True Then
            If txtCliente.Seleccionado = False Then
                Exit Sub
            End If

            WhereDetalle = WhereDetalle & " And IDCliente=" & txtCliente.Registro("ID") & " "
            WhereDetalle = WhereDetalle & " And IDCliente=" & txtCliente.Registro("ID") & " "

        End If

        If chkTipoProducto.Valor = True Then
            WhereDetalle = WhereDetalle & " and IDTipoProducto = " & cbxTipoProducto.GetValue
        End If

        If ChkTipoDescuento.Valor = True Then
            WhereDetalle = WhereDetalle & " and IDTipoDescuento = " & cbxTipoDescuento.GetValue
        End If

        'Sucursal
        If chkSucursal.Valor = True Then
            WhereDetalle = WhereDetalle & " and IdSucursal = " & cbxSucursal.GetValue
        End If

        'Lista de precio
        If chkMoneda.Valor = True Then
            WhereDetalle = WhereDetalle & " and IDMoneda = " & cbxMoneda.GetValue
        End If

        'Filtros del Reporte para Subtitulo
        Dim Filtro As String = ""
        'Sucursal
        If chkSucursal.Valor = True And cbxSucursal.txt.Text <> "" Then
            Filtro = Filtro & "(Suc. - " & cbxSucursal.txt.Text & ")"
        End If
        'Lista de precio
        If chkMoneda.Valor = True And cbxMoneda.txt.Text <> "" Then
            Filtro = Filtro & "(Moneda - " & cbxMoneda.txt.Text & ")"
        End If
        'Producto
        If chkProducto.Valor = True And txtProducto.txt.Texto <> "" Then
            Filtro = Filtro & "(Prod. - " & txtProducto.txt.Text & ")"
        End If
        'Cliente
        If chkCliente.Valor = True And txtCliente.txtReferencia.txt.Text <> "" Then
            Filtro = Filtro & "Clie. - " & txtCliente.txtReferencia.txt.Text & "-" & txtCliente.txtRazonSocial.txt.Text & ")"
        End If

        CReporte.ListadoDetalladoDescuento(frm, WhereDetalle, "LISTADO DETALLADO DE DESCUENTOS", Filtro, SubTitulo)

    End Sub

    Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub


    Private Sub chkCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkDesde_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean)
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoDetalladoDescuento_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub ChkTipoDescuento_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles ChkTipoDescuento.PropertyChanged
        cbxTipoDescuento.Enabled = value
    End Sub
End Class
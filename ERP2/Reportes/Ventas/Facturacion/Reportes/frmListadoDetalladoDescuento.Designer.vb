﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoDetalladoDescuento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.ChkTipoDescuento = New ERP.ocxCHK()
        Me.cbxTipoDescuento = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(133, 298)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 12
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.ChkTipoDescuento)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoDescuento)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.Label1)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.txtHasta)
        Me.gbxFiltro.Controls.Add(Me.txtDesde)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(442, 280)
        Me.gbxFiltro.TabIndex = 10
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'ChkTipoDescuento
        '
        Me.ChkTipoDescuento.BackColor = System.Drawing.Color.Transparent
        Me.ChkTipoDescuento.Color = System.Drawing.Color.Empty
        Me.ChkTipoDescuento.Location = New System.Drawing.Point(7, 105)
        Me.ChkTipoDescuento.Name = "ChkTipoDescuento"
        Me.ChkTipoDescuento.Size = New System.Drawing.Size(132, 21)
        Me.ChkTipoDescuento.SoloLectura = False
        Me.ChkTipoDescuento.TabIndex = 39
        Me.ChkTipoDescuento.Texto = "Tipo de Descuento:"
        Me.ChkTipoDescuento.Valor = False
        '
        'cbxTipoDescuento
        '
        Me.cbxTipoDescuento.CampoWhere = ""
        Me.cbxTipoDescuento.CargarUnaSolaVez = False
        Me.cbxTipoDescuento.DataDisplayMember = "Descripcion"
        Me.cbxTipoDescuento.DataFilter = Nothing
        Me.cbxTipoDescuento.DataOrderBy = "ID"
        Me.cbxTipoDescuento.DataSource = "VTipoDescuento"
        Me.cbxTipoDescuento.DataValueMember = "ID"
        Me.cbxTipoDescuento.dtSeleccionado = Nothing
        Me.cbxTipoDescuento.Enabled = False
        Me.cbxTipoDescuento.FormABM = Nothing
        Me.cbxTipoDescuento.Indicaciones = Nothing
        Me.cbxTipoDescuento.Location = New System.Drawing.Point(145, 105)
        Me.cbxTipoDescuento.Name = "cbxTipoDescuento"
        Me.cbxTipoDescuento.SeleccionMultiple = False
        Me.cbxTipoDescuento.SeleccionObligatoria = False
        Me.cbxTipoDescuento.Size = New System.Drawing.Size(278, 21)
        Me.cbxTipoDescuento.SoloLectura = False
        Me.cbxTipoDescuento.TabIndex = 40
        Me.cbxTipoDescuento.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(7, 78)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(132, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 37
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "VE.IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "ID"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(145, 78)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(278, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 38
        Me.cbxTipoProducto.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 233)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "Periodo"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(8, 207)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(421, 23)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 35
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(8, 184)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(69, 17)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 34
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtHasta.Location = New System.Drawing.Point(89, 251)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 30
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 5, 14, 23, 5, 359)
        Me.txtDesde.Location = New System.Drawing.Point(6, 251)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 29
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(7, 152)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(421, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 27
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(7, 132)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 26
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(7, 51)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(114, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 6
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(145, 51)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(278, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 7
        Me.cbxMoneda.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(7, 25)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 20
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(145, 25)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(278, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 21
        Me.cbxSucursal.Texto = ""
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(13, 298)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(98, 23)
        Me.btnInforme.TabIndex = 11
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmListadoDetalladoDescuento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(465, 336)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoDetalladoDescuento"
        Me.Text = "frmListadoDetalladoDescuento"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents ChkTipoDescuento As ocxCHK
    Friend WithEvents cbxTipoDescuento As ocxCBX
End Class

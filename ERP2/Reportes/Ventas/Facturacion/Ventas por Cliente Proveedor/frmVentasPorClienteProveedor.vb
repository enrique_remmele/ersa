﻿Imports ERP.Reporte
Public Class frmVentasPorClienteProveedor
    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'funciones
        CargarInformacion()

        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

    End Sub

    Sub Listar()

        'Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim TipoInforme As String = ""
        Dim SubTitulo As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And IdMoneda= " & cbxMoneda.GetValue
        WhereDetalle = " Where FechaEmision Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And IDProducto>0 "

        If cbxVentas.txt.Text = "Crédito" Then
            WhereDetalle = WhereDetalle & " And Credito=1"
        End If

        If cbxVentas.txt.Text = "Contado" Then
            WhereDetalle = WhereDetalle & " And Credito=0"
        End If

        'Producto
        If chkProducto.Valor = True Then
            If txtProducto.Seleccionado = False Then
                Exit Sub
            End If

            WhereDetalle = WhereDetalle & " And IDProducto=" & txtProducto.Registro("ID") & " "

        End If

        'Cliente
        If chkCliente.Valor = True Then
            If txtCliente.Seleccionado = False Then
                Exit Sub
            End If

            WhereDetalle = WhereDetalle & " And IDCliente=" & txtCliente.Registro("ID") & " "

        End If

        'Incluir Sin Control
        If chkIncluirProductosSinControl.Valor = False Then
            WhereDetalle = WhereDetalle & " and ControlarExistencia = 'True' "
        End If

        'Incluir Anulados
        If chkIncluirAnulados.Valor = False Then
            WhereDetalle = WhereDetalle & " and Anulado = 'False' "
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CReporte.ArmarSubTitulo(SubTitulo, gbxFiltro, nudRanking, cbxVentas, cbxMoneda, cbxOrdenadoPor, cbxEnForma, txtDesde, txtHasta)
        'EstablecerCondicionVenta(Where)
        EstablecerCondicionDetalleVenta(WhereDetalle)

        Select cbxTipoReporte.cbx.SelectedIndex
            Case 0
                CReporte.ListadoVentasTotalesClienteProveedor(frm, WhereDetalle, SubTitulo, OrderBy, Top, chkDevoluciones.Valor)
                '    Case 1
                '        CReporte.ListadoVentasTotalesProducto(frm, WhereDetalle, TipoInforme, OrderBy, Top, chkDevoluciones.Valor, "")
                '    Case 2
                '        CReporte.ListadoVentasTotalesTipoProducto(frm, WhereDetalle, "", TipoInforme, "", OrderBy, Top, chkDevoluciones.Valor)
        End Select

    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)

        SubTitulo = "Fecha: " & txtDesde.GetValue.ToShortDateString & " - " & txtHasta.GetValue.ToShortDateString & " "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubTitulo(SubTitulo)
            End If

        Next

    End Sub

    Sub EstablecerCondicionVenta(ByRef Where As String)

        'Tipo de Comprobante
        cbxTipoComprobante.EstablecerCondicion(Where, chkExcluirTipoComprobante.Valor)

        'Vendedor
        cbxVendedor.EstablecerCondicion(Where, chkExcluirVendedor.Valor)

        'Zona de Venta
        cbxZonaVenta.EstablecerCondicion(Where, chkExcluirZonaVenta.Valor)

        'Lista de Precio
        cbxListaPrecio.EstablecerCondicion(Where, chkExcluirListaPrecios.Valor)

        'Tipo Cliente
        cbxTipoCliente.EstablecerCondicion(Where, chkExcluirTipoCliente.Valor)

        'Proveedor
        cbxProveedor.EstablecerCondicion(Where, chkExcluirProveedor.Valor)

        'Ciudad
        cbxCiudad.EstablecerCondicion(Where, chkExcluirCiudad.Valor)

        'Sucursal
        cbxSucursal.EstablecerCondicion(Where, chkExcluirSucursal.Valor)

    End Sub

    Sub EstablecerCondicionDetalleVenta(ByRef Where As String)

        'Tipo de Comprobante
        cbxTipoComprobante.EstablecerCondicion(Where, chkExcluirTipoComprobante.Valor)

        'Vendedor
        cbxVendedor.EstablecerCondicion(Where, chkExcluirVendedor.Valor)

        'Zona de Venta
        cbxZonaVenta.EstablecerCondicion(Where, chkExcluirZonaVenta.Valor)

        'Lista de Precio
        cbxListaPrecio.EstablecerCondicion(Where, chkExcluirListaPrecios.Valor)

        'Tipo Cliente
        cbxTipoCliente.EstablecerCondicion(Where, chkExcluirTipoCliente.Valor)

        'Proveedor
        cbxProveedor.EstablecerCondicion(Where, chkExcluirProveedor.Valor)

        'Ciudad
        cbxCiudad.EstablecerCondicion(Where, chkExcluirCiudad.Valor)

        'Sucursal
        cbxSucursal.EstablecerCondicion(Where, chkExcluirSucursal.Valor)

        'Tipo de Producto
        cbxTipoProducto.EstablecerCondicion(Where, chkExcluirTipoProducto.Valor)

        'Linea
        cbxLinea.EstablecerCondicion(Where, chkExcluirLinea.Valor)

        'Marca
        cbxMarca.EstablecerCondicion(Where, chkExcluirMarca.Valor)

        'SubLinea
        cbxSubLinea.EstablecerCondicion(Where, chkExcluirSubLinea.Valor)

        'SubLinea2
        cbxSubLinea2.EstablecerCondicion(Where, chkExcluirSubLinea2.Valor)

        'Deposito
        cbxDeposito.EstablecerCondicion(Where, chkExcluirDeposito.Valor)

    End Sub

    Sub CargarInformacion()


        'Cliente
        txtCliente.Conectar()
        txtCliente.Consulta = "Select ID, Referencia, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Lista de Precio'=ListaPrecio, Vendedor, 'Saldo Credito'=SaldoCredito  From VCliente "
        txtCliente.frm = Me

        'Producto
        txtProducto.Conectar()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        'Reporte
        cbxTipoReporte.cbx.Items.Add("CLIENTE\PROVEEDOR")
        'cbxTipoReporte.cbx.Items.Add("PRODUCTO")
        'cbxTipoReporte.cbx.Items.Add("TIPO DE PRODUCTO")
        cbxTipoReporte.cbx.SelectedIndex = 0

        'Ventas
        cbxVentas.cbx.Items.Add("Contado + Crédito")
        cbxVentas.cbx.Items.Add("Contado")
        cbxVentas.cbx.Items.Add("Crédito")
        cbxVentas.cbx.SelectedIndex = 0

        'Ordenacion
        cbxOrdenadoPor.cbx.Items.Add("DireccionAlternativa")
        cbxOrdenadoPor.cbx.Items.Add("Cantidad")
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.SelectedIndex = 0


        'Tipo de Venta
        cbxVentas.cbx.SelectedIndex = 0

        'Configuracion
        cbxTipoReporte.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "REPORTE", "")
        cbxOrdenadoPor.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENADO", "")
        cbxMoneda.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "MONEDA", "")
        cbxEnForma.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ENFORMA", "")
        nudRanking.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "RANKING", "")

    End Sub

    Sub GuardarInformacion()

        'Configuracion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "REPORTE", cbxTipoReporte.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENADO", cbxOrdenadoPor.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ENFORMA", cbxEnForma.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RANKING", nudRanking.Text)


    End Sub

    Private Sub frmVentasPorClienteProveedor_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GuardarInformacion()
    End Sub

    Private Sub frmListadoVentasPorProductoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoVentasporProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
        chkExcluirTipoComprobante.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
        chkExcluirVendedor.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
        chkExcluirCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
        chkExcluirSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
        chkExcluirDeposito.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
        chkExcluirZonaVenta.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
        chkExcluirListaPrecios.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub ckLinea_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
        chkExcluirLinea.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
        chkExcluirTipoCliente.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
        chkExcluirProveedor.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
        chkExcluirTipoProducto.Enabled = value
    End Sub

    Private Sub chkMarca_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
        chkExcluirMarca.Enabled = value
    End Sub

    Private Sub chkSubLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSubLinea.PropertyChanged
        cbxSubLinea.Enabled = value
        chkExcluirSubLinea.Enabled = value
    End Sub

    Private Sub chkSubLinea2_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSubLinea2.PropertyChanged
        cbxSubLinea2.Enabled = value
        chkExcluirSubLinea2.Enabled = value
    End Sub

    Private Sub chkIncluirProductosSinControl_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncluirProductosSinControl.Load

    End Sub

    Private Sub chkIncluirAnulados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncluirAnulados.Load

    End Sub

    Private Sub cbxTipoReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoReporte.Load

    End Sub
End Class
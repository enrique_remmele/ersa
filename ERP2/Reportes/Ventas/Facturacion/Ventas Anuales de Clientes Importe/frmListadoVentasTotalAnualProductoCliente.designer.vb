﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoVentasTotalAnualProductoCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkProcedencia = New ERP.ocxCHK()
        Me.cbxProcedencia = New ERP.ocxCBX()
        Me.chkDivision = New ERP.ocxCHK()
        Me.cbxDivision = New ERP.ocxCBX()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.cbxProveedor = New ERP.ocxCBX()
        Me.chkCategoria = New ERP.ocxCHK()
        Me.cbxCategoria = New ERP.ocxCBX()
        Me.chkPresentacion = New ERP.ocxCHK()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.chkMarca = New ERP.ocxCHK()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkSubLinea2 = New ERP.ocxCHK()
        Me.cbxSubLinea2 = New ERP.ocxCBX()
        Me.chkSubLinea = New ERP.ocxCHK()
        Me.cbxSubLinea = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkListaPrecio = New ERP.ocxCHK()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.chkZonaVenta = New ERP.ocxCHK()
        Me.cbxZonaVenta = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxSeleccionInforme = New ERP.ocxCBX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkSucursalCliente = New ERP.ocxCHK()
        Me.chkDevoluciones = New ERP.ocxCHK()
        Me.chkIncluirAnulados = New ERP.ocxCHK()
        Me.chkIncluirProductosSinControl = New ERP.ocxCHK()
        Me.rdbCantidad = New System.Windows.Forms.RadioButton()
        Me.rdbImporte = New System.Windows.Forms.RadioButton()
        Me.cbxVentas = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.txtCliente = New ERP.ocxTXTCliente()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(531, 457)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(7, 302)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 15
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(7, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(7, 286)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 14
        Me.lblRanking.Text = "Ranking:"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(7, 233)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 11
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 192)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Moneda:"
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProcedencia)
        Me.gbxFiltro.Controls.Add(Me.cbxProcedencia)
        Me.gbxFiltro.Controls.Add(Me.chkDivision)
        Me.gbxFiltro.Controls.Add(Me.cbxDivision)
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.cbxProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkCategoria)
        Me.gbxFiltro.Controls.Add(Me.cbxCategoria)
        Me.gbxFiltro.Controls.Add(Me.chkPresentacion)
        Me.gbxFiltro.Controls.Add(Me.cbxPresentacion)
        Me.gbxFiltro.Controls.Add(Me.chkMarca)
        Me.gbxFiltro.Controls.Add(Me.cbxMarca)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea2)
        Me.gbxFiltro.Controls.Add(Me.chkSubLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxSubLinea)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.cbxListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.chkZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.cbxZonaVenta)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Location = New System.Drawing.Point(2, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(348, 497)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 260
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(9, 465)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(333, 23)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 37
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(9, 447)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 36
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkProcedencia
        '
        Me.chkProcedencia.BackColor = System.Drawing.Color.Transparent
        Me.chkProcedencia.Color = System.Drawing.Color.Empty
        Me.chkProcedencia.Location = New System.Drawing.Point(9, 357)
        Me.chkProcedencia.Name = "chkProcedencia"
        Me.chkProcedencia.Size = New System.Drawing.Size(114, 21)
        Me.chkProcedencia.SoloLectura = False
        Me.chkProcedencia.TabIndex = 31
        Me.chkProcedencia.Texto = "Procedencia:"
        Me.chkProcedencia.Valor = False
        '
        'cbxProcedencia
        '
        Me.cbxProcedencia.CampoWhere = "IDProcedencia"
        Me.cbxProcedencia.CargarUnaSolaVez = False
        Me.cbxProcedencia.DataDisplayMember = "Descripcion"
        Me.cbxProcedencia.DataFilter = Nothing
        Me.cbxProcedencia.DataOrderBy = "Descripcion"
        Me.cbxProcedencia.DataSource = "Pais"
        Me.cbxProcedencia.DataValueMember = "ID"
        Me.cbxProcedencia.dtSeleccionado = Nothing
        Me.cbxProcedencia.Enabled = False
        Me.cbxProcedencia.FormABM = Nothing
        Me.cbxProcedencia.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxProcedencia.Location = New System.Drawing.Point(126, 355)
        Me.cbxProcedencia.Name = "cbxProcedencia"
        Me.cbxProcedencia.SeleccionMultiple = False
        Me.cbxProcedencia.SeleccionObligatoria = False
        Me.cbxProcedencia.Size = New System.Drawing.Size(213, 19)
        Me.cbxProcedencia.SoloLectura = False
        Me.cbxProcedencia.TabIndex = 32
        Me.cbxProcedencia.Texto = ""
        '
        'chkDivision
        '
        Me.chkDivision.BackColor = System.Drawing.Color.Transparent
        Me.chkDivision.Color = System.Drawing.Color.Empty
        Me.chkDivision.Location = New System.Drawing.Point(9, 336)
        Me.chkDivision.Name = "chkDivision"
        Me.chkDivision.Size = New System.Drawing.Size(114, 21)
        Me.chkDivision.SoloLectura = False
        Me.chkDivision.TabIndex = 29
        Me.chkDivision.Texto = "División:"
        Me.chkDivision.Valor = False
        '
        'cbxDivision
        '
        Me.cbxDivision.CampoWhere = "IDDivision"
        Me.cbxDivision.CargarUnaSolaVez = False
        Me.cbxDivision.DataDisplayMember = "Descripcion"
        Me.cbxDivision.DataFilter = Nothing
        Me.cbxDivision.DataOrderBy = "Descripcion"
        Me.cbxDivision.DataSource = "Division"
        Me.cbxDivision.DataValueMember = "ID"
        Me.cbxDivision.dtSeleccionado = Nothing
        Me.cbxDivision.Enabled = False
        Me.cbxDivision.FormABM = Nothing
        Me.cbxDivision.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxDivision.Location = New System.Drawing.Point(126, 334)
        Me.cbxDivision.Name = "cbxDivision"
        Me.cbxDivision.SeleccionMultiple = False
        Me.cbxDivision.SeleccionObligatoria = False
        Me.cbxDivision.Size = New System.Drawing.Size(213, 21)
        Me.cbxDivision.SoloLectura = False
        Me.cbxDivision.TabIndex = 30
        Me.cbxDivision.Texto = ""
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(9, 315)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(114, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 27
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'cbxProveedor
        '
        Me.cbxProveedor.CampoWhere = "IDProveedor"
        Me.cbxProveedor.CargarUnaSolaVez = False
        Me.cbxProveedor.DataDisplayMember = ""
        Me.cbxProveedor.DataFilter = Nothing
        Me.cbxProveedor.DataOrderBy = ""
        Me.cbxProveedor.DataSource = ""
        Me.cbxProveedor.DataValueMember = "IDProveedor"
        Me.cbxProveedor.dtSeleccionado = Nothing
        Me.cbxProveedor.Enabled = False
        Me.cbxProveedor.FormABM = Nothing
        Me.cbxProveedor.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxProveedor.Location = New System.Drawing.Point(126, 313)
        Me.cbxProveedor.Name = "cbxProveedor"
        Me.cbxProveedor.SeleccionMultiple = False
        Me.cbxProveedor.SeleccionObligatoria = False
        Me.cbxProveedor.Size = New System.Drawing.Size(213, 21)
        Me.cbxProveedor.SoloLectura = False
        Me.cbxProveedor.TabIndex = 28
        Me.cbxProveedor.Texto = ""
        '
        'chkCategoria
        '
        Me.chkCategoria.BackColor = System.Drawing.Color.Transparent
        Me.chkCategoria.Color = System.Drawing.Color.Empty
        Me.chkCategoria.Location = New System.Drawing.Point(9, 294)
        Me.chkCategoria.Name = "chkCategoria"
        Me.chkCategoria.Size = New System.Drawing.Size(114, 21)
        Me.chkCategoria.SoloLectura = False
        Me.chkCategoria.TabIndex = 25
        Me.chkCategoria.Texto = "Categoría:"
        Me.chkCategoria.Valor = False
        '
        'cbxCategoria
        '
        Me.cbxCategoria.CampoWhere = "IDCategoria"
        Me.cbxCategoria.CargarUnaSolaVez = False
        Me.cbxCategoria.DataDisplayMember = "Descripcion"
        Me.cbxCategoria.DataFilter = Nothing
        Me.cbxCategoria.DataOrderBy = "Descripcion"
        Me.cbxCategoria.DataSource = "VCategoria"
        Me.cbxCategoria.DataValueMember = "ID"
        Me.cbxCategoria.dtSeleccionado = Nothing
        Me.cbxCategoria.Enabled = False
        Me.cbxCategoria.FormABM = Nothing
        Me.cbxCategoria.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxCategoria.Location = New System.Drawing.Point(126, 292)
        Me.cbxCategoria.Name = "cbxCategoria"
        Me.cbxCategoria.SeleccionMultiple = False
        Me.cbxCategoria.SeleccionObligatoria = False
        Me.cbxCategoria.Size = New System.Drawing.Size(213, 21)
        Me.cbxCategoria.SoloLectura = False
        Me.cbxCategoria.TabIndex = 26
        Me.cbxCategoria.Texto = ""
        '
        'chkPresentacion
        '
        Me.chkPresentacion.BackColor = System.Drawing.Color.Transparent
        Me.chkPresentacion.Color = System.Drawing.Color.Empty
        Me.chkPresentacion.Location = New System.Drawing.Point(9, 273)
        Me.chkPresentacion.Name = "chkPresentacion"
        Me.chkPresentacion.Size = New System.Drawing.Size(114, 21)
        Me.chkPresentacion.SoloLectura = False
        Me.chkPresentacion.TabIndex = 23
        Me.chkPresentacion.Texto = "Presentación:"
        Me.chkPresentacion.Valor = False
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = "IDPresentacion"
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = "Descripcion"
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = "Descripcion"
        Me.cbxPresentacion.DataSource = "VPresentacion"
        Me.cbxPresentacion.DataValueMember = "ID"
        Me.cbxPresentacion.dtSeleccionado = Nothing
        Me.cbxPresentacion.Enabled = False
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxPresentacion.Location = New System.Drawing.Point(126, 271)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionMultiple = False
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(213, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 24
        Me.cbxPresentacion.Texto = ""
        '
        'chkMarca
        '
        Me.chkMarca.BackColor = System.Drawing.Color.Transparent
        Me.chkMarca.Color = System.Drawing.Color.Empty
        Me.chkMarca.Location = New System.Drawing.Point(9, 252)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Size = New System.Drawing.Size(114, 21)
        Me.chkMarca.SoloLectura = False
        Me.chkMarca.TabIndex = 21
        Me.chkMarca.Texto = "Marca:"
        Me.chkMarca.Valor = False
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = "IDMarca"
        Me.cbxMarca.CargarUnaSolaVez = False
        Me.cbxMarca.DataDisplayMember = "Descripcion"
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = "Descripcion"
        Me.cbxMarca.DataSource = "VMarca"
        Me.cbxMarca.DataValueMember = "ID"
        Me.cbxMarca.dtSeleccionado = Nothing
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxMarca.Location = New System.Drawing.Point(126, 250)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionMultiple = False
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(213, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 22
        Me.cbxMarca.Texto = ""
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(9, 189)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(114, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 15
        Me.chkLinea.Texto = "Línea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "VLinea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.dtSeleccionado = Nothing
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxLinea.Location = New System.Drawing.Point(126, 187)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionMultiple = False
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(213, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 16
        Me.cbxLinea.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(9, 168)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 13
        Me.chkTipoProducto.Texto = "TipoProducto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxTipoProducto.Location = New System.Drawing.Point(126, 166)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 14
        Me.cbxTipoProducto.Texto = ""
        '
        'chkSubLinea2
        '
        Me.chkSubLinea2.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea2.Color = System.Drawing.Color.Empty
        Me.chkSubLinea2.Location = New System.Drawing.Point(9, 231)
        Me.chkSubLinea2.Name = "chkSubLinea2"
        Me.chkSubLinea2.Size = New System.Drawing.Size(114, 21)
        Me.chkSubLinea2.SoloLectura = False
        Me.chkSubLinea2.TabIndex = 19
        Me.chkSubLinea2.Texto = "Sub-Línea2:"
        Me.chkSubLinea2.Valor = False
        '
        'cbxSubLinea2
        '
        Me.cbxSubLinea2.CampoWhere = "IDSubLinea2"
        Me.cbxSubLinea2.CargarUnaSolaVez = False
        Me.cbxSubLinea2.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea2.DataFilter = Nothing
        Me.cbxSubLinea2.DataOrderBy = "Descripcion"
        Me.cbxSubLinea2.DataSource = "VSubLinea2"
        Me.cbxSubLinea2.DataValueMember = "ID"
        Me.cbxSubLinea2.dtSeleccionado = Nothing
        Me.cbxSubLinea2.Enabled = False
        Me.cbxSubLinea2.FormABM = Nothing
        Me.cbxSubLinea2.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea2.Location = New System.Drawing.Point(126, 229)
        Me.cbxSubLinea2.Name = "cbxSubLinea2"
        Me.cbxSubLinea2.SeleccionMultiple = False
        Me.cbxSubLinea2.SeleccionObligatoria = False
        Me.cbxSubLinea2.Size = New System.Drawing.Size(213, 21)
        Me.cbxSubLinea2.SoloLectura = False
        Me.cbxSubLinea2.TabIndex = 20
        Me.cbxSubLinea2.Texto = ""
        '
        'chkSubLinea
        '
        Me.chkSubLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkSubLinea.Color = System.Drawing.Color.Empty
        Me.chkSubLinea.Location = New System.Drawing.Point(9, 210)
        Me.chkSubLinea.Name = "chkSubLinea"
        Me.chkSubLinea.Size = New System.Drawing.Size(114, 21)
        Me.chkSubLinea.SoloLectura = False
        Me.chkSubLinea.TabIndex = 17
        Me.chkSubLinea.Texto = "Sub-Linea:"
        Me.chkSubLinea.Valor = False
        '
        'cbxSubLinea
        '
        Me.cbxSubLinea.CampoWhere = "IDSubLinea"
        Me.cbxSubLinea.CargarUnaSolaVez = False
        Me.cbxSubLinea.DataDisplayMember = "Descripcion"
        Me.cbxSubLinea.DataFilter = Nothing
        Me.cbxSubLinea.DataOrderBy = "Descripcion"
        Me.cbxSubLinea.DataSource = "VSubLinea"
        Me.cbxSubLinea.DataValueMember = "ID"
        Me.cbxSubLinea.dtSeleccionado = Nothing
        Me.cbxSubLinea.Enabled = False
        Me.cbxSubLinea.FormABM = Nothing
        Me.cbxSubLinea.Indicaciones = "(F2) Para editar las opciones"
        Me.cbxSubLinea.Location = New System.Drawing.Point(126, 208)
        Me.cbxSubLinea.Name = "cbxSubLinea"
        Me.cbxSubLinea.SeleccionMultiple = False
        Me.cbxSubLinea.SeleccionObligatoria = False
        Me.cbxSubLinea.Size = New System.Drawing.Size(213, 21)
        Me.cbxSubLinea.SoloLectura = False
        Me.cbxSubLinea.TabIndex = 18
        Me.cbxSubLinea.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 405)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 35
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkListaPrecio
        '
        Me.chkListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkListaPrecio.Location = New System.Drawing.Point(9, 385)
        Me.chkListaPrecio.Name = "chkListaPrecio"
        Me.chkListaPrecio.Size = New System.Drawing.Size(114, 21)
        Me.chkListaPrecio.SoloLectura = False
        Me.chkListaPrecio.TabIndex = 33
        Me.chkListaPrecio.Texto = "Lista de Precios:"
        Me.chkListaPrecio.Valor = False
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = "IDListaPrecio"
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = "Lista"
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = "Lista"
        Me.cbxListaPrecio.DataSource = "VListaPrecio"
        Me.cbxListaPrecio.DataValueMember = "ID"
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.Enabled = False
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(126, 385)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(213, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 34
        Me.cbxListaPrecio.Texto = "BARES Y COPETINES"
        '
        'chkZonaVenta
        '
        Me.chkZonaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkZonaVenta.Color = System.Drawing.Color.Empty
        Me.chkZonaVenta.Location = New System.Drawing.Point(9, 135)
        Me.chkZonaVenta.Name = "chkZonaVenta"
        Me.chkZonaVenta.Size = New System.Drawing.Size(114, 21)
        Me.chkZonaVenta.SoloLectura = False
        Me.chkZonaVenta.TabIndex = 11
        Me.chkZonaVenta.Texto = "Zona de Venta:"
        Me.chkZonaVenta.Valor = False
        '
        'cbxZonaVenta
        '
        Me.cbxZonaVenta.CampoWhere = "IDZonaVenta"
        Me.cbxZonaVenta.CargarUnaSolaVez = False
        Me.cbxZonaVenta.DataDisplayMember = "Descripcion"
        Me.cbxZonaVenta.DataFilter = Nothing
        Me.cbxZonaVenta.DataOrderBy = Nothing
        Me.cbxZonaVenta.DataSource = "ZonaVenta"
        Me.cbxZonaVenta.DataValueMember = "ID"
        Me.cbxZonaVenta.dtSeleccionado = Nothing
        Me.cbxZonaVenta.Enabled = False
        Me.cbxZonaVenta.FormABM = Nothing
        Me.cbxZonaVenta.Indicaciones = Nothing
        Me.cbxZonaVenta.Location = New System.Drawing.Point(126, 135)
        Me.cbxZonaVenta.Name = "cbxZonaVenta"
        Me.cbxZonaVenta.SeleccionMultiple = False
        Me.cbxZonaVenta.SeleccionObligatoria = False
        Me.cbxZonaVenta.Size = New System.Drawing.Size(213, 21)
        Me.cbxZonaVenta.SoloLectura = False
        Me.cbxZonaVenta.TabIndex = 12
        Me.cbxZonaVenta.Texto = "CENTRAL NORTE 1"
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(9, 114)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(114, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 9
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(126, 114)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(213, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 10
        Me.cbxDeposito.Texto = "ASUNCION - AVERIADOS"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 93)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 7
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(126, 93)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 8
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(9, 72)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(114, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 5
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudadCliente"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Descripcion"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(126, 72)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(213, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 6
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(9, 41)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(114, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 3
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(9, 20)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 1
        Me.chkTipoComprobante.Texto = "Tipo Comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = "FormName='frmVenta'"
        Me.cbxTipoComprobante.DataOrderBy = "Descripcion"
        Me.cbxTipoComprobante.DataSource = "VTipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.dtSeleccionado = Nothing
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(126, 20)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionMultiple = False
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 2
        Me.cbxTipoComprobante.Texto = "BOLETA DE VENTAS"
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = "frmVendedor"
        Me.cbxVendedor.Indicaciones = "Seleccion de Vendedor"
        Me.cbxVendedor.Location = New System.Drawing.Point(126, 41)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(213, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 4
        Me.cbxVendedor.Texto = "ALDO GONZALES"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxSeleccionInforme)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.chkSucursalCliente)
        Me.GroupBox2.Controls.Add(Me.chkDevoluciones)
        Me.GroupBox2.Controls.Add(Me.chkIncluirAnulados)
        Me.GroupBox2.Controls.Add(Me.chkIncluirProductosSinControl)
        Me.GroupBox2.Controls.Add(Me.rdbCantidad)
        Me.GroupBox2.Controls.Add(Me.rdbImporte)
        Me.GroupBox2.Controls.Add(Me.cbxVentas)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxMoneda)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(356, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 436)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxSeleccionInforme
        '
        Me.cbxSeleccionInforme.CampoWhere = Nothing
        Me.cbxSeleccionInforme.CargarUnaSolaVez = False
        Me.cbxSeleccionInforme.DataDisplayMember = Nothing
        Me.cbxSeleccionInforme.DataFilter = Nothing
        Me.cbxSeleccionInforme.DataOrderBy = Nothing
        Me.cbxSeleccionInforme.DataSource = Nothing
        Me.cbxSeleccionInforme.DataValueMember = Nothing
        Me.cbxSeleccionInforme.dtSeleccionado = Nothing
        Me.cbxSeleccionInforme.FormABM = Nothing
        Me.cbxSeleccionInforme.Indicaciones = Nothing
        Me.cbxSeleccionInforme.Location = New System.Drawing.Point(9, 88)
        Me.cbxSeleccionInforme.Name = "cbxSeleccionInforme"
        Me.cbxSeleccionInforme.SeleccionMultiple = False
        Me.cbxSeleccionInforme.SeleccionObligatoria = False
        Me.cbxSeleccionInforme.Size = New System.Drawing.Size(224, 21)
        Me.cbxSeleccionInforme.SoloLectura = False
        Me.cbxSeleccionInforme.TabIndex = 4
        Me.cbxSeleccionInforme.Texto = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tipo:"
        '
        'chkSucursalCliente
        '
        Me.chkSucursalCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalCliente.Color = System.Drawing.Color.Empty
        Me.chkSucursalCliente.Location = New System.Drawing.Point(7, 406)
        Me.chkSucursalCliente.Name = "chkSucursalCliente"
        Me.chkSucursalCliente.Size = New System.Drawing.Size(227, 21)
        Me.chkSucursalCliente.SoloLectura = False
        Me.chkSucursalCliente.TabIndex = 19
        Me.chkSucursalCliente.Texto = "Separar por sucursales de clientes"
        Me.chkSucursalCliente.Valor = True
        '
        'chkDevoluciones
        '
        Me.chkDevoluciones.BackColor = System.Drawing.Color.Transparent
        Me.chkDevoluciones.Color = System.Drawing.Color.Empty
        Me.chkDevoluciones.Location = New System.Drawing.Point(7, 381)
        Me.chkDevoluciones.Name = "chkDevoluciones"
        Me.chkDevoluciones.Size = New System.Drawing.Size(171, 21)
        Me.chkDevoluciones.SoloLectura = False
        Me.chkDevoluciones.TabIndex = 18
        Me.chkDevoluciones.Texto = "Descontar devoluciones"
        Me.chkDevoluciones.Valor = True
        '
        'chkIncluirAnulados
        '
        Me.chkIncluirAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirAnulados.Color = System.Drawing.Color.Empty
        Me.chkIncluirAnulados.Location = New System.Drawing.Point(7, 356)
        Me.chkIncluirAnulados.Name = "chkIncluirAnulados"
        Me.chkIncluirAnulados.Size = New System.Drawing.Size(171, 21)
        Me.chkIncluirAnulados.SoloLectura = False
        Me.chkIncluirAnulados.TabIndex = 17
        Me.chkIncluirAnulados.Texto = "Incluir Anulados"
        Me.chkIncluirAnulados.Valor = False
        '
        'chkIncluirProductosSinControl
        '
        Me.chkIncluirProductosSinControl.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirProductosSinControl.Color = System.Drawing.Color.Empty
        Me.chkIncluirProductosSinControl.Location = New System.Drawing.Point(7, 331)
        Me.chkIncluirProductosSinControl.Name = "chkIncluirProductosSinControl"
        Me.chkIncluirProductosSinControl.Size = New System.Drawing.Size(171, 21)
        Me.chkIncluirProductosSinControl.SoloLectura = False
        Me.chkIncluirProductosSinControl.TabIndex = 16
        Me.chkIncluirProductosSinControl.Texto = "Incluir productos sin control"
        Me.chkIncluirProductosSinControl.Valor = False
        '
        'rdbCantidad
        '
        Me.rdbCantidad.AutoSize = True
        Me.rdbCantidad.Location = New System.Drawing.Point(90, 115)
        Me.rdbCantidad.Name = "rdbCantidad"
        Me.rdbCantidad.Size = New System.Drawing.Size(86, 17)
        Me.rdbCantidad.TabIndex = 6
        Me.rdbCantidad.TabStop = True
        Me.rdbCantidad.Text = "Por Cantidad"
        Me.rdbCantidad.UseVisualStyleBackColor = True
        '
        'rdbImporte
        '
        Me.rdbImporte.AutoSize = True
        Me.rdbImporte.Location = New System.Drawing.Point(7, 115)
        Me.rdbImporte.Name = "rdbImporte"
        Me.rdbImporte.Size = New System.Drawing.Size(79, 17)
        Me.rdbImporte.TabIndex = 5
        Me.rdbImporte.TabStop = True
        Me.rdbImporte.Text = "Por Importe"
        Me.rdbImporte.UseVisualStyleBackColor = True
        '
        'cbxVentas
        '
        Me.cbxVentas.CampoWhere = Nothing
        Me.cbxVentas.CargarUnaSolaVez = False
        Me.cbxVentas.DataDisplayMember = Nothing
        Me.cbxVentas.DataFilter = Nothing
        Me.cbxVentas.DataOrderBy = Nothing
        Me.cbxVentas.DataSource = Nothing
        Me.cbxVentas.DataValueMember = Nothing
        Me.cbxVentas.dtSeleccionado = Nothing
        Me.cbxVentas.FormABM = Nothing
        Me.cbxVentas.Indicaciones = Nothing
        Me.cbxVentas.Location = New System.Drawing.Point(7, 163)
        Me.cbxVentas.Name = "cbxVentas"
        Me.cbxVentas.SeleccionMultiple = False
        Me.cbxVentas.SeleccionObligatoria = False
        Me.cbxVentas.Size = New System.Drawing.Size(157, 21)
        Me.cbxVentas.SoloLectura = False
        Me.cbxVentas.TabIndex = 8
        Me.cbxVentas.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 146)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Ventas:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(7, 208)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(157, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 10
        Me.cbxMoneda.Texto = "GUARANIES"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(165, 250)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 13
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(7, 250)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 12
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 2, 14, 9, 22, 0, 114)
        Me.txtHasta.Location = New System.Drawing.Point(90, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 2, 14, 9, 22, 0, 114)
        Me.txtDesde.Location = New System.Drawing.Point(7, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(363, 457)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 0
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(9, 421)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(330, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 38
        '
        'frmListadoVentasTotalAnualProductoCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(640, 513)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoVentasTotalAnualProductoCliente"
        Me.Text = "frmListadoVentasAnualesTotalesProductoCliente"
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents chkListaPrecio As ERP.ocxCHK
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents chkZonaVenta As ERP.ocxCHK
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents cbxListaPrecio As ERP.ocxCBX
    Friend WithEvents cbxZonaVenta As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxVentas As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkSubLinea As ERP.ocxCHK
    Friend WithEvents cbxSubLinea As ERP.ocxCBX
    Friend WithEvents chkSubLinea2 As ERP.ocxCHK
    Friend WithEvents cbxSubLinea2 As ERP.ocxCBX
    Friend WithEvents chkProcedencia As ERP.ocxCHK
    Friend WithEvents cbxProcedencia As ERP.ocxCBX
    Friend WithEvents chkDivision As ERP.ocxCHK
    Friend WithEvents cbxDivision As ERP.ocxCBX
    Friend WithEvents chkProveedor As ERP.ocxCHK
    Friend WithEvents cbxProveedor As ERP.ocxCBX
    Friend WithEvents chkCategoria As ERP.ocxCHK
    Friend WithEvents cbxCategoria As ERP.ocxCBX
    Friend WithEvents chkPresentacion As ERP.ocxCHK
    Friend WithEvents cbxPresentacion As ERP.ocxCBX
    Friend WithEvents chkMarca As ERP.ocxCHK
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents rdbCantidad As System.Windows.Forms.RadioButton
    Friend WithEvents rdbImporte As System.Windows.Forms.RadioButton
    Friend WithEvents chkDevoluciones As ERP.ocxCHK
    Friend WithEvents chkIncluirAnulados As ERP.ocxCHK
    Friend WithEvents chkIncluirProductosSinControl As ERP.ocxCHK
    Friend WithEvents chkSucursalCliente As ERP.ocxCHK
    Friend WithEvents cbxSeleccionInforme As ERP.ocxCBX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents txtCliente As ocxTXTCliente
End Class

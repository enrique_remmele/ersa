﻿Imports ERP.Reporte
Public Class frmListadoVentasTotalAnualProductoCliente


    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'VARIABLES
    Dim Titulo As String = ""
    Dim TipoInforme As String

    Sub Inicializar()
        CargarInformacion()

        'Fecha
        txtDesde.PrimerDiaAño()
        txtHasta.UltimoDiaMes()

        'RadioButton
        rdbImporte.Checked = True

        'combo
        cbxSeleccionInforme.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Moneda As String = ""
        Dim EsCantidad As Boolean = False
        Dim SeleccionInforme As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If cbxVentas.txt.Text = "Contado + Crédito" Then
            'Filtrar por Fecha, Moneda y Tipo de Pago Contado y Crédito
            Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And IdMoneda= " & cbxMoneda.GetValue
        End If

        If cbxVentas.txt.Text = "Crédito" Then
            'Filtrar por Fecha, Moneda y Tipo de Pago Crédito
            Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And IDMoneda=" & cbxMoneda.GetValue & " And Credito=1"
        End If

        If cbxVentas.txt.Text = "Contado" Then
            'Filtrar por Fecha, Moneda y Tipo de Pago Contado
            Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And IDMoneda=" & cbxMoneda.GetValue & " And Credito=0"
        End If

        If Not txtProducto.dtProductosSeleccionados Is Nothing Then
            If txtProducto.dtProductosSeleccionados.Rows.Count = 0 Then
                txtProducto.dtProductosSeleccionados.Reset()
            End If
        End If

        'Producto
        If chkProducto.Valor = True Then
            If txtProducto.Seleccionado = False And (txtProducto.dtProductosSeleccionados Is Nothing) Then
                Exit Sub
            End If

            If Not txtProducto.dtProductosSeleccionados Is Nothing Then

                If txtProducto.dtProductosSeleccionados.Rows.Count > 0 Then

                    Dim IDProductoIN As String = " IDProducto In (0"


                    For Each dRow As DataRow In txtProducto.dtProductosSeleccionados.Rows
                        IDProductoIN = IDProductoIN & ", " & dRow("ID")
                    Next
                    IDProductoIN = IDProductoIN & ")"

                    Where = IIf(String.IsNullOrEmpty(Where), " where " & IDProductoIN, Where & "And " & IDProductoIN)

                    GoTo Seguir

                End If

            End If

            Where = Where & " And IDProducto=" & txtProducto.Registro("ID") & " "

        End If
Seguir:

        'Incluir Sin Control
        If chkIncluirProductosSinControl.Valor = False Then
            Where = Where & " and ControlarExistencia = 'True' "
        End If

        'Incluir Anulados
        If chkIncluirAnulados.Valor = False Then
            Where = Where & " And Anulado = 'False'"
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        'Moneda
        If cbxMoneda.cbx.Text <> "" Then
            Moneda = "Moneda (" & cbxMoneda.cbx.Text & ")"
        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, nudRanking, cbxVentas, cbxMoneda, cbxOrdenadoPor, cbxEnForma, txtDesde, txtHasta)
        If chkCliente.chk.Checked Then
            If Where = "" Then
                Where = " Where IDCliente = " & txtCliente.Registro("ID") & " "
            Else
                Where = Where & " And IDCliente = " & txtCliente.Registro("ID") & " "
            End If

        End If



        'Seleccion de informe por valor o cantidad
        If rdbCantidad.Checked = True Then
            EsCantidad = True
        Else
            EsCantidad = False
        End If

        'Seleccinar informe
        Select Case cbxSeleccionInforme.cbx.SelectedIndex
            Case 0
                SeleccionInforme = "rptListadoTotalAnualCliente"
                Titulo = cbxSeleccionInforme.cbx.Text
            Case 1
                SeleccionInforme = "rptListadoTotalAnualTipoCliente"
                Titulo = cbxSeleccionInforme.cbx.Text
            Case 2
                SeleccionInforme = "rptListadoTotalAnualClienteProducto"
                Titulo = cbxSeleccionInforme.cbx.Text
            Case 3
                SeleccionInforme = "rptListadoTotalAnualProductoClienteOrdenadoPorTotal"
                Titulo = cbxSeleccionInforme.cbx.Text
        End Select

        CReporte.ListadoVentasAnualTotalProductoCliente(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, cbxVentas.cbx.Text, txtDesde.GetValueString, txtHasta.GetValueString, cbxMoneda.cbx.Text, EsCantidad, chkDevoluciones.Valor, chkSucursalCliente.Valor, SeleccionInforme)

    End Sub

    Sub CargarInformacion()

        'Producto
        txtProducto.Conectar()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Ventas
        cbxVentas.cbx.Items.Add("Contado + Crédito")
        cbxVentas.cbx.Items.Add("Contado")
        cbxVentas.cbx.Items.Add("Crédito")
        'Tipo de Venta
        cbxVentas.cbx.SelectedIndex = 0

        'Tipo Informe
        cbxSeleccionInforme.cbx.Items.Add("Ventas Anuales por Cliente")
        cbxSeleccionInforme.cbx.Items.Add("Ventas Anuales por Tipo Cliente")
        cbxSeleccionInforme.cbx.Items.Add("Ventas Anuales Cliente/Producto")
        cbxSeleccionInforme.cbx.Items.Add("Ventas Anuales Producto/Cliente")

        'Tipo Informe
        cbxSeleccionInforme.cbx.SelectedIndex = 0

        'Proveedor
        CSistema.SqlToComboBox(cbxProveedor, "select Distinct IDProveedor, Proveedor from VProducto")

        CambiarOrdenacion()

        'Configuracion
        cbxSeleccionInforme.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "REPORTE", "")
        cbxOrdenadoPor.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ORDENADO", "")
        cbxEnForma.cbx.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "ENFORMA", "")
        nudRanking.Text = CArchivoInicio.IniGet(VGArchivoINI, Me.Name, "RANKING", "")


        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

        txtProducto.SeleccionMultiple = True
        txtProducto.Conectar()


    End Sub

    Sub GuardarInformacion()

        'Configuracion
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "REPORTE", cbxSeleccionInforme.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ORDENADO", cbxOrdenadoPor.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "MONEDA", cbxMoneda.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "ENFORMA", cbxEnForma.cbx.Text)
        CArchivoInicio.IniWrite(VGArchivoINI, Me.Name, "RANKING", nudRanking.Text)

    End Sub

    Sub CambiarOrdenacion()

        cbxEnForma.cbx.Text = ""
        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VVentaTotalAnualProductoCliente", "Select Top(0) Producto, Cliente, Fecha From VVentaTotalAnualProductoCliente ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmListadoVentasAnualesTotalesCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkZonaVenta.PropertyChanged
        cbxZonaVenta.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkSubLinea2_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSubLinea2.PropertyChanged
        cbxSubLinea2.Enabled = value
    End Sub

    Private Sub chkSubLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSubLinea.PropertyChanged
        cbxSubLinea.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    Private Sub chkMarca_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
    End Sub

    Private Sub chkPresentacion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkPresentacion.PropertyChanged
        cbxPresentacion.Enabled = value
    End Sub

    Private Sub chkCategoria_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCategoria.PropertyChanged
        cbxCategoria.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProveedor.PropertyChanged
        cbxProveedor.Enabled = value
    End Sub

    Private Sub chkDivision_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDivision.PropertyChanged
        cbxDivision.Enabled = value
    End Sub

    Private Sub chkProcedencia_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProcedencia.PropertyChanged
        cbxProcedencia.Enabled = value
    End Sub

End Class
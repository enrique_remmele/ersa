﻿Imports ERP.Reporte
Public Class frmListadoRemision
    'CLASES
    Dim CReporte As New CReporteRemision
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE REMISIONES"
    Dim TipoInforme As String
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()
        CargarInformacion()

        CambiarOrdenacion()
        cbxTipoInforme.cbx.SelectedIndex = 0
        txtDesde.Hoy()
        txtHasta.Hoy()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
        cbxEstado.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipos de Informes
        cbxEstado.cbx.Items.Add("TODOS")
        cbxEstado.cbx.Items.Add("ACTIVOS")
        cbxEstado.cbx.Items.Add("ANULADOS")
        cbxEstado.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        cbxSucursal.Conectar()

        ReDim vControles(-1)
        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        TipoInforme = ""
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        
            'Filtrar por Fecha
        Where = " Where FechaInicio Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next
        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        If cbxEstado.cbx.SelectedIndex = 1 Then
            Where = Where & " and Anulado = 'False'"
            TipoInforme = TipoInforme & " - ACTIVOS"
        ElseIf cbxEstado.cbx.SelectedIndex = 2 Then
            Where = Where & " and Anulado = 'True'"
            TipoInforme = TipoInforme & " - ANULADOS"
        End If
        If cbxTipoInforme.cbx.SelectedIndex = 0 Then
            CReporte.ListadoRemision(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        Else
            CReporte.ListadpRemisionDetallada(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        End If

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("vRemision", "Select Top(0) fechaInicio, Sucursal, Comprobante, Chofer,Camion, Distribuidor From vRemision ")



        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkDistribuidor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkDistribuidor.PropertyChanged
        cbxDistribuidor.Enabled = value
    End Sub

    Private Sub chkVehiculo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVehiculo.PropertyChanged
        cbxVehiculo.Enabled = value
    End Sub

    Private Sub chkChofer_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComisionesporVendedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.chkProducto = New ERP.ocxCHK()
        Me.cbxProducto = New ERP.ocxCBX()
        Me.chkMarca = New ERP.ocxCHK()
        Me.cbxMarca = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxCliente = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.cbxVentas = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnInforme = New System.Windows.Forms.Button()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(6, 230)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 13
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 15)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(11, 176)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 9
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(12, 215)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 12
        Me.lblRanking.Text = "Ranking:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Moneda:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(522, 264)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 2
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMarca)
        Me.gbxFiltro.Controls.Add(Me.cbxMarca)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxCliente)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(348, 297)
        Me.gbxFiltro.TabIndex = 3
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(10, 214)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(88, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 14
        Me.chkTipoCliente.Texto = "Tipo Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "TipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(129, 216)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 15
        Me.cbxTipoCliente.Texto = "DESPENSA"
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(10, 260)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(101, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 18
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'cbxProducto
        '
        Me.cbxProducto.CampoWhere = "ID"
        Me.cbxProducto.CargarUnaSolaVez = False
        Me.cbxProducto.DataDisplayMember = "Descripcion"
        Me.cbxProducto.DataFilter = Nothing
        Me.cbxProducto.DataOrderBy = "Descripcion"
        Me.cbxProducto.DataSource = "VProducto"
        Me.cbxProducto.DataValueMember = "ID"
        Me.cbxProducto.Enabled = False
        Me.cbxProducto.FormABM = Nothing
        Me.cbxProducto.Indicaciones = Nothing
        Me.cbxProducto.Location = New System.Drawing.Point(129, 260)
        Me.cbxProducto.Name = "cbxProducto"
        Me.cbxProducto.SeleccionObligatoria = False
        Me.cbxProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxProducto.SoloLectura = False
        Me.cbxProducto.TabIndex = 19
        Me.cbxProducto.Texto = "AC. TRIMESTRAL SET/10 (50%)"
        '
        'chkMarca
        '
        Me.chkMarca.BackColor = System.Drawing.Color.Transparent
        Me.chkMarca.Color = System.Drawing.Color.Empty
        Me.chkMarca.Location = New System.Drawing.Point(10, 180)
        Me.chkMarca.Name = "chkMarca"
        Me.chkMarca.Size = New System.Drawing.Size(101, 21)
        Me.chkMarca.SoloLectura = False
        Me.chkMarca.TabIndex = 12
        Me.chkMarca.Texto = "Marca:"
        Me.chkMarca.Valor = False
        '
        'cbxMarca
        '
        Me.cbxMarca.CampoWhere = "IDMarca"
        Me.cbxMarca.CargarUnaSolaVez = False
        Me.cbxMarca.DataDisplayMember = "Descripcion"
        Me.cbxMarca.DataFilter = Nothing
        Me.cbxMarca.DataOrderBy = "Descripcion"
        Me.cbxMarca.DataSource = "Marca"
        Me.cbxMarca.DataValueMember = "ID"
        Me.cbxMarca.Enabled = False
        Me.cbxMarca.FormABM = Nothing
        Me.cbxMarca.Indicaciones = Nothing
        Me.cbxMarca.Location = New System.Drawing.Point(130, 177)
        Me.cbxMarca.Name = "cbxMarca"
        Me.cbxMarca.SeleccionObligatoria = False
        Me.cbxMarca.Size = New System.Drawing.Size(213, 21)
        Me.cbxMarca.SoloLectura = False
        Me.cbxMarca.TabIndex = 13
        Me.cbxMarca.Texto = "ADES"
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(10, 131)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(101, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 8
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "TipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(130, 133)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 9
        Me.cbxTipoProducto.Texto = "COMESTIBLE"
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(10, 155)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(101, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 10
        Me.chkLinea.Texto = "Linea:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "Linea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(130, 155)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(213, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 11
        Me.cbxLinea.Texto = "BEBIDAS"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(10, 237)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 16
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxCliente
        '
        Me.cbxCliente.CampoWhere = "IDCliente"
        Me.cbxCliente.CargarUnaSolaVez = False
        Me.cbxCliente.DataDisplayMember = "RazonSocial"
        Me.cbxCliente.DataFilter = Nothing
        Me.cbxCliente.DataOrderBy = "RazonSocial"
        Me.cbxCliente.DataSource = "VCliente"
        Me.cbxCliente.DataValueMember = "ID"
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormABM = Nothing
        Me.cbxCliente.Indicaciones = Nothing
        Me.cbxCliente.Location = New System.Drawing.Point(129, 238)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.SeleccionObligatoria = False
        Me.cbxCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxCliente.SoloLectura = False
        Me.cbxCliente.TabIndex = 17
        Me.cbxCliente.Texto = """AJE"" COMUNICACIONES"
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(10, 99)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(114, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 6
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(130, 99)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(213, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 7
        Me.cbxDeposito.Texto = "AVERIADOS"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(10, 77)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 4
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(130, 77)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 5
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(10, 55)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(114, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 2
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudadCliente"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Descripcion"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VCiudad"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(130, 55)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(213, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 3
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(9, 18)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(114, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 0
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "VVendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = "frmVendedor"
        Me.cbxVendedor.Indicaciones = "Seleccion de Vendedor"
        Me.cbxVendedor.Location = New System.Drawing.Point(129, 18)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(213, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 1
        Me.cbxVendedor.Texto = "ALDO GONZALES"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxTipoInforme)
        Me.GroupBox2.Controls.Add(Me.lblInforme)
        Me.GroupBox2.Controls.Add(Me.cbxVentas)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxMoneda)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(356, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 255)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(6, 151)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionObligatoria = False
        Me.cbxTipoInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 8
        Me.cbxTipoInforme.Texto = ""
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(10, 134)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(45, 13)
        Me.lblInforme.TabIndex = 7
        Me.lblInforme.Text = "Informe:"
        '
        'cbxVentas
        '
        Me.cbxVentas.CampoWhere = Nothing
        Me.cbxVentas.CargarUnaSolaVez = False
        Me.cbxVentas.DataDisplayMember = Nothing
        Me.cbxVentas.DataFilter = Nothing
        Me.cbxVentas.DataOrderBy = Nothing
        Me.cbxVentas.DataSource = Nothing
        Me.cbxVentas.DataValueMember = Nothing
        Me.cbxVentas.FormABM = Nothing
        Me.cbxVentas.Indicaciones = Nothing
        Me.cbxVentas.Location = New System.Drawing.Point(6, 72)
        Me.cbxVentas.Name = "cbxVentas"
        Me.cbxVentas.SeleccionObligatoria = False
        Me.cbxVentas.Size = New System.Drawing.Size(157, 21)
        Me.cbxVentas.SoloLectura = False
        Me.cbxVentas.TabIndex = 4
        Me.cbxVentas.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Ventas:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = "ID"
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(6, 111)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(157, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 6
        Me.cbxMoneda.Texto = "DOLARES E.E.U.U."
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 193)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 11
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 192)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 10
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 26, 10, 51, 30, 97)
        Me.txtHasta.Location = New System.Drawing.Point(89, 34)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 26, 10, 51, 30, 108)
        Me.txtDesde.Location = New System.Drawing.Point(6, 34)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(360, 264)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 1
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmComisionesporVendedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(601, 309)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmComisionesporVendedor"
        Me.Text = "frmComisionesporVendedor"
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbxVentas As ERP.ocxCBX
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkMarca As ERP.ocxCHK
    Friend WithEvents cbxMarca As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkLinea As ERP.ocxCHK
    Friend WithEvents cbxLinea As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents cbxCliente As ERP.ocxCBX
    Friend WithEvents cbxTipoInforme As ERP.ocxCBX
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents cbxProducto As ERP.ocxCBX
    Friend WithEvents chkTipoCliente As ERP.ocxCHK
    Friend WithEvents cbxTipoCliente As ERP.ocxCBX
End Class

﻿Imports ERP.Reporte
Public Class frmComisionesporVendedor

    'CLASES
    Dim CReporte As New CReporteComisionVendedor
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = ""
    Dim TipoInforme As String = ""

    Sub Inicializar()
        Me.AcceptButton = New Button
        Me.KeyPreview = True
        CargarInformacion()
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Vendedor As String = ""
        Dim GroupBy As String = ""


        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If cbxVentas.txt.Text = "Contado + Crédito" Then
            'Filtrar por Fecha, Moneda y Tipo de Pago Contado y Crédito
            Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'And IDMoneda=" & cbxMoneda.GetValue & ""
        End If

        If cbxVentas.txt.Text = "Crédito" Then
            'Filtrar por Fecha, Moneda y Tipo de Pago Crédito
            Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And IDMoneda=" & cbxMoneda.GetValue & " And Credito=1"
        End If

        If cbxVentas.txt.Text = "Contado" Then
            'Filtrar por Fecha, Moneda y Tipo de Pago Contado
            Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' And IDMoneda=" & cbxMoneda.GetValue & " And Credito=0"
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        Titulo = "COMISIONES DE VENDEDORES "
        TipoInforme = "Del " & txtDesde.GetValueString & " al " & txtHasta.GetValueString
        If chkVendedor.chk.Checked = True Then
            TipoInforme = TipoInforme & " VENDEDOR : " & cbxVendedor.cbx.Text
        End If

        GroupBy = " ," & cbxOrdenadoPor.cbx.Text

        Select Case cbxTipoInforme.cbx.SelectedIndex
            Case 0
                CReporte.ListadoComision(frm, Where, OrderBy, Top, Titulo, TipoInforme)
            Case 1
                CReporte.ListadoComisionDetalle(frm, Where, OrderBy, Top, vgUsuarioIdentificador, Titulo, TipoInforme)
        End Select

    End Sub
    Sub CargarInformacion()



        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Ventas
        cbxVentas.cbx.Items.Add("Contado + Crédito")
        cbxVentas.cbx.Items.Add("Contado")
        cbxVentas.cbx.Items.Add("Crédito")
        cbxVentas.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'Tipo de Venta
        cbxVentas.cbx.SelectedIndex = 0
        cbxTipoInforme.cbx.SelectedIndex = 0

        CambiarOrdenacion()

    End Sub
    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VComisionVendedor", "Select Top(0) Vendedor, Cliente, TipoProducto, Producto   From VComisionVendedor ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmListadoVentasTotalesProductoCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        'cbxTipoComprobante.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkZonaVenta_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean)
        'cbxZonaVenta.Enabled = value
    End Sub

   

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub frmComisionesporVendedor_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkLinea_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub

    
    Private Sub chkMarca_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkMarca.PropertyChanged
        cbxMarca.Enabled = value
    End Sub

    
    Private Sub chkProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProducto.PropertyChanged
        cbxProducto.Enabled = value
    End Sub

    Private Sub chkTipoCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub
End Class


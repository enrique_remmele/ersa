﻿Imports ERP.Reporte
Public Class frmVentasExcel

    'CLASES
    Dim CReporte As New CReporteVentas
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES
    Private CadenaConexionValue As String
    Public Property CadenaConexion() As String
        Get
            Return CadenaConexionValue
        End Get
        Set(ByVal value As String)
            CadenaConexionValue = value
        End Set
    End Property

    Sub Inicializar()

        'Form 
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        'Fecha
        txtDesde.PrimerDiaMes()
        txtHasta.UltimoDiaMes()

    End Sub

    Sub Listar()

        Dim WhereDetalle As String = ""
       
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        WhereDetalle = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "'"

        CReporte.ListadoVentasParaExcel(frm, WhereDetalle, "rptVentasParaExcel")

    End Sub

    Private Sub frmVentasExcel_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(sender As Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmListadoAbastecimientoSucursalDetallado
    'CLASES
    Dim CReporte As New CPedido
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim FormaPago As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
        txtDesde.PrimerDiaMes()
        txtPedidoDesde.PrimerDiaMes()
        txtHasta.Hoy()
        txtPedidoHasta.Hoy()
    End Sub

    Sub CargarInformacion()

        txtCliente.Conectar()

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        For Each ctr As Object In gbxFiltro.Controls

            ''Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If
            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
siguiente:

        Next

    End Sub

    Sub Listar()

        Dim Where As String = " where 1 = 1 "
        Dim whereDetalle As String = " where 1 = 1"
        Dim OrderBy As String = ""
        Dim Titulo As String = ""
        Dim Subtitulo As String = ""

        If rdbAbastecimiento.Checked Then
            Titulo = "Listado Abastecimientos Detallados"
        Else
            Titulo = "Listado Entregas de Pedidos Detallados"
        End If
        FormaPago = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtra por fecha
        'Where = "Where Fecha between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'  "
        If CHKPeriodo.Valor = True Then
            Where = Where & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False) & "'  "
        End If

        If chkPeriodoPedido.Valor = True Then
            whereDetalle = whereDetalle & " and Cast(FechaEntrega as date) between '" & CSistema.FormatoFechaBaseDatos(txtPedidoDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtPedidoHasta.GetValue, True, False) & "'  "
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltroDetalle.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(whereDetalle)
            End If
siguiente1:

        Next


        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If rdbAbastecimiento.Checked = True Then
            Where = Where & " and PedidoCliente = 'False' "
        End If

        If rdbPedidoCliente.Checked = True Then
            Where = Where & " and PedidoCliente = 'True' "
        End If

        If chkCliente.chk.Checked Then
            If txtCliente.Seleccionado Then
                whereDetalle = whereDetalle & " and IDCliente = " & txtCliente.Registro("ID")
            End If

        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        ArmarSubTitulo(Subtitulo)
        CReporte.ListadoAbastecimientoDetallado(frm, Where, whereDetalle, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo, txtDesde.txt.Text, txtHasta.txt.Text)
    End Sub

    Sub ArmarSubTitulo(ByRef Subtitulo As String)
        Subtitulo = "Fecha: " & txtDesde.GetValue & " - " & txtHasta.GetValue & " "

    End Sub


    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Pedido", " Select Top (0) Numero, Ciudad, Sucursal , Cliente , Deposito from VPedido ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub


    Private Sub frmPedidos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkChofer_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub

    Private Sub chkCamion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCamion.PropertyChanged
        cbxCamion.Enabled = value
    End Sub

    Private Sub CHKPeriodo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles CHKPeriodo.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkSucursalDestino_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursalDestino.PropertyChanged
        cbxSucursalDestino.Enabled = value
    End Sub

    Private Sub chkUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkSucursalPedido_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursalPedido.PropertyChanged
        cbxSucursalPedido.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub rdbPedidoCliente_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdbPedidoCliente.CheckedChanged
        chkSucursalPedido.Enabled = rdbPedidoCliente.Checked
        chkUsuario.Enabled = rdbPedidoCliente.Checked
        chkVendedor.Enabled = rdbPedidoCliente.Checked
        chkCliente.Enabled = rdbPedidoCliente.Checked
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub rdbAbastecimiento_CheckedChanged(sender As Object, e As EventArgs) Handles rdbAbastecimiento.CheckedChanged
        chkSucursal.Enabled = rdbAbastecimiento.Checked
        chkSucursalDestino.Enabled = rdbAbastecimiento.Checked
        chkChofer.Enabled = rdbAbastecimiento.Checked
        chkCamion.Enabled = rdbAbastecimiento.Checked
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoAbastecimientoSucursalDetallado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkSucursalDestino = New ERP.ocxCHK()
        Me.cbxSucursalDestino = New ERP.ocxCBX()
        Me.chkCamion = New ERP.ocxCHK()
        Me.cbxCamion = New ERP.ocxCBX()
        Me.chkChofer = New ERP.ocxCHK()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.CHKPeriodo = New ERP.ocxCHK()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkPeriodoPedido = New ERP.ocxCHK()
        Me.txtPedidoHasta = New ERP.ocxTXTDate()
        Me.txtPedidoDesde = New ERP.ocxTXTDate()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rdbPedidoCliente = New System.Windows.Forms.RadioButton()
        Me.rdbAbastecimiento = New System.Windows.Forms.RadioButton()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.gbxFiltroDetalle = New System.Windows.Forms.GroupBox()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkSucursalPedido = New ERP.ocxCHK()
        Me.cbxSucursalPedido = New ERP.ocxCBX()
        Me.chkUsuario = New ERP.ocxCHK()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbxFiltroDetalle.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSucursalDestino)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursalDestino)
        Me.gbxFiltro.Controls.Add(Me.chkCamion)
        Me.gbxFiltro.Controls.Add(Me.cbxCamion)
        Me.gbxFiltro.Controls.Add(Me.chkChofer)
        Me.gbxFiltro.Controls.Add(Me.cbxChofer)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(453, 134)
        Me.gbxFiltro.TabIndex = 35
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros de Abastecimiento"
        '
        'chkSucursalDestino
        '
        Me.chkSucursalDestino.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalDestino.Color = System.Drawing.Color.Empty
        Me.chkSucursalDestino.Location = New System.Drawing.Point(7, 50)
        Me.chkSucursalDestino.Name = "chkSucursalDestino"
        Me.chkSucursalDestino.Size = New System.Drawing.Size(106, 21)
        Me.chkSucursalDestino.SoloLectura = False
        Me.chkSucursalDestino.TabIndex = 38
        Me.chkSucursalDestino.Texto = "Sucursal Destino:"
        Me.chkSucursalDestino.Valor = False
        '
        'cbxSucursalDestino
        '
        Me.cbxSucursalDestino.CampoWhere = "IDSucursalAbastecer"
        Me.cbxSucursalDestino.CargarUnaSolaVez = False
        Me.cbxSucursalDestino.DataDisplayMember = "Descripcion"
        Me.cbxSucursalDestino.DataFilter = Nothing
        Me.cbxSucursalDestino.DataOrderBy = "Descripcion"
        Me.cbxSucursalDestino.DataSource = "vSucursal"
        Me.cbxSucursalDestino.DataValueMember = "ID"
        Me.cbxSucursalDestino.dtSeleccionado = Nothing
        Me.cbxSucursalDestino.Enabled = False
        Me.cbxSucursalDestino.FormABM = Nothing
        Me.cbxSucursalDestino.Indicaciones = Nothing
        Me.cbxSucursalDestino.Location = New System.Drawing.Point(119, 47)
        Me.cbxSucursalDestino.Name = "cbxSucursalDestino"
        Me.cbxSucursalDestino.SeleccionMultiple = False
        Me.cbxSucursalDestino.SeleccionObligatoria = False
        Me.cbxSucursalDestino.Size = New System.Drawing.Size(316, 21)
        Me.cbxSucursalDestino.SoloLectura = False
        Me.cbxSucursalDestino.TabIndex = 39
        Me.cbxSucursalDestino.Texto = "MOLINO"
        '
        'chkCamion
        '
        Me.chkCamion.BackColor = System.Drawing.Color.Transparent
        Me.chkCamion.Color = System.Drawing.Color.Empty
        Me.chkCamion.Location = New System.Drawing.Point(7, 104)
        Me.chkCamion.Name = "chkCamion"
        Me.chkCamion.Size = New System.Drawing.Size(75, 21)
        Me.chkCamion.SoloLectura = False
        Me.chkCamion.TabIndex = 36
        Me.chkCamion.Texto = "Camion"
        Me.chkCamion.Valor = False
        '
        'cbxCamion
        '
        Me.cbxCamion.CampoWhere = "IDCamion"
        Me.cbxCamion.CargarUnaSolaVez = False
        Me.cbxCamion.DataDisplayMember = "descripcion"
        Me.cbxCamion.DataFilter = Nothing
        Me.cbxCamion.DataOrderBy = "Descripcion"
        Me.cbxCamion.DataSource = "vCamion"
        Me.cbxCamion.DataValueMember = "ID"
        Me.cbxCamion.dtSeleccionado = Nothing
        Me.cbxCamion.Enabled = False
        Me.cbxCamion.FormABM = Nothing
        Me.cbxCamion.Indicaciones = Nothing
        Me.cbxCamion.Location = New System.Drawing.Point(119, 101)
        Me.cbxCamion.Name = "cbxCamion"
        Me.cbxCamion.SeleccionMultiple = False
        Me.cbxCamion.SeleccionObligatoria = False
        Me.cbxCamion.Size = New System.Drawing.Size(316, 21)
        Me.cbxCamion.SoloLectura = False
        Me.cbxCamion.TabIndex = 37
        Me.cbxCamion.Texto = ""
        '
        'chkChofer
        '
        Me.chkChofer.BackColor = System.Drawing.Color.Transparent
        Me.chkChofer.Color = System.Drawing.Color.Empty
        Me.chkChofer.Location = New System.Drawing.Point(7, 77)
        Me.chkChofer.Name = "chkChofer"
        Me.chkChofer.Size = New System.Drawing.Size(75, 21)
        Me.chkChofer.SoloLectura = False
        Me.chkChofer.TabIndex = 34
        Me.chkChofer.Texto = "Chofer"
        Me.chkChofer.Valor = False
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = "IDChofer"
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "vChofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.dtSeleccionado = Nothing
        Me.cbxChofer.Enabled = False
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(119, 74)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionMultiple = False
        Me.cbxChofer.SeleccionObligatoria = False
        Me.cbxChofer.Size = New System.Drawing.Size(316, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 35
        Me.cbxChofer.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(7, 23)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(75, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 32
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "vSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(119, 20)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(316, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 33
        Me.cbxSucursal.Texto = "MOLINO"
        '
        'CHKPeriodo
        '
        Me.CHKPeriodo.BackColor = System.Drawing.Color.Transparent
        Me.CHKPeriodo.Color = System.Drawing.Color.Empty
        Me.CHKPeriodo.Location = New System.Drawing.Point(11, 16)
        Me.CHKPeriodo.Name = "CHKPeriodo"
        Me.CHKPeriodo.Size = New System.Drawing.Size(165, 21)
        Me.CHKPeriodo.SoloLectura = False
        Me.CHKPeriodo.TabIndex = 36
        Me.CHKPeriodo.Texto = "Periodo Documento"
        Me.CHKPeriodo.Valor = True
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(131, 145)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(45, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 32
        Me.cbxEnForma.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(595, 331)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 38
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(471, 331)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(105, 23)
        Me.btnInforme.TabIndex = 37
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(12, 145)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(113, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 31
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesde.Location = New System.Drawing.Point(12, 44)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkPeriodoPedido)
        Me.GroupBox2.Controls.Add(Me.txtPedidoHasta)
        Me.GroupBox2.Controls.Add(Me.txtPedidoDesde)
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Controls.Add(Me.CHKPeriodo)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(471, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(187, 249)
        Me.GroupBox2.TabIndex = 36
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkPeriodoPedido
        '
        Me.chkPeriodoPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkPeriodoPedido.Color = System.Drawing.Color.Empty
        Me.chkPeriodoPedido.Location = New System.Drawing.Point(11, 74)
        Me.chkPeriodoPedido.Name = "chkPeriodoPedido"
        Me.chkPeriodoPedido.Size = New System.Drawing.Size(165, 21)
        Me.chkPeriodoPedido.SoloLectura = False
        Me.chkPeriodoPedido.TabIndex = 40
        Me.chkPeriodoPedido.Texto = "Periodo a Entregar"
        Me.chkPeriodoPedido.Valor = True
        '
        'txtPedidoHasta
        '
        Me.txtPedidoHasta.AñoFecha = 0
        Me.txtPedidoHasta.Color = System.Drawing.Color.Empty
        Me.txtPedidoHasta.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtPedidoHasta.Location = New System.Drawing.Point(92, 102)
        Me.txtPedidoHasta.MesFecha = 0
        Me.txtPedidoHasta.Name = "txtPedidoHasta"
        Me.txtPedidoHasta.PermitirNulo = False
        Me.txtPedidoHasta.Size = New System.Drawing.Size(84, 20)
        Me.txtPedidoHasta.SoloLectura = False
        Me.txtPedidoHasta.TabIndex = 39
        '
        'txtPedidoDesde
        '
        Me.txtPedidoDesde.AñoFecha = 0
        Me.txtPedidoDesde.Color = System.Drawing.Color.Empty
        Me.txtPedidoDesde.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtPedidoDesde.Location = New System.Drawing.Point(12, 102)
        Me.txtPedidoDesde.MesFecha = 0
        Me.txtPedidoDesde.Name = "txtPedidoDesde"
        Me.txtPedidoDesde.PermitirNulo = False
        Me.txtPedidoDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtPedidoDesde.SoloLectura = False
        Me.txtPedidoDesde.TabIndex = 38
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.rdbPedidoCliente)
        Me.Panel1.Controls.Add(Me.rdbAbastecimiento)
        Me.Panel1.Location = New System.Drawing.Point(12, 171)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(164, 70)
        Me.Panel1.TabIndex = 37
        '
        'rdbPedidoCliente
        '
        Me.rdbPedidoCliente.AutoSize = True
        Me.rdbPedidoCliente.Location = New System.Drawing.Point(11, 38)
        Me.rdbPedidoCliente.Name = "rdbPedidoCliente"
        Me.rdbPedidoCliente.Size = New System.Drawing.Size(108, 17)
        Me.rdbPedidoCliente.TabIndex = 1
        Me.rdbPedidoCliente.Text = "Pedido de Cliente"
        Me.rdbPedidoCliente.UseVisualStyleBackColor = True
        '
        'rdbAbastecimiento
        '
        Me.rdbAbastecimiento.AutoSize = True
        Me.rdbAbastecimiento.Checked = True
        Me.rdbAbastecimiento.Location = New System.Drawing.Point(11, 10)
        Me.rdbAbastecimiento.Name = "rdbAbastecimiento"
        Me.rdbAbastecimiento.Size = New System.Drawing.Size(97, 17)
        Me.rdbAbastecimiento.TabIndex = 0
        Me.rdbAbastecimiento.TabStop = True
        Me.rdbAbastecimiento.Text = "Abastecimiento"
        Me.rdbAbastecimiento.UseVisualStyleBackColor = True
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(12, 127)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 30
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHasta.Location = New System.Drawing.Point(92, 44)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(84, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'gbxFiltroDetalle
        '
        Me.gbxFiltroDetalle.Controls.Add(Me.txtCliente)
        Me.gbxFiltroDetalle.Controls.Add(Me.chkCliente)
        Me.gbxFiltroDetalle.Controls.Add(Me.chkVendedor)
        Me.gbxFiltroDetalle.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltroDetalle.Controls.Add(Me.chkSucursalPedido)
        Me.gbxFiltroDetalle.Controls.Add(Me.cbxSucursalPedido)
        Me.gbxFiltroDetalle.Controls.Add(Me.chkUsuario)
        Me.gbxFiltroDetalle.Controls.Add(Me.cbxUsuario)
        Me.gbxFiltroDetalle.Location = New System.Drawing.Point(12, 162)
        Me.gbxFiltroDetalle.Name = "gbxFiltroDetalle"
        Me.gbxFiltroDetalle.Size = New System.Drawing.Size(453, 168)
        Me.gbxFiltroDetalle.TabIndex = 38
        Me.gbxFiltroDetalle.TabStop = False
        Me.gbxFiltroDetalle.Text = "Filtros de Pedido"
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Enabled = False
        Me.chkVendedor.Location = New System.Drawing.Point(7, 80)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(71, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 34
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "Vendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(119, 78)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(316, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 35
        Me.cbxVendedor.Texto = ""
        '
        'chkSucursalPedido
        '
        Me.chkSucursalPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalPedido.Color = System.Drawing.Color.Empty
        Me.chkSucursalPedido.Enabled = False
        Me.chkSucursalPedido.Location = New System.Drawing.Point(7, 50)
        Me.chkSucursalPedido.Name = "chkSucursalPedido"
        Me.chkSucursalPedido.Size = New System.Drawing.Size(75, 21)
        Me.chkSucursalPedido.SoloLectura = False
        Me.chkSucursalPedido.TabIndex = 32
        Me.chkSucursalPedido.Texto = "Sucursal:"
        Me.chkSucursalPedido.Valor = False
        '
        'cbxSucursalPedido
        '
        Me.cbxSucursalPedido.CampoWhere = "IDSucursal"
        Me.cbxSucursalPedido.CargarUnaSolaVez = False
        Me.cbxSucursalPedido.DataDisplayMember = "Descripcion"
        Me.cbxSucursalPedido.DataFilter = Nothing
        Me.cbxSucursalPedido.DataOrderBy = "Descripcion"
        Me.cbxSucursalPedido.DataSource = "vSucursal"
        Me.cbxSucursalPedido.DataValueMember = "ID"
        Me.cbxSucursalPedido.dtSeleccionado = Nothing
        Me.cbxSucursalPedido.Enabled = False
        Me.cbxSucursalPedido.FormABM = Nothing
        Me.cbxSucursalPedido.Indicaciones = Nothing
        Me.cbxSucursalPedido.Location = New System.Drawing.Point(119, 47)
        Me.cbxSucursalPedido.Name = "cbxSucursalPedido"
        Me.cbxSucursalPedido.SeleccionMultiple = False
        Me.cbxSucursalPedido.SeleccionObligatoria = False
        Me.cbxSucursalPedido.Size = New System.Drawing.Size(316, 21)
        Me.cbxSucursalPedido.SoloLectura = False
        Me.cbxSucursalPedido.TabIndex = 33
        Me.cbxSucursalPedido.Texto = "MOLINO"
        '
        'chkUsuario
        '
        Me.chkUsuario.BackColor = System.Drawing.Color.Transparent
        Me.chkUsuario.Color = System.Drawing.Color.Empty
        Me.chkUsuario.Enabled = False
        Me.chkUsuario.Location = New System.Drawing.Point(7, 24)
        Me.chkUsuario.Name = "chkUsuario"
        Me.chkUsuario.Size = New System.Drawing.Size(75, 21)
        Me.chkUsuario.SoloLectura = False
        Me.chkUsuario.TabIndex = 30
        Me.chkUsuario.Texto = "Usuario:"
        Me.chkUsuario.Valor = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuarioPedido"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "vUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(119, 21)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(316, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 31
        Me.cbxUsuario.Texto = "CAST"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Enabled = False
        Me.chkCliente.Location = New System.Drawing.Point(7, 108)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(95, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 36
        Me.chkCliente.Texto = "Cliente"
        Me.chkCliente.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 0
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(7, 136)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(428, 56)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 37
        '
        'frmListadoAbastecimientoSucursalDetallado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(677, 367)
        Me.Controls.Add(Me.gbxFiltroDetalle)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmListadoAbastecimientoSucursalDetallado"
        Me.Text = "frmListadoAbastecimientoSucursalDetallado"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbxFiltroDetalle.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkCamion As ERP.ocxCHK
    Friend WithEvents cbxCamion As ERP.ocxCBX
    Friend WithEvents chkChofer As ERP.ocxCHK
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents CHKPeriodo As ERP.ocxCHK
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents rdbPedidoCliente As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAbastecimiento As System.Windows.Forms.RadioButton
    Friend WithEvents chkSucursalDestino As ERP.ocxCHK
    Friend WithEvents cbxSucursalDestino As ERP.ocxCBX
    Friend WithEvents gbxFiltroDetalle As System.Windows.Forms.GroupBox
    Friend WithEvents chkSucursalPedido As ERP.ocxCHK
    Friend WithEvents cbxSucursalPedido As ERP.ocxCBX
    Friend WithEvents chkUsuario As ERP.ocxCHK
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents chkPeriodoPedido As ERP.ocxCHK
    Friend WithEvents txtPedidoHasta As ERP.ocxTXTDate
    Friend WithEvents txtPedidoDesde As ERP.ocxTXTDate
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents chkCliente As ocxCHK
End Class

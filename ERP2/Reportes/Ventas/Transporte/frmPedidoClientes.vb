﻿Imports ERP.Reporte
Public Class frmPedidoClientes
    'CLASES
    Dim CReporte As New CPedido
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim FormaPago As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
        txtDesde.Hoy()
        txtHasta.Hoy()
        txtDesdePedido.Hoy()
        txtHastaPedido.Hoy()
        cbxSucursal.Texto = "MOLINO"
    End Sub

    Sub CargarInformacion()

        txtCliente.Conectar()

        ''Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("POR PEDIDO DEL CLIENTE")
        'cbxTipoInforme.cbx.SelectedIndex = 0
        'cbxTipoInforme.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Pedidos
        cbxPedidos.cbx.Items.Add("TODOS")
        cbxPedidos.cbx.Items.Add("FACTURADO")
        cbxPedidos.cbx.Items.Add("NO FACTURADO")
        'cbxPedidos.cbx.Items.Add("ANULADO")
        cbxPedidos.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        For Each ctr As Object In gbxFiltro.Controls

            ''Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If
            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
siguiente:

        Next

    End Sub

    Sub Listar()

        Dim Where As String = " where 1 = 1 "
        Dim Where1 As String = " where EntregaCliente = 'True' and CantidadAEntregar > 0 "
        Dim OrderBy As String = ""
        Dim Titulo As String = "Listado de Pedidos de Clientes"
        Dim Subtitulo As String = ""
        FormaPago = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtra por fecha
        'Where = "Where Fecha between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'  "
        If chkFechaFacturacion.Valor = True Then
            Where = Where & " and Cast(FechaFacturar as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False) & "'  "
            Where1 = Where1 & " and Cast(FechaFacturar as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False) & "'  "
        End If
        If chkFechaPedido.Valor = True Then
            Where = Where & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdePedido.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaPedido.GetValue, True, False) & "'  "
            Where1 = Where1 & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdePedido.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaPedido.GetValue, True, False) & "'  "
        End If
        'Filtrar por Cliente
        If chkCliente.chk.Checked = True Then
            If txtCliente.Seleccionado = True Then
                Where = Where + " And IDCliente = " & txtCliente.Registro("ID").ToString
                Where1 = Where1 + " And IDCliente = " & txtCliente.Registro("ID").ToString
            Else

            End If
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente1
            End If
            If ctr.name = "cbxSucursalCliente" Then
                GoTo siguiente1
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where1)
            End If
siguiente1:

        Next

        'Filtrar por tipo de pedidos
        If chkPedido.chk.Checked = True Then
            If cbxPedidos.cbx.Text = "TODOS" Then
                Where = Where
            End If

        End If

        If chkPedido.chk.Checked = True Then
            If cbxPedidos.cbx.Text = "FACTURADO" Then
                Where = Where + " And Facturado='TRUE'"
            End If
        End If

        If chkPedido.chk.Checked = True Then
            If cbxPedidos.cbx.Text = "NO FACTURADO" Then
                Where = Where + " And Facturado='False'"
            End If
        End If

        'If chkPedido.chk.Checked = True Then
        '    If cbxPedidos.cbx.Text = "ANULADO" Then
        '        Where = Where + " And Anulado='True'"
        '    End If
        'End If


        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If


        ArmarSubTitulo(Subtitulo)
        Dim whereDetalle As String = ""
        'whereDetalle = " and CantidadAEntregar = 0"
        CReporte.ListadoPedidoAsignacionCamion(frm, Where, Where1, whereDetalle, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo)



    End Sub

    Sub ArmarSubTitulo(ByRef Subtitulo As String)
        Subtitulo = "Fecha: " & txtDesde.GetValue & " - " & txtHasta.GetValue & " "

    End Sub


    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Pedido", " Select Top (0) Numero, Ciudad, Sucursal , Cliente , Deposito from VPedido ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub frmPedidos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkPedido_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxPedidos.Enabled = value
    End Sub

    Private Sub chkVendedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVendedor.Load

    End Sub

    Private Sub chkActivo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkPedido.PropertyChanged
        cbxPedidos.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ckhUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles ckhUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkSucursalCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursalCliente.PropertyChanged
        cbxSucursalCliente.Enabled = value
    End Sub

    Private Sub chkSucursalPedido_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursalPedido.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub
End Class
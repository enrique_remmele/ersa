﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPedidoClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.chkSucursalPedido = New ERP.ocxCHK()
        Me.chkSucursalCliente = New ERP.ocxCHK()
        Me.cbxSucursalCliente = New ERP.ocxCBX()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.ckhUsuario = New ERP.ocxCHK()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.chkPedido = New ERP.ocxCHK()
        Me.cbxPedidos = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.chkFechaFacturacion = New ERP.ocxCHK()
        Me.chkFechaPedido = New ERP.ocxCHK()
        Me.txtHastaPedido = New ERP.ocxTXTDate()
        Me.txtDesdePedido = New ERP.ocxTXTDate()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkIncluirAnulados = New ERP.ocxCHK()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(597, 296)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 30
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(454, 296)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 29
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSucursalPedido)
        Me.gbxFiltro.Controls.Add(Me.chkSucursalCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursalCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.ckhUsuario)
        Me.gbxFiltro.Controls.Add(Me.cbxUsuario)
        Me.gbxFiltro.Controls.Add(Me.chkPedido)
        Me.gbxFiltro.Controls.Add(Me.cbxPedidos)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(453, 273)
        Me.gbxFiltro.TabIndex = 27
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkIncluirAnulados)
        Me.GroupBox2.Controls.Add(Me.chkFechaFacturacion)
        Me.GroupBox2.Controls.Add(Me.chkFechaPedido)
        Me.GroupBox2.Controls.Add(Me.txtHastaPedido)
        Me.GroupBox2.Controls.Add(Me.txtDesdePedido)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(471, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(187, 273)
        Me.GroupBox2.TabIndex = 28
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(12, 173)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 30
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'chkSucursalPedido
        '
        Me.chkSucursalPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalPedido.Color = System.Drawing.Color.Empty
        Me.chkSucursalPedido.Location = New System.Drawing.Point(7, 132)
        Me.chkSucursalPedido.Name = "chkSucursalPedido"
        Me.chkSucursalPedido.Size = New System.Drawing.Size(105, 21)
        Me.chkSucursalPedido.SoloLectura = False
        Me.chkSucursalPedido.TabIndex = 36
        Me.chkSucursalPedido.Texto = "Suc. del Pedido:"
        Me.chkSucursalPedido.Valor = False
        '
        'chkSucursalCliente
        '
        Me.chkSucursalCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalCliente.Color = System.Drawing.Color.Empty
        Me.chkSucursalCliente.Location = New System.Drawing.Point(7, 159)
        Me.chkSucursalCliente.Name = "chkSucursalCliente"
        Me.chkSucursalCliente.Size = New System.Drawing.Size(105, 21)
        Me.chkSucursalCliente.SoloLectura = False
        Me.chkSucursalCliente.TabIndex = 34
        Me.chkSucursalCliente.Texto = "Suc. del Cliente:"
        Me.chkSucursalCliente.Valor = False
        '
        'cbxSucursalCliente
        '
        Me.cbxSucursalCliente.CampoWhere = "IDSucursalClienteAtencion"
        Me.cbxSucursalCliente.CargarUnaSolaVez = False
        Me.cbxSucursalCliente.DataDisplayMember = "Descripcion"
        Me.cbxSucursalCliente.DataFilter = Nothing
        Me.cbxSucursalCliente.DataOrderBy = "Descripcion"
        Me.cbxSucursalCliente.DataSource = "vSucursal"
        Me.cbxSucursalCliente.DataValueMember = "ID"
        Me.cbxSucursalCliente.dtSeleccionado = Nothing
        Me.cbxSucursalCliente.Enabled = False
        Me.cbxSucursalCliente.FormABM = Nothing
        Me.cbxSucursalCliente.Indicaciones = Nothing
        Me.cbxSucursalCliente.Location = New System.Drawing.Point(119, 159)
        Me.cbxSucursalCliente.Name = "cbxSucursalCliente"
        Me.cbxSucursalCliente.SeleccionMultiple = False
        Me.cbxSucursalCliente.SeleccionObligatoria = False
        Me.cbxSucursalCliente.Size = New System.Drawing.Size(316, 21)
        Me.cbxSucursalCliente.SoloLectura = False
        Me.cbxSucursalCliente.TabIndex = 35
        Me.cbxSucursalCliente.Texto = "ASUNCION"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "Descripcion"
        Me.cbxSucursal.DataSource = "vSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(119, 132)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(316, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 33
        Me.cbxSucursal.Texto = "MOLINO"
        '
        'ckhUsuario
        '
        Me.ckhUsuario.BackColor = System.Drawing.Color.Transparent
        Me.ckhUsuario.Color = System.Drawing.Color.Empty
        Me.ckhUsuario.Location = New System.Drawing.Point(7, 106)
        Me.ckhUsuario.Name = "ckhUsuario"
        Me.ckhUsuario.Size = New System.Drawing.Size(75, 21)
        Me.ckhUsuario.SoloLectura = False
        Me.ckhUsuario.TabIndex = 30
        Me.ckhUsuario.Texto = "Usuario:"
        Me.ckhUsuario.Valor = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "vUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(119, 106)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(316, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 31
        Me.cbxUsuario.Texto = "CAST"
        '
        'chkPedido
        '
        Me.chkPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkPedido.Color = System.Drawing.Color.Empty
        Me.chkPedido.Location = New System.Drawing.Point(7, 80)
        Me.chkPedido.Name = "chkPedido"
        Me.chkPedido.Size = New System.Drawing.Size(78, 21)
        Me.chkPedido.SoloLectura = False
        Me.chkPedido.TabIndex = 29
        Me.chkPedido.Texto = "Pedidos:"
        Me.chkPedido.Valor = False
        '
        'cbxPedidos
        '
        Me.cbxPedidos.CampoWhere = Nothing
        Me.cbxPedidos.CargarUnaSolaVez = False
        Me.cbxPedidos.DataDisplayMember = Nothing
        Me.cbxPedidos.DataFilter = Nothing
        Me.cbxPedidos.DataOrderBy = Nothing
        Me.cbxPedidos.DataSource = Nothing
        Me.cbxPedidos.DataValueMember = Nothing
        Me.cbxPedidos.dtSeleccionado = Nothing
        Me.cbxPedidos.Enabled = False
        Me.cbxPedidos.FormABM = Nothing
        Me.cbxPedidos.Indicaciones = Nothing
        Me.cbxPedidos.Location = New System.Drawing.Point(119, 80)
        Me.cbxPedidos.Name = "cbxPedidos"
        Me.cbxPedidos.SeleccionMultiple = False
        Me.cbxPedidos.SeleccionObligatoria = False
        Me.cbxPedidos.Size = New System.Drawing.Size(316, 21)
        Me.cbxPedidos.SoloLectura = False
        Me.cbxPedidos.TabIndex = 28
        Me.cbxPedidos.Texto = "TODOS"
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(9, 213)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(423, 50)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 19
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(7, 54)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(75, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 20
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(119, 54)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(316, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 21
        Me.cbxDeposito.Texto = "MOL - DEP. MOL."
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(7, 28)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(71, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 4
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "Vendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(119, 28)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(316, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 5
        Me.cbxVendedor.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(7, 192)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(77, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 18
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'chkFechaFacturacion
        '
        Me.chkFechaFacturacion.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaFacturacion.Color = System.Drawing.Color.Empty
        Me.chkFechaFacturacion.Location = New System.Drawing.Point(11, 16)
        Me.chkFechaFacturacion.Name = "chkFechaFacturacion"
        Me.chkFechaFacturacion.Size = New System.Drawing.Size(165, 21)
        Me.chkFechaFacturacion.SoloLectura = False
        Me.chkFechaFacturacion.TabIndex = 36
        Me.chkFechaFacturacion.Texto = "Fecha Facturacion:"
        Me.chkFechaFacturacion.Valor = True
        '
        'chkFechaPedido
        '
        Me.chkFechaPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaPedido.Color = System.Drawing.Color.Empty
        Me.chkFechaPedido.Location = New System.Drawing.Point(11, 70)
        Me.chkFechaPedido.Name = "chkFechaPedido"
        Me.chkFechaPedido.Size = New System.Drawing.Size(170, 21)
        Me.chkFechaPedido.SoloLectura = False
        Me.chkFechaPedido.TabIndex = 32
        Me.chkFechaPedido.Texto = "Fecha Pedido"
        Me.chkFechaPedido.Valor = False
        '
        'txtHastaPedido
        '
        Me.txtHastaPedido.Color = System.Drawing.Color.Empty
        Me.txtHastaPedido.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHastaPedido.Location = New System.Drawing.Point(92, 96)
        Me.txtHastaPedido.Name = "txtHastaPedido"
        Me.txtHastaPedido.PermitirNulo = False
        Me.txtHastaPedido.Size = New System.Drawing.Size(84, 20)
        Me.txtHastaPedido.SoloLectura = False
        Me.txtHastaPedido.TabIndex = 35
        '
        'txtDesdePedido
        '
        Me.txtDesdePedido.Color = System.Drawing.Color.Empty
        Me.txtDesdePedido.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesdePedido.Location = New System.Drawing.Point(12, 96)
        Me.txtDesdePedido.Name = "txtDesdePedido"
        Me.txtDesdePedido.PermitirNulo = False
        Me.txtDesdePedido.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdePedido.SoloLectura = False
        Me.txtDesdePedido.TabIndex = 34
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(131, 191)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(45, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 32
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(12, 191)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(113, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 31
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHasta.Location = New System.Drawing.Point(92, 44)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(84, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesde.Location = New System.Drawing.Point(12, 44)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'chkIncluirAnulados
        '
        Me.chkIncluirAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkIncluirAnulados.Color = System.Drawing.Color.Empty
        Me.chkIncluirAnulados.Location = New System.Drawing.Point(12, 132)
        Me.chkIncluirAnulados.Name = "chkIncluirAnulados"
        Me.chkIncluirAnulados.Size = New System.Drawing.Size(113, 21)
        Me.chkIncluirAnulados.SoloLectura = False
        Me.chkIncluirAnulados.TabIndex = 37
        Me.chkIncluirAnulados.Texto = "Incluir Anulados"
        Me.chkIncluirAnulados.Valor = False
        '
        'frmPedidoClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(676, 331)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmPedidoClientes"
        Me.Text = "frmPedidoClientes"
        Me.gbxFiltro.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkFechaPedido As ERP.ocxCHK
    Friend WithEvents txtHastaPedido As ERP.ocxTXTDate
    Friend WithEvents txtDesdePedido As ERP.ocxTXTDate
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkFechaFacturacion As ERP.ocxCHK
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents ckhUsuario As ERP.ocxCHK
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents chkPedido As ERP.ocxCHK
    Friend WithEvents cbxPedidos As ERP.ocxCBX
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents chkSucursalCliente As ERP.ocxCHK
    Friend WithEvents cbxSucursalCliente As ERP.ocxCBX
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkSucursalPedido As ERP.ocxCHK
    Friend WithEvents chkIncluirAnulados As ERP.ocxCHK
End Class

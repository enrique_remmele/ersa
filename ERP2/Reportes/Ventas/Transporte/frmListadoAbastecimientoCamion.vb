﻿Imports ERP.Reporte
Public Class frmListadoAbastecimientoCamion
    'CLASES
    Dim CReporte As New CPedido
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim FormaPago As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
    End Sub

    Sub CargarInformacion()

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        For Each ctr As Object In gbxFiltro.Controls

            ''Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If
            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
siguiente:

        Next

    End Sub

    Sub Listar()

        Dim Where As String = " where 1 = 1 "
        Dim whereDetalle As String = " where Saldo > 0"
        Dim OrderBy As String = ""
        Dim Titulo As String = "Listado de Pedidos de Abastecimiento Pendientes"
        Dim Subtitulo As String = ""
        FormaPago = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtra por fecha
        'Where = "Where Fecha between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'  "
        If CHKFechaEntrega.Valor = True Then
            whereDetalle = whereDetalle & " and Cast(FechaEntrega as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesde.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHasta.GetValue, True, False) & "'  "
        End If

        If chkAnulado.Valor = True Then
            Where = Where & " and anulado = 0"
            whereDetalle = whereDetalle & " and anulado = 0"
        End If

        If chkFechaCarga.Valor = True Then
            Where = Where & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeCarga.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaCarga.GetValue, True, False) & "'  "
            whereDetalle = whereDetalle & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeCarga.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaCarga.GetValue, True, False) & "'  "
        End If
        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxUsuario" Then
                GoTo siguiente1
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(whereDetalle)
            End If
siguiente1:

        Next


        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        ArmarSubTitulo(Subtitulo)
        'whereDetalle = " and CantidadAEntregar = 0"
        CReporte.ListadoAbastecimientoCamion(frm, Where, whereDetalle, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo, txtDesde.txt.Text, txtHasta.txt.Text)
    End Sub

    Sub ArmarSubTitulo(ByRef Subtitulo As String)
        Subtitulo = "Fecha: " & txtDesde.GetValue & " - " & txtHasta.GetValue & " "

    End Sub


    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Pedido", " Select Top (0) Numero, Ciudad, Sucursal , Cliente , Deposito from VPedido ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub frmPedidos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub


    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ckhUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles ckhUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmInformeKilosTransportados
    'CLASES
    Dim CReporte As New CReporteLotes
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE LOTES EMITIDOS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        cbxEnForma.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxTipoInforme.cbx.SelectedIndex = 0
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()
    End Sub

    Sub CargarInformacion()
        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Orden en forma
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Ordenado por
        cbxOrdenadoPor.cbx.Items.Add("Fecha")
        cbxOrdenadoPor.cbx.Items.Add("Camion")
        cbxOrdenadoPor.cbx.Items.Add("Chofer")
        cbxOrdenadoPor.cbx.Items.Add("Producto")
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        txtProducto.Conectar()
        txtProducto.Enabled = False
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        TipoInforme = "FECHA:" & txtDesde.txt.Text & "-" & txtHasta.txt.Text
        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        Titulo = "Listado de Kilos Transportados"
        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)

        If cbxTipoInforme.cbx.SelectedIndex = 1 Then 'Detallado
            If chkProducto.Valor Then
                Where = Where & " and IDProducto = " & txtProducto.Registro("Id").ToString
                TipoInforme = TipoInforme & " - " & txtProducto.Registro("ProductoReferencia")
            End If
            CReporte.ListadoKilosTransportados(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        Else
            CReporte.ListadoKilosTransportadosResumido(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        End If






    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub
    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkChofer_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub

    Private Sub chkVehiculo_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVehiculo.PropertyChanged
        cbxVehiculo.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub
End Class
﻿Imports ERP.Reporte
Public Class frmListadoLoteDetalleDevolucion


    'CLASES
    Dim CReporte As New CReporteLotes
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    'Dim Titulo As String = "LISTADO DETALLE DEVOLUCION LOTES"
    Dim Titulo As String = "LISTADO DEVOLUCION LOTES / ORDEN DE CARGA"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        cbxOrdenadoPor.cbx.SelectedIndex = 1
        cbxEnForma.cbx.SelectedIndex = 0
        txtDesde.PrimerDiaMes()
        txtHasta.Hoy()

    End Sub

    Sub CargarInformacion()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        cbxOrdenadoPor.cbx.Items.Add("Comprobante")
        cbxOrdenadoPor.cbx.Items.Add("Ciudad")
        cbxOrdenadoPor.cbx.Items.Add("Sucursal")
        cbxOrdenadoPor.cbx.Items.Add("Zona")
        cbxOrdenadoPor.cbx.Items.Add("Chofer")
        cbxOrdenadoPor.cbx.Items.Add("Distribuidor")
        cbxOrdenadoPor.cbx.Items.Add("Camion")
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim TipoInforme As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)

        'CambiarOrdenacion()
        cbxDistribuidor.EstablecerCondicion(WhereDetalle)
        CReporte.ListadoDevolucionLotes(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top, txtDesde.txt.Text, txtHasta.txt.Text)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        'cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VLoteDistribucion", "Select Top(0) Comprobante, Ciudad,Sucursal,Zona,Chofer,Distribuidor,Camion From VLoteDistribucion")
        'dt = CData.GetStructure("vDevolucionLote", "Select Top(0) Comprobante, Ciudad,Sucursal,Zona,Chofer,Distribuidor From vDevolucionLote")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkTipoComprobante_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkTipoComprobante.PropertyChanged
        cbxTipoComprobante.Enabled = chkTipoComprobante.Valor
    End Sub

    Private Sub frmListadoLoteDetalleDevolucion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxTipoComprobante.Enabled = chkTipoComprobante.Valor
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CambiarOrdenacion()
    End Sub

    Private Sub chkDistribuidor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDistribuidor.PropertyChanged
        cbxDistribuidor.Enabled = value
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub
    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkChofer_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkChofer.PropertyChanged
        cbxChofer.Enabled = value
    End Sub

    Private Sub chkVehiculo_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVehiculo.PropertyChanged
        cbxVehiculo.Enabled = value
    End Sub

    Private Sub chkZona_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkZona.PropertyChanged
        cbxZona.Enabled = value
    End Sub
End Class
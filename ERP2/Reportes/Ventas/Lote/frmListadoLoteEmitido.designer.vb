﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoLoteEmitido
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkLote = New ERP.ocxCHK()
        Me.txtLoteDesde = New ERP.ocxTXTNumeric()
        Me.chkVehiculo = New ERP.ocxCHK()
        Me.cbxVehiculo = New ERP.ocxCBX()
        Me.chkChofer = New ERP.ocxCHK()
        Me.cbxChofer = New ERP.ocxCBX()
        Me.chkZona = New ERP.ocxCHK()
        Me.cbxZona = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkDistribuidor = New ERP.ocxCHK()
        Me.cbxDistribuidor = New ERP.ocxCBX()
        Me.chkTipoComprobante = New ERP.ocxCHK()
        Me.cbxTipoComprobante = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkCliente = New ERP.ocxCHK()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(421, 297)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 2
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 118)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 5
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(6, 184)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 9
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(6, 69)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(45, 13)
        Me.lblInforme.TabIndex = 3
        Me.lblInforme.Text = "Informe:"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(6, 168)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 8
        Me.lblRanking.Text = "Ranking:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxTipoInforme)
        Me.GroupBox2.Controls.Add(Me.lblInforme)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(412, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 270)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(6, 86)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionObligatoria = False
        Me.cbxTipoInforme.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 4
        Me.cbxTipoInforme.Texto = ""
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(169, 135)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(67, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 7
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 135)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 6
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 1, 28, 15, 33, 20, 50)
        Me.txtHasta.Location = New System.Drawing.Point(89, 38)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 1, 28, 15, 33, 20, 66)
        Me.txtDesde.Location = New System.Drawing.Point(6, 38)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkLote)
        Me.gbxFiltro.Controls.Add(Me.txtLoteDesde)
        Me.gbxFiltro.Controls.Add(Me.chkVehiculo)
        Me.gbxFiltro.Controls.Add(Me.cbxVehiculo)
        Me.gbxFiltro.Controls.Add(Me.chkChofer)
        Me.gbxFiltro.Controls.Add(Me.cbxChofer)
        Me.gbxFiltro.Controls.Add(Me.chkZona)
        Me.gbxFiltro.Controls.Add(Me.cbxZona)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkDistribuidor)
        Me.gbxFiltro.Controls.Add(Me.cbxDistribuidor)
        Me.gbxFiltro.Controls.Add(Me.chkTipoComprobante)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoComprobante)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(394, 308)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkLote
        '
        Me.chkLote.BackColor = System.Drawing.Color.Transparent
        Me.chkLote.Color = System.Drawing.Color.Empty
        Me.chkLote.Location = New System.Drawing.Point(9, 46)
        Me.chkLote.Name = "chkLote"
        Me.chkLote.Size = New System.Drawing.Size(114, 21)
        Me.chkLote.SoloLectura = False
        Me.chkLote.TabIndex = 2
        Me.chkLote.Texto = "Lote:"
        Me.chkLote.Valor = False
        '
        'txtLoteDesde
        '
        Me.txtLoteDesde.Color = System.Drawing.Color.Empty
        Me.txtLoteDesde.Decimales = True
        Me.txtLoteDesde.Enabled = False
        Me.txtLoteDesde.Indicaciones = Nothing
        Me.txtLoteDesde.Location = New System.Drawing.Point(126, 46)
        Me.txtLoteDesde.Name = "txtLoteDesde"
        Me.txtLoteDesde.Size = New System.Drawing.Size(103, 22)
        Me.txtLoteDesde.SoloLectura = False
        Me.txtLoteDesde.TabIndex = 3
        Me.txtLoteDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLoteDesde.Texto = "0"
        '
        'chkVehiculo
        '
        Me.chkVehiculo.BackColor = System.Drawing.Color.Transparent
        Me.chkVehiculo.Color = System.Drawing.Color.Empty
        Me.chkVehiculo.Location = New System.Drawing.Point(12, 202)
        Me.chkVehiculo.Name = "chkVehiculo"
        Me.chkVehiculo.Size = New System.Drawing.Size(114, 21)
        Me.chkVehiculo.SoloLectura = False
        Me.chkVehiculo.TabIndex = 14
        Me.chkVehiculo.Texto = "Vehiculo:"
        Me.chkVehiculo.Valor = False
        '
        'cbxVehiculo
        '
        Me.cbxVehiculo.CampoWhere = "IDCamion"
        Me.cbxVehiculo.CargarUnaSolaVez = False
        Me.cbxVehiculo.DataDisplayMember = "Descripcion"
        Me.cbxVehiculo.DataFilter = Nothing
        Me.cbxVehiculo.DataOrderBy = "Descripcion"
        Me.cbxVehiculo.DataSource = "VCamion"
        Me.cbxVehiculo.DataValueMember = "ID"
        Me.cbxVehiculo.Enabled = False
        Me.cbxVehiculo.FormABM = Nothing
        Me.cbxVehiculo.Indicaciones = Nothing
        Me.cbxVehiculo.Location = New System.Drawing.Point(126, 202)
        Me.cbxVehiculo.Name = "cbxVehiculo"
        Me.cbxVehiculo.SeleccionObligatoria = False
        Me.cbxVehiculo.Size = New System.Drawing.Size(213, 21)
        Me.cbxVehiculo.SoloLectura = False
        Me.cbxVehiculo.TabIndex = 15
        Me.cbxVehiculo.Texto = ""
        '
        'chkChofer
        '
        Me.chkChofer.BackColor = System.Drawing.Color.Transparent
        Me.chkChofer.Color = System.Drawing.Color.Empty
        Me.chkChofer.Location = New System.Drawing.Point(10, 160)
        Me.chkChofer.Name = "chkChofer"
        Me.chkChofer.Size = New System.Drawing.Size(114, 21)
        Me.chkChofer.SoloLectura = False
        Me.chkChofer.TabIndex = 10
        Me.chkChofer.Texto = "Chofer:"
        Me.chkChofer.Valor = False
        '
        'cbxChofer
        '
        Me.cbxChofer.CampoWhere = "IDChofer"
        Me.cbxChofer.CargarUnaSolaVez = False
        Me.cbxChofer.DataDisplayMember = "Nombres"
        Me.cbxChofer.DataFilter = Nothing
        Me.cbxChofer.DataOrderBy = "Nombres"
        Me.cbxChofer.DataSource = "Chofer"
        Me.cbxChofer.DataValueMember = "ID"
        Me.cbxChofer.Enabled = False
        Me.cbxChofer.FormABM = Nothing
        Me.cbxChofer.Indicaciones = Nothing
        Me.cbxChofer.Location = New System.Drawing.Point(126, 160)
        Me.cbxChofer.Name = "cbxChofer"
        Me.cbxChofer.SeleccionObligatoria = False
        Me.cbxChofer.Size = New System.Drawing.Size(213, 21)
        Me.cbxChofer.SoloLectura = False
        Me.cbxChofer.TabIndex = 11
        Me.cbxChofer.Texto = ""
        '
        'chkZona
        '
        Me.chkZona.BackColor = System.Drawing.Color.Transparent
        Me.chkZona.Color = System.Drawing.Color.Empty
        Me.chkZona.Location = New System.Drawing.Point(10, 118)
        Me.chkZona.Name = "chkZona"
        Me.chkZona.Size = New System.Drawing.Size(114, 21)
        Me.chkZona.SoloLectura = False
        Me.chkZona.TabIndex = 8
        Me.chkZona.Texto = "ZonaVenta:"
        Me.chkZona.Valor = False
        '
        'cbxZona
        '
        Me.cbxZona.CampoWhere = "IDZonaVenta"
        Me.cbxZona.CargarUnaSolaVez = False
        Me.cbxZona.DataDisplayMember = "Descripcion"
        Me.cbxZona.DataFilter = Nothing
        Me.cbxZona.DataOrderBy = Nothing
        Me.cbxZona.DataSource = "VZonaVenta"
        Me.cbxZona.DataValueMember = "ID"
        Me.cbxZona.Enabled = False
        Me.cbxZona.FormABM = Nothing
        Me.cbxZona.Indicaciones = Nothing
        Me.cbxZona.Location = New System.Drawing.Point(126, 118)
        Me.cbxZona.Name = "cbxZona"
        Me.cbxZona.SeleccionObligatoria = False
        Me.cbxZona.Size = New System.Drawing.Size(213, 21)
        Me.cbxZona.SoloLectura = False
        Me.cbxZona.TabIndex = 9
        Me.cbxZona.Texto = "ASUNCION"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 97)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 6
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(126, 97)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 7
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(9, 76)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(114, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 4
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudad"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Ciudad"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VSucursal"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(126, 76)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(213, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 5
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'chkDistribuidor
        '
        Me.chkDistribuidor.BackColor = System.Drawing.Color.Transparent
        Me.chkDistribuidor.Color = System.Drawing.Color.Empty
        Me.chkDistribuidor.Location = New System.Drawing.Point(11, 181)
        Me.chkDistribuidor.Name = "chkDistribuidor"
        Me.chkDistribuidor.Size = New System.Drawing.Size(114, 21)
        Me.chkDistribuidor.SoloLectura = False
        Me.chkDistribuidor.TabIndex = 12
        Me.chkDistribuidor.Texto = "Distribuidor:"
        Me.chkDistribuidor.Valor = False
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.CampoWhere = "IDDistribuidor"
        Me.cbxDistribuidor.CargarUnaSolaVez = False
        Me.cbxDistribuidor.DataDisplayMember = "Nombres"
        Me.cbxDistribuidor.DataFilter = Nothing
        Me.cbxDistribuidor.DataOrderBy = "Nombres"
        Me.cbxDistribuidor.DataSource = "VDistribuidor"
        Me.cbxDistribuidor.DataValueMember = "ID"
        Me.cbxDistribuidor.Enabled = False
        Me.cbxDistribuidor.FormABM = Nothing
        Me.cbxDistribuidor.Indicaciones = Nothing
        Me.cbxDistribuidor.Location = New System.Drawing.Point(126, 181)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.SeleccionObligatoria = False
        Me.cbxDistribuidor.Size = New System.Drawing.Size(213, 21)
        Me.cbxDistribuidor.SoloLectura = False
        Me.cbxDistribuidor.TabIndex = 13
        Me.cbxDistribuidor.Texto = ""
        '
        'chkTipoComprobante
        '
        Me.chkTipoComprobante.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoComprobante.Color = System.Drawing.Color.Empty
        Me.chkTipoComprobante.Location = New System.Drawing.Point(9, 19)
        Me.chkTipoComprobante.Name = "chkTipoComprobante"
        Me.chkTipoComprobante.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoComprobante.SoloLectura = False
        Me.chkTipoComprobante.TabIndex = 0
        Me.chkTipoComprobante.Texto = "Tipo Comprobante:"
        Me.chkTipoComprobante.Valor = False
        '
        'cbxTipoComprobante
        '
        Me.cbxTipoComprobante.CampoWhere = "IDTipoComprobante"
        Me.cbxTipoComprobante.CargarUnaSolaVez = False
        Me.cbxTipoComprobante.DataDisplayMember = "Descripcion"
        Me.cbxTipoComprobante.DataFilter = "FormName='frmVenta'"
        Me.cbxTipoComprobante.DataOrderBy = "Descripcion"
        Me.cbxTipoComprobante.DataSource = "VTipoComprobante"
        Me.cbxTipoComprobante.DataValueMember = "ID"
        Me.cbxTipoComprobante.Enabled = False
        Me.cbxTipoComprobante.FormABM = Nothing
        Me.cbxTipoComprobante.Indicaciones = Nothing
        Me.cbxTipoComprobante.Location = New System.Drawing.Point(126, 19)
        Me.cbxTipoComprobante.Name = "cbxTipoComprobante"
        Me.cbxTipoComprobante.SeleccionObligatoria = False
        Me.cbxTipoComprobante.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoComprobante.SoloLectura = False
        Me.cbxTipoComprobante.TabIndex = 1
        Me.cbxTipoComprobante.Texto = ""
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(575, 297)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(73, 23)
        Me.btnCerrar.TabIndex = 3
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 150
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(12, 249)
        Me.txtCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(375, 52)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 25
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(11, 229)
        Me.chkCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 24
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'frmListadoLoteEmitido
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(670, 333)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoLoteEmitido"
        Me.Text = "frmListadoLote"
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxTipoInforme As ERP.ocxCBX
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPeriodo As System.Windows.Forms.Label
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkDistribuidor As ERP.ocxCHK
    Friend WithEvents cbxDistribuidor As ERP.ocxCBX
    Friend WithEvents chkTipoComprobante As ERP.ocxCHK
    Friend WithEvents cbxTipoComprobante As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkZona As ERP.ocxCHK
    Friend WithEvents cbxZona As ERP.ocxCBX
    Friend WithEvents chkChofer As ERP.ocxCHK
    Friend WithEvents cbxChofer As ERP.ocxCBX
    Friend WithEvents chkVehiculo As ERP.ocxCHK
    Friend WithEvents cbxVehiculo As ERP.ocxCBX
    Friend WithEvents chkLote As ERP.ocxCHK
    Friend WithEvents txtLoteDesde As ERP.ocxTXTNumeric
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkCliente As ERP.ocxCHK
End Class

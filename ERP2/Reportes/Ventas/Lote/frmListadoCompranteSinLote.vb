﻿Imports ERP.Reporte
Public Class frmListadoComprobanteSinLote

    'CLASES
    Dim CReporte As New CReporteLotes
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE COMPROBANTES SIN LOTE"
    Dim TipoInforme As String = "CONTADO + CREDITO"

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
    End Sub

    Sub CargarInformacion()
        'Condicion
        cbxCondicion.cbx.Items.Add("CONTADO")
        cbxCondicion.cbx.Items.Add("CREDITO")
        cbxCondicion.cbx.Items.Add("CONTADO+CREDITO")
        cbxCondicion.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        CambiarOrdenacion()

    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxCondicion" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos la condicion
        If chkCondicion.Valor = True Then
            Select Case cbxCondicion.cbx.SelectedIndex
                Case 0
                    Where = Where & " And Credito=0"
                    TipoInforme = cbxCondicion.cbx.Text
                Case 1
                    Where = Where & "And Credito= 1"
                    TipoInforme = cbxCondicion.cbx.Text
                Case 2
                    Where = Where
                    TipoInforme = cbxCondicion.cbx.Text
            End Select
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If


        CReporte.ListadoComprobantesSinLote(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)




    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VVentasSinLotes", "Select Top(0) Comprobante, Vendedor, Cliente, Total, Saldo From VVentasSinLotes")
          
        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

   Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        CambiarOrdenacion()
    End Sub
    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub
    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkCondicion_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCondicion.PropertyChanged
        cbxCondicion.Enabled = value
    End Sub
End Class
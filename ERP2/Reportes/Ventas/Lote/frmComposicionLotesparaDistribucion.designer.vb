﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComposicionLotesparaDistribucion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.cbxVehiculo = New ERP.ocxCBX()
        Me.chkVehiculo = New ERP.ocxCHK()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.nudRanking = New System.Windows.Forms.NumericUpDown()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.txtLoteHasta = New ERP.ocxTXTNumeric()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.lblRanking = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.OcxCHK1 = New ERP.ocxCHK()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.chkDistribuidor = New ERP.ocxCHK()
        Me.cbxDistribuidor = New ERP.ocxCBX()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.txtLoteDesde = New ERP.ocxTXTNumeric()
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(27, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Lotes"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(8, 35)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(156, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 2
        Me.cbxOrdenadoPor.Texto = ""
        '
        'cbxVehiculo
        '
        Me.cbxVehiculo.CampoWhere = "IDCamion"
        Me.cbxVehiculo.DataDisplayMember = "Descripcion"
        Me.cbxVehiculo.DataFilter = Nothing
        Me.cbxVehiculo.DataOrderBy = "Descripcion"
        Me.cbxVehiculo.DataSource = "Camion"
        Me.cbxVehiculo.DataValueMember = "ID"
        Me.cbxVehiculo.Enabled = False
        Me.cbxVehiculo.FormABM = Nothing
        Me.cbxVehiculo.Indicaciones = Nothing
        Me.cbxVehiculo.Location = New System.Drawing.Point(93, 81)
        Me.cbxVehiculo.Name = "cbxVehiculo"
        Me.cbxVehiculo.SeleccionObligatoria = False
        Me.cbxVehiculo.Size = New System.Drawing.Size(213, 21)
        Me.cbxVehiculo.SoloLectura = False
        Me.cbxVehiculo.TabIndex = 7
        Me.cbxVehiculo.Texto = ""
        '
        'chkVehiculo
        '
        Me.chkVehiculo.Color = System.Drawing.Color.Empty
        Me.chkVehiculo.Location = New System.Drawing.Point(10, 83)
        Me.chkVehiculo.Name = "chkVehiculo"
        Me.chkVehiculo.Size = New System.Drawing.Size(75, 21)
        Me.chkVehiculo.SoloLectura = False
        Me.chkVehiculo.TabIndex = 6
        Me.chkVehiculo.Texto = "Vehiculo:"
        Me.chkVehiculo.Valor = False
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(8, 19)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 0
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'nudRanking
        '
        Me.nudRanking.Location = New System.Drawing.Point(242, 35)
        Me.nudRanking.Name = "nudRanking"
        Me.nudRanking.Size = New System.Drawing.Size(74, 20)
        Me.nudRanking.TabIndex = 4
        '
        'chkSucursal
        '
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(10, 39)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(78, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 2
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'txtLoteHasta
        '
        Me.txtLoteHasta.Color = System.Drawing.Color.Empty
        Me.txtLoteHasta.Decimales = True
        Me.txtLoteHasta.Indicaciones = Nothing
        Me.txtLoteHasta.Location = New System.Drawing.Point(185, 11)
        Me.txtLoteHasta.Name = "txtLoteHasta"
        Me.txtLoteHasta.Size = New System.Drawing.Size(103, 22)
        Me.txtLoteHasta.SoloLectura = False
        Me.txtLoteHasta.TabIndex = 9
        Me.txtLoteHasta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLoteHasta.Texto = "0"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(93, 39)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'lblRanking
        '
        Me.lblRanking.AutoSize = True
        Me.lblRanking.Location = New System.Drawing.Point(244, 19)
        Me.lblRanking.Name = "lblRanking"
        Me.lblRanking.Size = New System.Drawing.Size(50, 13)
        Me.lblRanking.TabIndex = 1
        Me.lblRanking.Text = "Ranking:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(164, 35)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 3
        Me.cbxEnForma.Texto = ""
        '
        'chkCiudad
        '
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(10, 18)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(79, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 0
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.nudRanking)
        Me.GroupBox2.Controls.Add(Me.lblRanking)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 180)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(318, 74)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.OcxCHK1)
        Me.gbxFiltro.Controls.Add(Me.cbxUsuario)
        Me.gbxFiltro.Controls.Add(Me.chkVehiculo)
        Me.gbxFiltro.Controls.Add(Me.cbxVehiculo)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Controls.Add(Me.chkDistribuidor)
        Me.gbxFiltro.Controls.Add(Me.cbxDistribuidor)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, 42)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(316, 137)
        Me.gbxFiltro.TabIndex = 10
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'OcxCHK1
        '
        Me.OcxCHK1.Color = System.Drawing.Color.Empty
        Me.OcxCHK1.Location = New System.Drawing.Point(10, 105)
        Me.OcxCHK1.Name = "OcxCHK1"
        Me.OcxCHK1.Size = New System.Drawing.Size(78, 21)
        Me.OcxCHK1.SoloLectura = False
        Me.OcxCHK1.TabIndex = 8
        Me.OcxCHK1.Texto = "Usuario:"
        Me.OcxCHK1.Valor = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "VUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(93, 103)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(213, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 9
        Me.cbxUsuario.Texto = ""
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "IDCiudad"
        Me.cbxCiudad.DataDisplayMember = "Ciudad"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VSucursal"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(93, 18)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(213, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 1
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'chkDistribuidor
        '
        Me.chkDistribuidor.Color = System.Drawing.Color.Empty
        Me.chkDistribuidor.Location = New System.Drawing.Point(10, 62)
        Me.chkDistribuidor.Name = "chkDistribuidor"
        Me.chkDistribuidor.Size = New System.Drawing.Size(83, 21)
        Me.chkDistribuidor.SoloLectura = False
        Me.chkDistribuidor.TabIndex = 4
        Me.chkDistribuidor.Texto = "Distribuidor:"
        Me.chkDistribuidor.Valor = False
        '
        'cbxDistribuidor
        '
        Me.cbxDistribuidor.CampoWhere = "IDDistribuidor"
        Me.cbxDistribuidor.DataDisplayMember = "Nombres"
        Me.cbxDistribuidor.DataFilter = Nothing
        Me.cbxDistribuidor.DataOrderBy = "Nombres"
        Me.cbxDistribuidor.DataSource = "VDistribuidor"
        Me.cbxDistribuidor.DataValueMember = "ID"
        Me.cbxDistribuidor.Enabled = False
        Me.cbxDistribuidor.FormABM = Nothing
        Me.cbxDistribuidor.Indicaciones = Nothing
        Me.cbxDistribuidor.Location = New System.Drawing.Point(93, 60)
        Me.cbxDistribuidor.Name = "cbxDistribuidor"
        Me.cbxDistribuidor.SeleccionObligatoria = False
        Me.cbxDistribuidor.Size = New System.Drawing.Size(213, 21)
        Me.cbxDistribuidor.SoloLectura = False
        Me.cbxDistribuidor.TabIndex = 5
        Me.cbxDistribuidor.Texto = ""
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(41, 257)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 12
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(222, 257)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 13
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'txtLoteDesde
        '
        Me.txtLoteDesde.Color = System.Drawing.Color.Empty
        Me.txtLoteDesde.Decimales = True
        Me.txtLoteDesde.Indicaciones = Nothing
        Me.txtLoteDesde.Location = New System.Drawing.Point(66, 11)
        Me.txtLoteDesde.Name = "txtLoteDesde"
        Me.txtLoteDesde.Size = New System.Drawing.Size(103, 22)
        Me.txtLoteDesde.SoloLectura = False
        Me.txtLoteDesde.TabIndex = 8
        Me.txtLoteDesde.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLoteDesde.Texto = "0"
        '
        'frmComposicionLotesparaDistribucion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(330, 297)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtLoteHasta)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.txtLoteDesde)
        Me.Name = "frmComposicionLotesparaDistribucion"
        Me.Text = "frmComposicionLotesparaDistribucion"
        CType(Me.nudRanking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents cbxVehiculo As ERP.ocxCBX
    Friend WithEvents chkVehiculo As ERP.ocxCHK
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents nudRanking As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents txtLoteHasta As ERP.ocxTXTNumeric
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents lblRanking As System.Windows.Forms.Label
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents chkDistribuidor As ERP.ocxCHK
    Friend WithEvents cbxDistribuidor As ERP.ocxCBX
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents txtLoteDesde As ERP.ocxTXTNumeric
    Friend WithEvents OcxCHK1 As ERP.ocxCHK
    Friend WithEvents cbxUsuario As ERP.ocxCBX
End Class

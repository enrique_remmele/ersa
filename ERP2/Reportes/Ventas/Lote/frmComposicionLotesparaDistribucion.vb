﻿Imports ERP.Reporte
Public Class frmComposicionLotesparaDistribucion

    'CLASES
    Dim CReporte As New CReporteLotes
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "COMPOSICION DE LOTES PARA DISTIBUCION"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

    End Sub

    Sub CargarInformacion()

        Me.KeyPreview = True

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Numeros no impresos, MIN y MAX de OP
        Dim vdttemp As DataTable = CSistema.ExecuteToDataTable("Select 'Min'=IsNull(MIN(Numero), 0), 'Max'=IsNull(MAX(Numero), 0) From VLoteDistribucion").Copy

        If vdttemp Is Nothing Then
            Exit Sub
        End If

        If vdttemp.Rows.Count = 0 Then
            Exit Sub
        End If

        Dim oRow As DataRow = vdttemp.Rows(0)
        txtLoteDesde.SetValue(oRow("Min"))
        txtLoteHasta.SetValue(oRow("Max"))
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where Numero Between " & txtLoteDesde.txt.Text & " And " & txtLoteHasta.txt.Text

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        CambiarOrdenacion()
        cbxDistribuidor.EstablecerCondicion(WhereDetalle)
        CReporte.ComposiciondeLotesparaDistribucion(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)

    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("VLoteDistribucion", "Select Top(0) Ciudad,Sucursal,Distribuidor,Camion,Total From VLoteDistribucion")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmHojaRuta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CargarInformacion()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDistribuidor_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkDistribuidor.PropertyChanged
        cbxDistribuidor.Enabled = value
    End Sub

    Private Sub chkVehiculo_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkVehiculo.PropertyChanged
        cbxVehiculo.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmHojaRuta_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub OcxCHK1_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles OcxCHK1.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub
End Class
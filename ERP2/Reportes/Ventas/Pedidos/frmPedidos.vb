﻿Imports ERP.Reporte
Public Class frmPedidos

    'CLASES
    Dim CReporte As New CPedido
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim FormaPago As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()
        cbxTipoInforme.cbx.SelectedIndex = 1
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0
        txtDesdePedido.Hoy()
        txtHastaPedido.Hoy()
    End Sub

    Sub CargarInformacion()

        txtCliente.Conectar()
        
        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO CON DESCUENTOS")
        cbxTipoInforme.cbx.SelectedIndex = 0
        cbxTipoInforme.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Moneda
        cbxMoneda.cbx.Items.Add("GUARANIES")
        cbxMoneda.cbx.Items.Add("DOLARES")
        cbxMoneda.cbx.Items.Add("TODOS")
        cbxMoneda.cbx.SelectedIndex = 0
        cbxMoneda.cbx.DropDownStyle = ComboBoxStyle.DropDownList


        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Pedidos
        cbxPedidos.cbx.Items.Add("TODOS")
        cbxPedidos.cbx.Items.Add("FACTURADO")
        cbxPedidos.cbx.Items.Add("NO FACTURADO")
        cbxPedidos.cbx.Items.Add("ANULADO")
        cbxPedidos.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        CSistema.SqlToComboBox(cbxListaPrecio, "Select distinct Descripcion, Descripcion from ListaPrecio where Estado = 1")


        'Habilitamos Seleccion multiple de todos los filtros ocxcbx
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

        txtProducto.SeleccionMultiple = True
        txtProducto.Conectar()

    End Sub

    Sub Listar()

        Dim Where As String = " where 1 = 1 "
        Dim WhereDetalle As String = " where 1 = 1 "
        Dim OrderBy As String = ""
        Dim Titulo As String = ""
        Dim Subtitulo As String = ""
        Dim TablaCabecera As String = "VPedido"
        Dim TablaDetalle As String = "VDetallePedidoDescuentoDetalle"

        If chkSinIVA.chk.Checked Then
            TablaDetalle = "VDetallePedidoDescuentoDetalleSinIVA"
        End If

        FormaPago = ""
     
        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtra por fecha
        'Where = "Where Fecha between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'  "
        If chkFechaFacturacion.Valor = True Then
            Where = Where & " and Cast(FechaFactura as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeFacturado.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaFacturado.GetValue, True, False) & "'  "
            WhereDetalle = WhereDetalle & " and Cast(FechaFactura as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeFacturado.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaFacturado.GetValue, True, False) & "'  "
        End If
        If chkFechaPedido.Valor = True Then
            Where = Where & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdePedido.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaPedido.GetValue, True, False) & "'  "
            WhereDetalle = WhereDetalle & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdePedido.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaPedido.GetValue, True, False) & "'  "
        End If
        If chkFechaFacturar.Valor = True Then
            Where = Where & " and Cast(FechaFacturar as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeFacturar.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaFacturar.GetValue, True, False) & "'  "
            WhereDetalle = WhereDetalle & " and Cast(FechaFacturar as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeFacturar.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaFacturar.GetValue, True, False) & "'  "
        End If

        If Not txtProducto.dtProductosSeleccionados Is Nothing Then
            If txtProducto.dtProductosSeleccionados.Rows.Count = 0 Then
                txtProducto.dtProductosSeleccionados.Reset()
            End If
        End If

        'Filtro por producto
        If chkProducto.Valor = True Then
            If txtProducto.Seleccionado = False And (txtProducto.dtProductosSeleccionados Is Nothing) Then
                Exit Sub
            End If
            If Not txtProducto.dtProductosSeleccionados Is Nothing Then

                If txtProducto.dtProductosSeleccionados.Rows.Count > 0 Then

                    Dim IDProductoIN As String = ""
                    If chkExcluirProducto.chk.Checked Then
                        IDProductoIN = " IDProducto NOT In (0"
                    Else
                        IDProductoIN = " IDProducto In (0"
                    End If

                    For Each dRow As DataRow In txtProducto.dtProductosSeleccionados.Rows
                        IDProductoIN = IDProductoIN & ", " & dRow("ID")
                    Next
                    IDProductoIN = IDProductoIN & ")"

                    WhereDetalle = IIf(String.IsNullOrEmpty(WhereDetalle), " where " & IDProductoIN, WhereDetalle & "And " & IDProductoIN)


                    GoTo Seguir

                End If

            End If
            If chkExcluirProducto.chk.Checked Then
                WhereDetalle = WhereDetalle & " And IDProducto <> " & txtProducto.Registro("ID") & " "
            Else
                WhereDetalle = WhereDetalle & " And IDProducto = " & txtProducto.Registro("ID") & " "
            End If

        End If
Seguir:

        'Filtrar por Cliente
        If chkCliente.chk.Checked = True Then
            If txtCliente.Seleccionado = True Then
                Where = Where + " And IDCliente = " & txtCliente.Registro("ID").ToString
                WhereDetalle = WhereDetalle + " And IDCliente = " & txtCliente.Registro("ID").ToString
            Else

            End If
        End If

        CargaFormaPago()

        'Filtrar por Forma de pago
        If FormaPago <> "" Then
            FormaPago = "and (IDFormaPagoFactura = 0" & FormaPago & ")"
            Where = Where & FormaPago
        End If

        EstablecerCondicionVenta(Where)
        EstablecerCondicionVenta(WhereDetalle)


        If chkPedido.chk.Checked = True Then
            If cbxPedidos.cbx.Text = "FACTURADO" Then
                Where = Where + " And Facturado='TRUE'"
                WhereDetalle = WhereDetalle + " And Facturado='TRUE'"
            End If

            If cbxPedidos.cbx.Text = "NO FACTURADO" Then
                Where = Where + " And Facturado='False' and Anulado='False'"
                WhereDetalle = WhereDetalle + " And Facturado='False' and Anulado='False'"
            End If

            If cbxPedidos.cbx.Text = "ANULADO" Then
                Where = Where + " And Anulado='True'"
                WhereDetalle = WhereDetalle + " And Anulado='True'"
            End If
        End If

        'WhereDetalle = Where

        If cbxTipoInforme.cbx.Text = "DETALLADO" Then
            If chkTipoProducto.chk.Checked = True Then
                WhereDetalle = WhereDetalle & " And IDTipoProducto =" & cbxTipoProducto.GetValue & " "
                Where = Where & " And IDTipoProducto =" & cbxTipoProducto.GetValue & " "
            End If

        End If

        If chkListaPrecio.chk.Checked Then
            WhereDetalle = WhereDetalle & " and [Lista de Precio] = '" & cbxListaPrecio.cbx.Text & "'"
            Where = Where & " and [Lista de Precio] = '" & cbxListaPrecio.cbx.Text & "'"
        End If


        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If


        ArmarSubTitulo(Subtitulo)

        If chkEntregaAlCliente.Valor Then
            Where = Where & " and EntregaCliente = 'True'"
            WhereDetalle = WhereDetalle & " and EntregaCliente = 'True'"
        End If

        If chkExcluirCancelacionAutomatica.Valor Then
            Where = Where & " and CancelarAutomatico = 'False'"
            WhereDetalle = WhereDetalle & " and CancelarAutomatico = 'False'"
        End If

        Select Case cbxMoneda.Texto
            Case "GUARANIES"
                Where = Where & " and IDMoneda = 1"
                WhereDetalle = WhereDetalle & " and IDMoneda = 1"
            Case "DOLARES"
                Where = Where & " and IDMoneda = 2"
                WhereDetalle = WhereDetalle & " and IDMoneda = 2"
            Case "TODOS"
                TablaDetalle = "VDetallePedidoDescuentoDetalleAGuaranies"
                If chkSinIVA.chk.Checked Then
                    TablaDetalle = "VDetallePedidoDescuentoDetalleAGuaraniesSINIVA"
                End If

        End Select

        If chkTotalPorProducto.Valor = False Then
            Select Case cbxTipoInforme.cbx.SelectedIndex
                Case 0
                    CReporte.ListadoPedido(frm, Where, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo)
                Case 1
                    CReporte.ListadoPedidoDetalle(frm, Where, WhereDetalle, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo)
                Case 2
                    CReporte.ListadoPedidoDetalleDescuento(frm, Where, WhereDetalle, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo)
            End Select
        Else
            If cbxTipoInforme.cbx.SelectedIndex <> 2 Then
                CReporte.ListadoTotalProductoPedido(frm, Where, WhereDetalle, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo)
            Else
                CReporte.ListadoTotalProductoPedidoDetalleDescuento(frm, Where, WhereDetalle, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo, TablaCabecera, TablaDetalle)

            End If
        End If


    End Sub

    Sub EstablecerCondicionVenta(ByRef Where As String)


        'Vendedor
        cbxVendedor.EstablecerCondicion(Where, chkExcluirVendedor.Valor)

        'Deposito
        cbxDeposito.EstablecerCondicion(Where, chkExcluirDeposito.Valor)

        'Tipo Producto
        cbxTipoProducto.EstablecerCondicion(Where, chkExcluirTipoProducto.Valor)

        'Usuario
        cbxUsuario.EstablecerCondicion(Where, chkExluirUsuario.Valor)

        'Tipo Cliente
        cbxTipoCliente.EstablecerCondicion(Where, chkExcluirTipoCliente.Valor)

        'Lista de Precio
        cbxListaPrecio.EstablecerCondicion(Where, chkExcluirListaPrecio.Valor)

    End Sub


    Sub ArmarSubTitulo(ByRef Subtitulo As String)
        Subtitulo = ""
        If chkFechaFacturacion.chk.Checked Then
            Subtitulo = "Periodo Facturacion: " & txtDesdeFacturado.GetValue & " - " & txtHastaFacturado.GetValue & " "
        Else
            Subtitulo = "Periodo Pedido: " & txtDesdeFacturado.GetValue & " - " & txtHastaFacturado.GetValue & " "
        End If


        If chkDeposito.Valor = True Then
            Subtitulo = Subtitulo & " - Deposito: " & cbxDeposito.Texto
        End If
        If chkTipoProducto.Valor = True Then
            Subtitulo = Subtitulo & " - Tipo. Prod: " & cbxTipoProducto.Texto
        End If
    End Sub


    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Pedido", " Select Top (0) Fecha, Numero, Ciudad, Sucursal , Cliente , Deposito from VPedido ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Private Sub CargaFormaPago()
        If chkContado.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 1"
        End If
        If chkCredito.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 2"
        End If
        If chkDiferido.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 3"
        End If
        If chkDonacion.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 4"
        End If
        If chkMuestra.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 5"
        End If
        If chkBonifiacion.Valor = True Then
            FormaPago = FormaPago & " or IDFormaPagoFactura = 6"
        End If
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkVendedor_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub frmPedidos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkPedido_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxPedidos.Enabled = value
    End Sub

    Private Sub chkVendedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVendedor.Load

    End Sub

    Private Sub chkActivo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkPedido.PropertyChanged
        cbxPedidos.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.Load

    End Sub

    Private Sub ckhUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles ckhUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkTipoProducto_Load(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value

    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        If cbxTipoInforme.Texto = "RESUMIDO" Then
            chkProducto.chk.Checked = False
            chkProducto.Enabled = False
        Else
            chkProducto.Enabled = True
        End If

        If cbxTipoInforme.Texto = "DETALLADO CON DESCUENTOS" Then
            chkPedido.chk.Checked = True
            cbxPedidos.cbx.SelectedIndex = 1
            cbxPedidos.Enabled = False
            chkPedido.Enabled = False
        Else
            chkPedido.Enabled = True
            If chkPedido.chk.Checked Then
                cbxPedidos.Enabled = True
            Else
                cbxPedidos.Enabled = False
            End If

        End If

    End Sub

    Private Sub chkTipoCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub

    Private Sub chkListaPrecio_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkListaPrecio.PropertyChanged
        cbxListaPrecio.Enabled = value
    End Sub

    Private Sub chkTotalPorProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTotalPorProducto.PropertyChanged
        chkSinIVA.Enabled = value
        chkTotalDevoluciones.Enabled = value
    End Sub

    Private Sub chkFechaFacturar_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaFacturar.PropertyChanged
        txtDesdeFacturar.Enabled = value
        txtHastaFacturar.Enabled = value
    End Sub
    Private Sub chkFechaFacturacion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaFacturacion.PropertyChanged
        txtDesdeFacturado.Enabled = value
        txtHastaFacturado.Enabled = value
    End Sub
    Private Sub chkFechaPedido_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaPedido.PropertyChanged
        txtDesdePedido.Enabled = value
        txtHastaPedido.Enabled = value
    End Sub

    Private Sub frmPedidos_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        LiberarMemoria()
    End Sub
End Class
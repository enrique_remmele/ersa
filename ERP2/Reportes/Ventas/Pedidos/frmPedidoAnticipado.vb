﻿Imports ERP.Reporte
Public Class frmPedidoAnticipado
    'CLASES
    Dim CReporte As New CPedido
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "EXTRACTO PEDIDOS CON ANTICIPACION"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        'Formularios
        Me.AcceptButton = New Button
        Me.KeyPreview = True

        CargarInformacion()
        txtDesde.Hoy()
        txtHasta.UnaSemana()

    End Sub

    Sub CargarInformacion()
        Try
            'Moneda
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Cond As String = ""
        Dim frm As New frmReporte
        Dim Orden As Boolean = True
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        If Not IsDate(txtDesde.txt.Text) Or Not IsDate(txtHasta.txt.Text) Then
            Dim mensaje As String = "La fecha no es correcta!"
            ctrError.SetError(txtDesde, mensaje)
            ctrError.SetIconAlignment(txtDesde, ErrorIconAlignment.TopRight)
            tsslEstado.Text = mensaje
            Exit Sub
        End If

        If chkSucursal.Valor = True Then
            Where = "@FechaDesde='" & txtDesde.GetValueString & "',@FechaHasta='" & txtHasta.GetValueString & "',@IDSucursal=" & cbxSucusal.GetValue
        Else
            Where = "@FechaDesde='" & txtDesde.GetValueString & "',@FechaHasta='" & txtHasta.GetValueString & "',@IDSucursal= 0"
        End If


        WhereDetalle = "IDTipoProducto = " & cbxTipoProducto.GetValue


        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        CReporte.ListadoPedidosAnticipados(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, True)


    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmExtractoMovimientoCliente_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucusal.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub frmPedidoAnticipado_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkVendedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub
End Class




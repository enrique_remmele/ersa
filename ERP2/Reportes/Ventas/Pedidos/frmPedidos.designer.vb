﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPedidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkExcluirProducto = New ERP.ocxCHK()
        Me.chkExcluirListaPrecio = New ERP.ocxCHK()
        Me.chkExcluirTipoCliente = New ERP.ocxCHK()
        Me.chkExluirUsuario = New ERP.ocxCHK()
        Me.chkExcluirTipoProducto = New ERP.ocxCHK()
        Me.chkExcluirDeposito = New ERP.ocxCHK()
        Me.chkExcluirVendedor = New ERP.ocxCHK()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkListaPrecio = New ERP.ocxCHK()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxListaPrecio = New ERP.ocxCBX()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.ckhUsuario = New ERP.ocxCHK()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.chkPedido = New ERP.ocxCHK()
        Me.cbxPedidos = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkFechaFacturar = New ERP.ocxCHK()
        Me.txtHastaFacturar = New ERP.ocxTXTDate()
        Me.txtDesdeFacturar = New ERP.ocxTXTDate()
        Me.chkTotalDevoluciones = New ERP.ocxCHK()
        Me.chkSinIVA = New ERP.ocxCHK()
        Me.chkExcluirCancelacionAutomatica = New ERP.ocxCHK()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.chkEntregaAlCliente = New ERP.ocxCHK()
        Me.chkFechaFacturacion = New ERP.ocxCHK()
        Me.chkFechaPedido = New ERP.ocxCHK()
        Me.txtHastaPedido = New ERP.ocxTXTDate()
        Me.txtDesdePedido = New ERP.ocxTXTDate()
        Me.chkTotalPorProducto = New ERP.ocxCHK()
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHastaFacturado = New ERP.ocxTXTDate()
        Me.txtDesdeFacturado = New ERP.ocxTXTDate()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkBonifiacion = New ERP.ocxCHK()
        Me.chkMuestra = New ERP.ocxCHK()
        Me.chkDonacion = New ERP.ocxCHK()
        Me.chkDiferido = New ERP.ocxCHK()
        Me.chkCredito = New ERP.ocxCHK()
        Me.chkContado = New ERP.ocxCHK()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkExcluirProducto)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.chkExluirUsuario)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirVendedor)
        Me.gbxFiltro.Controls.Add(Me.Label3)
        Me.gbxFiltro.Controls.Add(Me.chkListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.chkTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxListaPrecio)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.ckhUsuario)
        Me.gbxFiltro.Controls.Add(Me.cbxUsuario)
        Me.gbxFiltro.Controls.Add(Me.chkPedido)
        Me.gbxFiltro.Controls.Add(Me.cbxPedidos)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Location = New System.Drawing.Point(2, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(489, 374)
        Me.gbxFiltro.TabIndex = 1
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkExcluirProducto
        '
        Me.chkExcluirProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirProducto.Color = System.Drawing.Color.Empty
        Me.chkExcluirProducto.Location = New System.Drawing.Point(445, 251)
        Me.chkExcluirProducto.Name = "chkExcluirProducto"
        Me.chkExcluirProducto.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirProducto.SoloLectura = False
        Me.chkExcluirProducto.TabIndex = 50
        Me.chkExcluirProducto.Texto = ""
        Me.chkExcluirProducto.Valor = False
        '
        'chkExcluirListaPrecio
        '
        Me.chkExcluirListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkExcluirListaPrecio.Location = New System.Drawing.Point(445, 219)
        Me.chkExcluirListaPrecio.Name = "chkExcluirListaPrecio"
        Me.chkExcluirListaPrecio.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirListaPrecio.SoloLectura = False
        Me.chkExcluirListaPrecio.TabIndex = 49
        Me.chkExcluirListaPrecio.Texto = ""
        Me.chkExcluirListaPrecio.Valor = False
        '
        'chkExcluirTipoCliente
        '
        Me.chkExcluirTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoCliente.Location = New System.Drawing.Point(445, 185)
        Me.chkExcluirTipoCliente.Name = "chkExcluirTipoCliente"
        Me.chkExcluirTipoCliente.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoCliente.SoloLectura = False
        Me.chkExcluirTipoCliente.TabIndex = 48
        Me.chkExcluirTipoCliente.Texto = ""
        Me.chkExcluirTipoCliente.Valor = False
        '
        'chkExluirUsuario
        '
        Me.chkExluirUsuario.BackColor = System.Drawing.Color.Transparent
        Me.chkExluirUsuario.Color = System.Drawing.Color.Empty
        Me.chkExluirUsuario.Location = New System.Drawing.Point(445, 154)
        Me.chkExluirUsuario.Name = "chkExluirUsuario"
        Me.chkExluirUsuario.Size = New System.Drawing.Size(17, 13)
        Me.chkExluirUsuario.SoloLectura = False
        Me.chkExluirUsuario.TabIndex = 47
        Me.chkExluirUsuario.Texto = ""
        Me.chkExluirUsuario.Valor = False
        '
        'chkExcluirTipoProducto
        '
        Me.chkExcluirTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoProducto.Location = New System.Drawing.Point(445, 122)
        Me.chkExcluirTipoProducto.Name = "chkExcluirTipoProducto"
        Me.chkExcluirTipoProducto.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoProducto.SoloLectura = False
        Me.chkExcluirTipoProducto.TabIndex = 46
        Me.chkExcluirTipoProducto.Texto = ""
        Me.chkExcluirTipoProducto.Valor = False
        '
        'chkExcluirDeposito
        '
        Me.chkExcluirDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirDeposito.Color = System.Drawing.Color.Empty
        Me.chkExcluirDeposito.Location = New System.Drawing.Point(445, 58)
        Me.chkExcluirDeposito.Name = "chkExcluirDeposito"
        Me.chkExcluirDeposito.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirDeposito.SoloLectura = False
        Me.chkExcluirDeposito.TabIndex = 44
        Me.chkExcluirDeposito.Texto = ""
        Me.chkExcluirDeposito.Valor = False
        '
        'chkExcluirVendedor
        '
        Me.chkExcluirVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirVendedor.Color = System.Drawing.Color.Empty
        Me.chkExcluirVendedor.Location = New System.Drawing.Point(445, 26)
        Me.chkExcluirVendedor.Name = "chkExcluirVendedor"
        Me.chkExcluirVendedor.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirVendedor.SoloLectura = False
        Me.chkExcluirVendedor.TabIndex = 43
        Me.chkExcluirVendedor.Texto = ""
        Me.chkExcluirVendedor.Valor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(443, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 12)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Excluir"
        '
        'chkListaPrecio
        '
        Me.chkListaPrecio.BackColor = System.Drawing.Color.Transparent
        Me.chkListaPrecio.Color = System.Drawing.Color.Empty
        Me.chkListaPrecio.Location = New System.Drawing.Point(9, 219)
        Me.chkListaPrecio.Name = "chkListaPrecio"
        Me.chkListaPrecio.Size = New System.Drawing.Size(91, 21)
        Me.chkListaPrecio.SoloLectura = False
        Me.chkListaPrecio.TabIndex = 41
        Me.chkListaPrecio.Texto = "Lista de Precio: "
        Me.chkListaPrecio.Valor = False
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(9, 185)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(88, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 40
        Me.chkTipoCliente.Texto = "Tipo Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxListaPrecio
        '
        Me.cbxListaPrecio.CampoWhere = ""
        Me.cbxListaPrecio.CargarUnaSolaVez = False
        Me.cbxListaPrecio.DataDisplayMember = ""
        Me.cbxListaPrecio.DataFilter = Nothing
        Me.cbxListaPrecio.DataOrderBy = ""
        Me.cbxListaPrecio.DataSource = ""
        Me.cbxListaPrecio.DataValueMember = ""
        Me.cbxListaPrecio.dtSeleccionado = Nothing
        Me.cbxListaPrecio.Enabled = False
        Me.cbxListaPrecio.FormABM = Nothing
        Me.cbxListaPrecio.Indicaciones = Nothing
        Me.cbxListaPrecio.Location = New System.Drawing.Point(103, 219)
        Me.cbxListaPrecio.Name = "cbxListaPrecio"
        Me.cbxListaPrecio.SeleccionMultiple = False
        Me.cbxListaPrecio.SeleccionObligatoria = False
        Me.cbxListaPrecio.Size = New System.Drawing.Size(329, 21)
        Me.cbxListaPrecio.SoloLectura = False
        Me.cbxListaPrecio.TabIndex = 39
        Me.cbxListaPrecio.Texto = "MAYORISTA"
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "vTipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(103, 186)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(329, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 38
        Me.cbxTipoCliente.Texto = "SUPERMERCADO"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(105, 251)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(329, 65)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 36
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(9, 251)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(88, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 37
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(9, 122)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(91, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 32
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(105, 122)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(329, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 33
        Me.cbxTipoProducto.Texto = ""
        '
        'ckhUsuario
        '
        Me.ckhUsuario.BackColor = System.Drawing.Color.Transparent
        Me.ckhUsuario.Color = System.Drawing.Color.Empty
        Me.ckhUsuario.Location = New System.Drawing.Point(9, 154)
        Me.ckhUsuario.Name = "ckhUsuario"
        Me.ckhUsuario.Size = New System.Drawing.Size(75, 21)
        Me.ckhUsuario.SoloLectura = False
        Me.ckhUsuario.TabIndex = 30
        Me.ckhUsuario.Texto = "Usuario:"
        Me.ckhUsuario.Valor = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "vUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(105, 154)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(329, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 31
        Me.cbxUsuario.Texto = "CAST"
        '
        'chkPedido
        '
        Me.chkPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkPedido.Color = System.Drawing.Color.Empty
        Me.chkPedido.Location = New System.Drawing.Point(9, 90)
        Me.chkPedido.Name = "chkPedido"
        Me.chkPedido.Size = New System.Drawing.Size(78, 21)
        Me.chkPedido.SoloLectura = False
        Me.chkPedido.TabIndex = 29
        Me.chkPedido.Texto = "Pedidos:"
        Me.chkPedido.Valor = False
        '
        'cbxPedidos
        '
        Me.cbxPedidos.CampoWhere = Nothing
        Me.cbxPedidos.CargarUnaSolaVez = False
        Me.cbxPedidos.DataDisplayMember = Nothing
        Me.cbxPedidos.DataFilter = Nothing
        Me.cbxPedidos.DataOrderBy = Nothing
        Me.cbxPedidos.DataSource = Nothing
        Me.cbxPedidos.DataValueMember = Nothing
        Me.cbxPedidos.dtSeleccionado = Nothing
        Me.cbxPedidos.Enabled = False
        Me.cbxPedidos.FormABM = Nothing
        Me.cbxPedidos.Indicaciones = Nothing
        Me.cbxPedidos.Location = New System.Drawing.Point(105, 90)
        Me.cbxPedidos.Name = "cbxPedidos"
        Me.cbxPedidos.SeleccionMultiple = False
        Me.cbxPedidos.SeleccionObligatoria = False
        Me.cbxPedidos.Size = New System.Drawing.Size(329, 21)
        Me.cbxPedidos.SoloLectura = False
        Me.cbxPedidos.TabIndex = 28
        Me.cbxPedidos.Texto = "TODOS"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(9, 328)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(423, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 19
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(9, 58)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(75, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 20
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(105, 58)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(329, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 21
        Me.cbxDeposito.Texto = "CENTRAL"
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(9, 26)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(71, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 4
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "Vendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(105, 26)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(329, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 5
        Me.cbxVendedor.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 307)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(77, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 18
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(9, 287)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 30
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaFacturar)
        Me.GroupBox2.Controls.Add(Me.txtHastaFacturar)
        Me.GroupBox2.Controls.Add(Me.txtDesdeFacturar)
        Me.GroupBox2.Controls.Add(Me.chkTotalDevoluciones)
        Me.GroupBox2.Controls.Add(Me.chkSinIVA)
        Me.GroupBox2.Controls.Add(Me.chkExcluirCancelacionAutomatica)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxMoneda)
        Me.GroupBox2.Controls.Add(Me.chkEntregaAlCliente)
        Me.GroupBox2.Controls.Add(Me.chkFechaFacturacion)
        Me.GroupBox2.Controls.Add(Me.chkFechaPedido)
        Me.GroupBox2.Controls.Add(Me.txtHastaPedido)
        Me.GroupBox2.Controls.Add(Me.txtDesdePedido)
        Me.GroupBox2.Controls.Add(Me.chkTotalPorProducto)
        Me.GroupBox2.Controls.Add(Me.cbxTipoInforme)
        Me.GroupBox2.Controls.Add(Me.lblInforme)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHastaFacturado)
        Me.GroupBox2.Controls.Add(Me.txtDesdeFacturado)
        Me.GroupBox2.Location = New System.Drawing.Point(513, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(187, 472)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkFechaFacturar
        '
        Me.chkFechaFacturar.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaFacturar.Color = System.Drawing.Color.Empty
        Me.chkFechaFacturar.Location = New System.Drawing.Point(11, 122)
        Me.chkFechaFacturar.Name = "chkFechaFacturar"
        Me.chkFechaFacturar.Size = New System.Drawing.Size(165, 21)
        Me.chkFechaFacturar.SoloLectura = False
        Me.chkFechaFacturar.TabIndex = 45
        Me.chkFechaFacturar.Texto = "Fecha Facturar:"
        Me.chkFechaFacturar.Valor = False
        '
        'txtHastaFacturar
        '
        Me.txtHastaFacturar.AñoFecha = 0
        Me.txtHastaFacturar.Color = System.Drawing.Color.Empty
        Me.txtHastaFacturar.Enabled = False
        Me.txtHastaFacturar.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHastaFacturar.Location = New System.Drawing.Point(92, 150)
        Me.txtHastaFacturar.MesFecha = 0
        Me.txtHastaFacturar.Name = "txtHastaFacturar"
        Me.txtHastaFacturar.PermitirNulo = False
        Me.txtHastaFacturar.Size = New System.Drawing.Size(84, 20)
        Me.txtHastaFacturar.SoloLectura = False
        Me.txtHastaFacturar.TabIndex = 44
        '
        'txtDesdeFacturar
        '
        Me.txtDesdeFacturar.AñoFecha = 0
        Me.txtDesdeFacturar.Color = System.Drawing.Color.Empty
        Me.txtDesdeFacturar.Enabled = False
        Me.txtDesdeFacturar.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesdeFacturar.Location = New System.Drawing.Point(12, 150)
        Me.txtDesdeFacturar.MesFecha = 0
        Me.txtDesdeFacturar.Name = "txtDesdeFacturar"
        Me.txtDesdeFacturar.PermitirNulo = False
        Me.txtDesdeFacturar.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeFacturar.SoloLectura = False
        Me.txtDesdeFacturar.TabIndex = 43
        '
        'chkTotalDevoluciones
        '
        Me.chkTotalDevoluciones.BackColor = System.Drawing.Color.Transparent
        Me.chkTotalDevoluciones.Color = System.Drawing.Color.Empty
        Me.chkTotalDevoluciones.Enabled = False
        Me.chkTotalDevoluciones.Location = New System.Drawing.Point(8, 434)
        Me.chkTotalDevoluciones.Name = "chkTotalDevoluciones"
        Me.chkTotalDevoluciones.Size = New System.Drawing.Size(164, 21)
        Me.chkTotalDevoluciones.SoloLectura = False
        Me.chkTotalDevoluciones.TabIndex = 42
        Me.chkTotalDevoluciones.Texto = "Total Devoluciones"
        Me.chkTotalDevoluciones.Valor = False
        '
        'chkSinIVA
        '
        Me.chkSinIVA.BackColor = System.Drawing.Color.Transparent
        Me.chkSinIVA.Color = System.Drawing.Color.Empty
        Me.chkSinIVA.Enabled = False
        Me.chkSinIVA.Location = New System.Drawing.Point(8, 407)
        Me.chkSinIVA.Name = "chkSinIVA"
        Me.chkSinIVA.Size = New System.Drawing.Size(164, 21)
        Me.chkSinIVA.SoloLectura = False
        Me.chkSinIVA.TabIndex = 41
        Me.chkSinIVA.Texto = "Sin IVA"
        Me.chkSinIVA.Valor = False
        '
        'chkExcluirCancelacionAutomatica
        '
        Me.chkExcluirCancelacionAutomatica.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirCancelacionAutomatica.Color = System.Drawing.Color.Empty
        Me.chkExcluirCancelacionAutomatica.Location = New System.Drawing.Point(8, 359)
        Me.chkExcluirCancelacionAutomatica.Name = "chkExcluirCancelacionAutomatica"
        Me.chkExcluirCancelacionAutomatica.Size = New System.Drawing.Size(164, 21)
        Me.chkExcluirCancelacionAutomatica.SoloLectura = False
        Me.chkExcluirCancelacionAutomatica.TabIndex = 40
        Me.chkExcluirCancelacionAutomatica.Texto = "Excluir Cancelacion Autom."
        Me.chkExcluirCancelacionAutomatica.Valor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 234)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Moneda:"
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = Nothing
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = Nothing
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = Nothing
        Me.cbxMoneda.DataValueMember = Nothing
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(11, 250)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(161, 23)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 38
        Me.cbxMoneda.Texto = ""
        '
        'chkEntregaAlCliente
        '
        Me.chkEntregaAlCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkEntregaAlCliente.Color = System.Drawing.Color.Empty
        Me.chkEntregaAlCliente.Location = New System.Drawing.Point(8, 332)
        Me.chkEntregaAlCliente.Name = "chkEntregaAlCliente"
        Me.chkEntregaAlCliente.Size = New System.Drawing.Size(164, 21)
        Me.chkEntregaAlCliente.SoloLectura = False
        Me.chkEntregaAlCliente.TabIndex = 37
        Me.chkEntregaAlCliente.Texto = "Entrega Directa al Cliente"
        Me.chkEntregaAlCliente.Valor = False
        '
        'chkFechaFacturacion
        '
        Me.chkFechaFacturacion.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaFacturacion.Color = System.Drawing.Color.Empty
        Me.chkFechaFacturacion.Location = New System.Drawing.Point(11, 16)
        Me.chkFechaFacturacion.Name = "chkFechaFacturacion"
        Me.chkFechaFacturacion.Size = New System.Drawing.Size(165, 21)
        Me.chkFechaFacturacion.SoloLectura = False
        Me.chkFechaFacturacion.TabIndex = 36
        Me.chkFechaFacturacion.Texto = "Fecha Facturacion:"
        Me.chkFechaFacturacion.Valor = False
        '
        'chkFechaPedido
        '
        Me.chkFechaPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaPedido.Color = System.Drawing.Color.Empty
        Me.chkFechaPedido.Location = New System.Drawing.Point(11, 70)
        Me.chkFechaPedido.Name = "chkFechaPedido"
        Me.chkFechaPedido.Size = New System.Drawing.Size(170, 21)
        Me.chkFechaPedido.SoloLectura = False
        Me.chkFechaPedido.TabIndex = 32
        Me.chkFechaPedido.Texto = "Fecha Pedido"
        Me.chkFechaPedido.Valor = False
        '
        'txtHastaPedido
        '
        Me.txtHastaPedido.AñoFecha = 0
        Me.txtHastaPedido.Color = System.Drawing.Color.Empty
        Me.txtHastaPedido.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHastaPedido.Location = New System.Drawing.Point(92, 96)
        Me.txtHastaPedido.MesFecha = 0
        Me.txtHastaPedido.Name = "txtHastaPedido"
        Me.txtHastaPedido.PermitirNulo = False
        Me.txtHastaPedido.Size = New System.Drawing.Size(84, 20)
        Me.txtHastaPedido.SoloLectura = False
        Me.txtHastaPedido.TabIndex = 35
        '
        'txtDesdePedido
        '
        Me.txtDesdePedido.AñoFecha = 0
        Me.txtDesdePedido.Color = System.Drawing.Color.Empty
        Me.txtDesdePedido.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesdePedido.Location = New System.Drawing.Point(12, 96)
        Me.txtDesdePedido.MesFecha = 0
        Me.txtDesdePedido.Name = "txtDesdePedido"
        Me.txtDesdePedido.PermitirNulo = False
        Me.txtDesdePedido.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdePedido.SoloLectura = False
        Me.txtDesdePedido.TabIndex = 34
        '
        'chkTotalPorProducto
        '
        Me.chkTotalPorProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTotalPorProducto.Color = System.Drawing.Color.Empty
        Me.chkTotalPorProducto.Location = New System.Drawing.Point(9, 382)
        Me.chkTotalPorProducto.Name = "chkTotalPorProducto"
        Me.chkTotalPorProducto.Size = New System.Drawing.Size(164, 21)
        Me.chkTotalPorProducto.SoloLectura = False
        Me.chkTotalPorProducto.TabIndex = 33
        Me.chkTotalPorProducto.Texto = "Total por Producto"
        Me.chkTotalPorProducto.Valor = False
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.dtSeleccionado = Nothing
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(9, 202)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionMultiple = False
        Me.cbxTipoInforme.SeleccionObligatoria = False
        Me.cbxTipoInforme.Size = New System.Drawing.Size(164, 21)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 27
        Me.cbxTipoInforme.Texto = ""
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(9, 185)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(45, 13)
        Me.lblInforme.TabIndex = 26
        Me.lblInforme.Text = "Informe:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(131, 251)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(45, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 32
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(9, 305)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(113, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 31
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHastaFacturado
        '
        Me.txtHastaFacturado.AñoFecha = 0
        Me.txtHastaFacturado.Color = System.Drawing.Color.Empty
        Me.txtHastaFacturado.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHastaFacturado.Location = New System.Drawing.Point(92, 44)
        Me.txtHastaFacturado.MesFecha = 0
        Me.txtHastaFacturado.Name = "txtHastaFacturado"
        Me.txtHastaFacturado.PermitirNulo = False
        Me.txtHastaFacturado.Size = New System.Drawing.Size(84, 20)
        Me.txtHastaFacturado.SoloLectura = False
        Me.txtHastaFacturado.TabIndex = 2
        '
        'txtDesdeFacturado
        '
        Me.txtDesdeFacturado.AñoFecha = 0
        Me.txtDesdeFacturado.Color = System.Drawing.Color.Empty
        Me.txtDesdeFacturado.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesdeFacturado.Location = New System.Drawing.Point(12, 44)
        Me.txtDesdeFacturado.MesFecha = 0
        Me.txtDesdeFacturado.Name = "txtDesdeFacturado"
        Me.txtDesdeFacturado.PermitirNulo = False
        Me.txtDesdeFacturado.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeFacturado.SoloLectura = False
        Me.txtDesdeFacturado.TabIndex = 1
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(637, 501)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 25
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(494, 501)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 24
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkBonifiacion)
        Me.GroupBox1.Controls.Add(Me.chkMuestra)
        Me.GroupBox1.Controls.Add(Me.chkDonacion)
        Me.GroupBox1.Controls.Add(Me.chkDiferido)
        Me.GroupBox1.Controls.Add(Me.chkCredito)
        Me.GroupBox1.Controls.Add(Me.chkContado)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 389)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(390, 67)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Formas de Pago"
        '
        'chkBonifiacion
        '
        Me.chkBonifiacion.BackColor = System.Drawing.Color.Transparent
        Me.chkBonifiacion.Color = System.Drawing.Color.Empty
        Me.chkBonifiacion.Location = New System.Drawing.Point(227, 40)
        Me.chkBonifiacion.Name = "chkBonifiacion"
        Me.chkBonifiacion.Size = New System.Drawing.Size(109, 21)
        Me.chkBonifiacion.SoloLectura = False
        Me.chkBonifiacion.TabIndex = 27
        Me.chkBonifiacion.Texto = "BONIFICACION"
        Me.chkBonifiacion.Valor = False
        '
        'chkMuestra
        '
        Me.chkMuestra.BackColor = System.Drawing.Color.Transparent
        Me.chkMuestra.Color = System.Drawing.Color.Empty
        Me.chkMuestra.Location = New System.Drawing.Point(227, 19)
        Me.chkMuestra.Name = "chkMuestra"
        Me.chkMuestra.Size = New System.Drawing.Size(85, 21)
        Me.chkMuestra.SoloLectura = False
        Me.chkMuestra.TabIndex = 26
        Me.chkMuestra.Texto = "MUESTRA"
        Me.chkMuestra.Valor = False
        '
        'chkDonacion
        '
        Me.chkDonacion.BackColor = System.Drawing.Color.Transparent
        Me.chkDonacion.Color = System.Drawing.Color.Empty
        Me.chkDonacion.Location = New System.Drawing.Point(112, 18)
        Me.chkDonacion.Name = "chkDonacion"
        Me.chkDonacion.Size = New System.Drawing.Size(109, 21)
        Me.chkDonacion.SoloLectura = False
        Me.chkDonacion.TabIndex = 25
        Me.chkDonacion.Texto = "DONACION"
        Me.chkDonacion.Valor = False
        '
        'chkDiferido
        '
        Me.chkDiferido.BackColor = System.Drawing.Color.Transparent
        Me.chkDiferido.Color = System.Drawing.Color.Empty
        Me.chkDiferido.Location = New System.Drawing.Point(112, 40)
        Me.chkDiferido.Name = "chkDiferido"
        Me.chkDiferido.Size = New System.Drawing.Size(71, 21)
        Me.chkDiferido.SoloLectura = False
        Me.chkDiferido.TabIndex = 24
        Me.chkDiferido.Texto = "DIFERIDO"
        Me.chkDiferido.Valor = False
        '
        'chkCredito
        '
        Me.chkCredito.BackColor = System.Drawing.Color.Transparent
        Me.chkCredito.Color = System.Drawing.Color.Empty
        Me.chkCredito.Location = New System.Drawing.Point(9, 39)
        Me.chkCredito.Name = "chkCredito"
        Me.chkCredito.Size = New System.Drawing.Size(71, 21)
        Me.chkCredito.SoloLectura = False
        Me.chkCredito.TabIndex = 23
        Me.chkCredito.Texto = "CREDITO"
        Me.chkCredito.Valor = False
        '
        'chkContado
        '
        Me.chkContado.BackColor = System.Drawing.Color.Transparent
        Me.chkContado.Color = System.Drawing.Color.Empty
        Me.chkContado.Location = New System.Drawing.Point(9, 19)
        Me.chkContado.Name = "chkContado"
        Me.chkContado.Size = New System.Drawing.Size(71, 21)
        Me.chkContado.SoloLectura = False
        Me.chkContado.TabIndex = 22
        Me.chkContado.Texto = "CONTADO"
        Me.chkContado.Valor = False
        '
        'frmPedidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 555)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmPedidos"
        Me.Text = "frmPedido"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtHastaFacturado As ERP.ocxTXTDate
    Friend WithEvents txtDesdeFacturado As ERP.ocxTXTDate
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxTipoInforme As ERP.ocxCBX
    Friend WithEvents lblInforme As System.Windows.Forms.Label
    Friend WithEvents cbxPedidos As ERP.ocxCBX
    Friend WithEvents chkPedido As ERP.ocxCHK
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkBonifiacion As ERP.ocxCHK
    Friend WithEvents chkMuestra As ERP.ocxCHK
    Friend WithEvents chkDonacion As ERP.ocxCHK
    Friend WithEvents chkDiferido As ERP.ocxCHK
    Friend WithEvents chkCredito As ERP.ocxCHK
    Friend WithEvents chkContado As ERP.ocxCHK
    Friend WithEvents chkTotalPorProducto As ERP.ocxCHK
    Friend WithEvents ckhUsuario As ERP.ocxCHK
    Friend WithEvents cbxUsuario As ERP.ocxCBX
    Friend WithEvents chkFechaFacturacion As ERP.ocxCHK
    Friend WithEvents chkFechaPedido As ERP.ocxCHK
    Friend WithEvents txtHastaPedido As ERP.ocxTXTDate
    Friend WithEvents txtDesdePedido As ERP.ocxTXTDate
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents txtProducto As ERP.ocxTXTProducto
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents chkEntregaAlCliente As ERP.ocxCHK
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxMoneda As ocxCBX
    Friend WithEvents chkListaPrecio As ocxCHK
    Friend WithEvents chkTipoCliente As ocxCHK
    Friend WithEvents cbxListaPrecio As ocxCBX
    Friend WithEvents cbxTipoCliente As ocxCBX
    Friend WithEvents Label3 As Label
    Friend WithEvents chkExcluirProducto As ocxCHK
    Friend WithEvents chkExcluirListaPrecio As ocxCHK
    Friend WithEvents chkExcluirTipoCliente As ocxCHK
    Friend WithEvents chkExluirUsuario As ocxCHK
    Friend WithEvents chkExcluirTipoProducto As ocxCHK
    Friend WithEvents chkExcluirDeposito As ocxCHK
    Friend WithEvents chkExcluirVendedor As ocxCHK
    Friend WithEvents chkExcluirCancelacionAutomatica As ocxCHK
    Friend WithEvents chkTotalDevoluciones As ocxCHK
    Friend WithEvents chkSinIVA As ocxCHK
    Friend WithEvents chkFechaFacturar As ocxCHK
    Friend WithEvents txtHastaFacturar As ocxTXTDate
    Friend WithEvents txtDesdeFacturar As ocxTXTDate
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoNotaCreditoDetalle
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkFechaVenta = New ERP.ocxCHK()
        Me.chkFechaNC = New ERP.ocxCHK()
        Me.txtHastaVenta = New ERP.ocxTXTDate()
        Me.txtDesdeVenta = New ERP.ocxTXTDate()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkDetalladoProductos = New ERP.ocxCHK()
        Me.chkVerAnulados = New ERP.ocxCHK()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkMotivoDevolucion = New ERP.ocxCHK()
        Me.cbxMotivoDevolucion = New ERP.ocxCBX()
        Me.chkTipo = New ERP.ocxCHK()
        Me.cbxMotivoNC = New ERP.ocxCBX()
        Me.chkMotivoAnulacion = New ERP.ocxCHK()
        Me.cbxMotivoAnulacion = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxCliente = New ERP.ocxCBX()
        Me.chkProducto = New ERP.ocxCHK()
        Me.cbxProducto = New ERP.ocxCBX()
        Me.chkDeposito = New ERP.ocxCHK()
        Me.cbxDeposito = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCiudad = New ERP.ocxCHK()
        Me.cbxCiudad = New ERP.ocxCBX()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.chkLinea = New ERP.ocxCHK()
        Me.cbxLinea = New ERP.ocxCBX()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(392, 297)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 10
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaVenta)
        Me.GroupBox2.Controls.Add(Me.chkFechaNC)
        Me.GroupBox2.Controls.Add(Me.txtHastaVenta)
        Me.GroupBox2.Controls.Add(Me.txtDesdeVenta)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.chkDetalladoProductos)
        Me.GroupBox2.Controls.Add(Me.chkVerAnulados)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Location = New System.Drawing.Point(386, -1)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 292)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkFechaVenta
        '
        Me.chkFechaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaVenta.Color = System.Drawing.Color.Empty
        Me.chkFechaVenta.Location = New System.Drawing.Point(6, 70)
        Me.chkFechaVenta.Name = "chkFechaVenta"
        Me.chkFechaVenta.Size = New System.Drawing.Size(133, 17)
        Me.chkFechaVenta.SoloLectura = False
        Me.chkFechaVenta.TabIndex = 32
        Me.chkFechaVenta.Texto = "Fecha Factura"
        Me.chkFechaVenta.Valor = False
        '
        'chkFechaNC
        '
        Me.chkFechaNC.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaNC.Color = System.Drawing.Color.Empty
        Me.chkFechaNC.Location = New System.Drawing.Point(6, 17)
        Me.chkFechaNC.Name = "chkFechaNC"
        Me.chkFechaNC.Size = New System.Drawing.Size(133, 17)
        Me.chkFechaNC.SoloLectura = False
        Me.chkFechaNC.TabIndex = 31
        Me.chkFechaNC.Texto = "Fecha Nota de Credito"
        Me.chkFechaNC.Valor = True
        '
        'txtHastaVenta
        '
        Me.txtHastaVenta.AñoFecha = 0
        Me.txtHastaVenta.Color = System.Drawing.Color.Empty
        Me.txtHastaVenta.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtHastaVenta.Location = New System.Drawing.Point(89, 92)
        Me.txtHastaVenta.MesFecha = 0
        Me.txtHastaVenta.Name = "txtHastaVenta"
        Me.txtHastaVenta.PermitirNulo = False
        Me.txtHastaVenta.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaVenta.SoloLectura = False
        Me.txtHastaVenta.TabIndex = 30
        '
        'txtDesdeVenta
        '
        Me.txtDesdeVenta.AñoFecha = 0
        Me.txtDesdeVenta.Color = System.Drawing.Color.Empty
        Me.txtDesdeVenta.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtDesdeVenta.Location = New System.Drawing.Point(6, 92)
        Me.txtDesdeVenta.MesFecha = 0
        Me.txtDesdeVenta.Name = "txtDesdeVenta"
        Me.txtDesdeVenta.PermitirNulo = False
        Me.txtDesdeVenta.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeVenta.SoloLectura = False
        Me.txtDesdeVenta.TabIndex = 29
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtHasta.Location = New System.Drawing.Point(89, 36)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 28
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtDesde.Location = New System.Drawing.Point(6, 36)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 27
        '
        'chkDetalladoProductos
        '
        Me.chkDetalladoProductos.BackColor = System.Drawing.Color.Transparent
        Me.chkDetalladoProductos.Color = System.Drawing.Color.Empty
        Me.chkDetalladoProductos.Location = New System.Drawing.Point(8, 210)
        Me.chkDetalladoProductos.Name = "chkDetalladoProductos"
        Me.chkDetalladoProductos.Size = New System.Drawing.Size(200, 21)
        Me.chkDetalladoProductos.SoloLectura = False
        Me.chkDetalladoProductos.TabIndex = 23
        Me.chkDetalladoProductos.Texto = "Ver detalle de productos"
        Me.chkDetalladoProductos.Valor = False
        '
        'chkVerAnulados
        '
        Me.chkVerAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkVerAnulados.Color = System.Drawing.Color.Empty
        Me.chkVerAnulados.Location = New System.Drawing.Point(8, 185)
        Me.chkVerAnulados.Name = "chkVerAnulados"
        Me.chkVerAnulados.Size = New System.Drawing.Size(200, 21)
        Me.chkVerAnulados.SoloLectura = False
        Me.chkVerAnulados.TabIndex = 22
        Me.chkVerAnulados.Texto = "Ver anulados"
        Me.chkVerAnulados.Valor = False
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 158)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 158)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 141)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkLinea)
        Me.gbxFiltro.Controls.Add(Me.cbxLinea)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMotivoDevolucion)
        Me.gbxFiltro.Controls.Add(Me.cbxMotivoDevolucion)
        Me.gbxFiltro.Controls.Add(Me.chkTipo)
        Me.gbxFiltro.Controls.Add(Me.cbxMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.chkMotivoAnulacion)
        Me.gbxFiltro.Controls.Add(Me.cbxMotivoAnulacion)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxCliente)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxProducto)
        Me.gbxFiltro.Controls.Add(Me.chkDeposito)
        Me.gbxFiltro.Controls.Add(Me.cbxDeposito)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCiudad)
        Me.gbxFiltro.Controls.Add(Me.cbxCiudad)
        Me.gbxFiltro.Location = New System.Drawing.Point(3, -1)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(377, 292)
        Me.gbxFiltro.TabIndex = 8
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(9, 100)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 32
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "VDetalleNotaCredito.IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(129, 100)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 33
        Me.cbxTipoProducto.Texto = ""
        '
        'chkMotivoDevolucion
        '
        Me.chkMotivoDevolucion.BackColor = System.Drawing.Color.Transparent
        Me.chkMotivoDevolucion.Color = System.Drawing.Color.Empty
        Me.chkMotivoDevolucion.Enabled = False
        Me.chkMotivoDevolucion.Location = New System.Drawing.Point(9, 251)
        Me.chkMotivoDevolucion.Name = "chkMotivoDevolucion"
        Me.chkMotivoDevolucion.Size = New System.Drawing.Size(114, 21)
        Me.chkMotivoDevolucion.SoloLectura = False
        Me.chkMotivoDevolucion.TabIndex = 31
        Me.chkMotivoDevolucion.Texto = "Motivo Devolucion:"
        Me.chkMotivoDevolucion.Valor = False
        '
        'cbxMotivoDevolucion
        '
        Me.cbxMotivoDevolucion.CampoWhere = "IDMotivoDevolucion"
        Me.cbxMotivoDevolucion.CargarUnaSolaVez = False
        Me.cbxMotivoDevolucion.DataDisplayMember = "Descripcion"
        Me.cbxMotivoDevolucion.DataFilter = Nothing
        Me.cbxMotivoDevolucion.DataOrderBy = "Descripcion"
        Me.cbxMotivoDevolucion.DataSource = "MotivoDevolucionNC"
        Me.cbxMotivoDevolucion.DataValueMember = "ID"
        Me.cbxMotivoDevolucion.dtSeleccionado = Nothing
        Me.cbxMotivoDevolucion.Enabled = False
        Me.cbxMotivoDevolucion.FormABM = Nothing
        Me.cbxMotivoDevolucion.Indicaciones = Nothing
        Me.cbxMotivoDevolucion.Location = New System.Drawing.Point(129, 251)
        Me.cbxMotivoDevolucion.Name = "cbxMotivoDevolucion"
        Me.cbxMotivoDevolucion.SeleccionMultiple = False
        Me.cbxMotivoDevolucion.SeleccionObligatoria = False
        Me.cbxMotivoDevolucion.Size = New System.Drawing.Size(213, 21)
        Me.cbxMotivoDevolucion.SoloLectura = False
        Me.cbxMotivoDevolucion.TabIndex = 30
        Me.cbxMotivoDevolucion.Texto = ""
        '
        'chkTipo
        '
        Me.chkTipo.BackColor = System.Drawing.Color.Transparent
        Me.chkTipo.Color = System.Drawing.Color.Empty
        Me.chkTipo.Location = New System.Drawing.Point(9, 224)
        Me.chkTipo.Name = "chkTipo"
        Me.chkTipo.Size = New System.Drawing.Size(114, 21)
        Me.chkTipo.SoloLectura = False
        Me.chkTipo.TabIndex = 29
        Me.chkTipo.Texto = "Motivo de NC:"
        Me.chkTipo.Valor = False
        '
        'cbxMotivoNC
        '
        Me.cbxMotivoNC.CampoWhere = Nothing
        Me.cbxMotivoNC.CargarUnaSolaVez = False
        Me.cbxMotivoNC.DataDisplayMember = Nothing
        Me.cbxMotivoNC.DataFilter = Nothing
        Me.cbxMotivoNC.DataOrderBy = Nothing
        Me.cbxMotivoNC.DataSource = Nothing
        Me.cbxMotivoNC.DataValueMember = Nothing
        Me.cbxMotivoNC.dtSeleccionado = Nothing
        Me.cbxMotivoNC.Enabled = False
        Me.cbxMotivoNC.FormABM = Nothing
        Me.cbxMotivoNC.Indicaciones = Nothing
        Me.cbxMotivoNC.Location = New System.Drawing.Point(129, 224)
        Me.cbxMotivoNC.Name = "cbxMotivoNC"
        Me.cbxMotivoNC.SeleccionMultiple = False
        Me.cbxMotivoNC.SeleccionObligatoria = False
        Me.cbxMotivoNC.Size = New System.Drawing.Size(213, 21)
        Me.cbxMotivoNC.SoloLectura = False
        Me.cbxMotivoNC.TabIndex = 28
        Me.cbxMotivoNC.Texto = ""
        '
        'chkMotivoAnulacion
        '
        Me.chkMotivoAnulacion.BackColor = System.Drawing.Color.Transparent
        Me.chkMotivoAnulacion.Color = System.Drawing.Color.Empty
        Me.chkMotivoAnulacion.Location = New System.Drawing.Point(9, 197)
        Me.chkMotivoAnulacion.Name = "chkMotivoAnulacion"
        Me.chkMotivoAnulacion.Size = New System.Drawing.Size(114, 21)
        Me.chkMotivoAnulacion.SoloLectura = False
        Me.chkMotivoAnulacion.TabIndex = 22
        Me.chkMotivoAnulacion.Texto = "Motivo de Anulacion"
        Me.chkMotivoAnulacion.Valor = False
        '
        'cbxMotivoAnulacion
        '
        Me.cbxMotivoAnulacion.CampoWhere = "VDetalleNotaCredito.IDMotivo"
        Me.cbxMotivoAnulacion.CargarUnaSolaVez = False
        Me.cbxMotivoAnulacion.DataDisplayMember = "Descripcion"
        Me.cbxMotivoAnulacion.DataFilter = Nothing
        Me.cbxMotivoAnulacion.DataOrderBy = "Descripcion"
        Me.cbxMotivoAnulacion.DataSource = "MotivoAnulacionNotaCredito"
        Me.cbxMotivoAnulacion.DataValueMember = "ID"
        Me.cbxMotivoAnulacion.dtSeleccionado = Nothing
        Me.cbxMotivoAnulacion.Enabled = False
        Me.cbxMotivoAnulacion.FormABM = Nothing
        Me.cbxMotivoAnulacion.Indicaciones = Nothing
        Me.cbxMotivoAnulacion.Location = New System.Drawing.Point(129, 197)
        Me.cbxMotivoAnulacion.Name = "cbxMotivoAnulacion"
        Me.cbxMotivoAnulacion.SeleccionMultiple = False
        Me.cbxMotivoAnulacion.SeleccionObligatoria = False
        Me.cbxMotivoAnulacion.Size = New System.Drawing.Size(213, 21)
        Me.cbxMotivoAnulacion.SoloLectura = False
        Me.cbxMotivoAnulacion.TabIndex = 23
        Me.cbxMotivoAnulacion.Texto = "ERROR EN IMPRESION"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 173)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 20
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxCliente
        '
        Me.cbxCliente.CampoWhere = "VDetalleNotaCredito.IDCliente"
        Me.cbxCliente.CargarUnaSolaVez = False
        Me.cbxCliente.DataDisplayMember = "RazonSocial"
        Me.cbxCliente.DataFilter = Nothing
        Me.cbxCliente.DataOrderBy = "RazonSocial"
        Me.cbxCliente.DataSource = "VCliente"
        Me.cbxCliente.DataValueMember = "ID"
        Me.cbxCliente.dtSeleccionado = Nothing
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormABM = Nothing
        Me.cbxCliente.Indicaciones = Nothing
        Me.cbxCliente.Location = New System.Drawing.Point(129, 173)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.SeleccionMultiple = False
        Me.cbxCliente.SeleccionObligatoria = False
        Me.cbxCliente.Size = New System.Drawing.Size(213, 21)
        Me.cbxCliente.SoloLectura = False
        Me.cbxCliente.TabIndex = 21
        Me.cbxCliente.Texto = ""
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(9, 149)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 18
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'cbxProducto
        '
        Me.cbxProducto.CampoWhere = "IDProducto"
        Me.cbxProducto.CargarUnaSolaVez = False
        Me.cbxProducto.DataDisplayMember = "Descripcion"
        Me.cbxProducto.DataFilter = Nothing
        Me.cbxProducto.DataOrderBy = "Descripcion"
        Me.cbxProducto.DataSource = "VProducto"
        Me.cbxProducto.DataValueMember = "ID"
        Me.cbxProducto.dtSeleccionado = Nothing
        Me.cbxProducto.Enabled = False
        Me.cbxProducto.FormABM = Nothing
        Me.cbxProducto.Indicaciones = Nothing
        Me.cbxProducto.Location = New System.Drawing.Point(129, 149)
        Me.cbxProducto.Name = "cbxProducto"
        Me.cbxProducto.SeleccionMultiple = False
        Me.cbxProducto.SeleccionObligatoria = False
        Me.cbxProducto.Size = New System.Drawing.Size(213, 21)
        Me.cbxProducto.SoloLectura = False
        Me.cbxProducto.TabIndex = 19
        Me.cbxProducto.Texto = ""
        '
        'chkDeposito
        '
        Me.chkDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkDeposito.Color = System.Drawing.Color.Empty
        Me.chkDeposito.Location = New System.Drawing.Point(9, 61)
        Me.chkDeposito.Name = "chkDeposito"
        Me.chkDeposito.Size = New System.Drawing.Size(114, 21)
        Me.chkDeposito.SoloLectura = False
        Me.chkDeposito.TabIndex = 10
        Me.chkDeposito.Texto = "Deposito:"
        Me.chkDeposito.Valor = False
        '
        'cbxDeposito
        '
        Me.cbxDeposito.CampoWhere = "VDetalleNotaCredito.IDDeposito"
        Me.cbxDeposito.CargarUnaSolaVez = False
        Me.cbxDeposito.DataDisplayMember = "SucDeposito"
        Me.cbxDeposito.DataFilter = Nothing
        Me.cbxDeposito.DataOrderBy = "SucDeposito"
        Me.cbxDeposito.DataSource = "VDeposito"
        Me.cbxDeposito.DataValueMember = "ID"
        Me.cbxDeposito.dtSeleccionado = Nothing
        Me.cbxDeposito.Enabled = False
        Me.cbxDeposito.FormABM = Nothing
        Me.cbxDeposito.Indicaciones = Nothing
        Me.cbxDeposito.Location = New System.Drawing.Point(129, 61)
        Me.cbxDeposito.Name = "cbxDeposito"
        Me.cbxDeposito.SeleccionMultiple = False
        Me.cbxDeposito.SeleccionObligatoria = False
        Me.cbxDeposito.Size = New System.Drawing.Size(213, 21)
        Me.cbxDeposito.SoloLectura = False
        Me.cbxDeposito.TabIndex = 11
        Me.cbxDeposito.Texto = "CENTRAL"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 40)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 8
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "VDetalleNotaCredito.IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(129, 40)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(213, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 9
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkCiudad
        '
        Me.chkCiudad.BackColor = System.Drawing.Color.Transparent
        Me.chkCiudad.Color = System.Drawing.Color.Empty
        Me.chkCiudad.Location = New System.Drawing.Point(9, 19)
        Me.chkCiudad.Name = "chkCiudad"
        Me.chkCiudad.Size = New System.Drawing.Size(114, 21)
        Me.chkCiudad.SoloLectura = False
        Me.chkCiudad.TabIndex = 6
        Me.chkCiudad.Texto = "Ciudad:"
        Me.chkCiudad.Valor = False
        '
        'cbxCiudad
        '
        Me.cbxCiudad.CampoWhere = "VDetalleNotaCredito.IDCiudad"
        Me.cbxCiudad.CargarUnaSolaVez = False
        Me.cbxCiudad.DataDisplayMember = "Ciudad"
        Me.cbxCiudad.DataFilter = Nothing
        Me.cbxCiudad.DataOrderBy = Nothing
        Me.cbxCiudad.DataSource = "VSucursal"
        Me.cbxCiudad.DataValueMember = "ID"
        Me.cbxCiudad.dtSeleccionado = Nothing
        Me.cbxCiudad.Enabled = False
        Me.cbxCiudad.FormABM = Nothing
        Me.cbxCiudad.Indicaciones = Nothing
        Me.cbxCiudad.Location = New System.Drawing.Point(129, 19)
        Me.cbxCiudad.Name = "cbxCiudad"
        Me.cbxCiudad.SeleccionMultiple = False
        Me.cbxCiudad.SeleccionObligatoria = False
        Me.cbxCiudad.Size = New System.Drawing.Size(213, 21)
        Me.cbxCiudad.SoloLectura = False
        Me.cbxCiudad.TabIndex = 7
        Me.cbxCiudad.Texto = "ASUNCION"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(544, 297)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 11
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'chkLinea
        '
        Me.chkLinea.BackColor = System.Drawing.Color.Transparent
        Me.chkLinea.Color = System.Drawing.Color.Empty
        Me.chkLinea.Location = New System.Drawing.Point(9, 125)
        Me.chkLinea.Name = "chkLinea"
        Me.chkLinea.Size = New System.Drawing.Size(114, 21)
        Me.chkLinea.SoloLectura = False
        Me.chkLinea.TabIndex = 34
        Me.chkLinea.Texto = "Sub Tipo Producto:"
        Me.chkLinea.Valor = False
        '
        'cbxLinea
        '
        Me.cbxLinea.CampoWhere = "VDetalleNotaCredito.IDLinea"
        Me.cbxLinea.CargarUnaSolaVez = False
        Me.cbxLinea.DataDisplayMember = "Descripcion"
        Me.cbxLinea.DataFilter = Nothing
        Me.cbxLinea.DataOrderBy = "Descripcion"
        Me.cbxLinea.DataSource = "VLinea"
        Me.cbxLinea.DataValueMember = "ID"
        Me.cbxLinea.dtSeleccionado = Nothing
        Me.cbxLinea.Enabled = False
        Me.cbxLinea.FormABM = Nothing
        Me.cbxLinea.Indicaciones = Nothing
        Me.cbxLinea.Location = New System.Drawing.Point(129, 125)
        Me.cbxLinea.Name = "cbxLinea"
        Me.cbxLinea.SeleccionMultiple = False
        Me.cbxLinea.SeleccionObligatoria = False
        Me.cbxLinea.Size = New System.Drawing.Size(213, 21)
        Me.cbxLinea.SoloLectura = False
        Me.cbxLinea.TabIndex = 35
        Me.cbxLinea.Texto = ""
        '
        'frmListadoNotaCreditoDetalle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(632, 332)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoNotaCreditoDetalle"
        Me.Text = "NotaCreditoDetalle"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbxCliente As ERP.ocxCBX
    Friend WithEvents chkProducto As ERP.ocxCHK
    Friend WithEvents cbxProducto As ERP.ocxCBX
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents chkDeposito As ERP.ocxCHK
    Friend WithEvents cbxDeposito As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents chkCiudad As ERP.ocxCHK
    Friend WithEvents cbxCiudad As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkVerAnulados As ERP.ocxCHK
    Friend WithEvents chkMotivoAnulacion As ERP.ocxCHK
    Friend WithEvents cbxMotivoAnulacion As ERP.ocxCBX
    Friend WithEvents chkTipo As ERP.ocxCHK
    Friend WithEvents cbxMotivoNC As ERP.ocxCBX
    Friend WithEvents chkDetalladoProductos As ERP.ocxCHK
    Friend WithEvents chkMotivoDevolucion As ERP.ocxCHK
    Friend WithEvents cbxMotivoDevolucion As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkFechaVenta As ocxCHK
    Friend WithEvents chkFechaNC As ocxCHK
    Friend WithEvents txtHastaVenta As ocxTXTDate
    Friend WithEvents txtDesdeVenta As ocxTXTDate
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents chkLinea As ocxCHK
    Friend WithEvents cbxLinea As ocxCBX
End Class

﻿Imports ERP.Reporte
Public Class frmListadoNotaCreditoAplicar

    'CLASES
    Dim CReporte As New CReporteNotaCredito
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE NOTAS CREDITO APLICAR"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        Me.AcceptButton = New Button
        Me.KeyPreview = True
        CargarInformacion()

        CambiarOrdenacion()
        cbxMotivoNC.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'Orden en forma
        cbxMotivoNC.cbx.Items.Add("DEVOLUCION")
        cbxMotivoNC.cbx.Items.Add("DESCUENTO")
        cbxMotivoNC.cbx.DropDownStyle = ComboBoxStyle.DropDownList


        cbxInforme.cbx.Items.Add("RESUMIDO")
        cbxInforme.cbx.Items.Add("DETALLADO")
    End Sub

    Sub Listar()

        Dim Where As String = ""
        Dim WhereDetalle As String = ""
        Dim OrderBy As String = ""
        Dim Top As String = ""
        TipoInforme = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
        Where = Where & " and Anulado = 'False' "
        'Filtrar anulados

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        If nudRanking.Value > 0 Then
            Top = "Top (" & nudRanking.Value & ")"
        End If

        If chkAplicado.chk.Checked = False Then
            Where = Where & " And Aplicar='True' And Saldo > 0 "
            Titulo = "LISTADO DE NOTAS CREDITOS NO APLICADAS"
        Else
            Where = Where & " And Aplicar='False' And Saldo > 0 "
            Titulo = "LISTADO DE NOTAS CREDITOS APLICADAS"
        End If

        If chkTipo.Valor = True Then
            If cbxMotivoNC.cbx.SelectedIndex = 0 Then
                Where = Where & " and devolucion = 'True'"
            Else
                Where = Where & " and Descuento = 'True'"
            End If
        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)

        If cbxInforme.cbx.Text = "RESUMIDO" Then
            CReporte.ImprimirNotaCreditoAplicar(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top)
        Else
            CReporte.ImprimirNotaCreditoAplicarDetalleFactura(frm, Titulo, TipoInforme, Where, WhereDetalle, OrderBy, Top)
        End If



    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VNotaCredito", "Select Top(0) Comprobante, Cliente, Fecha, Sucursal, Deposito, Total From VNotaCredito ")



        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub


    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoNotaCreditoAplicar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmListadoNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkTipo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipo.PropertyChanged
        cbxMotivoNC.Enabled = value
    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub
End Class




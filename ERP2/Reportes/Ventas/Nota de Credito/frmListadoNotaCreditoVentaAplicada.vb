﻿Imports ERP.Reporte
Public Class frmListadoNotaCreditoVentaAplicada
    'CLASES
    Dim CReporte As New CReporteNotaCredito
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE NOTAS CREDITO CON VENTAS APLICADAS"
    Dim TipoInforme As String 
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()
        CargarInformacion()

        'CambiarOrdenacion()

        txtDesde.PrimerDiaAño()
        txtHasta.Hoy()
        cbxMotivoNC.cbx.SelectedIndex = 0
    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxMoneda.Conectar()
        cbxSucursal.Conectar()

        ReDim vControles(-1)

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        cbxOrdenadoPor.cbx.Items.Add("Fecha Factura")
        cbxOrdenadoPor.cbx.Items.Add("Fecha NC")
        cbxOrdenadoPor.cbx.Items.Add("Cliente")
        cbxOrdenadoPor.cbx.Items.Add("Importe NC")
        cbxOrdenadoPor.cbx.Items.Add("Motivo NC")
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.SelectedIndex = 0


        'Orden en forma
        cbxMotivoNC.cbx.Items.Add("DEVOLUCION")
        cbxMotivoNC.cbx.Items.Add("DESCUENTO")
        cbxMotivoNC.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxMotivoNC.cbx.SelectedIndex = 0

        CSistema.SqlToComboBox(cbxSubMotivoNC.cbx, "Select ID, Descripcion From SubMotivoNotaCredito")

        'Tipo de Factura
        cbxTipoFactura.cbx.Items.Add("CONTADO + CREDITO")
        cbxTipoFactura.cbx.Items.Add("CONTADO")
        cbxTipoFactura.cbx.Items.Add("CREDITO")
        cbxTipoFactura.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxTipoFactura.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1"
        Dim WhereDetalle As String = ""
        Dim TipoInforme As String = ""
        Dim OrderBy As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm


        'Filtrar por Fecha
        'Where = " Where FechaNC Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "



        If chkFechaNotaCredito.Valor Then
            Where = Where & " And FechaNC Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "' "
        End If

        If chkFechaVenta.Valor Then
            Where = Where & " And FechaEmision Between '" & CSistema.FormatoFechaBaseDatos(txtVentaDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtVentaHasta.txt.Text, True, False) & "' "
        End If

        If chkAnulados.Valor = False Then
            Where = Where & " And Anulado = 'False'"
        End If



        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        If chkAnulados.Valor = True And rdbSoloAnulados.Checked Then
            Where = Where & "And Anulado = 'True'"
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By "

            Select Case cbxOrdenadoPor.cbx.Text
                Case "Fecha Factura"
                    OrderBy = OrderBy & " Cast(FechaEmision as date) "
                Case "Fecha NC"
                    OrderBy = OrderBy & "Cast(FechaNC as date) "
                Case "Cliente"
                    OrderBy = OrderBy & "Cliente "
                Case "Importe NC"
                    OrderBy = OrderBy & "ImporteNC "
                Case "Motivo NC"
                    OrderBy = OrderBy & "MotivoNotaCredito "
            End Select
        End If


        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        If chkMotivo.Valor = True Then
            If cbxMotivoNC.cbx.SelectedIndex = 0 Then
                Where = Where & " and devolucion = 'True'"
                TipoInforme = TipoInforme & " Por Devolucion - "
            Else
                Where = Where & " and Descuento = 'True'"
                TipoInforme = TipoInforme & " Por Descuento - "
            End If
        End If

        If ChkSubMotivo.Valor = True Then
            If cbxSubMotivoNC.cbx.Text = "" Then
                Where = Where & " and SubMotivoNotaCredito = '' "
                TipoInforme = TipoInforme
            Else
                Where = Where & " and SubMotivoNotaCredito = '" & cbxSubMotivoNC.cbx.Text & "' "
                TipoInforme = TipoInforme & " Por SubMotivo - "
            End If
        End If

        Select Case cbxTipoFactura.cbx.Text
            Case "CONTADO"
                Where = Where & " and Condicion = 'CONT'"
            Case "CREDITO"
                Where = Where & " and Condicion = 'CRED'"
        End Select

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        CReporte.ListadoNotaCreditoVentaAplicada(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, "", chkAgruparPorCliente.Valor)
    End Sub

    'Sub CambiarOrdenacion()

    '    'Ordenado por
    '    Dim dt As DataTable = Nothing

    '    cbxOrdenadoPor.cbx.Items.Clear()


    '    dt = CData.GetStructure("VNotaCreditoVentaAplicacionInforme", "Select Top(0) Cliente, FechaEmision, FechaNC, Sucursal From VNotaCreditoVentaAplicacionInforme ")



    '    If dt Is Nothing Then
    '        Exit Sub
    '    End If

    '    For i As Integer = 0 To dt.Columns.Count - 1
    '        cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
    '    Next

    '    cbxOrdenadoPor.cbx.SelectedIndex = 0

    'End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub


    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub


    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkMoneda_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMoneda.PropertyChanged
        cbxMoneda.Enabled = value
    End Sub

    Private Sub chkMotivo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMotivo.PropertyChanged
        cbxMotivoNC.Enabled = value
    End Sub

    Private Sub chkSubMotivo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles ChkSubMotivo.PropertyChanged
        cbxSubMotivoNC.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkAnulados_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkAnulados.PropertyChanged
        rdbIncluir.Enabled = value
        rdbSoloAnulados.Enabled = value
    End Sub

    Private Sub chkFechaNotaCredito_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaNotaCredito.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
    End Sub

    Private Sub chkFechaVenta_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaVenta.PropertyChanged
        txtVentaDesde.Enabled = value
        txtVentaHasta.Enabled = value
    End Sub
End Class
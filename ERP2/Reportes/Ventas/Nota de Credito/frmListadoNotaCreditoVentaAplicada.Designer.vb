﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoNotaCreditoVentaAplicada
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.ChkSubMotivo = New ERP.ocxCHK()
        Me.cbxSubMotivoNC = New ERP.ocxCBX()
        Me.chkAnulados = New ERP.ocxCHK()
        Me.rdbIncluir = New System.Windows.Forms.RadioButton()
        Me.rdbSoloAnulados = New System.Windows.Forms.RadioButton()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkMotivo = New ERP.ocxCHK()
        Me.cbxMotivoNC = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.cbxCliente = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkMoneda = New ERP.ocxCHK()
        Me.cbxMoneda = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkAgruparPorCliente = New ERP.ocxCHK()
        Me.chkFechaVenta = New ERP.ocxCHK()
        Me.chkFechaNotaCredito = New ERP.ocxCHK()
        Me.txtVentaHasta = New ERP.ocxTXTDate()
        Me.txtVentaDesde = New ERP.ocxTXTDate()
        Me.cbxTipoFactura = New ERP.ocxCBX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxFiltro.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(396, 265)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 10
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.ChkSubMotivo)
        Me.gbxFiltro.Controls.Add(Me.cbxSubMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.chkAnulados)
        Me.gbxFiltro.Controls.Add(Me.rdbIncluir)
        Me.gbxFiltro.Controls.Add(Me.rdbSoloAnulados)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMotivo)
        Me.gbxFiltro.Controls.Add(Me.cbxMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkMoneda)
        Me.gbxFiltro.Controls.Add(Me.cbxMoneda)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(370, 217)
        Me.gbxFiltro.TabIndex = 0
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "`"
        '
        'ChkSubMotivo
        '
        Me.ChkSubMotivo.BackColor = System.Drawing.Color.Transparent
        Me.ChkSubMotivo.Color = System.Drawing.Color.Empty
        Me.ChkSubMotivo.Location = New System.Drawing.Point(9, 120)
        Me.ChkSubMotivo.Name = "ChkSubMotivo"
        Me.ChkSubMotivo.Size = New System.Drawing.Size(114, 21)
        Me.ChkSubMotivo.SoloLectura = False
        Me.ChkSubMotivo.TabIndex = 38
        Me.ChkSubMotivo.Texto = "SubMotivo de NC:"
        Me.ChkSubMotivo.Valor = False
        '
        'cbxSubMotivoNC
        '
        Me.cbxSubMotivoNC.CampoWhere = ""
        Me.cbxSubMotivoNC.CargarUnaSolaVez = False
        Me.cbxSubMotivoNC.DataDisplayMember = Nothing
        Me.cbxSubMotivoNC.DataFilter = Nothing
        Me.cbxSubMotivoNC.DataOrderBy = Nothing
        Me.cbxSubMotivoNC.DataSource = Nothing
        Me.cbxSubMotivoNC.DataValueMember = Nothing
        Me.cbxSubMotivoNC.dtSeleccionado = Nothing
        Me.cbxSubMotivoNC.Enabled = False
        Me.cbxSubMotivoNC.FormABM = Nothing
        Me.cbxSubMotivoNC.Indicaciones = Nothing
        Me.cbxSubMotivoNC.Location = New System.Drawing.Point(137, 120)
        Me.cbxSubMotivoNC.Name = "cbxSubMotivoNC"
        Me.cbxSubMotivoNC.SeleccionMultiple = False
        Me.cbxSubMotivoNC.SeleccionObligatoria = False
        Me.cbxSubMotivoNC.Size = New System.Drawing.Size(157, 21)
        Me.cbxSubMotivoNC.SoloLectura = False
        Me.cbxSubMotivoNC.TabIndex = 37
        Me.cbxSubMotivoNC.Texto = ""
        '
        'chkAnulados
        '
        Me.chkAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkAnulados.Color = System.Drawing.Color.Empty
        Me.chkAnulados.Location = New System.Drawing.Point(9, 173)
        Me.chkAnulados.Name = "chkAnulados"
        Me.chkAnulados.Size = New System.Drawing.Size(114, 21)
        Me.chkAnulados.SoloLectura = False
        Me.chkAnulados.TabIndex = 36
        Me.chkAnulados.Texto = "Anulados:"
        Me.chkAnulados.Valor = False
        '
        'rdbIncluir
        '
        Me.rdbIncluir.AutoSize = True
        Me.rdbIncluir.Enabled = False
        Me.rdbIncluir.Location = New System.Drawing.Point(140, 173)
        Me.rdbIncluir.Name = "rdbIncluir"
        Me.rdbIncluir.Size = New System.Drawing.Size(53, 17)
        Me.rdbIncluir.TabIndex = 34
        Me.rdbIncluir.TabStop = True
        Me.rdbIncluir.Text = "Incluir"
        Me.rdbIncluir.UseVisualStyleBackColor = True
        '
        'rdbSoloAnulados
        '
        Me.rdbSoloAnulados.AutoSize = True
        Me.rdbSoloAnulados.Enabled = False
        Me.rdbSoloAnulados.Location = New System.Drawing.Point(201, 173)
        Me.rdbSoloAnulados.Name = "rdbSoloAnulados"
        Me.rdbSoloAnulados.Size = New System.Drawing.Size(93, 17)
        Me.rdbSoloAnulados.TabIndex = 35
        Me.rdbSoloAnulados.TabStop = True
        Me.rdbSoloAnulados.Text = "Solo Anulados"
        Me.rdbSoloAnulados.UseVisualStyleBackColor = True
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(9, 146)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 32
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(137, 146)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(219, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 33
        Me.cbxTipoProducto.Texto = ""
        '
        'chkMotivo
        '
        Me.chkMotivo.BackColor = System.Drawing.Color.Transparent
        Me.chkMotivo.Color = System.Drawing.Color.Empty
        Me.chkMotivo.Location = New System.Drawing.Point(9, 95)
        Me.chkMotivo.Name = "chkMotivo"
        Me.chkMotivo.Size = New System.Drawing.Size(114, 21)
        Me.chkMotivo.SoloLectura = False
        Me.chkMotivo.TabIndex = 31
        Me.chkMotivo.Texto = "Motivo de NC:"
        Me.chkMotivo.Valor = False
        '
        'cbxMotivoNC
        '
        Me.cbxMotivoNC.CampoWhere = Nothing
        Me.cbxMotivoNC.CargarUnaSolaVez = False
        Me.cbxMotivoNC.DataDisplayMember = Nothing
        Me.cbxMotivoNC.DataFilter = Nothing
        Me.cbxMotivoNC.DataOrderBy = Nothing
        Me.cbxMotivoNC.DataSource = Nothing
        Me.cbxMotivoNC.DataValueMember = Nothing
        Me.cbxMotivoNC.dtSeleccionado = Nothing
        Me.cbxMotivoNC.Enabled = False
        Me.cbxMotivoNC.FormABM = Nothing
        Me.cbxMotivoNC.Indicaciones = Nothing
        Me.cbxMotivoNC.Location = New System.Drawing.Point(137, 95)
        Me.cbxMotivoNC.Name = "cbxMotivoNC"
        Me.cbxMotivoNC.SeleccionMultiple = False
        Me.cbxMotivoNC.SeleccionObligatoria = False
        Me.cbxMotivoNC.Size = New System.Drawing.Size(157, 21)
        Me.cbxMotivoNC.SoloLectura = False
        Me.cbxMotivoNC.TabIndex = 30
        Me.cbxMotivoNC.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 70)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 4
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'cbxCliente
        '
        Me.cbxCliente.CampoWhere = "IDCliente"
        Me.cbxCliente.CargarUnaSolaVez = False
        Me.cbxCliente.DataDisplayMember = "RazonSocial"
        Me.cbxCliente.DataFilter = Nothing
        Me.cbxCliente.DataOrderBy = "RazonSocial"
        Me.cbxCliente.DataSource = "VCliente"
        Me.cbxCliente.DataValueMember = "ID"
        Me.cbxCliente.dtSeleccionado = Nothing
        Me.cbxCliente.Enabled = False
        Me.cbxCliente.FormABM = Nothing
        Me.cbxCliente.Indicaciones = Nothing
        Me.cbxCliente.Location = New System.Drawing.Point(137, 70)
        Me.cbxCliente.Name = "cbxCliente"
        Me.cbxCliente.SeleccionMultiple = False
        Me.cbxCliente.SeleccionObligatoria = False
        Me.cbxCliente.Size = New System.Drawing.Size(219, 21)
        Me.cbxCliente.SoloLectura = False
        Me.cbxCliente.TabIndex = 5
        Me.cbxCliente.Texto = ""
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 44)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 2
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(137, 44)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(219, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 3
        Me.cbxSucursal.Texto = "ASUNCION"
        '
        'chkMoneda
        '
        Me.chkMoneda.BackColor = System.Drawing.Color.Transparent
        Me.chkMoneda.Color = System.Drawing.Color.Empty
        Me.chkMoneda.Location = New System.Drawing.Point(9, 20)
        Me.chkMoneda.Name = "chkMoneda"
        Me.chkMoneda.Size = New System.Drawing.Size(114, 21)
        Me.chkMoneda.SoloLectura = False
        Me.chkMoneda.TabIndex = 0
        Me.chkMoneda.Texto = "Moneda:"
        Me.chkMoneda.Valor = False
        '
        'cbxMoneda
        '
        Me.cbxMoneda.CampoWhere = "IDMoneda"
        Me.cbxMoneda.CargarUnaSolaVez = False
        Me.cbxMoneda.DataDisplayMember = "Descripcion"
        Me.cbxMoneda.DataFilter = Nothing
        Me.cbxMoneda.DataOrderBy = Nothing
        Me.cbxMoneda.DataSource = "VMoneda"
        Me.cbxMoneda.DataValueMember = "ID"
        Me.cbxMoneda.dtSeleccionado = Nothing
        Me.cbxMoneda.Enabled = False
        Me.cbxMoneda.FormABM = Nothing
        Me.cbxMoneda.Indicaciones = Nothing
        Me.cbxMoneda.Location = New System.Drawing.Point(137, 19)
        Me.cbxMoneda.Name = "cbxMoneda"
        Me.cbxMoneda.SeleccionMultiple = False
        Me.cbxMoneda.SeleccionObligatoria = False
        Me.cbxMoneda.Size = New System.Drawing.Size(219, 21)
        Me.cbxMoneda.SoloLectura = False
        Me.cbxMoneda.TabIndex = 1
        Me.cbxMoneda.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 152)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkAgruparPorCliente)
        Me.GroupBox2.Controls.Add(Me.chkFechaVenta)
        Me.GroupBox2.Controls.Add(Me.chkFechaNotaCredito)
        Me.GroupBox2.Controls.Add(Me.txtVentaHasta)
        Me.GroupBox2.Controls.Add(Me.txtVentaDesde)
        Me.GroupBox2.Controls.Add(Me.cbxTipoFactura)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(388, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 247)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkAgruparPorCliente
        '
        Me.chkAgruparPorCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkAgruparPorCliente.Color = System.Drawing.Color.Empty
        Me.chkAgruparPorCliente.Location = New System.Drawing.Point(8, 125)
        Me.chkAgruparPorCliente.Name = "chkAgruparPorCliente"
        Me.chkAgruparPorCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkAgruparPorCliente.SoloLectura = False
        Me.chkAgruparPorCliente.TabIndex = 33
        Me.chkAgruparPorCliente.Texto = "Agrupar por Cliente"
        Me.chkAgruparPorCliente.Valor = True
        '
        'chkFechaVenta
        '
        Me.chkFechaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaVenta.Color = System.Drawing.Color.Empty
        Me.chkFechaVenta.Location = New System.Drawing.Point(6, 60)
        Me.chkFechaVenta.Name = "chkFechaVenta"
        Me.chkFechaVenta.Size = New System.Drawing.Size(151, 19)
        Me.chkFechaVenta.SoloLectura = False
        Me.chkFechaVenta.TabIndex = 14
        Me.chkFechaVenta.Texto = "Periodo Venta:"
        Me.chkFechaVenta.Valor = False
        '
        'chkFechaNotaCredito
        '
        Me.chkFechaNotaCredito.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaNotaCredito.Color = System.Drawing.Color.Empty
        Me.chkFechaNotaCredito.Location = New System.Drawing.Point(6, 13)
        Me.chkFechaNotaCredito.Name = "chkFechaNotaCredito"
        Me.chkFechaNotaCredito.Size = New System.Drawing.Size(151, 19)
        Me.chkFechaNotaCredito.SoloLectura = False
        Me.chkFechaNotaCredito.TabIndex = 13
        Me.chkFechaNotaCredito.Texto = "Periodo NotaCredito:"
        Me.chkFechaNotaCredito.Valor = True
        '
        'txtVentaHasta
        '
        Me.txtVentaHasta.AñoFecha = 0
        Me.txtVentaHasta.Color = System.Drawing.Color.Empty
        Me.txtVentaHasta.Enabled = False
        Me.txtVentaHasta.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtVentaHasta.Location = New System.Drawing.Point(92, 81)
        Me.txtVentaHasta.MesFecha = 0
        Me.txtVentaHasta.Name = "txtVentaHasta"
        Me.txtVentaHasta.PermitirNulo = False
        Me.txtVentaHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtVentaHasta.SoloLectura = False
        Me.txtVentaHasta.TabIndex = 12
        '
        'txtVentaDesde
        '
        Me.txtVentaDesde.AñoFecha = 0
        Me.txtVentaDesde.Color = System.Drawing.Color.Empty
        Me.txtVentaDesde.Enabled = False
        Me.txtVentaDesde.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtVentaDesde.Location = New System.Drawing.Point(6, 81)
        Me.txtVentaDesde.MesFecha = 0
        Me.txtVentaDesde.Name = "txtVentaDesde"
        Me.txtVentaDesde.PermitirNulo = False
        Me.txtVentaDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtVentaDesde.SoloLectura = False
        Me.txtVentaDesde.TabIndex = 11
        '
        'cbxTipoFactura
        '
        Me.cbxTipoFactura.CampoWhere = Nothing
        Me.cbxTipoFactura.CargarUnaSolaVez = False
        Me.cbxTipoFactura.DataDisplayMember = Nothing
        Me.cbxTipoFactura.DataFilter = Nothing
        Me.cbxTipoFactura.DataOrderBy = Nothing
        Me.cbxTipoFactura.DataSource = Nothing
        Me.cbxTipoFactura.DataValueMember = Nothing
        Me.cbxTipoFactura.dtSeleccionado = Nothing
        Me.cbxTipoFactura.FormABM = Nothing
        Me.cbxTipoFactura.Indicaciones = Nothing
        Me.cbxTipoFactura.Location = New System.Drawing.Point(9, 208)
        Me.cbxTipoFactura.Name = "cbxTipoFactura"
        Me.cbxTipoFactura.SeleccionMultiple = False
        Me.cbxTipoFactura.SeleccionObligatoria = False
        Me.cbxTipoFactura.Size = New System.Drawing.Size(157, 21)
        Me.cbxTipoFactura.SoloLectura = False
        Me.cbxTipoFactura.TabIndex = 9
        Me.cbxTipoFactura.Texto = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 192)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Tipo de Factura:"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(163, 169)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 169)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtHasta.Location = New System.Drawing.Point(90, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtDesde.Location = New System.Drawing.Point(6, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(548, 265)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 11
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'frmListadoNotaCreditoVentaAplicada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(657, 312)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoNotaCreditoVentaAplicada"
        Me.Text = "frmListadoNotaCreditoVentaAplicada"
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents cbxCliente As ERP.ocxCBX
    Friend WithEvents chkSucursal As ERP.ocxCHK
    Friend WithEvents cbxSucursal As ERP.ocxCBX
    Friend WithEvents chkMoneda As ERP.ocxCHK
    Friend WithEvents cbxMoneda As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents chkMotivo As ERP.ocxCHK
    Friend WithEvents cbxMotivoNC As ERP.ocxCBX
    Friend WithEvents chkTipoProducto As ERP.ocxCHK
    Friend WithEvents cbxTipoProducto As ERP.ocxCBX
    Friend WithEvents chkAnulados As ERP.ocxCHK
    Friend WithEvents rdbIncluir As System.Windows.Forms.RadioButton
    Friend WithEvents rdbSoloAnulados As System.Windows.Forms.RadioButton
    Friend WithEvents cbxTipoFactura As ocxCBX
    Friend WithEvents Label1 As Label
    Friend WithEvents txtVentaHasta As ocxTXTDate
    Friend WithEvents txtVentaDesde As ocxTXTDate
    Friend WithEvents chkFechaVenta As ocxCHK
    Friend WithEvents chkFechaNotaCredito As ocxCHK
    Friend WithEvents chkAgruparPorCliente As ocxCHK
    Friend WithEvents ChkSubMotivo As ocxCHK
    Friend WithEvents cbxSubMotivoNC As ocxCBX
End Class

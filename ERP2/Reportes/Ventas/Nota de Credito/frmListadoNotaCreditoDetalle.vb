﻿Imports ERP.Reporte
Public Class frmListadoNotaCreditoDetalle



    'CLASES
    Dim CReporte As New CReporteNotaCredito
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "DETALLE DE NOTAS CREDITO EMITIDAS"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()

        CambiarOrdenacion()
        cbxMotivoNC.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxEnForma.cbx.SelectedIndex = 0

    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxCiudad.Conectar()
        cbxSucursal.Conectar()

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        'Orden en forma
        cbxMotivoNC.cbx.Items.Add("DEVOLUCION")
        cbxMotivoNC.cbx.Items.Add("DESCUENTO")
        cbxMotivoNC.cbx.DropDownStyle = ComboBoxStyle.DropDownList
    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1 "
        Dim WhereDetalle As String = "Where 1 = 1 "
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim Responsable As String = ""
        Dim Desde As String = ""
        Dim Hasta As String = ""
        Dim Join As String = ""

        Desde = txtDesde.GetValueString
        Hasta = txtHasta.GetValueString

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtrar por Fecha
        'Where = " Where Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "

        If chkFechaNC.Valor Then
            Where = Where & " and Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
            WhereDetalle = WhereDetalle & " and VDetalleNotaCredito.Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
            If chkFechaVenta.Valor Then
                Join = " join Venta V on VDetalleNotaCredito.IDTransaccionVenta = V.IDTransaccion "
                WhereDetalle = WhereDetalle & " and V.FechaEmision Between '" & txtDesdeVenta.GetValueString & "' And '" & txtHastaVenta.GetValueString & "' "
            End If
        Else
            Join = " join Venta V on VDetalleNotaCredito.IDTransaccionVenta = V.IDTransaccion "
            WhereDetalle = WhereDetalle & " and V.FechaEmision Between '" & txtDesdeVenta.GetValueString & "' And '" & txtHastaVenta.GetValueString & "' "
        End If




        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If
            If ctr.name = "cbxMotivoDevolucion" Then
                GoTo siguiente
            End If
            If ctr.name = "cbxLinea" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next



        If chkVerAnulados.Valor = False Then
            Where = Where & " and Anulado = 'False'"
            If chkFechaVenta.Valor Then
                WhereDetalle = WhereDetalle & " and VDetalleNotaCredito.Anulado = 'False' and V.Anulado = 'False' "
            Else
                WhereDetalle = WhereDetalle & " and VDetalleNotaCredito.Anulado = 'False' "
            End If
        ElseIf chkVerAnulados.Valor = True Then
            Where = Where & " and Anulado = 'True'"
            If chkFechaVenta.Valor Then
                WhereDetalle = WhereDetalle & " and VDetalleNotaCredito.Anulado = 'True' and V.Anulado = 'True' "
            Else
                WhereDetalle = WhereDetalle & " and VDetalleNotaCredito.Anulado = 'True' "
            End If
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        'Ranking
        'If nudRanking.Value > 0 Then
        '    Top = "Top (" & nudRanking.Value & ")"
        'End If
        If chkTipo.Valor = True Then
            If cbxMotivoNC.cbx.SelectedIndex = 0 Then
                WhereDetalle = WhereDetalle & " And VDetalleNotaCredito.devolucion = 'True'"
            Else
                WhereDetalle = WhereDetalle & " and VDetalleNotaCredito.devolucion = 'False'"
            End If
        End If

        If chkProducto.chk.Checked Then
            WhereDetalle = WhereDetalle & " and idproducto = " & cbxProducto.GetValue
        End If

        If chkTipoProducto.chk.Checked Then
            WhereDetalle = WhereDetalle & " and idtipoproducto = " & cbxTipoProducto.GetValue
        End If

        If chkLinea.chk.Checked Then
            WhereDetalle = WhereDetalle & " and idlinea = " & cbxLinea.GetValue
        End If

        If chkMotivoDevolucion.Valor = True Then
            WhereDetalle = WhereDetalle & " and IDMotivoDevolucion = " & cbxMotivoDevolucion.cbx.SelectedValue
        End If

        If chkFechaVenta.Valor = False Then
            Where = Where.Replace("VDetalleNotaCredito.", "")
        End If



        'CReporte.ListadoNotaCreditoDetalle(frm, Where, WhereDetalle, Responsable, Desde, Hasta, OrderBy, Top)
        If chkDetalladoProductos.Valor = False Then
            If chkFechaVenta.Valor Then
                CReporte.NotaCreditoDetalladaFechaVenta(frm, Join, Where, WhereDetalle, Responsable, Desde, Hasta, OrderBy, Top, "rptNotaDeCreditoDetallada")
            Else
                CReporte.NotaCreditoDetallada(frm, Where, WhereDetalle, Responsable, Desde, Hasta, OrderBy, Top, "rptNotaDeCreditoDetallada")
            End If

        Else
            If chkFechaVenta.Valor Then
                CReporte.NotaCreditoDetalladaFechaVenta(frm, Join, Where, WhereDetalle, Responsable, Desde, Hasta, OrderBy, Top, "rptNotaDeCreditoDetalladaProducto")
            Else
                CReporte.NotaCreditoDetallada(frm, Where, WhereDetalle, Responsable, Desde, Hasta, OrderBy, Top, "rptNotaDeCreditoDetalladaProducto")
            End If

        End If

        'CReporte.ImprimirNotaCreditoDetallada(frm, Where, WhereDetalle, Responsable, Desde, Hasta, OrderBy, Top)
        'Select Case cbxTipoInforme.cbx.SelectedIndex

        '    Case 0
        '        TipoInforme = cbxTipoInforme.cbx.Text
        '        CReporte.ListadoFacturasEmitidas(frm, Where, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        '    Case 1
        '        TipoInforme = cbxTipoInforme.cbx.Text
        '        cbxProducto.EstablecerCondicion(WhereDetalle)
        '        CReporte.ListadoFacturasEmitidasDetalle(frm, Where, WhereDetalle, Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy, Top)
        'End Select



    End Sub

    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VNotaCredito", "Select Top(0) Comprobante, Cliente, Fecha, Sucursal, Deposito, Total From VNotaCredito ")



        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub

    Private Sub chkCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCiudad.PropertyChanged
        cbxCiudad.Enabled = value
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkDeposito.PropertyChanged
        cbxDeposito.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        cbxCliente.Enabled = value
    End Sub

    Private Sub cbxCiudad_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxCiudad.PropertyChanged
        cbxSucursal.DataFilter = " IDCiudad = " & cbxSucursal.GetValue
    End Sub

    Private Sub cbxSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxSucursal.PropertyChanged
        cbxDeposito.DataFilter = " IDSucursal = " & cbxSucursal.GetValue
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkProducto_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkProducto.PropertyChanged
        cbxProducto.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(sender As System.Object, e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub chkMotivoAnulacion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMotivoAnulacion.PropertyChanged
        cbxMotivoAnulacion.Enabled = value
    End Sub

    Private Sub chkTipo_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipo.PropertyChanged
        cbxMotivoNC.Enabled = value
        If chkTipo.chk.Checked = True And cbxMotivoNC.Texto = "DEVOLUCION" Then
            chkMotivoDevolucion.Enabled = True
        Else
            chkMotivoDevolucion.Enabled = False
        End If
    End Sub

    Private Sub cbxMotivoNC_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxMotivoNC.PropertyChanged
        If chkTipo.chk.Checked = True And cbxMotivoNC.Texto = "DEVOLUCION" Then
            chkMotivoDevolucion.Enabled = True
        Else
            chkMotivoDevolucion.Enabled = False
        End If

    End Sub

    Private Sub chkMotivoDevolucion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMotivoDevolucion.PropertyChanged
        cbxMotivoDevolucion.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkFechaNC_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaNC.PropertyChanged
        txtDesde.Enabled = value
        txtHasta.Enabled = value
        If value = False Then
            chkFechaVenta.chk.Checked = True
        End If
    End Sub

    Private Sub chkFechaVenta_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaVenta.PropertyChanged
        txtDesdeVenta.Enabled = value
        txtHastaVenta.Enabled = value

    End Sub

    Private Sub chkLinea_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkLinea.PropertyChanged
        cbxLinea.Enabled = value
    End Sub
End Class



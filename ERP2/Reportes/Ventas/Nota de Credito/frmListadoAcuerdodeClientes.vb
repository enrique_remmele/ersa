﻿Imports ERP.Reporte
Public Class frmListadoAcuerdodeClientes
    'CLASES
    'Dim CReporte As New CReporteAcuerdo
    Dim CReporte As New CReporteNotaCredito
    Dim CSistema As New CSistema
    Dim CData As New CData

    'VARIABLES
    Dim Titulo As String = "LISTADO DE ACUERDOS DE CLIENTES / COMERCIAL"
    Dim TipoInforme As String

    'FUNCIONES
    Sub Inicializar()
        Me.KeyPreview = True
        Me.AcceptButton = New Button
        txtProducto.Conectar()
        CargarInformacion()
    End Sub

    Sub CargarInformacion()

        txtDesde.SetValue(Date.Today)
        txtHasta.SetValue(Date.Today)

        'Tipo de Acuerdo
        cbxTipoAcuerdo.cbx.Items.Add("")
        cbxTipoAcuerdo.cbx.Items.Add("NOTA DE CREDITO")
        cbxTipoAcuerdo.cbx.Items.Add("FACTURA DE CLIENTE")
        cbxTipoAcuerdo.cbx.SelectedIndex = 0
        cbxTipoAcuerdo.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.SelectedIndex = 0
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Ordenado por
        cbxOrdenadoPor.cbx.Items.Add("NroAcuerdo")
        cbxOrdenadoPor.cbx.Items.Add("Fecha")
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Estado
        cbxEstado.cbx.Items.Add("ACTIVOS")
        cbxEstado.cbx.Items.Add("INACTIVOS")
        cbxEstado.cbx.Items.Add("TODOS")
        cbxEstado.cbx.SelectedIndex = 0
        cbxEstado.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        txtProveedor.Conectar()
        txtProveedor.Consulta = "Select ID, 'Razon Social'=RazonSocial, 'Nombre Fantasia'=NombreFantasia, RUC, Direccion, Telefono, 'Saldo Credito'=SaldoCredito  From VProveedor "


    End Sub

    Sub Listar()

        'If txtProducto.Seleccionado = False Then
        '    MessageBox.Show("Selecione un Producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        '    Exit Sub
        'End If

        Dim Where As String = ""
        Dim SubTitulo As String = ""
        Dim Producto As String = ""
        Dim Existencia As Decimal = 0
        Dim OrderBy As String = ""
        Dim Top As String = ""
        Dim OrdenadoFechaOperacion As Boolean = False

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        Where = " Where Fecha Between '" & CSistema.FormatoFechaBaseDatos(txtDesde.txt.Text, True, False) & "' And '" & CSistema.FormatoFechaBaseDatos(txtHasta.txt.Text, True, False) & "'"
        SubTitulo = "  Fecha: " & txtDesde.txt.Text & " - " & txtHasta.txt.Text
        If chkNroAcuerdo.chk.Checked = True And txtNroAcuerdo.Text <> "" Then
            Where = Where & " And NroAcuerdo=" & txtNroAcuerdo.Text & " "
            SubTitulo = SubTitulo & "  -  NroAcuerdo: " & txtNroAcuerdo.Text
        End If
        If chkTipoAcuerdo.chk.Checked = True And cbxTipoAcuerdo.cbx.Text <> "" Then
            If cbxTipoAcuerdo.cbx.Text = "FACTURA DE CLIENTE" Then
                Where = Where & " And FACTURA= 1 "
                SubTitulo = SubTitulo & "  -  Tipo de Acuerdo: " & cbxTipoAcuerdo.cbx.Text
            Else
                Where = Where & " And NC= 1 "
                SubTitulo = SubTitulo & "  -  Tipo de Acuerdo: " & cbxTipoAcuerdo.cbx.Text
            End If

        End If
        If chkCliente.chk.Checked = True And txtCliente.txtRazonSocial.txt.Text <> "" Then
            Where = Where & " And IDCliente=" & txtCliente.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Cliente: " & txtProducto.txt.txt.Text
        End If
        If txtProveedor.Seleccionado = True And txtProveedor.txtRazonSocial.txt.Text <> "" Then
            Where = Where & " And IDProveedor=" & txtProveedor.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prov: " & txtProveedor.txtRazonSocial.txt.Text
        End If
        If txtProducto.Seleccionado = True And txtProducto.txt.txt.Text <> "" Then
            Where = Where & " And IDProducto=" & txtProducto.Registro("ID") & " "
            SubTitulo = SubTitulo & "  -  Prod: " & txtProducto.txt.txt.Text
        End If

        If cbxEstado.txt.Text = "ACTIVOS" Then
            Where = Where & " and Estado = 'Activo' "
        ElseIf cbxEstado.txt.Text = "INACTIVOS" Then
            Where = Where & " and Estado = 'Inactivo' "
        Else
            Where = Where
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls
            If ctr.GetType.Name.ToUpper = "OCXCBX" Then

                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        'OrderBy = " Order By Fecha"

        If cbxOrdenadoPor.txt.Text <> "" Then
            OrderBy = "Order by " & cbxOrdenadoPor.txt.Text
            If cbxEnForma.txt.Text <> "" Then
                OrderBy = OrderBy & " " & cbxEnForma.txt.Text
            End If
        End If

        'ArmarSubTitulo(SubTitulo)
        TipoInforme = ""
        'CReporte.Acuerdo(frm, Where, Titulo, TipoInforme & " - " & SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo)
        CReporte.ListadodeAcuerdodeClientes(frm, Where, Titulo, TipoInforme & " - " & SubTitulo, vgUsuarioIdentificador, OrderBy, Top, Existencia, txtDesde.txt.Text, txtHasta.txt.Text, SubTitulo)

    End Sub

    Sub ArmarSubTitulo(ByRef SubTitulo As String)
        SubTitulo = "Del " & txtDesde.GetValue.ToShortDateString & " Al " & txtHasta.GetValue.ToShortDateString
        For Each ctr As Object In gbxFiltro.Controls


            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerSubtitulo(SubTitulo)
            End If
        Next
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub txtProducto_ItemSeleccionado(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProducto.ItemSeleccionado
        If txtProducto.Seleccionado = True Then
            txtDesde.Focus()
        End If
    End Sub

    Private Sub frmInformeAcuerdo_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        CSistema.SelectNextControl(Me, e.KeyCode)
    End Sub

    Private Sub frmInformeAcuerdo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkNroAcuerdo_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkNroAcuerdo.PropertyChanged
        txtNroAcuerdo.Enabled = value
    End Sub

    Private Sub chkTipoAcuerdo_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoAcuerdo.PropertyChanged
        If chkTipoAcuerdo.chk.Checked = True Then
            cbxTipoAcuerdo.Enabled = value

        Else
            cbxTipoAcuerdo.Enabled = value
            chkCliente.Enabled = True
            txtCliente.Enabled = True
            chkProveedor.Enabled = True
            txtProveedor.Enabled = True
        End If

    End Sub

    Private Sub chkCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkProveedor_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProveedor.PropertyChanged
        txtProveedor.Enabled = value
    End Sub

    Private Sub cbxTipoAcuerdo_PropertyChanged(sender As Object, e As EventArgs) Handles cbxTipoAcuerdo.PropertyChanged
        If cbxTipoAcuerdo.Texto = "NOTA DE CREDITO" Then
            chkCliente.Enabled = True
            txtCliente.Enabled = True
            chkProveedor.Enabled = False
            txtProveedor.Enabled = False
        ElseIf cbxTipoAcuerdo.Texto = "FACTURA DE CLIENTE" Then
            chkCliente.Enabled = False
            txtCliente.Enabled = False
            chkProveedor.Enabled = True
            txtProveedor.Enabled = True
        End If
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value
    End Sub

    Private Sub chkPresentacion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkPresentacion.PropertyChanged
        cbxPresentacion.Enabled = value
    End Sub

    Private Sub chkFamilia_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFamilia.PropertyChanged
        cbxFamilia.Enabled = value
    End Sub

    Private Sub chkAnulados_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkAnulados.PropertyChanged
        rdbIncluir.Enabled = value
        rdbSoloAnulados.Enabled = value
    End Sub

End Class
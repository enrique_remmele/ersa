﻿Imports ERP.Reporte
Public Class frmListadoNotaCredito

    'CLASES
    Dim CReporte As New CReporteNotaCredito
    Dim CSistema As New CSistema
    Dim CData As New CData

    'PROPIEDADES

    'VARIABLES
    Dim Titulo As String = "LISTADO DE NOTAS CREDITO EMITIDAS"
    Dim TipoInforme As String
    Dim vControles() As Control

    'FUNCIONES
    Sub Inicializar()

        'Radio Button
        rdbIncluir.Checked = True
        rdbIncluir.Enabled = False
        rdbSoloAnulados.Enabled = False

        CargarInformacion()
        txtCliente.Conectar()

        CambiarOrdenacion()
        cbxMotivoNC.cbx.SelectedIndex = 0
    End Sub

    Sub CargarInformacion()

        'Tipos de Informes
        'cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        'cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxSucursal.Conectar()
        cbxSucursalVenta.Conectar()

        ReDim vControles(-1)
        CSistema.CargaControl(vControles, rdbIncluir)
        CSistema.CargaControl(vControles, rdbSoloAnulados)

        'Orden en forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0
        'Motivo
        cbxMotivoNC.cbx.Items.Add("DEVOLUCION")
        cbxMotivoNC.cbx.Items.Add("DESCUENTO")
        cbxMotivoNC.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxMotivoNC.cbx.SelectedIndex = 0
        'Sub Motivo
        CSistema.SqlToComboBox(cbxSubMotivoNC.cbx, "Select ID, Descripcion from SubMotivoNotaCredito where Estado=1")
        cbxMotivoNC.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxMotivoNC.cbx.SelectedIndex = 0

    End Sub

    Sub Listar()

        Dim Where As String = " Where 1 = 1 "
        Dim OrderBy As String = ""
        TipoInforme = ""


        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        If chkFechaNC.Valor Then
            Where = Where & " and VNotaCredito.Fecha Between '" & txtDesde.GetValueString & "' And '" & txtHasta.GetValueString & "' "
            '    If chkFechaVenta.Valor Then
            '        Where = Where & " and VNotaCredito.idtransaccion in(Select Distinct IDTransaccion from vNotaCreditoVenta NCV Where NCV.FechaEmision Between '" & txtDesdeVenta.GetValueString & "' And '" & txtHastaVenta.GetValueString & "') "
            '    End If
            'Else
            '    Where = " and VNotaCredito.idtransaccion in(Select Distinct IDTransaccion from vNotaCreditoVenta NCV Where NCV.FechaEmision Between '" & txtDesdeVenta.GetValueString & "' And '" & txtHastaVenta.GetValueString & "') "
        End If

        Where = FiltrosVenta(Where)

        If chkAnulados.Valor = False Then
            Where = Where & " And VNotaCredito.Anulado = 'False' "
        Else
            If rdbSoloAnulados.Checked Then
                Where = Where & " And VNotaCredito.Anulado = 'True' "
            End If
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'If chkAnulados.Valor = True And rdbSoloAnulados.Checked Then
        '    Where = Where & "And VNotaCredito.Anulado = 'True'"
        'End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By VNotaCredito." & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If

        If chkMotivoNC.Valor = True Then
            If cbxMotivoNC.cbx.SelectedIndex = 0 Then
                Where = Where & " and devolucion = 'True'"
            Else
                Where = Where & " and descuento = 'True'"
            End If
        End If

        If chkNumeroSolicitudCliente.chk.Checked Then
            Where = Where & " and NumeroSolicitudCliente = '" & txtNumeroSolicitudCliente.Text.Trim & "' "
        End If

        If chkCliente.chk.Checked = True Then

            If txtCliente.txtID.txt.Text = "" Then
                Dim mensaje As String = "Ingrese primero algún cliente!"
                MessageBox.Show(mensaje, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            Else
                Where = Where & " And (IDCliente = " & txtCliente.Registro("ID").ToString & ") "
            End If

        End If

        CReporte.ArmarSubTitulo(TipoInforme, gbxFiltro, txtDesde, txtHasta)
        If chkMostrarDatos.chk.Checked = True Then
            CReporte.ImprimirNotaCreditoConPedido(frm, Where, "", Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy)
        Else
            CReporte.ImprimirNotaCredito(frm, Where, "", Titulo, TipoInforme, vgUsuarioIdentificador, OrderBy)
        End If
    End Sub


    Function FiltrosVenta(ByVal Where As String) As String

        If chkFechaVenta.Valor Or chkTipoProductoVenta.Valor Or chkSucursalVenta.Valor Then
            Where = Where & " and VNotaCredito.idtransaccion in(Select Distinct IDTransaccion from vNotaCreditoVenta NCV Where 1=1 "

            If chkFechaVenta.Valor Then
                Where = Where & " and NCV.FechaEmision Between '" & txtDesdeVenta.GetValueString & "' And '" & txtHastaVenta.GetValueString & "' "
            End If

            If chkSucursalVenta.Valor Then
                Where = Where & " and NCV.IDSucursal = " & cbxSucursalVenta.cbx.SelectedValue
            End If

            If chkTipoProductoVenta.Valor Then
                Where = Where & " and (Select top(1) idtipoproducto from vdetalleventa dv where dv.idtransaccion= NCV.IDTransaccionVenta) = " & cbxTipoProductoVenta.cbx.SelectedValue
            End If

            Where = Where & " ) "
        End If
        Return Where
    End Function


    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()


        dt = CData.GetStructure("VNotaCredito", "Select Top(0) Comprobante, Cliente, Fecha, Sucursal, Deposito, Total From VNotaCredito ")



        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next
        cbxOrdenadoPor.cbx.SelectedIndex = 0

    End Sub

    Private Sub frmListadoFacturacionEmitida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Inicializar()
    End Sub

    Private Sub chkSucursal_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub btnInforme_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub frmListadoNotaCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub OcxAnulados_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkAnulados.PropertyChanged
        rdbIncluir.Enabled = value
        rdbSoloAnulados.Enabled = value
    End Sub

    Private Sub chkMotivoAnulacion_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMotivoAnulacion.PropertyChanged
        cbxMotivoAnulacion.Enabled = value
    End Sub

    Private Sub chkMotivoNC_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkMotivoNC.PropertyChanged
        cbxMotivoNC.Enabled = value
    End Sub

    Private Sub chkTipoProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkFechaNC_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaNC.PropertyChanged
        If value = False Then
            chkFechaVenta.Valor = True
            chkFechaVenta.Enabled = False

        Else
            chkFechaVenta.Enabled = True


        End If
    End Sub

    Private Sub chkSucursalVenta_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSucursalVenta.PropertyChanged
        cbxSucursalVenta.Enabled = value
    End Sub

    Private Sub chkTipoProductoVenta_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoProductoVenta.PropertyChanged
        cbxTipoProductoVenta.Enabled = value
    End Sub

    Private Sub chkNumeroSolicitudCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkNumeroSolicitudCliente.PropertyChanged
        txtNumeroSolicitudCliente.Enabled = value
    End Sub

    Private Sub frmListadoNotaCredito_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        LiberarMemoria()
    End Sub

    Private Sub chkSubMotivoNC_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSubMotivoNC.PropertyChanged
        cbxSubMotivoNC.Enabled = value
        'Dim Dttemp As DataTable
        'If chkMotivoNC.chk.Checked = "true" Then
        '    CSistema.SqlToComboBox(cbxSubMotivoNC.cbx, "Select Descripcion from SubMotivoNotaCredito where Motivo= '" & cbxMotivoNC.cbx.Text & "'")
        '    '    Dttemp = CData.GetTable("SubMotivoNotaCredito", "Motivo='" & cbxMotivoNC.cbx.Text & "'").Copy
        '    '    CSistema.SqlToComboBox(cbxSubMotivoNC.cbx, Dttemp)
        '    '    'CSistema.SqlToComboBox(cbxSubMotivoNC.cbx, "Select Descripcion from SubMotivoNotaCredito where Motivo= 'DEVOLUCION' and estado = 1")
        'Else
        '    Exit Sub
        'End If
    End Sub
End Class


﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoNotaCredito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkMostrarDatos = New ERP.ocxCHK()
        Me.chkFechaVenta = New ERP.ocxCHK()
        Me.chkFechaNC = New ERP.ocxCHK()
        Me.txtHastaVenta = New ERP.ocxTXTDate()
        Me.txtDesdeVenta = New ERP.ocxTXTDate()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxFiltroVenta = New System.Windows.Forms.GroupBox()
        Me.chkTipoProductoVenta = New ERP.ocxCHK()
        Me.cbxTipoProductoVenta = New ERP.ocxCBX()
        Me.chkSucursalVenta = New ERP.ocxCHK()
        Me.cbxSucursalVenta = New ERP.ocxCBX()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.chkCliente = New ERP.ocxCHK()
        Me.rdbSoloAnulados = New System.Windows.Forms.RadioButton()
        Me.rdbIncluir = New System.Windows.Forms.RadioButton()
        Me.chkAnulados = New ERP.ocxCHK()
        Me.cbxMotivoAnulacion = New ERP.ocxCBX()
        Me.chkMotivoAnulacion = New ERP.ocxCHK()
        Me.cbxMotivoNC = New ERP.ocxCBX()
        Me.chkMotivoNC = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.txtNumeroSolicitudCliente = New System.Windows.Forms.TextBox()
        Me.chkNumeroSolicitudCliente = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.cbxSubMotivoNC = New ERP.ocxCBX()
        Me.chkSubMotivoNC = New ERP.ocxCHK()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltroVenta.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(531, 323)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(163, 23)
        Me.btnInforme.TabIndex = 6
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 125)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 3
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkMostrarDatos)
        Me.GroupBox2.Controls.Add(Me.chkFechaVenta)
        Me.GroupBox2.Controls.Add(Me.chkFechaNC)
        Me.GroupBox2.Controls.Add(Me.txtHastaVenta)
        Me.GroupBox2.Controls.Add(Me.txtDesdeVenta)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Location = New System.Drawing.Point(531, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(244, 295)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'chkMostrarDatos
        '
        Me.chkMostrarDatos.BackColor = System.Drawing.Color.Transparent
        Me.chkMostrarDatos.Color = System.Drawing.Color.Empty
        Me.chkMostrarDatos.Location = New System.Drawing.Point(6, 181)
        Me.chkMostrarDatos.Name = "chkMostrarDatos"
        Me.chkMostrarDatos.Size = New System.Drawing.Size(144, 21)
        Me.chkMostrarDatos.SoloLectura = False
        Me.chkMostrarDatos.TabIndex = 29
        Me.chkMostrarDatos.Texto = "Mostrar Datos de Pedido"
        Me.chkMostrarDatos.Valor = False
        '
        'chkFechaVenta
        '
        Me.chkFechaVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaVenta.Color = System.Drawing.Color.Empty
        Me.chkFechaVenta.Location = New System.Drawing.Point(6, 71)
        Me.chkFechaVenta.Name = "chkFechaVenta"
        Me.chkFechaVenta.Size = New System.Drawing.Size(133, 21)
        Me.chkFechaVenta.SoloLectura = False
        Me.chkFechaVenta.TabIndex = 26
        Me.chkFechaVenta.Texto = "Fecha Factura"
        Me.chkFechaVenta.Valor = False
        '
        'chkFechaNC
        '
        Me.chkFechaNC.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaNC.Color = System.Drawing.Color.Empty
        Me.chkFechaNC.Location = New System.Drawing.Point(6, 19)
        Me.chkFechaNC.Name = "chkFechaNC"
        Me.chkFechaNC.Size = New System.Drawing.Size(133, 21)
        Me.chkFechaNC.SoloLectura = False
        Me.chkFechaNC.TabIndex = 25
        Me.chkFechaNC.Texto = "Fecha Nota de Credito"
        Me.chkFechaNC.Valor = True
        '
        'txtHastaVenta
        '
        Me.txtHastaVenta.AñoFecha = 0
        Me.txtHastaVenta.Color = System.Drawing.Color.Empty
        Me.txtHastaVenta.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtHastaVenta.Location = New System.Drawing.Point(89, 98)
        Me.txtHastaVenta.MesFecha = 0
        Me.txtHastaVenta.Name = "txtHastaVenta"
        Me.txtHastaVenta.PermitirNulo = False
        Me.txtHastaVenta.Size = New System.Drawing.Size(74, 20)
        Me.txtHastaVenta.SoloLectura = False
        Me.txtHastaVenta.TabIndex = 8
        '
        'txtDesdeVenta
        '
        Me.txtDesdeVenta.AñoFecha = 0
        Me.txtDesdeVenta.Color = System.Drawing.Color.Empty
        Me.txtDesdeVenta.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtDesdeVenta.Location = New System.Drawing.Point(6, 98)
        Me.txtDesdeVenta.MesFecha = 0
        Me.txtDesdeVenta.Name = "txtDesdeVenta"
        Me.txtDesdeVenta.PermitirNulo = False
        Me.txtDesdeVenta.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeVenta.SoloLectura = False
        Me.txtDesdeVenta.TabIndex = 7
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(123, 142)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(73, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 5
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(6, 142)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(111, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 4
        Me.cbxOrdenadoPor.Texto = ""
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtHasta.Location = New System.Drawing.Point(89, 42)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 1, 18, 10, 56, 41, 603)
        Me.txtDesde.Location = New System.Drawing.Point(6, 42)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(712, 323)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 7
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'gbxFiltroVenta
        '
        Me.gbxFiltroVenta.Controls.Add(Me.chkTipoProductoVenta)
        Me.gbxFiltroVenta.Controls.Add(Me.cbxTipoProductoVenta)
        Me.gbxFiltroVenta.Controls.Add(Me.chkSucursalVenta)
        Me.gbxFiltroVenta.Controls.Add(Me.cbxSucursalVenta)
        Me.gbxFiltroVenta.Location = New System.Drawing.Point(13, 270)
        Me.gbxFiltroVenta.Name = "gbxFiltroVenta"
        Me.gbxFiltroVenta.Size = New System.Drawing.Size(512, 76)
        Me.gbxFiltroVenta.TabIndex = 30
        Me.gbxFiltroVenta.TabStop = False
        Me.gbxFiltroVenta.Text = "Filtros Venta"
        '
        'chkTipoProductoVenta
        '
        Me.chkTipoProductoVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProductoVenta.Color = System.Drawing.Color.Empty
        Me.chkTipoProductoVenta.Location = New System.Drawing.Point(8, 45)
        Me.chkTipoProductoVenta.Name = "chkTipoProductoVenta"
        Me.chkTipoProductoVenta.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProductoVenta.SoloLectura = False
        Me.chkTipoProductoVenta.TabIndex = 28
        Me.chkTipoProductoVenta.Texto = "Tipo de Producto:"
        Me.chkTipoProductoVenta.Valor = False
        '
        'cbxTipoProductoVenta
        '
        Me.cbxTipoProductoVenta.CampoWhere = "V.IDTipoProducto"
        Me.cbxTipoProductoVenta.CargarUnaSolaVez = False
        Me.cbxTipoProductoVenta.DataDisplayMember = "Descripcion"
        Me.cbxTipoProductoVenta.DataFilter = Nothing
        Me.cbxTipoProductoVenta.DataOrderBy = "Descripcion"
        Me.cbxTipoProductoVenta.DataSource = "VTipoProducto"
        Me.cbxTipoProductoVenta.DataValueMember = "ID"
        Me.cbxTipoProductoVenta.dtSeleccionado = Nothing
        Me.cbxTipoProductoVenta.Enabled = False
        Me.cbxTipoProductoVenta.FormABM = Nothing
        Me.cbxTipoProductoVenta.Indicaciones = Nothing
        Me.cbxTipoProductoVenta.Location = New System.Drawing.Point(135, 45)
        Me.cbxTipoProductoVenta.Name = "cbxTipoProductoVenta"
        Me.cbxTipoProductoVenta.SeleccionMultiple = False
        Me.cbxTipoProductoVenta.SeleccionObligatoria = False
        Me.cbxTipoProductoVenta.Size = New System.Drawing.Size(371, 21)
        Me.cbxTipoProductoVenta.SoloLectura = False
        Me.cbxTipoProductoVenta.TabIndex = 29
        Me.cbxTipoProductoVenta.Texto = ""
        '
        'chkSucursalVenta
        '
        Me.chkSucursalVenta.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursalVenta.Color = System.Drawing.Color.Empty
        Me.chkSucursalVenta.Location = New System.Drawing.Point(8, 18)
        Me.chkSucursalVenta.Name = "chkSucursalVenta"
        Me.chkSucursalVenta.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursalVenta.SoloLectura = False
        Me.chkSucursalVenta.TabIndex = 8
        Me.chkSucursalVenta.Texto = "Sucursal:"
        Me.chkSucursalVenta.Valor = False
        '
        'cbxSucursalVenta
        '
        Me.cbxSucursalVenta.CampoWhere = "V.IDSucursal"
        Me.cbxSucursalVenta.CargarUnaSolaVez = False
        Me.cbxSucursalVenta.DataDisplayMember = "Descripcion"
        Me.cbxSucursalVenta.DataFilter = Nothing
        Me.cbxSucursalVenta.DataOrderBy = Nothing
        Me.cbxSucursalVenta.DataSource = "VSucursal"
        Me.cbxSucursalVenta.DataValueMember = "ID"
        Me.cbxSucursalVenta.dtSeleccionado = Nothing
        Me.cbxSucursalVenta.Enabled = False
        Me.cbxSucursalVenta.FormABM = Nothing
        Me.cbxSucursalVenta.Indicaciones = Nothing
        Me.cbxSucursalVenta.Location = New System.Drawing.Point(136, 18)
        Me.cbxSucursalVenta.Name = "cbxSucursalVenta"
        Me.cbxSucursalVenta.SeleccionMultiple = False
        Me.cbxSucursalVenta.SeleccionObligatoria = False
        Me.cbxSucursalVenta.Size = New System.Drawing.Size(371, 21)
        Me.cbxSucursalVenta.SoloLectura = False
        Me.cbxSucursalVenta.TabIndex = 9
        Me.cbxSucursalVenta.Texto = "ASUNCION - CENTRAL"
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "VNotaCredito.IDSucursal"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "Descripcion"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = Nothing
        Me.cbxSucursal.DataSource = "VSucursal"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(137, 19)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(370, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 9
        Me.cbxSucursal.Texto = "ASUNCION - CENTRAL"
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 19)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(114, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 8
        Me.chkSucursal.Texto = "Sucursal:"
        Me.chkSucursal.Valor = False
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(6, 73)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 20
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'rdbSoloAnulados
        '
        Me.rdbSoloAnulados.AutoSize = True
        Me.rdbSoloAnulados.Location = New System.Drawing.Point(201, 132)
        Me.rdbSoloAnulados.Name = "rdbSoloAnulados"
        Me.rdbSoloAnulados.Size = New System.Drawing.Size(93, 17)
        Me.rdbSoloAnulados.TabIndex = 23
        Me.rdbSoloAnulados.TabStop = True
        Me.rdbSoloAnulados.Text = "Solo Anulados"
        Me.rdbSoloAnulados.UseVisualStyleBackColor = True
        '
        'rdbIncluir
        '
        Me.rdbIncluir.AutoSize = True
        Me.rdbIncluir.Location = New System.Drawing.Point(140, 132)
        Me.rdbIncluir.Name = "rdbIncluir"
        Me.rdbIncluir.Size = New System.Drawing.Size(53, 17)
        Me.rdbIncluir.TabIndex = 22
        Me.rdbIncluir.TabStop = True
        Me.rdbIncluir.Text = "Incluir"
        Me.rdbIncluir.UseVisualStyleBackColor = True
        '
        'chkAnulados
        '
        Me.chkAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkAnulados.Color = System.Drawing.Color.Empty
        Me.chkAnulados.Location = New System.Drawing.Point(8, 132)
        Me.chkAnulados.Name = "chkAnulados"
        Me.chkAnulados.Size = New System.Drawing.Size(114, 21)
        Me.chkAnulados.SoloLectura = False
        Me.chkAnulados.TabIndex = 24
        Me.chkAnulados.Texto = "Anulados:"
        Me.chkAnulados.Valor = False
        '
        'cbxMotivoAnulacion
        '
        Me.cbxMotivoAnulacion.CampoWhere = "VNotaCredito.IDMotivo"
        Me.cbxMotivoAnulacion.CargarUnaSolaVez = False
        Me.cbxMotivoAnulacion.DataDisplayMember = "Descripcion"
        Me.cbxMotivoAnulacion.DataFilter = Nothing
        Me.cbxMotivoAnulacion.DataOrderBy = "Descripcion"
        Me.cbxMotivoAnulacion.DataSource = "MotivoAnulacionNotaCredito"
        Me.cbxMotivoAnulacion.DataValueMember = "ID"
        Me.cbxMotivoAnulacion.dtSeleccionado = Nothing
        Me.cbxMotivoAnulacion.Enabled = False
        Me.cbxMotivoAnulacion.FormABM = Nothing
        Me.cbxMotivoAnulacion.Indicaciones = Nothing
        Me.cbxMotivoAnulacion.Location = New System.Drawing.Point(137, 105)
        Me.cbxMotivoAnulacion.Name = "cbxMotivoAnulacion"
        Me.cbxMotivoAnulacion.SeleccionMultiple = False
        Me.cbxMotivoAnulacion.SeleccionObligatoria = False
        Me.cbxMotivoAnulacion.Size = New System.Drawing.Size(370, 21)
        Me.cbxMotivoAnulacion.SoloLectura = False
        Me.cbxMotivoAnulacion.TabIndex = 25
        Me.cbxMotivoAnulacion.Texto = "ERROR EN IMPRESION"
        '
        'chkMotivoAnulacion
        '
        Me.chkMotivoAnulacion.BackColor = System.Drawing.Color.Transparent
        Me.chkMotivoAnulacion.Color = System.Drawing.Color.Empty
        Me.chkMotivoAnulacion.Location = New System.Drawing.Point(8, 105)
        Me.chkMotivoAnulacion.Name = "chkMotivoAnulacion"
        Me.chkMotivoAnulacion.Size = New System.Drawing.Size(114, 21)
        Me.chkMotivoAnulacion.SoloLectura = False
        Me.chkMotivoAnulacion.TabIndex = 26
        Me.chkMotivoAnulacion.Texto = "Motivo de Anulacion"
        Me.chkMotivoAnulacion.Valor = False
        '
        'cbxMotivoNC
        '
        Me.cbxMotivoNC.CampoWhere = Nothing
        Me.cbxMotivoNC.CargarUnaSolaVez = False
        Me.cbxMotivoNC.DataDisplayMember = Nothing
        Me.cbxMotivoNC.DataFilter = Nothing
        Me.cbxMotivoNC.DataOrderBy = Nothing
        Me.cbxMotivoNC.DataSource = Nothing
        Me.cbxMotivoNC.DataValueMember = Nothing
        Me.cbxMotivoNC.dtSeleccionado = Nothing
        Me.cbxMotivoNC.Enabled = False
        Me.cbxMotivoNC.FormABM = Nothing
        Me.cbxMotivoNC.Indicaciones = Nothing
        Me.cbxMotivoNC.Location = New System.Drawing.Point(137, 160)
        Me.cbxMotivoNC.Name = "cbxMotivoNC"
        Me.cbxMotivoNC.SeleccionMultiple = False
        Me.cbxMotivoNC.SeleccionObligatoria = False
        Me.cbxMotivoNC.Size = New System.Drawing.Size(191, 21)
        Me.cbxMotivoNC.SoloLectura = False
        Me.cbxMotivoNC.TabIndex = 8
        Me.cbxMotivoNC.Texto = ""
        '
        'chkMotivoNC
        '
        Me.chkMotivoNC.BackColor = System.Drawing.Color.Transparent
        Me.chkMotivoNC.Color = System.Drawing.Color.Empty
        Me.chkMotivoNC.Location = New System.Drawing.Point(8, 160)
        Me.chkMotivoNC.Name = "chkMotivoNC"
        Me.chkMotivoNC.Size = New System.Drawing.Size(114, 21)
        Me.chkMotivoNC.SoloLectura = False
        Me.chkMotivoNC.TabIndex = 27
        Me.chkMotivoNC.Texto = "Motivo de NC:"
        Me.chkMotivoNC.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "VNotaCredito.IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(137, 46)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(370, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 29
        Me.cbxTipoProducto.Texto = ""
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(8, 46)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(114, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 28
        Me.chkTipoProducto.Texto = "Tipo de Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkSubMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.cbxSubMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.txtNumeroSolicitudCliente)
        Me.gbxFiltro.Controls.Add(Me.chkNumeroSolicitudCliente)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.cbxMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.chkMotivoAnulacion)
        Me.gbxFiltro.Controls.Add(Me.cbxMotivoAnulacion)
        Me.gbxFiltro.Controls.Add(Me.chkAnulados)
        Me.gbxFiltro.Controls.Add(Me.rdbIncluir)
        Me.gbxFiltro.Controls.Add(Me.rdbSoloAnulados)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(513, 252)
        Me.gbxFiltro.TabIndex = 4
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros Nota de Credito"
        '
        'txtNumeroSolicitudCliente
        '
        Me.txtNumeroSolicitudCliente.Enabled = False
        Me.txtNumeroSolicitudCliente.Location = New System.Drawing.Point(138, 218)
        Me.txtNumeroSolicitudCliente.Name = "txtNumeroSolicitudCliente"
        Me.txtNumeroSolicitudCliente.Size = New System.Drawing.Size(113, 20)
        Me.txtNumeroSolicitudCliente.TabIndex = 32
        '
        'chkNumeroSolicitudCliente
        '
        Me.chkNumeroSolicitudCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkNumeroSolicitudCliente.Color = System.Drawing.Color.Empty
        Me.chkNumeroSolicitudCliente.Location = New System.Drawing.Point(9, 218)
        Me.chkNumeroSolicitudCliente.Name = "chkNumeroSolicitudCliente"
        Me.chkNumeroSolicitudCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkNumeroSolicitudCliente.SoloLectura = False
        Me.chkNumeroSolicitudCliente.TabIndex = 31
        Me.chkNumeroSolicitudCliente.Texto = "Solicitud Cliente:"
        Me.chkNumeroSolicitudCliente.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 150
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(137, 71)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(366, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 30
        '
        'cbxSubMotivoNC
        '
        Me.cbxSubMotivoNC.CampoWhere = Nothing
        Me.cbxSubMotivoNC.CargarUnaSolaVez = False
        Me.cbxSubMotivoNC.DataDisplayMember = Nothing
        Me.cbxSubMotivoNC.DataFilter = Nothing
        Me.cbxSubMotivoNC.DataOrderBy = Nothing
        Me.cbxSubMotivoNC.DataSource = Nothing
        Me.cbxSubMotivoNC.DataValueMember = Nothing
        Me.cbxSubMotivoNC.dtSeleccionado = Nothing
        Me.cbxSubMotivoNC.Enabled = False
        Me.cbxSubMotivoNC.FormABM = Nothing
        Me.cbxSubMotivoNC.Indicaciones = Nothing
        Me.cbxSubMotivoNC.Location = New System.Drawing.Point(136, 188)
        Me.cbxSubMotivoNC.Name = "cbxSubMotivoNC"
        Me.cbxSubMotivoNC.SeleccionMultiple = False
        Me.cbxSubMotivoNC.SeleccionObligatoria = False
        Me.cbxSubMotivoNC.Size = New System.Drawing.Size(192, 21)
        Me.cbxSubMotivoNC.SoloLectura = False
        Me.cbxSubMotivoNC.TabIndex = 33
        Me.cbxSubMotivoNC.Texto = ""
        '
        'chkSubMotivoNC
        '
        Me.chkSubMotivoNC.BackColor = System.Drawing.Color.Transparent
        Me.chkSubMotivoNC.Color = System.Drawing.Color.Empty
        Me.chkSubMotivoNC.Location = New System.Drawing.Point(8, 188)
        Me.chkSubMotivoNC.Name = "chkSubMotivoNC"
        Me.chkSubMotivoNC.Size = New System.Drawing.Size(114, 21)
        Me.chkSubMotivoNC.SoloLectura = False
        Me.chkSubMotivoNC.TabIndex = 34
        Me.chkSubMotivoNC.Texto = "Sub Motivo de NC:"
        Me.chkSubMotivoNC.Valor = False
        '
        'frmListadoNotaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(788, 367)
        Me.Controls.Add(Me.gbxFiltroVenta)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnCerrar)
        Me.Name = "frmListadoNotaCredito"
        Me.Text = "frmListadoNotaCredito"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltroVenta.ResumeLayout(False)
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents txtHastaVenta As ocxTXTDate
    Friend WithEvents txtDesdeVenta As ocxTXTDate
    Friend WithEvents chkFechaVenta As ocxCHK
    Friend WithEvents chkFechaNC As ocxCHK
    Friend WithEvents chkMostrarDatos As ocxCHK
    Friend WithEvents gbxFiltroVenta As GroupBox
    Friend WithEvents chkTipoProductoVenta As ocxCHK
    Friend WithEvents cbxTipoProductoVenta As ocxCBX
    Friend WithEvents chkSucursalVenta As ocxCHK
    Friend WithEvents cbxSucursalVenta As ocxCBX
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents chkSucursal As ocxCHK
    Friend WithEvents chkCliente As ocxCHK
    Friend WithEvents rdbSoloAnulados As RadioButton
    Friend WithEvents rdbIncluir As RadioButton
    Friend WithEvents chkAnulados As ocxCHK
    Friend WithEvents cbxMotivoAnulacion As ocxCBX
    Friend WithEvents chkMotivoAnulacion As ocxCHK
    Friend WithEvents cbxMotivoNC As ocxCBX
    Friend WithEvents chkMotivoNC As ocxCHK
    Friend WithEvents cbxTipoProducto As ocxCBX
    Friend WithEvents chkTipoProducto As ocxCHK
    Friend WithEvents gbxFiltro As GroupBox
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents txtNumeroSolicitudCliente As TextBox
    Friend WithEvents chkNumeroSolicitudCliente As ocxCHK
    Friend WithEvents chkSubMotivoNC As ocxCHK
    Friend WithEvents cbxSubMotivoNC As ocxCBX
End Class

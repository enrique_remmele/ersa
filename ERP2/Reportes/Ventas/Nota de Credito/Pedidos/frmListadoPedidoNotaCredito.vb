﻿Imports ERP.Reporte
Public Class frmListadoPedidoNotaCredito
    'CLASES
    Dim CReporte As New CReporteNotaCredito
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        CambiarOrdenacion()
        cbxTipoInforme.cbx.SelectedIndex = 1
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        txtDesdePedido.Hoy()
        txtHastaPedido.Hoy()
    End Sub

    Sub CargarInformacion()

        txtCliente.Conectar()

        'Tipos de Informes
        cbxTipoInforme.cbx.Items.Add("RESUMIDO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO")
        cbxTipoInforme.cbx.Items.Add("DETALLADO CON DESCUENTOS")
        cbxTipoInforme.cbx.SelectedIndex = 0
        cbxTipoInforme.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        'Pedidos
        cbxPedidos.cbx.Items.Add("TODOS")
        cbxPedidos.cbx.Items.Add("EMITIDO")
        cbxPedidos.cbx.Items.Add("NO EMITIDO")
        cbxPedidos.cbx.Items.Add("ANULADO")
        cbxPedidos.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        cbxTipoNC.cbx.Items.Add("DESCUENTO")
        cbxTipoNC.cbx.Items.Add("DEVOLUCION")


        'Habilitamos Seleccion multiple de todos los filtros ocxcbx
        For Each ctr As Object In gbxFiltro.Controls

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
Siguiente:
        Next

        txtProducto.SeleccionMultiple = True
        txtProducto.Conectar()

    End Sub

    Sub Listar()

        Dim Where As String = " where 1 = 1 "
        Dim WhereDetalle As String = " where 1 = 1 "
        Dim OrderBy As String = ""
        Dim Titulo As String = "LISTADO DE PEDIDOS DE NOTA DE CREDITO"
        Dim TipoInforme As String = ""
        Dim Subtitulo As String = ""
        Dim TablaCabecera As String = "VPedidoNotaCredito"
        Dim TablaDetalle As String = "VDetallePedidoNotaCredito"


        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtra por fecha
        'Where = "Where Fecha between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'  "
        If chkFechaFacturacion.Valor = True Then
            Where = Where & " and Cast(FechaNotaCredito as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeFacturado.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaFacturado.GetValue, True, False) & "'  "
            WhereDetalle = WhereDetalle & " and Cast(FechaNotaCredito as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeFacturado.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaFacturado.GetValue, True, False) & "'  "
        End If
        If chkFechaPedido.Valor = True Then
            Where = Where & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdePedido.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaPedido.GetValue, True, False) & "'  "
            WhereDetalle = WhereDetalle & " and Cast(Fecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdePedido.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaPedido.GetValue, True, False) & "'  "
        End If

        If Not txtProducto.dtProductosSeleccionados Is Nothing Then
            If txtProducto.dtProductosSeleccionados.Rows.Count = 0 Then
                txtProducto.dtProductosSeleccionados.Reset()
            End If
        End If

        'Filtro por producto
        If chkProducto.Valor = True Then
            If txtProducto.Seleccionado = False And (txtProducto.dtProductosSeleccionados Is Nothing) Then
                Exit Sub
            End If
            If Not txtProducto.dtProductosSeleccionados Is Nothing Then

                If txtProducto.dtProductosSeleccionados.Rows.Count > 0 Then

                    Dim IDProductoIN As String = ""
                    If chkExcluirProducto.chk.Checked Then
                        IDProductoIN = " IDProducto NOT In (0"
                    Else
                        IDProductoIN = " IDProducto In (0"
                    End If

                    For Each dRow As DataRow In txtProducto.dtProductosSeleccionados.Rows
                        IDProductoIN = IDProductoIN & ", " & dRow("ID")
                    Next
                    IDProductoIN = IDProductoIN & ")"

                    WhereDetalle = IIf(String.IsNullOrEmpty(WhereDetalle), " where " & IDProductoIN, WhereDetalle & "And " & IDProductoIN)


                    GoTo Seguir

                End If

            End If
            If chkExcluirProducto.chk.Checked Then
                WhereDetalle = WhereDetalle & " And IDProducto <> " & txtProducto.Registro("ID") & " "
            Else
                WhereDetalle = WhereDetalle & " And IDProducto = " & txtProducto.Registro("ID") & " "
            End If

        End If
Seguir:

        'Filtrar por Cliente
        If chkCliente.chk.Checked = True Then
            If txtCliente.Seleccionado = True Then
                Where = Where + " And IDCliente = " & txtCliente.Registro("ID").ToString
                WhereDetalle = WhereDetalle + " And IDCliente = " & txtCliente.Registro("ID").ToString
            Else

            End If
        End If

        EstablecerCondicion(Where)
        EstablecerCondicionDetalle(WhereDetalle)


        If chkPedido.chk.Checked = True Then
            If cbxPedidos.cbx.Text = "EMITIDO" Then
                Where = Where + " And PROCESADONC='PROCESADO'"
                'WhereDetalle = WhereDetalle + " And PROCESADONC='PROCESADO'"
            End If

            If cbxPedidos.cbx.Text = "NO EMITIDO" Then
                Where = Where + " And PROCESADONC <>'PROCESADO' and Anulado='False'"
                'WhereDetalle = WhereDetalle + " And PROCESADONC <>'PROCESADO' and Anulado='False'"
            End If

            If cbxPedidos.cbx.Text = "ANULADO" Then
                Where = Where + " And Anulado='True'"
                'WhereDetalle = WhereDetalle + " And Anulado='True'"
            End If
        End If

        If chkTipoNC.chk.Checked Then
            If cbxTipoNC.cbx.Text <> "" Then
                Where = Where + " and Tipo = '" & cbxTipoNC.cbx.Text & "' "
            End If
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If chkNumeroSolicitudCliente.chk.Checked Then
            Where = Where & " and NumeroSolicitudCliente = '" & txtNumeroSolicitudCliente.Text.Trim & "' "
        End If

        ' ArmarSubTitulo(Subtitulo)
        'SC: 08/06/2022 - Para que aparezca el titulo
        CReporte.ArmarSubtitulo(TipoInforme, gbxFiltro)
        If chkFechaPedido.chk.Checked Then
            TipoInforme = TipoInforme & " - Al " & txtHastaPedido.txt.Text
        End If

        Select Case cbxTipoInforme.cbx.SelectedIndex
            Case 0
                CReporte.ListadoPedidoNotaCredito(frm, Where, OrderBy, vgUsuarioIdentificador, Titulo, Subtitulo)
            Case 1
                CReporte.ListadoPedidoNotaCreditoDetalle(frm, Where, WhereDetalle, OrderBy, vgUsuarioIdentificador, txtDesdePedido.GetValue, txtHastaPedido.GetValue, Titulo, TipoInforme)
        End Select



    End Sub

    Sub EstablecerCondicion(ByRef Where As String)

        'Deposito
        cbxSucursal.EstablecerCondicion(Where, chkExcluirDeposito.Valor)

        'Usuario
        cbxUsuario.EstablecerCondicion(Where, chkExluirUsuario.Valor)

        'Tipo Cliente
        cbxTipoCliente.EstablecerCondicion(Where, chkExcluirTipoCliente.Valor)

        'Sub Motivo
        cbxSubMotivoNC.EstablecerCondicion(Where, chkExcluirSubMotivo.Valor)

    End Sub

    Sub EstablecerCondicionDetalle(ByRef Where As String)

        'Tipo Producto
        cbxTipoProducto.EstablecerCondicion(Where, chkExcluirTipoProducto.Valor)

        'Motivo Devolucion
        cbxMotivoDevolucion.EstablecerCondicion(Where, chkExcluirMotivoDevolucion.Valor)

    End Sub

    Sub ArmarSubTitulo(ByRef Subtitulo As String)
        Subtitulo = ""
        If chkFechaFacturacion.chk.Checked Then
            Subtitulo = "Periodo Facturacion: " & txtDesdeFacturado.GetValue & " - " & txtHastaFacturado.GetValue & " "
        Else
            Subtitulo = "Periodo Pedido: " & txtDesdeFacturado.GetValue & " - " & txtHastaFacturado.GetValue & " "
        End If


        If chkSucursal.Valor = True Then
            Subtitulo = Subtitulo & " - Deposito: " & cbxSucursal.Texto
        End If
        If chkTipoProducto.Valor = True Then
            Subtitulo = Subtitulo & " - Tipo. Prod: " & cbxTipoProducto.Texto
        End If
    End Sub


    Sub CambiarOrdenacion()

        'Ordenado por
        Dim dt As DataTable = Nothing

        cbxOrdenadoPor.cbx.Items.Clear()

        dt = CData.GetStructure("Pedido", " Select Top (0) Fecha, Numero, Ciudad, Sucursal , Cliente , Deposito from VPedido ")

        If dt Is Nothing Then
            Exit Sub
        End If

        For i As Integer = 0 To dt.Columns.Count - 1
            cbxOrdenadoPor.cbx.Items.Add(dt.Columns(i).ColumnName)
        Next

        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Private Sub chkCliente_PropertyChanged(ByVal sender As Object, ByVal e As System.EventArgs, ByVal value As Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub

    Private Sub chkDeposito_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkSucursal.PropertyChanged
        cbxSucursal.Enabled = value
    End Sub

    Private Sub frmPedidos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Inicializar()
    End Sub

    Private Sub btnInforme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Private Sub chkPedido_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean)
        cbxPedidos.Enabled = value
    End Sub

    Private Sub chkActivo_PropertyChanged(ByVal sender As System.Object, ByVal e As System.EventArgs, ByVal value As System.Boolean) Handles chkPedido.PropertyChanged
        cbxPedidos.Enabled = value
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub cbxTipoInforme_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxTipoInforme.Load

    End Sub

    Private Sub ckhUsuario_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles ckhUsuario.PropertyChanged
        cbxUsuario.Enabled = value
    End Sub

    Private Sub chkTipoProducto_Load(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkTipoProducto.PropertyChanged
        cbxTipoProducto.Enabled = value
    End Sub

    Private Sub chkProducto_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkProducto.PropertyChanged
        txtProducto.Enabled = value

    End Sub

    Private Sub cbxTipoInforme_PropertyChanged(sender As System.Object, e As System.EventArgs) Handles cbxTipoInforme.PropertyChanged
        If cbxTipoInforme.Texto = "RESUMIDO" Then
            chkProducto.chk.Checked = False
            chkProducto.Enabled = False
        Else
            chkProducto.Enabled = True
        End If

        If cbxTipoInforme.Texto = "DETALLADO CON DESCUENTOS" Then
            chkPedido.chk.Checked = True
            cbxPedidos.cbx.SelectedIndex = 1
            cbxPedidos.Enabled = False
            chkPedido.Enabled = False
        Else
            chkPedido.Enabled = True
            If chkPedido.chk.Checked Then
                cbxPedidos.Enabled = True
            Else
                cbxPedidos.Enabled = False
            End If

        End If

    End Sub

    Private Sub chkTipoCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoCliente.PropertyChanged
        cbxTipoCliente.Enabled = value
    End Sub

    Private Sub chkFechaNotaCreditocion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaFacturacion.PropertyChanged
        txtDesdeFacturado.Enabled = value
        txtHastaFacturado.Enabled = value
    End Sub
    Private Sub chkFechaPedido_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkFechaPedido.PropertyChanged
        txtDesdePedido.Enabled = value
        txtHastaPedido.Enabled = value
    End Sub

    Private Sub chkTipoNC_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkTipoNC.PropertyChanged
        cbxTipoNC.Enabled = value
    End Sub

    Private Sub chkMotivoDevolucion_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkMotivoDevolucion.PropertyChanged
        cbxMotivoDevolucion.Enabled = value
    End Sub

    Private Sub chkSubMotivoNC_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkSubMotivoNC.PropertyChanged
        cbxSubMotivoNC.Enabled = value
    End Sub

    Private Sub chkNumeroSolicitudCliente_PropertyChanged(sender As Object, e As EventArgs, value As Boolean) Handles chkNumeroSolicitudCliente.PropertyChanged
        txtNumeroSolicitudCliente.Enabled = value
    End Sub
End Class
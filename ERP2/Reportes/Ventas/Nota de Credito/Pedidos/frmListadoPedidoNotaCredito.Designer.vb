﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoPedidoNotaCredito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkFechaFacturacion = New ERP.ocxCHK()
        Me.chkFechaPedido = New ERP.ocxCHK()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtHastaPedido = New ERP.ocxTXTDate()
        Me.txtDesdePedido = New ERP.ocxTXTDate()
        Me.cbxTipoInforme = New ERP.ocxCBX()
        Me.lblInforme = New System.Windows.Forms.Label()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.txtHastaFacturado = New ERP.ocxTXTDate()
        Me.txtDesdeFacturado = New ERP.ocxTXTDate()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkTipoNC = New ERP.ocxCHK()
        Me.cbxTipoNC = New ERP.ocxCBX()
        Me.chkExcluirSubMotivo = New ERP.ocxCHK()
        Me.chkSubMotivoNC = New ERP.ocxCHK()
        Me.cbxSubMotivoNC = New ERP.ocxCBX()
        Me.chkExcluirMotivoDevolucion = New ERP.ocxCHK()
        Me.chkMotivoDevolucion = New ERP.ocxCHK()
        Me.cbxMotivoDevolucion = New ERP.ocxCBX()
        Me.chkExcluirProducto = New ERP.ocxCHK()
        Me.chkExcluirTipoCliente = New ERP.ocxCHK()
        Me.chkExluirUsuario = New ERP.ocxCHK()
        Me.chkExcluirTipoProducto = New ERP.ocxCHK()
        Me.chkExcluirDeposito = New ERP.ocxCHK()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkTipoCliente = New ERP.ocxCHK()
        Me.cbxTipoCliente = New ERP.ocxCBX()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.chkProducto = New ERP.ocxCHK()
        Me.chkTipoProducto = New ERP.ocxCHK()
        Me.cbxTipoProducto = New ERP.ocxCBX()
        Me.ckhUsuario = New ERP.ocxCHK()
        Me.cbxUsuario = New ERP.ocxCBX()
        Me.chkPedido = New ERP.ocxCHK()
        Me.cbxPedidos = New ERP.ocxCBX()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkSucursal = New ERP.ocxCHK()
        Me.cbxSucursal = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.txtNumeroSolicitudCliente = New System.Windows.Forms.TextBox()
        Me.chkNumeroSolicitudCliente = New ERP.ocxCHK()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkFechaFacturacion
        '
        Me.chkFechaFacturacion.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaFacturacion.Color = System.Drawing.Color.Empty
        Me.chkFechaFacturacion.Location = New System.Drawing.Point(11, 16)
        Me.chkFechaFacturacion.Name = "chkFechaFacturacion"
        Me.chkFechaFacturacion.Size = New System.Drawing.Size(165, 21)
        Me.chkFechaFacturacion.SoloLectura = False
        Me.chkFechaFacturacion.TabIndex = 36
        Me.chkFechaFacturacion.Texto = "Fecha Emision NC:"
        Me.chkFechaFacturacion.Valor = False
        '
        'chkFechaPedido
        '
        Me.chkFechaPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaPedido.Color = System.Drawing.Color.Empty
        Me.chkFechaPedido.Location = New System.Drawing.Point(11, 70)
        Me.chkFechaPedido.Name = "chkFechaPedido"
        Me.chkFechaPedido.Size = New System.Drawing.Size(170, 21)
        Me.chkFechaPedido.SoloLectura = False
        Me.chkFechaPedido.TabIndex = 32
        Me.chkFechaPedido.Texto = "Fecha Pedido"
        Me.chkFechaPedido.Valor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaFacturacion)
        Me.GroupBox2.Controls.Add(Me.chkFechaPedido)
        Me.GroupBox2.Controls.Add(Me.txtHastaPedido)
        Me.GroupBox2.Controls.Add(Me.txtDesdePedido)
        Me.GroupBox2.Controls.Add(Me.cbxTipoInforme)
        Me.GroupBox2.Controls.Add(Me.lblInforme)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHastaFacturado)
        Me.GroupBox2.Controls.Add(Me.txtDesdeFacturado)
        Me.GroupBox2.Location = New System.Drawing.Point(503, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(203, 334)
        Me.GroupBox2.TabIndex = 28
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'txtHastaPedido
        '
        Me.txtHastaPedido.AñoFecha = 0
        Me.txtHastaPedido.Color = System.Drawing.Color.Empty
        Me.txtHastaPedido.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHastaPedido.Location = New System.Drawing.Point(92, 96)
        Me.txtHastaPedido.MesFecha = 0
        Me.txtHastaPedido.Name = "txtHastaPedido"
        Me.txtHastaPedido.PermitirNulo = False
        Me.txtHastaPedido.Size = New System.Drawing.Size(84, 20)
        Me.txtHastaPedido.SoloLectura = False
        Me.txtHastaPedido.TabIndex = 35
        '
        'txtDesdePedido
        '
        Me.txtDesdePedido.AñoFecha = 0
        Me.txtDesdePedido.Color = System.Drawing.Color.Empty
        Me.txtDesdePedido.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesdePedido.Location = New System.Drawing.Point(12, 96)
        Me.txtDesdePedido.MesFecha = 0
        Me.txtDesdePedido.Name = "txtDesdePedido"
        Me.txtDesdePedido.PermitirNulo = False
        Me.txtDesdePedido.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdePedido.SoloLectura = False
        Me.txtDesdePedido.TabIndex = 34
        '
        'cbxTipoInforme
        '
        Me.cbxTipoInforme.CampoWhere = Nothing
        Me.cbxTipoInforme.CargarUnaSolaVez = False
        Me.cbxTipoInforme.DataDisplayMember = Nothing
        Me.cbxTipoInforme.DataFilter = Nothing
        Me.cbxTipoInforme.DataOrderBy = Nothing
        Me.cbxTipoInforme.DataSource = Nothing
        Me.cbxTipoInforme.DataValueMember = Nothing
        Me.cbxTipoInforme.dtSeleccionado = Nothing
        Me.cbxTipoInforme.FormABM = Nothing
        Me.cbxTipoInforme.Indicaciones = Nothing
        Me.cbxTipoInforme.Location = New System.Drawing.Point(11, 146)
        Me.cbxTipoInforme.Name = "cbxTipoInforme"
        Me.cbxTipoInforme.SeleccionMultiple = False
        Me.cbxTipoInforme.SeleccionObligatoria = False
        Me.cbxTipoInforme.Size = New System.Drawing.Size(164, 21)
        Me.cbxTipoInforme.SoloLectura = False
        Me.cbxTipoInforme.TabIndex = 27
        Me.cbxTipoInforme.Texto = ""
        '
        'lblInforme
        '
        Me.lblInforme.AutoSize = True
        Me.lblInforme.Location = New System.Drawing.Point(11, 129)
        Me.lblInforme.Name = "lblInforme"
        Me.lblInforme.Size = New System.Drawing.Size(45, 13)
        Me.lblInforme.TabIndex = 26
        Me.lblInforme.Text = "Informe:"
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(12, 196)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(113, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 31
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(12, 178)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 30
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'txtHastaFacturado
        '
        Me.txtHastaFacturado.AñoFecha = 0
        Me.txtHastaFacturado.Color = System.Drawing.Color.Empty
        Me.txtHastaFacturado.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHastaFacturado.Location = New System.Drawing.Point(92, 44)
        Me.txtHastaFacturado.MesFecha = 0
        Me.txtHastaFacturado.Name = "txtHastaFacturado"
        Me.txtHastaFacturado.PermitirNulo = False
        Me.txtHastaFacturado.Size = New System.Drawing.Size(84, 20)
        Me.txtHastaFacturado.SoloLectura = False
        Me.txtHastaFacturado.TabIndex = 2
        '
        'txtDesdeFacturado
        '
        Me.txtDesdeFacturado.AñoFecha = 0
        Me.txtDesdeFacturado.Color = System.Drawing.Color.Empty
        Me.txtDesdeFacturado.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesdeFacturado.Location = New System.Drawing.Point(12, 44)
        Me.txtDesdeFacturado.MesFecha = 0
        Me.txtDesdeFacturado.Name = "txtDesdeFacturado"
        Me.txtDesdeFacturado.PermitirNulo = False
        Me.txtDesdeFacturado.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeFacturado.SoloLectura = False
        Me.txtDesdeFacturado.TabIndex = 1
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(643, 343)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 30
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(500, 343)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(125, 23)
        Me.btnInforme.TabIndex = 29
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtNumeroSolicitudCliente)
        Me.gbxFiltro.Controls.Add(Me.chkNumeroSolicitudCliente)
        Me.gbxFiltro.Controls.Add(Me.chkTipoNC)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoNC)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirSubMotivo)
        Me.gbxFiltro.Controls.Add(Me.chkSubMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.cbxSubMotivoNC)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirMotivoDevolucion)
        Me.gbxFiltro.Controls.Add(Me.chkMotivoDevolucion)
        Me.gbxFiltro.Controls.Add(Me.cbxMotivoDevolucion)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirProducto)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.chkExluirUsuario)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.chkExcluirDeposito)
        Me.gbxFiltro.Controls.Add(Me.Label3)
        Me.gbxFiltro.Controls.Add(Me.chkTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoCliente)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.chkTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoProducto)
        Me.gbxFiltro.Controls.Add(Me.ckhUsuario)
        Me.gbxFiltro.Controls.Add(Me.cbxUsuario)
        Me.gbxFiltro.Controls.Add(Me.chkPedido)
        Me.gbxFiltro.Controls.Add(Me.cbxPedidos)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkSucursal)
        Me.gbxFiltro.Controls.Add(Me.cbxSucursal)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Location = New System.Drawing.Point(8, 3)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(489, 436)
        Me.gbxFiltro.TabIndex = 27
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkTipoNC
        '
        Me.chkTipoNC.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoNC.Color = System.Drawing.Color.Empty
        Me.chkTipoNC.Location = New System.Drawing.Point(9, 172)
        Me.chkTipoNC.Name = "chkTipoNC"
        Me.chkTipoNC.Size = New System.Drawing.Size(88, 21)
        Me.chkTipoNC.SoloLectura = False
        Me.chkTipoNC.TabIndex = 61
        Me.chkTipoNC.Texto = "Tipo:"
        Me.chkTipoNC.Valor = False
        '
        'cbxTipoNC
        '
        Me.cbxTipoNC.CampoWhere = ""
        Me.cbxTipoNC.CargarUnaSolaVez = False
        Me.cbxTipoNC.DataDisplayMember = ""
        Me.cbxTipoNC.DataFilter = Nothing
        Me.cbxTipoNC.DataOrderBy = ""
        Me.cbxTipoNC.DataSource = ""
        Me.cbxTipoNC.DataValueMember = ""
        Me.cbxTipoNC.dtSeleccionado = Nothing
        Me.cbxTipoNC.Enabled = False
        Me.cbxTipoNC.FormABM = Nothing
        Me.cbxTipoNC.Indicaciones = Nothing
        Me.cbxTipoNC.Location = New System.Drawing.Point(104, 172)
        Me.cbxTipoNC.Name = "cbxTipoNC"
        Me.cbxTipoNC.SeleccionMultiple = False
        Me.cbxTipoNC.SeleccionObligatoria = False
        Me.cbxTipoNC.Size = New System.Drawing.Size(138, 21)
        Me.cbxTipoNC.SoloLectura = False
        Me.cbxTipoNC.TabIndex = 60
        Me.cbxTipoNC.Texto = ""
        '
        'chkExcluirSubMotivo
        '
        Me.chkExcluirSubMotivo.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirSubMotivo.Color = System.Drawing.Color.Empty
        Me.chkExcluirSubMotivo.Location = New System.Drawing.Point(446, 232)
        Me.chkExcluirSubMotivo.Name = "chkExcluirSubMotivo"
        Me.chkExcluirSubMotivo.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirSubMotivo.SoloLectura = False
        Me.chkExcluirSubMotivo.TabIndex = 59
        Me.chkExcluirSubMotivo.Texto = ""
        Me.chkExcluirSubMotivo.Valor = False
        '
        'chkSubMotivoNC
        '
        Me.chkSubMotivoNC.BackColor = System.Drawing.Color.Transparent
        Me.chkSubMotivoNC.Color = System.Drawing.Color.Empty
        Me.chkSubMotivoNC.Location = New System.Drawing.Point(9, 232)
        Me.chkSubMotivoNC.Name = "chkSubMotivoNC"
        Me.chkSubMotivoNC.Size = New System.Drawing.Size(88, 21)
        Me.chkSubMotivoNC.SoloLectura = False
        Me.chkSubMotivoNC.TabIndex = 58
        Me.chkSubMotivoNC.Texto = "SubMotivo NC:"
        Me.chkSubMotivoNC.Valor = False
        '
        'cbxSubMotivoNC
        '
        Me.cbxSubMotivoNC.CampoWhere = "IDSubMotivoNotaCredito"
        Me.cbxSubMotivoNC.CargarUnaSolaVez = False
        Me.cbxSubMotivoNC.DataDisplayMember = "Descripcion"
        Me.cbxSubMotivoNC.DataFilter = Nothing
        Me.cbxSubMotivoNC.DataOrderBy = "Descripcion"
        Me.cbxSubMotivoNC.DataSource = "submotivonotacredito"
        Me.cbxSubMotivoNC.DataValueMember = "ID"
        Me.cbxSubMotivoNC.dtSeleccionado = Nothing
        Me.cbxSubMotivoNC.Enabled = False
        Me.cbxSubMotivoNC.FormABM = Nothing
        Me.cbxSubMotivoNC.Indicaciones = Nothing
        Me.cbxSubMotivoNC.Location = New System.Drawing.Point(104, 232)
        Me.cbxSubMotivoNC.Name = "cbxSubMotivoNC"
        Me.cbxSubMotivoNC.SeleccionMultiple = False
        Me.cbxSubMotivoNC.SeleccionObligatoria = False
        Me.cbxSubMotivoNC.Size = New System.Drawing.Size(329, 21)
        Me.cbxSubMotivoNC.SoloLectura = False
        Me.cbxSubMotivoNC.TabIndex = 57
        Me.cbxSubMotivoNC.Texto = ""
        '
        'chkExcluirMotivoDevolucion
        '
        Me.chkExcluirMotivoDevolucion.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirMotivoDevolucion.Color = System.Drawing.Color.Empty
        Me.chkExcluirMotivoDevolucion.Location = New System.Drawing.Point(446, 202)
        Me.chkExcluirMotivoDevolucion.Name = "chkExcluirMotivoDevolucion"
        Me.chkExcluirMotivoDevolucion.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirMotivoDevolucion.SoloLectura = False
        Me.chkExcluirMotivoDevolucion.TabIndex = 53
        Me.chkExcluirMotivoDevolucion.Texto = ""
        Me.chkExcluirMotivoDevolucion.Valor = False
        '
        'chkMotivoDevolucion
        '
        Me.chkMotivoDevolucion.BackColor = System.Drawing.Color.Transparent
        Me.chkMotivoDevolucion.Color = System.Drawing.Color.Empty
        Me.chkMotivoDevolucion.Location = New System.Drawing.Point(9, 202)
        Me.chkMotivoDevolucion.Name = "chkMotivoDevolucion"
        Me.chkMotivoDevolucion.Size = New System.Drawing.Size(88, 21)
        Me.chkMotivoDevolucion.SoloLectura = False
        Me.chkMotivoDevolucion.TabIndex = 52
        Me.chkMotivoDevolucion.Texto = "Motivo Dev.:"
        Me.chkMotivoDevolucion.Valor = False
        '
        'cbxMotivoDevolucion
        '
        Me.cbxMotivoDevolucion.CampoWhere = "IDMotivoDevolucion"
        Me.cbxMotivoDevolucion.CargarUnaSolaVez = False
        Me.cbxMotivoDevolucion.DataDisplayMember = "Descripcion"
        Me.cbxMotivoDevolucion.DataFilter = Nothing
        Me.cbxMotivoDevolucion.DataOrderBy = "Descripcion"
        Me.cbxMotivoDevolucion.DataSource = "MotivoDevolucionNC"
        Me.cbxMotivoDevolucion.DataValueMember = "ID"
        Me.cbxMotivoDevolucion.dtSeleccionado = Nothing
        Me.cbxMotivoDevolucion.Enabled = False
        Me.cbxMotivoDevolucion.FormABM = Nothing
        Me.cbxMotivoDevolucion.Indicaciones = Nothing
        Me.cbxMotivoDevolucion.Location = New System.Drawing.Point(104, 202)
        Me.cbxMotivoDevolucion.Name = "cbxMotivoDevolucion"
        Me.cbxMotivoDevolucion.SeleccionMultiple = False
        Me.cbxMotivoDevolucion.SeleccionObligatoria = False
        Me.cbxMotivoDevolucion.Size = New System.Drawing.Size(329, 21)
        Me.cbxMotivoDevolucion.SoloLectura = False
        Me.cbxMotivoDevolucion.TabIndex = 51
        Me.cbxMotivoDevolucion.Texto = ""
        '
        'chkExcluirProducto
        '
        Me.chkExcluirProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirProducto.Color = System.Drawing.Color.Empty
        Me.chkExcluirProducto.Location = New System.Drawing.Point(446, 290)
        Me.chkExcluirProducto.Name = "chkExcluirProducto"
        Me.chkExcluirProducto.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirProducto.SoloLectura = False
        Me.chkExcluirProducto.TabIndex = 50
        Me.chkExcluirProducto.Texto = ""
        Me.chkExcluirProducto.Valor = False
        '
        'chkExcluirTipoCliente
        '
        Me.chkExcluirTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoCliente.Location = New System.Drawing.Point(446, 142)
        Me.chkExcluirTipoCliente.Name = "chkExcluirTipoCliente"
        Me.chkExcluirTipoCliente.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoCliente.SoloLectura = False
        Me.chkExcluirTipoCliente.TabIndex = 48
        Me.chkExcluirTipoCliente.Texto = ""
        Me.chkExcluirTipoCliente.Valor = False
        '
        'chkExluirUsuario
        '
        Me.chkExluirUsuario.BackColor = System.Drawing.Color.Transparent
        Me.chkExluirUsuario.Color = System.Drawing.Color.Empty
        Me.chkExluirUsuario.Location = New System.Drawing.Point(446, 112)
        Me.chkExluirUsuario.Name = "chkExluirUsuario"
        Me.chkExluirUsuario.Size = New System.Drawing.Size(17, 13)
        Me.chkExluirUsuario.SoloLectura = False
        Me.chkExluirUsuario.TabIndex = 47
        Me.chkExluirUsuario.Texto = ""
        Me.chkExluirUsuario.Valor = False
        '
        'chkExcluirTipoProducto
        '
        Me.chkExcluirTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkExcluirTipoProducto.Location = New System.Drawing.Point(446, 82)
        Me.chkExcluirTipoProducto.Name = "chkExcluirTipoProducto"
        Me.chkExcluirTipoProducto.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirTipoProducto.SoloLectura = False
        Me.chkExcluirTipoProducto.TabIndex = 46
        Me.chkExcluirTipoProducto.Texto = ""
        Me.chkExcluirTipoProducto.Valor = False
        '
        'chkExcluirDeposito
        '
        Me.chkExcluirDeposito.BackColor = System.Drawing.Color.Transparent
        Me.chkExcluirDeposito.Color = System.Drawing.Color.Empty
        Me.chkExcluirDeposito.Location = New System.Drawing.Point(446, 22)
        Me.chkExcluirDeposito.Name = "chkExcluirDeposito"
        Me.chkExcluirDeposito.Size = New System.Drawing.Size(17, 13)
        Me.chkExcluirDeposito.SoloLectura = False
        Me.chkExcluirDeposito.TabIndex = 44
        Me.chkExcluirDeposito.Texto = ""
        Me.chkExcluirDeposito.Valor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(444, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 12)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "Excluir"
        '
        'chkTipoCliente
        '
        Me.chkTipoCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoCliente.Color = System.Drawing.Color.Empty
        Me.chkTipoCliente.Location = New System.Drawing.Point(9, 142)
        Me.chkTipoCliente.Name = "chkTipoCliente"
        Me.chkTipoCliente.Size = New System.Drawing.Size(88, 21)
        Me.chkTipoCliente.SoloLectura = False
        Me.chkTipoCliente.TabIndex = 40
        Me.chkTipoCliente.Texto = "Tipo Cliente:"
        Me.chkTipoCliente.Valor = False
        '
        'cbxTipoCliente
        '
        Me.cbxTipoCliente.CampoWhere = "IDTipoCliente"
        Me.cbxTipoCliente.CargarUnaSolaVez = False
        Me.cbxTipoCliente.DataDisplayMember = "Descripcion"
        Me.cbxTipoCliente.DataFilter = Nothing
        Me.cbxTipoCliente.DataOrderBy = "Descripcion"
        Me.cbxTipoCliente.DataSource = "vTipoCliente"
        Me.cbxTipoCliente.DataValueMember = "ID"
        Me.cbxTipoCliente.dtSeleccionado = Nothing
        Me.cbxTipoCliente.Enabled = False
        Me.cbxTipoCliente.FormABM = Nothing
        Me.cbxTipoCliente.Indicaciones = Nothing
        Me.cbxTipoCliente.Location = New System.Drawing.Point(104, 142)
        Me.cbxTipoCliente.Name = "cbxTipoCliente"
        Me.cbxTipoCliente.SeleccionMultiple = False
        Me.cbxTipoCliente.SeleccionObligatoria = False
        Me.cbxTipoCliente.Size = New System.Drawing.Size(329, 21)
        Me.cbxTipoCliente.SoloLectura = False
        Me.cbxTipoCliente.TabIndex = 38
        Me.cbxTipoCliente.Texto = "SUPERMERCADO"
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(104, 290)
        Me.txtProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(329, 65)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 36
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(9, 290)
        Me.chkProducto.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(88, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 37
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        '
        'chkTipoProducto
        '
        Me.chkTipoProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoProducto.Color = System.Drawing.Color.Empty
        Me.chkTipoProducto.Location = New System.Drawing.Point(9, 82)
        Me.chkTipoProducto.Name = "chkTipoProducto"
        Me.chkTipoProducto.Size = New System.Drawing.Size(91, 21)
        Me.chkTipoProducto.SoloLectura = False
        Me.chkTipoProducto.TabIndex = 32
        Me.chkTipoProducto.Texto = "Tipo Producto:"
        Me.chkTipoProducto.Valor = False
        '
        'cbxTipoProducto
        '
        Me.cbxTipoProducto.CampoWhere = "IDTipoProducto"
        Me.cbxTipoProducto.CargarUnaSolaVez = False
        Me.cbxTipoProducto.DataDisplayMember = "Descripcion"
        Me.cbxTipoProducto.DataFilter = Nothing
        Me.cbxTipoProducto.DataOrderBy = "Descripcion"
        Me.cbxTipoProducto.DataSource = "VTipoProducto"
        Me.cbxTipoProducto.DataValueMember = "ID"
        Me.cbxTipoProducto.dtSeleccionado = Nothing
        Me.cbxTipoProducto.Enabled = False
        Me.cbxTipoProducto.FormABM = Nothing
        Me.cbxTipoProducto.Indicaciones = Nothing
        Me.cbxTipoProducto.Location = New System.Drawing.Point(104, 82)
        Me.cbxTipoProducto.Name = "cbxTipoProducto"
        Me.cbxTipoProducto.SeleccionMultiple = False
        Me.cbxTipoProducto.SeleccionObligatoria = False
        Me.cbxTipoProducto.Size = New System.Drawing.Size(329, 21)
        Me.cbxTipoProducto.SoloLectura = False
        Me.cbxTipoProducto.TabIndex = 33
        Me.cbxTipoProducto.Texto = ""
        '
        'ckhUsuario
        '
        Me.ckhUsuario.BackColor = System.Drawing.Color.Transparent
        Me.ckhUsuario.Color = System.Drawing.Color.Empty
        Me.ckhUsuario.Location = New System.Drawing.Point(9, 112)
        Me.ckhUsuario.Name = "ckhUsuario"
        Me.ckhUsuario.Size = New System.Drawing.Size(75, 21)
        Me.ckhUsuario.SoloLectura = False
        Me.ckhUsuario.TabIndex = 30
        Me.ckhUsuario.Texto = "Usuario:"
        Me.ckhUsuario.Valor = False
        '
        'cbxUsuario
        '
        Me.cbxUsuario.CampoWhere = "IDUsuario"
        Me.cbxUsuario.CargarUnaSolaVez = False
        Me.cbxUsuario.DataDisplayMember = "Nombre"
        Me.cbxUsuario.DataFilter = Nothing
        Me.cbxUsuario.DataOrderBy = "Nombre"
        Me.cbxUsuario.DataSource = "vUsuario"
        Me.cbxUsuario.DataValueMember = "ID"
        Me.cbxUsuario.dtSeleccionado = Nothing
        Me.cbxUsuario.Enabled = False
        Me.cbxUsuario.FormABM = Nothing
        Me.cbxUsuario.Indicaciones = Nothing
        Me.cbxUsuario.Location = New System.Drawing.Point(104, 112)
        Me.cbxUsuario.Name = "cbxUsuario"
        Me.cbxUsuario.SeleccionMultiple = False
        Me.cbxUsuario.SeleccionObligatoria = False
        Me.cbxUsuario.Size = New System.Drawing.Size(329, 21)
        Me.cbxUsuario.SoloLectura = False
        Me.cbxUsuario.TabIndex = 31
        Me.cbxUsuario.Texto = "CAST"
        '
        'chkPedido
        '
        Me.chkPedido.BackColor = System.Drawing.Color.Transparent
        Me.chkPedido.Color = System.Drawing.Color.Empty
        Me.chkPedido.Location = New System.Drawing.Point(9, 52)
        Me.chkPedido.Name = "chkPedido"
        Me.chkPedido.Size = New System.Drawing.Size(78, 21)
        Me.chkPedido.SoloLectura = False
        Me.chkPedido.TabIndex = 29
        Me.chkPedido.Texto = "Pedidos:"
        Me.chkPedido.Valor = False
        '
        'cbxPedidos
        '
        Me.cbxPedidos.CampoWhere = Nothing
        Me.cbxPedidos.CargarUnaSolaVez = False
        Me.cbxPedidos.DataDisplayMember = Nothing
        Me.cbxPedidos.DataFilter = Nothing
        Me.cbxPedidos.DataOrderBy = Nothing
        Me.cbxPedidos.DataSource = Nothing
        Me.cbxPedidos.DataValueMember = Nothing
        Me.cbxPedidos.dtSeleccionado = Nothing
        Me.cbxPedidos.Enabled = False
        Me.cbxPedidos.FormABM = Nothing
        Me.cbxPedidos.Indicaciones = Nothing
        Me.cbxPedidos.Location = New System.Drawing.Point(104, 52)
        Me.cbxPedidos.Name = "cbxPedidos"
        Me.cbxPedidos.SeleccionMultiple = False
        Me.cbxPedidos.SeleccionObligatoria = False
        Me.cbxPedidos.Size = New System.Drawing.Size(329, 21)
        Me.cbxPedidos.SoloLectura = False
        Me.cbxPedidos.TabIndex = 28
        Me.cbxPedidos.Texto = "TODOS"
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(9, 365)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(423, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 19
        '
        'chkSucursal
        '
        Me.chkSucursal.BackColor = System.Drawing.Color.Transparent
        Me.chkSucursal.Color = System.Drawing.Color.Empty
        Me.chkSucursal.Location = New System.Drawing.Point(9, 22)
        Me.chkSucursal.Name = "chkSucursal"
        Me.chkSucursal.Size = New System.Drawing.Size(75, 21)
        Me.chkSucursal.SoloLectura = False
        Me.chkSucursal.TabIndex = 20
        Me.chkSucursal.Texto = "Deposito:"
        Me.chkSucursal.Valor = False
        '
        'cbxSucursal
        '
        Me.cbxSucursal.CampoWhere = "IDDeposito"
        Me.cbxSucursal.CargarUnaSolaVez = False
        Me.cbxSucursal.DataDisplayMember = "SucDeposito"
        Me.cbxSucursal.DataFilter = Nothing
        Me.cbxSucursal.DataOrderBy = "SucDeposito"
        Me.cbxSucursal.DataSource = "VDeposito"
        Me.cbxSucursal.DataValueMember = "ID"
        Me.cbxSucursal.dtSeleccionado = Nothing
        Me.cbxSucursal.Enabled = False
        Me.cbxSucursal.FormABM = Nothing
        Me.cbxSucursal.Indicaciones = Nothing
        Me.cbxSucursal.Location = New System.Drawing.Point(104, 22)
        Me.cbxSucursal.Name = "cbxSucursal"
        Me.cbxSucursal.SeleccionMultiple = False
        Me.cbxSucursal.SeleccionObligatoria = False
        Me.cbxSucursal.Size = New System.Drawing.Size(329, 21)
        Me.cbxSucursal.SoloLectura = False
        Me.cbxSucursal.TabIndex = 21
        Me.cbxSucursal.Texto = "CENTRAL"
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 344)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(77, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 18
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'txtNumeroSolicitudCliente
        '
        Me.txtNumeroSolicitudCliente.Enabled = False
        Me.txtNumeroSolicitudCliente.Location = New System.Drawing.Point(104, 260)
        Me.txtNumeroSolicitudCliente.Name = "txtNumeroSolicitudCliente"
        Me.txtNumeroSolicitudCliente.Size = New System.Drawing.Size(113, 20)
        Me.txtNumeroSolicitudCliente.TabIndex = 63
        '
        'chkNumeroSolicitudCliente
        '
        Me.chkNumeroSolicitudCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkNumeroSolicitudCliente.Color = System.Drawing.Color.Empty
        Me.chkNumeroSolicitudCliente.Location = New System.Drawing.Point(9, 259)
        Me.chkNumeroSolicitudCliente.Name = "chkNumeroSolicitudCliente"
        Me.chkNumeroSolicitudCliente.Size = New System.Drawing.Size(114, 21)
        Me.chkNumeroSolicitudCliente.SoloLectura = False
        Me.chkNumeroSolicitudCliente.TabIndex = 62
        Me.chkNumeroSolicitudCliente.Texto = "Solicitud Cliente:"
        Me.chkNumeroSolicitudCliente.Valor = False
        '
        'frmListadoPedidoNotaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(711, 451)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmListadoPedidoNotaCredito"
        Me.Text = "frmListadoPedidoNotaCredito"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkFechaFacturacion As ocxCHK
    Friend WithEvents chkFechaPedido As ocxCHK
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtHastaPedido As ocxTXTDate
    Friend WithEvents txtDesdePedido As ocxTXTDate
    Friend WithEvents cbxTipoInforme As ocxCBX
    Friend WithEvents lblInforme As Label
    Friend WithEvents cbxOrdenadoPor As ocxCBX
    Friend WithEvents lblOrdenado As Label
    Friend WithEvents txtHastaFacturado As ocxTXTDate
    Friend WithEvents txtDesdeFacturado As ocxTXTDate
    Friend WithEvents btnCerrar As Button
    Friend WithEvents btnInforme As Button
    Friend WithEvents gbxFiltro As GroupBox
    Friend WithEvents chkExcluirProducto As ocxCHK
    Friend WithEvents chkExcluirTipoCliente As ocxCHK
    Friend WithEvents chkExluirUsuario As ocxCHK
    Friend WithEvents chkExcluirTipoProducto As ocxCHK
    Friend WithEvents chkExcluirDeposito As ocxCHK
    Friend WithEvents Label3 As Label
    Friend WithEvents chkTipoCliente As ocxCHK
    Friend WithEvents cbxTipoCliente As ocxCBX
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents chkProducto As ocxCHK
    Friend WithEvents chkTipoProducto As ocxCHK
    Friend WithEvents cbxTipoProducto As ocxCBX
    Friend WithEvents ckhUsuario As ocxCHK
    Friend WithEvents cbxUsuario As ocxCBX
    Friend WithEvents chkPedido As ocxCHK
    Friend WithEvents cbxPedidos As ocxCBX
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents chkSucursal As ocxCHK
    Friend WithEvents cbxSucursal As ocxCBX
    Friend WithEvents chkCliente As ocxCHK
    Friend WithEvents chkExcluirMotivoDevolucion As ocxCHK
    Friend WithEvents chkMotivoDevolucion As ocxCHK
    Friend WithEvents cbxMotivoDevolucion As ocxCBX
    Friend WithEvents chkTipoNC As ocxCHK
    Friend WithEvents cbxTipoNC As ocxCBX
    Friend WithEvents chkExcluirSubMotivo As ocxCHK
    Friend WithEvents chkSubMotivoNC As ocxCHK
    Friend WithEvents cbxSubMotivoNC As ocxCBX
    Friend WithEvents txtNumeroSolicitudCliente As TextBox
    Friend WithEvents chkNumeroSolicitudCliente As ocxCHK
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoAcuerdodeClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.cbxEstado = New ERP.ocxCBX()
        Me.lblPeriodo = New System.Windows.Forms.Label()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.chkProveedor = New ERP.ocxCHK()
        Me.chkFamilia = New ERP.ocxCHK()
        Me.cbxFamilia = New ERP.ocxCBX()
        Me.chkProducto = New ERP.ocxCHK()
        Me.txtNroAcuerdo = New System.Windows.Forms.TextBox()
        Me.chkNroAcuerdo = New ERP.ocxCHK()
        Me.chkPresentacion = New ERP.ocxCHK()
        Me.cbxPresentacion = New ERP.ocxCBX()
        Me.chkTipoAcuerdo = New ERP.ocxCHK()
        Me.cbxTipoAcuerdo = New ERP.ocxCBX()
        Me.chkAnulados = New ERP.ocxCHK()
        Me.rdbIncluir = New System.Windows.Forms.RadioButton()
        Me.rdbSoloAnulados = New System.Windows.Forms.RadioButton()
        Me.chkCliente = New ERP.ocxCHK()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.txtProveedor = New ERP.ocxTXTProveedor()
        Me.txtProducto = New ERP.ocxTXTProducto()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblEstado)
        Me.GroupBox2.Controls.Add(Me.cbxEstado)
        Me.GroupBox2.Controls.Add(Me.lblPeriodo)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Location = New System.Drawing.Point(499, 22)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(268, 177)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(6, 119)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 11
        Me.lblEstado.Text = "Estado:"
        '
        'cbxEstado
        '
        Me.cbxEstado.CampoWhere = Nothing
        Me.cbxEstado.CargarUnaSolaVez = False
        Me.cbxEstado.DataDisplayMember = Nothing
        Me.cbxEstado.DataFilter = Nothing
        Me.cbxEstado.DataOrderBy = Nothing
        Me.cbxEstado.DataSource = Nothing
        Me.cbxEstado.DataValueMember = Nothing
        Me.cbxEstado.dtSeleccionado = Nothing
        Me.cbxEstado.FormABM = Nothing
        Me.cbxEstado.Indicaciones = Nothing
        Me.cbxEstado.Location = New System.Drawing.Point(9, 135)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.SeleccionMultiple = False
        Me.cbxEstado.SeleccionObligatoria = False
        Me.cbxEstado.Size = New System.Drawing.Size(97, 21)
        Me.cbxEstado.SoloLectura = False
        Me.cbxEstado.TabIndex = 10
        Me.cbxEstado.Texto = ""
        '
        'lblPeriodo
        '
        Me.lblPeriodo.AutoSize = True
        Me.lblPeriodo.Location = New System.Drawing.Point(6, 19)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(46, 13)
        Me.lblPeriodo.TabIndex = 0
        Me.lblPeriodo.Text = "Periodo:"
        '
        'txtHasta
        '
        Me.txtHasta.AñoFecha = 0
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtHasta.Location = New System.Drawing.Point(93, 38)
        Me.txtHasta.MesFecha = 0
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(74, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 2
        '
        'txtDesde
        '
        Me.txtDesde.AñoFecha = 0
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 6, 3, 8, 7, 1, 128)
        Me.txtDesde.Location = New System.Drawing.Point(9, 38)
        Me.txtDesde.MesFecha = 0
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 1
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(171, 86)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(66, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 7
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(8, 86)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(157, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 6
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(6, 70)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 5
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(667, 223)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(100, 23)
        Me.btnCerrar.TabIndex = 11
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.chkProveedor)
        Me.gbxFiltro.Controls.Add(Me.chkFamilia)
        Me.gbxFiltro.Controls.Add(Me.cbxFamilia)
        Me.gbxFiltro.Controls.Add(Me.chkProducto)
        Me.gbxFiltro.Controls.Add(Me.txtNroAcuerdo)
        Me.gbxFiltro.Controls.Add(Me.chkNroAcuerdo)
        Me.gbxFiltro.Controls.Add(Me.chkPresentacion)
        Me.gbxFiltro.Controls.Add(Me.cbxPresentacion)
        Me.gbxFiltro.Controls.Add(Me.chkTipoAcuerdo)
        Me.gbxFiltro.Controls.Add(Me.cbxTipoAcuerdo)
        Me.gbxFiltro.Controls.Add(Me.chkAnulados)
        Me.gbxFiltro.Controls.Add(Me.rdbIncluir)
        Me.gbxFiltro.Controls.Add(Me.rdbSoloAnulados)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.txtProveedor)
        Me.gbxFiltro.Controls.Add(Me.txtProducto)
        Me.gbxFiltro.Location = New System.Drawing.Point(9, 22)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(471, 359)
        Me.gbxFiltro.TabIndex = 8
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'chkProveedor
        '
        Me.chkProveedor.BackColor = System.Drawing.Color.Transparent
        Me.chkProveedor.Color = System.Drawing.Color.Empty
        Me.chkProveedor.Location = New System.Drawing.Point(7, 109)
        Me.chkProveedor.Margin = New System.Windows.Forms.Padding(4)
        Me.chkProveedor.Name = "chkProveedor"
        Me.chkProveedor.Size = New System.Drawing.Size(72, 21)
        Me.chkProveedor.SoloLectura = False
        Me.chkProveedor.TabIndex = 45
        Me.chkProveedor.Texto = "Proveedor:"
        Me.chkProveedor.Valor = False
        '
        'chkFamilia
        '
        Me.chkFamilia.BackColor = System.Drawing.Color.Transparent
        Me.chkFamilia.Color = System.Drawing.Color.Empty
        Me.chkFamilia.Location = New System.Drawing.Point(9, 268)
        Me.chkFamilia.Name = "chkFamilia"
        Me.chkFamilia.Size = New System.Drawing.Size(114, 21)
        Me.chkFamilia.SoloLectura = False
        Me.chkFamilia.TabIndex = 43
        Me.chkFamilia.Texto = "Familia:"
        Me.chkFamilia.Valor = False
        Me.chkFamilia.Visible = False
        '
        'cbxFamilia
        '
        Me.cbxFamilia.CampoWhere = "VSubLinea.ID"
        Me.cbxFamilia.CargarUnaSolaVez = False
        Me.cbxFamilia.DataDisplayMember = "Descripcion"
        Me.cbxFamilia.DataFilter = Nothing
        Me.cbxFamilia.DataOrderBy = "Descripcion"
        Me.cbxFamilia.DataSource = "VSubLinea"
        Me.cbxFamilia.DataValueMember = "ID"
        Me.cbxFamilia.dtSeleccionado = Nothing
        Me.cbxFamilia.Enabled = False
        Me.cbxFamilia.FormABM = Nothing
        Me.cbxFamilia.Indicaciones = Nothing
        Me.cbxFamilia.Location = New System.Drawing.Point(138, 268)
        Me.cbxFamilia.Name = "cbxFamilia"
        Me.cbxFamilia.SeleccionMultiple = False
        Me.cbxFamilia.SeleccionObligatoria = False
        Me.cbxFamilia.Size = New System.Drawing.Size(276, 21)
        Me.cbxFamilia.SoloLectura = False
        Me.cbxFamilia.TabIndex = 44
        Me.cbxFamilia.Texto = ""
        Me.cbxFamilia.Visible = False
        '
        'chkProducto
        '
        Me.chkProducto.BackColor = System.Drawing.Color.Transparent
        Me.chkProducto.Color = System.Drawing.Color.Empty
        Me.chkProducto.Location = New System.Drawing.Point(9, 194)
        Me.chkProducto.Name = "chkProducto"
        Me.chkProducto.Size = New System.Drawing.Size(62, 21)
        Me.chkProducto.SoloLectura = False
        Me.chkProducto.TabIndex = 42
        Me.chkProducto.Texto = "Producto:"
        Me.chkProducto.Valor = False
        Me.chkProducto.Visible = False
        '
        'txtNroAcuerdo
        '
        Me.txtNroAcuerdo.Enabled = False
        Me.txtNroAcuerdo.Location = New System.Drawing.Point(96, 19)
        Me.txtNroAcuerdo.Name = "txtNroAcuerdo"
        Me.txtNroAcuerdo.Size = New System.Drawing.Size(113, 20)
        Me.txtNroAcuerdo.TabIndex = 41
        '
        'chkNroAcuerdo
        '
        Me.chkNroAcuerdo.BackColor = System.Drawing.Color.Transparent
        Me.chkNroAcuerdo.Color = System.Drawing.Color.Empty
        Me.chkNroAcuerdo.Location = New System.Drawing.Point(7, 19)
        Me.chkNroAcuerdo.Name = "chkNroAcuerdo"
        Me.chkNroAcuerdo.Size = New System.Drawing.Size(83, 21)
        Me.chkNroAcuerdo.SoloLectura = False
        Me.chkNroAcuerdo.TabIndex = 40
        Me.chkNroAcuerdo.Texto = "Nro Acuerdo:"
        Me.chkNroAcuerdo.Valor = False
        '
        'chkPresentacion
        '
        Me.chkPresentacion.BackColor = System.Drawing.Color.Transparent
        Me.chkPresentacion.Color = System.Drawing.Color.Empty
        Me.chkPresentacion.Location = New System.Drawing.Point(9, 241)
        Me.chkPresentacion.Name = "chkPresentacion"
        Me.chkPresentacion.Size = New System.Drawing.Size(114, 21)
        Me.chkPresentacion.SoloLectura = False
        Me.chkPresentacion.TabIndex = 38
        Me.chkPresentacion.Texto = "Presentacion :"
        Me.chkPresentacion.Valor = False
        Me.chkPresentacion.Visible = False
        '
        'cbxPresentacion
        '
        Me.cbxPresentacion.CampoWhere = "VPresentacion.ID"
        Me.cbxPresentacion.CargarUnaSolaVez = False
        Me.cbxPresentacion.DataDisplayMember = "Descripcion"
        Me.cbxPresentacion.DataFilter = Nothing
        Me.cbxPresentacion.DataOrderBy = "Descripcion"
        Me.cbxPresentacion.DataSource = "VPresentacion"
        Me.cbxPresentacion.DataValueMember = "ID"
        Me.cbxPresentacion.dtSeleccionado = Nothing
        Me.cbxPresentacion.Enabled = False
        Me.cbxPresentacion.FormABM = Nothing
        Me.cbxPresentacion.Indicaciones = Nothing
        Me.cbxPresentacion.Location = New System.Drawing.Point(138, 241)
        Me.cbxPresentacion.Name = "cbxPresentacion"
        Me.cbxPresentacion.SeleccionMultiple = False
        Me.cbxPresentacion.SeleccionObligatoria = False
        Me.cbxPresentacion.Size = New System.Drawing.Size(276, 21)
        Me.cbxPresentacion.SoloLectura = False
        Me.cbxPresentacion.TabIndex = 39
        Me.cbxPresentacion.Texto = ""
        Me.cbxPresentacion.Visible = False
        '
        'chkTipoAcuerdo
        '
        Me.chkTipoAcuerdo.BackColor = System.Drawing.Color.Transparent
        Me.chkTipoAcuerdo.Color = System.Drawing.Color.Empty
        Me.chkTipoAcuerdo.Location = New System.Drawing.Point(230, 19)
        Me.chkTipoAcuerdo.Name = "chkTipoAcuerdo"
        Me.chkTipoAcuerdo.Size = New System.Drawing.Size(101, 21)
        Me.chkTipoAcuerdo.SoloLectura = False
        Me.chkTipoAcuerdo.TabIndex = 37
        Me.chkTipoAcuerdo.Texto = "Tipo de Acuerdo:"
        Me.chkTipoAcuerdo.Valor = False
        '
        'cbxTipoAcuerdo
        '
        Me.cbxTipoAcuerdo.CampoWhere = ""
        Me.cbxTipoAcuerdo.CargarUnaSolaVez = False
        Me.cbxTipoAcuerdo.DataDisplayMember = ""
        Me.cbxTipoAcuerdo.DataFilter = Nothing
        Me.cbxTipoAcuerdo.DataOrderBy = ""
        Me.cbxTipoAcuerdo.DataSource = ""
        Me.cbxTipoAcuerdo.DataValueMember = ""
        Me.cbxTipoAcuerdo.dtSeleccionado = Nothing
        Me.cbxTipoAcuerdo.Enabled = False
        Me.cbxTipoAcuerdo.FormABM = Nothing
        Me.cbxTipoAcuerdo.Indicaciones = Nothing
        Me.cbxTipoAcuerdo.Location = New System.Drawing.Point(336, 19)
        Me.cbxTipoAcuerdo.Name = "cbxTipoAcuerdo"
        Me.cbxTipoAcuerdo.SeleccionMultiple = False
        Me.cbxTipoAcuerdo.SeleccionObligatoria = False
        Me.cbxTipoAcuerdo.Size = New System.Drawing.Size(126, 21)
        Me.cbxTipoAcuerdo.SoloLectura = False
        Me.cbxTipoAcuerdo.TabIndex = 36
        Me.cbxTipoAcuerdo.Texto = ""
        '
        'chkAnulados
        '
        Me.chkAnulados.BackColor = System.Drawing.Color.Transparent
        Me.chkAnulados.Color = System.Drawing.Color.Empty
        Me.chkAnulados.Location = New System.Drawing.Point(9, 301)
        Me.chkAnulados.Name = "chkAnulados"
        Me.chkAnulados.Size = New System.Drawing.Size(114, 21)
        Me.chkAnulados.SoloLectura = False
        Me.chkAnulados.TabIndex = 35
        Me.chkAnulados.Texto = "Anulados:"
        Me.chkAnulados.Valor = False
        Me.chkAnulados.Visible = False
        '
        'rdbIncluir
        '
        Me.rdbIncluir.AutoSize = True
        Me.rdbIncluir.Checked = True
        Me.rdbIncluir.Enabled = False
        Me.rdbIncluir.Location = New System.Drawing.Point(141, 301)
        Me.rdbIncluir.Name = "rdbIncluir"
        Me.rdbIncluir.Size = New System.Drawing.Size(53, 17)
        Me.rdbIncluir.TabIndex = 33
        Me.rdbIncluir.TabStop = True
        Me.rdbIncluir.Text = "Incluir"
        Me.rdbIncluir.UseVisualStyleBackColor = True
        Me.rdbIncluir.Visible = False
        '
        'rdbSoloAnulados
        '
        Me.rdbSoloAnulados.AutoSize = True
        Me.rdbSoloAnulados.Enabled = False
        Me.rdbSoloAnulados.Location = New System.Drawing.Point(202, 301)
        Me.rdbSoloAnulados.Name = "rdbSoloAnulados"
        Me.rdbSoloAnulados.Size = New System.Drawing.Size(93, 17)
        Me.rdbSoloAnulados.TabIndex = 34
        Me.rdbSoloAnulados.Text = "Solo Anulados"
        Me.rdbSoloAnulados.UseVisualStyleBackColor = True
        Me.rdbSoloAnulados.Visible = False
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(7, 50)
        Me.chkCliente.Margin = New System.Windows.Forms.Padding(4)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(53, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 31
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'txtCliente
        '
        Me.txtCliente.Actualizar = True
        Me.txtCliente.AlturaMaxima = 61
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.ControlCorto = False
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(7, 71)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(458, 24)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 11
        '
        'txtProveedor
        '
        Me.txtProveedor.AlturaMaxima = 80
        Me.txtProveedor.Consulta = Nothing
        Me.txtProveedor.Enabled = False
        Me.txtProveedor.frm = Nothing
        Me.txtProveedor.Location = New System.Drawing.Point(7, 133)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Registro = Nothing
        Me.txtProveedor.Seleccionado = False
        Me.txtProveedor.Size = New System.Drawing.Size(458, 42)
        Me.txtProveedor.SoloLectura = False
        Me.txtProveedor.Sucursal = Nothing
        Me.txtProveedor.SucursalSeleccionada = False
        Me.txtProveedor.TabIndex = 9
        '
        'txtProducto
        '
        Me.txtProducto.AlturaMaxima = 393
        Me.txtProducto.ColumnasNumericas = Nothing
        Me.txtProducto.Compra = False
        Me.txtProducto.Consulta = Nothing
        Me.txtProducto.ControlarExistencia = False
        Me.txtProducto.ControlarReservas = False
        Me.txtProducto.Cotizacion = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.dtDescuento = Nothing
        Me.txtProducto.dtProductosSeleccionados = Nothing
        Me.txtProducto.Enabled = False
        Me.txtProducto.FechaFacturarPedido = New Date(CType(0, Long))
        Me.txtProducto.IDCliente = 0
        Me.txtProducto.IDClienteSucursal = 0
        Me.txtProducto.IDDeposito = 0
        Me.txtProducto.IDListaPrecio = 0
        Me.txtProducto.IDMoneda = 0
        Me.txtProducto.IDSucursal = 0
        Me.txtProducto.Location = New System.Drawing.Point(96, 194)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Pedido = False
        Me.txtProducto.Precios = Nothing
        Me.txtProducto.Registro = Nothing
        Me.txtProducto.Seleccionado = False
        Me.txtProducto.SeleccionMultiple = False
        Me.txtProducto.Size = New System.Drawing.Size(296, 47)
        Me.txtProducto.SoloLectura = False
        Me.txtProducto.TabIndex = 7
        Me.txtProducto.TieneDescuento = False
        Me.txtProducto.TotalPorcentajeDescuento = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtProducto.Venta = False
        Me.txtProducto.Visible = False
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(499, 223)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(100, 23)
        Me.btnInforme.TabIndex = 10
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'frmListadoAcuerdodeClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 275)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Controls.Add(Me.btnInforme)
        Me.Name = "frmListadoAcuerdodeClientes"
        Me.Tag = "frmListadoAcuerdodeClientes"
        Me.Text = "frmListadoAcuerdodeClientes"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.gbxFiltro.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lblEstado As Label
    Friend WithEvents cbxEstado As ocxCBX
    Friend WithEvents lblPeriodo As Label
    Friend WithEvents txtHasta As ocxTXTDate
    Friend WithEvents txtDesde As ocxTXTDate
    Friend WithEvents cbxEnForma As ocxCBX
    Friend WithEvents cbxOrdenadoPor As ocxCBX
    Friend WithEvents lblOrdenado As Label
    Friend WithEvents btnCerrar As Button
    Friend WithEvents gbxFiltro As GroupBox
    Public WithEvents txtProveedor As ocxTXTProveedor
    Friend WithEvents txtProducto As ocxTXTProducto
    Friend WithEvents btnInforme As Button
    Friend WithEvents txtCliente As ocxTXTCliente
    Friend WithEvents chkCliente As ocxCHK
    Friend WithEvents txtNroAcuerdo As TextBox
    Friend WithEvents chkNroAcuerdo As ocxCHK
    Friend WithEvents chkPresentacion As ocxCHK
    Friend WithEvents cbxPresentacion As ocxCBX
    Friend WithEvents chkTipoAcuerdo As ocxCHK
    Friend WithEvents cbxTipoAcuerdo As ocxCBX
    Friend WithEvents chkAnulados As ocxCHK
    Friend WithEvents rdbIncluir As RadioButton
    Friend WithEvents rdbSoloAnulados As RadioButton
    Friend WithEvents chkProducto As ocxCHK
    Friend WithEvents chkFamilia As ocxCHK
    Friend WithEvents cbxFamilia As ocxCBX
    Friend WithEvents chkProveedor As ocxCHK
End Class

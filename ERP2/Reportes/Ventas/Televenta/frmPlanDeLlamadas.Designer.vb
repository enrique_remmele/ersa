﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPlanDeLlamadas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxRealizado = New ERP.ocxCBX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxEnForma = New ERP.ocxCBX()
        Me.cbxOrdenadoPor = New ERP.ocxCBX()
        Me.lblOrdenado = New System.Windows.Forms.Label()
        Me.txtHastaVolverLlamar = New ERP.ocxTXTDate()
        Me.txtDesdeVolverLlamar = New ERP.ocxTXTDate()
        Me.gbxFiltro = New System.Windows.Forms.GroupBox()
        Me.txtCliente = New ERP.ocxTXTCliente()
        Me.chkVendedor = New ERP.ocxCHK()
        Me.cbxVendedor = New ERP.ocxCBX()
        Me.chkCliente = New ERP.ocxCHK()
        Me.txtHasta = New ERP.ocxTXTDate()
        Me.txtDesde = New ERP.ocxTXTDate()
        Me.chkFechaVolverLlamar = New ERP.ocxCHK()
        Me.chkFechaLlamada = New ERP.ocxCHK()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFiltro.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCerrar
        '
        Me.btnCerrar.Location = New System.Drawing.Point(596, 225)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(63, 23)
        Me.btnCerrar.TabIndex = 29
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(471, 225)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(107, 23)
        Me.btnInforme.TabIndex = 28
        Me.btnInforme.Text = "Listar"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkFechaLlamada)
        Me.GroupBox2.Controls.Add(Me.chkFechaVolverLlamar)
        Me.GroupBox2.Controls.Add(Me.txtHasta)
        Me.GroupBox2.Controls.Add(Me.txtDesde)
        Me.GroupBox2.Controls.Add(Me.cbxRealizado)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cbxEnForma)
        Me.GroupBox2.Controls.Add(Me.cbxOrdenadoPor)
        Me.GroupBox2.Controls.Add(Me.lblOrdenado)
        Me.GroupBox2.Controls.Add(Me.txtHastaVolverLlamar)
        Me.GroupBox2.Controls.Add(Me.txtDesdeVolverLlamar)
        Me.GroupBox2.Location = New System.Drawing.Point(471, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(187, 207)
        Me.GroupBox2.TabIndex = 27
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opciones"
        '
        'cbxRealizado
        '
        Me.cbxRealizado.CampoWhere = Nothing
        Me.cbxRealizado.CargarUnaSolaVez = False
        Me.cbxRealizado.DataDisplayMember = Nothing
        Me.cbxRealizado.DataFilter = Nothing
        Me.cbxRealizado.DataOrderBy = Nothing
        Me.cbxRealizado.DataSource = Nothing
        Me.cbxRealizado.DataValueMember = Nothing
        Me.cbxRealizado.dtSeleccionado = Nothing
        Me.cbxRealizado.FormABM = Nothing
        Me.cbxRealizado.Indicaciones = Nothing
        Me.cbxRealizado.Location = New System.Drawing.Point(12, 136)
        Me.cbxRealizado.Name = "cbxRealizado"
        Me.cbxRealizado.SeleccionMultiple = False
        Me.cbxRealizado.SeleccionObligatoria = False
        Me.cbxRealizado.Size = New System.Drawing.Size(113, 21)
        Me.cbxRealizado.SoloLectura = False
        Me.cbxRealizado.TabIndex = 34
        Me.cbxRealizado.Texto = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 118)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Llamadas"
        '
        'cbxEnForma
        '
        Me.cbxEnForma.CampoWhere = Nothing
        Me.cbxEnForma.CargarUnaSolaVez = False
        Me.cbxEnForma.DataDisplayMember = Nothing
        Me.cbxEnForma.DataFilter = Nothing
        Me.cbxEnForma.DataOrderBy = Nothing
        Me.cbxEnForma.DataSource = Nothing
        Me.cbxEnForma.DataValueMember = Nothing
        Me.cbxEnForma.dtSeleccionado = Nothing
        Me.cbxEnForma.FormABM = Nothing
        Me.cbxEnForma.Indicaciones = Nothing
        Me.cbxEnForma.Location = New System.Drawing.Point(131, 178)
        Me.cbxEnForma.Name = "cbxEnForma"
        Me.cbxEnForma.SeleccionMultiple = False
        Me.cbxEnForma.SeleccionObligatoria = False
        Me.cbxEnForma.Size = New System.Drawing.Size(45, 21)
        Me.cbxEnForma.SoloLectura = False
        Me.cbxEnForma.TabIndex = 32
        Me.cbxEnForma.Texto = ""
        '
        'cbxOrdenadoPor
        '
        Me.cbxOrdenadoPor.CampoWhere = Nothing
        Me.cbxOrdenadoPor.CargarUnaSolaVez = False
        Me.cbxOrdenadoPor.DataDisplayMember = Nothing
        Me.cbxOrdenadoPor.DataFilter = Nothing
        Me.cbxOrdenadoPor.DataOrderBy = Nothing
        Me.cbxOrdenadoPor.DataSource = Nothing
        Me.cbxOrdenadoPor.DataValueMember = Nothing
        Me.cbxOrdenadoPor.dtSeleccionado = Nothing
        Me.cbxOrdenadoPor.FormABM = Nothing
        Me.cbxOrdenadoPor.Indicaciones = Nothing
        Me.cbxOrdenadoPor.Location = New System.Drawing.Point(12, 178)
        Me.cbxOrdenadoPor.Name = "cbxOrdenadoPor"
        Me.cbxOrdenadoPor.SeleccionMultiple = False
        Me.cbxOrdenadoPor.SeleccionObligatoria = False
        Me.cbxOrdenadoPor.Size = New System.Drawing.Size(113, 21)
        Me.cbxOrdenadoPor.SoloLectura = False
        Me.cbxOrdenadoPor.TabIndex = 31
        Me.cbxOrdenadoPor.Texto = ""
        '
        'lblOrdenado
        '
        Me.lblOrdenado.AutoSize = True
        Me.lblOrdenado.Location = New System.Drawing.Point(12, 160)
        Me.lblOrdenado.Name = "lblOrdenado"
        Me.lblOrdenado.Size = New System.Drawing.Size(57, 13)
        Me.lblOrdenado.TabIndex = 30
        Me.lblOrdenado.Text = "Ordenado:"
        '
        'txtHastaVolverLlamar
        '
        Me.txtHastaVolverLlamar.Color = System.Drawing.Color.Empty
        Me.txtHastaVolverLlamar.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHastaVolverLlamar.Location = New System.Drawing.Point(92, 93)
        Me.txtHastaVolverLlamar.Name = "txtHastaVolverLlamar"
        Me.txtHastaVolverLlamar.PermitirNulo = False
        Me.txtHastaVolverLlamar.Size = New System.Drawing.Size(84, 20)
        Me.txtHastaVolverLlamar.SoloLectura = False
        Me.txtHastaVolverLlamar.TabIndex = 2
        '
        'txtDesdeVolverLlamar
        '
        Me.txtDesdeVolverLlamar.Color = System.Drawing.Color.Empty
        Me.txtDesdeVolverLlamar.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesdeVolverLlamar.Location = New System.Drawing.Point(12, 93)
        Me.txtDesdeVolverLlamar.Name = "txtDesdeVolverLlamar"
        Me.txtDesdeVolverLlamar.PermitirNulo = False
        Me.txtDesdeVolverLlamar.Size = New System.Drawing.Size(74, 20)
        Me.txtDesdeVolverLlamar.SoloLectura = False
        Me.txtDesdeVolverLlamar.TabIndex = 1
        '
        'gbxFiltro
        '
        Me.gbxFiltro.Controls.Add(Me.txtCliente)
        Me.gbxFiltro.Controls.Add(Me.chkVendedor)
        Me.gbxFiltro.Controls.Add(Me.cbxVendedor)
        Me.gbxFiltro.Controls.Add(Me.chkCliente)
        Me.gbxFiltro.Location = New System.Drawing.Point(12, 12)
        Me.gbxFiltro.Name = "gbxFiltro"
        Me.gbxFiltro.Size = New System.Drawing.Size(453, 128)
        Me.gbxFiltro.TabIndex = 26
        Me.gbxFiltro.TabStop = False
        Me.gbxFiltro.Text = "Filtros"
        '
        'txtCliente
        '
        Me.txtCliente.AlturaMaxima = 115
        Me.txtCliente.ClienteVario = False
        Me.txtCliente.Consulta = Nothing
        Me.txtCliente.Enabled = False
        Me.txtCliente.frm = Nothing
        Me.txtCliente.Location = New System.Drawing.Point(9, 81)
        Me.txtCliente.MostrarSucursal = False
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Registro = Nothing
        Me.txtCliente.Seleccionado = False
        Me.txtCliente.Size = New System.Drawing.Size(423, 26)
        Me.txtCliente.SoloLectura = False
        Me.txtCliente.Sucursal = Nothing
        Me.txtCliente.SucursalSeleccionada = False
        Me.txtCliente.TabIndex = 19
        '
        'chkVendedor
        '
        Me.chkVendedor.BackColor = System.Drawing.Color.Transparent
        Me.chkVendedor.Color = System.Drawing.Color.Empty
        Me.chkVendedor.Location = New System.Drawing.Point(10, 32)
        Me.chkVendedor.Name = "chkVendedor"
        Me.chkVendedor.Size = New System.Drawing.Size(71, 21)
        Me.chkVendedor.SoloLectura = False
        Me.chkVendedor.TabIndex = 4
        Me.chkVendedor.Texto = "Vendedor:"
        Me.chkVendedor.Valor = False
        '
        'cbxVendedor
        '
        Me.cbxVendedor.CampoWhere = "IDVendedor"
        Me.cbxVendedor.CargarUnaSolaVez = False
        Me.cbxVendedor.DataDisplayMember = "Nombres"
        Me.cbxVendedor.DataFilter = Nothing
        Me.cbxVendedor.DataOrderBy = "Nombres"
        Me.cbxVendedor.DataSource = "Vendedor"
        Me.cbxVendedor.DataValueMember = "ID"
        Me.cbxVendedor.dtSeleccionado = Nothing
        Me.cbxVendedor.Enabled = False
        Me.cbxVendedor.FormABM = Nothing
        Me.cbxVendedor.Indicaciones = Nothing
        Me.cbxVendedor.Location = New System.Drawing.Point(92, 30)
        Me.cbxVendedor.Name = "cbxVendedor"
        Me.cbxVendedor.SeleccionMultiple = False
        Me.cbxVendedor.SeleccionObligatoria = False
        Me.cbxVendedor.Size = New System.Drawing.Size(341, 21)
        Me.cbxVendedor.SoloLectura = False
        Me.cbxVendedor.TabIndex = 5
        Me.cbxVendedor.Texto = ""
        '
        'chkCliente
        '
        Me.chkCliente.BackColor = System.Drawing.Color.Transparent
        Me.chkCliente.Color = System.Drawing.Color.Empty
        Me.chkCliente.Location = New System.Drawing.Point(9, 60)
        Me.chkCliente.Name = "chkCliente"
        Me.chkCliente.Size = New System.Drawing.Size(77, 21)
        Me.chkCliente.SoloLectura = False
        Me.chkCliente.TabIndex = 18
        Me.chkCliente.Texto = "Cliente:"
        Me.chkCliente.Valor = False
        '
        'txtHasta
        '
        Me.txtHasta.Color = System.Drawing.Color.Empty
        Me.txtHasta.Fecha = New Date(2013, 5, 22, 15, 8, 34, 924)
        Me.txtHasta.Location = New System.Drawing.Point(92, 36)
        Me.txtHasta.Name = "txtHasta"
        Me.txtHasta.PermitirNulo = False
        Me.txtHasta.Size = New System.Drawing.Size(84, 20)
        Me.txtHasta.SoloLectura = False
        Me.txtHasta.TabIndex = 40
        '
        'txtDesde
        '
        Me.txtDesde.Color = System.Drawing.Color.Empty
        Me.txtDesde.Fecha = New Date(2013, 5, 22, 15, 8, 34, 933)
        Me.txtDesde.Location = New System.Drawing.Point(12, 36)
        Me.txtDesde.Name = "txtDesde"
        Me.txtDesde.PermitirNulo = False
        Me.txtDesde.Size = New System.Drawing.Size(74, 20)
        Me.txtDesde.SoloLectura = False
        Me.txtDesde.TabIndex = 39
        '
        'chkFechaVolverLlamar
        '
        Me.chkFechaVolverLlamar.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaVolverLlamar.Color = System.Drawing.Color.Empty
        Me.chkFechaVolverLlamar.Location = New System.Drawing.Point(12, 70)
        Me.chkFechaVolverLlamar.Name = "chkFechaVolverLlamar"
        Me.chkFechaVolverLlamar.Size = New System.Drawing.Size(130, 17)
        Me.chkFechaVolverLlamar.SoloLectura = False
        Me.chkFechaVolverLlamar.TabIndex = 41
        Me.chkFechaVolverLlamar.Texto = "Fecha a volver a llamar:"
        Me.chkFechaVolverLlamar.Valor = False
        '
        'chkFechaLlamada
        '
        Me.chkFechaLlamada.BackColor = System.Drawing.Color.Transparent
        Me.chkFechaLlamada.Color = System.Drawing.Color.Empty
        Me.chkFechaLlamada.Location = New System.Drawing.Point(12, 13)
        Me.chkFechaLlamada.Name = "chkFechaLlamada"
        Me.chkFechaLlamada.Size = New System.Drawing.Size(130, 17)
        Me.chkFechaLlamada.SoloLectura = False
        Me.chkFechaLlamada.TabIndex = 42
        Me.chkFechaLlamada.Texto = "Fecha llamada:"
        Me.chkFechaLlamada.Valor = False
        '
        'frmPlanDeLlamadas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(673, 267)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbxFiltro)
        Me.Name = "frmPlanDeLlamadas"
        Me.Tag = "frmPlanDeLlamadas"
        Me.Text = "frmPlanDeLlamadas"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxFiltro.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbxEnForma As ERP.ocxCBX
    Friend WithEvents cbxOrdenadoPor As ERP.ocxCBX
    Friend WithEvents lblOrdenado As System.Windows.Forms.Label
    Friend WithEvents txtHastaVolverLlamar As ERP.ocxTXTDate
    Friend WithEvents txtDesdeVolverLlamar As ERP.ocxTXTDate
    Friend WithEvents gbxFiltro As System.Windows.Forms.GroupBox
    Friend WithEvents txtCliente As ERP.ocxTXTCliente
    Friend WithEvents chkVendedor As ERP.ocxCHK
    Friend WithEvents cbxVendedor As ERP.ocxCBX
    Friend WithEvents chkCliente As ERP.ocxCHK
    Friend WithEvents cbxRealizado As ERP.ocxCBX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtHasta As ERP.ocxTXTDate
    Friend WithEvents txtDesde As ERP.ocxTXTDate
    Friend WithEvents chkFechaLlamada As ERP.ocxCHK
    Friend WithEvents chkFechaVolverLlamar As ERP.ocxCHK
End Class

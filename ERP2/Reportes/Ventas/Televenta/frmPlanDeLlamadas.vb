﻿Imports ERP.Reporte
Public Class frmPlanDeLlamadas
    Dim CReporte As New CTeleventa
    Dim CSistema As New CSistema
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio

    'FUNCIONES
    Sub Inicializar()

        CargarInformacion()
        txtDesdeVolverLlamar.Hoy()
        txtHastaVolverLlamar.Hoy()

    End Sub

    Sub CargarInformacion()

        txtCliente.Conectar()

        'Llamada Realizada
        cbxRealizado.cbx.Items.Add("Todos")
        cbxRealizado.cbx.Items.Add("Realizado")
        cbxRealizado.cbx.Items.Add("No Realizado")

        'En Forma
        cbxEnForma.cbx.Items.Add("ASC")
        cbxEnForma.cbx.Items.Add("DESC")
        cbxEnForma.cbx.DropDownStyle = ComboBoxStyle.DropDownList
        cbxEnForma.cbx.SelectedIndex = 0

        'Ordenado por
        cbxOrdenadoPor.cbx.Items.Add("Fecha")
        cbxOrdenadoPor.cbx.Items.Add("Cliente")
        cbxOrdenadoPor.cbx.SelectedIndex = 0
        cbxOrdenadoPor.cbx.DropDownStyle = ComboBoxStyle.DropDownList

        For Each ctr As Object In gbxFiltro.Controls

            ''Menos los controles que no quiero, Ejemplo Producto
            'If ctr.name = "cbxProducto" Then
            '    GoTo siguiente
            'End If
            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.SeleccionMultiple = True
            End If
siguiente:

        Next

    End Sub

    Sub Listar()

        Dim Where As String = " where 1 = 1 "
        Dim OrderBy As String = ""
        Dim Titulo As String = ""
        Dim Subtitulo As String = ""

        Dim frm As New frmReporte
        frm.MdiParent = My.Application.ApplicationContext.MainForm

        'Filtra por fecha
        If chkFechaLlamada.chk.Checked Then
            Where = Where & "And cast(Fecha as date) between '" & txtDesde.GetValueString & "' and '" & txtHasta.GetValueString & "'  "
        End If

        If chkFechaVolverLlamar.chk.Checked Then
            Where = Where & " And Cast(LlamarFecha as date) between '" & CSistema.FormatoFechaBaseDatos(txtDesdeVolverLlamar.GetValue, True, False) & "' and '" & CSistema.FormatoFechaBaseDatos(txtHastaVolverLlamar.GetValue, True, False) & "'  "
        End If

        'Filtrar por Cliente
        If chkCliente.chk.Checked = True Then
            If txtCliente.Seleccionado = True Then
                Where = Where + " And IDCliente = " & txtCliente.Registro("ID").ToString
            Else

            End If
        End If

        'Establecemos los filtros
        For Each ctr As Object In gbxFiltro.Controls

            'Menos los controles que no quiero, Ejemplo Producto
            If ctr.name = "cbxProducto" Then
                GoTo siguiente
            End If

            If ctr.GetType.Name.ToUpper = "OCXCBX" Then
                ctr.EstablecerCondicion(Where)
            End If
siguiente:

        Next

        'Establecemos el Orden
        If cbxRealizado.cbx.Text <> "" Then
            Select Case cbxRealizado.cbx.Text
                Case "Realizado"
                    Where = Where + " And Realizado = 1"
                Case "No Realizado"
                    Where = Where + " And Realizado = 0"
            End Select
        End If

        'Establecemos el Orden
        If cbxOrdenadoPor.cbx.Text <> "" Then
            OrderBy = " Order By " & cbxOrdenadoPor.cbx.Text
        End If

        If cbxEnForma.cbx.Text <> "" Then
            OrderBy = OrderBy & " " & cbxEnForma.cbx.Text
        End If


        ArmarSubTitulo(Subtitulo)
        Dim ParamOrder As String = ""
        If cbxOrdenadoPor.cbx.SelectedIndex = 0 Then
            ParamOrder = "F"
        Else
            ParamOrder = "C"
        End If

        CReporte.PlanDeLlamada(frm, Where, OrderBy, vgUsuarioIdentificador, txtDesdeVolverLlamar.txt.Text, txtHastaVolverLlamar.txt.Text, ParamOrder)
    End Sub

    Private Sub btnInforme_Click(sender As System.Object, e As System.EventArgs) Handles btnInforme.Click
        Listar()
    End Sub

    Sub ArmarSubTitulo(ByRef Subtitulo As String)
        Subtitulo = "Fecha: " & txtDesdeVolverLlamar.GetValue & " - " & txtHastaVolverLlamar.GetValue & " "

    End Sub

    Private Sub frmPlanDeLlamadas_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Inicializar()
    End Sub

    Private Sub chkVendedor_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkVendedor.PropertyChanged
        cbxVendedor.Enabled = value
    End Sub

    Private Sub chkCliente_PropertyChanged(sender As System.Object, e As System.EventArgs, value As System.Boolean) Handles chkCliente.PropertyChanged
        txtCliente.Enabled = value
    End Sub
End Class
﻿Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Windows.Forms


Module MFuncionesGlobales

    'CLASES
    Dim CError As New CError
    Dim CData As New CData
    Dim CArchivoInicio As New CArchivoInicio
    Dim CSistema As New CSistema
    Dim CSeguridad As New CSeguridad
    Dim CDecode As New ERP2ED.CDecode

    Private Declare Auto Function SetProcessWorkingSetSize Lib "kernel32.dll" (ByVal procHandle As IntPtr, ByVal min As Int32, ByVal max As Int32) As Boolean

    Public Sub LiberarMemoria()

        Try

            Dim memoria As Process
            memoria = Process.GetCurrentProcess()
            SetProcessWorkingSetSize(memoria.Handle, -1, -1)

        Catch ex As Exception

        End Try

    End Sub

    Sub FGIniciarSistema()
        Try


            If CultureInfo.CurrentCulture.Name <> "es-PY" Then
                MessageBox.Show("Revise su configuracion regional. Debe ser Español-Paraguay", "Configuracion Regional.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End
            End If

            If CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator <> "," Then
                MessageBox.Show("Revise su configuracion regional. Separador de decimal debe ser Coma", "Configuracion Regional.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End
            End If

            If CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator <> "." Then
                MessageBox.Show("Revise su configuracion regional. Separador de miles debe ser Punto", "Configuracion Regional.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End
            End If

            Dim CLicencia As New CLicencia
            CError = New CError
            CData = New CData
            CArchivoInicio = New CArchivoInicio
            CSistema = New CSistema
            CSeguridad = New CSeguridad
            CDecode = New ERP2ED.CDecode

            'Cargar Archivo INI
            FGCargarInformacionINI()

            'Actulizacion
            FGActualizacion()

            'Buscamos la licencia
            If CLicencia.Existe() = False Then
                frmLicenciaPath.ShowDialog()
            End If

            If CLicencia.Existe() = False Then
                MessageBox.Show("El sistema no encuentra la licencia!!! Favor intentelo de nuevo.", "Licencia no encontrada.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End
            End If



            'Cargar informacion de la Licencia
            CLicencia.InfoLicencia()

            'Establecemos primeramente la conexion al servidor

            If FGEstablecerConexion() = False Then

                'Formulario de Servidor
                Dim frm As New Form
                Dim ocx As New ocxConexionBaseDeDatos
                frm.Controls.Add(ocx)
                ocx.Dock = DockStyle.Fill
                ocx.Inicializar()
                frm.Text = "Configuracion de Datos"
                frm.StartPosition = FormStartPosition.CenterScreen
                frm.Size = New Size(400, 300)
                frm.ShowDialog()

                If ocx.Procesado = True Then
                    If FGEstablecerConexion() = False Then
                        End
                    End If
                End If

            End If

            'Validar Licencia
            Dim CInfoHardware As New CInfoHardware
            If CLicencia.TerminalHabilitada(CInfoHardware) = False Then
                MessageBox.Show("Esta terminal no esta habilitada en la licencia! Debe agregar con la cuenta de la licencia.", "Terminal", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Dim frm As New frmAccesoTerminal
                frm.Text = "Agregar terminal"
                frm.StartPosition = FormStartPosition.CenterScreen
                FGEstiloFormulario(frm)
                frm.ShowDialog()
            End If

            'Volvemos a controlar la terminal
            If CLicencia.TerminalHabilitada(CInfoHardware) = False Then
                MessageBox.Show("Debe habilitar esta terminal en la licencia!", "Acceso denegado", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                End
            End If

            'Identificar la Terminal en la Base de Datos
            If CInfoHardware.TerminalHabilitadaBD = False Then
                MessageBox.Show("Esta terminal no esta habilitada en la Base de Datos! Agreguelo para continuar", "Terminal", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Dim frm As New frmAccesoTerminal
                frm.Text = "Agregar terminal"
                frm.StartPosition = FormStartPosition.CenterScreen
                FGEstiloFormulario(frm)
                frm.ShowDialog()

                If frm.Procesado = False Then
                    End
                End If

            End If

            'Verificamos que la licencia no este caducada
            If CLicencia.FechaCaducada() = False Then
                End
            End If

            ''Importar Configuraciones de BD
            'FGConfiguraciones()

            'Inicializamos el sistema
            FGInicializarSistema()

            FGEstiloFormulario(frmLogin)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub FGIniciarSistema2()

        Dim CLicencia As New CLicencia

        'Cargar Archivo INI
        FGCargarInformacionINI()

        'Buscamos la licencia
        If CLicencia.Existe() = False Then
            frmLicenciaPath.ShowDialog()
        End If

        If CLicencia.Existe() = False Then
            MessageBox.Show("El sistema no encuentra la licencia!!! Favor intentelo de nuevo.", "Licencia no encontrada.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End
        End If

        'Cargar informacion de la Licencia
        CLicencia.InfoLicencia2()

        'Validar el Hardware
        Dim CInfoHardware As New CInfoHardware

        'Identificar la Terminal en la Base de Datos
        If CInfoHardware.TerminalHabilitadaBD = False Then
            MessageBox.Show("Esta terminal no esta habilitada en la Base de Datos! Agreguelo para continuar", "Terminal", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Dim frm As New frmAccesoTerminal
            frm.Text = "Agregar terminal"
            frm.StartPosition = FormStartPosition.CenterScreen
            FGEstiloFormulario(frm)
            frm.ShowDialog()

            If frm.Procesado = False Then
                End
            End If

        End If

        'Verificamos que la licencia no este caducada
        If CLicencia.FechaCaducada2() = False Then
            End
        End If

        'Importar Configuraciones de BD
        FGConfiguraciones()

        'Inicializamos el sistema
        FGInicializarSistema()

        FGEstiloFormulario(frmLogin)

    End Sub

    'CONEXION
    ''' <summary>
    ''' Devuelve Verdadero si establecio conexion con el SERVIDOR
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function FGEstablecerConexion() As Boolean

        FGEstablecerConexion = False

        'Verificamos que la cadena de conexion se haya cargado completamente
        If FGEStablecerCadenaConexion() = False Then
            MessageBox.Show("La cadena de conexion no esta configurada correctamente! No se podra establecer conexion con el servidor hasta que se solucione el problema.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Function
        End If
        Dim vConexion As String
        If VGCadenaConexion.ToString.Contains("Timeout") Then
            vConexion = VGCadenaConexion
        Else
            vConexion = VGCadenaConexion & ";Connection Timeout=5;"
        End If

        Dim conn As New SqlConnection(vConexion)
        Dim previousConnectionState As ConnectionState = conn.State
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
                FGEstablecerConexion = True
            End If
        Catch ex As Exception
            CError.CargarError(ex, "MFuncionesGlobales", "FGEstablecerConexion", "", "")
            Return False
        Finally
            If previousConnectionState = ConnectionState.Closed Then
                conn.Close()
            End If
        End Try

    End Function

    ''' <summary>
    ''' Configura la cadena de conexion, si uno de los parametros no esta cargado devuelve false
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function FGEStablecerCadenaConexion() As Boolean

        FGEStablecerCadenaConexion = False

        'Controlar que exista el nombre del servidor
        If Trim(MVariablesGlobales.vgNombreServidor) = "" Then
            Return False
        End If

        'Controlar que exista el nombre de la Base de Datos
        If Trim(MVariablesGlobales.vgNombreBaseDatos) = "" Then
            Return False
        End If

        'Controlar que exista el nombre del Usuario
        If Trim(MVariablesGlobales.vgNombreUsuarioBD) = "" Then
            Return False
        End If

        VGCadenaConexion = "Data Source=" & MVariablesGlobales.vgNombreServidor & ";Initial Catalog=" & MVariablesGlobales.vgNombreBaseDatos & ";User Id=" & MVariablesGlobales.vgNombreUsuarioBD & ";Password=" & MVariablesGlobales.vgPasswordBD & ""
        Return True

    End Function

    Public Sub FGCargarInformacionINI()

        'Creamos la carpeta de configuraciones
        If My.Computer.FileSystem.DirectoryExists(VGCarpetaAplicacion & "\" & My.Application.Info.ProductName) = False Then
            Try
                My.Computer.FileSystem.CreateDirectory(VGCarpetaAplicacion & "\" & My.Application.Info.ProductName)
                'Volver a comprobar
                If My.Computer.FileSystem.DirectoryExists(VGCarpetaAplicacion & "\" & My.Application.Info.ProductName) = False Then
                    GoTo EnError
                End If
            Catch ex As Exception
                GoTo EnError
            End Try
        End If


        Try

            vgNombreServidor = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "SERVIDOR", "")
            vgNombreBaseDatos = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "BASE DE DATOS", "")
            vgNombreUsuarioBD = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "USUARIO", "")
            CDecode.InClearText = CArchivoInicio.IniGet(VGArchivoINI, "BASE DE DATOS", "PASSWORD", "")
            CDecode.Decrypt()
            vgPasswordBD = CDecode.CryptedText
            'vgPasswordBD = "Abc1234"

            'INICIO
            vgModoPedido = CArchivoInicio.IniGet(VGArchivoINI, "INICIO", "MODO PEDIDO", "0")

            'APARIENCIA
            vgColorFocus = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR FOCO", Color.AliceBlue.ToArgb.ToString)
            vgColorSoloLectura = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR SOLO LECTURA", Color.WhiteSmoke.ToArgb.ToString)
            vgColorSeleccionado = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR SELECCION LISTA", Color.LightGreen.ToArgb.ToString)
            vgColorFormulario = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE FONDO", Color.FromKnownColor(KnownColor.Control).ToArgb)
            vgColorFormulario2 = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE FONDO 2", Color.FromKnownColor(KnownColor.Control).ToArgb)
            vgColorTextos = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE TEXTO", Color.FromKnownColor(KnownColor.ControlText).ToArgb)
            vgColorBoton = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE BOTON", Color.FromKnownColor(KnownColor.Control).ToArgb)
            vgColorTextosBoton = CArchivoInicio.IniGet(VGArchivoINI, "FORMULARIO", "COLOR DE TEXTO BOTON", Color.FromKnownColor(KnownColor.ControlText).ToArgb)

            'Facturacion
            vgImpresionFacturaPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA PATH", "")
            vgImpresionFacturaConversionPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA CONVERSION PATH", "")

            'Nota Credito/Debito
            vgImpresionNotaCreditoPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "NOTACREDITO PATH", "")
            vgImpresionNotaDebitoPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "NOTADEBITO PATH", "")

            'Retencion 
            vgImpresionRetencionPath = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "RETENCION PATH", "")

            'Impresora
            vgImpresionFacturaImpresora = CArchivoInicio.IniGet(VGArchivoINI, "CONFIGURACION IMPRESORA", "FACTURA IMPRESORA", "")

            'Informes
            VGCarpetaReporteTemporal = CArchivoInicio.IniGet(VGArchivoINI, "INFORMES", "PATH", "\\192.168.0.55\publicaciones\sain\reportes\")

            'Actualizacion
            vgActualizacionPath = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "PATH", "ftp://192.168.244.100")
            vgActualizacionUsuario = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "USUARIO", "ERSA")
            CDecode.InClearText = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "PASSWORD", "ˆžôµ]")
            vgActualizacionVersion = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "VERSIONSISTEMA", "1.0.0.0")
            CDecode.Decrypt()
            vgActualizacionPassword = CDecode.CryptedText

        Catch ex As Exception

        End Try

EnError:


    End Sub

    Public Function FGIniInfoHardware() As Boolean

        FGIniInfoHardware = False
        Dim dt As DataTable = CSistema.ExecuteToDataTable("Exec SpInicializarTerminal @Terminal='" & CSeguridad.NAME & "', @CodigoActivacion='" & CSeguridad.CODIGO & "', @Version='" & VGSoftwareVersion & "'").Copy

        If dt Is Nothing Then
            MessageBox.Show("No se puede tener acceso a la identificacion de la terminal!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No se puede tener acceso a la identificacion de la terminal!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        Dim oRow As DataRow = dt.Rows(0)

        'Verificar la informacion recibida
        If oRow("Procesado") = False Then
            MessageBox.Show(oRow("Mensaje").ToString, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If oRow("Procesado") = True Then

            'Verificar si la terminal se asocio a algun deposito
            If oRow("IDSucursal").ToString = "" Or oRow("IDDeposito").ToString = "" Then

                'Formulario de asociacion

            End If

        End If

        vgIDSucursal = oRow("IDSucursal").ToString
        vgIDDeposito = oRow("IDDeposito").ToString
        vgIDTerminal = oRow("ID").ToString
        vgSucursal = oRow("Sucursal").ToString
        vgSucursalCodigo = oRow("CodigoSucursal").ToString
        vgDeposito = oRow("Deposito").ToString
        vgTerminal = oRow("Descripcion").ToString


        Return True

    End Function

    Public Sub FGConfiguraciones()

        Dim CSistema As New CSistema
        Try
            vgConfiguraciones = CSistema.ExecuteToDataTable("Select TOP 1 * From Configuraciones Where IDSucursal=" & vgIDSucursal, "", 15)(0)

            'Validar
            If vgConfiguraciones Is Nothing Then

                Dim SQL As String = "Insert Into Configuraciones(IDSucursal, FormatoFecha, PropietarioBD) Values(" & vgIDSucursal & ", 'dd-MM-yyyy','dbo') "
                CSistema.ExecuteNonQuery(SQL)

            End If

            'Fecha de Facturacion

            VGFechaFacturacion = CSistema.ExecuteScalar("Select TOP 1 Fecha From FechaFacturacion Where IDSucursal=" & vgIDSucursal)
            vgConfiguraciones("VentaFechaFacturacion") = VGFechaFacturacion
        Catch ex As Exception
            VGFechaFacturacion = CSistema.ExecuteScalar("Select GetDate()")
        End Try

    End Sub

    ''' <summary>
    ''' Metodo de Inicio de Sistema, Carga todas las configuraciones en memoria.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub FGInicializarSistema()

        'Cargamos las Configuraciones
        FGConfiguraciones()

    End Sub

    Function FGConfigurarPermiso(ByVal frm As Form) As Boolean

        If vgIDPerfil = 1 Then
            Return True
        End If

        Dim ControlesUsuario() As String = {"ERP.ocxSucursalesClientes", "ERP.ocxListaPrecioProducto", "ERP.ocxProductoUbicacion"}

        'Obtenemos los permisos
        Dim dt As DataTable = CData.GetTable("VAccesoPerfil").Copy
        dt = CData.FiltrarDataTable(dt, "IDPerfil='" & vgIDPerfil & "' And Tag='" & frm.GetType.Name & "' ").Copy

        'si no tiene registro, acceso denegado
        If dt Is Nothing Then
            MessageBox.Show("No tiene suficientes privilegios para el acceso!", "Acceso denegado", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        If dt.Rows.Count = 0 Then
            MessageBox.Show("No tiene suficientes privilegios para el acceso!", "Acceso denegado", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return False
        End If

        For Each oRow As DataRow In dt.Rows

            Dim Visualizar As Boolean = oRow("Visualizar")
            Dim Agregar As Boolean = oRow("Agregar")
            Dim Modificar As Boolean = oRow("Modificar")
            Dim Eliminar As Boolean = oRow("Eliminar")
            Dim Anular As Boolean = oRow("Anular")
            Dim Imprimir As Boolean = oRow("Imprimir")
            Dim Asiento As Boolean = oRow("Asiento")

            If Visualizar = False Then
                MessageBox.Show("No tiene suficientes privilegios para el acceso!", "Acceso denegado", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return False
            End If

            For Each ctr As Control In frm.Controls

                If ctr.Controls.Count > 0 Then

                    'No entrar en controles del usuario
                    If Array.IndexOf(ControlesUsuario, ctr.GetType.ToString) < 0 Then
                        FGConfigurarPermiso(oRow, ctr)
                    End If

                Else
                    Select Case ctr.Name.ToUpper
                        Case "BTNACEPTAR", "BTNNUEVO"
                            ctr.Visible = Agregar
                        Case "BTNMODIFICAR", "BTNEDITAR"
                            ctr.Visible = Modificar
                        Case "BTNELIMINAR"
                            ctr.Visible = Eliminar
                        Case "BTNANULAR"
                            ctr.Visible = Anular
                        Case "BTNIMPRIMIR"
                            ctr.Visible = Imprimir
                        Case "BTNASIENTO"
                            ctr.Visible = Asiento
                        Case Else

                    End Select
                End If

            Next
        Next

        Return True

    End Function

    Public Sub FGConfigurarMenu(ByVal Menu As MenuStrip)

        'Si es administrador, salir
        If vgIDPerfil = 1 Then
            Exit Sub
        End If

        Menu.Cursor = Cursors.Hand

        Dim dt As DataTable = CData.GetTable("VAccesoPerfil").Copy

        For Each menu1 As ToolStripMenuItem In Menu.Items

            Dim Consulta As String = " IDPerfil = " & vgIDPerfil & " And NombreControl = '" & menu1.Name & "' "
            If menu1.Name = "NotaDeCreditoToolStripMenuItem" Then
                Dim a As Integer
                a = 1
            End If

            If dt.Select(Consulta).Count = 0 Then
                FGFormatoMenuOculto(menu1)
                GoTo siguiente
            End If

            'Verificar si es visible
            Dim oRow As DataRow = dt.Select(Consulta)(0)

            If oRow("Visualizar") = False Then
                FGFormatoMenuOculto(menu1)
                GoTo siguiente
            End If

            FGFormatoMenuVisible(menu1)

            If menu1.DropDownItems.Count > 0 Then
                FGConfigurarMenu(menu1)
            End If

siguiente:

        Next

        'Quitar los separadores que estan juntos
        FGQuitarSeparadoresJuntos(Menu)

    End Sub

    Private Sub FGConfigurarMenu(ByVal menu1 As ToolStripMenuItem)

        Dim dt As DataTable = CData.GetTable("VAccesoPerfil").Copy

        For i As Integer = 0 To menu1.DropDownItems.Count - 1

            Dim Tipo As String = menu1.DropDownItems.Item(i).GetType.ToString()

            If Tipo <> "System.Windows.Forms.ToolStripMenuItem" Then
                menu1.DropDownItems.Item(i).ForeColor = Color.FromArgb(vgColorBackColor)
                GoTo siguiente
            End If

            Dim Consulta As String = " IDPerfil = " & vgIDPerfil & " And NombreControl = '" & menu1.DropDownItems.Item(i).Name & "' "

            If dt.Select(Consulta).Count = 0 Then
                FGFormatoMenuOculto(menu1.DropDownItems.Item(i))
                GoTo siguiente
            End If

            'Verificar si es visible
            Dim oRow As DataRow = dt.Select(Consulta)(0)

            If oRow("Visualizar") = False Then
                FGFormatoMenuOculto(menu1.DropDownItems.Item(i))
                GoTo siguiente
            End If

            'menu1.BackColor = Color.WhiteSmoke

            FGFormatoMenuVisible(menu1.DropDownItems.Item(i))

            Dim menu2 As ToolStripMenuItem = menu1.DropDownItems.Item(i)

            If menu2.DropDownItems.Count > 0 Then
                FGConfigurarMenu(menu2)
            End If
siguiente:

        Next

    End Sub

    Private Sub FGQuitarSeparadoresJuntos(ByVal Menu As MenuStrip)

        For Each menu1 As ToolStripMenuItem In Menu.Items

            If menu1.Visible = False Then
                GoTo siguiente
            End If

            If menu1.DropDownItems.Count > 0 Then
                FGQuitarSeparadoresJuntos(menu1)
            End If

siguiente:

        Next

    End Sub

    Private Sub FGQuitarSeparadoresJuntos(ByVal menu1 As ToolStripMenuItem)

        For i As Integer = 0 To menu1.DropDownItems.Count - 1

            Dim Tipo As String = menu1.DropDownItems.Item(i).GetType.ToString()

            If Tipo = "System.Windows.Forms.ToolStripSeparator" Then

                'Si I = 0, esconder
                If i = 0 Then
                    menu1.Visible = False
                    GoTo siguiente
                End If

                'Verificamos si el anterior en del mismo tipo
                Dim TipoAnterior As String = FGBuscarTipoMenuAnterior(menu1, i)

                If Tipo = TipoAnterior Then
                    menu1.DropDownItems.Item(i).Visible = False
                End If

            Else
                Dim menu2 As ToolStripMenuItem = menu1.DropDownItems.Item(i)

                If menu2.DropDownItems.Count > 0 Then
                    FGQuitarSeparadoresJuntos(menu2)
                End If

            End If

siguiente:

        Next

    End Sub

    Private Sub FGFormatoMenuVisible(ByVal menu1 As ToolStripMenuItem)

        'menu1.BackColor = Color.FromArgb(vgColorBackColor)
        'menu1.ForeColor = Color.FromArgb(vgColorTextos)
        'menu1.Font = New Font(menu1.Font.FontFamily, 8, FontStyle.Bold)
        'menu1.Enabled = True
        menu1.Visible = True

    End Sub

    Private Sub FGFormatoMenuOculto(ByVal menu1 As ToolStripMenuItem)

        'menu1.BackColor = Color.FromArgb(vgColorBackColor)
        'menu1.Enabled = False
        menu1.Visible = False
        'menu1.Font = New Font(menu1.Font.FontFamily, 8, FontStyle.Regular)

    End Sub

    Private Function FGBuscarTipoMenuAnterior(ByVal menu1 As ToolStripMenuItem, ByVal I As Integer) As String
        FGBuscarTipoMenuAnterior = ""
        Dim Index As Integer

        For Index = 0 To menu1.DropDownItems.Count - 1

            If Index = I Then
                Exit For
            End If

            'If menu1.DropDownItems.Item(Index).Visible = True Then
            Dim Tipo As String = menu1.DropDownItems.Item(Index).GetType.ToString()
            If Tipo = "System.Windows.Forms.ToolStripSeparator" Then
                FGBuscarTipoMenuAnterior = Tipo
            End If
            'End If

        Next

    End Function

    Private Sub FGConfigurarPermiso(ByVal oRow As DataRow, ByVal ctrls As Control)

        Dim ControlesUsuario() As String = {"ERP.ocxSucursalesClientes", "ERP.ocxListaPrecioProducto", "ERP.ocxProductoUbicacion"}

        Dim Visualizar As Boolean = oRow("Visualizar")
        Dim Agregar As Boolean = oRow("Agregar")
        Dim Modificar As Boolean = oRow("Modificar")
        Dim Eliminar As Boolean = oRow("Eliminar")
        Dim Anular As Boolean = oRow("Anular")
        Dim Imprimir As Boolean = oRow("Imprimir")
        Dim Asiento As Boolean = oRow("Asiento")

        For Each ctr As Control In ctrls.Controls

            Debug.Print(ctr.GetType.ToString)

            If ctr.Controls.Count > 0 Then
                'No entrar en controles del usuario
                If Array.IndexOf(ControlesUsuario, ctr.GetType.ToString) < 0 Then
                    FGConfigurarPermiso(oRow, ctr)
                Else
                    Debug.Print(ctr.GetType.ToString)
                End If
            Else
                Select Case ctr.Name.ToUpper
                    Case "BTNACEPTAR", "BTNNUEVO"
                        ctr.Visible = Agregar
                    Case "BTNMODIFICAR", "BTNEDITAR"
                        ctr.Visible = Modificar
                    Case "BTNELIMINAR"
                        ctr.Visible = Eliminar
                    Case "BTNANULAR"
                        ctr.Visible = Anular
                    Case "BTNIMPRIMIR"
                        ctr.Visible = Imprimir
                    Case "BTNASIENTO"
                        ctr.Visible = Asiento
                    Case Else

                End Select
            End If


        Next


    End Sub

    Sub FGMostrarFormulario(ByVal Padre As Form, ByVal frm As Form, ByVal Nombre As String, Optional ByVal Estilo As FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable, Optional ByVal Localizacion As FormStartPosition = FormStartPosition.CenterScreen, Optional ByVal ShowDialog As Boolean = False, Optional ByVal ControlarPermisos As Boolean = True, Optional ByVal AjustarTamañoCompleto As Boolean = False)

        'frm.Icon = My.Resources.SAIN
        frm.Text = VGSoftwareNombre & " ERSA :: " & Nombre
        frm.FormBorderStyle = Estilo
        frm.MinimizeBox = True
        frm.StartPosition = Localizacion
        frm.AcceptButton = New Button
        frm.Icon = My.Resources.SAIN
        '---------------------
        'Apariencia
        FGEstiloFormulario(frm)

        If Padre.IsMdiContainer Then
            frm.MdiParent = Padre
        End If

        'Configurar permisos
        If ControlarPermisos = True Then
            If FGConfigurarPermiso(frm) = False Then
                Exit Sub
            End If
        End If

        'Formato vendedor
        If vgUsuarioEsVendedor Then
            FGFormatoVendedor(frm)
        End If

        'Formato chofer
        If vgUsuarioEsChofer Then
            FGFormatoChofer(frm)
        End If

        'Ajustar el tamaño al maximo
        If AjustarTamañoCompleto Then
            frm.Size = New Size(frmPrincipal2.Size.Width - 50, frmPrincipal2.Height - 130)
            frm.Location = New Point(5, 10)
        End If

        If ShowDialog = True Then
            frm.ShowDialog(Padre)
        Else
            frm.Show()
        End If

        'Auditoria
        If frm.Tag Is Nothing Then
            frm.Tag = Nombre.ToUpper
        End If

        'Eventos
        'CSeguridad.RegistrarSuceso(ERP.CSistema.NUMTipoOperaciones.ING, frm.Name)

        'Ocultar controles denegados
        CSeguridad.OcultarControlesDenegados(frm)

        frm.BringToFront()

    End Sub

    Sub FGMostrarFormulario(ByVal Padre As Form, ByVal frm As String, ByVal Nombre As String, Optional ByVal Estilo As FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable, Optional ByVal Localizacion As FormStartPosition = FormStartPosition.CenterScreen, Optional ByVal ShowDialog As Boolean = False)

        Dim type As Type = Assembly.GetExecutingAssembly().GetType("ERP." & frm)

        'Verificar primero que no este abierto el formulario
        For Each f As Form In My.Application.OpenForms
            If frm = f.Name Then
                f.Show()
                f.BringToFront()
                Exit Sub
            End If
        Next

        If Not type Is Nothing Then
            Dim form As Object = Activator.CreateInstance(type)
            FGMostrarFormulario(Padre, form, Nombre, Estilo, Localizacion, ShowDialog)
        End If

    End Sub

    Public Function FGExisteControl(ByVal control As Control, ByVal Buscar As String) As Boolean

        FGExisteControl = False

        If control.Name.ToUpper = Buscar.ToUpper Then
            Return True
        End If

        If control.Controls.Count > 0 Then
            For Each ctr As Control In control.Controls
                If FGExisteControl(ctr, Buscar) = True Then
                    Return True
                End If
            Next
        Else
            Return False
        End If

    End Function

    Public Function FGReturnControl(ByVal control As Control, ByVal Buscar As String) As Control

        FGReturnControl = Nothing

        If control.Name.ToUpper = Buscar.ToUpper Then
            Return control
        End If

        If control.Controls.Count > 0 Then
            For Each ctr As Control In control.Controls
                If ctr.Name = Buscar Then
                    Return ctr
                End If

                If ctr.Controls.Count > 0 Then
                    FGReturnControl = FGReturnControl(ctr, Buscar)

                    If Not FGReturnControl Is Nothing Then
                        Exit For
                    End If

                End If

            Next
        Else
            Return Nothing
        End If

    End Function

    Public Function FGReturnControlType(ByVal control As Control, ByVal Buscar As String) As Control

        FGReturnControlType = Nothing

        If control.GetType.Name.ToUpper = Buscar.ToUpper Then
            Return control
        End If

        If control.Controls.Count > 0 Then
            For Each ctr As Control In control.Controls
                If ctr.GetType.Name.ToUpper = Buscar.ToUpper Then
                    Return ctr
                End If

                If ctr.Controls.Count > 0 Then
                    FGReturnControlType = FGReturnControl(ctr, Buscar)

                    If Not FGReturnControlType Is Nothing Then
                        Exit For
                    End If

                End If

            Next
        Else
            Return Nothing
        End If

    End Function

    Public Sub FGEstiloFormulario(ByVal frm As Form)

        frm.BackColor = Color.FromArgb(vgColorFormulario)

        For Each ctr As Control In frm.Controls

            Select Case ctr.GetType.Name
                Case "Label"
                    If ctr.Tag = "" Then
                        ctr.ForeColor = Color.FromArgb(vgColorTextos)
                    End If

                    FGClasificacionProductoLBL(ctr)
                Case "CheckBox"
                    ctr.BackColor = Color.FromArgb(vgColorFormulario)
                Case "ocxCHK"
                    Dim chk As ocxCHK = ctr
                    chk.BackColor = Color.FromArgb(vgColorFormulario)
                    chk.lbl.ForeColor = Color.FromArgb(vgColorTextos)
                    FGClasificacionProductoCHK(chk)
                Case "TabPage", "StatusStrip"
                    ctr.BackColor = Color.FromArgb(vgColorFormulario2)
                Case "DataGridView"
                    Dim dgv As Object = ctr
                    dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.DarkSlateGray
                    dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.WhiteSmoke
                Case "GroupBox"
                    ctr.BackColor = Color.FromArgb(vgColorFormulario)
                Case "Button"
                    Dim btn As Button = ctr
                    btn.BackColor = Color.FromArgb(vgColorBoton)
                    btn.ForeColor = Color.FromArgb(vgColorTextosBoton)
                    btn.Cursor = Cursors.Hand
                    btn.FlatStyle = FlatStyle.Flat
                Case "TabControl"
                    Dim tab As TabControl = CType(ctr, TabControl)
                    For i As Integer = 0 To tab.TabPages.Count - 1
                        tab.TabPages(i).BackColor = Color.FromArgb(vgColorFormulario2)
                    Next
            End Select

            If ctr.Controls.Count > 0 Then
                FGEstiloFormualrio(ctr)
            End If

        Next

    End Sub

    Private Sub FGEstiloFormualrio(ByVal ctr As Control)

        For Each ctrs As Control In ctr.Controls

            Select Case ctrs.GetType.Name
                Case "Label"
                    If ctrs.Tag = "" Then
                        ctrs.ForeColor = Color.FromArgb(vgColorTextos)
                    End If
                    FGClasificacionProductoLBL(ctrs)
                Case "CheckBox"
                    ctrs.BackColor = Color.FromArgb(vgColorFormulario)
                Case "ocxCHK"
                    Dim chk As ocxCHK = ctrs
                    chk.BackColor = Color.FromArgb(vgColorFormulario)
                    chk.lbl.ForeColor = Color.FromArgb(vgColorTextos)
                    FGClasificacionProductoCHK(chk)
                Case "TabPage", "StatusStrip"
                    ctrs.BackColor = Color.FromArgb(vgColorFormulario2)
                Case "DataGridView"
                    Dim dgv As Object = ctrs
                    dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.DarkSlateGray
                    dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.WhiteSmoke
                Case "GroupBox", "Panel"
                    ctrs.BackColor = Color.FromArgb(vgColorFormulario)
                Case "Button"
                    Dim btn As Button = ctrs
                    btn.BackColor = Color.FromArgb(vgColorBoton)
                    btn.ForeColor = Color.FromArgb(vgColorTextosBoton)
                    btn.Cursor = Cursors.Hand
                    btn.FlatStyle = FlatStyle.Flat
                Case "TabControl"
                    Dim tab As TabControl = CType(ctrs, TabControl)
                    For i As Integer = 0 To tab.TabPages.Count - 1
                        tab.TabPages(i).BackColor = Color.FromArgb(vgColorFormulario2)
                    Next

            End Select

            If ctrs.Controls.Count > 0 Then
                FGEstiloFormualrio(ctrs)
            End If


        Next

    End Sub

    Public Function FGNombreFuncion(ByVal sf As StackFrame) As String

        ' Obtenemos el nombre del método pasado 
        Dim nombreMetodo As String = sf.GetMethod().Name
        Return nombreMetodo

    End Function

    Private Sub FGFormatoVendedor(ByVal frm As Form)

        Dim ControlCHK As Boolean = True
        Dim ControlCBX As Boolean = True

        ControlCHK = FGExisteControl(frm, "chkVendedor")
        ControlCBX = FGExisteControl(frm, "cbxVendedor")

        If ControlCBX = False Or ControlCHK = False Then
            Exit Sub
        End If

        Dim chk As ocxCHK = CType(FGReturnControl(frm, "chkVendedor"), ERP.ocxCHK)
        Dim cbx As ocxCBX = CType(FGReturnControl(frm, "cbxVendedor"), ERP.ocxCBX)

        If chk Is Nothing Then
            Exit Sub
        End If

        If cbx Is Nothing Then
            Exit Sub
        End If

        chk.Valor = True
        chk.Enabled = False

        cbx.SoloLectura = True
        cbx.Conectar()
        cbx.CargarUnaSolaVez = True
        cbx.SelectedValue(vgUsuarioIDVendedor)
        cbx.cbx.Text = vgUsuarioVendedor
        cbx.txt.Text = vgUsuarioVendedor

    End Sub

    Private Sub FGFormatoChofer(ByVal frm As Form)

        Dim ControlCHK As Boolean = True
        Dim ControlCBX As Boolean = True

        ControlCHK = FGExisteControl(frm, "chkChofer")
        ControlCBX = FGExisteControl(frm, "cbxChofer")

        If ControlCBX = False Or ControlCHK = False Then
            Exit Sub
        End If

        Dim chk As ocxCHK = CType(FGReturnControl(frm, "chkChofer"), ERP.ocxCHK)
        Dim cbx As ocxCBX = CType(FGReturnControl(frm, "cbxChofer"), ERP.ocxCBX)

        If chk Is Nothing Then
            Exit Sub
        End If

        If cbx Is Nothing Then
            Exit Sub
        End If

        chk.Valor = True
        chk.Enabled = False

        cbx.SoloLectura = True
        cbx.Conectar()
        cbx.CargarUnaSolaVez = True
        cbx.SelectedValue(vgUsuarioIDChofer)
        cbx.cbx.Text = vgUsuarioChofer
        cbx.txt.Text = vgUsuarioChofer

    End Sub

    Private Sub FGClasificacionProductoLBL(ByVal lbl As Label)

        'Clasificacion 1
        If lbl.Name.ToUpper = "LBLTIPOPRODUCTO" Then
            lbl.Text = vgConfiguraciones("ProductoClasificacion1").ToString & ":"
        End If

        'Clasificacion 2
        If lbl.Name.ToUpper = "LBLLINEA" Then
            lbl.Text = vgConfiguraciones("ProductoClasificacion2").ToString & ":"
        End If

        'Clasificacion 3
        If lbl.Name.ToUpper = "LBLSUBLINEA" Then
            lbl.Text = vgConfiguraciones("ProductoClasificacion3").ToString & ":"
        End If

        'Clasificacion 4
        If lbl.Name.ToUpper = "LBLSUBLINEA2" Then
            lbl.Text = vgConfiguraciones("ProductoClasificacion4").ToString & ":"
        End If

        'Clasificacion 5
        If lbl.Name.ToUpper = "LBLMARCA" Then
            lbl.Text = vgConfiguraciones("ProductoClasificacion5").ToString & ":"
        End If

        'Clasificacion 6
        If lbl.Name.ToUpper = "LBLPRESENTACION" Then
            lbl.Text = vgConfiguraciones("ProductoClasificacion6").ToString & ":"
        End If

        'Clasificacion 7
        If lbl.Name.ToUpper = "LBLCATEGORIA" Then
            lbl.Text = vgConfiguraciones("ProductoClasificacion7").ToString & ":"
        End If

    End Sub

    Private Sub FGClasificacionProductoCHK(ByVal chk As ERP.ocxCHK)

        'Clasificacion 1
        If chk.Name.ToUpper = "CHKTIPOPRODUCTO" Then
            chk.Texto = vgConfiguraciones("ProductoClasificacion1").ToString
        End If

        'Clasificacion 2
        If chk.Name.ToUpper = "CHKLINEA" Then
            chk.Texto = vgConfiguraciones("ProductoClasificacion2").ToString
        End If

        'Clasificacion 3
        If chk.Name.ToUpper = "CHKSUBLINEA" Then
            chk.Texto = vgConfiguraciones("ProductoClasificacion3").ToString
        End If

        'Clasificacion 4
        If chk.Name.ToUpper = "CHKSUBLINEA2" Then
            chk.Texto = vgConfiguraciones("ProductoClasificacion4").ToString
        End If

        'Clasificacion 5
        If chk.Name.ToUpper = "CHKMARCA" Then
            chk.Texto = vgConfiguraciones("ProductoClasificacion5").ToString
        End If

        'Clasificacion 6
        If chk.Name.ToUpper = "CHKPRESENTACION" Then
            chk.Texto = vgConfiguraciones("ProductoClasificacion6").ToString
        End If

        'Clasificacion 7
        If chk.Name.ToUpper = "CHKCATEGORIA" Then
            chk.Texto = vgConfiguraciones("ProductoClasificacion7").ToString
        End If

    End Sub

    Sub FGCerrarFormulario(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Escape Then
            sender.close()
        End If


    End Sub

    Sub FGKeyUpFormulario(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)



    End Sub

    Sub FGKeyUpFormularioSelectNexControl(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        CSistema.SelectNextControl(sender, e.KeyCode)

    End Sub

    Sub FGToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim t As ToolStripMenuItem = sender
        Dim ts As ToolStrip = t.Owner
        Dim form As Form = ts.FindForm
        Dim dgv As DataGridView = FGReturnControl(form, "DataGridView")

        CSistema.dtToExcel(dgv.DataSource)

    End Sub

    ''' <summary>
    ''' Funcion de actualizacion del sistema
    ''' </summary>
    ''' <param name="AvisaQueNoHayActualizacion">fuerza a que la funcion muestre una ventana cuando no existen actualizaciones</param>
    ''' <param name="ExibeMensajeError">fuerza a mostrar una ventana cuando ocurre algun error de conexion o descarga</param>
    ''' <remarks></remarks>
    Public Sub FGActualizacion(Optional ByVal AvisaQueNoHayActualizacion As Boolean = False, Optional ByVal ExibeMensajeError As Boolean = False)

        vgActualizacionPath = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "PATH", "ftp.201.217.51.104:37000")
        vgActualizacionUsuario = CArchivoInicio.IniGet(VGArchivoINI, "ACTUALIZACION", "USUARIO", "ERSA")
        vgActualizacionPassword = "Abc1234"

        'Si existe una actualizacion, y se cancela, salir
        If AutoUpdate.AutoUpdate("", vgActualizacionPath, vgActualizacionUsuario, vgActualizacionPassword, VGSoftwareVersion, AvisaQueNoHayActualizacion, ExibeMensajeError) = True Then
            End
        End If

    End Sub

    'Get the first day of the month
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(sourceDate.Year, sourceDate.Month, 1)
    End Function

    'Get the last day of the month
    Public Function LastDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Dim lastDay As DateTime = New DateTime(sourceDate.Year, sourceDate.Month, 1)
        Return lastDay.AddMonths(1).AddDays(-1)
    End Function


    Public Function registroArchivo(objeto As String, funcionNm As String, texto As String)
        System.IO.File.AppendAllText("c:\tmp\registroEventos-" & nmModuloActivo & ".txt", objeto & " | " & funcionNm & " | " & texto & " | " & Date.Now.ToString() & vbCrLf)
    End Function
    Public Function bloquearEstadoOperacion(connectionString As String, tipoReporte As String) As String
        Dim query As String = "UPDATE bloqueoOperaciones SET estado = 1 WHERE tipoOperacion = '" & tipoReporte & "' AND estado = 0;"

        Try
            ' Crear una nueva conexión SQL
            Using connection As New SqlConnection(connectionString)
                ' Abrir la conexión
                connection.Open()

                ' Crear un comando SQL
                Using command As New SqlCommand(query, connection)
                    ' Ejecutar el comando y obtener el número de filas afectadas
                    Dim rowsAffected As Integer = command.ExecuteNonQuery()

                    ' Verificar si se actualizaron filas
                    If rowsAffected > 0 Then
                        Return "OK"
                    Else
                        Return "ER"
                    End If
                End Using
            End Using

        Catch ex As Exception
            ' Capturar cualquier excepción y devolver un mensaje de error
            Return "Error al intentar actualizar: " & ex.Message
        End Try
    End Function

    Public Function desBloquearEstadoOperacion(connectionString As String, tipoReporte As String) As String
        Dim query As String = "UPDATE bloqueoOperaciones SET estado = 0 WHERE tipoOperacion = '" & tipoReporte & "';"

        Try
            ' Crear una nueva conexión SQL
            Using connection As New SqlConnection(connectionString)
                ' Abrir la conexión
                connection.Open()

                ' Crear un comando SQL
                Using command As New SqlCommand(query, connection)
                    ' Ejecutar el comando y obtener el número de filas afectadas
                    Dim rowsAffected As Integer = command.ExecuteNonQuery()

                    ' Verificar si se actualizaron filas
                    If rowsAffected > 0 Then
                        Return "OK"
                    Else
                        Return "ER"
                    End If
                End Using
            End Using

        Catch ex As Exception
            ' Capturar cualquier excepción y devolver un mensaje de error
            Return "Error al intentar actualizar: " & ex.Message
        End Try
    End Function




End Module

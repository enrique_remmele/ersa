﻿Module MVariablesGlobales

    'CONEXION
    Public vgConexiones() As String = {}
    Public vgIDConexion As String = 0
    Public vgNombreEmpresa As String = ""
    Public vgNombreServidor As String = ""
    Public vgNombreBaseDatos As String = ""
    Public vgNombreUsuarioBD As String = ""
    Public vgPasswordBD As String = "Abc1234"
    Public VGCadenaConexion As String = "Data Source=" & vgNombreServidor & ";Initial Catalog=" & vgNombreBaseDatos & ";User Id=sa;Password=" & vgPasswordBD & ";Connection Timeout=10"

    'PATH
    Public VGCarpetaAplicacion As String = Application.StartupPath
    Public VGCarpetaError As String = VGCarpetaAplicacion & "\Error\"
    Public VGCarpetaTemporal As String = VGCarpetaAplicacion & "\Temporales\"
    Public VGCarpetaReporte As String = VGCarpetaAplicacion & "\Reportes\"
    Public VGCarpetaReporteTemporal As String = VGCarpetaAplicacion
    Public VGPathEjecutable As String = My.Application.Info.DirectoryPath & "\"

    'INFORMACION DE APLICACION
    Public VGSoftwareNombre As String = My.Application.Info.ProductName
    Public VGSoftwareVersion As String = My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Build & "." & My.Application.Info.Version.Revision
    Public VGSoftwareUltimaActualizacion As String = "15-10-2024" 'Version 213 Modificacion consumo de logistica/ limpiar memoria al facturar
    'Public VGSoftwareUltimaActualizacion As String = "20-09-2024" 'Version 212 Modificacion crystal cr35 uso unico en 64bit - login y frmbienvenida
    'Public VGSoftwareUltimaActualizacion As String = "20-08-2024" 'Version 209 Modificacion de Asientos/ Informe de control de tesoreria
    'Public VGSoftwareUltimaActualizacion As String = "24-07-2024" 'Version 207 Modificacion en cobranzas para las formas de pago/ Agregado la opcion documento electronico en NDP/ Agregado la opcion de Observacion en la carga de Remision para AQUAY
    'Public VGSoftwareUltimaActualizacion As String = "03-07-2024" 'Version 206 Infomr de Uniad de negocio/ Unidad de negocio en tipo de producto
    'Public VGSoftwareUltimaActualizacion As String = "24-06-2024" 'Version 205 Informe de Unidad de negocio/ Listado de descuento de cheque por numero de comprobante/ Filtro de Unidad de negocio en listado de debito/credito
    'Public VGSoftwareUltimaActualizacion As String = "21-06-2024" 'Version 204 Informe de UN/CORRECCION EN DESCUENTO DE CHEQUE
    'Public VGSoftwareUltimaActualizacion As String = "11-06-2024" 'Version 202 Correccion de cast cobranzas en al carga de cheque/ Correccion de gastos por auerdo
    'Public VGSoftwareUltimaActualizacion As String = "06-06-2024" 'Version 201 Correccion de gastos/ Preparar Pago por UN
    'Public VGSoftwareUltimaActualizacion As String = "05-06-2024" 'Version 200 Informe UN-SUC/ Informe Acuerdo/ Informe control de tesoreria/ NOta de credito
    'Public VGSoftwareUltimaActualizacion As String = "03-06-2024" 'Version 199 Puesta en produccion cast cobranzas/Informe UN-SUC/ Unidad de negocio en cuenta bancaria y preparar pago(Falta hacer lo de las cuentas que estire segun el filtro)
    'Public VGSoftwareUltimaActualizacion As String = "17-05-2024" 'Version 198 Cast Cobranzas/Reporteria UN/Acuerdo Proveedor
    'Public VGSoftwareUltimaActualizacion As String = "14-03-2024" 'Version 197 Campos de acuerdo/ Impresion de acuerdo/ ABM Proveedor/ Reporteria para tesoreria
    'Public VGSoftwareUltimaActualizacion As String = "27-02-2024" 'Version 196 Control de cheques e informes varios
    'Public VGSoftwareUltimaActualizacion As String = "22-02-2024" 'Version 195 Control de carga de pedido 
    'Public VGSoftwareUltimaActualizacion As String = "20-02-2024" 'Version 194 Modificaciones en nota de credito por acuerdo/ ABM ACUERDO DETALLE/ INFORME DEVOLUCION DE LOTE/ DOCUMENTOS PAGADOS/ VALIDAR COBRANZA CAST
    'Public VGSoftwareUltimaActualizacion As String = "30-01-2024" 'Version 193 Ajuste ABM Cliente Tipo de operacion
    'Public VGSoftwareUltimaActualizacion As String = "29-01-2024" 'Version 192 Automatizacion Generacion Remision
    'Public VGSoftwareUltimaActualizacion As String = "15-01-2024" 'Version 190 Re-envio de comprobantes electronicos/ Acuerdos/ Avances de remision automatica
    'Public VGSoftwareUltimaActualizacion As String = "21-11-2023" 'Version 189 Informes de acuerdo/ Asociacion de acuerdo a gasto y a nota de credito/Nota de credito Proveedor si es electronica
    'Public VGSoftwareUltimaActualizacion As String = "18-10-2023" 'Version 188 ABM de acuerdo de cliente 
    'Public VGSoftwareUltimaActualizacion As String = "31-08-2023" 'Version 187 Actualizacion de campos para remision electronica y carga de kilometros
    'Public VGSoftwareUltimaActualizacion As String = "28-08-2023" 'Version 186 Actualizacion de campos varios para remision electronica 
    'Public VGSoftwareUltimaActualizacion As String = "12-08-2023" 'Version 185 Actualizacion de consumo de combustible y descarga de stock para generar asientos de acuerdo a los motivos
    'Public VGSoftwareUltimaActualizacion As String = "22-06-2023" 'Version 184 Modificacion ABM Cliente 
    'Public VGSoftwareUltimaActualizacion As String = "10-06-2023" 'Version 183 Correcion de saldo en orden de pago y modificaciones de asientos
    'Public VGSoftwareUltimaActualizacion As String = "07-06-2023" 'Version 182 MODIFICACION DE ASIENTOS/ CARGA DE PRECIO CREDITO EN MACHEOS PARA INTERES A DEVENGAR
    'Public VGSoftwareUltimaActualizacion As String = "23-05-2023" 'Version 181 Actualizacion de depatamentos y secciones para carga en gastos/fondo fijo/ movimientos 
    'Public VGSoftwareUltimaActualizacion As String = "13-04-2023" 'Version 180 Cambio en cobranzas para que estire la cotizacion de la fecha cobranza/ Movimiento Informatica / Rango de fecha contabilidad por perfil
    'Public VGSoftwareUltimaActualizacion As String = "30-03-2023" 'Version 179 Nota de credito por diferencia de precio/departameno y seccion
    'Public VGSoftwareUltimaActualizacion As String = "09-02-2023" 'Version 178 Modificacion para compras y gastos para la gente de compras y contabilidad / Modificacion de nombre de "Gastos Varios"
    'Public VGSoftwareUltimaActualizacion As String = "18-01-2023" 'Version 177 NC cambios en Devolucion/ABM de vencimiento de productos
    'Public VGSoftwareUltimaActualizacion As String = "02-01-2023" 'Version 176 plan de cuenta/unidad de negocio/cc y depto
    'Public VGSoftwareUltimaActualizacion As String = "24-11-2022" 'Version 175 Ajuste y Modificación en NC
    'Public VGSoftwareUltimaActualizacion As String = "07-10-2022" 'Version 174 Actualizacion de Check para especificar Factura electronica Compra Modificar Gasto y Comprobante libro IVA
    'Public VGSoftwareUltimaActualizacion As String = "30-09-2022" 'Version 173 Actualizacion de Check para especificar Factura electronica de FF y Gasto / Carga de decimales NCProveedor
    'Public VGSoftwareUltimaActualizacion As String = "11-08-2022" 'Version 172 Actualizacion de orden de carga - Logistica / Reporte de ventas totales por producto
    'Public VGSoftwareUltimaActualizacion As String = "29-06-2022" 'Version 169 Actualizacion de aplicacion de anticipo a proveedores / ABM proveedor opcion DelExterior
    'Public VGSoftwareUltimaActualizacion As String = "20-06-2022" 'Version 168 Cambio en el timeout de Grabar de Desgloce de FRMRENDICIONLOTE
    'Public VGSoftwareUltimaActualizacion As String = "16-06-2022" 'Version 167 Se modifica operativa en frmnotacreditopedido para que calcule opciones para impresion de factura

    'Public VGSoftwareUltimaActualizacion As String = "16-06-2022" 'Version 165 Se modifica operativa en frmnotacreditopedido para que imprima factura relacionada
    'Public VGSoftwareUltimaActualizacion As String = "15-06-2022" 'Version 164 Se modifica cantidadcaja para que tome valores decimales y no solo int en frmCompra
    'Public VGSoftwareUltimaActualizacion As String = "14-06-2022" 'Version 163 Plazo en frmPedidoNotaCredito
    'Public VGSoftwareUltimaActualizacion As String = "11-06-2022" 'Version 161 Corrección ListadoFacturasEmitidas, separando IDTipoProducto e IDProducto
    'Public VGSoftwareUltimaActualizacion As String = "08-06-2022" 'Version 160 Corrección cobranza credito, comprobante de retencion
    'Public VGSoftwareUltimaActualizacion As String = "08-06-2022" 'Version 159 Correccion reporte listado pedido nota de credito
    'Public VGSoftwareUltimaActualizacion As String = "08-06-2022" 'Version 158 Correccion
    'Public VGSoftwareUltimaActualizacion As String = "06-06-2022" 'Version 157 Reproceso para Impresion de Notas de Credito, Visualizacion de Asiento Gastos RRHH desde Preparar Pago, Validacion de Comprobante de Retencion
    'Public VGSoftwareUltimaActualizacion As String = "25-05-2022" 'Version 156 Correctivo en Listado de Venta Productos
    'Public VGSoftwareUltimaActualizacion As String = "24-05-2022" 'Version 155 Correctivo para proceso de reproceso de pedido
    'Public VGSoftwareUltimaActualizacion As String = "21-05-2022" 'Version 154 Listado de Cheques Rechazados, tiene filtro estado ahora
    'Public VGSoftwareUltimaActualizacion As String = "11-05-2022" 'Version 153 Listado de Facturas Emitidas
    'Public VGSoftwareUltimaActualizacion As String = "26-04-2022" 'Version 152 Modificaciones en Preparar Pago por Pedido de Tesoreria
    'Public VGSoftwareUltimaActualizacion As String = "21-04-2022" 'Version 151 Modificaciones Proveedor Inactivado, OP; NCP; Gastos; Compra
    'Public VGSoftwareUltimaActualizacion As String = "11-04-2022" 'Version 150 Modificaciones para Solicitud de Descuento Precio
    'Public VGSoftwareUltimaActualizacion As String = "30-03-2022" 'Version 149 Cierre y Apertura de Ejercicio, Correcion en ComprobanteLibroIVA
    'Public VGSoftwareUltimaActualizacion As String = "23-02-2022" 'Version 148 Resolucion 90
    'Public VGSoftwareUltimaActualizacion As String = "10-02-2022" 'Version 147 Modificaciones AdministracionVentas, ControldeIntegridad, Rendicion
    'Public VGSoftwareUltimaActualizacion As String = "31-01-2022" 'Version 146 Modificaciones NotaCredito
    'Public VGSoftwareUltimaActualizacion As String = "13-01-2022" 'Version 145 TipoEntrega desde Pedidos, Venta y Logistica
    'Public VGSoftwareUltimaActualizacion As String = "03-12-2021" 'Version 144
    'Public VGSoftwareUltimaActualizacion As String = "19-11-2021" 'Version 143
    'Public VGSoftwareUltimaActualizacion As String = "11-11-2021" 'Version 141
    ' Public VGSoftwareUltimaActualizacion As String = "26-10-2021" 'Version 140
    'Public VGSoftwareUltimaActualizacion As String = "20-10-2021"
    'Public VGSoftwareUltimaActualizacion As String = "24-09-2021" 'Ajuste en carga de gastos y autorizacion de nota de credito FA
    'Public VGSoftwareUltimaActualizacion As String = "23-09-2021" 'Ajuste en Listado Estado Cheque y Total de AsientoCabecera DebitoCredito por Extorno
    'Public VGSoftwareUltimaActualizacion As String = "20-09-2021" 'Ajuste Cliente, Proveedor, Asiento Tarjeta Deposito Ect.
    'Public VGSoftwareUltimaActualizacion As String = "09-09-2021" 'Ajuste Hechauka Ventas IVADebito10% e IVADebito5%
    'Public VGSoftwareUltimaActualizacion As String = "03-09-2021" 'Ajustes en Extorno por DepositoBancario, Reporte AsientoDiario, frmDescargaCompra, etc.
    'Public VGSoftwareUltimaActualizacion As String = "02-09-2021" 'Extorno por DepositoBancario
    'Public VGSoftwareUltimaActualizacion As String = "25-08-2021" 'Extorno DebitoCreditoBancario
    'Public VGSoftwareUltimaActualizacion As String = "24-08-2021" 'Extorno DebitoCreditoBancario
    'Public VGSoftwareUltimaActualizacion As String = "17-08-2021"
    'Public VGSoftwareUltimaActualizacion As String = "11-08-2021" Cambios de Preparar Pago
    'Public VGSoftwareUltimaActualizacion As String = "22-07-2021"
    'Public VGSoftwareUltimaActualizacion As String = "12-07-2021"
    'Public VGSoftwareUltimaActualizacion As String = "09-07-2021"
    'Public VGSoftwareUltimaActualizacion As String = "01-07-2021"
    'Public VGSoftwareUltimaActualizacion As String = "28-06-2021"
    'Public VGSoftwareUltimaActualizacion As String = "21-05-2021"
    'Public VGSoftwareUltimaActualizacion As String = "20-05-2021"
    'Public VGSoftwareUltimaActualizacion As String = "19-05-2021"
    'Public VGSoftwareUltimaActualizacion As String = "18-05-2021"
    'Public VGSoftwareUltimaActualizacion As String = "28-04-2021"
    'Public VGSoftwareUltimaActualizacion As String = "22-04-2021"
    'Public VGSoftwareUltimaActualizacion As String = "31-03-2021"
    'Public VGSoftwareUltimaActualizacion As String = "06-01-2019"


    Public VGSoftwareLicenciaOtorgadaA As String = ""
    Public VGSoftwareDesarrollador As String = My.Application.Info.CompanyName
    Public VGSoftwareDesarrolladorRUC As String = ""
    Public VGSoftwareDesarrolladorDireccion As String = ""
    Public VGSoftwareDesarrolladorTelefono As String = ""
    Public VGSoftwareDesarrolladorWeb As String = ""
    Public VGSoftwareDesarrolladorEmail As String = ""
    Public VGSoftwareDesarrolladorInfo As String = ""
    Public VGSoftwareInfo As String = VGSoftwareNombre & "  ::  Versión del Sistema: " & VGSoftwareVersion & " www.ersa.com.py"

    'CONFIGURACIONES
    Public VGArchivoINI As String = VGCarpetaAplicacion & "\" & My.Application.Info.ProductName & ".INI"
    Public VGFechaHoraSistema As Date
    Public VGFechaFacturacion As Date
    Public vgConfiguraciones As DataRow
    Public vgModoPedido As Boolean = False
    Public vgIDTransaccionCaja As Integer = 0
    Public VGFechaFacturacionHasta As Date

    'IDENTIFICACIONES
    Public nmModuloActivo As String = "SistemaBase"
    Public vFacturaAsistida As Boolean = False



    Public vgIDSucursal As Integer = 0
    Public vgIDDeposito As Integer = 0
    Public vgIDTerminal As Integer = 0
    Public vgIDUsuario As Integer = 0
    Public vgIDPerfil As Integer = 0

    'Public vgIDSucursal As Integer = 1
    'Public vgIDDeposito As Integer = 1
    'Public vgIDTerminal As Integer = 1
    'Public vgIDUsuario As Integer = 1

    'Public vgIDPerfil As Integer = 1
    Public vgSucursal As String = ""
    Public vgSucursalCodigo As String = ""
    Public vgReferenciaSucursal As String = ""
    Public vgDeposito As String = ""
    Public vgTerminal As String = ""
    Public vgUsuario As String = ""
    Public vgUsuarioIdentificador As String = ""
    Public vgPerfil As String = ""
    Public vgOwnerDBFunction As String = "dbo"
    Public vgUsuarioEsVendedor As Boolean = False
    Public vgUsuarioVendedor As String = ""
    Public vgUsuarioIDVendedor As Integer = 0
    Public vgVerCosto As Boolean = False

    Public vgUsuarioEsChofer As Boolean = False
    Public vgUsuarioChofer As String = ""
    Public vgUsuarioIDChofer As Integer = 0

    'EMPRESA
    Public vgEmpresa As String = "ENRIQUE REMMELE S.A.C.I."
    Public vgEmpresaRUC As String = "80008448-9"
    Public vgEmpresaDireccion As String = "CAPITAN JULIAN INSFRAN 148 C/ INGLATERRA"
    Public vgEmpresaTelefono As String = "+595 21 299-575"
    Public vgEmpresaWeb As String = "http://www.ersa.com.py"

    'RRHH
    Public vgIDEmpresaRRHH As Integer = 1

    'LICENCIA
    Public vgKey As String = "!·$%&/()=?¿¬€~#@|"
    Public vgLicencia As String = ""
    Public vgLicenciaPath As String = VGCarpetaAplicacion & "\" & My.Application.Info.ProductName & ".lic"
    Public vgLicenciaOtorgado As String
    Public vgLicenciaOtorgadoRazonSocial As String
    Public vgLicenciaOtorgadoRUC As String
    Public vgLicenciaOtorgadoDireccion As String
    Public vgLicenciaOtorgadoTelefono As String
    Public vgLicenciaOtorgadoEmail As String
    Public vgLicenciaOtorgadoWEB As String
    Public vgLicenciaOtorgadoFechaInicio As String
    Public vgLicenciaOtorgadoFechaFin As String
    Public vgLicenciaOtorgadoEmpresas As String = "0"
    Public vgLicenciaOtorgadoSucursales As String = "0"
    Public vgLicenciaOtorgadoDepositos As String = "0"
    Public vgLicenciaOtorgadoTerminales As String = "0"
    Public vgLicenciaOtorgadoAdmin As String
    Public vgLicenciaOtorgadoPassword As String

    'APARIENCIA
    Public vgColorFocus As String = -8075777
    Public vgColorSoloLectura As String = -196962
    Public vgColorBackColor As String = -197380
    Public vgColorSeleccionado As String = -16711936
    Public vgColorFormulario As String = -197380
    Public vgColorFormulario2 As String = -855300
    Public vgColorTextos As String = -12582912
    Public vgColorBoton As String = -855310
    Public vgColorTextosBoton As String = -12582912
    Public vgColorDataGridAlternancia As String = Color.FromKnownColor(KnownColor.WhiteSmoke).ToArgb

    'DATA
    Public vgData As New DataSet
    Public vgSAIN As New DataSet

    'TECLAS DE ACCESO RAPIDO
    Public vgKeyConsultar As Integer = Keys.F1
    Public vgKeyEditar As Integer = Keys.F2
    Public vgKeyVerInformacionDetallada As Integer = Keys.F3
    Public vgKeyActualizarTabla As Integer = Keys.F4
    Public vgKeyInicializar As Integer = Keys.F5

    'Transacciones
    Public vgKeyProcesar As Integer = Keys.F8
    Public vgKeyCerrar As Integer = Keys.Escape
    Public vgKeyNuevoRegistro As Integer = Keys.Add

    'Otros
    'Facturacion
    Public vgIDPaperSize As Integer = -1
    Public vgImpresionFacturaPath As String
    Public vgImpresionFacturaConversionPath As String
    Public vgImpresionFacturaImpresora As String

    'Nota Credito/Debito
    Public vgImpresionNotaCreditoPath As String
    Public vgImpresionNotaDebitoPath As String

    'Retenciones
    Public vgImpresionRetencionPath As String

    'Remision
    Public vgImpresionRemisionPath As String

    'ACTUALIZACION
    Public vgActualizacionPath As String
    Public vgActualizacionUsuario As String
    Public vgActualizacionPassword As String
    Public vgActualizacionVersion As String

End Module

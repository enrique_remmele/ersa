﻿Imports System.Collections.Specialized

Module Funciones_Globales

    ' ''' <summary>
    ' ''' Encripta
    ' ''' </summary>
    ' ''' <param name="cadena"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Function FGEncripta(ByVal cadena As String) As String
    '    Dim Clave As String, i As Integer, Pass2 As String
    '    Dim CAR As String, Codigo As String
    '    Clave = "%ü&/@#$A"
    '    Pass2 = ""
    '    For i = 1 To Len(cadena)
    '        CAR = Mid(cadena, i, 1)
    '        Codigo = Mid(Clave, ((i - 1) Mod Len(Clave)) + 1, 1)
    '        Pass2 = Pass2 & Microsoft.VisualBasic.Strings.Right("0" & Hex(Asc(Codigo) Xor Asc(CAR)), 2)

    '    Next i
    '    FGEncripta = Pass2
    'End Function

    ' ''' <summary>
    ' ''' DesEncripta
    ' ''' </summary>
    ' ''' <param name="cadena"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Function FGDesEncripta(ByVal cadena As String) As String
    '    Dim Clave As String, i As Integer, Pass2 As String
    '    Dim CAR As String, Codigo As String
    '    Dim j As Integer

    '    Clave = "%ü&/@#$A"
    '    Pass2 = ""
    '    j = 1
    '    For i = 1 To Len(cadena) Step 2
    '        CAR = Mid(cadena, i, 2)
    '        Codigo = Mid(Clave, ((j - 1) Mod Len(Clave)) + 1, 1)
    '        Pass2 = Pass2 & Chr(Asc(Codigo) Xor Val("&h" + CAR))
    '        j = j + 1
    '    Next i
    '    FGDesEncripta = Pass2
    'End Function

End Module

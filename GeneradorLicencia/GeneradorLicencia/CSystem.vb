﻿Imports System.Collections.Specialized

Public Class CSystem


    Public Function SetNameValueCollection(ByVal cadena As String) As NameValueCollection

        SetNameValueCollection = New NameValueCollection

        'Parseamos la cadena
        Dim Parameters() As String = cadena.Split("&")

        'Agregamos a la coleccion
        For i As Integer = 0 To Parameters.GetLength(0) - 1
            If Parameters(i).IndexOf("=") >= 0 Then
                Dim Key As String = Parameters(i).Split("=")(0)
                Dim Value As String = Parameters(i).Split("=")(1)

                SetNameValueCollection.Add(Key, Value)

            End If
        Next

    End Function

    Public Function SetNameValueCollectionToString(ByVal Parameter As NameValueCollection) As String

        SetNameValueCollectionToString = ""

        Dim Retorno As String = ""

        'Agregamos a la coleccion
        For i As Integer = 0 To Parameter.Count - 1
            If i <> Parameter.Count - 1 Then
                Retorno = Retorno & Parameter.GetKey(i) & "=" & Parameter.Get(Parameter.GetKey(i)) & "&"
            Else
                Retorno = Retorno & Parameter.GetKey(i) & "=" & Parameter.Get(Parameter.GetKey(i))
            End If
        Next

        Return Retorno

    End Function

    Public Function ReturnNameValueCollectionKeyVector(ByVal Parameter As NameValueCollection) As String()

        Dim Retorno() As String = Nothing

        'Agregamos a la coleccion
        For i As Integer = 0 To Parameter.Count - 1
            ReDim Preserve Retorno(i)
            Retorno(i) = Parameter.GetKey(i)
        Next

        Return Retorno

    End Function


End Class

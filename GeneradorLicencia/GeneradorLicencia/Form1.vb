﻿Imports System.Collections.Specialized
Imports System.IO

Public Class Form1

    'Clases
    Dim CSystem As New CSystem
    Dim CDecode As New ERP2ED.CDecode

    '
    Sub CargarArchivo()

        OpenFileDialog1.ShowDialog()
        Dim SPath As String = OpenFileDialog1.FileName
        Dim sContent As String = vbNullString
        Dim sTerminales As String = vbNullString

        ' verifica si existe el path   
        If My.Computer.FileSystem.FileExists(SPath) Then

            ' lee todo el contenido   
            sContent = My.Computer.FileSystem.ReadAllText(SPath)
            CDecode.InClearText = sContent
            CDecode.Decrypt()
            txtArchivo.Text = SPath

        Else
            MessageBox.Show("No se encontro el archivo!!!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Dim values As NameValueCollection = CSystem.SetNameValueCollection(CDecode.CryptedText)

        If values.Count = 0 Then
            Exit Sub
        End If

        'EMPRESA
        txtRazonSocial.Text = values("RAZON SOCIAL").ToString
        txtRUC.Text = values("RUC").ToString
        txtDireccion.Text = values("DIRECCION").ToString
        txtTelefono.Text = values("TELEFONO").ToString
        txtWeb.Text = values("WEB").ToString
        txtEmail.Text = values("EMAIL").ToString
        txtAdmin.Text = values("ADMIN").ToString
        txtContraseña.Text = values("PASSWORD").ToString


        'PARAMETROS
        If values("FECHA INICIO") <> "" Then
            chkInicio.Checked = True
            dtpInicio.Value = CDate(values("FECHA INICIO"))
        Else
            chkInicio.Checked = False
        End If

        If values("FECHA FIN") <> "" Then
            chkFin.Checked = True
            dtpFin.Value = CDate(values("FECHA FIN"))
        Else
            chkFin.Checked = False
        End If

        If IsNumeric(values("CANTIDAD USUARIOS")) = True Then
            nudCantidadUsuarios.Value = values("CANTIDAD USUARIOS")
        Else
            nudCantidadUsuarios.Value = 0
        End If

        If IsNumeric(values("CANTIDAD DEPOSITOS")) = True Then
            nudCantidadDeposito.Value = values("CANTIDAD DEPOSITOS")
        Else
            nudCantidadDeposito.Value = 0
        End If

        If IsNumeric(values("CANTIDAD TERMINALES")) = True Then
            nudCantidadTerminal.Value = values("CANTIDAD TERMINALES")
        Else
            nudCantidadTerminal.Value = 0
        End If

        lvTerminales.Items.Clear()
        sTerminales = values("TERMINALES").ToString
        If sTerminales.Length > 0 Then
            Dim TERMINALES() As String = sTerminales.Split("#")
            For I As Integer = 0 To TERMINALES.GetLength(0) - 1
                lvTerminales.Items.Add(TERMINALES(I))
            Next
        End If

        'RESUMEN
        txtResumen.Text = CDecode.CryptedText

    End Sub

    Sub Nuevo()
        SaveFileDialog1.DefaultExt = ".lic|.lic"
        SaveFileDialog1.ShowDialog()
        txtArchivo.Text = SaveFileDialog1.FileName

        Dim obj_FSO As Object
        Dim Archivo As Object

        Try
            obj_FSO = CreateObject("Scripting.FileSystemObject")

            'Creamos un archivo con el método CreateTextFile
            Archivo = obj_FSO.CreateTextFile(txtArchivo.Text, False)

            'Escribimos lineas
            Archivo.WriteLine("")

            'Cerramos el fichero
            Archivo.Close()

            obj_FSO = Nothing
            Archivo = Nothing

        Catch ex As Exception

        End Try

    End Sub

    Sub Generar()

        'Validar
        If File.Exists(txtArchivo.Text) = False Then
            MessageBox.Show("Seleccione un archivo!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        If nudCantidadTerminal.Value > 0 Then
            If nudCantidadTerminal.Value < lvTerminales.Items.Count Then
                MessageBox.Show("Cantidad de terminales es mayor a la permitida!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If
        End If

        If txtAdmin.Text.Length = 0 Or txtContraseña.Text.Length = 0 Then
            MessageBox.Show("La licencia debe permitir un usuario!", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        Try

            'Borrar todo el contenido
            My.Computer.FileSystem.WriteAllText(txtArchivo.Text, "", False)

            'Volver a generar
            Dim Codigo As String = String.Format("RAZON SOCIAL={0}&RUC={1}&DIRECCION={2}&TELEFONO={3}&WEB={4}&EMAIL={5}&ADMIN={6}&PASSWORD={7}&FECHA INICIO={8}&FECHA FIN={9}&CANTIDAD USUARIOS={10}&CANTIDAD DEPOSITOS={11}&CANTIDAD TERMINALES={12}", _
                                                 txtRazonSocial.Text, txtRUC.Text, txtDireccion.Text, txtTelefono.Text, txtWeb.Text, txtEmail.Text, txtAdmin.Text, txtContraseña.Text, If(chkInicio.Checked = True, dtpInicio.Value.ToShortDateString, ""), If(chkFin.Checked = True, dtpFin.Value.ToShortDateString, ""), nudCantidadUsuarios.Value, nudCantidadDeposito.Value, nudCantidadTerminal.Value)

            'Generar terminales
            Dim Terminales As String = ""
            Codigo = Codigo & "&TERMINALES="
            Dim i As Integer = 0
            For Each item As ListViewItem In lvTerminales.Items
                If i = lvTerminales.Items.Count - 1 Then
                    Terminales = Terminales & item.Text
                Else
                    Terminales = Terminales & item.Text & "#"
                End If

                i = i + 1

            Next

            'Grabar
            CDecode.InClearText = Codigo & Terminales
            CDecode.Encrypt()

            Dim Resumen As String = CDecode.InClearText

            My.Computer.FileSystem.WriteAllText(txtArchivo.Text, CDecode.CryptedText, False)
            txtResumen.Text = Resumen
            MessageBox.Show("Licencia generada", "Informe", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtResumen.Text = "Error"
        End Try
        

    End Sub

    Sub AgregarTerminal()

        If txtTerminal.Text.Trim.Length = 0 Then
            Exit Sub
        End If

        If nudCantidadTerminal.Value > 0 Then
            'Nro de Terminales
            If nudCantidadTerminal.Value = lvTerminales.Items.Count Then
                Exit Sub
            End If
        End If

        'Preguntar si ya existe
        For Each item As ListViewItem In lvTerminales.Items
            If item.Text = txtTerminal.Text.Trim Then
                Exit Sub
            End If
        Next

        lvTerminales.Items.Add(txtTerminal.Text.Trim)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        CargarArchivo()
    End Sub

    Private Sub chkInicio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInicio.CheckedChanged
        If chkInicio.Enabled = True Then
            If chkInicio.Checked = True Then
                dtpInicio.Enabled = True
            Else
                dtpInicio.Enabled = False
            End If
        End If
        
    End Sub

    Private Sub chkFin_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFin.CheckedChanged
        If chkFin.Enabled = True Then
            If chkFin.Checked = True Then
                dtpFin.Enabled = True
            Else
                dtpFin.Enabled = False
            End If
        End If
    End Sub

    Private Sub txtTerminal_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTerminal.KeyUp
        If e.KeyCode = Keys.Enter Then
            AgregarTerminal()
        End If

    End Sub

    Private Sub lvTerminales_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvTerminales.KeyUp
        If e.KeyCode = Keys.Delete Then
            If lvTerminales.SelectedItems.Count = 0 Then
                Exit Sub
            End If

            lvTerminales.SelectedItems(0).Remove()

        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Generar()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Nuevo()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

End Class

﻿ALTER ROLE [db_owner] ADD MEMBER [AuditoriaASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [CobranzasASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [CobranzasCDE];


GO
ALTER ROLE [db_owner] ADD MEMBER [CobranzasCON];


GO
ALTER ROLE [db_owner] ADD MEMBER [CobranzasMOL];


GO
ALTER ROLE [db_owner] ADD MEMBER [ComprasASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [ContabilidadASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [DepositoASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [FacturacionASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [FacturacionCDE];


GO
ALTER ROLE [db_owner] ADD MEMBER [FacturacionCON];


GO
ALTER ROLE [db_owner] ADD MEMBER [FacturacionMOL];


GO
ALTER ROLE [db_owner] ADD MEMBER [InformaticaASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [TeleventasASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [TesoreriaASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [TransporteASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [VendedoresASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [VentasASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [VentasCDE];


GO
ALTER ROLE [db_owner] ADD MEMBER [VentasCON];


GO
ALTER ROLE [db_owner] ADD MEMBER [VentasMOL];


GO
ALTER ROLE [db_owner] ADD MEMBER [DirectorioASU];


GO
ALTER ROLE [db_owner] ADD MEMBER [remmele_user];


GO
ALTER ROLE [db_accessadmin] ADD MEMBER [remmele];


GO
ALTER ROLE [db_datareader] ADD MEMBER [AuditoriaASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [CobranzasASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [CobranzasCDE];


GO
ALTER ROLE [db_datareader] ADD MEMBER [CobranzasCON];


GO
ALTER ROLE [db_datareader] ADD MEMBER [CobranzasMOL];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ComprasASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ContabilidadASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [DepositoASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [FacturacionASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [FacturacionCDE];


GO
ALTER ROLE [db_datareader] ADD MEMBER [FacturacionCON];


GO
ALTER ROLE [db_datareader] ADD MEMBER [FacturacionMOL];


GO
ALTER ROLE [db_datareader] ADD MEMBER [InformaticaASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [remmele];


GO
ALTER ROLE [db_datareader] ADD MEMBER [RRHHASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [TeleventasASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [TesoreriaASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [TransporteASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [VendedoresASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [VentasASU];


GO
ALTER ROLE [db_datareader] ADD MEMBER [VentasCDE];


GO
ALTER ROLE [db_datareader] ADD MEMBER [VentasCON];


GO
ALTER ROLE [db_datareader] ADD MEMBER [VentasMOL];


GO
ALTER ROLE [db_datareader] ADD MEMBER [DirectorioASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [AuditoriaASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [CobranzasASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [CobranzasCDE];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [CobranzasCON];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [CobranzasMOL];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [ComprasASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [ContabilidadASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [DepositoASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [FacturacionASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [FacturacionCDE];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [FacturacionCON];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [FacturacionMOL];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [InformaticaASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [RRHHASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [TeleventasASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [TesoreriaASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [TransporteASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [VendedoresASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [VentasASU];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [VentasCDE];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [VentasCON];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [VentasMOL];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [DirectorioASU];


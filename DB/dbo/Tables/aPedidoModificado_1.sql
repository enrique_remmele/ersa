﻿CREATE TABLE [dbo].[aPedidoModificado] (
    [idtransaccion]         INT      NULL,
    [FechaFacturar]         DATE     NULL,
    [EntregaCliente]        BIT      NULL,
    [IDUsuarioModificacion] INT      NULL,
    [FechaModificacion]     DATETIME NULL
);


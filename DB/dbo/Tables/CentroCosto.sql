﻿CREATE TABLE [dbo].[CentroCosto] (
    [ID]          TINYINT      NOT NULL,
    [Codigo]      VARCHAR (10) NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_CentroCosto_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_CentroCosto] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[Cotizacion] (
    [ID]         INT      NOT NULL,
    [Fecha]      DATETIME NOT NULL,
    [IDMoneda]   TINYINT  NOT NULL,
    [Cotizacion] MONEY    CONSTRAINT [DF_Cotizacion_Cotizacion] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Cotizacion] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[aMotivoAnulacionCobranza] (
    [ID]                    INT           NOT NULL,
    [Descripcion]           VARCHAR (100) NOT NULL,
    [Estado]                BIT           NULL,
    [IDUsuarioModificacion] INT           NULL,
    [FechaModificacion]     DATETIME      NULL,
    [IDTerminal]            INT           NULL
);


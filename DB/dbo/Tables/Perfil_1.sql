﻿CREATE TABLE [dbo].[Perfil] (
    [ID]                    INT          NOT NULL,
    [Descripcion]           VARCHAR (50) NOT NULL,
    [Estado]                BIT          NOT NULL,
    [Visualizar]            BIT          NULL,
    [Agregar]               BIT          NULL,
    [Modificar]             BIT          NULL,
    [Eliminar]              BIT          NULL,
    [Anular]                BIT          NULL,
    [Imprimir]              BIT          NULL,
    [VerCosto]              BIT          NULL,
    [IDDepartamentoEmpresa] INT          NULL,
    CONSTRAINT [PK_Perfil] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
Create TRIGGER TiPerfilAccesoEspecifico ON dbo.Perfil
   AFTER INSERT, UPDATE
AS 

Begin

	Declare @vIDPerfil tinyint = (Select ID From Inserted)
	Declare @vIDAccesoEspecifico int
	Declare @vIDMenu numeric(18,0)

	Declare db_cursor cursor for
	Select ID, IDMenu From AccesoEspecifico
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDAccesoEspecifico, @vIDMenu
	While @@FETCH_STATUS = 0 Begin 
	
		--Verificamos si existe ya en la tabla
		If Exists(Select IDPerfil From AccesoEspecificoPerfil Where IDPerfil=@vIDPerfil And IDAccesoEspecifico=@vIDAccesoEspecifico And IDMenu=@vIDMenu) Begin
			GoTo Seguir
		End		

		--Insertamos sin privilegios
		Insert Into AccesoEspecificoPerfil(IDPerfil, IDAccesoEspecifico, IDMenu, Habilitar)
		Values(@vIDPerfil, @vIDAccesoEspecifico, @vIDMenu, 'False')

Seguir:

		Fetch Next From db_cursor Into @vIDAccesoEspecifico, @vIDMenu
											
	End
			
	--Cierra el cursor
	Close db_cursor   
	Deallocate db_cursor

End

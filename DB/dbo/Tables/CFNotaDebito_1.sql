﻿CREATE TABLE [dbo].[CFNotaDebito] (
    [IDOperacion]      TINYINT NOT NULL,
    [ID]               TINYINT NOT NULL,
    [Credito]          BIT     NULL,
    [Contado]          BIT     NULL,
    [BuscarEnCliente]  BIT     NULL,
    [BuscarEnProducto] BIT     NULL,
    [IDTipoCuentaFija] TINYINT NULL,
    [IDTipoDescuento]  TINYINT NULL,
    CONSTRAINT [PK_CFNotaDebito] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[NotaCreditoProveedorCompra] (
    [IDTransaccionNotaCreditoProveedor] NUMERIC (18) NOT NULL,
    [IDTransaccionEgreso]               NUMERIC (18) NOT NULL,
    [ID]                                TINYINT      NOT NULL,
    [Importe]                           MONEY        NOT NULL,
    [cobrado]                           MONEY        NOT NULL,
    [Descontado]                        MONEY        NOT NULL,
    [Saldo]                             MONEY        NOT NULL,
    CONSTRAINT [PK_NotaCreditoProveedorCompra] PRIMARY KEY CLUSTERED ([IDTransaccionNotaCreditoProveedor] ASC, [IDTransaccionEgreso] ASC, [ID] ASC),
    CONSTRAINT [FK_NotaCreditoProveedorCompra_NotaCreditoProveedor] FOREIGN KEY ([IDTransaccionNotaCreditoProveedor]) REFERENCES [dbo].[NotaCreditoProveedor] ([IDTransaccion])
);


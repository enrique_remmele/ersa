﻿CREATE TABLE [dbo].[PedidoNotaCrediroAprobarRechazarDevolucion] (
    [IDTransaccionPedidoNotaCredito]         NUMERIC (18) NOT NULL,
    [IDTransaccionAprobarRechazarDevolucion] NUMERIC (18) NOT NULL,
    CONSTRAINT [PK_PedidoNotaCrediroAprobarRechazarDevolucion] PRIMARY KEY CLUSTERED ([IDTransaccionPedidoNotaCredito] ASC, [IDTransaccionAprobarRechazarDevolucion] ASC),
    CONSTRAINT [FK_PedidoNotaCrediroAprobarRechazarDevolucion_AprobarRechazarDevolucion] FOREIGN KEY ([IDTransaccionAprobarRechazarDevolucion]) REFERENCES [dbo].[AprobarRechazarDevolucion] ([IDTransaccion]),
    CONSTRAINT [FK_PedidoNotaCrediroAprobarRechazarDevolucion_PedidoNotaCrediroAprobarRechazarDevolucion] FOREIGN KEY ([IDTransaccionPedidoNotaCredito]) REFERENCES [dbo].[PedidoNotaCredito] ([IDTransaccion])
);


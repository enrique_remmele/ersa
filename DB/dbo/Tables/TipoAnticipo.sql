﻿CREATE TABLE [dbo].[TipoAnticipo] (
    [ID]               TINYINT      NOT NULL,
    [Descripcion]      VARCHAR (50) NOT NULL,
    [IDCuentaContable] INT          NOT NULL,
    [Estado]           BIT          CONSTRAINT [DF_TipoAnticipo_Estado] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_TipoAnticipo] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TipoAnticipo_CuentaContable] FOREIGN KEY ([IDCuentaContable]) REFERENCES [dbo].[CuentaContable] ([ID])
);


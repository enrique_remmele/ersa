﻿CREATE TABLE [dbo].[DetalleRemision] (
    [IDTransaccion] NUMERIC (18)    NOT NULL,
    [IDProducto]    INT             NOT NULL,
    [ID]            TINYINT         NOT NULL,
    [IDDeposito]    TINYINT         NOT NULL,
    [Observacion]   VARCHAR (50)    NOT NULL,
    [Cantidad]      DECIMAL (10, 2) NOT NULL,
    CONSTRAINT [PK_DetalleRemision] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleRemision_Deposito] FOREIGN KEY ([IDDeposito]) REFERENCES [dbo].[Deposito] ([ID]),
    CONSTRAINT [FK_DetalleRemision_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID]),
    CONSTRAINT [FK_DetalleRemision_Remision] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Remision] ([IDTransaccion])
);


GO
ALTER TABLE [dbo].[DetalleRemision] NOCHECK CONSTRAINT [FK_DetalleRemision_Deposito];


GO
ALTER TABLE [dbo].[DetalleRemision] NOCHECK CONSTRAINT [FK_DetalleRemision_Producto];


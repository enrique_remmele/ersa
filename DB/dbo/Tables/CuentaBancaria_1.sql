﻿CREATE TABLE [dbo].[CuentaBancaria] (
    [ID]                   TINYINT       NOT NULL,
    [IDBanco]              TINYINT       NOT NULL,
    [IDMoneda]             TINYINT       NOT NULL,
    [CuentaBancaria]       VARCHAR (50)  NOT NULL,
    [Nombre]               VARCHAR (50)  NOT NULL,
    [Titulares]            VARCHAR (50)  NULL,
    [Firma1]               VARCHAR (50)  NULL,
    [Firma2]               VARCHAR (50)  NULL,
    [Firma3]               VARCHAR (50)  NULL,
    [CodigoCuentaContable] VARCHAR (50)  NULL,
    [IDTipoCuentaBancaria] TINYINT       NOT NULL,
    [Apertura]             DATE          NULL,
    [Estado]               BIT           CONSTRAINT [DF_CuentaBancaria_Estado] DEFAULT ('True') NOT NULL,
    [Observacion]          VARCHAR (150) NULL,
    [FormatoAlDia]         VARCHAR (50)  NULL,
    [FormatoDiferido]      VARCHAR (50)  NULL,
    CONSTRAINT [PK_CuentaBancaria] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CuentaBancaria_Banco] FOREIGN KEY ([IDBanco]) REFERENCES [dbo].[Banco] ([ID]),
    CONSTRAINT [FK_CuentaBancaria_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_CuentaBancaria_TipoCuentaBancaria] FOREIGN KEY ([IDTipoCuentaBancaria]) REFERENCES [dbo].[TipoCuentaBancaria] ([ID])
);


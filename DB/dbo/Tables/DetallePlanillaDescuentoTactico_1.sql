﻿CREATE TABLE [dbo].[DetallePlanillaDescuentoTactico] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [IDActividad]   INT          NOT NULL,
    CONSTRAINT [PK_DetallePlanillaDescuentoTactico_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDActividad] ASC),
    CONSTRAINT [FK_DetallePlanillaDescuentoTactico_Actividad] FOREIGN KEY ([IDActividad]) REFERENCES [dbo].[Actividad] ([ID]),
    CONSTRAINT [FK_DetallePlanillaDescuentoTactico_PlanillaDescuentoTactico] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[PlanillaDescuentoTactico] ([IDTransaccion])
);


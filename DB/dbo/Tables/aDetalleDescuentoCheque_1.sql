﻿CREATE TABLE [dbo].[aDetalleDescuentoCheque] (
    [IDAuditoria]                  INT          NOT NULL,
    [IDTransaccionDescuentoCheque] NUMERIC (18) NOT NULL,
    [IDTransaccionChequeCliente]   NUMERIC (18) NOT NULL,
    [IDUsuario]                    INT          NOT NULL,
    [Accion]                       VARCHAR (3)  NOT NULL
);


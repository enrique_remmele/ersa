﻿CREATE TABLE [dbo].[aDebitoCreditoBancario] (
    [IDAuditoria]       INT           NOT NULL,
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Debito]            BIT           NOT NULL,
    [Credito]           BIT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Numero]            INT           NOT NULL,
    [NroComprobante]    BIGINT        NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDCuentaBancaria]  TINYINT       NOT NULL,
    [Cotizacion]        MONEY         NULL,
    [Observacion]       VARCHAR (100) NULL,
    [Facturado]         BIT           NOT NULL,
    [Total]             MONEY         NOT NULL,
    [TotalImpuesto]     MONEY         NOT NULL,
    [TotalDiscriminado] MONEY         NOT NULL,
    [Conciliado]        BIT           NULL,
    [EsCobranza]        BIT           NULL,
    [EsProcesado]       BIT           NULL,
    [IDUsuario]         INT           NOT NULL,
    [Accion]            VARCHAR (3)   NOT NULL
);


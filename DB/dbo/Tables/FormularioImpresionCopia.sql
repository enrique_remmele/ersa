﻿CREATE TABLE [dbo].[FormularioImpresionCopia] (
    [IDFormularioImpresion] TINYINT  NOT NULL,
    [ID]                    TINYINT  NOT NULL,
    [PosicionX]             SMALLINT NOT NULL,
    [PosicionY]             SMALLINT NOT NULL,
    CONSTRAINT [PK_FormularioImpresionCopia] PRIMARY KEY CLUSTERED ([IDFormularioImpresion] ASC, [ID] ASC),
    CONSTRAINT [FK_FormularioImpresionCopia_FormularioImpresion] FOREIGN KEY ([IDFormularioImpresion]) REFERENCES [dbo].[FormularioImpresion] ([IDFormularioImpresion])
);


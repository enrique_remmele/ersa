﻿CREATE TABLE [dbo].[PlanCuenta] (
    [ID]            TINYINT      NOT NULL,
    [Descripcion]   VARCHAR (50) NOT NULL,
    [Resolucion173] BIT          NOT NULL,
    [Titular]       BIT          NOT NULL,
    [Estado]        BIT          NULL,
    CONSTRAINT [PK_PlanCuenta] PRIMARY KEY CLUSTERED ([ID] ASC)
);


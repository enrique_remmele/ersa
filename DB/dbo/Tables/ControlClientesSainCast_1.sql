﻿CREATE TABLE [dbo].[ControlClientesSainCast] (
    [FechaOperacion]            DATE             NOT NULL,
    [Codigo]                    VARCHAR (50)     NOT NULL,
    [Fecha]                     DATE             NULL,
    [Latitud]                   DECIMAL (18, 16) NULL,
    [Longitud]                  DECIMAL (18, 16) NULL,
    [Sincronizado]              BIT              NULL,
    [ObservacionSincronizacion] VARCHAR (200)    NULL,
    [Local]                     VARCHAR (50)     NULL,
    [Plataforma]                VARCHAR (50)     NULL,
    CONSTRAINT [PK_ControlClientesSainCast] PRIMARY KEY CLUSTERED ([FechaOperacion] ASC, [Codigo] ASC)
);


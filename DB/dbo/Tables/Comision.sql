﻿CREATE TABLE [dbo].[Comision] (
    [IDTransaccionDetalleVenta] NUMERIC (18)   NOT NULL,
    [IDProducto]                INT            NOT NULL,
    [ID]                        TINYINT        NOT NULL,
    [Fecha]                     DATE           NULL,
    [Porcentaje]                DECIMAL (5, 3) NULL,
    [Monto]                     MONEY          NULL,
    [Venta]                     BIT            NULL,
    [Devolucion]                BIT            NULL,
    CONSTRAINT [PK_Comision] PRIMARY KEY CLUSTERED ([IDTransaccionDetalleVenta] ASC, [IDProducto] ASC, [ID] ASC)
);


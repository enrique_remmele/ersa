﻿CREATE TABLE [dbo].[MotivoAnulacion] (
    [ID]          INT           NULL,
    [IDOperacion] INT           NULL,
    [Descripcion] VARCHAR (100) NULL,
    [Estado]      BIT           NULL,
    [Nombre]      VARCHAR (50)  NULL
);


﻿CREATE TABLE [dbo].[cast_t_orden_carga_det] (
    [OC_NUMERO]                 VARCHAR (50)  NOT NULL,
    [OC_IDSUCURSAL]             TINYINT       NOT NULL,
    [OC_NUMERO_FACTURA]         VARCHAR (50)  NOT NULL,
    [OC_COD_CLIENTE]            INT           NOT NULL,
    [Sincronizado]              BIT           NULL,
    [ObservacionSincronizacion] VARCHAR (200) NULL
);


GO
CREATE Trigger [dbo].[TI_cast_t_orden_carga_det] On [dbo].[cast_t_orden_carga_det]
For Insert, Update

As

Begin

	Set NoCount On  

	--Variables
	Declare @vMensaje varchar(200)
	Declare @vProcesado bit

	Declare @vIDTransaccionLote numeric(18,0)
	Declare @vIDTransaccionVenta numeric(18,0)
	Declare @vID int
	Declare @vImporte money

	Declare @vOC_NUMERO varchar(50) 
	Declare @vOC_IDSUCURSAL int 
	Declare @vOC_NUMERO_FACTURA varchar(50)
	Declare @vOC_COD_CLIENTE int

	--Obtener valores
	Select	@vOC_NUMERO=OC_NUMERO,
				@vOC_IDSUCURSAL=OC_IDSUCURSAL,
				@vOC_NUMERO_FACTURA=OC_NUMERO_FACTURA,
				@vOC_COD_CLIENTE = OC_COD_CLIENTE
	From Inserted

	--IDTransaccion de la factura
	Set @vIDTransaccionVenta = IsNull((Select Top(1) IDTransaccion From VVenta Where Procesado =1 and Anulado = 0 and Comprobante=@vOC_NUMERO_FACTURA and IDCliente = @vOC_COD_CLIENTE), 0)
	Set @vImporte = IsNull((Select Top(1) Total From VVenta Where Procesado =1 and Anulado = 0 and Comprobante=@vOC_NUMERO_FACTURA and IDCliente = @vOC_COD_CLIENTE), 0)

	If @vIDTransaccionVenta = 0 Begin
		Set @vMensaje = 'No se encontro la factura'
		Set @vProcesado = 'False'
		GoTo Salir
	End
	
	if exists(select V.* from VentaLoteDistribucion V join LoteDistribucion L on L.IDTransaccion = V.IDTransaccionLote where V.IDTransaccionVenta = @vIDTransaccionVenta and Isnull(L.Anulado,0) = 0) begin
		Set @vMensaje = 'El comprobante ya tiene una orden de carga asociada'
		Set @vProcesado = 'False'
		GoTo Salir
	end

	--IDTransaccion del Lote
	Set @vIDTransaccionLote = IsNull((Select IDTransaccion From cast_t_orden_carga Where Sincronizado=1 and OC_NUMERO=@vOC_NUMERO and OC_IDSUCURSAL = @vOC_IDSUCURSAL), 0)

	If @vIDTransaccionLote = 0 Begin
		Set @vMensaje = 'No se encontro el registro de cabecera o no fue sincronizado'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	Set @vID= IsNull((Select Max(ID)+1 From VentaLoteDistribucion Where IDTransaccionLote=@vIDTransaccionLote), 1)

	Insert Into VentaLoteDistribucion(IDTransaccionLote,IDTransaccionVenta,ID,Importe)
							   Values(@vIDTransaccionLote,@vIDTransaccionVenta,@vID, @vImporte)
	
	
	Set @vMensaje = 'Registro sincronizado'
	Set @vProcesado = 'True'
	GoTo Salir

Salir:

	If @vProcesado = 'False' Begin
		Update cast_t_orden_carga_det Set Sincronizado='False',
										  ObservacionSincronizacion=@vMensaje
		Where OC_NUMERO = @vOC_NUMERO
		and OC_IDSUCURSAL = @vOC_IDSUCURSAL
		and OC_NUMERO_FACTURA = @vOC_NUMERO_FACTURA
		and OC_COD_CLIENTE = @vOC_COD_CLIENTE
	End

	If @vProcesado = 'True' Begin
		Update cast_t_orden_carga_det Set Sincronizado='True',
										  ObservacionSincronizacion=@vMensaje
		Where OC_NUMERO = @vOC_NUMERO
		and OC_IDSUCURSAL = @vOC_IDSUCURSAL
		and OC_NUMERO_FACTURA = @vOC_NUMERO_FACTURA
		and OC_COD_CLIENTE = @vOC_COD_CLIENTE

		--Si tiene detalle, Eliminar us detalle
		If Exists(Select * From DetalleLoteDistribucion Where IDTransaccion=@vIDTransaccionLote) Begin
			Delete From DetalleLoteDistribucion Where IDTransaccion=@vIDTransaccionLote
		End
		
		--Cargamos el detalle del lote con un cursor de ventas
		--Variables
		Declare @vIDProducto int
		Declare @vCantidad decimal(10, 2)
		Declare @vTotal money
		Declare @vTotalDescuento money
		Declare @vTotalDiscriminado money
		Declare @vCantidadCaja decimal(10, 2)
				 
		--Cursor
		Declare db_cursor cursor for
		Select DV.IDProducto, 
		'Cantidad'=SUM(DV.Cantidad), 
		'Total'=Sum(DV.Total), 
		'TotalDescuento'=Sum(DV.TotalDescuento), 
		'TotalDiscriminado'=Sum(DV.TotalDiscriminado), 
		'CantidadCaja'=SUM(DV.CantidadCaja) 
		From DetalleVenta DV 
		Join VentaLoteDistribucion VLD On DV.IDTransaccion=VLD.IDTransaccionVenta 
		Where VLD.IDTransaccionLote=@vIDTransaccionLote 
		Group By IDProducto
		
		--Abrir
		Open db_cursor   
		fetch next from db_cursor into @vIDProducto, @vCantidad, @vTotal, @vTotalDescuento, @vTotalDiscriminado, @vCantidadCaja
		While @@FETCH_STATUS = 0 Begin  			  

			Insert Into DetalleLoteDistribucion(IDTransaccion, IDProducto, Cantidad, Total, TotalDescuento, TotalDiscriminado, CantidadCaja)		
			Values(@vIDTransaccionLote, @vIDProducto, @vCantidad, @vTotal, @vTotalDescuento, @vTotalDiscriminado, @vCantidadCaja)
			
			fetch next from db_cursor into @vIDProducto, @vCantidad, @vTotal, @vTotalDescuento, @vTotalDiscriminado, @vCantidadCaja
		End   

		Close db_cursor   
		deallocate db_cursor


		--actualizar cabecera
		Declare @vTotalCabecera money = (select sum(importe) from VentaLoteDistribucion where IDTransaccionLote = @vIDTransaccionLote)
		Declare @vTotalContadoCabecera money = (select sum(VLD.importe) from VentaLoteDistribucion VLD join Venta V on VLD.IDTransaccionVenta = V.IDTransaccion where IDTransaccionLote = @vIDTransaccionLote and isnull(V.Credito,0) = 0)
		Declare @vTotalCreditoCabecera money = (select sum(VLD.importe) from VentaLoteDistribucion VLD join Venta V on VLD.IDTransaccionVenta = V.IDTransaccion where IDTransaccionLote = @vIDTransaccionLote and isnull(V.Credito,0) = 1)
		Declare @vTotalDescuentoCabecera money = (select sum(TotalDescuento) from DetalleLoteDistribucion where IDTransaccion = @vIDTransaccionLote)
		Declare @vTotalDiscriminadoCabecera money = (select sum(TotalDiscriminado) from DetalleLoteDistribucion where IDTransaccion = @vIDTransaccionLote)
		Declare @vTotalImpuestoCabecera money =(@vTotalCabecera-@vTotalDiscriminadoCabecera)
		Declare @vCantidadComprobantes int = (select count(*) from (select distinct IDtransaccionVenta from VentaLoteDistribucion where IDTransaccionLote = @vIDTransaccionLote) as a)
		
		Update LoteDistribucion
				Set Total=isnull(@vTotalCabecera,0),
				TotalContado = isnull(@vTotalContadoCabecera,0),
				TotalCredito= isnull(@vTotalCreditoCabecera,0),
				TotalDescuento= isnull(@vTotalDescuentoCabecera,0),
				TotalDiscriminado= isnull(@vTotalDiscriminadoCabecera,0),
				TotalImpuesto= isnull(@vTotalImpuestoCabecera,0),
				CantidadComprobantes = isnull(@vCantidadComprobantes,0)
		where IDTransaccion = @vIDTransaccionLote


	End

End

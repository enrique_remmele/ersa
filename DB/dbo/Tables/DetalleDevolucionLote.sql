﻿CREATE TABLE [dbo].[DetalleDevolucionLote] (
    [IDTransaccion] NUMERIC (18)    NOT NULL,
    [IDProducto]    INT             NOT NULL,
    [Cantidad]      DECIMAL (10, 2) NOT NULL,
    [CantidadCaja]  DECIMAL (10, 2) NULL,
    CONSTRAINT [PK_DetalleDevolucionLote] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC),
    CONSTRAINT [FK_DetalleDevolucionLote_DevolucionLote] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[DevolucionLote] ([IDTransaccion]) ON DELETE CASCADE
);


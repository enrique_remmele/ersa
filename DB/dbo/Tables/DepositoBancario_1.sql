﻿CREATE TABLE [dbo].[DepositoBancario] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Numero]            INT           NOT NULL,
    [NroComprobante]    VARCHAR (50)  NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDCuentaBancaria]  TINYINT       NOT NULL,
    [Cotizacion]        MONEY         NULL,
    [Observacion]       VARCHAR (100) NULL,
    [TotalEfectivo]     MONEY         CONSTRAINT [DF_DepositoBancario_TotalEfectivo] DEFAULT ((0)) NOT NULL,
    [TotalChequeLocal]  MONEY         CONSTRAINT [DF_DepositoBancario_TotalChequeLocal] DEFAULT ((0)) NOT NULL,
    [TotalChequeOtros]  MONEY         CONSTRAINT [DF_DepositoBancario_TotalChequeOtros] DEFAULT ((0)) NOT NULL,
    [TotalDocumento]    MONEY         NULL,
    [Total]             MONEY         CONSTRAINT [DF_DepositoBancario_Total] DEFAULT ((0)) NOT NULL,
    [Procesado]         BIT           CONSTRAINT [DF_DepositoBancario_Procesado] DEFAULT ('False') NOT NULL,
    [Conciliado]        BIT           NULL,
    [DiferenciaCambio]  MONEY         NULL,
    [TotalTarjeta]      MONEY         NULL,
    CONSTRAINT [PK_DepositoBancario] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


GO
CREATE NONCLUSTERED INDEX [missing_index_403_402]
    ON [dbo].[DepositoBancario]([IDSucursal] ASC, [Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_420_419]
    ON [dbo].[DepositoBancario]([IDTipoComprobante] ASC, [Numero] ASC, [NroComprobante] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_418_417]
    ON [dbo].[DepositoBancario]([IDSucursal] ASC)
    INCLUDE([Numero]);


GO
CREATE NONCLUSTERED INDEX [missing_index_406_405]
    ON [dbo].[DepositoBancario]([IDSucursal] ASC, [IDCuentaBancaria] ASC)
    INCLUDE([IDTransaccion], [IDTipoComprobante], [Numero]);


GO
CREATE NONCLUSTERED INDEX [missing_index_408_407]
    ON [dbo].[DepositoBancario]([IDSucursal] ASC)
    INCLUDE([IDTransaccion], [IDTipoComprobante], [Numero], [IDCuentaBancaria]);


GO
CREATE NONCLUSTERED INDEX [missing_index_2582_2581]
    ON [dbo].[DepositoBancario]([Fecha] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_29002_29001]
    ON [dbo].[DepositoBancario]([Fecha] ASC, [IDCuentaBancaria] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [IDTipoComprobante], [Numero], [NroComprobante], [Cotizacion], [Observacion], [TotalEfectivo], [TotalChequeLocal], [TotalChequeOtros], [TotalDocumento], [Total], [Conciliado], [DiferenciaCambio], [TotalTarjeta]);


GO
CREATE NONCLUSTERED INDEX [missing_index_29004_29003]
    ON [dbo].[DepositoBancario]([Fecha] ASC, [IDCuentaBancaria] ASC);


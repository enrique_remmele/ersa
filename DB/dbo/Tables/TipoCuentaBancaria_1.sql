﻿CREATE TABLE [dbo].[TipoCuentaBancaria] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TipoCuentaBancaria] PRIMARY KEY CLUSTERED ([ID] ASC)
);


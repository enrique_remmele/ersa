﻿CREATE TABLE [dbo].[ImpresionSeccion] (
    [IDOperacion]       INT          NOT NULL,
    [IDTipoComprobante] SMALLINT     NOT NULL,
    [ID]                TINYINT      NOT NULL,
    [Descripcion]       VARCHAR (50) NOT NULL,
    [Alto]              INT          NOT NULL,
    [Tabla]             VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ImpresionSeccion] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [IDTipoComprobante] ASC, [ID] ASC),
    CONSTRAINT [FK_ImpresionSeccion_Operacion] FOREIGN KEY ([IDOperacion]) REFERENCES [dbo].[Operacion] ([ID]),
    CONSTRAINT [FK_ImpresionSeccion_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


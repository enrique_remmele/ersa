﻿CREATE TABLE [dbo].[PorcentajeExcesoLineaCredito] (
    [IDUsuario]  INT             NOT NULL,
    [Porcentaje] DECIMAL (18, 2) NOT NULL,
    [Importe]    MONEY           NULL,
    [Estado]     BIT             NULL,
    CONSTRAINT [PK_PorcentajeExcesoLineaCredito] PRIMARY KEY CLUSTERED ([IDUsuario] ASC)
);


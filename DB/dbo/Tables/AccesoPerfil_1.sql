﻿CREATE TABLE [dbo].[AccesoPerfil] (
    [IDMenu]        NUMERIC (18)  NOT NULL,
    [IDPerfil]      INT           NOT NULL,
    [Visualizar]    BIT           CONSTRAINT [DF_AccesoPerfil_Visualizar] DEFAULT ('False') NULL,
    [Agregar]       BIT           CONSTRAINT [DF_AccesoPerfil_Agregar] DEFAULT ('False') NULL,
    [Modificar]     BIT           CONSTRAINT [DF_AccesoPerfil_Modificar] DEFAULT ('False') NULL,
    [Eliminar]      BIT           CONSTRAINT [DF_AccesoPerfil_Eliminar] DEFAULT ('False') NULL,
    [Anular]        BIT           CONSTRAINT [DF_AccesoPerfil_Anular] DEFAULT ('False') NULL,
    [Imprimir]      BIT           CONSTRAINT [DF_AccesoPerfil_Imprimir] DEFAULT ('False') NULL,
    [NombreControl] VARCHAR (100) NULL,
    [Asiento]       BIT           NULL,
    CONSTRAINT [FK_AccesoPerfil_Perfil] FOREIGN KEY ([IDPerfil]) REFERENCES [dbo].[Perfil] ([ID])
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20200901-124932]
    ON [dbo].[AccesoPerfil]([IDPerfil] ASC, [NombreControl] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_400750_400749]
    ON [dbo].[AccesoPerfil]([NombreControl] ASC)
    INCLUDE([IDPerfil], [Visualizar], [Agregar], [Modificar], [Eliminar], [Anular], [Imprimir], [Asiento]);


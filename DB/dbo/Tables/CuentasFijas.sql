﻿CREATE TABLE [dbo].[CuentasFijas] (
    [ID]                       TINYINT NOT NULL,
    [Orden]                    TINYINT NOT NULL,
    [IDOperacion]              INT     NOT NULL,
    [IDTipoOperacion]          TINYINT NULL,
    [IDTipoCuentaFija]         TINYINT NOT NULL,
    [Debe]                     BIT     NULL,
    [Haber]                    BIT     NULL,
    [IDCuentaContable]         INT     NOT NULL,
    [BuscarEnProducto]         BIT     NULL,
    [BuscarEnClienteProveedor] BIT     NULL,
    [EsVenta]                  BIT     NULL,
    [Contado]                  BIT     NULL,
    [Credito]                  BIT     NULL,
    CONSTRAINT [PK_CuentasFijas] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CuentasFijas_CuentaContable] FOREIGN KEY ([IDCuentaContable]) REFERENCES [dbo].[CuentaContable] ([ID]),
    CONSTRAINT [FK_CuentasFijas_Operacion] FOREIGN KEY ([IDOperacion]) REFERENCES [dbo].[Operacion] ([ID]),
    CONSTRAINT [FK_CuentasFijas_TipoCuentaFija] FOREIGN KEY ([IDTipoCuentaFija]) REFERENCES [dbo].[TipoCuentaFija] ([ID])
);


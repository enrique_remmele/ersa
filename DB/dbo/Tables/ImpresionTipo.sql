﻿CREATE TABLE [dbo].[ImpresionTipo] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Texto]       BIT          NOT NULL,
    [Numero]      BIT          NOT NULL,
    [Fecha]       BIT          NOT NULL,
    CONSTRAINT [PK_ImpresionTipo] PRIMARY KEY CLUSTERED ([ID] ASC)
);


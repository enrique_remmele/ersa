﻿CREATE TABLE [dbo].[CFVentaDescuento] (
    [IDOperacion]     TINYINT NOT NULL,
    [ID]              TINYINT NOT NULL,
    [IDTipoDescuento] TINYINT NOT NULL,
    [IDProveedor]     INT     NULL,
    CONSTRAINT [PK_CFVentaDescuento] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


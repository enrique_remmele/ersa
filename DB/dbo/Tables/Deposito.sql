﻿CREATE TABLE [dbo].[Deposito] (
    [ID]                        TINYINT      NOT NULL,
    [IDSucursal]                TINYINT      NOT NULL,
    [Descripcion]               VARCHAR (50) NOT NULL,
    [Estado]                    BIT          CONSTRAINT [DF_Deposito_Estado] DEFAULT ('True') NOT NULL,
    [CuentaContableCombustible] VARCHAR (10) NULL,
    [CuentaContableMercaderia]  VARCHAR (10) NULL,
    [IDTipoDeposito]            TINYINT      NULL,
    [Compra]                    BIT          NULL,
    [DescargaStock]             BIT          NULL,
    [Venta]                     BIT          NULL,
    [ConsumoCombustible]        BIT          CONSTRAINT [DF__Deposito__Consum__2E91A8E5] DEFAULT ('False') NULL,
    [MovimientoMateriaPrima]    BIT          CONSTRAINT [DF__Deposito__Movimi__68BE4A7A] DEFAULT ('false') NULL,
    [DescargaCompra]            BIT          NULL,
    [IDSucursalDestinoTransito] TINYINT      NULL,
    CONSTRAINT [PK_Deposito] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Deposito_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


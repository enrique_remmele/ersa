﻿CREATE TABLE [dbo].[VencimientoChequeDescontado] (
    [IDTransaccion]       NUMERIC (18)  NOT NULL,
    [IDSucursal]          TINYINT       NOT NULL,
    [Numero]              INT           NOT NULL,
    [Fecha]               DATE          NOT NULL,
    [IDTransaccionCheque] NUMERIC (18)  NOT NULL,
    [Cotizacion]          MONEY         NULL,
    [Observacion]         VARCHAR (100) NULL,
    [Anulado]             BIT           NOT NULL,
    [FechaAnulado]        DATE          NULL,
    [IDUsuarioAnulado]    SMALLINT      NULL,
    [Conciliado]          BIT           NULL,
    CONSTRAINT [PK_VencimientoChequeDescontado] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


GO
CREATE NONCLUSTERED INDEX [missing_index_5729_5728]
    ON [dbo].[VencimientoChequeDescontado]([IDSucursal] ASC)
    INCLUDE([Numero]);


GO
CREATE NONCLUSTERED INDEX [missing_index_418768_418767]
    ON [dbo].[VencimientoChequeDescontado]([IDSucursal] ASC, [Numero] ASC);


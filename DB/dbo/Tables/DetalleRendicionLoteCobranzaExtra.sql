﻿CREATE TABLE [dbo].[DetalleRendicionLoteCobranzaExtra] (
    [IDTransaccion] INT          NULL,
    [ID]            INT          NULL,
    [Cliente]       VARCHAR (50) NULL,
    [Comprobante]   VARCHAR (50) NULL,
    [Importe]       MONEY        NULL
);


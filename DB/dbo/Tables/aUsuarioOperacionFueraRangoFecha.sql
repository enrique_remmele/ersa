﻿CREATE TABLE [dbo].[aUsuarioOperacionFueraRangoFecha] (
    [IDUsuario]           INT          NOT NULL,
    [IDOperacion]         INT          NOT NULL,
    [Desde]               DATE         NOT NULL,
    [Hasta]               DATE         NOT NULL,
    [Operacion]           VARCHAR (50) NULL,
    [IDUsuarioOperacion]  INT          NOT NULL,
    [FechaOperacion]      DATETIME     NOT NULL,
    [IDTerminalOperacion] INT          NULL
);


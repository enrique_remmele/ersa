﻿CREATE TABLE [dbo].[ExistenciaDeposito] (
    [IDDeposito]        TINYINT         NOT NULL,
    [IDProducto]        INT             NOT NULL,
    [Existencia]        DECIMAL (10, 2) NOT NULL,
    [ExistenciaMinima]  DECIMAL (10, 2) NULL,
    [ExistenciaCritica] DECIMAL (10, 2) NULL,
    [PlazoReposicion]   TINYINT         NULL,
    CONSTRAINT [PK_ExistenciaDeposito] PRIMARY KEY CLUSTERED ([IDProducto] ASC, [IDDeposito] ASC),
    CONSTRAINT [FK_ExistenciaDeposito_Deposito] FOREIGN KEY ([IDDeposito]) REFERENCES [dbo].[Deposito] ([ID]),
    CONSTRAINT [FK_ExistenciaDeposito_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID])
);


﻿CREATE TABLE [dbo].[DetalleAjusteExistenciaSaldoKardex] (
    [IDTransaccion]            NUMERIC (18)    NOT NULL,
    [IDProducto]               INT             NOT NULL,
    [ExistenciaValorizada]     DECIMAL (10, 2) NOT NULL,
    [ExistenciaKardexAnterior] DECIMAL (10, 2) NOT NULL,
    [CantidadAjuste]           DECIMAL (10, 2) NOT NULL,
    [Costo]                    DECIMAL (18, 6) NOT NULL
);


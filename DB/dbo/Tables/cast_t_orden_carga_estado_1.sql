﻿CREATE TABLE [dbo].[cast_t_orden_carga_estado] (
    [numero_factura]             VARCHAR (50)  NULL,
    [orden_carga]                VARCHAR (50)  NULL,
    [fecha_hora_inicio_descarga] DATETIME      NULL,
    [fecha_hora_fin_descarga]    DATETIME      NULL,
    [estado]                     VARCHAR (50)  NULL,
    [motivo_no_entregado]        VARCHAR (200) NULL,
    [observacion]                VARCHAR (200) NULL
);


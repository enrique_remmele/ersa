﻿CREATE TABLE [dbo].[DetalleFondoFijo] (
    [IDSucursal]                      TINYINT      NOT NULL,
    [IDGrupo]                         TINYINT      NOT NULL,
    [ID]                              NUMERIC (18) NOT NULL,
    [IDTransaccionVale]               NUMERIC (18) NULL,
    [IDTransaccionGasto]              NUMERIC (18) NULL,
    [Importe]                         MONEY        NOT NULL,
    [Diferencia]                      MONEY        NULL,
    [Cancelado]                       BIT          NULL,
    [IDTransaccionRendicionFondoFijo] NUMERIC (18) NULL,
    CONSTRAINT [PK_DetalleFondoFijo] PRIMARY KEY CLUSTERED ([IDSucursal] ASC, [IDGrupo] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleFondoFijo_Gasto] FOREIGN KEY ([IDTransaccionGasto]) REFERENCES [dbo].[Gasto] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleFondoFijo_Grupo] FOREIGN KEY ([IDGrupo]) REFERENCES [dbo].[Grupo] ([ID]),
    CONSTRAINT [FK_DetalleFondoFijo_RendicionFondoFijo] FOREIGN KEY ([IDTransaccionRendicionFondoFijo]) REFERENCES [dbo].[RendicionFondoFijo] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleFondoFijo_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


GO
ALTER TABLE [dbo].[DetalleFondoFijo] NOCHECK CONSTRAINT [FK_DetalleFondoFijo_Gasto];


GO
CREATE NONCLUSTERED INDEX [missing_index_758_757]
    ON [dbo].[DetalleFondoFijo]([IDTransaccionGasto] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_416596_416595]
    ON [dbo].[DetalleFondoFijo]([IDSucursal] ASC, [Cancelado] ASC)
    INCLUDE([IDTransaccionVale], [IDTransaccionGasto]);


﻿CREATE TABLE [dbo].[EquipoConteoUsuario] (
    [IDEquipo]  SMALLINT NOT NULL,
    [IDUsuario] INT      NOT NULL,
    CONSTRAINT [PK_EquipoConteoUsuario] PRIMARY KEY CLUSTERED ([IDEquipo] ASC, [IDUsuario] ASC),
    CONSTRAINT [FK_EquipoConteoUsuario_EquipoConteo] FOREIGN KEY ([IDEquipo]) REFERENCES [dbo].[EquipoConteo] ([ID]),
    CONSTRAINT [FK_EquipoConteoUsuario_Usuario] FOREIGN KEY ([IDUsuario]) REFERENCES [dbo].[Usuario] ([ID])
);


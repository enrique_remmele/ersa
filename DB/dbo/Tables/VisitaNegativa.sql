﻿CREATE TABLE [dbo].[VisitaNegativa] (
    [ID]          SMALLINT     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_VisitaNegativa] PRIMARY KEY CLUSTERED ([ID] ASC)
);


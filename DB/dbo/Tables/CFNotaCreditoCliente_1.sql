﻿CREATE TABLE [dbo].[CFNotaCreditoCliente] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    [Credito]     BIT     NULL,
    [Contado]     BIT     NULL,
    CONSTRAINT [PK_CFNotaCreditoCliente] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


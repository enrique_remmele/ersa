﻿CREATE TABLE [dbo].[UsuarioOperacionFueraRangoFecha] (
    [IDUsuario]   SMALLINT NOT NULL,
    [IDOperacion] INT      NOT NULL,
    [Desde]       DATE     NOT NULL,
    [Hasta]       DATE     NOT NULL,
    CONSTRAINT [PK_UsuarioOperacionFueraRangoFecha] PRIMARY KEY CLUSTERED ([IDUsuario] ASC, [IDOperacion] ASC)
);


﻿CREATE TABLE [dbo].[DetalleMovimiento] (
    [IDTransaccion]     NUMERIC (18)    NOT NULL,
    [IDProducto]        INT             NOT NULL,
    [ID]                TINYINT         NOT NULL,
    [IDDepositoEntrada] TINYINT         NULL,
    [IDDepositoSalida]  TINYINT         NULL,
    [Observacion]       VARCHAR (50)    NOT NULL,
    [Cantidad]          DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario]    MONEY           NOT NULL,
    [Total]             MONEY           NOT NULL,
    [IDImpuesto]        TINYINT         NOT NULL,
    [TotalImpuesto]     MONEY           NOT NULL,
    [TotalDiscriminado] MONEY           NOT NULL,
    [TotalDescuento]    MONEY           CONSTRAINT [DF_DetalleMovimiento_TotalDescuento] DEFAULT ((0)) NULL,
    [Caja]              BIT             NOT NULL,
    [CantidadCaja]      SMALLINT        NOT NULL,
    [Anulado]           BIT             CONSTRAINT [DF_DetalleMovimiento_Anulado] DEFAULT ('False') NOT NULL,
    CONSTRAINT [PK_DetalleMovimiento] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleMovimiento_Movimiento] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Movimiento] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleMovimiento_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID])
);


﻿CREATE TABLE [dbo].[Efectivo] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [ID]                SMALLINT      NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Numero]            INT           NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [Fecha]             DATETIME      NOT NULL,
    [VentasCredito]     MONEY         CONSTRAINT [DF_Efectivo_VentasCredito] DEFAULT ((0)) NOT NULL,
    [VentasContado]     MONEY         CONSTRAINT [DF_Efectivo_VentasContado] DEFAULT ((0)) NOT NULL,
    [Gastos]            MONEY         CONSTRAINT [DF_Efectivo_Gastos] DEFAULT ((0)) NOT NULL,
    [IDMoneda]          TINYINT       NOT NULL,
    [ImporteMoneda]     MONEY         NOT NULL,
    [Cotizacion]        MONEY         NOT NULL,
    [Observacion]       VARCHAR (100) NULL,
    [Depositado]        MONEY         CONSTRAINT [DF_Efectivo_Depositado] DEFAULT ((0)) NULL,
    [Cancelado]         BIT           CONSTRAINT [DF_Efectivo_Cancelado] DEFAULT ('False') NULL,
    [Total]             MONEY         NULL,
    [Saldo]             MONEY         CONSTRAINT [DF_Efectivo_Saldo] DEFAULT ((0)) NULL,
    [ImporteHabilitado] MONEY         NULL,
    [Habilitado]        BIT           CONSTRAINT [DF_Efectivo_Habilitado] DEFAULT ('False') NULL,
    [IDMotivo]          TINYINT       NULL,
    [Anulado]           BIT           CONSTRAINT [DF_Efectivo_Anulado] DEFAULT ('False') NULL,
    [FechaAnulado]      DATETIME      NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    CONSTRAINT [PK_Efectivo_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_Efectivo_FormaPago] FOREIGN KEY ([ID], [IDTransaccion]) REFERENCES [dbo].[FormaPago] ([ID], [IDTransaccion]),
    CONSTRAINT [FK_Efectivo_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID]),
    CONSTRAINT [FK_Efectivo_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


GO
ALTER TABLE [dbo].[Efectivo] NOCHECK CONSTRAINT [FK_Efectivo_FormaPago];


GO
CREATE TRIGGER [dbo].[TICalcularTotalEfectivo] ON  [dbo].[Efectivo]
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;

    Declare @vTotal money
	Declare @vSaldo money
	
	If Exists(Select * From Inserted) And Not Exists(Select * From Deleted) Begin

		Set @vTotal = (Select Cotizacion From Inserted)	* (Select ImporteMoneda From Inserted)	
		Set @vSaldo = (Select Cotizacion From Inserted)	* (Select ImporteMoneda From Inserted)	
		
	End

END

GO
CREATE TRIGGER [dbo].[TIQuitarPuntuaciones] ON  [dbo].[Efectivo]
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;
	/*JGR 20140808 Elimina las puntuaciones en el comprobante*/
    UPDATE Efectivo
	SET 
      Efectivo.Comprobante = LTRIM(RTRIM(REPLACE(Efectivo.Comprobante, '.', '')))
	FROM Efectivo JOIN INSERTED ON Efectivo.IDTransaccion = INSERTED.IDTransaccion
END

﻿CREATE TABLE [dbo].[PedidoNotaCreditoVenta] (
    [IDTransaccionPedidoNotaCredito] NUMERIC (18) NOT NULL,
    [IDTransaccionVentaGenerada]     NUMERIC (18) NOT NULL,
    [ID]                             TINYINT      NOT NULL,
    [Importe]                        MONEY        NOT NULL,
    [cobrado]                        MONEY        NOT NULL,
    [Descontado]                     MONEY        NOT NULL,
    [Saldo]                          MONEY        NOT NULL,
    CONSTRAINT [PK_PedidoNotaCreditoVenta_1] PRIMARY KEY CLUSTERED ([IDTransaccionPedidoNotaCredito] ASC, [IDTransaccionVentaGenerada] ASC, [ID] ASC),
    CONSTRAINT [FK_PedidoNotaCreditoVenta_PedidoNotaCredito] FOREIGN KEY ([IDTransaccionPedidoNotaCredito]) REFERENCES [dbo].[PedidoNotaCredito] ([IDTransaccion]),
    CONSTRAINT [FK_PedidoNotaCreditoVenta_Venta1] FOREIGN KEY ([IDTransaccionVentaGenerada]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_255_254]
    ON [dbo].[PedidoNotaCreditoVenta]([IDTransaccionVentaGenerada] ASC);


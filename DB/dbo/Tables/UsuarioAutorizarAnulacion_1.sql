﻿CREATE TABLE [dbo].[UsuarioAutorizarAnulacion] (
    [ID]                 INT NULL,
    [IDUsuario]          INT NULL,
    [PeriodoFueraDelMes] BIT NULL,
    [Estado]             BIT NULL
);


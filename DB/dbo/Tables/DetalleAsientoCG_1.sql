﻿CREATE TABLE [dbo].[DetalleAsientoCG] (
    [Numero]           INT           NOT NULL,
    [ID]               SMALLINT      NOT NULL,
    [IDCuentaContable] INT           NULL,
    [CuentaContable]   VARCHAR (50)  NULL,
    [Credito]          MONEY         CONSTRAINT [DF_DetalleAsientoCG_Credito] DEFAULT ((0)) NOT NULL,
    [Debito]           MONEY         CONSTRAINT [DF_DetalleAsientoCG_Debito] DEFAULT ((0)) NOT NULL,
    [Importe]          MONEY         CONSTRAINT [DF_DetalleAsientoCG_Importe] DEFAULT ((0)) NULL,
    [Observacion]      VARCHAR (200) NULL,
    [IDSucursal]       TINYINT       NULL,
    [TipoComprobante]  VARCHAR (50)  NULL,
    [NroComprobante]   VARCHAR (50)  NULL,
    [IDCentroCosto]    TINYINT       NULL,
    [IDUnidadNegocio]  TINYINT       NULL,
    CONSTRAINT [PK_DetalleAsientoCG] PRIMARY KEY CLUSTERED ([Numero] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleAsientoCG_AsientoCG] FOREIGN KEY ([Numero]) REFERENCES [dbo].[AsientoCG] ([Numero]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE Trigger [dbo].[Ti_DetalleAsientoCG] On [dbo].[DetalleAsientoCG]

For Insert, Update

As

Begin

	Set NoCount On  
	
	--variables
	Declare @vCuenta varchar(50)
	Declare @vFecha date
	Declare @vNumero numeric(18,0)
	Declare @vID numeric(18,0)
	Declare @vAño smallint
	Declare @vMes tinyint
	Declare @vIDSucursal tinyint
	Declare @vCredito money
	Declare @vDebito money
	Declare @vSaldo money
	Declare @vCreditoAcumulado money
	Declare @vDebitoAcumulado money
	Declare @vSaldoAcumulado money
	
	--Hayar valores
	Begin
	
		Set @vNumero = (Select Numero From Inserted)
		Set @vID = (Select ID From Inserted)
		Set @vCuenta = (Select CuentaContable From Inserted)
		Set @vFecha = (Select A.Fecha From DetalleAsientoCG D Join AsientoCG A On D.Numero=A.Numero Where D.Numero=@vNumero And D.ID=@vID)
		Set @vIDSucursal = (Select IDSucursal From Inserted)
		If @vIDSucursal Is Null Begin
			Set @vIDSucursal = (Select A.IDSucursal From DetalleAsientoCG D Join AsientoCG A On D.Numero=A.Numero Where D.Numero=@vNumero And D.ID=@vID)
		End
		Set @vCredito = IsNull((Select Credito From Inserted),0)
		Set @vDebito = IsNull((Select Debito From Inserted),0)
		
		Set @vAño = Year(@vFecha)
		Set @vMes = Month(@vFecha)
		
	End
	
	--Validar
	Begin
		If @vCuenta Is Null Begin
			Goto Salir
		End
	End
	
	Declare @vCuentaTipo varchar(50)
	Declare @vPrefijo varchar(50)
	
	Set @vPrefijo = SubString(@vCuenta, 0, 2)
	
	--Cuentas del Debe
	If @vPrefijo = '1' Or @vPrefijo = '5' Begin
		Set @vCuentaTipo = 'DEBE'
	End
	
	--Cuentas del Haber
	If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
		Set @vCuentaTipo = 'HABER'
	End
	
	
	--Si no existe, agregamos
	If Not Exists(Select * From PlanCuentaSaldoCG Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal) Begin
	
		--Hayar el saldo
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldo = IsNUll(@vDebito - @vCredito,0)
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldo = Isnull(@vCredito - @vDebito,0)
		End
		
		Insert Into	PlanCuentaSaldoCG(Año, Mes, Cuenta, IDSucursal, Credito, Debito, Saldo)
		Values(@vAño, @vMes, @vCuenta, @vIDSucursal, @vCredito, @vDebito, @vSaldo)
		
	End Else Begin
		
		Set @vCredito = @vCredito + IsNull((Select Sum(Credito) From PlanCuentaSaldoCG Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal),0)
		Set @vDebito = @vDebito + IsNull((Select Sum(Debito) From PlanCuentaSaldoCG Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal),0)
		
		--Hayar el saldo
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldo = @vDebito - @vCredito
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldo = @vCredito - @vDebito
		End
		
		print 'Tipo: ' + convert(varchar(50), @vCuentaTipo)
		print 'Credito: ' + convert(varchar(50), @vCredito)		
		print 'Debito: ' + convert(varchar(50), @vDebito)
		print 'Saldo: ' + convert(varchar(50), @vSaldo)
		
		--Actualizamos
		Update PlanCuentaSaldoCG Set Credito=@vCredito,
									Debito=@vDebito,
									Saldo=@vSaldo
		Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal
		 	
	End
	
	--Calcular los Saldos Acumulados
	Begin	
		Set @vCreditoAcumulado = IsNull((Select Sum(Credito) From PlanCuentaSaldoCG Where Año=@vAño And Mes<=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal),0)
		Set @vDebitoAcumulado = IsNull((Select Sum(Debito) From PlanCuentaSaldoCG Where Año=@vAño And Mes<=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal),0)
		
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldoAcumulado = @vDebitoAcumulado - @vCreditoAcumulado
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldoAcumulado = @vCreditoAcumulado - @vDebitoAcumulado
		End
		
		Update PlanCuentaSaldoCG Set CreditoAcumulado=@vCreditoAcumulado,
									DebitoAcumulado=@vDebitoAcumulado,
									SaldoAcumulado=@vSaldoAcumulado
		Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal
	End
	
	--Actualizar Totales de Asiento
	Begin

		Set @vCredito = IsNull((Select Sum(Credito) From DetalleAsientoCG Where Numero=@vNumero),0)
		Set @vDebito = IsNull((Select Sum(Debito) From DetalleAsientoCG Where Numero=@vNumero),0)
				
		Update AsientoCG Set	Debito=@vDebito,
							Credito=@vCredito,
							Total=@vCredito,
							Saldo=@vCredito-@vDebito
		Where Numero=@vNumero
	End
	
Salir:
	
End

﻿CREATE TABLE [dbo].[Visita] (
    [ID]                 INT           NOT NULL,
    [IDCliente]          INT           NOT NULL,
    [FechaVisita]        DATE          NULL,
    [IDVendedor]         TINYINT       NOT NULL,
    [FechaProximaVisita] DATE          NULL,
    [HoraProximaVisita]  TIME (7)      NULL,
    [Atendido]           VARCHAR (50)  NULL,
    [Comentario]         VARCHAR (254) NULL,
    [Estado]             BIT           NOT NULL,
    [HoraVisita]         TIME (7)      NULL,
    CONSTRAINT [PK_Visita] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Visita_Cliente] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[Cliente] ([ID]),
    CONSTRAINT [FK_Visita_Vendedor] FOREIGN KEY ([IDVendedor]) REFERENCES [dbo].[Vendedor] ([ID])
);


﻿CREATE TABLE [dbo].[ReciboVenta] (
    [IDTransaccionVenta] NUMERIC (18) NOT NULL,
    [IDRecibo]           INT          NOT NULL,
    [NroRecibo]          INT          NOT NULL,
    [Fecha]              DATE         NOT NULL,
    [Anulado]            BIT          CONSTRAINT [DF_ReciboVenta_Anulado] DEFAULT ('False') NOT NULL,
    [IDUsuarioAnulacion] INT          NULL,
    [FechaAnulacion]     DATETIME     NULL,
    [IDUsuario]          INT          NULL,
    [IDTerminal]         INT          NULL,
    CONSTRAINT [FK_ReciboVenta_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion]) NOT FOR REPLICATION
);


GO
ALTER TABLE [dbo].[ReciboVenta] NOCHECK CONSTRAINT [FK_ReciboVenta_Venta];


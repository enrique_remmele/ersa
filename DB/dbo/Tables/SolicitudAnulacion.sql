﻿CREATE TABLE [dbo].[SolicitudAnulacion] (
    [IDTransaccion]        INT      NULL,
    [IDOperacion]          INT      NULL,
    [IDMotivoAnulacion]    INT      NULL,
    [IDUsuarioSolicitante] INT      NULL,
    [FechaSolicitud]       DATETIME NULL,
    [IDUsuarioAprobacion]  INT      NULL,
    [FechaAprobacion]      DATETIME NULL,
    [Estado]               BIT      NULL
);


﻿CREATE TABLE [dbo].[MotivoRemision] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NULL,
    CONSTRAINT [PK_MotivoRemision] PRIMARY KEY CLUSTERED ([ID] ASC)
);


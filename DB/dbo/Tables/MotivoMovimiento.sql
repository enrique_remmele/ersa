﻿CREATE TABLE [dbo].[MotivoMovimiento] (
    [ID]                   TINYINT      NOT NULL,
    [IDTipoMovimiento]     TINYINT      NULL,
    [Descripcion]          VARCHAR (50) NULL,
    [Activo]               BIT          NULL,
    [CodigoCuentaContable] VARCHAR (50) NULL,
    CONSTRAINT [PK_MotivoMovimiento] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MotivoMovimiento_TipoMovimiento] FOREIGN KEY ([IDTipoMovimiento]) REFERENCES [dbo].[TipoOperacion] ([ID])
);


﻿CREATE TABLE [dbo].[Camion] (
    [ID]             INT          NOT NULL,
    [Descripcion]    VARCHAR (50) NOT NULL,
    [Patente]        VARCHAR (10) NULL,
    [Chasis]         VARCHAR (20) NULL,
    [Capacidad]      SMALLINT     NULL,
    [Estado]         BIT          NULL,
    [Abastecimiento] BIT          NULL,
    CONSTRAINT [PK_Camion] PRIMARY KEY CLUSTERED ([ID] ASC)
);


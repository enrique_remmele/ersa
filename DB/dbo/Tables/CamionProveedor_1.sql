﻿CREATE TABLE [dbo].[CamionProveedor] (
    [ID]          INT          NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Patente]     VARCHAR (10) NULL,
    [Chasis]      VARCHAR (20) NULL,
    [Capacidad]   SMALLINT     NULL,
    [Estado]      BIT          NULL,
    CONSTRAINT [PK_CamionProveedor] PRIMARY KEY CLUSTERED ([ID] ASC)
);


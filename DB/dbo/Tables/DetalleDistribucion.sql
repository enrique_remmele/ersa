﻿CREATE TABLE [dbo].[DetalleDistribucion] (
    [IDTransaccionDistribucion] NUMERIC (18) NOT NULL,
    [IDTransaccionVenta]        NUMERIC (18) NOT NULL,
    [IDMoneda]                  TINYINT      NOT NULL,
    [Total]                     MONEY        NOT NULL,
    CONSTRAINT [PK_DetalleDistribucion] PRIMARY KEY CLUSTERED ([IDTransaccionDistribucion] ASC, [IDTransaccionVenta] ASC),
    CONSTRAINT [FK_DetalleDistribucion_Distribucion] FOREIGN KEY ([IDTransaccionDistribucion]) REFERENCES [dbo].[Distribucion] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleDistribucion_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID])
);


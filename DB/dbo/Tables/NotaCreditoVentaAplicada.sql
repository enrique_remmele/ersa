﻿CREATE TABLE [dbo].[NotaCreditoVentaAplicada] (
    [IDTransaccionNotaCreditoAplicacion] NUMERIC (18) NOT NULL,
    [IDTransaccionNotaCredito]           NUMERIC (18) NOT NULL,
    [IDTransaccionVenta]                 NUMERIC (18) NOT NULL,
    [Importe]                            MONEY        NOT NULL,
    [Cobrado]                            MONEY        NOT NULL,
    [Descontado]                         MONEY        NOT NULL,
    [Saldo]                              MONEY        NOT NULL,
    [Transferido]                        BIT          CONSTRAINT [DF_NotaCreditoVentaAplicada_Transferible] DEFAULT ('False') NOT NULL,
    CONSTRAINT [PK_NotaCreditoVentaAplicada] PRIMARY KEY CLUSTERED ([IDTransaccionNotaCreditoAplicacion] ASC, [IDTransaccionNotaCredito] ASC, [IDTransaccionVenta] ASC),
    CONSTRAINT [FK_NotaCreditoVentaAplicada_NotaCredito] FOREIGN KEY ([IDTransaccionNotaCredito]) REFERENCES [dbo].[NotaCredito] ([IDTransaccion]),
    CONSTRAINT [FK_NotaCreditoVentaAplicada_NotaCreditoAplicacion] FOREIGN KEY ([IDTransaccionNotaCreditoAplicacion]) REFERENCES [dbo].[NotaCreditoAplicacion] ([IDTransaccion]),
    CONSTRAINT [FK_NotaCreditoVentaAplicada_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_508_507]
    ON [dbo].[NotaCreditoVentaAplicada]([IDTransaccionVenta] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1293_1292]
    ON [dbo].[NotaCreditoVentaAplicada]([IDTransaccionNotaCredito] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_419898_419897]
    ON [dbo].[NotaCreditoVentaAplicada]([IDTransaccionNotaCredito] ASC)
    INCLUDE([IDTransaccionNotaCreditoAplicacion], [IDTransaccionVenta]);


﻿CREATE TABLE [dbo].[DetalleRendicionLoteCheque] (
    [IDTransaccion]      NUMERIC (18) NOT NULL,
    [ID]                 TINYINT      NOT NULL,
    [Banco]              VARCHAR (50) NOT NULL,
    [NroCheque]          VARCHAR (50) NOT NULL,
    [Fecha]              DATE         NOT NULL,
    [FechaVencimiento]   DATE         NULL,
    [IDTransaccionVenta] NUMERIC (18) NOT NULL,
    [Importe]            MONEY        CONSTRAINT [DF_DetalleRendicionLoteCheque_Importe] DEFAULT ((0)) NOT NULL,
    [Diferido]           BIT          CONSTRAINT [DF_DetalleRendicionLoteCheque_Diferido] DEFAULT ('False') NOT NULL,
    [BancoLocal]         BIT          CONSTRAINT [DF_DetalleRendicionLoteCheque_BancoLocal] DEFAULT ('False') NOT NULL,
    CONSTRAINT [PK_DetalleRendicionLoteCheque] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleRendicionLoteCheque_RendicionLote] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[RendicionLote] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleRendicionLoteCheque_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


﻿CREATE TABLE [dbo].[DetalleDepositoBancario] (
    [IDTransaccionDepositoBancario] NUMERIC (18) NOT NULL,
    [ID]                            INT          NOT NULL,
    [IDTransaccionEfectivo]         NUMERIC (18) NULL,
    [IDEfectivo]                    SMALLINT     NULL,
    [IDTransaccionCheque]           NUMERIC (18) NULL,
    [IDTransaccionDocumento]        NUMERIC (18) NULL,
    [Redondeo]                      BIT          NULL,
    [Faltante]                      BIT          NULL,
    [Sobrante]                      BIT          NULL,
    [Importe]                       MONEY        NULL,
    [IDTransaccionTarjeta]          NUMERIC (18) NULL,
    [IDDetalleDocumento]            INT          NULL,
    CONSTRAINT [PK_DetalleDepositoBancario] PRIMARY KEY CLUSTERED ([IDTransaccionDepositoBancario] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleDepositoBancario_ChequeCliente] FOREIGN KEY ([IDTransaccionCheque]) REFERENCES [dbo].[ChequeCliente] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleDepositoBancario_DepositoBancario] FOREIGN KEY ([IDTransaccionDepositoBancario]) REFERENCES [dbo].[DepositoBancario] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleDepositoBancario_Efectivo] FOREIGN KEY ([IDTransaccionEfectivo], [IDEfectivo]) REFERENCES [dbo].[Efectivo] ([IDTransaccion], [ID])
);


GO
ALTER TABLE [dbo].[DetalleDepositoBancario] NOCHECK CONSTRAINT [FK_DetalleDepositoBancario_ChequeCliente];


GO
ALTER TABLE [dbo].[DetalleDepositoBancario] NOCHECK CONSTRAINT [FK_DetalleDepositoBancario_Efectivo];


GO
CREATE NONCLUSTERED INDEX [missing_index_4054_4053]
    ON [dbo].[DetalleDepositoBancario]([IDTransaccionCheque] ASC)
    INCLUDE([IDTransaccionDepositoBancario]);


GO
CREATE NONCLUSTERED INDEX [missing_index_1957_1956]
    ON [dbo].[DetalleDepositoBancario]([IDTransaccionCheque] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_352_351]
    ON [dbo].[DetalleDepositoBancario]([IDTransaccionDocumento] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_354_353]
    ON [dbo].[DetalleDepositoBancario]([IDTransaccionTarjeta] ASC)
    INCLUDE([IDDetalleDocumento]);


﻿CREATE TABLE [dbo].[GastoFondoFijo] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [Numero]        NUMERIC (18) NULL,
    [IDSucursal]    NUMERIC (18) NULL,
    CONSTRAINT [PK_GastoFondoFijo] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_GastoFondoFijo_Gasto] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Gasto] ([IDTransaccion]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [missing_index_686_685]
    ON [dbo].[GastoFondoFijo]([IDSucursal] ASC)
    INCLUDE([Numero]);


GO
CREATE NONCLUSTERED INDEX [missing_index_688_687]
    ON [dbo].[GastoFondoFijo]([Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_414382_414381]
    ON [dbo].[GastoFondoFijo]([Numero] ASC)
    INCLUDE([IDTransaccion]);


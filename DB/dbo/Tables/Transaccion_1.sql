﻿CREATE TABLE [dbo].[Transaccion] (
    [ID]                NUMERIC (18) NOT NULL,
    [Fecha]             DATETIME     NOT NULL,
    [IDUsuario]         INT          NOT NULL,
    [IDSucursal]        TINYINT      NOT NULL,
    [IDDeposito]        TINYINT      NOT NULL,
    [IDTerminal]        INT          NOT NULL,
    [IDOperacion]       INT          NOT NULL,
    [IDTransaccionCaja] NUMERIC (18) NULL,
    CONSTRAINT [PK_Transaccion] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Transaccion_Usuario] FOREIGN KEY ([IDUsuario]) REFERENCES [dbo].[Usuario] ([ID])
);


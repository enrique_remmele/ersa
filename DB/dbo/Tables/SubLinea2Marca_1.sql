﻿CREATE TABLE [dbo].[SubLinea2Marca] (
    [IDTipoProducto] TINYINT  NOT NULL,
    [IDLinea]        SMALLINT NOT NULL,
    [IDSubLinea]     SMALLINT NOT NULL,
    [IDSubLinea2]    SMALLINT NOT NULL,
    [IDMarca]        TINYINT  NOT NULL,
    CONSTRAINT [PK_SubLinea2Marca] PRIMARY KEY CLUSTERED ([IDTipoProducto] ASC, [IDLinea] ASC, [IDSubLinea] ASC, [IDSubLinea2] ASC, [IDMarca] ASC),
    CONSTRAINT [FK_SubLinea2Marca_Marca] FOREIGN KEY ([IDMarca]) REFERENCES [dbo].[Marca] ([ID]),
    CONSTRAINT [FK_SubLinea2Marca_SubLineaSubLinea2] FOREIGN KEY ([IDTipoProducto], [IDLinea], [IDSubLinea], [IDSubLinea2]) REFERENCES [dbo].[SubLineaSubLinea2] ([IDTipoProducto], [IDLinea], [IDSubLinea], [IDSubLinea2]) ON DELETE CASCADE ON UPDATE CASCADE
);


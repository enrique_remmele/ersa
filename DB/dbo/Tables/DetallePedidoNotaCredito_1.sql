﻿CREATE TABLE [dbo].[DetallePedidoNotaCredito] (
    [IDTransaccion]                 NUMERIC (18)    NOT NULL,
    [IDProducto]                    INT             NOT NULL,
    [ID]                            TINYINT         NOT NULL,
    [IDTransaccionVenta]            NUMERIC (18)    NULL,
    [IDTransaccionPedidoVenta]      NUMERIC (18)    NULL,
    [IDDeposito]                    TINYINT         NOT NULL,
    [Observacion]                   VARCHAR (100)   NULL,
    [IDImpuesto]                    TINYINT         NOT NULL,
    [Cantidad]                      DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario]                MONEY           NOT NULL,
    [CostoUnitario]                 MONEY           NULL,
    [DescuentoUnitario]             MONEY           CONSTRAINT [DF_DetallePedidoNotaCredito_DescuentoUnitario] DEFAULT ((0)) NULL,
    [DescuentoUnitarioDiscriminado] MONEY           NULL,
    [PorcentajeDescuento]           NUMERIC (2)     NULL,
    [Total]                         MONEY           CONSTRAINT [DF_DetallePedidoNotaCredito_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]                 MONEY           CONSTRAINT [DF_DetallePedidoNotaCredito_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalCostoImpuesto]            MONEY           NULL,
    [TotalCosto]                    MONEY           NULL,
    [TotalDescuento]                MONEY           CONSTRAINT [DF_DetallePedidoNotaCredito_TotalDescuento] DEFAULT ((0)) NULL,
    [TotalDiscriminado]             MONEY           NOT NULL,
    [TotalCostoDiscriminado]        MONEY           NULL,
    [TotalDescuentoDiscriminado]    MONEY           NULL,
    [Caja]                          BIT             NULL,
    [CantidadCaja]                  DECIMAL (10, 2) NULL,
    [IDMotivoDevolucion]            INT             NULL,
    CONSTRAINT [PK_DetallePedidoNotaCredito] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_DetallePedidoNotaCredito_PedidoNotaCredito] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[PedidoNotaCredito] ([IDTransaccion]),
    CONSTRAINT [FK_DetallePedidoNotaCredito_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_511_510]
    ON [dbo].[DetallePedidoNotaCredito]([IDTransaccionVenta] ASC);


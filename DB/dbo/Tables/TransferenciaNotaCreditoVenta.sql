﻿CREATE TABLE [dbo].[TransferenciaNotaCreditoVenta] (
    [IDTransaccion]                    NUMERIC (18) NOT NULL,
    [IDTransaccionVenta]               NUMERIC (18) NOT NULL,
    [IDTRansaccionNotaCredito]         NUMERIC (18) NOT NULL,
    [IDTransaccionNotaCreditoAplicada] NUMERIC (18) NOT NULL,
    [Importe]                          MONEY        NOT NULL,
    CONSTRAINT [PK_TransferenciaNotaCreditoVenta_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_TransferenciaNotaCreditoVenta_NotaCredito] FOREIGN KEY ([IDTRansaccionNotaCredito]) REFERENCES [dbo].[NotaCredito] ([IDTransaccion]),
    CONSTRAINT [FK_TransferenciaNotaCreditoVenta_TransferenciaNotaCredito] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[TransferenciaNotaCredito] ([IDTransaccion]),
    CONSTRAINT [FK_TransferenciaNotaCreditoVenta_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


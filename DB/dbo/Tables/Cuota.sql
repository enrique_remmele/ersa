﻿CREATE TABLE [dbo].[Cuota] (
    [IDTransaccion]    NUMERIC (18)  NOT NULL,
    [ID]               TINYINT       NOT NULL,
    [Importe]          MONEY         CONSTRAINT [DF_Cuota_Importe] DEFAULT ((0)) NOT NULL,
    [Saldo]            MONEY         CONSTRAINT [DF_Cuota_Saldo] DEFAULT ((0)) NOT NULL,
    [Vencimiento]      DATE          NOT NULL,
    [Cancelado]        BIT           CONSTRAINT [DF_Cuota_Cancelado] DEFAULT ('False') NOT NULL,
    [Pagar]            BIT           CONSTRAINT [DF_Cuota_Pagar] DEFAULT ('False') NULL,
    [ImporteAPagar]    MONEY         CONSTRAINT [DF_Cuota_ImporteAPagar] DEFAULT ((0)) NULL,
    [IDCuentaBancaria] TINYINT       NULL,
    [ObservacionPago]  VARCHAR (100) NULL,
    CONSTRAINT [PK_Cuota] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_Cuota_Gasto] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Gasto] ([IDTransaccion])
);


GO
ALTER TABLE [dbo].[Cuota] NOCHECK CONSTRAINT [FK_Cuota_Gasto];


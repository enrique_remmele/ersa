﻿CREATE TABLE [dbo].[DetalleProductoCosto] (
    [IDTransaccion] NUMERIC (18)    NOT NULL,
    [IDProducto]    INT             NOT NULL,
    [Observacion]   VARCHAR (50)    NOT NULL,
    [Costo]         DECIMAL (18, 6) NOT NULL,
    [CostoAnterior] DECIMAL (18, 6) NOT NULL,
    [Anulado]       BIT             CONSTRAINT [DF_DetalleProductoCosto_Anulado] DEFAULT ('False') NOT NULL,
    CONSTRAINT [PK_DetalleProductoCosto] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC)
);


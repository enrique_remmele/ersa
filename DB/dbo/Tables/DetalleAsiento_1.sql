﻿CREATE TABLE [dbo].[DetalleAsiento] (
    [IDTransaccion]         NUMERIC (18)  NOT NULL,
    [ID]                    SMALLINT      NOT NULL,
    [IDCuentaContable]      INT           NOT NULL,
    [CuentaContable]        VARCHAR (50)  NOT NULL,
    [Credito]               MONEY         CONSTRAINT [DF_DetalleAsiento_Credito] DEFAULT ((0)) NOT NULL,
    [Debito]                MONEY         CONSTRAINT [DF_DetalleAsiento_Debito] DEFAULT ((0)) NOT NULL,
    [Importe]               MONEY         CONSTRAINT [DF_DetalleAsiento_Importe] DEFAULT ((0)) NULL,
    [Observacion]           VARCHAR (200) NULL,
    [IDSucursal]            TINYINT       NULL,
    [TipoComprobante]       VARCHAR (50)  NULL,
    [NroComprobante]        VARCHAR (50)  NULL,
    [IDCentroCosto]         TINYINT       NULL,
    [IDUnidadNegocio]       TINYINT       NULL,
    [IDDepartamentoEmpresa] INT           CONSTRAINT [DF_DetalleAsiento_IDDepartamentoEmpresa] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_DetalleAsiento_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleAsiento_Asiento] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Asiento] ([IDTransaccion]) ON DELETE CASCADE,
    CONSTRAINT [FK_DetalleAsiento_CuentaContable] FOREIGN KEY ([IDCuentaContable]) REFERENCES [dbo].[CuentaContable] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_DetalleAsiento_CuentaContable]
    ON [dbo].[DetalleAsiento]([CuentaContable] ASC);


GO
CREATE Trigger [dbo].[Ti_DetalleAsiento] On dbo.DetalleAsiento

For Insert, Update

As

Begin

	Set NoCount On  
	
	--variables
	Declare @vCuenta varchar(50)
	Declare @vFecha date
	Declare @vIDTransaccion numeric(18,0)
	Declare @vID numeric(18,0)
	Declare @vAño smallint
	Declare @vMes tinyint
	Declare @vIDSucursal tinyint
	Declare @vCredito money
	Declare @vDebito money
	Declare @vSaldo money = 0
	Declare @vCreditoAcumulado money = 0
	Declare @vDebitoAcumulado money = 0
	Declare @vSaldoAcumulado money = 0
	Declare @vIDCentroCosto tinyint

	--Hayar valores
	Begin
	
	--No anda esta porqueria
	goto Salir

		Select @vIDTransaccion=IDTransaccion,
		@vID = ID,
		@vCuenta = CuentaContable,
		@vIDSucursal = IDSucursal,
		@vCredito = Credito,
		@vDebito = Debito,
		@vIDCentroCosto = IDCentroCosto
		From Inserted
		
		If @vIDSucursal Is Null Begin
			Set @vIDSucursal = (Select Top(1) A.IDSucursal From DetalleAsiento D Join Asiento A On D.IDTransaccion=A.IDTransaccion Where D.IDTransaccion=@vIDTransaccion And D.ID=@vID)
		End
		
		If @vIDCentroCosto Is Null Begin
			Set @vIDCentroCosto =  IsNull((Select Top(1) A.IDCentroCosto From DetalleAsiento D Join Asiento A On D.IDTransaccion=A.IDTransaccion Where D.IDTransaccion=@vIDTransaccion And D.ID=@vID), 1)
		End

		Select	@vFecha = A.Fecha
		From DetalleAsiento D 
		Join Asiento A On D.IDTransaccion=A.IDTransaccion 
		Where D.IDTransaccion=@vIDTransaccion And D.ID=@vID
		
		Set @vCredito = IsNull(@vCredito,0)
		Set @vDebito = IsNull(@vDebito,0)
		
		Set @vAño = Year(@vFecha)
		Set @vMes = Month(@vFecha)
		
	End
	
	--Validar
	Begin
		If @vCuenta Is Null Begin
			Goto Salir
		End
	End
	
	Declare @vCuentaTipo varchar(50)
	Declare @vPrefijo varchar(50)
	
	Set @vPrefijo = SubString(@vCuenta, 0, 2)
	
	--Cuentas del Debe
	If @vPrefijo = '1' Or @vPrefijo = '5' Begin
		Set @vCuentaTipo = 'DEBE'
	End
	
	--Cuentas del Haber
	If @vPrefijo = '2' Or @vPrefijo = '3' Or @vPrefijo = '4' Begin
		Set @vCuentaTipo = 'HABER'
	End
	
	
	--Si no existe, agregamos
	If Not Exists(Select Top(1) * From PlanCuentaSaldo Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto) Begin
	
		--Hayar el saldo
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldo = IsNUll(@vDebito - @vCredito,0)
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldo = Isnull(@vCredito - @vDebito,0)
		End
		
		Insert Into	PlanCuentaSaldo(Año, Mes, Cuenta, IDSucursal, Credito, Debito, Saldo, IDCentroCosto)
		Values(@vAño, @vMes, @vCuenta, @vIDSucursal, @vCredito, @vDebito, @vSaldo, @vIDCentroCosto)
		
	End Else Begin
		
		Set @vCredito = @vCredito + IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto),0)
		Set @vDebito = @vDebito + IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto),0)
		
		--Hayar el saldo
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldo = @vDebito - @vCredito
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldo = @vCredito - @vDebito
		End
		
		--Actualizamos
		Update PlanCuentaSaldo Set Credito=@vCredito,
									Debito=@vDebito,
									Saldo=@vSaldo
		Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto
		 	
	End
	
	--Calcular los Saldos Acumulados
	Begin	
		Set @vCreditoAcumulado = IsNull((Select Sum(Credito) From PlanCuentaSaldo Where Año=@vAño And Mes<=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto),0)
		Set @vDebitoAcumulado = IsNull((Select Sum(Debito) From PlanCuentaSaldo Where Año=@vAño And Mes<=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto),0)
		
		If @vCuentaTipo = 'DEBE' Begin
			Set @vSaldoAcumulado = @vDebitoAcumulado - @vCreditoAcumulado
		End
		
		If @vCuentaTipo = 'HABER' Begin
			Set @vSaldoAcumulado = @vCreditoAcumulado - @vDebitoAcumulado
		End
		
		Update PlanCuentaSaldo Set CreditoAcumulado=@vCreditoAcumulado,
									DebitoAcumulado=@vDebitoAcumulado,
									SaldoAcumulado=@vSaldoAcumulado
		Where Año=@vAño And Mes=@vMes And Cuenta=@vCuenta And IDSucursal=@vIDSucursal And IDCentroCosto=@vIDCentroCosto
	End
	
	--Actualizar Totales de Asiento
	Begin

		Set @vCredito = IsNull((Select Sum(Credito) From DetalleAsiento Where IDTransaccion=@vIDTransaccion),0)
		Set @vDebito = IsNull((Select Sum(Debito) From DetalleAsiento Where IDTransaccion=@vIDTransaccion),0)
				
		Update Asiento Set	Debito=@vDebito,
							Credito=@vCredito,
							Saldo=@vCredito-@vDebito
		Where IDTransaccion=@vIDTransaccion
	End
	
Salir:
	
End

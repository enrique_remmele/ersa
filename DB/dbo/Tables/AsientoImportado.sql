﻿CREATE TABLE [dbo].[AsientoImportado] (
    [IDTransaccion] NUMERIC (18)  NOT NULL,
    [IDSucursal]    TINYINT       NOT NULL,
    [Numero]        NUMERIC (18)  NOT NULL,
    [Fecha]         DATE          NOT NULL,
    [Cotizacion]    MONEY         NULL,
    [Observacion]   VARCHAR (100) NULL,
    [Conciliado]    BIT           NULL,
    [Operacion]     VARCHAR (50)  NULL,
    [Total]         MONEY         NULL,
    CONSTRAINT [PK_AsientoImportado] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


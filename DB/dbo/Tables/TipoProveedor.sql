﻿CREATE TABLE [dbo].[TipoProveedor] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_TipoProveedor_Estado] DEFAULT ('True') NULL,
    [Referencia]  VARCHAR (50) NULL,
    CONSTRAINT [PK_TipoProveedor] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[Sucursal] (
    [ID]                 TINYINT       NOT NULL,
    [Descripcion]        VARCHAR (50)  NOT NULL,
    [Codigo]             VARCHAR (5)   NULL,
    [IDPais]             TINYINT       NULL,
    [IDDepartamento]     TINYINT       NULL,
    [IDCiudad]           SMALLINT      NULL,
    [Direccion]          VARCHAR (100) NULL,
    [Telefono]           VARCHAR (20)  NULL,
    [Referencia]         VARCHAR (5)   NULL,
    [Estado]             BIT           NULL,
    [CodigoDistribuidor] VARCHAR (50)  NULL,
    [CuentaContable]     VARCHAR (50)  NULL,
    [VentaSinPedido]     BIT           NULL,
    CONSTRAINT [PK_Sucursal] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Sucursal_Ciudad] FOREIGN KEY ([IDCiudad]) REFERENCES [dbo].[Ciudad] ([ID]),
    CONSTRAINT [FK_Sucursal_Departamento] FOREIGN KEY ([IDDepartamento]) REFERENCES [dbo].[Departamento] ([ID]),
    CONSTRAINT [FK_Sucursal_Pais] FOREIGN KEY ([IDPais]) REFERENCES [dbo].[Pais] ([ID])
);


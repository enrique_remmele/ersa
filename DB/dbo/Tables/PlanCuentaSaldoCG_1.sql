﻿CREATE TABLE [dbo].[PlanCuentaSaldoCG] (
    [Año]              SMALLINT     NOT NULL,
    [Mes]              TINYINT      NOT NULL,
    [Cuenta]           VARCHAR (50) NOT NULL,
    [IDSucursal]       TINYINT      NOT NULL,
    [Debito]           MONEY        CONSTRAINT [DF_PlanCuentaSaldoCG_Debito] DEFAULT ((0)) NOT NULL,
    [Credito]          MONEY        CONSTRAINT [DF_PlanCuentaSaldoCG_Credito] DEFAULT ((0)) NOT NULL,
    [Saldo]            MONEY        CONSTRAINT [DF_PlanCuentaSaldoCG_Saldo] DEFAULT ((0)) NOT NULL,
    [DebitoAcumulado]  MONEY        CONSTRAINT [DF_PlanCuentaSaldoCG_DebitoAcumulado] DEFAULT ((0)) NOT NULL,
    [CreditoAcumulado] MONEY        CONSTRAINT [DF_PlanCuentaSaldoCG_CreditoAcumulado] DEFAULT ((0)) NOT NULL,
    [SaldoAcumulado]   MONEY        CONSTRAINT [DF_PlanCuentaSaldoCG_SaldoAcumulado] DEFAULT ((0)) NOT NULL,
    [IDCentroCosto]    TINYINT      NULL,
    CONSTRAINT [PK_PlanCuentaSaldoCG_1] PRIMARY KEY CLUSTERED ([Año] ASC, [Mes] ASC, [Cuenta] ASC, [IDSucursal] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PlanCuentaSaldoCG_IDCentroCosto]
    ON [dbo].[PlanCuentaSaldoCG]([IDCentroCosto] ASC);


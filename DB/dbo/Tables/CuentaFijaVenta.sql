﻿CREATE TABLE [dbo].[CuentaFijaVenta] (
    [ID]                TINYINT      NOT NULL,
    [IDCuentaContable]  SMALLINT     NOT NULL,
    [Debe]              BIT          NULL,
    [Haber]             BIT          NULL,
    [IDTipoComprobante] SMALLINT     NULL,
    [IDMoneda]          TINYINT      NOT NULL,
    [IDTipoCuentaFija]  TINYINT      NOT NULL,
    [Orden]             TINYINT      NOT NULL,
    [Descripcion]       VARCHAR (50) NOT NULL,
    [BuscarProveedor]   BIT          NOT NULL,
    CONSTRAINT [PK_CuentaFijaVenta] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[DetalleActividad] (
    [IDActividad] INT             NOT NULL,
    [ID]          SMALLINT        NOT NULL,
    [IDProducto]  INT             NOT NULL,
    [Cantidad]    DECIMAL (10, 2) NULL,
    [Total]       MONEY           NULL,
    CONSTRAINT [PK_DetalleActividad_1] PRIMARY KEY CLUSTERED ([IDActividad] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleActividad_Actividad] FOREIGN KEY ([IDActividad]) REFERENCES [dbo].[Actividad] ([ID]),
    CONSTRAINT [FK_DetalleActividad_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID])
);


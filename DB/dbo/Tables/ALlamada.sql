﻿CREATE TABLE [dbo].[ALlamada] (
    [ID]                INT           NOT NULL,
    [IDCliente]         INT           NOT NULL,
    [Fecha]             DATETIME      NULL,
    [IDTipoLlamada]     INT           NOT NULL,
    [IDVendedor]        SMALLINT      NULL,
    [IDResultado]       INT           NOT NULL,
    [LlamarFecha]       DATE          NULL,
    [LlamarHora]        TIME (7)      NULL,
    [Atendido]          VARCHAR (50)  NULL,
    [Comentario]        VARCHAR (254) NULL,
    [Estado]            BIT           NOT NULL,
    [Realizado]         BIT           NULL,
    [Entrante]          BIT           NULL,
    [Saliente]          BIT           NULL,
    [IDUsuario]         INT           NULL,
    [Modificacion]      VARCHAR (20)  NULL,
    [FechaModificacion] DATETIME      NOT NULL
);


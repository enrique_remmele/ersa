﻿CREATE TABLE [dbo].[Meta] (
    [ID]             INT     NOT NULL,
    [Mes]            INT     NOT NULL,
    [Año]            INT     NOT NULL,
    [IDSucursal]     TINYINT NOT NULL,
    [IDVendedor]     TINYINT NOT NULL,
    [IDTipoProducto] TINYINT NOT NULL,
    [IDUnidadMedida] TINYINT NOT NULL,
    [IDProducto]     INT     NOT NULL,
    [IDListaPrecio]  INT     NOT NULL,
    [Importe]        MONEY   NOT NULL,
    [Estado]         BIT     NOT NULL,
    [metaKg]         MONEY   NULL,
    CONSTRAINT [PK_Meta] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Meta_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID]),
    CONSTRAINT [FK_Meta_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_Meta_TipoProducto] FOREIGN KEY ([IDTipoProducto]) REFERENCES [dbo].[TipoProducto] ([ID]),
    CONSTRAINT [FK_Meta_UnidadMedida] FOREIGN KEY ([IDUnidadMedida]) REFERENCES [dbo].[UnidadMedida] ([ID]),
    CONSTRAINT [FK_Meta_Vendedor] FOREIGN KEY ([IDVendedor]) REFERENCES [dbo].[Vendedor] ([ID])
);


﻿CREATE TABLE [dbo].[NotaCreditoAplicacion] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [Numero]            INT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [NroComprobante]    VARCHAR (100) NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [IDCliente]         INT           NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDMoneda]          TINYINT       NOT NULL,
    [Cotizacion]        MONEY         NOT NULL,
    [Anulado]           BIT           CONSTRAINT [DF_NotaCreditoAplicacion_Anulado] DEFAULT ('False') NULL,
    [FechaAnulado]      DATE          NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    [Observacion]       VARCHAR (200) NULL,
    [Total]             MONEY         NULL,
    CONSTRAINT [PK_NotaCreditoAplicacion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


GO
CREATE NONCLUSTERED INDEX [missing_index_89_88]
    ON [dbo].[NotaCreditoAplicacion]([Numero] ASC, [IDSucursal] ASC);


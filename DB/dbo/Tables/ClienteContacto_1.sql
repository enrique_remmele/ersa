﻿CREATE TABLE [dbo].[ClienteContacto] (
    [IDCliente]  INT          NOT NULL,
    [ID]         TINYINT      NOT NULL,
    [Nombres]    VARCHAR (50) NOT NULL,
    [Cargo]      VARCHAR (50) NULL,
    [Telefono]   VARCHAR (50) NULL,
    [Email]      VARCHAR (50) NULL,
    [Cumpleaños] DATE         NULL,
    CONSTRAINT [PK_ClienteContacto] PRIMARY KEY CLUSTERED ([IDCliente] ASC, [ID] ASC),
    CONSTRAINT [FK_ClienteContacto_Cliente] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[Cliente] ([ID])
);


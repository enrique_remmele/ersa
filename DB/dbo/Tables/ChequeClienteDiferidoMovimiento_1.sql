﻿CREATE TABLE [dbo].[ChequeClienteDiferidoMovimiento] (
    [ID]              TINYINT      NOT NULL,
    [fecha]           DATE         NOT NULL,
    [Dia]             NCHAR (10)   NULL,
    [Ingreso]         NUMERIC (18) NULL,
    [Deposito]        NUMERIC (18) NULL,
    [DescuentoCheque] NUMERIC (18) NULL,
    [Saldo]           NUMERIC (18) NOT NULL,
    [IDSucursal]      TINYINT      NULL,
    [Condicion]       VARCHAR (50) NULL
);


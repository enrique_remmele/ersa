﻿CREATE TABLE [dbo].[ZonaVenta] (
    [ID]            SMALLINT      NOT NULL,
    [Descripcion]   VARCHAR (50)  NULL,
    [Estado]        BIT           CONSTRAINT [DF_ZonaVenta_Estado] DEFAULT ('True') NULL,
    [Orden]         SMALLINT      CONSTRAINT [DF_ZonaVenta_Orden] DEFAULT ((0)) NULL,
    [Poligono]      VARCHAR (MAX) NULL,
    [ColorPoligono] VARCHAR (50)  NULL,
    [Referencia]    VARCHAR (10)  NULL,
    [IDSucursal]    TINYINT       NULL,
    [IDArea]        TINYINT       NULL,
    CONSTRAINT [PK_ZonaVenta] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[AprobarRechazarDevolucion] (
    [IDTransaccion] NUMERIC (18)  NOT NULL,
    [IDSucursal]    INT           NULL,
    [Numero]        INT           NULL,
    [Fecha]         DATE          NULL,
    [Destino]       VARCHAR (50)  NULL,
    [Aprobado]      BIT           NULL,
    [Observacion]   VARCHAR (500) NULL,
    CONSTRAINT [PK_AprobarRechazarDevolucion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


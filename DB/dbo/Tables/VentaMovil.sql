﻿CREATE TABLE [dbo].[VentaMovil] (
    [IDTransaccion]       NUMERIC (18) NOT NULL,
    [IDSucursal]          TINYINT      NOT NULL,
    [IDDeposito]          TINYINT      NOT NULL,
    [IDTerminal]          INT          NULL,
    [IDTransaccionOrigen] NUMERIC (18) NOT NULL,
    [Fecha]               DATETIME     NOT NULL,
    [IDUsuario]           INT          NOT NULL,
    [IDPuntoExpedicion]   INT          NULL,
    [Comprobante]         VARCHAR (50) NULL,
    [IDCliente]           INT          NULL,
    [Total]               MONEY        NULL,
    [FechaEmision]        DATETIME     NULL,
    CONSTRAINT [PK_VentaMovil] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDSucursal] ASC, [IDDeposito] ASC)
);


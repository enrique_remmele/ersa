﻿CREATE TABLE [dbo].[PrestamoBancario] (
    [IDTransaccion]                      NUMERIC (18)   NOT NULL,
    [Numero]                             INT            NOT NULL,
    [IDTipoComprobante]                  SMALLINT       NOT NULL,
    [NroComprobante]                     VARCHAR (50)   NOT NULL,
    [IDSucursal]                         TINYINT        NOT NULL,
    [Fecha]                              DATE           NOT NULL,
    [IDCuentaBancaria]                   TINYINT        NOT NULL,
    [Cotizacion]                         MONEY          NULL,
    [Observacion]                        VARCHAR (500)  NULL,
    [PlazoDias]                          INT            NOT NULL,
    [PorcentajeInteres]                  NUMERIC (4, 2) NOT NULL,
    [Capital]                            MONEY          NOT NULL,
    [Interes]                            MONEY          NOT NULL,
    [ImpuestoInteres]                    MONEY          NULL,
    [Gastos]                             MONEY          NULL,
    [Neto]                               MONEY          NOT NULL,
    [Anulado]                            BIT            NULL,
    [IDTransaccionDebitoCreditoBancario] NUMERIC (18)   NULL,
    [TipoGarantia]                       VARCHAR (50)   NULL,
    CONSTRAINT [PK_PrestamoBancario] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_PrestamoBancario_CuentaBancaria] FOREIGN KEY ([IDCuentaBancaria]) REFERENCES [dbo].[CuentaBancaria] ([ID]),
    CONSTRAINT [FK_PrestamoBancario_DebitoCreditoBancario] FOREIGN KEY ([IDTransaccionDebitoCreditoBancario]) REFERENCES [dbo].[DebitoCreditoBancario] ([IDTransaccion]) ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT [FK_PrestamoBancario_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_PrestamoBancario_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


GO
ALTER TABLE [dbo].[PrestamoBancario] NOCHECK CONSTRAINT [FK_PrestamoBancario_DebitoCreditoBancario];


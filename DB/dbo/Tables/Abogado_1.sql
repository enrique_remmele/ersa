﻿CREATE TABLE [dbo].[Abogado] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Referencia]  VARCHAR (5)  NULL,
    [Estado]      BIT          CONSTRAINT [DF_Abogado_Estado] DEFAULT ('True') NULL,
    [IDSucursal]  TINYINT      NULL,
    CONSTRAINT [PK_Abogado] PRIMARY KEY CLUSTERED ([ID] ASC)
);


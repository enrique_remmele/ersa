﻿CREATE TABLE [dbo].[DetalleVentaTemporal] (
    [IDTransaccion]          NUMERIC (18) NOT NULL,
    [IDProducto]             INT          NOT NULL,
    [ID]                     TINYINT      NOT NULL,
    [CostoUnitario]          MONEY        NOT NULL,
    [TotalCosto]             MONEY        NOT NULL,
    [TotalCostoImpuesto]     MONEY        NOT NULL,
    [TotalCostoDiscriminado] MONEY        NOT NULL,
    CONSTRAINT [PK_DetalleVentaTemporal] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC)
);


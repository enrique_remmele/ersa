﻿CREATE TABLE [dbo].[ZonaDeposito] (
    [ID]          INT          NOT NULL,
    [IDSucursal]  TINYINT      NOT NULL,
    [IDDeposito]  SMALLINT     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_ZonaDeposito_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_ZonaDeposito] PRIMARY KEY CLUSTERED ([ID] ASC)
);


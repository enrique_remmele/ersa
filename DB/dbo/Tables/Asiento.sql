﻿CREATE TABLE [dbo].[Asiento] (
    [IDTransaccion]       NUMERIC (18)  NOT NULL,
    [Numero]              INT           NOT NULL,
    [IDSucursal]          TINYINT       NOT NULL,
    [Fecha]               DATE          NOT NULL,
    [IDMoneda]            TINYINT       NOT NULL,
    [Cotizacion]          MONEY         NULL,
    [IDTipoComprobante]   SMALLINT      NOT NULL,
    [NroComprobante]      VARCHAR (50)  NOT NULL,
    [Detalle]             VARCHAR (500) NOT NULL,
    [Total]               MONEY         CONSTRAINT [DF_Asiento_Total] DEFAULT ((0)) NOT NULL,
    [Debito]              MONEY         CONSTRAINT [DF_Asiento_Debito] DEFAULT ((0)) NOT NULL,
    [Credito]             MONEY         CONSTRAINT [DF_Asiento_Credito] DEFAULT ((0)) NOT NULL,
    [Saldo]               MONEY         CONSTRAINT [DF_Asiento_Saldo] DEFAULT ((0)) NOT NULL,
    [Anulado]             BIT           NULL,
    [IDCentroCosto]       TINYINT       NULL,
    [Conciliado]          BIT           NULL,
    [FechaConciliado]     DATE          NULL,
    [IDUsuarioConciliado] INT           NULL,
    [Bloquear]            BIT           CONSTRAINT [DF_Asiento_Bloquear] DEFAULT ('False') NULL,
    [Descripcion]         VARCHAR (500) NULL,
    [IDUnidadNegocio]     TINYINT       NULL,
    CONSTRAINT [PK_Asiento] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_Asiento_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_Asiento_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_Asiento_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Asiento_IDTipoComprobante]
    ON [dbo].[Asiento]([IDTipoComprobante] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Asiento_Numero]
    ON [dbo].[Asiento]([Numero] ASC);


﻿CREATE TABLE [dbo].[CFDepositoBancario] (
    [IDOperacion]            TINYINT NOT NULL,
    [ID]                     TINYINT NOT NULL,
    [BuscarEnCuentaBancaria] BIT     NULL,
    [Efectivo]               BIT     NULL,
    [ChequeAlDia]            BIT     NULL,
    [ChequeDiferido]         BIT     NULL,
    [ChequeRechazado]        BIT     NULL,
    [TipoCuenta]             BIT     NULL,
    [DiferenciaCambio]       BIT     NULL,
    [Documento]              BIT     CONSTRAINT [DF__CFDeposit__Docum__0DEFCF29] DEFAULT ('False') NULL,
    CONSTRAINT [PK_CFDepositoBancario] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[DepartamentoEmpresa] (
    [ID]            INT          NULL,
    [Departamento]  VARCHAR (50) NULL,
    [IDUsuarioJefe] INT          NULL,
    [IDSucursal]    TINYINT      NULL
);


﻿CREATE TABLE [dbo].[TipoCuentaFija] (
    [ID]               TINYINT      NOT NULL,
    [Descripcion]      VARCHAR (50) NULL,
    [Impuesto]         BIT          NULL,
    [IDImpuesto]       TINYINT      NULL,
    [Producto]         BIT          NULL,
    [ProductoCosto]    BIT          NULL,
    [Total]            BIT          NULL,
    [Descuento]        BIT          NULL,
    [IncluirImpuesto]  BIT          NULL,
    [IncluirDescuento] BIT          NULL,
    [Campo]            VARCHAR (50) NULL,
    CONSTRAINT [PK_DetalleTipoCuentaFija] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TipoCuentaFija_Impuesto] FOREIGN KEY ([IDImpuesto]) REFERENCES [dbo].[Impuesto] ([ID])
);


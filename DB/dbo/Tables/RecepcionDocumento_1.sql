﻿CREATE TABLE [dbo].[RecepcionDocumento] (
    [IDTransaccion]  INT           NULL,
    [NombreRecibido] VARCHAR (80)  NULL,
    [FechaRecibido]  DATE          NULL,
    [Observacion]    VARCHAR (100) NULL,
    [IDUsuario]      INT           NULL,
    [FechaRegistro]  DATETIME      NULL
);


﻿CREATE TABLE [dbo].[DetalleMacheo] (
    [IDTransaccionMacheo] NUMERIC (18) NOT NULL,
    [ID]                  TINYINT      NOT NULL,
    [IDTransaccionTicket] NUMERIC (18) NOT NULL,
    [Anulado]             BIT          CONSTRAINT [DF_Macheo_Anulado] DEFAULT ('False') NOT NULL,
    CONSTRAINT [PK_DetalleMacheo] PRIMARY KEY CLUSTERED ([IDTransaccionMacheo] ASC, [IDTransaccionTicket] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleMacheo_MacheoFacturaTicket] FOREIGN KEY ([IDTransaccionMacheo]) REFERENCES [dbo].[MacheoFacturaTicket] ([IDTransaccion])
);


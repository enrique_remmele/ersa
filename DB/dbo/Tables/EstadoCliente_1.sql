﻿CREATE TABLE [dbo].[EstadoCliente] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NULL,
    [Orden]       TINYINT      NULL,
    [Estado]      BIT          NULL,
    [Codigo]      VARCHAR (10) NULL,
    CONSTRAINT [PK_EstadoCliente] PRIMARY KEY CLUSTERED ([ID] ASC)
);


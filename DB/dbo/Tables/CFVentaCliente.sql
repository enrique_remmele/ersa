﻿CREATE TABLE [dbo].[CFVentaCliente] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    [Credito]     BIT     NULL,
    [Contado]     BIT     NULL,
    CONSTRAINT [PK_CFVentaCliente] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


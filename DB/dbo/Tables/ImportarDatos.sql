﻿CREATE TABLE [dbo].[ImportarDatos] (
    [ID]            TINYINT       NOT NULL,
    [Descripcion]   VARCHAR (50)  NULL,
    [Scrip]         VARCHAR (50)  NULL,
    [Estructura]    VARCHAR (MAX) NULL,
    [Consulta]      VARCHAR (MAX) NULL,
    [Varios]        BIT           NULL,
    [EsTransaccion] BIT           CONSTRAINT [DF_ImportarDatos_EsTransaccion] DEFAULT ('False') NULL,
    [IDOperacion]   INT           NULL,
    CONSTRAINT [PK_ImportarDatos] PRIMARY KEY CLUSTERED ([ID] ASC)
);


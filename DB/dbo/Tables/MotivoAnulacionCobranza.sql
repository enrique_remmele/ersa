﻿CREATE TABLE [dbo].[MotivoAnulacionCobranza] (
    [ID]          INT           NOT NULL,
    [Descripcion] VARCHAR (100) NOT NULL,
    [Estado]      BIT           NULL
);


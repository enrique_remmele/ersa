﻿CREATE TABLE [dbo].[Efectivizacion] (
    [IDTransaccion]       NUMERIC (18) NOT NULL,
    [IDSucursal]          TINYINT      NOT NULL,
    [IDTipoComprobante]   SMALLINT     NOT NULL,
    [Numero]              INT          NOT NULL,
    [NroComprobante]      INT          NOT NULL,
    [Fecha]               DATE         NULL,
    [IDTransaccionCheque] NUMERIC (18) NULL,
    [Anulado]             BIT          NULL,
    [FechaAnulado]        DATE         NULL,
    [IDUsuarioAnulado]    SMALLINT     NULL,
    CONSTRAINT [PK_EfectivizacionEfectivo] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_EfectivizacionEfectivo_ChequeCliente] FOREIGN KEY ([IDTransaccionCheque]) REFERENCES [dbo].[ChequeCliente] ([IDTransaccion])
);


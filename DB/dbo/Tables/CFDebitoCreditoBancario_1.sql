﻿CREATE TABLE [dbo].[CFDebitoCreditoBancario] (
    [IDOperacion]            TINYINT NOT NULL,
    [ID]                     TINYINT NOT NULL,
    [BuscarEnCuentaBancaria] BIT     NULL,
    CONSTRAINT [PK_CFDebitoCreditoBancario] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


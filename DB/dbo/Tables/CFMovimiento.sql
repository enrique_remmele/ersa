﻿CREATE TABLE [dbo].[CFMovimiento] (
    [IDOperacion]      TINYINT NOT NULL,
    [ID]               TINYINT NOT NULL,
    [IDTipoOperacion]  TINYINT NULL,
    [IDTipoCuentaFija] TINYINT NULL,
    [BuscarEnProducto] BIT     NULL,
    [BuscarEnDeposito] BIT     NULL,
    CONSTRAINT [PK_CFMovimiento] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


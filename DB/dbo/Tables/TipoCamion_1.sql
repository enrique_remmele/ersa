﻿CREATE TABLE [dbo].[TipoCamion] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TipoCamion] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[aProductoListaPrecioExcepciones] (
    [IDProducto]          INT            NOT NULL,
    [IDListaPrecio]       INT            NOT NULL,
    [IDCliente]           INT            NOT NULL,
    [IDSucursal]          TINYINT        NOT NULL,
    [IDTipoDescuento]     TINYINT        NOT NULL,
    [IDMoneda]            TINYINT        NOT NULL,
    [Descuento]           MONEY          CONSTRAINT [DF__aProducto__Descu__125F7E8B] DEFAULT ((0)) NOT NULL,
    [Porcentaje]          DECIMAL (9, 6) CONSTRAINT [DF__aProducto__Porce__1353A2C4] DEFAULT ((0)) NOT NULL,
    [Desde]               DATE           NULL,
    [Hasta]               DATE           NULL,
    [CantidadLimite]      INT            NULL,
    [CantidadLimiteSaldo] INT            NULL,
    [UnicaVez]            BIT            NULL,
    [IDTransaccionPedido] INT            NULL,
    [Accion]              VARCHAR (3)    NULL,
    [FechaModificacion]   DATETIME       NULL,
    [IDUsuario]           INT            NULL
);


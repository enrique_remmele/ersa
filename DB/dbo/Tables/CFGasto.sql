﻿CREATE TABLE [dbo].[CFGasto] (
    [IDOperacion]       TINYINT NOT NULL,
    [ID]                TINYINT NOT NULL,
    [IDTipoCuentaFija]  TINYINT NOT NULL,
    [BuscarEnProveedor] BIT     NOT NULL,
    [FondoFijo]         BIT     CONSTRAINT [DF_CFGasto_FondoFijo] DEFAULT ('False') NULL,
    CONSTRAINT [PK_CFGasto] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


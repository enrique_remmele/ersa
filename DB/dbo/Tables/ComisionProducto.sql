﻿CREATE TABLE [dbo].[ComisionProducto] (
    [IDProducto]             INT            NOT NULL,
    [IDVendedor]             TINYINT        NOT NULL,
    [IDSucursal]             TINYINT        NOT NULL,
    [PorcentajeBase]         DECIMAL (5, 3) NULL,
    [PorcentajeEsteProducto] DECIMAL (5, 3) NULL,
    [PorcentajeAnterior]     DECIMAL (5, 3) NULL,
    [FechaUltimoCambio]      DATE           NULL,
    [IDUsuario]              SMALLINT       NULL,
    CONSTRAINT [PK_ComisionProducto] PRIMARY KEY CLUSTERED ([IDProducto] ASC, [IDVendedor] ASC, [IDSucursal] ASC)
);


﻿CREATE TABLE [dbo].[DetalleRendicionLoteDocumento] (
    [IDTransaccion]      NUMERIC (18)  NOT NULL,
    [ID]                 TINYINT       NOT NULL,
    [TipoComprobante]    VARCHAR (50)  NOT NULL,
    [NroComprobante]     VARCHAR (50)  NOT NULL,
    [IDTransaccionVenta] NUMERIC (18)  NULL,
    [Importe]            MONEY         NOT NULL,
    [Aplicar]            BIT           CONSTRAINT [DF_DetalleRendicionLoteDocumento_Aplicar] DEFAULT ('False') NOT NULL,
    [Observacion]        VARCHAR (100) NULL,
    CONSTRAINT [PK_DetalleRendicionLoteDocumento] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleRendicionLoteDocumento_RendicionLote] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[RendicionLote] ([IDTransaccion])
);


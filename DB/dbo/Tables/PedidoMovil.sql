﻿CREATE TABLE [dbo].[PedidoMovil] (
    [IDTransaccion]       NUMERIC (18) NOT NULL,
    [Numero]              INT          NOT NULL,
    [IDTransaccionOrigen] NUMERIC (18) NOT NULL,
    [IDVendedor]          SMALLINT     NULL,
    [IDCliente]           INT          NOT NULL,
    [Total]               MONEY        NOT NULL,
    [Fecha]               DATE         NOT NULL,
    CONSTRAINT [PK_PedidoMovil] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_PedidoMovil_Pedido] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Pedido] ([IDTransaccion])
);


﻿CREATE TABLE [dbo].[CFMacheoTicketFactura] (
    [IDOperacion]          TINYINT NOT NULL,
    [ID]                   TINYINT NOT NULL,
    [BuscarEnTipoProducto] BIT     NULL,
    [IDTipoProducto]       TINYINT NULL,
    [AjusteContrato]       BIT     NULL,
    [DiferenciaCambio]     BIT     NULL,
    CONSTRAINT [PK_CFMacheoTicketFactura] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[LineaMarca] (
    [IDLinea] SMALLINT NOT NULL,
    [IDMarca] TINYINT  NOT NULL,
    CONSTRAINT [PK_LineaMarca] PRIMARY KEY CLUSTERED ([IDLinea] ASC, [IDMarca] ASC)
);


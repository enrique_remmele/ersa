﻿CREATE TABLE [dbo].[CanjeRecaudacion] (
    [IDTransaccion]    NUMERIC (18)  NOT NULL,
    [IDSucursal]       TINYINT       NULL,
    [Numero]           INT           NOT NULL,
    [Fecha]            DATE          NOT NULL,
    [Total]            MONEY         CONSTRAINT [DF__CanjeReca__Total__23CACD06] DEFAULT ((0)) NOT NULL,
    [Anulado]          BIT           CONSTRAINT [DF__CanjeReca__Anula__24BEF13F] DEFAULT ('False') NOT NULL,
    [FechaAnulado]     DATE          NULL,
    [IDUsuarioAnulado] SMALLINT      NULL,
    [Observacion]      VARCHAR (200) NULL,
    [IDmoneda]         TINYINT       NULL,
    [Cotizacion]       MONEY         NULL
);


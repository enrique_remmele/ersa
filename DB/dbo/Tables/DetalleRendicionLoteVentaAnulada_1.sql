﻿CREATE TABLE [dbo].[DetalleRendicionLoteVentaAnulada] (
    [IDTransaccion]      NUMERIC (18) NOT NULL,
    [ID]                 TINYINT      NOT NULL,
    [IDTransaccionVenta] NUMERIC (18) NOT NULL,
    [IDMotivo]           INT          NOT NULL,
    [Importe]            MONEY        CONSTRAINT [DF_DetalleRendicionLoteVentaAnulada_Importe] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DetalleRendicionLoteVentaAnulada] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleRendicionLoteVentaAnulada_Motivo] FOREIGN KEY ([IDMotivo]) REFERENCES [dbo].[MotivoAnulacionVenta] ([ID]),
    CONSTRAINT [FK_DetalleRendicionLoteVentaAnulada_RendicionLote] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[RendicionLote] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleRendicionLoteVentaAnulada_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


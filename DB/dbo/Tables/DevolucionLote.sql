﻿CREATE TABLE [dbo].[DevolucionLote] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDTransaccionLote] NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Numero]            INT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [Fecha]             DATETIME      NOT NULL,
    [Anulado]           BIT           CONSTRAINT [DF_DevolucionLote_Anulado_1] DEFAULT ('False') NOT NULL,
    [Observacion]       VARCHAR (100) NULL,
    [FechaAnulado]      BIT           NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    CONSTRAINT [PK_DevolucionLote_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_DevolucionLote_LoteDistribucion] FOREIGN KEY ([IDTransaccionLote]) REFERENCES [dbo].[LoteDistribucion] ([IDTransaccion])
);


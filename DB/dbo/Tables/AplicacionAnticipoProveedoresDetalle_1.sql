﻿CREATE TABLE [dbo].[AplicacionAnticipoProveedoresDetalle] (
    [IDTransaccion]            NUMERIC (18) NOT NULL,
    [IDTransaccionOrdenPago]   NUMERIC (18) NOT NULL,
    [IDTransaccionProveedores] NUMERIC (18) NOT NULL,
    [Fecha]                    DATE         NOT NULL,
    [TotalEgreso]              MONEY        NULL,
    [TotalOrdenPago]           MONEY        NULL,
    CONSTRAINT [PK_AplicacionAnticipoProveedoresDetalle] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDTransaccionOrdenPago] ASC, [IDTransaccionProveedores] ASC),
    CONSTRAINT [FK_AplicacionAnticipoProveedoresDetalle_OrdenPago] FOREIGN KEY ([IDTransaccionOrdenPago]) REFERENCES [dbo].[OrdenPago] ([IDTransaccion])
);


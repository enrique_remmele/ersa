﻿CREATE TABLE [dbo].[ClasificacionProducto] (
    [ID]          SMALLINT     NOT NULL,
    [IDPadre]     SMALLINT     NULL,
    [Nivel]       TINYINT      NULL,
    [Numero]      TINYINT      NULL,
    [Codigo]      VARCHAR (10) NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Alias]       VARCHAR (10) NULL,
    [Imagen]      IMAGE        NULL,
    [IDAgrupador] TINYINT      NULL,
    CONSTRAINT [PK_ClasificacionProducto] PRIMARY KEY CLUSTERED ([ID] ASC)
);


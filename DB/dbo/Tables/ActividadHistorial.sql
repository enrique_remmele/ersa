﻿CREATE TABLE [dbo].[ActividadHistorial] (
    [ID]                  NUMERIC (18) NOT NULL,
    [Fecha]               DATETIME     NULL,
    [IDActividad]         INT          NULL,
    [Total]               MONEY        NULL,
    [Saldo]               MONEY        NULL,
    [PorcentajeUtilizado] INT          NULL,
    [Excedente]           MONEY        NULL,
    CONSTRAINT [PK_ActividadHistorial] PRIMARY KEY CLUSTERED ([ID] ASC)
);


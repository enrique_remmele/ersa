﻿CREATE TABLE [dbo].[RegistroHabilitacion] (
    [IDTransaccion]      NUMERIC (18)  NOT NULL,
    [Numero]             INT           NOT NULL,
    [Asunto]             VARCHAR (50)  NULL,
    [Mensaje]            VARCHAR (500) NOT NULL,
    [IDUsuarioSolicitud] INT           NOT NULL,
    [CodigoActivacion]   VARCHAR (50)  NOT NULL,
    [FechaActivacion]    DATETIME      NULL,
    [Pendiente]          BIT           CONSTRAINT [DF_RegistroHabilitacion_Pendiente] DEFAULT ('True') NOT NULL,
    [Comprobante]        VARCHAR (50)  NULL,
    [FechaComprobante]   DATE          NULL,
    [Importe]            MONEY         NULL,
    CONSTRAINT [PK_RegistroHabilitacion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


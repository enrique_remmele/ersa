﻿CREATE TABLE [dbo].[MotivoAnulacionNotaCredito] (
    [ID]                    INT           NOT NULL,
    [Descripcion]           VARCHAR (100) NOT NULL,
    [Estado]                BIT           NULL,
    [MantenerPedidoVigente] BIT           NULL,
    CONSTRAINT [PK_MotivoAnulacionNC] PRIMARY KEY CLUSTERED ([ID] ASC)
);


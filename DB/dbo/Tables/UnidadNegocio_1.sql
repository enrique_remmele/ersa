﻿CREATE TABLE [dbo].[UnidadNegocio] (
    [ID]          TINYINT      NOT NULL,
    [Codigo]      VARCHAR (10) NULL,
    [Descripcion] VARCHAR (50) NULL,
    [Estado]      BIT          NULL,
    CONSTRAINT [PK_UnidadNegocio] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[Chequera] (
    [ID]               TINYINT       NOT NULL,
    [NroComprobante]   INT           NOT NULL,
    [IDBanco]          TINYINT       NOT NULL,
    [IDCuentaBancaria] TINYINT       NULL,
    [Observacion]      VARCHAR (150) NULL,
    [NroDesde]         INT           NOT NULL,
    [NroHasta]         INT           NOT NULL,
    [Estado]           BIT           CONSTRAINT [DF_Chequera_Estado] DEFAULT ('True') NOT NULL,
    [AUtilizar]        INT           NULL,
    CONSTRAINT [PK_Chequera] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Chequera_Banco] FOREIGN KEY ([IDBanco]) REFERENCES [dbo].[Banco] ([ID]),
    CONSTRAINT [FK_Chequera_CuentaBancaria] FOREIGN KEY ([IDCuentaBancaria]) REFERENCES [dbo].[CuentaBancaria] ([ID])
);


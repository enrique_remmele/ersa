﻿CREATE TABLE [dbo].[aCotizacion] (
    [ID]             INT      NOT NULL,
    [Fecha]          DATETIME NOT NULL,
    [IDMoneda]       TINYINT  NOT NULL,
    [Cotizacion]     MONEY    NOT NULL,
    [IDUsuario]      INT      NOT NULL,
    [FechaAuditoria] DATETIME NOT NULL
);


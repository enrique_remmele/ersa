﻿CREATE TABLE [dbo].[DetalleFormularioImpresion] (
    [IDFormularioImpresion]        TINYINT       NOT NULL,
    [IDDetalleFormularioImpresion] TINYINT       NOT NULL,
    [NombreCampo]                  VARCHAR (50)  NOT NULL,
    [Campo]                        VARCHAR (50)  NOT NULL,
    [PosicionX]                    SMALLINT      CONSTRAINT [DF_DetalleFormularioImpresion_PosicionX] DEFAULT ((0)) NOT NULL,
    [PosicionY]                    SMALLINT      CONSTRAINT [DF_DetalleFormularioImpresion_PosicionY] DEFAULT ((0)) NOT NULL,
    [TipoLetra]                    VARCHAR (15)  CONSTRAINT [DF_DetalleFormularioImpresion_TipoLetra] DEFAULT ('Arial') NOT NULL,
    [TamañoLetra]                  TINYINT       CONSTRAINT [DF_DetalleFormularioImpresion_TamañoLetra] DEFAULT ((7)) NOT NULL,
    [Formato]                      VARCHAR (10)  CONSTRAINT [DF_DetalleFormularioImpresion_Formato] DEFAULT ('Normal') NOT NULL,
    [Valor]                        VARCHAR (200) CONSTRAINT [DF_DetalleFormularioImpresion_Valor] DEFAULT ('--valor--') NULL,
    [Detalle]                      BIT           CONSTRAINT [DF_DetalleFormularioImpresion_Detalle] DEFAULT ('False') NULL,
    [Numerico]                     BIT           NULL,
    [Alineacion]                   VARCHAR (15)  CONSTRAINT [DF_DetalleFormularioImpresion_Alineacion] DEFAULT ('Izquierda') NULL,
    [Largo]                        TINYINT       CONSTRAINT [DF_DetalleFormularioImpresion_Largo] DEFAULT ((0)) NULL,
    [PuedeCrecer]                  BIT           NULL,
    [Lineas]                       TINYINT       NULL,
    [MargenPrimeraLinea]           TINYINT       CONSTRAINT [DF_DetalleFormularioImpresion_MargenPrimeraLinea] DEFAULT ((0)) NULL,
    [Interlineado]                 TINYINT       CONSTRAINT [DF_DetalleFormularioImpresion_Interlineado] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_DetalleFormularioImpresion] PRIMARY KEY CLUSTERED ([IDFormularioImpresion] ASC, [IDDetalleFormularioImpresion] ASC),
    CONSTRAINT [FK_DetalleFormularioImpresion_FormularioImpresion] FOREIGN KEY ([IDFormularioImpresion]) REFERENCES [dbo].[FormularioImpresion] ([IDFormularioImpresion])
);


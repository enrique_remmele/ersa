﻿CREATE TABLE [dbo].[GastoTipoComprobante] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [Numero]        NUMERIC (18) NULL,
    [IDSucursal]    TINYINT      NULL,
    CONSTRAINT [PK_GastoTipoComprobante] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_GastoTipoComprobante_Gasto] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Gasto] ([IDTransaccion]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [missing_index_646_645]
    ON [dbo].[GastoTipoComprobante]([Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_648_647]
    ON [dbo].[GastoTipoComprobante]([IDSucursal] ASC)
    INCLUDE([Numero]);


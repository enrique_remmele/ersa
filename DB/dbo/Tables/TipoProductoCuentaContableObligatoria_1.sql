﻿CREATE TABLE [dbo].[TipoProductoCuentaContableObligatoria] (
    [IDTipoProducto]              INT  NULL,
    [CuentaContableVenta]         BIT  NULL,
    [CuentaContableCompra]        BIT  NULL,
    [CuentaContableDeudor]        BIT  NULL,
    [CuentaContableCosto]         BIT  NULL,
    [IDUsuarioUltimaModificacion] INT  NULL,
    [FechaUltimaModificacion]     DATE NULL
);


﻿CREATE TABLE [dbo].[ListaPrecio] (
    [ID]          INT          NOT NULL,
    [IDSucursal]  TINYINT      NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          NULL,
    [Referencia]  VARCHAR (10) NULL,
    CONSTRAINT [PK_ListaPrecio] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ListaPrecio_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


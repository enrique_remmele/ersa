﻿CREATE TABLE [dbo].[AProductoListaPrecio] (
    [IDListaPrecio]     INT             NOT NULL,
    [IDProducto]        INT             NOT NULL,
    [IDMoneda]          TINYINT         NOT NULL,
    [IDSucursal]        TINYINT         NOT NULL,
    [Precio]            MONEY           NOT NULL,
    [TPR]               MONEY           NULL,
    [TPRPorcentaje]     DECIMAL (10, 3) NULL,
    [TPRDesde]          DATE            NULL,
    [TPRHasta]          DATE            NULL,
    [FactorVenta]       DECIMAL (10, 4) NULL,
    [FechaModificacion] DATETIME        NULL,
    [IDUsuario]         INT             NULL,
    [Accion]            VARCHAR (3)     NULL
);


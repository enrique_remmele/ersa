﻿CREATE TABLE [dbo].[AccesoEspecifico] (
    [ID]            INT          NOT NULL,
    [IDMenu]        NUMERIC (18) NOT NULL,
    [Descripcion]   VARCHAR (50) NOT NULL,
    [Funcion]       BIT          NULL,
    [Control]       BIT          NULL,
    [NombreFuncion] VARCHAR (50) NULL,
    [NombreControl] VARCHAR (50) NULL,
    [Enabled]       BIT          NULL,
    [Visible]       BIT          NULL,
    CONSTRAINT [PK_AccesoEspecifico] PRIMARY KEY CLUSTERED ([ID] ASC, [IDMenu] ASC)
);


GO
CREATE TRIGGER [dbo].[TiAccesoEspecifico] ON [dbo].[AccesoEspecifico]
   AFTER INSERT, UPDATE
AS 
BEGIN
	
	SET NOCOUNT ON;

	Declare @vIDPerfil tinyint
	Declare @vIDAccesoEspecifico int
	Declare @vIDMenu numeric(18,0)
	
	Set @vIDAccesoEspecifico = (Select ID From Inserted)
	Set @vIDMenu = (Select IDMenu From Inserted)
	
	Declare db_cursor cursor for
	Select ID From Perfil
	Open db_cursor   
	Fetch Next From db_cursor Into @vIDPerfil
	While @@FETCH_STATUS = 0 Begin  
	
		--Cargamos solo si no existe
		If Not Exists(Select * From AccesoEspecificoPerfil Where IDAccesoEspecifico=@vIDAccesoEspecifico And IDMenu=@vIDMenu And IDPerfil=@vIDPerfil) Begin
			Insert Into AccesoEspecificoPerfil(IDPerfil, IDAccesoEspecifico, IDMenu, Habilitar)
			Values(@vIDPerfil, @vIDAccesoEspecifico, @vIDMenu, 'False')
		End
		
		Fetch Next From db_cursor Into @vIDPerfil
			
	End
	
	Close db_cursor   
	Deallocate db_cursor		   			
			    
END

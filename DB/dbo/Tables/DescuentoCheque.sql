﻿CREATE TABLE [dbo].[DescuentoCheque] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Numero]            INT           NOT NULL,
    [NroComprobante]    VARCHAR (50)  NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDCuentaBancaria]  TINYINT       NULL,
    [Cotizacion]        MONEY         NULL,
    [Observacion]       VARCHAR (100) NULL,
    [TotalDescontado]   MONEY         NOT NULL,
    [TotalAcreditado]   MONEY         NULL,
    [GastoBancario]     MONEY         NULL,
    [Procesado]         BIT           CONSTRAINT [DF_DescuentoCheque_Procesado] DEFAULT ('False') NOT NULL,
    [CuentaBancaria]    BIT           CONSTRAINT [DF_DescuentoCheque_CuentaBancaria] DEFAULT ('True') NULL,
    [Conciliado]        BIT           NULL,
    CONSTRAINT [PK_DescuentoCheque] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


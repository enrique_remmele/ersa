﻿CREATE TABLE [dbo].[TerminalPuntoExpedicion] (
    [IDTerminal]        INT NOT NULL,
    [IDOperacion]       INT NOT NULL,
    [IDPuntoExpedicion] INT NOT NULL,
    CONSTRAINT [PK_TerminalPuntoExpedicion] PRIMARY KEY CLUSTERED ([IDTerminal] ASC, [IDOperacion] ASC, [IDPuntoExpedicion] ASC),
    CONSTRAINT [FK_TerminalPuntoExpedicion_Operacion] FOREIGN KEY ([IDOperacion]) REFERENCES [dbo].[Operacion] ([ID]),
    CONSTRAINT [FK_TerminalPuntoExpedicion_PuntoExpedicion] FOREIGN KEY ([IDPuntoExpedicion]) REFERENCES [dbo].[PuntoExpedicion] ([ID]),
    CONSTRAINT [FK_TerminalPuntoExpedicion_Terminal] FOREIGN KEY ([IDTerminal]) REFERENCES [dbo].[Terminal] ([ID])
);


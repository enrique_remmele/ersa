﻿CREATE TABLE [dbo].[CuentaContable] (
    [ID]              INT           NOT NULL,
    [IDPlanCuenta]    TINYINT       NOT NULL,
    [Codigo]          VARCHAR (50)  NOT NULL,
    [Descripcion]     VARCHAR (100) NOT NULL,
    [Alias]           VARCHAR (50)  NULL,
    [Categoria]       TINYINT       NOT NULL,
    [Imputable]       BIT           CONSTRAINT [DF_CuentasContables_Imputable] DEFAULT ('False') NOT NULL,
    [IDPadre]         SMALLINT      NULL,
    [Estado]          BIT           CONSTRAINT [DF_CuentaContable_Estado] DEFAULT ('True') NOT NULL,
    [Sucursal]        BIT           NULL,
    [IDSucursal]      TINYINT       NULL,
    [CajaChica]       BIT           NULL,
    [IDUnidadNegocio] TINYINT       NULL,
    [IDCentroCosto]   TINYINT       NULL,
    CONSTRAINT [PK_CuentaContable] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CuentaContable_PlanCuenta] FOREIGN KEY ([IDPlanCuenta]) REFERENCES [dbo].[PlanCuenta] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_CodigoCuentaContable]
    ON [dbo].[CuentaContable]([Codigo] ASC);


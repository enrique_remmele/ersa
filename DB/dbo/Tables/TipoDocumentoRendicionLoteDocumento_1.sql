﻿CREATE TABLE [dbo].[TipoDocumentoRendicionLoteDocumento] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TipoDocumentoRendicionLoteDocumento] PRIMARY KEY CLUSTERED ([ID] ASC)
);


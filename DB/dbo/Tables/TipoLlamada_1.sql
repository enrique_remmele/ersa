﻿CREATE TABLE [dbo].[TipoLlamada] (
    [ID]          INT          NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Referencia]  VARCHAR (5)  NULL,
    [Estado]      BIT          CONSTRAINT [DF_TipoLlamada_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_TipoLlamada] PRIMARY KEY CLUSTERED ([ID] ASC)
);


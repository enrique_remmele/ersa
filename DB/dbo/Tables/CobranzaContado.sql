﻿CREATE TABLE [dbo].[CobranzaContado] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Numero]            INT           NOT NULL,
    [IDTransaccionLote] NUMERIC (18)  NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [NroComprobante]    VARCHAR (50)  NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [Fecha]             DATETIME      NOT NULL,
    [Total]             MONEY         CONSTRAINT [DF_CobranzaContado_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]     MONEY         CONSTRAINT [DF_CobranzaContado_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalDiscriminado] MONEY         CONSTRAINT [DF_CobranzaContado_TotalDiscriminado] DEFAULT ((0)) NOT NULL,
    [TotalDescuento]    MONEY         CONSTRAINT [DF_CobranzaContado_TotalDescuento] DEFAULT ((0)) NULL,
    [Anulado]           BIT           CONSTRAINT [DF_CobranzaContado_Anulado] DEFAULT ('False') NOT NULL,
    [FechaAnulado]      DATE          NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    [IDDistribuidor]    TINYINT       NULL,
    [Observacion]       VARCHAR (100) NULL,
    [Procesado]         BIT           CONSTRAINT [DF_CobranzaContado_Procesado] DEFAULT ('False') NOT NULL,
    [IDmoneda]          TINYINT       NULL,
    [Cotizacion]        MONEY         NULL,
    [DiferenciaCambio]  MONEY         NULL,
    [IDMotivoAnulacion] INT           NULL,
    CONSTRAINT [PK_CobranzaContado] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_CobranzaContado_LoteDistribucion] FOREIGN KEY ([IDTransaccionLote]) REFERENCES [dbo].[LoteDistribucion] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_201_200]
    ON [dbo].[CobranzaContado]([IDTransaccionLote] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_294_293]
    ON [dbo].[CobranzaContado]([IDSucursal] ASC, [Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_292_291]
    ON [dbo].[CobranzaContado]([IDSucursal] ASC)
    INCLUDE([Numero]);


GO
CREATE NONCLUSTERED INDEX [missing_index_308_307]
    ON [dbo].[CobranzaContado]([IDSucursal] ASC, [IDTipoComprobante] ASC, [NroComprobante] ASC, [Anulado] ASC);


GO
CREATE TRIGGER [dbo].[TIQuitarPuntuacionesCCONT] ON  dbo.CobranzaContado
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;
	/*JGR 20140808 Elimina las puntuaciones en el comprobante*/
    UPDATE dbo.CobranzaContado
	SET 
      CobranzaContado.Comprobante = LTRIM(RTRIM(REPLACE(CobranzaContado.Comprobante, '.', '')))
	FROM CobranzaContado JOIN INSERTED ON CobranzaContado.IDTransaccion = INSERTED.IDTransaccion
END

﻿CREATE TABLE [dbo].[MotivoRechazoCheque] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_MotivoRechazoCheque] PRIMARY KEY CLUSTERED ([ID] ASC)
);


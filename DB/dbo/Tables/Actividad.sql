﻿CREATE TABLE [dbo].[Actividad] (
    [ID]                  INT            NOT NULL,
    [IDSucursal]          TINYINT        NOT NULL,
    [IDProveedor]         INT            NOT NULL,
    [Codigo]              VARCHAR (50)   NOT NULL,
    [Mecanica]            VARCHAR (500)  CONSTRAINT [DF_Actividad_Mecanica] DEFAULT ('') NOT NULL,
    [ImporteAsignado]     MONEY          NULL,
    [Entrada]             MONEY          NULL,
    [Salida]              MONEY          NULL,
    [DescuentoMaximo]     DECIMAL (5, 2) NULL,
    [CarteraMaxima]       TINYINT        NULL,
    [Tactico]             MONEY          NULL,
    [Acuerdo]             MONEY          NULL,
    [Total]               MONEY          NULL,
    [PorcentajeUtilizado] INT            NULL,
    [Saldo]               MONEY          NULL,
    [Excedente]           MONEY          NULL,
    [DescuentoMinimo]     TINYINT        NULL,
    [CarteraMinima]       TINYINT        NULL,
    [Cobertura]           TINYINT        NULL,
    CONSTRAINT [PK_DetalleActividad] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE Trigger [dbo].[Ti_ActividadTotales] On [dbo].[Actividad]

For Update

As

Begin

	Set NoCount On  
	
	Declare @vID int
	Declare @vTotalTactico money
	Declare @vTotalAcuerdo money
	Declare @vAsignado money
	Declare @vTotal money
	Declare @vPorcentajeUtilizado int
	Declare @vSaldo money
	Declare @vExcedente money
	
	Select @vID = ID, @vAsignado = ImporteAsignado, @vTotalTactico = Tactico, 
	@vTotalAcuerdo = Acuerdo From Inserted
	
	If (Select Tactico From Deleted) != @vTotalTactico Or (Select Acuerdo From Deleted) != @vTotalAcuerdo Begin 
	
		Set @vTotal = @vTotalTactico + @vTotalAcuerdo
		Set @vSaldo = @vAsignado - @vTotal
		Set @vExcedente = 0
		Set @vPorcentajeUtilizado = 0
		
		If  @vSaldo < 0 Begin
			Set @vExcedente = @vSaldo *-1
			Set @vSaldo = 0
		End
		
		If @vAsignado > 0 Begin
			Set @vPorcentajeUtilizado = (@vTotal / @vAsignado) * 100
		End
		
		
		
		Update Actividad Set	Total=@vTotal,
								Saldo=@vSaldo,
								PorcentajeUtilizado=@vPorcentajeUtilizado,
								Excedente=@vExcedente
		Where ID=@vID
		
		
		--Insertar historial
		declare @vIndice numeric(18,0)
		Set @vIndice = (Select ISNull(Max(ID) + 1, 1) From ActividadHistorial)
		Insert Into ActividadHistorial(ID, Fecha, IDActividad, Total, Saldo, PorcentajeUtilizado, Excedente)
		values(@vIndice, GetDate(), @vID, @vTotal, @vSaldo, @vPorcentajeUtilizado, @vExcedente)
		
	End
						
End

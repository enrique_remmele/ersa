﻿CREATE TABLE [dbo].[aVencimientoChequeDescontado] (
    [IDAuditoria]         INT           NOT NULL,
    [IDTransaccion]       NUMERIC (18)  NOT NULL,
    [IDSucursal]          TINYINT       NOT NULL,
    [Numero]              INT           NOT NULL,
    [Fecha]               DATE          NOT NULL,
    [IDTransaccionCheque] NUMERIC (18)  NOT NULL,
    [Cotizacion]          MONEY         NULL,
    [Observacion]         VARCHAR (100) NULL,
    [Anulado]             BIT           NOT NULL,
    [FechaAnulado]        DATE          NULL,
    [IDUsuarioAnulado]    SMALLINT      NULL,
    [Conciliado]          BIT           NULL,
    [IDUsuario]           INT           NOT NULL,
    [Accion]              VARCHAR (3)   NOT NULL
);


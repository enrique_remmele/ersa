﻿CREATE TABLE [dbo].[CuentaFijaCobranzaCredito] (
    [ID]                TINYINT      NOT NULL,
    [Descripcion]       VARCHAR (50) NOT NULL,
    [Orden]             TINYINT      NOT NULL,
    [IDTipoComprobante] SMALLINT     NOT NULL,
    [Credito]           BIT          CONSTRAINT [DF_CuentaFijaCobranzaCredito_Credito] DEFAULT ('False') NOT NULL,
    [Contado]           BIT          CONSTRAINT [DF_CuentaFijaCobranzaCredito_Contado] DEFAULT ('False') NOT NULL,
    [IDMoneda]          TINYINT      NOT NULL,
    [IDCuentaContable]  SMALLINT     NOT NULL,
    CONSTRAINT [PK_CuentaFijaCobranzaCredito] PRIMARY KEY CLUSTERED ([ID] ASC)
);


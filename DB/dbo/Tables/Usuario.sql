﻿CREATE TABLE [dbo].[Usuario] (
    [ID]            INT           NOT NULL,
    [Nombre]        VARCHAR (50)  NOT NULL,
    [Usuario]       VARCHAR (50)  NOT NULL,
    [Password]      VARCHAR (500) NOT NULL,
    [IDPerfil]      TINYINT       NOT NULL,
    [Estado]        BIT           NULL,
    [Identificador] VARCHAR (3)   NULL,
    [EsVendedor]    BIT           CONSTRAINT [DF_Usuario_Vendedor] DEFAULT ('False') NULL,
    [IDVendedor]    INT           NULL,
    [Email]         VARCHAR (50)  NULL,
    [EsChofer]      BIT           NULL,
    [IDChofer]      TINYINT       NULL,
    CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Asiento_Usuario] FOREIGN KEY ([ID]) REFERENCES [dbo].[Usuario] ([ID]),
    CONSTRAINT [FK_Usuario_Usuario] FOREIGN KEY ([ID]) REFERENCES [dbo].[Usuario] ([ID])
);


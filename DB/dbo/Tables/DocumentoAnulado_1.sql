﻿CREATE TABLE [dbo].[DocumentoAnulado] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [Fecha]         DATETIME     NOT NULL,
    [IDUsuario]     SMALLINT     NOT NULL,
    [IDSucursal]    TINYINT      NOT NULL,
    [IDTerminal]    INT          NOT NULL,
    CONSTRAINT [PK_DocumentoAnulado] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


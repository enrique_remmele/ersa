﻿CREATE TABLE [dbo].[PagoChequeCliente] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [IDTipoComprobante] NCHAR (10)    NOT NULL,
    [Numero]            INT           NOT NULL,
    [NroComprobante]    NUMERIC (18)  NOT NULL,
    [Fecha]             DATETIME      NOT NULL,
    [Total]             MONEY         NOT NULL,
    [Observacion]       VARCHAR (100) NULL,
    [Anulado]           BIT           NULL,
    CONSTRAINT [PK_PagoChequeCliente] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


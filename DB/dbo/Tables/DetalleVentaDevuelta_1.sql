﻿CREATE TABLE [dbo].[DetalleVentaDevuelta] (
    [IDTransaccionVenta]            NUMERIC (18)    NOT NULL,
    [IDProducto]                    INT             NOT NULL,
    [Cantidad]                      DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario]                MONEY           CONSTRAINT [DF_DetalleVentaDevuelta_PrecioUnitario] DEFAULT ((0)) NOT NULL,
    [Total]                         MONEY           CONSTRAINT [DF_DetalleVentaDevuelta_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]                 MONEY           CONSTRAINT [DF_DetalleVentaDevuelta_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalDiscriminado]             MONEY           NOT NULL,
    [PorcentajeDescuento]           NUMERIC (5, 2)  NULL,
    [DescuentoUnitario]             MONEY           CONSTRAINT [DF_DetalleVentaDevuelta_DescuentoUnitario] DEFAULT ((0)) NULL,
    [DescuentoUnitarioDiscriminado] MONEY           CONSTRAINT [DF_DetalleVentaDevuelta_DescuentoUnitarioDiscriminado] DEFAULT ((0)) NULL,
    [TotalDescuento]                MONEY           CONSTRAINT [DF_DetalleVentaDevuelta_TotalDescuento] DEFAULT ((0)) NULL,
    [TotalDescuentoDiscriminado]    MONEY           CONSTRAINT [DF_DetalleVentaDevuelta_TotalDescuentoDiscriminado] DEFAULT ((0)) NULL,
    [CostoUnitario]                 MONEY           NULL,
    [TotalCosto]                    MONEY           NULL,
    [TotalCostoImpuesto]            MONEY           NULL,
    [TotalCostoDiscriminado]        MONEY           NULL,
    [Caja]                          BIT             NULL,
    [CantidadCaja]                  DECIMAL (10, 2) NULL
);


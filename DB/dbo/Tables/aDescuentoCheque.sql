﻿CREATE TABLE [dbo].[aDescuentoCheque] (
    [IDAuditoria]       INT           NOT NULL,
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Numero]            INT           NOT NULL,
    [NroComprobante]    VARCHAR (50)  NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDCuentaBancaria]  TINYINT       NULL,
    [Cotizacion]        MONEY         NULL,
    [Observacion]       VARCHAR (100) NULL,
    [TotalDescontado]   MONEY         NOT NULL,
    [TotalAcreditado]   MONEY         NULL,
    [GastoBancario]     MONEY         NULL,
    [Procesado]         BIT           NOT NULL,
    [CuentaBancaria]    BIT           NULL,
    [Conciliado]        BIT           NULL,
    [IDUsuario]         INT           NOT NULL,
    [Accion]            VARCHAR (3)   NOT NULL
);


﻿CREATE TABLE [dbo].[OrdenPagoEfectivo] (
    [IDTransaccionOrdenPago] NUMERIC (18) NOT NULL,
    [IDTransaccionEfectivo]  NUMERIC (18) NOT NULL,
    [IDEfectivo]             SMALLINT     NOT NULL,
    [Importe]                MONEY        NOT NULL,
    CONSTRAINT [PK_OrdenPagoEfectivo] PRIMARY KEY CLUSTERED ([IDTransaccionOrdenPago] ASC, [IDTransaccionEfectivo] ASC, [IDEfectivo] ASC),
    CONSTRAINT [FK_OrdenPagoEfectivo_Efectivo1] FOREIGN KEY ([IDTransaccionEfectivo], [IDEfectivo]) REFERENCES [dbo].[Efectivo] ([IDTransaccion], [ID]),
    CONSTRAINT [FK_OrdenPagoEfectivo_OrdenPago] FOREIGN KEY ([IDTransaccionOrdenPago]) REFERENCES [dbo].[OrdenPago] ([IDTransaccion])
);


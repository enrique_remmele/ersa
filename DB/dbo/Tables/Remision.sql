﻿CREATE TABLE [dbo].[Remision] (
    [IDTransaccion]        NUMERIC (18)  NOT NULL,
    [IDPuntoExpedicion]    INT           NOT NULL,
    [IDTipoComprobante]    SMALLINT      NOT NULL,
    [NroComprobante]       INT           NOT NULL,
    [IDCliente]            INT           NOT NULL,
    [DireccionPartida]     VARCHAR (200) NULL,
    [DireccionLlegada]     VARCHAR (200) NULL,
    [IDSucursal]           TINYINT       NOT NULL,
    [IDDeposito]           TINYINT       NOT NULL,
    [FechaInicio]          DATE          NOT NULL,
    [FechaFin]             DATE          NOT NULL,
    [IDDistribuidor]       TINYINT       NULL,
    [IDCamion]             INT           NULL,
    [IDChofer]             TINYINT       NULL,
    [MotivoObservacion]    VARCHAR (200) NULL,
    [ComprobanteVenta]     VARCHAR (50)  NULL,
    [Observacion]          VARCHAR (500) NULL,
    [Anulado]              BIT           NOT NULL,
    [FechaAnulado]         DATE          NULL,
    [IDUsuarioAnulado]     SMALLINT      NULL,
    [Procesado]            BIT           NULL,
    [IDMotivo]             TINYINT       NULL,
    [DomicilioCliente]     VARCHAR (50)  NULL,
    [ComprobanteVentaTipo] VARCHAR (50)  NULL,
    [TimbradoVenta]        VARCHAR (50)  NULL,
    [FechaExpedicionVenta] DATE          NULL,
    [CiudadPartida]        VARCHAR (50)  NULL,
    [DepartamentoPartida]  VARCHAR (50)  NULL,
    [CiudadLlegada]        VARCHAR (50)  NULL,
    [DepartamentoLlegada]  VARCHAR (50)  NULL,
    [KilometrajeEstimado]  VARCHAR (50)  NULL,
    CONSTRAINT [PK_Remision] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_Remision_Camion] FOREIGN KEY ([IDCamion]) REFERENCES [dbo].[Camion] ([ID]),
    CONSTRAINT [FK_Remision_Chofer] FOREIGN KEY ([IDChofer]) REFERENCES [dbo].[Chofer] ([ID]),
    CONSTRAINT [FK_Remision_Deposito] FOREIGN KEY ([IDDeposito]) REFERENCES [dbo].[Deposito] ([ID]),
    CONSTRAINT [FK_Remision_Deposito1] FOREIGN KEY ([IDDeposito]) REFERENCES [dbo].[Deposito] ([ID]),
    CONSTRAINT [FK_Remision_Distribuidor] FOREIGN KEY ([IDDistribuidor]) REFERENCES [dbo].[Distribuidor] ([ID]),
    CONSTRAINT [FK_Remision_MotivoRemision] FOREIGN KEY ([IDMotivo]) REFERENCES [dbo].[MotivoRemision] ([ID]),
    CONSTRAINT [FK_Remision_PuntoExpedicion] FOREIGN KEY ([IDPuntoExpedicion]) REFERENCES [dbo].[PuntoExpedicion] ([ID]),
    CONSTRAINT [FK_Remision_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_Remision_Sucursal1] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_Remision_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID]),
    CONSTRAINT [FK_Remision_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


GO
ALTER TABLE [dbo].[Remision] NOCHECK CONSTRAINT [FK_Remision_Camion];


GO
ALTER TABLE [dbo].[Remision] NOCHECK CONSTRAINT [FK_Remision_Chofer];


GO
ALTER TABLE [dbo].[Remision] NOCHECK CONSTRAINT [FK_Remision_Deposito];


GO
ALTER TABLE [dbo].[Remision] NOCHECK CONSTRAINT [FK_Remision_Deposito1];


GO
ALTER TABLE [dbo].[Remision] NOCHECK CONSTRAINT [FK_Remision_Distribuidor];


GO
ALTER TABLE [dbo].[Remision] NOCHECK CONSTRAINT [FK_Remision_MotivoRemision];


GO
ALTER TABLE [dbo].[Remision] NOCHECK CONSTRAINT [FK_Remision_Sucursal];


GO
ALTER TABLE [dbo].[Remision] NOCHECK CONSTRAINT [FK_Remision_Sucursal1];


GO
CREATE NONCLUSTERED INDEX [missing_index_546_545]
    ON [dbo].[Remision]([IDPuntoExpedicion] ASC)
    INCLUDE([IDTransaccion], [NroComprobante]);


GO
CREATE NONCLUSTERED INDEX [missing_index_203_202]
    ON [dbo].[Remision]([IDPuntoExpedicion] ASC, [NroComprobante] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1237_1236]
    ON [dbo].[Remision]([Anulado] ASC, [FechaInicio] ASC)
    INCLUDE([IDTransaccion], [IDPuntoExpedicion], [NroComprobante], [IDCliente], [FechaFin], [IDDistribuidor], [IDCamion], [IDChofer]);


GO
CREATE NONCLUSTERED INDEX [missing_index_417909_417908]
    ON [dbo].[Remision]([FechaInicio] ASC)
    INCLUDE([IDTransaccion], [IDPuntoExpedicion], [NroComprobante], [IDCliente], [FechaFin], [IDDistribuidor], [IDCamion], [IDChofer], [Anulado]);


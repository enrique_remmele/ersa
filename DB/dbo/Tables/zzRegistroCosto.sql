﻿CREATE TABLE [dbo].[zzRegistroCosto] (
    [ID]                    NUMERIC (18)    NOT NULL,
    [IDProducto]            INT             NOT NULL,
    [FechaInicio]           DATE            NOT NULL,
    [UltimaActualizacion]   DATETIME        NOT NULL,
    [UltimoCosto]           MONEY           NOT NULL,
    [UltimoCostoTemp]       MONEY           NOT NULL,
    [ActualCantidad]        DECIMAL (10, 2) NOT NULL,
    [ActualTotalCosto]      MONEY           NOT NULL,
    [ActualCostoPromedio]   MONEY           NOT NULL,
    [AnteriorCantidad]      DECIMAL (10, 2) NOT NULL,
    [AnteriorTotalCosto]    MONEY           NOT NULL,
    [AnteriorCostoPromedio] MONEY           NOT NULL,
    [GeneralCantidad]       DECIMAL (10, 2) NOT NULL,
    [GeneralTotalCosto]     MONEY           NOT NULL,
    [GeneralCostoPromedio]  MONEY           NOT NULL,
    [Cerrado]               BIT             NOT NULL,
    [FechaCerrado]          DATETIME        NULL,
    [IDUsuarioCierre]       SMALLINT        NULL,
    CONSTRAINT [PK_RegistroCosto] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_RegistroCosto_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID])
);


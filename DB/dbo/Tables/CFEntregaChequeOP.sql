﻿CREATE TABLE [dbo].[CFEntregaChequeOP] (
    [IDOperacion]         TINYINT NULL,
    [ID]                  TINYINT NULL,
    [Proveedor]           BIT     NULL,
    [Cheque]              BIT     NULL,
    [BuscarEnProveedores] BIT     NULL,
    [ChequeDiferido]      BIT     NULL
);


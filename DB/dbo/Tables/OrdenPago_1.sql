﻿CREATE TABLE [dbo].[OrdenPago] (
    [IDTransaccion]          NUMERIC (18)    NOT NULL,
    [Numero]                 INT             NOT NULL,
    [IDTipoComprobante]      SMALLINT        NOT NULL,
    [NroComprobante]         VARCHAR (50)    NOT NULL,
    [IDSucursal]             TINYINT         NOT NULL,
    [Fecha]                  DATE            NOT NULL,
    [IDProveedor]            INT             NULL,
    [Observacion]            VARCHAR (500)   NULL,
    [Cotizacion]             MONEY           CONSTRAINT [DF_OrdenPago_Cotizacion] DEFAULT ((1)) NULL,
    [ImporteMoneda]          MONEY           CONSTRAINT [DF_OrdenPago_ImporteMoneda] DEFAULT ((0)) NOT NULL,
    [Total]                  MONEY           CONSTRAINT [DF_OrdenPago_Total] DEFAULT ((0)) NOT NULL,
    [Cancelado]              BIT             CONSTRAINT [DF_OrdenPago_Cancelado] DEFAULT ('False') NOT NULL,
    [Anulado]                BIT             CONSTRAINT [DF_OrdenPago_Anulado] DEFAULT ('False') NOT NULL,
    [FechaAnulado]           BIT             NULL,
    [IDUsuarioAnulado]       SMALLINT        NULL,
    [Conciliado]             BIT             NULL,
    [Impreso]                BIT             NULL,
    [PagoProveedor]          BIT             NULL,
    [AnticipoProveedor]      BIT             NULL,
    [EgresoRendir]           BIT             NULL,
    [AplicarRetencion]       BIT             NULL,
    [IDMonedaEfectivo]       TINYINT         NULL,
    [ImporteMonedaEfectivo]  MONEY           NULL,
    [CotizacionEfectivo]     MONEY           NULL,
    [IDMoneda]               TINYINT         NULL,
    [ChequeEntregado]        BIT             NULL,
    [FechaEntrega]           DATE            NULL,
    [Recibo]                 VARCHAR (50)    NULL,
    [RetiradoPor]            VARCHAR (200)   NULL,
    [DiferenciaCambio]       MONEY           CONSTRAINT [DF_OrdenPago_DiferenciaCambio] DEFAULT ((0)) NULL,
    [IDMonedaDocumento]      TINYINT         NULL,
    [ImporteDocumento]       MONEY           NULL,
    [ImporteMonedaDocumento] MONEY           NULL,
    [CotizacionDocumento]    MONEY           NULL,
    [IDMotivoAnulacion]      INT             NULL,
    [RetencionManual]        BIT             NULL,
    [TotalRetencion]         DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_OrdenPago] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_OrdenPago_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID]),
    CONSTRAINT [FK_OrdenPago_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
    ON [dbo].[OrdenPago]([Anulado] ASC)
    INCLUDE([IDTransaccion], [IDTipoComprobante], [IDSucursal]);


GO
CREATE NONCLUSTERED INDEX [missing_index_998_997]
    ON [dbo].[OrdenPago]([Numero] ASC, [IDSucursal] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_994_993]
    ON [dbo].[OrdenPago]([IDSucursal] ASC)
    INCLUDE([Numero]);


GO
CREATE NONCLUSTERED INDEX [missing_index_1023_1022]
    ON [dbo].[OrdenPago]([Numero] ASC, [IDSucursal] ASC)
    INCLUDE([IDTransaccion], [IDTipoComprobante]);


GO
CREATE NONCLUSTERED INDEX [missing_index_2694_2693]
    ON [dbo].[OrdenPago]([Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_2698_2697]
    ON [dbo].[OrdenPago]([Numero] ASC);


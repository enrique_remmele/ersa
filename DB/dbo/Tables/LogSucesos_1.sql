﻿CREATE TABLE [dbo].[LogSucesos] (
    [ID]                 NUMERIC (18) NOT NULL,
    [IDTipoOperacionLog] TINYINT      NOT NULL,
    [Fecha]              DATETIME     NOT NULL,
    [IDTabla]            SMALLINT     NOT NULL,
    [Tabla]              VARCHAR (50) NOT NULL,
    [IDTransaccion]      NUMERIC (18) NULL,
    [Comprobante]        VARCHAR (50) NULL,
    [IDUsuario]          SMALLINT     NULL,
    [Usuario]            VARCHAR (50) NULL,
    [IDTerminal]         INT          NULL,
    CONSTRAINT [PK_LogSucesos] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_LogSucesos_TablaLog] FOREIGN KEY ([IDTabla]) REFERENCES [dbo].[TablaLog] ([ID]),
    CONSTRAINT [FK_LogSucesos_TipoOperacionLog] FOREIGN KEY ([IDTipoOperacionLog]) REFERENCES [dbo].[TipoOperacionLog] ([ID]) ON UPDATE CASCADE
);


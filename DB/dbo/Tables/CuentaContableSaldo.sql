﻿CREATE TABLE [dbo].[CuentaContableSaldo] (
    [IDPlanCuenta]     TINYINT      NOT NULL,
    [IDCuentaContable] INT          NOT NULL,
    [Año]              SMALLINT     NOT NULL,
    [Mes]              TINYINT      NOT NULL,
    [Codigo]           VARCHAR (50) NOT NULL,
    [DebitoAcumulado]  MONEY        NOT NULL,
    [CreditoAcumulado] MONEY        NOT NULL,
    [SaldoAcumulado]   MONEY        NOT NULL,
    [DebitoDiario]     MONEY        NOT NULL,
    [CreditoDiario]    MONEY        NOT NULL,
    [SaldoDiario]      MONEY        NOT NULL,
    CONSTRAINT [PK_CuentaContableSaldo] PRIMARY KEY CLUSTERED ([IDPlanCuenta] ASC, [IDCuentaContable] ASC, [Año] ASC, [Mes] ASC),
    CONSTRAINT [FK_CuentaContableSaldo_CuentaContable] FOREIGN KEY ([IDCuentaContable]) REFERENCES [dbo].[CuentaContable] ([ID]),
    CONSTRAINT [FK_CuentaContableSaldo_PlanCuenta] FOREIGN KEY ([IDPlanCuenta]) REFERENCES [dbo].[PlanCuenta] ([ID])
);


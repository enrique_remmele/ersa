﻿CREATE TABLE [dbo].[MateriaPrimaCuentaContable] (
    [IDCuentaContable] INT NOT NULL,
    [Estado]           BIT CONSTRAINT [DF_MateriaPrimaCuentaContable_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_MateriaPrimaCuentaContable] PRIMARY KEY CLUSTERED ([IDCuentaContable] ASC)
);


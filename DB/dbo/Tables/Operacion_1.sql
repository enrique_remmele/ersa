﻿CREATE TABLE [dbo].[Operacion] (
    [ID]             INT          NOT NULL,
    [Descripcion]    VARCHAR (50) NOT NULL,
    [Codigo]         VARCHAR (10) NOT NULL,
    [AsientoCredito] BIT          NULL,
    [AsientoDebito]  BIT          NULL,
    [FormName]       VARCHAR (50) NULL,
    [Tabla]          VARCHAR (50) NULL,
    CONSTRAINT [PK_Operacion] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Operacion_Operacion] FOREIGN KEY ([ID]) REFERENCES [dbo].[Operacion] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Operacion]
    ON [dbo].[Operacion]([Descripcion] ASC);


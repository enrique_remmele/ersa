﻿CREATE TABLE [dbo].[Promotor] (
    [ID]           TINYINT       NOT NULL,
    [Nombres]      VARCHAR (50)  NOT NULL,
    [NroDocumento] VARCHAR (15)  NULL,
    [Telefono]     VARCHAR (20)  NULL,
    [Celular]      VARCHAR (20)  NULL,
    [Direccion]    VARCHAR (100) NULL,
    [Email]        VARCHAR (50)  NULL,
    [Estado]       BIT           NULL,
    CONSTRAINT [PK_Promotor] PRIMARY KEY CLUSTERED ([ID] ASC)
);


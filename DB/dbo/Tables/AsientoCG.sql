﻿CREATE TABLE [dbo].[AsientoCG] (
    [Numero]              INT           NOT NULL,
    [IDTransaccionOrigen] NUMERIC (18)  CONSTRAINT [DF_AsientoCG_IDTransaccionOrigen] DEFAULT ((0)) NOT NULL,
    [Resumido]            BIT           NOT NULL,
    [IDSucursal]          TINYINT       NOT NULL,
    [Fecha]               DATETIME      NOT NULL,
    [IDMoneda]            TINYINT       NOT NULL,
    [Cotizacion]          MONEY         NULL,
    [IDTipoComprobante]   SMALLINT      NOT NULL,
    [NroComprobante]      VARCHAR (50)  NOT NULL,
    [Detalle]             VARCHAR (500) NOT NULL,
    [Total]               MONEY         CONSTRAINT [DF_AsientoCG_Total] DEFAULT ((0)) NOT NULL,
    [Debito]              MONEY         CONSTRAINT [DF_AsientoCG_Debito] DEFAULT ((0)) NOT NULL,
    [Credito]             MONEY         CONSTRAINT [DF_AsientoCG_Credito] DEFAULT ((0)) NOT NULL,
    [Saldo]               MONEY         CONSTRAINT [DF_AsientoCG_Saldo] DEFAULT ((0)) NOT NULL,
    [Anulado]             BIT           NULL,
    [IDCentroCosto]       TINYINT       NULL,
    [Conciliado]          BIT           NULL,
    [FechaConciliado]     DATE          NULL,
    [IDUsuarioConciliado] SMALLINT      NULL,
    [Bloquear]            BIT           CONSTRAINT [DF_AsientoCG_Bloquear] DEFAULT ('False') NULL,
    [Operacion]           VARCHAR (50)  NULL,
    [TipoOperacion]       VARCHAR (50)  NULL,
    [Tipo]                VARCHAR (50)  NULL,
    [NumeroAsiento]       INT           NULL,
    CONSTRAINT [PK_AsientoCG] PRIMARY KEY CLUSTERED ([Numero] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20151030-161808]
    ON [dbo].[AsientoCG]([Numero] ASC, [IDTransaccionOrigen] ASC, [IDSucursal] ASC, [IDTipoComprobante] ASC, [TipoOperacion] ASC, [Fecha] ASC, [Tipo] ASC);


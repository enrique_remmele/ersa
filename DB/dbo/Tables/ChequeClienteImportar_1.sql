﻿CREATE TABLE [dbo].[ChequeClienteImportar] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [NroOperacion]  INT          NOT NULL,
    [NroCheque]     VARCHAR (15) NULL,
    [IDBanco]       TINYINT      NULL,
    [IDCliente]     INT          NULL,
    CONSTRAINT [PK_ChequeClienteImportar] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


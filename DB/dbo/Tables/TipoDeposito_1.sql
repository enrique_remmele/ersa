﻿CREATE TABLE [dbo].[TipoDeposito] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          NULL,
    [Transferir]  BIT          NULL,
    CONSTRAINT [PK_TipoDeposito] PRIMARY KEY CLUSTERED ([ID] ASC)
);


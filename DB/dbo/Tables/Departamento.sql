﻿CREATE TABLE [dbo].[Departamento] (
    [ID]          TINYINT      NOT NULL,
    [IDPais]      TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          NULL,
    [Orden]       SMALLINT     NULL,
    [Codigo]      VARCHAR (10) NULL,
    CONSTRAINT [PK_Departamento] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Departamento_Pais] FOREIGN KEY ([IDPais]) REFERENCES [dbo].[Pais] ([ID])
);


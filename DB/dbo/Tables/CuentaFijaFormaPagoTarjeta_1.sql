﻿CREATE TABLE [dbo].[CuentaFijaFormaPagoTarjeta] (
    [ID]                TINYINT      NOT NULL,
    [Descripcion]       VARCHAR (50) NOT NULL,
    [Orden]             TINYINT      NOT NULL,
    [IDTipoComprobante] SMALLINT     NOT NULL,
    [IDMoneda]          TINYINT      NOT NULL,
    [IDCuentaContable]  INT          NOT NULL,
    CONSTRAINT [PK_CuentaFijaCobranzaCreditoTarjeta] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CuentaFijaFormaPagoTarjeta_CuentaContable] FOREIGN KEY ([IDCuentaContable]) REFERENCES [dbo].[CuentaContable] ([ID]),
    CONSTRAINT [FK_CuentaFijaFormaPagoTarjeta_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_CuentaFijaFormaPagoTarjeta_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


﻿CREATE TABLE [dbo].[ResumenMovimientoSaldo] (
    [ID]          INT           NOT NULL,
    [IDCliente]   INT           NOT NULL,
    [RazonSocial] VARCHAR (100) NOT NULL,
    [Saldo]       MONEY         NOT NULL,
    CONSTRAINT [PK_ResumenMovimientoSaldo] PRIMARY KEY CLUSTERED ([ID] ASC, [IDCliente] ASC)
);


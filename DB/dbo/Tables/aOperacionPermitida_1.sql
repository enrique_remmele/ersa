﻿CREATE TABLE [dbo].[aOperacionPermitida] (
    [ID]                INT          NOT NULL,
    [IDOperacion]       INT          NULL,
    [IDTipoComprobante] SMALLINT     NULL,
    [IDTipoOperacion]   TINYINT      NULL,
    [Estado]            BIT          NULL,
    [Fecha]             DATETIME     NULL,
    [IDUsuario]         INT          NULL,
    [Operacion]         VARCHAR (10) NULL
);


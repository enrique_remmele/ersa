﻿CREATE TABLE [dbo].[Area] (
    [ID]          INT          NOT NULL,
    [IDSucursal]  TINYINT      NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          NULL,
    [Referencia]  VARCHAR (10) NULL,
    CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Area_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


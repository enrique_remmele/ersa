﻿CREATE TABLE [dbo].[ProductoCombinado] (
    [IDProducto]           INT             NOT NULL,
    [IDProductoPrincipal]  INT             NOT NULL,
    [IDProductoSecundario] INT             NOT NULL,
    [CantidadPrincipal]    DECIMAL (10, 2) CONSTRAINT [DF_ProductoCombinado_CantidadPrincipal] DEFAULT ((1)) NOT NULL,
    [CantidadSecundario]   DECIMAL (10, 2) CONSTRAINT [DF_ProductoCombinado_CantidadSecundario] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ProductoCombinado] PRIMARY KEY CLUSTERED ([IDProducto] ASC, [IDProductoPrincipal] ASC, [IDProductoSecundario] ASC)
);


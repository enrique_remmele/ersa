﻿CREATE TABLE [dbo].[AsociarVentaRetencion] (
    [IDTransaccion]          NUMERIC (18)   NOT NULL,
    [IDSucursal]             TINYINT        NOT NULL,
    [Numero]                 INT            NOT NULL,
    [IDCliente]              INT            NOT NULL,
    [Fecha]                  DATE           NOT NULL,
    [IDTransaccionRetencion] NUMERIC (18)   NOT NULL,
    [IDFormaPago]            TINYINT        NULL,
    [PorcentajeRetencion]    DECIMAL (5, 2) CONSTRAINT [DF_AsociarVentaRetencion_PorcentajeRetencion] DEFAULT ((0)) NOT NULL,
    [Total]                  MONEY          CONSTRAINT [DF_AsociarVentaRetencion_Total] DEFAULT ((0)) NOT NULL,
    [Anulado]                BIT            CONSTRAINT [DF_AsociarVentaRetencion_Anulado] DEFAULT ('False') NULL,
    [Observacion]            VARCHAR (200)  NULL,
    CONSTRAINT [PK_AsociarVentaRetencion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


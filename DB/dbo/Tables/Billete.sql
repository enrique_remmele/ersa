﻿CREATE TABLE [dbo].[Billete] (
    [ID]      TINYINT NOT NULL,
    [Valor]   MONEY   CONSTRAINT [DF_Billete_Valor] DEFAULT ((0)) NULL,
    [Decimal] BIT     CONSTRAINT [DF_Billete_Decimal] DEFAULT ('false') NULL,
    CONSTRAINT [PK_Billete] PRIMARY KEY CLUSTERED ([ID] ASC)
);


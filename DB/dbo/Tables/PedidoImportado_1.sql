﻿CREATE TABLE [dbo].[PedidoImportado] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [NroPedido]     INT          NULL,
    [Operador]      VARCHAR (10) NULL,
    [NroFactura]    VARCHAR (50) NULL,
    [FechaFactura]  VARCHAR (50) NULL,
    CONSTRAINT [PK_PedidoImportado] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


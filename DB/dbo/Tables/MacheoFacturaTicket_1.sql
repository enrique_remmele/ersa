﻿CREATE TABLE [dbo].[MacheoFacturaTicket] (
    [IDTransaccion]      NUMERIC (18)  NOT NULL,
    [Numero]             INT           NOT NULL,
    [IDTipoComprobante]  VARCHAR (50)  NOT NULL,
    [NroComprobante]     VARCHAR (50)  NOT NULL,
    [IDSucursal]         TINYINT       NOT NULL,
    [Fecha]              DATE          NOT NULL,
    [IDMoneda]           TINYINT       NOT NULL,
    [Cotizacion]         MONEY         NOT NULL,
    [Observacion]        VARCHAR (150) NULL,
    [NroAcuerdo]         INT           NOT NULL,
    [IDTransaccionGasto] NUMERIC (18)  NOT NULL,
    [Anulado]            BIT           NULL,
    CONSTRAINT [PK_MacheoFacturaTicket] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_MacheoFacturaTicket_Acuerdo] FOREIGN KEY ([NroAcuerdo]) REFERENCES [dbo].[Acuerdo] ([Numero]),
    CONSTRAINT [FK_MacheoFacturaTicket_Gasto] FOREIGN KEY ([IDTransaccionGasto]) REFERENCES [dbo].[Gasto] ([IDTransaccion])
);


GO
ALTER TABLE [dbo].[MacheoFacturaTicket] NOCHECK CONSTRAINT [FK_MacheoFacturaTicket_Acuerdo];


GO
ALTER TABLE [dbo].[MacheoFacturaTicket] NOCHECK CONSTRAINT [FK_MacheoFacturaTicket_Gasto];


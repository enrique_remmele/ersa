﻿CREATE TABLE [dbo].[Transporte] (
    [ID]                TINYINT      NOT NULL,
    [Descripcion]       VARCHAR (50) NOT NULL,
    [IDZonaVenta]       TINYINT      NULL,
    [IDDistribuidor]    TINYINT      NULL,
    [IDCamion]          INT          NULL,
    [IDChofer]          TINYINT      NULL,
    [FechaModificacion] DATE         NOT NULL,
    CONSTRAINT [PK_Transporte] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Transporte_Camion] FOREIGN KEY ([IDCamion]) REFERENCES [dbo].[Camion] ([ID]),
    CONSTRAINT [FK_Transporte_Chofer] FOREIGN KEY ([IDChofer]) REFERENCES [dbo].[Chofer] ([ID]),
    CONSTRAINT [FK_Transporte_Distribuidor] FOREIGN KEY ([IDDistribuidor]) REFERENCES [dbo].[Distribuidor] ([ID])
);


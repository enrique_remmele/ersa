﻿CREATE TABLE [dbo].[ProductoPrecio] (
    [IDProducto]                     INT     NOT NULL,
    [IDCliente]                      INT     NOT NULL,
    [IDMoneda]                       TINYINT NOT NULL,
    [PrecioUnico]                    BIT     NOT NULL,
    [Precio]                         MONEY   NOT NULL,
    [Desde]                          DATE    NOT NULL,
    [IDTransaccionPedidoPrecioUnico] INT     NULL,
    [CantidadMinimaPrecioUnico]      INT     NOT NULL,
    CONSTRAINT [PK_ProductoPrecio] PRIMARY KEY CLUSTERED ([IDProducto] ASC, [IDCliente] ASC, [IDMoneda] ASC, [PrecioUnico] ASC)
);


﻿CREATE TABLE [dbo].[Cliente] (
    [ID]                    INT              NOT NULL,
    [RazonSocial]           VARCHAR (100)    NOT NULL,
    [RUC]                   VARCHAR (15)     NOT NULL,
    [CI]                    BIT              CONSTRAINT [DF_Cliente_CI] DEFAULT ('False') NULL,
    [Referencia]            VARCHAR (50)     NOT NULL,
    [NombreFantasia]        VARCHAR (100)    NULL,
    [Direccion]             VARCHAR (100)    NULL,
    [Telefono]              VARCHAR (20)     NULL,
    [IDEstado]              TINYINT          NULL,
    [IDPais]                TINYINT          NULL,
    [IDDepartamento]        TINYINT          NULL,
    [IDCiudad]              SMALLINT         NULL,
    [IDBarrio]              SMALLINT         NULL,
    [IDZonaVenta]           TINYINT          NULL,
    [IDListaPrecio]         INT              NULL,
    [IDTipoCliente]         TINYINT          NULL,
    [IDCuentaContable]      INT              NULL,
    [CodigoCuentaContable]  VARCHAR (50)     NULL,
    [Contado]               BIT              NULL,
    [Credito]               BIT              NULL,
    [IDMoneda]              TINYINT          NULL,
    [FechaAlta]             DATE             NULL,
    [IDUsuarioAlta]         INT              NULL,
    [FechaModificacion]     DATE             NULL,
    [IDUsuarioModificacion] SMALLINT         NULL,
    [FechaUltimaCompra]     DATE             NULL,
    [FechaUltimaCobranza]   DATE             NULL,
    [Aniversario]           DATE             NULL,
    [IDSucursal]            TINYINT          NULL,
    [IDPromotor]            TINYINT          NULL,
    [IDVendedor]            SMALLINT         NULL,
    [IDCobrador]            TINYINT          NULL,
    [IDDistribuidor]        TINYINT          NULL,
    [LimiteCredito]         MONEY            NULL,
    [Deuda]                 MONEY            CONSTRAINT [DF_Cliente_Deuda] DEFAULT ((0)) NULL,
    [Descuento]             NUMERIC (2)      NULL,
    [PlazoCredito]          INT              NULL,
    [PlazoCobro]            INT              NULL,
    [PlazoChequeDiferido]   INT              NULL,
    [IVAExento]             BIT              NULL,
    [PaginaWeb]             VARCHAR (50)     NULL,
    [Fax]                   VARCHAR (20)     NULL,
    [Email]                 VARCHAR (50)     NULL,
    [Horario]               VARCHAR (50)     NULL,
    [Latitud]               DECIMAL (18, 16) NULL,
    [Longitud]              DECIMAL (18, 16) NULL,
    [Observacion]           VARCHAR (254)    NULL,
    [ClienteVario]          BIT              NULL,
    [IDArea]                TINYINT          NULL,
    [IDRuta]                INT              NULL,
    [celular]               VARCHAR (20)     NULL,
    [IDAbogado]             INT              NULL,
    [Judicial]              BIT              NULL,
    [Firma]                 IMAGE            NULL,
    [Boleta]                BIT              NULL,
    [IdCategoriaCliente]    INT              NULL,
    [IDClienteProducto]     INT              NULL,
    CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Cliente_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_Cliente_Usuario] FOREIGN KEY ([IDUsuarioAlta]) REFERENCES [dbo].[Usuario] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Cliente_IDBarrio]
    ON [dbo].[Cliente]([IDBarrio] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cliente_IDCiudad]
    ON [dbo].[Cliente]([IDCiudad] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cliente_IDZonaVenta]
    ON [dbo].[Cliente]([IDZonaVenta] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cliente_RazonSocial]
    ON [dbo].[Cliente]([RazonSocial] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cliente_Referencia]
    ON [dbo].[Cliente]([Referencia] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Cliente_RUC]
    ON [dbo].[Cliente]([RUC] ASC);


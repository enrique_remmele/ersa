﻿CREATE TABLE [dbo].[Banco] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Referencia]  VARCHAR (5)  NULL,
    [Estado]      BIT          CONSTRAINT [DF_Banco_Estado] DEFAULT ('True') NULL,
    [IDSucursal]  TINYINT      NULL,
    CONSTRAINT [PK_Banco] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[Descuento] (
    [IDTransaccion]         NUMERIC (18)   NOT NULL,
    [IDProducto]            INT            NOT NULL,
    [ID]                    TINYINT        NOT NULL,
    [IDDescuento]           TINYINT        NOT NULL,
    [IDDeposito]            TINYINT        NOT NULL,
    [IDTipoDescuento]       TINYINT        NOT NULL,
    [IDActividad]           INT            NULL,
    [Descuento]             MONEY          NOT NULL,
    [Porcentaje]            NUMERIC (5, 2) NOT NULL,
    [DescuentoDiscriminado] MONEY          CONSTRAINT [DF_Descuento_DescuentoDiscriminado] DEFAULT ((0)) NULL,
    [DecuentoImpuesto]      MONEY          NULL,
    CONSTRAINT [PK_Descuento] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC, [IDDescuento] ASC),
    CONSTRAINT [FK_Descuento_Actividad] FOREIGN KEY ([IDActividad]) REFERENCES [dbo].[Actividad] ([ID]),
    CONSTRAINT [FK_Descuento_DetalleVenta] FOREIGN KEY ([IDTransaccion], [IDProducto], [ID]) REFERENCES [dbo].[DetalleVenta] ([IDTransaccion], [IDProducto], [ID])
);


GO
ALTER TABLE [dbo].[Descuento] NOCHECK CONSTRAINT [FK_Descuento_Actividad];


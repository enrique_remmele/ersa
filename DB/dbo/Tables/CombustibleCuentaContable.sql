﻿CREATE TABLE [dbo].[CombustibleCuentaContable] (
    [IDCuentaContable] INT NOT NULL,
    [Estado]           BIT CONSTRAINT [DF_CombustibleCuentaContable_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_CombustibleCuentaContable] PRIMARY KEY CLUSTERED ([IDCuentaContable] ASC)
);


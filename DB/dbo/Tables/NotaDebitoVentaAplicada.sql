﻿CREATE TABLE [dbo].[NotaDebitoVentaAplicada] (
    [IDTransaccionNotaDebitoAplicacion] NUMERIC (18) NOT NULL,
    [IDTransaccionNotaDebito]           NUMERIC (18) NOT NULL,
    [IDTransaccionVenta]                NUMERIC (18) NOT NULL,
    [Importe]                           MONEY        NOT NULL,
    [Cobrado]                           MONEY        NOT NULL,
    [Acreditado]                        MONEY        NOT NULL,
    [Saldo]                             MONEY        NOT NULL,
    CONSTRAINT [PK_NotaDebitoVentaAplicada] PRIMARY KEY CLUSTERED ([IDTransaccionNotaDebitoAplicacion] ASC, [IDTransaccionNotaDebito] ASC, [IDTransaccionVenta] ASC),
    CONSTRAINT [FK_NotaDebitoVentaAplicada_NotaDebito] FOREIGN KEY ([IDTransaccionNotaDebito]) REFERENCES [dbo].[NotaDebito] ([IDTransaccion]),
    CONSTRAINT [FK_NotaDebitoVentaAplicada_NotaDebitoAplicacion] FOREIGN KEY ([IDTransaccionNotaDebitoAplicacion]) REFERENCES [dbo].[NotaDebitoAplicacion] ([IDTransaccion]),
    CONSTRAINT [FK_NotaDebitoVentaAplicada_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


﻿CREATE TABLE [dbo].[Impresion] (
    [IDImpresion]  TINYINT       NOT NULL,
    [Descripcion]  VARCHAR (50)  NOT NULL,
    [Detalle]      BIT           CONSTRAINT [DF_Impresion_Detalle] DEFAULT ('True') NOT NULL,
    [Vista]        VARCHAR (50)  NULL,
    [VistaDetalle] VARCHAR (50)  NULL,
    [Observacion]  VARCHAR (100) NULL,
    [IDOperacion]  INT           NULL,
    CONSTRAINT [PK_Impresion] PRIMARY KEY CLUSTERED ([IDImpresion] ASC)
);


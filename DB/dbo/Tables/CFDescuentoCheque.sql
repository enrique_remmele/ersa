﻿CREATE TABLE [dbo].[CFDescuentoCheque] (
    [IDOperacion]      TINYINT NOT NULL,
    [ID]               TINYINT NOT NULL,
    [CuentaBancaria]   BIT     NULL,
    [GastoBancario]    BIT     NULL,
    [Cheque]           BIT     NULL,
    [IDSucursalCheque] TINYINT NULL,
    CONSTRAINT [PK_CFDescuentoCheque] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[CFAjuste] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    [Sobrante]    BIT     NULL,
    [Faltante]    BIT     NULL,
    CONSTRAINT [PK_CFAjuste] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[DetalleGastoDebitoCredito] (
    [IDTransaccionGasto]         NUMERIC (18) NOT NULL,
    [IDTransaccionDebitoCredito] NUMERIC (18) NOT NULL,
    CONSTRAINT [PK_DetalleGastoDebitoCredito] PRIMARY KEY CLUSTERED ([IDTransaccionGasto] ASC, [IDTransaccionDebitoCredito] ASC)
);


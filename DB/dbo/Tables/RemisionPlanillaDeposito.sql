﻿CREATE TABLE [dbo].[RemisionPlanillaDeposito] (
    [IDDeposito] TINYINT         NOT NULL,
    [Numero]     SMALLINT        NOT NULL,
    [ID]         SMALLINT        NOT NULL,
    [Creado]     DATE            NULL,
    [IDProducto] INT             NULL,
    [Cantidad]   DECIMAL (10, 2) NULL,
    [Cargado]    BIT             NULL,
    [Parte]      INT             NULL,
    CONSTRAINT [PK_RemisionPlanillaDeposito] PRIMARY KEY CLUSTERED ([IDDeposito] ASC, [Numero] ASC, [ID] ASC)
);


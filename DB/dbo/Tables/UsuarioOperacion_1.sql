﻿CREATE TABLE [dbo].[UsuarioOperacion] (
    [IDUsuario]   INT NOT NULL,
    [IDOperacion] INT NOT NULL,
    CONSTRAINT [PK_UsuarioOperacion] PRIMARY KEY CLUSTERED ([IDUsuario] ASC, [IDOperacion] ASC),
    CONSTRAINT [FK_UsuarioOperacion_UsuarioPermitido] FOREIGN KEY ([IDUsuario]) REFERENCES [dbo].[UsuarioPermitido] ([IDUsuario]) ON DELETE CASCADE ON UPDATE CASCADE
);


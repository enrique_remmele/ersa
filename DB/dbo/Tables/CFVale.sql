﻿CREATE TABLE [dbo].[CFVale] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    CONSTRAINT [PK_CFVale] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


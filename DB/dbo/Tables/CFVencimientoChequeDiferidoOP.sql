﻿CREATE TABLE [dbo].[CFVencimientoChequeDiferidoOP] (
    [IDOperacion]   TINYINT NULL,
    [ID]            TINYINT NULL,
    [Cheque]        BIT     NULL,
    [Banco]         BIT     NULL,
    [BuscarEnBanco] BIT     NULL
);


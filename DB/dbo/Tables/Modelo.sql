﻿CREATE TABLE [dbo].[Modelo] (
    [ID]          SMALLINT     NOT NULL,
    [IDMarca]     TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Modelo] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Modelo_Marca] FOREIGN KEY ([IDMarca]) REFERENCES [dbo].[Marca] ([ID])
);


﻿CREATE TABLE [dbo].[ResumenMovimientoSaldoDetallado] (
    [ID]            INT           NOT NULL,
    [IDCliente]     INT           NOT NULL,
    [RazonSocial]   VARCHAR (100) NOT NULL,
    [TotalVenta]    MONEY         NOT NULL,
    [TotalCobranza] MONEY         NOT NULL,
    [TotalDebito]   MONEY         NOT NULL,
    [TotalCredito]  MONEY         NOT NULL,
    [SaldoAnterior] MONEY         NOT NULL,
    [Saldo]         MONEY         NOT NULL,
    CONSTRAINT [PK_ResumenMovimientoSaldoDetallado] PRIMARY KEY CLUSTERED ([ID] ASC, [IDCliente] ASC)
);


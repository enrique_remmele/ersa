﻿CREATE TABLE [dbo].[TipoProductoCuentaContableMotivoNC] (
    [IDTipoProducto]                 INT          NULL,
    [CuentaContableAcuerdo]          VARCHAR (50) NULL,
    [CuentaContableDiferenciaPrecio] VARCHAR (50) NULL,
    [CuentaContableDescuento]        VARCHAR (50) NULL,
    [UltimoUsuarioModificacion]      INT          NULL,
    [UltimaFechaModificacion]        DATETIME     NULL
);


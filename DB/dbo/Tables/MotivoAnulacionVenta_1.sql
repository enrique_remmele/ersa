﻿CREATE TABLE [dbo].[MotivoAnulacionVenta] (
    [ID]                    INT           NOT NULL,
    [Descripcion]           VARCHAR (100) NOT NULL,
    [Estado]                BIT           NULL,
    [MantenerPedidoVigente] BIT           CONSTRAINT [DF_MotivoAnulacionVenta_MantenerPedidoVigente] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Motivo] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[aConciliacionAsiento] (
    [IDTransaccion]          NUMERIC (18) NOT NULL,
    [Conciliado]             BIT          NULL,
    [FechaConciliado]        DATETIME     NOT NULL,
    [IDUsuarioConciliado]    INT          NOT NULL,
    [FechaDesconciliado]     DATETIME     NOT NULL,
    [IDUsuarioDesconciliado] INT          NOT NULL
);


﻿CREATE TABLE [dbo].[ExistenciaDepositoInventarioEquipo] (
    [IDTransaccion] NUMERIC (18)    NOT NULL,
    [IDDeposito]    TINYINT         NOT NULL,
    [IDProducto]    INT             NOT NULL,
    [IDEquipo]      SMALLINT        NOT NULL,
    [Averiados]     DECIMAL (10, 2) NOT NULL,
    [Conteo]        DECIMAL (10, 2) NOT NULL,
    [Total]         DECIMAL (10, 2) NOT NULL,
    CONSTRAINT [PK_ExistenciaDepositoInventarioEquipo] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDDeposito] ASC, [IDProducto] ASC, [IDEquipo] ASC),
    CONSTRAINT [FK_ExistenciaDepositoInventarioEquipo_EquipoConteo] FOREIGN KEY ([IDEquipo]) REFERENCES [dbo].[EquipoConteo] ([ID]),
    CONSTRAINT [FK_ExistenciaDepositoInventarioEquipo_ExistenciaDepositoInventario] FOREIGN KEY ([IDTransaccion], [IDDeposito], [IDProducto]) REFERENCES [dbo].[ExistenciaDepositoInventario] ([IDTransaccion], [IDDeposito], [IDProducto])
);


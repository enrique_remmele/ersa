﻿CREATE TABLE [dbo].[ProductoLote] (
    [IDProducto]  INT          NOT NULL,
    [ID]          INT          NOT NULL,
    [Lote]        VARCHAR (30) NOT NULL,
    [Elaboracion] DATE         NULL,
    [Vencimiento] DATE         NOT NULL,
    CONSTRAINT [PK_ProductoLote] PRIMARY KEY CLUSTERED ([IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_ProductoLote_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID])
);


﻿CREATE TABLE [dbo].[Ruta] (
    [ID]          INT          NOT NULL,
    [Descripcion] VARCHAR (50) NULL,
    [Estado]      BIT          CONSTRAINT [DF_Ruta_Estado] DEFAULT ('True') NULL,
    [Referencia]  VARCHAR (10) NULL,
    [IDSucursal]  TINYINT      NULL,
    [IDArea]      TINYINT      NULL,
    [IDZonaVenta] TINYINT      NULL,
    CONSTRAINT [PK_Ruta] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[SolicitudProductoListaPrecioExcepciones] (
    [IDProducto]         INT            NOT NULL,
    [IDListaPrecio]      INT            NOT NULL,
    [IDCliente]          INT            NOT NULL,
    [IDSucursal]         TINYINT        NULL,
    [IDTipoDescuento]    TINYINT        NOT NULL,
    [IDMoneda]           TINYINT        NOT NULL,
    [Descuento]          MONEY          NOT NULL,
    [Porcentaje]         DECIMAL (9, 6) NOT NULL,
    [Desde]              DATE           NULL,
    [Hasta]              DATE           NULL,
    [CantidadLimite]     INT            NULL,
    [FechaSolicitud]     DATETIME       NULL,
    [IDUsuarioSolicitud] INT            NULL,
    [FechaAprobado]      DATETIME       NULL,
    [IDUsuarioAprobado]  INT            NULL,
    [Estado]             BIT            NULL,
    [IDTerminalAprobado] INT            NULL
);


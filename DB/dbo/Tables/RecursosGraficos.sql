﻿CREATE TABLE [dbo].[RecursosGraficos] (
    [ID]          INT           NULL,
    [Formulario]  VARCHAR (100) NULL,
    [Imagen]      IMAGE         NULL,
    [Descripcion] VARCHAR (50)  NULL,
    [Version]     INT           NULL
);


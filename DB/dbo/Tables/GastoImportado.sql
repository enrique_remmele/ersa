﻿CREATE TABLE [dbo].[GastoImportado] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [NroOperacion]  INT          NOT NULL,
    [Importe]       MONEY        NULL,
    [Pagado]        MONEY        NULL,
    [Saldo]         MONEY        NULL,
    CONSTRAINT [PK_GastoImportado] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


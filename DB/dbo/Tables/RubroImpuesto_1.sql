﻿CREATE TABLE [dbo].[RubroImpuesto] (
    [ID]             SMALLINT      NOT NULL,
    [Rubro]          VARCHAR (200) NULL,
    [CodigoRubro]    TINYINT       NULL,
    [Inc]            CHAR (2)      NULL,
    [DescripcionInc] VARCHAR (200) NULL,
    [Codigo]         SMALLINT      NULL,
    CONSTRAINT [PK_RubroIVA] PRIMARY KEY CLUSTERED ([ID] ASC)
);


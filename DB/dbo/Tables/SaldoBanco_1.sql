﻿CREATE TABLE [dbo].[SaldoBanco] (
    [ID]                 TINYINT      NULL,
    [Banco]              VARCHAR (50) NULL,
    [Debito]             MONEY        NULL,
    [Credito]            MONEY        NULL,
    [SaldoContable]      MONEY        NULL,
    [DepositoAConfirmar] MONEY        NULL,
    [ChequesNoPagados]   MONEY        NULL,
    [SaldoConciliado]    MONEY        NULL
);


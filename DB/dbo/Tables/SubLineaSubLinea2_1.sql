﻿CREATE TABLE [dbo].[SubLineaSubLinea2] (
    [IDTipoProducto] TINYINT  NOT NULL,
    [IDLinea]        SMALLINT NOT NULL,
    [IDSubLinea]     SMALLINT NOT NULL,
    [IDSubLinea2]    SMALLINT NOT NULL,
    CONSTRAINT [PK_SubLineaSubLinea2] PRIMARY KEY CLUSTERED ([IDTipoProducto] ASC, [IDLinea] ASC, [IDSubLinea] ASC, [IDSubLinea2] ASC),
    CONSTRAINT [FK_SubLineaSubLinea2_LineaSubLinea] FOREIGN KEY ([IDTipoProducto], [IDLinea], [IDSubLinea]) REFERENCES [dbo].[LineaSubLinea] ([IDTipoProducto], [IDLinea], [IDSubLinea]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_SubLineaSubLinea2_SubLinea2] FOREIGN KEY ([IDSubLinea2]) REFERENCES [dbo].[SubLinea2] ([ID])
);


﻿CREATE TABLE [dbo].[FormaPagoTarjeta] (
    [IDTransaccion]           NUMERIC (18)  NOT NULL,
    [ID]                      TINYINT       NOT NULL,
    [IDSucursal]              TINYINT       NOT NULL,
    [IDTipoComprobante]       SMALLINT      NOT NULL,
    [Comprobante]             VARCHAR (50)  NOT NULL,
    [Fecha]                   DATETIME      NOT NULL,
    [IDMoneda]                TINYINT       NOT NULL,
    [ImporteMoneda]           MONEY         NOT NULL,
    [Cotizacion]              MONEY         NOT NULL,
    [Total]                   MONEY         NULL,
    [Saldo]                   MONEY         NULL,
    [Observacion]             VARCHAR (100) NULL,
    [Cancelado]               BIT           NULL,
    [TerminacionTarjeta]      CHAR (4)      NULL,
    [FechaVencimientoTarjeta] SMALLDATETIME NULL,
    [IDTipoTarjeta]           TINYINT       NULL,
    [Importe]                 MONEY         NULL,
    [TipoComprobante]         VARCHAR (50)  NULL,
    [Moneda]                  VARCHAR (50)  NULL,
    [Boleta]                  VARCHAR (50)  NULL,
    CONSTRAINT [PK_FormaPagoTarjeta] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC)
);


GO
CREATE TRIGGER [dbo].[TIQuitarPuntuacionesFPT] ON  [dbo].[FormaPagoTarjeta]
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;
	/*JGR 20140808 Elimina las puntuaciones en el comprobante*/
    UPDATE FormaPagoTarjeta
	SET 
      FormaPagoTarjeta.Comprobante = LTRIM(RTRIM(REPLACE(FormaPagoTarjeta.Comprobante, '.', '')))
	FROM FormaPagoTarjeta JOIN INSERTED ON FormaPagoTarjeta.IDTransaccion = INSERTED.IDTransaccion
END

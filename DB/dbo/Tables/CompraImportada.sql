﻿CREATE TABLE [dbo].[CompraImportada] (
    [IDTransaccion]  NUMERIC (18) NOT NULL,
    [NroOperacion]   INT          NOT NULL,
    [NroComprobante] VARCHAR (50) NULL,
    CONSTRAINT [PK_CompraImportada] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


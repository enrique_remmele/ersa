﻿CREATE TABLE [dbo].[AnticipoVenta] (
    [IDTransaccionAnticipo] NUMERIC (18) NOT NULL,
    [IDTransaccionVenta]    NUMERIC (18) NOT NULL,
    [Importe]               MONEY        NOT NULL,
    [Cancelar]              BIT          CONSTRAINT [DF_AnticipoVenta_Cancelar] DEFAULT ('False') NOT NULL,
    [ImporteGs]             MONEY        NULL,
    CONSTRAINT [PK_AnticipoVenta] PRIMARY KEY CLUSTERED ([IDTransaccionAnticipo] ASC, [IDTransaccionVenta] ASC),
    CONSTRAINT [FK_AnticipoVenta_AnticipoAplicacion] FOREIGN KEY ([IDTransaccionAnticipo]) REFERENCES [dbo].[AnticipoAplicacion] ([IDTransaccion]),
    CONSTRAINT [FK_AnticipoVenta_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


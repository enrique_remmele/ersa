﻿CREATE TABLE [dbo].[UnidadMedida] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Referencia]  VARCHAR (6)  NULL,
    [Estado]      BIT          CONSTRAINT [DF_UnidadMedida_Estado] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_UnidadMedida] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[GastoVale] (
    [IDTransaccionGasto] NUMERIC (18) NOT NULL,
    [IDTransaccionVale]  NUMERIC (18) NOT NULL,
    [Importe]            MONEY        NULL,
    CONSTRAINT [PK_GastoVale] PRIMARY KEY CLUSTERED ([IDTransaccionGasto] ASC, [IDTransaccionVale] ASC),
    CONSTRAINT [FK_GastoVale_Gasto] FOREIGN KEY ([IDTransaccionGasto]) REFERENCES [dbo].[Gasto] ([IDTransaccion]),
    CONSTRAINT [FK_GastoVale_Vale] FOREIGN KEY ([IDTransaccionVale]) REFERENCES [dbo].[Vale] ([IDTransaccion])
);


﻿CREATE TABLE [dbo].[aDetalleDepositoBancario] (
    [IDAuditoria]                   INT          NOT NULL,
    [IDTransaccionDepositoBancario] NUMERIC (18) NOT NULL,
    [ID]                            INT          NOT NULL,
    [IDTransaccionEfectivo]         NUMERIC (18) NULL,
    [IDEfectivo]                    SMALLINT     NULL,
    [IDTransaccionCheque]           NUMERIC (18) NULL,
    [IDTransaccionDocumento]        NUMERIC (18) NULL,
    [Redondeo]                      BIT          NULL,
    [Faltante]                      BIT          NULL,
    [Sobrante]                      BIT          NULL,
    [Importe]                       MONEY        NULL,
    [IDTransaccionTarjeta]          NUMERIC (18) NULL,
    [IDDetalleDocumento]            INT          NULL,
    [IDUsuario]                     INT          NOT NULL,
    [Accion]                        VARCHAR (3)  NOT NULL
);


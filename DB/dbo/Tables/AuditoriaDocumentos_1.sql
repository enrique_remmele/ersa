﻿CREATE TABLE [dbo].[AuditoriaDocumentos] (
    [IDTipoComprobante] INT          NULL,
    [IDCliente]         INT          NULL,
    [IDProveedor]       INT          NULL,
    [FechaDocumento]    DATE         NULL,
    [Comprobante]       VARCHAR (16) NULL,
    [Importe]           MONEY        NULL,
    [Condicion]         VARCHAR (16) NULL,
    [FechaModificacion] DATETIME     NULL,
    [IDUsuario]         INT          NULL,
    [Accion]            VARCHAR (3)  NULL,
    [TipoComprobante]   VARCHAR (16) NULL,
    [IDSucursal]        INT          NULL
);


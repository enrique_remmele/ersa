﻿CREATE TABLE [dbo].[OperacionPermitida] (
    [ID]                INT      NOT NULL,
    [IDOperacion]       INT      NULL,
    [IDTipoComprobante] SMALLINT NULL,
    [IDTipoOperacion]   TINYINT  NULL,
    [Estado]            BIT      NULL,
    CONSTRAINT [PK_OperacionPermitida] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_OperacionPermitida_Operacion] FOREIGN KEY ([IDOperacion]) REFERENCES [dbo].[Operacion] ([ID]),
    CONSTRAINT [FK_OperacionPermitida_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID]),
    CONSTRAINT [FK_OperacionPermitida_TipoOperacion] FOREIGN KEY ([IDTipoOperacion]) REFERENCES [dbo].[TipoOperacion] ([ID])
);


﻿CREATE TABLE [dbo].[CFCanjeCheque] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    CONSTRAINT [PK_CFCanjeCheque] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


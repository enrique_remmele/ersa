﻿CREATE TABLE [dbo].[CFVentaCostoMercaderia] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    CONSTRAINT [PK_CFVentaCostoMercaderia] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


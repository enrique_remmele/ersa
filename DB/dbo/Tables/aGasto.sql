﻿CREATE TABLE [dbo].[aGasto] (
    [IDTransaccion]            NUMERIC (18)  NOT NULL,
    [Numero]                   INT           NOT NULL,
    [IDTipoComprobante]        SMALLINT      NOT NULL,
    [NroComprobante]           VARCHAR (50)  NOT NULL,
    [IDProveedor]              INT           NOT NULL,
    [IDSucursal]               TINYINT       NOT NULL,
    [IDGrupo]                  TINYINT       NULL,
    [Fecha]                    DATE          NOT NULL,
    [Credito]                  BIT           NOT NULL,
    [FechaVencimiento]         DATE          NULL,
    [IDMoneda]                 TINYINT       NOT NULL,
    [Cotizacion]               MONEY         NULL,
    [Observacion]              VARCHAR (500) NULL,
    [IDTransaccionRemision]    NUMERIC (18)  NULL,
    [Total]                    MONEY         NOT NULL,
    [TotalImpuesto]            MONEY         NOT NULL,
    [TotalDiscriminado]        MONEY         NOT NULL,
    [Saldo]                    MONEY         NOT NULL,
    [Cancelado]                BIT           NOT NULL,
    [Directo]                  BIT           NULL,
    [Efectivo]                 BIT           NULL,
    [Cheque]                   BIT           NULL,
    [CajaChica]                BIT           NULL,
    [Cuota]                    TINYINT       NULL,
    [RetencionIVA]             MONEY         NULL,
    [RetencionRenta]           MONEY         NULL,
    [NroTimbrado]              VARCHAR (50)  NULL,
    [FechaVencimientoTimbrado] DATE          NULL,
    [IncluirLibro]             BIT           NULL,
    [IDDeposito]               TINYINT       NULL,
    [IDChofer]                 TINYINT       NULL,
    [IDCamion]                 TINYINT       NULL,
    [Pagar]                    BIT           NULL,
    [ImporteAPagar]            MONEY         NULL,
    [IDCuentaBancaria]         TINYINT       NULL,
    [ObservacionPago]          VARCHAR (100) NULL,
    [RRHH]                     BIT           NULL,
    [IDDepartamentoEmpresa]    TINYINT       NULL,
    [FechaModificacion]        DATETIME      NOT NULL,
    [IDUsuarioModificacion]    INT           NOT NULL,
    [IDTerminalModificacion]   INT           NOT NULL,
    [Operacion]                VARCHAR (10)  NULL
);


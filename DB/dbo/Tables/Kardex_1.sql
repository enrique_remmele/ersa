﻿CREATE TABLE [dbo].[Kardex] (
    [IDProducto]             INT             NOT NULL,
    [IDKardex]               NUMERIC (18)    NOT NULL,
    [Indice]                 INT             NULL,
    [Fecha]                  DATETIME        NOT NULL,
    [IDTransaccion]          NUMERIC (18)    NOT NULL,
    [ID]                     TINYINT         NOT NULL,
    [Orden]                  TINYINT         NULL,
    [IDSucursal]             TINYINT         NULL,
    [Comprobante]            VARCHAR (20)    NULL,
    [Observacion]            VARCHAR (100)   NULL,
    [CantidadEntrada]        DECIMAL (10, 2) CONSTRAINT [DF_Kardex_CantidadEntrada] DEFAULT ((0)) NULL,
    [CostoUnitarioEntrada]   MONEY           CONSTRAINT [DF_Kardex_CostoUnitarioEntrada] DEFAULT ((0)) NULL,
    [TotalEntrada]           DECIMAL (18, 6) NULL,
    [CantidadSalida]         DECIMAL (10, 2) CONSTRAINT [DF_Kardex_CantidadSalida] DEFAULT ((0)) NULL,
    [CostoUnitarioSalida]    MONEY           CONSTRAINT [DF_Kardex_CostoUnitarioSalida] DEFAULT ((0)) NULL,
    [TotalSalida]            DECIMAL (18, 6) NULL,
    [CantidadSaldo]          DECIMAL (10, 2) CONSTRAINT [DF_Kardex_CantidadSaldo] DEFAULT ((0)) NULL,
    [CostoPromedio]          DECIMAL (18, 6) NULL,
    [TotalSaldo]             MONEY           CONSTRAINT [DF_Kardex_TotalSaldo] DEFAULT ((0)) NULL,
    [IDKardexAnterior]       NUMERIC (18)    NULL,
    [CostoPromedioOperacion] MONEY           NULL,
    [DiferenciaTotal]        AS              ((case when [CantidadSaldo]<>(0) then [TotalSaldo]/[CantidadSaldo] else [CostoUnitarioEntrada]+[CostoUnitarioSalida] end-[CostoPromedioOperacion])*([CantidadEntrada]+[CantidadSalida])),
    CONSTRAINT [PK_Kardex] PRIMARY KEY CLUSTERED ([IDKardex] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Kardex_IDProducto]
    ON [dbo].[Kardex]([IDProducto] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Kardex_IDProducto_IN_Indice]
    ON [dbo].[Kardex]([IDProducto] ASC, [Fecha] ASC)
    INCLUDE([Indice]);


GO
CREATE NONCLUSTERED INDEX [IX_Kardex_Indice]
    ON [dbo].[Kardex]([Indice] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Kardex_Fecha]
    ON [dbo].[Kardex]([Fecha] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Kardex_IDTransaccion]
    ON [dbo].[Kardex]([IDTransaccion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Kardex_IDKardexAnterior]
    ON [dbo].[Kardex]([IDKardexAnterior] ASC);


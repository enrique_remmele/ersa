﻿CREATE TABLE [dbo].[FormularioImpresion] (
    [IDFormularioImpresion] TINYINT      NOT NULL,
    [IDImpresion]           TINYINT      NOT NULL,
    [Descripcion]           VARCHAR (20) NOT NULL,
    [Detalle]               BIT          NULL,
    [CantidadDetalle]       TINYINT      NULL,
    [Copias]                TINYINT      NULL,
    [Fondo]                 IMAGE        NULL,
    [TamañoFondoX]          SMALLINT     NULL,
    [TamañoFondoY]          SMALLINT     NULL,
    [TamañoPapel]           VARCHAR (50) CONSTRAINT [DF_FormularioImpresion_TamañoPapel] DEFAULT ('A4') NULL,
    [TamañoPapelX]          SMALLINT     CONSTRAINT [DF_FormularioImpresion_TamañoPapelX] DEFAULT ((0)) NULL,
    [TamañoPapelY]          SMALLINT     CONSTRAINT [DF_FormularioImpresion_TamañoPapelY] DEFAULT ((0)) NULL,
    [Horizontal]            BIT          CONSTRAINT [DF_FormularioImpresion_Horizontal] DEFAULT ('False') NULL,
    [MargenIzquierdo]       TINYINT      CONSTRAINT [DF_FormularioImpresion_MargenIzquierdo] DEFAULT ((0)) NULL,
    [MargenDerecho]         TINYINT      CONSTRAINT [DF_FormularioImpresion_MargenDerecho] DEFAULT ((0)) NULL,
    [MargenArriba]          TINYINT      CONSTRAINT [DF_FormularioImpresion_MargenArriba] DEFAULT ((0)) NULL,
    [MargenAbajo]           TINYINT      CONSTRAINT [DF_FormularioImpresion_MargenAbajo] DEFAULT ((0)) NULL,
    [Estado]                BIT          NULL,
    CONSTRAINT [PK_FormularioImpresion] PRIMARY KEY CLUSTERED ([IDFormularioImpresion] ASC),
    CONSTRAINT [FK_FormularioImpresion_Impresion] FOREIGN KEY ([IDImpresion]) REFERENCES [dbo].[Impresion] ([IDImpresion])
);


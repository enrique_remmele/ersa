﻿CREATE TABLE [dbo].[CobranzaCredito] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NULL,
    [Numero]            INT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [NroComprobante]    VARCHAR (50)  NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [NroPlanilla]       NCHAR (10)    NULL,
    [IDCliente]         INT           NOT NULL,
    [FechaEmision]      DATE          NOT NULL,
    [IDCobrador]        TINYINT       NOT NULL,
    [Total]             MONEY         CONSTRAINT [DF_ReciboCliente_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]     MONEY         CONSTRAINT [DF_ReciboCliente_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalDiscriminado] MONEY         CONSTRAINT [DF_ReciboCliente_TotalDiscriminado] DEFAULT ((0)) NOT NULL,
    [TotalDescuento]    MONEY         CONSTRAINT [DF_ReciboCliente_TotalDescuento] DEFAULT ((0)) NULL,
    [Anulado]           BIT           CONSTRAINT [DF_ReciboCliente_Anulado] DEFAULT ('False') NOT NULL,
    [FechaAnulado]      DATE          NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    [Observacion]       VARCHAR (200) NULL,
    [Procesado]         BIT           CONSTRAINT [DF_CobranzaCredito_Procesado] DEFAULT ('False') NULL,
    [IDmoneda]          TINYINT       NULL,
    [Cotizacion]        MONEY         NULL,
    [DiferenciaCambio]  MONEY         NULL,
    [AnticipoCliente]   BIT           CONSTRAINT [DF__CobranzaC__Antic__57B3BA09] DEFAULT ('FALSE') NULL,
    [IDTipoAnticipo]    TINYINT       NULL,
    [ReciboInterno]     BIT           NULL,
    [IDRecibo]          INT           NULL,
    [IDMotivoAnulacion] INT           NULL,
    CONSTRAINT [PK_ReciboCliente] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_Cobranza_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_CobranzaCredito_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [Numero]
    ON [dbo].[CobranzaCredito]([Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [Fecha]
    ON [dbo].[CobranzaCredito]([FechaEmision] ASC);


GO
CREATE TRIGGER [dbo].[TIQuitarPuntuacionesCC] ON  dbo.CobranzaCredito
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;
	/*JGR 20140808 Elimina las puntuaciones en el comprobante*/
    UPDATE dbo.CobranzaCredito
	SET 
      CobranzaCredito.Comprobante = LTRIM(RTRIM(REPLACE(CobranzaCredito.Comprobante, '.', '')))
      , CobranzaCredito.NroPlanilla = LTRIM(RTRIM(REPLACE(CobranzaCredito.NroPlanilla, '.', '')))
	FROM CobranzaCredito JOIN INSERTED ON CobranzaCredito.IDTransaccion = INSERTED.IDTransaccion
END

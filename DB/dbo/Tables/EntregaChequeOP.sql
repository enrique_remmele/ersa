﻿CREATE TABLE [dbo].[EntregaChequeOP] (
    [IDTransaccion]   NUMERIC (18)  NULL,
    [IDTransaccionOP] NUMERIC (18)  NULL,
    [ChequeEntregado] BIT           NULL,
    [FechaEntrega]    DATE          NULL,
    [Recibo]          VARCHAR (50)  NULL,
    [RetiradoPor]     VARCHAR (200) NULL,
    [Anulado]         BIT           NULL
);


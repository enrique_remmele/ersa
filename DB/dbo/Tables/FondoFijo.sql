﻿CREATE TABLE [dbo].[FondoFijo] (
    [IDSucursal]  TINYINT NOT NULL,
    [IDGrupo]     TINYINT NOT NULL,
    [Tope]        MONEY   NULL,
    [TotalRendir] MONEY   NULL,
    [Saldo]       MONEY   NULL,
    CONSTRAINT [PK_FondoFijo] PRIMARY KEY CLUSTERED ([IDSucursal] ASC, [IDGrupo] ASC),
    CONSTRAINT [FK_FondoFijo_Grupo] FOREIGN KEY ([IDGrupo]) REFERENCES [dbo].[Grupo] ([ID]),
    CONSTRAINT [FK_FondoFijo_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


﻿CREATE TABLE [dbo].[LoteDistribucion] (
    [IDTransaccion]        NUMERIC (18)  NOT NULL,
    [IDSucursal]           TINYINT       NOT NULL,
    [Numero]               INT           NOT NULL,
    [IDTipoComprobante]    SMALLINT      NOT NULL,
    [Comprobante]          VARCHAR (50)  NOT NULL,
    [Fecha]                DATETIME      NOT NULL,
    [FechaReparto]         DATE          NOT NULL,
    [IDDistribuidor]       TINYINT       NULL,
    [IDCamion]             INT           NULL,
    [IDChofer]             TINYINT       NULL,
    [IDZonaVenta]          TINYINT       NULL,
    [TotalContado]         MONEY         NOT NULL,
    [TotalCredito]         MONEY         NOT NULL,
    [Total]                MONEY         CONSTRAINT [DF_LoteDistribucion_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]        MONEY         CONSTRAINT [DF_LoteDistribucion_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalDiscriminado]    MONEY         CONSTRAINT [DF_LoteDistribucion_TotalDiscriminado] DEFAULT ((0)) NOT NULL,
    [TotalDescuento]       MONEY         CONSTRAINT [DF_LoteDistribucion_TotalDescuento] DEFAULT ((0)) NULL,
    [CantidadComprobantes] SMALLINT      NOT NULL,
    [Anulado]              BIT           CONSTRAINT [DF_LoteDistribucion_Anulado] DEFAULT ('False') NOT NULL,
    [Observacion]          VARCHAR (100) NULL,
    [FechaAnulado]         BIT           NULL,
    [IDUsuarioAnulado]     SMALLINT      NULL,
    CONSTRAINT [PK_LoteDistribucion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_LoteDistribucion_Camion] FOREIGN KEY ([IDCamion]) REFERENCES [dbo].[Camion] ([ID]),
    CONSTRAINT [FK_LoteDistribucion_Chofer] FOREIGN KEY ([IDChofer]) REFERENCES [dbo].[Chofer] ([ID]),
    CONSTRAINT [FK_LoteDistribucion_Distribuidor] FOREIGN KEY ([IDDistribuidor]) REFERENCES [dbo].[Distribuidor] ([ID]),
    CONSTRAINT [FK_LoteDistribucion_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_LoteDistribucion_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [IX_LoteDistribucion_Chofer]
    ON [dbo].[LoteDistribucion]([IDChofer] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_LoteDistribucion_Fecha]
    ON [dbo].[LoteDistribucion]([Fecha] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_LoteDistribucion_IDCamion]
    ON [dbo].[LoteDistribucion]([IDCamion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_LoteDistribucion_IDDistribuidor]
    ON [dbo].[LoteDistribucion]([IDDistribuidor] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_LoteDistribucion_Numero]
    ON [dbo].[LoteDistribucion]([Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_LoteDistribucion_IDSucursal]
    ON [dbo].[LoteDistribucion]([IDSucursal] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_199_198]
    ON [dbo].[LoteDistribucion]([Anulado] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [IDTipoComprobante], [Comprobante], [Fecha], [IDDistribuidor], [IDCamion], [IDChofer], [IDZonaVenta]);


GO
CREATE NONCLUSTERED INDEX [missing_index_269_268]
    ON [dbo].[LoteDistribucion]([IDSucursal] ASC, [IDTipoComprobante] ASC, [Comprobante] ASC, [Anulado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_477_476]
    ON [dbo].[LoteDistribucion]([IDSucursal] ASC)
    INCLUDE([Numero]);


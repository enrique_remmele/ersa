﻿CREATE TABLE [dbo].[CuentaFijaOrdenPago] (
    [ID]                   TINYINT      NOT NULL,
    [IDCuentaContable]     SMALLINT     NOT NULL,
    [Debe]                 BIT          NOT NULL,
    [Haber]                BIT          NOT NULL,
    [IDTipoComprobante]    SMALLINT     NULL,
    [IDMoneda]             TINYINT      NOT NULL,
    [Orden]                TINYINT      NOT NULL,
    [Descripcion]          VARCHAR (50) NOT NULL,
    [BuscarProveedor]      BIT          NOT NULL,
    [BuscarCuentaBancaria] BIT          NULL,
    [TipoProveedor]        BIT          NULL,
    [TipoCheque]           BIT          NULL,
    [TipoEfectivo]         BIT          NULL,
    CONSTRAINT [PK_CuentaFijaOrdenPago] PRIMARY KEY CLUSTERED ([ID] ASC)
);


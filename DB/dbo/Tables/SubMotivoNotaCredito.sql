﻿CREATE TABLE [dbo].[SubMotivoNotaCredito] (
    [ID]               INT          NOT NULL,
    [Motivo]           VARCHAR (50) NOT NULL,
    [Descripcion]      VARCHAR (50) NOT NULL,
    [Estado]           BIT          NOT NULL,
    [Devolucion]       BIT          NULL,
    [Anulacion]        BIT          NULL,
    [Financiero]       BIT          NULL,
    [DiferenciaPrecio] BIT          NULL,
    [AcuerdoComercial] BIT          NULL,
    [DiferenciaPeso]   BIT          NULL
);


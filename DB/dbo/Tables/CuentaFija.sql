﻿CREATE TABLE [dbo].[CuentaFija] (
    [ID]                TINYINT  NOT NULL,
    [IDTipoComprobante] SMALLINT NOT NULL,
    [Orden]             TINYINT  NOT NULL,
    [Debe]              BIT      NOT NULL,
    [Haber]             BIT      NOT NULL,
    [IDCuentaContable]  SMALLINT NOT NULL,
    CONSTRAINT [PK_CuentaFija] PRIMARY KEY CLUSTERED ([ID] ASC)
);


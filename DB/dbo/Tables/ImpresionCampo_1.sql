﻿CREATE TABLE [dbo].[ImpresionCampo] (
    [IDOperacion]            INT          NOT NULL,
    [IDTipoComprobante]      SMALLINT     NOT NULL,
    [IDSeccion]              TINYINT      NOT NULL,
    [ID]                     TINYINT      NOT NULL,
    [Descripcion]            VARCHAR (50) NOT NULL,
    [Campo]                  VARCHAR (50) NOT NULL,
    [Columna]                TINYINT      NOT NULL,
    [TipoCampo]              VARCHAR (50) NOT NULL,
    [IDFormato]              TINYINT      NULL,
    [IDTipo]                 TINYINT      NULL,
    [Color]                  VARCHAR (50) CONSTRAINT [DF_ImpresionCampo_Color] DEFAULT ('0,0,0') NULL,
    [PuedeCrecer]            BIT          CONSTRAINT [DF_ImpresionCampo_PuedeCrecer] DEFAULT ('False') NULL,
    [MantenerJunto]          BIT          CONSTRAINT [DF_ImpresionCampo_MantenerJunto] DEFAULT ('False') NULL,
    [Mostrar]                BIT          CONSTRAINT [DF_ImpresionCampo_Mostrar] DEFAULT ('True') NULL,
    [CoordenadaXY]           VARCHAR (50) CONSTRAINT [DF_ImpresionCampo_CoordenadaXY] DEFAULT ('0,0') NULL,
    [TamañoAnchoAlto]        VARCHAR (50) CONSTRAINT [DF_ImpresionCampo_TamañoAnchoAlto] DEFAULT ('10,10') NULL,
    [MargenIzquierdaDerecha] VARCHAR (50) CONSTRAINT [DF_ImpresionCampo_MargenIzquierdaDerecha] DEFAULT ('0,0') NULL,
    CONSTRAINT [PK_ImpresionCampo] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [IDTipoComprobante] ASC, [IDSeccion] ASC, [ID] ASC),
    CONSTRAINT [FK_ImpresionCampo_ImpresionFormato] FOREIGN KEY ([IDFormato]) REFERENCES [dbo].[ImpresionFormato] ([ID]),
    CONSTRAINT [FK_ImpresionCampo_ImpresionSeccion] FOREIGN KEY ([IDOperacion], [IDTipoComprobante], [IDSeccion]) REFERENCES [dbo].[ImpresionSeccion] ([IDOperacion], [IDTipoComprobante], [ID]),
    CONSTRAINT [FK_ImpresionCampo_ImpresionTipo] FOREIGN KEY ([IDTipo]) REFERENCES [dbo].[ImpresionTipo] ([ID])
);


﻿CREATE TABLE [dbo].[OrdenPagoEgreso] (
    [IDTransaccionOrdenPago] NUMERIC (18) NOT NULL,
    [IDTransaccionEgreso]    NUMERIC (18) NOT NULL,
    [Cuota]                  TINYINT      CONSTRAINT [DF_OrdenPagoEgreso_Cuota] DEFAULT ((0)) NOT NULL,
    [ID]                     SMALLINT     NULL,
    [Importe]                MONEY        CONSTRAINT [DF_OrdenPagoEgreso_Importe] DEFAULT ((0)) NOT NULL,
    [Saldo]                  MONEY        CONSTRAINT [DF_OrdenPagoEgreso_Saldo] DEFAULT ((0)) NOT NULL,
    [Procesado]              BIT          CONSTRAINT [DF_OrdenPagoEgreso_Procesado] DEFAULT ('False') NULL,
    [Retenido]               BIT          NULL,
    [Retencion]              MONEY        NULL,
    [IVA]                    MONEY        NULL,
    [ProcesaRetencion]       BIT          NULL,
    CONSTRAINT [PK_OrdenPagoEgreso] PRIMARY KEY CLUSTERED ([IDTransaccionOrdenPago] ASC, [IDTransaccionEgreso] ASC, [Cuota] ASC),
    CONSTRAINT [FK_OrdenPagoEgreso_OrdenPago] FOREIGN KEY ([IDTransaccionOrdenPago]) REFERENCES [dbo].[OrdenPago] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_610_609]
    ON [dbo].[OrdenPagoEgreso]([IDTransaccionEgreso] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_797_796]
    ON [dbo].[OrdenPagoEgreso]([IDTransaccionEgreso] ASC)
    INCLUDE([IDTransaccionOrdenPago]);


GO
CREATE NONCLUSTERED INDEX [missing_index_246062_246061]
    ON [dbo].[OrdenPagoEgreso]([IDTransaccionEgreso] ASC)
    INCLUDE([IDTransaccionOrdenPago], [Importe]);


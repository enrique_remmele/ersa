﻿CREATE TABLE [dbo].[CFTicketBascula] (
    [IDOperacion]          TINYINT NOT NULL,
    [ID]                   TINYINT NOT NULL,
    [BuscarEnTipoProducto] BIT     NULL,
    [IDTipoProducto]       TINYINT NULL,
    [Flete]                BIT     NULL,
    CONSTRAINT [PK_CFTicketBascula] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


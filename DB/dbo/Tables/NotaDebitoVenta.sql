﻿CREATE TABLE [dbo].[NotaDebitoVenta] (
    [IDTransaccionNotaDebito]    NUMERIC (18) NOT NULL,
    [IDTransaccionVentaGenerada] NUMERIC (18) NOT NULL,
    [ID]                         TINYINT      NOT NULL,
    [Importe]                    MONEY        NOT NULL,
    [cobrado]                    MONEY        NOT NULL,
    [Acreditado]                 MONEY        NOT NULL,
    [Saldo]                      MONEY        NOT NULL,
    CONSTRAINT [PK_NotaDebitoVenta] PRIMARY KEY CLUSTERED ([IDTransaccionNotaDebito] ASC, [IDTransaccionVentaGenerada] ASC, [ID] ASC),
    CONSTRAINT [FK_NotaDebitoVenta_NotaDebito] FOREIGN KEY ([IDTransaccionNotaDebito]) REFERENCES [dbo].[NotaDebito] ([IDTransaccion]),
    CONSTRAINT [FK_NotaDebitoVenta_Venta] FOREIGN KEY ([IDTransaccionVentaGenerada]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


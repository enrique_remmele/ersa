﻿CREATE TABLE [dbo].[aDepartamentoEmpresa] (
    [ID]                     INT          NULL,
    [Departamento]           VARCHAR (50) NULL,
    [IDUsuarioJefe]          INT          NULL,
    [IDSucursal]             TINYINT      NULL,
    [FechaModificacion]      DATETIME     NOT NULL,
    [IDUsuarioModificacion]  INT          NOT NULL,
    [IDTerminalModificacion] INT          NOT NULL,
    [Operacion]              VARCHAR (10) NULL
);


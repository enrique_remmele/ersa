﻿CREATE TABLE [dbo].[SolicitudProductoPrecio] (
    [IDProducto]                INT      NOT NULL,
    [IDCliente]                 INT      NOT NULL,
    [IDMoneda]                  TINYINT  NOT NULL,
    [PrecioUnico]               BIT      NOT NULL,
    [Precio]                    MONEY    NOT NULL,
    [Desde]                     DATE     NOT NULL,
    [CantidadMinimaPrecioUnico] INT      NOT NULL,
    [FechaSolicitud]            DATETIME NOT NULL,
    [IDUsuarioSolicitud]        INT      NOT NULL,
    [FechaAprobado]             DATETIME NULL,
    [IDUsuarioAprobado]         INT      NULL,
    [Estado]                    BIT      NULL,
    [IDTerminalAprobado]        INT      NULL
);


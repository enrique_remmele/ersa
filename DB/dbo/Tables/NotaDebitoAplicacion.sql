﻿CREATE TABLE [dbo].[NotaDebitoAplicacion] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [Numero]            INT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [NroComprobante]    VARCHAR (100) NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [IDCliente]         INT           NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDMoneda]          TINYINT       NOT NULL,
    [Cotizacion]        MONEY         NOT NULL,
    [Anulado]           BIT           CONSTRAINT [DF_NotaDebitoAplicacion_Anulado] DEFAULT ('False') NULL,
    [FechaAnulado]      DATE          NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    [Observacion]       VARCHAR (200) NULL,
    [Total]             MONEY         NULL,
    CONSTRAINT [PK_NotaDebitoAplicacion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


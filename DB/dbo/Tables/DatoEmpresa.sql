﻿CREATE TABLE [dbo].[DatoEmpresa] (
    [ID]                  TINYINT       NOT NULL,
    [RazonSocial]         VARCHAR (100) NOT NULL,
    [RUC]                 VARCHAR (12)  NOT NULL,
    [DVRUC]               TINYINT       NULL,
    [Direccion]           VARCHAR (100) NULL,
    [Telefono]            VARCHAR (20)  NULL,
    [WEB]                 NVARCHAR (50) NULL,
    [email]               VARCHAR (30)  NULL,
    [fax]                 VARCHAR (20)  NULL,
    [Propietario]         VARCHAR (50)  NULL,
    [RUCPropietario]      VARCHAR (12)  NULL,
    [DVRUCPropietario]    TINYINT       NULL,
    [Dereccion]           VARCHAR (100) NULL,
    [TelefonoPropietario] VARCHAR (20)  NULL
);


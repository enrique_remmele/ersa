﻿CREATE TABLE [dbo].[DetalleNotaCredito] (
    [IDTransaccion]                 NUMERIC (18)    NOT NULL,
    [IDProducto]                    INT             NOT NULL,
    [ID]                            TINYINT         NOT NULL,
    [IDTransaccionVenta]            NUMERIC (18)    NULL,
    [IDDeposito]                    TINYINT         NOT NULL,
    [Observacion]                   VARCHAR (100)   NULL,
    [IDImpuesto]                    TINYINT         NOT NULL,
    [Cantidad]                      DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario]                MONEY           NOT NULL,
    [CostoUnitario]                 MONEY           NULL,
    [DescuentoUnitario]             MONEY           CONSTRAINT [DF_DetalleNotaCredito_DescuentoUnitario] DEFAULT ((0)) NULL,
    [DescuentoUnitarioDiscriminado] MONEY           NULL,
    [PorcentajeDescuento]           NUMERIC (2)     NULL,
    [Total]                         MONEY           CONSTRAINT [DF_DetalleNotaCredito_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]                 MONEY           CONSTRAINT [DF_DetalleNotaCredito_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalCostoImpuesto]            MONEY           NULL,
    [TotalCosto]                    MONEY           NULL,
    [TotalDescuento]                MONEY           CONSTRAINT [DF_DetalleNotaCredito_TotalDescuento] DEFAULT ((0)) NULL,
    [TotalDiscriminado]             MONEY           NOT NULL,
    [TotalCostoDiscriminado]        MONEY           NULL,
    [TotalDescuentoDiscriminado]    MONEY           NULL,
    [Caja]                          BIT             NULL,
    [CantidadCaja]                  DECIMAL (10, 2) NULL,
    [IDMotivoDevolucion]            INT             NULL,
    CONSTRAINT [PK_DetalleNotaCredito] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleNotaCredito_NotaCredito] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[NotaCredito] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleNotaCredito_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [IX_DetalleNotaCredito]
    ON [dbo].[DetalleNotaCredito]([IDTransaccionVenta] ASC);


GO
CREATE TRIGGER [dbo].[TGDetalleNotaCreditoDevolucion] ON [dbo].[DetalleNotaCredito] 
AFTER INSERT,DELETE
AS 
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

END

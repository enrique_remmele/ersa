﻿CREATE TABLE [dbo].[FormaPago] (
    [IDTransaccion]          NUMERIC (18) NOT NULL,
    [ID]                     SMALLINT     NOT NULL,
    [Efectivo]               BIT          NOT NULL,
    [ImporteEfectivo]        MONEY        NULL,
    [IDMonedaEfectivo]       TINYINT      NULL,
    [ImporteMonedaEfectivo]  MONEY        NULL,
    [Cheque]                 BIT          NOT NULL,
    [ImporteCheque]          MONEY        NULL,
    [IDTransaccionCheque]    NUMERIC (18) NULL,
    [IDMonedaCheque]         TINYINT      NULL,
    [CancelarCheque]         BIT          CONSTRAINT [DF_FormaPago_CancelarCheque] DEFAULT ('False') NULL,
    [Documento]              BIT          CONSTRAINT [DF_FormaPago_Documento] DEFAULT ('False') NULL,
    [ImporteDocumento]       MONEY        NULL,
    [IDMonedaDocumento]      TINYINT      NULL,
    [ImporteMonedaDocumento] MONEY        NULL,
    [Importe]                MONEY        NOT NULL,
    [Tarjeta]                BIT          CONSTRAINT [DF_FormaPago_Tarjeta] DEFAULT ((0)) NOT NULL,
    [IDTipoTarjeta]          TINYINT      NULL,
    [ImporteMonedaTarjeta]   MONEY        NULL,
    [Cotizacion]             MONEY        NULL,
    CONSTRAINT [PK_FormaPago] PRIMARY KEY CLUSTERED ([ID] ASC, [IDTransaccion] ASC),
    CONSTRAINT [FK_FormaPago_ChequeCliente] FOREIGN KEY ([IDTransaccionCheque]) REFERENCES [dbo].[ChequeCliente] ([IDTransaccion]),
    CONSTRAINT [FK_FormaPago_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_20_19]
    ON [dbo].[FormaPago]([IDTransaccion] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_447_446]
    ON [dbo].[FormaPago]([IDTransaccion] ASC)
    INCLUDE([ID]);


GO
CREATE NONCLUSTERED INDEX [missing_index_318_317]
    ON [dbo].[FormaPago]([IDTransaccionCheque] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1946_1945]
    ON [dbo].[FormaPago]([IDTransaccionCheque] ASC)
    INCLUDE([IDTransaccion]);


GO
CREATE NONCLUSTERED INDEX [missing_index_371_370]
    ON [dbo].[FormaPago]([IDTransaccion] ASC)
    INCLUDE([ImporteCheque], [IDTransaccionCheque], [Cotizacion]);


GO
CREATE NONCLUSTERED INDEX [missing_index_30071_30070]
    ON [dbo].[FormaPago]([IDTransaccionCheque] ASC)
    INCLUDE([IDTransaccion], [ImporteCheque], [Cotizacion]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4147_4146]
    ON [dbo].[FormaPago]([IDTransaccionCheque] ASC)
    INCLUDE([IDTransaccion], [ImporteCheque]);


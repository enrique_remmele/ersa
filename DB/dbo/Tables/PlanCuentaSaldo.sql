﻿CREATE TABLE [dbo].[PlanCuentaSaldo] (
    [Año]              SMALLINT     NOT NULL,
    [Mes]              TINYINT      NOT NULL,
    [Cuenta]           VARCHAR (50) NOT NULL,
    [IDSucursal]       TINYINT      NOT NULL,
    [IDCentroCosto]    TINYINT      NOT NULL,
    [Debito]           MONEY        CONSTRAINT [DF_PlanCuentaSaldo_Debito] DEFAULT ((0)) NOT NULL,
    [Credito]          MONEY        CONSTRAINT [DF_PlanCuentaSaldo_Credito] DEFAULT ((0)) NOT NULL,
    [Saldo]            MONEY        CONSTRAINT [DF_PlanCuentaSaldo_Saldo] DEFAULT ((0)) NOT NULL,
    [DebitoAcumulado]  MONEY        CONSTRAINT [DF_PlanCuentaSaldo_DebitoAcumulado] DEFAULT ((0)) NOT NULL,
    [CreditoAcumulado] MONEY        CONSTRAINT [DF_PlanCuentaSaldo_CreditoAcumulado] DEFAULT ((0)) NOT NULL,
    [SaldoAcumulado]   MONEY        CONSTRAINT [DF_PlanCuentaSaldo_SaldoAcumulado] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PlanCuentaSaldo_1] PRIMARY KEY CLUSTERED ([Año] ASC, [Mes] ASC, [Cuenta] ASC, [IDSucursal] ASC, [IDCentroCosto] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_PlanCuentaSaldo_IDCentroCosto]
    ON [dbo].[PlanCuentaSaldo]([IDCentroCosto] ASC);


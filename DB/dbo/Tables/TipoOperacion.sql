﻿CREATE TABLE [dbo].[TipoOperacion] (
    [ID]          TINYINT      NOT NULL,
    [IDOperacion] INT          NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Activo]      BIT          NOT NULL,
    [Entrada]     BIT          NOT NULL,
    [Salida]      BIT          NOT NULL,
    CONSTRAINT [PK_TipoMovimiento] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_TipoMovimiento_Operacion] FOREIGN KEY ([IDOperacion]) REFERENCES [dbo].[Operacion] ([ID])
);


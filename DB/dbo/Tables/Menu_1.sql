﻿CREATE TABLE [dbo].[Menu] (
    [Codigo]        NUMERIC (18)  NOT NULL,
    [Descripcion]   VARCHAR (100) NOT NULL,
    [Nivel]         TINYINT       NOT NULL,
    [NombreControl] VARCHAR (100) NOT NULL,
    [CodigoPadre]   NUMERIC (18)  NOT NULL,
    [Tag]           VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED ([Codigo] ASC)
);


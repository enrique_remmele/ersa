﻿CREATE TABLE [dbo].[CargaMercaderia] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDTransaccionLote] NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Numero]            INT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [Fecha]             DATETIME      NOT NULL,
    [Anulado]           BIT           CONSTRAINT [DF_CargaMercaderia_Anulado] DEFAULT ('False') NOT NULL,
    [Observacion]       VARCHAR (100) NULL,
    [FechaAnulado]      BIT           NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    CONSTRAINT [PK_CargaMercaderia] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_CargaMercaderia_LoteDistribucion] FOREIGN KEY ([IDTransaccionLote]) REFERENCES [dbo].[LoteDistribucion] ([IDTransaccion]),
    CONSTRAINT [FK_CargaMercaderia_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


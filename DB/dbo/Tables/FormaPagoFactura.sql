﻿CREATE TABLE [dbo].[FormaPagoFactura] (
    [ID]                   TINYINT      NOT NULL,
    [Descripcion]          VARCHAR (50) NOT NULL,
    [Referencia]           VARCHAR (16) NOT NULL,
    [Contado]              BIT          NOT NULL,
    [Credito]              BIT          NOT NULL,
    [CancelarAutomatico]   BIT          NOT NULL,
    [IDCuentaContable]     INT          NOT NULL,
    [Estado]               BIT          NULL,
    [Aprobacion]           BIT          CONSTRAINT [DF_FormaPagoFactura_Aprobacion] DEFAULT ('True') NULL,
    [FacturacionSinPedido] BIT          NULL,
    [Condicion]            VARCHAR (20) NULL,
    CONSTRAINT [PK_FormaPagoFactura] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_FormaPagoFactura_CuentaContable] FOREIGN KEY ([IDCuentaContable]) REFERENCES [dbo].[CuentaContable] ([ID])
);


﻿CREATE TABLE [dbo].[SubLinea] (
    [ID]          SMALLINT     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_SubLinea_Estado] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_SubLinea] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[DetalleCompra] (
    [IDTransaccion]     NUMERIC (18)    NOT NULL,
    [IDProducto]        INT             NOT NULL,
    [ID]                TINYINT         NOT NULL,
    [IDDeposito]        TINYINT         NOT NULL,
    [Observacion]       VARCHAR (50)    NOT NULL,
    [IDImpuesto]        TINYINT         NOT NULL,
    [Cantidad]          DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario]    MONEY           NOT NULL,
    [Total]             MONEY           NOT NULL,
    [TotalImpuesto]     MONEY           NOT NULL,
    [TotalDescuento]    MONEY           CONSTRAINT [DF_DetalleCompra_TotalDescuento] DEFAULT ((0)) NULL,
    [TotalDiscriminado] MONEY           NOT NULL,
    [Caja]              BIT             NULL,
    [CantidadCaja]      DECIMAL (10, 2) NULL,
    [RetencionIVA]      MONEY           NULL,
    [RetencionRenta]    MONEY           NULL,
    CONSTRAINT [PK_DetalleCompra] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleCompra_Compra] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Compra] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleCompra_Deposito] FOREIGN KEY ([IDDeposito]) REFERENCES [dbo].[Deposito] ([ID]),
    CONSTRAINT [FK_DetalleCompra_Impuesto] FOREIGN KEY ([IDImpuesto]) REFERENCES [dbo].[Impuesto] ([ID]),
    CONSTRAINT [FK_DetalleCompra_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_61_60]
    ON [dbo].[DetalleCompra]([IDProducto] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_416592_416591]
    ON [dbo].[DetalleCompra]([IDProducto] ASC)
    INCLUDE([IDTransaccion], [ID], [IDDeposito], [Observacion], [IDImpuesto], [Cantidad], [PrecioUnitario], [Total], [TotalImpuesto], [TotalDescuento], [TotalDiscriminado], [Caja], [CantidadCaja]);


﻿CREATE TABLE [dbo].[RendicionLoteChofer] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [IDChofer]      TINYINT      NOT NULL,
    [Numero]        INT          NOT NULL,
    CONSTRAINT [PK_RendicionLoteChofer] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDChofer] ASC, [Numero] ASC),
    CONSTRAINT [FK_RendicionLoteChofer_RendicionLote] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[RendicionLote] ([IDTransaccion]) ON DELETE CASCADE
);


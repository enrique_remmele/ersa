﻿CREATE TABLE [dbo].[RendicionLote] (
    [IDTransaccion]               NUMERIC (18)  NOT NULL,
    [IDTransaccionLote]           NUMERIC (18)  NOT NULL,
    [IDSucursal]                  TINYINT       NOT NULL,
    [Numero]                      INT           NOT NULL,
    [IDTipoComprobante]           SMALLINT      NOT NULL,
    [Comprobante]                 VARCHAR (50)  NOT NULL,
    [Fecha]                       DATETIME      NOT NULL,
    [Anulado]                     BIT           CONSTRAINT [DF_RendicionLote_Anulado] DEFAULT ('False') NOT NULL,
    [FechaAnulado]                DATETIME      NULL,
    [IDUsuarioAnulado]            SMALLINT      NULL,
    [TotalEfectivo]               MONEY         NULL,
    [TotalDocumento]              MONEY         NULL,
    [TotalAnulados]               MONEY         NULL,
    [TotalCheque]                 MONEY         NULL,
    [TotalChequeAlDiaBancoLocal]  MONEY         NULL,
    [TotalChequeAlDiaOtrosBancos] MONEY         NULL,
    [TotalChequeDiferido]         MONEY         NULL,
    [Total]                       MONEY         CONSTRAINT [DF_RendicionLote_Total] DEFAULT ((0)) NOT NULL,
    [Observacion]                 VARCHAR (100) NULL,
    [TotalCobranzaExtra]          MONEY         NULL,
    CONSTRAINT [PK_RendicionLote] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_RendicionLote_LoteDistribucion] FOREIGN KEY ([IDTransaccionLote]) REFERENCES [dbo].[LoteDistribucion] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_4231_4230]
    ON [dbo].[RendicionLote]([IDSucursal] ASC)
    INCLUDE([Numero]);


GO
CREATE NONCLUSTERED INDEX [missing_index_271_270]
    ON [dbo].[RendicionLote]([IDTransaccionLote] ASC, [Anulado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_4239_4238]
    ON [dbo].[RendicionLote]([IDSucursal] ASC, [Numero] ASC, [Anulado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_4241_4240]
    ON [dbo].[RendicionLote]([IDSucursal] ASC, [IDTipoComprobante] ASC, [Comprobante] ASC, [Anulado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_188864_188863]
    ON [dbo].[RendicionLote]([IDSucursal] ASC, [Numero] ASC);


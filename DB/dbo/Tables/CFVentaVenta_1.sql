﻿CREATE TABLE [dbo].[CFVentaVenta] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    CONSTRAINT [PK_CFVentaVenta] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


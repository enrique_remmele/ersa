﻿CREATE TABLE [dbo].[RetencionIVAImportada] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [Comprobante]   VARCHAR (20) NOT NULL,
    [Timbrado]      VARCHAR (12) NOT NULL,
    CONSTRAINT [PK_RetencionIVAImportada] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


﻿CREATE TABLE [dbo].[DebitoCreditoBancario] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Debito]            BIT           NOT NULL,
    [Credito]           BIT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Numero]            INT           NOT NULL,
    [NroComprobante]    BIGINT        NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDCuentaBancaria]  TINYINT       NOT NULL,
    [Cotizacion]        MONEY         NULL,
    [Observacion]       VARCHAR (100) NULL,
    [Facturado]         BIT           CONSTRAINT [DF_DebitoCreditoBancario_Facturado] DEFAULT ('False') NOT NULL,
    [Total]             MONEY         CONSTRAINT [DF_DebitoCreditosBancarios_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]     MONEY         CONSTRAINT [DF_Table_1_TotalEfectivo] DEFAULT ((0)) NOT NULL,
    [TotalDiscriminado] MONEY         CONSTRAINT [DF_Table_1_TotalChequeLocal] DEFAULT ((0)) NOT NULL,
    [Conciliado]        BIT           NULL,
    [EsCobranza]        BIT           NULL,
    [EsProcesado]       BIT           NULL,
    CONSTRAINT [PK_DebitoCreditoBancario] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_DebitoCreditoBancario_CuentaBancaria] FOREIGN KEY ([IDCuentaBancaria]) REFERENCES [dbo].[CuentaBancaria] ([ID]),
    CONSTRAINT [FK_DebitoCreditoBancario_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_DebitoCreditoBancario_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_591_590]
    ON [dbo].[DebitoCreditoBancario]([IDSucursal] ASC, [Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_778_777]
    ON [dbo].[DebitoCreditoBancario]([IDTipoComprobante] ASC, [Numero] ASC, [NroComprobante] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_594_593]
    ON [dbo].[DebitoCreditoBancario]([IDSucursal] ASC, [Numero] ASC)
    INCLUDE([IDTransaccion], [IDTipoComprobante], [IDCuentaBancaria]);


GO
CREATE NONCLUSTERED INDEX [missing_index_417826_417825]
    ON [dbo].[DebitoCreditoBancario]([Fecha] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Credito], [IDTipoComprobante], [Numero], [NroComprobante], [IDCuentaBancaria], [Observacion], [Total]);


GO
CREATE NONCLUSTERED INDEX [missing_index_417843_417842]
    ON [dbo].[DebitoCreditoBancario]([IDCuentaBancaria] ASC, [Fecha] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Credito], [IDTipoComprobante], [Numero], [NroComprobante], [Observacion], [Total]);


GO
CREATE NONCLUSTERED INDEX [missing_index_421805_421804]
    ON [dbo].[DebitoCreditoBancario]([IDCuentaBancaria] ASC, [Fecha] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Debito], [Credito], [IDTipoComprobante], [Numero], [NroComprobante], [Cotizacion], [Observacion], [Facturado], [Total], [TotalImpuesto], [TotalDiscriminado], [Conciliado]);


GO
CREATE NONCLUSTERED INDEX [missing_index_421807_421806]
    ON [dbo].[DebitoCreditoBancario]([Fecha] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Debito], [Credito], [IDTipoComprobante], [Numero], [NroComprobante], [IDCuentaBancaria], [Cotizacion], [Observacion], [Facturado], [Total], [TotalImpuesto], [TotalDiscriminado], [Conciliado]);


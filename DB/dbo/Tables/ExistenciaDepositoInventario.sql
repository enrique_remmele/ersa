﻿CREATE TABLE [dbo].[ExistenciaDepositoInventario] (
    [IDTransaccion]       NUMERIC (18)    NOT NULL,
    [IDDeposito]          TINYINT         NOT NULL,
    [IDProducto]          INT             NOT NULL,
    [Existencia]          DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_Existencia] DEFAULT ((0)) NOT NULL,
    [CantidadDeposito]    DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_CantidadDeposito] DEFAULT ((0)) NOT NULL,
    [Provisorio]          DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_Provisorio] DEFAULT ((0)) NOT NULL,
    [Equipo1]             DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_Equipo1] DEFAULT ((0)) NULL,
    [Averiados1]          DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_Averiados1] DEFAULT ((0)) NULL,
    [Equipo2]             DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_Equipo2] DEFAULT ((0)) NULL,
    [Averiados2]          DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_Averiados2] DEFAULT ((0)) NULL,
    [ConteoDiferencia]    DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_ConteoDiferencia] DEFAULT ((0)) NULL,
    [AveriadosDiferencia] DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_AveriadosDiferencia] DEFAULT ((0)) NULL,
    [TotalDiferencia]     DECIMAL (10, 2) CONSTRAINT [DF_ExistenciaDepositoInventario_TotalDiferencia] DEFAULT ((0)) NULL,
    [DiferenciaSistema]   DECIMAL (10, 2) NULL,
    CONSTRAINT [PK_ExistenciaDepositoInventario] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDDeposito] ASC, [IDProducto] ASC),
    CONSTRAINT [FK_ExistenciaDepositoInventario_ControlInventario] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[ControlInventario] ([IDTransaccion]),
    CONSTRAINT [FK_ExistenciaDepositoInventario_Deposito] FOREIGN KEY ([IDDeposito]) REFERENCES [dbo].[Deposito] ([ID])
);


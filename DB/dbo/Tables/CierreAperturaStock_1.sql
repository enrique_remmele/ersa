﻿CREATE TABLE [dbo].[CierreAperturaStock] (
    [IDTransaccion]    NUMERIC (18) NOT NULL,
    [Numero]           NUMERIC (18) NULL,
    [Fecha]            DATETIME     NULL,
    [Observacion]      VARCHAR (50) NULL,
    [Procesado]        BIT          NULL,
    [Anulado]          BIT          NULL,
    [FechaAnulado]     DATE         NULL,
    [IDUsuarioAnulado] SMALLINT     NULL,
    CONSTRAINT [PK_CierreAperturaStock] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


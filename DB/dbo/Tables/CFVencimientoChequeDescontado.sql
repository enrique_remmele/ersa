﻿CREATE TABLE [dbo].[CFVencimientoChequeDescontado] (
    [IDOperacion]          TINYINT NOT NULL,
    [ID]                   TINYINT NOT NULL,
    [ChequeDiferido]       BIT     NULL,
    [ChequeDescontado]     BIT     NULL,
    [IDSucursalDiferido]   TINYINT NULL,
    [IDSucursalDescontado] TINYINT NULL,
    CONSTRAINT [PK_CFVencimientoChequeDescontado] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


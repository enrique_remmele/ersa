﻿CREATE TABLE [dbo].[CFConsumoCombustible] (
    [IDOperacion]      TINYINT NOT NULL,
    [ID]               TINYINT NOT NULL,
    [IDTipoOperacion]  TINYINT NULL,
    [IDTipoCuentaFija] TINYINT NULL,
    [BuscarEnProducto] BIT     NULL,
    [BuscarEnDeposito] BIT     NULL,
    CONSTRAINT [PK_CFConsumoCombustible] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[PedidoVendedor] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [IDVendedor]    TINYINT      NOT NULL,
    [Numero]        INT          NOT NULL,
    CONSTRAINT [PK_PedidoVendedor] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDVendedor] ASC, [Numero] ASC),
    CONSTRAINT [FK_PedidoVendedor_Pedido] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Pedido] ([IDTransaccion]) ON DELETE CASCADE,
    CONSTRAINT [FK_PedidoVendedor_Vendedor] FOREIGN KEY ([IDVendedor]) REFERENCES [dbo].[Vendedor] ([ID])
);


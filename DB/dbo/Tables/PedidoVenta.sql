﻿CREATE TABLE [dbo].[PedidoVenta] (
    [IDTransaccionPedido] NUMERIC (18) NOT NULL,
    [IDTransaccionVenta]  NUMERIC (18) NOT NULL,
    CONSTRAINT [PK_PedidoVenta] PRIMARY KEY CLUSTERED ([IDTransaccionPedido] ASC, [IDTransaccionVenta] ASC),
    CONSTRAINT [FK_PedidoVenta_Pedido] FOREIGN KEY ([IDTransaccionPedido]) REFERENCES [dbo].[Pedido] ([IDTransaccion]),
    CONSTRAINT [FK_PedidoVenta_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


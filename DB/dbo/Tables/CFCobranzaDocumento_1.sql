﻿CREATE TABLE [dbo].[CFCobranzaDocumento] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    CONSTRAINT [PK_CFCobranzaDocumento] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


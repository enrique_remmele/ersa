﻿CREATE TABLE [dbo].[PorcentajeProductoPrecio] (
    [IDUsuario]      INT             NOT NULL,
    [IDPresentacion] TINYINT         NOT NULL,
    [Porcentaje]     DECIMAL (18, 2) NOT NULL,
    [Importe]        MONEY           NULL,
    [Estado]         BIT             NULL
);


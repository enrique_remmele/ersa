﻿CREATE TABLE [dbo].[PedidoNotaCredito] (
    [IDTransaccion]          NUMERIC (18)  NOT NULL,
    [Numero]                 INT           NOT NULL,
    [NumeroSolicitudCliente] VARCHAR (50)  NULL,
    [IDCliente]              INT           NOT NULL,
    [IDSucursal]             TINYINT       NOT NULL,
    [IDDeposito]             TINYINT       NOT NULL,
    [Fecha]                  DATE          NOT NULL,
    [IDMoneda]               TINYINT       NOT NULL,
    [Cotizacion]             MONEY         NOT NULL,
    [Observacion]            VARCHAR (500) NULL,
    [Total]                  MONEY         NOT NULL,
    [TotalImpuesto]          MONEY         NOT NULL,
    [TotalDiscriminado]      MONEY         NOT NULL,
    [TotalDescuento]         MONEY         NOT NULL,
    [Saldo]                  MONEY         NOT NULL,
    [Anulado]                BIT           NULL,
    [FechaAnulado]           DATE          NULL,
    [IDUsuarioAnulado]       SMALLINT      NULL,
    [Procesado]              BIT           NULL,
    [Devolucion]             BIT           NULL,
    [Descuento]              BIT           NULL,
    [Aplicar]                BIT           NULL,
    [ConComprobantes]        BIT           CONSTRAINT [DF_PedidoNotaCredito_ConComprobantes] DEFAULT ('True') NULL,
    [IDSubMotivoNotaCredito] INT           NULL,
    [EstadoNC]               BIT           NULL,
    [IDUsuarioAprobado]      INT           NULL,
    [FechaAprobado]          DATE          NULL,
    [ObservacionAutorizador] VARCHAR (500) NULL,
    [ProcesadoNC]            BIT           NULL,
    [DesdePedidoVenta]       BIT           NULL,
    [IDSucursalCliente]      INT           NULL,
    [EsVentaSucursal]        BIT           NULL,
    [Direccion]              VARCHAR (100) NULL,
    [Telefono]               VARCHAR (50)  NULL,
    [AnuladoAprobado]        BIT           NULL,
    CONSTRAINT [PK_PedidoNotaCredito] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


GO
CREATE NONCLUSTERED INDEX [missing_index_1119_1118]
    ON [dbo].[PedidoNotaCredito]([Numero] ASC, [IDSucursal] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1627_1626]
    ON [dbo].[PedidoNotaCredito]([Numero] ASC, [IDSucursal] ASC, [Procesado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1567_1566]
    ON [dbo].[PedidoNotaCredito]([Anulado] ASC, [Fecha] ASC)
    INCLUDE([IDTransaccion], [Numero], [IDCliente], [IDSucursal], [IDSubMotivoNotaCredito], [EstadoNC], [IDUsuarioAprobado], [FechaAprobado], [ProcesadoNC]);


GO
CREATE NONCLUSTERED INDEX [missing_index_1646_1645]
    ON [dbo].[PedidoNotaCredito]([IDSucursal] ASC, [Fecha] ASC)
    INCLUDE([IDTransaccion], [Numero], [IDCliente], [Anulado], [IDSubMotivoNotaCredito], [EstadoNC], [IDUsuarioAprobado], [ProcesadoNC], [AnuladoAprobado]);


GO
CREATE NONCLUSTERED INDEX [missing_index_1393_1392]
    ON [dbo].[PedidoNotaCredito]([IDSucursal] ASC)
    INCLUDE([Numero]);


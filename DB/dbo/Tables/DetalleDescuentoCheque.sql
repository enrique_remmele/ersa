﻿CREATE TABLE [dbo].[DetalleDescuentoCheque] (
    [IDTransaccionDescuentoCheque] NUMERIC (18) NOT NULL,
    [IDTransaccionChequeCliente]   NUMERIC (18) NOT NULL,
    CONSTRAINT [PK_DetalleDescuentoCheque] PRIMARY KEY CLUSTERED ([IDTransaccionDescuentoCheque] ASC, [IDTransaccionChequeCliente] ASC),
    CONSTRAINT [FK_DetalleDescuentoCheque_ChequeCliente] FOREIGN KEY ([IDTransaccionChequeCliente]) REFERENCES [dbo].[ChequeCliente] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleDescuentoCheque_DescuentoCheque] FOREIGN KEY ([IDTransaccionDescuentoCheque]) REFERENCES [dbo].[DescuentoCheque] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_5733_5732]
    ON [dbo].[DetalleDescuentoCheque]([IDTransaccionChequeCliente] ASC);


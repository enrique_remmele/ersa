﻿CREATE TABLE [dbo].[RendicionFondoFijo] (
    [IDTransaccion]          NUMERIC (18)  NOT NULL,
    [IDSucursal]             TINYINT       NOT NULL,
    [Numero]                 INT           NOT NULL,
    [IDGrupo]                TINYINT       NOT NULL,
    [Fecha]                  DATE          NOT NULL,
    [Observacion]            VARCHAR (100) NULL,
    [IDTransaccionOrdenPago] DECIMAL (18)  NULL,
    [Anulado]                BIT           NULL,
    CONSTRAINT [PK_RendicionFondoFijo] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_RendicionFondoFijo_Grupo] FOREIGN KEY ([IDGrupo]) REFERENCES [dbo].[Grupo] ([ID]),
    CONSTRAINT [FK_RendicionFondoFijo_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


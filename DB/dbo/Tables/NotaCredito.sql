﻿CREATE TABLE [dbo].[NotaCredito] (
    [IDTransaccion]          NUMERIC (18)  NOT NULL,
    [IDPuntoExpedicion]      INT           NOT NULL,
    [IDTipoComprobante]      SMALLINT      NOT NULL,
    [NroComprobante]         INT           NOT NULL,
    [IDCliente]              INT           NOT NULL,
    [IDSucursal]             TINYINT       NOT NULL,
    [IDDeposito]             TINYINT       NOT NULL,
    [Fecha]                  DATE          NOT NULL,
    [IDMoneda]               TINYINT       NOT NULL,
    [Cotizacion]             MONEY         NULL,
    [Observacion]            VARCHAR (500) NULL,
    [Total]                  MONEY         NOT NULL,
    [TotalImpuesto]          MONEY         NOT NULL,
    [TotalDiscriminado]      MONEY         NOT NULL,
    [TotalDescuento]         MONEY         NOT NULL,
    [Saldo]                  MONEY         NOT NULL,
    [Anulado]                BIT           NOT NULL,
    [FechaAnulado]           DATE          NULL,
    [IDUsuarioAnulado]       SMALLINT      NULL,
    [Procesado]              BIT           NULL,
    [Devolucion]             BIT           NULL,
    [Descuento]              BIT           NULL,
    [Aplicar]                BIT           NULL,
    [ConComprobantes]        BIT           CONSTRAINT [DF_NotaCredito_ConComprobantes] DEFAULT ('True') NULL,
    [IDSucursalCliente]      TINYINT       NULL,
    [IDMotivo]               INT           NULL,
    [IDSubMotivoNotaCredito] INT           NULL,
    [Direccion]              VARCHAR (100) NULL,
    [Telefono]               VARCHAR (50)  NULL,
    CONSTRAINT [PK_NotaCredito] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_NotaCredito_PuntoExpedicion] FOREIGN KEY ([IDPuntoExpedicion]) REFERENCES [dbo].[PuntoExpedicion] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_91_90]
    ON [dbo].[NotaCredito]([IDCliente] ASC, [Anulado] ASC, [Procesado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1462_1461]
    ON [dbo].[NotaCredito]([IDPuntoExpedicion] ASC, [Procesado] ASC)
    INCLUDE([IDTransaccion], [NroComprobante]);


GO
CREATE NONCLUSTERED INDEX [missing_index_98_97]
    ON [dbo].[NotaCredito]([IDCliente] ASC, [Anulado] ASC, [Procesado] ASC, [Aplicar] ASC, [Saldo] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1610_1609]
    ON [dbo].[NotaCredito]([IDPuntoExpedicion] ASC, [IDTipoComprobante] ASC, [NroComprobante] ASC, [IDSucursal] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_186308_186307]
    ON [dbo].[NotaCredito]([Procesado] ASC, [Aplicar] ASC, [Fecha] ASC)
    INCLUDE([IDTransaccion], [Anulado]);


GO
CREATE NONCLUSTERED INDEX [missing_index_186310_186309]
    ON [dbo].[NotaCredito]([Procesado] ASC, [Fecha] ASC)
    INCLUDE([IDTransaccion]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4079_4078]
    ON [dbo].[NotaCredito]([Fecha] ASC, [Anulado] ASC, [Procesado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_3886_3885]
    ON [dbo].[NotaCredito]([Fecha] ASC, [Procesado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_4077_4076]
    ON [dbo].[NotaCredito]([Fecha] ASC, [Anulado] ASC, [Procesado] ASC)
    INCLUDE([IDTransaccion], [IDPuntoExpedicion], [IDTipoComprobante], [NroComprobante], [IDCliente], [IDSucursal], [IDDeposito], [IDMoneda], [Cotizacion], [Observacion], [Total], [TotalImpuesto], [TotalDiscriminado], [Saldo], [Devolucion], [Descuento], [Aplicar], [ConComprobantes], [IDSucursalCliente], [IDMotivo], [IDSubMotivoNotaCredito], [Direccion], [Telefono]);


GO
CREATE NONCLUSTERED INDEX [missing_index_6825_6824]
    ON [dbo].[NotaCredito]([IDTipoComprobante] ASC, [IDCliente] ASC, [Anulado] ASC, [Procesado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_417539_417538]
    ON [dbo].[NotaCredito]([Fecha] ASC, [Procesado] ASC)
    INCLUDE([IDTransaccion], [IDPuntoExpedicion], [IDTipoComprobante], [NroComprobante], [IDCliente], [IDSucursal], [IDDeposito], [IDMoneda], [Cotizacion], [Observacion], [Total], [TotalImpuesto], [TotalDiscriminado], [Saldo], [Anulado], [Devolucion], [Descuento], [Aplicar], [ConComprobantes], [IDSucursalCliente], [IDMotivo], [IDSubMotivoNotaCredito], [Direccion], [Telefono]);


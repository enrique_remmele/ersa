﻿CREATE TABLE [dbo].[Venta] (
    [IDTransaccion]          NUMERIC (18)  NOT NULL,
    [IDPuntoExpedicion]      INT           NOT NULL,
    [IDTipoComprobante]      SMALLINT      NOT NULL,
    [NroComprobante]         NUMERIC (18)  NOT NULL,
    [Comprobante]            VARCHAR (50)  NOT NULL,
    [IDCliente]              INT           NOT NULL,
    [IDSucursalCliente]      TINYINT       NULL,
    [Direccion]              VARCHAR (100) NULL,
    [Telefono]               VARCHAR (50)  NULL,
    [FechaEmision]           DATETIME      NOT NULL,
    [IDSucursal]             TINYINT       NOT NULL,
    [IDDeposito]             TINYINT       NOT NULL,
    [Credito]                BIT           NOT NULL,
    [FechaVencimiento]       DATE          NULL,
    [IDListaPrecio]          TINYINT       NULL,
    [Descuento]              NUMERIC (2)   NULL,
    [IDVendedor]             SMALLINT      NULL,
    [IDPromotor]             TINYINT       NULL,
    [IDMoneda]               TINYINT       NULL,
    [Cotizacion]             MONEY         CONSTRAINT [DF_Venta_Cotizacion] DEFAULT ((1)) NULL,
    [Observacion]            VARCHAR (500) NULL,
    [NroComprobanteRemision] VARCHAR (50)  NULL,
    [Total]                  MONEY         CONSTRAINT [DF_Venta_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]          MONEY         CONSTRAINT [DF_Venta_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalDiscriminado]      MONEY         CONSTRAINT [DF_Venta_TotalDiscriminado] DEFAULT ((0)) NOT NULL,
    [TotalDescuento]         MONEY         CONSTRAINT [DF_Venta_TotalDescuento] DEFAULT ((0)) NULL,
    [Cobrado]                MONEY         CONSTRAINT [DF_Venta_Cobrado] DEFAULT ((0)) NULL,
    [Descontado]             MONEY         CONSTRAINT [DF_Venta_Descontado] DEFAULT ((0)) NULL,
    [Acreditado]             MONEY         CONSTRAINT [DF_Venta_Acreditado] DEFAULT ((0)) NULL,
    [Saldo]                  MONEY         CONSTRAINT [DF_Venta_Saldo] DEFAULT ((0)) NOT NULL,
    [Cancelado]              BIT           NOT NULL,
    [Despachado]             BIT           CONSTRAINT [DF_Venta_Despachado] DEFAULT ('False') NULL,
    [EsVentaSucursal]        BIT           CONSTRAINT [DF_Venta_EsVentaSucursal] DEFAULT ('False') NULL,
    [Anulado]                BIT           CONSTRAINT [DF_Venta_Anulado] DEFAULT ('False') NOT NULL,
    [FechaAnulado]           BIT           NULL,
    [IDUsuarioAnulado]       SMALLINT      NULL,
    [Procesado]              BIT           CONSTRAINT [DF_Venta_Procesado] DEFAULT ('False') NULL,
    [Autoriza]               VARCHAR (50)  NULL,
    [IDMotivo]               INT           NULL,
    [FecAnulado]             DATE          NULL,
    [IDVendedorTemp]         INT           NULL,
    [IdFormaPagoFactura]     TINYINT       NULL,
    [RazonSocial]            VARCHAR (100) NULL,
    [RUC]                    VARCHAR (15)  NULL,
    CONSTRAINT [PK_Venta] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Venta_Anulado]
    ON [dbo].[Venta]([Anulado] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Venta_FechaEmision]
    ON [dbo].[Venta]([FechaEmision] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Venta_IDCliente]
    ON [dbo].[Venta]([IDCliente] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Venta_IDSucursal]
    ON [dbo].[Venta]([IDSucursal] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Venta_NroComprobante]
    ON [dbo].[Venta]([NroComprobante] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Venta_Procesado]
    ON [dbo].[Venta]([Procesado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_16_15]
    ON [dbo].[Venta]([Anulado] ASC, [Procesado] ASC)
    INCLUDE([IDTransaccion]);


﻿CREATE TABLE [dbo].[FondoFijoCuentaContable] (
    [IDFondoFijo]      INT NOT NULL,
    [IDCuentaContable] INT NOT NULL,
    [Estado]           BIT CONSTRAINT [DF_FondoFijoCuentaContable_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_FondoFijoCuentaContable] PRIMARY KEY CLUSTERED ([IDFondoFijo] ASC, [IDCuentaContable] ASC)
);


﻿CREATE TABLE [dbo].[FormularioImpresionTerminal] (
    [IDTerminal]            INT          NOT NULL,
    [IDFormularioImpresion] SMALLINT     NOT NULL,
    [Impresora]             VARCHAR (50) CONSTRAINT [DF_FormularioImpresionTerminal_Impresora] DEFAULT ('') NULL,
    CONSTRAINT [PK_FormularioImpresionTerminal] PRIMARY KEY CLUSTERED ([IDTerminal] ASC, [IDFormularioImpresion] ASC)
);


﻿CREATE TABLE [dbo].[Marca] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_Marca_Estado] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_Marca] PRIMARY KEY CLUSTERED ([ID] ASC)
);


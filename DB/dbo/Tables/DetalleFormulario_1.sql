﻿CREATE TABLE [dbo].[DetalleFormulario] (
    [IDFormulario]        TINYINT       NOT NULL,
    [IDDetalleFormulario] TINYINT       NOT NULL,
    [NombreCampo]         VARCHAR (50)  NOT NULL,
    [Campo]               VARCHAR (50)  NOT NULL,
    [PosicionX]           SMALLINT      CONSTRAINT [DF_DetalleFormulario_PosicionX] DEFAULT ((0)) NOT NULL,
    [PosicionY]           SMALLINT      CONSTRAINT [DF_DetalleFormulario_PosicionY] DEFAULT ((0)) NOT NULL,
    [TipoLetra]           VARCHAR (15)  CONSTRAINT [DF_DetalleFormulario_TipoLetra] DEFAULT ('Arial') NOT NULL,
    [TamañoLetra]         TINYINT       CONSTRAINT [DF_DetalleFormulario_TamañoLetra] DEFAULT ((7)) NOT NULL,
    [Formato]             VARCHAR (10)  CONSTRAINT [DF_DetalleFormulario_Formato] DEFAULT ('Normal') NOT NULL,
    [Valor]               VARCHAR (100) CONSTRAINT [DF_DetalleFormulario_Valor] DEFAULT ('--valor--') NULL,
    [Detalle]             BIT           CONSTRAINT [DF_DetalleFormulario_Detalle] DEFAULT ('False') NULL,
    [Numerico]            BIT           NULL,
    [Alineacion]          VARCHAR (15)  CONSTRAINT [DF_DetalleFormulario_Alineacion] DEFAULT ('Izquierda') NULL,
    [Largo]               TINYINT       CONSTRAINT [DF_DetalleFormulario_Largo] DEFAULT ((0)) NULL,
    [PuedeCrecer]         BIT           NULL,
    [Lineas]              TINYINT       NULL,
    CONSTRAINT [PK_DetalleFormulario] PRIMARY KEY CLUSTERED ([IDFormulario] ASC, [IDDetalleFormulario] ASC)
);


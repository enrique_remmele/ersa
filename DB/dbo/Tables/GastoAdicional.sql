﻿CREATE TABLE [dbo].[GastoAdicional] (
    [ID]          TINYINT       NOT NULL,
    [Descripcion] VARCHAR (100) NOT NULL,
    [Estado]      BIT           CONSTRAINT [DF_GastoAdicional_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_GastoAdicional] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[DetalleDevolucionSinNotaCredito] (
    [IDTransaccion]  NUMERIC (18)    NOT NULL,
    [IDProducto]     INT             NOT NULL,
    [ID]             TINYINT         NOT NULL,
    [Observacion]    VARCHAR (50)    NOT NULL,
    [Cantidad]       DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario] MONEY           NOT NULL,
    [Total]          MONEY           NOT NULL
);


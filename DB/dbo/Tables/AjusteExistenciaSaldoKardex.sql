﻿CREATE TABLE [dbo].[AjusteExistenciaSaldoKardex] (
    [IDTransaccion]    NUMERIC (18)  NOT NULL,
    [Numero]           INT           NOT NULL,
    [Fecha]            DATE          NOT NULL,
    [Observacion]      VARCHAR (200) NULL,
    [IDUsuario]        INT           NULL,
    [Anulado]          BIT           NULL,
    [FechaAnulado]     DATE          NULL,
    [IDUsuarioAnulado] INT           NULL
);


﻿CREATE TABLE [dbo].[CierreReapertura] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [NroComprobante]    NUMERIC (18)  NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [IDSucursal]        TINYINT       NULL,
    [FechaEmision]      DATETIME      NOT NULL,
    [Descripcion]       VARCHAR (100) NULL,
    [Total]             MONEY         NOT NULL,
    [Debito]            MONEY         NOT NULL,
    [Credito]           MONEY         NOT NULL,
    [Saldo]             MONEY         NULL,
    [Anulado]           BIT           NOT NULL,
    [Procesado]         BIT           NOT NULL,
    [IdAccion]          TINYINT       NULL,
    [Accion]            VARCHAR (100) NULL,
    [Anho]              VARCHAR (50)  NULL,
    CONSTRAINT [PK_CierreReapertura] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


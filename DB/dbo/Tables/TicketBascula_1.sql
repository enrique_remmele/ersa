﻿CREATE TABLE [dbo].[TicketBascula] (
    [IDTransaccion]       NUMERIC (18)  NOT NULL,
    [IDSucursal]          TINYINT       NOT NULL,
    [Numero]              INT           NOT NULL,
    [Fecha]               DATE          NOT NULL,
    [IDTipoComprobante]   SMALLINT      NOT NULL,
    [NroComprobante]      VARCHAR (50)  NULL,
    [IDProveedor]         INT           NULL,
    [IDMoneda]            TINYINT       NULL,
    [Cotizacion]          MONEY         NOT NULL,
    [IDDeposito]          TINYINT       NULL,
    [IDProducto]          INT           NULL,
    [IDImpuesto]          TINYINT       NULL,
    [PesoRemision]        MONEY         NOT NULL,
    [PesoBascula]         MONEY         NOT NULL,
    [Diferencia]          MONEY         NOT NULL,
    [PrecioUnitario]      MONEY         NOT NULL,
    [PrecioUnitarioUS]    MONEY         NOT NULL,
    [Total]               MONEY         NOT NULL,
    [TotalUS]             MONEY         NOT NULL,
    [TotalImpuesto]       MONEY         NOT NULL,
    [TotalImpuestoUS]     MONEY         NOT NULL,
    [TotalDiscriminado]   MONEY         NOT NULL,
    [TotalDiscriminadoUS] MONEY         NOT NULL,
    [CostoAdicional]      MONEY         NOT NULL,
    [CostoAdicionalUS]    MONEY         NOT NULL,
    [TipoFlete]           VARCHAR (16)  NOT NULL,
    [IDCamion]            INT           NOT NULL,
    [IDChofer]            BIGINT        NOT NULL,
    [Observacion]         VARCHAR (200) NULL,
    [Anulado]             BIT           CONSTRAINT [DF__TicketBas__Anula__61A73897] DEFAULT ('False') NOT NULL,
    [IDTransaccionGasto]  NUMERIC (18)  NULL,
    [NroAcuerdo]          INT           NULL,
    [IDUsuarioAnulacion]  INT           NULL,
    [FechaAnulacion]      DATETIME      NULL,
    [Procesado]           BIT           NULL,
    [NroRemision]         VARCHAR (16)  NULL,
    [NroRecepcion]        VARCHAR (16)  NULL,
    CONSTRAINT [PK_TicketBascula] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_TicketBascula_CamionProveedor] FOREIGN KEY ([IDCamion]) REFERENCES [dbo].[CamionProveedor] ([ID]),
    CONSTRAINT [FK_TicketBascula_ChoferProveedor] FOREIGN KEY ([IDChofer]) REFERENCES [dbo].[ChoferProveedor] ([ID]),
    CONSTRAINT [FK_TicketBascula_Deposito] FOREIGN KEY ([IDDeposito]) REFERENCES [dbo].[Deposito] ([ID]),
    CONSTRAINT [FK_TicketBascula_Impuesto] FOREIGN KEY ([IDImpuesto]) REFERENCES [dbo].[Impuesto] ([ID]),
    CONSTRAINT [FK_TicketBascula_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_TicketBascula_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID]),
    CONSTRAINT [FK_TicketBascula_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_TicketBascula_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_147802_147801]
    ON [dbo].[TicketBascula]([Anulado] ASC, [Procesado] ASC, [Fecha] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Numero], [IDTipoComprobante], [IDProveedor], [IDMoneda], [IDDeposito], [IDProducto], [IDImpuesto], [PesoRemision], [PesoBascula], [Diferencia], [PrecioUnitario], [PrecioUnitarioUS], [TotalDiscriminado], [TotalDiscriminadoUS], [IDCamion], [IDChofer], [NroAcuerdo]);


GO
CREATE NONCLUSTERED INDEX [missing_index_781_780]
    ON [dbo].[TicketBascula]([Anulado] ASC, [NroAcuerdo] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_479_478]
    ON [dbo].[TicketBascula]([Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_216260_216259]
    ON [dbo].[TicketBascula]([IDProducto] ASC, [Anulado] ASC, [Procesado] ASC)
    INCLUDE([Fecha], [IDDeposito], [IDImpuesto], [PrecioUnitario]);


GO
CREATE NONCLUSTERED INDEX [missing_index_1422_1421]
    ON [dbo].[TicketBascula]([IDSucursal] ASC, [Numero] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1420_1419]
    ON [dbo].[TicketBascula]([IDSucursal] ASC, [Numero] ASC)
    INCLUDE([IDTransaccion], [IDTipoComprobante], [IDProveedor], [IDMoneda], [IDDeposito], [IDProducto], [IDImpuesto], [IDCamion], [IDChofer]);


GO
CREATE NONCLUSTERED INDEX [missing_index_8366_8365]
    ON [dbo].[TicketBascula]([IDProducto] ASC, [Fecha] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Numero], [IDTipoComprobante], [NroComprobante], [IDProveedor], [IDMoneda], [Cotizacion], [IDDeposito], [IDImpuesto], [PesoRemision], [PesoBascula], [Diferencia], [PrecioUnitario], [PrecioUnitarioUS], [Total], [TotalUS], [TotalImpuesto], [TotalImpuestoUS], [TotalDiscriminado], [TotalDiscriminadoUS], [CostoAdicional], [CostoAdicionalUS], [TipoFlete], [IDCamion], [IDChofer], [Observacion], [Anulado], [IDTransaccionGasto], [NroAcuerdo], [IDUsuarioAnulacion], [FechaAnulacion], [Procesado], [NroRemision], [NroRecepcion]);


GO
CREATE NONCLUSTERED INDEX [missing_index_415182_415181]
    ON [dbo].[TicketBascula]([Fecha] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Numero], [IDTipoComprobante], [NroComprobante], [IDProveedor], [IDMoneda], [Cotizacion], [IDDeposito], [IDProducto], [IDImpuesto], [PesoRemision], [PesoBascula], [Diferencia], [PrecioUnitario], [PrecioUnitarioUS], [Total], [TotalUS], [TotalImpuesto], [TotalImpuestoUS], [TotalDiscriminado], [TotalDiscriminadoUS], [CostoAdicional], [CostoAdicionalUS], [TipoFlete], [IDCamion], [IDChofer], [Observacion], [Anulado], [IDTransaccionGasto], [NroAcuerdo], [IDUsuarioAnulacion], [FechaAnulacion], [Procesado], [NroRemision], [NroRecepcion]);


GO
CREATE NONCLUSTERED INDEX [missing_index_417698_417697]
    ON [dbo].[TicketBascula]([NroAcuerdo] ASC, [Fecha] ASC);


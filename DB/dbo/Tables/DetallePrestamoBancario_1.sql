﻿CREATE TABLE [dbo].[DetallePrestamoBancario] (
    [IDTransaccion]                      NUMERIC (18)  NOT NULL,
    [NroCuota]                           SMALLINT      NOT NULL,
    [FechaVencimiento]                   DATE          NOT NULL,
    [Amortizacion]                       MONEY         NOT NULL,
    [Interes]                            MONEY         NULL,
    [Impuesto]                           MONEY         NULL,
    [ImporteCuota]                       MONEY         NOT NULL,
    [Pagar]                              BIT           NULL,
    [FechaPago]                          DATE          NULL,
    [ImporteAPagar]                      MONEY         NULL,
    [PagosVarios]                        MONEY         NULL,
    [ObservacionPago]                    VARCHAR (100) NULL,
    [Cancelado]                          BIT           NULL,
    [IDTransaccionDebitoCreditoBancario] NUMERIC (18)  NULL,
    [DebitoAutomatico]                   BIT           CONSTRAINT [DF_DetallePrestamoBancario_DebitoAutomatico] DEFAULT ('True') NULL,
    [TotalPagado]                        MONEY         NULL,
    [Saldo]                              AS            (isnull([ImporteCuota],(0))-isnull([TotalPagado],(0))),
    CONSTRAINT [PK_DetallePrestamoBancario] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [NroCuota] ASC),
    CONSTRAINT [FK_DetallePrestamoBancario_DebitoCreditoBancario] FOREIGN KEY ([IDTransaccionDebitoCreditoBancario]) REFERENCES [dbo].[DebitoCreditoBancario] ([IDTransaccion]) ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT [FK_DetallePrestamoBancario_PrestamoBancario] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[PrestamoBancario] ([IDTransaccion])
);


GO
ALTER TABLE [dbo].[DetallePrestamoBancario] NOCHECK CONSTRAINT [FK_DetallePrestamoBancario_DebitoCreditoBancario];


﻿CREATE TABLE [dbo].[CheckRegistry] (
    [CheckNumber] SMALLINT     NULL,
    [PayTo]       VARCHAR (20) NULL,
    [Amount]      MONEY        NULL,
    [CheckFor]    VARCHAR (20) NULL,
    [CheckDate]   DATE         NULL
);


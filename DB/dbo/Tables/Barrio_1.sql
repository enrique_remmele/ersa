﻿CREATE TABLE [dbo].[Barrio] (
    [ID]          SMALLINT     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Orden]       SMALLINT     NULL,
    [Estado]      BIT          CONSTRAINT [DF_Barrio_Estado] DEFAULT ('True') NULL,
    [Referencia]  VARCHAR (10) NULL,
    CONSTRAINT [PK_Barrio] PRIMARY KEY CLUSTERED ([ID] ASC)
);


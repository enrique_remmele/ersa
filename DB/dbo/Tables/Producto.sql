﻿CREATE TABLE [dbo].[Producto] (
    [ID]                      INT             NOT NULL,
    [Descripcion]             VARCHAR (100)   NOT NULL,
    [Referencia]              VARCHAR (30)    NULL,
    [CodigoBarra]             VARCHAR (30)    NULL,
    [IDClasificacion]         SMALLINT        NULL,
    [IDTipoProducto]          TINYINT         NULL,
    [IDLinea]                 SMALLINT        NULL,
    [IDSubLinea]              SMALLINT        NULL,
    [IDSubLinea2]             SMALLINT        NULL,
    [IDMarca]                 TINYINT         NULL,
    [IDPresentacion]          SMALLINT        NULL,
    [IDCategoria]             TINYINT         NULL,
    [IDProveedor]             INT             NULL,
    [IDDivision]              SMALLINT        NULL,
    [IDProcedencia]           TINYINT         NULL,
    [IDUnidadMedida]          TINYINT         NULL,
    [UnidadPorCaja]           SMALLINT        NULL,
    [IDUnidadMedidaConvertir] TINYINT         NULL,
    [UnidadConvertir]         DECIMAL (10, 3) NULL,
    [Peso]                    VARCHAR (50)    NULL,
    [Volumen]                 VARCHAR (15)    NULL,
    [IDImpuesto]              TINYINT         CONSTRAINT [DF_Producto_IDIVA] DEFAULT ((1)) NOT NULL,
    [Estado]                  BIT             CONSTRAINT [DF_Producto_Estado] DEFAULT ('True') NOT NULL,
    [ControlarExistencia]     BIT             CONSTRAINT [DF_Producto_ControlarExistencia] DEFAULT ('True') NOT NULL,
    [IDCuentaContableCompra]  INT             NULL,
    [CuentaContableCompra]    VARCHAR (50)    NULL,
    [IDCuentaContableVenta]   INT             NULL,
    [CuentaContableVenta]     VARCHAR (50)    NULL,
    [CuentaContableCosto]     VARCHAR (50)    NULL,
    [IDCuentaContableDeudor]  INT             NULL,
    [CuentaContableDeudor]    VARCHAR (50)    NULL,
    [UltimaEntrada]           DATETIME        NULL,
    [CantidadEntrada]         DECIMAL (10, 2) NULL,
    [UltimaSalida]            DATETIME        NULL,
    [CantidadSalida]          DECIMAL (10, 2) NULL,
    [UltimoCosto]             MONEY           NULL,
    [CostoSinIVA]             MONEY           NULL,
    [IDMonedaUltimoCosto]     TINYINT         NULL,
    [TipoCambio]              MONEY           NULL,
    [CostoCG]                 MONEY           NULL,
    [CostoPromedio]           MONEY           NULL,
    [ExistenciaGeneral]       DECIMAL (10, 2) NULL,
    [EnEspera]                BIT             NULL,
    [Reemplazado]             BIT             NULL,
    [FactorCosto]             DECIMAL (10, 4) CONSTRAINT [DF_Producto_FactorCosto] DEFAULT ((1)) NOT NULL,
    [UltimoCostoSinFactor]    MONEY           NULL,
    [MargenMinimo]            DECIMAL (5, 2)  CONSTRAINT [DF_Producto_MargenMinimo] DEFAULT ((0)) NULL,
    [Vendible]                BIT             NULL,
    [IDDeposito]              TINYINT         NULL,
    [ConsumoCombustible]      BIT             CONSTRAINT [DF__Producto__Consum__1F4F6555] DEFAULT ('False') NULL,
    [MateriaPrima]            BIT             CONSTRAINT [DF__Producto__Materi__7ADCFAB5] DEFAULT ('False') NULL,
    [DescargaCompra]          BIT             CONSTRAINT [DF__Producto__Descar__0060C9E1] DEFAULT ('False') NULL,
    CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Producto_Categoria] FOREIGN KEY ([IDCategoria]) REFERENCES [dbo].[Categoria] ([ID]),
    CONSTRAINT [FK_Producto_CuentaContable] FOREIGN KEY ([IDCuentaContableCompra]) REFERENCES [dbo].[CuentaContable] ([ID]),
    CONSTRAINT [FK_Producto_CuentaContable1] FOREIGN KEY ([IDCuentaContableVenta]) REFERENCES [dbo].[CuentaContable] ([ID]),
    CONSTRAINT [FK_Producto_Division] FOREIGN KEY ([IDDivision]) REFERENCES [dbo].[Division] ([ID]),
    CONSTRAINT [FK_Producto_Linea] FOREIGN KEY ([IDLinea]) REFERENCES [dbo].[Linea] ([ID]),
    CONSTRAINT [FK_Producto_Marca] FOREIGN KEY ([IDMarca]) REFERENCES [dbo].[Marca] ([ID]),
    CONSTRAINT [FK_Producto_Moneda] FOREIGN KEY ([IDMonedaUltimoCosto]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_Producto_Pais] FOREIGN KEY ([IDProcedencia]) REFERENCES [dbo].[Pais] ([ID]),
    CONSTRAINT [FK_Producto_Presentacion] FOREIGN KEY ([IDPresentacion]) REFERENCES [dbo].[Presentacion] ([ID]),
    CONSTRAINT [FK_Producto_Proveedor] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[Proveedor] ([ID]),
    CONSTRAINT [FK_Producto_TipoProducto] FOREIGN KEY ([IDTipoProducto]) REFERENCES [dbo].[TipoProducto] ([ID]),
    CONSTRAINT [FK_Producto_UnidadMedida] FOREIGN KEY ([IDUnidadMedida]) REFERENCES [dbo].[UnidadMedida] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_2_1]
    ON [dbo].[Producto]([Estado] ASC, [ControlarExistencia] ASC)
    INCLUDE([ID], [Descripcion], [Referencia], [IDTipoProducto], [IDLinea], [IDSubLinea], [IDMarca], [Peso], [CuentaContableCompra], [CuentaContableVenta], [CuentaContableCosto], [CuentaContableDeudor]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'P/ ERSA. Es un coeficiente que se multiplica por el costo de compra de un producto y cuyo resultado es el costoFinal.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Producto', @level2type = N'COLUMN', @level2name = N'FactorCosto';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Ultimo costo directo (sin calcular por el factor de costo)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Producto', @level2type = N'COLUMN', @level2name = N'UltimoCostoSinFactor';


﻿CREATE TABLE [dbo].[Informe] (
    [idInforme]   INT           IDENTITY (1, 1) NOT NULL,
    [nombre]      VARCHAR (200) NOT NULL,
    [archivo]     IMAGE         NULL,
    [version]     INT           NOT NULL,
    [usuario]     VARCHAR (50)  NULL,
    [fecha]       DATETIME      CONSTRAINT [DF_Informe_fecha] DEFAULT (getdate()) NULL,
    [observacion] VARCHAR (500) NULL,
    CONSTRAINT [PK_Informe] PRIMARY KEY CLUSTERED ([nombre] ASC, [version] ASC)
);


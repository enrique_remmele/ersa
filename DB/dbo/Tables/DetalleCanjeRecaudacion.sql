﻿CREATE TABLE [dbo].[DetalleCanjeRecaudacion] (
    [IDTransaccion]          NUMERIC (18) NOT NULL,
    [ID]                     INT          NOT NULL,
    [IDTransaccionEfectivo]  NUMERIC (18) NULL,
    [IDEfectivo]             TINYINT      NULL,
    [IDTransaccionCheque]    NUMERIC (18) NULL,
    [IDTransaccionDocumento] NUMERIC (18) NULL,
    [IDDetalleDocumento]     INT          NULL,
    [IDTransaccionTarjeta]   NUMERIC (18) NULL,
    [Importe]                MONEY        NULL
);


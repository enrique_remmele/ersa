﻿CREATE TABLE [dbo].[TipoDescuento] (
    [ID]               TINYINT      NOT NULL,
    [Descripcion]      VARCHAR (50) NOT NULL,
    [Codigo]           VARCHAR (5)  NOT NULL,
    [RangoFecha]       BIT          CONSTRAINT [DF_TipoDescuento_RangoFecha] DEFAULT ('True') NOT NULL,
    [Limite]           TINYINT      CONSTRAINT [DF_TipoDescuento_Limite] DEFAULT ((100)) NOT NULL,
    [CuentaVenta]      VARCHAR (50) NULL,
    [CuentaCompra]     VARCHAR (50) NULL,
    [Editable]         BIT          NULL,
    [PlanillaTactico]  BIT          NULL,
    [DescuentoTactico] BIT          NULL,
    [DescuentosFijos]  BIT          NULL,
    CONSTRAINT [PK_TipoDescuento] PRIMARY KEY CLUSTERED ([ID] ASC)
);


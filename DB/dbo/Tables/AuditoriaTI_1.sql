﻿CREATE TABLE [dbo].[AuditoriaTI] (
    [ID]                     INT           NULL,
    [Fecha]                  DATETIME      NULL,
    [IDUsuario]              SMALLINT      NULL,
    [IDTerminal]             SMALLINT      NULL,
    [Evento]                 VARCHAR (50)  NULL,
    [Operacion]              VARCHAR (50)  NULL,
    [Observacion]            VARCHAR (500) NULL,
    [IDTransaccionOperacion] NUMERIC (18)  NOT NULL,
    [Tabla]                  VARCHAR (100) NULL
);


﻿CREATE TABLE [dbo].[ChoferProveedor] (
    [ID]           BIGINT        NOT NULL,
    [IDCamion]     INT           NOT NULL,
    [Nombres]      VARCHAR (50)  NOT NULL,
    [NroDocumento] VARCHAR (15)  NULL,
    [Telefono]     VARCHAR (20)  NULL,
    [Celular]      VARCHAR (20)  NULL,
    [Direccion]    VARCHAR (100) NULL,
    [Email]        VARCHAR (50)  NULL,
    [Estado]       BIT           CONSTRAINT [DF_ChoferProveedor_Estado] DEFAULT ('False') NULL,
    CONSTRAINT [PK_ChoferProveedor] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ChoferProveedor_Camion] FOREIGN KEY ([IDCamion]) REFERENCES [dbo].[CamionProveedor] ([ID])
);


﻿CREATE TABLE [dbo].[DetalleImpuesto] (
    [IDTransaccion]     NUMERIC (18) NOT NULL,
    [IDImpuesto]        TINYINT      NOT NULL,
    [Total]             MONEY        NOT NULL,
    [TotalImpuesto]     MONEY        NOT NULL,
    [TotalDiscriminado] MONEY        NOT NULL,
    [TotalDescuento]    MONEY        NULL,
    [RetencionIVA]      MONEY        NULL,
    [RetencionRenta]    MONEY        NULL,
    CONSTRAINT [PK_DetalleImpuesto] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDImpuesto] ASC),
    CONSTRAINT [FK_DetalleImpuesto_Impuesto] FOREIGN KEY ([IDImpuesto]) REFERENCES [dbo].[Impuesto] ([ID]),
    CONSTRAINT [FK_DetalleImpuesto_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


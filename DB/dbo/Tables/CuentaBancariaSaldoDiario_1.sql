﻿CREATE TABLE [dbo].[CuentaBancariaSaldoDiario] (
    [IDCuentaBancaria] SMALLINT NOT NULL,
    [Fecha]            DATE     NOT NULL,
    [Saldo]            MONEY    NOT NULL
);


﻿CREATE TABLE [dbo].[HistorialCostoProducto] (
    [IDProducto]      INT             NULL,
    [Indice]          INT             NULL,
    [IDTransaccion]   INT             NULL,
    [UltimoCosto]     DECIMAL (18, 3) NULL,
    [CantidadEntrada] DECIMAL (18, 3) NULL,
    [CantidadTotal]   DECIMAL (18, 3) NULL,
    [CostoPromedio]   DECIMAL (18, 3) NULL
);


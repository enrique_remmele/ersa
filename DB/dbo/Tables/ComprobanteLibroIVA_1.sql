﻿CREATE TABLE [dbo].[ComprobanteLibroIVA] (
    [IDTransaccion]            NUMERIC (18)  NOT NULL,
    [NroComprobante]           VARCHAR (50)  NULL,
    [Fecha]                    DATE          NULL,
    [Detalle]                  VARCHAR (200) NULL,
    [IDMoneda]                 TINYINT       NULL,
    [Cotizacion]               MONEY         NULL,
    [Total]                    MONEY         NULL,
    [TotalImpuesto]            MONEY         NULL,
    [TotalDiscriminado]        MONEY         NULL,
    [IDTipoComprobante]        TINYINT       NULL,
    [NroTimbrado]              VARCHAR (50)  NULL,
    [FechaVencimientoTimbrado] DATE          NULL,
    [IDProveedor]              INT           NULL,
    [IDCliente]                INT           NULL,
    [Credito]                  BIT           NULL,
    [Numero]                   INT           NULL,
    [IDSucursal]               TINYINT       NULL,
    [IDOperacion]              TINYINT       NULL,
    [Directo]                  BIT           CONSTRAINT [DF_ComprobanteLibroIVA_Directo] DEFAULT ('True') NULL,
    CONSTRAINT [PK_ComprobanteLibroIVA] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


﻿CREATE TABLE [dbo].[Cobrador] (
    [ID]           TINYINT       NOT NULL,
    [Nombres]      VARCHAR (50)  NOT NULL,
    [NroDocumento] VARCHAR (15)  NULL,
    [Telefono]     VARCHAR (20)  NULL,
    [Celular]      VARCHAR (20)  NULL,
    [Direccion]    VARCHAR (100) NULL,
    [Email]        VARCHAR (50)  NULL,
    [Estado]       BIT           NULL,
    [Referencia]   VARCHAR (10)  NULL,
    [IdSucursal]   TINYINT       NULL,
    CONSTRAINT [PK_Cobrador] PRIMARY KEY CLUSTERED ([ID] ASC)
);


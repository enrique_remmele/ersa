﻿CREATE TABLE [dbo].[Grupo] (
    [ID]               TINYINT      NOT NULL,
    [Descripcion]      VARCHAR (50) NULL,
    [Estado]           BIT          NULL,
    [IDCuentaContable] INT          NULL,
    [SoloVales]        BIT          NULL,
    CONSTRAINT [PK_Grupo] PRIMARY KEY CLUSTERED ([ID] ASC)
);


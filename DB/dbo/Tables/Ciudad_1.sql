﻿CREATE TABLE [dbo].[Ciudad] (
    [ID]             SMALLINT     NOT NULL,
    [IDPais]         TINYINT      NOT NULL,
    [IDDepartamento] TINYINT      NOT NULL,
    [Descripcion]    VARCHAR (50) NOT NULL,
    [Codigo]         VARCHAR (5)  NULL,
    [Estado]         BIT          NULL,
    [Orden]          TINYINT      NULL,
    [Referencia]     VARCHAR (10) NULL,
    [CodigoUnilever] VARCHAR (10) NULL,
    CONSTRAINT [PK_Ciudad] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Ciudad_Departamento] FOREIGN KEY ([IDDepartamento]) REFERENCES [dbo].[Departamento] ([ID]),
    CONSTRAINT [FK_Ciudad_Pais] FOREIGN KEY ([IDPais]) REFERENCES [dbo].[Pais] ([ID])
);


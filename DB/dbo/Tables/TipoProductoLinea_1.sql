﻿CREATE TABLE [dbo].[TipoProductoLinea] (
    [IDTipoProducto] TINYINT  NOT NULL,
    [IDLinea]        SMALLINT NOT NULL,
    CONSTRAINT [PK_TipoProductoLinea] PRIMARY KEY CLUSTERED ([IDTipoProducto] ASC, [IDLinea] ASC),
    CONSTRAINT [FK_TipoProductoLinea_Linea] FOREIGN KEY ([IDLinea]) REFERENCES [dbo].[Linea] ([ID]),
    CONSTRAINT [FK_TipoProductoLinea_TipoProducto] FOREIGN KEY ([IDTipoProducto]) REFERENCES [dbo].[TipoProducto] ([ID])
);


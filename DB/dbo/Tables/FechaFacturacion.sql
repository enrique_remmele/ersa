﻿CREATE TABLE [dbo].[FechaFacturacion] (
    [IDSucursal]                  TINYINT  NOT NULL,
    [Fecha]                       DATE     NOT NULL,
    [IDUsuarioUltimaModificacion] INT      NULL,
    [FechaUltimaModificacion]     DATETIME NULL
);


﻿CREATE TABLE [dbo].[PlanillaDescuentoTactico] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Numero]            INT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [IDProveedor]       INT           NOT NULL,
    [Desde]             DATE          NOT NULL,
    [Hasta]             DATE          NOT NULL,
    [TotalPresupuesto]  MONEY         CONSTRAINT [DF_PlanillaDescuentoTactico_TotalPresupuesto] DEFAULT ((0)) NOT NULL,
    [TotalUtilizado]    MONEY         CONSTRAINT [DF_PlanillaDescuentoTactico_TotalUtilizado] DEFAULT ((0)) NOT NULL,
    [TotalSaldo]        MONEY         CONSTRAINT [DF_PlanillaDescuentoTactico_TotalSaldo] DEFAULT ((0)) NOT NULL,
    [TotalExedente]     MONEY         CONSTRAINT [DF_PlanillaDescuentoTactico_TotalExedente] DEFAULT ((0)) NOT NULL,
    [Observacion]       VARCHAR (500) NULL,
    [Anulado]           BIT           NOT NULL,
    [FechaAnulado]      DATE          NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    CONSTRAINT [PK_PlanillaDescuentoTactico] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_PlanillaDescuentoTactico_Proveedor] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[Proveedor] ([ID])
);


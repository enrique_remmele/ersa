﻿CREATE TABLE [dbo].[NotaCreditoVenta] (
    [IDTransaccionNotaCredito]   NUMERIC (18) NOT NULL,
    [IDTransaccionVentaGenerada] NUMERIC (18) NOT NULL,
    [ID]                         TINYINT      NOT NULL,
    [Importe]                    MONEY        NOT NULL,
    [cobrado]                    MONEY        NOT NULL,
    [Descontado]                 MONEY        NOT NULL,
    [Saldo]                      MONEY        NOT NULL,
    CONSTRAINT [PK_NotaCreditoVenta_1] PRIMARY KEY CLUSTERED ([IDTransaccionNotaCredito] ASC, [IDTransaccionVentaGenerada] ASC, [ID] ASC),
    CONSTRAINT [FK_NotaCreditoVenta_NotaCredito] FOREIGN KEY ([IDTransaccionNotaCredito]) REFERENCES [dbo].[NotaCredito] ([IDTransaccion]),
    CONSTRAINT [FK_NotaCreditoVenta_Venta1] FOREIGN KEY ([IDTransaccionVentaGenerada]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200324-114410]
    ON [dbo].[NotaCreditoVenta]([IDTransaccionVentaGenerada] ASC);


﻿CREATE TABLE [dbo].[DetallePedidoTemp] (
    [IDTransaccion]          NUMERIC (18)    NOT NULL,
    [IDProducto]             INT             NOT NULL,
    [ID]                     TINYINT         NOT NULL,
    [IDDeposito]             TINYINT         NOT NULL,
    [Observacion]            VARCHAR (50)    NOT NULL,
    [IDImpuesto]             TINYINT         NOT NULL,
    [Cantidad]               DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario]         MONEY           CONSTRAINT [DF_DetallePedidoTemp_PrecioUnitario] DEFAULT ((0)) NOT NULL,
    [CostoUnitario]          MONEY           NULL,
    [DescuentoUnitario]      MONEY           CONSTRAINT [DF_DetallePedidoTemp_DescuentoUnitario] DEFAULT ((0)) NULL,
    [PorcentajeDescuento]    NUMERIC (2)     NULL,
    [Total]                  MONEY           CONSTRAINT [DF_DetallePedidoTemp_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]          MONEY           CONSTRAINT [DF_DetallePedidoTemp_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalCostoImpuesto]     MONEY           NULL,
    [TotalCosto]             MONEY           NULL,
    [TotalDescuento]         MONEY           CONSTRAINT [DF_DetallePedidoTemp_TotalDescuento] DEFAULT ((0)) NULL,
    [TotalDiscriminado]      MONEY           NOT NULL,
    [TotalCostoDiscriminado] MONEY           NULL,
    [Caja]                   BIT             NULL,
    [CantidadCaja]           DECIMAL (10, 2) NULL,
    [Eliminar]               BIT             CONSTRAINT [DF_DetallePedidoTemp_Eliminar] DEFAULT ('False') NULL,
    CONSTRAINT [PK_DetallePedidoTemp] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC)
);


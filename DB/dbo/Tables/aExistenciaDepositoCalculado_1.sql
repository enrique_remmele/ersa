﻿CREATE TABLE [dbo].[aExistenciaDepositoCalculado] (
    [IDDeposito]             TINYINT         NOT NULL,
    [IDProducto]             INT             NOT NULL,
    [ExistenciaAnterior]     DECIMAL (10, 2) NOT NULL,
    [ExistenciaNueva]        DECIMAL (10, 2) NULL,
    [IDUsuario]              SMALLINT        NOT NULL,
    [IDSucursal]             TINYINT         NOT NULL,
    [IDDepositoOperacion]    TINYINT         NOT NULL,
    [IDTerminal]             INT             NOT NULL,
    [IDTransaccionActualiza] INT             NULL
);


﻿CREATE TABLE [dbo].[DetalleAjusteInicial] (
    [IDTransaccion]     NUMERIC (18)    NOT NULL,
    [IDProducto]        INT             NOT NULL,
    [IDSucursal]        TINYINT         NOT NULL,
    [IDDeposito]        TINYINT         NOT NULL,
    [ExistenciaSistema] DECIMAL (10, 2) CONSTRAINT [DF_DetalleAjusteInicial_ExistenciaSistema] DEFAULT ((0)) NOT NULL,
    [Existencia]        DECIMAL (10, 2) CONSTRAINT [DF_DetalleAjusteInicial_Existencia] DEFAULT ((0)) NOT NULL,
    [Diferencia]        DECIMAL (10, 2) CONSTRAINT [DF_DetalleAjusteInicial_Diferencia] DEFAULT ((0)) NOT NULL,
    [Entrada]           DECIMAL (10, 2) CONSTRAINT [DF_DetalleAjusteInicial_Entrada] DEFAULT ((0)) NOT NULL,
    [Salida]            DECIMAL (10, 2) CONSTRAINT [DF_DetalleAjusteInicial_Salida] DEFAULT ((0)) NOT NULL,
    [Costo]             MONEY           NULL,
    [Total]             AS              ([Diferencia]*[Costo]),
    CONSTRAINT [PK_DetalleAjusteInicial] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [IDSucursal] ASC, [IDDeposito] ASC)
);


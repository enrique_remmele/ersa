﻿CREATE TABLE [dbo].[TransferenciaNotaCredito] (
    [IDTransaccion]            NUMERIC (18)  NOT NULL,
    [IDTransaccionNotaCredito] NUMERIC (18)  NOT NULL,
    [Numero]                   INT           NOT NULL,
    [IDTipoComprobante]        VARCHAR (50)  NOT NULL,
    [NroComprobante]           VARCHAR (50)  NOT NULL,
    [IDSucursal]               TINYINT       NOT NULL,
    [IDCliente]                INT           NOT NULL,
    [Fecha]                    DATE          NOT NULL,
    [IDMoneda]                 TINYINT       NOT NULL,
    [Cotizacion]               MONEY         NOT NULL,
    [Observacion]              VARCHAR (150) NULL,
    [ComprobanteNotaCredito]   VARCHAR (50)  NULL,
    CONSTRAINT [PK_TransferenciaNotaCredito] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_TransferenciaNotaCredito_NotaCredito] FOREIGN KEY ([IDTransaccionNotaCredito]) REFERENCES [dbo].[NotaCredito] ([IDTransaccion])
);


GO
ALTER TABLE [dbo].[TransferenciaNotaCredito] NOCHECK CONSTRAINT [FK_TransferenciaNotaCredito_NotaCredito];


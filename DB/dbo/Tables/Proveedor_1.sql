﻿CREATE TABLE [dbo].[Proveedor] (
    [ID]                     INT              NOT NULL,
    [RazonSocial]            VARCHAR (100)    NOT NULL,
    [RUC]                    VARCHAR (15)     NOT NULL,
    [Referencia]             VARCHAR (50)     NULL,
    [NombreFantasia]         VARCHAR (100)    NULL,
    [Direccion]              VARCHAR (100)    NULL,
    [Telefono]               VARCHAR (20)     NULL,
    [Estado]                 BIT              NOT NULL,
    [Contado]                BIT              NULL,
    [Credito]                BIT              NULL,
    [PlazoCredito]           TINYINT          NULL,
    [IDPais]                 TINYINT          NULL,
    [IDDepartamento]         TINYINT          NULL,
    [IDCiudad]               SMALLINT         NULL,
    [IDBarrio]               SMALLINT         NULL,
    [IDSucursal]             TINYINT          NULL,
    [IDTipoProveedor]        TINYINT          NULL,
    [IDMoneda]               TINYINT          NULL,
    [FechaAlta]              DATE             NULL,
    [IDUsuarioAlta]          INT              NULL,
    [FechaModificacion]      DATE             NULL,
    [IDUsuarioModificacion]  SMALLINT         NULL,
    [FechaUltimaCompra]      DATE             NULL,
    [PaginaWeb]              VARCHAR (50)     NULL,
    [Fax]                    VARCHAR (20)     NULL,
    [Email]                  VARCHAR (50)     NULL,
    [IDCuentaContableCompra] INT              NULL,
    [IDCuentaContableVenta]  SMALLINT         NULL,
    [Deuda]                  MONEY            CONSTRAINT [DF_Proveedor_Deuda] DEFAULT ((0)) NULL,
    [Retentor]               BIT              NULL,
    [TipoCompra]             CHAR (10)        NULL,
    [Exportador]             BIT              NULL,
    [SujetoRetencion]        BIT              NULL,
    [CuentaContableCompra]   VARCHAR (50)     NULL,
    [CuentaContableVenta]    VARCHAR (50)     NULL,
    [CuentaContableAnticipo] VARCHAR (50)     NULL,
    [Acopiador]              BIT              NULL,
    [Latitud]                DECIMAL (18, 16) NULL,
    [Longitud]               DECIMAL (18, 16) NULL,
    CONSTRAINT [PK_Proveedor] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Proveedor_Usuario] FOREIGN KEY ([IDUsuarioAlta]) REFERENCES [dbo].[Usuario] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_1921_1920]
    ON [dbo].[Proveedor]([Referencia] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1919_1918]
    ON [dbo].[Proveedor]([RUC] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1923_1922]
    ON [dbo].[Proveedor]([RUC] ASC, [ID] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_415405_415404]
    ON [dbo].[Proveedor]([Estado] ASC)
    INCLUDE([ID], [RazonSocial], [RUC], [Referencia], [NombreFantasia], [Direccion], [Telefono]);


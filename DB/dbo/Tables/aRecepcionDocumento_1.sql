﻿CREATE TABLE [dbo].[aRecepcionDocumento] (
    [IDTransaccion]      INT           NULL,
    [NombreRecibido]     VARCHAR (80)  NULL,
    [FechaRecibido]      DATE          NULL,
    [Observacion]        VARCHAR (100) NULL,
    [IDUsuario]          INT           NULL,
    [FechaRegistro]      DATETIME      NULL,
    [OperacionAuditoria] VARCHAR (50)  NULL,
    [FechaAuditoria]     DATETIME      NULL,
    [IDUsuarioAuditoria] INT           NULL
);


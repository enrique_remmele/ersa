﻿CREATE TABLE [dbo].[CFNotaCreditoProveedor] (
    [IDOperacion]       TINYINT NOT NULL,
    [ID]                TINYINT NOT NULL,
    [BuscarEnProveedor] BIT     NULL,
    [BuscarEnProducto]  BIT     NULL,
    [IDTipoCuentaFija]  TINYINT NULL,
    CONSTRAINT [PK_CFNotaCreditoProveedor] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


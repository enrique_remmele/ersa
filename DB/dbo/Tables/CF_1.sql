﻿CREATE TABLE [dbo].[CF] (
    [IDOperacion]       TINYINT      NOT NULL,
    [ID]                TINYINT      NOT NULL,
    [IDSucursal]        TINYINT      NOT NULL,
    [IDMoneda]          TINYINT      NOT NULL,
    [IDTipoComprobante] SMALLINT     NULL,
    [CuentaContable]    VARCHAR (50) NOT NULL,
    [Debe]              BIT          NOT NULL,
    [Haber]             BIT          NOT NULL,
    [Descripcion]       VARCHAR (50) NOT NULL,
    [Orden]             TINYINT      NOT NULL,
    CONSTRAINT [PK_CF] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[DetallePedido] (
    [IDTransaccion]                 NUMERIC (18)    NOT NULL,
    [IDProducto]                    INT             NOT NULL,
    [ID]                            TINYINT         NOT NULL,
    [IDDeposito]                    TINYINT         NOT NULL,
    [Observacion]                   VARCHAR (200)   NOT NULL,
    [IDImpuesto]                    TINYINT         NOT NULL,
    [Cantidad]                      DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario]                MONEY           CONSTRAINT [DF_DetallePedido_PrecioUnitario_2] DEFAULT ((0)) NOT NULL,
    [Total]                         MONEY           CONSTRAINT [DF_DetallePedido_Total_2] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]                 MONEY           CONSTRAINT [DF_DetallePedido_TotalImpuesto_2] DEFAULT ((0)) NOT NULL,
    [TotalDiscriminado]             MONEY           NOT NULL,
    [PorcentajeDescuento]           NUMERIC (5, 2)  NULL,
    [DescuentoUnitario]             MONEY           CONSTRAINT [DF_DetallePedido_DescuentoUnitario_2] DEFAULT ((0)) NULL,
    [DescuentoUnitarioDiscriminado] MONEY           CONSTRAINT [DF_DetallePedido_DescuentoUnitarioDiscriminado_1] DEFAULT ((0)) NULL,
    [TotalDescuento]                MONEY           CONSTRAINT [DF_DetallePedido_TotalDescuento_2] DEFAULT ((0)) NULL,
    [TotalDescuentoDiscriminado]    MONEY           CONSTRAINT [DF_DetallePedido_TotalDescuentoDiscriminado_1] DEFAULT ((0)) NULL,
    [CostoUnitario]                 MONEY           NULL,
    [TotalCosto]                    MONEY           NULL,
    [TotalCostoImpuesto]            MONEY           NULL,
    [TotalCostoDiscriminado]        MONEY           NULL,
    [Caja]                          BIT             NULL,
    [CantidadCaja]                  DECIMAL (10, 2) NULL,
    [Secuencia]                     VARCHAR (50)    NULL,
    [CantidadAEntregar]             DECIMAL (10, 2) NULL,
    CONSTRAINT [PK_DetallePedido_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_DetallePedido_Pedido] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Pedido] ([IDTransaccion])
);


GO
CREATE NONCLUSTERED INDEX [idx_IDDeposito]
    ON [dbo].[DetallePedido]([IDDeposito] ASC);


GO
CREATE Trigger [dbo].[TI_DetallePedido] On [dbo].[DetallePedido]
For Insert, Update

As

Begin

	Set NoCount On  

	--Variables
	Declare @vIDTransaccion int
	Declare @vEntregaDirecta bit = 0

	--Obtener valores
	Select	@vIDTransaccion=IDTransaccion
	From Inserted
	--select * from pedido where numero = 32890
	--Entrega directa
	if (select sum(Peso * Cantidad) from vDetallePedido where IDTransaccion = @vIDTransaccion)>=25000 begin
		--Set @vEntregaDirecta = 1
		Update Pedido set EntregaCliente = 'True'
		Where IDTransaccion=@vIDTransaccion
	end

	--Habilitar el pedido
	--Update Pedido set EntregaCliente = @vEntregaDirecta
	--Where IDTransaccion=@vIDTransaccion

	GoTo Salir

Salir:

	
End

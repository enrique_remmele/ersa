﻿CREATE TABLE [dbo].[DetalleCargaMercaderia] (
    [IDTransaccion] NUMERIC (18)    NOT NULL,
    [IDProducto]    INT             NOT NULL,
    [Cantidad]      DECIMAL (10, 2) NOT NULL,
    [CantidadCaja]  DECIMAL (10, 2) NULL,
    CONSTRAINT [PK_DetalleCargaMercaderia] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC),
    CONSTRAINT [FK_DetalleCargaMercaderia_CargaMercaderia] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[CargaMercaderia] ([IDTransaccion]) ON DELETE CASCADE
);


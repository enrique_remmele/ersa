﻿CREATE TABLE [dbo].[OrdenCompra] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [Numero]            INT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [NroComprobante]    VARCHAR (50)  NOT NULL,
    [IDProveedor]       INT           NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [Credito]           BIT           NOT NULL,
    [FechaVencimiento]  DATE          NULL,
    [IDMoneda]          TINYINT       NOT NULL,
    [Cotizacion]        MONEY         NULL,
    [Observacion]       VARCHAR (500) NULL,
    [Total]             MONEY         NOT NULL,
    [TotalImpuesto]     MONEY         NOT NULL,
    [TotalDiscriminado] MONEY         NOT NULL,
    [Saldo]             MONEY         NOT NULL,
    [Cancelado]         BIT           NOT NULL,
    [Directo]           BIT           CONSTRAINT [DF_OrdenCompra_Directo] DEFAULT ('True') NULL,
    CONSTRAINT [PK_OrdenCompra] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


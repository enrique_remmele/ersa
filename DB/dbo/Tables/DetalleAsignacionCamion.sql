﻿CREATE TABLE [dbo].[DetalleAsignacionCamion] (
    [IDTransaccion]               NUMERIC (18)    NOT NULL,
    [ID]                          TINYINT         NOT NULL,
    [IDTransaccionAbastecimiento] NUMERIC (18)    NOT NULL,
    [IDTransaccionPedido]         NUMERIC (18)    NOT NULL,
    [IDOperacion]                 TINYINT         NOT NULL,
    [Tipo]                        VARCHAR (32)    NOT NULL,
    [IDProducto]                  INT             NOT NULL,
    [Cantidad]                    DECIMAL (10, 2) NOT NULL,
    [Anulado]                     BIT             CONSTRAINT [DF_DetalleAsignacionCamion_Anulado] DEFAULT ('False') NOT NULL,
    CONSTRAINT [PK_DetalleAsignacionCamion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleAsignacionCamion_AsignacionCamion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[AsignacionCamion] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleAsignacionCamion_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_978_977]
    ON [dbo].[DetalleAsignacionCamion]([IDTransaccionPedido] ASC);


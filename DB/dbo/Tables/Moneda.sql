﻿CREATE TABLE [dbo].[Moneda] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Impresion]   VARCHAR (50) NULL,
    [Referencia]  VARCHAR (10) NOT NULL,
    [Decimales]   BIT          NOT NULL,
    [Estado]      BIT          NULL,
    [Divide]      BIT          NULL,
    [Multiplica]  BIT          NULL,
    CONSTRAINT [PK_Moneda] PRIMARY KEY CLUSTERED ([ID] ASC)
);


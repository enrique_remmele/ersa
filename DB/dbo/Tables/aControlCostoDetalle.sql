﻿CREATE TABLE [dbo].[aControlCostoDetalle] (
    [IDTransaccion]          INT             NULL,
    [IDTransaccionOperacion] INT             NULL,
    [IDProducto]             INT             NULL,
    [CostoViejo]             DECIMAL (18, 3) NULL,
    [CostoNuevo]             DECIMAL (18, 3) NULL,
    [CantidadSaldo]          DECIMAL (18, 3) NULL
);


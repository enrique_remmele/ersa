﻿CREATE TABLE [dbo].[Ayudante] (
    [ID]             TINYINT       NOT NULL,
    [IDDistribuidor] TINYINT       NOT NULL,
    [Nombres]        VARCHAR (50)  NOT NULL,
    [NroDocumento]   VARCHAR (15)  NULL,
    [Telefono]       VARCHAR (20)  NULL,
    [Celular]        VARCHAR (20)  NULL,
    [Direccion]      VARCHAR (100) NULL,
    [Email]          VARCHAR (50)  NULL,
    CONSTRAINT [PK_Ayudante] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Ayudante_Distribuidor] FOREIGN KEY ([IDDistribuidor]) REFERENCES [dbo].[Distribuidor] ([ID])
);


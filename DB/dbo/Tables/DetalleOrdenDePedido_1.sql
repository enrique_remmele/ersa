﻿CREATE TABLE [dbo].[DetalleOrdenDePedido] (
    [IDTransaccion]     NUMERIC (18)    NOT NULL,
    [IDProducto]        INT             NOT NULL,
    [ID]                TINYINT         NOT NULL,
    [IDDeposito]        TINYINT         NOT NULL,
    [Observacion]       VARCHAR (50)    NOT NULL,
    [Existencia]        DECIMAL (10, 2) NOT NULL,
    [Pedido]            DECIMAL (10, 2) NOT NULL,
    [Extra]             DECIMAL (10, 2) NOT NULL,
    [Anulado]           BIT             CONSTRAINT [DF_DetalleOrdenDePedido_Anulado] DEFAULT ('False') NOT NULL,
    [FechaEntrega]      DATE            NULL,
    [CantidadAEntregar] DECIMAL (10, 2) NULL,
    CONSTRAINT [PK_DetalleOrdenDePedido] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleOrdenDePedido_OrdenDePedido] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[OrdenDePedido] ([IDTransaccion])
);


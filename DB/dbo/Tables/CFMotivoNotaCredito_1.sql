﻿CREATE TABLE [dbo].[CFMotivoNotaCredito] (
    [IDSubMotivoNotaCredito] INT NOT NULL,
    [IDTipoProducto]         INT NOT NULL,
    [IDCuentaContable]       INT NULL,
    CONSTRAINT [PK_CFMotivoNotaCredito] PRIMARY KEY CLUSTERED ([IDSubMotivoNotaCredito] ASC, [IDTipoProducto] ASC)
);


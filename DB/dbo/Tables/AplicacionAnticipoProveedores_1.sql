﻿CREATE TABLE [dbo].[AplicacionAnticipoProveedores] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [Numero]            INT           NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [NroComprobante]    VARCHAR (50)  NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDProveedor]       INT           NULL,
    [IDMoneda]          TINYINT       NULL,
    [Observacion]       VARCHAR (500) NULL,
    [TotalEgreso]       MONEY         NULL,
    [TotalOrdenPago]    MONEY         NULL,
    [Procesado]         BIT           CONSTRAINT [DF_AplicacionAnticipoProveedores_Cancelado] DEFAULT ('False') NOT NULL,
    [Anulado]           BIT           CONSTRAINT [DF_AplicacionAnticipoProveedores_Anulado] DEFAULT ('False') NOT NULL,
    [FechaAnulado]      DATE          NULL,
    [IDUsuarioAnulado]  SMALLINT      NULL,
    [IDMotivoAnulacion] INT           NULL,
    CONSTRAINT [PK_AplicacionAnticipoProveedores] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


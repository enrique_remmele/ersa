﻿CREATE TABLE [dbo].[NotaCreditoImportada] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [NroOperacion]  INT          NULL,
    [Total]         MONEY        NULL,
    [Operador]      VARCHAR (10) NULL,
    CONSTRAINT [PK_NotaCreditoImportada] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


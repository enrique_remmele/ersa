﻿CREATE TABLE [dbo].[GastoLogistica] (
    [IDTransaccion] INT           NULL,
    [IDCamion]      INT           NULL,
    [Importe]       MONEY         NULL,
    [Observacion]   VARCHAR (300) NULL
);


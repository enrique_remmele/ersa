﻿CREATE TABLE [dbo].[CFNotaCredito] (
    [IDOperacion]      TINYINT NOT NULL,
    [ID]               TINYINT NOT NULL,
    [Credito]          BIT     NULL,
    [Contado]          BIT     NULL,
    [BuscarEnCliente]  BIT     NULL,
    [BuscarEnProducto] BIT     NULL,
    [IDTipoCuentaFija] TINYINT NULL,
    [IDTipoDescuento]  TINYINT NULL,
    CONSTRAINT [PK_CFNotaCredito] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[Division] (
    [ID]          SMALLINT     NOT NULL,
    [IDProveedor] INT          NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_Division_Estado] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Division_Proveedor] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[Proveedor] ([ID])
);


﻿CREATE TABLE [dbo].[Vendedor] (
    [ID]           TINYINT       NOT NULL,
    [Nombres]      VARCHAR (50)  NOT NULL,
    [Apellidos]    VARCHAR (50)  NULL,
    [NroDocumento] VARCHAR (15)  NULL,
    [Telefono]     VARCHAR (20)  NULL,
    [Celular]      VARCHAR (20)  NULL,
    [Direccion]    VARCHAR (100) NULL,
    [Email]        VARCHAR (50)  NULL,
    [Estado]       BIT           NULL,
    [IDSucursal]   TINYINT       NULL,
    [Referencia]   VARCHAR (10)  NULL,
    [Resumen]      VARCHAR (50)  NULL,
    [IDDeposito]   TINYINT       NULL,
    CONSTRAINT [PK_Vendedor] PRIMARY KEY CLUSTERED ([ID] ASC)
);


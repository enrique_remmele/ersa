﻿CREATE TABLE [dbo].[aPorcentajeProductoPrecio] (
    [IDUsuario]             INT             NOT NULL,
    [IDPresentacion]        TINYINT         NOT NULL,
    [Porcentaje]            DECIMAL (18, 2) NOT NULL,
    [Importe]               MONEY           NULL,
    [Estado]                BIT             NULL,
    [IDUsuarioModificacion] INT             NOT NULL,
    [FechaModificacion]     DATETIME        NOT NULL,
    [Operacion]             VARCHAR (10)    NOT NULL
);


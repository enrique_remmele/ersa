﻿CREATE TABLE [dbo].[DetalleLoteDistribucion] (
    [IDTransaccion]     NUMERIC (18)    NOT NULL,
    [IDProducto]        INT             NOT NULL,
    [Cantidad]          DECIMAL (10, 2) NULL,
    [Total]             MONEY           CONSTRAINT [DF_DetalleLoteDistribucion_Total_1] DEFAULT ((0)) NULL,
    [TotalDescuento]    MONEY           CONSTRAINT [DF_DetalleLoteDistribucion_TotalDescuento_1] DEFAULT ((0)) NULL,
    [TotalDiscriminado] MONEY           NULL,
    [CantidadCaja]      DECIMAL (10, 2) NULL,
    CONSTRAINT [PK_DetalleLoteDistribucion_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC),
    CONSTRAINT [FK_DetalleLoteDistribucion_LoteDistribucion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[LoteDistribucion] ([IDTransaccion]) ON DELETE CASCADE
);


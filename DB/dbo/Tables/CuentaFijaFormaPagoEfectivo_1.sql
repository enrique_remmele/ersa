﻿CREATE TABLE [dbo].[CuentaFijaFormaPagoEfectivo] (
    [ID]                TINYINT      NOT NULL,
    [Descripcion]       VARCHAR (50) NOT NULL,
    [Orden]             TINYINT      NOT NULL,
    [IDTipoComprobante] SMALLINT     NOT NULL,
    [IDMoneda]          TINYINT      NOT NULL,
    [IDCuentaContable]  INT          NOT NULL,
    CONSTRAINT [PK_CuentaFijaCobranzaCreditoEfectivo] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CuentaFijaFormaPagoEfectivo_CuentaContable] FOREIGN KEY ([IDCuentaContable]) REFERENCES [dbo].[CuentaContable] ([ID]),
    CONSTRAINT [FK_CuentaFijaFormaPagoEfectivo_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_CuentaFijaFormaPagoEfectivo_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


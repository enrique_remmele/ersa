﻿CREATE TABLE [dbo].[CFCobranzaCheque] (
    [IDOperacion]      TINYINT NOT NULL,
    [ID]               TINYINT NOT NULL,
    [IDcuentaBancaria] TINYINT NULL,
    [Diferido]         BIT     NULL,
    CONSTRAINT [PK_CFCobranzaCheque] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


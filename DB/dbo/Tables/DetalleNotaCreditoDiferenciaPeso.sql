﻿CREATE TABLE [dbo].[DetalleNotaCreditoDiferenciaPeso] (
    [IDProducto]                     INT             NOT NULL,
    [IDCliente]                      INT             NULL,
    [IDListaPrecio]                  INT             NULL,
    [Cantidad]                       DECIMAL (18, 3) NULL,
    [ImporteDescuentoUnitario]       MONEY           NULL,
    [ImporteDescuento]               MONEY           NULL,
    [IDTransaccionPedidoNotaCredito] INT             NULL,
    [IDTransaccionVenta]             INT             NULL,
    [IDTransaccionNotaCredito]       INT             NULL,
    [AutomaticoPorExcepcion]         BIT             NULL
);


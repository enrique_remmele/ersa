﻿CREATE TABLE [dbo].[ExtractoMovimientoBancario] (
    [ID]                   TINYINT      NULL,
    [IDSucursal]           TINYINT      NULL,
    [IDCuentaBancaria]     TINYINT      NULL,
    [Fecha]                DATE         NULL,
    [Operacion]            VARCHAR (50) NULL,
    [CompCheque]           VARCHAR (50) NULL,
    [OrdenDetalleConcepto] VARCHAR (50) NULL,
    [Debito]               MONEY        NULL,
    [Credito]              MONEY        NULL,
    [Movimiento]           VARCHAR (50) NULL,
    [SaldoInicial]         MONEY        NULL,
    [SaldoAnterior]        MONEY        NULL,
    [Saldo]                MONEY        NULL
);


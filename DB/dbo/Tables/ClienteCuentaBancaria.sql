﻿CREATE TABLE [dbo].[ClienteCuentaBancaria] (
    [ID]             INT          NOT NULL,
    [IDCliente]      INT          NOT NULL,
    [IDBanco]        TINYINT      NOT NULL,
    [CuentaBancaria] VARCHAR (50) NOT NULL,
    [IDMoneda]       TINYINT      NOT NULL,
    CONSTRAINT [PK_ClienteCuentaBancaria] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ClienteCuentaBancaria_Banco] FOREIGN KEY ([IDBanco]) REFERENCES [dbo].[Banco] ([ID]),
    CONSTRAINT [FK_ClienteCuentaBancaria_Cliente] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[Cliente] ([ID]),
    CONSTRAINT [FK_ClienteCuentaBancaria_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID])
);


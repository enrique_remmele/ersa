﻿CREATE TABLE [dbo].[AsignacionCamion] (
    [IDTransaccion]       NUMERIC (18)  NOT NULL,
    [Numero]              INT           NOT NULL,
    [Fecha]               DATE          NOT NULL,
    [IDTipoComprobante]   SMALLINT      NOT NULL,
    [NroComprobante]      VARCHAR (50)  NULL,
    [Observacion]         VARCHAR (200) NULL,
    [Anulado]             BIT           NOT NULL,
    [IDCamion]            INT           NOT NULL,
    [IDChofer]            TINYINT       NOT NULL,
    [IDSucursal]          TINYINT       NOT NULL,
    [IDSucursalAbastecer] TINYINT       NOT NULL,
    [PedidoCliente]       BIT           NULL,
    CONSTRAINT [PK_AsignacionCamion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_AsignacionCamion_Camion] FOREIGN KEY ([IDCamion]) REFERENCES [dbo].[Camion] ([ID]),
    CONSTRAINT [FK_AsignacionCamion_Chofer] FOREIGN KEY ([IDChofer]) REFERENCES [dbo].[Chofer] ([ID]),
    CONSTRAINT [FK_AsignacionCamion_IDCamion] FOREIGN KEY ([IDCamion]) REFERENCES [dbo].[Camion] ([ID]),
    CONSTRAINT [FK_AsignacionCamion_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID]),
    CONSTRAINT [FK_AsignacionCamion_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


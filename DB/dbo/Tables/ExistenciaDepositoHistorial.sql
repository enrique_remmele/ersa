﻿CREATE TABLE [dbo].[ExistenciaDepositoHistorial] (
    [IDDeposito]       TINYINT         NULL,
    [IDProducto]       INT             NULL,
    [ExistenciaActual] DECIMAL (10, 2) NULL,
    [Signo]            CHAR (1)        NULL,
    [Cantidad]         DECIMAL (10, 2) NULL,
    [ExistenciaNueva]  DECIMAL (10, 2) NULL,
    [CantidadGuardada] DECIMAL (10, 2) NULL,
    [Fecha]            DATETIME        NULL
);


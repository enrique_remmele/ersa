﻿CREATE TABLE [dbo].[CompraLogistica] (
    [IDTransaccion] INT             NULL,
    [IDCamion]      INT             NULL,
    [IDProducto]    INT             NULL,
    [Cantidad]      DECIMAL (10, 2) NULL
);


﻿CREATE TABLE [dbo].[ActividadHistorialDetalle] (
    [ID]                  NUMERIC (18) NOT NULL,
    [Fecha]               DATETIME     NULL,
    [IDActividad]         INT          NULL,
    [IDTransaccion]       NUMERIC (18) NULL,
    [IDProducto]          INT          NULL,
    [IDDescuento]         INT          NULL,
    [Descuento]           MONEY        NULL,
    [IDTipoDescuento]     INT          NULL,
    [Total]               MONEY        NULL,
    [Saldo]               MONEY        NULL,
    [PorcentajeUtilizado] INT          NULL,
    [Excedente]           MONEY        NULL,
    CONSTRAINT [PK_ActividadHistorialDetalle] PRIMARY KEY CLUSTERED ([ID] ASC)
);


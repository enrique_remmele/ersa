﻿CREATE TABLE [dbo].[CFVenta] (
    [IDOperacion]      TINYINT NOT NULL,
    [ID]               TINYINT NOT NULL,
    [Credito]          BIT     NULL,
    [Contado]          BIT     NULL,
    [BuscarEnCliente]  BIT     NULL,
    [BuscarEnProducto] BIT     NULL,
    [IDTipoCuentaFija] TINYINT NULL,
    [IDTipoDescuento]  TINYINT NULL,
    CONSTRAINT [PK_CFVenta] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


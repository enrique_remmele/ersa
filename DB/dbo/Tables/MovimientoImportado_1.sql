﻿CREATE TABLE [dbo].[MovimientoImportado] (
    [IDTransaccion]   NUMERIC (18) NOT NULL,
    [NroOperacion]    VARCHAR (50) NOT NULL,
    [TipoComprobante] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_MovimientoImportado] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


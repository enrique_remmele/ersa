﻿CREATE TABLE [dbo].[ChequeClienteCobranzaContado] (
    [IDTransaccionCheque]   NUMERIC (18) NOT NULL,
    [IDTransaccionVenta]    NUMERIC (18) NOT NULL,
    [IDTransaccionCObranza] NUMERIC (18) NULL,
    CONSTRAINT [PK_ChequeClienteCobranzaContado] PRIMARY KEY CLUSTERED ([IDTransaccionCheque] ASC, [IDTransaccionVenta] ASC),
    CONSTRAINT [FK_ChequeClienteCobranzaContado_ChequeCliente] FOREIGN KEY ([IDTransaccionCheque]) REFERENCES [dbo].[ChequeCliente] ([IDTransaccion]),
    CONSTRAINT [FK_ChequeClienteCobranzaContado_CobranzaContado] FOREIGN KEY ([IDTransaccionCObranza]) REFERENCES [dbo].[CobranzaContado] ([IDTransaccion]),
    CONSTRAINT [FK_ChequeClienteCobranzaContado_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


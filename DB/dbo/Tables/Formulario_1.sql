﻿CREATE TABLE [dbo].[Formulario] (
    [IDFormulario]    TINYINT      NOT NULL,
    [IDImpresion]     TINYINT      NOT NULL,
    [Descripcion]     VARCHAR (20) NOT NULL,
    [Detalle]         BIT          NULL,
    [CantidadDetalle] TINYINT      NULL,
    [Copias]          TINYINT      NULL,
    [Fondo]           IMAGE        NULL,
    [TamañoFondoX]    SMALLINT     NULL,
    [TamañoFondoY]    SMALLINT     NULL,
    [TamañoPapel]     VARCHAR (50) CONSTRAINT [DF_Formulario_TamañoPapel] DEFAULT ('A4') NULL,
    [TamañoPapelX]    SMALLINT     CONSTRAINT [DF_Formulario_TamañoPapelX] DEFAULT ((0)) NULL,
    [TamañoPapelY]    SMALLINT     CONSTRAINT [DF_Formulario_TamañoPapelY] DEFAULT ((0)) NULL,
    [Horizontal]      BIT          CONSTRAINT [DF_Formulario_Horizontal] DEFAULT ('False') NULL,
    [MargenIzquierdo] TINYINT      CONSTRAINT [DF_Formulario_MargenIzquierdo] DEFAULT ((0)) NULL,
    [MargenDerecho]   TINYINT      CONSTRAINT [DF_Formulario_MargenDerecho] DEFAULT ((0)) NULL,
    [MargenArriba]    TINYINT      CONSTRAINT [DF_Formulario_MargenArriba] DEFAULT ((0)) NULL,
    [MargenAbajo]     TINYINT      CONSTRAINT [DF_Formulario_MargenAbajo] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_Formulario] PRIMARY KEY CLUSTERED ([IDFormulario] ASC)
);


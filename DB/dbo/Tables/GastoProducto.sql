﻿CREATE TABLE [dbo].[GastoProducto] (
    [IDTransaccionGasto] NUMERIC (18)  NOT NULL,
    [NroAcuerdo]         INT           NOT NULL,
    [Cantidad]           MONEY         NULL,
    [Observacion]        VARCHAR (100) NULL,
    CONSTRAINT [PK_GastoProducto] PRIMARY KEY CLUSTERED ([IDTransaccionGasto] ASC, [NroAcuerdo] ASC),
    CONSTRAINT [FK_GastoProducto_Acuerdo] FOREIGN KEY ([NroAcuerdo]) REFERENCES [dbo].[Acuerdo] ([Numero]) NOT FOR REPLICATION,
    CONSTRAINT [FK_GastoProducto_Gasto] FOREIGN KEY ([IDTransaccionGasto]) REFERENCES [dbo].[Gasto] ([IDTransaccion]) NOT FOR REPLICATION
);


GO
ALTER TABLE [dbo].[GastoProducto] NOCHECK CONSTRAINT [FK_GastoProducto_Acuerdo];


GO
ALTER TABLE [dbo].[GastoProducto] NOCHECK CONSTRAINT [FK_GastoProducto_Gasto];


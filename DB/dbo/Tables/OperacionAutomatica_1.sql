﻿CREATE TABLE [dbo].[OperacionAutomatica] (
    [ID]                    INT           NULL,
    [DB]                    VARCHAR (50)  NULL,
    [Titulo]                VARCHAR (50)  NULL,
    [Descripcion]           VARCHAR (400) NULL,
    [IDTipoOperacion]       INT           NULL,
    [HoraOperacion]         TIME (7)      NULL,
    [Recurrencia]           VARCHAR (50)  NULL,
    [FechaOperacion]        DATE          NULL,
    [DiaSemana]             TINYINT       NULL,
    [DiaMes]                TINYINT       NULL,
    [IDTipoRecalculoKardex] INT           NULL,
    [IDTipoProducto]        INT           NULL,
    [IDProducto]            INT           NULL,
    [FechaInicioKardex]     DATE          NULL,
    [SQL]                   VARCHAR (MAX) NULL,
    [Activo]                BIT           NULL,
    [Ejecutando]            BIT           NULL
);


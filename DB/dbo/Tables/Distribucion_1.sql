﻿CREATE TABLE [dbo].[Distribucion] (
    [IDTransaccion]  NUMERIC (18)  NOT NULL,
    [Numero]         INT           NOT NULL,
    [Fecha]          DATETIME      NOT NULL,
    [FechaReparto]   DATETIME      NOT NULL,
    [IDSucursal]     TINYINT       NOT NULL,
    [IDDistribuidor] TINYINT       NOT NULL,
    [IDCamion]       INT           NOT NULL,
    [IDChofer]       TINYINT       NULL,
    [IDAyudante]     TINYINT       NULL,
    [Anulado]        BIT           NULL,
    [Observacion]    VARCHAR (100) NULL,
    CONSTRAINT [PK_Distribucion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_Distribucion_Ayudante] FOREIGN KEY ([IDAyudante]) REFERENCES [dbo].[Ayudante] ([ID]),
    CONSTRAINT [FK_Distribucion_Camion] FOREIGN KEY ([IDCamion]) REFERENCES [dbo].[Camion] ([ID]),
    CONSTRAINT [FK_Distribucion_Chofer] FOREIGN KEY ([IDChofer]) REFERENCES [dbo].[Chofer] ([ID]),
    CONSTRAINT [FK_Distribucion_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_Distribucion_Transaccion] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[Transaccion] ([ID])
);


﻿CREATE TABLE [dbo].[DistribucionSaldo] (
    [ID]                 INT           NOT NULL,
    [IDTransaccionVenta] NUMERIC (18)  NOT NULL,
    [RazonSocial]        VARCHAR (100) NOT NULL,
    [Fecha1]             MONEY         NOT NULL,
    [Fecha2]             MONEY         NOT NULL,
    [Fecha3]             MONEY         NOT NULL,
    [Fecha4]             MONEY         NOT NULL,
    [Fecha5]             MONEY         NOT NULL,
    [Fecha6]             MONEY         NOT NULL,
    [Fecha7]             MONEY         NOT NULL,
    [Fecha8]             MONEY         NOT NULL,
    [Fecha9]             MONEY         NOT NULL,
    [Fecha10]            MONEY         NOT NULL,
    [Total]              MONEY         NOT NULL,
    CONSTRAINT [PK_DistribucionSaldo_1] PRIMARY KEY CLUSTERED ([ID] ASC, [IDTransaccionVenta] ASC)
);


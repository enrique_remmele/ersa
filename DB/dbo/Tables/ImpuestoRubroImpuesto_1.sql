﻿CREATE TABLE [dbo].[ImpuestoRubroImpuesto] (
    [IDOperacion]     INT      NOT NULL,
    [IDRubroImpuesto] SMALLINT NOT NULL,
    [IDImpuesto]      TINYINT  NOT NULL,
    CONSTRAINT [PK_ImpuestoRubroImpuesto] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [IDRubroImpuesto] ASC, [IDImpuesto] ASC),
    CONSTRAINT [FK_ImpuestoRubroImpuesto_Impuesto] FOREIGN KEY ([IDImpuesto]) REFERENCES [dbo].[Impuesto] ([ID]),
    CONSTRAINT [FK_ImpuestoRubroImpuesto_Operacion] FOREIGN KEY ([IDOperacion]) REFERENCES [dbo].[Operacion] ([ID]),
    CONSTRAINT [FK_ImpuestoRubroImpuesto_RubroImpuesto] FOREIGN KEY ([IDRubroImpuesto]) REFERENCES [dbo].[RubroImpuesto] ([ID])
);


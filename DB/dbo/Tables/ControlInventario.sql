﻿CREATE TABLE [dbo].[ControlInventario] (
    [IDTransaccion]      NUMERIC (18)  NOT NULL,
    [IDSucursal]         TINYINT       NOT NULL,
    [IDDeposito]         SMALLINT      NOT NULL,
    [Numero]             INT           NOT NULL,
    [Fecha]              DATE          NOT NULL,
    [Observacion]        VARCHAR (200) NULL,
    [Total]              MONEY         NOT NULL,
    [TotalImpuesto]      MONEY         NOT NULL,
    [TotalDiscriminado]  MONEY         NOT NULL,
    [Anulado]            BIT           NOT NULL,
    [FechaAnulado]       DATETIME      NULL,
    [IDUsuarioAnulado]   SMALLINT      NULL,
    [Procesado]          BIT           CONSTRAINT [DF_ControlInventario_Procesado] DEFAULT ('False') NULL,
    [ControlEquipo]      BIT           CONSTRAINT [DF_ControlInventario_ControlEquipo] DEFAULT ('False') NULL,
    [ControlSistema]     BIT           CONSTRAINT [DF_ControlInventario_ControlSistema] DEFAULT ('False') NULL,
    [IDUsuarioProcesado] SMALLINT      NULL,
    [FechaProcesado]     DATE          NULL,
    [IDUsuarioAjuste]    SMALLINT      NULL,
    [FechaAjuste]        DATE          NULL,
    CONSTRAINT [PK_ControlInventario] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


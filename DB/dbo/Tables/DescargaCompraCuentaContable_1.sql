﻿CREATE TABLE [dbo].[DescargaCompraCuentaContable] (
    [IDCuentaContable] INT NOT NULL,
    [Estado]           BIT CONSTRAINT [DF_DescargaCompraCuentaContable_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_DescargaCompraCuentaContable] PRIMARY KEY CLUSTERED ([IDCuentaContable] ASC)
);


﻿CREATE TABLE [dbo].[DetalleCierreAperturaStock] (
    [IDTransaccion] NUMERIC (18)    NOT NULL,
    [IDProducto]    INT             NOT NULL,
    [IDSucursal]    TINYINT         NULL,
    [IDDeposito]    TINYINT         NULL,
    [Existencia]    DECIMAL (18, 2) NULL,
    [Costo]         MONEY           NULL,
    [Total]         AS              ([Existencia]*[Costo]),
    CONSTRAINT [PK_DetalleCierreAperturaStock] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_IDTransaccion]
    ON [dbo].[DetalleCierreAperturaStock]([IDTransaccion] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDProducto]
    ON [dbo].[DetalleCierreAperturaStock]([IDProducto] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDSucursal]
    ON [dbo].[DetalleCierreAperturaStock]([IDSucursal] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDDeposito]
    ON [dbo].[DetalleCierreAperturaStock]([IDDeposito] ASC);


﻿CREATE TABLE [dbo].[Calendario] (
    [Fecha]  DATE          NOT NULL,
    [Habil]  BIT           NOT NULL,
    [Motivo] VARCHAR (200) NULL,
    CONSTRAINT [PK_Calendario] PRIMARY KEY CLUSTERED ([Fecha] ASC)
);


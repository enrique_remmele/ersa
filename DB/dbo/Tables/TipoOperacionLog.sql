﻿CREATE TABLE [dbo].[TipoOperacionLog] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Codigo]      VARCHAR (50) NULL,
    CONSTRAINT [PK_TipoLogSuceso] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[AnticipoAplicacion] (
    [IDTransaccion]         NUMERIC (18)  NOT NULL,
    [IDTransaccionCobranza] NUMERIC (18)  NOT NULL,
    [Numero]                INT           NOT NULL,
    [IDTipoComprobante]     SMALLINT      NOT NULL,
    [NroComprobante]        VARCHAR (100) NOT NULL,
    [IDSucursal]            TINYINT       NOT NULL,
    [Fecha]                 DATE          NOT NULL,
    [IDMoneda]              TINYINT       NOT NULL,
    [Cotizacion]            MONEY         NOT NULL,
    [Anulado]               BIT           CONSTRAINT [DF_AnticipoAplicacion_Anulado] DEFAULT ('False') NULL,
    [FechaAnulado]          DATETIME      NULL,
    [IDUsuarioAnulado]      SMALLINT      NULL,
    [Observacion]           VARCHAR (200) NULL,
    [Total]                 MONEY         NULL,
    [DiferenciaCambio]      MONEY         NULL,
    [Procesado]             BIT           NULL,
    CONSTRAINT [PK_AnticipoAplicacion_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200324-094229]
    ON [dbo].[AnticipoAplicacion]([IDTransaccionCobranza] ASC);


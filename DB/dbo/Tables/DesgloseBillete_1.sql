﻿CREATE TABLE [dbo].[DesgloseBillete] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [ID]            TINYINT      NOT NULL,
    [IDBillete]     TINYINT      NOT NULL,
    [Cantidad]      SMALLINT     NOT NULL,
    [Total]         MONEY        NOT NULL,
    CONSTRAINT [PK_DesgloseBillete] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_DesgloseBillete_Billete] FOREIGN KEY ([IDBillete]) REFERENCES [dbo].[Billete] ([ID]),
    CONSTRAINT [FK_DesgloseBillete_RendicionLote] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[RendicionLote] ([IDTransaccion])
);


﻿CREATE TABLE [dbo].[ProductoListaPrecioExcepciones] (
    [IDProducto]          INT             NOT NULL,
    [IDListaPrecio]       INT             NOT NULL,
    [IDCliente]           INT             NOT NULL,
    [IDSucursal]          TINYINT         NOT NULL,
    [IDTipoDescuento]     TINYINT         NOT NULL,
    [IDMoneda]            TINYINT         NOT NULL,
    [Descuento]           MONEY           CONSTRAINT [DF_ProductoListaPrecioExcepciones_Descuento] DEFAULT ((0)) NOT NULL,
    [Porcentaje]          DECIMAL (9, 6)  CONSTRAINT [DF_ProductoListaPrecioExcepciones_Porcentaje] DEFAULT ((0)) NOT NULL,
    [Desde]               DATE            NOT NULL,
    [Hasta]               DATE            NOT NULL,
    [CantidadLimite]      DECIMAL (18, 3) NULL,
    [CantidadLimiteSaldo] DECIMAL (18, 3) NULL,
    [UnicaVez]            BIT             NULL,
    [IDTransaccionPedido] INT             NOT NULL,
    CONSTRAINT [PK_ProductoListaPrecioExcepciones] PRIMARY KEY CLUSTERED ([IDProducto] ASC, [IDListaPrecio] ASC, [IDCliente] ASC, [IDSucursal] ASC, [IDTipoDescuento] ASC, [IDMoneda] ASC, [Desde] ASC, [Hasta] ASC, [IDTransaccionPedido] ASC),
    CONSTRAINT [FK_ProductoListaPrecioExcepciones_Cliente] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[Cliente] ([ID]),
    CONSTRAINT [FK_ProductoListaPrecioExcepciones_ListaPrecio] FOREIGN KEY ([IDListaPrecio]) REFERENCES [dbo].[ListaPrecio] ([ID]),
    CONSTRAINT [FK_ProductoListaPrecioExcepciones_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_ProductoListaPrecioExcepciones_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID]),
    CONSTRAINT [FK_ProductoListaPrecioExcepciones_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


﻿CREATE TABLE [dbo].[LimiteDescuentoUsuario] (
    [IDUsuario]     INT             NOT NULL,
    [IDListaPrecio] INT             NOT NULL,
    [Porcentaje]    DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_LimiteDescuentoUsuario] PRIMARY KEY CLUSTERED ([IDUsuario] ASC, [IDListaPrecio] ASC)
);


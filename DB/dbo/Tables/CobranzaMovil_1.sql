﻿CREATE TABLE [dbo].[CobranzaMovil] (
    [IDTransaccion]       NUMERIC (18) NOT NULL,
    [IDSucursal]          TINYINT      NOT NULL,
    [IDDeposito]          TINYINT      NOT NULL,
    [IDTransaccionOrigen] NUMERIC (18) NOT NULL,
    [IDTerminal]          INT          NULL,
    [Fecha]               DATETIME     NOT NULL,
    [IDUsuario]           INT          NOT NULL,
    [NumeroOrigen]        INT          NULL,
    [IDTipoComprobante]   INT          NULL,
    [Comprobante]         VARCHAR (50) NULL,
    [IDCliente]           INT          NULL,
    [FechaEmision]        DATE         NULL,
    [Total]               MONEY        NULL,
    CONSTRAINT [PK_CobranzaMovil] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDSucursal] ASC, [IDDeposito] ASC)
);


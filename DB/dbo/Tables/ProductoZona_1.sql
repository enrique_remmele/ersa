﻿CREATE TABLE [dbo].[ProductoZona] (
    [IDSucursal]     TINYINT  NOT NULL,
    [IDDeposito]     SMALLINT NOT NULL,
    [IDZonaDeposito] INT      NOT NULL,
    [IDProducto]     INT      NOT NULL,
    CONSTRAINT [PK_ProductoZona] PRIMARY KEY CLUSTERED ([IDProducto] ASC, [IDSucursal] ASC, [IDDeposito] ASC, [IDZonaDeposito] ASC),
    CONSTRAINT [FK_ProductoZona_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ProductoZona_ZonaDeposito] FOREIGN KEY ([IDZonaDeposito]) REFERENCES [dbo].[ZonaDeposito] ([ID])
);


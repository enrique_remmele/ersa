﻿CREATE TABLE [dbo].[ChequeClienteMovil] (
    [IDTransaccion]       NUMERIC (18) NOT NULL,
    [IDSucursal]          TINYINT      NOT NULL,
    [IDDeposito]          TINYINT      NOT NULL,
    [IDTransaccionOrigen] NUMERIC (18) NOT NULL,
    [Fecha]               DATETIME     NOT NULL,
    [IDUsuario]           INT          NOT NULL,
    [IDTerminal]          INT          NULL,
    [Numero]              INT          NULL,
    [IDCliente]           INT          NULL,
    [IDBanco]             INT          NULL,
    [FechaCheque]         DATE         NULL,
    [NroCheque]           VARCHAR (50) NULL,
    [Importe]             MONEY        NULL,
    CONSTRAINT [PK_ChequeClienteMovil] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDSucursal] ASC, [IDDeposito] ASC)
);


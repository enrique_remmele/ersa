﻿CREATE TABLE [dbo].[DetallePagoChequeCliente] (
    [IDTransaccionChequeCliente]     NUMERIC (18) NOT NULL,
    [IDTransaccionPagoChequeCliente] NUMERIC (18) NOT NULL,
    [Importe]                        MONEY        CONSTRAINT [DF_DetallePagoChequeCliente_Importe] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DetallePagoChequeCliente] PRIMARY KEY CLUSTERED ([IDTransaccionChequeCliente] ASC, [IDTransaccionPagoChequeCliente] ASC),
    CONSTRAINT [FK_DetallePagoChequeCliente_ChequeCliente] FOREIGN KEY ([IDTransaccionChequeCliente]) REFERENCES [dbo].[ChequeCliente] ([IDTransaccion]),
    CONSTRAINT [FK_DetallePagoChequeCliente_PagoChequeCliente] FOREIGN KEY ([IDTransaccionPagoChequeCliente]) REFERENCES [dbo].[PagoChequeCliente] ([IDTransaccion])
);


﻿CREATE TABLE [dbo].[Pais] (
    [ID]           TINYINT      NOT NULL,
    [Descripcion]  VARCHAR (50) NOT NULL,
    [Nacionalidad] VARCHAR (50) NULL,
    [Orden]        SMALLINT     CONSTRAINT [DF_Pais_Orden] DEFAULT ((0)) NULL,
    [Estado]       BIT          CONSTRAINT [DF_Pais_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_Pais] PRIMARY KEY CLUSTERED ([ID] ASC)
);


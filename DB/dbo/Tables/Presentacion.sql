﻿CREATE TABLE [dbo].[Presentacion] (
    [ID]          SMALLINT     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_Presentacion_Estado] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_Presentacion] PRIMARY KEY CLUSTERED ([ID] ASC)
);


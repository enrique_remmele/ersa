﻿CREATE TABLE [dbo].[OrdenDePedido] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [Numero]            INT           NOT NULL,
    [Fecha]             DATE          NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [NroComprobante]    VARCHAR (50)  NULL,
    [IDDeposito]        TINYINT       NULL,
    [Observacion]       VARCHAR (200) NULL,
    [Autorizacion]      VARCHAR (50)  NOT NULL,
    [Anulado]           BIT           NOT NULL,
    [FechaDesde]        DATE          NOT NULL,
    [FechaHasta]        DATE          NOT NULL,
    [PedidoCliente]     BIT           NULL,
    CONSTRAINT [PK_OrdenDePedido] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_OrdenDePedido_Deposito] FOREIGN KEY ([IDDeposito]) REFERENCES [dbo].[Deposito] ([ID]),
    CONSTRAINT [FK_OrdenDePedido_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


﻿CREATE TABLE [dbo].[CFCobranzaEfectivo] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    CONSTRAINT [PK_CFCobranzaEfectivo] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


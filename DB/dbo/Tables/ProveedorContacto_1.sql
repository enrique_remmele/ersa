﻿CREATE TABLE [dbo].[ProveedorContacto] (
    [IDProveedor] INT          NOT NULL,
    [ID]          TINYINT      NOT NULL,
    [Nombres]     VARCHAR (50) NOT NULL,
    [Cargo]       VARCHAR (50) NULL,
    [Telefono]    VARCHAR (50) NULL,
    [Email]       VARCHAR (50) NULL,
    CONSTRAINT [PK_ProveedorContacto] PRIMARY KEY CLUSTERED ([IDProveedor] ASC)
);


﻿CREATE TABLE [dbo].[TipoProducto] (
    [ID]                      TINYINT      NOT NULL,
    [Descripcion]             VARCHAR (50) NOT NULL,
    [Estado]                  BIT          CONSTRAINT [DF_TipoProducto_Estado] DEFAULT ('True') NOT NULL,
    [Referencia]              VARCHAR (50) NULL,
    [CuentaContableVenta]     VARCHAR (50) NULL,
    [CuentaContableCliente]   VARCHAR (50) NULL,
    [CuentaContableCosto]     VARCHAR (50) NULL,
    [Producido]               BIT          NULL,
    [CuentaContableProveedor] VARCHAR (50) NULL,
    CONSTRAINT [PK_TipoProducto] PRIMARY KEY CLUSTERED ([ID] ASC)
);


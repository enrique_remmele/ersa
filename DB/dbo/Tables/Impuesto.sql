﻿CREATE TABLE [dbo].[Impuesto] (
    [ID]                 TINYINT         NOT NULL,
    [Descripcion]        VARCHAR (10)    NOT NULL,
    [FactorDiscriminado] DECIMAL (10, 7) NULL,
    [FactorImpuesto]     DECIMAL (10, 7) NULL,
    [Exento]             BIT             NULL,
    [Referencia]         VARCHAR (5)     NULL,
    [RetencionIVA]       MONEY           NULL,
    [RetencionRenta]     MONEY           NULL,
    CONSTRAINT [PK_Impuesto] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[ControlSaldosFF] (
    [IDSucursal]      TINYINT      NOT NULL,
    [IDGrupo]         TINYINT      NOT NULL,
    [Sucursal]        VARCHAR (50) NULL,
    [Grupo]           VARCHAR (50) NULL,
    [Fecha]           DATE         NOT NULL,
    [Tope]            MONEY        NULL,
    [SaldoGasto]      MONEY        NULL,
    [TotalGasto]      MONEY        NULL,
    [Codigo]          VARCHAR (50) NULL,
    [Debito]          MONEY        NULL,
    [Credito]         MONEY        NULL,
    [SaldoLibroMayor] MONEY        NULL,
    [Control]         VARCHAR (50) NULL,
    CONSTRAINT [PK_ControlSaldosFF] PRIMARY KEY CLUSTERED ([IDSucursal] ASC, [IDGrupo] ASC, [Fecha] ASC),
    CONSTRAINT [FK_ControlSaldosFF_Grupo] FOREIGN KEY ([IDGrupo]) REFERENCES [dbo].[Grupo] ([ID])
);


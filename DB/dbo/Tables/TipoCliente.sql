﻿CREATE TABLE [dbo].[TipoCliente] (
    [ID]                                    TINYINT      NOT NULL,
    [Descripcion]                           VARCHAR (50) NOT NULL,
    [Estado]                                BIT          NULL,
    [Referencia]                            VARCHAR (10) NULL,
    [ToleranciaDiasCobranzaPostVencimiento] INT          NULL,
    CONSTRAINT [PK_TipoCliente] PRIMARY KEY CLUSTERED ([ID] ASC)
);


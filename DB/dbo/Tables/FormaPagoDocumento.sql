﻿CREATE TABLE [dbo].[FormaPagoDocumento] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [ID]                SMALLINT      NOT NULL,
    [IDSucursal]        TINYINT       NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [Fecha]             DATETIME      NOT NULL,
    [IDMoneda]          TINYINT       NOT NULL,
    [ImporteMoneda]     MONEY         NOT NULL,
    [Cotizacion]        MONEY         NOT NULL,
    [Total]             MONEY         NULL,
    [Saldo]             MONEY         NULL,
    [Observacion]       VARCHAR (100) NULL,
    [Cancelado]         BIT           NULL,
    [IDBanco]           INT           NULL,
    CONSTRAINT [PK_FormaPagoDocumento] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_FormaPagoDocumento_FormaPago] FOREIGN KEY ([ID], [IDTransaccion]) REFERENCES [dbo].[FormaPago] ([ID], [IDTransaccion]),
    CONSTRAINT [FK_FormaPagoDocumento_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_416_415]
    ON [dbo].[FormaPagoDocumento]([IDTipoComprobante] ASC)
    INCLUDE([IDTransaccion], [ID], [IDSucursal], [Comprobante], [Fecha], [IDMoneda], [ImporteMoneda], [Cotizacion], [Total], [Saldo], [Observacion], [Cancelado], [IDBanco]);


GO
CREATE TRIGGER [dbo].[TIQuitarPuntuacionesFPD] ON  [dbo].[FormaPagoDocumento]
   AFTER INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;
	/*JGR 20140808 Elimina las puntuaciones en el comprobante*/
    UPDATE FormaPagoDocumento
	SET 
      FormaPagoDocumento.Comprobante = LTRIM(RTRIM(REPLACE(FormaPagoDocumento.Comprobante, '.', '')))
	FROM FormaPagoDocumento JOIN INSERTED ON FormaPagoDocumento.IDTransaccion = INSERTED.IDTransaccion
END

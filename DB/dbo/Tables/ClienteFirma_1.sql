﻿CREATE TABLE [dbo].[ClienteFirma] (
    [IDCliente] INT     NOT NULL,
    [ID]        TINYINT NOT NULL,
    [Firma]     IMAGE   NULL,
    CONSTRAINT [PK_ClienteFirma] PRIMARY KEY CLUSTERED ([IDCliente] ASC, [ID] ASC)
);


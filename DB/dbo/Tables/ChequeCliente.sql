﻿CREATE TABLE [dbo].[ChequeCliente] (
    [IDTransaccion]    NUMERIC (18)  NOT NULL,
    [IDSucursal]       TINYINT       NOT NULL,
    [Numero]           INT           NOT NULL,
    [IDCliente]        INT           NOT NULL,
    [IDBanco]          TINYINT       NOT NULL,
    [IDCuentaBancaria] INT           NULL,
    [Fecha]            DATE          NOT NULL,
    [NroCheque]        VARCHAR (50)  NOT NULL,
    [IDMoneda]         TINYINT       NOT NULL,
    [Cotizacion]       MONEY         CONSTRAINT [DF_ChequeCliente_Cotizacion] DEFAULT ((1)) NOT NULL,
    [ImporteMoneda]    MONEY         NULL,
    [Importe]          MONEY         CONSTRAINT [DF_ChequeCliente_Importe] DEFAULT ((0)) NOT NULL,
    [Diferido]         BIT           CONSTRAINT [DF_ChequeCliente_Diferido] DEFAULT ('False') NOT NULL,
    [FechaVencimiento] DATE          NULL,
    [ChequeTercero]    BIT           NULL,
    [Librador]         VARCHAR (50)  NULL,
    [Saldo]            MONEY         CONSTRAINT [DF_ChequeCliente_Saldo] DEFAULT ((0)) NOT NULL,
    [Cancelado]        BIT           CONSTRAINT [DF_ChequeCliente_Cancelado] DEFAULT ('False') NOT NULL,
    [Cartera]          BIT           CONSTRAINT [DF_ChequeCliente_Cartera] DEFAULT ('False') NOT NULL,
    [Depositado]       BIT           CONSTRAINT [DF_ChequeCliente_Depositado] DEFAULT ('False') NOT NULL,
    [Rechazado]        BIT           CONSTRAINT [DF_ChequeCliente_Rechazado] DEFAULT ('False') NOT NULL,
    [Conciliado]       BIT           CONSTRAINT [DF_ChequeCliente_Conciliado] DEFAULT ('False') NULL,
    [IDMotivoRechazo]  TINYINT       NULL,
    [SaldoACuenta]     MONEY         NULL,
    [Titular]          VARCHAR (150) NULL,
    [FechaCobranza]    DATE          NULL,
    [Descontado]       BIT           CONSTRAINT [DF__ChequeCli__Desco__035D321D] DEFAULT ('FAlse') NULL,
    CONSTRAINT [PK_ChequeCliente] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_ChequeCliente_Banco] FOREIGN KEY ([IDBanco]) REFERENCES [dbo].[Banco] ([ID]),
    CONSTRAINT [FK_ChequeCliente_Cliente] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[Cliente] ([ID]),
    CONSTRAINT [FK_ChequeCliente_ClienteCuentaBancaria] FOREIGN KEY ([IDCuentaBancaria]) REFERENCES [dbo].[ClienteCuentaBancaria] ([ID]),
    CONSTRAINT [FK_ChequeCliente_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_ChequeCliente_MotivoRechazoCheque] FOREIGN KEY ([IDMotivoRechazo]) REFERENCES [dbo].[MotivoRechazoCheque] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160201-103712]
    ON [dbo].[ChequeCliente]([Numero] ASC, [IDCliente] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_93_92]
    ON [dbo].[ChequeCliente]([IDCliente] ASC, [Diferido] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_334_333]
    ON [dbo].[ChequeCliente]([IDCliente] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1938_1937]
    ON [dbo].[ChequeCliente]([IDSucursal] ASC, [IDMoneda] ASC)
    INCLUDE([Numero], [IDCliente], [IDBanco]);


GO
CREATE NONCLUSTERED INDEX [missing_index_1940_1939]
    ON [dbo].[ChequeCliente]([IDSucursal] ASC)
    INCLUDE([Numero], [IDCliente], [IDBanco], [IDMoneda]);


GO
CREATE NONCLUSTERED INDEX [missing_index_4059_4058]
    ON [dbo].[ChequeCliente]([Depositado] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Numero], [IDCliente], [IDBanco], [NroCheque], [IDMoneda], [Importe], [Diferido], [Cartera], [Rechazado], [Conciliado], [SaldoACuenta], [Descontado]);


GO
CREATE NONCLUSTERED INDEX [missing_index_703_702]
    ON [dbo].[ChequeCliente]([IDCliente] ASC, [Rechazado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_2176_2175]
    ON [dbo].[ChequeCliente]([IDCliente] ASC, [Diferido] ASC, [FechaVencimiento] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_199926_199925]
    ON [dbo].[ChequeCliente]([NroCheque] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_1952_1951]
    ON [dbo].[ChequeCliente]([IDCliente] ASC, [IDBanco] ASC, [NroCheque] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_5675_5674]
    ON [dbo].[ChequeCliente]([IDSucursal] ASC)
    INCLUDE([Numero]);


GO
CREATE NONCLUSTERED INDEX [missing_index_414376_414375]
    ON [dbo].[ChequeCliente]([FechaCobranza] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Numero], [IDCliente], [IDBanco], [Fecha], [NroCheque], [IDMoneda], [Cotizacion], [ImporteMoneda], [Importe], [Diferido], [FechaVencimiento], [Saldo], [Cartera], [Depositado], [Rechazado], [Conciliado], [IDMotivoRechazo], [SaldoACuenta], [Descontado]);


GO
CREATE NONCLUSTERED INDEX [missing_index_418934_418933]
    ON [dbo].[ChequeCliente]([IDMoneda] ASC, [FechaVencimiento] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [Numero], [IDCliente], [IDBanco], [Fecha], [NroCheque], [Cotizacion], [ImporteMoneda], [Importe], [Diferido], [Saldo], [Cartera], [Depositado], [Rechazado], [Conciliado], [IDMotivoRechazo], [SaldoACuenta], [Descontado]);


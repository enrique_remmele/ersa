﻿CREATE TABLE [dbo].[PuntoExpedicion] (
    [ID]                     INT          NOT NULL,
    [IDTipoComprobante]      SMALLINT     NOT NULL,
    [IDSucursal]             TINYINT      NOT NULL,
    [Referencia]             VARCHAR (5)  CONSTRAINT [DF_PuntoExpedicion_Referencia] DEFAULT ((1)) NOT NULL,
    [Descripcion]            VARCHAR (50) NOT NULL,
    [Timbrado]               VARCHAR (50) NOT NULL,
    [Vencimiento]            DATE         NOT NULL,
    [NumeracionDesde]        NUMERIC (18) CONSTRAINT [DF_PuntoExpedicion_NumeracionDesde] DEFAULT ((0)) NOT NULL,
    [NumeracionHasta]        NUMERIC (18) CONSTRAINT [DF_PuntoExpedicion_NumeracionHasta] DEFAULT ((0)) NOT NULL,
    [CantidadPorTalonario]   SMALLINT     CONSTRAINT [DF_PuntoExpedicion_CantidadPorTalonario] DEFAULT ((0)) NOT NULL,
    [UltimoNumero]           NUMERIC (18) CONSTRAINT [DF_PuntoExpedicion_UltimoNumero] DEFAULT ((0)) NULL,
    [Estado]                 BIT          NULL,
    [VentaDirecta]           BIT          NULL,
    [IDDepositoVentaDirecta] TINYINT      NULL,
    CONSTRAINT [PK_PuntoVenta] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_PuntoExpedicion_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID]),
    CONSTRAINT [FK_PuntoExpedicion_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


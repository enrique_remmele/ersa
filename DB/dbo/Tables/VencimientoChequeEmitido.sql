﻿CREATE TABLE [dbo].[VencimientoChequeEmitido] (
    [IDTransaccion]   NUMERIC (18) NULL,
    [IDTransaccionOP] NUMERIC (18) NULL,
    [Fecha]           DATE         NULL,
    [Anulado]         BIT          NULL,
    [Procesado]       BIT          NULL
);


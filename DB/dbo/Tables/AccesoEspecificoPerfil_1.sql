﻿CREATE TABLE [dbo].[AccesoEspecificoPerfil] (
    [IDPerfil]           INT          NOT NULL,
    [IDAccesoEspecifico] INT          NOT NULL,
    [IDMenu]             NUMERIC (18) NOT NULL,
    [Habilitar]          BIT          CONSTRAINT [DF_AccesoEspecificoPerfil_Habilitar] DEFAULT ('False') NOT NULL,
    CONSTRAINT [PK_AccesoEspecificoPerfil] PRIMARY KEY CLUSTERED ([IDPerfil] ASC, [IDAccesoEspecifico] ASC, [IDMenu] ASC),
    CONSTRAINT [FK_AccesoEspecificoPerfil_AccesoEspecifico] FOREIGN KEY ([IDAccesoEspecifico], [IDMenu]) REFERENCES [dbo].[AccesoEspecifico] ([ID], [IDMenu]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_AccesoEspecificoPerfil_Perfil] FOREIGN KEY ([IDPerfil]) REFERENCES [dbo].[Perfil] ([ID])
);


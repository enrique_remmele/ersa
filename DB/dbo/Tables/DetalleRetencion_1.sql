﻿CREATE TABLE [dbo].[DetalleRetencion] (
    [IDTransaccion]       NUMERIC (18) NOT NULL,
    [ID]                  TINYINT      NOT NULL,
    [IDTransaccionEgreso] NUMERIC (18) NULL,
    [TipoyComprobante]    VARCHAR (50) NULL,
    [Fecha]               DATE         NULL,
    [Gravado5]            MONEY        NULL,
    [IVA5]                MONEY        NULL,
    [Gravado10]           MONEY        NULL,
    [IVA10]               MONEY        NULL,
    [Total5]              MONEY        NULL,
    [Total10]             MONEY        NULL,
    [PorcRetencion]       DECIMAL (18) NULL,
    [RetencionIVA]        MONEY        NULL,
    [PorRenta]            DECIMAL (18) NULL,
    [Renta]               MONEY        NULL,
    CONSTRAINT [PK_DetalleRetencion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160406-065032]
    ON [dbo].[DetalleRetencion]([TipoyComprobante] ASC, [Fecha] ASC);


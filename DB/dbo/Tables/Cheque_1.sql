﻿CREATE TABLE [dbo].[Cheque] (
    [IDTransaccion]    NUMERIC (18)  NOT NULL,
    [IDSucursal]       TINYINT       NOT NULL,
    [Numero]           INT           NOT NULL,
    [IDCuentaBancaria] TINYINT       NOT NULL,
    [NroCheque]        VARCHAR (50)  NOT NULL,
    [Fecha]            DATE          NOT NULL,
    [FechaPago]        DATE          NOT NULL,
    [Cotizacion]       MONEY         CONSTRAINT [DF_Cheque_Cotizacion] DEFAULT ((1)) NOT NULL,
    [ImporteMoneda]    MONEY         CONSTRAINT [DF_Cheque_ImporteMoneda] DEFAULT ((0)) NOT NULL,
    [Importe]          MONEY         CONSTRAINT [DF_Cheque_Importe] DEFAULT ((0)) NOT NULL,
    [Diferido]         BIT           CONSTRAINT [DF_Cheque_Diferido] DEFAULT ('False') NOT NULL,
    [FechaVencimiento] DATE          NULL,
    [ALaOrden]         VARCHAR (100) NOT NULL,
    [Conciliado]       BIT           CONSTRAINT [DF_Cheque_Cancelado] DEFAULT ('False') NOT NULL,
    [Anulado]          BIT           CONSTRAINT [DF_Cheque_Anulado] DEFAULT ('False') NOT NULL,
    CONSTRAINT [PK_Cheque] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC),
    CONSTRAINT [FK_Cheque_CuentaBancaria] FOREIGN KEY ([IDCuentaBancaria]) REFERENCES [dbo].[CuentaBancaria] ([ID]),
    CONSTRAINT [FK_Cheque_OrdenPago] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[OrdenPago] ([IDTransaccion]),
    CONSTRAINT [FK_Cheque_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_2664_2663]
    ON [dbo].[Cheque]([IDCuentaBancaria] ASC, [NroCheque] ASC)
    INCLUDE([IDTransaccion], [IDSucursal]);


GO
CREATE NONCLUSTERED INDEX [missing_index_2666_2665]
    ON [dbo].[Cheque]([NroCheque] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_3454_3453]
    ON [dbo].[Cheque]([Numero] ASC, [IDCuentaBancaria] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_147996_147995]
    ON [dbo].[Cheque]([Numero] ASC, [IDCuentaBancaria] ASC)
    INCLUDE([IDTransaccion], [IDSucursal], [NroCheque], [Fecha], [FechaPago], [Cotizacion], [ImporteMoneda], [Importe], [Diferido], [FechaVencimiento], [ALaOrden], [Conciliado], [Anulado]);


GO
CREATE NONCLUSTERED INDEX [missing_index_417372_417371]
    ON [dbo].[Cheque]([Fecha] ASC, [Diferido] ASC, [FechaVencimiento] ASC);


﻿CREATE TABLE [dbo].[AjusteInicial] (
    [IDTransaccion] NUMERIC (18) NOT NULL,
    [Numero]        NUMERIC (18) NOT NULL,
    [Fecha]         DATETIME     NOT NULL,
    [Observacion]   VARCHAR (50) NOT NULL,
    [Procesado]     BIT          CONSTRAINT [DF_AjusteInicial_Procesado] DEFAULT ('False') NULL,
    CONSTRAINT [PK_AjusteInicial_1] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


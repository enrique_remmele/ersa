﻿CREATE TABLE [dbo].[DetalleVenta] (
    [IDTransaccion]                 NUMERIC (18)    NOT NULL,
    [IDProducto]                    INT             NOT NULL,
    [ID]                            TINYINT         NOT NULL,
    [IDDeposito]                    TINYINT         NOT NULL,
    [Observacion]                   VARCHAR (200)   NOT NULL,
    [IDImpuesto]                    TINYINT         NOT NULL,
    [Cantidad]                      DECIMAL (10, 2) NOT NULL,
    [PrecioUnitario]                MONEY           CONSTRAINT [DF_DetalleVenta_PrecioUnitario] DEFAULT ((0)) NOT NULL,
    [Total]                         MONEY           CONSTRAINT [DF_DetalleVenta_Total] DEFAULT ((0)) NOT NULL,
    [TotalImpuesto]                 MONEY           CONSTRAINT [DF_DetalleVenta_TotalImpuesto] DEFAULT ((0)) NOT NULL,
    [TotalDiscriminado]             MONEY           NOT NULL,
    [PorcentajeDescuento]           NUMERIC (5, 2)  NULL,
    [DescuentoUnitario]             MONEY           CONSTRAINT [DF_DetalleVenta_DescuentoUnitario] DEFAULT ((0)) NULL,
    [DescuentoUnitarioDiscriminado] MONEY           CONSTRAINT [DF_DetalleVenta_DescuentoUnitario1] DEFAULT ((0)) NULL,
    [TotalDescuento]                MONEY           CONSTRAINT [DF_DetalleVenta_TotalDescuento] DEFAULT ((0)) NULL,
    [TotalDescuentoDiscriminado]    MONEY           CONSTRAINT [DF_DetalleVenta_TotalDescuento1] DEFAULT ((0)) NULL,
    [CostoUnitario]                 MONEY           NULL,
    [TotalCosto]                    MONEY           NULL,
    [TotalCostoImpuesto]            MONEY           NULL,
    [TotalCostoDiscriminado]        MONEY           NULL,
    [Caja]                          BIT             NULL,
    [CantidadCaja]                  DECIMAL (10, 2) NULL,
    CONSTRAINT [PK_DetalleVenta] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC)
);


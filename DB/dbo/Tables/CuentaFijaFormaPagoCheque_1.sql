﻿CREATE TABLE [dbo].[CuentaFijaFormaPagoCheque] (
    [ID]               TINYINT      NOT NULL,
    [Descripcion]      VARCHAR (50) NOT NULL,
    [Orden]            TINYINT      NOT NULL,
    [Diferido]         BIT          NOT NULL,
    [IDMoneda]         TINYINT      NOT NULL,
    [IDCuentaContable] INT          NOT NULL,
    CONSTRAINT [PK_CuentaFijaCobranzaCreditoCheque] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CuentaFijaFormaPagoCheque_CuentaContable] FOREIGN KEY ([IDCuentaContable]) REFERENCES [dbo].[CuentaContable] ([ID]),
    CONSTRAINT [FK_CuentaFijaFormaPagoCheque_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID])
);


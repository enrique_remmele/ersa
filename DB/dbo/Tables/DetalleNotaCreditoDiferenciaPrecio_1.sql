﻿CREATE TABLE [dbo].[DetalleNotaCreditoDiferenciaPrecio] (
    [IDProducto]                     INT             NOT NULL,
    [IDCliente]                      INT             NULL,
    [IDListaPrecio]                  INT             NULL,
    [Cantidad]                       DECIMAL (18, 3) NULL,
    [PrecioOriginal]                 MONEY           NULL,
    [NuevoPrecio]                    MONEY           NULL,
    [IDTransaccionPedidoVenta]       INT             NULL,
    [IDTransaccionPedidoNotaCredito] INT             NULL,
    [IDTransaccionVenta]             INT             NULL,
    [IDTransaccionNotaCredito]       INT             NULL,
    [AutomaticoPorExcepcion]         BIT             NULL
);


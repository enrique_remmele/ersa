﻿CREATE TABLE [dbo].[DestinoDevolucion] (
    [IDSucursal] INT          NOT NULL,
    [IDDeposito] INT          NOT NULL,
    [Destino]    VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_DestinoDevolucion] PRIMARY KEY CLUSTERED ([IDSucursal] ASC, [Destino] ASC)
);


﻿CREATE TABLE [dbo].[CFVentaMercaderia] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    [Produccion]  BIT     NULL,
    CONSTRAINT [PK_CFVentaCosto] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[Terminal] (
    [ID]               INT          NOT NULL,
    [IDSucursal]       TINYINT      NULL,
    [IDDeposito]       TINYINT      NULL,
    [Descripcion]      VARCHAR (50) NOT NULL,
    [Impresora]        VARCHAR (50) NULL,
    [CodigoActivacion] VARCHAR (50) NULL,
    [Activado]         BIT          NULL,
    [UltimoAcceso]     DATETIME     NULL,
    CONSTRAINT [PK_Terminal] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Terminal_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


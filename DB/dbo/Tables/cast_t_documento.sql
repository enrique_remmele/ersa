﻿CREATE TABLE [dbo].[cast_t_documento] (
    [id]                        NUMERIC (18)  NOT NULL,
    [cliente_codigo]            VARCHAR (50)  NULL,
    [cliente_descripcion]       VARCHAR (100) NULL,
    [nit]                       VARCHAR (15)  NULL,
    [tipo_pago_codigo]          TINYINT       NULL,
    [Forma_pago]                INT           NULL,
    [tipo_pago_nombre]          VARCHAR (50)  NULL,
    [listas_de_precio_codigo]   INT           NULL,
    [fecha_hora_pedido]         DATETIME      NULL,
    [vendedor_codigo]           TINYINT       NULL,
    [vendedor_descripcion]      VARCHAR (50)  NULL,
    [descuento]                 MONEY         NULL,
    [importe_total]             MONEY         NULL,
    [total_gral]                MONEY         NULL,
    [total_iva]                 MONEY         NULL,
    [tipo_documento]            VARCHAR (50)  NULL,
    [observacion]               VARCHAR (800) NULL,
    [deposito]                  TINYINT       NULL,
    [anulado]                   BIT           NULL,
    [restriccion_hora_entrega]  BIT           NULL,
    [hora_entrega_desde]        TIME (7)      NULL,
    [hora_entrega_hasta]        TIME (7)      NULL,
    [IDTransaccion]             NUMERIC (18)  NULL,
    [Sincronizado]              BIT           CONSTRAINT [DF_cast_t_documento_Sincronizado] DEFAULT ('False') NULL,
    [ObservacionSincronizacion] VARCHAR (200) NULL,
    [id_direccion]              TINYINT       NULL,
    CONSTRAINT [PK_cast_t_documento] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE Trigger [dbo].[TI_Cast_Documento] On [dbo].[cast_t_documento]
For Insert, Update

As

Begin
	
	Set NoCount On  

	--Variables
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDPedido int
	Declare @vIDTransaccion numeric(18,0)
	Declare @vNumero int 
	Declare @vIDCliente int 
	Declare @vRazonSocial varchar(50)
	Declare @vRUC varchar(50) 
	Declare @vDireccion varchar(100)
	Declare @vTelefono varchar(50)
	Declare @vFecha date 
	Declare @vFechaFacturar date 
	Declare @vIDSucursal int 
	Declare @vIDDeposito int 
	Declare @vIDListaPrecio int 
	Declare @vIDVendedor int
	Declare @vObservacion varchar(800)
	Declare @vTotal money
	Declare @vTotalImpuesto money 
	Declare @vTotalDiscriminado money
	Declare @vTotalDescuento money
	Declare @vIDFormaPagoFactura int
	Declare @vIDUsuario int
	Declare @vIDTerminal int
	Declare @vIDOperacion int
	Declare @Credito bit
	Declare @Tipo_Pago_Nombre as varchar(16)
	Declare @vIDSucursalCliente int
	Declare @vHoraDesde time(7)
	Declare @vHoraHasta time(7)
	

	--Solo si no se proceso
	If (Select Sincronizado From inserted) = 1 Begin
		Set @vMensaje = (Select ObservacionSincronizacion From inserted)
		Set @vProcesado = 'True'
		GoTo Salir
	End

	--Obtner Valores
	Begin
	
		--Pedido
		Set @vIDPedido = (Select id From inserted)

		--Cliente
		Set @vIDCliente = IsNull((Select Top(1) ID From Cliente Where Referencia=(Select Cliente_Codigo From Inserted)), 0)
		If @vIDCliente = 0 Begin
			Set @vMensaje = 'No se encontro el cliente'
			Set @vProcesado = 'False'
			GoTo Salir
		End

		Select	@vRazonSocial=RazonSocial,
				@vRUC=RUC,
				@vDireccion=Direccion,
				@vTelefono=Telefono
		From Cliente
		Where ID=@vIDCliente

		--If @vIDSucursalCliente > 0 Begin
		--	Select	@vDireccion=Direccion,
		--			@vTelefono=Telefono
		--	From vClienteSucursal
		--	Where IDCliente=@vIDCliente
		--	and ID = @vIDSucursalCliente
		--End
		
	End

	--Varios
	Select	@vIDDeposito=deposito,
			@vIDListaPrecio=listas_de_precio_codigo,
			@vIDVendedor=vendedor_codigo,
			@vFecha = GetDate(),
			@vFechaFacturar = fecha_hora_pedido,
			@vObservacion=Observacion,
			@vTotal=importe_total,
			@vTotalImpuesto=total_iva,
			@vTotalDiscriminado=importe_total-total_iva,
			@vTotalDescuento=descuento,
			@vIDFormaPagoFactura=Forma_Pago,
			@Tipo_Pago_Nombre = tipo_pago_nombre,
			@vIDSucursalCliente=IsNull(id_direccion, 0),
			@vHoraDesde = hora_entrega_desde,
			@vHoraHasta = hora_entrega_hasta
	From Inserted

	--Se cambiar porque no estaba cargado el valor de @vIDSucursalCliente en el momento del control mas arriba
	If @vIDSucursalCliente > 0 Begin
			Select	@vDireccion=Direccion,
					@vTelefono=Telefono
			From vClienteSucursal
			Where IDCliente=@vIDCliente
			and ID = @vIDSucursalCliente
		End

	--Validar
	--Deposito
	If Not Exists(Select * From Deposito Where ID=@vIDDeposito) Begin
		Set @vMensaje = 'No se encontro el deposito'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	--Sucursal
	Set @vIDSucursal = (Select IDSucursal From Deposito Where ID=@vIDDeposito)
	If @vIDSucursal = 0 Begin
		Set @vMensaje = 'No se encontro la sucursal'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	--Vendedor
	If Not Exists(Select * From Vendedor Where ID=@vIDVendedor) Begin
		Set @vMensaje = 'No se encontro el vendedor'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	--Lista de precio
	If Not Exists(Select * From ListaPrecio Where ID=@vIDListaPrecio) Begin
		Set @vMensaje = 'No se encontro la lista de precio'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	--Usuario
	Set @vIDUsuario = IsNull((Select ID From Usuario Where Usuario='cast'), 0)
	If @vIDUsuario = 0 Begin
		Set @vIDUsuario = IsNull((Select Max(ID) + 1 From Usuario), 1)
		Insert Into Usuario(ID, Nombre, Usuario, [Password], IDPerfil, Estado, Identificador, EsVendedor, IDVendedor, EsChofer, IDChofer)
		Values(@vIDUsuario, 'cast', 'cast', '#$%^&^%$#@', 1, 1, 'CAS', 0, NULL, 0, NULL)
	End

	

	--Terminal
	set @vIDTerminal = IsNull((Select ID From Terminal Where Descripcion='cast'), 0)
	If @vIDTerminal = 0 Begin
		Set @vIDTerminal = IsNull((Select Max(ID) + 1 From Terminal), 1)
		Insert Into Terminal(ID, IDSucursal, IDDeposito, Descripcion, Impresora, CodigoActivacion, Activado)
		Values(@vIDTerminal, @vIDSucursal, @vIDDeposito, 'cast', '', '', 1)
	End

	--Forma de Pago 
	If @vIDFormaPagoFactura Is Null Begin
		Set @vIDFormaPagoFactura = (Select Top(1) ID From FormaPagoFactura Where Descripcion='EFECTIVO')
	End

	--Condicion de venta
	IF @Tipo_Pago_Nombre = 'CREDITO' Begin
		set @Credito = 'True'
	end else
		set @Credito = 'False'
	

	--Operacion
	Set @vIDOperacion = (Select Top(1) ID From Operacion Where FormName='frmPedido' And ID>0)

	--Transaccion
	EXEC SpTransaccion
		@IDUsuario = @vIDUsuario,
		@IDSucursal = @vIDSucursal,
		@IDDeposito = @vIDDeposito,
		@IDTerminal = @vIDTerminal,
		@IDOperacion = @vIDOperacion,
		@Mensaje = @vMensaje OUTPUT,
		@Procesado = @vProcesado OUTPUT,
		@IDTransaccion = @vIDTransaccion OUTPUT

	--Numero
	Set @vNumero = IsNull((Select Max(Numero)+1 From Pedido where IDSucursal = @vIDSucursal), 1)

	--Insertamos
	Insert Into Pedido(IDTransaccion, Numero, IDCliente, IDSucursalCliente, RazonSocial, RUC, Direccion, Telefono, Fecha, FechaFacturar, IDSucursal, IDDeposito, IDListaPrecio, IDVendedor, IDMoneda, Cotizacion, Observacion, Total, TotalImpuesto, TotalDiscriminado, TotalDescuento, Facturado, EsVentaSucursal, Anulado, FechaAnulado, IDUsuarioAnulado, Procesado, Vendedor, IDFormaPagoFactura, Aprobado, Credito)
	Values(@vIDTransaccion, @vNumero, @vIDCliente, NULL, @vRazonSocial, @vRUC, @vDireccion, @vTelefono, @vFecha, @vFechaFacturar, @vIDSucursal, @vIDDeposito, @vIDListaPrecio, @vIDVendedor, 1, 1, @vObservacion, @vTotal, @vTotalImpuesto, @vTotalDiscriminado, @vTotalDescuento, 0, 0, 0, NULL, NULL, 0, NULL, @vIDFormaPagoFactura, 1, @Credito)

	--Pedido Vendedor
	Set @vNumero = IsNull((Select Max(Numero)+1 From PedidoVendedor Where IDVendedor=@vIDVendedor), 1)
	Insert Into PedidoVendedor(IDTransaccion, IDVendedor, Numero)
	Values(@vIDTransaccion, @vIDVendedor, @vNumero)

	--Aprobacion de pedido
	Declare @vAprobado bit = 'True'
	Declare @vIDFormaPago smallint = (Select Top(1) IDFormaPagoFactura From Pedido Where IDTransaccion=@vIDTransaccion)

	If (Select Top(1) Aprobacion From VFormaPagoFactura Where ID=@vIDFormaPago) = 'True' Begin
		Set @vAprobado = 'False'
	End
		
	Update Pedido
	set Aprobado = @vAprobado
	Where IDTransaccion=@vIDTransaccion


	--Venta Sucursal
	If @vIDSucursalCliente !=0 Begin
		Update Pedido Set	EsVentaSucursal = 1,
							IDSucursalCliente=@vIDSucursalCliente
		Where IDTransaccion=@vIDTransaccion
	End

	if (@vHoraDesde is not null) begin
		Update Pedido Set HoraDesde = @vHoraDesde,
						HoraHasta = @vHoraHasta,
						RestriccionHorariaEntrega = 1
		Where IDTransaccion=@vIDTransaccion
		
	end

	Set @vMensaje = 'Registro procesado'
	Set @vProcesado = 'True'

Salir:

	If @vProcesado = 'False' Begin
		Update cast_t_documento Set	Sincronizado='False',
									ObservacionSincronizacion=@vMensaje
		Where id=@vIDPedido
	End

	If @vProcesado = 'True' Begin
		Update cast_t_documento Set	IDTransaccion=@vIDTransaccion,
									Sincronizado='True',
									ObservacionSincronizacion=@vMensaje
		Where id=@vIDPedido
	End

End

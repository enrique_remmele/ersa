﻿CREATE TABLE [dbo].[CFOrdenPago] (
    [IDOperacion]            TINYINT NOT NULL,
    [ID]                     TINYINT NOT NULL,
    [Proveedor]              BIT     NULL,
    [Efectivo]               BIT     NULL,
    [Cheque]                 BIT     NULL,
    [Retencion]              BIT     NULL,
    [BuscarEnCuentaBancaria] BIT     NULL,
    [BuscarEnProveedores]    BIT     NULL,
    [DiferenciaCambio]       BIT     CONSTRAINT [DF_CFOrdenPago_DiferenciaCambio] DEFAULT ('False') NULL,
    [Documento]              BIT     NULL,
    [ChequeDiferido]         BIT     NULL,
    [SoloDiferido]           BIT     NULL,
    CONSTRAINT [PK_CFOrdenPago] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


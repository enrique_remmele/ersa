﻿CREATE TABLE [dbo].[Recibo] (
    [ID]              INT          NOT NULL,
    [Referencia]      VARCHAR (10) CONSTRAINT [DF_Recibo_Referencia] DEFAULT ((1)) NOT NULL,
    [Descripcion]     VARCHAR (50) NOT NULL,
    [NumeracionDesde] NUMERIC (18) CONSTRAINT [DF_Recibo_NumeracionDesde] DEFAULT ((0)) NOT NULL,
    [NumeracionHasta] NUMERIC (18) CONSTRAINT [DF_Recibo_NumeracionHasta] DEFAULT ((0)) NOT NULL,
    [UltimoNumero]    NUMERIC (18) CONSTRAINT [DF_Recibo_UltimoNumero] DEFAULT ((0)) NULL,
    [Estado]          BIT          NULL,
    CONSTRAINT [PK_Recibo] PRIMARY KEY CLUSTERED ([ID] ASC)
);


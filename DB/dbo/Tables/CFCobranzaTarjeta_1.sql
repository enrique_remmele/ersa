﻿CREATE TABLE [dbo].[CFCobranzaTarjeta] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    CONSTRAINT [PK_CFCobranzaTarjeta] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[AccesoBasico] (
    [IDMenu]     NUMERIC (18) NOT NULL,
    [IDPerfil]   SMALLINT     NOT NULL,
    [Visualizar] BIT          CONSTRAINT [DF_AccesoBasico_Visualizar] DEFAULT ('False') NULL,
    [Agregar]    BIT          CONSTRAINT [DF_AccesoBasico_Agregar] DEFAULT ('False') NULL,
    [Modificar]  BIT          CONSTRAINT [DF_AccesoBasico_Modificar] DEFAULT ('False') NULL,
    [Eliminar]   BIT          CONSTRAINT [DF_AccesoBasico_Eliminar] DEFAULT ('False') NULL,
    [Anular]     BIT          CONSTRAINT [DF_AccesoBasico_Anular] DEFAULT ('False') NULL,
    [Imprimir]   BIT          CONSTRAINT [DF_AccesoBasico_Imprimir] DEFAULT ('False') NULL,
    CONSTRAINT [PK_AccesoBasico] PRIMARY KEY CLUSTERED ([IDMenu] ASC, [IDPerfil] ASC)
);


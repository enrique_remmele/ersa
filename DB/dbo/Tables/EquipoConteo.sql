﻿CREATE TABLE [dbo].[EquipoConteo] (
    [ID]             SMALLINT     NOT NULL,
    [Descripcion]    VARCHAR (50) NOT NULL,
    [IDZonaDeposito] INT          NULL,
    [Estado]         BIT          CONSTRAINT [DF_EquipoConteo_Estado] DEFAULT ('False') NULL,
    [EquipoConteo]   TINYINT      NULL,
    CONSTRAINT [PK_EquipoConteo] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_EquipoConteo_ZonaDeposito] FOREIGN KEY ([IDZonaDeposito]) REFERENCES [dbo].[ZonaDeposito] ([ID])
);


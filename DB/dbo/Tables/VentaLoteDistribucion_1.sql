﻿CREATE TABLE [dbo].[VentaLoteDistribucion] (
    [IDTransaccionVenta] NUMERIC (18) NOT NULL,
    [IDTransaccionLote]  NUMERIC (18) NOT NULL,
    [ID]                 SMALLINT     NOT NULL,
    [Importe]            MONEY        NOT NULL,
    CONSTRAINT [PK_VentaLoteDistribucion] PRIMARY KEY CLUSTERED ([IDTransaccionVenta] ASC, [IDTransaccionLote] ASC),
    CONSTRAINT [FK_VentaLoteDistribucion_LoteDistribucion] FOREIGN KEY ([IDTransaccionLote]) REFERENCES [dbo].[LoteDistribucion] ([IDTransaccion]) ON DELETE CASCADE,
    CONSTRAINT [FK_VentaLoteDistribucion_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


﻿CREATE TABLE [dbo].[RetencionIVA] (
    [IDTransaccion]          NUMERIC (18) NOT NULL,
    [IDPuntoExpedicion]      INT          NOT NULL,
    [IDTipoComprobante]      SMALLINT     NOT NULL,
    [NroComprobante]         NUMERIC (18) NOT NULL,
    [IDProveedor]            INT          NOT NULL,
    [IDSucursal]             TINYINT      NOT NULL,
    [Fecha]                  DATE         NOT NULL,
    [IDMoneda]               TINYINT      NOT NULL,
    [Cotizacion]             MONEY        NULL,
    [Observacion]            VARCHAR (50) NULL,
    [TotalIVA]               MONEY        NOT NULL,
    [TotalRenta]             MONEY        NOT NULL,
    [Anulado]                BIT          NOT NULL,
    [FechaAnulado]           DATE         NULL,
    [IDUsuarioAnulado]       SMALLINT     NULL,
    [Procesado]              BIT          NULL,
    [IDTransaccionOrdenPago] NUMERIC (18) NULL,
    [Total]                  MONEY        NULL,
    [OP]                     BIT          NULL,
    CONSTRAINT [FK_RetencionIVA_PuntoExpedicion] FOREIGN KEY ([IDPuntoExpedicion]) REFERENCES [dbo].[PuntoExpedicion] ([ID]),
    CONSTRAINT [FK_RetencionIVA_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


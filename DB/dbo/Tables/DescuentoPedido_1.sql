﻿CREATE TABLE [dbo].[DescuentoPedido] (
    [IDTransaccion]         NUMERIC (18)   NOT NULL,
    [IDProducto]            INT            NOT NULL,
    [ID]                    TINYINT        NOT NULL,
    [IDDescuento]           TINYINT        NOT NULL,
    [IDDeposito]            TINYINT        NOT NULL,
    [IDTipoDescuento]       TINYINT        NOT NULL,
    [IDActividad]           INT            NULL,
    [Descuento]             MONEY          NOT NULL,
    [Porcentaje]            NUMERIC (5, 2) NULL,
    [DescuentoDiscriminado] MONEY          CONSTRAINT [DF_DescuentoPedido_DescuentoDiscriminado_1] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_DescuentoPedido] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDProducto] ASC, [ID] ASC, [IDDescuento] ASC),
    CONSTRAINT [FK_DescuentoPedido_Actividad] FOREIGN KEY ([IDActividad]) REFERENCES [dbo].[Actividad] ([ID]),
    CONSTRAINT [FK_DescuentoPedido_DetallePedido] FOREIGN KEY ([IDTransaccion], [IDProducto], [ID]) REFERENCES [dbo].[DetallePedido] ([IDTransaccion], [IDProducto], [ID]),
    CONSTRAINT [FK_DescuentoPedido_TipoDescuento] FOREIGN KEY ([IDTipoDescuento]) REFERENCES [dbo].[TipoDescuento] ([ID]) ON UPDATE CASCADE
);


GO
ALTER TABLE [dbo].[DescuentoPedido] NOCHECK CONSTRAINT [FK_DescuentoPedido_Actividad];


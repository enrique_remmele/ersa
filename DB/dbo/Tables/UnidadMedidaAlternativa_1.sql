﻿CREATE TABLE [dbo].[UnidadMedidaAlternativa] (
    [IDProducto]        INT             NOT NULL,
    [ID]                TINYINT         NOT NULL,
    [IDUnidadMedida]    TINYINT         NOT NULL,
    [CantidadPorUnidad] DECIMAL (10, 2) NOT NULL,
    CONSTRAINT [PK_UnidadMedidaAlternativa] PRIMARY KEY CLUSTERED ([IDProducto] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[LogAcceso] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [IDTerminal] INT          NOT NULL,
    [Usuario]    VARCHAR (50) NOT NULL,
    [Version]    VARCHAR (50) NOT NULL,
    [Fecha]      DATETIME     CONSTRAINT [DF_LogAcceso_Fecha] DEFAULT (getdate()) NOT NULL,
    [IP]         VARCHAR (50) NULL,
    CONSTRAINT [PK_LogAcceso] PRIMARY KEY CLUSTERED ([ID] ASC)
);


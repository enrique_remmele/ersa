﻿CREATE TABLE [dbo].[cast_t_detalle_documento] (
    [id_pedido]                 NUMERIC (18)    NOT NULL,
    [id_detalle]                TINYINT         NOT NULL,
    [item]                      INT             NULL,
    [articulo_codigo]           VARCHAR (15)    NULL,
    [articulo_descripcion]      VARCHAR (100)   NULL,
    [cantidad]                  DECIMAL (10, 2) NULL,
    [descuento]                 MONEY           NULL,
    [precio]                    MONEY           NULL,
    [importe]                   MONEY           NULL,
    [iva]                       MONEY           NULL,
    [total_iva]                 MONEY           NULL,
    [color]                     VARCHAR (50)    NULL,
    [presentacion]              VARCHAR (50)    NULL,
    [IDTransaccion]             NUMERIC (18)    NULL,
    [IDDetalle]                 TINYINT         NULL,
    [Sincronizado]              BIT             CONSTRAINT [DF_cast_t_detalle_documento_Sincronizado] DEFAULT ('False') NULL,
    [ObservacionSincronizacion] VARCHAR (200)   NULL,
    CONSTRAINT [PK_cast_t_detalle_documento_1] PRIMARY KEY CLUSTERED ([id_pedido] ASC, [id_detalle] ASC)
);


GO
CREATE Trigger [dbo].[TI_Cast_Detalle_Documento] On [dbo].[cast_t_detalle_documento]
For Insert, Update

As

Begin

	Set NoCount On  

	--Variables
	Declare @vMensaje varchar(50)
	Declare @vProcesado bit
	Declare @vIDPedido int
	Declare @vIDTransaccion numeric(18,0)
	Declare @vIDDetalle int
	Declare @vReferencia varchar(50)
	Declare @vIDProducto int 
	Declare @vID int
	Declare @vIDDeposito int 
	Declare @vIDImpuesto int 
	Declare @vCantidad decimal(10,2) 
	Declare @vPrecioUnitario money
	Declare @vTotal money
	Declare @vTotalImpuesto money 
	Declare @vTotalDiscriminado money
	Declare @vPorcentajeDescuento decimal(5,2)
	Declare @vDescuentoUnitario money
	Declare @vDescuentoUnitarioDiscriminado money 
	Declare @vTotalDescuento money
	Declare @vTotalDescuentoDiscriminado money 
	Declare @vCostoUnitario money
	Declare @vTotalCosto money
	Declare @vTotalCostoImpuesto money 
	Declare @vTotalCostoDiscriminado money
	Declare @vFactorDiscriminado decimal(10,2)
	Declare @vFactorImpuesto decimal(10,2)
	Declare @vEntregaDirecta bit = 0
	Declare @PrecioNuevo money
	Declare @vIDListaPrecio int
	Declare @vIDCliente int
	Declare @vFecha Date

	--Obtener valores
	Select	@vIDPedido=id_pedido,
			@vIDDetalle=id_detalle,
			@vID=item,
			@vReferencia=articulo_codigo,
			@vCantidad=cantidad,
			@vPrecioUnitario=precio,
			@vTotal=cantidad*precio,
			@vTotalImpuesto=total_iva,
			@vTotalDiscriminado=(cantidad*precio)-total_iva,
			@vDescuentoUnitario=descuento,
			@vIDImpuesto=iva
	From Inserted

	--Valor del producto
	Set @vIDProducto = IsNull((Select Top(1) ID From Producto Where Referencia=@vReferencia), 0)

	If @vIDProducto = 0 Begin
		Set @vMensaje = 'No se encontro el producto'
		Set @vProcesado = 'False'
		GoTo Salir
	End

	--Valores del pedido
	Select	@vIDTransaccion=IDTransaccion,
			@vIDDeposito=deposito
	From cast_t_documento Where ID=@vIDPedido

	--Impuesto
	Begin
		If @vIDImpuesto = 5 Begin
			Select	@vIDImpuesto=ID,
					@vFactorDiscriminado=FactorDiscriminado,
					@vFactorImpuesto=FactorImpuesto 
			From Impuesto 
			where Referencia='5%'
		End

		If @vIDImpuesto = 10 Begin
			Select	@vIDImpuesto=ID,
					@vFactorDiscriminado=FactorDiscriminado,
					@vFactorImpuesto=FactorImpuesto  
			From Impuesto 
			where Referencia='10%'
		End

		If @vIDImpuesto = 0 Begin
			Select	@vIDImpuesto=ID,
					@vFactorDiscriminado=FactorDiscriminado,
					@vFactorImpuesto=FactorImpuesto  
			From Impuesto 
			where Referencia='EX'
		End

		If @vIDImpuesto > 3 Or @vIDImpuesto = 0 Begin
			Set @vMensaje = 'No se encontro el impuesto'
			Set @vProcesado = 'False'
			GoTo Salir
		End

	End

	--Valores de descuento
	Begin

		Set @vPorcentajeDescuento = 0
		Set @vDescuentoUnitarioDiscriminado = 0 
		Set @vTotalDescuento = 0
		Set @vTotalDescuentoDiscriminado = 0 

		If @vDescuentoUnitario > 0 Begin
			Set @vPorcentajeDescuento = (@vDescuentoUnitario / @vPrecioUnitario) * 100
			Set @vDescuentoUnitarioDiscriminado = @vDescuentoUnitario / @vFactorDiscriminado
			Set @vTotalDescuento = @vDescuentoUnitario * @vPrecioUnitario
			Set @vTotalDescuentoDiscriminado = @vTotalDescuento / @vFactorDiscriminado
		End

	End

	--Valores de Costo
	Begin
		Set @vCostoUnitario = dbo.FCostoProducto(@vIDProducto)
		Set @vTotalCosto = @vCostoUnitario * @vCantidad
		Set @vTotalCostoImpuesto = @vTotalCosto / @vFactorImpuesto
		Set @vTotalCostoDiscriminado = @vTotalCosto

	End

	--Validar registro
	If Not Exists(Select * From Pedido Where IDTransaccion=@vIDTransaccion) Begin
		Set @vMensaje = 'No se encuentra el pedido'
		Set @vProcesado = 'False'
		GoTo Salir
	End

		Select @vIDListaPrecio = IDListaPrecio, 
				@vIDCliente = IDCliente,
				@vFecha = FechaFacturar 
		From Pedido Where IDTransaccion=@vIDTransaccion

	--Insertar si no exite
	If Not Exists(Select IDTransaccion From DetallePedido Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID) Begin
		Insert Into DetallePedido(IDTransaccion, IDProducto, ID, IDDeposito, Observacion, IDImpuesto, Cantidad, PrecioUnitario, Total, TotalImpuesto, TotalDiscriminado, PorcentajeDescuento, DescuentoUnitario, DescuentoUnitarioDiscriminado, TotalDescuento, TotalDescuentoDiscriminado, CostoUnitario, TotalCosto, TotalCostoImpuesto, TotalCostoDiscriminado, Caja, CantidadCaja)
		Values(@vIDTransaccion, @vIDProducto, @vID, @vIDDeposito, '', @vIDImpuesto, @vCantidad, @vPrecioUnitario, @vTotal, @vTotalImpuesto, @vTotalDiscriminado, @vPorcentajeDescuento, @vDescuentoUnitario, @vDescuentoUnitarioDiscriminado, @vTotalDescuento, @vTotalDescuentoDiscriminado, @vCostoUnitario, @vTotalCosto, @vTotalCostoImpuesto, @vTotalCostoDiscriminado, 0, @vCantidad)
	End Else Begin
		Update DetallePedido Set IDImpuesto=@vIDImpuesto
		Where IDTransaccion=@vIDTransaccion And IDProducto=@vIDProducto And ID=@vID
	End
	
	--Entrega directa
	if (select sum(Peso) from vDetallePedido where IDTransaccion = @vIDTransaccion)>=25000 begin
		Set @vEntregaDirecta = 'True'
	end

	
	--Si tiene excepcion con limite de cantidad, restar el saldo
	Update ProductoListaPrecioExcepciones 
	Set CantidadLimiteSaldo = Isnull(CantidadLimiteSaldo,0) + @vCantidad,
	IDTransaccionPedido=@vIDTransaccion
	Where IDProducto = @vIDProducto 
	and IDCliente = @vIDCliente
	and (@vFecha Between Desde and Hasta)
	and (Case when isnull(UnicaVez,'False') = 'True' then 1 else Isnull(CantidadLimite,0)  end )>0 
	--and ABS(Descuento - round(@vDescuentoUnitario,2)) < 10
	and IDTipoDescuento <> 3
	and IDTransaccionPedido = 0


	--Habilitar el pedido
	Update Pedido set Procesado=1, EntregaCliente = @vEntregaDirecta
	Where IDTransaccion=@vIDTransaccion
	
	Set @vMensaje = 'Registro sincronizado'
	Set @vProcesado = 'True'
	GoTo Salir

Salir:

	If @vProcesado = 'False' Begin
		Update cast_t_detalle_documento Set	Sincronizado='False',
									ObservacionSincronizacion=@vMensaje
		Where id_pedido=@vIDPedido And id_detalle=@vIDDetalle
	End

	If @vProcesado = 'True' Begin
		Update cast_t_detalle_documento Set	IDTransaccion=@vIDTransaccion,
											IDDetalle=@vIDDetalle,
											Sincronizado='True',
											ObservacionSincronizacion=@vMensaje
		Where id_pedido=@vIDPedido And id_detalle=@vIDDetalle

	

		Set @PrecioNuevo = @vPrecioUnitario - (select top(1) ISNULL(Descuento,0) 
													from vProductoListaPrecioExcepciones 
													where IDProducto = @vIDProducto 
													and ListaPrecio = (Select top(1) descripcion from listaprecio where id = @vIDListaPrecio) 
													and IDCliente = @vIDCliente
													and IDTipoDescuento = 3
													and Convert(date,@vFecha) between Convert(date,Desde) and Convert(date,Hasta)
													and (Case When isnull(CantidadLimite,0) > 0 then isnull(CantidadLimite,0) else 0 end) >= (Case When isnull(CantidadLimite,0) > 0 then isnull(CantidadLimiteSaldo,0)+@vCantidad else 0 end))
													
		if @PrecioNuevo <@vPrecioUnitario begin
			insert into DetalleNotaCreditoDiferenciaPrecio(IDProducto, IDCliente, IDListaPrecio, Cantidad, PrecioOriginal, NuevoPrecio, IDTransaccionPedidoVenta, IDTransaccionPedidoNotaCredito, IDTransaccionVenta, IDTransaccionNotaCredito,AutomaticoPorExcepcion)
				Values (@vIDProducto, @vIDCliente, @vIDListaPrecio, @vCantidad, @vPrecioUnitario, @PrecioNuevo,@vIDTransaccion,0,0,0,1)
					
		end

	End

End

﻿CREATE TABLE [dbo].[Caja] (
    [IDTransaccion]   NUMERIC (18)  NOT NULL,
    [IDSucursal]      TINYINT       NOT NULL,
    [Numero]          INT           NOT NULL,
    [Habilitado]      BIT           CONSTRAINT [DF_Caja_Habilitado] DEFAULT ('True') NOT NULL,
    [Fecha]           DATE          NOT NULL,
    [Anulado]         BIT           CONSTRAINT [DF_Caja_Anulado] DEFAULT ('False') NOT NULL,
    [FechaCierre]     DATETIME      NULL,
    [IDUsuarioCierre] INT           NULL,
    [Observacion]     VARCHAR (200) NULL,
    CONSTRAINT [PK_Caja] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


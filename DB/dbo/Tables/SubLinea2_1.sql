﻿CREATE TABLE [dbo].[SubLinea2] (
    [ID]          SMALLINT     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_SubLinea2_Estado] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_SubLinea2] PRIMARY KEY CLUSTERED ([ID] ASC)
);


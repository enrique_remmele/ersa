﻿CREATE TABLE [dbo].[DevolucionSinNotaCredito] (
    [IDTransaccion]                NUMERIC (18)  NOT NULL,
    [Fecha]                        DATE          NOT NULL,
    [IDSucursal]                   TINYINT       NOT NULL,
    [Numero]                       INT           NOT NULL,
    [NumeroSolicitudCliente]       VARCHAR (50)  NULL,
    [IDCliente]                    INT           NOT NULL,
    [IDDeposito]                   TINYINT       NULL,
    [Observacion]                  VARCHAR (200) NULL,
    [Aprobado]                     BIT           NULL,
    [IDUsuarioAprobado]            INT           NULL,
    [FechaAprobado]                DATE          NULL,
    [ObservacionAutorizador]       VARCHAR (500) NULL,
    [Total]                        MONEY         NOT NULL,
    [Anulado]                      BIT           NOT NULL,
    [FechaAnulado]                 DATE          NULL,
    [IDUsuarioAnulado]             SMALLINT      NULL,
    [IDTransaccionMovimientoStock] INT           NULL
);


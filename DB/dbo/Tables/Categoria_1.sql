﻿CREATE TABLE [dbo].[Categoria] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_Categoria_Estado] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED ([ID] ASC)
);


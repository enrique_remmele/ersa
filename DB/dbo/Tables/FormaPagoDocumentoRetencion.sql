﻿CREATE TABLE [dbo].[FormaPagoDocumentoRetencion] (
    [IDTransaccion]      NUMERIC (18)   NOT NULL,
    [ID]                 SMALLINT       NOT NULL,
    [IDTransaccionVenta] NUMERIC (18)   NOT NULL,
    [Porcentaje]         DECIMAL (5, 2) NOT NULL,
    [Importe]            MONEY          NOT NULL,
    [Total]              MONEY          NULL,
    [TotalDiscriminado]  MONEY          NULL,
    [TotalImpuesto]      MONEY          NULL,
    CONSTRAINT [PK_FormaPagoDocumentoRetencion] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC, [IDTransaccionVenta] ASC),
    CONSTRAINT [FK_FormaPagoDocumentoRetencion_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


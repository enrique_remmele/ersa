﻿CREATE TABLE [dbo].[DetalleEfectivo] (
    [IDTransaccion]     NUMERIC (18)  NOT NULL,
    [ID]                SMALLINT      NOT NULL,
    [IDTipoComprobante] SMALLINT      NOT NULL,
    [Comprobante]       VARCHAR (50)  NOT NULL,
    [Fecha]             DATETIME      NOT NULL,
    [VentasCredito]     MONEY         NOT NULL,
    [VentasContado]     MONEY         NOT NULL,
    [Gastos]            MONEY         NOT NULL,
    [IDMoneda]          TINYINT       NOT NULL,
    [ImporteMoneda]     MONEY         NOT NULL,
    [Cotizacion]        MONEY         NOT NULL,
    [Observacion]       VARCHAR (100) NULL,
    CONSTRAINT [PK_DetalleEfectivo] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleEfectivo_TipoComprobante] FOREIGN KEY ([IDTipoComprobante]) REFERENCES [dbo].[TipoComprobante] ([ID])
);


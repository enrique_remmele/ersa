﻿CREATE TABLE [dbo].[MotivoDevolucionNC] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NULL,
    [Activo]      BIT          NULL,
    CONSTRAINT [PK_MotivoDevolucionNC] PRIMARY KEY CLUSTERED ([ID] ASC)
);


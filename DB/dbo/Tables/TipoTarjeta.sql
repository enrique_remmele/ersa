﻿CREATE TABLE [dbo].[TipoTarjeta] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Tipo]        CHAR (1)     NOT NULL,
    CONSTRAINT [PK_TipoTarjeta] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'C=CREDITO;D=DEBITO', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TipoTarjeta', @level2type = N'COLUMN', @level2name = N'Tipo';


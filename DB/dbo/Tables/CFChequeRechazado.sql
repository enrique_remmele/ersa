﻿CREATE TABLE [dbo].[CFChequeRechazado] (
    [IDOperacion]            TINYINT NOT NULL,
    [ID]                     TINYINT NOT NULL,
    [BuscarEnCuentaBancaria] BIT     NULL,
    CONSTRAINT [PK_CFChequeRechazado] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


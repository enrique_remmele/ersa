﻿CREATE TABLE [dbo].[SolicitudEliminacionOrdenPagoEgreso] (
    [IDTransaccionOrdenPago] NUMERIC (18) NOT NULL,
    [IDTransaccionEgreso]    NUMERIC (18) NOT NULL,
    [Cuota]                  TINYINT      NOT NULL,
    [IDUsuarioSolicitud]     INT          NOT NULL,
    [FechaSolicitud]         DATETIME     NOT NULL,
    [IDUsuarioAprobador]     INT          NULL,
    [FechaAprobacion]        DATETIME     NULL,
    [Aprobado]               BIT          NULL
);


﻿CREATE TABLE [dbo].[acast_cliente] (
    [codigo]                    VARCHAR (50)     NOT NULL,
    [sucursal]                  TINYINT          NULL,
    [longitud]                  DECIMAL (18, 16) NULL,
    [latitud]                   DECIMAL (18, 16) NULL,
    [foto]                      IMAGE            NULL,
    [fecha]                     DATE             NULL,
    [sincronizado]              BIT              NULL,
    [ObservacionSincronizacion] VARCHAR (200)    NULL
);


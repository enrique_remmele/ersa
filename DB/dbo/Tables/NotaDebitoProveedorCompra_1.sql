﻿CREATE TABLE [dbo].[NotaDebitoProveedorCompra] (
    [IDTransaccionNotaDebitoProveedor] NUMERIC (18) NOT NULL,
    [IDTransaccionEgreso]              NUMERIC (18) NOT NULL,
    [Importe]                          MONEY        NOT NULL,
    [cobrado]                          MONEY        NOT NULL,
    [Descontado]                       MONEY        NOT NULL,
    [Saldo]                            MONEY        NOT NULL,
    [Tipo]                             VARCHAR (50) NULL,
    CONSTRAINT [PK_NotaDebitoProveedorCompra] PRIMARY KEY CLUSTERED ([IDTransaccionNotaDebitoProveedor] ASC, [IDTransaccionEgreso] ASC),
    CONSTRAINT [FK_NotaDebitoProveedorCompra_Compra] FOREIGN KEY ([IDTransaccionEgreso]) REFERENCES [dbo].[Compra] ([IDTransaccion]),
    CONSTRAINT [FK_NotaDebitoProveedorCompra_Gasto] FOREIGN KEY ([IDTransaccionEgreso]) REFERENCES [dbo].[Gasto] ([IDTransaccion])
);


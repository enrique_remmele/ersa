﻿CREATE TABLE [dbo].[CFCobranzaVenta] (
    [IDOperacion]      TINYINT NOT NULL,
    [ID]               TINYINT NOT NULL,
    [Contado]          BIT     NULL,
    [Credito]          BIT     NULL,
    [DiferenciaCambio] BIT     NULL,
    [AnticipoCliente]  BIT     CONSTRAINT [DF__CFCobranz__Antic__4C0CFD33] DEFAULT ('False') NULL,
    CONSTRAINT [PK_CFCobranzaVenta] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[ImpresionFormato] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Fuente]      VARCHAR (50) NOT NULL,
    [Tamaño]      TINYINT      NOT NULL,
    [Negrita]     BIT          NOT NULL,
    CONSTRAINT [PK_ImpresionFormato] PRIMARY KEY CLUSTERED ([ID] ASC)
);


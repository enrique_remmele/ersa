﻿CREATE TABLE [dbo].[ResultadoLlamada] (
    [ID]          INT          NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Referencia]  VARCHAR (5)  NULL,
    [Estado]      BIT          CONSTRAINT [DF_ResultadoLlamada_Estado] DEFAULT ('True') NULL,
    CONSTRAINT [PK_ResultadoLlamada] PRIMARY KEY CLUSTERED ([ID] ASC)
);


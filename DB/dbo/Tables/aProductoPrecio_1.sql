﻿CREATE TABLE [dbo].[aProductoPrecio] (
    [IDProducto]                     INT          NOT NULL,
    [IDCliente]                      INT          NOT NULL,
    [IDMoneda]                       TINYINT      NOT NULL,
    [PrecioUnico]                    BIT          NOT NULL,
    [Precio]                         MONEY        NOT NULL,
    [Desde]                          DATE         NOT NULL,
    [IDTransaccionPedidoPrecioUnico] INT          NULL,
    [CantidadMinimaPrecioUnico]      INT          NULL,
    [FechaOperacion]                 DATETIME     NOT NULL,
    [IDUsuarioOperacion]             INT          NOT NULL,
    [IDTerminalOperacion]            INT          NOT NULL,
    [Operacion]                      VARCHAR (50) NOT NULL
);


﻿CREATE TABLE [dbo].[GrupoUsuario] (
    [IDGrupo]   TINYINT NOT NULL,
    [IDUsuario] INT     NOT NULL,
    CONSTRAINT [PK_GrupoUsuario] PRIMARY KEY CLUSTERED ([IDGrupo] ASC, [IDUsuario] ASC),
    CONSTRAINT [FK_GrupoUsuario_Usuario] FOREIGN KEY ([IDUsuario]) REFERENCES [dbo].[Usuario] ([ID])
);


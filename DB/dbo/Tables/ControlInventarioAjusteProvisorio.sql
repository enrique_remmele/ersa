﻿CREATE TABLE [dbo].[ControlInventarioAjusteProvisorio] (
    [IDTransaccion] NUMERIC (18)    NOT NULL,
    [IDDeposito]    TINYINT         NOT NULL,
    [IDProducto]    INT             NOT NULL,
    [ID]            TINYINT         NOT NULL,
    [TipoDocumento] VARCHAR (50)    NOT NULL,
    [Comprobante]   VARCHAR (50)    NOT NULL,
    [Observacion]   VARCHAR (50)    NOT NULL,
    [Cantidad]      DECIMAL (10, 2) NOT NULL,
    [IDUsuario]     SMALLINT        NOT NULL,
    [Fecha]         DATETIME        NOT NULL,
    CONSTRAINT [PK_ControlInventarioConteoProvisorio] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDDeposito] ASC, [IDProducto] ASC, [ID] ASC),
    CONSTRAINT [FK_ControlInventarioConteoProvisorio_ExistenciaDepositoInventario] FOREIGN KEY ([IDTransaccion], [IDDeposito], [IDProducto]) REFERENCES [dbo].[ExistenciaDepositoInventario] ([IDTransaccion], [IDDeposito], [IDProducto])
);


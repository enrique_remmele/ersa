﻿CREATE TABLE [dbo].[MarcaPresentacion] (
    [IDTipoProducto] TINYINT  NOT NULL,
    [IDLinea]        SMALLINT NOT NULL,
    [IDSubLinea]     SMALLINT NOT NULL,
    [IDSubLinea2]    SMALLINT NOT NULL,
    [IDMarca]        TINYINT  NOT NULL,
    [IDPresentacion] SMALLINT NOT NULL,
    CONSTRAINT [PK_MarcaPersentacion] PRIMARY KEY CLUSTERED ([IDTipoProducto] ASC, [IDLinea] ASC, [IDSubLinea] ASC, [IDSubLinea2] ASC, [IDMarca] ASC, [IDPresentacion] ASC),
    CONSTRAINT [FK_MarcaPresentacion_Presentacion] FOREIGN KEY ([IDPresentacion]) REFERENCES [dbo].[Presentacion] ([ID]),
    CONSTRAINT [FK_MarcaPresentacion_SubLinea2Marca] FOREIGN KEY ([IDTipoProducto], [IDLinea], [IDSubLinea], [IDSubLinea2], [IDMarca]) REFERENCES [dbo].[SubLinea2Marca] ([IDTipoProducto], [IDLinea], [IDSubLinea], [IDSubLinea2], [IDMarca]) ON DELETE CASCADE ON UPDATE CASCADE
);


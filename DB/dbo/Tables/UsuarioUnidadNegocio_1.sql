﻿CREATE TABLE [dbo].[UsuarioUnidadNegocio] (
    [ID]     TINYINT NOT NULL,
    [Estado] BIT     CONSTRAINT [DF_UsuarioUnidadNegocio_Estado] DEFAULT ('False') NULL,
    CONSTRAINT [PK_UsuarioUnidadNegocio] PRIMARY KEY CLUSTERED ([ID] ASC)
);


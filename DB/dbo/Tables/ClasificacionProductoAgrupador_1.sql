﻿CREATE TABLE [dbo].[ClasificacionProductoAgrupador] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          NOT NULL,
    CONSTRAINT [PK_ClasificacionProductoAgrupador] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿CREATE TABLE [dbo].[DetalleTransferenciaNotaCredito] (
    [IDTransaccionTransferenciaNotaCredito] NUMERIC (18) NOT NULL,
    [ID]                                    TINYINT      NOT NULL,
    [IDTransaccionVenta]                    NUMERIC (18) NOT NULL,
    [Entrante]                              BIT          CONSTRAINT [DF_DetalleTransferenciaNotaCredito_Entrante] DEFAULT ('False') NOT NULL,
    [Saliente]                              BIT          CONSTRAINT [DF_DetalleTransferenciaNotaCredito_Saliente] DEFAULT ('False') NOT NULL,
    [Importe]                               MONEY        NULL,
    [IDTransaccionNotaCreditoAplicacion]    NUMERIC (18) NULL,
    CONSTRAINT [PK_DetalleTransferenciaNotaCredito] PRIMARY KEY CLUSTERED ([IDTransaccionTransferenciaNotaCredito] ASC, [ID] ASC),
    CONSTRAINT [FK_DetalleTransferenciaNotaCredito_TransferenciaNotaCredito] FOREIGN KEY ([IDTransaccionTransferenciaNotaCredito]) REFERENCES [dbo].[TransferenciaNotaCredito] ([IDTransaccion]),
    CONSTRAINT [FK_DetalleTransferenciaNotaCredito_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


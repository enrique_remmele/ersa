﻿CREATE TABLE [dbo].[TablasPrincipales] (
    [Tabla]       VARCHAR (50) NOT NULL,
    [NombreTabla] VARCHAR (50) NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [OrderBy]     VARCHAR (50) NULL,
    [Sp]          BIT          NULL
);


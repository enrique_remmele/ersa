﻿CREATE TABLE [dbo].[ProductoModificarPrecio] (
    [idproducto] INT NOT NULL,
    [estado]     BIT NOT NULL,
    CONSTRAINT [PK_ProductoModificarPrecio] PRIMARY KEY CLUSTERED ([idproducto] ASC),
    CONSTRAINT [FK_ProductoModificarPrecio_ProductoModificarPrecio] FOREIGN KEY ([idproducto]) REFERENCES [dbo].[Producto] ([ID])
);


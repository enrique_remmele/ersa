﻿CREATE TABLE [dbo].[TicketBasculaGastoAdicional] (
    [IDTransaccion]    NUMERIC (18) NOT NULL,
    [ID]               TINYINT      NOT NULL,
    [IDGastoAdicional] TINYINT      NOT NULL,
    [IDMoneda]         TINYINT      NOT NULL,
    [Cotizacion]       MONEY        NOT NULL,
    [Importe]          MONEY        NOT NULL,
    [ImporteGS]        MONEY        NOT NULL,
    [ImporteUS]        MONEY        NOT NULL,
    [CotizacionUS]     MONEY        NOT NULL,
    [IDImpuesto]       TINYINT      NULL,
    [ImporteSinIVa]    MONEY        NULL,
    [IDProveedor]      INT          NULL,
    CONSTRAINT [PK_TicketBasculaGastoAdicional] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDGastoAdicional] ASC, [ID] ASC),
    CONSTRAINT [FK_TicketBasculaGastoAdicional_TicketBascula] FOREIGN KEY ([IDTransaccion]) REFERENCES [dbo].[TicketBascula] ([IDTransaccion])
);


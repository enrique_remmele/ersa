﻿CREATE TABLE [dbo].[VentaCobranza] (
    [IDTransaccionVenta]    NUMERIC (18) NOT NULL,
    [IDTransaccionCobranza] NUMERIC (18) NOT NULL,
    [Importe]               MONEY        NOT NULL,
    [Cancelar]              BIT          CONSTRAINT [DF_VentaCobranza_Cancelar] DEFAULT ('False') NOT NULL,
    [ImporteGs]             MONEY        NULL,
    CONSTRAINT [PK_VentaReciboDinero] PRIMARY KEY CLUSTERED ([IDTransaccionVenta] ASC, [IDTransaccionCobranza] ASC),
    CONSTRAINT [FK_VentaCobranza_CobranzaContado] FOREIGN KEY ([IDTransaccionCobranza]) REFERENCES [dbo].[CobranzaContado] ([IDTransaccion]) NOT FOR REPLICATION,
    CONSTRAINT [FK_VentaCobranza_CobranzaCredito] FOREIGN KEY ([IDTransaccionCobranza]) REFERENCES [dbo].[CobranzaCredito] ([IDTransaccion]) NOT FOR REPLICATION,
    CONSTRAINT [FK_VentaCobranza_Venta] FOREIGN KEY ([IDTransaccionVenta]) REFERENCES [dbo].[Venta] ([IDTransaccion])
);


GO
ALTER TABLE [dbo].[VentaCobranza] NOCHECK CONSTRAINT [FK_VentaCobranza_CobranzaContado];


GO
ALTER TABLE [dbo].[VentaCobranza] NOCHECK CONSTRAINT [FK_VentaCobranza_CobranzaCredito];


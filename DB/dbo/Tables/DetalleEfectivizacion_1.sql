﻿CREATE TABLE [dbo].[DetalleEfectivizacion] (
    [IDTransaccion]         NUMERIC (18) NOT NULL,
    [IDTransaccionCheque]   NUMERIC (18) NOT NULL,
    [IDTransaccionEfectivo] NUMERIC (18) NOT NULL,
    [IDEfectivo]            TINYINT      NULL,
    [Importe]               MONEY        NOT NULL
);


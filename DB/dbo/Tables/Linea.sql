﻿CREATE TABLE [dbo].[Linea] (
    [ID]          SMALLINT     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          CONSTRAINT [DF_Linea_Estado00] DEFAULT ('True') NOT NULL,
    CONSTRAINT [PK_Linea] PRIMARY KEY CLUSTERED ([ID] ASC)
);


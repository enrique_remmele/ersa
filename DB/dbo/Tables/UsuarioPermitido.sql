﻿CREATE TABLE [dbo].[UsuarioPermitido] (
    [IDUsuario] INT NOT NULL,
    CONSTRAINT [PK_UsuarioPermitido] PRIMARY KEY CLUSTERED ([IDUsuario] ASC),
    CONSTRAINT [FK_UsuarioPermitido_Usuario] FOREIGN KEY ([IDUsuario]) REFERENCES [dbo].[Usuario] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
);


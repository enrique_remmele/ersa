﻿CREATE TABLE [dbo].[Llamada] (
    [ID]            INT           NOT NULL,
    [IDCliente]     INT           NOT NULL,
    [Fecha]         DATETIME      NULL,
    [IDTipoLlamada] INT           NOT NULL,
    [IDVendedor]    TINYINT       NOT NULL,
    [IDResultado]   INT           NOT NULL,
    [LlamarFecha]   DATE          NULL,
    [LlamarHora]    TIME (7)      NULL,
    [Atendido]      VARCHAR (50)  NULL,
    [Comentario]    VARCHAR (254) NULL,
    [Estado]        BIT           NOT NULL,
    [Realizado]     BIT           NULL,
    [Entrante]      BIT           NULL,
    [Saliente]      BIT           NULL,
    CONSTRAINT [PK_Llamada] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Llamada_Cliente] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[Cliente] ([ID]),
    CONSTRAINT [FK_Llamada_ResultadoLlamada] FOREIGN KEY ([IDResultado]) REFERENCES [dbo].[ResultadoLlamada] ([ID]),
    CONSTRAINT [FK_Llamada_TipoLlamada] FOREIGN KEY ([IDTipoLlamada]) REFERENCES [dbo].[TipoLlamada] ([ID]),
    CONSTRAINT [FK_Llamada_Vendedor] FOREIGN KEY ([IDVendedor]) REFERENCES [dbo].[Vendedor] ([ID])
);


GO
CREATE NONCLUSTERED INDEX [missing_index_2181_2180]
    ON [dbo].[Llamada]([IDCliente] ASC, [Estado] ASC);


GO
CREATE NONCLUSTERED INDEX [missing_index_2179_2178]
    ON [dbo].[Llamada]([IDCliente] ASC, [Estado] ASC)
    INCLUDE([ID], [Fecha], [IDTipoLlamada], [IDVendedor], [IDResultado], [LlamarFecha]);


﻿CREATE TABLE [dbo].[DetalleNotaCreditoAcuerdo] (
    [IDProducto]                     INT   NOT NULL,
    [IDCliente]                      INT   NULL,
    [DescuentoUnitario]              MONEY NULL,
    [IDTransaccionPedidoVenta]       INT   NULL,
    [IDTransaccionPedidoNotaCredito] INT   NULL,
    [IDTransaccionVenta]             INT   NULL,
    [IDTransaccionNotaCredito]       INT   NULL
);


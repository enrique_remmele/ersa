﻿CREATE TABLE [dbo].[TablaLog] (
    [ID]          SMALLINT     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TablaLog] PRIMARY KEY CLUSTERED ([ID] ASC)
);


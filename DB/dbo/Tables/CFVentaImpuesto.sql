﻿CREATE TABLE [dbo].[CFVentaImpuesto] (
    [IDOperacion] TINYINT NOT NULL,
    [ID]          TINYINT NOT NULL,
    [IDImpuesto]  TINYINT NULL,
    CONSTRAINT [PK_CFVentaImpuesto] PRIMARY KEY CLUSTERED ([IDOperacion] ASC, [ID] ASC)
);


﻿CREATE TABLE [dbo].[cast_cliente] (
    [codigo]                    VARCHAR (50)     NOT NULL,
    [sucursal]                  TINYINT          NULL,
    [longitud]                  DECIMAL (18, 16) NULL,
    [latitud]                   DECIMAL (18, 16) NULL,
    [foto]                      IMAGE            NULL,
    [fecha]                     DATE             NULL,
    [sincronizado]              BIT              NULL,
    [ObservacionSincronizacion] VARCHAR (200)    NULL,
    [id]                        INT              NULL
);


GO
CREATE CLUSTERED INDEX [id_index]
    ON [dbo].[cast_cliente]([id] ASC);


GO
CREATE Trigger [dbo].[TI_Cast_Cliente] On [dbo].[cast_cliente]
For Insert, Update

As

Begin
	
	Set NoCount On  

	--Variables
	Declare @vCodigo varchar
	Declare @vIDCliente int
	Declare @vIDSucursal int
	Declare @vlongitud decimal(18,16) 
	Declare @vlatitud decimal(18,16)   
--	Declare @vfoto image
	Declare @vfecha date 
	Declare @vIDUsuarioModificacion int
	Declare @vMensaje varchar(200)  ='Registro no procesado'
	Declare @vProcesado bit = 'False'

	--Variables aCast
	--Declare @acodigo varchar
	--Declare @asucursal tinyint
	--Declare @alongitud decimal(18, 16)
	--Declare @alatitud decimal(18, 16)
	--Declare @afoto image
	--Declare @afecha date
	--Declare @asincronizado bit
	--Declare @afechamodificacion date
	--Declare @aIDUsuarioModificacion int
	--Declare @aMensaje varchar
	--Declare @aProcesado bit
	--Declare @aObservacionSincronizacion varchar
	--Variables aCast

	--Obtner Valores
	Begin
		Set @vIDUsuarioModificacion = (Select id from usuario where usuario = 'cast')
		--Pedido
		Set @vCodigo = (Select Codigo From inserted)
		--Consultamos Cliente
		if not exists(Select * from VCliente where Referencia = @vCodigo) begin
			if CHARINDEX('-',@vCodigo) > 0 begin
				Set @vCodigo = SUBSTRING(@vCodigo,1,CHARINDEX('-',@vCodigo)-1)
			end
		end
		--Volvemos a Consultar
		if not exists(Select * from Cliente where Referencia = @vCodigo) begin
			Set @vMensaje = 'No se encontro el cliente'
			Set @vProcesado = 'False'
			GoTo Salir
		end

		Set @vIDCliente = (Select top(1) ID from cliente where Referencia = @vCodigo)

		--Sucursal
		if @vIDSucursal > 0 begin
			if not exists (Select * from ClienteSucursal where id = @vIDSucursal and idCliente = @vIDCliente) begin
				Set @vMensaje = 'No se encontro la sucursal'
				Set @vProcesado = 'False'
				GoTo Salir
			end 
		end
		
	End

	--Varios
	Select	@vIDSucursal=Sucursal,
			@vlongitud=longitud,
			@vlatitud=latitud,
			--@vfoto = foto,
			@vfecha = fecha
	From Inserted

	if @vIDSucursal = '0' begin
		Update Cliente
			Set latitud = @vlatitud,
			longitud = @vlongitud,
			FechaModificacion=@vfecha,
			idUsuarioModificacion= @vIDUsuarioModificacion
		Where ID = @vIDCliente

		Set @vMensaje = 'Registro procesado'
		Set @vProcesado = 'True'

	end
	--select * from clientesucursal
	if @vIDSucursal > '0' begin
		Update ClienteSucursal
			Set latitud = @vlatitud,
			longitud = @vlongitud
			Where IDCliente = @vIDCliente and ID = @vIDSucursal

		Update Cliente 
			set FechaModificacion=@vfecha,
			idUsuarioModificacion= @vIDUsuarioModificacion
			Where ID = @vIDCliente

		Set @vMensaje = 'Registro procesado'
		Set @vProcesado = 'True'

	end
	
	

Salir:

	Update cast_cliente Set	Sincronizado=@vProcesado,
								ObservacionSincronizacion=@vMensaje
	Where codigo=@vCodigo
	exec CastActualizarLatitudLongitud

	--Para Probar Auditoria ACast_cliente 31-05-2021
	insert into acast_cliente (codigo,sucursal,longitud,latitud,foto,fecha,sincronizado,ObservacionSincronizacion)
		                select codigo,sucursal,longitud,latitud,foto,fecha,sincronizado,ObservacionSincronizacion
		                  from cast_cliente 
		                 Where codigo=@vCodigo
	
End
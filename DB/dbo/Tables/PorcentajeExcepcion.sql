﻿CREATE TABLE [dbo].[PorcentajeExcepcion] (
    [IDUsuario]  INT             NOT NULL,
    [Porcentaje] DECIMAL (18, 2) NOT NULL,
    [Importe]    MONEY           NULL,
    [Estado]     BIT             NULL,
    CONSTRAINT [PK_PorcentajeExcepcion] PRIMARY KEY CLUSTERED ([IDUsuario] ASC)
);


﻿CREATE TABLE [dbo].[UsuarioAutorizarPedidoNotaCredito] (
    [ID]               INT             NOT NULL,
    [IDSubMotivo]      INT             NOT NULL,
    [IDUsuario]        INT             NOT NULL,
    [MontoLimite]      DECIMAL (18, 2) NULL,
    [PorcentajeLimite] DECIMAL (18, 2) NULL,
    [Estado]           BIT             NOT NULL
);


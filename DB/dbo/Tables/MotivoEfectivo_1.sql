﻿CREATE TABLE [dbo].[MotivoEfectivo] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          NOT NULL,
    CONSTRAINT [PK_MotivoEfectivo] PRIMARY KEY CLUSTERED ([ID] ASC)
);


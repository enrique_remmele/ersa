﻿CREATE TABLE [dbo].[DetalleCuotaPrestamoBancario] (
    [IDTransaccionPrestamoBancario]      NUMERIC (18) NOT NULL,
    [IDTransaccionDebitoCreditoBancario] NUMERIC (18) NOT NULL,
    [NroCuota]                           SMALLINT     NOT NULL,
    [Fecha]                              DATE         NULL,
    [AmortizacionCapital]                MONEY        NULL,
    [Interes]                            MONEY        NULL,
    [Impuesto]                           MONEY        NULL,
    [PagoTotal]                          MONEY        NOT NULL,
    [DebitoAutomatico]                   BIT          NULL,
    CONSTRAINT [PK_DetalleCuotaPrestamoBancario] PRIMARY KEY CLUSTERED ([IDTransaccionPrestamoBancario] ASC, [IDTransaccionDebitoCreditoBancario] ASC, [NroCuota] ASC)
);


GO
CREATE TRIGGER [dbo].[trg_PagoCuota]
   ON  [dbo].[DetalleCuotaPrestamoBancario]
   AFTER INSERT, DELETE
AS 
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @IDTransaccionPrestamoBancario DECIMAL(18,6)
	DECLARE @NroCuota SMALLINT
    
	If Exists(Select * From inserted) Begin
    	--Actualizar Totales del Prestamo (Cabecera)
		SET @IDTransaccionPrestamoBancario = (Select IDTransaccionPrestamoBancario From Inserted)
		SET @NroCuota = (Select NroCuota From Inserted)

		Update DetallePrestamoBancario
		Set TotalPagado = IsNull(TotalPagado,0) + IsNull((Select PagoTotal From Inserted),0)
		Where IDTransaccion=@IDTransaccionPrestamoBancario And NroCuota = @NroCuota
	End	
	
	If Exists(Select * From deleted) Begin
		--Actualizar Totales del Prestamo (Cabecera)
		SET @IDTransaccionPrestamoBancario = (Select IDTransaccionPrestamoBancario From Deleted)
		SET @NroCuota = (Select NroCuota From Deleted)
		
		Update DetallePrestamoBancario
		--Set TotalPagado = 0
		Set TotalPagado = IsNull(TotalPagado,0) - IsNull((Select PagoTotal From deleted),0)
		Where IDTransaccion=@IDTransaccionPrestamoBancario And NroCuota = @NroCuota
	End
		
END

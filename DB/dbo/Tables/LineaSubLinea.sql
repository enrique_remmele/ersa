﻿CREATE TABLE [dbo].[LineaSubLinea] (
    [IDTipoProducto] TINYINT  NOT NULL,
    [IDLinea]        SMALLINT NOT NULL,
    [IDSubLinea]     SMALLINT NOT NULL,
    CONSTRAINT [PK_LineaSubLinea] PRIMARY KEY CLUSTERED ([IDTipoProducto] ASC, [IDLinea] ASC, [IDSubLinea] ASC),
    CONSTRAINT [FK_LineaSubLinea_SubLinea] FOREIGN KEY ([IDSubLinea]) REFERENCES [dbo].[SubLinea] ([ID]),
    CONSTRAINT [FK_LineaSubLinea_TipoProductoLinea] FOREIGN KEY ([IDTipoProducto], [IDLinea]) REFERENCES [dbo].[TipoProductoLinea] ([IDTipoProducto], [IDLinea]) ON DELETE CASCADE ON UPDATE CASCADE
);


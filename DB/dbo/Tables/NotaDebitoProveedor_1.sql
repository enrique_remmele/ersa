﻿CREATE TABLE [dbo].[NotaDebitoProveedor] (
    [IDTransaccion]            NUMERIC (18) NOT NULL,
    [IDTipoComprobante]        SMALLINT     NOT NULL,
    [Numero]                   INT          NOT NULL,
    [NroComprobante]           VARCHAR (50) NOT NULL,
    [NroTimbrado]              INT          NOT NULL,
    [IDProveedor]              INT          NOT NULL,
    [IDSucursal]               TINYINT      NOT NULL,
    [IDDeposito]               TINYINT      NOT NULL,
    [Fecha]                    DATE         NOT NULL,
    [IDMoneda]                 TINYINT      NOT NULL,
    [Cotizacion]               MONEY        NULL,
    [Observacion]              VARCHAR (50) NULL,
    [Total]                    MONEY        NOT NULL,
    [TotalImpuesto]            MONEY        NOT NULL,
    [TotalDiscriminado]        MONEY        NOT NULL,
    [TotalDescuento]           MONEY        NOT NULL,
    [Saldo]                    MONEY        NOT NULL,
    [Anulado]                  BIT          NOT NULL,
    [FechaAnulado]             DATE         NULL,
    [FechaVencimientoTimbrado] DATE         NULL,
    [IDUsuarioAnulado]         SMALLINT     NULL,
    [Procesado]                BIT          NULL,
    [Devolucion]               BIT          NULL,
    [Descuento]                BIT          NULL
);


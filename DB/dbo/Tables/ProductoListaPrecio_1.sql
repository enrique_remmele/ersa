﻿CREATE TABLE [dbo].[ProductoListaPrecio] (
    [IDListaPrecio] INT             NOT NULL,
    [IDProducto]    INT             NOT NULL,
    [IDMoneda]      TINYINT         NOT NULL,
    [IDSucursal]    TINYINT         NOT NULL,
    [Precio]        MONEY           NOT NULL,
    [TPR]           MONEY           NULL,
    [TPRPorcentaje] DECIMAL (10, 3) NULL,
    [TPRDesde]      DATE            NULL,
    [TPRHasta]      DATE            NULL,
    [FactorVenta]   DECIMAL (10, 4) NULL,
    CONSTRAINT [PK_ProductoListaPrecio] PRIMARY KEY CLUSTERED ([IDListaPrecio] ASC, [IDProducto] ASC, [IDMoneda] ASC, [IDSucursal] ASC),
    CONSTRAINT [FK_ProductoListaPrecio_ListaPrecio] FOREIGN KEY ([IDListaPrecio]) REFERENCES [dbo].[ListaPrecio] ([ID]),
    CONSTRAINT [FK_ProductoListaPrecio_Moneda] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[Moneda] ([ID]),
    CONSTRAINT [FK_ProductoListaPrecio_Producto] FOREIGN KEY ([IDProducto]) REFERENCES [dbo].[Producto] ([ID]),
    CONSTRAINT [FK_ProductoListaPrecio_Sucursal] FOREIGN KEY ([IDSucursal]) REFERENCES [dbo].[Sucursal] ([ID])
);


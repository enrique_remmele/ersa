﻿CREATE TABLE [dbo].[RegistroCosto] (
    [IDProducto]          INT             NOT NULL,
    [UltimaActualizacion] DATETIME        NULL,
    [UltimoCosto]         MONEY           NULL,
    [Cantidad]            DECIMAL (10, 2) NULL,
    [Importe]             MONEY           NULL,
    [Promedio]            MONEY           NULL,
    CONSTRAINT [PK_RegistroCosto_2] PRIMARY KEY CLUSTERED ([IDProducto] ASC)
);


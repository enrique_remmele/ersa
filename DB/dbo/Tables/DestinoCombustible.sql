﻿CREATE TABLE [dbo].[DestinoCombustible] (
    [ID]          TINYINT      NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Estado]      BIT          NULL,
    CONSTRAINT [PK_DestinoCombustible] PRIMARY KEY CLUSTERED ([ID] ASC)
);


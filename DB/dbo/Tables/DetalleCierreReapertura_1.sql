﻿CREATE TABLE [dbo].[DetalleCierreReapertura] (
    [IDTransaccion]    NUMERIC (18) NOT NULL,
    [ID]               NUMERIC (18) NOT NULL,
    [IDCuentaContable] NUMERIC (18) NOT NULL,
    [Total]            MONEY        NOT NULL,
    [Debito]           MONEY        NOT NULL,
    [Credito]          MONEY        NOT NULL,
    [IDSucursal]       TINYINT      NULL,
    [Comprobante]      VARCHAR (50) NOT NULL,
    [NroComprobante]   NUMERIC (18) NOT NULL,
    CONSTRAINT [PK_DetalleCierreReapertura] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC, [IDCuentaContable] ASC, [ID] ASC)
);


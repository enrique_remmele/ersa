﻿CREATE TABLE [dbo].[VentaImportada] (
    [IDTransaccion]   NUMERIC (18) NOT NULL,
    [Comprobante]     VARCHAR (20) NULL,
    [TipoComprobante] VARCHAR (10) NULL,
    [Timbrado]        VARCHAR (15) NULL,
    [Importe]         MONEY        CONSTRAINT [DF_VentaImportada_Importe] DEFAULT ((0)) NULL,
    [Cobrado]         MONEY        CONSTRAINT [DF_VentaImportada_Cobrado] DEFAULT ((0)) NULL,
    [Saldo]           MONEY        CONSTRAINT [DF_VentaImportada_Saldo] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_VentaImportada] PRIMARY KEY CLUSTERED ([IDTransaccion] ASC)
);


﻿
--USE [SAIN1]
--GO

--/****** Object:  View [dbo].[VDetalleAjusteInicial]    Script Date: 08/19/2013 10:25:56 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

CREATE View [dbo].[VDetalleAjusteInicial]

As

Select

--Cabecera
DA.IDTransaccion,

--Producto
DA.IDProducto,
'Producto'=P.Descripcion,
P.Referencia,
P.CodigoBarra,
P.Peso,

--Lugar
DA.IDSucursal,
'Sucursal'=S.Descripcion,
DA.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidades
DA.ExistenciaSistema,
DA.Existencia,
DA.Diferencia,
DA.Entrada,
DA.Salida

From DetalleAjusteInicial DA
Join AjusteInicial A On DA.IDTransaccion=A.IDTransaccion
Join Sucursal S On DA.IDSucursal=S.ID
Join Deposito D On DA.IDDeposito=D.ID And S.ID=D.IDSucursal
Join Producto P On DA.IDProducto=P.ID





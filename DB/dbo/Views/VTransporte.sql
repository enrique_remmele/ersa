﻿
CREATE View [dbo].[VTransporte]
As

Select 
T.ID,
T.Descripcion,

T.IDZonaVenta,
'Zona'=IsNull(Z.Descripcion, ''),

T.IDDistribuidor,
'Distribuidor'=IsNull(D.Nombres, ''),

T.IDCamion,
'Camion'=IsNull(CA.Descripcion, ''),

T.IDChofer,
'Chofer'=IsNull(C.Nombres,'')

From Transporte T
Left Outer Join ZonaVenta Z On T.IDZonaVenta=Z.ID
Left Outer Join Distribuidor D On T.IDDistribuidor=D.ID
Left Outer Join Chofer C On T.IDChofer=C.ID
Left Outer Join Camion CA On T.IDCamion=CA.ID




﻿CREATE view vSolicitudEliminacionOrdenPagoEgreso 
as
select 
OP.Numero,
S.IDTransaccionOrdenPago,
S.IDTransaccionEgreso,
OP.Fecha,
E.Comprobante,
S.Cuota,
E.Proveedor,
E.Total,
S.IDUsuarioSolicitud,
'UsuarioSolicitud'=US.Nombre,
S.FechaSolicitud,
'Aprobado'=(Case when S.Aprobado is null then 'PENDIENTE' else 
			(Case when S.Aprobado = 'True' then 'APROBADO' else
			(Case when S.Aprobado = 'False' then 'RECHAZADO' end) end) end),
S.IDUsuarioAprobador,
'UsuarioAprobador'=UA.Nombre,
S.FechaAprobacion
from SolicitudEliminacionOrdenPagoEgreso S
join OrdenPago OP on S.IDTransaccionOrdenPago = OP.IDTransaccion
join VEgreso E on S.IDTransaccionEgreso = E.IDTransaccion
Join Usuario US on S.IDUsuarioSolicitud = US.ID
left outer join Usuario UA on S.IDUsuarioAprobador = UA.ID

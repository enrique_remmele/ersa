﻿
CREATE View [dbo].[VChequesClienteFormaPago]
As

Select
V.IDTransaccion,
'ID'=0,
'Sel'='False',
V.Ciudad,
V.Banco,
V.NroCheque,
V.CuentaBancaria,
'Fecha'=convert(varchar(50),V.Fecha,3),
V.Fec,
'FechaVencimiento'=convert(varchar(50),V.FechaVencimiento,3),
V.Moneda,
V.ImporteMoneda,
V.Cotizacion,
'Total'=V.Importe,
V.Saldo,
'SaldoGS'=
(Case When (V.IDMOneda)= 1 Then round(V.Saldo * 1,0) 
			Else round(V.Saldo * (Select top(1)isnull(cotizacion,1) from VCotizacion where IDMoneda = V.IDmoneda order by fecha Desc),0) End),
'Importe'=V.Saldo,
'ImporteGS'=
(Case When (V.IDMOneda)= 1 Then round(V.Saldo * 1,0) 
			Else round(V.Saldo * (Select top(1)isnull(cotizacion,1) from VCotizacion where IDMoneda = V.IDmoneda order by fecha Desc),0) End),
V.IDCliente,
V.Cliente,
V.Tipo,
V.Diferido,
V.IDMoneda,
'Cancelar'='False',
'CotizacionHoy'= (Case When (V.IDMoneda)= 1 Then 1 Else (Select top(1)(cotizacion) from VCotizacion where IDMOneda = V.IDmoneda order by fecha Desc) End)


From VChequeCliente V
Where ((V.Cancelado='False') or (saldo > 0)) 















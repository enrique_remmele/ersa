﻿
CREATE View [dbo].[VDetalleImpuestoDesglosado]
As
Select IDTransaccion, 'IVA10%'=IsNull([1],0), 'IVA5%'=IsNull([2],0), 'EX'=IsNull([3],0)
From (SELECT IDTransaccion, IDImpuesto, TotalImpuesto
    FROM VDetalleImpuesto) AS SourceTable
PIVOT
(
Sum(TotalImpuesto)
FOR IDImpuesto IN ([1], [2], [3])
) AS PivotTable;

﻿Create View [dbo].[VDetalleVentaParaNotaDebito]
As
Select 

--Producto
P.ID, 
P.Descripcion, 
P.CodigoBarra, 
P.Ref, 
P.[Ref Imp], 
P.UnidadPorCaja, 

--Impuesto
P.IDImpuesto,
P.Impuesto,
P.Exento, 
P.Costo, 

--Detalle
DV.IDTransaccion, 
DV.PrecioUnitario, 
DV.Cantidad, 
'SaldoCantidad'=0,
DV.Total,

--Descuento
DV.PorcentajeDescuento,
DV.DescuentoUnitario,
DV.DescuentoUnitarioDiscriminado,
DV.TotalDescuento,
DV.TotalDescuentoDiscriminado,
DV.TotalNetoConDescuentoNeto,

--Venta
V.Cobrado,
V.Descontado,
V.Saldo,

V.IDCliente,
V.Cancelado, 
'Dias'=DATEDIFF(DD, V.FechaEmision, GetDate())

From Venta V 
Join VDetalleVenta DV On DV.IDTransaccion=V.IDTransaccion 
Join VProducto P On DV.IDProducto=P.ID





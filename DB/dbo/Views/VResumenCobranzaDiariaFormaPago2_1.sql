﻿CREATE VIEW [dbo].[VResumenCobranzaDiariaFormaPago2]
AS

----1910
SELECT  
'Fecha' = CONVERT(Date, C.FechaEmision),
'TipoComprobante' = E.CodigoComprobante,
C.IDSucursal,
'Cantidad' = COUNT(*), 
'Valores' = SUM(E.ImporteMoneda),
'Planilla' = CONVERT(varchar(50),
C.NroPlanilla),
E.Comprobante,
E.IDTipoComprobante,
C.IDCobrador, C.IDCliente, 
C.IDVendedor,
C.IdSucursal as IdSucursalOperacion,
T.IDUsuario,

--Monena
E.IDMoneda,
E.Moneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM FormaPago FP
JOIN VCobranzaCredito C ON FP.IDTransaccion = C.IDTransaccion
JOIN VEfectivo E ON FP.IDTransaccion = E.IDTransaccion And FP.ID=E.ID
JOIN Transaccion T on T.ID = C.IDTransaccion
WHERE     FP.Efectivo = 'True' AND C.Anulado = 'False'
GROUP BY C.FechaEmision, C.IDSucursal, C.NroPlanilla, C.IDCobrador, E.CodigoComprobante, E.IDTipoComprobante, E.Comprobante, C.IDCliente, 
C.IDVendedor, C.IdSucursal, T.IDUsuario, E.IDMoneda, E.Moneda, C.IDTipoComprobante

UNION ALL

SELECT
'Fecha' = CONVERT(Date, C.FechaEmision),
'TipoComprobante' = CHQ.CodigoTipo,
C.IDSucursal, 
'Cantidad' = COUNT(*), 
'Valores' = SUM(FP.ImporteCheque),
'Planilla' = CONVERT(varchar(50), 
C.NroPlanilla),
CONVERT(varchar(50),
C.NroComprobante), 
C.IDTipoComprobante,
C.IDCobrador,
C.IDCliente,
C.IDVendedor,
C.IdSucursal as IdSucursalOperacion,
T.IDUsuario,

--Monena
CHQ.IDMoneda,
CHQ.Moneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM FormaPago FP
JOIN VCobranzaCredito C ON FP.IDTransaccion = C.IDTransaccion
JOIN VChequeCliente CHQ ON FP.IDTransaccionCheque = CHQ.IDTransaccion
join TRansaccion T on T.id = c.IDTransaccion
WHERE     FP.Cheque = 'True' AND C.Anulado = 'False'
GROUP BY C.FechaEmision, C.IDSucursal, CHQ.CodigoTipo, C.NroPlanilla, C.IDCobrador, C.NroComprobante, 
C.IDTipoComprobante, C.IDCliente, C.IDVendedor, C.IdSucursal, T.IDUsuario, CHQ.IDMoneda, CHQ.Moneda

UNION ALL

SELECT
'Fecha' = CONVERT(Date, C.FechaEmision),
'TipoComprobante' = TC.Codigo,
C.IDSucursal,
'Cantidad' = COUNT(*), 
'Valores' = SUM(FP.ImporteDocumento),
'Planilla' = CONVERT(varchar(50), C.NroPlanilla),
CONVERT(varchar(50),
C.NroComprobante), 
C.IDTipoComprobante,
C.IDCobrador,
C.IDCliente,
C.IDVendedor,
C.IdSucursal as IdSucursalOperacion,
T.IdUsuario,

--Monena
FPD.IDMoneda,
M.Referencia,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM FormaPago FP
JOIN VCobranzaCredito C ON FP.IDTransaccion = C.IDTransaccion
JOIN FormaPagoDocumento FPD ON FP.IDTransaccion = FPD.IDTransaccion AND FP.ID = FPD.ID 
JOIN TipoComprobante TC ON FPD.IDTipoComprobante = TC.ID
JOIN Transaccion T on T.ID = C.IDTransaccion
Join Moneda M On FPD.IDMoneda=M.ID
WHERE FP.Documento = 'True' AND C.Anulado = 'False'
GROUP BY C.FechaEmision, C.IDSucursal, TC.Codigo, C.NroPlanilla, C.IDCobrador, C.NroComprobante, 
C.IDTipoComprobante, C.IDCliente, C.IDVendedor, C.IdSucursal, T.IDUsuario, FPD.IDMoneda, M.Referencia

union all 

--Tarjetas
SELECT
'Fecha' = CONVERT(Date, C.FechaEmision),
'TipoComprobante' = TC.Codigo,
C.IDSucursal,
'Cantidad' = COUNT(*), 
'Valores' = SUM(FP.Importe),
'Planilla' = CONVERT(varchar(50), C.NroPlanilla),
CONVERT(varchar(50),
C.NroComprobante), 
C.IDTipoComprobante,
C.IDCobrador,
C.IDCliente,
C.IDVendedor,
C.IdSucursal as IdSucursalOperacion,
T.IdUsuario,

--Monena
FPD.IDMoneda,
M.Referencia,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM FormaPago FP
JOIN VCobranzaCredito C ON FP.IDTransaccion = C.IDTransaccion
JOIN FormaPagoTarjeta FPD ON FP.IDTransaccion = FPD.IDTransaccion AND FP.ID = FPD.ID 
JOIN TipoComprobante TC ON FPD.IDTipoComprobante = TC.ID
JOIN Transaccion T on T.ID = C.IDTransaccion
Join Moneda M On FPD.IDMoneda=M.ID
WHERE C.Anulado = 'False'
GROUP BY C.FechaEmision, C.IDSucursal, TC.Codigo, C.NroPlanilla, C.IDCobrador, C.NroComprobante, 
C.IDTipoComprobante, C.IDCliente, C.IDVendedor, C.IdSucursal, T.IDUsuario, FPD.IDMoneda, M.Referencia

UNION ALL

SELECT 
'Fecha' = CONVERT(Date, C.FechaEmision),
'TipoComprobante' = V.TipoComprobante,
C.IDSucursal,
'Cantidad' = Count(*),
'Cobrado' = Sum(VC.Importe), 
'Planilla' = CONVERT(varchar(50),
C.NroPlanilla),
CONVERT(varchar(50),
C.NroComprobante),
C.IDTipoComprobante,
C.IDCobrador,
C.IDCliente, 
C.IDVendedor,
C.IdSucursal as IdSucursalOperacion,
T.IdUsuario,

--Monena
V.IDMoneda,
V.Moneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM VCobranzaCredito C
JOIN VentaCobranza VC ON C.IDTransaccion = VC.IDTransaccionCobranza
JOIN VVenta V ON VC.IDTransaccionVenta = V.IDTransaccion
JOIN Transaccion T on T.ID = C.IDTransaccion

WHERE     C.Anulado = 'False'
GROUP BY C.FechaEmision, V.TipoComprobante, V.IDTipoComprobante, C.IDSucursal, C.NroPlanilla, C.IDCobrador, C.NroComprobante, C.IDTipoComprobante, 
C.IDCliente, C.IDVendedor, C.IdSucursal, T.IdUsuario, V.IDMoneda, V.Moneda

UNION ALL

SELECT
'Fecha' = CONVERT(Date, C.Fecha),
'TipoComprobante' = E.CodigoComprobante,
C.IDSucursal,
'Cantidad' = COUNT(*), 
'Valores' = SUM(FP.Importe),
'Planilla' = CONVERT(varchar(50),
C.Comprobante),
CONVERT(varchar(50),
E.Comprobante), 
E.IDTipoComprobante,
0,
0, 
0,
C.IdSucursal as IdSucursalOperacion,
T.IDUsuario,

--Monena
E.IDMoneda,
E.Moneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM         FormaPago FP
JOIN VCobranzaContado C ON FP.IDTransaccion = C.IDTransaccion
JOIN VEfectivo E ON FP.IDTransaccion = E.IDTransaccion
JOIN Transaccion T on T.ID = C.IDTransaccion
WHERE     FP.Efectivo = 'True' AND C.Anulado = 'False'
GROUP BY C.Fecha, C.IDSucursal, C.Comprobante, E.Comprobante, E.CodigoComprobante, E.IDTipoComprobante,
C.IdSucursal, T.IDUsuario, E.IDMoneda, E.Moneda,C.IDTipoComprobante

UNION ALL

SELECT  
'Fecha' = CONVERT(Date, C.Fecha),
'TipoComprobante' = CHQ.CodigoTipo,
C.IDSucursal,
'Cantidad' = COUNT(*),
'Valores' = SUM(FP.ImporteCheque), 
'Planilla' = CONVERT(varchar(50), C.Comprobante),
CONVERT(varchar(50), C.NroComprobante),
C.IDTipoComprobante,
0,
0,
0, 
C.IdSucursal as IdSucursalOperacion,
T.IdUsuario,

--Monena
CHQ.IDMoneda,
CHQ.Moneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM FormaPago FP 
JOIN VCobranzaContado C ON FP.IDTransaccion = C.IDTransaccion
JOIN VChequeCliente CHQ ON FP.IDTransaccionCheque = CHQ.IDTransaccion
JOIN Transaccion T on T.ID = C.IDTransaccion
WHERE     FP.Cheque = 'True' AND C.Anulado = 'False'
GROUP BY C.Fecha, C.IDSucursal, CHQ.CodigoTipo, C.NroComprobante, C.IDTipoComprobante, C.Comprobante,
C.IdSucursal, T.IDUsuario, CHQ.IDMoneda, CHQ.Moneda

UNION ALL

SELECT
'Fecha' = CONVERT(Date, C.Fecha),
'TipoComprobante' = TC.Codigo,
C.IDSucursal,
'Cantidad' = COUNT(*), 
'Valores' = SUM(FP.ImporteDocumento),
'Planilla' = CONVERT(varchar(50),
C.Comprobante),
CONVERT(varchar(50),
C.NroComprobante), 
C.IDTipoComprobante, 
0, 
0,
0,
C.IDSucursal as IdSucursalOperacion,
T.IdUsuario,

--Monena
FPD.IDMoneda,
M.Referencia,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM FormaPago FP
JOIN VCobranzaContado C ON FP.IDTransaccion = C.IDTransaccion
JOIN FormaPagoDocumento FPD ON FP.IDTransaccion = FPD.IDTransaccion AND FP.ID = FPD.ID 
JOIN TipoComprobante TC ON FPD.IDTipoComprobante = TC.ID
JOIN Transaccion T on T.ID = C.IDTransaccion
Join Moneda M On FPD.IDMoneda=M.ID

WHERE     FP.Documento = 'True' AND C.Anulado = 'False'
GROUP BY C.Fecha, C.IDSucursal, TC.Codigo, C.NroComprobante, C.IDTipoComprobante, C.Comprobante, C.IdSucursal, T.IDUsuario, FPD.IDMoneda, M.Referencia

union all

--tarjeta cobrnza por lote
SELECT
'Fecha' = CONVERT(Date, C.Fecha),
'TipoComprobante' = TC.Codigo,
C.IDSucursal,
'Cantidad' = COUNT(*), 
'Valores' = SUM(FP.Importe),
'Planilla' = CONVERT(varchar(50),
C.Comprobante),
CONVERT(varchar(50),
C.NroComprobante), 
C.IDTipoComprobante, 
0, 
0,
0,
C.IDSucursal as IdSucursalOperacion,
T.IdUsuario,

--Monena
FPD.IDMoneda,
M.Referencia,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM FormaPago FP
JOIN VCobranzaContado C ON FP.IDTransaccion = C.IDTransaccion
JOIN FormaPagoTarjeta FPD ON FP.IDTransaccion = FPD.IDTransaccion AND FP.ID = FPD.ID 
JOIN TipoComprobante TC ON FPD.IDTipoComprobante = TC.ID
JOIN Transaccion T on T.ID = C.IDTransaccion
Join Moneda M On FPD.IDMoneda=M.ID

WHERE  C.Anulado = 'False'
GROUP BY C.Fecha, C.IDSucursal, TC.Codigo, C.NroComprobante, C.IDTipoComprobante, C.Comprobante, C.IdSucursal, T.IDUsuario, FPD.IDMoneda, M.Referencia
UNION ALL

SELECT  
'Fecha' = CONVERT(Date, C.Fecha),
'TipoComprobante' = V.TipoComprobante,
C.IDSucursal, 
'Cantidad' = Count(*),
'Cobrado' = Sum(VC.Importe), 
'Planilla' = CONVERT(varchar(50), C.Comprobante),
CONVERT(varchar(50), C.NroComprobante),
C.IDTipoComprobante,
0,
0,
0,
C.IDSucursal as IdSucursalOperacion,
T.IDUsuario,

--Monena
V.IDMoneda,
V.Moneda,
'IDTipoComprobanteCobranza'=C.IDTipoComprobante

FROM VCobranzaContado C 
JOIN VentaCobranza VC ON C.IDTransaccion = VC.IDTransaccionCobranza
JOIN VVenta V ON VC.IDTransaccionVenta = V.IDTransaccion
JOIN Transaccion T on T.ID = C.IDTransaccion
WHERE     C.Anulado = 'False'
GROUP BY C.Fecha, V.TipoComprobante, V.IDTipoComprobante, C.IDSucursal, C.Comprobante, C.NroComprobante, C.IDTipoComprobante, C.Comprobante,
C.IDSucursal, T.IDUsuario, V.IDMoneda, V.Moneda

UNION ALL

--/*EFECTIVIZACIONES - EFECTIVO*/
SELECT
'Fecha' = CONVERT(Date, E.Fecha),
'TipoComprobante' = E.TipoComprobante,
E.IDSucursal, 
'Cantidad' = COUNT(*), 
'Valores' = SUM(DE.Importe) *-1, 
'Planilla' = CONVERT(varchar(50), EFE.Comprobante),
CONVERT(varchar(50), EFE.Comprobante), 
E.IDTipoComprobante,
0,
0,
0,
0,
T.IDUsuario,

--Monena
EFE.IDMoneda,
M.Referencia,
'IDTipoComprobanteCobranza'=E.IDTipoComprobante

FROM VEfectivizacion E
JOIN DetalleEfectivizacion DE ON E.IDTransaccion = DE.IDTransaccion 
JOIN Efectivo EFE ON DE.IDTransaccionEfectivo = EFE.IDTransaccion
JOIN Transaccion T on T.ID = E.IDTransaccion
Join Moneda M On EFE.IDMoneda=M.ID
WHERE E.Anulado = 'False'
GROUP BY E.Fecha, E.IDSucursal, EFE.Comprobante, E.TipoComprobante, E.IDTipoComprobante, EFE.Comprobante, T.IDUsuario, EFE.IDMoneda, M.Referencia

UNION ALL

--/*EFECTIVIZACIONES - CHEQUES*/
SELECT 
'Fecha' = CONVERT(Date, E.Fecha),
'TipoComprobante' = CHQ.CodigoTipo,
E.IDSucursal,
'Cantidad' = COUNT(*), 
'Valores' = SUM(CHQ.ImporteMoneda),
'Planilla' = CONVERT(varchar(50), CHQ.NroCheque),
CONVERT(varchar(50), CHQ.Numero), 
E.IDTipoComprobante,
0, 
0, 
0,
0,
T.IDUsuario,

--Monena
CHQ.IDMoneda,
CHQ.Moneda,
'IDTipoComprobanteCobranza'=E.IDTipoComprobante

FROM VEfectivizacion E 
JOIN VChequeCliente CHQ ON E.IDTransaccionCheque = CHQ.IDTransaccion
JOIN Transaccion T on T.ID = E.IDTransaccion
WHERE E.Anulado = 'False'
GROUP BY E.Fecha, E.IDSucursal, CHQ.NroCheque, CHQ.CodigoTipo, E.IDTipoComprobante, CHQ.Numero, T.IdUsuario, CHQ.IDMoneda, CHQ.Moneda


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VResumenCobranzaDiariaFormaPago2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VResumenCobranzaDiariaFormaPago2';


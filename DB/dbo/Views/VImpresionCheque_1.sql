﻿



--select * from VImpresionCheque
CREATE View [dbo].[VImpresionCheque]

As

Select 
C.[Suc],
C.[Numero],
C.Fecha,
'Fecha2'=C.Fecha,
C.Dia,
C.Mes,
C.Año,
'AñoCorto'=SUBSTRING(cast(C.Año as varchar),3,4),
'MesCorto'=(Case When MONTH(C.fechaFiltro) < 10 then concat('0',cast(month(C.FechaFiltro) as varchar)) else concat(month(C.FechaFiltro),'') end ),
'DiaCorto'=(Case When DAY(C.fechaFiltro) < 10 then concat('0',cast(day(C.FechaFiltro) as varchar)) else concat(day(C.FechaFiltro),'') end ),
'A la Orden'=Concat(C.ALaOrden,'======================'),
'A la Orden1'=Concat(C.ALaOrden,'======================'),
--'Importe'=Cast(C.ImporteMoneda as varchar),
--'Importe2'=cast(C.ImporteMoneda as varchar),
'Importe'=Concat(Replace(Replace(Replace((Case when C.IDMoneda = 1 then Replace(CONVERT(varchar(17), CAST(C.ImporteMoneda AS money), 1),'.00','') else CONVERT(varchar(17), CAST(C.ImporteMoneda AS money), 1) end),'.','-'),',','.'),'-',','),'===='),
'Importe2'=Concat(Replace(Replace(Replace((Case when C.IDMoneda = 1 then Replace(CONVERT(varchar(17), CAST(C.ImporteMoneda AS money), 1),'.00','') else CONVERT(varchar(17), CAST(C.ImporteMoneda AS money), 1) end),'.','-'),',','.'),'-',','),'===='),
--'Importe en Letras'=dbo.FCantidadConLetra(C.ImporteMoneda),
'Importe en Letras'=Concat((Case When (C.IDMoneda)= 1 Then dbo.FCantidadConLetra(C.ImporteMoneda) Else UPPER(dbo.ConvertirNumero(C.ImporteMoneda)) End),'======================'),
--'Concepto'=OP.Observacion,
'Concepto'=concat('OP Nro - ',OP.Numero),
C.IDTransaccion,
C.DiaVencimiento,
C.MesVencimiento,
C.AñoVencimiento,
C.IDMoneda,
C.Moneda,
'FechaVencimiento'=Convert(varchar(50),C.FechaVencimiento,5),
'AñoCortoVencimiento'=SUBSTRING(cast(C.AñoVencimiento as varchar),3,4),
'MesCortoVencimiento'=(Case When MONTH(C.FechaVencimiento) < 10 then concat('0',cast(month(C.FechaVencimiento) as varchar)) else concat(month(C.FechaVencimiento),'') end ),
'DiaCortoVencimiento'=(Case When DAY(C.FechaVencimiento) < 10 then concat('0',cast(day(C.FechaVencimiento) as varchar)) else concat(day(C.FechaVencimiento),'') end )


From VCheque C
Join OrdenPago OP On C.IDTransaccion=OP.IDTransaccion




--select * from vcheque where nrocheque = '27709746'
--select * from VImpresionCheque where idtransaccion = 1122347











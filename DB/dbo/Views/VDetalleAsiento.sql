﻿
CREATE View [dbo].[VDetalleAsiento]
As
Select 
DA.IDTransaccion,
DA.IDCuentaContable,
'Codigo'=(IsNull(CC.Codigo, DA.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
'Cuenta'=IsNull(CC.Codigo, DA.CuentaContable) + ' - ' + IsNull(CC.Descripcion, '---'),
'Alias'=IsNull(CC.Alias, '---'),
DA.ID,
DA.Credito,
DA.Debito,
DA.Importe,
'Observacion'=(Case When DA.Observacion Is Null Then A.Detalle Else (Case When DA.Observacion != '' Then DA.Observacion Else A.Detalle End) End),
'Orden'=DA.ID,
'CodSucursal'=S.Codigo,
M.Referencia as Moneda,
A.Fecha,

'IDSucursal'=IsNull(DA.IDSucursal, A.IDSucursal),
DA.TipoComprobante,
'NroComprobante'=(Case When (DA.NroComprobante) Is Null Then A.NroComprobante 
					   When (DA.NroComprobante) = '' Then A.NroComprobante 
					   Else (DA.NroComprobante) End),

--Unidad de negocio
--'IDUnidadNegocio'= IsNull(DA.IDUnidadNegocio, ISnull(A.IDUnidadNegocio,0)),	 Se saca el dato de la cabecera
'IDUnidadNegocio'= IsNull(CC.IDUnidadNegocio, 0),	
'CodigoUnidadNegocio'=Isnull((Select Top(1) Codigo From UnidadNegocio U Where U.ID= IsNull(CC.IDUnidadNegocio, A.IDUnidadNegocio)),'---'),
'UnidadNegocio'=Isnull((Select Top(1) Descripcion From UnidadNegocio U Where U.ID= IsNull(CC.IDUnidadNegocio, A.IDUnidadNegocio)),'---'),
'IDUnidadNegocioActualCuenta' = ISNULL(CC.IDUnidadNegocio,0),
--Centro de Costos
--'IDCentroCosto'=IsNull(DA.IDCentroCosto, ISnull(A.IDCentroCosto,0)), Se saca el dato de la cabecera
'IDCentroCosto'=IsNull(CC.IDCentroCosto, 0),
'CodigoCentroCosto'=ISnull((Select Top(1) Codigo From CentroCosto CCo Where CCo.ID= IsNull(CC.IDCentroCosto, A.IDCentroCosto)),'---'),
'CentroCosto'=Isnull((Select Top(1) Descripcion From CentroCosto CCo Where CCo.ID= IsNull(CC.IDCentroCosto, A.IDCentroCosto)),'---'),
--(DA.Debito / (Select Cotizacion from Asiento where Asiento.idtransaccion = DA.Idtransaccion)) as DebitoDolares,
--(DA.Credito / (Select Cotizacion from Asiento where Asiento.idtransaccion = DA.Idtransaccion)) as CreditoDolares
(DA.Debito / (Select (case when Cotizacion = 0 then 1 when Cotizacion <> 0 then Cotizacion end) as Cotizacion from Asiento where Asiento.idtransaccion = DA.Idtransaccion)) as DebitoDolares,
(DA.Credito / (Select (case when Cotizacion = 0 then 1 when Cotizacion <> 0 then Cotizacion end) as Cotizacion from Asiento where Asiento.idtransaccion = DA.Idtransaccion)) as CreditoDolares,
'FechaTransaccion'= (Select Fecha from Transaccion where ID = DA.IDTransaccion),
'Operacion'=(select Descripcion from operacion where id = (select idoperacion from transaccion where id = DA.IDTransaccion)),
'IDOperacion'=(select idoperacion from transaccion where id = DA.IDTransaccion),
A.Bloquear,
A.Conciliado


From DetalleAsiento DA
Join Asiento A On DA.IDTransaccion=A.IDTransaccion
Left Outer Join VCuentaContable CC On DA.CuentaContable=CC.Codigo 
Left Outer Join Sucursal S on DA.IDSucursal=S.ID
Join Moneda M on A.IDMoneda=M.ID

Where CC.PlanCuentaTitular='True'



















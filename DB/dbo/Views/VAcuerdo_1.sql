﻿
CREATE View [dbo].[VAcuerdo]
As
Select 
A.Numero,
A.Fecha,
A.IDProveedor,
A.NroAcuerdo,
A.IDMoneda,
A.IDProducto,
A.Cantidad,
A.Precio,
'PrecioDiscriminado'= A.Precio / Pr.FactorDiscriminado,
A.TipoFlete,
A.Zafra,
A.Observacion,
A.Anulado,
A.IDUsuarioAnulacion,
A.FechaAnulacion,
A.Finalizado,
A.FechaDesdeEmbarque,
A.FechaHastaEmbarque,
'CostoFlete'=Isnull(A.CostoFlete,0),
A.IDMonedaFlete,
'ReferenciaProveedor'=P.Referencia,
'Proveedor'=P.RazonSocial,
'ReferenciaMoneda'=M.Referencia,
'Moneda'=M.Descripcion,
'ReferenciaProducto'=Pr.Referencia,
'Producto'= Pr.Descripcion,
'UsuarioAnulacion'=IsNull((Select Usuario from Usuario where ID = A.IDUsuarioAnulacion),''),
'CCProvision'='',
Pr.IDTipoProducto,
'CCProducto'=(Case when Pr.IDTipoProducto = 24 then '11121202' else '11121203' end), -- 24 trigo 25 balanceado
'CCaRecibir'=(Case when Pr.IDTipoProducto = 24 then '11121210' else '11121212' end),
'UnidadMedida'=(Select Descripcion from UnidadMedida where ID =  Pr.IDUnidadMedida),
'ReferenciaMonedaFlete'=MF.Referencia,
'MonedaFlete'=MF.Descripcion,
'CantidadAsociada'= Isnull((Select Sum(PesoBascula) from TicketBascula where NroAcuerdo = A.NroAcuerdo and Anulado = 0),0),
'CantidadAsociadaValorizadaGs'= Isnull((Select Sum(Total+CostoAdicional) from TicketBascula where NroAcuerdo = A.NroAcuerdo and Anulado = 0),0)
From Acuerdo A
Join Proveedor P on P.ID = A.IDProveedor
Join Moneda M on M.ID = A.IDMoneda
Join vProducto Pr on Pr.ID = A.IDProducto
left Join Moneda MF on MF.ID = A.IDMonedaFlete














﻿Create View VDetalleRetencion
As
Select 

RI.IDTransaccion,
'IDTransaccionOrdenPago'=NULL,
'IDTransaccionEgreso'=DR.IDTransaccion,
'ID'=DR.ID,
DR.TipoyComprobante,

'Fecha'=CONVERT(varchar(50), DR.Fecha, 5),
'Credito'=E.Credito,
Gravado5,
IVA5,
Gravado10,
IVA10,
Total5,
Total10,
'TotalGravado'=(Gravado10+Gravado5),
'TotalIVA'=(IVA5+IVA10),
'IVA'=(IVA5+IVA10),
'Total'=(Total5+Total10),
'PorcRetencion'=(Select Top 1 CompraPorcentajeRetencion From Configuraciones),
'RetencionIVA'=DR.RetencionIVA ,
'PorcRenta'=0,
'Renta'=0,

--Egreso
'Comprobante'=E.Comprobante,
E.TotalDiscriminado,
E.TotalImpuesto

From DetalleRetencion DR
Join VRetencionIVA RI on DR.IDTransaccion=RI.IDTransaccion
Left Outer Join VEgresos E On DR.IDTransaccionEgreso=E.IDTransaccion
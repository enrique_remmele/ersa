﻿CREATE VIew [dbo].[vLibroDiarioAgrupado]
as 
Select 
Conciliado,
FechaConciliado,
IDUsuarioConciliado,
'UsuarioConciliacion'= (Select usuario From usuario where ID=IDUsuarioConciliado),
IDSucursal,
Sucursal,
Moneda,
Tipo,
Operacion,
Fecha,
NroComprobante,
Observacion,
Importe,
IDTransaccion,
FechaTransaccion,
IDUsuario,
Usuario,
IDTipoComprobante,
NumeroAsiento,
IDOperacion

from VLibroDiarioConciliar
group by
Fecha,
IDSucursal,
Sucursal,
Moneda,
Operacion,
Tipo,
Observacion,
IDTransaccion,
FechaTransaccion,
IDUsuario,
Usuario,
IDTipoComprobante,
NroComprobante,
NumeroAsiento,
IDOperacion,
Conciliado,
FechaConciliado,
IDUsuarioConciliado,
Importe



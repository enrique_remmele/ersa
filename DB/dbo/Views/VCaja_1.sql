﻿CREATE View [dbo].[VCaja]
As
Select
C.IDTransaccion,

--Sucursal
C.IDSucursal,
'Sucursal'=S.Descripcion,
'CodigoSucursal'=S.Codigo,
C.Numero,
C.Fecha,

--Estado
'EstadoCaja'=IsNull(CONVERT(varchar(50), C.Fecha) + ' -  ' + (Case When(C.Habilitado) = 'True' Then 'Habilitado' Else 'Cerrado' End), '---'),
C.Anulado,
C.Habilitado,

--Cierre
'FechaCierre'=Case When (C.Habilitado)='False' Then Convert(varchar(50), C.FechaCierre) Else '---' End,
C.IDUsuarioCierre,
'UsuarioCierre'=Case When (C.Habilitado)='False' Then U.Usuario Else '---' End,
C.Observacion

From Caja C
Join Sucursal S On C.IDSucursal=S.ID
Left Outer Join Usuario U On C.IDUsuarioCierre=U.ID


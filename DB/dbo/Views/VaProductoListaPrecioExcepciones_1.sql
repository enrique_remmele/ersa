﻿
CREATE View [dbo].[VaProductoListaPrecioExcepciones]
As
Select 
PLPE.idpRODUCTO,
PLPE.IDLISTAPRECIO,
PLPE.IDCLIENTE,
PLPE.IDSucursal,
PLPE.IDTipoDescuento,
PLPE.IDMoneda,
PLPE.Descuento,
PLPE.Porcentaje,
PLPE.Desde,
PLPE.Hasta,
PLPE.CantidadLimite,
PLPE.CantidadLimiteSaldo,
PLPE.Accion,
PLPE.FechaModificacion,
PLPE.IDUsuario,
'TipoDescuento'=TD.Descripcion,
'TipoDescuentoCodigo'=TD.Codigo,
'FechaDesde'=IsNull(CONVERT(varchar(50), PLPE.Desde, 5), '---'),
'FechaHasta'=IsNull(CONVERT(varchar(50), PLPE.Hasta, 5), '---'),
'Cliente'=C.RazonSocial,
'Moneda'=M.Referencia,
'Sucursal'=S.Descripcion,
'Referencia' = C.Referencia,
'Usuario'=U.Usuario,
'Producto'=P.Descripcion,
'ReferenciaCliente'= C.Referencia,
'ReferenciaProducto'=P.Referencia,
LP.Lista
From aProductoListaPrecioExcepciones PLPE
Join VProducto P On PLPE.IDProducto=P.ID
Join VListaPrecio LP On PLPE.IDListaPrecio=LP.ID
Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID
Join VCliente C On PLPE.IDCliente = C.ID
Join VMoneda M On PLPE.IDMoneda = M.ID
Join Sucursal S On PLPE.IDSucursal=S.ID
Join Usuario U on U.ID = PLPE.IDUsuario


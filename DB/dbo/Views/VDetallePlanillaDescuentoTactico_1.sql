﻿CREATE View [dbo].[VDetallePlanillaDescuentoTactico]
As
Select 
DPDT.IDTransaccion,
'Asignado'=A.ImporteAsignado,
A.*,
PDT.Anulado 
From 
DetallePlanillaDescuentoTactico DPDT
Join VPlanillaDescuentoTactico PDT On DPDT.IDTransaccion=PDT.IDTransaccion
Join Actividad A On DPDT.IDActividad=A.ID AND PDT.IDSucursal=A.IDSucursal

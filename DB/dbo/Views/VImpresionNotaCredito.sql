﻿CREATE View [dbo].[VImpresionNotaCredito]
As
Select 
V.IDTransaccion,
'Comprobante'=V.Comprobante,
'Fecha'=dbo.FFormatoFecha(V.Fecha),
V.Tipo,
'RUC'=RUC,
'Razon Social'=V.Cliente,
'ComprobanteVenta'=IsNull(dbo.SpViewNotaCreditoVenta(V.IDTransaccion),''),
--Totales
'Total EX'=dbo.FFormatoNumericoImpresion((Select Exento From VDetalleImpuestoDesglosadoGravado DI Where DI.IDTransaccion=V.IDTransaccion),M.Decimales),
'Total 5%'=dbo.FFormatoNumericoImpresion((Select [Gravado5%] From VDetalleImpuestoDesglosadoGravado DI Where DI.IDTransaccion=V.IDTransaccion),M.Decimales),
'Total 10%'=dbo.FFormatoNumericoImpresion((Select [Gravado10%] From VDetalleImpuestoDesglosadoGravado DI Where DI.IDTransaccion=V.IDTransaccion),M.Decimales),
'LETRAS'= (Case when M.Decimales = 'False' then dbo.FCantidadConLetra(V.Total) else  concat(M.Impresion,' ',UPPER(dbo.ConvertirNumero(V.Total)))
end),
--'IVA 5%'=(Case when ABS((Select [IVA5%] From VDetalleImpuestoDesglosado DI Where DI.IDTransaccion=V.IDTransaccion) - V.TotalImpuesto)>50 
--			then dbo.FFormatoNumericoImpresion((Select [IVA5%] From VDetalleImpuestoDesglosado DI Where DI.IDTransaccion=V.IDTransaccion),M.Decimales) else dbo.FFormatoNumericoImpresion(V.TotalImpuesto,M.Decimales) end),
--'IVA 10%'=(Case when ABS((Select [IVA10%] From VDetalleImpuestoDesglosado DI Where DI.IDTransaccion=V.IDTransaccion) - V.TotalImpuesto)>50 
--			then dbo.FFormatoNumericoImpresion((Select [IVA10%] From VDetalleImpuestoDesglosado DI Where DI.IDTransaccion=V.IDTransaccion),M.Decimales) else dbo.FFormatoNumericoImpresion(V.TotalImpuesto,M.Decimales) end),
--'Total IVA'=dbo.FFormatoNumericoImpresion(V.TotalImpuesto,M.Decimales),

'IVA 5%'=isnull(dbo.FFormatoNumericoImpresion((select Round(Case when (sum(((precioUnitario - descuentounitario) * cantidad)/21))= 0 then sum(TotalImpuesto) else (sum(((precioUnitario - descuentounitario) * cantidad)/21)) end,0) from detallenotacredito where idimpuesto = 2 and idtransaccion =V.IDTransaccion), M.decimales),0),
'IVA 10%'=isnull(dbo.FFormatoNumericoImpresion((select Round(Case when(sum(((precioUnitario - descuentounitario) * cantidad)/11))= 0 then sum(TotalImpuesto) else (sum(((precioUnitario - descuentounitario) * cantidad)/11)) end,0) from detallenotacredito where idimpuesto = 1 and idtransaccion =V.IDTransaccion), M.decimales),0),
'Total IVA'=dbo.FFormatoNumericoImpresion(Round(isnull(( select Case when (sum(((precioUnitario - descuentounitario) * cantidad)/21))= 0 then sum(TotalImpuesto) else (sum(((precioUnitario - descuentounitario) * cantidad)/21)) end from detallenotacredito where idimpuesto = 2 and idtransaccion =V.IDTransaccion),0) + isnull(( select Case when(sum(((precioUnitario - descuentounitario) * cantidad)/11))= 0 then sum(TotalImpuesto) else (sum(((precioUnitario - descuentounitario) * cantidad)/11)) end from detallenotacredito where idimpuesto = 1 and idtransaccion =V.IDTransaccion),0),0), M.decimales), 


'Total'=dbo.FFormatoNumericoImpresion(V.Total,M.Decimales),
'Emision' =  Concat((Select Ciudad from VSucursal where ID = IDSucursal),' ',dbo.FFormatoDosDigitos(DatePart(dd, Fecha)),' de ', dbo.FMes(DatePart(MM, Fecha)),' de ',DatePart(yyyy, Fecha)),
V.Direccion,
V.Telefono

From VNotaCredito V
Join Moneda M on M.ID = V.IDMOneda

Where Anulado = 'False'
--and v.idtransaccion = 1136735
--select * from Vnotacredito where comprobante like '%15379%'
--select * from detalleimpuesto where idtransaccion = 1118863











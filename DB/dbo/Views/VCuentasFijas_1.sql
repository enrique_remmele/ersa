﻿

CREATE View [dbo].[VCuentasFijas]

As

Select 
CF.IDOperacion,
CF.ID,
'Operacion'=O.Descripcion,
CF.IDTipoOperacion,
'TipoOperacion'=(Select IsNull((Select TP.Descripcion From TipoOperacion TP Where TP.ID=CF.IDTipoOperacion), '---')),

CF.IDTipoCuentaFija,
'TipoCuentaFija'=TCF.Descripcion,
'Tipo'=(Case When (TCF.Impuesto)='True' Then 'IMPUESTO' Else (Case When (TCF.Producto)='True' Then 'PRODUCTO' Else (Case When (TCF.Total)='True' Then 'TOTAL' Else (Case When (TCF.Descuento)='True' Then 'DESCUENTO' Else '---' End) End) End) End),
'DebeHaber'=(Case When (CF.Debe) = 'True' Then 'DEBE' Else (Case When (CF.Haber) = 'True' Then 'HABER' Else '---' End) End),	
CF.Debe,
CF.Haber,
CF.IDCuentaContable,
CF.Orden,
'Cuenta'=CC.Descripcion,
'Codigo'=CC.Codigo,
'CuentaContable'=CC.Codigo + ' - ' + CC.Descripcion,
CF.BuscarEnProducto,
CF.BuscarEnClienteProveedor,
CF.EsVenta,
CF.Contado,
CF.Credito,

--Tipo Cuenta
TCF.Impuesto,
TCF.IDImpuesto,
TCF.Producto,
TCF.Total,
TCF.Descuento,
TCF.IncluirImpuesto,
TCF.IncluirDescuento,
TCF.Campo


From CuentasFijas CF
Join Operacion O On CF.IDOperacion=O.ID
Join TipoCuentaFija TCF On CF.IDTipoCuentaFija=TCF.ID
Join CuentaContable CC On CF.IDCuentaContable=CC.ID






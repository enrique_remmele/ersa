﻿CREATE View [dbo].[VDetalleAsientoContableCaja]

As

--Ventas
Select

'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Venta - ' +' '+V.[Cod.] + '  ' +CONVERT(varchar(50),V.NroComprobante)+'  '+V.Condicion,
D.Debito,
D.Credito,
'Tipo'='VENTA DETALLADA',
'Observacion'=V.Cliente,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
V.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VVenta V On D.IDTransaccion=V.IDTransaccion
Left Outer Join Transaccion T On V.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal = S.ID

Union All

--Ventas Resumidas
Select
D.Codigo,
D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Ventas del ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='VENTA RESUMIDA',
'Observacion'='Ventas del dia',
D.Orden,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VVenta V On D.IDTransaccion=V.IDTransaccion
Left Outer Join Transaccion T On V.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Group By A.Fecha, D.Codigo, D.Descripcion,A.IDSucursal,A.Sucursal,A.Moneda, D.Orden, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto, T.IDTransaccionCaja, C.Numero, C.Fecha, C.FechaCierre, C.Anulado, C.Observacion, C.IDSucursal, S.Codigo

Union All

--Nota de Débito Cliente
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Deb. Cli '+' '+ND.[Cod.] + '  ' +CONVERT(varchar(50),ND.NroComprobante)+'  '+ND.Condicion,
D.Debito,
D.Credito,
'Tipo'='NOTA DEBITO CLIENTE',
'Observacion'=ND.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
ND.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaDebito ND On D.IDTransaccion=ND.IDTransaccion
Left Outer Join Transaccion T On ND.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
union all

--Nota de Crédito Cliente
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Cred. Cli '+' '+NC.[Cod.] + '  ' +CONVERT(varchar(50),NC.NroComprobante)+'  '+NC.Condicion,
D.Debito,
D.Credito,
'Tipo'='NOTA CREDITO CLIENTE',
'Observacion'=NC.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
NC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaCredito NC On D.IDTransaccion=NC.IDTransaccion
Left Outer Join Transaccion T On NC.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID 

union all

--Cobranza Contado Detallado
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. por Lote: '+'  '+ Convert(varchar(50), CCO.Numero),
D.Debito,
D.Credito,
'Tipo'='COBRANZA CONTADO DETALLADO',
'Observacion'='Nro de Lote: ' + CCO.ComprobanteLote,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
CCO.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaContado CCO On D.IDTransaccion=CCO.IDTransaccion
Left Outer Join Transaccion T On CCO.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

union all

--Cobranza Crédito Detallado
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Crédito: '+'  '+ Convert(varchar(50), CCR.Numero),
D.Debito,
D.Credito,
'Tipo'='COBRANZA CREDITO DETALLADO',
'Observacion'=CCR.[Cod.] + '  ' +CONVERT(varchar(50),CCR.NroComprobante)+' - '+CCR.Cliente,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
CCR.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaCredito CCR On D.IDTransaccion=CCR.IDTransaccion
Left Outer Join Transaccion T On CCR.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
union all

--Cobranza Contado Resumido
Select
D.Codigo,
D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Contado del ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='COBRANZA CONTADO RESUMIDO',
'Observacion'='Cobranzas del dia',
D.Orden,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaContado CCO On D.IDTransaccion=CCO.IDTransaccion
Left Outer Join Transaccion T On CCO.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Group By A.Fecha, D.Codigo, D.Descripcion,A.IDSucursal,A.Sucursal,A.Moneda, D.Orden, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto, T.IDTransaccionCaja, C.Numero, C.Fecha, C.FechaCierre, C.Anulado, C.Observacion, C.IDSucursal, S.Codigo

union all

--Cobranza Crédito Resumido
Select
D.Codigo,
D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Crédito del ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='COBRANZA CREDITO RESUMIDO',
'Observacion'='Cobranzas del dia',
D.Orden,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaCredito CCR On D.IDTransaccion=CCR.IDTransaccion
Left Outer Join Transaccion T On CCR.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Group By A.Fecha, D.Codigo, D.Descripcion,A.IDSucursal,A.Sucursal,A.Moneda, D.Orden, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto, T.IDTransaccionCaja, C.Numero, C.Fecha, C.FechaCierre, C.Anulado, C.Observacion, C.IDSucursal, S.Codigo

union all

--Compra
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Compra: '+'  '+ Convert(varchar(50), C.Numero),
D.Debito,
D.Credito,
'Tipo'='COMPRA',
'Observacion'= C.TipoComprobante + ' ' + CONVERT(varchar(50),C.NroComprobante)+' - '+C.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
C.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
CA.Numero,
'FechaCaja'=Convert(datetime,CA.Fecha),
CA.FechaCierre,
'ObservacionCaja'=CA.Observacion,
CA.Anulado,
'IDSucursalCaja'=CA.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCompra C On D.IDTransaccion=C.IDTransaccion
Left Outer Join Transaccion T On C.IDTransaccion=T.ID
Left Outer Join Caja CA On T.IDTransaccionCaja=CA.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

union all

--Gasto Contado
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Gasto Contado: ' + convert(varchar(50),G.Numero),
D.Debito,
D.Credito,
'Tipo'='GASTO CONTADO',
--'Observacion'=G.[Cod.]+' '+CONVERT(varchar(50),G.NroComprobante)+' : '+CONVERT(varchar(50),G.Proveedor),
'Observacion'=G.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
G.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VGasto G On D.IDTransaccion=G.IDTransaccion
Left Outer Join Transaccion T On G.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

Union All

--Gasto Credito
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Gasto Credito: ' + convert(varchar(50),G.Numero),
D.Debito,
D.Credito,
'Tipo'='GASTO CREDITO',
--'Observacion'=G.[Cod.]+' '+CONVERT(varchar(50),G.NroComprobante)+' : '+CONVERT(varchar(50),G.Proveedor),
'Observacion'=G.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
G.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VGasto G On D.IDTransaccion=G.IDTransaccion
Left Outer Join Transaccion T On G.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Where G.Credito = 'True'

union all

--Nota de Débito Proveedor

Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Deb. Prov: '+'  '+ convert(varchar(50), ND.Numero),
D.Debito,
D.Credito,
'Tipo'='NOTA DEBITO PROVEEDOR',
'Observacion'=ND.[Cod.] + '  ' +CONVERT(varchar(50),ND.NroComprobante)+' - '+ND.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
ND.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaDebitoProveedor ND On D.IDTransaccion=ND.IDTransaccion
Left Outer Join Transaccion T On ND.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

union all

--Nota de Crédito Proveedor

Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Cred. Prov '+'  '+ convert(varchar(50),NC.Numero),
D.Debito,
D.Credito,
'Tipo'='NOTA CREDITO PROVEEDOR',
'Observacion'=NC.[Cod.] + '  ' +CONVERT(varchar(50),NC.NroComprobante)+' - '+NC.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
NC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaCreditoProveedor NC On D.IDTransaccion=NC.IDTransaccion
Left Outer Join Transaccion T On NC.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer join Sucursal S On C.IDSucursal=S.ID

union all

--Depósito Bancario Detallado
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Dep. Bancario '+'  ' + convert(varchar(50), DB.Numero),
D.Debito,
D.Credito,
'Tipo'='DEPOSITO DETALLADO',
'Observacion'= DB.TipoComprobante + ' ' + convert(varchar(50), DB.Comprobante) + ' - ' +  DB.Banco + ' - Nro Cuenta: ' + DB.Cuenta,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
DB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDepositoBancario DB On D.IDTransaccion=DB.IDTransaccion
Left Outer Join Transaccion T On DB.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

union all

--Depósito Bancario Resumido
Select
D.Codigo,
D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Depósito del  ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='DEPOSITO RESUMIDO',
'Observacion'='Depositos del Dia',
D.Orden,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDepositoBancario DB On D.IDTransaccion=DB.IDTransaccion
Left Outer Join Transaccion T On DB.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Group By A.Fecha, D.Codigo, D.Descripcion,A.IDSucursal,A.Sucursal,A.Moneda, D.Orden, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto, T.IDTransaccionCaja, C.Numero, C.Fecha, C.FechaCierre, C.Anulado, C.Observacion, C.IDSucursal, S.Codigo

union all

--Descuento Cheque
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Desc. Cheque '+'  '+ convert(varchar(50), DC.Numero),
D.Debito,
D.Credito,
'Tipo'='DESCUENTO CHEQUE',
'Observacion'= DC.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
DC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDescuentoCheque DC On D.IDTransaccion=DC.IDTransaccion
Left Outer Join Transaccion T On DC.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

Union All

--Débito Bancario
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Deb. Bancario '+'  '+ convert(varchar(50), DCB.Numero),
D.Debito,
D.Credito,
--EL TIPO ES DEBITO/CREDITO PORQUE EN EL FORM DE ASIENTOS CONTABLES NO DISCRIMINA
--SI SE CAMBIA, CAMBIAR EL FORM TAMBIEN
'Tipo'='DEBITO/CREDITO BANCARIO',
'Observacion'=DCB.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
DCB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDebitoCreditoBancario DCB On D.IDTransaccion=DCB.IDTransaccion
Left Outer Join Transaccion T On DCB.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Where DCB.Debito = 'True'

Union All

--Crédito Bancario
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cred. Bancario '+'  '+CONVERT(varchar(50),DCB.Numero),
D.Debito,
D.Credito,
--EL TIPO ES DEBITO/CREDITO PORQUE EN EL FORM DE ASIENTOS CONTABLES NO DISCRIMINA
--SI SE CAMBIA, CAMBIAR EL FORM TAMBIEN
'Tipo'='DEBITO/CREDITO BANCARIO',
'Observacion'=DCB.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
DCB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDebitoCreditoBancario DCB On D.IDTransaccion=DCB.IDTransaccion
Left Outer Join Transaccion T On DCB.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Where DCB.Credito = 'True'

Union All

--Orden de Pago
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Orden Pago '+'  '+CONVERT(varchar(50),OP.Numero),
D.Debito,
D.Credito,
'Tipo'='ORDEN PAGO',
'Observacion'=OP.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
OP.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VOrdenPago OP On D.IDTransaccion=OP.IDTransaccion
Left Outer Join Transaccion T On D.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

Union All

--Movimiento
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Mov. Stock '+'  '+CONVERT(varchar(50),M.Operacion),
D.Debito,
D.Credito,
'Tipo'='MOVIMIENTO',
'Observacion'=M.Motivo,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VMovimiento M On D.IDTransaccion=M.IDTransaccion
Left Outer Join Transaccion T On D.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

Union All

--Ajuste
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Ajuste ',
D.Debito,
D.Credito,
'Tipo'='AJUSTE',
'Observacion'=AI.Sucursal + ' - ' + AI.Deposito,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VControlInventario AI On D.IDTransaccion=AI.IDTransaccion
Left Outer Join Transaccion T On D.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

Union All

--Asiento Importado
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= '* ' + V.Operacion,
D.Debito,
D.Credito,
'Tipo'='* ASIENTOS IMPORTADOS',
'Observacion'=Convert(varchar(50), V.Operacion) + ' - Nro:' + Convert(varchar(50), V.Numero),
D.Orden,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VAsientoImportado V On D.IDTransaccion=V.IDTransaccion
Left Outer Join Transaccion T On V.IDTransaccion=T.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID

Union All

--Asiento con Operaciones sin Formularios de Carga
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= O.Descripcion + ' ' + A.Comprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=A.Detalle,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
C.Numero,
'FechaCaja'=Convert(datetime,C.Fecha),
C.FechaCierre,
'ObservacionCaja'=C.Observacion,
C.Anulado,
'IDSucursalCaja'=C.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Left Outer Join Caja C On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Where O.FormName Is Null

Union All

--Asiento con Operaciones en Comprobantes para Libro IVA
Select
'Codigo'=D.Codigo,
'Descripcion'=D.Descripcion,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= TC.Codigo + ' ' + C.NroComprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=C.Detalle,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
T.IDTransaccionCaja,
CA.Numero,
'FechaCaja'=Convert(datetime,CA.Fecha),
CA.FechaCierre,
'ObservacionCaja'=CA.Observacion,
CA.Anulado,
'IDSucursalCaja'=CA.IDSucursal,
'SucursalCaja'=S.Codigo

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join ComprobanteLibroIVA C On A.IDTransaccion=C.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Join TipoComprobante TC On C.IDTipoComprobante=TC.ID
Left Outer Join Caja CA On T.IDTransaccionCaja=C.IDTransaccion
Left Outer Join Sucursal S On C.IDSucursal=S.ID




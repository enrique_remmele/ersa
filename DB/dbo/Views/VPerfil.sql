﻿



CREATE View [dbo].[VPerfil] 
As 
Select
P.*,
'V'=(Case When (P.Visualizar) = 'True' Then 'SI' Else '---' End),
'A'=(Case When (P.Agregar) = 'True' Then 'SI' Else '---' End),
'M'=(Case When (P.Modificar) = 'True' Then 'SI' Else '---' End),
'E'=(Case When (P.Eliminar) = 'True' Then 'SI' Else '---' End),
'N'=(Case When (P.Anular) = 'True' Then 'SI' Else '---' End),
'I'=(Case When (P.Imprimir) = 'True' Then 'SI' Else '---' End),
de.Departamento

From Perfil P
left outer join DepartamentoEmpresa de on P.IDDepartamentoEmpresa = de.ID



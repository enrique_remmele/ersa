﻿


CREATE View [dbo].[VResumenCobranzaDiariaCheque]
As

Select 
FP.IDTransaccion, 
'Cobranza'=C.Numero, 
CHQ.Numero, 
'Tipo'=CHQ.CodigoTipo, 
CHQ.Cliente, 
CHQ.Banco, 
'Fecha'=C.FechaEmision, 
'Nro. CHQ'=CHQ.NroCheque, 
CHQ.Importe, 
'Utilizado'=FP.ImporteCheque, 
CHQ.Estado,
'NroPlanilla'=C.NroPlanilla 
From FormaPago FP 
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion 
Join VChequeCliente CHQ On FP.IDTransaccionCheque=CHQ.IDTransaccion

Union All

Select 
FP.IDTransaccion, 
'Cobranza'=LD.numero, 
CHQ.Numero, 
'Tipo'=CHQ.CodigoTipo, 
CHQ.Cliente, 
CHQ.Banco, 
'Fecha'=C.Fecha, 
'Nro. CHQ'=CHQ.NroCheque, 
CHQ.Importe, 
'Utilizado'=FP.ImporteCheque, 
CHQ.Estado,
'NroPlanilla'=LD.numero
 
From FormaPago FP 
Join CobranzaContado C On FP.IDTransaccion=C.IDTransaccion 
join LoteDistribucion LD on IDTransaccionLote=LD.IDTransaccion 
Join VChequeCliente CHQ On FP.IDTransaccionCheque=CHQ.IDTransaccion



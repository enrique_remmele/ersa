﻿CREATE view [dbo].[VPedidoVenta]
As
Select 
Pv.IDTransaccionPedido,
PV.IDtransaccionVenta,
'NumeroPedido'= P.Numero,
'NumeroVenta'= V.NroComprobante

From PedidoVenta PV
Join Pedido P On PV.IDTransaccionPedido=P.IDTransaccion
Join Venta V On PV.IDTransaccionVenta= V.IDTransaccion



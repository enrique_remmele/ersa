﻿CREATE View [dbo].[VACliente]
As


Select
--Comerciales
C.ID,
C.RazonSocial,
C.RUC,
'Tipo'= (Case When (C.CI)= 'True' Then 'CI' Else 'RUC' End),
C.CI,
C.Referencia,
C.NombreFantasia,
C.Direccion,
C.Telefono,
'RazonSocialReferencia'=C.RazonSocial + ' (' + C.Referencia + ')',

--De configuracion
C.IDEStado,
'Estado'=(Select IsNull((Select V.Descripcion From EstadoCliente V Where V.ID=C.IDEstado), '')),
'Contado'=IsNull(C.Contado, 'True'),
'Credito'=IsNull(C.Credito, 'False'),
'Condicion'=(Case When (IsNull(C.Contado, 'True')) = 'True' Then 'CONTADO' Else 'CREDITO' End),
'IDMoneda'=(IsNull(C.IDMoneda, 1)),
'Moneda'=(Select IsNull((Select V.Descripcion From Moneda V Where V.ID=C.IDMoneda ), '')),
C.IDListaPrecio,
'ListaPrecio'=(Select IsNull((Select V.Descripcion From ListaPrecio V Where V.ID=C.IDListaPrecio), '')),
C.IDTipoCliente,
'TipoCliente'=(Select IsNull((Select V.Descripcion From TipoCliente V Where V.ID=C.IDTipoCliente), '')),
'IVAExento'=IsNull(C.IVAExento, 'False'),

--Contabilidar
C.CodigoCuentaContable,
'Cuenta'=(Select IsNull((Select V.Cuenta From VCuentaContable V Where V.Codigo=C.CodigoCuentaContable), '')),

--Localizacion
C.IDPais,
'Pais'=(Select IsNull((Select V.Descripcion From Pais V Where V.ID=C.IDPais), '')),
C.IDDepartamento,
'Departamento'=(Select IsNull((Select V.Descripcion From Departamento V Where V.ID=C.IDDepartamento), '')),
C.IDCiudad,
'Ciudad'=(Select IsNull((Select V.Descripcion From Ciudad V Where V.ID=C.IDCiudad), '')),
C.IDBarrio,
'Barrio'=(Select IsNull((Select V.Descripcion From Barrio V Where V.ID=C.IDBarrio), '')),
C.IDZonaVenta,
'ZonaVenta'=(Select IsNull((Select V.Descripcion From ZonaVenta V Where V.ID=C.IDZonaVenta), '')),
'Latitud'=IsNull(C.Latitud, 0),
'Longitud'=IsNull(C.Longitud, 0),

--Referencias
C.IDSucursal,
'Sucursal'=(Select IsNull((Select V.Descripcion From Sucursal V Where V.ID=C.IDSucursal), '')),
C.IDPromotor,
'Promotor'=(Select IsNull((Select V.Nombres From Promotor V Where V.ID=C.IDPromotor), '')),
'IDVendedor'=isnull(C.IDVendedor,0),
'Vendedor'=(Select IsNull((Select V.Nombres From Vendedor V Where V.ID=C.IDVendedor), '')),
C.IDCobrador,
'Cobrador'=(Select IsNull((Select V.Nombres From Cobrador V Where V.ID=C.IDCobrador), '')),
C.IDDistribuidor,
'Distribuidor'=(Select IsNull((Select V.Nombres From Distribuidor V Where V.ID=C.IDDistribuidor), '')),


--Estadisticos
'FechaAlta'=C.FechaAlta,
'Alta'=IsNull(Convert(varchar(50), C.FechaAlta, 6), '---'),
'UsuarioAlta'=IsNull((Select U.Usuario From Usuario U Where U.ID=C.IDUsuarioAlta), '---'),
'FechaModificacion'=C.FechaModificacion,
'Modificacion'=IsNull(Convert(varchar(50), C.FechaModificacion, 6), '---'),
'UsuarioModificacion'=IsNull((Select U.Usuario From Usuario U Where U.ID=C.IDUsuarioModificacion), '---'),
'UltimaCobranza'=C.FechaUltimaCobranza,
'Ult. Cobranza'=IsNull(Convert(varchar(50), C.FechaUltimaCobranza, 6), '---'),
'UltimaCompra'=C.FechaUltimaCompra,
'Ult. Compra'=IsNull(Convert(varchar(50), C.FechaUltimaCompra, 6), '---'),

--Credito
'LimiteCredito'=IsNull(LimiteCredito, 0),
'Deuda'=IsNull(Deuda, 0),
'SaldoCredito'= IsNull(LimiteCredito, 0) - IsNull(Deuda, 0),
'Descuento'=IsNull(Descuento, 0),
'PlazoCredito'=IsNull(PlazoCredito, 0),
'PlazoCobro'=IsNull(PlazoCobro, 0),
'PlazoChequeDiferido'=IsNull(PlazoChequeDiferido, 0),
'DeudaTotal'=IsNull(Deuda, 0),
--Datos Adicionales
C.PaginaWeb,
C.Email,
C.Fax,

'TieneSucursales'=Case When (Select Top(1) CS.ID From ClienteSucursal CS Where IDCliente=C.ID) Is NULL Then 'False' Else 'True' End,
Observacion,
'ClienteVario'=(Case When (C.ClienteVario)= 'True' Then 'True' Else 'False' End),
C.IDArea,
'ReferenciaArea'= (Select IsNull((Select A.Referencia From Area A Where A.ID=C.IDArea), '')),
'Area'= (Select IsNull((Select A.Descripcion From Area A Where A.ID=C.IDArea), '')),
C.IDRuta,
'ReferenciaRuta'= (Select IsNull((Select R.Referencia From Ruta R Where R.ID=C.IDRuta), '')),
'Ruta'= (Select IsNull((Select R.Descripcion From Ruta R Where R.ID=C.IDRuta), '')),
C.celular,
C.IDUsuario,
'Usuario'=(Select concat(Usuario,' - ',Nombre) from Usuario where ID = C.IDUsuario),
C.Accion,
'ModiLimiteCredito'=(Case When (C.ModiLimiteCredito)= 'True' Then 'True' Else 'False' End),
'ModiVendedor'=(Case When (C.ModiVendedor)= 'True' Then 'True' Else 'False' End),
'ModiTipoCliente'=(Case When (C.ModiTipoCliente)= 'True' Then 'True' Else 'False' End),
'ModiListaPrecio'=(Case When (C.ModiListaPrecio)= 'True' Then 'True' Else 'False' End),
'ModiPlazo'=(Case When (C.ModiPlazo)= 'True' Then 'True' Else 'False' End),
'ModiCondicion'=(Case When (C.ModiCondicion)= 'True' Then 'True' Else 'False' End)


From

ACliente as C






































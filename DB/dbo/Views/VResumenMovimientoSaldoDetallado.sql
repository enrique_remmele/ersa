﻿
CREATE view [dbo].[VResumenMovimientoSaldoDetallado]

as


Select
--Para Filtrar 
R .ID,
C.IDTipoCliente,
C.IDVendedor,
C.IDCobrador,
C.IDCiudad,
C.IDSucursal,
C.IDZonaVenta,
C.IDEStado, 
C.IDMoneda,  
--Mosatrar Imforme
C.Referencia,
C.RazonSocial,  
R.Saldo,
'CondVta'= (Case When (C.Contado)= 'True' Then 'CONT' ELSE 'CRED' END ) ,
C.PlazoCredito,
C.LimiteCredito,
C.[Ult. Compra],
C.[Ult. Cobranza],
C.TipoCliente,
C.Vendedor,
C.Cobrador,
R.TotalVenta,
R.TotalCobranza,
R.TotalDebito,
R.TotalCredito,
R.SaldoAnterior 
 
From ResumenMovimientoSaldoDetallado R
Join VCliente C On R.IDCliente=C.ID 





﻿






CREATE View [dbo].[VTicketBascula]
As
Select 
T.*,
'PrecioUnitarioDiscriminado'=T.PrecioUnitario/I.FactorDiscriminado,
'PrecioUnitarioImpuesto'=T.PrecioUnitario/I.FactorImpuesto,
'PrecioUnitarioDiscriminadoUS'=T.PrecioUnitarioUS/I.FactorDiscriminado,
'PrecioUnitarioImpuestoUS'=T.PrecioUnitarioUS/I.FactorImpuesto,
'ReferenciaTipoComprobante'= TC.Codigo,
'TipoComprobante'= TC.Descripcion,
'ReferenciaProveedor'=P.Referencia,
'Proveedor'=P.RazonSocial,
'ReferenciaSucursal'= S.Codigo,
'Sucursal'= S.Descripcion,
'ReferenciaMoneda'=M.Referencia,
'Moneda'=M.Descripcion,
'Deposito'= D.Descripcion,
'CuentaContableDeposito'=(Case when D.IDTipoDeposito = 5 then P.CuentaContableCompra else  D.CuentaContableMercaderia end),
'ReferenciaProducto'=Pr.Referencia,
'Producto'= Pr.Descripcion,
'Gasto'= IsNull((Select Comprobante from VGasto where IDTransaccion = T.IDTransaccionGasto),''),
'Impuesto'= I.Descripcion,
'ReferenciaImpuesto'= I.Referencia,
--Transaccion
'FechaTransaccion'=TR.Fecha,
'IDDepositoTransaccion'=TR.IDDeposito,
'IDSucursalTransaccion'=TR.IDSucursal,
'IDTerminalTransaccion'=TR.IDTerminal,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),
--Anulacion
'UsuarioAnulacion'=ISnull((Select U.Usuario From Usuario U Where U.ID=T.IDUsuarioAnulacion),'---'),
'UsuarioIdentificacionAnulacion'=ISnull((Select U.Identificador From Usuario U Where U.ID=T.IDUsuarioAnulacion),'---'),
'Chofer' = CP.Nombres,
'Camion'= CA.Descripcion,
'Chapa'=CA.Patente,
Pr.IDTipoProducto,
Pr.TipoProducto,
Pr.IDLinea,
'UnidadMedida' = Pr.UnidadMedida,
'FechaMacheo'='---',
'FechaFactura'='---',
Pr.FactorDiscriminado,
Pr.FactorImpuesto
From TicketBascula T
Join Sucursal S on S.ID = T.IDSucursal
Join TipoComprobante TC on TC.ID = T.IDTipoComprobante
Join Proveedor P on P.ID = T.IDProveedor
Join Moneda M on M.ID = T.IDMoneda
Join Deposito D on D.ID = T.IDDeposito
Join vProducto Pr on Pr.ID = T.IDProducto
Join Impuesto I on I.ID = T.IDImpuesto
Join Transaccion TR on TR.ID = T.IDTransaccion
Join CamionProveedor CA on CA.ID = T.IDCamion
Join ChoferProveedor CP on CP.ID = T.IDChofer



















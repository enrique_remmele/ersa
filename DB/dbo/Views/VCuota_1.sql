﻿
CREATE View [dbo].[VCuota]
As
Select
C.IDTransaccion,
C.ID,
C.Vencimiento,
C.Importe,
C.Saldo,
C.Cancelado
From Cuota C

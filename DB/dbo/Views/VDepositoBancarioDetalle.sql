﻿CREATE view [dbo].[VDepositoBancarioDetalle]
as


Select

--Efectivo
'Tipo' = E.CodigoComprobante, 
'Banco'='',
'Importe'= Sum(DDB.Importe),
'Cliente'='',
'Referencia'='',
'CuentaBancaria'='',
'Rechazado'='False',
'Cartera'='False',

'FechaCobranza'=NULL,
--'FechaCobranza'=(Select Top 1 CC.FechaEmision  From Efectivo EE 
--Join CobranzaCredito CC on CC.IDTransaccion=E.IDTransaccion
--Join VentaCobranza VC on VC.IDTransaccionCobranza=E.IDTransaccion 
--Join Venta V on V.IDTransaccion=VC.IDTransaccionVenta
--Where EE.IDTransaccion=E.IDTransaccion),


'Condicion'='False',

--'Condicion'=(Select Top 1  Credito From Efectivo E 
--Join VentaCobranza VC on VC.IDTransaccionCobranza=E.IDTransaccion 
--Join Venta V on V.IDTransaccion=VC.IDTransaccionVenta
--Where E.IDTransaccion=DDB.IDTransaccionEfectivo),

'NroCobranza'=0,

--Depósito
DB.Numero,
DB.Fecha, 
DB.NroComprobante,
'IDSucursal'=DB.IDSucursal,
'Sucursal'=DB.Sucursal,
'Total'=0,
'IDCuentaBancaria'=DB.IDCuentaBancaria,
'IDMoneda'=DB.IDMoneda,
'FechaTransaccion'=T.Fecha  

From DetalleDepositoBancario DDB
Join Transaccion T On DDB.IDTransaccionDepositoBancario =T.ID
Join VDepositoBancario DB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
join VEfectivo E on DDB.IDTransaccionEfectivo = E.IDTransaccion And DDB.IDEfectivo=E.ID 
Group By DB.Fecha,DB.Numero,DB.IDSucursal,DB.Sucursal,DB.IDMoneda,DB.NroComprobante,E.CodigoComprobante, 
DB.IDCuentaBancaria,T.Fecha  


Union All

Select
--Cheque
'Tipo'=C.CodigoTipo, 
'Banco'=C.CodigoBanco,
'Importe'= C.Importe,
C.Cliente,
'Referencia'=C.CodigoCliente, 
C.CuentaBancaria,
C.Rechazado,
C.Cartera,
'FechaCobranza'=(Select Top 1 CC.FechaEmision From FormaPago FP
 Join CobranzaCredito CC on CC.IDTransaccion=FP.IDTransaccion
 Join ChequeCliente CHQ on CHQ.IDTransaccion=FP.IDTransaccionCheque 
 Where CHQ.IDTransaccion=DDB.IDTransaccionCheque),

'Condicion'=(Select Top 1  Credito From ChequeCliente CC 
Join FormaPago FP on CC.IDTransaccion=FP.IDTransaccionCheque 
Join VentaCobranza VC on VC.IDTransaccionCobranza=FP.IDTransaccion 
Join Venta V on V.IDTransaccion=VC.IDTransaccionVenta
Where CC.IDTransaccion=C.IDTransaccion),

'NroCobranza'=(Select Top 1 CC.Numero From FormaPago FP
 Join CobranzaCredito CC on CC.IDTransaccion=FP.IDTransaccion
 Join ChequeCliente CHQ on CHQ.IDTransaccion=FP.IDTransaccionCheque 
 Where CHQ.IDTransaccion=DDB.IDTransaccionCheque),

----Deposito
DB.Numero,
DB.Fecha, 
DB.NroComprobante,
DB.IDSucursal,   
DB.Sucursal,
DB.Total,
DB.IDCuentaBancaria,
DB.IDMoneda,
'FechaTransaccion'=T.Fecha  


From DetalleDepositoBancario DDB
Join Transaccion T On DDB .IDTransaccionDepositoBancario =T.ID
Join VDepositoBancario DB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion 
Join VChequeCliente  C on DDB.IDTransaccionCheque = C.IDTransaccion

Union All

Select

--Redondeo
'Tipo' = 'REDOND', 
'Banco'='',
'Importe'= Importe,
'Cliente'='',
'Referencia'='',
'CuentaBancaria'='',
'Rechazado'='False',
'Cartera'='False',


'FechaCobranza'=NULL,

'Condicion'='False',


'NroCobranza'=0,

--Depósito
DB.Numero,
DB.Fecha, 
DB.NroComprobante,
'IDSucursal'=DB.IDSucursal,
'Sucursal'=DB.Sucursal,
'Total'=0,
'IDCuentaBancaria'=DB.IDCuentaBancaria,
'IDMoneda'=DB.IDMoneda  ,
'FechaTransaccion'=T.Fecha  

From DetalleDepositoBancario DDB
Join Transaccion T On DDB.IDTransaccionDepositoBancario =T.ID
Join VDepositoBancario DB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
Where DDB.Redondeo = 'True'

union all
--Documento
select 
'Tipo' = E.CodigoComprobante, 
'Banco'='',
'Importe'= Sum(DDB.Importe),
'Cliente'='',
'Referencia'='',
'CuentaBancaria'='',
'Rechazado'='False',
'Cartera'='False',

'FechaCobranza'=NULL,
'Condicion'='False',
'NroCobranza'=0,

--Depósito
DB.Numero,
DB.Fecha, 
DB.NroComprobante,
'IDSucursal'=DB.IDSucursal,
'Sucursal'=DB.Sucursal,
'Total'=0,
'IDCuentaBancaria'=DB.IDCuentaBancaria,
'IDMoneda'=DB.IDMoneda,
'FechaTransaccion'=T.Fecha    

From DetalleDepositoBancario DDB
Join Transaccion T On DDB.IDTransaccionDepositoBancario =T.ID
Join VDepositoBancario DB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
join VFormaPagoDocumento E on DDB.IDTransaccionDocumento = E.IDTransaccion And DDB.IDTransaccionDocumento=E.IDTransaccion 
where (select deposito from TipoComprobante where id = E.IdTipoComprobante) = 1 
Group By DB.Fecha,DB.Numero,DB.IDSucursal,DB.Sucursal,DB.IDMoneda,DB.NroComprobante,E.CodigoComprobante, 
DB.IDCuentaBancaria,T.Fecha  





























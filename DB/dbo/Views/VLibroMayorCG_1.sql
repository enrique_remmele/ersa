﻿CREATE VIEW [dbo].[VLibroMayorCG]

AS

SELECT

'Codigo'=CONVERT(VARCHAR(50),DA.Codigo),
DA.Descripcion,
a.Fecha, 
'Asiento'=A.NumeroAsiento,
DA.NroComprobante,
'Comprobante'=DA.TipoComprobante + ' ' + DA.NroComprobante,
DA.IDSucursal,
'Suc'=S.Descripcion,
'CodSucursal'=S.Codigo,
A.IDTipoComprobante,
'Concepto'=DA.Observacion,
DA.Debito,
DA.Credito,
'TipoCuenta'=dbo.FCuentaContableTipo(CONVERT(VARCHAR(50),DA.Codigo)),
A.Numero,
A.NumeroAsiento,

--Centro de Costos
DA.IDCentroCosto,
'CentroCosto'=ISNull(CC.Descripcion, '---'),
'CodigoCentroCosto'=IsNull(CC.Codigo, '')

From VDetalleAsientoCG DA 
Join VAsientoCG A On A.Numero=DA.Numero
Left Outer Join Sucursal S On DA.IDSucursal=S.ID
Left Outer Join CentroCosto CC On DA.IDCentroCosto=CC.ID

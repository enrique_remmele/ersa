﻿
CREATE View [dbo].[VDetalleRendicionLoteVentasAnuladas]

As

Select 
D.IDTransaccion,
D.ID,

--Motivo
D.IDMotivo,
'Motivo'=M.Descripcion,

--Venta
D.IDTransaccionVenta,
'Venta'=V.Comprobante,
V.Cliente,
V.Saldo,
V.Credito,

--Total
'Importe'=V.Saldo

From DetalleRendicionLoteVentaAnulada D
Join MotivoAnulacionVenta M On D.IDMotivo=M.ID
Join RendicionLote RL On D.IDTransaccion=RL.IDTransaccion
Join VVenta V On D.IDTransaccionVenta=V.IDTransaccion



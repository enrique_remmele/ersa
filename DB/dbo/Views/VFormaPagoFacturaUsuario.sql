﻿CREATE View [dbo].[VFormaPagoFacturaUsuario]
As

Select  distinct
FPFU.IDFormaPagoFactura,
FPFU.IDUsuario,
'FormaPagoFactura'=FPF.Descripcion,
U.Usuario,
'Nombre Usuario'=U.Nombre
From FormaPagoFacturaUsuario FPFU
Left Outer Join FormaPagoFactura FPF On FPFU.IDFormaPagoFactura=FPF.ID
Left Outer Join Usuario U On FPFU.IDUsuario=U.ID


﻿
CREATE View [dbo].[VImpresionNotaCreditoDetalle]
As
Select 
IDTransaccion,
DNC.Referencia,
Cantidad,
DNC.Descripcion,
'PrecioUnitario'= dbo.FFormatoNumericoImpresion(PrecioUnitario,M.Decimales),
'Exento'=dbo.FFormatoNumericoImpresion((Case when IDImpuesto = 3 then Total else 0 end ),M.Decimales),
'IVA10' = dbo.FFormatoNumericoImpresion((Case when IDImpuesto = 1 then Total else 0 end ),M.Decimales),
'IVA5'=dbo.FFormatoNumericoImpresion((Case when IDImpuesto = 2 then Total else 0 end ),M.decimales)
 from VDetalleNotaCredito DNC
 Join Moneda M on M.ID = DNC.IDMoneda




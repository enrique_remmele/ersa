﻿


CREATE View [dbo].[VCFAjuste]
As
Select
V.*,
CFV.Sobrante,
CFV.Faltante,
'Tipo'=(Case When (CFV.Sobrante) = 'True' Then 'SOBRANTE' Else (Case When (CFV.Faltante) = 'True' Then 'FALTANTE' Else '---' End) End)

From VCF V
Join CFAjuste CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID




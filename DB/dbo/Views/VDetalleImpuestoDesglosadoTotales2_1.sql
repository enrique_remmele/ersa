﻿Create View [dbo].[VDetalleImpuestoDesglosadoTotales2]
As
Select 
IDTransaccion,
'GRAVADO10%'=Sum([GRAVADO10%]),
'GRAVADO5%'=Sum([GRAVADO5%]),
'EXENTO'=Sum(EXENTO),
'DISCRIMINADO10%'=Sum([DISCRIMINADO10%]),
'IVA10%'=Sum([IVA10%]),
'DISCRIMINADO5%'=Sum([DISCRIMINADO5%]),
'IVA5%'=Sum([IVA5%])
From VDetalleImpuestoDesglosadoTotales
Group By IDTransaccion


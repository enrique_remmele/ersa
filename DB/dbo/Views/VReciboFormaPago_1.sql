﻿CREATE View [dbo].[VReciboFormaPago] As 

Select IDTransaccion,
CodigoTipoComprobante,
ImporteMoneda,
'Banco'= (select Descripcion from Banco where ID = IDBanco),
Comprobante,
FechaEmision,
Vencimiento

From VFormaPagoCobranzaCredito
Where (CodigoTipoComprobante <> 'RETEC' and  CodigoTipoComprobante <> 'EFE')

union all

Select IDTransaccion,
CodigoTipoComprobante,
'ImporteMoneda'=Sum(ImporteMoneda),
'Banco'= '',
'Comprobante'='',
'FechaEmision'='',
'Vencimiento'=''
From VFormaPagoCobranzaCredito
Where (CodigoTipoComprobante = 'RETEC' or  CodigoTipoComprobante = 'EFE')
Group by IDTransaccion, CodigoTipoComprobante

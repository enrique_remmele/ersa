﻿
CREATE view [dbo].[VVentaInferiorAlCosto]
as
Select 
V.[Cod.],
V.NroComprobante,
V.Condicion,
V.Fecha,
DV.Producto,
'ReferenciaProducto'=DV.Referencia,
DV.Cantidad,
DV.PrecioUnitario,
DV.CostoUnitario,
V.Cliente,
V.Referencia,
V.Vendedor,
V.IDVendedor,
V.Sucursal,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
DV.IDLinea,
DV.IDMarca,
V.Credito,
V.IDCiudad,
V.IDTipoComprobante 
From VDetalleVenta DV 
Join VVenta V on DV.IDTransaccion=V.IDTransaccion 
Where DV.PrecioUnitario < DV.CostoUnitario 

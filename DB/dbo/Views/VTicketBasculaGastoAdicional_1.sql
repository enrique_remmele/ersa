﻿
CREATE View [dbo].[VTicketBasculaGastoAdicional]
As
Select TG.*,
'GastoAdicional'= G.Descripcion,
'ReferenciaMoneda'= M.Referencia,
'Moneda'=M.Descripcion,
'Proveedor' = P.RazonSocial
From TicketBasculaGastoAdicional TG
Join GastoAdicional G on G.ID = TG.IDGastoAdicional
Join Moneda M on M.ID = TG.IDMoneda
Left Join Proveedor P on P.ID = TG.IDProveedor


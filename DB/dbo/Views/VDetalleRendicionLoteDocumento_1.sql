﻿CREATE View [dbo].[VDetalleRendicionLoteDocumento]

As

Select 
D.IDTransaccion,
D.ID,

--Comprobante
D.TipoComprobante,
'Tipo'=D.TipoComprobante,
D.NroComprobante,
'Nro'=D.NroComprobante,

--Venta
D.IDTransaccionVenta,
'Venta'=V.Comprobante,
V.Cliente,
V.Saldo,
V.Credito,

--Total
D.Importe,
D.Aplicar,
'Aplica'=Case When (D.Aplicar) = 'True' Then 'SI' Else '---' End,
D.Observacion

From DetalleRendicionLoteDocumento D
Join RendicionLote RL On D.IDTransaccion=RL.IDTransaccion
Left Outer Join VVenta V On D.IDTransaccionVenta=V.IDTransaccion



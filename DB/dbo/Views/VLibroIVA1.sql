﻿CREATE view [dbo].[VLibroIVA1]

as

--Compra
Select 
'Fecha'=Convert(date, C.Fecha),
'Comprobante'=convert(varchar(50),C.Comprobante),
substring(C.NroComprobante,9,15) as NroComprobante,
C.IDTipoComprobante,
C.DescripcionTipoComprobante,
C.[Cod.],
'Sucursal'=S.Descripcion,
C.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Compra',
'Credito'=NULL,
'RazonSocial'=C.Proveedor,
C.RUC,
C.Directo,
'EX'=D.EXENTO,
'GRAVADO5%'=D.[DISCRIMINADO5%],
'GRAVADO10%'=D.[DISCRIMINADO10%],
D.EXENTO,
D.[IVA5%],
D.[IVA10%],
C.Total,
'Descripcion'='Compras del ' + convert(varchar(50), C.Fecha, 5),
C.IDTransaccion
From VCompra C
Join VDetalleImpuestoDesglosadoTotales2 D on C.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=C.IDSucursal 

Union All

--Compra
Select 
'Fecha'=Convert(date, C.Fecha),
'Comprobante'=convert(varchar(50),C.Comprobante),
Substring(C.NroComprobante,9,15) as NroComprobante,
C.IDTipoComprobante,
C.DescripcionTipoComprobante,
C.[Cod.],
'Sucursal'=S.Descripcion,
C.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Compra',
'Credito'=NULL,
'RazonSocial'=C.Proveedor,
C.RUC,
C.Directo,
'EX'=D.EXENTO,
'GRAVADO5%'=D.[DISCRIMINADO5%],
'GRAVADO10%'=D.[DISCRIMINADO10%],
D.EXENTO,
D.[IVA5%],
D.[IVA10%],
C.Total,
'Descripcion'='Compras del ' + convert(varchar(50), C.Fecha, 5),
C.IDTransaccion
From VGasto C
Join VDetalleImpuestoDesglosadoTotales2 D on C.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=C.IDSucursal 
Where C.IncluirLibro='True'

Union All

--Comprobantes de Libro Compra y Venta
Select 
'Fecha'=Convert(date, C.Fecha),
'Comprobante'=convert(varchar(50),C.NroComprobante),
Substring(C.NroComprobante,9,15)as NroComprobante,
C.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'Cod.'=TC.Codigo,
'Sucursal'=S.Descripcion,
C.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'=(Case When (TC.Tipo) = 'PROVEEDOR' Then 'Compra' Else 'Venta' End),
'Credito'=NULL,
'RazonSocial'=C.RazonSocial,
C.RUC,
C.Directo,
'EX'=D.EXENTO,
'GRAVADO5%'=D.[DISCRIMINADO5%],
'GRAVADO10%'=D.[DISCRIMINADO10%],
D.EXENTO,
D.[IVA5%],
D.[IVA10%],
C.Total,
'Descripcion'=(Case When (TC.Tipo) = 'PROVEEDOR' Then 'Compras' Else 'Ventas' End)+ ' del ' + convert(varchar(50), C.Fecha, 5),
C.IDTransaccion
From VComprobanteLibroIVA C
Join VTipoComprobante TC On C.IDTipoComprobante=TC.ID
Join VDetalleImpuestoDesglosadoTotales2 D on C.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=C.IDSucursal 
WHERE TC.LibroCompra='True' Or TC.LibroVenta='True'

Union All

--Venta
Select 
'Fecha'=convert(date, V.FechaEmision),
'Comprobante'=convert(varchar(50),V.Comprobante),
V.NroComprobante,
V.IDTipoComprobante,
V.DescripcionTipoComprobante,
V.[Cod.],
'Sucursal'=S.Descripcion,
V.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Venta',
'Credito'=V.Credito,
'RazonSocial'=V.Cliente,
V.RUC,
'Directo'=NULL,
'EX'=D.EXENTO,
'GRAVADO5%'=D.[DISCRIMINADO5%],
'GRAVADO10%'=D.[DISCRIMINADO10%],
D.EXENTO,
D.[IVA5%],
D.[IVA10%],
V.Total,
'Descripcion'= V.Sucursal + ' - ' + convert(varchar(50), v.FechaEmision, 5) + ' - Punto Exp. ' + V.ReferenciaPunto,
V.IDTransaccion
From VVenta V
Join VDetalleImpuestoDesglosadoTotales2 D on v.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=V.IDSucursal 

Where V.Anulado='False'

Union All

--Venta Anulada
Select 
'Fecha'=Convert(date, V.FechaEmision),
'Comprobante'=convert(varchar(50),V.Comprobante),
V.NroComprobante,
V.IDTipoComprobante,
V.DescripcionTipoComprobante,
V.[Cod.],
'Sucursal'=S.Descripcion,
V.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Venta',
'Credito'=V.Credito,
'RazonSocial'='-----Comprobante Anulado-----',
'RUC'='',
'Directo'=NULL,
'EX'=0,
'GRAVADO5%'= 0,
'GRAVADO10%'=0,
'EXENTO'=0,
'IVA5%'=0,
'IVA10%'=0,
'Total'=0,
'Descripcion'= V.Sucursal + ' - ' + convert(varchar(50), v.FechaEmision, 5) + ' - Punto Exp. ' + V.ReferenciaPunto,
V.IDTransaccion

From VVenta V
Join VSucursal S on S.ID=V.IDSucursal 

Where V.Anulado='True'

Union All

--Nota Crédito Cliente
Select 
'Fecha'=Convert(date, NC.Fecha),
'Comprobante'=convert(varchar(50),NC.Comprobante),
NC.NroComprobante,
NC.IDTipoComprobante,
NC.DescripcionTipoComprobante,
NC.[Cod.],
'Sucursal'=S.Descripcion,
NC.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Credito Emitido',
'Credito'=NULL,
'RazonSocial'=NC.Cliente,
NC.RUC,
'Directo'=NULL,
'EX'=D.EXENTO*-1,
'GRAVADO5%'=D.[DISCRIMINADO5%]*-1,
'GRAVADO10%'=D.[DISCRIMINADO10%]*-1,
D.EXENTO*-1,
D.[IVA5%]*-1,
D.[IVA10%]*-1,
NC.Total*-1,
'Descripcion'= NC.Sucursal + ' - ' + convert(varchar(50), NC.Fecha, 5) + ' - Punto Exp. ' + NC.ReferenciaPunto,
NC.IDTransaccion

From VNotaCredito NC
Join VDetalleImpuestoDesglosadoTotales2 D on nC.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=NC.IDSucursal 
Where Anulado = 'False'

Union All

--Nota Crédito Cliente Anulado
Select 
'Fecha'=Convert(date, NC.Fecha),
'Comprobante'=convert(varchar(50),NC.Comprobante),
NC.NroComprobante,
NC.IDTipoComprobante,
NC.DescripcionTipoComprobante,
NC.[Cod.],
'Sucursal'=S.Descripcion,
NC.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Credito Emitido',
'Credito'=NULL,
'RazonSocial'='-----Comprobante Anulado-----',
'RUC'='',
'Directo'=NULL,
'EX'=0,
'GRAVADO5%'= 0,
'GRAVADO10%'=0,
'EXENTO'=0,
'IVA5%'=0,
'IVA10%'=0,
'Total'=0,
'Descripcion'= NC.Sucursal + ' - ' + convert(varchar(50), NC.Fecha, 5) + ' - Punto Exp. ' + NC.ReferenciaPunto,
NC.IDTransaccion

From VNotaCredito NC
Join VDetalleImpuestoDesglosadoTotales2 D on nC.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=NC.IDSucursal 
Where Anulado = 'True'

Union all

--Nota Débito Cliente
Select 
'Fecha'=Convert(date, ND.Fecha),
'Comprobante'=convert(varchar(50),ND.Comprobante),
ND.NroComprobante,
ND.IDTipoComprobante,
ND.DescripcionTipoComprobante,
ND.[Cod.],
'Sucursal'=S.Descripcion,
ND.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Debito Emitido',
'Credito'=NULL,
'RazonSocial'=ND.Cliente,
ND.RUC,
'Directo'=NULL,
'EX'=D.EXENTO,
'GRAVADO5%'=D.[DISCRIMINADO5%],
'GRAVADO10%'=D.[DISCRIMINADO10%],
D.EXENTO,
D.[IVA5%],
D.[IVA10%],
ND.Total,
'Descripcion'='Debitos a Clientes del ' + convert(varchar(50), ND.Fecha, 5),
ND.IDTransaccion

From VNotaDebito ND
Join VDetalleImpuestoDesglosadoTotales2 D on ND.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=ND.IDSucursal 

Union All

--Nota Crédito Proveedor
Select 
'Fecha'=Convert(date, NC.Fecha),
'Comprobante'=convert(varchar(50),NC.Comprobante),
Substring(NC.NroComprobante,9,15) as NroComprobante,
NC.IDTipoComprobante,
NC.DescripcionTipoComprobante,
NC.[Cod.],
'Sucursal'=S.Descripcion,
NC.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Credito Recibido',
'Credito'=NULL,
'RazonSocial'=NC.Proveedor,
NC.RUC,
'Directo'=NULL,
'EX'=D.EXENTO*-1,
'GRAVADO5%'=D.[DISCRIMINADO5%]*-1,
'GRAVADO10%'=D.[DISCRIMINADO10%]*-1,
D.EXENTO*-1,
D.[IVA5%]*-1,
D.[IVA10%]*-1,
NC.Total*-1,
'Descripcion'='Creditos de Proveedores del ' + convert(varchar(50), NC.Fecha, 5),
NC.IDTransaccion

From VNotaCreditoProveedor NC
Join VDetalleImpuestoDesglosadoTotales2 D on NC.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=NC.IDSucursal 


Union All

--Nota Débito Proveedor
Select 
'Fecha'=Convert(date, ND.Fecha),
'Comprobante'=convert(varchar(50),ND.Comprobante),
Substring(ND.NroComprobante,9,15) as NroComprobante,
ND.IDTipoComprobante,
ND.DescripcionTipoComprobante,
ND.[Cod.],
'Sucursal'=S.Descripcion,
ND.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Nota Debito Recibido',
'Credito'=NULL,
'RazonSocial'=ND.Proveedor,
ND.RUC,
'Directo'=NULL,
'EX'=D.EXENTO,
'GRAVADO5%'=D.[DISCRIMINADO5%],
'GRAVADO10%'=D.[DISCRIMINADO10%],
D.EXENTO,
D.[IVA5%],
D.[IVA10%],
ND.Total,
'Descripcion'='Debitos de Proveedores del ' + convert(varchar(50), ND.Fecha, 5),
ND.IDTransaccion
From VNotaDebitoProveedor ND
Join VDetalleImpuestoDesglosadoTotales2 D on ND.IDTransaccion=D.IDTransaccion
Join VSucursal S on S.ID=ND.IDSucursal 

Union All

--Libro Retencion
Select 
'Fecha'=Convert(date, ND.Fecha),
'Comprobante'=convert(varchar(50),ND.Comprobante),
substring(ND.Comprobante,9,15) as NroComprobante,
ND.IDTipoComprobante,
ND.DescripcionTipoComprobante,
ND.[Cod.],
'Sucursal'=S.Descripcion,
ND.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Retencion',
'Credito'=NULL,
'RazonSocial'=ND.Proveedor,
ND.RUC,
'Directo'=NULL,
'EX'=0,
'GRAVADO5%'= Sum(DR.Gravado5)  - Sum(DR.IVA5) ,
'GRAVADO10%'= Sum(DR.Gravado10)  - Sum(DR.IVA10) ,
'EXENTO'=0,
Sum(DR.IVA5) ,
Sum(DR.IVA10) ,
'Total'=Sum(DR.RetencionIVA),
'Descripcion'='Retenciones del ' + convert(varchar(50), ND.Fecha, 5),
ND.IDTransaccion


From VRetencionIVA  ND
Join VComprobanteRetencion DR On DR.IDTransaccion = ND.IDTransaccion 
Join VSucursal S on S.ID=ND.IDSucursal 
Where Anulado = 'False'
Group By ND.IDTransaccion, ND.Fecha, ND.Comprobante, ND.IDTipoComprobante, ND.DescripcionTipoComprobante, ND.[Cod.],
S.Descripcion, ND.IDSucursal, S.IDCiudad, S.Ciudad, ND.Proveedor, ND.RUC

Union All

--Libro Retencion Anulado
Select 
'Fecha'=Convert(date, ND.Fecha),
'Comprobante'=convert(varchar(50),ND.Comprobante),
substring(ND.Comprobante,9,15) as NroComprobante,
ND.IDTipoComprobante,
ND.DescripcionTipoComprobante,
ND.[Cod.],
'Sucursal'=S.Descripcion,
ND.IDSucursal,
S.IDCiudad,
S.Ciudad,
'Tipo'='Retencion',
'Credito'=NULL,
'RazonSocial'='-----Comprobante Anulado-----',
ND.RUC,
'Directo'=NULL,
'EX'=0,
'GRAVADO5%'=0,
'GRAVADO10%'=0,
'EXENTO'=0,
0 ,
0,
'Total'=0,
'Descripcion'='Retenciones del ' + convert(varchar(50), ND.Fecha, 5),
ND.IDTransaccion

From VRetencionIVA  ND
Join VSucursal S on S.ID=ND.IDSucursal 
Where Anulado = 'True'
Group By ND.IDTransaccion, ND.Fecha, ND.Comprobante, ND.IDTipoComprobante, ND.DescripcionTipoComprobante, ND.[Cod.],
S.Descripcion, ND.IDSucursal, S.IDCiudad, S.Ciudad, ND.Proveedor, ND.RUC






﻿CREATE View [dbo].[VCFVentaDescuento]
As
Select
V.*,
CFV.IDTipoDescuento,
'TipoDescuento'=TD.Descripcion,
'Tipo'=TD.Codigo,

--Proveedor
CFV.IDProveedor,
'Proveedor'=IsNull(P.RazonSocial, '---')

From VCF V
Join CFVentaDescuento CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join TipoDescuento TD On CFV.IDTipoDescuento=TD.ID
Left Outer Join Proveedor P On CFV.IDProveedor=P.ID





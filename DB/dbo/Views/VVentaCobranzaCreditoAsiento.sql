﻿CREATE View [dbo].[VVentaCobranzaCreditoAsiento]
As

Select 
CV.IDTransaccionCobranza,
CV.IDtransaccionVenta,
'Importe'= CV.Importe * V.Cotizacion
From Venta V
Join VentaCobranza CV on CV.IDTransaccionVenta = V.IDTransaccion
Join CobranzaCredito CC on CV.IDTransaccionCobranza = CC.IDTransaccion



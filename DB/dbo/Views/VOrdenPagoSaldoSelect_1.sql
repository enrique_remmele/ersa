﻿


create view [dbo].[VOrdenPagoSaldoSelect] as
SELECT
OP.IDTransaccion,
OP.Numero,
OP.Num,
OP.IDTipoComprobante,
OP.DescripcionTipoComprobante,
OP.TipoComprobante,
OP.[Cod.],
OP.NroComprobante,
OP.Comprobante,
OP.Fecha,
OP.Fec,
OP.Observacion,
OP.PagoProveedor,
OP.AnticipoProveedor,
OP.EgresoRendir,
OP.IDSucursal,
OP.Sucursal,
OP.Suc,
OP.IDCiudad,
OP.Ciudad,
OP.Total,
OP.TotalImporteMoneda,
OP.EstadoOP,
'Seleccionado'='False',
'Cancelar'='False',
'Saldo' =isnull(((IsNull(OP.ImporteMoneda,0) + IsNull(OP.ImporteMonedaDocumento,0)) 
- (Case when OP.AplicarRetencion = 1 then(IsNull((select (sum(VOPE.Importe)) - IsNull(sum(CASE WHEN ISnull(OPE.Retenido,0) = 0 THEN 0 ELSE VOPE.RetencionIVA END),0)  
				from VOrdenPagoEgreso VOPE 
				JOIN OrdenPagoEgreso OPE ON VOPE.IDTransaccionOrdenPago = OPE.IDTransaccionOrdenPago AND VOPE.IDTransaccion = OPE.IDTransaccionEgreso 
			where VOPE.IDTransaccionOrdenPago = OP.IDTransaccion),0)) 
									else (select isnull(sum(VOPE.Importe*VOPE.Cotizacion),0)
				from VOrdenPagoEgreso VOPE 
				JOIN OrdenPagoEgreso OPE ON VOPE.IDTransaccionOrdenPago = OPE.IDTransaccionOrdenPago AND VOPE.IDTransaccion = OPE.IDTransaccionEgreso 
			where VOPE.IDTransaccionOrdenPago = OP.IDTransaccion) end)),0),
OP.AplicarRetencion,
OP.DiferenciaCambio,
OP.IDMoneda,
OP.Moneda,
OP.IDMonedaComprobante,
OP.MonedaComprobante,
OP.IDProveedor,
OP.Proveedor,
OP.RUC,
OP.Referencia,
OP.IDTipoProveedor,
OP.Cheque,
OP.IDCuentaBancaria,
OP.[Cuenta Bancaria],
OP.CuentaBancaria,
OP.IDBanco,
OP.Banco,
OP.CodigoBanco,
OP.Mon,
OP.NroCheque,
OP.FechaCheque,
OP.[Fec Chq],
OP.FechaPago,
OP.FecPago,
OP.Cotizacion,
OP.ImporteMoneda,
OP.ImporteMonedaInforme,
'Importe' = isnull((isnull(OP.Importe,0)+isnull(OP.ImporteDocumento,0)),0),
OP.Diferido,
OP.FechaVencimiento,
OP.[Fec. Venc.],
OP.ALaOrden,
OP.Conciliado,
OP.ChequeAnulado,
OP.ChequeEstado,
OP.CotizacionEfectivo,
OP.IDMonedaEfectivo,
OP.ImporteMonedaEfectivo,
OP.IdMonedaDocumento,
OP.ImporteDocumento,
OP.ImporteMonedaDocumento,
OP.CotizacionDocumento,
OP.FechaTransaccion,
OP.IDDepositoTransaccion,
OP.IDSucursalTransaccion,
OP.IDTerminalTransaccion,
OP.IDUsuario,
OP.usuario,
OP.UsuarioIdentificador,
OP.Anulado,
OP.Estado,
OP.IDUsuarioAnulacion,
OP.UsuarioAnulacion,
OP.UsuarioIdentificacionAnulacion,
OP.FechaAnulacion,
OP.Impreso,
OP.Tipo,
OP.Retencion,
OP.Grupo1,
OP.Grupo2,
OP.Grupo3
FROM VOrdenPago OP
where isnull(OP.Anulado,'false') = 'false'
and OP.IDMoneda <> 1

union all

SELECT
VOP.IDTransaccion,
VOP.Numero,
VOP.Num,
VOP.IDTipoComprobante,
VOP.DescripcionTipoComprobante,
VOP.TipoComprobante,
VOP.[Cod.],
VOP.NroComprobante,
VOP.Comprobante,
VOP.Fecha,
VOP.Fec,
VOP.Observacion,
VOP.PagoProveedor,
VOP.AnticipoProveedor,
VOP.EgresoRendir,
VOP.IDSucursal,
VOP.Sucursal,
VOP.Suc,
VOP.IDCiudad,
VOP.Ciudad,
VOP.Total,
VOP.TotalImporteMoneda,
VOP.EstadoOP,
'Seleccionado'='False',
'Cancelar'='False',
'Saldo' = ISnull(((IsNull(VOP.Importe,0) + IsNull(VOP.ImporteDocumento,0)) 
- (Case when VOP.AplicarRetencion = 1 then(IsNull((select (sum(VOPE.Importe*VOPE.Cotizacion))  - IsNull(sum(CASE WHEN ISnull(OPE.Retenido,0) = 0 THEN 0 ELSE VOPE.RetencionIVA END*VOPE.Cotizacion),0)  
				from VOrdenPagoEgreso VOPE 
				JOIN OrdenPagoEgreso OPE ON VOPE.IDTransaccionOrdenPago = OPE.IDTransaccionOrdenPago AND VOPE.IDTransaccion = OPE.IDTransaccionEgreso 
			where VOPE.IDTransaccionOrdenPago = VOP.IDTransaccion),0)) 
									else (select isnull(sum(VOPE.Importe*VOPE.Cotizacion),0)
				from VOrdenPagoEgreso VOPE 
				JOIN OrdenPagoEgreso OPE ON VOPE.IDTransaccionOrdenPago = OPE.IDTransaccionOrdenPago AND VOPE.IDTransaccion = OPE.IDTransaccionEgreso 
			where VOPE.IDTransaccionOrdenPago = VOP.IDTransaccion) end)),0),
VOP.AplicarRetencion,
VOP.DiferenciaCambio,
VOP.IDMoneda,
VOP.Moneda,
VOP.IDMonedaComprobante,
VOP.MonedaComprobante,
Vop.IDProveedor,
VOP.Proveedor,
VOP.RUC,
VOP.Referencia,
VOP.IDTipoProveedor,
VOP.Cheque,
VOP.IDCuentaBancaria,
VOP.[Cuenta Bancaria],
VOP.CuentaBancaria,
VOP.IDBanco,
VOP.Banco,
VOP.CodigoBanco,
VOP.Mon,
VOP.NroCheque,
VOP.FechaCheque,
VOP.[Fec Chq],
VOP.FechaPago,
VOP.FecPago,
VOP.Cotizacion,
VOP.ImporteMoneda,
VOP.ImporteMonedaInforme,
'Importe' = ISNUll((isnull(VOP.Importe,0)+isnull(VOP.ImporteDocumento,0)),0),
VOP.Diferido,
VOP.FechaVencimiento,
VOP.[Fec. Venc.],
VOP.ALaOrden,
VOP.Conciliado,
VOP.ChequeAnulado,
VOP.ChequeEstado,
VOP.CotizacionEfectivo,
VOP.IDMonedaEfectivo,
VOP.ImporteMonedaEfectivo,
VOP.IdMonedaDocumento,
VOP.ImporteDocumento,
VOP.ImporteMonedaDocumento,
VOP.CotizacionDocumento,
VOP.FechaTransaccion,
VOP.IDDepositoTransaccion,
VOP.IDSucursalTransaccion,
VOP.IDTerminalTransaccion,
VOP.IDUsuario,
VOP.usuario,
VOP.UsuarioIdentificador,
VOP.Anulado,
VOP.Estado,
VOP.IDUsuarioAnulacion,
VOP.UsuarioAnulacion,
VOP.UsuarioIdentificacionAnulacion,
VOP.FechaAnulacion,
VOP.Impreso,
VOP.Tipo,
VOP.Retencion,
VOP.Grupo1,
VOP.Grupo2,
VOP.Grupo3
FROM VOrdenPago VOP
where isnull(VOP.Anulado,'false') = 'false'
and ISnull(VOP.IDMoneda,1) = 1





--GO


--ALTER view [dbo].[VOrdenPagoSaldo] as
--SELECT
--VOP.IDTransaccion,
--VOP.Numero,
--VOP.Num,
--VOP.IDTipoComprobante,
--VOP.DescripcionTipoComprobante,
--VOP.TipoComprobante,
--VOP.[Cod.],
--VOP.NroComprobante,
--VOP.Comprobante,
--VOP.Fecha,
--VOP.Fec,
--VOP.Observacion,
--VOP.PagoProveedor,
--VOP.AnticipoProveedor,
--VOP.EgresoRendir,
--VOP.IDSucursal,
--VOP.Sucursal,
--VOP.Suc,
--VOP.IDCiudad,
--VOP.Ciudad,
--VOP.Total,
--VOP.TotalImporteMoneda,
----'Saldo' = 
----((IsNull(VOP.Importe,0) + IsNull(VOP.ImporteDocumento,0)) - IsNull((select sum(VOPE.Importe)*IsNull (VOP.Cotizacion,1) -Isnull(Case when VOP.AplicarRetencion = 1 Then 
---- - (Select sum (VOPE.RetencionIVA) 
----else 
----0 
----from VOrdenPagoEgreso VOPE 
----where VOPE.IDTransaccionOrdenPago = VOP.IDTransaccion),0))
--'Saldo' = ISnull(((IsNull(VOP.Importe,0) + IsNull(VOP.ImporteDocumento,0))- (Case when VOP.AplicarRetencion = 1 then
--(IsNull((select (sum(VOPE.Importe*VOPE.Cotizacion))  - IsNull(sum(CASE WHEN ISnull(OPE.Retenido,0) = 0 THEN
-- 0 
-- ELSE
--  VOPE.RetencionIVA END*VOPE.Cotizacion),0)  
--				from VOrdenPagoEgreso VOPE 
--				JOIN OrdenPagoEgreso OPE ON VOPE.IDTransaccionOrdenPago = OPE.IDTransaccionOrdenPago AND VOPE.IDTransaccion = OPE.IDTransaccionEgreso 
--			where VOPE.IDTransaccionOrdenPago = VOP.IDTransaccion),0)) 
--									else 
--									(select isnull(sum(VOPE.Importe*OP.Cotizacion),0) + IsNull(VOP.ImporteDocumento,0) 
--				from VOrdenPagoEgreso VOPE 
--				JOIN OrdenPagoEgreso OPE ON VOPE.IDTransaccionOrdenPago = OPE.IDTransaccionOrdenPago AND VOPE.IDTransaccion = OPE.IDTransaccionEgreso 
--				JOIN OrdenPago OP ON OPE.IDTransaccionOrdenPago = OP.IDTransaccion
--			where VOPE.IDTransaccionOrdenPago = VOP.IDTransaccion) end)),0),
--VOP.AplicarRetencion,
--VOP.DiferenciaCambio,
--VOP.IDMoneda,
--VOP.Moneda,
--VOP.IDMonedaComprobante,
--VOP.MonedaComprobante,
--Vop.IDProveedor,
--VOP.Proveedor,
--VOP.RUC,
--VOP.Referencia,
--VOP.IDTipoProveedor,
--VOP.Cheque,
--VOP.IDCuentaBancaria,
--VOP.[Cuenta Bancaria],
--VOP.CuentaBancaria,
--VOP.IDBanco,
--VOP.Banco,
--VOP.CodigoBanco,
--VOP.Mon,
--VOP.NroCheque,
--VOP.FechaCheque,
--VOP.[Fec Chq],
--VOP.FechaPago,
--VOP.FecPago,
--VOP.Cotizacion,
--VOP.ImporteMoneda,
--VOP.ImporteMonedaInforme,
--'Importe' = ISNUll((isnull(VOP.Importe,0)+isnull(VOP.ImporteDocumento,0)),0),
--VOP.Diferido,
--VOP.FechaVencimiento,
--VOP.[Fec. Venc.],
--VOP.ALaOrden,
--VOP.Conciliado,
--VOP.ChequeAnulado,
--VOP.ChequeEstado,
--VOP.CotizacionEfectivo,
--VOP.IDMonedaEfectivo,
--VOP.ImporteMonedaEfectivo,
--VOP.IdMonedaDocumento,
--VOP.ImporteDocumento,
--VOP.ImporteMonedaDocumento,
--VOP.CotizacionDocumento,
--VOP.FechaTransaccion,
--VOP.IDDepositoTransaccion,
--VOP.IDSucursalTransaccion,
--VOP.IDTerminalTransaccion,
--VOP.IDUsuario,
--VOP.usuario,
--VOP.UsuarioIdentificador,
--VOP.Anulado,
--VOP.Estado,
--VOP.IDUsuarioAnulacion,
--VOP.UsuarioAnulacion,
--VOP.UsuarioIdentificacionAnulacion,
--VOP.FechaAnulacion,
--VOP.Impreso,
--VOP.Tipo,
--VOP.Retencion,
--VOP.Grupo1,
--VOP.Grupo2,
--VOP.Grupo3
--FROM VOrdenPago VOP
--where isnull(VOP.Anulado,'false') = 'false'








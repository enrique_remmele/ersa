﻿Create view [dbo].[VInformeAccesoUsuarioAgrupado]
as
select 
Codigo,
VA.Descripcion,
VA.Nivel,
VA.NombreControl,
VA.CodigoPadre,
VA.Tag,
VA.IDPerfil,
VA.Perfil,
'Visualizar'=Case When VA.Visualizar = 'True' Then 'SI' Else 'NO' End,
'Agregar'=Case When VA.Agregar = 'True' Then 'SI' Else 'NO' End,
'Modificar'=Case When VA.Modificar = 'True' Then 'SI' Else 'NO' End,
'Eliminar'=Case When VA.Eliminar = 'True' Then 'SI' Else 'NO' End,
'Anular'=Case When VA.Anular = 'True' Then 'SI' Else 'NO' End,
'Imprimir'=Case When VA.Imprimir = 'True' Then 'SI' Else 'NO' End,
'IDUsuario'=U.ID,
U.Nombre,
U.Usuario,
U.Identificador,
U.IDVendedor,
'Estado'=Case When U.Estado = 'True' Then 'Activo' Else 'Inactivo' End

from VAccesoPerfil VA
Join Usuario U On VA.IDPerfil = U.IDPerfil

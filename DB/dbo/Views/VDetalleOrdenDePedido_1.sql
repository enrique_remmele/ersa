﻿CREATE View [dbo].[VDetalleOrdenDePedido]
As

Select	

--Sergio 07-03-2014

DM.IDTransaccion,
DM.IDProducto,
DM.ID,
M.IDDeposito,
'Deposito'=IsNull((Select D.Descripcion From Deposito D Where D.ID=DM.IDDeposito), '---') ,
DM.Observacion,
DM.Existencia,
DM.Pedido,
DM.Extra,
DM.FechaEntrega,
'FecEntrega'=CONVERT(varchar(50), DM.FechaEntrega, 6),
M.Anulado, --JGR 20140520 estaba DM.Anulado

--Operacion
M.Numero,
M.Fecha,
M.Fec,

--Producto
'Producto'=Concat(P.Referencia,' - ',P.Descripcion) + (Case When DM.Observacion!='' Then ' - ' + DM.Observacion Else '' End),
'Descripcion'=P.Descripcion,
'CodigoBarra'=P.CodigoBarra,
'Ref'=P.Referencia,
P.IDTipoProducto,

 'Cod'=M.[Cod.],
 M.NroComprobante,
 M.IDSucursal,
 M.Autorizacion,
M.IdTipoComprobante,
'CantidadAEntregar'=Isnull(DM.CantidadAEntregar,DM.Extra),
'Peso'=cast(P.Peso as money),
'ExistenciaMinima'= IsNull((Select ExistenciaMinima from ExistenciaDeposito where IDProducto = DM.IDProducto and IDDeposito = DM.IDDeposito),0),
'Kilos'= (Extra*P.Peso),
'Saldo' = IsNull(CantidadAEntregar - (select Isnull(Sum(Cantidad),0) from detalleAsignacionCamion where IDtransaccion = M.IDtransaccion and ID = DM.ID ),0)
From DetalleOrdenDePedido DM
Join VOrdenDePedido M On DM.IDTransaccion=M.IDTransaccion
Join Producto P On DM.IDProducto=P.ID

















﻿
--USE [EXPRESS]
--GO

--/****** Object:  View [dbo].[VCuentaFijaCobranzaCredito]    Script Date: 07/25/2012 09:22:46 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


CREATE View [dbo].[VCuentaFijaCobranzaCredito]
As

Select 
C.ID,
C.Descripcion,
C.Orden,
C.IDTipoComprobante,
'TipoComprobante'=IsNull((Select TC.Descripcion From TipoComprobante TC Where TC.ID=C.IDTipoComprobante), '---'),
C.IDMoneda,
'Moneda'=M.Descripcion,
'Tipo'=(Case When C.Contado='True' Then 'CONTADO' Else (Case When C.Credito='True' Then 'CREDITO' Else '---' End) End),

'Debe'='False',
'Haber'='True',
'Credito'=0,
'Debito'=0,

C.IDCuentaContable,
'Cuenta'=CC.Codigo + ' ' + CC.Descripcion,
'CuentaContable'=CC.Descripcion,
'Codigo'=CC.Codigo


From CuentaFijaCobranzaCredito C
Join CuentaContable CC On C.IDCuentaContable=CC.ID
Join Moneda M On C.IDMoneda=M.ID




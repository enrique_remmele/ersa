﻿CREATE View [dbo].[VOrdenPagoEfectivo]

As

Select 

--Orden de Pago
OPE.IDTransaccionOrdenPago,

--Efectivo
E.IDTransaccion,
'Sel'='True',
E.Comprobante,
E.CodigoComprobante,
E.Fec,
OPE.Importe,
'Saldo'=E.Saldo,
'ImporteHabilitado'=E.Importe,
'APagar'=OPE.Importe,
'Cancelar'='False',
'Generado'= E.Generado 

From OrdenPagoEfectivo OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join VEfectivo E On OPE.IDTransaccionEfectivo=E.IDTransaccion And OPE.IDEfectivo=E.ID

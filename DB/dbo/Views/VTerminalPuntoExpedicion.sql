﻿


CREATE View [dbo].[VTerminalPuntoExpedicion]
As

Select 
'Terminal'=T.Descripcion,
TP.IDTerminal,

--Operacion
TP.IDOperacion,
'Operacion'=O.Descripcion,

--Comprobante
PE.IDTipoComprobante,
PE.TipoComprobante,

--Referencias
PE.IDSucursal,
PE.ReferenciaSucursal,
PE.Suc ,
PE.Ciudad,
PE.IDCiudad,
PE.ReferenciaPunto,

--Punto de Expedicion
TP.IDPuntoExpedicion,
'PE'=PE.Descripcion,
PE.Vencimiento,
PE.Talonarios,
PE.TalonarioActual,
PE.NumeracionDesde,
PE.NumeracionHasta,
PE.ProximoNumero, 
PE.Timbrado,
'Saldo'=(PE.NumeracionHasta - PE.ProximoNumero) + 1 
From TerminalPuntoExpedicion TP
Join VTerminal T On TP.IDTerminal=T.ID
Join Operacion O On TP.IDOperacion=O.ID 
Join VPuntoExpedicion PE On TP.IDPuntoExpedicion=PE.ID





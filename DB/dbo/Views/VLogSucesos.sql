﻿


CREATE View [dbo].[VLogSucesos]

As

Select 
L.ID,
L.Fecha,
'Fec'=CONVERT(varchar(50), L.Fecha, 6),
'Hora'=CONVERT(varchar(50), L.Fecha, 8),
L.IDTipoOperacionLog,
'Operacion'=T.Descripcion,
'Cod. Oper.'=T.Codigo,
L.IDTabla,
'Tabla'=TA.Descripcion,
'IDUsuario'=IsNull(L.IDUsuario, 0),
'Usuario'=IsNull(U.Usuario, '---'),
'IDTerminal'=IsNull(L.IDTerminal, 0),
'Terminal'=IsNull(TER.Descripcion, '---'),
'IDSucursal'=IsNull(TER.IDSucursal, 0),
'Sucursal'=IsNull(TER.Sucursal, '---'),
'Comprobante'=ISNULL(L.Comprobante, '---'),
'IDTransaccion'=ISNULL(L.IDTransaccion, 0)

From LogSucesos L
Join TipoOperacionLog T On L.IDTipoOperacionLog=T.ID
Join TablaLog TA On L.IDTabla = TA.ID
Left Outer Join Usuario U On L.IDUsuario=U.ID
Left Outer Join VTerminal TER On L.IDTerminal=TER.ID





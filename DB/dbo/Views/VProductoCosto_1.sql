﻿
CREATE View [dbo].[VProductoCosto]

As

Select 
M.IDTransaccion,
M.Numero,
'Num'=M.Numero,
M.Fecha,
'Fec'=CONVERT(varchar(50), M.Fecha, 6),
M.Observacion,
M.Total,
M.Anulado,
'Estado'=Case When M.Anulado='True' Then 'Anulado' Else 'OK' End,
'CostoProduccion'= isnull(M.CostoProduccion,'False'),
--Transaccion
'FechaTransaccion'=TR.Fecha,
TR.IDDeposito,
TR.IDSucursal,
TR.IDTerminal,
TR.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion)

From ProductoCosto M
Join Transaccion TR On M.IDTransaccion=TR.ID

﻿create view vPorcentajeExcepcion 
as
Select 
P.IDUsuario,
'NombreUsuario'=U.Nombre,
P.Porcentaje,
P.Importe,
P.Estado
From PorcentajeExcepcion P
join Usuario U on P.IDUsuario = U.ID

﻿
CREATE view [dbo].[VVentaTotalAnualProductoCliente]
as
--Total Enero
Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=DV.Total,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Enero' 

--Total Febrero

union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=DV.Total,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Febrero'

--Total Marzo
Union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=DV.Total,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Marzo'

union all

--Total Abril
Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=DV.Total,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Abril'

--Total Mayo
union all 

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=DV.Total,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Mayo'

--Total Junio

Union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=DV.Total,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Junio'

--Total Julio

Union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=DV.Total,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Julio'

--Total Agosto

Union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=DV.Total,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Agosto'

--Total Setiembre

Union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=DV.Total,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Setiembre'

--Total Octubre

Union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=DV.Total,
'Noviembre'=0,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Octubre'

--Total Noviembre

Union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=DV.Total,
'Diciembre'=0,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Noviembre'

--Total Diciembre

Union all

Select 
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.IDMoneda,
V.IDTipoComprobante,
V.IDVendedor,
V.IDCiudad,
V.IDSucursal,
V.IDDeposito,
V.IDZonaVenta,
V.IDListaPrecio,
V.Credito,
DV.Producto,
V.IDCiudadCliente,
'ReferenciaProducto'=DV.Referencia,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=DV.Total,
DV.IDTipoProducto,
DV.IDLinea,
DV.IDSubLinea,
DV.IDSubLinea2,
DV.IDMarca,
DV.IDPresentacion,
DV.IDCategoria,
DV.IDProveedor,
DV.IDDivision,
DV.IDProcedencia
From VVenta V 
Join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion
Where V.MesP='Diciembre'



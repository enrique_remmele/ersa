﻿CREATE View [dbo].[VFormularioImpresion]
As

Select 
F.IDFormularioImpresion,
F.IDImpresion,
F.Descripcion,
F.Detalle,
F.CantidadDetalle,
F.Copias,
F.Fondo,
F.TamañoFondoX,
F.TamañoFondoY,

--Impresion
'Impresion'=I.Descripcion,
I.Vista,
I.VistaDetalle,
I.Observacion,
I.IDOperacion,
I.Operacion,
I.FormName,

--Papel 
'TamañoPapel'=IsNull(F.TamañoPapel, 'A4'),
'TamañoPapelX'=IsNull(F.TamañoPapelX, 827),
'TamañoPapelY'=IsNull(F.TamañoPapelY, 1169),
'Horizontal'=IsNull(F.Horizontal, 'False'),
'MargenIzquierdo'=IsNull(F.MargenIzquierdo, 0),
'MargenDerecho'=IsNull(F.MargenDerecho, 0),
'MargenArriba'=IsNull(F.MargenArriba, 0),
'MargenAbajo'=IsNull(F.MargenAbajo, 0),
'Estado'= IsNull(F.Estado,'False')

From FormularioImpresion F
Join VImpresion I On F.IDImpresion=I.IDImpresion



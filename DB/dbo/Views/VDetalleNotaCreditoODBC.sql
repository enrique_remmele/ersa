﻿
CREATE View [dbo].[VDetalleNotaCreditoODBC]
As
Select
DNC.IDTransaccion,

--Producto
DNC.IDProducto,
'Producto'=P.Descripcion,
DNC.ID,
--DNC.Observacion, 
--'Observacion'=concat(P.Descripcion,' - ',DNC.Observacion),
'Observacion'=(Case When (NC.Devolucion) = 0 Then DNC.Observacion Else concat(P.Descripcion,' - ',DNC.Observacion) End),
--P.Descripcion,
'Descripcion'=(Case When (NC.Devolucion) = 0 Then DNC.Observacion Else P.Descripcion End),
'CodigoBarra'=P.CodigoBarra,
P.Referencia ,

--Deposito
NC.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DNC.Cantidad,
DNC.PrecioUnitario,
'Pre. Uni.'=DNC.PrecioUnitario,
DNC.Total,

--Impuesto
DNC.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
DNC.TotalImpuesto,
DNC.TotalDiscriminado,
'Exento'=I.Exento,

--Contabilidar
P.CuentaContableVenta,

--Descuento
DNC.TotalDescuento,
'Descuento'=DNC.TotalDescuento,
DNC.DescuentoUnitario,
'Desc Uni'=DNC.DescuentoUnitario,
DNC.PorcentajeDescuento,
'Desc %'=DNC.PorcentajeDescuento,
'DescuentoUnitarioDiscriminado'=IsNull(DNC.DescuentoUnitarioDiscriminado,0),
'TotalDescuentoDiscriminado'=IsNull(DNC.TotalDescuentoDiscriminado,0),

--Costos
DNC.CostoUnitario,
DNC.TotalCosto,
DNC.TotalCostoImpuesto,
DNC.TotalCostoDiscriminado,

--Venta
DNC.IDTransaccionVenta,
'Comprobante'= IsNull(V.Comprobante , '---'),

--Cantidad de Caja
'Caja'=IsNull(DNC.Caja, 'False'),
DNC.CantidadCaja,
'Cajas'=  ISNULL ((DNC.Cantidad ),1) / ISNULL((Case When (P.UnidadPorCaja) = 0 Then 1 Else P.UnidadPorCaja End),1),

--NotaCredito
'FechaNCR'=NC.Fecha,
NC.Fecha,
NC.IDSucursal,
NC.NroComprobante,
NC.IDCliente,
NC.Anulado,
'ComprobanteNC'=(select top(1) Comprobante from VNotaCredito where IDTransaccion = NC.IDTransaccion),
-- Cliente
NC.RUc,
NC.Cliente,
'RazonSocialSucursal'= (Case when isnull(NC.SucursalCliente,'') = '' then NC.Cliente else COncat(NC.Cliente, ' - ', NC.SucursalCliente) end),
NC.IDSucursalCliente,
'SucursalCliente' = (Case when NC.IDSucursalCliente = 0 then 'MATRIZ' else (Select Sucursal from ClienteSucursal where idcliente = NC.IDCliente and ID = NC.IDSucursalCliente)end),

--Totales
'TotalBruto'= isnull(DNC.TotalDescuento,0)+isnull(DNC.Total,0),
'TotalSinDescuento'=DNC.Total - DNC.TotalDescuento,
'TotalSinImpuesto'=DNC.Total - DNC.TotalImpuesto,
'TotalNeto'=DNC.Total - (DNC.TotalImpuesto + DNC.TotalDescuento),
'TotalNetoConDescuentoNeto'=DNC.TotalDiscriminado + DNC.TotalDescuentoDiscriminado,
NC.Devolucion,
'IDCiudad'=NC.IDSucursal,
'IDMotivo'=Isnull(NC.IDMotivo,0),
'IDMotivoDevolucion'= (Case when IDMotivoDevolucion is null then 0 else IDMotivoDevolucion end),
'MotivoDevolucion'=(Case when (IDMotivoDevolucion = 0 or IDMotivoDevolucion is null) then '' else (select Descripcion from MotivoDevolucionNC where ID = IDMotivoDevolucion) end),
NC.IDMoneda,
P.IDTipoProducto,
'TipoProducto'=(Select Descripcion from TipoProducto where ID = P.IDTipoProducto),
P.IDLinea,
P.IDUnidadMedida,
'UnidadMedida'=(Select Referencia from UnidadMedida where ID = P.IDUnidadMedida),
P.Peso,
NC.IDPromotor,
NC.Promotor,
NC.IDVendedor,
NC.Vendedor

From DetalleNotaCredito DNC
Join vNotaCredito NC On DNC.IDTransaccion=NC.IDTransaccion
Left Outer Join  VVenta V On DNC.IDTransaccionVenta=V.IDTransaccion
Join Producto P On DNC.IDProducto=P.ID
Join Deposito D On NC.IDDeposito=D.ID
Join Impuesto I On DNC.IDImpuesto=I.ID
where NC.Procesado = 1














﻿

CREATE View [dbo].[VAnticipoAplicacion]
As
Select 
A.IDTransaccion,
A.IDTransaccionCobranza,
A.Numero,
A.IDTipoComprobante,
'TipoComprobante'= (Select Codigo from TipoComprobante where ID = A.IDTipoComprobante),
A.NroComprobante,
A.IDSucursal,
'Sucursal'=(Select Codigo from Sucursal where id = A.IDSucursal),
'IDSucursalCobranza' = CC.IDSucursal,
A.Fecha,
A.IDMoneda,
'Moneda'=(Select Referencia from MOneda where id = A.IDMoneda),
A.Cotizacion,
'Anulado' = (Case when A.Anulado = 1 then 'True' else 'False' end),
'FechaAnulacion'=A.FechaAnulado,
A.IDUsuarioAnulado,
'UsuarioIdentificacionAnulacion' = ISnull((select usuario from usuario where ID = A.IDUsuarioAnulado),''),
A.Observacion,
A.Total,
A.DiferenciaCambio,
CC.FechaEmision,
CC.IDCliente,
CC.Referencia,
CC.Cliente,
CC.RUC,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
CC.Cobrador,
CC.NroPlanilla,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario)

From AnticipoAplicacion A
Join Sucursal S on S.ID = A.IDSucursal
Join VCobranzaCredito CC on CC.IDTransaccion = A.IDTransaccionCobranza
Join Transaccion T on T.ID = A.IDTransaccion





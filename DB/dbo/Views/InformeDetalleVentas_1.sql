﻿

CREATE View [dbo].[InformeDetalleVentas]
As
Select 
D.IDTransaccion
      ,D.IDProducto
      ,D.ID
      ,D.Producto
      ,D.Descripcion
      ,D.CodigoBarra
      ,D.IDLinea
      ,D.Linea
      ,D.IDSubLinea
      ,D.SubLinea
      ,D.IDSubLinea2
      ,D.SubLinea2
      ,D.IDTipoProducto
      ,D.TipoProducto
      ,D.IDMarca
      ,D.Marca
      ,D.IDPresentacion
      ,D.Referencia
      ,D.Observacion
      ,D.IDUnidadMedida
      ,D.PesoUnitario
      ,D.Peso
      ,D.UnidadMedida
      ,D.IDDeposito
      ,D.Deposito
      ,D.Cantidad
      ,D.PrecioUnitario
      ,D.[Pre. Uni.]
      ,D.Total
      ,D.IDImpuesto
      ,D.Impuesto
      ,D.[Ref Imp]
      ,D.TotalImpuesto
      ,D.TotalDiscriminado
      ,D.Exento
      ,D.TotalDescuento
      ,D.Descuento
      ,D.DescuentoUnitario
      ,D.[Desc Uni]
      ,D.DescuentoUnitarioDiscriminado
      ,D.TotalDescuentoDiscriminado
      ,D.PorcentajeDescuento
      ,D.[Desc %]
      ,D.PrecioNeto
      ,D.TotalPrecioNeto
      ,D.CostoUnitario
      ,D.TotalCosto
      ,D.TotalCostoImpuesto
      ,D.TotalCostoDiscriminado
      ,D.Caja
      ,D.CantidadCaja
      ,D.Cajas
      ,D.UnidadMedidas
      ,D.UnidadMedidaConvertir
      ,D.UnidadConvertir
      ,D.TotalBruto
      ,D.TotalSinDescuento
      ,D.TotalSinImpuesto
      ,D.TotalNeto
      ,D.TotalNetoConDescuentoNeto
      ,D.CuentaContableVenta
      ,D.IDCliente
      ,D.Cliente
      ,D.TipoCliente
      ,D.FechaEmision
      ,D.IDMoneda
      ,D.Moneda
      ,D.Credito
      ,D.IDVendedor
      ,D.IDZonaVenta
      ,D.ReferenciaSucursal
      ,D.ReferenciaPunto
      ,D.NroComprobante
      ,D.[Cod.]
      ,D.MesNumero
      ,D.IDSucursal
,'Comprobante'=V.Comprobante 
From VDetalleVenta D
Join VVenta V On D.IDTransaccion=V.IDTransaccion





﻿


CREATE view [dbo].[vSolicitudProductoListaPrecioExcepciones]
as
select 
P.Referencia,
S.IDProducto,
'Producto' = P.Descripcion,
S.IDListaPrecio,
'ListaPrecio' = L.Descripcion,
S.IDCliente,
'Codigo Cliente'=C.Referencia,
'Cliente'= C.RazonSocial,
S.IDTipoDescuento,
'TipoDescuento' = TD.Descripcion,
S.IDMoneda,
'Moneda' = (Select Referencia from Moneda where id = S.IDMoneda),
'PrecioLista' = Convert(decimal(18,0),PLP.Precio),
'Descuento' = Convert(decimal(18,0),S.Descuento),
'PrecioConDescuento'= Convert(decimal(18,0), PLP.Precio - S.Descuento),
'Costo'=Isnull(Convert(decimal(18,0),P.CostoPromedio),Convert(decimal(18,0),P.UltimoCosto)),
'Porcentaje' = Case When S.Porcentaje > 99 then 99 else S.Porcentaje end,
'Desde' = Convert(date, S.Desde),
'Hasta' = Convert(date, S.Hasta),
S.CantidadLimite,
S.FechaSolicitud,
S.IDUsuarioSolicitud,
'Solicitante' = (select Nombre from Usuario where id = S.IDUsuarioSolicitud),
S.FechaAprobado,
S.IDUsuarioAprobado,
'Aprobado/Rechazado' = (Case when S.IDUsuarioAprobado is null then '' else (Select Nombre from usuario where id = S.IDUsuarioAprobado) end),
'Estado' = (Case when S.Estado is null then 'PENDIENTE' else
			(case when S.Estado = 1 then 'APROBADO' else
			(case when S.Estado = 0 then 'RECHAZADO' end) end)end)
from SolicitudProductoListaPrecioExcepciones S
join ListaPrecio L on S.IDListaPrecio = L.ID
join Producto P on S.IDProducto = P.ID
left outer join ProductoListaPrecio PLP on L.ID = PLP.IDListaPrecio and P.ID = PLP.IDProducto
join TipoDescuento TD on S.IDTipoDescuento = TD.ID
join Cliente C on S.IDCliente = C.ID







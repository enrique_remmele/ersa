﻿Create View [dbo].[VMoneda]
As
Select 
M.ID, 
'Local'=Case When (M.ID) = 1 Then 'True' Else 'False' End,
'Moneda'=Case When (M.ID) = 1 Then 'LOC' Else 'EXT' End,
M.Descripcion, 
M.Referencia, 
M.Decimales, 
M.Estado, 
M.Divide, 
M.Multiplica,
'Cotizacion'=dbo.FCotizacionAlDia(M.ID,1),
'Fec. Cot.'=dbo.FFechaCotizacionAlDia(M.ID,1,5)
From Moneda M


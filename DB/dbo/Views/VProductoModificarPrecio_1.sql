﻿CREATE view VProductoModificarPrecio
as
Select
PMP.IDProducto,
P.Descripcion,
P.Referencia,
PMP.estado 
from Producto P
join ProductoModificarPrecio PMP on P.ID = PMP.idproducto

﻿
CREATE View [dbo].[VDocumentosSinAsientos]

As

--VENTA
Select
'IDTransaccion'=T.ID,
'Numero'=V.Comprobante,
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.FechaEmision),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=V.Sucursal,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VVenta V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False' And V.Procesado='True'

Union All

--NC
Select
'IDTransaccion'=T.ID,
'Numero'=V.Comprobante,
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=V.Sucursal,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VNotaCredito V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False' And V.Procesado='True'

Union All

--ND
Select
'IDTransaccion'=T.ID,
'Numero'=V.Comprobante,
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=V.Sucursal,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VNotaDebito V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False' And V.Procesado='True'

Union All

--COMPRAS
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Num),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=V.Sucursal,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VCompra V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null

Union All

--NCP
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Num),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=V.Sucursal,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VNotaCreditoProveedor V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False'

Union All

--NDP
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VNotaDebitoProveedor V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False'

Union All

--MOVIMIENTO
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=Concat(cast(V.Operacion as varchar(3)),' - ',V.[Cod.]),
'Comprobante'=V.Comprobante,
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VMovimiento V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False'
--Documentos sin costo, no mostrar
--And V.Total<>0

Union All

--COBRANZA
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.FechaEmision),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=Convert(varchar(50), V.Comprobante),
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VCobranzaCredito V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False'

Union All

--COBRANZA POR LOTE
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=Convert(varchar(50), V.Comprobante),
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VCobranzaContado V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False'

Union All

--CHEQUE RECHAZADO
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'='CHQ.RECH',
'IDTipoComprobante'=0,
'CodigoTipoComprobante'='',
'Comprobante'='',
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Importe,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VChequeClienteRechazado V On T.ID=V.IDTransaccion
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False'

Union All

--DESCUENTO DE CHEQUE

Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=0,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)

From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VDescuentoCheque V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null

Union All

--GASTOS
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VGasto V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null

Union All

--DEPOSITOS BANCARIOS

Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=Convert(varchar(50), V.Comprobante),
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VDepositoBancario V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null

Union All

--DEBITOS Y CREDITOS

Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=Convert(varchar(50), V.Comprobante),
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VDebitoCreditoBancario V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null

Union All

--ORDEN DE PAGO
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=V.IDTipoComprobante,
'CodigoTipoComprobante'=V.[Cod.],
'Comprobante'=V.Comprobante,
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VOrdenPago V On T.ID=V.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null And V.Anulado='False'

Union All

--ENTREGA DE CHEQUE
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), OP.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.FechaEntrega),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=OP.IDTipoComprobante,
'CodigoTipoComprobante'=OP.[Cod.],
'Comprobante'=OP.Comprobante,
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=OP.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)

From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VEntregaChequeOP V On T.ID=V.IDTransaccion
Join VOrdenPago OP On V.IDTransaccionOP=OP.IDTransaccion
Join TipoComprobante TC On OP.IDTipoComprobante=TC.ID
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Where A.IDTransaccion Is Null 
And V.Anulado='False'
And V.ChequeEntregado='True'
And OP.PagoProveedor='True'
And (Select Top(1) Year(OPE.Fecha) From VOrdenPagoEgreso OPE Where OPE.IDTransaccionOrdenPago=OP.IDTransaccion) >= Year(OP.Fecha)

union all

--CANJE CHEQUE RECHAZADO
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), V.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=TC.ID,
'CodigoTipoComprobante'=TC.Codigo,
'Comprobante'='',
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=V.Total,
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VPagoChequeCliente V On T.ID=V.IDTransaccion
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Where A.IDTransaccion Is Null And V.Anulado='False'

union all

--VENCIMIENTO CHEQUE
Select
'IDTransaccion'=T.ID,
'Numero'=Convert(varchar(50), OP.Numero),
'Operacion'=O.Descripcion,
'IDOperacion'=O.ID,
'Fecha'=Convert(date, V.Fecha),
'TipoComprobante'=TC.Descripcion,
'IDTipoComprobante'=TC.ID,
'CodigoTipoComprobante'=TC.Codigo,
'Comprobante'='',
'Sucursal'=S.Descripcion,
'IDSucursal'=V.IDSucursal,
'Importe'=OP.Total, 
'Usuario'= (select usuario from VTransaccion where ID = T.ID)
From Transaccion T
Join Operacion O On T.IDOperacion=O.ID
Join VVencimientoChequeEmitido V On T.ID=V.IDTransaccion
join VOrdenPago OP on V.IDTransaccionOP = OP.IDTransaccion
Join VSucursal S on V.IDSucursal=S.ID
Left Outer Join DetalleAsiento A On V.IDTransaccion=A.IDTransaccion
join TipoComprobante TC on TC.ID = OP.IDTipoComprobante
Where A.IDTransaccion Is Null And V.Anulado='False'




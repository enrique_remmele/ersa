﻿CREATE view [dbo].[vProductoPrecio] 
as 
select 
P.Referencia,
S.IDProducto,
'Producto' = P.Descripcion,
S.IDCliente,
'Codigo Cliente'=C.Referencia,
'Cliente'= C.RazonSocial,
S.PrecioUnico,
'TipoDescuento' = (Case when s.PrecioUnico = 1 then 'UNICO' else 'FIJO'end),
S.IDMoneda,
'Moneda' = (Select Referencia from Moneda where id = S.IDMoneda),
'PrecioLista' = Convert(decimal(18,0),PLP.Precio),
'Descuento' = Convert(decimal(18,0), PLP.Precio - S.Precio),
'PrecioConDescuento'= Convert(decimal(18,0),S.Precio),
'Costo'=Isnull(Convert(decimal(18,0),P.CostoPromedio),Convert(decimal(18,0),P.UltimoCosto)),
'Desde' = Convert(date, S.Desde),
S.CantidadMinimaPrecioUnico																			

from ProductoPrecio S
join Producto P on S.IDProducto = P.ID
join Cliente C on S.IDCliente= C.ID
join Moneda M on S.IDMoneda = M.ID
left outer join ProductoListaPrecio PLP on S.IDProducto = PLP.IDProducto 
										and C.IDListaPrecio = PLP.IDListaPrecio
										and S.IDMoneda =  PLP.IDMoneda 
										and PLP.IDSucursal=1


﻿CREATE View [dbo].[VChequeClienteDocumentosPagados]
As
Select
CC.IDTransaccion,
'Numero'=CC.Numero,
'TipoComprobante'=TC.Descripcion,
CCRE.NroComprobante,
CC.NroCheque,
'Comprobante'=TC.Codigo + ' - ' + CCRE.Suc + ' (' + Convert(varchar(50), CCRE.Numero)  + ')',
'Fecha'=CCRE.FechaEmision,
'Fec'=CONVERT(varchar(50), CCRE.FechaEmision, 6),
'ImporteCheque'=FP.ImporteCheque,
'Importe'=FP.ImporteCheque
 
From ChequeCliente CC
Join FormaPago FP On CC.IDTransaccion=FP.IDTransaccionCheque
Join VCobranzaCredito CCRE On FP.IDTransaccion=CCRE.IDTransaccion
Join TipoComprobante TC On CCRE.IDTipoComprobante=TC.ID
Left Outer Join Sucursal S On CC.IDSucursal=S.ID

Union All

Select
CC.IDTransaccion,
'Numero'=CC.Numero,
'TipoComprobante'=TC.Descripcion,
CCRE.NroComprobante,
CC.NroCheque,
'Comprobante'=TC.Codigo + ' - ' + CCRE.Suc + ' (' + Convert(varchar(50), CCRE.Numero)  + ')',
CCRE.Fecha,
'Fec'=CONVERT(varchar(50), CCRE.Fecha, 6),
'ImporteCheque'=FP.ImporteCheque,
'Importe'=FP.ImporteCheque

From ChequeCliente CC
Join FormaPago FP On CC.IDTransaccion=FP.IDTransaccionCheque
Join VCobranzaContado CCRE On FP.IDTransaccion=CCRE.IDTransaccion
Join TipoComprobante TC On CCRE.IDTipoComprobante=TC.ID
Left Outer Join Sucursal S On CC.IDSucursal=S.ID

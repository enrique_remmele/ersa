﻿CREATE View [dbo].[VTipoProductoVenta]
as
Select
TP.ID,
TP.Referencia,
TP.Descripcion,
TP.Estado,
TP.CuentaContableCliente,
TP.CuentaContableVenta,
TP.CuentaContableCosto,
TP.CuentaContableProveedor,
'Producido'= Isnull(TP.Producido, 'False')
 
From TipoProducto TP
where TP.ID in (select distinct P.IDTipoProducto 
				from Producto P 
				Join DetalleVenta DM on P.ID = DM.IDProducto
				where P.Vendible = 1
				--and P.ControlarExistencia = 1 
				and P.Estado = 1)


﻿

CREATE View [dbo].[VPlanillaDescuentoTactico]
As
Select 

--Cabecera
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
PDT.Numero,
PDT.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
PDT.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
'Comprobante'=PDT.Comprobante,
PDT.IDProveedor,
'Proveedor'=P.RazonSocial,
PDT.Desde,
'Fec. Desde'=CONVERT(varchar(50), PDT.Desde, 6),
PDT.Hasta,
'Fec. Hasta'=CONVERT(varchar(50), PDT.Hasta, 6),


PDT.Observacion,

--Totales
PDT.TotalExedente,
PDT.TotalPresupuesto,
PDT.TotalSaldo,
PDT.TotalUtilizado,
PDT.Anulado ,

--Transaccion
PDT.IDTransaccion,
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=PDT.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=PDT.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=PDT.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=PDT.IDTransaccion)




From PlanillaDescuentoTactico PDT
Join Transaccion T On PDT.IDTransaccion=T.ID
Left Outer Join Sucursal S On PDT.IDSucursal=S.ID
Left Outer Join TipoComprobante TC On PDT.IDTipoComprobante=TC.ID
Left Outer Join Proveedor P On PDT.IDProveedor=P.ID


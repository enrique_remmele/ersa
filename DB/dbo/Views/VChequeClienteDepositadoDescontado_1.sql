﻿CREATE View [dbo].[VChequeClienteDepositadoDescontado]
As

select VCC.idtransaccion,
'FechaDeposito'=case when (select Top(1) convert(varchar(50),Fecha,5) from DepositoBancario DB
join DetalleDepositoBancario DDB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
where DDB.IDTransaccionCheque = VCC.IDTransaccion) <> ''
then
(select Top(1) convert(varchar(50),Fecha,5) from DepositoBancario DB
join DetalleDepositoBancario DDB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
where DDB.IDTransaccionCheque = VCC.IDTransaccion)
else ''
end,

'NumeroDeposito' = Case when(select Top(1) DB.Numero from DepositoBancario DB
join DetalleDepositoBancario DDB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
where DDB.IDTransaccionCheque = VCC.IDTransaccion) <> ''
then
(select Top(1) DB.Numero from DepositoBancario DB
join DetalleDepositoBancario DDB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
where DDB.IDTransaccionCheque = VCC.IDTransaccion)
else
''
end,
'ComprobanteDeposito' = Case when(select Top(1) DB.NroComprobante from DepositoBancario DB
join DetalleDepositoBancario DDB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
where DDB.IDTransaccionCheque = VCC.IDTransaccion) <> ''
then
(select Top(1) DB.NroComprobante from DepositoBancario DB
join DetalleDepositoBancario DDB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion
where DDB.IDTransaccionCheque = VCC.IDTransaccion)
else
''
end,
'FechaDescuento'=case when (select Top(1) Convert(varchar(50),Fecha,5) from DescuentoCheque DC
join DetalleDescuentoCheque DDC on DDC.IDTransaccionDescuentoCheque = DC.IDTransaccion
where IDTransaccionChequeCliente = VCC.IDTransaccion) <> ''
then
(select Top(1)Convert(varchar(50),Fecha,5) from DescuentoCheque DC
join DetalleDescuentoCheque DDC on DDC.IDTransaccionDescuentoCheque = DC.IDTransaccion
where IDTransaccionChequeCliente = VCC.IDTransaccion)
else ''
end,
'NumeroDescuento' = Case when (select Numero from DescuentoCheque DC
join DetalleDescuentoCheque DDC on DDC.IDTransaccionDescuentoCheque = DC.IDTransaccion
where DDC.IDTransaccionChequeCliente =VCC.IDTransaccion) <> ''
then
(select Numero from DescuentoCheque DC
join DetalleDescuentoCheque DDC on DDC.IDTransaccionDescuentoCheque = DC.IDTransaccion
where DDC.IDTransaccionChequeCliente =VCC.IDTransaccion)
else
''
end
from ChequeCliente VCC






﻿


CREATE View [dbo].[VAProductoListaPrecio]
As
Select 
'IDListaPrecio'=LP.ID,
'ListaPrecio'=LP.Lista,
'Estado'=Case When LP.Estado = 'True' Then 'OK' Else '-' End,
'IDProducto'=P.ID,
'Producto'=P.Descripcion,
'Referencia'=P.Referencia ,
'Precio'=PLP.Precio,
'IDMoneda'=PLP.IDMoneda,
'Moneda'=ISNULL(M.Referencia, '-'),
'Decimales'=ISNULL(M.Decimales, 'False'),

--TPR
'TPR'=IsNull(PLP.TPR, 0),
'TPRPorcentaje'=IsNull(PLP.TPRPorcentaje, 0),
--'TPRDesde'=IsNull(convert(varchar(50), PLP.TPRDesde), '---'),
--'TPRHasta'=IsNull(convert(varchar(50), PLP.TPRHasta), '---'),
PLP.TPRDesde,
PLP.TPRHasta,

--Sucursal
LP.IDSucursal,
'Sucursal'=S.Descripcion,
PLP.IDUsuario,
'Usuario'=(Select concat(usuario,' - ',Nombre) from usuario where id = PLP.IDUsuario),
PLP.Accion,
PLP.FechaModificacion

From vListaPrecio LP
Join AProductoListaPrecio PLP On lp.ID = PLP.IDListaPrecio
Join Producto P On PLP.IDProducto  = P.ID
Left Outer Join Moneda M on PLP.IDMoneda = M.ID
Join Sucursal S On LP.IDSucursal=S.ID








﻿-- View

CREATE View [dbo].[VDetalleDepositoBancarioDocumento]
As

Select

D.IDTransaccion,

D.ID,
'Sel'='True',
'CodigoComprobante' = D.CodigoComprobante, 
'Comprobante' = Cast(D.Comprobante as varchar),
'Banco'='',
'BancoLocal'='', 
'Importe'= DDB.Importe,
'Cliente'='',
'CuentaBancaria'='',
'Estado'='',
'Numero'=0,
'IDSucursal'=0,
'Ciudad'='',


'IDTransaccionDepositoBancario' = DDB .IDTransaccionDepositoBancario ,
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
D.Deposito,
'IDCuentaBancariaDeposito'=0,
DB.Fecha,
'NroOperacion'=DB.Numero,
TC.Restar,
D.Cotizacion

From DetalleDepositoBancario DDB
Join Transaccion T On DDB .IDTransaccionDepositoBancario =T.ID
Join DepositoBancario DB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion 
Join VFormaPagoDocumento D on DDB.IDTransaccionDocumento = D.IDTransaccion and DDB.IDDetalleDocumento = d.ID
Join TipoComprobante TC On D.IDTipoComprobante=TC.ID




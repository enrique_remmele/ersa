﻿CREATE View [dbo].[VVale]

As

Select

V.IDTransaccion,
V.Numero,
'Num'=V.Numero,
V.NroComprobante,
'Cod'='Vale',
V.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
V.IDGrupo,
'Grupo'=IsNull(G.Descripcion, '---'), 
V.Fecha,
'Fec'=CONVERT(varchar(50), V.Fecha, 6),
V.IDMoneda,
'Moneda'=M.Referencia,
V.Cotizacion,
V.Motivo,
V.Nombre ,

--Total
V.Total,
V.Anulado,
'Estado'=(Case When V.Anulado ='True'Then 'Anulado'Else '---'End),
V.ARendir,
V.Saldo,
'Importe'= V.Saldo, 
V.Cancelado,
V.Pagado,

--Pagos
'Pagar'=IsNull(V.Pagar, 'False'),
'ImporteAPagar'=(Case When IsNull(V.Pagar, 'False') = 'True' Then V.ImporteAPagar Else 0 End),
V.IDCuentaBancaria,
'CuentaBancaria'=CB.Descripcion,
V.ObservacionPago,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=V.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=V.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion)

From Vale V
Join Transaccion T On V.IDTransaccion=T.ID
Left Outer Join Sucursal S On V.IDSucursal=S.ID
Left Outer Join Moneda M On V.IDMoneda=M.ID
Left Outer Join Grupo G On V.IDGrupo=G.ID
Left Outer Join VCuentaBancaria CB On V.IDCuentaBancaria=CB.ID
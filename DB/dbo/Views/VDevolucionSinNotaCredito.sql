﻿

CREATE View [dbo].[VDevolucionSinNotaCredito]
As

Select 
M.IDTransaccion,
M.Numero,
'Num'=M.Numero,
'Suc'=(Select S.Codigo from Sucursal S where S.ID=M.IDSucursal),
M.IDSucursal,
M.Fecha,
'Fec'=CONVERT(varchar(50), M.Fecha, 6),
M.IDCliente,
'Cliente'=(Select RazonSocial from Cliente where ID = M.IDCliente),
'RUC'=(Select RUC from Cliente where ID = M.IDCliente),
'IDDeposito' = IsNull((Select D.ID From Deposito D Where D.ID=M.IDDeposito), 0) ,
'Deposito'=IsNull((Select D.[Suc-Dep] From VDeposito D Where D.ID=M.IDDeposito), '---') ,
M.Observacion,
M.Aprobado,
M.IDUsuarioAprobado,
M.ObservacionAutorizador,
'UsuarioAprobado'=(Select U.Usuario From Usuario U Where U.ID=M.IDUsuarioAprobado),
'NombreUsuarioAprobado'=(Select U.Nombre From Usuario U Where U.ID=M.IDUsuarioAprobado),
M.FechaAprobado,
M.Total,
M.Anulado,
'Estado'=(Case When M.Anulado='True' Then 'ANULADO' Else 
		(Case when M.Aprobado = 'True' then 'APROBADO' else 
		'PENDIENTE' end) End),

--Transaccion
'FechaTransaccion'=TR.Fecha,
TR.IDUsuario,
'NombreUsuario'=(Select U.Nombre From Usuario U Where U.ID=TR.IDUsuario),
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),
TR.IDTerminal,
--Anulacion
M.IDUsuarioAnulado,
'UsuarioAnulado'=(Select IsNull((Select U.Usuario From  Usuario U Where U.ID = M.IDUsuarioAnulado), '---')),
M.FechaAnulado,
--Otros
'CodigoSucursalOperacion'=IsNull((Select S.Codigo From Sucursal S Where S.ID=TR.IDSucursal), '---'),
M.IDTransaccionMovimientoStock,
M.NumeroSolicitudCliente

From DevolucionSinNotaCredito M
Join Transaccion TR On M.IDTransaccion=TR.ID




























﻿
CREATE view [dbo].[VUsuarioAutorizarPedidoNotaCredito] as 
select
ua.ID,
sm.Motivo,
ua.IDSubMotivo,
'SubMotivo' = sm.Descripcion,
ua.IDUsuario,
'Usuario'=u.nombre,
ua.MontoLimite,
ua.PorcentajeLimite,
ua.Estado
 from 
UsuarioAutorizarPedidoNotaCredito ua
left outer join SubMotivoNotaCredito sm on ua.IDSubMotivo = sm.ID
left outer join Usuario u on ua.idusuario = u.ID


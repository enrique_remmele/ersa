﻿
CREATE View [dbo].[VResumenCobranzaContado2]
As
Select
IDSucursal,
Fecha,
Lote,
'Efectivo'=Sum(Efectivo),
'Cantidad'=Sum(Cantidad),
'Efectivizacion'=SUM(Efectivizacion),
'OrdenPago'=SUM(OrdenPago),
'SaldoEfectivo'=Sum(Efectivo)-(SUM(OrdenPago)+(SUM(Efectivizacion)*-1)), 
'ChequesAlDia'=SUM(ChequesAlDia),
'CantidadChequesAlDia'=SUM(CantidadChequesAlDia),
'ChequesDiferidos'=SUM(ChequesDiferidos),
'CantidadChequesDiferidos'=SUM(CantidadChequesDiferidos),
'OtrosDocumentos'=SUM(OtrosDocumentos),
'CantidadOtrosDocumentos'=SUM(CantidadOtrosDocumentos),
--'Total'=Sum(Efectivo) + SUM(ChequesAlDia) + SUM(ChequesDiferidos) + SUM(OtrosDocumentos)
'Total'=Sum(Efectivo)-(SUM(OrdenPago)+(SUM(Efectivizacion)*-1)) + SUM(ChequesAlDia) + SUM(ChequesDiferidos) + SUM(OtrosDocumentos)

From VResumenCobranzaContado
Group By IDSucursal, Fecha, Lote

﻿
CREATE View [dbo].[VDetalleAsignacionCamion]
As

Select	
DM.IDTransaccion,
DM.ID,
DM.IDTransaccionAbastecimiento,
DM.IDTransaccionPedido,
DM.IDOperacion,
DM.IDProducto,
DM.Cantidad,
'Tipo'='Abastecimiento '+ ODP.TipoComprobante + '-' +ODP.CodigoSucursalOperacion + Convert(varchar(18),ODP.Num),

M.Anulado, 
--Operacion
M.Numero,
M.Fecha,
M.Fec,

--Producto
'Producto'=Concat(P.Referencia,' - ',P.Descripcion),
'Descripcion'=ODP.Observacion,
'CodigoBarra'=P.CodigoBarra,
'Ref'=P.Referencia,
 M.[Cod.],
 'Solicita'=ODP.Deposito,
 'FechaEntrega'= (Select DOP.FechaEntrega from DetalleOrdenDePedido DOP where DOP.IDTransaccion = DM.IDTransaccionAbastecimiento and DOP.IDProducto=DM.IDProducto and DOP.ID=DM.IDOperacion),
 --M.Operacion,
 M.NroComprobante,
 M.IDSucursal,
M.IdTipoComprobante,
'Peso' = (DM.Cantidad * Peso),
'Cliente' = '',
'IDCliente' = 0,
'IDUsuarioPedido'= ODP.IDUsuario
 --select * from VOrdenDePedido
From DetalleAsignacionCamion DM
Join VAsignacionCamion M On DM.IDTransaccion=M.IDTransaccion
Join Producto P On DM.IDProducto=P.ID
left outer join VOrdenDePedido ODP on DM.IDTransaccionAbastecimiento = ODP.IDTransaccion
Where M.PedidoCliente = 'False'
And M.Anulado = 0

Union All

Select	
DM.IDTransaccion,
DM.ID,
DM.IDTransaccionAbastecimiento,
DM.IDTransaccionPedido,
DM.IDOperacion,
DM.IDProducto,
DM.Cantidad,
DM.Tipo,

M.Anulado, 
--Operacion
M.Numero,
M.Fecha,
M.Fec,

--Producto
'Producto'=Concat(P.Referencia,' - ',P.Descripcion),
'Descripcion'=Pe.Observacion,
'CodigoBarra'=P.CodigoBarra,
'Ref'=P.Referencia,
 M.[Cod.],
 'Solicita'=Pe.Cliente,
 'FechaEntrega'= Pe.FechaFacturar,
 --M.Operacion,
 M.NroComprobante,
 M.IDSucursal,
M.IdTipoComprobante,
'Peso' = (DM.Cantidad * Peso),
'Cliente' = Pe.Cliente,
'IDCliente' = Pe.IDCliente,
'IDUsuarioPedido'= Pe.IDUsuario
 
From DetalleAsignacionCamion DM
Join VAsignacionCamion M On DM.IDTransaccion=M.IDTransaccion
Join Producto P On DM.IDProducto=P.ID
Join VPedido Pe on DM.IDTransaccionPedido = Pe.IDTransaccion
Where M.PedidoCliente = 'True'
And M.Anulado = 0













﻿
CREATE View [dbo].[VCFVentaImpuesto]
As
Select
V.*,
CFV.IDImpuesto,
'Impuesto'=I.Descripcion,
'Referencia'=I.Referencia

From VCF V
Join CFVentaImpuesto CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join Impuesto I On CFV.IDImpuesto=I.ID


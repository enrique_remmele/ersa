﻿CREATE view [dbo].[VResumenCobranzaDiaria]
as

--Cobranza Crédito

--Efectivo
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.FechaEmision,6),
'Fecha'=CC.FechaEmision,
VDC.IDCliente,
VDC.Cliente,
VDC.Referencia,
VDC.[Cod.],

--Cantidad de Comprobantes
'CantComprobantes'=(Select COUNT(*) From VCobranzaCredito CC2
Join VVentaDetalleCobranzaCredito VDC on CC2.IDTransaccion=VDC.IDTransaccionCobranza
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),

VDC.Cobrador,
VDC.IDCobrador,

VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=convert(varchar(50), VDC.Numero),
'FormaPago'='Efectivo',
'Moneda'=M.Referencia,
'Cambio'=E.Cotizacion,
'Banco'='---',
'Comprobante'=convert(varchar(50), E.Comprobante),
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'=TC.Codigo,

--Cantidad de Efectivos
'Cant'=(Select Count(*)From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Join CobranzaCredito CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),

--Total Cobranza

'TotalCobrado'=(Select Sum(CC2.Total)From CobranzaCredito CC2 
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),

--Importe Efectivo

'Importe'= (Select SUM(FP.Importe) from VFormaPago FP
join Efectivo E On E.IDTransaccion=FP.IDTransaccion
join CobranzaCredito CC2 on CC2.IDTransaccion=FP.IDTransaccion
Where CC2.FechaEmision=CC.FechaEmision And CC2.Anulado='False'),

'CancelarCheque'=FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Lote'=convert(varchar(50), CC.Numero),
'Recibo'=convert(varchar(50), CC.Comprobante)

From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Join VCobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaCredito VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Where CC.Anulado = 'False'


Union All

--Cheque Cliente
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.FechaEmision,6),
'Fecha'=CC.FechaEmision,
VDC.IDCliente,
VDC.Cliente,
VDC.Referencia,
VDC.[Cod.],

--Cantidad Comprobantes
'CantComprobantes'=(Select COUNT(*) From VCobranzaCredito CC2 
Join VVentaDetalleCobranzaCredito VDC on CC2.IDTransaccion=VDC.IDTransaccionCobranza
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),

VDC.Cobrador,
VDC.IDCobrador,


VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=VDC.Numero,
'FormaPago'='Cheque',
'Moneda'=V.Moneda,
'Cambio'=V.Cotizacion,
'Banco'=V.Banco,
'Comprobante'=V.NroCheque,
'FechaPago'=V.Fecha,
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE' End),
'Tipo'=V.Tipo,

--Cantidad de Cheques
'Cant'=Case When V.Diferido= 'True'Then
(Select COUNT(*) From FormaPago FP
Join ChequeCliente  CH on CH.IDTransaccion=FP.IDTransaccionCheque 
Join CobranzaCredito CC2 on CC2.IDTransaccion=FP.IDTransaccion
Where CC2.FechaEmision =CC.FechaEmision  And CC2.Anulado ='False' And CH.Diferido='True')Else
 (Select COUNT(*) From FormaPago FP
Join ChequeCliente  CH on CH.IDTransaccion=FP.IDTransaccionCheque 
Join CobranzaCredito CC2 on CC2.IDTransaccion=FP.IDTransaccion
Where CC2.FechaEmision =CC.FechaEmision  And CC2.Anulado ='False' And CH.Diferido='False')End,

--Total Cobranza
'TotalCobrado'=(Select Sum(CC2.Total)From CobranzaCredito CC2 
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),

--Importe Cheque
'Importe'=Case When V.Diferido= 'True'Then
(Select SUM(FP.Importe) From FormaPago FP
Join ChequeCliente  CH on CH.IDTransaccion=FP.IDTransaccionCheque 
Join CobranzaCredito CC2 on CC2.IDTransaccion=FP.IDTransaccion
Where CC2.FechaEmision =CC.FechaEmision  And CC2.Anulado ='False' And CH.Diferido='True')Else
 (Select SUM(FP.Importe) From FormaPago FP
Join ChequeCliente  CH on CH.IDTransaccion=FP.IDTransaccionCheque 
Join CobranzaCredito CC2 on CC2.IDTransaccion=FP.IDTransaccion
Where CC2.FechaEmision =CC.FechaEmision  And CC2.Anulado ='False' And CH.Diferido='False')End,


FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Lote'=CC.Numero,
'Recibo'=CC.Comprobante

From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion 
Join VCobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaCredito VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion

Where CC.Anulado='False'



Union All



--Documento
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.FechaEmision,6),
'Fecha'=CC.FechaEmision,
VDC.IDCliente,
VDC.Cliente,
VDC.Referencia,
VDC.[Cod.],

--Cantidad de Comprobantes
'CantComprobantes'=(Select COUNT(*) From VCobranzaCredito CC2 
Join VVentaDetalleCobranzaCredito VDC on CC2.IDTransaccion=VDC.IDTransaccionCobranza
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),

VDC.Cobrador,
VDC.IDCobrador,
VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=VDC.Numero,
'FormaPago'='Documento',
'Moneda'=M.Referencia,
'Cambio'=FPD.Cotizacion,
'Banco'='---',
'Comprobante'=FPD.Comprobante,
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'=TC.Codigo,

--Cantidad de Documentos
'Cant'=(Select Count(*) From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join CobranzaCredito CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),

--Total Cobranza

'TotalCobrado'=(Select Sum(CC2.Total)From CobranzaCredito CC2 
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),


--Importe Documento
'Importe'=(Select Sum(FP.Importe) From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join CobranzaCredito CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.FechaEmision=CC2.FechaEmision And CC2.Anulado='False'),


FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Lote'=CC.Numero,
'Recibo'=CC.Comprobante

From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join VCobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaCredito VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion


Where CC.Anulado='False'











Union All
--Cobranza Contado

--Efectivo
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.Fecha,6),
CC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.ReferenciaCliente,
VDC.[Cod.],

--Cantidad de Comprobantes
'CantComprobantes'=(Select COUNT(*) From VCobranzaContado CC2
Join VVentaDetalleCobranzaContado VDC on CC2.IDTransaccion=VDC.IDTransaccionCobranza
Where CC.Fecha=CC2.Fecha),

VDC.Cobrador,
VDC.IDCobrador,


VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=VDC.Numero,
'FormaPago'='Efectivo',
'Moneda'=M.Referencia,
'Cambio'=E.Cotizacion,
'Banco'='---',
'Comprobante'=E.Comprobante,
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'=TC.Codigo,

--Cantidad de Efectivos
'Cant'=(Select Count(*)From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Join CobranzaContado CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.Fecha=CC2.Fecha),

--Total Cobranza

'TotalCobrado'=(Select Sum(CC2.Total)From CobranzaContado CC2 
Where CC.Fecha=CC2.Fecha),

--Importe Efectivo

'Importe'=(Select Sum(FP.Importe)From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Join CobranzaContado CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.Fecha=CC2.Fecha),

'CancelarCheque'=FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Lote'=CC.ComprobanteLote,
'Recibo'=CC.Comprobante

From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID
Join VCobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False'


Union All

--Cheque Cliente
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.Fecha,6),
CC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.ReferenciaCliente,
VDC.[Cod.],

--Cantidad Comprobantes
'CantComprobantes'=(Select COUNT(*) From VCobranzaContado CC2 
Join VVentaDetalleCobranzaContado VDC on CC2.IDTransaccion=VDC.IDTransaccionCobranza
Where CC.Fecha=CC2.Fecha),

VDC.Cobrador,
VDC.IDCobrador,


VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=VDC.Numero,
'FormaPago'='Cheque',
'Moneda'=V.Moneda,
'Cambio'=V.Cotizacion,
'Banco'=V.Banco,
'Comprobante'=V.NroCheque,
'FechaPago'=V.Fecha,
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE' End),
'Tipo'=V.Tipo,

--Cantidad de Cheques
'Cant'=(Select Count(*) From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion 
Join CobranzaContado CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.Fecha=CC2.Fecha),

--Total Cobranza
'TotalCobrado'=(Select Sum(CC2.Total)From CobranzaContado CC2 
Where CC.Fecha=CC2.Fecha),

--Importe Cheque
'Importe'=(Select Sum(FP.Importe) From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion 
Join CobranzaContado CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.Fecha=CC2.Fecha),


FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Lote'=CC.ComprobanteLote,
'Recibo'=CC.Comprobante

From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion 
Join VCobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False'

Union All

--Documento
Select
FP.IDTransaccion,
VDC.IDTransaccionCobranza,
'Fec'=convert(varchar(50),CC.Fecha,6),
CC.Fecha,
VDC.IDCliente,
VDC.Cliente,
VDC.ReferenciaCliente,
VDC.[Cod.],

--Cantidad de Comprobantes
'CantComprobantes'=(Select COUNT(*) From VCobranzaContado CC2 
Join VVentaDetalleCobranzaContado VDC on CC2.IDTransaccion=VDC.IDTransaccionCobranza
Where CC.Fecha=CC2.Fecha),

VDC.Cobrador,
VDC.IDCobrador,
VDC.IDVendedor,

VDC.IDSucursal,
VDC.IDDeposito,
VDC.IDTipoComprobante,
'ComprobanteFactura'=VDC.Comprobante,
'NumeroCobranza'=VDC.Numero,
'FormaPago'='Documento',
'Moneda'=M.Referencia,
'Cambio'=FPD.Cotizacion,
'Banco'='---',
'Comprobante'=FPD.Comprobante,
'FechaPago'='',
'TipoComprobante'=TC.Descripcion,
'Tipo'='DOC',

--Cantidad de Documentos
'Cant'=(Select Count(*) From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join CobranzaContado CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.Fecha=CC2.Fecha),

--Total Cobranza

'TotalCobrado'=(Select Sum(CC2.Total)From CobranzaContado CC2 
Where CC.Fecha=CC2.Fecha),


--Importe Documento
'Importe'=(Select Sum(FP.Importe) From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join CobranzaContado CC2 On CC2.IDTransaccion=FP.IDTransaccion
Where CC.Fecha=CC2.Fecha),


FP.CancelarCheque,
CC.Anulado,
VDC.Credito,
'Lote'=CC.ComprobanteLote,
'Recibo'=CC.Comprobante

From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Join VCobranzaContado CC On CC.IDTransaccion=FP.IDTransaccion
Join VVentaDetalleCobranzaContado VDC On VDC.IDTransaccionCobranza=FP.IDTransaccion
Join VVentaLoteDistribucion VLD On VLD.IDTransaccionVenta=VDC.IDTransaccion

Where CC.Anulado='False'

﻿



CREATE View [dbo].[VTipoCuentaFija]
As
Select 
T.ID,
T.Descripcion,
T.Impuesto,
'TipoImpuesto'=ISNULL((Select I.Descripcion From Impuesto I Where I.ID=T.IDImpuesto), '---'),
T.IDImpuesto,
T.Producto,
T.Total,
T.Descuento,
T.IncluirImpuesto,
T.IncluirDescuento,
'Tipo'=(Case When(T.Impuesto)='True' Then 'IMPUESTO' Else (Case When(T.Producto)='True' Then 'PRODUCTO' Else (Case When(T.Total)='True' Then 'TOTAL' Else (Case When(T.Descuento)='True' Then 'DESCUENTO' Else '---' End) End) End) End),
T.Campo
From TipoCuentaFija T




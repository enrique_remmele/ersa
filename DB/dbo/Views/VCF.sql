﻿


CREATE View [dbo].[VCF]
As
Select 
CF.IDOperacion,
'Operacion'=O.Descripcion,
CF.ID,
CF.IDSucursal,
'Sucursal'=S.Descripcion,
'SucursalCodigo'=S.Codigo,
CF.IDMoneda,
'Moneda'=M.Descripcion,
'ReferenciaMoneda'=M.Referencia,
CF.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,
'CodigoComprobante'=TC.Codigo,

--Cuenta
CF.CuentaContable,
'Denominacion'=CC.Descripcion,
'IDCuentaContable'=CC.ID,
'Codigo'=CC.Codigo,
'Cuenta'=CC.Descripcion,
'Alias'=CC.Alias,

CF.Debe,
CF.Haber,
'Debe/Haber'=(Case When (CF.Debe) = 'True' Then 'DEBE' Else (Case When (CF.Haber) = 'True' Then 'HABER' Else '---' End) End),
CF.Descripcion,
CF.Orden,

'CajaChica'=isnull(CC.CajaChica,'False') 

From CF 
Join Operacion O On CF.IDOperacion=O.ID
Join Sucursal S On CF.IDSucursal=S.ID
Join Moneda M On CF.IDMoneda=M.ID
Left Outer Join TipoComprobante TC On CF.IDTipoComprobante=TC.Id
Join CuentaContable CC On CF.CuentaContable=CC.Codigo 
Join PlanCuenta PC On CC.IDPlanCuenta=PC.ID
Left Outer Join UnidadNegocio U On CC.IDUnidadNegocio = U.ID
Left Outer Join CentroCosto C On CC.IDCentroCosto = C.ID

Where PC.Titular='True'











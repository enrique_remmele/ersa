﻿
CREATE View [dbo].[VFormaPagoDocumento]
As
Select 

FPD.IDTransaccion,
FPD.ID,
FPD.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,
'CodigoComprobante'=TC.Codigo,
'ComprobanteLote'=0,
TC.IDOperacion,
FPD.IDSucursal,
FPD.Comprobante,
FPD.Fecha,
'Fec'=CONVERT(varchar(50), FPD.Fecha, 6),
FPD.IDMoneda,
'Moneda'=M.Referencia,
FPD.ImporteMoneda,
FPD.Cotizacion,
FPD.Total,
'Saldo'=ISNULL(FPD.Saldo,0),
'Importe'=FPD.Saldo,
FPD.Observacion,
'Cancelado'=ISNULL(FPD.Cancelado,0),
'Deposito'=ISNULL(TC.Deposito,'False'),
TC.Restar,
T.IDUsuario,
FPD.IdBanco,
'Banco'= Isnull((select referencia from banco where id = FPD.IDBanco), '-'),
'Cobranza' = Isnull((select concat(Suc,' ',Numero,' - ', Cliente) from VCobranzaCredito CC where CC.IDTransaccion = FPD.IDTransaccion),'---')

From FormaPagoDocumento FPD
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
join Transaccion T on T.Id = FPD.IDTransaccion







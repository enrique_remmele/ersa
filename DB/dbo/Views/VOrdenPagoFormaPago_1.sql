﻿
CREATE view [dbo].[VOrdenPagoFormaPago]
as

--Efectivo
Select 
'IDTransaccion'=IDTransaccionOrdenPago,
Comprobante,
CodigoComprobante,
Fec,
Importe
From VOrdenPagoEfectivo 
union all

--Cheque

Select
C.IDTransaccion,
'Comprobante'=NroCheque,
'CodigoComprobante'='CHQ',
Fec,
Importe
From VCheque C
Join OrdenPago OP on OP.IDTransaccion=C.IDTransaccion
union all

--Documento
Select
FPD.IDTransaccion,
FPD.Comprobante,
'CodigoComprobante' = T.Codigo,
FPD.Fecha,
FPD.ImporteMoneda
From FormaPagoDocumento FPD
join TipoComprobante T on FPD.IDTipoComprobante = T.ID




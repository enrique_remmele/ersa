﻿Create View [dbo].[VLineaSubLinea2]
As
Select
SLSL.*,
'SubLinea'=SL.Descripcion,
'SubLinea2'=SL2.Descripcion
From
SubLineaSubLinea2 SLSL
Join SubLinea SL On SLSL.IDSubLinea=SL.ID
Join SubLinea2 SL2 On SLSL.IDSubLinea2=SL2.ID

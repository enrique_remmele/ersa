﻿
--Select Top(1000) * From VKardex 

CREATE View [dbo].[VKardex] 

As 

Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'=IsNull(O.Descripcion, 'SIN OPERACION'),
K.IDTransaccion,
K.IDProducto,
K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Cantidades
--Entrada
K.CantidadEntrada,
K.CostoUnitarioEntrada,
K.TotalEntrada,

--Salida
K.CantidadSalida,
K.CostoUnitarioSalida,
K.TotalSalida,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal,

--Producto
P.Referencia,
'Producto'=P.Descripcion,
P.TipoProducto,
P.IDTipoProducto,
P.Linea,
P.SubLinea,
P.IDSubLinea,
P.SubLinea2,
P.IDSubLinea2,
P.Clasificacion,
P.IDClasificacion

From Kardex K
Join VProducto P On K.IDProducto=P.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID



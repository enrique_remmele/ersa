﻿
CREATE View [dbo].[VTransferenciaNotaCredito]

As


Select
NCV.IDTransaccionNotaCredito, 
V.IDTransaccion , 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
NCV.Importe 

From NotaCreditoVenta  NCV
join VVenta  V on NCV.IDTransaccionVentaGenerada  = V .IDTransaccion 

Union All


Select
NCVA.IDTransaccionNotaCredito, 
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
NCVA.Importe 

From NotaCreditoVentaAplicada   NCVA
join VVenta  V on NCVA.IDTransaccionVenta  = V .IDTransaccion 






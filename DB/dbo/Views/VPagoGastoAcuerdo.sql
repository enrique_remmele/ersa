﻿


CREATE View [dbo].[VPagoGastoAcuerdo]
As
Select
--Gasto
'IDTransaccionGasto'=G.IDTransaccion,
'NroComprobanteGasto'=G.NroComprobante,
'TipoComprobanteGasto'=G.TipoComprobante,
'FechaGasto'=G.Fecha,
'FechaVencimientoGasto'=G.FechaVencimiento,
'TotalGasto'=G.Total,
'TotalImpuestoGasto'=G.TotalImpuesto,
'TotalDiscriminadoGasto'=G.TotalDiscriminado,
'TotalGastoGs'=G.Total * G.Cotizacion,
'IDMonedaGasto'=G.IDMoneda,
'MonedaGasto'=G.Moneda,
'CreditoGasto'=G.Credito,
'CotizacionGasto' = (Case when isnull(G.IDMoneda,0) = 1 then 1 else (case when isnull(G.Cotizacion,0)=0 then 1 else G.Cotizacion end) end),
'ObservacionGasto'=G.Observacion,
'CantidadFacturada'=GP.Cantidad,

--Orden de Pago
OPE.IDTransaccionOrdenPago,
'NumeroOP'=OP.Numero,
'FechaOP'=OP.Fecha,
'IDSucursalOP'=OP.IDSucursal,
'SucOP'=OP.Suc,
'ImporteOP'=OPE.Importe,
'ImporteOPGs'=OPE.Importe*OP.Cotizacion,
'NroOP'=OP.NroComprobante,
'TipoComprobanteOP'= OP.TipoComprobante,
'CotizacionOP'=OP.Cotizacion,
'IDMonedaOP' = OP.IDMoneda,
'ObservacionOP'=OP.Observacion,
'CotizacionVencimiento'= VCE.Cotizacion,
'ImporteVtoOPGs'=isnull(VCE.Cotizacion,OP.Cotizacion)*OPE.Importe,
'CotizacionVtoOp'=isnull(VCE.Cotizacion,OP.Cotizacion),

--Acuerdo
A.IDProducto,
A.IDProveedor,
A.NroAcuerdo,
A.Zafra,
'Acuerdo'=A.NroAcuerdo,
'CotizacionFechaAcuerdo'=(dbo.FCotizacionAlDiafecha(2,0,A.Fecha)),
'ObservacionAcuerdo'=A.Observacion,
'FechaAcuerdo'=A.Fecha,
'CantidadAcuerdo'=A.Cantidad,
'PrecioAcuerdo'=A.Precio,
'PrecioDiscriminadoAcuerdo'=A.PrecioDiscriminado,
'TipoFleteAcuerdo'=A.TipoFlete,
'AcuerdoFinalizado'=A.Finalizado,
'ProveedorAcuerdo'=A.Proveedor,
A.FechaDesdeEmbarque,
A.FechaHastaEmbarque,
A.CostoFlete,
A.IDMonedaFlete,
'MonedaFlete' = A.ReferenciaMonedaFlete,
'MonedaAcuerdo'=A.ReferenciaMoneda,
'IDMonedaAcuerdo'=A.IDMoneda,
'ProductoAcuerdo'=A.Producto,
'ReferenciaProductoAcuerdo'=A.ReferenciaProducto,
A.CantidadAsociada,
A.CantidadAsociadaValorizadaGs,
'CotizacionPromedioTicketsAsociados'=(Select AVG(Cotizacion) from TicketBascula where NroAcuerdo = A.NroAcuerdo),
'FechaPrimerTicket' = (Select Min(Fecha) from TicketBascula where NroAcuerdo = A.NroAcuerdo),
'FechaUltimoTicket' = (Select Max(Fecha) from TicketBascula where NroAcuerdo = A.NroAcuerdo)

From OrdenPagoEgreso OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion and OP.Anulado = 0
Join VGasto G On OPE.IDTransaccionEgreso=G.IDTransaccion
Join VProveedor P on P.ID=G.IDProveedor
Join GastoProducto GP on GP.IDTransaccionGasto = G.IDTransaccion
Join vAcuerdo A on A.NroAcuerdo = GP.NroAcuerdo 
left outer join vVencimientoChequeEmitido VCE on OP.IDTransaccion = VCE.IDTransaccionOP and VCE.Anulado = 0






﻿CREATE View [dbo].[VChequesDiferidosDepositados]
As
--Para ERSA
Select 'Cheque Nro'=C.NroCheque,
C.Banco,
C.CuentaBancaria,
'Emitido Por'=ISNULL(C.[Emitido Por],'---'), 
'Cliente'=ISNULL(C.Cliente,'---') ,
C.Importe ,
--C.Fecha,
D.Fecha,
C.IDTransaccion, 
'FechaVencimiento'=convert(varchar(50),C.FechaVencimiento,5),
C.FechaCobranza,
'Cond.'='' ,
'Fact.Nro'=0,
'Fecha Emis.Fact'=0,
'Plazo Diaz'=0,
D.Sucursal,
'Deposito/Descuento'= CONVERT (varchar(50), D.TipoComprobante)  +'.'+  CONVERT (varchar(50), D.NroComprobante ) + ' - ' + CONVERT (varchar(50), D.Fecha ,3),
'FechaDeposito'=D.Fecha, 
C.IDBanco,
C.IDCliente,
C.Diferido,
'DescuentoCheque'='False',
D.IDSucursal,
C.Estado  

From VDepositoBancario  D
Join DetalleDepositoBancario DDB on D.IDTransaccion =DDB.IDTransaccionDepositoBancario 
Join VChequeCliente C On C.IDTransaccion=DDB.IDTransaccionCheque
Where C.Diferido = 'True'

----Con Cobranza Desde aca para abajo es de Express
--Select 

--CCV.[Cheque Nro],
--CCV.Banco,
--CCV.[Cuenta Bancaria], 
--'Emitido Por'=ISNULL(CCV.[Emitido Por],'---'), 
--'Cliente'=ISNULL(CCV.Cliente,'---') ,
--CCV.Importe ,
----C.Fecha,
--D.Fecha,
--CCV.IDTransaccion, 
--'FechaVencimiento'=convert(varchar(50),CCV.[Vto.Cheque],5),
--C.FechaCobranza,
--CCV.[Cond.] ,
--CCV.[Fact.Nro],
--CCV.[Fecha Emis.Fact],
--CCV.[Plazo Diaz],
--CCV.Sucursal,
--'Deposito/Descuento'= CONVERT (varchar(50), D.TipoComprobante)  +'.'+  CONVERT (varchar(50), D.NroComprobante ) + ' - ' + CONVERT (varchar(50), D.Fecha ,3),
--'FechaDeposito'=D.Fecha, 
--CCV.IDBanco,
--CCV.IDCliente,
--CCV.Diferido,
--'DescuentoCheque'='False',
--CCV.IDSucursal,
--C.Estado  

--From VDepositoBancario  D
--Join DetalleDepositoBancario DDB on D.IDTransaccion =DDB.IDTransaccionDepositoBancario  
--Join VChequeClienteVenta CCV on DDB.IDTransaccionCheque =CCV.IDTransaccion  
--Join VChequeCliente C On C.IDTransaccion=CCV.IDTransaccion 
--Where C.Diferido = 'True'

--Union All

-----sin cobranza
--Select 

--C.NroCheque ,
--C.Banco,
--C.CuentaBancaria, 
--'Emitido Por'=ISNULL(C.[Emitido Por],'---'), 
--'Cliente'=ISNULL(C.Cliente,'---') ,
--C.Importe ,
----C.Fecha, 
--D.Fecha,
--C.IDTransaccion, 
--'FechaVencimiento'=convert(varchar(50),C.FechaVencimiento,5),
--C.FechaCobranza,
--'Cond.'= '---',
--'Fact.Nro'='0',
--'Fecha Emis.Fact'='---',
--'Plazo Dias'= 0  ,
--'Sucursal'=D.Suc ,
--'Deposito/Descuento'= CONVERT (varchar(50), D.TipoComprobante)  +'.'+  CONVERT (varchar(50), D.NroComprobante ) + ' - ' + CONVERT (varchar(50), D.Fecha ,3),
--'FechaDeposito'=D.Fecha, 
--C.IDBanco,
--C.IDCliente,
--C.Diferido,
--'DescuentoCheque'='False',
--C.IDSucursal,
--C.Estado

--From VDepositoBancario D
--Join DetalleDepositoBancario DDB on D.IDTransaccion =DDB.IDTransaccionDepositoBancario  
--Join VChequeCliente C on DDB.IDTransaccionCheque=C.IDTransaccion
--Where C.Diferido = 'True'
--And (Select Top 1 CC.IDTransaccion From VChequeClienteVenta CC Where CC.IDTransaccion = C.IDTransaccion) Is Null 


--Union all 

----Descuento de Cheque

--Select 

--C.NroCheque ,
--C.Banco,
--C.CuentaBancaria, 
--'Emitido Por'=ISNULL(C.[Emitido Por],'---'), 
--'Cliente'=ISNULL(C.Cliente,'---') ,
--C.Importe ,
--C.Fecha, 
--C.IDTransaccion, 
--'FechaVencimiento'=convert(varchar(50),C.FechaVencimiento,5),
--C.FechaCobranza,
--'Cond.'= '---',
--'Fact.Nro'='0',
--'Fecha Emis.Fact'='---',
--'Plazo Dias'= 0  ,
--'Sucursal'=D.Suc ,
--'Deposito/Descuento'= CONVERT (varchar(50), D.TipoComprobante)  +'.'+  CONVERT (varchar(50), D.NroComprobante ) + ' - ' + CONVERT (varchar(50), D.Fecha ,3),
--'FechaDeposito'=D.Fecha,
--C.IDBanco,
--C.IDCliente,
--C.Diferido,
--'DescuentoCheque'='True',
--C.IDSucursal,
--C.Estado  
 
--From VDescuentoCheque   D
--Join DetalleDescuentoCheque  DDC on D.IDTransaccion =DDC.IDTransaccionDescuentoCheque 
--Join VChequeCliente C on DDC.IDTransaccionChequeCliente=C.IDTransaccion         
--Where C.Diferido = 'True'

--Union All

----Con Cobranza
--Select 

--C.NroCheque ,
--C.Banco,
--C.CuentaBancaria, 
--'Emitido Por'=ISNULL(C.[Emitido Por],'---'), 
--'Cliente'=ISNULL(C.Cliente,'---') ,
--C.Importe ,
--C.Fecha, 
--C.IDTransaccion, 
--'FechaVencimiento'=convert(varchar(50),C.FechaVencimiento,5),
--C.FechaCobranza,
--'Cond.'= '---',
--'Fact.Nro'='0',
--'Fecha Emis.Fact'='---',
--'Plazo Dias'= 0  ,
--'Sucursal'='' ,
--'Deposito/Descuento'= '--',
--'FechaDeposito'='',
--C.IDBanco,
--C.IDCliente,
--C.Diferido,
--'DescuentoCheque'='True',
--C.IDSucursal,
--C.Estado  
 
--From VChequeCliente C  
--Join VFormaPagoCheque FPC On FPC.IDTransaccion=C.IDTransaccion     
--Where C.Diferido = 'True'

--Union All

----Cheques Cliente
--Select 

--C.NroCheque ,
--Banco='',
--C.CuentaBancaria, 
--'Emitido Por'=ISNULL(C.[Emitido Por],'---'), 
--'Cliente'=ISNULL(C.Cliente,'---') ,
--C.Importe ,
--C.Fecha, 
--C.IDTransaccion, 
--'FechaVencimiento'=convert(varchar(50),C.FechaVencimiento,5),
--C.FechaCobranza,
--'Cond.'= '---',
--'Fact.Nro'='0',
--'Fecha Emis.Fact'=NULL,
--'Plazo Dias'= 0  ,
--'Sucursal'='' ,
--'Deposito/Descuento'= '--',
--'FechaDeposito'=NULL,
--C.IDBanco,
--C.IDCliente,
--C.Diferido,
--'DescuentoCheque'='True',
--C.IDSucursal,
--'Estado'=''  
 
--From VChequeCliente C 
--Where C.Diferido = 'True'

--Union All


--Select 

--C.NroCheque ,
--Banco='',
--'CuentaBancaria'='---', 
--'Emitido Por'='---', 
--'Cliente'=ISNULL(C.ALaOrden,'---') ,
--C.Importe ,
--C.Fecha, 
--C.IDTransaccion, 
--'FechaVencimiento'=convert(varchar(50),C.FechaVencimiento,5),
--'FechaCobranza'=NULL,
--'Cond.'= '---',
--'Fact.Nro'='0',
--'Fecha Emis.Fact'=NULL,
--'Plazo Dias'= 0  ,
--'Sucursal'='' ,
--'Deposito/Descuento'= '--',
--'FechaDeposito'=NULL,
--'IDBanco'='',
--'IDCliente'='',
--C.Diferido,
--'DescuentoCheque'='True',
--C.IDSucursal,
--'Estado'=''  
 
--From Cheque C 
--Where C.Diferido = 'True'






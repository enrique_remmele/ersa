﻿CREATE View [dbo].[vAjusteExistenciaSaldoKardex]

As

Select 
M.IDTransaccion,
M.Numero,
'Num'=M.Numero,
M.Fecha,
'Fec'=CONVERT(varchar(50), M.Fecha, 6),
M.Observacion,
M.Anulado,
'Estado'=Case When M.Anulado='True' Then 'Anulado' Else 'OK' End,
--Transaccion
'FechaTransaccion'=TR.Fecha,
TR.IDDeposito,
TR.IDSucursal,
TR.IDTerminal,
TR.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

--Anulacion
'IDUsuarioAnulado'=IsNull(M.IDUsuarioAnulado, 0),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From Usuario U Where U.ID = M.IDUsuarioAnulado), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From Usuario U Where U.ID = M.IDUsuarioAnulado), '---')),
'FechaAnulacion'=M.FechaAnulado

From AjusteExistenciaSaldoKardex M
Join Transaccion TR On M.IDTransaccion=TR.ID


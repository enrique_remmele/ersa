﻿

CREATE View [dbo].[VResumenCobranzaDiaria2]

As

Select
C.IDTransaccion, 
'Fecha'=Convert(Date, C.FechaEmision),
'IDTipoComprobante'=V.IDTipoComprobante,
C.IDSucursal,
'Cobrado'=Sum(VD.Importe),
'Cantidad'=Count(V.Comprobante),
'Planilla'=C.NroPlanilla,
C.IDCobrador

From VCobranzaCredito C
Join VVentaDetalleCobranza VD On C.IDTransaccion=VD.IDTransaccionCobranza
Join VVenta V On VD.IDTransaccion=V.IDTransaccion
Where C.Anulado='False'
Group By C.FechaEmision, V.TipoComprobante,V.IDTipoComprobante,C.IDSucursal,C.NroPlanilla,C.IDCobrador,C.IDTransaccion










﻿CREATE View [dbo].[VEntregaChequeOP]
As

Select 

E.IDTransaccion,
'ChequeEntregado'=IsNull(E.ChequeEntregado, 'False'),
E.FechaEntrega,
E.Recibo,
E.RetiradoPor,
'Anulado'=IsNull(E.Anulado, 'False'),

--OP
E.IDTransaccionOP, 
'NumeroOP' = VO.Numero,
VO.IDSucursal

From EntregaChequeOP E
Join VOrdenPago VO On E.IDTransaccionOP = VO.IDTransaccion
﻿CREATE View [dbo].[VDetalleRemision]
As
Select

DR.IDTransaccion,
R.Comprobante,
--Producto
DR.IDProducto,
'Producto'=P.Descripcion,
DR.ID,
DR.Observacion, 
'Descripcion'=(Case When DR.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DR.Observacion End),
'CodigoBarra'=P.CodigoBarra,
P.Referencia ,
'Uni Med'=U.Referencia,


--Deposito
DR.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DR.Cantidad,

R.IDSucursal,
R.Anulado,
R.IDChofer,
R.IDDistribuidor,
R.IDCamion,
R.FechaFin,
R.FechaInicio,

R.IDCliente

 

From DetalleRemision DR
Join vRemision R On DR.IDTransaccion=R.IDTransaccion
Join Producto P On DR.IDProducto=P.ID
Left Outer Join UnidadMedida U On P.IDUnidadMedida=U.ID
Join Deposito D On DR.IDDeposito=D.ID




﻿CREATE View [dbo].[VAsientoCG]
As
Select 
A.IDTransaccionOrigen,
A.Numero,
A.NumeroAsiento,
A.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
S.IDCiudad,
'Ciudad'=C.Codigo,
A.Fecha,
A.IDMoneda,
'Moneda'=M.Referencia,
A.Cotizacion,
A.IDTipoComprobante,
'TipoComprobante'=IsNull(TP.Codigo, '---'),
'NroComprobante'=A.NroComprobante,
'Comprobante'=IsNull(TP.Codigo, '---') + ' ' + A.NroComprobante,
A.Detalle,
A.Debito,
A.Credito,
A.Saldo,
A.Total,
A.Anulado,
A.IDCentroCosto,
'CentroCosto'=(Select ISNULL((Select CC.Descripcion From CentroCosto CC Where CC.ID=A.IDCentroCosto),'---')),
'Conciliado'=IsNull(A.Conciliado, 'False'),
'Estado'=(Select Case When(IsNull(A.Conciliado, 'False')) = 'True' Then 'CONCILIADO' Else '---' End),
A.FechaConciliado,
A.IDUsuarioConciliado,
'Balanceado'=(Case When (A.Credito) != A.Debito Then 'False' Else 'True' End),
A.Bloquear,
'CajaChica'='False',
A.TipoOperacion,
A.Tipo

From AsientoCG A
Join Sucursal S On A.IDSucursal=S.ID
Join Ciudad C On S.IDCiudad=C.ID
Join Moneda M On A.IDMoneda=M.ID
Left Outer Join TipoComprobante TP On A.IDTipoComprobante=TP.ID








﻿




CREATE View [dbo].[VLoteDistribucion]
As

Select 

--Cabecera
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
LD.Numero,
'IDSucursal' = T.IDSucursal,--LD.IDSucursal, Se modifico porque tardaba demasiado al filtrar por sucursal de la tabla LD

'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
LD.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
'Comprobante'=LD.Comprobante,
LD.Fecha,
'Fec'=CONVERT(varchar(50), LD.Fecha, 6),
LD.FechaReparto,
'Fec Reparto'=CONVERT(varchar(50), LD.FechaReparto, 6),
LD.Observacion,

LD.IDDistribuidor,
'Distribuidor'=D.Nombres,
LD.IDCamion,
'Camion'=C.Descripcion,
C.Patente ,
LD.IDChofer,
'Chofer'=CH.Nombres,
'CI'=CH.NroDocumento ,
CH.Direccion ,
LD.IDZonaVenta,
'Zona'=ISNULL(ZV.Descripcion, '---'),

TR.ID,
'Transporte'=TR.Descripcion,

--Totales
--'TotalCancelacionAutomatica'=isnull((Select Sum(v.Total) from vVenta v where isnull(v.CancelarAutomatico,0)=1 and v.IDTransaccion in (select q.idtransaccionventa from VentaLoteDistribucion q where q.idtransaccionlote = LD.IDTransaccion)),0),
'TotalCancelacionAutomatica' = Isnull((Select Sum(v.total) 
										from vVenta v 
										join VentaLoteDistribucion q  on  v.IDTransaccion =  q.idtransaccionventa
										where isnull(v.CancelarAutomatico,0)=1 
										and q.idtransaccionlote = LD.IDTransaccion),0),

LD.TotalContado,
LD.TotalCredito,
LD.Total,
LD.TotalImpuesto,
LD.TotalDiscriminado,
LD.TotalDescuento,
'CantidadComprobantes'=(Select Count(*) From VentaLoteDistribucion VLD Where VLD.IDTransaccionLote=LD.IDTransaccion),

--Transaccion
LD.IDTransaccion,
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
LD.Anulado,
'Estado'=Case When LD.Anulado='True' Then 'Anulado' Else '---' End,
'EstadoLote'=Case When LD.Anulado='True' Then 'Anulado' Else 'Vigente' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=LD.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=LD.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=LD.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=LD.IDTransaccion),
'RENDIDO'=(Case when (Select count(*) From RendicionLote   Where Anulado='False' and IDTRansaccionLote = LD.IDTransaccion)>0 then 'RENDIDO' else
		  (Case when (Select count(*) From CobranzaContado Where Anulado='False' and IDTRansaccionLote = LD.IDTransaccion)>0 then 'RENDIDO' ELSE
		  'PENDIENTE' end)end),
'TOTAL KILO'=(Select sum(Peso) from vdetallelotedistribucion where IDTransaccion= LD.IDTransaccion)--,
--'Cantidad Cliente'=(select count(IDCliente) from vVentaLoteDistribucion where idtransaccionlote = LD.IDTransaccion)

From LoteDistribucion LD
Join Transaccion T On LD.IDTransaccion=T.ID
Left Outer Join Sucursal S On LD.IDSucursal=S.ID
Left Outer Join TipoComprobante TC On LD.IDTipoComprobante=TC.ID
Left Outer Join Distribuidor D On LD.IDDistribuidor=D.ID
Left Outer Join Camion C On LD.IDCamion=C.ID
Left Outer Join Chofer CH On LD.IDChofer=CH.ID
Left Outer Join ZonaVenta ZV On LD.IDZonaVenta=ZV.ID
Left Outer Join Transporte TR On TR.IDDistribuidor=LD.IDDistribuidor And TR.IDCamion=C.ID And TR.IDChofer=CH.ID And TR.IDZonaVenta=ZV.ID








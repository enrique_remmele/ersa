﻿CREATE view [dbo].[VExistenciaMovimientoCalculado]
AS


--Compra
Select
C.IDProducto,
C.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
C.IDSucursal,
C.Fecha,
'Movimiento'='COMPRA',
'Compras'=C.Cantidad,
'Entradas'=0,
'Salidas'=0,
'Ventas'=0,
E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
'Anulado'='False',
C.IDTransaccion

From VDetalleCompra C 
join Producto P on C.IDProducto = P.ID 
Join VExistenciaDeposito E on C.IDProducto=E.IDProducto and C.IDDeposito=E.IDDeposito

union all

--Ticket de Bascula
Select
C.IDProducto,
C.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
C.IDSucursal,
C.Fecha,
'Movimiento'='TICKET DE BASCULA',
'Compras'=C.PesoBascula,
'Entradas'=0,
'Salidas'=0,
'Ventas'=0,
E.CostoPromedio ,
'PesoUnitario'=1,
'ExistenciaAnterior'=0,
C.Anulado,
C.IDTransaccion

From VTicketBascula C 
Join VExistenciaDeposito E on C.IDProducto=E.IDProducto and C.IDDeposito=E.IDDeposito
Where Anulado = 'False' and Procesado = 'True'

union all

--Entradas

--NotaCreditoCliente
Select
NCC.IDProducto,
NCC.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
NCC.IDSucursal,
Convert(date, NCC.Fecha) Fecha,
'Movimiento'='NOTA DE CREDITO CLIENTE',
'Compras'=0,
'Entradas'=NCC.Cantidad,
'Salidas'=0,
'Ventas'=0,
E.CostoPromedio, 
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
NCC.Anulado,
NCC.IDTransaccion
From VDetalleNotaCredito NCC 
join Producto P on NCC.IDProducto = P.ID 
join VExistenciaDeposito E on NCC.IDProducto=E.IDProducto and NCC.IDDeposito=E.IDDeposito 
--WHERE NCC.Anulado = 0

union all
--Nota de credito cliente anulada
Select
NCC.IDProducto,
NCC.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
NCC.IDSucursal,
Convert(date, NC.FechaAnulado) Fecha,
'Movimiento'='NOTA DE CREDITO CLIENTE ANULADO',
'Compras'=0,
'Entradas'=0,
'Salidas'=NCC.Cantidad,
'Ventas'=0,
E.CostoPromedio, 
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
NCC.Anulado,
NCC.IDTransaccion
From VDetalleNotaCredito NCC 
join Producto P on NCC.IDProducto = P.ID 
Join NotaCredito NC on NC.IDTransaccion = NCC.IDTransaccion
join VExistenciaDeposito E on NCC.IDProducto=E.IDProducto and NCC.IDDeposito=E.IDDeposito 
WHERE NCC.Anulado = 1 and NC.Procesado=1

Union all

--Movimiento Entrada
Select
V.IDProducto,
V.Producto,
P.Referencia,
P.CodigoBarra,
P.TipoProducto,
P.IDTipoProducto,
P.IDMarca,
P.Marca,
P.IDLinea,
P.Linea,
P.Estado,
V.IDDepositoEntrada AS IDDeposito,
E.Deposito,
M.IDSucursal,
Convert(date, M.Fecha) Fecha,
'Movimiento'=M.Motivo,
'Compras'=0,
'Entradas'=V.Cantidad,
'Salidas'=0,
'Ventas'=0,
ISNULL(E.CostoPromedio,0) CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
V.Anulado,
V.IDTransaccion
From VDetalleMovimiento V
Join VMovimiento M On V.IDTransaccion=M.IDTransaccion
JOIN dbo.VProducto P ON P.ID = V.IDProducto
LEFT Join VExistenciaDeposito E on E.IDProducto=V.IDProducto and E.IDDeposito=V.IDDepositoEntrada
where V.Entrada='True'and V.Salida='False'-- AND M.Anulado = 0

union all
--Entrada anulada
--Movimiento Entrada
Select
V.IDProducto,
V.Producto,
P.Referencia,
P.CodigoBarra,
P.TipoProducto,
P.IDTipoProducto,
P.IDMarca,
P.Marca,
P.IDLinea,
P.Linea,
P.Estado,
V.IDDepositoEntrada AS IDDeposito,
E.Deposito,
M.IDSucursal,
Convert(date, M.FechaAnulacion) Fecha,
'Movimiento'=M.Motivo,
'Compras'=0,
'Entradas'=0,
'Salidas'=V.Cantidad,
'Ventas'=0,
ISNULL(E.CostoPromedio,0) CostoPromedio ,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
V.Anulado,
V.IDTransaccion

From VDetalleMovimiento V 
Join VMovimiento M On V.IDTransaccion=M.IDTransaccion
JOIN dbo.VProducto P ON P.ID = V.IDProducto
LEFT Join VExistenciaDeposito E on E.IDProducto=V.IDProducto and E.IDDeposito=V.IDDepositoEntrada
where V.Entrada='True'and V.Salida='False' AND M.Anulado = 1

union all

--Transaccion Entrada por transferencia
Select
DM.IDProducto,
DM.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
DM.IDDepositoEntrada as IDDeposito,
E.Deposito,
M.IDSucursal,
Convert(date, M.Fecha) Fecha,
'Movimiento'=M.Motivo,
'Compras'=0,
'Entradas'=Cantidad,
'Salidas'=0,
'Ventas'=0,
E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
DM.Anulado,
DM.IDTransaccion
From VDetalleMovimiento DM 
join Producto P on DM.IDProducto = P.ID 
join VMovimiento M On DM.IDTransaccion=M.IDTransaccion
Join VExistenciaDeposito E on E.IDProducto=DM.IDProducto and E.IDDeposito=DM.IDDepositoEntrada
where DM.Entrada='True'and DM.Salida='True' --AND DM.Anulado = 0

union all

--Transaccion Entrada por transferencia anulada
Select
DM.IDProducto,
DM.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
DM.IDDepositoEntrada as IDDeposito,
E.Deposito,
M.IDSucursal,
Convert(date, M.FechaAnulacion) Fecha,
'Movimiento'=M.Motivo,
'Compras'=0,
'Entradas'=0,
'Salidas'=Cantidad,
'Ventas'=0,
E.CostoPromedio ,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
DM.Anulado,
DM.IDTransaccion
From VDetalleMovimiento DM 
join Producto P on DM.IDProducto = P.ID 
join VMovimiento M On DM.IDTransaccion=M.IDTransaccion
Join VExistenciaDeposito E on E.IDProducto=DM.IDProducto and E.IDDeposito=DM.IDDepositoEntrada
where DM.Entrada='True'and DM.Salida='True' AND DM.Anulado = 1

Union all
--Ajuste Inventario Positivo
Select
ED.IDProducto,
ED.Producto,
ED.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
ED.IDSucursal,
Convert(date, FechaAjuste) Fecha,
'Movimiento'='AJUSTE POSITIVO',
'Compras'=0,
'Entradas'=ED.DiferenciaSistema,
'Salidas'=0,
'Ventas'=0,
E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
'Anulado'='False',
ED.IDTransaccion

From VExistenciaDepositoInventario ED 
join Producto P on ED.IDProducto = P.ID 
Join VExistenciaDeposito E on ED.IDProducto=E.IDProducto and ED.IDDeposito=E.IDDeposito
where DiferenciaSistema > 0 AND E.Estado = 1

Union all
--Ajuste Inicial Positivo
Select
ED.IDProducto,
ED.Producto,
ED.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
ED.IDSucursal,
Convert(date, Fecha) Fecha,
'Movimiento'='AJUSTE INICIAL POSITIVO',
'Compras'=0,
'Entradas'=ED.Entrada,
'Salidas'=0,
'Ventas'=0,
E.CostoPromedio ,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
'Anulado'='False',
ED.IDTransaccion
From VDetalleAjusteInicial ED
join Producto P on ED.IDProducto = P.ID 
Join AjusteInicial A On ED.IDTransaccion=A.IDTransaccion 
Join VExistenciaDeposito E on ED.IDProducto=E.IDProducto and ED.IDDeposito=E.IDDeposito
where Entrada > 0 and A.Procesado = 1

union all


--Venta
Select
V.IDProducto,
V.Producto,
V.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
V.IDSucursal,
Convert(date, FechaEmision) Fecha,
'Movimiento'='VENTA',
'Compras'=0,
'Entradas'=0,
'Salidas'=0,
'Ventas'=V.Cantidad,

E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
V.Anulado,
V.IDTransaccion
From VDetalleVenta V
join Producto P on V.IDProducto = P.ID 
join VExistenciaDeposito E on V.IDProducto=E.IDProducto and V.IDDeposito=E.IDDeposito 
WHERE
--V.Anulado = 0
 V.ControlarExistencia = 1

union all

--Ventas Anuladas
Select
V.IDProducto,
V.Producto,
V.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
V.IDSucursal,
--Se realiza este case when porque cuando se facturan para el dia siguiente y luego se anula 
--se guarda de esta forma Fecha Emision:02/01/2019, Fecha Anulacion:01/01/2019 y genera inconsistencias
--Convert(date, V.FechaEmision) Fecha,
Case When DA.Fecha > V.FechaEmision then Convert(date, DA.Fecha) else Convert(date, V.FechaEmision) end Fecha,

'Movimiento'='VENTA ANULADA',
'Compras'=0,
'Entradas'=V.Cantidad,
'Salidas'=0,
'Ventas'=0,

E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
V.Anulado  ,
V.IDTransaccion
From VDetalleVenta V 
join Producto P on V.IDProducto = P.ID 
join VExistenciaDeposito E on V.IDProducto=E.IDProducto and V.IDDeposito=E.IDDeposito 
join DocumentoAnulado DA on V.IDTransaccion=DA.IDTransaccion
WHERE
V.Anulado = 1 
AND V.ControlarExistencia = 1

Union all

--Salida
--NotaCreditoProveedor
Select
NCP.IDProducto,
NCP.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
NCP.IDSucursal,
Convert(date, NCP.Fecha) Fecha,
'Movimiento'='NOTA DE CREDITO PROVEEDOR',
'Compras'=0,
'Entradas'=0,
'Salidas'=NCP.Cantidad,
'Ventas'=0,

E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
NCP.Anulado,
NCP.IDTransaccion
From VDetalleNotaCreditoProveedor NCP
join Producto P on NCP.IDProducto = P.ID 
LEFT Join VExistenciaDeposito E on NCP.IDProducto=E.IDProducto and NCP.IDDeposito=E.IDDeposito
Union all

--Movimiento Salida
Select
DM.IDProducto,
DM.Producto,
P.Referencia,
P.CodigoBarra,
P.TipoProducto,
P.IDTipoProducto,
P.IDMarca,
P.Marca,
P.IDLinea,
P.Linea,
P.Estado,
DM.IDDepositoSalida AS IDDeposito,
E.Deposito,
M.IDSucursal,
Convert(date, M.Fecha) Fecha,
'Movimiento'=M.Motivo,
'Compras'=0,
'Entradas'=0,
'Salidas'=DM.Cantidad,
'Ventas'=0,
ISNULL(E.CostoPromedio,0) CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
DM.Anulado,
DM.IDTransaccion
From VDetalleMovimiento DM 
join VMovimiento M on DM.IDTransaccion=M.IDTransaccion
JOIN dbo.VProducto P ON P.ID = DM.IDProducto
LEFT Join VExistenciaDeposito E on E.IDProducto=DM.IDProducto and E.IDDeposito=DM.IDDepositoSalida
where DM.Entrada='False'and DM.Salida='True' --AND M.Anulado = 0

union all

--Movimiento Salida anulada
Select
DM.IDProducto,
DM.Producto,
P.Referencia,
P.CodigoBarra,
P.TipoProducto,
P.IDTipoProducto,
P.IDMarca,
P.Marca,
P.IDLinea,
P.Linea,
P.Estado,
DM.IDDepositoSalida AS IDDeposito,
E.Deposito,
M.IDSucursal,
Convert(date, M.FechaAnulacion) Fecha,
'Movimiento'=M.Motivo,
'Compras'=0,
'Entradas'=DM.Cantidad,
'Salidas'=0,
'Ventas'=0,
ISNULL(E.CostoPromedio,0) CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
DM.Anulado,
DM.IDTransaccion
From VDetalleMovimiento DM 
join VMovimiento M on DM.IDTransaccion=M.IDTransaccion
JOIN dbo.VProducto P ON P.ID = DM.IDProducto
LEFT Join VExistenciaDeposito E on E.IDProducto=DM.IDProducto and E.IDDeposito=DM.IDDepositoSalida
where DM.Entrada='False'and DM.Salida='True' AND M.Anulado = 1
Union all

 --Transferencia Salida
Select
DM.IDProducto,
DM.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
DM.IDDepositoSalida as IDDeposito,
E.Deposito,
M.IDSucursal,
Convert(date, M.Fecha) Fecha,
'Movimiento'=M.Motivo,
'Compras'=0,
'Entradas'=0,
'Salidas'=DM.Cantidad,
'Ventas'=0,

E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
DM.Anulado ,
DM.IDTransaccion
From VDetalleMovimiento DM
join Producto P on DM.IDProducto = P.ID  
join VMovimiento M on DM.IDTransaccion=M.IDTransaccion
LEFT Join VExistenciaDeposito E on E.IDProducto=DM.IDProducto and E.IDDeposito=DM.IDDepositoSalida
where DM.Entrada='True'and DM.Salida='True' --AND M.Anulado = 0
 
 union all

 --Transferencia Salida anulada
Select
DM.IDProducto,
DM.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
DM.IDDepositoSalida as IDDeposito,
E.Deposito,
M.IDSucursal,
Convert(date, M.FechaAnulacion) Fecha,
'Movimiento'=M.Motivo,
'Compras'=0,
'Entradas'=DM.Cantidad,
'Salidas'=0,
'Ventas'=0,

E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
DM.Anulado,
DM.IDTransaccion
From VDetalleMovimiento DM 
join Producto P on DM.IDProducto = P.ID 
join VMovimiento M on DM.IDTransaccion=M.IDTransaccion
LEFT Join VExistenciaDeposito E on E.IDProducto=DM.IDProducto and E.IDDeposito=DM.IDDepositoSalida
where DM.Entrada='True'and DM.Salida='True' AND M.Anulado = 1
 union all
 
--Ajuste Inventario Negativo
Select
ED.IDProducto,
ED.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
ED.IDSucursal,
Convert(date, FechaAjuste) Fecha,
'Movimiento'='AJUSTE NEGATIVO',
'Compras'=0,
'Entradas'=0,
'Salidas'=ED.DiferenciaSistema,
'Ventas'=0,

E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
'Anulado'='False',
ED.IDTransaccion
From VExistenciaDepositoInventario ED 
join Producto P on ED.IDProducto = P.ID 
LEFT Join VExistenciaDeposito E on ED.IDProducto=E.IDProducto and ED.IDDeposito=E.IDDeposito
where DiferenciaSistema < 0

 union all
 
--Ajuste Inicial Negativo
Select
ED.IDProducto,
ED.Producto,
E.Referencia,
E.CodigoBarra,
E.TipoProducto,
E.IDTipoProducto,
E.IDMarca,
E.Marca,
E.IDLinea,
E.Linea,
E.Estado,
E.IDDeposito,
E.Deposito,
ED.IDSucursal,
Convert(date, Fecha) Fecha,
'Movimiento'='AJUSTE NEGATIVO',
'Compras'=0,
'Entradas'=0,
'Salidas'=ED.Salida,
'Ventas'=0,
E.CostoPromedio,
'PesoUnitario'=P.Peso,
'ExistenciaAnterior'=0,
'Anulado'='False',
ED.IDTransaccion
From VDetalleAjusteInicial ED
join Producto P on ED.IDProducto = P.ID  
Join AjusteInicial A On ED.IDTransaccion=A.IDTransaccion
LEFT Join VExistenciaDeposito E on ED.IDProducto=E.IDProducto and ED.IDDeposito=E.IDDeposito
where Salida > 0
and A.Procesado = 1






﻿CREATE View [dbo].[VClienteSucursalODBC]
As

Select
CS.IDCliente,
CS.ID,
CS.Sucursal,
CS.Direccion,
CS.Contacto,
CS.Telefono,
CS.Estado,

CS.IDVendedor,
'Vendedor'=(Select ISNULL((Select V.Nombres From Vendedor V Where V.ID=CS.IDVendedor), '')),

CS.IDPais,
'Pais'=(Select ISNULL((Select P.Descripcion From Pais P Where P.ID=CS.IDPais), '')),

CS.IDDepartamento,
'Departamento'=(Select ISNULL((Select D.Descripcion From Departamento D Where D.ID=CS.IDDepartamento), '')),

CS.IDCiudad,
'Ciudad'=(Select ISNULL((Select CIU.Descripcion From Ciudad CIU Where CIU.ID=CS.IDCiudad), '')),

CS.IDBarrio,
'Barrio'=(Select ISNULL((Select B.Descripcion From Barrio B Where B.ID=CS.IDBarrio), '')),

CS.IDZonaVenta,
'ZonaVenta'=(Select ISNULL((Select Z.Descripcion From ZonaVenta Z Where Z.ID=CS.IDZonaVenta), '')),

CS.IDSucursal,
'ClienteSucursal'=(Select ISNULL((Select S.Descripcion From Sucursal S Where S.ID=CS.IDSucursal), '')),

CS.IDListaPrecio,
'ListaPrecio'= (Select ISNULL ((Select LP.Descripcion  From ListaPrecio LP Where LP.ID = CS.IDListaPrecio), '')),

'Contado'=IsNull(CS.Contado, 'True'),
'Credito'=IsNull(CS.Credito, 'False'),


CS.IDCobrador,
'Cobrador'= (Select ISNULL (( Select C.Nombres  From Cobrador C Where C.ID=CS.Idcobrador), '')),

CS.IDPromotor,
'Promotor'= (Select ISNULL (( Select C.Nombres  From Promotor C Where C.ID=CS.IDPromotor), '')),

CS.IDEstado,
'Estados'= (Select ISNULL (( Select EC.Descripcion  From EstadoCliente EC Where EC.ID=CS.IDEstado), '')),

IsNull(CS.Latitud, 0) AS Latitud,
IsNull(CS.Longitud,0) AS Longitud,
'Condicion'=(Case When (IsNull(CS.Contado, 'True')) = 'True' Then 'CONTADO' Else 'CREDITO' End),
'CodigoSucursal'= Isnull((Select codigo from Sucursal where ID = CS.IDSucursal),'ASU'),
C.Referencia,
C.RazonSocial,
C.NombreFantasia,
'PlazoCredito'= (Case when CS.PlazoCredito is null then 0 else CS.PlazoCredito end)

From ClienteSucursal CS
Join Cliente C On CS.IDCliente=C.ID











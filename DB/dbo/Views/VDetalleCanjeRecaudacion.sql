﻿CREATE View [dbo].[VDetalleCanjeRecaudacion]

As

--ChequeAlDia
Select
DDB.IDTransaccion,
DDB.ID,
'IDTransaccionDocumento'=DDB.IDTransaccionCheque,
'IDEfectivo'=0,
'Valor'= Convert(varchar(50),C.CodigoTipo), 
'Banco'= C.Banco,
'Numero'= Convert(varchar(50),C.NroCheque),
'Importe'= DDB.Importe ,
'Cliente'= C.Cliente,
'Tipo'='CHEQUE AL DIA',
DB.Fecha,
C.IDMoneda,
C.Cotizacion,
'FechaTransaccion'=(Select Fecha from Transaccion where ID = DB.IDTransaccion)

From DetalleCanjeRecaudacion DDB
join VChequeCliente C on DDB.IDTransaccionCheque = C.IDTransaccion 
Join CanjeRecaudacion DB On DB.IDTransaccion=DDB.IDTransaccion
Where C.Diferido = 0 
and Cast(DB.Fecha as date) < Case when isdate(C.FechaRechazo) = 1 then C.FecRechazo else DateAdd(day,1,DB.Fecha) end

Union All

--ChequeDiferido
Select
DDB.IDTransaccion,
DDB.ID,
'IDTransaccionDocumento'=DDB.IDTransaccionCheque,
'IDEfectivo'=0,
'Valor'= Convert(varchar(50),C.CodigoTipo), 
'Banco'= C.Banco,
'Numero'= Convert(varchar(50),C.NroCheque),
'Importe'= DDB.Importe ,
'Cliente'= C.Cliente,
'Tipo'='CHEQUE DIFERIDO',
DB.Fecha,
C.IDMoneda,
C.Cotizacion,
'FechaTransaccion'=(Select Fecha from Transaccion where ID = DB.IDTransaccion)

From DetalleCanjeRecaudacion DDB
join VChequeCliente C on DDB.IDTransaccionCheque = C.IDTransaccion 
Join CanjeRecaudacion DB On DB.IDTransaccion=DDB.IDTransaccion
Where C.Diferido = 1 
and Cast(DB.Fecha as date) < Case when isdate(C.FechaRechazo) = 1 then C.FecRechazo else DateAdd(day,1,DB.Fecha) end

Union All


--ChequeRechazado
Select
DDB.IDTransaccion,
DDB.ID,
'IDTransaccionDocumento'=DDB.IDTransaccionCheque,
'IDEfectivo'=0,
'Valor'= Convert(varchar(50),C.CodigoTipo), 
'Banco'= C.Banco,
'Numero'= Convert(varchar(50),C.NroCheque),
'Importe'= DDB.Importe ,
'Cliente'= C.Cliente,
'Tipo'='CHEQUE RECHAZADO',
DB.Fecha,
C.IDMoneda,
C.Cotizacion,
'FechaTransaccion'=(Select Fecha from Transaccion where ID = DB.IDTransaccion)

From DetalleCanjeRecaudacion DDB
join VChequeCliente C on DDB.IDTransaccionCheque = C.IDTransaccion 
Join CanjeRecaudacion DB On DB.IDTransaccion=DDB.IDTransaccion
JOIN ChequeClienteRechazado CCR on C.IDTransaccion = CCR.IDTransaccionCheque
and Cast(DB.Fecha as date) >= Case when isdate(C.FechaRechazo) = 1 then C.FecRechazo else DateAdd(day,1,DB.Fecha) end

Union All

--Efectivo
Select
DDB.IDTransaccion,
DDB.ID,
'IDTransaccionDocumento'=DDB.IDTransaccionEfectivo,
DDB.IDEfectivo,
'Valor'= E.CodigoComprobante, 
'Banco'= '---',
'Numero'= Convert(varchar(50),E.Comprobante),
'Importe'= DDB.Importe ,
'Cliente'= '---',
'Tipo'='EFECTIVO',
DB.Fecha,
E.IDMoneda,
E.Cotizacion,
'FechaTransaccion'=(Select Fecha from Transaccion where ID = DB.IDTransaccion)

From DetalleCanjeRecaudacion DDB
Join CanjeRecaudacion DB On DB.IDTransaccion=DDB.IDTransaccion
join VEfectivo E on DDB.IDTransaccionEfectivo = E.IDTransaccion And DDB.IDEfectivo=E.ID

Union All

--DiferenciaCambio
--Select
--DB.IDTransaccion,
--0,
--'IDTransaccionDocumento'=0,
--0,
--'Valor'= 'DIFERENCIACAMBIO', 
--'Banco'= '---',
--'Numero'= '0',
--'Importe'= DB.DiferenciaCambio ,
--'Cliente'= '---',
--'Tipo'='DIFERENCIACAMBIO',
--'False',
--'False',
--'False',
--DB.Fecha,
--DB.IDMoneda,
--DB.Cotizacion,
--'FechaTransaccion'=(Select Fecha from Transaccion where ID = DB.IDTransaccion)
--From vCanjeRecaudacion DB

--Union All

----Documento
--Select
--DDB.IDTransaccionCanjeRecaudacion,
--DDB.ID,
--'IDTransaccionDocumento'=DDB.IDTransaccionCanjeRecaudacion,
--'IDEfectivo'=0,
--'Valor'= (Case When(DDB.Faltante)='True' Then 'FALT' Else (Case When(DDB.Sobrante)='True' Then 'SOB' Else '---' End) End) , 
--'Banco'= '---',
--'Numero'= Convert(varchar(50),0),
--'Importe'= DDB.Importe ,
--'Cliente'= '---',
--'Tipo'='REDONDEO',
--Redondeo,
--Faltante,
--Sobrante,
--DB.Fecha,
--DB.IDMOneda,
--DB.Cotizacion,
--'FechaTransaccion'=(Select Fecha from Transaccion where ID = DB.IDTransaccion)

--From DetalleCanjeRecaudacion DDB
--Join VCanjeRecaudacion DB On DB.IDTransaccion=DDB.IDTransaccionCanjeRecaudacion
--Where DDB.Redondeo='True'

--Union all

Select
DDB.IDTransaccion,
DDB.ID,
'IDTransaccionDocumento'=DDB.IDTransaccionDocumento,
'IDEfectivo'=0,
'Valor'= FPD.CodigoComprobante, 
'Banco'= '---',
'Numero'= Convert(varchar(50),FPD.Comprobante),
'Importe'= DDB.Importe ,
'Cliente'= '---',
'Tipo'='DOCUMENTO',
DB.Fecha,
FPD.IDMOneda,
FPD.Cotizacion,
'FechaTransaccion'=(Select Fecha from Transaccion where ID = DB.IDTransaccion)

From DetalleCanjeRecaudacion DDB
Join VCanjeRecaudacion DB On DB.IDTransaccion=DDB.IDTransaccion
Join VFormaPagoDocumento FPD on DDB.IDTransaccionDocumento = FPD.IDTransaccion and DDB.IDDetalleDocumento = FPD.ID










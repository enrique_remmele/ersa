﻿
CREATE View [dbo].[VFormaPagoFactura]
As
select 
ID,
Referencia,
Descripcion,
'Contado'=Case When (Contado) = 1 Then 'True' Else 'False' End,
'Credito'=Case When (Credito) = 1 Then 'True' Else 'False' End,
'CancelarAutomatico'=Case When (CancelarAutomatico) = 1 Then 'True' Else 'False' End,
IDCuentaContable,
'CodigoCuentaContable'=(Select codigo from CuentaContable C where C.ID = IDCuentaContable),
'CuentaContable'= (Select Descripcion from CuentaContable C where C.ID = IDCuentaContable),
Estado,
'Aprobacion' =IsNull(Aprobacion, 'True'),
'FacturacionSinPedido'=Isnull(FacturacionSinPedido,'False'),
Condicion
From FormaPagoFactura



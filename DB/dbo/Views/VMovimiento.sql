﻿

CREATE View [dbo].[VMovimiento]
As

Select 
M.IDTransaccion,
M.Numero,
'Num'=M.Numero,
M.Fecha,
'Fec'=CONVERT(varchar(50), M.Fecha, 6),
M.IDTipoOperacion,
'Operacion'=T.Descripcion,
T.Entrada,
T.Salida,
M.IDMotivo,
'Motivo'=MM.Descripcion,
M.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
M.NroComprobante,
'Comprobante'=Concat((Case When M.MovimientoCombustible=1 then 'CCO' else
				(Case When M.MovimientoStock=1 Then 'MOV' else
				(Case WHen M.DescargaStock=1 Then 'DST' else
				(Case WHen M.MovimientoMateriaPrima=1 Then 'MMP' else
				'DCOM'end)end)end)end),'', M.Numero, ' - ',  M.NroComprobante),
'IDDepositoEntrada' = IsNull((Select D.ID From Deposito D Where D.ID=M.IDDepositoEntrada), 0) ,
'DepositoEntrada'=IsNull((Select D.[Suc-Dep] From VDeposito D Where D.ID=M.IDDepositoEntrada), '---') ,
'IDDepositoSalida' = IsNull((Select D.ID From Deposito D Where D.ID=M.IDDepositoSalida), 0) ,
'DepositoSalida'=IsNull((Select D.[Suc-Dep] From VDeposito D Where D.ID=M.IDDepositoSalida), '---'),
M.Observacion,
M.Autorizacion,
M.Total,
M.Anulado,
'Estado'=Case When M.Anulado='True' Then 'Anulado' Else 'OK' End,

--Transaccion
'FechaTransaccion'=TR.Fecha,
TR.IDDeposito,
TR.IDSucursal,
TR.IDTerminal,
TR.IDUsuario,
'SUC'=(select Codigo from Sucursal where ID = Tr.IDSucursal),
'NombreUsuario'=(Select U.Nombre From Usuario U Where U.ID=TR.IDUsuario),
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion),

--Otros
'CodigoSucursalOperacion'=IsNull((Select S.Codigo From Sucursal S Where S.ID=TR.IDSucursal), '---')
, CASE T.Entrada
      WHEN 1 THEN M.Total
      ELSE 0
      END AS TotalEntrada
      , CASE T.Salida
      WHEN 1 THEN M.Total
      ELSE 0
      END AS TotalSalida,

'MovimientoCombustible'=Case When M.MovimientoCombustible='True' Then 'True' Else 'False' End,
'MovimientoStock' =Case When M.MovimientoStock='True' Then 'True' Else 'False' End,
'DescargaStock' = Case When M.DescargaStock='True' Then 'True' Else 'False' End,
'MovimientoMateriaPrima' = Case When M.MovimientoMateriaPrima='True' Then 'True' Else 'False' End,
'DescargaCompra' = Case When M.DescargaCompra='True' Then 'True' Else 'False' End,
'IDCamion'=isnull(M.IDCamion,0),
'Camion'=Isnull((select Descripcion from Camion where ID = Isnull(M.IDCamion,0)),''),
'IDChofer'=isnull(M.IDChofer,0),
'Chofer'=Isnull((select Nombres from Chofer where ID = Isnull(M.IDChofer,0)),''),
M.Kilometro,
M.IDDestinoCombustible,
'DestinoCombustible'= (select isnull(descripcion,'') from DestinoCombustible where ID = M.IDDestinoCombustible),
'IDTransaccionEnvio'=ISNULL(M.IDTransaccionEnvio,0),
--'Enviar'=isnull(M.Enviar,'False'),
'Enviar'=(Case when (Select TOP(1) TipoDeposito from vDeposito where id = M.IDDepositoEntrada)like '%TRANSITO%' and (M.Fecha >= '20190828') then 'True' else 'False'end),
'Recibir'=isnull(M.Recibir,'False'),
'Recibido'=isnull(M.Recibido,'False'),
'IDSucursalDestino'= (select IDSucursalDestinoTransito from deposito where id = M.IDDepositoEntrada),
'NumeroSolicitudCliente' = isnull((select top(1) NumeroSolicitudCliente from devolucionsinnotacredito where IDTransaccionMovimientoStock = M.IDTransaccion),'')


From Movimiento M
Join Transaccion TR On M.IDTransaccion=TR.ID
Join TipoOperacion T On M.IDTipoOperacion=T.ID
Join MotivoMovimiento MM On M.IDMotivo=MM.ID
Join TipoComprobante TC On M.IDTipoComprobante=TC.ID

























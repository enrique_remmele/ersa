﻿







CREATE View [dbo].[VNotaDebitoProveedor]
As
Select

--Proveedor
N .IDProveedor ,
'Proveedor'=P.RazonSocial,
P.Referencia,
P.IDTipoProveedor,
P.Telefono,
P.RUC,

--Cabecera
S.IDCiudad,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
N.Numero ,
N.IDTransaccion,
N.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
N.NroComprobante,
'Comprobante'=N.NroComprobante,
N.NroTimbrado, 
N .IDDeposito,
'Deposito'=D.Descripcion,
'Suc'=S.Codigo,
N .Fecha,
N.IDSucursal,
'Fec'=CONVERT(varchar(50), N .Fecha, 6),
N .IDMoneda,
'Moneda'=M.Referencia,
'DescripcionMoneda'=M.Descripcion,
N .Cotizacion,
N .Observacion,
N.Descuento ,
N.Devolucion ,

--Totales
N.Total,
N.TotalImpuesto,
N.TotalDiscriminado,
N.Saldo,
N.Anulado,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion)


From NotaDebitoProveedor   N
Join Transaccion T On N.IDTransaccion=T.ID
Left Outer Join TipoComprobante TC On N.IDTipoComprobante=TC.ID
Left Outer Join Proveedor  P On N.IDProveedor =P.ID
Left Outer Join Sucursal S On N.IDSucursal=S.ID
Left Outer Join Deposito D On N.IDDeposito=D.ID
Left Outer Join Moneda M On N.IDMoneda=M.ID









﻿
CREATE  view [dbo].[VComisionVendedor]
as

Select 
V.IDTransaccion ,
--Vendedor
V.IDVendedor,
V.Vendedor,


--Cliente
V.IDCliente,
V.Cliente,
V.ReferenciaCliente,
V.IDTipoCliente ,
 
V.IDCiudad,
V.IDCiudadCliente,
V.Ciudad,
V.IDSucursal, 
V.Sucursal,
V.IDDeposito, 
V.Deposito,
V.IDMoneda,
V.Moneda ,
V.Credito, 
V.Fecha,

--Clasificacion
P.ID,
'Producto'=P.Descripcion,
P.Referencia,
P.IDTipoProducto,
P.TipoProducto ,
P.IDLinea,
P.Linea,
P.IDMarca,
P.Marca, 



--Porcentaje y monto de las Comisiones
 C.Porcentaje,
 C.Monto,
 'ImporteVenta'=DV.Total ,
 'ImporteDevuelto'=0,
 C.Devolucion,
 
'CantidadVenta'=DV.Cantidad,
'CantidadDevuelta'=0

From Comision C
Join VDetalleVenta DV On C.IDTRansaccionDetalleVenta=DV.IDTransaccion And C.IDProducto=DV.IDProducto And C.ID=DV.ID 
Join VVenta V On DV.IDTransaccion =V.IDTransaccion 
Join VProducto P On C.IDProducto=P.ID 


Union All

Select 
V.IDTransaccion ,
--Vendedor
CL.IDVendedor,
CL.Vendedor,

--Cliente
V.IDCliente,
V.Cliente,
'ReferenciaCliente'=CL.Referencia,
V.IDTipoCliente ,
 
V.IDCiudad,
V.IDCiudadCliente,
V.Ciudad,
V.IDSucursal, 
V.Sucursal,
V.IDDeposito, 
V.Deposito,
V.IDMoneda,
V.Moneda ,
V.Credito,
V.Fecha,

--Clasificacion
P.ID,
'Producto'=P.Descripcion,
P.Referencia,
P.IDTipoProducto,
P.TipoProducto ,
P.IDLinea,
P.Linea,
P.IDMarca,
P.Marca, 

--Porcentaje y monto de las Comisiones
 C.Porcentaje,
 C.Monto,
 'ImporteVenta'=0,
 'ImporteDevuelto'= DN.Total ,
 C.Devolucion,
 
 'CantidadVenta'=0,
 'CantidadDevuelta'=DN.Cantidad 

From Comision C
Join VDetalleNotaCredito  DN On C.IDTransaccionDetalleVenta=DN.IDTransaccion And C.IDProducto=DN.IDProducto And C.ID=DN.ID 
Join VNotaCredito  V On DN.IDTransaccion =V.IDTransaccion 
Join VProducto P On C.IDProducto=P.ID 
Join VCliente CL On V.IDCliente=CL.ID 













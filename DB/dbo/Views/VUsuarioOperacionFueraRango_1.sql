﻿
CREATE View [dbo].[VUsuarioOperacionFueraRango]
As
Select 

UO.IDUsuario,
'Usuario' = U.Nombre,
UO.IDOperacion,
'Operacion' = O.Descripcion,
'Desde'=UO.Desde,
'Hasta'=UO.Hasta

From UsuarioOperacionFueraRangoFecha UO
Join Usuario U On UO.IDUsuario=U.ID
Join Operacion O On UO.IDOperacion=O.ID



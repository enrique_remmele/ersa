﻿CREATE view [dbo].[VaControlCostoDetalle]
as 
Select
a.IDTransaccion, 
a.IDTransaccionOperacion, 
a.idproducto, 
producto, 
referencia, 
fecha, 
movimiento,
operacion, 
tipo, 
[cod.], 
[Cliente/Proveedor], 
Entrada, 
Salida, 
'Saldo'= a.CantidadSaldo, 
'Costo'= a.CostoViejo, 
'Calculado'=a.CostoNuevo, 
'Diferencia'=a.CostoViejo - a.CostoNuevo
from aControlCostoDetalle a
join VExtractoMovimientoProductoDetalleCosteado e on a.idtransaccionOperacion = e.IDTransaccion and e.IDProducto = a.IDProducto
Where e.anulado = 'False'



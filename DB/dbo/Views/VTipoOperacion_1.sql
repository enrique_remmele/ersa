﻿Create View [dbo].[VTipoOperacion]
As
Select 
T.ID, 
'Operacion'=O.Descripcion,
T.IDOperacion,
T.Descripcion,
T.Activo,
'Estado'=Case When (T.Activo) = 'True' Then 'OK' Else '---' End,
T.Entrada,
T.Salida
From TipoOperacion T 
Join Operacion O On T.IDOperacion=O.ID

﻿
CREATE View [dbo].[VNotaCreditoVentaAplicacionInforme]

As

Select
'IDTransaccion'=NCV.IDTransaccionNotaCredito, 
'IDTransaccionVenta'=V.IDTransaccion , 
V.Comprobante,
V.IDCliente,
V.Saldo,
'FechaEmision'=Cast(V.FechaEmision as date),
'TotalVenta'=V.Total,
'Importe'=Sum(NCV.Importe),
'IDTransaccionNotaCreditoAplicacion'=0,
--'Aplicado'=(Case When NC.Aplicar='False' Then'True' Else (Case When (Select NCVA.IDTransaccionNotaCredito From NotaCreditoVentaAplicada NCVA Where NCVA.IDTransaccionNotaCredito=NCV.IDTransaccionNotaCredito) Is Null Then 'False' Else 'True' End) End)
'Aplicado'=(Case When NC.Aplicar='False' Then 'True' Else 'False' End),
V.Cliente,
NC.Sucursal,
NC.IDSucursal,
NC.IDMoneda,
NC.Moneda,
'ComprobanteNC'= NC.Comprobante,
'FechaNC'=Cast(NC.Fecha as date),
'ImporteNC'=NC.Total,
NC.Devolucion,
NC.Descuento,
NC.IDTipoProducto,
V.Condicion,
'Anulado' = 'False',
'MotivoNotaCredito' = (Case When NC.Devolucion = 1 then 'Devolucion' else  SMNC.Descripcion end)

From NotaCreditoVenta  NCV
Join VNotaCredito NC On NCV.IDTransaccionNotaCredito=NC.IDTransaccion
join VVenta  V on NCV.IDTransaccionVentaGenerada  = V.IDTransaccion 
left outer join SubMotivoNotacredito SMNC on SMNC.ID = NC.IDSubMotivoNotaCredito
where nc.anulado = 0
and v.Anulado = 0
and nc.aplicar = 0

Group by NCV.IDTransaccionNotaCredito, V.IDTransaccion, V.Comprobante, V.IDCliente, V.Saldo, V.FechaEmision, V.Total,NC.Aplicar,
V.Cliente,NC.IDSucursal,NC.Sucursal, NC.IDMoneda, NC.MOneda, NC.Comprobante, NC.Fecha, NC.Total, NC.Devolucion, NC.Descuento,
NC.IdTipoProducto, V.Condicion,SMNC.Descripcion

Union All

Select
NCVA.IDTransaccionNotaCredito, 
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
'FechaEmision'=Cast(V.FechaEmision as date),
'TotalVenta'=V.Total,
'Importe'=Sum(NCVA.Importe),
NCVA.IDTransaccionNotaCreditoAplicacion ,
'Aplicado'='True',
V.CLiente,
V.Sucursal,
V.IDSucursal,
V.IDMoneda,
V.Moneda,
'ComprobanteNC'= NC.Comprobante,
'FechaNC'=Cast(NC.Fecha as date),
'ImporteNC'=NC.Total,
NC.Devolucion,
NC.Descuento,
NC.IdTipoProducto,
V.Condicion,
'Anulado' = 'False',
'MotivoNotaCredito' = (Case When NC.Devolucion = 1 then 'Devolucion' else  SMNC.Descripcion end)

From NotaCreditoVentaAplicada   NCVA
join VVenta  V on NCVA.IDTransaccionVenta  = V .IDTransaccion 
Join VNotaCredito NC on NC.IDTransaccion = NCVA.IDTransaccionNotaCredito
Join NotaCreditoAplicacion NCA on NCA.IDTransaccion = NCVA.IDTransaccionNotaCreditoAplicacion and NCA.Anulado = 0
left outer join SubMotivoNotacredito SMNC on SMNC.ID = NC.IDSubMotivoNotaCredito
where nc.anulado = 0
and v.Anulado = 0
Group by NCVA.IDTransaccionNotaCredito, V.IDTransaccion, V.Comprobante, V.IDCliente, V.Saldo, V.FechaEmision, V.Total,NC.Aplicar,
V.Cliente,V.IDSucursal,V.Sucursal, V.IDMoneda, V.MOneda, IDTransaccionNotaCreditoAplicacion, NC.Comprobante,
NC.Fecha, NC.Total, NC.Devolucion,NC.Descuento, NC.IdTipoProducto, V.Condicion,SMNC.Descripcion















﻿


CREATE View [dbo].[VDetalleActividad]
As
Select
DA.IDActividad,
--Por este caso ID va a ser igaul a IDProducto
--E ID igual a indice
'ID'=DA.IDProducto,
'IDProducto'=DA.IDProducto,
'Indice'=DA.ID,
P.Descripcion,
'Producto'=P.Descripcion,
'Ref'=P.Referencia,
P.Referencia,
P.Estado,
P.IDTipoProducto,
P.IDLinea,
P.IDSubLinea,
P.IDSubLinea2,
P.IDMarca,
P.IDPresentacion,
'Relacionado'=(Select Case When(Select Min(IDActividad) From DetallePlanillaDescuentoTactico DPDT 
								Where DPDT.IDActividad=A.ID) IS Null Then 'False' Else 'True' End),

D.DescuentoMaximo,
D.Excedente,
D.Asignado,
D.Codigo,
D.Mecanica,
D.PorcentajeUtilizado,
D.Saldo,
D.Total
From DetalleActividad DA
Join Actividad A On DA.IDActividad=A.ID
Left Outer Join VDetallePlanillaDescuentoTactico D On DA.IDActividad=D.ID
Join Producto P On DA.IDProducto=P.ID













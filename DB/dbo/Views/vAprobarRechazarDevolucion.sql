﻿Create view vAprobarRechazarDevolucion
as 
select 
 ARD.IDTransaccion,
 ARD.IDSucursal,
 'Sucursal'=S.Descripcion,
 ARD.Numero,
 ARD.Fecha,
 ARD.Destino,
 ARD.Aprobado,
 ARD.Observacion,
 'FechaTransaccion'=T.Fecha,
 'IDUsuario'=T.IDUsuario,
 U.Usuario
from APROBARRECHAZARDEVOLUCION ARD
Join Sucursal S on ARD.IDSucursal = S.ID
Join Transaccion T on ARD.IDTransaccion = T.ID
Join Usuario U on T.IDUsuario = U.ID

﻿CREATE view [dbo].[VSaldoBanco]

as

--Depósito Bancario
Select 
DB.Banco,
DB.Cuenta,
DB.Conciliado, 
'Debito'=DB.Total,
'Credito'=0,
'Movimiento'='DB',
'UltimoDebito'=DB.Fecha,
'UltimoCredito'=NULL,
Moneda

From VDepositoBancario DB 

 
Union All

--CREDITO BANCARIO
Select  
DCB.Banco,
DCB.Cuenta,
DCB.Conciliado,
'Debito'=0,
'Credito'=DCB.Total,
'Movimiento'='CB',
'UltimoDebito'=NULL ,
'UltimoCredito'=DCB.Fecha ,
Moneda

From VDebitoCreditoBancario DCB 
Where DCB.Credito='True'



Union All

--DEBITO BANCARIO
Select  
DCB.Banco, 
DCB.Cuenta,
DCB.Conciliado, 
'Debito'=DCB.Total,
'Credito'=0,
'Movimiento'='DB',
'UltimoDebito'=DCB.Fecha,
'UltimoCredito'=NULL,
Moneda

From VDebitoCreditoBancario DCB 
Where DCB.Debito='True'


  
Union All

--Orden Pago cheque al dia
Select 
OP.Banco,
'Cuenta'=OP.CuentaBancaria,
OP.Conciliado,
'Debito'=0,
'Credito'=OP.ImporteMoneda,
'Movimiento'='OP',
'UltimoDebito'=NULL ,
'UltimoCredito'=OP.Fecha   ,
Moneda

 From VOrdenPago OP
 Where Cheque= 'True'And Anulado = 'False'
 and OP.Diferido = 0

 Union All

--Orden Pago cheque diferido
Select 
OP.Banco,
'Cuenta'=OP.CuentaBancaria,
OP.Conciliado,
'Debito'=0,
'Credito'=OP.ImporteMoneda,
'Movimiento'='OP',
'UltimoDebito'=NULL ,
'UltimoCredito'=OP.FechaVencimiento   ,
Moneda

 From VOrdenPago OP
 Where Cheque= 'True'And Anulado = 'False'
 and OP.Diferido = 1


 Union All
 
--Descuento Cheque 
Select 
DC.Banco,
DC.Cuenta,
DC.Conciliado,
'Debito'=DC.TotalDescontado,
'Credito'=DC.TotalAcreditado,
'Movimiento'='DC',
'UltimoDebito'=DC.Fecha,
'UltimoCredito'=DC.Fecha ,
Moneda
From VDescuentoCheque DC  

--Union All

----Cheque Rechazado
--Select 
--CC.Banco,
--'Cuenta'=CC.CuentaBancaria,
--CC.Conciliado,
--'Debito'=CC.Importe,
--'Credito'=0,
--'Movimiento'='CCR',
--'UltimoDebito'=CR.Fecha, 
--'UltimoCredito'=NULL,
--CC.Moneda
--From VChequeClienteRechazado CR
--Join VChequeCliente CC On CR.IDTRansaccionCheque=CC.IDTransaccion    



















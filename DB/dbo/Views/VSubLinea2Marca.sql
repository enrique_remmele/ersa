﻿CREATE View [dbo].[VSubLinea2Marca]
As
Select
SL2M.*,
'TipoProducto'=TP.Descripcion,
'Linea'=L.Descripcion,
'SubLinea'=SL.Descripcion,
'SubLinea2'=SL2.Descripcion,
'Marca'=M.Descripcion

From SubLinea2Marca SL2M
Join TipoProducto TP On SL2M.IDTipoProducto=TP.ID
Join Linea L On SL2M.IDLinea=L.ID
JOin SubLinea SL On SL2M.IDSubLinea=SL.ID
JOin SubLinea2 SL2 On SL2M.IDSubLinea2=SL2.ID
JOin Marca M On SL2M.IDMarca=M.ID

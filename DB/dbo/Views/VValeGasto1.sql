﻿CREATE View [dbo].[VValeGasto1]
As
Select V.IdTransaccion, V.Numero, V.NroComprobante, V.Cod, V.Suc,'IDGrupo'=(select ID From Grupo where Descripcion = Grupo), Grupo,
V.fecha, V.fec, V.Moneda, V.Motivo, V.Nombre, V.Total, V.Estado, V.Saldo,V.Anulado,
V.Cancelado, G.IdTransaccion as IdGasto, G.Numero as NroGasto, G.TipoComprobante, G.Comprobante,
NroTimbrado,G.Proveedor,G.Referencia, G.Fecha as FechaGasto, G.Fec as FechGasto,
G.CreditoContado, G.Fechavencimiento, G.Observacion, G.Total as TotalGasto, G.Cancelado as CanceladoGasto,
G.Comprobante as ComprobanteGasto, G.TipoComprobante as TipoComprobanteGasto,
G.Numero as NumeroGasto, V.ARendir, 'ImporteAplicado'=GV.Importe
from Vvale V
left Join vGastoVale GV on GV.Idtransaccion = V.Idtransaccion
left Join vGasto G on G.Idtransaccion = GV.IdtransaccionGasto


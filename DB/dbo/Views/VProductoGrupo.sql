﻿Create View [dbo].[VProductoGrupo]
As
Select
TP.IDTipoProducto,
TP.TipoProducto,
L.IDLinea,
L.Linea,
SL.IDSubLinea,
SL.SubLinea,
SL2.IDSubLinea2,
SL2.SubLinea2,
M.IDMarca,
M.Marca,
M.IDPresentacion,
M.Presentacion


From
VTipoProductoLinea TP
Join VLineaSubLinea L On TP.IDTipoProducto=L.IDTipoProducto
JOin VSubLineaSubLinea2 SL On L.IDTipoProducto=SL.IDTipoProducto And L.IDLinea=SL.IDLinea
Join VSubLinea2Marca SL2 On SL.IDTipoProducto=SL2.IDTipoProducto And SL.IDLinea=SL2.IDLinea And SL.IDSubLinea=SL.IDSubLinea
Join VMarcaPresentacion M On SL2.IDTipoProducto=M.IDTipoProducto And SL2.IDLinea=M.IDLinea And SL2.IDSubLinea=M.IDSubLinea


﻿CREATE View [dbo].[VCubo_Clientes_Por_Canales]
As
Select
'IDVendedor'=Convert(varchar(50), IDVendedor),
'Vendedor'=Case When (Vendedor) = '' Then 'INACTIVO' Else Vendedor End,
'IDCliente'=Convert(varchar(50), Referencia),
'RazonSocial'=RazonSocial,
'Canal'=TipoCliente,
'SC'='EXPRESS CENTRAL',
'Idsupervisor'='1',
'Supervisor'='NIMIA VALIENTE'

From VCliente

Where IDSucursal=2

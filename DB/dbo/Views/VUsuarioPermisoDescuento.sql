﻿CREATE View [dbo].[VUsuarioPermisoDescuento]
As

Select  distinct 
U.ID,
U.Nombre,
U.Usuario,
U.[Password],

--Perfil
U.IDPerfil,
'Perfil'=IsNull(P.Descripcion, '---'),

U.Identificador,
U.Estado,
U.Email,

--Vendedor
'EsVendedor'=IsNull(U.EsVendedor, 'False'),
U.IDVendedor,
'Vendedor'=IsNull(V.Nombres, '---'),

--Chofer
'EsChofer'=IsNull(U.EsChofer, 'False'),
U.IDChofer,
'Chofer'=IsNull(C.Nombres, '---'),
'VerCosto'=IsNull(P.VerCosto, 'False')

From Usuario U
Left Outer Join Perfil P On U.IDPerfil=P.ID
Left Outer Join Vendedor V On U.IDvendedor=V.ID
Left Outer Join Chofer C On U.IDChofer=C.ID
Join vAccesoEspecificoPerfil AEP On P.ID= AEP.IDPerfil
where ((AEP.Tag = 'frmProducto' 
And AEP.NombreControl = 'tabPrecios'
And AEP.habilitado = 1) or P.Id = 1)






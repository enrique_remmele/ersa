﻿
CREATE View [dbo].[VCuentaFijaFormaPagoEfectivo]
As
Select 
CE.ID,
CE.Descripcion,
CE.Orden,

CE.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,
CE.IDMoneda,
'Moneda'=M.Referencia,

'Debe'='True',
'Haber'='False',
'Credito'=0,
'Debito'=0,

CE.IDCuentaContable,
'Cuenta'=CC.Codigo + ' ' + CC.Descripcion,
'CuentaContable'=CC.Descripcion,
'Codigo'=CC.Codigo


From CuentaFijaFormaPagoEfectivo CE
Join TipoComprobante TC On CE.IDTipoComprobante=TC.ID
Join Moneda M On CE.IDMoneda=M.ID
Join CuentaContable CC On CE.IDCuentaContable=CC.ID

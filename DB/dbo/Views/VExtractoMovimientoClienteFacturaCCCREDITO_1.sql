﻿CREATE view [dbo].[VExtractoMovimientoClienteFacturaCCCREDITO]

As

--Cobranzas credito
Select
CC.IDTransaccion, 
'Operacion'=Concat('COB.CRED',CC.Numero),
'Codigo'=V.IDCliente,
'Fecha'=CC.FechaEmision,
'Documento'=Concat('COB.CRED',CC.Numero),
'Detalle/Concepto'=V.Observacion,
'Debito'=0,
'Credito'=VC.Importe * ISNULL(V.Cotizacion,1), 
'Saldo'=0.00,
'Cotizacion'=isnull(CC.Cotizacion,1),
'IDMoneda'=CC.IDMoneda,
'Movimiento'='COBRANZACREDITO',
'ComprobanteAsociado'=V.IDTransaccion,
'CodigoCC' = DA.Codigo
From VentaCobranza VC 
Join Venta V On VC.IDTransaccionVenta = V.IDTransaccion
Join CobranzaCredito CC On VC.IDTransaccionCobranza = CC.IDTransaccion
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = CC.IDTransaccion and Da.Credito>0 and DA.Descripcion like '%Deudor%'
where CC.Anulado = 0 and isnull(CC.AnticipoCliente,0) = 0

union all

--Anticipo
Select
CC.IDTransaccion, 
'Operacion'=Concat('APL.',CC.Numero),
'Codigo'=V.IDCliente,
'Fecha'=CC.FechaEmision,
'Documento'=Concat('APL.',CC.Numero),
'Detalle/Concepto'=V.Observacion,
'Debito'=0,
'Credito'=VC.Importe * ISNULL(V.Cotizacion,1), 
'Saldo'=0.00,
'Cotizacion'=isnull(CC.Cotizacion,1),
'IDMoneda'=CC.IDMoneda,
'Movimiento'='APLICACIONANTICIPO',
'ComprobanteAsociado'=V.IDTransaccion,
'CodigoCC' = DA.Codigo
From AnticipoVenta VC
Join vAnticipoAplicacion AP on AP.IDTransaccion = VC.IDTransaccionAnticipo
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join CobranzaCredito CC On AP.IDTransaccionCobranza = CC.IDTransaccion
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = AP.IDTransaccion and Da.Credito>0 and DA.Descripcion like '%Deudor%'
where CC.Anulado = 0 and isnull(CC.AnticipoCliente,0) = 1

union all

--Cobranzas contado
Select
CC.IDTransaccion, 
'Operacion'=Concat('COB.CONT',CC.Numero),
'Codigo'=V.IDCliente,
'Fecha'=CC.Fecha,
'Documento'=Concat('COB.CONT',CC.Numero),
'Detalle/Concepto'=V.Observacion,
'Debito'=0,
'Credito'=VC.Importe * ISNULL(V.Cotizacion,1), 
'Saldo'=0.00,
'Cotizacion'=isnull(CC.Cotizacion,1),
'IDMoneda'=CC.IDMoneda,
'Movimiento'='COBRANZACONTADO',
'ComprobanteAsociado'=V.IDTransaccion,
'CodigoCC' = DA.Codigo
From VentaCobranza VC 
Join Venta V On VC.IDTransaccionVenta = V.IDTransaccion
Join CobranzaContado CC On VC.IDTransaccionCobranza = CC.IDTransaccion
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = CC.IDTransaccion and Da.Credito>0 and DA.Descripcion like '%Deudor%'
where CC.Anulado = 0 


union all

--Nota Credito
Select
CC.IDTransaccion, 
'Operacion'=Concat('NCR',CC.NroComprobante),
'Codigo'=V.IDCliente,
'Fecha'=CC.Fecha,
'Documento'=Concat('NCR',CC.NroComprobante),
'Detalle/Concepto'=CC.Observacion,
'Debito'=0,
'Credito'=VC.Importe * ISNULL(V.Cotizacion,1), 
'Saldo'=0.00,
'Cotizacion'=isnull(CC.Cotizacion,1),
'IDMoneda'=CC.IDMoneda,
'Movimiento'='NOTACREDITO',
'ComprobanteAsociado'=V.IDTransaccion,
'CodigoCC' = DA.Codigo
From NotaCreditoVenta VC 
Join Venta V On VC.IDTransaccionVentaGenerada = V.IDTransaccion
Join NotaCredito CC On VC.IDTransaccionNotaCredito = CC.IDTransaccion
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = CC.IDTransaccion and Da.Credito>0 and DA.Descripcion like '%Deudor%'
where CC.Anulado = 0  and isnull(CC.Aplicar,0) = 0

union all

--Nota Credito
Select
CC.IDTransaccion, 
'Operacion'=Concat('APL',CC.NroComprobante),
'Codigo'=V.IDCliente,
'Fecha'=CC.Fecha,
'Documento'=Concat('APL',CC.NroComprobante),
'Detalle/Concepto'=CC.Observacion,
'Debito'=0,
'Credito'=VC.Importe * ISNULL(V.Cotizacion,1), 
'Saldo'=0.00,
'Cotizacion'=isnull(CC.Cotizacion,1),
'IDMoneda'=CC.IDMoneda,
'Movimiento'='APLICACIONNCR',
'ComprobanteAsociado'=V.IDTransaccion,
'CodigoCC' = DA.Codigo
From NotaCreditoVentaAplicada VC 
Join Venta V On VC.IDTransaccionVenta = V.IDTransaccion
Join NotaCredito CC On VC.IDTransaccionNotaCredito = CC.IDTransaccion
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = CC.IDTransaccion and Da.Credito>0 and DA.Descripcion like '%Deudor%'
where CC.Anulado = 0  and isnull(CC.Aplicar,0) = 1


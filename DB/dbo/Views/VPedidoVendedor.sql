﻿

CREATE View [dbo].[VPedidoVendedor]
As

Select

P.IDTransaccion,

--Cabecera
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
PV.Numero,
P.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
P.Fecha,
'Fec'=CONVERT(varchar(50), P.Fecha, 6),
P.FechaFacturar,
'Facturar'=CONVERT(varchar(50), P.FechaFacturar, 6),
'FacturaNro'= IsNull((Select V.[Cod.]  + '  ' + V.Comprobante  from VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=V.IDTransaccion), '---'), 

--Cliente
P.IDCliente,
'Cliente'=C.RazonSocial,
'Ref.Cliente'=C.Referencia ,
C.RUC,
P.IDSucursalCliente,
P.Direccion,
P.Telefono,
'IDZonaVenta'=IsNull(C.IDZonaVenta, 0),
C.ZonaVenta,
P.Procesado, 

--Opciones
P.IDDeposito,
'Deposito'=D.Descripcion,
P.Observacion,
P.IDMoneda,
'Moneda'=M.Referencia,
P.Cotizacion,
P.Anulado,

--Otros
P.IDListaPrecio,
'Lista de Precio'=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=P.IDListaPrecio), '---'),
PV.IDVendedor,
'Vendedor'=ISNULL((Select VE.Nombres From Vendedor VE Where VE.ID=PV.IDVendedor), '---'),
P.EsVentaSucursal,
--P.Facturado,
'Facturado'=(Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End),

'Estado'=(Case When (Anulado) = 'True' Then 'Anulado' Else (Case When (Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End) = 'True' Then 'Facturado' Else 'Pendiente' End) End),
'Venta'=IsNull((Select Top(1) V.Comprobante From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc), '---'),

--Totales
P.Total,
P.TotalImpuesto,
P.TotalDiscriminado,
P.TotalDescuento,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Importado
'Comprobante'=ISNULL(PE.NroFactura, '---'),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion),
P.IDFormaPagoFactura,
'FormaPagoFactura'=(Select FPF.Referencia From FormaPagoFactura FPF Where FPF.ID=P.IDFormaPagoFactura),
C.LimiteCredito,
C.SaldoCredito,
'Aprobado'=(Case When (P.Aprobado) = 'True' Then 'True' Else 'False' End)

From Pedido P
Join Transaccion T On P.IDTransaccion=T.ID
Join PedidoVendedor PV On P.IDTransaccion=PV.IDTransaccion
Left Outer Join VCliente C On P.IDCliente=C.ID
Left Outer Join Sucursal S On P.IDSucursal=S.ID
Left Outer Join Deposito D On P.IDDeposito=D.ID
Left Outer Join Moneda M On P.IDMoneda=M.ID
Left Outer Join PedidoImportado PE On P.IDTransaccion=PE.IDTransaccion



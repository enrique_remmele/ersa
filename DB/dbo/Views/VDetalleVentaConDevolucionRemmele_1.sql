﻿CREATE View [dbo].[VDetalleVentaConDevolucionRemmele]
As

Select
'ID'=(ROW_NUMBER() OVER(PARTITION BY DT.IDTransaccion ORDER BY DT.IDTransaccion ASC)),
'Fecha'=CONVERT(Date,V.FechaEmision),
'TDOC'=V.TipoComprobante,
'DOCUM'=V.Comprobante,
'CODIGO'=V.ReferenciaCliente,
'RSOCIAL'=V.DireccionAlternativa,
--'SUCURS'=V.ReferenciaSucursal,
'CVENTA'=V.Condicion,
'PRODUCTO'=P.Referencia,
'DESCRIP'=P.Descripcion,
'CANT'=DT.Cantidad - IsNull(VD.Cantidad, 0),
'KILOS'=ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0 ),
'TOTKLS'=(DT.Cantidad-IsNull(VD.Cantidad,0)) * ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0),
--'TOTKLS'=(DT.Cantidad) * ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0),
'IMPORTE'=(DT.Total - IsNull(VD.Total,0)) - ((DT.TotalImpuesto - IsNull(VD.TotalImpuesto,0))),
--'IMPORTE'=DT.Total - DT.TotalImpuesto,
'VEND'=VEN.Referencia,
'VENDNOM'=V.Vendedor,
'TCOM'=V.IDTipoCliente,--Tipo de Comercio

'IDDEPARTAMENTOCLIENTE'=CLI.IDDEPARTAMENTO,
'DEPARTAMENTOCLIENTE'=CLI.DEPARTAMENTO,
'IDCIUDADCLIENTE'=CLI.IDCIUDAD,
'CIUDADCLIENTE'=CLI.CIUDAD,
'BARRIOCLIENTE'=CLI.Barrio,
'IDZONACLIENTE'=CLI.IDZonaVenta,
'ZONANCLIENTE'=CLI.ZonaVenta,
'AREA'=CLI.ReferenciaArea,
'REGIONCLIENTE'=CLI.IDSucursal,


'IDDEPARTAMENTOENTREGA'=Isnull(CS.IDDepartamento,CLI.IDDEPARTAMENTO),
'DEPARTAMENTOENTREGA'=Isnull(CS.Departamento,CLI.DEPARTAMENTO),
'IDCIUDADENTREGA'=Isnull(CS.IDCiudad,CLI.IDCIUDAD),
'CIUDADENTREGA'=Isnull(CS.Ciudad,CLI.CIUDAD),
'BARRIOENTREGA'=Isnull(CS.Barrio, CLI.Barrio),
'IDZONAENTREGA'=Isnull(CS.IDZonaVenta,CLI.IDZonaVenta),
'ZONANENTREGA'=Isnull(CS.ZonaVenta,CLI.ZonaVenta),
'REGIONENTREGA'=Isnull(CS.ID,CLI.IDSucursal),

'RUTA'=CLI.ReferenciaRuta,
'CANAL'=V.IDListaPrecio,
'CANALNOM'=V.[Lista de Precio],
--'VEHI'='',
--'REPARTO'='',
'MES'=Month(V.FechaEmision),
'AÑO'=Year(V.FechaEmision),
'SUCURSAL'=V.Ciudad,
'Categoria'=P.TipoProducto,
'SubCategoria'=P.Linea,
'Empaque'=P.[Uni. Med.],
'Sub-Empaque'=P.Peso,
'DescTCOM'=V.TipoCliente,
'DescCanal'=V.[Lista de Precio],
'Sub-Canal'='',
'DescArea'=CLI.Area,
'Vendedor'=V.Vendedor,
'Precio neto CU'=IsNull(DT.PrecioUnitario/(Select IMP.FactorDiscriminado From Impuesto IMP Where IMP.ID=DT.IDImpuesto),0),
--'IVA'=(DT.TotalImpuesto - IsNull(VD.TotalImpuesto, 0)),
'IVA'=IsNull(DT.PrecioUnitario/(Select IMP.FactorImpuesto From Impuesto IMP Where IMP.ID=DT.IDImpuesto),0),
'Preciobruto'= DT.PrecioUnitario,
'TipoPrecio'=(select Lista from vListaPrecio where ID = V.IDListaPrecio),
V.FormaPago,
V.CancelarAutomatico,
'IDSucursalFiltro'= V.IDSucursalFiltro,
'SucursalCliente'= (Select Codigo from sucursal where ID = V.IDSucursalFiltro),

--Nota de Credito
--'ComprobanteNC'= (Select Top(1) NCV.[Comp. NC] from vnotacreditoventa NCV WHere NCV.IDTransaccionVenta = V.IDTransaccion),
--'FechaNC'=(Select Top(1) NCV.Fecha from vnotacreditoventa NCV WHere NCV.IDTransaccionVenta = V.IDTransaccion),
--'ImporteNC' = (Case When (ROW_NUMBER() OVER(PARTITION BY DT.IDTransaccion ORDER BY DT.IDTransaccion ASC)) = 1 Then 
--		(Select Sum(NC.TotalDiscriminado) from vnotacredito NC join vnotacreditoventa NCV on NCV.IDTransaccion = NC.IDTransaccion WHere NCV.IDTransaccionVenta = V.IDTransaccion) Else 0 End), 
--'TipoNC' =(Select Top(1)  NCV.[Tipo NC] from vnotacreditoventa NCV WHere NCV.IDTransaccionVenta = V.IDTransaccion),


'TipoAsignacion'=isnull(PAC.Tipo, 'Lote Distribucion'),
'ComprobanteCarga'=Isnull(PAC.Comprobante, LD.Comprobante),
'IDCamion'=Isnull(PAC.IDCamion, LD.IDCamion),
'Camion'=isnull(PAC.Camion,LD.Camion),
'IDChofer'=isnull(PAC.IDChofer,LD.IDChofer),
'Chofer'=Isnull(PAC.Chofer, LD.Chofer),
'Anulado' = 'False',
'MotivoAnulacion'=''

From VDetalleVentaAgrupado DT
Join VVenta V On DT.IDTransaccion=V.IDTransaccion
Join VCliente CLI On V.IDCliente=CLI.ID
Left Outer Join Vendedor VEN On V.IDVendedor=VEN.ID
Left Outer Join Sucursal S On V.IDSucursal=S.ID
left outer join VClienteSucursal CS on CS.ID = V.IDSucursalCliente and CS.IDCliente = V.IDCliente
Right outer Join VProducto P On DT.IDProducto=P.ID
Join Deposito D On DT.IDDeposito=D.ID
Join Impuesto I On DT.IDImpuesto=I.ID
Left Outer Join Linea L on L.ID=P.IDLinea
Left Outer Join SubLinea SL on SL.ID=P.IDSubLinea
Left Outer Join SubLinea2 SL2 on SL2.ID=P.IDSubLinea2 
Left Outer Join TipoProducto TP on TP.ID=P.IDTipoProducto
Left Outer Join Marca M on M.ID=P.IDMarca
Left Outer Join Categoria C on P.IDCategoria=C.ID
Left Outer Join UnidadMedida UM on UM.ID=P.IDUnidadMedida
Left Outer Join VDetalleNotaCreditoAgrupado VD	On V.IDTransaccion=VD.IDTransaccionVenta And DT.IDProducto=VD.IDProducto
--Left Outer Join NotaCredito NC On VD.IDTransaccion=NC.IDTransaccion
--left outer join vnotacreditoventa NCV on V.IDTransaccion = NCV.IDTransaccionVenta 
left outer join vpedidoventa PV on V.IDTransaccion = PV.IDTRansaccionVenta
left outer join VPedidoAsignacionCamion PAC on PV.IDTransaccionPedido = PAC.IDTransaccionPedido
left outer join VentaLoteDistribucion VLD on V.IDTransaccion = VLD.IDTransaccionVenta
left outer join vLoteDistribucion LD on VLD.IDTransaccionLote = LD.IDTransaccion
where V.Anulado = 'False' 

union all

Select
'ID'=(ROW_NUMBER() OVER(PARTITION BY DT.IDTransaccion ORDER BY DT.IDTransaccion ASC)),
'Fecha'=CONVERT(Date,V.FechaEmision),
'TDOC'=V.TipoComprobante,
'DOCUM'=V.Comprobante,
'CODIGO'=V.ReferenciaCliente,
'RSOCIAL'=V.DireccionAlternativa,
--'SUCURS'=V.ReferenciaSucursal,
'CVENTA'=V.Condicion,
'PRODUCTO'=P.Referencia,
'DESCRIP'=P.Descripcion,
'CANT'=DT.Cantidad,
'KILOS'=ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0 ),
'TOTKLS'=(DT.Cantidad * ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0)),
'IMPORTE'=DT.Total - DT.TotalImpuesto,
'VEND'=VEN.Referencia,
'VENDNOM'=V.Vendedor,
'TCOM'=V.IDTipoCliente,--Tipo de Comercio

'IDDEPARTAMENTOCLIENTE'=CLI.IDDEPARTAMENTO,
'DEPARTAMENTOCLIENTE'=CLI.DEPARTAMENTO,
'IDCIUDADCLIENTE'=CLI.IDCIUDAD,
'CIUDADCLIENTE'=CLI.CIUDAD,
'BARRIOCLIENTE'=CLI.Barrio,
'IDZONACLIENTE'=CLI.IDZonaVenta,
'ZONANCLIENTE'=CLI.ZonaVenta,
'AREA'=CLI.ReferenciaArea,
'REGIONCLIENTE'=CLI.IDSucursal,


'IDDEPARTAMENTOENTREGA'=Isnull(CS.IDDepartamento,CLI.IDDEPARTAMENTO),
'DEPARTAMENTOENTREGA'=Isnull(CS.Departamento,CLI.DEPARTAMENTO),
'IDCIUDADENTREGA'=Isnull(CS.IDCiudad,CLI.IDCIUDAD),
'CIUDADENTREGA'=Isnull(CS.Ciudad,CLI.CIUDAD),
'BARRIOENTREGA'=Isnull(CS.Barrio, CLI.Barrio),
'IDZONAENTREGA'=Isnull(CS.IDZonaVenta,CLI.IDZonaVenta),
'ZONANENTREGA'=Isnull(CS.ZonaVenta,CLI.ZonaVenta),
'REGIONENTREGA'=Isnull(CS.ID,CLI.IDSucursal),

'RUTA'=CLI.ReferenciaRuta,
'CANAL'=V.IDListaPrecio,
'CANALNOM'=V.[Lista de Precio],
--'VEHI'='',
--'REPARTO'='',
'MES'=Month(V.FechaEmision),
'AÑO'=Year(V.FechaEmision),
'SUCURSAL'=V.Ciudad,
'Categoria'=P.TipoProducto,
'SubCategoria'=P.Linea,
'Empaque'=P.[Uni. Med.],
'Sub-Empaque'=P.Peso,
'DescTCOM'=V.TipoCliente,
'DescCanal'=V.[Lista de Precio],
'Sub-Canal'='',
'DescArea'=CLI.Area,
'Vendedor'=V.Vendedor,
'Precio neto CU'=IsNull(DT.PrecioUnitario/(Select IMP.FactorDiscriminado From Impuesto IMP Where IMP.ID=DT.IDImpuesto),0),
--'IVA'=(DT.TotalImpuesto - IsNull(VD.TotalImpuesto, 0)),
'IVA'=IsNull(DT.PrecioUnitario/(Select IMP.FactorImpuesto From Impuesto IMP Where IMP.ID=DT.IDImpuesto),0),
'Preciobruto'= DT.PrecioUnitario,
'TipoPrecio'=(select Lista from vListaPrecio where ID = V.IDListaPrecio),
V.FormaPago,
V.CancelarAutomatico,
'IDSucursalFiltro'= V.IDSucursalFiltro,
'SucursalCliente'= (Select Codigo from sucursal where ID = V.IDSucursalFiltro),

--DT.IDTransaccion


--Nota de Credito
--'ComprobanteNC'= '',
--'FechaNC'='',
--'ImporteNC' = '',
--'TipoNC' ='',
-- Asignacion a Camion 

'TipoAsignacion'=isnull(PAC.Tipo, 'Lote Distribucion'),
'ComprobanteCarga'=Isnull(PAC.Comprobante, LD.Comprobante),
'IDCamion'=Isnull(PAC.IDCamion, LD.IDCamion),
'Camion'=isnull(PAC.Camion,LD.Camion),
'IDChofer'=isnull(PAC.IDChofer,LD.IDChofer),
'Chofer'=Isnull(PAC.Chofer, LD.Chofer),
'Anulado' = 'True',
'MotivoAnulacion'=(Select MAV.Descripcion from MotivoAnulacionVenta MAV where MAV.id = V.IDMotivo)

From VDetalleVentaAgrupado DT
Join VVenta V On DT.IDTransaccion=V.IDTransaccion
Join VCliente CLI On V.IDCliente=CLI.ID
Left Outer Join Vendedor VEN On V.IDVendedor=VEN.ID
Left Outer Join Sucursal S On V.IDSucursal=S.ID
left outer join VClienteSucursal CS on CS.ID = V.IDSucursalCliente and CS.IDCliente = V.IDCliente
Right outer Join VProducto P On DT.IDProducto=P.ID
Join Deposito D On DT.IDDeposito=D.ID
Join Impuesto I On DT.IDImpuesto=I.ID
Left Outer Join Linea L on L.ID=P.IDLinea
Left Outer Join SubLinea SL on SL.ID=P.IDSubLinea
Left Outer Join SubLinea2 SL2 on SL2.ID=P.IDSubLinea2 
Left Outer Join TipoProducto TP on TP.ID=P.IDTipoProducto
Left Outer Join Marca M on M.ID=P.IDMarca
Left Outer Join Categoria C on P.IDCategoria=C.ID
Left Outer Join UnidadMedida UM on UM.ID=P.IDUnidadMedida
left outer join vpedidoventa PV on V.IDTransaccion = PV.IDTRansaccionVenta
left outer join VPedidoAsignacionCamion PAC on PV.IDTransaccionPedido = PAC.IDTransaccionPedido
left outer join VentaLoteDistribucion VLD on V.IDTransaccion = VLD.IDTransaccionVenta
left outer join vLoteDistribucion LD on VLD.IDTransaccionLote = LD.IDTransaccion
where V.Anulado = 'True' 




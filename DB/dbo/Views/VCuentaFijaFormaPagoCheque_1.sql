﻿
CREATE View [dbo].[VCuentaFijaFormaPagoCheque]
As
Select 
CE.ID,
CE.Descripcion,
CE.Orden,
CE.Diferido,

'Tipo'=(Case When (CE.Diferido)='True' Then 'DIFERIDO' Else 'A LA VISTA' End),
CE.IDMoneda,
'Moneda'=M.Referencia,

'Debe'='True',
'Haber'='False',
'Credito'=0,
'Debito'=0,

CE.IDCuentaContable,
'Cuenta'=CC.Codigo + ' ' + CC.Descripcion,
'CuentaContable'=CC.Descripcion,
'Codigo'=CC.Codigo

From CuentaFijaFormaPagoCheque CE
Join Moneda M On CE.IDMoneda=M.ID
Join CuentaContable CC On CE.IDCuentaContable=CC.ID

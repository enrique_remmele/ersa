﻿


CREATE View [dbo].[VOrdenPagoCheque]
As
Select 
OP.IDTransaccion,
OP.Numero,
'Num'=OP.Numero,
OP.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
OP.NroComprobante,
'Comprobante'=OP.NroComprobante,
'Fecha'=OP.Fecha,
'Fec'=CONVERT(varchar(50), OP.Fecha, 6),
OP.Observacion,
OP.PagoProveedor,
OP.AnticipoProveedor,
OP.EgresoRendir,
--Sucursal
OP.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
S.IDCiudad,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
OP.Total,
'TotalImporteMoneda'=OP.ImporteMoneda,
'AplicarRetencion'=IsNull(OP.AplicarRetencion, 'False'),

--Moneda
'IDMoneda'=IsNull(OP.IDMoneda, CB.IDMoneda),
'Moneda'=IsNull(M.Referencia, CB.Moneda),

--Proveedor
'IDProveedor'=IsNull(OP.IDProveedor, 0),
'Proveedor'=ISNull(P.RazonSocial, ''),
'RUC'=IsNull(P.RUC, ''),
'Referencia'=P.Referencia,
P.IDTipoProveedor,

--Cheque
C.IdBanco,
'Cheque'=Case When (C.IDTransaccion) Is Null Then 'False' Else 'True' End,
C.IDCuentaBancaria,
'BancoReferencia'=C.Referencia,
C.CuentaBancaria,
C.Banco,
C.Mon,
C.NroCheque,
'FechaCheque'=CONVERT(varchar(50), C.Fecha, 6),
'FechaChequeFiltro'=C.FechaFiltro,
'Fec Chq'=C.Fec,
C.FechaPago,
'FecPago'=C.[Fec Pago],
C.Cotizacion,
'ImporteMoneda' = case when C.Anulado = 0 THEN C.ImporteMoneda ELSE 0 END,
'Importe' = case when C.Anulado = 0 THEN C.Importe ELSE 0 END,
C.Diferido,
C.FechaVencimiento,
C.[Fec. Venc.],
C.ALaOrden,
C.Conciliado,
'ChequeAnulado'=C.Anulado,
'ChequeEstado'=C.Estado,
'IDMonedaCuentaBancaria'=CB.IDMoneda,

--Efectivo
'CotizacionEfectivo'=IsNull(OP.CotizacionEfectivo, 0),
'IDMonedaEfectivo'=IsNull(OP.IDMonedaEfectivo, 0),
'ImporteMonedaEfectivo'=IsNull(OP.ImporteMonedaEfectivo, 0),

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
OP.Anulado,
'Estado'=Case When OP.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=OP.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=OP.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=OP.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=OP.IDTransaccion),
--Impreso
OP.Impreso,

--Tipo Orden de Pago
'Tipo'=Case When (OP.PagoProveedor) = 'True' Then 'Pago Proveedor' When (OP.AnticipoProveedor)='True' Then 'Anticipo Proveedor' When(OP.EgresoRendir)='True' Then 'Egreso a Rendir'  End,
'Retencion'='',
'MontoRetencion'= (Case when OP.AplicarRetencion = 'False' then 0 else
			 (isnull(Case When (OP.IDMoneda) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) Else 
					(Case When (isnull(CB.IDMoneda,ISNULL(OP.IdMonedaDocumento,1))) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) / OP.Cotizacion Else 
					(select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) End) End,0))end),
'FechaEntrega'=EC.FechaEntrega ,--(Case when (OP.EgresoRendir)='True' then OP.Fecha else EC.FechaEntrega end),
'RetiradoPor'=(Case when (OP.EgresoRendir)='True' then 'Egreso a Rendir no registra entrega' else EC.RetiradoPor end),
OP.DiferenciaCambio,

'FechaCobroProveedor'=VCE.Fecha

From OrdenPago OP
Join Transaccion T On OP.IDTransaccion=T.ID
Join TipoComprobante TC On OP.IDTipoComprobante=TC.ID
Join Sucursal S On OP.IDSucursal=S.ID
Left Outer Join Proveedor P On OP.IDProveedor=P.ID
Left Outer Join vCheque C On OP.IDTransaccion=C.IDTransaccion
Left Outer Join VCuentaBancaria CB on C.IDCuentaBancaria =CB.ID
Left Outer Join Moneda M On OP.IDMoneda=M.ID
left outer join vEntregaChequeOP EC on EC.IdtransaccionOP = OP.IdTransaccion
left outer join VencimientoChequeEmitido VCE on VCE.IDTransaccionOP = OP.IDTransaccion






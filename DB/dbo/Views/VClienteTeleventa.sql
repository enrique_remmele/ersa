﻿CREATE View [dbo].[VClienteTeleventa]
As
select 
ID,
'NombreFantasia'=IsNull(NombreFantasia,''),
'Direccion'=IsNull(Direccion,''),
'Telefono'=IsNull(Telefono,''),
'Email'=IsNull(Email,''),
Vendedor,
LimiteCredito,
PlazoCredito,
'Condicion'=(Case When (IsNull(Contado, 'True')) = 'True' Then 'CONTADO' Else 'CREDITO' End),
'Estado'=(Select IsNull((Select V.Descripcion From EstadoCliente V Where V.ID=IDEstado), '')),
RUC,
'TipoCliente'=(Select IsNull((Select V.Descripcion From TipoCliente V Where V.ID=IDTipoCliente), '')),
'Pais'= (Select IsNull((Select Descripcion From Pais P Where P.ID=C.IDPais), '')),
'IDZona'=Isnull(C.IDZonaVenta,0),
'Zona'= (Select IsNull((Select Descripcion From ZonaVenta Z Where Z.ID=C.IDZonaVenta), '')),
'Departamento'= (Select IsNull((Select Descripcion From departamento D Where D.ID=C.IDDepartamento), '')),
'Ciudad'= (Select IsNull((Select Descripcion From Ciudad Ci Where Ci.ID=C.IDCiudad), '')),
'Barrio'= (Select IsNull((Select Descripcion From Barrio B Where B.ID=C.IDBarrio), '')),
'FechaAlta'=IsNull(FechaAlta,''),
UltimaCompra,
'Observacion'=IsNull(Observacion,''),
'Contado'=IsNull(C.Contado, 'True'),
'Credito'=IsNull(C.Credito, 'False'),
Ci,
UsuarioAlta,
C.celular,
C.SaldoCredito
from vCliente C









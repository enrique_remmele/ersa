﻿
--USE [SAIN1]
--GO

--/****** Object:  View [dbo].[VRetencionClientePendiente]    Script Date: 11/25/2014 11:26:24 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

CREATE View [dbo].[VRetencionClientePendiente]
As
Select

--Cabecera
FP.IDTransaccion,
FP.ID,
'CodigoComprobante'=TC.Codigo,
'ComprobanteRetencion'=FP.Comprobante,
FP.Fecha,
FP.Observacion,

--Totales
F.Importe,

--Cliente
'IDCliente'=C.ID,
'Cliente'=C.RazonSocial,
'RUC'=C.RUC,

--Venta
VC.IDTransaccionVenta,
'ComprobanteVenta'=V.Comprobante,
V.Total,
V.TotalImpuesto,
V.TotalDiscriminado,
V.FechaEmision

From FormaPagoDocumento FP
Join FormaPago F On FP.IDTransaccion=F.IDTransaccion And FP.ID=F.ID
Join TipoComprobante TC On FP.IDTipoComprobante=TC.ID
Join VentaCobranza VC On FP.IDTransaccion=VC.IDTransaccionCobranza
Join Venta V On VC.IDTransaccionVenta=V.IDTransaccion
Join Cliente C On V.IDCliente=C.ID

--Where FP.IDTipoComprobante=(Select Top(1) CobranzaCreditoTipoComprobanteRetencion From Configuraciones Where IDSucursal=FP.IDSucursal)
--La consula de arriba tarda mucho
Where FP.IDTipoComprobante=31
and (Select Top(1) FPR.IDTransaccion From FormaPagoDocumentoRetencion FPR Where FPR.IDTransaccion=FP.IDTransaccion And FPR.ID=FP.ID) Is Null

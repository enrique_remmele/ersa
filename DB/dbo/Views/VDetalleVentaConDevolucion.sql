﻿

CREATE View [dbo].[VDetalleVentaConDevolucion]
As

Select

DT.IDTransaccion,

--Producto
DT.IDProducto,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DT.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DT.Observacion End),
'CodigoBarra'=P.CodigoBarra,
P.IDLinea,
'Linea'=ISNULL(P.Linea, ''),
P.IDSubLinea,
'SubLinea'=ISNULL(P.SubLinea, ''),
P.IDSubLinea2,
'SubLinea2'=ISNULL(P.SubLinea2, ''),
P.IDTipoProducto,
'TipoProducto'=ISNULL(P.TipoProducto, ''),
P.IDMarca,
'Marca'=ISNULL(P.Marca, ''),
P.IDPresentacion,
P.Referencia,
DT.Observacion,
P.IDUnidadMedida,
'PesoUnitario'= ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0 ),
'Peso'= (DT.Cantidad-IsNull(VD.Cantidad,0)) * ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0),
'UnidadMedida'=P.[Uni. Med.],
P.ControlarExistencia,
P.IDProveedor,
'Proveedor'=P.Proveedor,
'ReferenciaProveedor'=P.ReferenciaProveedor ,

--Deposito
DT.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
'Cantidad'=DT.Cantidad - IsNull(VD.Cantidad, 0),
DT.PrecioUnitario,
'Pre. Uni.'=DT.PrecioUnitario,
'Total'=DT.Total - IsNull(VD.Total, 0),
'Bruto'=DT.PrecioUnitario * (DT.Cantidad-IsNull(VD.Cantidad,0)),

--Impuesto
DT.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
'TotalImpuesto'=DT.TotalImpuesto - IsNull(VD.TotalImpuesto, 0),
'TotalDiscriminado'=DT.TotalDiscriminado - IsNull(VD.TotalDiscriminado, 0),
'Exento'=I.Exento,

--Descuento
'TotalDescuento'=DT.TotalDescuento - IsNull(VD.TotalDescuento, 0),
'Descuento'=DT.TotalDescuento - IsNull(VD.TotalDescuento, 0),
DT.DescuentoUnitario,
'Desc Uni'=DT.DescuentoUnitario,
'DescuentoUnitarioDiscriminado'=IsNull(DT.DescuentoUnitarioDiscriminado, 0.00),
'TotalDescuentoDiscriminado'=IsNull((DT.DescuentoUnitarioDiscriminado*DT.Cantidad)-IsNull((VD.DescuentoUnitarioDiscriminado*VD.Cantidad),0),0.00),
DT.PorcentajeDescuento,
'Desc %'=DT.PorcentajeDescuento,
'PrecioNeto'=IsNull(DT.PrecioUnitario/(Select IMP.FactorDiscriminado From Impuesto IMP Where IMP.ID=DT.IDImpuesto),0),
'TotalPrecioNeto'=(IsNull(DT.PrecioUnitario/(Select IMP.FactorDiscriminado From Impuesto IMP Where IMP.ID=DT.IDImpuesto),0)) * (DT.Cantidad - IsNull(VD.Cantidad,0)),

--Costos
DT.CostoUnitario,
'TotalCosto'=isnull((DT.TotalCosto-IsNull(VD.TotalCosto,0)),0),
'TotalCostoImpuesto'=DT.TotalCostoImpuesto - IsNull(VD.TotalCostoImpuesto,0),
'TotalCostoDiscriminado'=DT.TotalCostoDiscriminado - IsNull(VD.TotalCostoDiscriminado,0),

--Cantidad de Caja
'Caja'=IsNull(DT.Caja, 'False'),
'CantidadCaja'=DT.CantidadCaja - IsNull(VD.CantidadCaja,0),

'Cajas'=convert (decimal (10,2) ,IsNull(((DT.Cantidad-IsNull(VD.Cantidad,0)) / P.UnidadPorCaja), 0)),
'UnidadMedidas'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),'UND'),
'UnidadPorCaja'= P.UnidadPorCaja,
--Unidad de medida
'UnidadMedidaConvertir'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),'UND'),
'UnidadConvertir'=Isnull (P.UnidadConvertir,'1'),

--Totales
'TotalBruto'= isnull((DT.TotalDescuento-IsNull(VD.TotalDescuento,0)),0)+isnull((DT.Total-IsNull(VD.Total,0)),0),
'TotalSinDescuento'=(DT.Total - IsNull(VD.Total,0)),
'TotalSinImpuesto'=(DT.Total - IsNull(VD.Total,0)) - (DT.TotalImpuesto - IsNull(VD.TotalImpuesto,0)),
'TotalNeto'=(DT.Total - IsNull(VD.Total,0)) - ((DT.TotalImpuesto - IsNull(VD.TotalImpuesto,0))),
'TotalNetoConDescuentoNeto'=(DT.TotalDiscriminado - IsNull(VD.TotalDiscriminado,0)) + (DT.TotalDescuentoDiscriminado - IsNull(VD.TotalDescuentoDiscriminado,0)),

--Utilidad
'Utilidad'=(DT.Total - IsNull(VD.Total,0)) - ((DT.TotalImpuesto - IsNull(VD.TotalImpuesto,0))) - (DT.TotalCosto - IsNull(VD.TotalCosto,0)),

--Contabilidad
'CuentaContableVenta'=P.CodigoCuentaVenta,
'CuentaContableCosto'=P.CodigoCuentaCosto,

--Venta
V.IDCliente,
V.Vendedor,
V.Cliente,
V.ReferenciaCliente,
V.TipoCliente,
V.IDTipoCliente,
V.IDListaPrecio,
'ListaPrecio'=V.[Lista de Precio],
V.IDCiudadCliente,
V.FechaEmision,
'Fecha'=V.FechaEmision,
v.Fec,
V.IDMoneda,
V.Moneda,
V.Credito,
V.IDVendedor,
V.IDZonaVenta,
V.ReferenciaSucursal,
V.ReferenciaPunto,
V.IDTipoComprobante, 
V.TipoComprobante,
V.Comprobante,
V.Condicion,
V.NroComprobante,
V.[Cod.],
V.Anulado,

'MesNumero'=MONTH(V.Fecha),
V.IDSucursal,

--Direccion Alternativa
'DireccionAlternativa'=(Case When V.EsVentaSucursal='False' Then V.Cliente Else V.Cliente + ' - ' + V.SucursalCliente End),
'IDSucursalCliente'=IsNull(V.IDSucursalCliente,0),

P.IDCategoria,
P.IDDivision,
P.IDProcedencia,

--Nota de crédito
'FechaNCR' = '',
'NroNCR' = '',
'TotalBrutoNCR' = VD.Total,
'TotalDescuentoDiscriminadoNCR' = VD.TotalDescuentoDiscriminado,
'TotalDiscriminadoNCR' = VD.TotalDiscriminado,
'TipoPrecio'=(select Lista from vListaPrecio where id = V.IDListaPrecio),

V.IDFormaPagoFactura,
V.FormaPago,
V.CancelarAutomatico,
'IDSucursalFiltro'= Isnull((Select IDSucursal from ClienteSucursal where id =  IDSucursalCliente and IDCliente = V.IDCliente),(Select IDSucursal from Cliente where ID = V.IDCliente)),
'FormaPagoFactura'=V.FormaPago,
'Tipo'= (Case When DT.IDProducto in (218,219,230) then 'Descuento' else 'Devolucion' end)


From VDetalleVentaAgrupado DT
Join VVenta V On DT.IDTransaccion=V.IDTransaccion
Left Outer Join VProducto P On DT.IDProducto=P.ID
Join Deposito D On DT.IDDeposito=D.ID
Join Impuesto I On DT.IDImpuesto=I.ID
--Left Outer Join Linea L on L.ID=P.IDLinea
--Left Outer Join SubLinea SL on SL.ID=P.IDSubLinea
--Left Outer Join SubLinea2 SL2 on SL2.ID=P.IDSubLinea2 
--Left Outer Join TipoProducto TP on TP.ID=P.IDTipoProducto
--Left Outer Join Marca M on M.ID=P.IDMarca
--Left Outer Join Categoria C on P.IDCategoria=C.ID
--Left Outer Join UnidadMedida UM on UM.ID=P.IDUnidadMedida
Left Outer Join VDetalleNotaCreditoAgrupado VD	On V.IDTransaccion=VD.IDTransaccionVenta And DT.IDProducto=VD.IDProducto
--Left Outer Join NotaCredito NC On VD.IDTransaccion=NC.IDTransaccion










﻿
CREATE View [dbo].[VEgresos]

As

--Gasto - Tipo de Comprobante
Select 

'IDOperacion'=T.IDOperacion,
'Operacion'=T.Operacion,
'CodigoOperacion'=T.CodigoOperacion,
G.IDTransaccion,
G.IDSucursal,
G.Suc,
G.Numero,

--Comprobante
G.IDTipoComprobante,
'TipoComprobante'=G.[Cod.],
'Comprobante'=G.NroComprobante,
G.NroTimbrado,
G.FechaVencimientoTimbrado,

--Proveedor
G.IDProveedor,
G.Referencia,
G.Proveedor,
G.RUC,

--Grupo
G.IDGrupo,
G.Grupo,

G.Fecha,
G.Credito,
G.Condicion,
G.FechaVencimiento,

--Moneda
G.IDMoneda,
G.Moneda,
G.Cotizacion,

G.Observacion,
G.Cuota,

--Totales
G.Total,
G.TotalImpuesto,
G.TotalDiscriminado,
G.RetencionIVA,
G.Saldo,
G.Cancelado,

--Usuario de registro
'IDUsuarioRegistro'=G.IDUsuario,
'FechaRegistro'=G.FechaTransaccion,
'UsuarioRegistro'=G.UsuarioIdentificador,

--Transaccion
'Transaccion'=T.ID,
'SucursalTransaccion'=T.Sucursal,
'CodigoSucursalTransaccion'=T.CodigoSucursal,
'TerminalTransaccion'=T.Terminal,

--Orden de Pago
'NumeroOrdenPago'=IsNull((Select Top(1) OP.Numero From VOrdenPagoEgreso OP Where OP.IDTransaccion=G.IDTransaccion), 0),
G.RRHH

From VGastoTipoComprobante G
Join VTransaccion T On G.IDTransaccion=T.ID

Union all

--Gasto - Fondo Fijo
Select 

--Debemos poner un codigo diferente ya que se mezcla con Gasto normal
'IDOperacion'=999,
'Operacion'='FONDO FIJO',
'CodigoOperacion'='F.F.',
G.IDTransaccion,
G.IDSucursal,
G.Suc,
G.Numero,

--Comprobante
G.IDTipoComprobante,
'TipoComprobante'=G.[Cod.],
G.NroComprobante,
G.NroTimbrado,
G.FechaVencimientoTimbrado,

--Proveedor
G.IDProveedor,
G.Referencia,
G.Proveedor,
G.RUC,

--Grupo
G.IDGrupo,
G.Grupo,

G.Fecha,
G.Credito,
G.Condicion,
G.FechaVencimiento,

--Moneda
G.IDMoneda,
G.Moneda,
G.Cotizacion,

G.Observacion,
G.Cuota,

--Totales
G.Total,
G.TotalImpuesto,
G.TotalDiscriminado,
G.RetencionIVA,
G.Saldo,
G.Cancelado,

--Usuario de registro
'IDUsuarioRegistro'=G.IDUsuario,
'FechaRegistro'=G.FechaTransaccion,
'UsuarioRegistro'=G.UsuarioIdentificador,

--Transaccion
'Transaccion'=T.ID,
'SucursalTransaccion'=T.Sucursal,
'CodigoSucursalTransaccion'=T.CodigoSucursal,
'TerminalTransaccion'=T.Terminal,

--Orden de Pago
'NumeroOrdenPago'=IsNull((Select Top(1) OP.Numero From VOrdenPagoEgreso OP Where OP.IDTransaccion=G.IDTransaccion), 0),
G.RRHH

From VGastoFondoFijo G
Join VTransaccion T On G.IDTransaccion=T.ID

Union all

--Compra
Select 

'IDOperacion'=T.IDOperacion,
'Operacion'=T.Operacion,
'CodigoOperacion'=T.CodigoOperacion,
G.IDTransaccion,
G.IDSucursal,
G.Suc,
G.Numero,

--Comprobante
G.IDTipoComprobante,
'TipoComprobante'=G.[Cod.],
G.NroComprobante,
G.Timbrado,
G.FechaVencimientoTimbrado,

--Proveedor
G.IDProveedor,
G.Referencia,
G.Proveedor,
G.RUC,

--Grupo
'IDGrupo'=0,
'Grupo'='---',

G.Fecha,
G.Credito,
G.Condicion,
G.FechaVencimiento,

--Moneda
G.IDMoneda,
G.Moneda,
G.Cotizacion,

G.Observacion,
'Cuota'=0,

--Totales
G.Total,
G.TotalImpuesto,
G.TotalDiscriminado,
G.RetencionIVA,
G.Saldo,
G.Cancelado,

--Usuario de registro
'IDUsuarioRegistro'=G.IDUsuario,
'FechaRegistro'=G.FechaTransaccion,
'UsuarioRegistro'=G.UsuarioIdentificador,

--Transaccion
'Transaccion'=T.ID,
'SucursalTransaccion'=T.Sucursal,
'CodigoSucursalTransaccion'=T.CodigoSucursal,
'TerminalTransaccion'=T.Terminal,

--Orden de Pago
'NumeroOrdenPago'=IsNull((Select Top(1) OP.Numero From VOrdenPagoEgreso OP Where OP.IDTransaccion=G.IDTransaccion), 0),
'RRHH' = 'False'

From VCompra G
Join VTransaccion T On G.IDTransaccion=T.ID


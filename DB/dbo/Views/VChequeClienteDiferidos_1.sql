﻿


CREATE View [dbo].[VChequeClienteDiferidos]
As

Select 
--Compos para informe cheques diferidos
'Cheque Nro'=Chq.NroCheque, 
'Banco'= Chq.Banco ,
'Cuenta Bancaria'=Chq.CuentaBancaria  ,
'Emitido Por'= Case When CHQ.Librador is Null Then  C.RazonSocial    Else chq.Librador End ,
 'Clientes'=Chq.Cliente ,
'Importes'= Chq.Importe ,
'Emision'=chq.Fec, 
'Fecha de Cobranza'= convert (varchar(50), CC.FechaEmision,5), 
'Vto.Cheque'=IsNull(CONVERT(varchar(50), Chq.FechaVencimiento, 6), '---'),
'Cond.'=v.Condicion ,
'Fact.Nro'=V.NroComprobante ,
'Fecha Emis.Fact' = CONVERT (varchar(50), v.FechaEmision,5 ),
'Plazo Diaz'= DATEDIFF(D ,V.FechaEmision,Chq.FechaVencimiento ) ,
'Sucursal'=V.Sucursal, 
CHQ.IDBanco ,

CHQ.Fecha ,
CHQ.Diferido 


From vChequeCliente CHQ
Join VSucursal S On CHQ.IDSucursal=S.ID
Join Cliente C On CHQ.IDCliente=C.ID
Join Banco B On CHQ.IDBanco=B.ID
Join ClienteCuentaBancaria CCB On CHQ.IDCuentaBancaria=CCB.ID
Join Moneda M On CHQ.IDMoneda=M.ID
join FormaPago FP on Chq.IDTransaccion  = FP.IDTransaccionCheque
join CobranzaCredito CC on FP.IDTransaccion = CC.IDTransaccion  
join VentaCobranza VC on CC.IDTransaccion = VC.IDTransaccionCobranza 
join VVenta  V on VC.IDTransaccionVenta = v.IDTransaccion
join Cliente CL on Chq.IDCliente = CL.ID 















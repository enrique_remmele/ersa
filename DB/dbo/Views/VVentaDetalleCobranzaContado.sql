﻿
CREATE View [dbo].[VVentaDetalleCobranzaContado]
As
Select
V.IDTransaccion,
V.IDSucursal,
V.Sucursal,
V.IDDeposito,
V.Comprobante,
V.[Cod.],
V.IDCliente,
V.Cliente,
V.ReferenciaCliente,
V.IDCobrador,
V.IDVendedor,
V.Referencia,
V.Fecha,
V.Fec,
V.FechaVencimiento,
V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,
V.Cobrador,
V.Vendedor,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,
VC.Importe,
V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
VC.IDTransaccionCobranza,
'Lote'=LD.Comprobante, 

--Cobranza
CC.Numero,
'FechaCobranza'=CC.Fecha,
'ComprobanteCobranza'=CC.Comprobante,


--Para asiento
V.IDMoneda,
V.IDTipoComprobante,

'CobranzaAnulada'=CC.Anulado ,
CC.IdSucursal as IdSucursalOperacion,
'FechaFactura'=V.FechaEmision,
'CotizacionCobranza'=CC.Cotizacion,
'TipoComprobanteLote'=(select Codigo from TipoComprobante where ID = CC.IDTipoComprobante),
'SucursalLote'=(Select Descripcion from Sucursal where ID = CC.IdSucursal)


From VentaCobranza VC
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join CobranzaContado CC On VC.IDTransaccionCobranza = CC.IDTransaccion
Join LoteDistribucion LD on LD.IDTransaccion=CC.IDTransaccionLote 




































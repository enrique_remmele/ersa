﻿
CREATE View [dbo].[InformeDetalleCompra]
As
Select
C.IDProveedor,
C.Proveedor,
C.Comprobante,
DC.IDTransaccion
      ,DC.IDTipoComprobante
      ,DC.IDProducto
      ,DC.ID
      ,DC.Producto
      ,DC.Descripcion
      ,DC.Referencia
      ,DC.CodigoBarra
      ,DC.Peso
      ,DC.PesoNumerico
      ,DC.IDDeposito
      ,DC.Deposito
      ,DC.Observacion
      ,DC.IDImpuesto
      ,DC.Impuesto
      ,DC.Cantidad
      ,DC.PrecioUnitario
      ,DC.ReferenciaProveedor
      ,DC.Sucursal
      ,DC.Total
      ,DC.TotalImpuesto
      ,DC.TotalDiscriminado
      ,DC.TotalDescuento
      ,DC.TotalSinDescuento
      ,DC.TotalSinImpuesto
      ,DC.Caja
      ,DC.CantidadCaja
      ,DC.Cajas
      ,DC.Fecha
      ,DC.Fec
      ,DC.FechaEntrada
      ,DC.IDSucursal
      , DC.[Pre. Uni.]
From VDetalleCompra DC
Join VCompra C On DC.IDTransaccion=C.IDTransaccion

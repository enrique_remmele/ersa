﻿


CREATE View [dbo].[VResumenCobranzaDiariaDocumentos]
As

--Sergio 13-03-2014

Select
FP.IDTransaccion,
D.TipoComprobante,
D.Comprobante,
'Fecha' = C.Fecha,
D.Importe,
D.Observacion,
'NroPlanilla'=LD.Numero
From FormaPago FP
Join CobranzaContado C On FP.IDTransaccion=C.IDTransaccion
join LoteDistribucion LD on IDTransaccionLote=LD.IDTransaccion
Join VFormaPagoDocumento D On FP.IDTransaccion=D.IDTransaccion And FP.ID=D.ID

Union All

Select
FP.IDTransaccion,
D.TipoComprobante,
D.Comprobante,
'Fecha' = C.FechaEmision,
D.Importe,
D.Observacion,
'NroPlanilla'=C.NroPlanilla
From FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join VFormaPagoDocumento D On FP.IDTransaccion=D.IDTransaccion And FP.ID=D.ID



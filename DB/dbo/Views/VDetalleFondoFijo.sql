﻿CREATE view [dbo].[VDetalleFondoFijo]
as
--DetalleFondoFijo y Gasto
Select 

--Gasto
GFF.Numero,
'Tipo'='GASTO',
--'TipoComprobante'=G.[Cod.],
'TipoComprobante'=concat(G.[Cod.],(case when G.IncluirLibro = 1 then '' else ' - N' end)),
DFF.IDSucursal,
'Sucursal'=S.Descripcion,
--DFF.IDGrupo,
G.IDGrupo,
DFF.ID,
'Grupo'=GR.Descripcion,
'NroComprobante'=G.NroComprobante,
G.Observacion,
'RazonSocial'=P.RazonSocial,

--Detalle Fondo Fijo
'IDTransaccionVale'=IsNull(DFF.IDTransaccionVale,0),
'IDTransaccionGasto'=IsNull(DFF.IDTransaccionGasto,0),
DFF.Cancelado,
DFF.IDTransaccionRendicionFondoFijo,
'Fecha'=convert(varchar(50),G.Fecha,5),
--G.Total,
(G.Total - (Isnull((select Importe from NotaCreditoProveedorCompra where idtransaccionEgreso = GFF.IDTransaccion),0))) as Total

From DetalleFondoFijo DFF
Join VGasto G on DFF.IDTransaccionGasto=G.IDTransaccion 
Join VGastoFondoFijo GFF On G.IDTransaccion=GFF.IDTransaccion
Join Sucursal S on S.ID=DFF.IDSucursal 
--Join Grupo GR on GR.ID=DFF.IDGrupo 
Join Grupo GR on GR.ID=G.IDGrupo 
Join Proveedor  P on G.IDProveedor=P.ID
where G.cancelado = 0 

 Union all
 
--Vale
Select 
--Vale
V.Numero,
'Tipo'='VALE',
'TipoComprobante'='VALE',
DFF.IDSucursal,
'Sucursal'=S.Descripcion,
DFF.IDGrupo,
DFF.ID,
'Grupo'=GR.Descripcion,
'NroComprobante'=CONVERT(Varchar(50),V.NroComprobante),
'Observacion'=V.Motivo,
'RazonSocial'=V.Nombre,

--Detalle Fondo Fijo
'IDTransaccionVale'=IsNull(DFF.IDTransaccionVale,0),
'IDTransaccionGasto'=IsNull(DFF.IDTransaccionGasto,0),
DFF.Cancelado,
DFF.IDTransaccionRendicionFondoFijo,
'Fecha'=convert(varchar(50),V.Fecha,5),
V.Total 

From VVale V 
Join DetalleFondoFijo DFF on DFF.IDTransaccionVale=V.IDTransaccion 
Join Sucursal S on S.ID=DFF.IDSucursal
Join Grupo GR on GR.ID=DFF.IDGrupo 
Where V.ARendir = 'False' And  V.Anulado = 'False'

































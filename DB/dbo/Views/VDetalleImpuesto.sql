﻿


CREATE View [dbo].[VDetalleImpuesto]
As
Select
DI.IDTransaccion,
DI.IDImpuesto,
'COD'=(Case When DI.IDImpuesto=1 Then 1 When DI.IDImpuesto=2 Then 2 Else 3 End),
'Impuesto'=I.Descripcion,
DI.Total,
DI.TotalDiscriminado,
DI.TotalImpuesto,
'TotalSinImpuesto'=DI.Total - DI.TotalImpuesto,
'TotalSinDescuento'=0.0,
DI.TotalDescuento,

--Redondeado
'TotalDiscriminadoRedondeado'=Round(DI.TotalDiscriminado,0,0),
'TotalImpuestoRedondeado'=Round(DI.TotalImpuesto, 0,0)


From DetalleImpuesto DI
Join Impuesto I On DI.IDImpuesto=I.ID


﻿CREATE view [dbo].[VExtractoMovimientoProveedorFacturaCCSaldo]

As
Select 
C.IDTransaccion, 
C.Operacion,
C.Codigo,
'Proveedor'=Concat(P.Ruc , '-', P.RazonSocial),
C.Fecha,
C.Documento,
C.[Detalle/Concepto],
'Debito'=Sum(D.Debito),
C.Credito,
'Saldo'=C.Credito-Sum(D.Debito),
C.IDMoneda,
C.Movimiento,
C.ComprobanteAsociado,
'FechaPago'=D.Fecha,
C.CodigoCC
from VExtractoMovimientoProveedorFacturaCCCredito C
left outer join VExtractoMovimientoProveedorFacturaDebito D ON C.IDTransaccion = D.ComprobanteAsociado
join Proveedor P on C.Codigo = P.ID
Group by C.IDTransaccion, 
C.Operacion,
C.Codigo,
C.Fecha,
C.Documento,
C.[Detalle/Concepto],
C.Debito,
C.Credito,
C.IDMoneda,
C.Movimiento,
C.ComprobanteAsociado,
C.CodigoCC,
D.ComprobanteAsociado,
D.Fecha,
P.Ruc,
P.RazonSocial


﻿CREATE View [dbo].[VValeGasto]
As

select v.IDTransaccion,
v.Numero,
v.Fecha,
v.Fec,
v.NroComprobante,
v.IdSucursal,
v.Sucursal,
V.Suc,
V.IdGrupo,
v.Grupo,
v.IDMoneda,
v.Moneda,
v.motivo,
v.nombre,
v.Total,
v.saldo,
v.importe,
v.Cancelado,
v.Anulado,
VFF.IDTransaccionVale,
VFF.Tipo,
vff.tipoComprobante,
vff.NroComprobante as ComprobanteGasto,
vff.Observacion,
vff.RazonSocial,
vff.fecha as FechaGasto,
vff.Cancelado as CanceladoGasto

from vvale V
left join VDetalleFondoFijo VFF
on VFF.IDTransaccionVale = V.IDTransaccion







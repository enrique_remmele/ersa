﻿
CREATE View [dbo].[VTeleventaCliente]
As
select 
C.Id,
'Cliente'=C.Referencia,
'RazonSocial'=C.RazonSocial,
'Telefono'=C.Telefono,
DV.IDProducto,
(Select Max(FechaEmision) from DetalleVenta 
Join Venta on  Venta.Idtransaccion = DetalleVenta.Idtransaccion
where IdProducto = DV.IdProducto
and IdCliente = C.Id
and FechaEmision between '01-01-2014' and '30-06-2014') UltimaCompra,
''UltimaLlamada,
''UltimaHora,
'UltimoPrecio'=(select top(1) PrecioUnitario from Venta 
join DetalleVenta on Venta.IdTransaccion = DetalleVenta.Idtransaccion
Join Cliente Cliente on Venta.IdCliente = Cliente.Id
where Venta.idcliente = C.Id
and DetalleVenta.idproducto = DV.IDProducto
order by DetalleVenta.idtransaccion desc),
'UltimaCantidad'=(select top(1) Cantidad from Venta 
join DetalleVenta on Venta.IdTransaccion = DetalleVenta.Idtransaccion
Join Cliente Cliente on Venta.IdCliente = Cliente.Id
where Venta.idcliente = C.Id
and DetalleVenta.idproducto = DV.IDProducto
order by DetalleVenta.idtransaccion desc),
'Total'=(select sum(Cantidad) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = C.Id),
'Maximo'=(select max(Cantidad) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = C.Id),
'Promedio'=(select Round(sum(Cantidad)/Count(Venta.Idtransaccion),0) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = C.Id),
'Veces'=(select count(Venta.Idtransaccion) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = C.Id),
V.IdVendedor,
'Vendedor'=Vr.Referencia,
'FechaAlta'=C.FechaAlta,
'Plazo'=C.PlazoCredito

from Venta V
left join DetalleVenta DV on V.IdTransaccion = DV.Idtransaccion
left Join Cliente C on V.IdCliente = C.Id
left Join Vendedor Vr on Vr.Id = V.IdVendedor
and V.Procesado = 1
and Anulado = 0
group by C.Id,
C.Referencia,
C.RazonSocial,
C.Telefono,
DV.IdProducto,
V.IdVendedor,
Vr.Referencia,
C.FechaAlta,
C.PlazoCredito











﻿
CREATE View [dbo].[VExtractoMovimientoProducto]

as

--Entrada 

--Compra
Select
'Movimiento'='COMPRA',
'Entrada'=Cantidad,
'Salida'=0,
IDProducto,
Referencia,
Producto,
IDDeposito,
IDSucursal,
'Fecha'=Convert(date, FechaEntrada)

From VDetalleCompra 

union all

--Ticket de Bascula
Select
'Movimiento'='TICKET DE BASCULA',
'Entrada'=PesoBascula,
'Salida'=0,
IDProducto,
'Referencia' = ReferenciaProducto,
Producto,
IDDeposito,
IDSucursal,
'Fecha'=Convert(date, Fecha)

From VTicketBascula 
Where Procesado = 'True' and NroAcuerdo > 0

union all

--NotaCreditoCliente
Select
'Movimiento'='NOTA DE CREDITO CLIENTE',
'Entrada'=IsNull(D.Cantidad, 0),
'Salida'=0,
D.IDProducto,
D.Referencia ,
D.Producto,
NC.IDDeposito,
NC.IDSucursal,
'Fecha'=Convert(date, D.Fecha)

From VProducto P
Left Outer Join VDetalleNotaCredito D On P.ID=D.IDProducto
Left Outer Join VNotaCredito NC On D.IDTransaccion=NC.IDTransaccion
Where NC.Procesado='True'

union all

--Movimiento Entrada
Select
'Movimiento'=M.Motivo,
'Entrada'=V.Cantidad,
'Salida'=0,
V.IDProducto,
V.Ref,
V.Producto,
V.IDDepositoEntrada,
D.IDSucursal,
'Fecha'=Convert(date, M.Fecha)
From VDetalleMovimiento V 
Join VMovimiento M On V.IDTransaccion=M.IDTransaccion
Join Deposito D On M.IDDepositoEntrada=D.ID
where V.Entrada='True'and V.Salida='False'

union all

--Movimiento Salida Anulada
Select
'Movimiento'='AJUSTE SALIDA ANULADA',
'Entrada'=V.Cantidad,
'Salida'=0,
V.IDProducto,
V.Ref,
V.Producto,
V.IDDepositoSalida,
D.IDSucursal,
'Fecha'=Convert(date, M.Fecha)

From VDetalleMovimiento V 
Join VMovimiento M On V.IDTransaccion=M.IDTransaccion
Join Deposito D On M.IDDepositoSalida=D.ID
where V.Entrada='False'and V.Salida='True' And M.Anulado='True' 

Union All

--Transferencia Entrada
Select
'Movimiento'=M.Motivo + ' ENT.',
'Entradas'=Cantidad,
'Salidas'=0,
DM.IDProducto,
DM.Ref,
DM.Producto,
M.IDDepositoEntrada,
D.IDSucursal,
'Fecha'=Convert(date, M.Fecha)

From VDetalleMovimiento DM 
join VMovimiento M On DM.IDTransaccion=M.IDTransaccion
Join Deposito D On M.IDDepositoEntrada=D.ID
where DM.Entrada='True'and DM.Salida='True'

union all

--Transferencia Salida Anulada
Select
'Movimiento'=M.Motivo + ' SAL. ANULADA(S)',
'Entradas'=Cantidad,
'Salidas'=0,
DM.IDProducto,
DM.Ref,
DM.Producto,
M.IDDepositoSalida,
D.IDSucursal,
'Fecha'=Convert(date, M.Fecha)

From VDetalleMovimiento DM 
join VMovimiento M On DM.IDTransaccion=M.IDTransaccion
Join Deposito D On M.IDDepositoSalida=D.ID
where DM.Entrada='True'and DM.Salida='True' And M.Anulado='True'

Union All

--Ajuste Inventario Positivo 
Select
'Movimiento'='AJUSTE POSITIVO',
'Entrada'=EDI.DiferenciaSistema,
'Salida'=0,
EDI.IDProducto,
EDI.Ref, 
EDI.Producto,
EDI.IDDeposito,
EDI.IDSucursal,
'Fecha'=Convert(date, V.Fecha)

From VControlInventario V
 join VExistenciaDepositoInventario EDI on V.IDTransaccion=EDI.IDTransaccion 
Where EDI.DiferenciaSistema > 0 And V.Procesado='True' And V.Anulado='False'

union all

--Ventas Anuladas
Select
'Movimiento'='VENTA ANULADA',
'Entrada'=Cantidad,
'Salida'=0,
IDProducto,
Referencia,
Producto,
VD.IDDeposito,
VD.IDSucursal,
'Fecha'=Convert(date, DA.Fecha)

From Venta v
join  VDetalleVenta VD on v.IDTransaccion = VD.IDTransaccion  
Join DocumentoAnulado DA On V.IDTransaccion=DA.IDTransaccion
where v.Anulado = 'True'
And V.Procesado='True'

Union all

--Ajuste Inicial Entrada Positivo
Select
'Movimiento'='AJUSTE INICIAL POSITIVO',
'Entrada'=D.Entrada,
'Salida'=0,
D.IDProducto,
D.Referencia,
D.Producto,
D.IDDeposito,
D.IDSucursal,
'Fecha'=Convert(date, A.Fecha)

From VDetalleAjusteInicial D
Join AjusteInicial A On D.IDTransaccion=A.IDTransaccion
Where A.Procesado='True'
And D.Entrada>0 

Union All

--Salida
--Venta
Select
'Movimiento'='VENTA',
'Entrada'=0,
'Salida'=DV.Cantidad,
DV.IDProducto,
DV.Referencia,
DV.Producto,
DV.IDDeposito,
DV.IDSucursal,
'Fecha'=Convert(date, DV.FechaEmision)

From VDetalleVenta DV
Join Venta V On DV.IDTransaccion=V.IDTransaccion
Where V.Procesado='True'

Union all

--Ticket de Bascula
Select
'Movimiento'='TICKET DE BASCULA',
'Entrada'=0,
'Salida'=PesoBascula,
IDProducto,
'Referencia' = ReferenciaProducto,
Producto,
IDDeposito,
IDSucursal,
'Fecha'=Convert(date, FechaAnulacion)

From VTicketBascula 
Where Procesado = 'True' and Anulado = 'True' and NroAcuerdo>0


Union all

--NotaCreditoCliente ANULADA
Select
'Movimiento'='NOTA DE CREDITO CLIENTE ANULADA',
'Entrada'=0,
'Salida'=IsNull(D.Cantidad, 0),
D.IDProducto,
D.Referencia ,
D.Producto,
D.IDDeposito,
D.IDSucursal,
'Fecha'=Convert(date, NC.FechaAnulado)

From VDetalleNotaCredito D 
Join NotaCredito NC On D.IDTransaccion=NC.IDTransaccion
Join VProducto P On D.IDProducto=P.ID
WHERE D.Anulado = 1
And NC.Procesado='True'

Union all

--NotaCreditoProveedor
Select
'Movimiento'='NOTA DE CREDITO PROVEEDOR',
'Entrada'=0,
'Salida'=Cantidad,
IDProducto,
Referencia,
Producto,
IDDeposito,
IDSucursal,
'Fecha'=Convert(date, Fecha)

From VDetalleNotaCreditoProveedor 

Union all

--Movimiento Salida
Select
'Movimiento'=M.Motivo,
'Entrada'=0,
'Salida'=DM.Cantidad,
DM.IDProducto,
DM.Ref,
DM.Producto,
M.IDDepositoSalida,
D.IDSucursal,
'Fecha'=Convert(date, M.Fecha)

From VDetalleMovimiento DM 
join VMovimiento M on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On M.IDDepositoSalida=D.ID
where DM.Entrada='False'and DM.Salida='True'

union ALL

--Movimiento Entrada Anulada
Select
'Movimiento'=M.Motivo + ' ANULADA',
'Entrada'=0,
'Salida'=V.Cantidad,
V.IDProducto,
V.Ref,
V.Producto,
V.IDDepositoEntrada,
D.IDSucursal,
'Fecha'=Convert(date, M.Fecha)
From VDetalleMovimiento V 
Join VMovimiento M On V.IDTransaccion=M.IDTransaccion
Join Deposito D On M.IDDepositoEntrada=D.ID
where V.Entrada='True'and V.Salida='False' AND V.Anulado = 1 

Union All

--Transferencia Salida
Select
'Movimiento'=M.Motivo + ' SAL.',
'Entrada'=0,
'Salida'=DM.Cantidad,
DM.IDProducto,
Dm.Ref,
DM.Producto,
M.IDDepositoSalida,
D.IDSucursal,
'Fecha'=Convert(date, M.Fecha)
From VDetalleMovimiento DM 
join VMovimiento M on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On M.IDDepositoSalida=D.ID
where DM.Entrada='True'and DM.Salida='True'
 
Union all

--Transferencia Entrada Anulada
Select
'Movimiento'=M.Motivo + ' ENT. ANULADA(S)',
'Entradas'=0,
'Salidas'=Cantidad,
DM.IDProducto,
DM.Ref,
DM.Producto,
M.IDDepositoEntrada,
D.IDSucursal,
'Fecha'=Convert(date, M.Fecha)

From VDetalleMovimiento DM 
join VMovimiento M On DM.IDTransaccion=M.IDTransaccion
Join Deposito D On M.IDDepositoEntrada=D.ID
where DM.Entrada='True'and DM.Salida='True' AND M.Anulado = 1

Union all

--Ajuste Inventario Negativo
Select
'Movimiento'='AJUSTE NEGATIVO',
'Entrada'=0,
'Salida'= EDI.DiferenciaSistema *-1,
 EDI.IDProducto,
 EDI.Referencia,
 EDI.Producto,
 EDI.IDDeposito,
 EDI.IDSucursal,
'Fecha'=Convert(date, V.Fecha)

From VControlInventario V
 join VExistenciaDepositoInventario EDI on V.IDTransaccion=EDI.IDTransaccion 
Where EDI.DiferenciaSistema < 0 And V.Procesado='True' And V.Anulado='False'

Union All

--Ajuste Inicial Entrada Negativo
Select
'Movimiento'='AJUSTE INICIAL NEGATIVO',
'Entrada'=0,
'Salida'=D.Salida ,
D.IDProducto,
D.Referencia,
D.Producto,
D.IDDeposito,
D.IDSucursal,
'Fecha'=Convert(date, A.Fecha)

From VDetalleAjusteInicial D
Join AjusteInicial A On D.IDTransaccion=A.IDTransaccion
Where A.Procesado='True'



































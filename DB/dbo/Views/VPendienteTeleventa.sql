﻿CREATE View [dbo].[VPendienteTeleventa]
As
select 
IDCliente,
Operacion, 
SucursalCliente, 
'FechaEmision'=CONVERT(date, FechaEmision, 3),
FechaVencimiento, 
Total, 
Cobrado, 
Saldo from vVenta
where saldo > 1 and Anulado = 'False' and Procesado = 'True'

Union all

--Cheque Diferidos pendientes
Select V.IDCliente,
'Operacion'= Concat('Chq.Dif. Nro: ', V.NroCheque),
'SucursalCliente'='',
'FechaEmision'=V.Fecha,
V.FechaVencimiento,
'Total'=V.ImporteMoneda,
'Cobrado'= (V.ImporteMoneda -  V.Saldo),
'Saldo'= V.Saldo
From VChequeCliente V
Join VCliente C On V.IDCliente=C.ID
Join VSucursal S On V.IDSucursal=S.ID
Where ((V.Cartera='True') or (V.Depositado = 'True' and V.Descontado = 'True')) -- Si esta descontado debe mostrar en inventario

And V.Diferido='True' and FechaVencimiento >= getdate()

Union all

Select V.IDCliente,
'Operacion'= Concat('Chq.Dif.Rech. Nro: ', V.NroCheque),
'SucursalCliente'='',
'FechaEmision'=V.Fecha,
V.FechaVencimiento,
'Total'=V.ImporteMoneda,
'Cobrado'= (V.ImporteMoneda -  V.Saldo),
'Saldo'= V.Saldo

From VChequeCliente V
Join VCliente C On V.IDCliente=C.ID
Join VSucursal S On V.IDSucursal=S.ID
Where V.Rechazado='True'
and V.IDTransaccion not in (select DPC.IDTransaccionChequeCliente from DetallePagoChequeCliente DPC
join PagoChequeCliente PCC on PCC.IDtransaccion = DPC.IdTransaccionPagoChequeCliente 
and PCC.Anulado = 0) 



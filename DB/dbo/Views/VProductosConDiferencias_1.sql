﻿CREATE View [dbo].[VProductosConDiferencias]
As

Select
ED.IDProducto,
ED.Referencia,
ED.Producto,

ED.IDDeposito,
ED.Deposito,
ED.IDSucursal,
ED.Existencia,

'Calculado'= IsNull((Select SUM(E.Entrada) From VExtractoMovimientoProducto E Where E.IDProducto=ED.IDProducto And E.IDDeposito=ED.IDDeposito),0)
			- IsNull((Select SUM(E.Salida) From VExtractoMovimientoProducto E Where E.IDProducto=ED.IDProducto And E.IDDeposito=ED.IDDeposito),0),
			

ED.Costo

From 

VExistenciaDeposito ED
Join Deposito D on ED.IDDeposito = D.ID
Join TipoDeposito TD on D.IDTipoDeposito = TD.ID
Where ControlarExistencia='True'
--and TD.Descripcion <> 'DEVOLUCION' DESCOMENTAR CUANDO SE IMPLEMENTE CIRCUITO DE DEVOLUCIONES





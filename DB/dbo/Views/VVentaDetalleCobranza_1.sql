﻿
CREATE view [dbo].[VVentaDetalleCobranza]

as 

Select
V.IDTransaccion,
V.IDSucursal,
V.Comprobante,
V.[Cod.],
V.IDCliente,
V.Cliente,
V.ReferenciaCliente,
V.Fecha ,
V.Fec ,
V.FechaVencimiento,
V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,

--Si es anulado, Importe = 0, si es necesario agregar el valor real, crear otro campo
'Importe'=(Case When(CC.Anulado) = 'False' Then VC.Importe Else 0 End),
'ImporteReal'=VC.Importe ,
V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
VC.IDTransaccionCobranza,

--Cobranza
CC.Numero,
'FechaCobranza'= CC.FechaEmision ,
'Fec. Cob.'= CC.Fec,
'TipoComprobante'= CC.TipoComprobante, --25052021 - SMC - Nuevo Campo
'T. Comp.'= CC.TipoComprobante + ' (' + CONVERT(varchar(50), CC.Numero) + ')',
'Comp. Cob.'=CC.Comprobante,
'Estado Cob'=CC.Estado,
'AnuladoCobranza'=CC.Anulado,
CC.Cobrador,
'Usuario'=CC.usuario,
'Observacion'=CC.Observacion,
CC.NroPlanilla,

--Para asiento
V.IDMoneda,
V.IDTipoComprobante

From VentaCobranza VC
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join VCobranzaCredito CC On VC.IDTransaccionCobranza = CC.IDTransaccion
where CC.AnticipoCliente = 'False'
 
union all 

Select
V.IDTransaccion,
V.IDSucursal,
V.Comprobante,
V.[Cod.],
V.IDCliente,
V.Cliente,
V.ReferenciaCliente,
V.Fecha,
V.Fec,
V.FechaVencimiento,
V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,

V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,

--Si es anulado, Importe = 0, si es necesario agregar el valor real, crear otro campo
'Importe'=(Case When(CC.Anulado) = 'False' Then VC.Importe Else 0 End),
'ImporteReal'=VC.Importe ,

V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
VC.IDTransaccionCobranza,

--Cobranza
CC.Numero,
'FechaCobranza'= CC.Fecha,
'Fec. Cob.'= CC.Fec,
'TipoComprobante'= CC.TipoComprobante, --25052021 - SMC - Nuevo Campo
'T. Comp.'= CC.TipoComprobante + ' (' + CONVERT(varchar(50), CC.Numero) + ')',
CC.Comprobante,
'Estado Cob'=CC.Estado,
'AnuladoCobranza'=CC.Anulado,
'Cobrador'='VENTANILLA',
'Usuario'=CC.usuario,
'Observacion'=CC.Observacion,
0,

--Para asiento
V.IDMoneda,
V.IDTipoComprobante



From VentaCobranza VC
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join VCobranzaContado CC On VC.IDTransaccionCobranza = CC.IDTransaccion

 
union all

Select
V.IDTransaccion,
V.IDSucursal,
V.Comprobante,
V.[Cod.],
V.IDCliente,
V.Cliente,
V.ReferenciaCliente,
V.Fecha ,
V.Fec ,
V.FechaVencimiento,
V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,

--Si es anulado, Importe = 0, si es necesario agregar el valor real, crear otro campo
'Importe'=(Case When(AP.Anulado) = 'False' Then VC.Importe Else 0 End),
'ImporteReal'=VC.Importe ,
V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
'IDTransaccionCobranza' = AP.IDTransaccionCobranza,

--Cobranza
'Numero'= AP.Numero,
'FechaCobranza'= AP.Fecha ,
'Fec. Cob.'= CC.Fec,
'TipoComprobante'= AP.TipoComprobante, --25052021 - SMC - Nuevo Campo
'T. Comp.'= AP.TipoComprobante + ' (' + CONVERT(varchar(50), AP.Numero) + ')',
'Comp. Cob.'=AP.NroComprobante,
'Estado Cob'=CC.Estado,
'AnuladoCobranza'=AP.Anulado,
CC.Cobrador,
'Usuario'=AP.usuario,
'Observacion'=AP.Observacion,
CC.NroPlanilla,

--Para asiento
V.IDMoneda,
V.IDTipoComprobante

From AnticipoVenta VC
Join vAnticipoAplicacion AP on AP.IDTransaccion = VC.IDTransaccionAnticipo
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join VCobranzaCredito CC On AP.IDTransaccionCobranza = CC.IDTransaccion
where CC.AnticipoCliente = 'True'









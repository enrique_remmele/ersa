﻿Create view  vRecepcionDocumento
as 
select 
R.IDTransaccion,
T.Operacion,
R.NombreRecibido,
R.FechaRecibido,
R.Observacion,
R.IDUsuario,
U.Usuario,
R.FechaRegistro
from RecepcionDocumento R
join vTransaccion T on R.IDTransaccion = T.ID
Join Usuario U on R.IDUsuario = U.ID

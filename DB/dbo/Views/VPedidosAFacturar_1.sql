﻿
CREATE View [dbo].[VPedidosAFacturar]
As
Select
'ZonaVenta'=Case When (C.ZonaVenta) = '' Then  '---' Else (C.ZonaVenta) End,
'IDZonaVenta'=IsNull(C.IDZonaVenta, 0),
V.FechaFacturar,
V.Vendedor,
V.IDVendedor,
V.Cliente,
'Dias'=DATEDIFF(d, GETDATE(), V.FechaFacturar),
V.IDSucursal,
FPF.Aprobacion,
V.AnuladoAprobado
From VPedido V
Join VCliente C On V.IDCliente=C.ID
Join VFormaPagoFactura FPF On V.IDFormaPagoFactura=FPF.ID
Where V.Anulado = 'False' 
And V.Facturado = 'False' 
And V.Aprobado = 'True'

--Sacar control de fecha, se hace desde el programa!
--And DATEDIFF(d, GETDATE(), V.FechaFacturar) >= 0

﻿

CREATE View [dbo].[VDetalleDescuentoCheque]
As

Select

C.IDTransaccion,

'Sel'='True',
C.CodigoTipo, 
C.Banco,
C.NroCheque, 
'NroOperacionCheque'=C.Numero,
'Comprobante'=IsNull((Select Top(1) CCDP.Comprobante From VChequeClienteDocumentosPagados CCDP Where CCDP.IDTransaccion=C.IDTransaccion), '---'),
'Importe'= C.Importe ,
C.IDCliente,
C.Cliente,
C.CuentaBancaria,
C.Estado,
DDB .IDTransaccionDescuentoCheque,
'IDTransaccionDepositoBancario' = DDB .IDTransaccionDescuentoCheque,
'Fecha'=convert(varchar(50),C.Fecha,5),
'Fec'= CONVERT(varchar(50),C.Fecha,6),
'FechaVencimiento'=convert(varchar(50),C.FechaVencimiento,5),
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario)

From DetalleDescuentoCheque  DDB
Join Transaccion T On DDB.IDTransaccionDescuentoCheque  =T.ID
Join DescuentoCheque  DB on DDB.IDTransaccionDescuentoCheque  = DB.IDTransaccion 
Join VChequeCliente  C on DDB.IDTransaccionChequeCliente  = C .IDTransaccion



﻿

CREATE View [dbo].[VMacheoFacturaTicket]

As

Select M.IDTransaccion,
M.Numero,
M.IDTipoComprobante,
M.NroComprobante,
m.IDSucursal,
m.Fecha,
m.IDMoneda,
m.Cotizacion,
m.Observacion,
m.NroAcuerdo,
m.IDTransacciongasto,
'Anulado'= (Case when M.anulado = 1 then 'True' else 'False' end),
A.Proveedor,
A.ReferenciaMoneda,
A.Precio,
A.IDProducto,
A.Producto,
'Sucursal' = S.Codigo,
'TipoComprobante' = TC.Codigo,
'Comprobante' = M.NroComprobante,
'Moneda' = Mo.Referencia,
'ComprobanteGasto' = G.Comprobante,
'FechaGasto'=G.Fecha,
'CotizacionGasto'= G.Cotizacion,
'CantidadGasto'= GP.Cantidad,
'TotalGasto'= G.Total,
--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion),
A.IDProveedor,
'FechaTransaccion'=T.Fecha,
'IDUsuarioIdentificador'=T.IDUsuario,
'UsuarioIdentificador'=(Select Identificador from Usuario where id = T.IDUsuario)
From MacheoFacturaTicket M

left outer join Transaccion T on M.IDTransaccion = T.id
Join vAcuerdo A on A.NroAcuerdo = M.Nroacuerdo
Join Sucursal S on S.ID = M.IDSucursal
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Moneda Mo on Mo.Id = M.IDMoneda
Join vGasto G on G.IDTransaccion = M.IDTransaccionGasto
Join GastoProducto GP on GP.IDTransaccionGasto = G.IDTransaccion








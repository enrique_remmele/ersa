﻿CREATE view [dbo].[VVentaTotalAnualTipoCliente]
as

--Total Enero
Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=Total,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=1 

--Total Febrero

union all
Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=Total,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=2

--Total Marzo

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=Total,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=3

--Total Abril

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=Total,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=4

--Total Mayo

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=Total,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=5

--Total Junio

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=Total,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=6

--Total Julio

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=Total,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=7

--Total Agosto

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=Total,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=8

--Total Setiembre

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=Total,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=9

--Total Octubre

Union all

Select
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=Total,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP=10

--Total Noviembre

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=Total,
'Diciembre'=0
from VVenta Where MesP=11

--Total Diciembre

Union all

Select 
IDTipoCliente,
TipoCliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=Total
from VVenta Where MesP=12





﻿CREATE View [dbo].[VDescuento] As 

Select
D.IDTransaccion,
D.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
D.ID,
D.IDDescuento,
D.IDDeposito,
'Deposito'=DEP.Descripcion,
D.IDTipoDescuento,
'Tipo'=(Case When (TD.Descripcion) Is Null Then (Case when (IDTipoDescuento) = 0 Then 'TPR' Else '---' End) Else (TD.Descripcion) End),
'T. Desc.'=(Case When (TD.Codigo) Is Null Then (Case when (IDTipoDescuento) = 0 Then 'TPR' Else '---' End) Else (TD.Codigo) End),
TD.PlanillaTactico,
D.IDActividad,
'Actividad'=ISNULL((A.Codigo), ''),

D.Porcentaje,

--Detalle
'Cantidad'=V.Cantidad,

--Totales
'Descuento'=Convert(money, D.Descuento * V.Cantidad),
'Total'=D.Descuento * V.Cantidad,
'DescuentoDiscriminado'=D.DescuentoDiscriminado * V.Cantidad,
'TotalDescuentoDiscriminado'=D.DescuentoDiscriminado * V.Cantidad,

--Unitario
'DescuentoUnitario'=D.Descuento,
'DescuentoUnitarioDiscriminado'=D.DescuentoDiscriminado,

--Venta
'Fecha'=VE.FechaEmision,
VE.FechaEmision,
VE.Comprobante,
VE.Anulado,
VE.Procesado,

--Proveedor
P.IDProveedor,

--Cliente
VE.IDCliente,
'CodigoCliente'=C.Referencia,
C.RazonSocial,
P.IDTipoProducto,
'TipoProducto'=TP.Descripcion,
VE.IDSucursal,
'CodigoSucursal'= S.Codigo,
'Sucursal' = S.Descripcion,
VE.IDMoneda,
'CodigoMoneda' = M.referencia,
'Moneda' = M.Descripcion,
'EsAfrecho'=cast(Case when V.IDProducto = 33 then 'True' else 'False' end as bit),
'TotalSoloAfrecho'=cast(Case when V.IDProducto = 33 then (D.Descuento * V.Cantidad) else 0 end as money),
'TotalSAfrecho'=cast(Case when V.IDProducto = 33 then 0 else (D.Descuento * V.Cantidad) end as money)


From Descuento D
Join DetalleVenta V On D.IDTransaccion=V.IDTransaccion And D.IDProducto=V.IDProducto And D.ID=V.ID
Join Venta VE On V.IDTransaccion=VE.IDTransaccion
Join Sucursal S on S.ID = VE.IDSucursal
Join Cliente C on C.ID = VE.IDCliente
Join Moneda M on M.ID = VE.IDMoneda
Left Outer Join TipoDescuento TD On D.IDTipoDescuento=TD.ID
Left Outer Join Actividad A On D.IDActividad=A.ID
Left Outer Join Producto P On V.IDProducto=P.ID
left Join TipoProducto TP on TP.ID = P.IDTipoProducto
Left Outer Join Deposito DEP On V.IDDeposito=DEP.ID


























﻿

CREATE view [dbo].[VGrupoUsuario]
as
Select 
GU.IDGrupo,
'ID'=GU.IDGrupo, --Necesario para que funcione el sistema
'Grupo'=G.Descripcion,
'IDUsuario'=U.ID,
U.Nombre,
U.Usuario,
'SoloVales' = Isnull(G.SoloVales,'False')
From 
GrupoUsuario GU
Join Grupo G on GU.IDGrupo=G.ID 
Join Usuario U on GU.IDUsuario=U.ID 





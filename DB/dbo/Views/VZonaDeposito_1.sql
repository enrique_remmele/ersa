﻿CREATE View [dbo].[VZonaDeposito]
As
Select
ZD.*,
'Sucursal'=S.Descripcion,
'CodigoSucursal'=S.Codigo,
'Deposito'=D.Descripcion
From ZonaDeposito ZD
Join Sucursal S On ZD.IDSucursal=S.ID
Join Deposito D On ZD.IDDeposito=D.ID

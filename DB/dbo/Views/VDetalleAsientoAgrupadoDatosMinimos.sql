﻿CREATE View [dbo].[VDetalleAsientoAgrupadoDatosMinimos]
As
Select 
DA.IDTransaccion,
DA.IDCuentaContable,
'Codigo'=(IsNull(CC.Codigo, DA.CuentaContable)),
'Descripcion'=IsNull(CC.Descripcion, '---'),
'Cuenta'=IsNull(CC.Codigo, DA.CuentaContable) + ' - ' + IsNull(CC.Descripcion, '---'),
'Alias'=IsNull(CC.Alias, '---'),
'Credito'=Sum(DA.Credito),
'Debito'=sum(DA.Debito),
A.Fecha

From DetalleAsiento DA
Join Asiento A On DA.IDTransaccion=A.IDTransaccion
Left Outer Join CuentaContable CC On DA.CuentaContable=CC.Codigo 
Join PlanCuenta PC On CC.IDPlanCuenta=PC.ID
Where PC.Titular = 'True'
group by PC.Titular, DA.IDTransaccion, DA.IDCuentaContable, 
A.IDTRansaccion, CC.Codigo, DA.CuentaContable, CC.Descripcion, CC.Descripcion,
CC.Alias,A.Fecha


















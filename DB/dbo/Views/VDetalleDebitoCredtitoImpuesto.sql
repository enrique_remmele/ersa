﻿Create View [dbo].[VDetalleDebitoCredtitoImpuesto]
As
Select 

E.IDTransaccion,
E.ID,
E.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,
'CodigoComprobante'=TC.Codigo,
E.IDSucursal,
E.Comprobante,
E.Fecha,
'Fec'=CONVERT(varchar(50), E.Fecha, 6),
'VentasCredito'=(Case When (E.VentasCredito) != 0 Then 'True' Else 'False' End),
'VentasContado'=(Case When (E.VentasContado) != 0 Then 'True' Else 'False' End),
'Gastos'=(Case When (E.Gastos) != 0 Then 'True' Else 'False' End),
'Tipo'=(Case When (E.VentasCredito) != 0 Then 'VENTA CREDITO' Else (Case When (E.VentasContado) != 0 Then 'VENTA CONTADO' Else (Case When (E.Gastos) != 0 Then 'GASTO' Else '---' End) End) End),
E.IDMoneda,
'Moneda'=M.Referencia,
E.ImporteMoneda,
E.Cotizacion,
'Importe'=E.VentasCredito + E.VentasContado + E.Gastos,
E.Observacion,
E.Depositado,
'Saldo'=(E.VentasCredito + E.VentasContado + E.Gastos) - E.Depositado,
E.Cancelado,
'Estado'=(Case When (E.Cancelado) = 'True' Then 'Cancelado' Else 'Pendiente' End)

From Efectivo E
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID




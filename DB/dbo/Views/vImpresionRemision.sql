﻿CREATE View [dbo].[vImpresionRemision]
As
Select
IDTransaccion,
Comprobante,
Cliente,
'RUC'=Isnull(RUC,''),
'DomicilioCLiente'=Isnull(DomicilioCliente,''),
'ComprobanteVentaTipo'=ISNULL(ComprobanteVentaTipo,''),
'ComprobanteVenta'=ISNULL(ComprobanteVenta,''),
'TimbradoVenta'=ISNULL(TimbradoVenta,''),
'FechaExpedicionVenta'= Isnull(CONVERT(varchar(50), FechaExpedicionVenta, 3),''),
'FechaInicio'=CONVERT(varchar(50), FechaInicio, 3),
'FechaFin'=CONVERT(varchar(50), FechaFin, 3),
'DireccionPartida'=ISNULL(DireccionPartida,''),
'CiudadPartida'=ISNULL(CiudadPartida,''),
'DepartamentoPartida'=ISNULL(DepartamentoPartida,''),
'DireccionLlegada'=isnull(DireccionLlegada,''),
'CiudadLlegada'=ISNULL(CiudadLlegada,''),
'DepartamentoLlegada'=ISNULL(DepartamentoLlegada,''),
'KilometrajeEstimado'=ISNULL(KilometrajeEstimado,0),
'Camion'=ISNULL(Camion,''),
'camionPatente'=ISNULL(CamionPatente,''),
'Chofer'=ISNULL(Chofer,''),
'ChoferDocumento'=ISNULL(ChoferDocumento,''),
'ChoferDireccion'=ISNULL(ChoferDireccion,''),
'Emision' =  Concat((Select Ciudad from VSucursal where ID = IDSucursal),' ',dbo.FFormatoDosDigitos(DatePart(dd, FechaInicio)),' de ', dbo.FMes(DatePart(MM, FechaInicio)),' de ',DatePart(yyyy, FechaInicio))

From VRemision 





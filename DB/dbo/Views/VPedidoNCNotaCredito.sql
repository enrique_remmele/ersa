﻿


CREATE View [dbo].[VPedidoNCNotaCredito]

As

Select
PNC.IDTransaccionPedido,
PNC.IDTransaccionNotaCredito,
'ComprobanteNotaCredito'=NC.Comprobante,
'NumeroPedido'=PN.Numero,
'FechaNotaCredito'=NC.Fecha


From PedidoNCNotaCredito PNC
left outer join VNotaCredito NC on PNC.IDTransaccionNotaCredito = NC.IDTransaccion
left outer join PedidoNotaCredito PN on PNC.IDTransaccionPedido = PN.IDTransaccion





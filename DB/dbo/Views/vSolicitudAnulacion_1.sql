﻿
CREATE view [dbo].[vSolicitudAnulacion]
as
select
SA.IDTransaccion,
SA.IDOperacion,
'Operacion'='VENTA',
V.Comprobante,
'Fecha'=V.Fec,
V.RUC,
V.Cliente,
SA.IDMotivoAnulacion,
'Motivo'= (Select Descripcion from MotivoAnulacionVenta where id = SA.IDMotivoAnulacion),
SA.IDUsuarioSolicitante,
'UsuarioSolicitante'=(Select Nombre from Usuario where id = SA.IDUsuarioSolicitante),
SA.FechaSolicitud,
SA.IDUsuarioAprobacion,
'UsuarioAprobacion'=(Select Nombre from Usuario where id = SA.IDUsuarioAprobacion),
SA.FechaAprobacion,
'Estado'= (Case When SA.Estado = 1 then 'APROBADO' else 
		  (Case When SA.Estado = 0 then 'RECHAZADO' else
		  'PENDIENTE'end) end)

from SolicitudAnulacion SA
join VVenta V on SA.IDTransaccion = V.IDTransaccion
--WHERE v.Anulado = 0
union all

select
SA.IDTransaccion,
SA.IDOperacion,
'Operacion'='NOTA DE CREDITO',
NC.Comprobante,
'Fecha'=NC.Fec,
NC.RUC,
NC.Cliente,
SA.IDMotivoAnulacion,
'Motivo'= (Select Descripcion from MotivoAnulacionNotaCredito where id = SA.IDMotivoAnulacion),
SA.IDUsuarioSolicitante,
'UsuarioSolicitante'=(Select Nombre from Usuario where id = SA.IDUsuarioSolicitante),
SA.FechaSolicitud,
SA.IDUsuarioAprobacion,
'UsuarioAprobacion'=(Select Nombre from Usuario where id = SA.IDUsuarioAprobacion),
SA.FechaAprobacion,
'Estado'= (Case When SA.Estado = 1 then 'APROBADO' else 
		  (Case When SA.Estado = 0 then 'RECHAZADO' else
		  'PENDIENTE'end) end)
 
from SolicitudAnulacion SA
join VNotaCredito NC on SA.IDTransaccion = NC.IDTransaccion
--WHERE NC.Anulado = 0


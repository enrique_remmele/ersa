﻿CREATE View [dbo].[VDetalleVentaDevoluciones]
As

Select

DISTINCT
--Producto
DT.IDProducto,
DT.IDTransaccion,
--DT.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DT.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DT.Observacion End),
'CodigoBarra'=P.CodigoBarra,
P.IDLinea,
'Linea'=ISNULL(L.Descripcion, ''),
P.IDSubLinea,
'SubLinea'=ISNULL(SL.Descripcion, ''),
P.IDSubLinea2,
'SubLinea2'=ISNULL(SL2.Descripcion, ''),
P.IDTipoProducto,
'TipoProducto'=ISNULL(TP.Descripcion, ''),
P.IDMarca,
'Marca'=ISNULL(M.Descripcion, ''),
P.IDPresentacion,
P.Referencia,
DT.Observacion,
P.IDUnidadMedida,
'PesoUnitario'= ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0 ),
'Peso'= DTC.Cantidad * ISNULL(convert(decimal(18,7), P.Peso) / P.UnidadPorCaja, 0),
'UnidadMedida'=UM.Referencia ,
P.ControlarExistencia,
P.IDProveedor,

--Deposito
DTC.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DTC.Cantidad,
DTC.PrecioUnitario,
'Pre. Uni.'=DTC.PrecioUnitario,
'Total'=DTC.Total,
'Bruto'=DTC.PrecioUnitario * DTC.Cantidad,

--Impuesto
DTC.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
DTC.TotalImpuesto,
DTC.TotalDiscriminado,
'Exento'=I.Exento,

--Descuento
DTC.TotalDescuento,
'Descuento'=DTC.TotalDescuento,
DTC.DescuentoUnitario,
'Desc Uni'=DTC.DescuentoUnitario,
'DescuentoUnitarioDiscriminado'=IsNull(DTC.DescuentoUnitarioDiscriminado, 0.00),
'TotalDescuentoDiscriminado'=IsNull(DTC.DescuentoUnitarioDiscriminado*DTC.Cantidad,0.00),
DTC.PorcentajeDescuento,
'Desc %'=DTC.PorcentajeDescuento,
'PrecioNeto'=IsNull(DTC.PrecioUnitario/(Select IMP.FactorDiscriminado From Impuesto IMP Where IMP.ID=DTC.IDImpuesto),0) - IsNull(DTC.DescuentoUnitarioDiscriminado,0),
'TotalPrecioNeto'=(IsNull(DTC.PrecioUnitario/(Select IMP.FactorDiscriminado From Impuesto IMP Where IMP.ID=DTC.IDImpuesto),0) - IsNull(DTC.DescuentoUnitarioDiscriminado,0)) * DTC.Cantidad,

--Costos
DTC.CostoUnitario,
'TotalCosto'=isnull(DTC.TotalCosto,0),
DTC.TotalCostoImpuesto,
DTC.TotalCostoDiscriminado,

--Cantidad de Caja
'Caja'=IsNull(DTC.Caja, 'False'),
DTC.CantidadCaja,
'Cajas'=convert (decimal (10,2) ,IsNull((DTC.Cantidad / P.UnidadPorCaja), 0)),
'UnidadMedidas'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),'UND'),
--Unidad de medida
'UnidadMedidaConvertir'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),'UND'),
'UnidadConvertir'=Isnull (P.UnidadConvertir,'1'),

--Totales
'TotalBruto'= isnull(DTC.TotalDescuento,0)+isnull(DTC.Total,0),
'TotalSinDescuento'=DTC.Total - DTC.TotalDescuento,
'TotalSinImpuesto'=DTC.Total - DTC.TotalImpuesto,
'TotalNeto'=DTC.Total - (DTC.TotalImpuesto + DTC.TotalDescuento),
'TotalNetoConDescuentoNeto'=IsNull(DTC.TotalDiscriminado, 0) + IsNull(DTC.TotalDescuentoDiscriminado, 0),

--Contabilidar
P.CuentaContableVenta,
P.CuentaContableCosto,

--Venta
V.IDCliente,
V.Cliente,
V.TipoCliente,
V.IDTipoCliente,
V.IDListaPrecio,
'ListaPrecio'=V.[Lista de Precio],
V.IDCiudadCliente,
V.FechaEmision,
'Fecha'=V.FechaEmision,
V.IDMoneda,
V.Moneda,
V.Credito,
V.IDVendedor,
V.IDZonaVenta,
V.ReferenciaSucursal,
V.ReferenciaPunto,
V.IDTipoComprobante, 
V.NroComprobante,
V.[Cod.],
V.Anulado,

'MesNumero'=MONTH(V.Fec),
V.IDSucursal

From DetalleVenta DT
Join VVenta V On DT.IDTransaccion=V.IDTransaccion
right outer Join Producto P On DT.IDProducto=P.ID
Join Deposito D On DT.IDDeposito=D.ID
Join Impuesto I On DT.IDImpuesto=I.ID
Left Outer Join Linea L on L.ID=P.IDLinea
Left Outer Join SubLinea SL on SL.ID=P.IDSubLinea
Left Outer Join SubLinea2 SL2 on SL2.ID=P.IDSubLinea2 
Left Outer Join TipoProducto TP on TP.ID=P.IDTipoProducto
Left Outer Join Marca M on M.ID=P.IDMarca
Left Outer Join UnidadMedida UM on UM.ID=P.IDUnidadMedida
Join DetalleNotaCredito DTC On DT.IDTransaccion=DTC.IDTransaccionVenta And DT.IDProducto=DTC.IDProducto

 --Where FechaEmision Between '20-3-2014' And '20-3-2014' 
 --And DT.IDProducto>0  
 --And DT.IDProducto=475  
 --and ControlarExistencia = 'True'  
 --and Anulado = 'False'  
 --And (V.IDSucursal=2) 

﻿CREATE View [dbo].[VPedidoNotaCreditoAutorizador]
As
Select
'NroOperacion'= N.Numero,
N.Numero,
'NumeroSolicitudCliente'=isnull(N.NumeroSolicitudCliente,''),
N.IDSubMotivoNotaCredito,
'MotivoNotaCredito'=SMN.Motivo,
'SubMotivoNotaCredito'=SMN.Descripcion,

--Cliente
N.IDCliente,
C.idtipocliente,
N.IDSucursal,
'Sucursal' = S.Descripcion,
'Suc'=S.Codigo,
'Cliente'=C.RazonSocial, 
'Direccion'=ISNULL(N.Direccion, ISNULL(CS.Direccion,C.Direccion)),
'Telefono'=ISNULL(N.Telefono, ISNULL(CS.Telefono,C.Telefono)), 
c.Referencia ,
C.RUC ,
C.IDEstado, 
'Estado'= (Select Isnull  ((Select E.Descripcion  From EstadoCliente E Where E .ID = C.IDEstado ), '')),
C.IDZonaVenta, 
--'ConComprobantes'=Case When (Select Top(1) IDTransaccionVenta From DetalleNotaCredito D Where D.IDTransaccion=N.IDTransaccion And D.IDTransaccion Is Not Null) Is Not Null Then 'True' Else 'False' End,
N.ConComprobantes,
'IDCiudadCliente'=C.IDCiudad, 
N.IDTransaccion,
N .IDDeposito,
'Deposito'=D.Descripcion, 
N .Fecha,
'Fec'=CONVERT(varchar(50), N.Fecha, 5),
C.Credito,
'CreditoContado'=(Case When (C.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
N .IDMoneda,
'Moneda'=M.Referencia,
'DescripcionMoneda'=M.Descripcion,  
N.Cotizacion,
N.Observacion,
N.Aplicar ,
N.Descuento ,
N.Devolucion ,
'Tipo'=(Case When (N.Descuento) = 'True' Then 'DESCUENTO' Else (Case When (N.Devolucion) = 'True' Then 'DEVOLUCION' Else '---' End) End),
N.Anulado,
N.Procesado,
'EsVentaSucursal'=ISnull(N.EsVentaSucursal,0),
'IDSucursalCliente'=isnull(N.IDSucursalCliente,0),
--'EstadoNC'=(Case When N.Procesado='False' Then 'No se proceso correctamente' Else 
--			(Case When N.Anulado='True' Then 'Anulado' Else 
--			'---' 
--			End) 
---			End),
'IDTransaccionNotaCredito' = (Case When N.ProcesadoNC=1 then (Select Top(1) IDTransaccionNotaCredito from VPedidoNCNotaCredito where idtransaccionPedido = N.IDTransaccion) else 0 end),
'NroNotaCredito' = (Case When N.ProcesadoNC=1 then (Select Top(1)ComprobanteNotaCredito from VPedidoNCNotaCredito where idtransaccionPedido = N.IDTransaccion) else '---' end),
'FechaNotaCredito' = (Case When N.ProcesadoNC=1 then (Select Top(1)FechaNotaCredito from VPedidoNCNotaCredito where idtransaccionPedido = N.IDTransaccion) else null end),
'ProcesadoNC' = (Case When N.ProcesadoNC=1 Then 'PROCESADO' Else 
			'PENDIENTE' End),
			
'EstadoNC'=(Case When N.Anulado = 1 Then 'ANULADO' else
			(Case When N.EstadoNC='False' Then 'RECHAZADO' Else 
			(Case When N.EstadoNC='True' Then 'APROBADO' Else
			'PENDIENTE' 
			End)
			End) 
			End),

N.IDusuarioAprobado,
'UsuarioAprobado'= (Select Usuario from Usuario where id = N.IDUsuarioAprobado),
--'NombreUsuarioAprobado'= (Case when isnull(DesdePedidoVenta,0) = 0 then (Select Nombre from Usuario where id = N.IDUsuarioAprobado) else 'PreAprobado' end),
'NombreUsuarioAprobado'= Isnull((Select Nombre from Usuario where id = N.IDUsuarioAprobado),''),
N.ObservacionAutorizador,
N.FechaAprobado,

--Totales
N.Total,
N.TotalImpuesto,
N.TotalDiscriminado,
N.Saldo,
'Aplicado'=N.Total - N.Saldo ,

--Transaccion 
'FechaTransaccion'=T.Fecha, 
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'NombreUsuario'=(Select U.Nombre From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion),

--Mes y Año
'DiaP'=dbo.FFormatoDosDigitos(DatePart(dd,N.Fecha )),
'MesP'=dbo.FMes(DatePart(MM,N.Fecha )),
'Año'= Convert (Varchar (50),YEAR(N.Fecha)),
'IDTipoProducto' = IsNull((Select top(1) DV.IDTipoProducto from vDetalleVenta DV join NotaCreditoVenta ncv on dv.IDTransaccion = ncv.IDTransaccionVentaGenerada  where ncv.IDTransaccionNotaCredito= N.IDTransaccion),0),
'DesdePedidoVenta'=IsNull(N.DesdePedidoVenta,0),
'TotalDescuentoComercial' = (select dbo.FMontoDescontadoComercialPorPedidoNC(N.IDTransaccion)),
'TotalVenta'=(select dbo.FMontoVentasAsociadasPedidoNC(N.IDTransaccion)),
'Porcentaje'= ((select dbo.FMontoDescontadoComercialPorPedidoNC(N.IDTransaccion))/(select dbo.FMontoVentasAsociadasPedidoNC(N.IDTransaccion)))*100,
'AnuladoAprobado'= ISNULL(N.AnuladoAprobado,'False'),
'TotalDescuentoPrevio'=(Case when ISnull(N.ProcesadoNC,0) = 1 then 0 else (Select dbo.FTotalDescuentoPrevio(N.IDTransaccion)) end)

From PedidoNotaCredito  N
Join Transaccion T On N.IDTransaccion=T.ID
Left Outer Join Cliente C On N.IDCliente=C.ID
Left Outer Join Sucursal S On N.IDSucursal=S.ID
Left Outer Join Deposito D On N.IDDeposito=D.ID
Left Outer Join Moneda M On N.IDMoneda=M.ID
left outer join SubMotivoNotaCredito SMN On N.IDSubMotivoNotaCredito = SMN.ID
Left join ClienteSucursal CS on N.IDSucursalCliente = CS.ID AND n.IDCliente = cs.IDCliente

union all

Select
'NroOperacion'= N.Numero,
N.Numero,
'NumeroSolicitudCliente'=isnull(N.NumeroSolicitudCliente,''),
0,
'MotivoNotaCredito'='DEVOLUCION',
'SubMotivoNotaCredito'='DEVOLUCION SIN NOTA DE CREDITO',

--Cliente
N.IDCliente,
C.idtipocliente,
N.IDSucursal,
'Sucursal' = S.Descripcion,
'Suc'=S.Codigo,
'Cliente'=C.RazonSocial, 
'Direccion'=C.Direccion,
'Telefono'=C.Telefono, 
c.Referencia ,
C.RUC ,
C.IDEstado, 
'Estado'= (Select Isnull  ((Select E.Descripcion  From EstadoCliente E Where E .ID = C.IDEstado ), '')),
C.IDZonaVenta, 
--'ConComprobantes'=Case When (Select Top(1) IDTransaccionVenta From DetalleNotaCredito D Where D.IDTransaccion=N.IDTransaccion And D.IDTransaccion Is Not Null) Is Not Null Then 'True' Else 'False' End,
0,
'IDCiudadCliente'=C.IDCiudad, 
N.IDTransaccion,
N .IDDeposito,
'Deposito'=D.Descripcion, 
N .Fecha,
'Fec'=CONVERT(varchar(50), N.Fecha, 5),
C.Credito,
'CreditoContado'=(Case When (C.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
1,
'Moneda'=(Select M.Referencia from Moneda M where ID = 1),
'DescripcionMoneda'=(Select M.Descripcion from Moneda M where ID = 1),
1,
N.Observacion,
0,
0,
0,
'Tipo'='DEVOLUCION SIN NOTA DE CREDITO',
N.Anulado,
'Procesado'=(Case when N.IDTransaccionMovimientoStock > 0 then 'True' else 'False' end),
'EsVentaSucursal'=0,
'IDSucursalCliente'=0,
--'EstadoNC'=(Case When N.Procesado='False' Then 'No se proceso correctamente' Else 
--			(Case When N.Anulado='True' Then 'Anulado' Else 
--			'---' 
--			End) 
---			End),
'IDTransaccionNotaCredito' = N.IDTransaccionMovimientoStock,
'NroNotaCredito' = '',
'FechaNotaCredito' = null,
'ProcesadoNC' = (Case when N.IDTransaccionMovimientoStock > 0 Then 'PROCESADO' Else 
			'PENDIENTE' End),
			
'EstadoNC'=(Case When N.Anulado = 1 Then 'ANULADO' else
			(Case When N.Aprobado='False' Then 'RECHAZADO' Else 
			(Case When N.Aprobado='True' Then 'APROBADO' Else
			'PENDIENTE' 
			End)
			End) 
			End),

N.IDusuarioAprobado,
'UsuarioAprobado'= (Select Usuario from Usuario where id = N.IDUsuarioAprobado),
--'NombreUsuarioAprobado'= (Case when isnull(DesdePedidoVenta,0) = 0 then (Select Nombre from Usuario where id = N.IDUsuarioAprobado) else 'PreAprobado' end),
'NombreUsuarioAprobado'= Isnull((Select Nombre from Usuario where id = N.IDUsuarioAprobado),''),
N.ObservacionAutorizador,
N.FechaAprobado,

--Totales
N.Total,
0,
0,
0,
'Aplicado'=0,

--Transaccion 
'FechaTransaccion'=T.Fecha, 
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'NombreUsuario'=(Select U.Nombre From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=N.IDUsuarioAnulado,
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario from Usuario U Where U.ID = N.IDUsuarioAnulado), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador from Usuario U Where U.ID = N.IDUsuarioAnulado), '---')),
'FechaAnulacion'=N.FechaAnulado,

--Mes y Año
'DiaP'=dbo.FFormatoDosDigitos(DatePart(dd,N.Fecha )),
'MesP'=dbo.FMes(DatePart(MM,N.Fecha )),
'Año'= Convert (Varchar (50),YEAR(N.Fecha)),
'IDTipoProducto' = 0,
'DesdePedidoVenta'=0,
'TotalDescuentoComercial' = 0,
'TotalVenta'=0,
'Porcentaje'= 0,
'AnuladoAprobado'= 'False',
'TotalDescuentoPrevio'=0

From DevolucionSinNotaCredito  N
Join Transaccion T On N.IDTransaccion=T.ID
Left Outer Join Cliente C On N.IDCliente=C.ID
Left Outer Join Sucursal S On N.IDSucursal=S.ID
Left Outer Join Deposito D On N.IDDeposito=D.ID





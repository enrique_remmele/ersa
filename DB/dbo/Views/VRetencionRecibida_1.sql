﻿
CREATE View [dbo].[VRetencionRecibida]
As
Select 
FPD.IDTransaccion,
FPD.ID,

--Venta
V.IDSucursal,
V.IDCliente,
C.RUC,
'Cliente'=C.RazonSocial,
'Fecha'=V.FechaEmision,
'Venta'=V.Comprobante,
'TotalImponible'=Round(IsNull((FPDR.TotalImpuesto * FPD.Cotizacion), (V.TotalImpuesto * FPD.Cotizacion)),0),

--Retencion
'FechaRetencion'=FPD.Fecha,
'ComprobanteRetencion'=FPD.Comprobante,
'MontoRetencion'=round(FPDR.Importe * FPD.Cotizacion,0),
FPD.Observacion,
FPD.Cotizacion

From FormaPagoDocumento FPD
Join FormaPagoDocumentoRetencion FPDR On FPD.IDTransaccion=FPDR.IDTransaccion And FPD.ID=FPDR.ID
Join Venta V On FPDR.IDTransaccionVenta=V.IDTransaccion
Join Cliente C On V.IDCliente=C.ID



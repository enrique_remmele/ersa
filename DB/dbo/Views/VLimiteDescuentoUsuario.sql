﻿CREATE View [dbo].[VLimiteDescuentoUsuario]
As

Select 
LDU.IDListaPrecio,
'ListaPrecio'=LP.Lista,
LDU.IDUsuario,
'NombreUsuario'=U.Nombre,
U.Usuario,
LDU.Porcentaje

From LimiteDescuentoUsuario LDU
left outer join Usuario U on LDU.IDUsuario = U.ID
left outer join VListaPrecio LP on LDU.IDListaPrecio = LP.ID






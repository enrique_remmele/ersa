﻿-- View

CREATE View [dbo].[VDetalleCierreReaperturaStock]
As
Select
CA.Numero,
CA.Fecha,
CA.Procesado,
CA.Observacion,
CA.IDTransaccion, 
DCA.IDProducto,
P.Referencia,
p.CodigoBarra,
'Producto'=P.Descripcion,
DCA.Existencia,
DCA.Costo,
DCA.Total,
DCA.IDDeposito,
DCA.IDSucursal,
CA.Anulado

From DetalleCierreAperturaStock DCA
Join CierreAperturaStock CA On DCA.IDTransaccion = CA.IDTransaccion
Join Producto P On DCA.IDProducto = P.ID



﻿CREATE View [dbo].[VFormaPagoTarjeta]
As
Select 
FPT.IDTransaccion,
FPT.ID,
FPT.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,
'CodigoComprobante'=TC.Codigo,
'ComprobanteLote'=0,
TC.IDOperacion,
FPT.IDSucursal,
FPT.Comprobante,
FPT.Fecha,
'Fec'=CONVERT(varchar(50), FPT.Fecha, 6),
FPT.IDMoneda,
'Moneda'=M.Referencia,
FPT.ImporteMoneda,
FPT.Cotizacion,
FPT.Total,
'Saldo'=ISNULL(FPT.Saldo,0),
'Importe'=FPT.Saldo,
--'Importe'=FPT.Importe,
'Observacion'=concat('Boleta = ',FPT.Boleta,FPT.Observacion),
'Cancelado'=ISNULL(FPT.Cancelado,0),
'Deposito'=ISNULL(TC.Deposito,'False'),
TC.Restar,
T.IDUsuario,
'SucuCobranza'=(select Sucursal from vCobranzaCredito where IDTransaccion = FPT.IDTransaccion),
'NroCobranza'=(select Numero from vCobranzaCredito where IDTransaccion = FPT.IDTransaccion),
'TipoTarjeta'=isnull((select descripcion from TipoTarjeta where id = FPT.IdTipoTarjeta),'')

From FormaPagoTarjeta FPT
Join TipoComprobante TC On FPT.IDTipoComprobante=TC.ID
Join Moneda M On FPT.IDMoneda=M.ID
join Transaccion T on T.Id = FPT.IDTransaccion




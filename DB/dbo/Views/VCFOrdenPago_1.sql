﻿


CREATE View [dbo].[VCFOrdenPago]
As
Select
V.*,
'BuscarEncuentaBancaria'=IsNull(CFV.BuscarEncuentaBancaria, 'False'),
'BuscarEnProveedores'=IsNull(CFV.BuscarEnProveedores, 'False'),
'Proveedor'=IsNull(CFV.Proveedor, 'False'),
'Efectivo'=IsNull(CFV.Efectivo, 'False'),
'Cheque'=IsNull(CFV.Cheque, 'False'),
'Retencion'=IsNull(CFV.Retencion, 'False'),
'DiferenciaCambio'=IsNull(CFV.DiferenciaCambio, 'False'),
'Documento'=IsNull(CFV.Documento, 'False'),
'Tipo'=(Case 
		When (CFV.Proveedor) = 'True' Then 
		'PROVEEDOR' 
		Else 
		(Case When (CFV.Cheque) = 'True' Then 
		'CHEQUE' 
		Else 
		(Case When (CFV.ChequeDiferido) = 'True' Then 
		'CHEQUE DIFERIDO' 
		Else
		(Case When (CFV.EFECTIVO) = 'True' Then 
		'EFECTIVO' 
		Else 
		(Case When (CFV.Retencion) = 'True' Then 
		'RETENCION' 
		Else 
		(Case When (CFV.DiferenciaCambio) = 'True' Then 
		'DIF. CAMBIO' 
		Else 
		(Case When (CFV.Documento) = 'True' Then 
		'DOCUMENTO' 
		Else '---' 
		End) 
		End)
		End) 
		End) 
		End) 
		End) 
		End),
'ChequeDiferido'=IsNull(CFV.ChequeDiferido, 'False'),
'SoloDiferido'=IsNull(CFV.SoloDiferido, 'False')

From VCF V
Join CFOrdenPago CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID



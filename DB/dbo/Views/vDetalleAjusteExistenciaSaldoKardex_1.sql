﻿
CREATE View [dbo].[vDetalleAjusteExistenciaSaldoKardex]
as 
select 
DK.IDTransaccion,
DK.IDProducto,
P.Referencia,
P.Descripcion,
'Producto'=P.Descripcion,
DK.ExistenciaValorizada,
DK.ExistenciaKardexAnterior,
DK.CantidadAjuste,
DK.Costo
from
DetalleAjusteExistenciaSaldoKardex DK
Join Producto P on DK.IDProducto = P.ID
	



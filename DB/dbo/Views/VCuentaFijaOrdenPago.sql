﻿
CREATE View [dbo].[VCuentaFijaOrdenPago]
AS
Select 
C.ID,

--Cuenta Contable
C.IDCuentaContable,
'Cuenta'=CC.Descripcion,
'Codigo'=CC.Codigo,
'CuentaContable'=CC.Codigo + ' - ' + CC.Descripcion,

C.Debe,
C.Haber,
'DebeHaber'=(Case When (C.Debe) = 'True' Then 'DEBE' Else (Case When (C.Haber) = 'True' Then 'HABER' Else '---' End) End),	

--Tipo de Comprobante
C.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,

--Moneda
C.IDMoneda,
'Moneda'=M.Descripcion,

--Tipo de Cuenta
'Tipo'=(Case When (C.TipoProveedor)='True' Then 'PROVEEDOR' Else (Case When (C.TipoCheque)='True' Then 'CHEQUE' Else (Case When (C.TipoEfectivo)='True' Then 'EFECTIVO' Else '---' End) End) End),
C.TipoProveedor,
C.TipoCheque,
C.TipoEfectivo,

C.Orden,
C.Descripcion,
C.BuscarProveedor,
C.BuscarCuentaBancaria

From CuentaFijaOrdenPago C
Join CuentaContable CC on C.IDCuentaContable=CC.ID
Join TipoComprobante TC On C.IDTipoComprobante=TC.ID
Join Moneda M On C.IDMoneda=M.ID


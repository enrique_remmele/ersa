﻿

CREATE view [dbo].[VC_cast_v_medios_pago]
as 
select 
'cod_medio_pago'=ID,
Codigo,
Descripcion
from vTipoComprobante TC
where TC.Operacion in ('PAGOS Y COBROS EN EFECTIVO', 'PAGOS Y COBROS CON DOCUMENTOS', 'PAGOS Y COBROS CON TARJETA')
and Estado = 1


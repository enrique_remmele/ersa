﻿


CREATE View [dbo].[VVendedor]
As
Select 
V.*,

--Sucursal
'Sucursal'=S.Descripcion,
'CodigoSucursal'=S.Codigo,

--Deposito
'Deposito'=ISNULL(D.Descripcion, '---')

From Vendedor V 
Join Sucursal S on V.IDSucursal=S.ID
LEFT OUTER JOIN Deposito D ON V.IDDeposito=D.ID





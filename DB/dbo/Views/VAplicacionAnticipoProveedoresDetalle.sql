﻿


CREATE View [dbo].[VAplicacionAnticipoProveedoresDetalle]

As

Select 
AP.IDTransaccion,
AP.IDTransaccionOrdenPago,
AP.IDTransaccionProveedores,
AP.Fecha , 
AP.TotalEgreso,
AP.TotalOrdenPago,
'NumOP'=OP.Numero,
'IDTipoComprobanteOP'=OP.IDTipoComprobante,
'DescripcionTipoComprobanteOP'=TC.Descripcion,
'TipoComprobanteOP'=TC.Codigo,
'Cod.OP'=TC.Codigo,
OP.NroComprobante,
'ComprobanteOP'=OP.NroComprobante,
OP.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
S.IDCiudad,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
'TotalOP'=OP.Total,
--'IDMoneda'= AP.IDMoneda,
--'Moneda'=M2.Referencia,

--Moneda de Comprobantes
--'IDMonedaComprobante'=OP.IDMoneda,
--'MonedaComprobante'=M2.Referencia,

--Proveedor
'IDProveedor'=IsNull(OP.IDProveedor, 0),
'Proveedor'=ISNull(P.RazonSocial, ''),
'RUC'=IsNull(P.RUC, ''),
'Referencia'=P.Referencia,
P.IDTipoProveedor,


--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'EstadoOP'=Case When OP.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacionOP'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=OP.IDTransaccion), 0)),
'UsuarioAnulacionOP'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=OP.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacionOP'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=OP.IDTransaccion), '---')),
'FechaAnulacionOP'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=OP.IDTransaccion),
--Impreso
OP.Impreso,

--Tipo Orden de Pago
'Tipo'=Case When (OP.PagoProveedor) = 'True' Then 'Pago Proveedor' When (OP.AnticipoProveedor)='True' Then 'Anticipo Proveedor' When(OP.EgresoRendir)='True' Then 'Egreso a Rendir'  End,
'Retencion'=''


From AplicacionAnticipoProveedoresDetalle AP
join vOrdenPago OP on AP.IDTransaccionOrdenPago = op.IDTransaccion
join VCompraGasto CG on AP.IDTransaccionProveedores = CG.IDTransaccion
Join Transaccion T On AP.IDTransaccion=T.ID
Join TipoComprobante TC On OP.IDTipoComprobante=TC.ID
Join Sucursal S On OP.IDSucursal=S.ID
Left Outer Join Proveedor P On OP.IDProveedor=P.ID


















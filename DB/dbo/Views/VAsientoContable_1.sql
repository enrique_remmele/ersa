﻿


CREATE view [dbo].[VAsientoContable]
as
--Venta
Select 
V.IDTransaccion,
V.Fecha,
'Operacion'= [Cod.] + '  ' +CONVERT(varchar(50),V.NroComprobante)+'  '+Condicion,
V.IDSucursal 
From
VVenta V
Join VAsiento A on V.IDTransaccion=A.IDTransaccion

union all

--NotaCreditoVenta
Select
NCV.IDTransaccion,
NCV.Fecha,
'Operacion'= 'NCC' + '  ' +CONVERT(varchar(50),V.NroComprobante)+'  '+Condicion,
V.IDSucursal
From VNotaCreditoVenta NCV
Join VVenta V on NCV.IDTransaccionVenta=V.IDTransaccion 
Join VAsiento A on A.IDTransaccion =NCV.IDTransaccion

union all

--NotaDebitoVenta
Select
NDV.IDTransaccion,
NDV.Fecha,
'Operacion'= 'NDC' + '  ' +CONVERT(varchar(50),V.NroComprobante)+'  '+Condicion,
V.IDSucursal
From VNotaDebitoVenta NDV
Join VVenta V on NDV.IDTransaccionVenta=V.IDTransaccion
Join VAsiento A on A.IDTransaccion =NDV.IDTransaccion

union all

--Compra
Select
C.IDTransaccion,
C.Fecha,
'Operacion'= [Cod.] + '  ' +CONVERT(varchar(50),C.NroComprobante)+'  '+Condicion,
C.IDSucursal
From VCompra C
Join VAsiento A on A.IDTransaccion =C.IDTransaccion

union all

--NotaCreditoCompra
Select
NC.IDTransaccion,
NC.Fecha,
'Operacion'= 'NCP' + '  ' +CONVERT(varchar(50),C.NroComprobante)+'  '+C.Condicion,
NC.IDSucursal
From VNotaCredito NC
Join VCompra C on NC.IDTransaccion=C.IDTransaccion
Join VAsiento A on A.IDTransaccion =NC.IDTransaccion

union all

--NotaDebitoCompra
Select
ND.IDTransaccion,
ND.Fecha,
'Operacion'= 'NDP' + '  ' +CONVERT(varchar(50),C.NroComprobante)+'  '+C.Condicion,
ND.IDSucursal
From VNotaDebito ND
Join VCompra C on ND.IDTransaccion=C.IDTransaccion
Join VAsiento A on A.IDTransaccion =ND.IDTransaccion

Union all

--Movimiento
Select 
M.IDTransaccion,
M.Fecha,
'Operacion'= [Cod.] + '  ' +CONVERT(varchar(50),M.Numero),
M.IDSucursal
From VMovimiento M
Join VAsiento A on M.IDTransaccion=A.IDTransaccion

--Ajuste 'FALTA'


union all

--Cobranza Contado
Select
CCO.IDTransaccion,
CCO.Fecha,
'Operacion'=[Cod.]+ convert(varchar(50),CCO.NroComprobante),
CCO.IDSucursal
From VCobranzaContado CCO
Join VAsiento A on CCO.IDTransaccion=A.IDTransaccion


union all

--Cobranza Crédito
Select
CCR.IDTransaccion,
FechaEmision,
'Operacion'=[Cod.]+'  '+convert(varchar(50),CCR.NroComprobante),
CCR.IDSucursal
From VCobranzaCredito CCR
Join VAsiento A on CCR.IDTransaccion=A.IDTransaccion

union all

--Cheque Rechazado
Select
CCR.IDTRansaccion,
CCR.Fecha,
'Operacion'=CONVERT(varchar(50),CCR.Numero),
CCR.IDSucursal
From VChequeClienteRechazado CCR
Join VAsiento A on CCR.IDTRansaccion=A.IDTransaccion

union all

--Descuento Cheque
Select
DC.IDTransaccion,
DC.Fecha,
'Operacion'=[Cod.]+'  '+CONVERT(varchar(50),DC.NroComprobante),
DC.IDSucursal
From VDescuentoCheque DC
Join VAsiento A on DC.IDTransaccion=A.IDTransaccion

union all

--Depósito Bancario
Select
DB.IDTransaccion,
DB.Fecha,
'Operacion'=[Cod.]+' '+CONVERT(varchar(50),DB.NroComprobante),
DB.IDSucursal
From VDepositoBancario DB
Join VAsiento A on DB.IDTransaccion=A.IDTransaccion

union all

--Orden Pago
Select
OP.IDTransaccion,
OP.Fecha,
'Operacion'=[Cod.]+'  '+CONVERT(varchar(50),OP.NroComprobante),
OP.IDSucursal
From VOrdenPago OP
Join VAsiento A on OP.IDTransaccion=A.IDTransaccion

union all

--Gastos
Select
G.IDTransaccion,
G.Fecha,
'Operacion'=[Cod.]+'  '+CONVERT(varchar(50),G.NroComprobante),
G.IDSucursal
From VGasto G
Join VAsiento A on G.IDTransaccion=A.IDTransaccion

Union All

--Asientos Anteriores
Select 
V.IDTransaccion,
V.Fecha,
'Operacion'= '* ' + Operacion,
V.IDSucursal 
From
VAsientoImportado V
Join VAsiento A on V.IDTransaccion=A.IDTransaccion

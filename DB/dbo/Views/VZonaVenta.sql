﻿
CREATE View [dbo].[VZonaVenta] As 

Select ZV.*, 
S.Descripcion as Sucursal, 
A.Descripcion as Area
From ZonaVenta ZV
join Sucursal S on S.Id = ZV.IDSucursal
join Area A on A.Id = ZV.IDArea


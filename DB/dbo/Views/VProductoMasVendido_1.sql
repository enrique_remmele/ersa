﻿
CREATE View [dbo].[VProductoMasVendido]
as
Select
Producto,
Cantidad = sum(Cantidad),
FechaEmision
From VDetalleVenta
Group By Producto,FechaEmision


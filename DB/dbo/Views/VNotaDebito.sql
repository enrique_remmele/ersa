﻿



CREATE View [dbo].[VNotaDebito]
As
Select

--Punto de Expedicion
N.IDPuntoExpedicion,
'PE'=PE.Descripcion,
PE.Ciudad,
PE.IDCiudad,
PE.IDSucursal,
PE.Sucursal,
PE.ReferenciaSucursal,
PE.ReferenciaPunto,

--Cliente
N .IDCliente,
'Cliente'=C.RazonSocial,
C.IDTipoCliente ,
C.Direccion,
C.Telefono ,
c.Referencia ,
C.RUC, 
C.IDEstado,
'Estado'= (Select Isnull((Select E.Descripcion From EstadoCliente E Where E.ID=1 ), ''  ) ) , 
C.IDZonaVenta,

N.IDTransaccion,
N.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
N.NroComprobante,
'Comprobante'=PE.ReferenciaSucursal + '-' + PE.ReferenciaPunto + '-' + CAST(N.NroComprobante AS VARCHAR(10)),


N .IDDeposito,
'Deposito'=D.Descripcion,

'Suc'=S.Codigo,

N .Fecha,
'Fec'=CONVERT(varchar(50), N .Fecha, 6),
C.Credito,
'CreditoContado'=(Case When (C.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
N .IDMoneda,
'Moneda'=M.Referencia,
'DescripcionMoneda'=M.Descripcion,
N .Cotizacion,

N .Observacion,
N.Aplicar ,
N.Reposicion ,
N.DeBito ,


--Totales
N.Total,
N.TotalImpuesto,
N.TotalDiscriminado,
N.Saldo,
N.Anulado,

N.Procesado,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=N.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=N.IDTransaccion)


From NotaDebito  N
Join Transaccion T On N.IDTransaccion=T.ID
Left Outer Join VPuntoExpedicion PE On N.IDPuntoExpedicion=PE.ID
Left Outer Join TipoComprobante TC On N.IDTipoComprobante=TC.ID
Left Outer Join Cliente C On N.IDCliente=C.ID
Left Outer Join Sucursal S On N.IDSucursal=S.ID
Left Outer Join Deposito D On N.IDDeposito=D.ID
Left Outer Join Moneda M On N.IDMoneda=M.ID

























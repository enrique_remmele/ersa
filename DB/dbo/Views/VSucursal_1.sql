﻿


CREATE View [dbo].[VSucursal]
As
Select 
S.ID, 
S.Descripcion, 
S.Codigo,
S.IDPais, 
'Pais'=ISNULL(P.Descripcion, ''),
S.IDDepartamento, 
'Departamento'=ISNULL(D.Descripcion, ''),
S.IDCiudad, 
'Ciudad'=ISNULL(C.Descripcion, ''),
'CodigoCiudad'=C.Codigo,
S.Direccion, 
S.Telefono, 
S.Referencia, 
'REF'=S.Referencia, 
S.Estado,
S.CodigoDistribuidor,
S.CuentaContable
  
From 
Sucursal S
Left Outer Join Pais P On S.IDPais=P.ID
Left Outer Join Departamento D On S.IDDepartamento=D.ID
Left Outer Join Ciudad C On S.IDCiudad=C.ID








﻿Create view vGraficoContable
as 
select 
GC.ID,
GC.IDUsuario,
U.Nombre,
GC.Titulo,
'Grafico'=Concat(GC.Titulo, ' - ', U.Nombre)

From GraficoContable GC
join Usuario U on GC.IDUsuario = U.ID

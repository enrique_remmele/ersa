﻿create View [dbo].[VCFConsumoCombustible]
As
Select
V.*,
CFV.IDTipoOperacion,
CFV.BuscarEnDeposito,
CFV.BuscarEnProducto,

--Tipo Cuenta Fija
CFV.IDTipoCuentaFija,
'TipoCuentaFija'=TC.Tipo,
'CuentaFija'=TC.Descripcion,
TC.IDImpuesto,

--Tipo de Movimiento
'Tipo Operacion'=TP.Descripcion,

--Tipo de Cuenta
TC.Campo,
TC.IncluirDescuento,
TC.IncluirImpuesto

From VCF V
Join CFConsumoCombustible CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join TipoOperacion TP On CFV.IDTipoOperacion=TP.ID
Join VTipoCuentaFija TC On CFV.IDTipoCuentaFija=TC.ID

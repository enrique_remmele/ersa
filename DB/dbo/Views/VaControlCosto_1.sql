﻿Create view VaControlCosto as 
select 
a.idtransaccion,
a.Numero,
a.Observacion,
'FechaTransaccion' = t.Fecha,
'UsuarioIdentificador' = u.Identificador
from aControlCosto a
join Transaccion t on a.IDTransaccion = t.ID
join Usuario u on t.IDUsuario = u.ID

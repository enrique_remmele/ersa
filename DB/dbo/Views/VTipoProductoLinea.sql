﻿Create View [dbo].[VTipoProductoLinea]
As
Select
TPL.*,
'TipoProducto'=TP.Descripcion,
'Linea'=L.Descripcion
From TipoProductoLinea TPL
Join TipoProducto TP On TPL.IDTipoProducto=TP.ID
Join Linea L On TPL.IDLinea=L.ID

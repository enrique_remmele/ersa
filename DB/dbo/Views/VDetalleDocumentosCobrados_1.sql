﻿CREATE view [dbo].[VDetalleDocumentosCobrados]

as

Select
VDC.IDTipoComprobante,
'Comprobante'=VDC.[Cod.] + '  '+ convert(varchar(50),VDC.Comprobante) + ' ' + VDC.Condicion,
VDC.IDCliente,
VDC.Cliente,
VDC.Referencia,
'Cobranza'=VDC.Numero,
'Fecha'=VDC.FechaCobranza,
VDC.IDSucursal,
VDC.Sucursal,
VDC.IDMoneda,
VDC.Moneda,
VDC.IDDeposito,
'Planilla'=convert(varchar(50),VDC.Lote),
VDC.Credito,
'SaldoAnterior'=(VDC.Total 
				- 
				(IsNull((Select SUM(VC.Importe) From VVentaDetalleCobranza VC Where VC.IDTransaccion=VDC.IDtransaccion And VC.FechaCobranza<VDC.FechaCobranza),0)
				+
				IsNull((Select SUM(NC.Importe) From VNotaCreditoVenta NC Where NC.IDTransaccionVenta=VDC.IDtransaccion And NC.FechaEmision<VDC.FechaCobranza),0))),
--'Cobrado'=VDC.Importe / VDC.Cotizacion,
'Cobrado'=VDC.Importe,
'Equivalente' = round(VDC.Importe * VDC.CotizacionCobranza,0),
VDC.Cotizacion,
VDC.IDCobrador,
VDC.Cobrador,
VDC.IDVendedor,
VDC.Vendedor,
'Recibo'=VDC.ComprobanteCobranza,
VDC.IdSucursalOperacion,
VDC.FechaFactura,
VDC.IDTransaccionCobranza as Idtransaccion,
(select idusuario from TRansaccion T where T.id = VDC.IDTransaccionCobranza) as IdUsuario,
VDC.CotizacionCobranza,
'AnticipoCliente' = 'False',
'IDTipoProducto'=Isnull((select top(1) IDTipoProducto from vDetalleventa where IDTransaccion = VDC.IDTransaccion),0),
'FechaVencimiento' = VDC.FechaVencimiento
From VVentaDetalleCobranzaContado VDC 
Where VDC.CobranzaAnulada = 'False'

Union All

Select
VDC.IDTipoComprobante,
'Comprobante'=VDC.[Cod.] + ' ' + convert(varchar(50),VDC.Comprobante) + ' ' + VDC.Condicion,
VDC.IDCliente,
VDC.Cliente,
VDC.Referencia,
'Cobranza'=VDC.Numero,
'Fecha'=VDC.FechaCobranza,
VDC.IDSucursal,
VDC.Sucursal,
VDC.IDMoneda,
VDC.Moneda,
VDC.IDDeposito,
'Planilla'=convert(varchar(50),VDC.NroPlanilla),
Credito,
'SaldoAnterior'=(VDC.Total 
				- 
				(IsNull((Select SUM(VC.Importe) From VVentaDetalleCobranza VC Where VC.IDTransaccion=VDC.IDtransaccion And VC.FechaCobranza<VDC.FechaCobranza),0)
				+
				IsNull((Select SUM(NC.Importe) From VNotaCreditoVenta NC Where NC.IDTransaccionVenta=VDC.IDtransaccion And NC.FechaEmision<VDC.FechaCobranza),0))),
				
				
--'Cobrado'=VDC.Importe / VDC.Cotizacion,
'Cobrado'=VDC.Importe,
'Equivalente' = round(VDC.Importe * VDC.CotizacionCobranza,0),
VDC.Cotizacion,
VDC.IDCobrador,
VDC.Cobrador,
VDC.IDVendedor,
VDC.Vendedor,
VDC.Recibo ,
VDC.IDSucursalOperacion,
VDC.FechaFactura,
VDC.IDTransaccionCobranza as Idtransaccion,
(select idusuario from TRansaccion T where T.id = VDC.IDTransaccionCobranza) as IdUsuario,
VDC.CotizacionCobranza,
'AnticipoCliente'= (Case when VDC.IDTransaccionAnticipo > 0 then 'True' else 'False' end),
'IDTipoProducto'=Isnull((select top(1) IDTipoProducto from vDetalleventa where IDTransaccion = VDC.IDTransaccion),0),
'FechaVencimiento' = VDC.FechaVencimiento
From VVentaDetalleCobranzaCredito VDC
Where VDC.CobranzaAnulada = 'False'



















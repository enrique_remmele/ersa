﻿
CREATE view [dbo].[VCompraGastoMinimosDatos]
as

--Gasto
Select 
'Operacion'='GASTO',
'CodigoOperacion'='GAST',
IDTransaccion,
Numero,
NroComprobante,
IDProveedor,
Fecha,
Total,
Observacion,
IDMoneda,
Saldo,
Cancelado,
Cotizacion,
Cuota,
CajaChica

From Gasto

union all

--Compra
Select 
'Operacion'='COMPRA',
'CodigoOperacion'='COMP',
IDTransaccion,
Numero,
NroComprobante,
IDProveedor,
Fecha,
Total,
Observacion,
IDMoneda,
Saldo,
Cancelado,
Cotizacion,
'Cuota'=0,
'CajaChica'=0

From Compra

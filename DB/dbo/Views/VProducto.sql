﻿


CREATE VIEW [dbo].[VProducto]

AS

SELECT 

--Identificadores
P.ID,
P.Descripcion,
P.CodigoBarra,
P.Referencia,
'Ref'=P.Referencia,
'ProductoReferencia'=P.Descripcion + ' (' + P.Referencia + ')',
--Esto no se puede usar, porque se pueden repetir
--Si es necesario, buscar otra alternativa
'IDSucursal'=0,
'IDDeposito'=0,

--Clasificaciones
P.IDTipoProducto,
'TipoProducto'=ISNULL(TP.Descripcion, ''),
P.IDLinea,
'Linea'=ISNULL(L.Descripcion, ''),
P.IDSubLinea,
'SubLinea'=ISNULL(SL.Descripcion, ''),
P.IDSubLinea2,
'SubLinea2'=ISNULL(SL2.Descripcion, ''),
P.IDMarca,
'Marca'=ISNULL(M.Descripcion, ''),
P.IDPresentacion,
'Presentacion'=ISNULL(PRE.Descripcion, ''),
P.IDCategoria,
'Categoria'=ISNULL(CA.Descripcion, ''),
P.IDProveedor,
'ReferenciaProveedor'=PRO.Referencia,
'Proveedor'=ISNULL(PRO.RazonSocial, ''),
'ProveedorReferencia'=PRO.RazonSocial + ' (' + PRO.Referencia + ')',
P.IDDivision,
'Division'=ISNULL(D.Descripcion, ''),
P.IDProcedencia,
'Procedencia'=ISNULL(PA.Descripcion , ''),

--Nueva Clasificacion
'IDClasificacion'=ISNULL(P.IDClasificacion, 0),
'Clasificacion'=ISNULL(CLA.Descripcion, ''),
'CodigoClasificacion'=ISNULL(CLA.Codigo, 0),
'NivelClasificacion'=ISNULL(CLA.Nivel, 0),
'NumeroClasificacion'=ISNULL(CLA.Numero, 0),

--Stock
P.IDUnidadMedida,
'UnidadMedida'=ISNULL(U.Referencia, 'UND'),
'Uni. Med.'=ISNULL(U.Descripcion, 'UNIDAD'),
'UnidadPorCaja'=ISNULL(P.UnidadPorCaja, 1),
P.IdUnidadMedidaConvertir,

'UnidadMedidaConvertir'= ISNULL((SELECT Referencia FROM UnidadMedida WHERE ID= P.IdUnidadMedidaConvertir ),ISNULL(U.Referencia, 'UND')),
'Uni. Med. Conver.'= ISNULL((SELECT Descripcion FROM UnidadMedida WHERE ID= P.IdUnidadMedidaConvertir ),ISNULL(U.Descripcion, 'UND')),
'UnidadConvertir'=ISNULL (P.UnidadConvertir,1),

'Peso'= (Case when (convert(decimal(18,2),Isnull(P.Peso,1))) = (convert(decimal(18,2),0)) then (convert(decimal(18,2),1)) else (convert(decimal(18,2),Isnull(P.Peso,1))) end),
P.Volumen,

--Impuestos
P.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
'Exento'=I.Exento,
I.FactorDiscriminado,
I.FactorImpuesto,

--Configuraciones
'Estado'=ISNULL(P.Estado, 'True'),
'EstadoDescripcion'=CASE WHEN P.Estado = 'True' THEN 'Activo' ELSE 'Inactivo' END,
'ControlarExistencia'=ISNULL(P.ControlarExistencia, 'False'),
P.ExistenciaGeneral,
'Existencia'=P.ExistenciaGeneral,

--Contabilidad 
'CuentaCompra'=ISNULL(CCCompra.Descripcion, ''),
'CodigoCuentaCompra'=ISNULL(CCCompra.Codigo, ''),
'CuentaVenta'=ISNULL(CCVenta.Descripcion, ''),
'CodigoCuentaVenta'=ISNULL(CCVenta.Codigo, ''),
'CuentaCosto'=ISNULL(CCCosto.Descripcion, ''),
'CodigoCuentaCosto'=ISNULL(CCCosto.Codigo, ''),
'CuentaDeudor'=ISNULL(CCDeudor.Descripcion, ''),
'CodigoCuentaDeudor'=ISNULL(CCDeudor.Codigo, ''),

--Costo
'Precio'=0.00,
'UltimaEntrada'=CONVERT(VARCHAR(50), P.UltimaEntrada, 6), 
'CantidadEntrada'=ISNULL(P.CantidadEntrada, 0),
'UltimaSalida'=CONVERT(VARCHAR(50), P.UltimaSalida, 6), 
'Costo'=dbo.FCostoProducto(P.ID),
'UltimoCosto'=ISNULL(P.UltimoCosto,0),
P.CostoSinIVA,
P.CostoCG,
P.CostoPromedio,

--Descuentos
'TotalDescuento'=0,
'DescuentoUnitario'=0,
'PorcentajeDescuento'=0,
P.FactorCosto,
'IDMoneda'= (SELECT TOP(1)IDMoneda FROM ProductoListaPrecio WHERE IDProducto = P.ID),
'Vendible'=ISNULL(P.vendible, 'True'),
'IDDepositoFijo'=P.IDDeposito,
'Deposito'= ISNULL((SELECT TOP(1)Descripcion FROM Deposito WHERE ID = P.IDDeposito),''),
'ConsumoCombustible'=ISNULL(P.ConsumoCombustible, 'False'),
'MateriaPrima'=ISNULL(P.MateriaPrima, 'False'),
'DescargaCompra'=ISNULL(P.DescargaCompra, 'False'),
'Anulado'=ISNULL(P.Estado, 'True'),
'ReferenciaProducto'=concat(P.Referencia,'-',P.Descripcion),
'Inmueble' = 0,
'Producido' = ISNULL(TP.Producido, 'False'),

--Tipo Producto
'CuentaContableVentaTipoProducto'=TP.CuentaContableVenta,
'CuentaContableDeudoresTipoProducto'=TP.CuentaContableCliente,
'CuentaContableCostoMercaderiaTipoProducto'=TP.CuentaContableCosto,
'CuentaContableProveedorTipoProducto'=TP.CuentaContableProveedor


--'UltimoCosto'=(Select IsNull((Select RC.UltimoCosto From RegistroCosto RC Where RC.IDProducto=P.ID), 0)),
--'CostoPromedio'=(Select IsNull((Select RC.Promedio From RegistroCosto RC Where RC.IDProducto=P.ID), 0)),
--'UltimaActualizacion'=(Select IsNull((Select Convert(varchar(50), RC.UltimaActualizacion, 6) From RegistroCosto RC Where RC.IDProducto=P.ID), '---')),
--'UsuarioAlta'= (Select Top(1) LS.Usuario from VLogSucesos LS where LS.IDTipoOperacionLog = 1 and LS.Comprobante = convert(varchar(10), P.ID)),
--'FechaAlta' = (Select Top(1) LS.Fecha  from VLogSucesos LS where LS.IDTipoOperacionLog = 1 and LS.Comprobante = convert(varchar(10), P.ID))

FROM Producto P
LEFT OUTER JOIN TipoProducto TP ON P.IDTipoProducto=TP.ID
LEFT OUTER JOIN Linea L ON P.IDLinea=L.ID
LEFT OUTER JOIN SubLinea SL ON P.IDSubLinea=SL.ID
LEFT OUTER JOIN SubLinea2 SL2 ON P.IDSubLinea2=SL2.ID
LEFT OUTER JOIN Marca M ON P.IDMarca=M.ID
LEFT OUTER JOIN Presentacion PRE ON P.IDPresentacion=PRE.ID
LEFT OUTER JOIN Categoria CA ON P.IDCategoria=CA.ID
LEFT OUTER JOIN Proveedor PRO ON P.IDProveedor=PRO.ID
LEFT OUTER JOIN Division D ON P.IDDivision=D.ID
LEFT OUTER JOIN Pais PA ON P.IDProcedencia=PA.ID
LEFT OUTER JOIN UnidadMedida U ON P.IDUnidadMedida=U.ID
LEFT OUTER JOIN ClasificacionProducto CLA ON P.IDClasificacion=CLA.ID
LEFT OUTER JOIN Impuesto I ON P.IDImpuesto=I.ID
--LEFT OUTER JOIN LogSucesos LS ON LS.Comprobante = P.ID AND LS.Tabla = 'PRODUCTO'
LEFT OUTER JOIN CuentaContable CCCompra ON P.CuentaContableCompra=CCCompra.Codigo AND P.CuentaContableCompra IS NOT null
LEFT OUTER JOIN CuentaContable CCVenta ON P.CuentaContableVenta=CCVenta.Codigo AND P.CuentaContableVenta IS NOT null
LEFT OUTER JOIN CuentaContable CCCosto ON P.CuentaContableCosto=CCCosto.Codigo AND P.CuentaContableCosto IS NOT null
LEFT OUTER JOIN CuentaContable CCDeudor ON P.CuentaContableDeudor=CCDeudor.Codigo AND P.CuentaContableDeudor IS NOT null
--No se puede hacer join con zona, se duplican los productos













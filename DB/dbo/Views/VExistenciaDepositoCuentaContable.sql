﻿
CREATE View [dbo].[VExistenciaDepositoCuentaContable]
As

Select
'IDDeposito'=D.ID,
'Deposito'=D.Deposito,
'IDSucursal'=D.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc-Dep'=D.[Suc-Dep],
D.IDTipoDeposito,
'TipoDeposito'= TD.Descripcion,
'CuentaContableMercaderiaDeposito'=(Case when D.IDTipoDeposito = 5 then P.CodigoCuentaCompra else  (Case when P.IDTipoProducto = 27 then D.CuentaContableCombustible else D.CuentaContableMercaderia end)  end),

--Producto
'IDProducto'=P.ID,
'Producto'=P.Descripcion,
'Referencia'=P.Referencia,
'CodigoBarra'=P.CodigoBarra,
'ControlarExistencia'=P.ControlarExistencia,
P.UnidadMedida ,
P.Estado,
P.UltimaEntrada,
'PesoUnitario'=convert(decimal(18,2),Isnull(P.Peso,1)),
'CuentaContableCompraProducto'=P.CodigoCuentaCompra,
'CuentaContableCostoProducto'=P.CodigoCuentaCosto,

--Clasificacion
P.IDTipoProducto,
P.TipoProducto,
P.IDLinea,
P.Linea,
P.IDSubLinea,
P.SubLinea,
P.IDSubLinea2,
P.SubLinea2,
P.IDMarca,
P.Marca,
P.IDPresentacion,
P.IDCategoria,
P.Categoria,
P.IDProveedor,
P.Proveedor,

--Existencias
'Existencia'=ED.Existencia,
'ExistenciaMinima'=ED.ExistenciaMinima,
'ExistenciaCritica'=ED.ExistenciaCritica,
'ExistenciaGeneral'=P.ExistenciaGeneral,
'PlazoReposicion'=ED.PlazoReposicion,
'CantidadAnulados'=0,--Anulados en gestion

--Costos
P.Costo,
P.CostoPromedio,
P.UltimoCosto ,

P.CostoPromedio * ED.Existencia AS TotalValorizadoCostoPromedio,
'TotalValorizadoAnulado'=0,

--'CuentaContable'= (Case when P.IDTipoProducto not in (1,2,6) THEN P.CuentaCompra else Concat(D.CuentaContableMercaderia, ' - ', D.CuentaContableMercaderiaDescripcion) end),
--'CodigoCuentaContable'= (Case when P.IDTipoProducto not in (1,2,6) THEN P.CodigoCuentaCompra else D.CuentaContableMercaderia end)
'CuentaContable'= (Case when D.IDTipoDeposito = 5 THEN P.CuentaCompra else (Case when P.IDTipoProducto = 27 then Concat(D.CuentaContableCombustible, ' - ', D.CuentaContableCombustibleDescripcion) else Concat(D.CuentaContableMercaderia, ' - ', D.CuentaContableMercaderiaDescripcion) end)  end),
'CodigoCuentaContable'= (Case when D.IDTipoDeposito = 5 THEN P.CodigoCuentaCompra else (Case when P.IDTipoProducto = 27 then D.CuentaContableCombustible else D.CuentaContableMercaderia end) end)

From vDeposito D
Join TipoDeposito TD on D.IDTipoDeposito = TD.ID
Join Sucursal S On D.IDSucursal=S.ID
Join ExistenciaDeposito ED On D.ID=ED.IDDeposito
Join VProducto P On ED.IDProducto=P.ID
WHERE P.ControlarExistencia = 1






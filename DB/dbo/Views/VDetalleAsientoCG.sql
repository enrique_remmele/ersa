﻿
CREATE View [dbo].[VDetalleAsientoCG]
As
Select 
DA.Numero,
DA.IDCuentaContable,
'Codigo'=(IsNull(CC.Codigo, DA.CuentaContable)),
'Descripcion'=IsNull(CC.Descripcion, '---'),
'Cuenta'=IsNull(CC.Codigo, DA.CuentaContable) + ' - ' + IsNull(CC.Descripcion, '---'),
'Alias'=IsNull(CC.Alias, '---'),
DA.ID,
DA.Credito,
DA.Debito,
DA.Importe,
'Observacion'=(Case When DA.Observacion Is Null Then A.Detalle Else (Case When DA.Observacion != '' Then DA.Observacion Else A.Detalle End) End),
'Orden'=DA.ID,
S.Codigo as CodSucursal,
M.Referencia as Moneda,
A.Fecha,

DA.IDSucursal,
DA.TipoComprobante,
'NroComprobante'=(Case When (DA.NroComprobante) Is Null Then A.NroComprobante 
					   When (DA.NroComprobante) = '' Then A.NroComprobante 
					   Else (DA.NroComprobante) End),

--Unidad de Negocio
DA.IDUnidadNegocio,
'UnidadNegocio'=U.Descripcion,

--Centro de Costos
'IDCentroCosto'=IsNull(DA.IDCentroCosto, A.IDCentroCosto),
'CodigoCentroCosto'=(Select Top(1) Codigo From CentroCosto CC Where CC.ID= IsNull(DA.IDCentroCosto, A.IDCentroCosto)),
'CentroCosto'=(Select Top(1) Descripcion From CentroCosto CC Where CC.ID= IsNull(DA.IDCentroCosto, A.IDCentroCosto))


From DetalleAsientoCG DA
Join AsientoCG A On DA.Numero=A.Numero
Left Outer Join VCuentaContable CC On DA.CuentaContable=CC.Codigo 
Join Sucursal S on A.IDSucursal=S.ID
Join Moneda M on A.IDMoneda=M.ID
Left Outer Join UnidadNegocio U On DA.IDUnidadNegocio = U.ID
Where CC.PlanCuentaTitular='True'





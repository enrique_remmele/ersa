﻿




CREATE View [dbo].[VC_Excepciones]
As
Select
'Cod_Lista'=LP.ID,
'Cod_Cliente'=C.Referencia,
'Cod_Producto'=P.Referencia,
'Precio_Excepcion'=PLP.Precio-PLPE.Descuento,
'Vigencia'=PLPE.Hasta

From vProductoListaPrecioExcepciones PLPE
Join Producto P On PLPE.IDProducto=P.ID
--Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
Join ListaPrecio LP On PLPE.ListaPrecio=LP.Descripcion
Join Cliente C On PLPE.IDCliente=C.ID
Join ProductoListaPrecio PLP On PLPE.IDProducto=PLP.IDProducto And PLPE.IDListaPrecio=PLP.IDListaPrecio
Where PLPE.IDTipoDescuento = 2
And ISnull(PLPE.IDTransaccionPedido,0) = 0
--And isnull(PLPE.CantidadLimiteSaldo,1)=0
and (select top(1) ProductoPrecio from Configuraciones) = 0

union all


Select
'Cod_Lista'=LP.ID,
'Cod_Cliente'=C.Referencia,
'Cod_Producto'=P.Referencia,
'Precio_Excepcion'=PLPE.PrecioConDescuento,
'Vigencia'=DateAdd(year,1,PLPE.Desde)
From vProductoPrecio PLPE
Join Producto P On PLPE.IDProducto=P.ID
Join Cliente C On PLPE.IDCliente=C.ID
Join ListaPrecio LP On C.IDListaPrecio=LP.ID
Join ProductoListaPrecio PLP On PLPE.IDProducto=PLP.IDProducto And C.IDListaPrecio=PLP.IDListaPrecio
Where PLPE.PrecioUnico = 0
and (select top(1) ProductoPrecio from Configuraciones) = 1


union all

--SC 12-08-2021 Para Mostrar Datos de ClienteSucursal
select 
'Cod_Lista'=LP.ID,
'Cod_Cliente'=C.Referencia,
'Cod_Producto'=P.Referencia,
'Precio_Excepcion'=PLPE.PrecioConDescuento,
'Vigencia'=DateAdd(year,1,PLPE.Desde) 
from vProductoPrecio PLPE 
Join Producto P On PLPE.IDProducto=P.ID
Join ClienteSucursal CS On PLPE.IDCliente=CS.IDCliente
Join Cliente C On CS.IDCliente = C.ID
Join ListaPrecio LP On CS.IDListaPrecio=LP.ID
Join ProductoListaPrecio PLP On PLPE.IDProducto=PLP.IDProducto And CS.IDListaPrecio=PLP.IDListaPrecio
where PLPE.PrecioUnico = 0
and (select top(1) ProductoPrecio from Configuraciones) = 1





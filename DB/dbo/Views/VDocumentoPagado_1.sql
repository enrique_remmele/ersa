﻿
CREATE View [dbo].[VDocumentoPagado]

As

--Con Cheque
Select 

--Proveedor
OP.IDProveedor,
OP.Proveedor,
OP.Referencia,
OP.IDTipoProveedor,

--Sucursal
OP.IDSucursal,
OP.IDCiudad,
--Banco
OP.IDCuentaBancaria,

--Tipo Comprobante
OPE.Numero,
OP.IDTipoComprobante,
'Comp'=OPE.[T. Comp.],
'Documento'=OPE.NroComprobante,
OPE.Fec,
OPE.Fecha,
'OP'=OP.Comprobante,
'Banco / Cta. - Efectivo'=OP.CuentaBancaria,
'Comprobante'=OP.NroCheque,
OPE.Importe,
'Concepto'=OP.Observacion,
OP.Mon,
OP.Cheque,
OP.IDTransaccion,
'FechaPago'=OP.Fecha,
'FecPago'=OP.Fec

From VOrdenPago OP
Join VOrdenPagoEgreso OPE On OP.IDTransaccion=OPE.IDTransaccionOrdenPago 
Where OP.Cheque='True'

Union All

--En Efectivo
Select 
--Proveedor
OP.IDProveedor,
OP.Proveedor,
OP.Referencia,
OP.IDTipoProveedor,

--Sucursal
OP.IDSucursal,
OP.IDCiudad,
--Banco
OP.IDCuentaBancaria,

--Tipo Comprobante
OPE.Numero,
OP.IDTipoComprobante,
'Comp'=OPEFE.CodigoComprobante,
OP.NroComprobante,
OP.Fec,
OP.Fecha,
OP.Comprobante,

OPEFE.CodigoComprobante,
OPEFE.Comprobante,

OPE.Importe,
OP.Observacion,
OP.Mon,
OP.Cheque,
OP.IDTransaccion,
'FechaPago'=OP.Fecha,
'FecPago'=OP.Fec

From VOrdenPago OP
Join VOrdenPagoEgreso OPE on OP.IDTransaccion=OPE.IDTransaccionOrdenPago 
Join VOrdenPagoEfectivo OPEFE On OP.IDTransaccion=OPEFE.IDTransaccionOrdenPago












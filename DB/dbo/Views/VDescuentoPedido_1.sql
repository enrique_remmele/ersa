﻿



CREATE View [dbo].[VDescuentoPedido]
As 
Select
DP.IDTransaccion,
DP.IDProducto,
V.Producto,
DP.ID,
DP.IDDescuento,
DP.IDDeposito,
V.Deposito,
DP.IDTipoDescuento,
'Tipo'=(Case When (TDP.Descripcion) Is Null Then (Case when (IDTipoDescuento) = 0 Then 'TPR' Else '---' End) Else (TDP.Descripcion) End),
'T. Desc.'=(Case When (TDP.Codigo) Is Null Then (Case when (IDTipoDescuento) = 0 Then 'TPR' Else '---' End) Else (TDP.Codigo) End),
TDP.PlanillaTactico,
DP.IDActividad,
'Actividad'=ISNULL((A.Codigo), ''),
DP.Descuento,
DP.Porcentaje,
'Cantidad'=V.Cantidad,
'Total'=V.Cantidad * DP.Descuento,
DP.DescuentoDiscriminado,
'TotalDescuentoDiscriminado'=DP.DescuentoDiscriminado * V.Cantidad


From DescuentoPedido DP
Join VDetallePedido V On DP.IDTransaccion=V.IDTransaccion And DP.IDProducto=V.IDProducto And DP.ID=V.ID
Left Outer Join TipoDescuento TDP On DP.IDTipoDescuento=TDP.ID
Left Outer Join Actividad A On DP.IDActividad=A.ID












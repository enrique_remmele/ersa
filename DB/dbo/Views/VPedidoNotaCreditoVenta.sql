﻿CREATE View [dbo].[VPedidoNotaCreditoVenta]

As

Select

--Venta
'IDTransaccion'=NCV.IDTransaccionPedidoNotaCredito, 
'IDTransaccionVenta'=V.IDTransaccion , 
V.Comprobante,
V.IDCliente,
V.Saldo,
'SaldoAConfirmar' =(select dbo.FSaldoConPedidoNC(V.IDTransaccion)),
V.FechaEmision,
'TotalVenta'=V.Total,

--Nota de Credito
NC.Fecha,
'Comp. NC'='PEDNC' + ' ' +Convert(varchar(50),NC.Numero),

'Usuario NC'=NC.Usuario,
'Tipo NC'=NC.Tipo,
'Estado NC'=NC.[EstadoNC], 
NC.ProcesadoNC,
NC.Total,
'Importe'=(Case When (NC.Anulado) = 'True' Then 0 Else NCV.Importe End),
'IDTransaccionPedidoNotaCreditoAplicacion'=0,
NC.Observacion,
'Aplicado'='True',
'Anulado'=NC.Anulado,
'Tipo' = (select Tipo from VSubMotivoNotaCredito where id = NC.IDSubMotivoNotaCredito)
From PedidoNotaCreditoVenta  NCV
Join VPedidoNotaCredito NC On NCV.IDTransaccionPedidoNotaCredito=NC.IDTransaccion
join VVenta  V on NCV.IDTransaccionVentaGenerada  = V .IDTransaccion 
Where NC.Aplicar = 'False'







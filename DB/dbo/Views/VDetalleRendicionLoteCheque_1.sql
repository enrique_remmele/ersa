﻿CREATE View [dbo].[VDetalleRendicionLoteCheque]

As

Select 
D.IDTransaccion,
D.ID,
D.Banco,
D.NroCheque,
D.Fecha,
'Fec.'=CONVERT(varchar(50), D.Fecha, 6),
D.Diferido,
'Dif'=Case When D.Diferido='True' Then 'SI' Else 'NO' End,
'TipoCheque'=Case When D.Diferido='True' Then 'Diferido' Else 'Al Día' End,
D.FechaVencimiento,
'Fec Venc.'=CONVERT(varchar(50), D.FechaVencimiento, 6),
D.BancoLocal,
'Local'=Case When D.BancoLocal='True' Then 'SI' Else 'NO' End,

--Venta
D.IDTransaccionVenta,
'Comprobante'=V.Comprobante,
'Tipo'=V.TipoComprobante,
V.Cliente,
V.Saldo,
V.Credito,

--Total
D.Importe

From DetalleRendicionLoteCheque D
Join RendicionLote RL On D.IDTransaccion=RL.IDTransaccion
Join VVenta V On D.IDTransaccionVenta=V.IDTransaccion


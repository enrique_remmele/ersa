﻿


CREATE view  [dbo].[VCompraGasto]
as

--Gasto
Select 
'Operacion'='GASTO',
'CodigoOperacion'='GAST',
IDTransaccion,
Numero,
IDTipoComprobante,
TipoComprobante ,
NroComprobante,
IDProveedor,
Fecha,
Total,
Observacion,
IDMoneda,
Moneda,
Saldo,
Cancelado,
FechaVencimiento,
IDSucursal,
IDDeposito,
RetencionIVA,
Cotizacion,
Referencia,
Cuota,
RRHH,
CajaChica

From VGasto

union all

--Compra
Select 
'Operacion'='COMPRA',
'CodigoOperacion'='COMP',
IDTransaccion,
Numero,
IDTipoComprobante,
TipoComprobante,
NroComprobante,
IDProveedor,
Fecha,
Total,
Observacion,
IDMoneda,
Moneda,
Saldo,
Cancelado,
FechaVencimiento,
IDSucursal,
IDDeposito,
RetencionIVA,
Cotizacion,
Referencia,
'Cuota'=0,
'RRHH'='False',
'CajaChica'=0

From vCompra







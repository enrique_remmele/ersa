﻿CREATE View [dbo].[VCheque]
As
Select
C.IDTransaccion,
--Sucursal
C.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
'CiudadDescripcion'='ASUNCION',

C.Numero,
'Num'=C.Numero,

--Cuenta Bancaria
C.IDCuentaBancaria,
CB.Referencia,
CB.CuentaBancaria,
CB.IDBanco,
CB.Banco,
CB.IDMoneda,
CB.Mon,
CB.Moneda,

--Cheque
C.NroCheque,
'Fecha'=Convert(varchar(50),C.Fecha,5),
'FechaFiltro'=C.Fecha,--Formato fecha
'Fec'=CONVERT(varchar(50), C.Fecha, 6),
'FechaCheque'=C.Fecha,
'Dia'=dbo.FFormatoDosDigitos(DatePart(dd, C.Fecha)),
'Mes'=dbo.FMes(DatePart(MM, C.Fecha)),
'Año'=DatePart(yyyy, C.Fecha),
C.FechaPago,
'Fec Pago'=CONVERT(varchar(50), C.FechaPago, 6),
'DiaP'=dbo.FFormatoDosDigitos(DatePart(dd,C.FechaPago)),
'MesP'=dbo.FMes(DatePart(MM,C.FechaPago)),
'AñoP'=DATEPART(yyyy,C.FechaPago),
--'Cotizacion'=(Case when isnull(CB.IDMoneda,1) = 1 then 1 else isnull(C.Cotizacion,1) end),
C.Cotizacion,
C.ImporteMoneda,
C.Importe,
C.Diferido,
C.FechaVencimiento,
'Fec. Venc.'=CONVERT(varchar(50), C.FechaVencimiento, 6),
C.ALaOrden,
C.Conciliado,
C.Anulado,
'Estado'=(Case When (C.Anulado) = 'True' Then 'ANULADO' Else (Case When (C.Conciliado) = 'True' Then 'CONCILIADO' Else '---' End) End),
'DiaVencimiento'=dbo.FFormatoDosDigitos(DatePart(dd, C.FechaVencimiento)),
'MesVencimiento'=dbo.FMes(DatePart(MM, C.FechaVencimiento)),
'AñoVencimiento'=DatePart(yyyy, C.FechaVencimiento)

From Cheque C
Join VCuentaBancaria CB On C.IDCuentaBancaria=CB.ID
Join Sucursal S On C.IDSucursal=S.ID
















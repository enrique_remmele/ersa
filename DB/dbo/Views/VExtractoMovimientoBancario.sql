﻿
CREATE view [dbo].[VExtractoMovimientoBancario]

as

--Depósito Bancario
Select 
DB.IDSucursal, 
DB.Fecha,
DB.IDCuentaBancaria,
'Fec'=Convert(Date,DB.Fec),
'Operacion'=DB.[Cod.]+' '+convert(varchar(50),DB.Numero),
'Comp/Cheque'=DB.[Cod.]+' '+convert(varchar(50),DB.NroComprobante),
'Ordende/Detalle/Concepto'=DB.Observacion,  
'Debito'= DB.Total,
'Credito'=0.00,
'Movimiento'='DPB',

'CuentaBanco'=CB.Banco,
CB.Cuenta,
'CuentaNombre'=CB.Nombre,
'CodigoCC'=CB.[Codigo CC],
M.Decimales,
'Moneda'=M.Descripcion,

'TipoComprobante'= DB.TipoComprobante,
'FechaVencimiento'=DB.Fecha

From VDepositoBancario DB 
Join VCuentaBancaria CB on CB.ID=DB.IDCuentaBancaria 
Join Moneda M On CB.IDMoneda = M.ID

Union All

--CREDITO BANCARIO
Select  
DCB.IDSucursal, 
DCB.Fecha,
DCB.IDCuentaBancaria,
'Fec'=DCB.Fec,
'Operacion'='CRED. '+convert(varchar(50),DCB.Numero),
'Comp/Cheque'=DCB.[Cod.]+' '+convert(varchar(50),DCB.NroComprobante),
'Ordende/Detalle/Concepto'=DCB.Observacion +' '+convert(varchar(50),DCB.Tipo),  
'Debito'=0.00,
'Credito'=DCB.Total,
'Movimiento'='CB',

'CuentaBanco'=CB.Banco,
CB.Cuenta,
'CuentaNombre'=CB.Nombre,
'CodigoCC'=CB.[Codigo CC],
M.Decimales,
'Moneda'=M.Descripcion,

'TipoComprobante'= DCB.TipoComprobante,
'FechaVencimiento'=DCB.Fecha

From VDebitoCreditoBancario DCB
Join VCuentaBancaria CB on CB.ID=DCB.IDCuentaBancaria 
Join Moneda M On CB.IDMoneda = M.ID
Where DCB.Credito='True'

Union All

--DEBITO BANCARIO
Select  
DCB.IDSucursal, 
DCB.Fecha,
DCB.IDCuentaBancaria,
'Fec'=DCB.Fec,
'Operacion'='DEB. '+convert(varchar(50),DCB.Numero),
'Comp/Cheque'=DCB.[Cod.]+' '+convert(varchar(50),DCB.NroComprobante),
'Ordende/Detalle/Concepto'=DCB.Observacion +' '+convert(varchar(50),DCB.Tipo),  
'Debito'=DCB.Total,
'Credito'=0.00,
'Movimiento'='DB',

'CuentaBanco'=CB.Banco,
CB.Cuenta,
'CuentaNombre'=CB.Nombre,
'CodigoCC'=CB.[Codigo CC],
M.Decimales,
'Moneda'=M.Descripcion, 

'TipoComprobante'= DCB.TipoComprobante,
'FechaVencimiento'=DCB.Fecha

From VDebitoCreditoBancario DCB 
Join VCuentaBancaria CB on CB.ID=DCB.IDCuentaBancaria
Join Moneda M On CB.IDMoneda = M.ID
Where DCB.Debito='True' 
 
Union All

--Orden de Pago
Select 
OP.IDSucursal, 
OP.Fecha,
OP.IDCuentaBancaria,
'Fec'=OP.Fec,
'Operacion'=OP.[Cod.]+' '+convert(varchar(50),OP.Numero),
'Comp/Cheque'='CHQ'+' '+convert(varchar(50),OP.Banco)+' ' +convert(varchar(50),OP.NroCheque),
'Ordende/Detalle/Concepto'=OP.Observacion,  
'Debito'=0.00,
'Credito'=OP.ImporteMoneda,
'Movimiento'='OP',

'CuentaBanco'=CB.Banco,
CB.Cuenta,
'CuentaNombre'=CB.Nombre,
'CodigoCC'=CB.[Codigo CC],	
M.Decimales,
'Moneda'=M.Descripcion,

'TipoComprobante'=Case C.Diferido When 0 Then 'CHQ' Else 'CDIF' End,
'FechaVencimiento'=C.FechaVencimiento

 From VOrdenPago OP
 Join VCheque C On OP.IDTransaccion = C.IDTransaccion
 Join VCuentaBancaria CB on CB.ID=OP.IDCuentaBancaria 
 Join Moneda M On CB.IDMoneda = M.ID
 Where Cheque= 'True'And OP.Anulado ='False'
 

 Union All

--Descuento Cheque
Select 
DC.IDSucursal, 
 DC.Fecha,
 DC.IDCuentaBancaria,
'Fec'=DC.Fec,
'Operacion'=DC.[Cod.]+' '+convert(varchar(50),DC.Numero),
'Comp/Cheque'='CHQ'+' '+convert(varchar(50),DC.Banco)+' ' +convert(varchar(50),DC.[Cod.]),
'Ordende/Detalle/Concepto'=DC.Observacion,  
'Debito'=DC.TotalAcreditado,
'Credito'=0.00,
'Movimiento'='DC',

'CuentaBanco'=CB.Banco,
CB.Cuenta,
'CuentaNombre'=CB.Nombre,
'CodigoCC'=CB.[Codigo CC],
M.Decimales,
'Moneda'=M.Descripcion,

'TipoComprobante'= DC.TipoComprobante,
'FechaVencimiento'=DC.Fecha

From VDescuentoCheque DC  
Join VCuentaBancaria CB on CB.ID=DC.IDCuentaBancaria 
Join Moneda M On CB.IDMoneda = M.ID 


Union All

--Cheque Rechazado
Select 
CR.IDSucursal, 
CR.Fecha,
CR.IDCuentaBancaria,
'Fec'=Convert(Date,CR.Fec),
'Operacion'='Rechazo'+' '+convert(varchar(50),Cr.Numero ),
'Comp/Cheque'='CHQ'+' '+convert(varchar(50),CC.Banco)+' ' +convert(varchar(50),CC.NroCheque),
'Ordende/Detalle/Concepto'=CR.Observacion,  
'Debito'=0.00,
'Credito'=CC.Importe,
'Movimiento'='CCR',

'CuentaBanco'=CB.Banco,
CB.Cuenta,
'CuentaNombre'=CB.Nombre,
'CodigoCC'=CB.[Codigo CC],
M.Decimales,
'Moneda'=M.Descripcion,

'TipoComprobante'= 'CHQR',
'FechaVencimiento'=CR.Fecha

From VChequeClienteRechazado CR
Join VChequeCliente CC On CR.IDTRansaccionCheque=CC.IDTransaccion
Join VCuentaBancaria CB on CB.ID=CR.IDCuentaBancaria 
Join Moneda M On CB.IDMoneda = M.ID 
Where CR.Anulado ='False'



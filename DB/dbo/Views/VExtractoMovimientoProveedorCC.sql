﻿CREATE view [dbo].[VExtractoMovimientoProveedorCC]
as
--Egreso
Select
C.IDTransaccion, 
'IDProveedor'=P.ID,
'Operación'=CodigoOperacion + ' ' + isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')),
'Codigo'=C.IDProveedor,
'RazonSocial'=P.RazonSocial ,
'Referencia'=P.Referencia ,
'Fecha'=C.Fecha,
'Documento'=C.TipoComprobante + ' ' + convert(varchar(50),C.NroComprobante) + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end),  
'Detalle/Concepto'=Observacion,
'Debito'=0,
'Credito'=DA.Credito,
'TotalComprobante' = C.Total,
C.Cotizacion,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=C.Moneda,
'Movimiento'='EG',
C.RRHH,
'CodigoCuentaContable'=DA.Codigo,
'CuentaContable'=DA.Cuenta
From 
VCompraGasto C 
Join VProveedor P on C.IDProveedor=P.ID
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Credito>0 

union all

--Nota Débito Proveedor
Select
C.IDTransaccion,
'IDProveedor'=C.IDProveedor,
'Operación'='Cr ' + isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')),
'Codigo'=C.IDProveedor,
'RazonSocial'=C.Proveedor,
'Referencia'=C.Referencia,
'Fecha'=C.Fecha,
'Documento'=C.[Cod.] + ' ' + convert(varchar(50),C.NroComprobante) + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end), 
'Detalle/Concepto'='',
'Debito'=0,
'Credito'=DA.Credito,
'TotalComprobante' = C.Total,
C.Cotizacion,
'IDMoneda'='',
'DescripcionMoneda'=C.DescripcionMoneda,
'Movimiento'='ND',
'RRHH'='False',
'CodigoCuentaContable'=DA.Codigo,
'CuentaContable'=DA.Cuenta
From
VNotaDebitoProveedor C
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Credito>0 

union all

--Nota de Crédito Proveedor
Select
NC.IDTransaccion,
'IDProveedor'=NC.IDProveedor,
'Operación'='Db ' + isnull(convert(varchar(50),NC.Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'RazonSocial'=Proveedor,
NC.Referencia,
'Fecha'=NC.Fecha,
'Documento'=[Cod.] + ' ' + convert(varchar(50),NroComprobante) + (Case when NC.Cotizacion = 1 then '' else concat(' (',cast(NC.Cotizacion as integer),')') end), 
'Detalle/Concepto'='',
'Debito'=DA.Debito,
'Credito'=0,
'TotalComprobante' = NC.Total,
NC.Cotizacion,
NC.IDMoneda,
'DescripcionMoneda'=M.Descripcion,
'Movimiento'='NC',
'RRHH'='False',
'CodigoCuentaContable'=DA.Codigo,
'CuentaContable'=DA.Cuenta
From
VNotaCreditoProveedor NC
Join VMoneda M on NC.IDMoneda=M.ID 
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = NC.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Debito>0 

union all

--Orden Pago
Select
C.IDTransaccion,
'IDProveedor'=C.IDProveedor,
'Operación'='OP '+isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')),
'Codigo'=C.IDProveedor,
'RazonSocial'=C.Proveedor,
'Referencia'=C.Referencia,
'Fecha'=C.Fecha,
'Documento'=C.[TipoComprobante] + ' ' + convert(varchar(50),C.NroComprobante) + + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end), 
'Detalle/Concepto'=C.Observacion,
'Debito'=DA.Debito,
'Credito'=0,
'TotalComprobante' = isnull(Case When (IDMonedaComprobante) = 1 Then Importe Else (Case When (IDMoneda) = 1 Then ImporteMoneda / Cotizacion Else ImporteMoneda End) End,0) + ImporteMonedaDocumento,
C.Cotizacion,
'IDMoneda'=C.IDMonedaComprobante,
'DescripcionMoneda'=C.MonedaComprobante,
'Movimiento'='OP',
'RRHH'='False',
'CodigoCuentaContable'=DA.Codigo,
'CuentaContable'=DA.Cuenta
From
VOrdenPago C
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Debito>0 
Where C.Anulado='False'

union all

--Entrega de Cheques
Select
C.IDTransaccion,
'IDProveedor'=C.IDProveedor,
'Operación'='ENTREGA CHEQUE '+isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')),
'Codigo'=C.IDProveedor,
'RazonSocial'=C.Proveedor,
'Referencia'=C.Referencia,
'Fecha'=EC.FechaEntrega,
'Documento'=C.[TipoComprobante] + ' ' + convert(varchar(50),C.NroComprobante) + + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end), 
'Detalle/Concepto'=C.Observacion,
'Debito'=DA.Debito,
'Credito'=0,
'TotalComprobante' = isnull(Case When (IDMonedaComprobante) = 1 Then Importe Else (Case When (IDMoneda) = 1 Then ImporteMoneda / Cotizacion Else ImporteMoneda End) End,0) + ImporteMonedaDocumento,
C.Cotizacion,
'IDMoneda'=C.IDMonedaComprobante,
'DescripcionMoneda'=C.MonedaComprobante,
'Movimiento'='OP',
'RRHH'='False',
'CodigoCuentaContable'=DA.Codigo,
'CuentaContable'=DA.Cuenta
From
VOrdenPago C
join EntregaChequeOP EC on C.IDTransaccion = EC.IDTransaccionOP
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = EC.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Debito>0 
Where EC.Anulado='False'



Union all

--Compras Importadas con cobros parciales en sistema anterior
Select
C.IDTransaccion, 
'IDProveedor'=C.IDProveedor,
'Operación'='Migracion',
'Codigo'=C.IDProveedor,
'RazonSocial'=C.Proveedor,
'Referencia'=C.Referencia ,
'Fecha'=Convert(date, '20151231'),
'Documento'=C.TipoComprobante + ' ' + convert(varchar(50),C.NroComprobante) + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end), 
'Detalle/Concepto'='Pagos parciales en sistema anterior',
'Debito'=DA.Debito,
'Credito'=DA.Credito,
'TotalComprobante' = GI.Pagado,
C.Cotizacion,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=C.Moneda,
'Movimiento'='PAG. MIGRACION',
C.RRHH,
'CodigoCuentaContable'=DA.Codigo,
'CuentaContable'=DA.Cuenta
From 
VGasto C
Join GastoImportado GI On C.IDTransaccion=GI.IDTransaccion 
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Credito>0 
Where IsNull(GI.Pagado,0)>0



﻿
CREATE view [dbo].[VExtractoMovimientoClienteFacturaCCDebito] 

As

--Ingreso
Select
V.IDTransaccion, 
'Operacion'=Concat('FAT ',v.Comprobante),
'Codigo'=V.IDCliente,
'Fecha'=V.FechaEmision,
'Documento'=Concat('FAT ',V.Comprobante),
'Detalle/Concepto'=V.Observacion,
'Debito'=V.Total,
'Credito'=0,
'Saldo'=0.00,
'SaldoSistema'=V.Saldo,
'FacturaCanceladaSistema'=V.Cancelado,
'Cotizacion'=isnull(V.Cotizacion,1),
'IDMoneda'=V.IDMoneda,
'Movimiento'='VENTA',
'ComprobanteAsociado'=V.IDTransaccion,
'CodigoCC' = DA.Codigo
From 
Venta V
--Join Cliente C on V.IDCliente=C.ID 
Left outer join FormaPagoFactura FPF on FPF.ID = V.IdFormaPagoFactura
Left Join VDetalleAsientoAgrupadoDatosMinimos DA on DA.IDTransaccion = V.IDTransaccion and Da.Debito>0 and DA.Descripcion like '%Deudor%'
where V.Procesado = 1 and V.Anulado='False'
and FPF.CancelarAutomatico = 0










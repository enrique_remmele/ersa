﻿
CREATE View [dbo].[VNotaCreditoVentaAplicacion]

As

Select
'IDTransaccion'=NCV.IDTransaccionNotaCredito, 
'IDTransaccionVenta'=V.IDTransaccion , 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
NCV.Importe,
'IDTransaccionNotaCreditoAplicacion'=0,
--'Aplicado'=(Case When NC.Aplicar='False' Then'True' Else (Case When (Select NCVA.IDTransaccionNotaCredito From NotaCreditoVentaAplicada NCVA Where NCVA.IDTransaccionNotaCredito=NCV.IDTransaccionNotaCredito) Is Null Then 'False' Else 'True' End) End)
'Aplicado'=(Case When NC.Aplicar='False' Then 'True' Else 'False' End),
V.Cliente,
NC.Sucursal,
NC.IDSucursal,
NC.IDMoneda,
NC.Moneda
From NotaCreditoVenta  NCV
Join VNotaCredito NC On NCV.IDTransaccionNotaCredito=NC.IDTransaccion
join VVenta  V on NCV.IDTransaccionVentaGenerada  = V .IDTransaccion 

Union All

Select
NCVA.IDTransaccionNotaCredito, 
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
NCVA.Importe,
NCVA.IDTransaccionNotaCreditoAplicacion ,
'Aplicado'='True',
V.CLiente,
V.Sucursal,
V.IDSucursal,
V.IDMoneda,
V.Moneda
From NotaCreditoVentaAplicada   NCVA
join VVenta  V on NCVA.IDTransaccionVenta  = V .IDTransaccion 






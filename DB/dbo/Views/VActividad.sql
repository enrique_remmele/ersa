﻿CREATE View [dbo].[VActividad]
As
Select
A.ID,
A.IDProveedor,
A.IDSucursal,
'Sucursal'=S.Descripcion,
'CodigoSucursal'=S.Codigo,
'Proveedor'=P.RazonSocial,
A.Codigo,
A.Mecanica,
A.ImporteAsignado,
'Asignado'=A.ImporteAsignado,
'Entrada'=IsNull(A.Entrada, 0),
'Salida'=IsNull(A.Salida,0),
'DescuentoMaximo'=IsNull(A.DescuentoMaximo, 0),
'Desc. Max.'=IsNull(A.DescuentoMaximo, 0),
'Tactico'=IsNull(A.Tactico, 0),
'Acuerdo'=IsNull(A.Acuerdo,0),
'Total'=IsNull(A.Total, 0),

--'Tactico'=IsNull((Select Sum(D.Descuento) From VDescuento D Where D.IDActividad=A.ID And D.Tipo='TAC' And D.Anulado='False'), 0),
--'Acuerdo'=IsNull((Select Sum(D.Descuento) From VDescuento D Where D.IDActividad=A.ID And D.Tipo='ACU' And D.Anulado='False'), 0),
--'Total'=0,

'PorcentajeUtilizado'=IsNull(A.PorcentajeUtilizado, 0),
'Saldo'=IsNull(A.Saldo,0),
'Excedente'=IsNull(A.Excedente, 0),
'Relacionado'=(Select Case When(Select Top(1) DPDT.IDActividad From DetallePlanillaDescuentoTactico DPDT Where DPDT.IDActividad=A.ID) IS Null Then 'False' Else 'True' End)

From Actividad A
Join Proveedor P On A.IDProveedor=P.ID
Join Sucursal S On A.IDSucursal=S.ID


﻿CREATE view [dbo].[VComprobanteRetencion]

As


--Comprobantes de una OP

Select 

RI.IDTransaccion,
OPE.IDTransaccionOrdenPago,
'IDTransaccionEgreso'=OPE.IDTransaccion,
'ID'='',
'TipoyComprobante'=OPE.TipoComprobante+''+OPE.NroComprobante,
'Fecha'=convert(varchar(50),OPE.Fecha,5),
OPE.Credito,
'Gravado5'=DID.[GRAVADO5%],
'IVA5'=DID.[IVA5%] ,
'Gravado10'=DID.[GRAVADO10%] ,
'IVA10'=DID.[IVA10%] ,
'Total5'=DID.[GRAVADO5%] + DID.[IVA5%],
'Total10'=DID.[GRAVADO10%] - DID.[IVA10%],
'TotalGravado'=OPE.TotalDiscriminado,
'TotalIVA'=OPE.TotalImpuesto,
OPE.Total,
'PorcRetencion'=(Select Top 1 CompraPorcentajeRetencion From Configuraciones),
'RetencionIVA'=OPE.RetencionIVA ,
'PorcRenta'=0,
'Renta'=0,
'NumeroOp'=OPE.Numero,
RI.IDProveedor,
RI.Proveedor,
'Comprobante'=OPE.NroComprobante

From VOrdenPagoEgreso OPE 
Left Outer Join VDetalleImpuestoDesglosadoTotales2 DID On OPE.IDTransaccion=DID.IDTransaccion
Left Outer Join VRetencionIVA RI on OPE.IDTransaccionOrdenPago=RI.IDTransaccionOrdenPago
LEFT OUTER JOIN VEgreso E ON OPE.IDTransaccion=E.IDTransaccion
LEFT OUTER JOIN TipoComprobante TC ON E.IDTipoComprobante=TC.ID

WHERE TC.LibroCompra='TRue'

Union All

--Comprobantes sin OP

Select 

RI.IDTransaccion,
'IDTransaccionOrdenPago'=NULL,
'IDTransaccionEgreso'=DR.IDTransaccion,
'ID'='',
DR.TipoyComprobante,
'Fecha'=CONVERT(varchar(50), DR.Fecha, 5),
'Credito'=NULL,
Gravado5,
IVA5,
Gravado10,
IVA10,
Total5,
Total10,
'TotalGravado'=(Gravado10+Gravado5),
'TotalIVA'=(IVA5+IVA10),
'Total'=(Total5+Total10),
'PorcRetencion'=(Select Top 1 CompraPorcentajeRetencion From Configuraciones),
'RetencionIVA'=RetencionIVA ,
'PorcRenta'=0,
'Renta'=0,
'NumeroOP'='',
RI.IDProveedor, 
RI.Proveedor,
'Comprobante'=REPLACE(DR.TipoyComprobante, 'FAT', '')

From DetalleRetencion DR
Left Outer Join VRetencionIVA RI on DR.IDTransaccion=RI.IDTransaccion
﻿CREATE View [dbo].[VPagoChequeCliente]

As

Select 

---Cabecera
PC .IDTransaccion,
PC.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=S.CodigoCiudad,
PC.Numero,
'Num'=PC.Numero,

PC.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
PC.NroComprobante,
'Comprobante'=PC .NroComprobante,

PC.Fecha,
'Fec'=CONVERT(varchar(50), PC.Fecha, 5),
PC.Total,
PC.Observacion,
PC.Anulado,

'Estado'=(Case When PC.Anulado='True'Then 'Anulado'Else'---'End),


--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=PC.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=PC.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=PC.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=PC.IDTransaccion)

 

 
From PagoChequeCliente  PC
Join Transaccion T On PC.IDTransaccion=T.ID
Join VSucursal S On PC.IDSucursal=S.ID
Join TipoComprobante TC On PC.IDTipoComprobante=TC.ID











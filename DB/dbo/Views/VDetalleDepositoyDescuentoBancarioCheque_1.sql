﻿CREATE View [dbo].[VDetalleDepositoyDescuentoBancarioCheque]
As

Select

C.IDTransaccion,
'ID'=0,

'Sel'='True',
C.CodigoTipo, 
'Comprobante'=C.NroCheque ,
CB.IDBanco,
'Banco'=(Select Descripcion from Banco where id = CB.IDBanco),
C.BancoLocal,
'Importe'= C.Importe ,
C.Cliente,
'Referencia'=C.CodigoCliente,
C.CuentaBancaria,
C.Estado,
C.Numero,
C.IDSucursal,
C.Ciudad,
C.NroCheque,

DDB.IDTransaccionCheque,
'IDTransaccionDepositoBancario' = DDB.IDTransaccionDepositoBancario ,
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
'Depositado'=C.Depositado,

'IDCuentaBancariaDeposito'=DB.IDCuentaBancaria,
DB.Fecha,
'NroOperacion'=DB.Numero,
DB.NroComprobante,
DB.Sucursal,

'NroCobranza'=(Select Top 1  CC.Numero From ChequeCliente  CHQ
Join FormaPago FP on CHQ.IDTransaccion=FP.IDTransaccionCheque
Join CobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Where CHQ.IDTransaccion=C.IDTransaccion),
C.Diferido

From DetalleDepositoBancario DDB
Join Transaccion T On DDB.IDTransaccionDepositoBancario =T.ID
Join VDepositoBancario DB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion 
Join VChequeCliente  C on DDB.IDTransaccionCheque  = C .IDTransaccion  
left outer join CuentaBancaria CB on CB.ID = DB.IDCuentaBancaria
Union All

Select

DDC.IDTransaccionChequeCliente,
'ID'=0,

'Sel'='True',
C.CodigoTipo, 
'Comprobante'=C.NroCheque ,
CB.IDBanco,
'Banco'=(Select Descripcion from Banco where id = CB.IDBanco),
C.BancoLocal,
'Importe'= C.Importe ,
C.Cliente,
'Referencia'=C.CodigoCliente,
C.CuentaBancaria,
C.Estado,
C.Numero,
C.IDSucursal,
C.Ciudad,
C.NroCheque,

0,
'IDTransaccionDepositoBancario' = Null,
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
'Depositado'=C.Depositado,

'IDCuentaBancariaDeposito'=Null,
DC.Fecha,
'NroOperacion'=DC.Numero,
DC.NroComprobante,
DC.IDSucursal,

'NroCobranza'=(Select Top 1  CC.Numero From ChequeCliente  CHQ
Join FormaPago FP on CHQ.IDTransaccion=FP.IDTransaccionCheque
Join CobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Where CHQ.IDTransaccion=C.IDTransaccion),
C.Diferido

From DescuentoCheque DC
Join DetalleDescuentoCheque DDC on DDC.IDTransaccionDescuentoCheque = DC.IDTransaccion
Join VChequeCliente  C on DDC.IDTransaccionChequeCliente  = C.IDTransaccion
Join Transaccion T On DC.IDTransaccion = T.ID
left outer join CuentaBancaria CB on CB.ID = DC.IDCuentaBancaria






















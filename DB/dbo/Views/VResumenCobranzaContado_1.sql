﻿
CREATE View [dbo].[VResumenCobranzaContado]
As

--EFECTIVOS
Select
IDSucursal,
Fecha,
'Lote'=Comprobante,
'Efectivo'=SUM(Importe),
'Cantidad'=COUNT(Comprobante),
'Efectivizacion'=0,
'CantidadEfectivizacion'=0,
'OrdenPago'=0,
'SaldoEfectivo'=0,
'ChequesAlDia'=0,
'CantidadChequesAlDia'=0,
'ChequesDiferidos'=0,
'CantidadChequesDiferidos'=0,
'OtrosDocumentos'=0,
'CantidadOtrosDocumentos'=0

From VEfectivo
Group By IDSucursal, Fecha, Comprobante

Union All

--EFECTIVIZACIONES
Select
IDSucursal,
Fecha,
'Lote'=Comprobante,
'Efectivo'=0,
'Cantidad'=0,
'Efectivizacion'=SUM(Importe)*-1,
'CantidadEfectivizacion'=COUNT(Comprobante),
'OrdenPago'=0,
'SaldoEfectivo'=0,
'ChequesAlDia'=SUM(Importe),
'CantidadChequesAlDia'=0,
'ChequesDiferidos'=0,
'CantidadChequesDiferidos'=0,
'OtrosDocumentos'=0,
'CantidadOtrosDocumentos'=0

From
VDetalleEfectivizacion
Group By IDSucursal, Fecha, Comprobante

Union All

--ORDENES DE PAGO
Select
EFE.IDSucursal,
EFE.Fecha,
'Lote'=EFE.Comprobante,
'Efectivo'=0,
'Cantidad'=0,
'Efectivizacion'=0,
'CantidadEfectivizacion'=0,
'OrdenPago'=SUM(OPE.Importe),
'SaldoEfectivo'=0,
'ChequesAlDia'=0,
'CantidadChequesAlDia'=0,
'ChequesDiferidos'=0,
'CantidadChequesDiferidos'=0,
'OtrosDocumentos'=0,
'CantidadOtrosDocumentos'=0

From
VOrdenPagoEfectivo OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join Efectivo EFE On OPE.IDTransaccion=EFE.IDTransaccion
Where OP.Anulado='False'
Group By EFE.IDSucursal, EFE.Fecha, EFE.Comprobante

Union All

--CHEQUES AL DIA
--CREDITO
Select
C.IDSucursal,
C.FechaEmision,
'Lote'=C.NroPlanilla,
'Efectivo'=0,
'Cantidad'=0,
'Efectivizacion'=0,
'CantidadEfectivizacion'=0,
'OrdenPago'=0,
'SaldoEfectivo'=0,
'ChequesAlDia'=SUM(FP.ImporteCheque),
'CantidadChequesAlDia'=COUNT(CHQ.Numero),
'ChequesDiferidos'=0,
'CantidadChequesDiferidos'=0,
'OtrosDocumentos'=0,
'CantidadOtrosDocumentos'=0

From
FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join VChequeCliente CHQ On FP.IDTransaccionCheque=CHQ.IDTransaccion
Where CHQ.Diferido='False'
and C.Anulado = 'False'
and C.Procesado = 'True'
Group By C.IDSucursal, C.FechaEmision, C.NroPlanilla

Union All

--CONTADO
Select
C.IDSucursal,
C.Fecha,
'Lote'=C.Comprobante,
'Efectivo'=0,
'Cantidad'=0,
'Efectivizacion'=0,
'CantidadEfectivizacion'=0,
'OrdenPago'=0,
'SaldoEfectivo'=0,
'ChequesAlDia'=SUM(FP.ImporteCheque),
'CantidadChequesAlDia'=COUNT(CHQ.Numero),
'ChequesDiferidos'=0,
'CantidadChequesDiferidos'=0,
'OtrosDocumentos'=0,
'CantidadOtrosDocumentos'=0

From
FormaPago FP
Join CobranzaContado C On FP.IDTransaccion=C.IDTransaccion
Join VChequeCliente CHQ On FP.IDTransaccionCheque=CHQ.IDTransaccion
Where CHQ.Diferido='False'
and C.Anulado = 'False'
and C.Procesado = 'True'
Group By C.IDSucursal, C.Fecha, C.Comprobante

Union All

--CHEQUES DIFERIDOS
--Credito
Select
C.IDSucursal,
C.FechaEmision,
'Lote'=C.NroPlanilla,
'Efectivo'=0,
'Cantidad'=0,
'Efectivizacion'=0,
'CantidadEfectivizacion'=0,
'OrdenPago'=0,
'SaldoEfectivo'=0,
'ChequesAlDia'=0,
'CantidadChequesAlDia'=0,
'ChequesDiferidos'=SUM(FP.ImporteCheque),
'CantidadChequesDiferidos'=COUNT(CHQ.Numero),
'OtrosDocumentos'=0,
'CantidadOtrosDocumentos'=0

From
FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join VChequeCliente CHQ On FP.IDTransaccionCheque=CHQ.IDTransaccion
Where CHQ.Diferido='True'
and C.Anulado = 'False'
and C.Procesado = 'True'
Group By C.IDSucursal, C.FechaEmision, C.NroPlanilla

Union All

--Contado
Select
C.IDSucursal,
C.Fecha,
'Lote'=C.Comprobante,
'Efectivo'=0,
'Cantidad'=0,
'Efectivizacion'=0,
'CantidadEfectivizacion'=0,
'OrdenPago'=0,
'SaldoEfectivo'=0,
'ChequesAlDia'=0,
'CantidadChequesAlDia'=0,
'ChequesDiferidos'=SUM(FP.ImporteCheque),
'CantidadChequesDiferidos'=COUNT(CHQ.Numero),
'OtrosDocumentos'=0,
'CantidadOtrosDocumentos'=0

From
FormaPago FP
Join CobranzaContado C On FP.IDTransaccion=C.IDTransaccion
Join VChequeCliente CHQ On FP.IDTransaccionCheque=CHQ.IDTransaccion
Where CHQ.Diferido='True'
and C.Anulado = 'False'
and C.Procesado = 'True'
Group By C.IDSucursal, C.Fecha, C.Comprobante

Union All

--OTROS
--Credito
Select
C.IDSucursal,
C.FechaEmision,
'Lote'=C.NroPlanilla,
'Efectivo'=0,
'Cantidad'=0,
'Efectivizacion'=0,
'CantidadEfectivizacion'=0,
'OrdenPago'=0,
'SaldoEfectivo'=0,
'ChequesAlDia'=0,
'CantidadChequesAlDia'=0,
'ChequesDiferidos'=0,
'CantidadChequesDiferidos'=0,
'OtrosDocumentos'=Sum(FP.ImporteDocumento),
'CantidadOtrosDocumentos'=COUNT(FPD.ID)

From
FormaPago FP
Join CobranzaCredito C On FP.IDTransaccion=C.IDTransaccion
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
where C.Anulado = 'False'
and C.Procesado = 'True'
Group By C.IDSucursal, C.FechaEmision, C.NroPlanilla

Union All

--OTROS
--Contado
Select
C.IDSucursal,
C.Fecha,
'Lote'=Convert(varchar(50), C.NroComprobante),
'Efectivo'=0,
'Cantidad'=0,
'Efectivizacion'=0,
'CantidadEfectivizacion'=0,
'OrdenPago'=0,
'SaldoEfectivo'=0,
'ChequesAlDia'=0,
'CantidadChequesAlDia'=0,
'ChequesDiferidos'=0,
'CantidadChequesDiferidos'=0,
'OtrosDocumentos'=Sum(FP.ImporteDocumento),
'CantidadOtrosDocumentos'=COUNT(FPD.ID)

From
FormaPago FP
Join CobranzaContado C On FP.IDTransaccion=C.IDTransaccion
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
where C.Anulado = 'False'
and C.Procesado = 'True'
Group By C.IDSucursal, C.Fecha, C.NroComprobante







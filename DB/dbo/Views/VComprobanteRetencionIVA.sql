﻿


CREATE view [dbo].[VComprobanteRetencionIVA]
As

select 
RI.IDTransaccion,
OPE.IDTransaccionOrdenPago,
'IDTransaccionEgreso'=OPE.IDTransaccion,
'ID'='',
'TipoyComprobante'=OPE.TipoComprobante+''+OPE.NroComprobante,
'Fecha'=convert(varchar(50),OPE.Fecha,5),
OPE.Credito,
'Gravado5'=DID.[GRAVADO5%],
'IVA5'=DID.[IVA5%] ,
'Gravado10'=DID.[GRAVADO10%] ,
'IVA10'=DID.[IVA10%] ,
'Total5'=DID.[GRAVADO5%] + DID.[IVA5%],
'Total10'=DID.[GRAVADO10%] - DID.[IVA10%],
'TotalGravado'=OPE.TotalDiscriminado,
'TotalIVA'=OPE.TotalImpuesto,
OPE.Total,
'PorcRetencion'=(Select Top 1 CompraPorcentajeRetencion From Configuraciones),
'RetencionIVA'=OPE.RetencionIVA ,
'PorcRenta'=0,
'Renta'=0,
'NumeroOp'=OPE.Numero,
RI.IDProveedor,
RI.Proveedor,
'Comprobante'=OPE.NroComprobante
from vRetencionIVA RI
left join vDetalleRetencion DR ON RI.IDTransaccion = DR.IDTransaccion
join VOrdenPagoEgreso OPE on OPE.IDTransaccionOrdenPago = RI.IDTransaccionOrdenPago 
                         and OPE.IDTransaccion = RI.IDTransaccionEgreso
						 --and OPE.RetencionIVA = RI.TotalIVA
join VDetalleImpuestoDesglosadoTotales2 DID on OPE.IDTransaccion = DID.IDTransaccion 
join VEgreso E on OPE.IDTransaccion = E.IDTransaccion 
join TipoComprobante TC on TC.ID = E.IDTipoComprobante
                       and TC.LibroCompra='TRue'

--where RI.IDTransaccion=1387587



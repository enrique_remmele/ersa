﻿

CREATE View [dbo].[vMacheoFacturaTicketDetalle]
As
Select 
DM.*,
--Macheo
'NroOperacionMacheo'=MFT.Numero,
MFT.NroAcuerdo,
'ReferenciaSucursal'= S.Codigo,
'Sucursal'= S.Descripcion,
'ReferenciaTipoComprobante'= TC.Codigo,
'TipoComprobanteMacheo'= TC.Descripcion,
MFT.Fecha,
'ReferenciaProveedor'=P.Referencia,
'Proveedor'=P.RazonSocial,
'ReferenciaMoneda'=M.Referencia,
'Moneda'=M.Descripcion,
MFT.Cotizacion,
MFT.Observacion,

--Acuerdo
'PrecioAcuerdo'=A.Precio,
'ReferenciaProducto'=Pr.Referencia,
'Producto'= Pr.Descripcion,

--Gasto
'Gasto'= IsNull((Select Comprobante from VGasto where IDTransaccion = MFT.IDTransaccionGasto),''),
'FechaGasto'=IsNull((Select Fecha from VGasto where IDTransaccion = MFT.IDTransaccionGasto),''),

--Ticket
'NroOperacionTicket'=T.Numero,
'FechaTicket'=T.Fecha,
'CotizacionTicket'= T.Cotizacion,
T.PesoBascula,
T.PesoRemision,
T.Diferencia,
T.Total,
T.TotalDiscriminado,
T.TotalUS,
T.TotalDiscriminadoUS,
A.IDProveedor,
A.IDProducto

From DetalleMacheo DM
Join MacheoFacturaTicket MFT on MFT.IDTransaccion = DM.IDTransaccionMacheo
join Acuerdo A on MFT.NroAcuerdo = A.NroAcuerdo
Join TicketBascula T on T.IDTransaccion = DM.IDTransaccionTicket
Join Sucursal S on S.ID = MFT.IDSucursal
Join TipoComprobante TC on TC.ID = T.IDTipoComprobante
Join Proveedor P on P.ID = A.IDProveedor
Join Moneda M on M.ID = MFT.IDMoneda
Join Producto Pr on Pr.ID = A.IDProducto
















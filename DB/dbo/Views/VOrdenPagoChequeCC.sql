﻿
CREATE View [dbo].[VOrdenPagoChequeCC]
As
--1
Select 
OP.IDTransaccion,
OP.Numero,
'Fecha'=OP.Fecha,
OP.Observacion,
OP.PagoProveedor,
OP.AnticipoProveedor,
OP.EgresoRendir,
--Sucursal

OP.IDSucursal,
'IDMoneda'=IsNull(OP.IDMoneda, CB.IDMoneda),

--Proveedor
'IDProveedor'=IsNull(OP.IDProveedor, 0),
'Proveedor'=ISNull(P.RazonSocial, ''),
'RUC'=IsNull(P.RUC, ''),
'Referencia'=P.Referencia,

--Cheque
C.IdBanco,
'Cheque'=Case When (C.IDTransaccion) Is Null Then 'False' Else 'True' End,
C.IDCuentaBancaria,
'BancoReferencia'=C.Referencia,
C.CuentaBancaria,
C.Banco,
C.Mon,
C.NroCheque,
'FechaCheque'=CONVERT(varchar(50), C.Fecha, 6),
'FechaChequeFiltro'=C.FechaFiltro,
'Fec Chq'=C.Fec,
C.FechaPago,
'FecPago'=C.[Fec Pago],
C.Cotizacion,
'ImporteMoneda' = case when C.Anulado = 0 THEN C.ImporteMoneda ELSE 0 END,
'Importe' = case when C.Anulado = 0 THEN C.Importe ELSE 0 END,
C.Diferido,
C.FechaVencimiento,
C.[Fec. Venc.],
C.ALaOrden,
C.Conciliado,
'ChequeAnulado'=C.Anulado,
'ChequeEstado'=C.Estado,

--Efectivo
'CotizacionEfectivo'=IsNull(OP.CotizacionEfectivo, 0),
'IDMonedaEfectivo'=IsNull(OP.IDMonedaEfectivo, 0),
'ImporteMonedaEfectivo'=IsNull(OP.ImporteMonedaEfectivo, 0),

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,

--Anulacion
OP.Anulado,
--Impreso
OP.Impreso,

--Tipo Orden de Pago
'Tipo'=Case When (OP.PagoProveedor) = 'True' Then 'Pago Proveedor' When (OP.AnticipoProveedor)='True' Then 'Anticipo Proveedor' When(OP.EgresoRendir)='True' Then 'Egreso a Rendir'  End,
'Retencion'='',
'MontoRetencion'= (Case when OP.AplicarRetencion = 'False' then 0 else
			 (isnull(Case When (OP.IDMoneda) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) Else 
					(Case When (isnull(CB.IDMoneda,ISNULL(OP.IdMonedaDocumento,1))) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) / OP.Cotizacion Else 
					(select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) End) End,0))end),
'FechaEntrega'=(Case when (OP.EgresoRendir)='True' then OP.Fecha else EC.FechaEntrega end),
'RetiradoPor'=(Case when (OP.EgresoRendir)='True' then 'Egreso a Rendir no registra entrega' else EC.RetiradoPor end),
OP.DiferenciaCambio,

'FechaCobroProveedor'=VCE.Fecha,

DA.Codigo,
'CuentaContable'='',
'TotalCC'=(select dbo.FImporteProporcional(DA.Credito,((OPE.Importe * OPE.Cotizacion)),(OPE.Total * OPE.Cotizacion)))
from vdetalleasientoAgrupado DA
join VOrdenPagoEgreso OPE on DA.IDTransaccion = OPE.IDTransaccion
join OrdenPago OP on OPE.IDTransaccionOrdenPago = OP.IDTransaccion
Join Transaccion T On OP.IDTransaccion=T.ID
Join TipoComprobante TC On OP.IDTipoComprobante=TC.ID
Join Sucursal S On OP.IDSucursal=S.ID
Left Outer Join Proveedor P On OP.IDProveedor=P.ID
Left Outer Join vCheque C On OP.IDTransaccion=C.IDTransaccion
Left Outer Join VCuentaBancaria CB on C.IDCuentaBancaria =CB.ID
Left Outer Join Moneda M On OP.IDMoneda=M.ID
left outer join vEntregaChequeOP EC on EC.IdtransaccionOP = OP.IdTransaccion
left outer join VencimientoChequeEmitido VCE on VCE.IDTransaccionOP = OP.IDTransaccion
where  Da.Credito > 1
and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%')
and OPE.RRHH = 'False'
And OP.AnticipoProveedor = 'False'


union all

--2
Select 
OP.IDTransaccion,
OP.Numero,
'Fecha'=OP.Fecha,
OP.Observacion,
OP.PagoProveedor,
OP.AnticipoProveedor,
OP.EgresoRendir,
--Sucursal

OP.IDSucursal,
'IDMoneda'=IsNull(OP.IDMoneda, CB.IDMoneda),
--Proveedor
'IDProveedor'=IsNull(OP.IDProveedor, 0),
'Proveedor'=ISNull(P.RazonSocial, ''),
'RUC'=IsNull(P.RUC, ''),
'Referencia'=P.Referencia,

--Cheque
C.IdBanco,
'Cheque'=Case When (C.IDTransaccion) Is Null Then 'False' Else 'True' End,
C.IDCuentaBancaria,
'BancoReferencia'=C.Referencia,
C.CuentaBancaria,
C.Banco,
C.Mon,
C.NroCheque,
'FechaCheque'=CONVERT(varchar(50), C.Fecha, 6),
'FechaChequeFiltro'=C.FechaFiltro,
'Fec Chq'=C.Fec,
C.FechaPago,
'FecPago'=C.[Fec Pago],
C.Cotizacion,
'ImporteMoneda' = case when C.Anulado = 0 THEN C.ImporteMoneda ELSE 0 END,
'Importe' = case when C.Anulado = 0 THEN C.Importe ELSE 0 END,
C.Diferido,
C.FechaVencimiento,
C.[Fec. Venc.],
C.ALaOrden,
C.Conciliado,
'ChequeAnulado'=C.Anulado,
'ChequeEstado'=C.Estado,

--Efectivo
'CotizacionEfectivo'=IsNull(OP.CotizacionEfectivo, 0),
'IDMonedaEfectivo'=IsNull(OP.IDMonedaEfectivo, 0),
'ImporteMonedaEfectivo'=IsNull(OP.ImporteMonedaEfectivo, 0),

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,

--Anulacion
OP.Anulado,
--Impreso
OP.Impreso,

--Tipo Orden de Pago
'Tipo'=Case When (OP.PagoProveedor) = 'True' Then 'Pago Proveedor' When (OP.AnticipoProveedor)='True' Then 'Anticipo Proveedor' When(OP.EgresoRendir)='True' Then 'Egreso a Rendir'  End,
'Retencion'='',
'MontoRetencion'= (Case when OP.AplicarRetencion = 'False' then 0 else
			 (isnull(Case When (OP.IDMoneda) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) Else 
					(Case When (isnull(CB.IDMoneda,ISNULL(OP.IdMonedaDocumento,1))) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) / OP.Cotizacion Else 
					(select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) End) End,0))end),

'FechaEntrega'=(Case when (OP.EgresoRendir)='True' then OP.Fecha else EC.FechaEntrega end),
'RetiradoPor'=(Case when (OP.EgresoRendir)='True' then 'Egreso a Rendir no registra entrega' else EC.RetiradoPor end),
OP.DiferenciaCambio,

'FechaCobroProveedor'=VCE.Fecha,

DA.Codigo,
'CuentaContable'='',
'TotalCC'=(select dbo.FImporteProporcional(DA.Credito,((OPE.Importe * OPE.Cotizacion)),(OPE.Total * OPE.Cotizacion)))
from vdetalleasientoAgrupado DA
join VOrdenPagoEgreso OPE on DA.IDTransaccion = OPE.IDTransaccion
join OrdenPago OP on OPE.IDTransaccionOrdenPago = OP.IDTransaccion
Join Transaccion T On OP.IDTransaccion=T.ID
Join TipoComprobante TC On OP.IDTipoComprobante=TC.ID
Join Sucursal S On OP.IDSucursal=S.ID
Left Outer Join Proveedor P On OP.IDProveedor=P.ID
Left Outer Join vCheque C On OP.IDTransaccion=C.IDTransaccion
Left Outer Join VCuentaBancaria CB on C.IDCuentaBancaria =CB.ID
Left Outer Join Moneda M On OP.IDMoneda=M.ID
left outer join vEntregaChequeOP EC on EC.IdtransaccionOP = OP.IdTransaccion
left outer join VencimientoChequeEmitido VCE on VCE.IDTransaccionOP = OP.IDTransaccion
where  Da.Credito > 1
and Da.Descripcion like 'Proveedo%'
and OPE.RRHH = 'True'
And OP.AnticipoProveedor = 'False'



union all
--3

Select 
OP.IDTransaccion,
OP.Numero,
'Fecha'=OP.Fecha,
OP.Observacion,
OP.PagoProveedor,
OP.AnticipoProveedor,
OP.EgresoRendir,
--Sucursal

OP.IDSucursal,
'IDMoneda'=IsNull(OP.IDMoneda, CB.IDMoneda),
--Proveedor
'IDProveedor'=IsNull(OP.IDProveedor, 0),
'Proveedor'=ISNull(P.RazonSocial, ''),
'RUC'=IsNull(P.RUC, ''),
'Referencia'=P.Referencia,

--Cheque
C.IdBanco,
'Cheque'=Case When (C.IDTransaccion) Is Null Then 'False' Else 'True' End,
C.IDCuentaBancaria,
'BancoReferencia'=C.Referencia,
C.CuentaBancaria,
C.Banco,
C.Mon,
C.NroCheque,
'FechaCheque'=CONVERT(varchar(50), C.Fecha, 6),
'FechaChequeFiltro'=C.FechaFiltro,
'Fec Chq'=C.Fec,
C.FechaPago,
'FecPago'=C.[Fec Pago],
C.Cotizacion,
'ImporteMoneda' = case when C.Anulado = 0 THEN C.ImporteMoneda ELSE 0 END,
'Importe' = case when C.Anulado = 0 THEN C.Importe ELSE 0 END,
C.Diferido,
C.FechaVencimiento,
C.[Fec. Venc.],
C.ALaOrden,
C.Conciliado,
'ChequeAnulado'=C.Anulado,
'ChequeEstado'=C.Estado,

--Efectivo
'CotizacionEfectivo'=IsNull(OP.CotizacionEfectivo, 0),
'IDMonedaEfectivo'=IsNull(OP.IDMonedaEfectivo, 0),
'ImporteMonedaEfectivo'=IsNull(OP.ImporteMonedaEfectivo, 0),

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,

--Anulacion
OP.Anulado,
--Impreso
OP.Impreso,

--Tipo Orden de Pago
'Tipo'=Case When (OP.PagoProveedor) = 'True' Then 'Pago Proveedor' When (OP.AnticipoProveedor)='True' Then 'Anticipo Proveedor' When(OP.EgresoRendir)='True' Then 'Egreso a Rendir'  End,
'Retencion'='',
'MontoRetencion'= (Case when OP.AplicarRetencion = 'False' then 0 else
			 (isnull(Case When (OP.IDMoneda) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) Else 
					(Case When (isnull(CB.IDMoneda,ISNULL(OP.IdMonedaDocumento,1))) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) / OP.Cotizacion Else 
					(select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) End) End,0))end),
'FechaEntrega'=(Case when (OP.EgresoRendir)='True' then OP.Fecha else EC.FechaEntrega end),
'RetiradoPor'=(Case when (OP.EgresoRendir)='True' then 'Egreso a Rendir no registra entrega' else EC.RetiradoPor end),
OP.DiferenciaCambio,

'FechaCobroProveedor'=VCE.Fecha,

P.CuentaContableAnticipo,
'CuentaContable'='',
'TotalCC'= (select dbo.FImporteProporcional(DA.Credito,((OPE.Importe * OPE.Cotizacion)),(OPE.Total * OPE.Cotizacion)))
from vOrdenPago OP 
Join vProveedor P on OP.IDProveedor = P.ID
join VOrdenPagoEgreso OPE on OP.IDTransaccion = OPE.IDTransaccionOrdenPago
left outer join vdetalleasientoAgrupado DA on OPE.IDTransaccion = DA.IDTransaccion and Da.Credito > 1 and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%')
Join Transaccion T On OP.IDTransaccion=T.ID
Join TipoComprobante TC On OP.IDTipoComprobante=TC.ID
Join Sucursal S On OP.IDSucursal=S.ID
Left Outer Join vCheque C On OP.IDTransaccion=C.IDTransaccion
Left Outer Join VCuentaBancaria CB on C.IDCuentaBancaria =CB.ID
Left Outer Join Moneda M On OP.IDMoneda=M.ID
left outer join vEntregaChequeOP EC on EC.IdtransaccionOP = OP.IdTransaccion
left outer join VencimientoChequeEmitido VCE on VCE.IDTransaccionOP = OP.IDTransaccion
where OP.AnticipoProveedor = 'True'



--4

union all


Select 
OP.IDTransaccion,
OP.Numero,
'Fecha'=OP.Fecha,
OP.Observacion,
OP.PagoProveedor,
OP.AnticipoProveedor,
OP.EgresoRendir,
--Sucursal

OP.IDSucursal,
'IDMoneda'=IsNull(OP.IDMoneda, CB.IDMoneda),
--Proveedor
'IDProveedor'=IsNull(OP.IDProveedor, 0),
'Proveedor'=ISNull(P.RazonSocial, ''),
'RUC'=IsNull(P.RUC, ''),
'Referencia'=P.Referencia,

--Cheque
C.IdBanco,
'Cheque'=Case When (C.IDTransaccion) Is Null Then 'False' Else 'True' End,
C.IDCuentaBancaria,
'BancoReferencia'=C.Referencia,
C.CuentaBancaria,
C.Banco,
C.Mon,
C.NroCheque,
'FechaCheque'=CONVERT(varchar(50), C.Fecha, 6),
'FechaChequeFiltro'=C.FechaFiltro,
'Fec Chq'=C.Fec,
C.FechaPago,
'FecPago'=C.[Fec Pago],
C.Cotizacion,
'ImporteMoneda' = case when C.Anulado = 0 THEN C.ImporteMoneda ELSE 0 END,
'Importe' = case when C.Anulado = 0 THEN C.Importe ELSE 0 END,
C.Diferido,
C.FechaVencimiento,
C.[Fec. Venc.],
C.ALaOrden,
C.Conciliado,
'ChequeAnulado'=C.Anulado,
'ChequeEstado'=C.Estado,

--Efectivo
'CotizacionEfectivo'=IsNull(OP.CotizacionEfectivo, 0),
'IDMonedaEfectivo'=IsNull(OP.IDMonedaEfectivo, 0),
'ImporteMonedaEfectivo'=IsNull(OP.ImporteMonedaEfectivo, 0),

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,

--Anulacion
OP.Anulado,
--Impreso
OP.Impreso,

--Tipo Orden de Pago
'Tipo'=Case When (OP.PagoProveedor) = 'True' Then 'Pago Proveedor' When (OP.AnticipoProveedor)='True' Then 'Anticipo Proveedor' When(OP.EgresoRendir)='True' Then 'Egreso a Rendir'  End,
'Retencion'='',
'MontoRetencion'= (Case when OP.AplicarRetencion = 'False' then 0 else
			 (isnull(Case When (OP.IDMoneda) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) Else 
					(Case When (isnull(CB.IDMoneda,ISNULL(OP.IdMonedaDocumento,1))) = 1 Then (select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) / OP.Cotizacion Else 
					(select Sum(OPE.Retencion) from OrdenPagoEgreso OPE where OPE.IDTransaccionOrdenPago = OP.IDTransaccion) End) End,0))end),

'FechaEntrega'=(Case when (OP.EgresoRendir)='True' then OP.Fecha else EC.FechaEntrega end),
'RetiradoPor'=(Case when (OP.EgresoRendir)='True' then 'Egreso a Rendir no registra entrega' else EC.RetiradoPor end),
OP.DiferenciaCambio,

'FechaCobroProveedor'=VCE.Fecha,

P.CuentaContableAnticipo,
'CuentaContable'='',
'TotalCC'= (Case when Cheque = 'True' then OP.Total else OP.ImporteDocumento end)--(select dbo.FImporteProporcional(DA.Credito,((OPE.Importe * OPE.Cotizacion)),(OPE.Total * OPE.Cotizacion)))
from vOrdenPago OP 
Join vProveedor P on OP.IDProveedor = P.ID
--left outer join VOrdenPagoEgreso OPE on OP.IDTransaccion = OPE.IDTransaccionOrdenPago
--left outer join vdetalleasientoAgrupado DA on OP.IDTransaccion = DA.IDTransaccion and Da.Credito > 1 and (Da.Descripcion like 'Proveedo%' or Da.Descripcion like 'Anticipo%')
Join Transaccion T On OP.IDTransaccion=T.ID
Join TipoComprobante TC On OP.IDTipoComprobante=TC.ID
Join Sucursal S On OP.IDSucursal=S.ID
Left Outer Join vCheque C On OP.IDTransaccion=C.IDTransaccion
Left Outer Join VCuentaBancaria CB on C.IDCuentaBancaria =CB.ID
Left Outer Join Moneda M On OP.IDMoneda=M.ID
left outer join vEntregaChequeOP EC on EC.IdtransaccionOP = OP.IdTransaccion
left outer join VencimientoChequeEmitido VCE on VCE.IDTransaccionOP = OP.IDTransaccion
where OP.AnticipoProveedor = 'True'
and OP.IDTransaccion not in (select idtransaccionOrdenPago from OrdenPagoEgreso)




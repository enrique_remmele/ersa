﻿
CREATE View [dbo].[VFormularioImpresionTerminal] 
As
Select

--Terminal
FIT.IDTerminal,
'Terminal'=T.Descripcion,

--Formulario
FIT.IDFormularioImpresion,
'Formulario'=FI.Descripcion,
'Impresion'=I.Descripcion,
I.IDOperacion,
--Impresora
FIT.Impresora


From FormularioImpresionTerminal FIT
Join Terminal T On FIT.IDTerminal=T.ID
Join FormularioImpresion FI On FIT.IDFormularioImpresion=FI.IDFormularioImpresion
Join Impresion I On FI.IDImpresion = I.IDImpresion

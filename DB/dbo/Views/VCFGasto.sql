﻿CREATE View [dbo].[VCFGasto]
As
Select

V.*,
CFG.BuscarEnProveedor,

--Tipo Cuenta Fija
CFG.IDTipoCuentaFija,
'TipoCuentaFija'=TC.Tipo,
'CuentaFija'=TC.Descripcion,
TC.IDImpuesto,

--Tipo de Cuenta
TC.Campo,
TC.IncluirDescuento,
TC.IncluirImpuesto,

'FondoFijo'=IsNull(CFG.FondoFijo, 'False'),
'Tipo'=Case When IsNull(CFG.FondoFijo, 'False') = 'True' Then 'FONDO FIJO' Else 'PROVEEDORES' End

From VCF V
Join CFGasto CFG On V.IDOperacion=CFG.IDOperacion And V.ID=CFG.ID
Join VTipoCuentaFija TC On CFG.IDTipoCuentaFija=TC.ID




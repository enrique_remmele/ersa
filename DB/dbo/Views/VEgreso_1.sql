﻿
CREATE View [dbo].[VEgreso]

As

Select
'Tipo'='Compra',
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
'Comprobante'=C.NroComprobante,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
IDTransaccion,
Numero,
IDTipoComprobante,
NroComprobante,

--Proveedor
C.IDProveedor,
'Proveedor'=P.RazonSocial,
P.RUC,
C.IDSucursal,
Fecha,
'Fec'=CONVERT(varchar(50), C.Fecha, 6),
C.Credito, 
'CreditoContado'=(Case When (C.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
FechaVencimiento,
'Fec. Venc.'= (Case When C.Credito='True' Then (CONVERT(varchar(50), C.FechaVencimiento, 6)) Else '---' End),
C.IDMoneda,
'Moneda'=M.Referencia,
Cotizacion,
Observacion,
IDTransaccionRemision,
Total,
TotalImpuesto,TotalDiscriminado,
Saldo,
Cancelado,
'Directo'=IsNull(C.Directo, 'False'),
C.RetencionIVA,
C.RetencionRenta,

--Departamento
C.IDDepartamentoEmpresa,
'DepartamentoEmpresa'=DE.Departamento,

P.IDTipoProveedor

From Compra   C
Left Outer Join TipoComprobante TC On C.IDTipoComprobante=TC.ID
Left Outer Join Proveedor P On C.IDProveedor=P.ID
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Left Outer Join Moneda M On C.IDMoneda=M.ID
left outer join DepartamentoEmpresa DE on C.IDDepartamentoEmpresa =DE.ID

Union All 

select 
'Tipo'='Gasto',
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
'Comprobante'=G.NroComprobante,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
IDTransaccion,
Numero,
IDTipoComprobante,
NroComprobante,

--Proveedor
G.IDProveedor,
'Proveedor'=P.RazonSocial,
P.RUC,
G.IDSucursal,
Fecha,
'Fec'=CONVERT(varchar(50), G.Fecha, 6),
G.Credito, 
'CreditoContado'=(Case When (G.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (G.Credito) = 'True' Then 'CRED' Else 'CONT' End),
FechaVencimiento,
'Fec. Venc.'= (Case When G.Credito='True' Then (CONVERT(varchar(50), G.FechaVencimiento, 6)) Else '---' End),
G.IDMoneda,
'Moneda'=M.Referencia,
Cotizacion,
Observacion,
IDTransaccionRemision,
Total,
TotalImpuesto,TotalDiscriminado,
Saldo,
Cancelado,
'Directo'=IsNull(G.Directo, 'False'),
G.RetencionIVA,
G.RetencionRenta,

--Departamento
G.IDDepartamentoEmpresa,
'DepartamentoEmpresa'=DE.Departamento,

P.IDTipoProveedor

From Gasto  G
Left Outer Join TipoComprobante TC On G.IDTipoComprobante=TC.ID
Left Outer Join Proveedor P On G.IDProveedor=P.ID
Left Outer Join Sucursal S On G.IDSucursal=S.ID
Left Outer Join Moneda M On G.IDMoneda=M.ID
left outer join DepartamentoEmpresa DE on G.IDDepartamentoEmpresa =DE.ID




﻿CREATE View [dbo].[VCubo_Producto]

As

Select
'SKU'=P.Referencia,
'SKUDescripcion'=P.Descripcion,
'PesoNeto'=P.Peso,
'UnidadPorCaja'=P.UnidadPorCaja,
'OP'=P.Marca,
'Marca'=P.SubLinea2,
'Categoria'=P.Linea

From VProducto P
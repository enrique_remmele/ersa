﻿CREATE View [dbo].[VDetalleVentaParaNotaCredito]
As
Select 

--Producto
P.ID, 
P.Descripcion, 
P.CodigoBarra, 
P.Ref, 
P.[Ref Imp], 
P.UnidadPorCaja, 
'IDProducto'=P.ID,

--Impuesto
P.IDImpuesto,
P.Impuesto,
P.Exento, 
P.Costo, 

--Detalle
DV.IDTransaccion, 
DV.PrecioUnitario, 
'Cantidad'=(Select Sum(DV2.Cantidad) From DetalleVenta DV2 Where DV2.IDTransaccion=DV.IDTransaccion And DV2.IDProducto=DV.IDProducto), 
--'SaldoCantidad'=DV.Cantidad - (IsNull((Select Sum(DNC.Cantidad) From DetalleNotaCredito DNC Join NotaCredito NC On  DNC.IDTransaccion=NC.IDTransaccion Where DNC.IDProducto=DV.IDProducto And DNC.IDTransaccionVenta=DV.IDTransaccion And NC.Anulado='False'), 0)), 
'SaldoCantidad'=(Select Sum(DV2.Cantidad) From DetalleVenta DV2 Where DV2.IDTransaccion=DV.IDTransaccion And DV2.IDProducto=DV.IDProducto) - (IsNull((Select Sum(DNC.Cantidad) From DetalleNotaCredito DNC Join NotaCredito NC On  DNC.IDTransaccion=NC.IDTransaccion Where DNC.IDProducto=DV.IDProducto And DNC.IDTransaccionVenta=DV.IDTransaccion And NC.Anulado='False' and NC.Procesado =1), 0)), 
DV.Total,

--Descuento
DV.PorcentajeDescuento,
DV.DescuentoUnitario,
DV.DescuentoUnitarioDiscriminado,
DV.TotalDescuento,
DV.TotalDescuentoDiscriminado,
DV.TotalNetoConDescuentoNeto,

--Venta
V.Comprobante,
V.FechaEmision,
V.Cobrado,
V.Descontado,
V.Saldo,

V.IDCliente,
V.Cancelado,
'Dias'=DATEDIFF(DD, V.FechaEmision, GetDate()),
V.Anulado,
'IDMoneda'=(select top(1) IDMoneda from ProductoListaPrecio where IDProducto = DV.IDProducto),
'PrecioUnitarioNeto' = dbo.FPrecioUnitarioNeto(P.ID, V.IDTransaccion)

From Venta V 
Join VDetalleVenta DV On DV.IDTransaccion=V.IDTransaccion 
Join VProducto P On DV.IDProducto=P.ID











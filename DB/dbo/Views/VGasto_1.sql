﻿


CREATE View [dbo].[VGasto]

As

Select

C.IDTransaccion,
--C.Numero,
'Numero'= case when C.CajaChica = 1 then (select top 1 GFF.Numero from GastoFondoFijo GFF where GFF.IDTransaccion = C.IDTransaccion) Else (select top 1 GTC.Numero from GastoTipoComprobante GTC where GTC.IDTransaccion = C.IDTransaccion) End,
'Num'=C.Numero,
C.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
C.NroComprobante,
'Comprobante'=C.NroComprobante,
C.NroTimbrado,
C.FechaVencimientoTimbrado,

--Proveedor
C.IDProveedor,
'Proveedor'=P.RazonSocial,
P.RUC,
P.Referencia,

C.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
C.Fecha,
'Fec'=CONVERT(varchar(50), C.Fecha, 6),
C.Credito,
'CreditoContado'=(Case When (C.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
'FechaVencimiento'=(Case When C.Credito='True' Then C.FechaVencimiento Else C.Fecha End),
'Fec. Venc.'= (Case When C.Credito='True' Then (CONVERT(varchar(50), C.FechaVencimiento, 6)) Else '---' End),
C.IDMoneda,
'Moneda'=M.Referencia,
C.Cotizacion,
C.Observacion,
'Directo'=IsNull(C.Directo, 'False'),
C.CajaChica, 
C.IncluirLibro,
'Cuota'=IsNull(C.Cuota, 0),

--Pagos
'Pagar'=IsNull(C.Pagar, 'False'),
'ImporteAPagar'=(Case When (C.Pagar) = 'True' Then C.ImporteAPagar Else 0 End),
C.IDCuentaBancaria,
'CuentaBancaria'=CB.Descripcion,
C.ObservacionPago,

--Totales
C.Total,
C.TotalImpuesto,
C.TotalDiscriminado,
C.RetencionIVA,
C.RetencionRenta,

--Credito
C.Saldo,
C.Cancelado,

--Forma de Pago
C.Efectivo,
C.Cheque,
'FormaPago'=(Case When (C.Efectivo) = 'True' Then 'EFECTIVO' Else (Case When (C.Cheque) = 'True' Then 'CHEQUE' Else '--' End) End),

--257 select * from proveedor where razonsocial like '%Administracion nacional de elec%'
--349 select * from proveedor where referencia like '%ESSAP%'
--158 select * from proveedor where referencia like '%cop%'
--select * from VEgresos where idproveedor = 349
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Distribucion
C.IDDeposito,
'Deposito'=ISNULL(D.Descripcion, ''),
C.IDCamion,
'Camion'=ISNULL(CA.Descripcion, ''),
C.IDChofer,
'Chofer'=ISNULL(CH.Nombres,''),



--Departamento
C.IDDepartamentoEmpresa,
'DepartamentoEmpresa'=DE.Departamento,

--Caja
T.IDTransaccionCaja,
'NroCaja'=IsNull(CJ.Numero, 0),
'CajaHabilitada'=IsNull(CJ.Habilitado, 'False'),
'FechaCaja'=CJ.Fecha,
'EstadoCaja'=IsNull(CONVERT(varchar(50), CJ.Fecha) + ' ' + (Case When(CJ.Habilitado) = 'True' Then 'Habilitado' Else 'Cerrado' End), '---'),
'IDGrupo'=ISnull(C.IDGrupo,0),
'RRHH'= (Case When C.RRHH = 1 Then 'True' else 'False' end)

From Gasto C
Join Transaccion T On C.IDTransaccion=T.ID
Left Outer Join Caja CJ On T.IDTransaccionCaja=CJ.IDTransaccion
Left Outer Join TipoComprobante TC On C.IDTipoComprobante=TC.ID
Left Outer Join Proveedor P On C.IDProveedor=P.ID
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Left Outer Join Moneda M On C.IDMoneda=M.ID
Left Outer JOIN Deposito D ON C.IDDeposito=D.ID
Left Outer JOIN Camion CA ON C.IDCamion=CA.ID
Left Outer JOIN Chofer CH ON C.IDChofer=CH.ID
Left Outer Join VCuentaBancaria CB On C.IDCuentaBancaria=CB.ID
left outer join vDepartamentoEmpresa DE on C.IDDepartamentoEmpresa =DE.ID






﻿CREATE View [dbo].[VDetalleProductoCosto]
As

Select	

DPC.IDTransaccion,
DPC.IDProducto,
DPC.Observacion,
PC.Anulado,

--Operacion
PC.Numero,
PC.Fecha,
PC.Fec,

--Producto
'Producto'=P.Descripcion + (Case When DPC.Observacion!='' Then ' - ' + DPC.Observacion Else '' End),
'Descripcion'=P.Descripcion,
'CodigoBarra'=P.CodigoBarra,
'Ref'=P.Referencia,
P.IDTipoProducto,
P.IDLinea,
 PC.IDSucursal,
 
 DPC.Costo,
 DPC.CostoAnterior
 
From DetalleProductoCosto DPC
Join VProductoCosto PC On DPC.IDTransaccion=PC.IDTransaccion
Join Producto P On DPC.IDProducto=P.ID




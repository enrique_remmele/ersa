﻿
CREATE View [dbo].[VGastoProducto]
As
Select 
GP.NroAcuerdo,
GP.IDTransaccionGasto,
GP.Cantidad,
GP.Observacion,
A.IDProducto,
Pr.IDTipoProducto,
'CuentaContableProveedorTipoProducto'=ISnull(TP.CuentaContableProveedor,''),
'CuentaContableCompra'=ISnull(Pr.CodigoCuentaCompra,''),
'CCaRecibir'=ISnull(A.CCaRecibir,'')
From GastoProducto GP
Join vAcuerdo A on GP.NroAcuerdo = A.NroAcuerdo
Join vProducto Pr on A.IDProducto = Pr.ID
Left Outer Join TipoProducto TP on TP.ID=Pr.IDTipoProducto

















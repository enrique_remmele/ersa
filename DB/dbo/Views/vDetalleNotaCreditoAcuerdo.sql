﻿

CREATE view [dbo].[vDetalleNotaCreditoAcuerdo]
as
select 
Distinct
DNCA.IDProducto,
P.Referencia,
P.Descripcion,
DNCA.IDCliente,
DNCA.DescuentoUnitario,
DNCA.IDTransaccionPedidoVenta,
DNCA.IDTransaccionPedidoNotaCredito,
DNCA.IDTransaccionVenta,
DNCA.IDTransaccionNotaCredito,
PV.Cantidad,
'TotalDescuentoProducto' = Round(PV.Cantidad * DNCA.DescuentoUnitario,0)

from DetalleNotaCreditoAcuerdo DNCA
left outer join DetallePedido PV on DNCA.IDTransaccionPedidoVenta = PV.IDTransaccion and DNCA.IDProducto = PV.IDProducto 
join Producto P on DNCA.IDProducto = P.ID
Where ISnull(DNCA.IDTransaccionVenta,0) = 0

union all

select 
DNCA.IDProducto,
P.Referencia,
P.Descripcion,
DNCA.IDCliente,
DNCA.DescuentoUnitario,
DNCA.IDTransaccionPedidoVenta,
DNCA.IDTransaccionPedidoNotaCredito,
DNCA.IDTransaccionVenta,
DNCA.IDTransaccionNotaCredito,
V.Cantidad,
'TotalDescuentoProducto' = V.Cantidad * DNCA.DescuentoUnitario

from DetalleNotaCreditoAcuerdo DNCA
left outer join DetalleVenta V on DNCA.IDTransaccionVenta = V.IDTransaccion and DNCA.IDProducto = V.IDProducto
join Producto P on DNCA.IDProducto = P.ID
where ISnull(DNCA.IDTransaccionVenta,0) > 0



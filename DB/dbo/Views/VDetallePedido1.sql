﻿CREATE View [dbo].[VDetallePedido1]
As
Select
DP.IDTransaccion,

--Producto
DP.IDProducto,
DP.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DP.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DP.Observacion End),
'CodigoBarra'=P.CodigoBarra,
'UnidadMedida'=P.UnidadMedida,
P.Referencia,
DP.Observacion,

--Deposito
DP.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DP.Cantidad,
DP.PrecioUnitario,
'Pre. Uni.'=DP.PrecioUnitario,
DP.Total,

--Impuesto
DP.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
DP.TotalImpuesto,
DP.TotalDiscriminado,
'Exento'=I.Exento,

--Descuento
DP.TotalDescuento,
'Descuento'=DP.TotalDescuento,
DP.DescuentoUnitario,
'Desc Uni'=DP.DescuentoUnitario,
DP.DescuentoUnitarioDiscriminado,
DP.TotalDescuentoDiscriminado,
DP.PorcentajeDescuento,
'Desc %'=DP.PorcentajeDescuento,
'PrecioNeto'=IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0),
'TotalPrecioNeto'=(IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0)) * DP.Cantidad,

--Costos
DP.CostoUnitario,
DP.TotalCosto,
DP.TotalCostoImpuesto,
DP.TotalCostoDiscriminado,

----Cantidad de Caja
'Caja'=IsNull(DP.Caja, 'False'),
DP.CantidadCaja,
'Cajas'=convert (decimal (10,2) ,IsNull((DP.Cantidad / P.UnidadPorCaja), 0)),
'UnidadMedidas'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),'UND'),
--Unidad de medida
'UnidadMedidaConvertir'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),'UND'),
'UnidadConvertir'=Isnull (P.UnidadConvertir,1),

--Totales
'TotalBruto'= isnull(DP.TotalDescuento,0)+isnull(DP.Total,0),
'TotalSinDescuento'=DP.Total - DP.TotalDescuento,
'TotalSinImpuesto'=DP.Total - DP.TotalImpuesto,
'TotalNeto'=DP.Total - (DP.TotalImpuesto + DP.TotalDescuento),
'TotalNetoConDescuentoNeto'=DP.TotalDiscriminado + DP.TotalDescuentoDiscriminado,

--Pedido
PE.Fecha,
PE.IDCliente,
PE.FechaFacturar,
PE.IDVendedor,
PE.IDSucursal,

T.IDUsuario,

'CantidadAEntregar'= Isnull(DP.CantidadAEntregar,DP.Cantidad),
PE.Numero,
'Suc'= (Select codigo from Sucursal where Id = PE.IDSucursal),
'Peso'=cast(P.Peso as money),
P.IDTipoProducto,
'IDSucursalCliente'=(Select IDSucursalCliente from VPedido where IDTransaccion = DP.IDTransaccion)



From DetallePedido DP
Join Pedido PE On DP.IDTransaccion=PE.IDTransaccion
Join VProducto P On DP.IDProducto=P.ID
Join Deposito D On DP.IDDeposito=D.ID
Join Impuesto I On DP.IDImpuesto=I.ID
Join Transaccion T on T.ID = PE.IDTransaccion

Where PE.Procesado='True'













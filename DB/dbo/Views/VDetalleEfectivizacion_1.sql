﻿CREATE view [dbo].[VDetalleEfectivizacion]
As
Select
DCE.IDTransaccion,
DCE.IDTransaccionEfectivo,
DCE.IDTransaccionCheque,
E.CodigoComprobante,
E.Comprobante,
DCE.Importe,
E.ID,
E.Cancelado,
E.Saldo,
E.Fecha,
'Fec'=CONVERT(varchar(50),E.Fecha,5),
E.Generado,

--Efectivizacion
'Efectivizacion'=EFE.NroComprobante,
EFE.IDSucursal
   
From DetalleEfectivizacion  DCE
JOin Efectivizacion EFE On DCE.IDTransaccion=EFE.IDTransaccion
Join VEfectivo E on DCE.IDTransaccionEfectivo=E.IDTransaccion  


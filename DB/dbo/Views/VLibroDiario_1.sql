﻿
/****** Object:  View [dbo].[VLibroDiario]    Script Date: 19/08/2020 10:11:53 ******/
CREATE View [dbo].[VLibroDiario]

As

--Ventas
Select

'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Venta - ' +' '+V.[Cod.] + '  ' +CONVERT(varchar(50),V.NroComprobante)+'  '+V.Condicion,
D.Debito,
D.Credito,
'Tipo'='VENTA DETALLADA',
'Observacion'=V.Cliente,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
V.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' END),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=V.Total * V.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VVenta V On D.IDTransaccion=V.IDTransaccion

Union All

--Nota de Débito Cliente
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Deb. Cli '+' '+ND.[Cod.] + '  ' +CONVERT(varchar(50),ND.NroComprobante)+'  '+ND.Condicion,
D.Debito,
D.Credito,
'Tipo'='NOTA DEBITO CLIENTE',
'Observacion'=ND.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
ND.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=ND.Total * ND.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaDebito ND On D.IDTransaccion=ND.IDTransaccion

union all

--Nota de Crédito Cliente
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Cred. Cli '+' '+NC.[Cod.] + '  ' +CONVERT(varchar(50),NC.NroComprobante)+'  '+NC.Condicion,
D.Debito,
D.Credito,
'Tipo'='NOTA CREDITO CLIENTE',
'Observacion'=NC.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
NC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=NC.Total * NC.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaCredito NC On D.IDTransaccion=NC.IDTransaccion


union all

--Cobranza Contado Detallado
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. por Lote: '+'  '+ Convert(varchar(50), CCO.Numero),
D.Debito,
D.Credito,
'Tipo'='COBRANZA CONTADO DETALLADO',
'Observacion'='Nro de Lote: ' + CCO.ComprobanteLote,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
CCO.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=CCO.Total * CCO.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaContado CCO On D.IDTransaccion=CCO.IDTransaccion

union all

--Cobranza Crédito Detallado
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Crédito: '+'  '+ Convert(varchar(50), CCR.Numero),
D.Debito,
D.Credito,
'Tipo'='COBRANZA CREDITO DETALLADO',
'Observacion'=CCR.[Cod.] + '  ' +CONVERT(varchar(50),CCR.NroComprobante)+' - '+CCR.Cliente,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
CCR.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=CCR.Total * CCR.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCobranzaCredito CCR On D.IDTransaccion=CCR.IDTransaccion

union all

--Compra
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Compra: '+'  '+ Convert(varchar(50), C.Numero),
D.Debito,
D.Credito,
'Tipo'='COMPRA',
'Observacion'= C.TipoComprobante + ' ' + CONVERT(varchar(50),C.NroComprobante)+' - '+C.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
C.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=C.Total * C.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VCompra C On D.IDTransaccion=C.IDTransaccion

union all

--Gasto
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Gasto: ' + convert(varchar(50),G.Numero),
D.Debito,
D.Credito,
'Tipo'='GASTO',
'Observacion'=G.[Cod.]+' '+CONVERT(varchar(50),G.NroComprobante)+' : '+CONVERT(varchar(50),G.Proveedor),
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
G.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=G.Total * G.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VGasto G On D.IDTransaccion=G.IDTransaccion

union all

--Nota de Débito Proveedor

Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Deb. Prov: '+'  '+ convert(varchar(50), ND.Numero),
D.Debito,
D.Credito,
'Tipo'='NOTA DEBITO PROVEEDOR',
'Observacion'=ND.[Cod.] + '  ' +CONVERT(varchar(50),ND.NroComprobante)+' - '+ND.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
ND.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=ND.Total * ND.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaDebitoProveedor ND On D.IDTransaccion=ND.IDTransaccion

union all

--Nota de Crédito Proveedor

Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Cred. Prov '+'  '+ convert(varchar(50),NC.Numero),
D.Debito,
D.Credito,
'Tipo'='NOTA CREDITO PROVEEDOR',
'Observacion'=NC.[Cod.] + '  ' +CONVERT(varchar(50),NC.NroComprobante)+' - '+NC.Proveedor,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
NC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=NC.Total * NC.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VNotaCreditoProveedor NC On D.IDTransaccion=NC.IDTransaccion

union all

--Depósito Bancario Detallado
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Dep. Bancario '+'  ' + convert(varchar(50), DB.Numero),
D.Debito,
D.Credito,
'Tipo'='DEPOSITO DETALLADO',
'Observacion'= DB.TipoComprobante + ' ' + convert(varchar(50), DB.Comprobante) + ' - ' +  DB.Banco + ' - Nro Cuenta: ' + DB.Cuenta,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
DB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=DB.Total * DB.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDepositoBancario DB On D.IDTransaccion=DB.IDTransaccion

union all

--Descuento Cheque
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Desc. Cheque '+'  '+ convert(varchar(50), DC.Numero),
D.Debito,
D.Credito,
'Tipo'='DESCUENTO CHEQUE',
'Observacion'= DC.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
DC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=DC.TotalDescontado * DC.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDescuentoCheque DC On D.IDTransaccion=DC.IDTransaccion

Union All

--Rechazo de Cheque Cliente
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Rechazo Cheq '+'  '+ convert(varchar(50), DC.Numero),
D.Debito,
D.Credito,
'Tipo'='RECHAZO DE CHEQUE DE CLIENTE',
'Observacion'= DC.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
0,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=DC.Importe * DC.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VChequeClienteRechazado DC On D.IDTransaccion=DC.IDTransaccion

Union All

--Débito Bancario
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Deb. Bancario '+'  '+ convert(varchar(50), DCB.Numero),
D.Debito,
D.Credito,
--EL TIPO ES DEBITO/CREDITO PORQUE EN EL FORM DE ASIENTOS CONTABLES NO DISCRIMINA
--SI SE CAMBIA, CAMBIAR EL FORM TAMBIEN
'Tipo'='DEBITO/CREDITO BANCARIO',
'Observacion'=DCB.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
DCB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=DCB.Total * DCB.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDebitoCreditoBancario DCB On D.IDTransaccion=DCB.IDTransaccion
Where DCB.Debito = 'True'

Union All

--Crédito Bancario
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cred. Bancario '+'  '+CONVERT(varchar(50),DCB.Numero),
D.Debito,
D.Credito,
--EL TIPO ES DEBITO/CREDITO PORQUE EN EL FORM DE ASIENTOS CONTABLES NO DISCRIMINA
--SI SE CAMBIA, CAMBIAR EL FORM TAMBIEN
'Tipo'='DEBITO/CREDITO BANCARIO',
'Observacion'=DCB.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
DCB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=DCB.Total * DCB.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDebitoCreditoBancario DCB On D.IDTransaccion=DCB.IDTransaccion
Where DCB.Credito = 'True'

Union All

--Orden de Pago
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
'Sucursal'=S.Descripcion,
'Moneda'=M.Referencia,
'Operacion'= 'Orden Pago '+'  '+CONVERT(varchar(50),OP.Numero),
D.Debito,
D.Credito,
'Tipo'='ORDEN PAGO',
'Observacion'=OP.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
OP.IDTipoComprobante,
'NroComprobante'=IsNull(TP.Codigo, '---') + ' ' + A.NroComprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=OP.Total * OP.Cotizacion

From vDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Sucursal S On A.IDSucursal=S.ID
Join Moneda M On A.IDMoneda=M.ID
Join vOrdenPago OP On D.IDTransaccion=OP.IDTransaccion
Join TipoComprobante TP On OP.IDTipoComprobante=TP.ID

Union All

--Vencimiento Cheque Emitido
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
'Sucursal'=S.Descripcion,
'Moneda'=M.Referencia,
'Operacion'= 'Orden Pago '+'  '+CONVERT(varchar(50),OP.Numero),
D.Debito,
D.Credito,
'Tipo'='ORDEN PAGO',
'Observacion'=OP.Observacion,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
OP.IDTipoComprobante,
'NroComprobante'=IsNull(TP.Codigo, '---') + ' ' + A.NroComprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=OP.Total * OP.Cotizacion

From vDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Sucursal S On A.IDSucursal=S.ID
Join Moneda M On A.IDMoneda=M.ID
Join vVencimientoChequeEmitido VOP On D.IDTransaccion=VOP.IDTransaccion
join OrdenPago OP on VOP.IDTransaccionOP = OP.IDTransaccion
Join TipoComprobante TP On OP.IDTipoComprobante=TP.ID

Union All



--Movimiento
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Mov. Stock '+'  '+CONVERT(varchar(50),M.Operacion),
D.Debito,
D.Credito,
'Tipo'='MOVIMIENTO',
'Observacion'=M.Motivo,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=M.Total

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VMovimiento M On D.IDTransaccion=M.IDTransaccion

Union All

--Ajuste
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Ajuste ',
D.Debito,
D.Credito,
'Tipo'='AJUSTE',
'Observacion'=AI.Sucursal + ' - ' + AI.Deposito,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
0,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=AI.Total 

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VControlInventario AI On D.IDTransaccion=AI.IDTransaccion

Union All

--Asiento Importado
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= '* ' + V.Operacion,
D.Debito,
D.Credito,
'Tipo'='* ASIENTOS IMPORTADOS',
'Observacion'=Convert(varchar(50), V.Operacion) + ' - Nro:' + Convert(varchar(50), V.Numero),
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
0,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=V.Total * V.Cotizacion

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VAsientoImportado V On D.IDTransaccion=V.IDTransaccion

Union All

--Asiento con Operaciones sin Formularios de Carga
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= O.Descripcion + ' ' + A.Comprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=A.Detalle,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
0,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=A.Total

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Where O.FormName Is Null

union all

--Asiento con Operaciones sin Formularios de Carga
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= O.Descripcion + ' ' + A.Comprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=A.Detalle,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
0,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=A.Total

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Where O.FormName ='frmAjusteControlInventario'


Union All

--Asiento con Operaciones en Comprobantes para Libro IVA
Select
'Codigo'=D.Codigo,
'Descripcion'=CASE WHEN d.Credito > 0 THEN '         ' + d.Descripcion ELSE d.Descripcion END,
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= TC.Codigo + ' ' + C.NroComprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=C.Detalle,
D.Orden,
'Resumido'='False',
A.IDTransaccion,
A.FechaTransaccion,
A.IDUsuario,
0,
'NroComprobante'=A.Comprobante,
A.Numero,
'NumeroAsiento'=A.Numero,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
A.IDOperacion,
'OperacionAgrupador'=A.Operacion,
A.Conciliado,
A.FechaConciliado,
A.IDUsuarioConciliado,
'Importe'=C.Total

From VDetalleAsiento D
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join ComprobanteLibroIVA C On A.IDTransaccion=C.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Join TipoComprobante TC On C.IDTipoComprobante=TC.ID







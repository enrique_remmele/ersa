﻿CREATE View [dbo].[VDetalleAsignacionCamionPedido]
As

Select	
DM.IDTransaccion,
DM.ID,
DM.IDTransaccionPedido,
DM.IDOperacion,
DM.IDProducto,
DM.Cantidad,
DM.Tipo,
Pe.Facturado,
Pe.FacturaNro,
Pe.IDTransaccionVenta,

M.Anulado, 
--Operacion
M.Numero,
M.Fecha,
M.Fec,

--Producto
'Producto'=Concat(P.Referencia,' - ',P.Descripcion),
'CodigoBarra'=P.CodigoBarra,
'Ref'=P.Referencia,
 M.[Cod.],

 --M.Operacion,
 M.NroComprobante,
 M.IDSucursal,
M.IdTipoComprobante,
'Peso' = (DM.Cantidad * Peso),
'Cliente' = Pe.Cliente,
'IDUsuarioPedido'= Pe.IDUsuario
 
From DetalleAsignacionCamion DM
Join VAsignacionCamion M On DM.IDTransaccion=M.IDTransaccion
Join Producto P On DM.IDProducto=P.ID
Join VPedido Pe on DM.IDTransaccionPedido = Pe.IDTransaccion
Where PedidoCliente = 'True'











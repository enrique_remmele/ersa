﻿CREATE view [dbo].[VRendicionFondoFijo]
as

Select
RFF.IDTransaccion,
RFF.IDSucursal,
'Sucursal'=S.Descripcion,
RFF.IDGrupo,
'Grupo'=G.Descripcion,
RFF.Numero,
RFF.Fecha,
RFF.Observacion,
RFF.IDTransaccionOrdenPago,
'NroOP'=OP.Numero,
OP.Total,
'ObservacionOP'=OP.Observacion,
'FechaOP'=OP.Fecha, 

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
FF.Tope ,

--Anulacion
'Anulado'=isnull(RFF.Anulado,'False'),
'Estado'=Case When RFF.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=RFF.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=RFF.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=RFF.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=RFF.IDTransaccion)

From RendicionFondoFijo RFF
Join Transaccion T on T.ID=RFF.IDTransaccion 
Join Sucursal S on S.ID=RFF.IDSucursal 
Join Grupo G on G.ID=RFF.IDGrupo 
Left Outer Join OrdenPago OP on OP.IDTransaccion=RFF.IDTransaccionOrdenPago 
Join FondoFijo FF on FF.IDSucursal=RFF.IDSucursal and
FF.IDGrupo =RFF.IDGrupo






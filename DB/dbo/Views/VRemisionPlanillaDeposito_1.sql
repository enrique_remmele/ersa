﻿CREATE View [dbo].[VRemisionPlanillaDeposito]
As
Select
IDDeposito,
Numero,
Creado,
'Total'=COUNT(*),
'Cargados'=COUNT(case when(Cargado) = 'True' Then 1 Else NULL End),
'Pendiente'=COUNT(case when(Cargado) = 'False' Then 1 Else NULL End)
From RemisionPlanillaDeposito
Group By IDDeposito, Numero, Creado

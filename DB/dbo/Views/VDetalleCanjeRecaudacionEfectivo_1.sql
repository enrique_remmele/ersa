﻿CREATE View [dbo].[VDetalleCanjeRecaudacionEfectivo]

As

Select


E.ID,
'Sel'='True',
'CodigoComprobante' = E.CodigoComprobante, 
'Comprobante' = E.Comprobante, 
'Banco'='',
'BancoLocal'='',

--Si la moneda no es GS (ID <> a 1) entonces mostrar importe moneda
--'Importe'= DDB.Importe,
--'Importe'=Case When(DB.IDMoneda) <> 1 Then E.ImporteMoneda Else DDB.Importe End ,
'Importe'= DDB.Importe,

'Cliente'='',
'CuentaBancaria'='',
'Estado'='',
'Numero'=0,
'IDSucursal'=0,
'Ciudad'='',
DDB.IDTransaccionEfectivo,
DDB.IDTransaccion,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
'Deposito'='True',
'IDCuentaBancariaDeposito'=0,
DB.Fecha,
'NroOperacion'=DB.Numero,
DB.Sucursal,
M.Decimales,
E.Cotizacion

From DetalleCanjeRecaudacion DDB
Join Transaccion T On DDB .IDTransaccion =T.ID
Join VCanjeRecaudacion DB on DDB.IDTransaccion = DB.IDTransaccion 
Join VEfectivo E on DDB.IDTransaccionEfectivo = E.IDTransaccion and DDB.IDEfectivo = E.ID
Join Moneda M On DB.IDMoneda=M.ID




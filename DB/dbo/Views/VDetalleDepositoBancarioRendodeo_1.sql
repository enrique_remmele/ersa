﻿CREATE view [dbo].[VDetalleDepositoBancarioRendodeo]
as

Select

'IDTransaccion'=0,

'ID'=0,
'Sel'='True',
'CodigoComprobante' = 'Redon', 
'Comprobante' ='', 
'Banco'='',
'BancoLocal'='',
'Importe'= 0,
'Cliente'='',
'CuentaBancaria'='',
'Estado'='',
'Numero'=0,
'IDSucursal'=0,
'Ciudad'='',

'IDTransaccionDepositoBancario' = DDB .IDTransaccionDepositoBancario ,
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
'Deposito'='True',
'IDCuentaBancariaDeposito'=0,
DB.Fecha,
'NroOperacion'=DB.Numero   

From DetalleDepositoBancario DDB
Join Transaccion T On DDB .IDTransaccionDepositoBancario =T.ID
Join DepositoBancario DB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion 
Where DDB.Redondeo = 'True'














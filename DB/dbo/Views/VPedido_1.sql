﻿
CREATE View [dbo].[VPedido]
As

Select

P.IDTransaccion,

--Cabecera
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
P.Numero,
P.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
P.Fecha,
'Fec'=CONVERT(varchar(50), P.Fecha, 6),
P.FechaFacturar,
'Facturar'=CONVERT(varchar(50), P.FechaFacturar, 6),
'RestriccionHorariaEntrega'=isnull(P.RestriccionHorariaEntrega,0),
'HoraDesde'= (Case when isnull(P.RestriccionHorariaEntrega,0) = 0 then '00:00:00' else isnull(P.HoraDesde, '00:00:00') end),
'HoraHasta'= (Case when isnull(P.RestriccionHorariaEntrega,0) = 0 then '00:00:00' else isnull(P.HoraHasta,'00:00:00') end),

'FacturaNro'= IsNull( (Select Top(1) V.[Cod.]  + '  ' + V.Comprobante  from VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=P.IDTransaccion), '---'), 
'IDTransaccionVenta' = Isnull((Select max(PV.IDTransaccionVenta) from PedidoVenta PV where PV.idtransaccionPedido = P.IDTransaccion),0),
--Cliente
P.IDCliente,
--'Cliente'=C.RazonSocial,
'Cliente'=Isnull(P.RazonSocial,C.RazonSocial),
'Ref.Cliente'=C.Referencia ,
--C.RUC,
'RUC'=Isnull(P.RUC,C.RUC),
--'IDSucursalCliente'=(Case when EsVentaSucursal = 1 then (Select IDSucursal from vClienteSucursal where ID=1 and IDCliente = P.IDCliente) else (C.IDSucursal) end),
P.IDSucursalCliente,
'CiudadEntrega'= (Case when EsVentaSucursal = 1 then (select Ciudad from vClienteSucursal WHERE ID = p.IDSucursalCliente AND IDCliente = P.IDCliente) else C.Ciudad end),
P.Direccion,
P.Telefono,
'IDZonaVenta'=(Case when EsVentaSucursal = 1 then (select IsNull(IDZonaVenta, 0) from vClienteSucursal WHERE ID = p.IDSucursalCliente AND IDCliente = P.IDCliente) else  IsNull(C.IDZonaVenta, 0) end),
'ZonaVenta'=(Case when EsVentaSucursal = 1 then (select ZonaVenta from vClienteSucursal WHERE ID = p.IDSucursalCliente AND IDCliente = P.IDCliente) else  C.ZonaVenta end),

--Opciones
P.IDDeposito,
'Deposito'=D.Descripcion,
P.Observacion,
P.IDMoneda,
'Moneda'=M.Referencia,
P.Cotizacion,
P.Anulado,

--Otros
P.IDListaPrecio,
'Lista de Precio'=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=P.IDListaPrecio), '---'),
'SucursalListaPrecio'=IsNull((Select LP.CodigoSucursal From vListaPrecio LP Where LP.ID=P.IDListaPrecio), '---'),
P.IDVendedor,
'Vendedor'=ISNULL((Select VE.Nombres From Vendedor VE Where VE.ID=P.IDVendedor), '---'),
P.EsVentaSucursal,
--P.Facturado,
'Facturado'=(Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End),
'Estado'=(Case When (Anulado) = 'True' Then 'Anulado' Else (Case When (Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End) = 'True' Then 'Facturado' Else 'Pendiente' End) End),
'Venta'=IsNull((Select Top(1) V.Comprobante From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc), '---'),
'NroFactura'=IsNull((Select Top(1) V.NroComprobante From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc), '0'),
'FechaFactura'=(Select Top(1) V.FechaEmision From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=P.IDTransaccion Order By IDTransaccionPedido Desc),

--Totales
P.Total,
P.TotalImpuesto,
P.TotalDiscriminado,
P.TotalDescuento,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Importado
--'Comprobante'=ISNULL(PE.NroFactura, '---'),
'Comprobante'= '---',

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion),

--Forma de Pago
P.IDFormaPagoFactura,
'FormaPagoFactura'=(Select FPF.Referencia From FormaPagoFactura FPF Where FPF.ID=P.IDFormaPagoFactura),
FPF.Aprobacion,
'CancelarAutomatico'=IsNull(FPF.CancelarAutomatico, 'False'),

C.LimiteCredito,
C.SaldoCredito,
'Aprobado'=(Case When (P.Aprobado) = 'True' Then 'True' Else 'False' End),
'AprobadoCancelarAutomatico'=(Case when P.Aprobado = 1 then 'APROBADO' else 
			(Case when P.Aprobado = 0 then 'RECHAZADO' else
			'PENDIENTE' end) End),
P.Credito,
'IDSucursalClienteAtencion'=(Case when EsVentaSucursal = 1 then (Select IDSucursal from vClienteSucursal where ID=IDSucursalCliente and IDCliente = P.IDCliente) else (C.IDSucursal) end),
'SucursalCliente'=(Case when EsVentaSucursal = 1 then (Select descripcion from Sucursal where ID = (Select IDSucursal from vClienteSucursal where ID=1 and IDCliente = P.IDCliente)) else (C.Sucursal) end),
'IDTipoProducto'= (Select top(1) IDTipoProducto from vdetallePedido where IDTransaccion = P.IDTransaccion),
'IDCiudadCliente' = (Case when EsVentaSucursal = 1 then (Select IDCiudad from vClienteSucursal where ID=1 and IDCliente = P.IDCliente) else (C.IDCiudad) end),
'CiudadCliente' = (Case when EsVentaSucursal = 1 then (Select Ciudad from vClienteSucursal where ID=1 and IDCliente = P.IDCliente) else (C.Ciudad) end),
AutorizacionLineaCredito,
'IDUsuarioAutorizacionLineaCredito'= ISNULL(IDUsuarioAutorizacionLineaCredito,0),
'FechaAutorizacionLineaCredito'=ISNULL(P.FechaAutorizacionLineaCredito, getdate()),
'PlazoCredito'=(Case when IDSucursalCliente > 0 then (Select PlazoCredito From ClienteSucursal where IDCliente = C.ID and ID = IDSucursalCliente) else C.PlazoCredito end),
'EntregaCliente'=(Case When (P.EntregaCliente) = 'True' Then 'True' Else
				(Case When ((Select Isnull(sum(isNull(Peso,0) * Cantidad),0) from vDetallePedido where IDTransaccion = P.IDTransaccion)>=25000) then 'True' Else
				 'False' End) End),
'PesoTotalPedido'=(Select  sum(Peso * Cantidad) from vDetallePedido where IDTransaccion = P.IDTransaccion),
'Reprocesado'=(Case When PR.IDTransaccionPedidoOriginal is null Then 'False' Else 'True' End),
'IDTransaccionReprocesado' = (Case When PR.IDTransaccionPedidoOriginal is null Then 0 Else PR.IDTransaccionPedidoOriginal End),
'FechaReproceso' = (Case When PR.IDTransaccionPedidoOriginal is null Then '' Else PR.Fecha End),
'Atraso'= IsNull((Select Top(1) max(DATEDIFF(day, FechaVencimiento,getdate()))
from VVenta where saldo > 1 and Anulado = 0 and Credito = 1 and IDCliente = C.ID),0),
'IDTransaccionPedidoNotaCredito'=Isnull((Select top(1) PNCPV.IDtransaccionPedidoNotaCredito from PedidoNotaCreditoPedidoVenta PNCPV join PedidoNotaCredito PNC on PNCPV.IDTransaccionPedidoNotaCredito = PNC.IDTransaccion where PNCPV.IDTransaccionPedidoVenta =  P.IDTransaccion and PNC.Anulado = 0),0),
'AnuladoAprobado'= Isnull(P.AnuladoAprobado,'False')

From Pedido P
Join Transaccion T On P.IDTransaccion=T.ID
Join VFormaPagoFactura FPF On P.IDFormaPagoFactura=FPF.ID
Left Outer Join VCliente C On P.IDCliente=C.ID
Left Outer Join Sucursal S On P.IDSucursal=S.ID
Left Outer Join Deposito D On P.IDDeposito=D.ID
Left Outer Join Moneda M On P.IDMoneda=M.ID
Left Outer Join PedidoImportado PE On P.IDTransaccion=PE.IDTransaccion
Left Outer Join PedidoReprocesado PR On P.IDTransaccion = PR.IDTransaccionPedidoReprocesado

Where P.Procesado = 'True'



























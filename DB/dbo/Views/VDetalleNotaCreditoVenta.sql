﻿CREATE View [dbo].[VDetalleNotaCreditoVenta]

As

Select

--Venta
'IDTransaccion'=NCV.IDTransaccionNotaCredito, 
'IDTransaccionVenta'=V.IDTransaccion , 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,

--Nota de Credito
NC.Fecha,
'Comp. NC'=NC.[Cod.] + ' ' + NC.Comprobante,
'Usuario NC'=NC.Usuario,
'Tipo NC'=NC.Tipo,
'Estado NC'=NC.[Estado NC], 
'IDTransaccionNotaCreditoAplicacion'=0,
NC.Observacion,
'Aplicado'='True',
DNC.IDProducto,
DNC.Producto,
DNC.PrecioUnitario,
DNC.Cantidad,
DNC.Total,
DNC.TotalImpuesto,
DNC.TotalDiscriminado

From vDetalleNotaCredito DNC
join NotaCreditoVenta  NCV ON NCV.IDTransaccionNotaCredito = DNC.IDTransaccion
Join VNotaCredito NC On NCV.IDTransaccionNotaCredito=NC.IDTransaccion
join VVenta  V on NCV.IDTransaccionVentaGenerada  = V .IDTransaccion 
Where NC.Aplicar = 'False'
And NC.Anulado = 'False'
Union All

Select
NCVA.IDTransaccionNotaCredito, 
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,

--Nota de Credito
'Fecha'= (Select Fecha From NotaCredito NC Where NC.Idtransaccion= NCVA.IDTransaccionNotaCredito),
'Comp. NC'=NCA.[Cod.] + ' (' + Convert(varchar(50), NCA.Numero) + ') ' + NCA.Comprobante,
'Usuario NC'=NCA.Usuario,
'Tipo NC'='Aplicacion',
'Estado NC'=(Case When NCA.Anulado='True' Then 'Anulado' Else 
			'---' 
			End),
NCVA.IDTransaccionNotaCreditoAplicacion ,
NCA.Observacion,
'Aplicado'='True',
DNC.IDProducto,
DNC.Producto,
DNC.PrecioUnitario,
DNC.Cantidad,
DNC.Total,
DNC.TotalImpuesto,
DNC.TotalDiscriminado
From vDetalleNotaCredito DNC
join NotaCreditoVentaAplicada NCVA on NCVA.IDTransaccionNotaCredito = DNC.IDTransaccion
join VVenta  V on NCVA.IDTransaccionVenta  = V .IDTransaccion 
--No se si es correcto el Join con Nota de Credito...
Join VNotaCredito NC On NCVA.IDTransaccionNotaCredito=NC.IDTransaccion
Join VNotaCreditoAplicacion NCA On NCVA.IDTransaccionNotaCreditoAplicacion=NCA.IDTransaccion
Where NC.Anulado = 'False'



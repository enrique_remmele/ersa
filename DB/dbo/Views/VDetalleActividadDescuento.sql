﻿
CREATE View [dbo].[VDetalleActividadDescuento]

As

Select 

--Planilla
P.IDTransaccion,
P.Comprobante,
P.Proveedor,
P.DescripcionTipoComprobante,
P.Numero,
P.Suc,
P.TotalPresupuesto,
P.TotalExedente,
P.TotalSaldo,
P.TotalUtilizado,
P.Desde,
P.Hasta,

--Actividad
'IDActividad'=A.ID,
A.Codigo,
A.Mecanica,
A.[Desc. Max.],
A.Asignado,
A.Tactico, 
A.Acuerdo,
A.PorcentajeUtilizado,
A.Saldo,
A.Excedente,

--Descuento
'Referencia'=DV.Referencia,
DV.IDProducto,
D.Producto,
D.Descuento,

--Venta
V.[Cod.] ,
'Venta'=V.Comprobante,
V.Cliente,
V.ReferenciaCliente ,
V.IDCliente,
V.Vendedor,
V.IDVendedor,
V.FechaEmision,


--Detalle Venta
DV.Cantidad,
DV.[Desc %],
DV.[Desc Uni],
DV.Total 

From VActividad A
Join DetallePlanillaDescuentoTactico DP On A.ID=DP.IDActividad
Join VPlanillaDescuentoTactico P On DP.IDTransaccion=P.IDTransaccion
Join VDescuento D On A.ID=D.IDActividad
Join VDetalleVenta DV On D.IDTransaccion=DV.IDTransaccion And D.IDProducto=DV.IDProducto And D.ID=DV.ID
Join VVenta V On D.IDTransaccion=V.IDTransaccion
Where V.Fecha Between P.Desde And P.Hasta 
And V.Anulado='False' And V.Procesado='True'

﻿

CREATE view [dbo].[VExtractoMovimientoClienteFacturaCREDITO]

As

--Cobranzas credito
Select
CC.IDTransaccion, 
'Fecha'=CC.FechaEmision,
'Debito'=0,
'Credito'=VC.Importe, 
'Saldo'=0.00,
'ComprobanteAsociado'=V.IDTransaccion
From VentaCobranza VC 
Join Venta V On VC.IDTransaccionVenta = V.IDTransaccion
Join CobranzaCredito CC On VC.IDTransaccionCobranza = CC.IDTransaccion
where CC.Anulado = 0 and isnull(CC.AnticipoCliente,0) = 0

union all

--Aplicacion de Anticipo
Select
CC.IDTransaccion, 
'Fecha'=AP.Fecha,
'Debito'=0,
'Credito'=VC.Importe, 
'Saldo'=0.00,
'ComprobanteAsociado'=V.IDTransaccion
From AnticipoVenta VC
Join AnticipoAplicacion AP on AP.IDTransaccion = VC.IDTransaccionAnticipo
Join Venta V On VC.IDTransaccionVenta = V.IDTransaccion
Join CobranzaCredito CC On AP.IDTransaccionCobranza = CC.IDTransaccion
where CC.Anulado = 0 and isnull(CC.AnticipoCliente,0) = 1

union all

--Cobranzas contado
Select
CC.IDTransaccion, 
'Fecha'=CC.Fecha,
'Debito'=0,
'Credito'=VC.Importe, 
'Saldo'=0.00,
'ComprobanteAsociado'=V.IDTransaccion
From VentaCobranza VC 
Join Venta V On VC.IDTransaccionVenta = V.IDTransaccion
Join CobranzaContado CC On VC.IDTransaccionCobranza = CC.IDTransaccion
where CC.Anulado = 0 


union all

--Nota Credito
Select
CC.IDTransaccion, 
'Fecha'=CC.Fecha,
'Debito'=0,
'Credito'=VC.Importe, 
'Saldo'=0.00,
'ComprobanteAsociado'=V.IDTransaccion
From NotaCreditoVenta VC 
Join Venta V On VC.IDTransaccionVentaGenerada = V.IDTransaccion
Join NotaCredito CC On VC.IDTransaccionNotaCredito = CC.IDTransaccion
where CC.Anulado = 0  and isnull(CC.Aplicar,0) = 0

union all

--Nota Credito APLICACION
Select
CC.IDTransaccion, 
'Fecha'=CC.Fecha,
'Debito'=0,
'Credito'=VC.Importe, 
'Saldo'=0.00,
'ComprobanteAsociado'=V.IDTransaccion
From NotaCreditoVentaAplicada VC 
Join Venta V On VC.IDTransaccionVenta = V.IDTransaccion
Join NotaCredito CC On VC.IDTransaccionNotaCredito = CC.IDTransaccion
join NotaCreditoAplicacion NCA on VC.IDTransaccionNotaCreditoAplicacion =NCA.IDTransaccion
where CC.Anulado = 0  
and isnull(CC.Aplicar,0) = 1
and NCA.Anulado = 0


Union All


--Importacion de Importe cobrado de facturas
Select
V.IDTransaccion, 
'Fecha'=Convert(date, '20151231'),
'Debito'=0,
'Credito'=VI.Cobrado, 
'Saldo'=0.00,
'ComprobanteAsociado'=V.IDTransaccion
From VVenta V 
Join VentaImportada VI On V.IDTransaccion=VI.IDTransaccion

Where V.Anulado='False'





﻿
CREATE View [dbo].[VCubo_Ventas_Netas]
As
Select

'NumDoc'=V.Comprobante,
'CodiSap'=V.Referencia,
'Fecha'=V.Fecha,
'ID_Cliente'=Convert(varchar(50), V.ReferenciaCliente),
'Cantidad'=Convert(int, V.Cantidad),
'Preriodo'=CONVERT(varchar(5), year(V.Fecha)) + dbo.FFormatoDosDigitos(Month(V.Fecha)),
'PrecioV'=Case When (V.Cantidad)=0 Then 0 Else Convert(int, Round(V.TotalNeto/V.Cantidad, 0)) End
From VDetalleVentaConDevolucion V
Where Year(V.Fecha)>=2015
And V.IDSucursal=2


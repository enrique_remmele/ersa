﻿
CREATE View [dbo].[VDetallePedidoDescuentoDetalleAGuaranies]
As
Select
DP.IDTransaccion,

--Producto
DP.IDProducto,
DP.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DP.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DP.Observacion End),
'CodigoBarra'=P.CodigoBarra,
'UnidadMedida'=P.UnidadMedida,
P.Referencia,
DP.Observacion,
PE.Cotizacion,
PE.IDMoneda,
--Deposito
DP.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DP.Cantidad,
'PrecioUnitario'=DP.PrecioUnitario * PE.Cotizacion,
'Pre. Uni.'=DP.PrecioUnitario * PE.Cotizacion,
--DP.Total, ***** Se calcula el total sin descuento tactico, porque en esta vista, el descuento tactico esta declarado a parte
'Total'= DP.PrecioUnitario * DP.Cantidad * PE.Cotizacion,

--Impuesto
DP.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
'TotalImpuesto'=DP.TotalImpuesto * PE.Cotizacion,
'TotalDiscriminado'=DP.TotalDiscriminado * PE.Cotizacion,
'Exento'=I.Exento,
'IDProductoDescuento' = (Case when DP.IDImpuesto = 1 then 218 else
						(Case When DP.IDImpuesto = 2 then 219 else 
						230 end)end),

--Descuento
'TotalDescuento'=DP.TotalDescuento * PE.Cotizacion,
'Descuento'=DP.TotalDescuento * PE.Cotizacion,
'DescuentoUnitario'=DP.DescuentoUnitario * PE.Cotizacion,
'Desc Uni'=DP.DescuentoUnitario * PE.Cotizacion,
'DescuentoUnitarioDiscriminado'=DP.DescuentoUnitarioDiscriminado * PE.Cotizacion,
'TotalDescuentoDiscriminado'=DP.TotalDescuentoDiscriminado * PE.Cotizacion,
DP.PorcentajeDescuento,
'Desc %'=DP.PorcentajeDescuento,
'PrecioNeto'=(IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0)) * PE.Cotizacion,
'TotalPrecioNeto'=(IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0)) * DP.Cantidad,

--Costos
DP.CostoUnitario,
DP.TotalCosto,
DP.TotalCostoImpuesto,
DP.TotalCostoDiscriminado,

----Cantidad de Caja
'Caja'=IsNull(DP.Caja, 'False'),
DP.CantidadCaja,
'Cajas'=convert (decimal (10,2) ,IsNull((DP.Cantidad / P.UnidadPorCaja), 0)),
'UnidadMedidas'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),'UND'),
--Unidad de medida
'UnidadMedidaConvertir'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),'UND'),
'UnidadConvertir'=Isnull (P.UnidadConvertir,1),

--Totales
'TotalBruto'= (isnull(DP.TotalDescuento,0)+isnull(DP.Total,0)) * PE.Cotizacion,
'TotalSinDescuento'=(DP.Total - DP.TotalDescuento) * PE.Cotizacion,
'TotalSinImpuesto'=(DP.Total - DP.TotalImpuesto)* PE.Cotizacion,
'TotalNeto'=(DP.Total - (DP.TotalImpuesto + DP.TotalDescuento))* PE.Cotizacion,
'TotalNetoConDescuentoNeto'=(DP.TotalDiscriminado + DP.TotalDescuentoDiscriminado) * PE.Cotizacion,

--Pedido
PE.Fecha,
PE.IDCliente,
PE.FechaFacturar,
PE.IDVendedor,
'Vendedor'=(select Nombres from Vendedor where id = PE.IDVendedor),
'FechaFactura'=(Select Top(1) V.FechaEmision From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc),
PE.IDSucursal,
'Facturado'=(Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End),

'Estado'=(Case When (PE.Anulado) = 'True' Then 'Anulado' Else (Case When (Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End) = 'True' Then 'Facturado' Else 'Pendiente' End) End),
'Lista de Precio'=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=PE.IDListaPrecio), '---'),
'IDTipoCliente'=(Select idtipocliente from cliente where id = PE.IDCliente),
T.IDUsuario,

'CantidadAEntregar'= Isnull(DP.CantidadAEntregar,DP.Cantidad),
PE.Numero,
'Suc'= (Select codigo from Sucursal where Id = PE.IDSucursal),
'Peso'=cast(P.Peso as money),
P.IDTipoProducto,
PE.Anulado,
'EntregaCliente'=(Case When (PE.EntregaCliente) = 'True' Then 'True' Else 'False' End),
'CancelarAutomatico'=ISnull((Select FPF.CancelarAutomatico from FormaPagoFactura FPF where FPF.id = PE.IDFormaPagoFactura), 'False'),
'DescuentoUnitarioFACT'=DV.DescuentoUnitario * PE.Cotizacion,
'DescuentoUnitarioNCDiferenciaPrecio'= (select dbo.FDescuentoUnitarioDiferenciaPrecio(DV.IDProducto,DV.IDTransaccion)) * PE.Cotizacion,
'DescuentoUnitarioNCAcuerdo' = (select dbo.FDescuentoUnitarioAcuerdo(DV.IDProducto,DV.IDTransaccion)) * PE.Cotizacion,
'DescuentoUnitarioNCDiferenciaPeso' = (select dbo.FDescuentoUnitarioDiferenciaPeso(DV.IDProducto,DV.IDTransaccion)) * PE.Cotizacion,
'PrecioUnitarioNeto' = (select dbo.FPrecioUnitarioNeto(DV.IDProducto,DV.IDTransaccion)) * PE.Cotizacion

From DetallePedido DP
join PedidoVenta PV on DP.IDTRansaccion = PV.IDTransaccionPedido
Join DetalleVenta DV on PV.IDTransaccionVenta = DV.IDTransaccion and DP.IDProducto = DV.IDProducto
Join Pedido PE On DP.IDTransaccion=PE.IDTransaccion
Join VProducto P On DP.IDProducto=P.ID
Join Deposito D On DP.IDDeposito=D.ID
Join Impuesto I On DP.IDImpuesto=I.ID
Join Transaccion T on T.ID = PE.IDTransaccion

Where PE.Procesado='True'


Union all
--Facturas sin pedido por error de sistema
Select
0,
--Producto
DV.IDProducto,
DV.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DV.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DV.Observacion End),
'CodigoBarra'=P.CodigoBarra,
'UnidadMedida'=P.UnidadMedida,
P.Referencia,
DV.Observacion,
V.Cotizacion,
V.IDMoneda,
--Deposito
DV.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DV.Cantidad,
'PrecioUnitario'=DV.PrecioUnitario * V.Cotizacion,
'Pre. Uni.'=DV.PrecioUnitario * V.Cotizacion,
--DP.Total, ***** Se calcula el total sin descuento tactico, porque en esta vista, el descuento tactico esta declarado a parte
'Total'= DV.PrecioUnitario * DV.Cantidad * V.Cotizacion,

--Impuesto
DV.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
'TotalImpuesto'=DV.TotalImpuesto * V.Cotizacion,
'TotalDiscriminado'=DV.TotalDiscriminado * V.Cotizacion,
'Exento'=I.Exento,
'IDProductoDescuento' = (Case when DV.IDImpuesto = 1 then 218 else
						(Case When DV.IDImpuesto = 2 then 219 else 
						230 end)end),

--Descuento
'TotalDescuento'=DV.TotalDescuento * V.Cotizacion,
'Descuento'=DV.TotalDescuento * V.Cotizacion,
'DescuentoUnitario'=DV.DescuentoUnitario * V.Cotizacion,
'Desc Uni'=DV.DescuentoUnitario * V.Cotizacion,
'DescuentoUnitarioDiscriminado'=DV.DescuentoUnitarioDiscriminado * V.Cotizacion,
'TotalDescuentoDiscriminado'=DV.TotalDescuentoDiscriminado * V.Cotizacion,
DV.PorcentajeDescuento,
'Desc %'=DV.PorcentajeDescuento,
'PrecioNeto'=(IsNull(DV.PrecioUnitario,0) - IsNull(DV.DescuentoUnitario,0)) * V.Cotizacion,
'TotalPrecioNeto'=(IsNull(DV.PrecioUnitario,0) - IsNull(DV.DescuentoUnitario,0)) * DV.Cantidad,

--Costos
DV.CostoUnitario,
DV.TotalCosto,
DV.TotalCostoImpuesto,
DV.TotalCostoDiscriminado,

----Cantidad de Caja
'Caja'=IsNull(DV.Caja, 'False'),
DV.CantidadCaja,
'Cajas'=convert (decimal (10,2) ,IsNull((DV.Cantidad / P.UnidadPorCaja), 0)),
'UnidadMedidas'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),'UND'),
--Unidad de medida
'UnidadMedidaConvertir'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),'UND'),
'UnidadConvertir'=Isnull (P.UnidadConvertir,1),

--Totales
'TotalBruto'= (isnull(DV.TotalDescuento,0)+isnull(DV.Total,0)) * V.Cotizacion,
'TotalSinDescuento'=(DV.Total - DV.TotalDescuento) * V.Cotizacion,
'TotalSinImpuesto'=(DV.Total - DV.TotalImpuesto)* V.Cotizacion,
'TotalNeto'=(DV.Total - (DV.TotalImpuesto + DV.TotalDescuento))* V.Cotizacion,
'TotalNetoConDescuentoNeto'=(DV.TotalDiscriminado + DV.TotalDescuentoDiscriminado) * V.Cotizacion,

--Pedido
V.FechaEmision,
V.IDCliente,
V.FechaEmision,
V.IDVendedor,
'Vendedor'=(select Nombres from Vendedor where id = V.IDVendedor),
'FechaFactura'=FechaEmision,
V.IDSucursal,
'Facturado'='True',

'Estado'='Facturado',
'Lista de Precio'=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=V.IDListaPrecio), '---'),
'IDTipoCliente'=(Select idtipocliente from cliente where id = V.IDCliente),
T.IDUsuario,

'CantidadAEntregar'= DV.Cantidad,
0,
'Suc'= (Select codigo from Sucursal where Id = V.IDSucursal),
'Peso'=cast(P.Peso as money),
P.IDTipoProducto,
'False',
'EntregaCliente'='False',
'CancelarAutomatico'=ISnull((Select FPF.CancelarAutomatico from FormaPagoFactura FPF where FPF.id = V.IDFormaPagoFactura), 'False'),
'DescuentoUnitarioFACT'=DV.DescuentoUnitario * V.Cotizacion,
'DescuentoUnitarioNCDiferenciaPrecio'= (select dbo.FDescuentoUnitarioDiferenciaPrecio(DV.IDProducto,DV.IDTransaccion)) * V.Cotizacion,
'DescuentoUnitarioNCAcuerdo' = (select dbo.FDescuentoUnitarioAcuerdo(DV.IDProducto,DV.IDTransaccion)) * V.Cotizacion,
'DescuentoUnitarioNCDiferenciaPeso' = (select dbo.FDescuentoUnitarioDiferenciaPeso(DV.IDProducto,DV.IDTransaccion)) * V.Cotizacion,
'PrecioUnitarioNeto' = (select dbo.FPrecioUnitarioNeto(DV.IDProducto,DV.IDTransaccion)) * V.Cotizacion

From DetalleVenta DV
Join Venta V on DV.IDTRansaccion = V.IDTRansaccion
Join VProducto P On DV.IDProducto=P.ID
Join Deposito D On DV.IDDeposito=D.ID
Join Impuesto I On DV.IDImpuesto=I.ID
Join Transaccion T on T.ID = V.IDTransaccion
Where V.IDTransaccion not in (select IDTransaccionVenta from pedidoventa)
And V.Anulado = 0 
and V.Procesado = 1




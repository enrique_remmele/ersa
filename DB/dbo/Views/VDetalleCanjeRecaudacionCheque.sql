﻿CREATE View [dbo].[VDetalleCanjeRecaudacionCheque]
As

Select

'ID'=0,

'Sel'='True',
C.CodigoTipo, 
'Comprobante'=C.NroCheque ,
C.Banco,
C.BancoLocal,

--Si la moneda no es GS (ID <> a 1) entonces mostrar importe moneda
'Importe'= Case When(C.IDMoneda) <> 1 Then C.ImporteMoneda Else C.Importe End ,

C.Cliente,
'Referencia'=C.CodigoCliente,
C.CuentaBancaria,
C.Estado,
C.Numero,
C.IDSucursal,
C.Ciudad,
C.NroCheque,

DDB.IDTransaccionCheque,
DDB.IDTransaccion,
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
'Depositado'=C.Depositado,

DB.Fecha,
'NroOperacion'=DB.Numero,
DB.Sucursal,

'NroCobranza'=(Select Top 1  CC.Numero From ChequeCliente  CHQ
Join FormaPago FP on CHQ.IDTransaccion=FP.IDTransaccionCheque
Join CobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Where CHQ.IDTransaccion=C.IDTransaccion),
C.Diferido,
M.Decimales,
C.Cotizacion


From DetalleCanjeRecaudacion DDB
Join Transaccion T On DDB.IDTransaccion =T.ID
Join VCanjeRecaudacion DB on DDB.IDTransaccion = DB.IDTransaccion 
Join VChequeCliente  C on DDB.IDTransaccionCheque  = C .IDTransaccion
Join Moneda M On C.IDMoneda=M.ID 



























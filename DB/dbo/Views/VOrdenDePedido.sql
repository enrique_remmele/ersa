﻿
CREATE View [dbo].[VOrdenDePedido]
As

--CARLOS 03-03-2014

Select 
M.IDTransaccion,
M.Numero,
'Num'=M.Numero,
M.Fecha,
'Fec'=CONVERT(varchar(50), M.Fecha, 6),
M.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
M.NroComprobante,
'Comprobante'=M.NroComprobante,
M.IDDeposito,
'Deposito'=IsNull((Select D.[Suc-Dep] From VDeposito D Where D.ID=M.IDDeposito), '---') ,
M.Observacion,
M.Autorizacion,
M.Anulado,
M.FechaDesde,
M.FechaHasta,
'Estado'=Case When M.Anulado='True' Then 'Anulado' Else 'OK' End,

--Transaccion
'FechaTransaccion'=TR.Fecha,
--TR.IDDeposito,
TR.IDSucursal,
TR.IDTerminal,
TR.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion),

--Otros
'CodigoSucursalOperacion'=IsNull((Select S.Codigo From Sucursal S Where S.ID=TR.IDSucursal), '---'),
'SucursalOperacion'=IsNull((Select S.Descripcion From Sucursal S Where S.ID=TR.IDSucursal), '---'),
'PedidoCliente'=(Case when PedidoCliente = 'True' then 'True' else 'False' end)

From OrdenDePedido M
Join Transaccion TR On M.IDTransaccion=TR.ID
Join TipoComprobante TC On M.IDTipoComprobante=TC.ID





















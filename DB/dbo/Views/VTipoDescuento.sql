﻿CREATE View [dbo].[VTipoDescuento] 

As 

--Descuentos Configurables
Select 
TD.ID,
TD.Descripcion,
TD.Codigo,
TD.RangoFecha,
TD.Limite,
TD.CuentaVenta,
TD.CuentaCompra,
TD.Editable,
TD.PlanillaTactico,
TD.DescuentoTactico,
TD.DescuentosFijos,

--Cuentas de Ventas
'CodigoCuentaVenta'=IsNull((Select CC.Codigo From CuentaContable CC Where CC.Codigo=TD.CuentaVenta), ''),
'DenominacionCuentaVenta'=IsNull((Select CC.Descripcion From CuentaContable CC Where CC.Codigo=TD.CuentaVenta), ''),
'IDCuentaContableVenta'=IsNull((Select CC.ID From CuentaContable CC Where CC.Codigo=TD.CuentaVenta), '')

From TipoDescuento TD

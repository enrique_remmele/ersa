﻿
CREATE view [dbo].[VSaldoBancoDiario]

as

--Depósito Bancario
Select 
DB.Banco,
DB.Cuenta,
DB.Conciliado, 
'Debito'=DB.Total,
'Credito'=0,
'Movimiento'='DB',
'Tipo'=Concat('DB - ' , DB.Suc,' ', DB.Numero, ' - ', DB.Observacion),
'UltimoDebito'=DB.Fecha,
'UltimoCredito'=NULL,
Moneda,
DB.IDCuentaBancaria,
DB.Fecha,
'DepositoAconfirmar' = (Case when Conciliado = 'False' then DB.Total else 0 end),
'ChequesNoPagados'=0,
'SaldoConciliado'=(Case when Conciliado = 'True' then DB.Total else 0 end)

From VDepositoBancario DB 
 
Union All

--CREDITO BANCARIO
Select  
DCB.Banco,
DCB.Cuenta,
DCB.Conciliado,
'Debito'=0,
'Credito'=DCB.Total,
'Movimiento'='CB',
'Tipo'=Concat('CB - ' , DCB.Suc,' ', DCB.Numero, ' - ', DCB.Observacion),
'UltimoDebito'=NULL ,
'UltimoCredito'=DCB.Fecha ,
Moneda,
DCB.IDCuentaBancaria,
DCB.Fecha,
'DepositoAconfirmar' = (Case when Conciliado = 'False' then DCB.Total else 0 end),
'ChequesNoPagados'=0,
'SaldoConciliado'=(Case when Conciliado = 'True' then -DCB.Total else 0 end)

From VDebitoCreditoBancario DCB 
Where DCB.Credito='True'



Union All

--DEBITO BANCARIO
Select  
DCB.Banco, 
DCB.Cuenta,
DCB.Conciliado, 
'Debito'=DCB.Total,
'Credito'=0,
'Movimiento'='DB',
'Tipo'=Concat('DB - ' , DCB.Suc,' ', DCB.Numero, ' - ', DCB.Observacion),
'UltimoDebito'=DCB.Fecha,
'UltimoCredito'=NULL,
Moneda,
DCB.IDCuentaBancaria,
DCB.Fecha,
'DepositoAconfirmar' = (Case when Conciliado = 'False' then DCB.Total else 0 end),
'ChequesNoPagados'=0,
'SaldoConciliado'=(Case when Conciliado = 'True' then DCB.Total else 0 end)

From VDebitoCreditoBancario DCB 
Where DCB.Debito='True'


  
Union All

--Orden Pago cheque al dia
Select 
OP.Banco,
'Cuenta'=OP.CuentaBancaria,
OP.Conciliado,
'Debito'=0,
'Credito'=OP.ImporteMoneda,
'Movimiento'='OP',
'Tipo'=Concat('OP - ' , OP.Suc,' ', OP.Numero, ' -  Prov: ', (Case when OP.Proveedor <> '' then OP.Proveedor else OP.AlaOrden end)),
'UltimoDebito'=NULL ,
'UltimoCredito'=OP.Fecha   ,
Moneda,
OP.IDCuentaBancaria,
'Fecha' =(Select max(Fecha) from Cheque where IDCuentaBancaria = OP.IDCuentaBancaria and NroCheque = OP.NroCheque and Anulado = 'False'),
'DepositoAconfirmar' = 0,
'ChequesNoPagados'=(Case when Conciliado = 'False' then OP.ImporteMoneda else 0 end),
'SaldoConciliado'=(Case when Conciliado = 'True' then -OP.ImporteMoneda else 0 end)

 From VOrdenPago OP
 Where Cheque= 'True'And Anulado = 'False'
 and OP.Diferido = 0

 Union All

--Orden Pago cheque diferido
Select 
OP.Banco,
'Cuenta'=OP.CuentaBancaria,
OP.Conciliado,
'Debito'=0,
'Credito'=OP.ImporteMoneda,
'Movimiento'='OP',
'Tipo'=Concat('OP - ' , OP.Suc,' ', OP.Numero, ' -  Prov: ', (Case when OP.Proveedor <> '' then OP.Proveedor else OP.AlaOrden end)),
'UltimoDebito'=NULL ,
'UltimoCredito'=OP.FechaVencimiento   ,
Moneda,
OP.IDCuentaBancaria,
--'Fecha' =(Select max(Fecha) from Cheque where IDCuentaBancaria = OP.IDCuentaBancaria and NroCheque = OP.NroCheque and Anulado = 'False'),
'Fecha' =(Select max(FechaPago) from Cheque where IDCuentaBancaria = OP.IDCuentaBancaria and NroCheque = OP.NroCheque and Anulado = 'False'),
'DepositoAconfirmar' = 0,
'ChequesNoPagados'=(Case when Conciliado = 'False' then OP.ImporteMoneda else 0 end),
'SaldoConciliado'=(Case when Conciliado = 'True' then -OP.ImporteMoneda else 0 end)

 From VOrdenPago OP
 Where Cheque= 'True'And Anulado = 'False'
 and OP.Diferido = 1


 Union All
 
--Descuento Cheque 
Select 
DC.Banco,
DC.Cuenta,
DC.Conciliado,
--'Debito'=DC.TotalDescontado,
--'Credito'=DC.TotalAcreditado,
'Debito'=DC.TotalAcreditado,
'Credito'=0,
'Movimiento'='DC',
'Tipo'=Concat('DC - ' , DC.Suc,' ', DC.Numero, ' - ', DC.Observacion),
'UltimoDebito'=DC.Fecha,
'UltimoCredito'=DC.Fecha ,
Moneda,
DC.IDCuentaBancaria,
DC.Fecha,
--'DepositoAconfirmar' = (Case when Conciliado = 'False' then (DC.TotalDescontado - DC.TotalAcreditado) else 0 end),
'DepositoAconfirmar' = (Case when Conciliado = 'False' then ( DC.TotalAcreditado) else 0 end),
'ChequesNoPagados'=0,
--'SaldoConciliado'=(Case when Conciliado = 'True' then (DC.TotalDescontado - DC.TotalAcreditado) else 0 end)
'SaldoConciliado'=(Case when Conciliado = 'True' then (DC.TotalAcreditado) else 0 end)
From VDescuentoCheque DC  

Union All

--Cheque Rechazado
Select 
CR.Banco,
'Cuenta'=CR.CuentaBancaria,
CR.Conciliado,
'Debito'=0,
'Credito'=CC.ImporteMoneda,
'Movimiento'='CCR',
'Tipo'=Concat('CCR - ' , CR.Suc,' ', CR.Numero, ' - ', CR.Observacion),
'UltimoDebito'=CR.Fecha, 
'UltimoCredito'=NULL,
CC.Moneda,
CR.IDCuentaBancaria,
CR.Fecha,
'DepositoAconfirmar' = (Case when (CR.Conciliado = 'False' and (CC.Estado = 'RECHAZADO')) then CC.Importe else 0 end),
'ChequesNoPagados'=0,
'SaldoConciliado'=(Case when CR.Conciliado = 'True' then CC.Importe else 0 end)
From VChequeClienteRechazado CR
Join VChequeCliente CC On CR.IDTRansaccionCheque=CC.IDTransaccion 
and CR.Anulado = 'False'



----Cheque Rechazado
--Select 
--CC.Banco,
--'Cuenta'=CC.CuentaBancaria,
--CC.Conciliado,
--'Debito'=CC.Importe,
--'Credito'=0,
--'Movimiento'='CCR',
--'UltimoDebito'=CR.Fecha, 
--'UltimoCredito'=NULL,
--CC.Moneda
--From VChequeClienteRechazado CR
--Join VChequeCliente CC On CR.IDTRansaccionCheque=CC.IDTransaccion    






























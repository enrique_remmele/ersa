﻿CREATE view [dbo].[VComprobanteLibroIVA]
as

Select

CLI.IDTransaccion,
CLI.Numero,
CLI.IDTipoComprobante,
'TipoComprobante'=TC.Codigo,
CLI.NroComprobante,
TC.Tipo,

CLI.Fecha,
'Fec'=CONVERT(varchar(50), CLI.Fecha, 6),
CLI.Detalle,
CLI.Cotizacion,
CLI.NroTimbrado,
CLI.FechaVencimientoTimbrado,
CLI.IDCliente,
CLI.IDProveedor,
CLI.IDSucursal,
'Sucursal'= S.Descripcion,
'CodigoSucursal'= S.Codigo,
'Ciudad'=S.Descripcion,
'CodigoCiudad'=S.CodigoCiudad,
CLI.IDOperacion,
'Operacion'=O2.Descripcion,
CLI.Directo,
'TipoIVA'=(Case When (CLI.Directo ) = 0 Then 'INDISTINTO' Else 'DIRECTO' End),

'RUC'=(Case When (CLI.IDCliente ) <> 0 Then C.RUC When (CLI.IDProveedor) <> 0 Then P.RUC End),
'RazonSocial'=(Case When (CLI.IDCliente ) <> 0 Then C.RazonSocial When (CLI.IDProveedor) <> 0 Then P.RazonSocial End),
CLI.Credito, 

'Condicion'=(Case When (CLI.Credito ) = 1 Then 'CRED' Else 'CON' End),

--Moneda
CLI.IDMoneda,
'Moneda'=M.Referencia,

--Totales
CLI.Total,
CLI.TotalImpuesto,
CLI.TotalDiscriminado,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDUsuarioTransaccion'=T.IDUsuario

From ComprobanteLibroIVA CLI
Join VSucursal S On CLI.IDSucursal=S.ID
Join Moneda M on M.ID=CLI.IDMoneda
Join VTipoComprobante TC on TC.ID=CLI.IDTipoComprobante 
Join Transaccion T on T.ID=CLI.IDTransaccion 
Join Operacion O on O.ID=T.IDOperacion 
Join Operacion O2 on O2.ID=CLI.IdOperacion
Left outer Join Cliente C on C.ID=CLI.IDCliente 
Left outer Join Proveedor P on P.ID=CLI.IDProveedor 


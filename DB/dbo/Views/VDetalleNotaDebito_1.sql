﻿

CREATE View [dbo].[VDetalleNotaDebito]
As
Select
DNC.IDTransaccion,

--Producto
DNC.IDProducto,
DNC.ID,
DNC.Observacion, 

--'Producto'=P.Descripcion,
'Descripcion'=(Case When DNC.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DNC.Observacion End),
'CodigoBarra'=P.CodigoBarra,
P.Referencia ,

--Deposito
DNC.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DNC.Cantidad,
DNC.PrecioUnitario,
'Pre. Uni.'=DNC.PrecioUnitario,
DNC.Total,

--Impuesto
DNC.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
DNC.TotalImpuesto,
DNC.TotalDiscriminado,
'Exento'=I.Exento,

--Contabilidar
P.CuentaContableVenta,

--Descuento
DNC.TotalDescuento,
'Descuento'=DNC.TotalDescuento,
DNC.DescuentoUnitario,
'Desc Uni'=DNC.DescuentoUnitario,
DNC.PorcentajeDescuento,
'Desc %'=DNC.PorcentajeDescuento,
'DescuentoUnitarioDiscriminado'=IsNull(DNC.DescuentoUnitarioDiscriminado,0),
'TotalDescuentoDiscriminado'=IsNull(DNC.TotalDescuentoDiscriminado,0),

--Costos
DNC.CostoUnitario,
DNC.TotalCosto,
DNC.TotalCostoImpuesto,
DNC.TotalCostoDiscriminado,

--Venta
DNC.IDTransaccionVenta,
V.Acreditado,
'Comprobante'= IsNull(V.Comprobante , '---'),

--Cantidad de Caja
'Caja'=IsNull(DNC.Caja, 'False'),
DNC.CantidadCaja,
'Cajas'=  ISNULL ((DNC.Cantidad ),1) / ISNULL((Case When (P.UnidadPorCaja) = 0 Then 1 Else P.UnidadPorCaja End),1),
NC.Fecha,
NC.IDSucursal,

--Totales
'TotalBruto'= isnull(DNC.TotalDescuento,0)+isnull(DNC.Total,0),
'TotalSinDescuento'=DNC.Total - DNC.TotalDescuento,
'TotalSinImpuesto'=DNC.Total - DNC.TotalImpuesto,
'TotalNeto'=DNC.Total - (DNC.TotalImpuesto + DNC.TotalDescuento),
'TotalNetoConDescuentoNeto'=DNC.TotalDiscriminado + DNC.TotalDescuentoDiscriminado

From DetalleNotaDebito DNC
Join NotaDebito NC On DNC.IDTransaccion=NC.IDTransaccion
Left Outer Join  VVenta V On DNC.IDTransaccionVenta=V.IDTransaccion
Join Producto P On DNC.IDProducto=P.ID
Join Deposito D On DNC.IDDeposito=D.ID
Join Impuesto I On DNC.IDImpuesto=I.ID







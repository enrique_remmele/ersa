﻿
CREATE View [dbo].[VAuditoriaDocumentos]
As
select A.*,
'Cliente'=ISnull((select concat(Referencia,' - ' , RazonSocial) from Cliente where id = IDCliente),''),
'Proveedor'=Isnull((select concat(Referencia,' - ' , RazonSocial) from Proveedor where id = IDProveedor),''),
'Usuario'=Isnull((select Usuario from usuario where id = IDUsuario),''),
'DescripcionTipoComprobante'=ISnull((select descripcion from TipoComprobante where ID = A.IDTipoComprobante),'VARIOS')
from AuditoriaDocumentos A
where IDTipoComprobante <> 25 --Debito/Credito Bancario
and TipoComprobante <> 'CONCILIACION'
and TipoComprobante <> 'DEB'
and TipoComprobante <> 'CRE'

union all

select A.*,
'Cliente'='',
'Proveedor'=Isnull((select Descripcion from vCuentaBancaria where id = IDProveedor),''),
'Usuario'=Isnull((select Usuario from usuario where id = IDUsuario),''),
'DescripcionTipoComprobante'=ISnull((select descripcion from TipoComprobante where ID = A.IDTipoComprobante),'VARIOS')
from AuditoriaDocumentos A
where (IDTipoComprobante = 25 or IDTipoComprobante = 24 or TipoComprobante = 'CONCILIACION')  --Debito/Credito Bancario







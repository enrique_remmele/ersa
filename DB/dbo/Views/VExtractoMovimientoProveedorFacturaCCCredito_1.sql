﻿

CREATE view [dbo].[VExtractoMovimientoProveedorFacturaCCCredito]

As

--Egreso
Select
C.IDTransaccion, 
'Operacion'=CodigoOperacion + ' ' + isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')),
'Codigo'=C.IDProveedor,
'Fecha'=C.Fecha,
'Documento'=Concat(C.Operacion,' ',C.Numero,' - Comprobante ',C.NroComprobante),
'Detalle/Concepto'=C.Observacion,
'Debito'=0,
--'Credito'=Isnull(Da.Credito/isnull(C.Cotizacion,1),C.Total*isnull(C.Cotizacion,1)),
'Credito'=DA.Credito/Cotizacion,
'Saldo'=0.00,
'Cotizacion'=isnull(C.Cotizacion,1),
'IDMoneda'=C.IDMoneda,
'Movimiento'='EG',
'ComprobanteAsociado'=C.IDTransaccion,
'CodigoCC' = DA.Codigo
From 
VCompraGastoMinimosDatos C 
Join VProveedor P on C.IDProveedor=P.ID 
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Credito>0
where C.CajaChica = 0










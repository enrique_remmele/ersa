﻿CREATE View [dbo].[VNotaCreditoVenta]

As

Select

--Venta
'IDTransaccion'=NCV.IDTransaccionNotaCredito, 
'IDTransaccionVenta'=V.IDTransaccion , 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
V.Condicion,
V.IDSucursal,

--Nota de Credito
NC.Fecha,
'Comp. NC'=NC.[Cod.] + ' ' + NC.Comprobante,

'Usuario NC'=NC.Usuario,
'Tipo NC'=NC.Tipo,
'Estado NC'=NC.[Estado NC], 
NC.Total,
'Importe'=(Case When (NC.Anulado) = 'True' Then 0 Else NCV.Importe End),
'ImporteDiscriminado'= (Select NCV.Importe/I.FactorDiscriminado From Impuesto I where I.ID =(Select Top(1) DI.IDImpuesto from DetalleImpuesto DI where DI.IDTransaccion = V.IDTransaccion and DI.TotalDiscriminado > 0)),

'IDTransaccionNotaCreditoAplicacion'=0,
NC.Observacion,
'Aplicado'='True',
'Anulado'=NC.Anulado

From NotaCreditoVenta  NCV
Join VNotaCredito NC On NCV.IDTransaccionNotaCredito=NC.IDTransaccion
join VVenta  V on NCV.IDTransaccionVentaGenerada  = V .IDTransaccion 
Where NC.Aplicar = 'False'

Union All

Select
NCVA.IDTransaccionNotaCredito, 
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
V.Condicion,
V.IDSucursal,

--Nota de Credito
'Fecha'= NC.Fecha,
'Comp. NC'=NCA.[Cod.] + ' (' + Convert(varchar(50), NCA.Numero) + ') ' + NCA.Comprobante,
'Usuario NC'=NCA.Usuario,
'Tipo NC'='Aplicacion',
'Estado NC'=(Case When NCA.Anulado='True' Then 'Anulado' Else 
			'---' 
			End),

NC.Total,
'Importe'=(Case When (NCA.Anulado) = 'True' Then 0 Else NCVA.Importe End),
'ImporteDiscriminado'= (Select NCVA.Importe/I.FactorDiscriminado From Impuesto I where I.ID =(Select Top(1) DI.IDImpuesto from DetalleImpuesto DI where DI.IDTransaccion = V.IDTransaccion and DI.TotalDiscriminado>0)),

NCVA.IDTransaccionNotaCreditoAplicacion ,
NCA.Observacion,
'Aplicado'='True',
'Anulado'=NCA.Anulado
From NotaCreditoVentaAplicada   NCVA
join VVenta  V on NCVA.IDTransaccionVenta  = V .IDTransaccion 
--No se si es correcto el Join con Nota de Credito...
Join VNotaCredito NC On NCVA.IDTransaccionNotaCredito=NC.IDTransaccion
Join VNotaCreditoAplicacion NCA On NCVA.IDTransaccionNotaCreditoAplicacion=NCA.IDTransaccion




﻿CREATE View [dbo].[VProductoListaPrecio]
As
Select 
'IDListaPrecio'=LP.ID,
'ListaPrecio'=LP.Descripcion,
'Estado'=Case When LP.Estado = 'True' Then 'OK' Else '-' End,
'IDProducto'=P.ID,
'Producto'=P.Descripcion,
'Referencia'=P.Referencia ,
'Precio'=PLP.Precio,
'IDMoneda'=PLP.IDMoneda,
'Moneda'=ISNULL(M.Referencia, '-'),
'Decimales'=ISNULL(M.Decimales, 'False'),

--TPR
'TPR'=IsNull(PLP.TPR, 0),
'TPRPorcentaje'=IsNull(PLP.TPRPorcentaje, 0),
'TPRDesde'=IsNull(convert(dATE, PLP.TPRDesde, 3), NULL),
'TPRHasta'=IsNull(convert(dATE, PLP.TPRHasta, 3), NULL),

--Sucursal
LP.IDSucursal,
'Sucursal'=S.Descripcion

From ListaPrecio LP
Join ProductoListaPrecio PLP On lp.ID = PLP.IDListaPrecio
Join Producto P On PLP.IDProducto  = P.ID
Left Outer Join Moneda M on PLP.IDMoneda = M.ID
Join Sucursal S On LP.IDSucursal=S.ID





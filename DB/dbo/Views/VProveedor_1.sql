﻿


CREATE View [dbo].[VProveedor]
as
Select

--Comerciales
P.ID,
P.RazonSocial,
P.RUC,
P.Referencia,
P.NombreFantasia,
P.Direccion,
P.Telefono,
'TipoCompra'=IsNull(P.TipoCompra, ''),
'RazonSocialReferencia'=P.RazonSocial + ' (' + P.Referencia + ')',

--De configuracion
'Estado'=P.Estado,
P.IDMoneda,
'Moneda'=(Select IsNull((Select V.Descripcion From Moneda V Where V.ID=P.IDMoneda ), '')),
P.IDTipoProveedor,
'TipoProveedor'=(Select IsNull((Select V.Descripcion From TipoProveedor V Where V.ID=P.IDTipoProveedor), '')),
P.Contado,
P.Credito,
'Condicion'=(Case When (P.Contado) = 'True' Then 'CONTADO' Else (Case When (P.Credito) = 'True' Then 'CREDITO' Else '---' End) End),
P.PlazoCredito,


'IDCuentaContableCompra'=Isnull((Select ID from CuentaContable where Codigo = CuentaContableCompra),0),
'CuentaCompra'=(Select IsNull((Select V.Descripcion From VCuentaContable V Where V.Codigo=P.CuentaContableCompra), '')),
'CodigoContableCompra'=P.CuentaContableCompra,
'CuentaContableCompra'=P.CuentaContableCompra,

'IDCuentaContableVenta'=Isnull((Select ID from CuentaContable where Codigo = CuentaContableVenta),0),
'CuentaVenta'=(Select IsNull((Select V.Descripcion From VCuentaContable V Where V.Codigo=P.CuentaContableVenta), '')),
'CodigoContableVenta'=P.CuentaContableVenta,
'CuentaContableVenta'=P.CuentaContableVenta,

'IDCuentaContableAnticipo'=Isnull((Select ID from CuentaContable where Codigo = CuentaContableAnticipo),0),
'CuentaAnticipo'=(Select IsNull((Select V.Descripcion From VCuentaContable V Where V.Codigo=P.CuentaContableAnticipo), '')),
'CodigoContableAnticipo'=P.CuentaContableAnticipo,
'CuentaContableAnticipo'=P.CuentaContableAnticipo,



--Localizacion
P.IDPais,
'Pais'=(Select IsNull((Select V.Descripcion From Pais V Where V.ID=P.IDPais), '')),
P.IDDepartamento,
'Departamento'=(Select IsNull((Select V.Descripcion From Departamento V Where V.ID=P.IDDepartamento), '')),
P.IDCiudad,
'Ciudad'=(Select IsNull((Select V.Descripcion From Ciudad V Where V.ID=P.IDCiudad), '')),
P.IDBarrio,
'Barrio'=(Select IsNull((Select V.Descripcion From Barrio V Where V.ID=P.IDBarrio), '')),

--Referencias
P.IDSucursal,
'Sucursal'=(Select IsNull((Select V.Descripcion From Sucursal V Where V.ID=P.IDSucursal), '')),

--Estadisticos
'UsuarioAlta'=U1.Usuario,
'FechaAlta'=P.FechaAlta,
'Alta'=IsNull(Convert(varchar(50), P.FechaAlta, 6), '---'),

'UsuarioModificacion'=IsNull(U2.Usuario, U1.Usuario),
'FechaModificacion'=P.FechaModificacion,
'Modificacion'=IsNull(Convert(varchar(50), P.FechaModificacion, 6), '---'),

'FechaUltimaCompra'=convert(date, P.FechaUltimaCompra),
'FechaUltimoPago'='',

--Datos Adicionales
P.PaginaWeb,
P.Email,
P.Fax,
'Latitud'=IsNull(P.Latitud, 0),
'Longitud'=IsNull(P.Longitud, 0),

--Retentor
P.Retentor,
P.Exportador,
P.SujetoRetencion,
'Acopiador'= (Case When (P.Acopiador)= 'True' Then 'True' Else 'False' End)

From

Proveedor as P
Left Outer Join Usuario U1 On P.IDUsuarioAlta=U1.ID
Left Outer Join Usuario U2 On P.IDUsuarioModificacion=U2.ID





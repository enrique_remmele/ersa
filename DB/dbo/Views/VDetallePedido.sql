﻿CREATE View [dbo].[VDetallePedido]
As
Select
DP.IDTransaccion,

--Producto
DP.IDProducto,
DP.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DP.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DP.Observacion End),
'CodigoBarra'=P.CodigoBarra,
'UnidadMedida'=P.UnidadMedida,
P.Referencia,
DP.Observacion,
PE.Cotizacion,
PE.IDMoneda,

--Deposito
DP.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DP.Cantidad,
DP.PrecioUnitario,
'Pre. Uni.'=DP.PrecioUnitario,
DP.Total,

--Impuesto
DP.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
DP.TotalImpuesto,
DP.TotalDiscriminado,
'Exento'=I.Exento,
'IDProductoDescuento' = (Case when DP.IDImpuesto = 1 then 218 else
						(Case When DP.IDImpuesto = 2 then 219 else 
						230 end)end),

--Descuento
DP.TotalDescuento,
'Descuento'=DP.TotalDescuento,
DP.DescuentoUnitario,
'Desc Uni'=DP.DescuentoUnitario,
DP.DescuentoUnitarioDiscriminado,
DP.TotalDescuentoDiscriminado,
DP.PorcentajeDescuento,
'Desc %'=DP.PorcentajeDescuento,
'PrecioNeto'=IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0),
'TotalPrecioNeto'=(IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0)) * DP.Cantidad,

--Costos
DP.CostoUnitario,
DP.TotalCosto,
DP.TotalCostoImpuesto,
DP.TotalCostoDiscriminado,

----Cantidad de Caja
'Caja'=IsNull(DP.Caja, 'False'),
DP.CantidadCaja,
'Cajas'=convert (decimal (10,2) ,IsNull((DP.Cantidad / P.UnidadPorCaja), 0)),
'UnidadMedidas'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),'UND'),
--Unidad de medida
'UnidadMedidaConvertir'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),'UND'),
'UnidadConvertir'=Isnull (P.UnidadConvertir,1),

--Totales
'TotalBruto'= isnull(DP.TotalDescuento,0)+isnull(DP.Total,0),
'TotalSinDescuento'=DP.Total - DP.TotalDescuento,
'TotalSinImpuesto'=DP.Total - DP.TotalImpuesto,
'TotalNeto'=DP.Total - (DP.TotalImpuesto + DP.TotalDescuento),
'TotalNetoConDescuentoNeto'=DP.TotalDiscriminado + DP.TotalDescuentoDiscriminado,

--Pedido
PE.Fecha,
PE.IDCliente,
PE.FechaFacturar,
PE.IDVendedor,
'Vendedor'=(select Nombres from Vendedor where id = PE.IDVendedor),
PE.IDSucursal,
'Facturado'=(Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End),
'FechaFactura'=(Select Top(1) V.FechaEmision From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc),
T.IDUsuario,

'CantidadAEntregar'= Isnull(DP.CantidadAEntregar,DP.Cantidad),
PE.Numero,
'Suc'= (Select codigo from Sucursal where Id = PE.IDSucursal),
'Peso'=cast(P.Peso as money),
P.IDTipoProducto,
PE.Anulado,
'EntregaCliente'=(Case When (PE.EntregaCliente) = 'True' Then 'True' Else 'False' End),
'CancelarAutomatico'=ISnull((Select FPF.CancelarAutomatico from FormaPagoFactura FPF where FPF.id = PE.IDFormaPagoFactura), 'False'),
PE.IDFormaPagoFactura,
PE.IDListaPrecio,
PE.IDSucursalCliente

From DetallePedido DP
Join Pedido PE On DP.IDTransaccion=PE.IDTransaccion
Join VProducto P On DP.IDProducto=P.ID
Join Deposito D On DP.IDDeposito=D.ID
Join Impuesto I On DP.IDImpuesto=I.ID
Join Transaccion T on T.ID = PE.IDTransaccion

Where PE.Procesado='True'


























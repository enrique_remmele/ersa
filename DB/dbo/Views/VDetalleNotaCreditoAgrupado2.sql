﻿
Create View [dbo].[VDetalleNotaCreditoAgrupado2]
As

Select
DT.IDTransaccionVenta,
DT.IDProducto,
DT.IDDeposito,
DT.Observacion,
DT.IDImpuesto,
'Cantidad'=IsNull(Sum(DT.Cantidad),0),
DT.PrecioUnitario,
'Total'=IsNull(Sum(DT.Total),0),
'TotalImpuesto'=IsNull(Sum(DT.TotalImpuesto),0),
'TotalDiscriminado'=IsNull(Sum(DT.TotalDiscriminado),0),
DT.PorcentajeDescuento,
DT.DescuentoUnitario,
DT.DescuentoUnitarioDiscriminado,
'TotalDescuento'=IsNull(Sum(DT.TotalDescuento),0),
'TotalDescuentoDiscriminado'=IsNull(Sum(DT.TotalDescuentoDiscriminado),0),
--'CostoUnitario'=IsNull(Sum(DT.CostoUnitario),0),
'TotalCosto'=IsNull(Sum(DT.TotalCosto),0),
'TotalCostoImpuesto'=IsNull(Sum(DT.TotalCostoImpuesto),0),
'TotalCostoDiscriminado'=IsNull(Sum(DT.TotalCostoDiscriminado),0),
DT.Caja,
'CantidadCaja'=IsNull(Sum(DT.CantidadCaja),0),
NC.Devolucion,
NC.Descuento

From DetalleNotaCredito DT
Join NotaCredito NC On DT.IDTransaccion=NC.IDTransaccion

Where NC.Anulado=0
and NC.Procesado = 1
and idtransaccionventa = 571383

Group by	

--DT.IDTransaccion,
DT.IDTransaccionVenta,
DT.IDProducto,
DT.IDDeposito,
DT.Observacion,
DT.IDImpuesto,
DT.PrecioUnitario,
DT.PorcentajeDescuento,
DT.DescuentoUnitario,
DT.DescuentoUnitarioDiscriminado,
--DT.CostoUnitario,
DT.Caja,
NC.Devolucion,
NC.Descuento






﻿CREATE View [dbo].[VAccesoPerfil] 

As 

Select 
M.*,
'IDPerfil'=P.ID,
'Perfil'=P.Descripcion,
AP.Visualizar,
AP.Agregar,
AP.Modificar,
AP.Eliminar,
AP.Anular,
AP.Imprimir,
'Asiento'= isnull(AP.Asiento,0),
'CodigoOrdenar'=Cast(Codigo as varchar(16))

From AccesoPerfil AP
--Join VMenu M On AP.IDMenu=M.Codigo
Join VMenu M On AP.NombreControl = M.NombreControl  -- and AP.IDMenu = M.codigo
Join VPerfil P On AP.IDPerfil=P.ID




﻿CREATE View [dbo].[VRemision]

As

Select

--Punto de Expedicion
R.IDPuntoExpedicion,
'PE'=PE.Descripcion,
PE.Ciudad,
PE.IDCiudad,
PE.IDSucursal,
PE.Sucursal,
PE.ReferenciaSucursal,
PE.ReferenciaPunto,
PE.Timbrado,

--Comprobante
R.IDTransaccion,
R.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
R.NroComprobante,
'Comprobante'=PE.ReferenciaSucursal + '-' + PE.ReferenciaPunto + '-' + Convert(varchar(50), R.NroComprobante),

--Cliente
R.IDCliente,
C.IDTipoCliente,
'Cliente'=C.RazonSocial,
C.Direccion,
R.DomicilioCliente,
C.Telefono, 
c.Referencia ,
C.RUC ,
'IDCiudadCliente'=C.IDCiudad,

--Venta
R.IDMotivo,
'Motivo'=MR.Descripcion,
R.MotivoObservacion,
R.ComprobanteVentaTipo,
R.ComprobanteVenta,
R.FechaExpedicionVenta,
R.TimbradoVenta,

--Fechas
R.FechaInicio,
'Fec Ini'=CONVERT(varchar(50), R .FechaInicio, 5),
R.FechaFin,
'Fec Fin'=CONVERT(varchar(50), R .FechaFin, 5),

--Direcciones
R.DireccionPartida,
R.CiudadPartida,
R.DepartamentoPartida,
R.DireccionLlegada,
R.CiudadLlegada,
R.DepartamentoLlegada,
R.KilometrajeEstimado,

'IDSucursalOperacion'=R.IDSucursal,
'SucursalOperacion'=S.Descripcion,
'Suc Operacion'=S.Codigo,
R.IDDeposito,
'DepositoOperacion'=D.Descripcion,

--Responsables
R.IDDistribuidor,
'Distribuidor'=IsNull(DI.Nombres, ''),
'DistribuidorDocumento'=IsNull(DI.NroDocumento, ''),
R.IDChofer,
'Chofer'=IsNull(CH.Nombres, ''),
'ChoferDocumento'=IsNull(CH.NroDocumento, ''),
'ChoferDireccion'=IsNull(CH.Direccion, ''),
R.IDCamion,
'Camion'=IsNull(CA.Descripcion, ''),
'CamionPatente'=IsNull(CA.Patente, ''),
	
R .Observacion,
R.Anulado,
R.Procesado,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=R.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=R.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=R.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=R.IDTransaccion)


From Remision R
Join Transaccion T On R.IDTransaccion=T.ID
Left Outer Join VPuntoExpedicion PE On R.IDPuntoExpedicion=PE.ID
Left Outer Join TipoComprobante TC On R.IDTipoComprobante=TC.ID
Left Outer Join Cliente C On R.IDCliente=C.ID
Left Outer Join Sucursal S On R.IDSucursal=S.ID
Left Outer Join Deposito D On R.IDDeposito=D.ID
Left Outer Join Distribuidor DI On R.IDDistribuidor=DI.ID
Left Outer Join Chofer CH On R.IDChofer=CH.ID
Left Outer Join Camion CA On R.IDCamion=CA.ID
Left Outer Join MotivoRemision MR On R.IDMotivo=MR.ID


﻿
CREATE View [dbo].[VDMSMinuta]
As
Select
'SC'='EXPRESS CENTRAL',
V.IDSucursal,
'Comprobante'=V.Comprobante,
EsVentaSucursal,
'Depar'=Case When(V.EsVentaSucursal)='True' Then (DEP2.Descripcion) Else (DEP.Descripcion) End,
'Ciudad'=Case When(V.EsVentaSucursal)='True' Then (CIU2.Descripcion) Else (CIU.Descripcion) End,
'Cod Tienda'=Case When(V.EsVentaSucursal)='True' Then (C.Referencia + '_' + CONVERT(varchar(50), CS.ID)) Else (C.Referencia) End,
'Nombre Tienda'=C.RazonSocial,
'SKU'=P.Referencia,
'Descripción SKU'=P.Descripcion,
'Fecha'=V.FechaEmision,
'Gs.'=D.TotalDiscriminado - IsNull(DTC.TotalDiscriminado,0),
'Cajas'=convert(decimal (10,2), IsNull((D.Cantidad / P.UnidadPorCaja), 0)) - convert(decimal (10,2) ,IsNull((DTC.Cantidad / P.UnidadPorCaja), 0)),
'Tons'= (convert(decimal (10,6), IsNull((D.Cantidad / P.UnidadPorCaja), 0)) - convert(decimal (10,6) ,IsNull((DTC.Cantidad / P.UnidadPorCaja), 0))) * P.Peso,
'Cantidad'=D.Cantidad,
'Devuelto'=IsNull(DTC.Cantidad,0),
P.Peso

From DetalleVenta D
Join Venta V On D.IDTransaccion=V.IDTransaccion
Join Producto P On D.IDProducto=P.ID
Left Outer Join Cliente C On V.IDCliente=C.ID

--Matriz
Left Join Sucursal S On C.IDSucursal=S.ID
Left Join Departamento DEP On C.IDDepartamento=DEP.ID
Left Join Ciudad CIU On C.IDCiudad=CIU.ID

--Sucursales de Clientes
Left Join ClienteSucursal CS On V.IDSucursalCliente=CS.ID And V.IDCliente=CS.IDCliente
Left Join Departamento DEP2 On CS.IDDepartamento=DEP2.ID
Left Join Ciudad CIU2 On Cs.IDCiudad=CIU2.ID

--Devoluciones
Left Outer Join DetalleNotaCredito DTC On D.IDTransaccion=DTC.IDTransaccionVenta And D.IDProducto=DTC.IDProducto
Left Outer Join NotaCredito NC On DTC.IDTransaccion=NC.IDTransaccion And NC.Anulado='False'
Where V.Anulado='False' 
And V.Procesado='True' 
And D.Cantidad>IsNULL(DTC.Cantidad,0)




﻿CREATE View [dbo].[VDetalleImpuestoDesglosadoGravado]

As

Select IDTransaccion, 'GRAVADO10%'=IsNull([1],0), 'GRAVADO5%'=IsNull([2],0), 'EXENTO'=IsNull([3],0)
From (SELECT IDTransaccion, IDImpuesto, Total
    FROM VDetalleImpuesto) AS SourceTable
PIVOT
(
Sum(Total)
FOR IDImpuesto IN ([1], [2], [3])
) AS PivotTable;

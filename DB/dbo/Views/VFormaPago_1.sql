﻿



CREATE View [dbo].[VFormaPago]
As

--Efectivo
	Select
	FP.IDTransaccion,
	FP.ID,
	TC.IDOperacion,
	'FormaPago'='Efectivo',
	'Moneda'=M.Referencia,
	'ImporteMoneda'=E.ImporteMoneda,
	'Cambio'=E.Cotizacion,
	'Banco'='---',
	'Comprobante'=E.Comprobante,
	'TipoComprobante'=TC.Codigo,
	'Tipo'='EFE',
	'Importe'=FP.Importe,
	'CancelarCheque'=FP.CancelarCheque,
	FP.Cheque,
	FP.Documento,
	FP.Efectivo,
	'IDTransaccionCheque' = ISNULL(FP.IDTransaccionCheque,0),
	'IDMoneda'=M.ID,
	'FechaFormaPago'= E.Fecha,
	'Observacion'=Isnull(E.Observacion,'-')
	
	From FormaPago FP
	Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
	Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
	Join Moneda M On E.IDMoneda=M.ID

	Union All

	--Cheque Cliente
	Select
	FP.IDTransaccion,
	FP.ID,
	V.IDOperacion,
	'FormaPago'='Cheque',
	'Moneda'=V.Moneda,
	'ImporteMoneda'=V.ImporteMoneda,
	'Cambio'=V.Cotizacion,
	'Banco'=V.Banco,
	'Comprobante'=V.NroCheque,
	'TipoComprobante'=(Case When (V.Diferido)='True' Then 'DIF' Else 'CHQ' End),
	'Tipo'=V.Tipo,
	'Importe'=FP.Importe,
	FP.CancelarCheque,
	FP.Cheque,
	FP.Documento,
	FP.Efectivo,
	'IDTransaccionCheque' = ISNULL(FP.IDTransaccionCheque,0),
	'IDMoneda'=V.IDMoneda,
	'FechaFormaPago'= V.Fecha,
	'Observacion'=V.Cliente

	From FormaPago FP
	Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion

	Union All

	--Documento
	Select
	FP.IDTransaccion,
	FP.ID,
	TC.IDOperacion,
	'FormaPago'='Documento',
	'Moneda'=M.Referencia,
	'ImporteMoneda'=FPD.ImporteMoneda,
	'Cambio'=FPD.Cotizacion,
	'Banco'=Isnull((B.Referencia),'---'),
	'Comprobante'=FPD.Comprobante,
	'TipoComprobante'=TC.Codigo,
	'Tipo'='DOC',
	'Importe'=FP.Importe,
	FP.CancelarCheque,
	FP.Cheque,
	FP.Documento,
	FP.Efectivo,
	'IDTransaccionCheque' = ISNULL(FP.IDTransaccionCheque,0),
	'IDMoneda'=M.ID,
	'FechaFormaPago'= FPD.Fecha,
	'Observacion'=Isnull(FPD.Observacion,'-')

	From FormaPago FP
	Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
	Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
	Join Moneda M On FPD.IDMoneda=M.ID
	Left join Banco B on B.ID = FP.ID

union all

	--Tarjeta
	Select
	FP.IDTransaccion,
	FP.ID,
	TC.IDOperacion,
	'FormaPago'='Tarjeta',
	'Moneda'=M.Referencia,
	'ImporteMoneda'=FPD.ImporteMoneda,
	'Cambio'=FPD.Cotizacion,
	'Banco'='---',
	'Comprobante'=FPD.Comprobante,
	'TipoComprobante'=TC.Codigo,
	'Tipo'='DOC',
	'Importe'=FP.Importe,
	FP.CancelarCheque,
	FP.Cheque,
	FP.Documento,
	FP.Efectivo,
	'IDTransaccionCheque' = ISNULL(FP.IDTransaccionCheque,0),
	'IDMoneda'=M.ID,
	'FechaFormaPago'= FPD.Fecha,
	'Observacion'=Isnull(FPD.Observacion,'-')

	From FormaPago FP
	Join FormaPagoTarjeta FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
	Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
	Join Moneda M On FPD.IDMoneda=M.ID

















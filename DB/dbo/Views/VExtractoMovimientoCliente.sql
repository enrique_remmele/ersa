﻿

CREATE view [dbo].[VExtractoMovimientoCliente]

as

--Venta
Select
V.IDTransaccion, 
'Codigo'=V.IDCliente,
'RazonSocial'=V.Cliente,
V.Referencia,
'Fecha'=FechaEmision,
'Operación'='Pedido(s) ' + ' '+ isnull(convert(varchar(50),P.Numero),convert(varchar(50),'--')),
'Documento'=[Cod.] + ' ' + convert(varchar(50),V.Comprobante) + ' ' + convert(varchar(50),V.Condicion) + (Case when V.Cotizacion = 1 then '' else concat(' (',cast(V.Cotizacion as integer),')') end), 
'Detalle/Concepto'='',
'Debito'=V.Total,
'Credito'=(Case When V.CancelarAutomatico = 'True' Then V.Total Else 0 End),
V.IDMoneda,
V.DescripcionMoneda,
'Movimiento'='VTA',
V.IDSucursal,
V.Cotizacion

From 
VVenta V 
Left Outer Join VPedido P on V.IDTransaccion=P.IDTransaccion 
Where V.Anulado='False'
and V.CancelarAutomatico = 0 and V.Procesado = 1

union all

--Venta Anulada
Select
V.IDTransaccion, 
'Codigo'=V.IDCliente,
'RazonSocial'=V.Cliente,
V.Referencia,
'Fecha'=FechaEmision,
'Operación'='Pedido(s) ' + ' '+ isnull(convert(varchar(50),P.Numero),convert(varchar(50),'--')),
'Documento'=[Cod.] + ' ' + convert(varchar(50),V.Comprobante) + ' ' + convert(varchar(50),V.Condicion) + (Case when V.Cotizacion = 1 then '' else concat(' (',cast(V.Cotizacion as integer),')') end), 
'Detalle/Concepto'='-----ANULADO-----',
'Debito'=0,
'Credito'=0,
V.IDMoneda,
V.DescripcionMoneda,
'Movimiento'='VTA',
V.IDSucursal,
V.Cotizacion

From 
VVenta V 
Left Outer Join VPedido P on V.IDTransaccion=P.IDTransaccion 
where V.Anulado = 'True' and V.CancelarAutomatico = 0 

union all

--Cobranza Crédito
Select
C.IDTransaccion,
'Codigo'=C.IDCliente,
'RazonSocial'=C.Cliente,
C.Referencia,
'Fecha'=C.FechaEmision,
'Operación'= 'Cob. '+ isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')) ,
'Documento'=V.TipoComprobante + ' ' + V.Comprobante + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end), 
'Detalle/Concepto'=C.[Cod.] + ' ' + convert(varchar(50), C.Comprobante) + ' / ' + C.Cobrador,
'Debito'=0,
'Credito'=VC.ImporteReal,
--'Credito'=C.Total,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = C.IDMoneda),
'Movimiento'='CCR',
C.IDSucursal,
C.Cotizacion

From
VCobranzaCredito C
Join VVentaDetalleCobranza VC On C.IDTransaccion=VC.IDTransaccionCobranza
Join VVenta V On VC.IDTransaccion=V.IDTransaccion
WHERE C.Anulado='False'

union all

--Cobranza Crédito anticipo con saldo
Select
C.IDTransaccion,
'Codigo'=C.IDCliente,
'RazonSocial'=C.Cliente,
C.Referencia,
'Fecha'=C.FechaEmision,
'Operación'= 'Cob. Anticipo'+ isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')) ,
'Documento'='',--V.TipoComprobante + ' ' + V.Comprobante + (Case when C.Cotizacion = 1 then '' else concat(' (',cast(C.Cotizacion as integer),')') end), 
'Detalle/Concepto'=C.[Cod.] + ' ' + convert(varchar(50), C.Comprobante) + ' / ' + C.Cobrador,
'Debito'=0,
'Credito'=C.Total - (Select Sum(VC.ImporteReal) from VVentaDetalleCobranza VC where VC.IDTransaccionCobranza = C.IDTransaccion),
--'Credito'=C.Total,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = C.IDMoneda),
'Movimiento'='CCR',
C.IDSucursal,
C.Cotizacion

From
VCobranzaCredito C
--left outer Join VVentaDetalleCobranza VC On C.IDTransaccion=VC.IDTransaccionCobranza
--left outer Join VVenta V On VC.IDTransaccion=V.IDTransaccion
WHERE C.Anulado='False'
and C.Total > (Select Sum(VC.ImporteReal) from VVentaDetalleCobranza VC where VC.IDTransaccionCobranza = C.IDTransaccion)
union all

--Cobranza Contado
Select
CC.IDTransaccion,
'Codigo'=VC.IDCliente,
'RazonSocial'=VC.Cliente,
'Referencia'=VC.Referencia,
'Fecha'=CC.Fecha,
'Operación'='Cobranza '+isnull(convert(varchar(50),CC.Numero),convert(varchar(50),'--')) ,
'Documento'=CC.[Cod.]+''+convert(varchar(50),CC.NroComprobante) + (Case when CC.Cotizacion = 1 then '' else concat(' (',cast(CC.Cotizacion as integer),')') end), 
'Detalle/Concepto'='',
'Debito'=0,
'Credito'=VC.Importe,
'IDMoneda'=VC.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = VC.IDMoneda),
'Movimiento'='CCO',
CC.IDSucursal,
CC.Cotizacion

From
VCobranzaContado CC
Join VVentaDetalleCobranzaContado VC On CC.IDTransaccion=VC.IDTransaccionCobranza
WHERE CC.Anulado='False'

union all

--Nota de Crédito
Select
IDTransaccion,
'Codigo'=IDCliente,
'RazonSocial'=Cliente,
Referencia,
'Fecha'=Fecha,
'Operación'=''+isnull(convert(varchar(50),''),'--') ,
'Documento'=[Cod.]+''+convert(varchar(50),NroComprobante) + (Case when Cotizacion = 1 then '' else concat(' (',cast(Cotizacion as integer),')') end), 
'Detalle/Concepto'='',
'Debito'=0,
'Credito'=Total,
IDMoneda,
DescripcionMoneda,
'Movimiento'='NC',
IDSucursal,
Cotizacion

From
VNotaCredito 
WHERE Anulado='False'

union all

--Nota de Débito
Select
IDTransaccion,
'Codigo'=IDCliente,
'RazonSocial'=Cliente,
Referencia,
'Fecha'=Fecha,
'Operación'=''+isnull(convert(varchar(50),''),'--'),
'Documento'=[Cod.]+''+convert(varchar(50),NroComprobante) + (Case when Cotizacion = 1 then '' else concat(' (',cast(Cotizacion as integer),')') end), 
'Detalle/Concepto'='',
'Debito'=Total,
'Credito'=0,
'IDMoneda'=IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = IDMoneda),
'Movimiento'='ND',
IDSucursal,
Cotizacion

From
VNotaDebito
WHERE Anulado='False'

Union all

--Nota de Credito Aplicacion
Select
NCA.IDTransaccion,
'Codigo'=NCA.IDCliente,
'RazonSocial'=NCA.Cliente ,
NCA.Referencia,
'Fecha'=NCA.Fecha,
'Operación'='ApCr ' + Isnull(convert(varchar(50),NCA.Numero ),'---'),
'Documento'='NCR ' + Isnull(Convert(varchar(50), NC.NroComprobante), '---') + (Case when NCA.Cotizacion = 1 then '' else concat(' (',cast(NCA.Cotizacion as integer),')') end), 
'Detalle/Concepto'='Apl.Cr '+ CONVERT (varchar(50), NCA.Numero) + ': '+ 'NCR-'+ convert (varchar (50),NC.NroComprobante)+ '->'+ ' FAT'+convert(varchar (50), V.NroComprobante),
'Debito'=NCVA.Importe  ,
'Credito'=0,
'IDMoneda'=NCA.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = NCA.IDMoneda),
'Movimiento'='NC',
NCA.IDSucursal,
NCA.Cotizacion

From
VNotaCreditoAplicacion   NCA
join NotaCreditoVentaAplicada NCVA On NCA.IDTransaccion = IDTransaccionNotaCreditoAplicacion 
join VNotaCredito NC On NC.IDTransaccion = NCVA.IDTransaccionNotaCredito 
join VVenta V On V.IDTransaccion = NCVA.IDTransaccionVenta 
WHERE NCA.Anulado='False'

Union All

--Nota de Débito Aplicacion
Select
NCA.IDTransaccion,
'Codigo'=NCA.IDCliente,
'RazonSocial'=NCA.Cliente ,
NCA.Referencia,
'Fecha'=NCA.Fecha,
'Operación'='ApCr ' + isnull(convert(varchar(50),NCA.Numero ),'--'),
'Documento'= 'FAT ' + Isnull(Convert(varchar(50),V.NroComprobante), '---')  + (Case when NCA.Cotizacion = 1 then '' else concat(' (',cast(NCA.Cotizacion as integer),')') end),    
'Detalle/Concepto'='Apl.Cr '+ CONVERT (varchar(50), NCA.Numero) + ': '+ 'NCR-'+ convert (varchar (50),NC.NroComprobante)+ '->'+ ' FAT'+convert(varchar (50), V.NroComprobante) ,
'Debito'=0,
'Credito'=NCVA.Importe,
'IDMoneda'=NCA.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = NCA.IDMoneda),
'Movimiento'='ND',
NCA.IDSucursal,
NCA.Cotizacion

From
VNotaCreditoAplicacion   NCA
join NotaCreditoVentaAplicada NCVA On NCA.IDTransaccion = IDTransaccionNotaCreditoAplicacion 
join VNotaCredito NC On NC.IDTransaccion = NCVA.IDTransaccionNotaCredito 
join VVenta V On V.IDTransaccion = NCVA.IDTransaccionVenta 
WHERE NCA.Anulado='False'

union all
--Cheques Diferidos
Select distinct
CC.IDTransaccion,
'Codigo'=CC.IDCliente,
'RazonSocial'=CC.Cliente ,
CC.CodigoCliente as Referencia,
'Fecha'=CC.Fecha,
'Operación'='Ch.Dif ' + isnull(convert(varchar(50),CC.NroCheque ),'--') ,
--'Documento'= 'FAT ' + Isnull(Convert(varchar(50),V.NroComprobante), '---') ,   
'Documento'= '' + (Case when CC.Cotizacion = 1 then '' else concat(' (',cast(CC.Cotizacion as integer),')') end),    
'Detalle/Concepto'='',
'Debito'=CC.ImporteMoneda * CC.Cotizacion,
'Credito'=0,
'IDMoneda'=CC.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = CC.IDMoneda),
'Movimiento'='CHQ.DIF',
CC.IDSucursal,
CC.Cotizacion

From
VChequeCliente   CC
Join FormaPago FP on FP.IDtransaccionCheque = CC.IDtransaccion
join CobranzaCredito CCR on CCR.idtransaccion = FP.Idtransaccion
Join VVentaDetalleCobranza VC On CCR.IDTransaccion=VC.IDTransaccionCobranza
Join VVenta V On VC.IDTransaccion=V.IDTransaccion
WHERE  CC.Estado <> 'DEPOSITADO'
and  CC.Estado <> 'RECHAZADO'
and  CC.Estado <> 'RECHAZADO CANCELADO'
and CC.Diferido = 'True'-- Solo deben aparecer como deuda los cheques diferidos 
and CC.Estado in ('DESCONTADO','CARTERA')

union all
--Cheques Diferidos
Select distinct
CC.IDTransaccion,
'Codigo'=CC.IDCliente,
'RazonSocial'=CC.Cliente ,
CC.CodigoCliente as Referencia,
'Fecha'=CC.Fecha,
'Operación'='Ch.Dif ' + isnull(convert(varchar(50),CC.NroCheque ),'--') ,
--'Documento'= 'FAT ' + Isnull(Convert(varchar(50),V.NroComprobante), '---') ,   
'Documento'= '' + (Case when CC.Cotizacion = 1 then '' else concat(' (',cast(CC.Cotizacion as integer),')') end),    
'Detalle/Concepto'='',
'Debito'=CC.ImporteMoneda * CC.Cotizacion,
'Credito'=0,
'IDMoneda'=CC.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = CC.IDMoneda),
'Movimiento'='CHQ.DIF',
CC.IDSucursal,
CC.Cotizacion

From
VChequeCliente   CC
Join FormaPago FP on FP.IDtransaccionCheque = CC.IDtransaccion
join CobranzaContado CCR on CCR.idtransaccion = FP.Idtransaccion
Join VVentaDetalleCobranza VC On CCR.IDTransaccion=VC.IDTransaccionCobranza
Join VVenta V On VC.IDTransaccion=V.IDTransaccion
WHERE  CC.Estado <> 'DEPOSITADO'
and  CC.Estado <> 'RECHAZADO'
and  CC.Estado <> 'RECHAZADO CANCELADO'
and CC.Diferido = 'True'-- Solo deben aparecer como deuda los cheques diferidos 
and CC.Estado in ('DESCONTADO','CARTERA')

union all

--Cheques Rechazados
Select distinct
CC.IDTransaccion,
'Codigo'=CC.IDCliente,
'RazonSocial'=CC.Cliente ,
CC.CodigoCliente as Referencia,
--'Fecha'=CC.Fecha,
'Fecha'=Isnull((Select max(Fecha) from ChequeClienteRechazado where IDTransaccionCheque = CC.IDTransaccion),CC.Fecha),
'Operación'='Ch.Rech ' + isnull(convert(varchar(50),CC.NroCheque ),'--'),
--'Documento'= 'FAT ' + Isnull(Convert(varchar(50),V.NroComprobante), '---') ,   
'Documento'= '' + (Case when CC.Cotizacion = 1 then '' else concat(' (',cast(CC.Cotizacion as integer),')') end),    
'Detalle/Concepto'='',
--'Debito'=CC.ImporteMoneda * CC.Cotizacion,
'Debito'=CC.ImporteMoneda,
'Credito'=0,
'IDMoneda'=CC.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = CC.IDMoneda),
'Movimiento'='CHQ.RECH',
CC.IDSucursal,
CC.Cotizacion

From
VChequeCliente   CC
Join FormaPago FP on FP.IDtransaccionCheque = CC.IDtransaccion
--pRUEBA LEFT OUTER
LEFT OUTER join CobranzaCredito CCR on CCR.idtransaccion = FP.Idtransaccion
LEFT OUTER Join VVentaDetalleCobranza VC On CCR.IDTransaccion=VC.IDTransaccionCobranza
LEFT OUTER Join VVenta V On VC.IDTransaccion=V.IDTransaccion
WHERE  CC.Rechazado = 'True'

union all

--Cheques rechazados
Select distinct
CC.IDTransaccion,
'Codigo'=CC.IDCliente,
'RazonSocial'=CC.Cliente ,
CC.CodigoCliente as Referencia,
--'Fecha'=CC.Fecha,
'Fecha'=Isnull((Select Fecha from ChequeClienteRechazado where IDTransaccionCheque = CC.IDTransaccion),CC.Fecha),
'Operación'='Ch.Rech ' + isnull(convert(varchar(50),CC.NroCheque ),'--') ,
--'Documento'= 'FAT ' + Isnull(Convert(varchar(50),V.NroComprobante), '---') ,   
'Documento'= '' + (Case when CC.Cotizacion = 1 then '' else concat(' (',cast(CC.Cotizacion as integer),')') end),    
'Detalle/Concepto'='',
--'Debito'=CC.ImporteMoneda * CC.Cotizacion,
'Debito'=CC.ImporteMoneda,
'Credito'=0,
'IDMoneda'=CC.IDMoneda,
'DescripcionMoneda'=(select descripcion from moneda where id = CC.IDMoneda),
'Movimiento'='CHQ.RECH',
CC.IDSucursal,
CC.Cotizacion

From
VChequeCliente   CC
Join FormaPago FP on FP.IDtransaccionCheque = CC.IDtransaccion
join CobranzaContado CCR on CCR.idtransaccion = FP.Idtransaccion
Join VVentaDetalleCobranza VC On CCR.IDTransaccion=VC.IDTransaccionCobranza
Join VVenta V On VC.IDTransaccion=V.IDTransaccion
WHERE  CC.Rechazado = 'True'

Union all
--Canje
Select distinct
VDetallePagoChequeCliente.IDTransaccion,
'Codigo'=VDetallePagoChequeCliente.IDCliente,
'RazonSocial'=VDetallePagoChequeCliente.Cliente ,
'Referencia'= (select referencia from cliente where id = VDetallePagoChequeCliente.IDCliente),
'Fecha'=(select Fecha from PagoChequeCliente where PagoChequeCliente.IDTransaccion = VDetallePagoChequeCliente.IDTransaccionPagoChequeCliente),
'Operación'='Canje ' + isnull(convert(varchar(50),VDetallePagoChequeCliente.NroCheque ),'--'),
--'Documento'= 'FAT ' + Isnull(Convert(varchar(50),V.NroComprobante), '---') ,   
'Documento'= '',   
'Detalle/Concepto'='',
'Debito'=0,
'Credito'=VDetallePagoChequeCliente.Importe,
'IDMoneda'=(select IDMoneda from vChequeCliente where vChequeCliente.IDTransaccion = VDetallePagoChequeCliente.IDTransaccion),
'DescripcionMoneda'=(select descripcion from moneda where id =(select IDMoneda from vChequeCliente where vChequeCliente.IDTransaccion = VDetallePagoChequeCliente.IDTransaccion)),
'Movimiento'='CANJE',
'IDSucursal'=(select IDSucursal from PagoChequeCliente where PagoChequeCliente.IDTransaccion = VDetallePagoChequeCliente.IDTransaccionPagoChequeCliente),
'Cotizacion'=(select Cotizacion from vChequeCliente where vChequeCliente.IDTransaccion = VDetallePagoChequeCliente.IDTransaccion)
From VDetallePagoChequeCliente 
Join VPagoChequeCliente on VPagoChequeCliente.Idtransaccion = VDetallePagoChequeCliente.IdTRansaccionPagoChequeCliente
where VPagoChequeCliente.Anulado = 0



Union All

--Venta Importadas con cobros parciales en sistema anterior
Select
V.IDTransaccion, 
'Codigo'=V.IDCliente,
'RazonSocial'=V.Cliente,
V.Referencia,
'Fecha'=Convert(date, '20151231'),
'Operación'='Migracion',
'Documento'=[Cod.] + ' ' + convert(varchar(50),V.Comprobante) + ' ' + convert(varchar(50),V.Condicion)  + (Case when V.Cotizacion = 1 then '' else concat(' (',cast(V.Cotizacion as integer),')') end),  
'Detalle/Concepto'='Cobros parciales en sistema anterior',
'Debito'=0,
'Credito'=VI.Cobrado,
V.IDMoneda,
V.DescripcionMoneda,
'Movimiento'='COB. MIGRACION',
V.IDSucursal,
V.Cotizacion

From 
VVenta V 
Join VentaImportada VI On V.IDTransaccion=VI.IDTransaccion

Where V.Anulado='False'








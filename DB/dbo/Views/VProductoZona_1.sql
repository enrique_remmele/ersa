﻿
CREATE View [dbo].[VProductoZona]
As
Select
PZ.*,
'Sucursal'=S.Descripcion,
'Deposito'=D.Descripcion,
'Zona'=ZD.Descripcion

From ProductoZona PZ
Join VProducto V On PZ.IDProducto=V.ID
Join Sucursal S on PZ.IDSucursal = S.ID
Join Deposito D On PZ.IDDeposito = D.ID
Join ZonaDeposito ZD On PZ.IDZonaDeposito=ZD.ID

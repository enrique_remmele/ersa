﻿CREATE view [dbo].[VVentaLoteDistribucion]
as
Select
VLD.IDTransaccionLote,
VLD.IDTransaccionVenta,
VLD.ID,
V.IDSucursal,
'Suc'=V.Sucursal,
V.[Cod.],
V.Comprobante,
'FechaFact'=V.Fec,
'VencimientoFact'= Case When V.Condicion = 'CONT' Then V.Fec  else V.[Fec. Venc.] End ,
V.[Fec. Venc.],
'Saldo'=VLD.Importe,
V.Condicion,
V.Moneda,
V.Cotizacion,
'NumeroPedido' = (Select Top(1) P.Numero From PedidoVenta PV
				  Join Pedido P On P.IDTransaccion = PV.IDTransaccionPedido and P.IDCliente = C.ID
				  Where  PV.IDTransaccionVenta = VLD.IDTransaccionVenta
				  Order By P.IDTransaccion Desc),
V.Vendedor,

--Lote
LD.Numero,
'CiudadLote'=LD.Ciudad,
LD.IDCiudad ,
'SucLote'=LD.Suc ,
'FechaLote'=LD.Fec,
LD.[Fec Reparto], 
LD.IDDistribuidor,
LD.Distribuidor,
LD.IDCamion,
LD.Camion ,
LD.Chofer,
LD.IDChofer,
V.IDCliente,
'Cliente'=V.Cliente,
V.Referencia,
'Suc. Cliente'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.Sucursal From ClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else 'MATRIZ' End),
'ZonaVenta'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.ZonaVenta From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.ZonaVenta End),
'Ciudad'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.Ciudad From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Ciudad End),
'Barrio'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.Barrio From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Barrio End),
'Direccion'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.Direccion From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Direccion End),
'Telefono'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.Telefono From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Telefono End),
'Contacto'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.Contacto From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.NombreFantasia End),

'Latitud'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.Latitud From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Latitud End),
'Longitud'=(Case When (V.EsVentaSucursal)='True' Then (Select top(1) CS.Longitud From VClienteSucursal CS Where CS.IDCliente=V.IDCliente And CS.ID=V.IDSucursalCliente) Else C.Longitud End),

'IDSucursalCliente'= (Case When (V.EsVentaSucursal)='True' Then V.IDSucursalCliente Else 1 End),
'IDUsuario'= LD.IDUsuario,
'Usuario'= LD.Usuario,

--Totales
'Contado'=Case When V.Credito='True' Then 0 Else VLD.Importe End,
'Credito'=Case When V.Credito='True' Then VLD.Importe Else 0 End,
'Total'=V.Total,
'TotalImpuesto'=V.TotalImpuesto,
'TotalDiscriminado'=V.TotalDiscriminado,
'TotalDescuento'=V.TotalDescuento,
'SaldoComprobante'=V.Saldo,
--Forma de Pago Factura
V.IdFormaPagoFactura,
'Forma.Pago'=(Select top(1) FP.Referencia From FormaPagoFactura FP Where FP.ID=V.IdFormaPagoFactura),
'CodVendedor' = Ve.Referencia,
V.Observacion,
C.NombreFantasia,
'FechaHoraInicioDescarga'= isnull((select top(1) fecha_hora_inicio_descarga from cast_t_orden_carga_estado where orden_carga = LD.Comprobante and numero_factura=v.NroComprobante),''),
'FechaHoraFinDescarga'=isnull((select top(1) fecha_hora_fin_descarga from cast_t_orden_carga_estado where orden_carga = LD.Comprobante and numero_factura=v.NroComprobante),''),
'EstadoCast' = isnull((Select top(1) estado from cast_t_orden_carga_estado where orden_carga = LD.Comprobante and numero_factura=v.NroComprobante),'Pendiente'),
'MotivoNoEntregado'=isnull((Select top(1) motivo_no_entregado from cast_t_orden_carga_estado where orden_carga = LD.Comprobante and numero_factura=v.NroComprobante),''),
'ObservacionCast'=isnull((Select top(1) observacion from cast_t_orden_carga_estado where orden_carga = LD.Comprobante and numero_factura=v.NroComprobante),''),
'HoraProgramadaDesde'= V.HoraDesde,
'HoraProgramadaHasta' = V.HoraHasta,
'Peso'=(Select top(1) sum(peso) from vdetalleventa where idtransaccion = V.IDTransaccion),
'Anulado'=ISnull(LD.Anulado,'False')

From VentaLoteDistribucion VLD
Join VVenta V On VLD.IDTransaccionVenta=V.IDTransaccion
Join VCliente C On V.IDCliente = C.ID
Join VLoteDistribucion LD on LD.IDTransaccion=VLD.IDTransaccionLote 
join FormaPagoFactura FP on FP.ID = V.IdFormaPagoFactura
left outer join Vendedor Ve on Ve.ID = V.Idvendedor
















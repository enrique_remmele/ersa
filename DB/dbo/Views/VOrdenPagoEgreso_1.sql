﻿


CREATE View [dbo].[VOrdenPagoEgreso]
As

--Compras
Select
C.IDTransaccion,
C.NroComprobante,
'T. Comp.'=C.TipoComprobante,
C.TipoComprobante,
'Comprobante'= concat (C.TipoComprobante , ' ',C.NroComprobante),
C.Proveedor,
C.IDProveedor,
P.Retentor,
C.Fecha,
C.Fec,
C.FechaVencimiento,
C.[Fec. Venc.],
Cuota=isnull(OPE.Cuota,1),
--Totales
C.Total,
OPE.Saldo,
OPE.Importe,
C.TotalImpuesto,
C.TotalDiscriminado,
C.IDMoneda,
C.Moneda,
C.Credito ,
'Cotizacion' = (Case when isnull(C.IDMoneda,0) = 1 then 1 else (case when isnull(C.Cotizacion,0)=0 then 1 else C.Cotizacion end) end),
C.Observacion,
'Seleccionado'='True',
'Cancelar'='False',
'PorcRetencion'=(Select Top 1 CompraPorcentajeRetencion From Configuraciones),
'RetencionIVA'= ISNULL(OPE.Retencion,0),
--'RetencionIVA'=(Case When (OP.IDTransaccion = (Select min(IDTransaccionOrdenPago) from OrdenPagoEgreso where OrdenPagoEgreso.IDTransaccionEgreso = OPE.IDTransaccionEgreso)) then ISNull(C.RetencionIVA,0) else 0 end),
'RetencionRenta'=ISNULL(C.RetencionRenta,0),

--Orden de Pago
OPE.IDTransaccionOrdenPago,
OP.Numero,
OP.IDSucursal,
OP.Suc,
'NroOP'=OP.NroComprobante,
'TipoComprobanteOP'= OP.TipoComprobante,

--Cheque
OP.NroCheque,
'FechaOP' = OP.Fecha,
'RRHH'='False',
OPE.ProcesaRetencion

From OrdenPagoEgreso OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join VCompra C On OPE.IDTransaccionEgreso=C.IDTransaccion
Join VProveedor P on P.ID=C.IDProveedor

Union All

Select
C.IDTransaccion,
C.NroComprobante,
'T. Comp.'=C.TipoComprobante,
C.TipoComprobante,
'Comprobante'= concat (C.TipoComprobante , ' ',C.NroComprobante),
C.Proveedor,
C.IDProveedor,
P.Retentor,
C.Fecha,
C.Fec,
C.FechaVencimiento,
C.[Fec. Venc.],
Cuota=isnull(OPE.Cuota,1),
--Totales
C.Total,
OPE.Saldo,
OPE.Importe,
C.TotalImpuesto,
C.TotalDiscriminado,
C.IDMoneda,
C.Moneda,
C.Credito ,
'Cotizacion' = (Case when isnull(C.IDMoneda,0) = 1 then 1 else (case when isnull(C.Cotizacion,0)=0 then 1 else C.Cotizacion end) end),
C.Observacion,
'Seleccionado'='True',
'Cancelar'='False',
'PorcRetencion'=(Select Top 1 CompraPorcentajeRetencion From Configuraciones),
'RetencionIVA'= ISNULL(OPE.Retencion,0),
--'RetencionIVA'=(Case When (OP.IDTransaccion = (Select min(IDTransaccionOrdenPago) from OrdenPagoEgreso where OrdenPagoEgreso.IDTransaccionEgreso = OPE.IDTransaccionEgreso)) then ISNull(C.RetencionIVA,0) else 0 end),
'RetencionRenta'=ISNULL(C.RetencionRenta,0),

--Orden de Pago
OPE.IDTransaccionOrdenPago,
OP.Numero,
OP.IDSucursal,
OP.Suc,
'NroOP'=OP.NroComprobante,
'TipoComprobanteOP'= OP.TipoComprobante,

--Cheque
OP.NroCheque,
'FechaOP' = OP.Fecha,
'RRHH'=C.RRHH,
OPE.ProcesaRetencion

From OrdenPagoEgreso OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join VGasto C On OPE.IDTransaccionEgreso=C.IDTransaccion
Join VProveedor P on P.ID=C.IDProveedor

Union All

Select
C.IDTransaccion,
'NroComprobante'=convert(varchar(50),C.NroComprobante),
'T. Comp.'='Vale',
TipoComprobante='Vale',
'Tipo y Nro.Comprobante'=concat (TipoComprobante , ' ',C.NroComprobante),
'Proveedor'='---',
'IDProveedor'=0,
'Retentor'='False',
C.Fecha,
C.Fec,
C.Fecha,
C.Fec,
Cuota=isnull(OPE.Cuota,1),
--Totales
C.Total,
OPE.Saldo,
OPE.Importe,
'TotalImpuesto'=0,
'TotalDiscriminado'=0,
C.IDMoneda,
C.Moneda,
'Credito'='False' ,
C.Cotizacion,

'Observacion'='---',
'Seleccionado'='True',
'Cancelar'='False',
'PorcRetencion'=(Select Top 1 CompraPorcentajeRetencion From Configuraciones),
'RetencionIVA'= ISNULL(OPE.Retencion,0),
'RetencionRenta'=0,

--Orden de Pago
OPE.IDTransaccionOrdenPago,
OP.Numero,
OP.IDSucursal,
OP.Suc,
'NroOP'=OP.NroComprobante,
'TipoComprobanteOP'= OP.TipoComprobante,

--Cheque
OP.NroCheque,
'FechaOP' = OP.Fecha,
'RRHH'='False',
OPE.ProcesaRetencion

From OrdenPagoEgreso OPE
Join VOrdenPago OP On OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join VVale C On OPE.IDTransaccionEgreso=C.IDTransaccion





﻿

CREATE view [dbo].[VProductoUtilidad]
as
Select
Top(10)
Producto,
'TotalVenta'=Total ,
TotalCosto, 
'Utilidad'=(Total - TotalCosto ),
FechaEmision
From VDetalleVenta 
 Order by Utilidad Desc


﻿CREATE View [dbo].[VC_ExcepcionesPRUEBA]
As
Select
'Cod_Lista'=LP.ID,
'Cod_Cliente'=C.Referencia,
'Cod_Producto'=P.Referencia,
'Precio_Excepcion'=PLP.Precio-PLPE.Descuento,
'Vigencia'=PLPE.Hasta

From vProductoListaPrecioExcepciones PLPE
Join Producto P On PLPE.IDProducto=P.ID
Join ListaPrecio LP On PLPE.ListaPrecio=LP.Descripcion
Join Cliente C On PLPE.IDCliente=C.ID
Join ProductoListaPrecio PLP On PLPE.IDProducto=PLP.IDProducto And PLPE.IDListaPrecio=PLP.IDListaPrecio
Where PLPE.IDTipoDescuento = 2


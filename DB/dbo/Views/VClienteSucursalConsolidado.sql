﻿
CREATE VIEW [dbo].[VClienteSucursalConsolidado]
AS
SELECT     CS.[IDCliente], CS.[ID], C.RazonSocial, C.[RUC], C.[Tipo], C.[Referencia], C.[NombreFantasia], CS.[Sucursal], S.Descripcion AS SucursalExpress, 
                      CS.[Direccion], CS.[Contacto], CS.[Telefono], CASE CS.[Estado] WHEN 1 THEN 'ACTIVA' WHEN 0 THEN 'INACTIVA' ELSE 'INDEFINIDO' END AS Estado, 
                      CS.[Vendedor], CS.[Pais], CS.[Departamento], CS.[Ciudad], CS.[Barrio], CS.[ZonaVenta], CS.[ListaPrecio], CS.[Cobrador], CS.[Latitud], CS.[Longitud], 
                      CS.[Condicion], 'SUCURSAL' AS TipoSucursal
FROM         dbo.VCliente C JOIN
                      dbo.VClienteSucursal CS ON C.ID = CS.IDCliente JOIN
                      dbo.Sucursal S ON C.IDSucursal = S.ID
WHERE     C.TieneSucursales = 'True'
UNION ALL
SELECT     C.ID AS IDCliente, 0 AS 'ID', C.[RazonSocial], C.[RUC], C.[Tipo], C.[Referencia], C.[NombreFantasia], C.[Sucursal], S.Descripcion AS SucursalExpress, 
                      C.[Direccion], NULL AS Contacto, C.[Telefono], C.[Estado], C.[Vendedor], C.[Pais], C.[Departamento], C.[Ciudad], C.[Barrio], C.[ZonaVenta], C.[ListaPrecio],
                       C.[Cobrador], C.[Latitud], C.[Longitud], C.[Condicion], 'MATRIZ' AS TipoSucursal
FROM         dbo.VCliente C JOIN
                      dbo.Sucursal S ON C.IDSucursal = S.ID


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[17] 4[20] 2[31] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 2295
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VClienteSucursalConsolidado';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VClienteSucursalConsolidado';


﻿CREATE view [dbo].[VExtractoMovimientoProductoDetalleCosteado]
as
--Entrada
--Compra
Select 
C.IDTransaccion,
DC.IDProducto,
DC.IDTipoProducto,
DC.IDLinea,
DC.Producto,
'Referencia'=DC.Referencia,
'Fecha'=convert(date, C.Fecha),
'Fec'=convert (varchar(50),C.Fecha,5),
'Movimiento'='COMPRA',
'Operación'='Cpra: ' + convert (varchar(50), C.Numero),
'Tipo'= 'COMPRA',
C.[Cod.],
'NroComprobante'=convert(varchar(50), C.NroComprobante),
C.IDDeposito,
C.IDSucursal,
'Cliente/Proveedor'=C.Proveedor,
'ComprobanteRelacionado'='',
'Entrada'=DC.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.FechaEntrada),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.FechaEntrada, 5),
'Costo'=Convert(decimal(18,3),(DC.TotalDiscriminado/DC.Cantidad) * C.Cotizacion),
'EstableceCosto'=1,
'Anulado'='False'

From VCompra C 
join VDetalleCompra DC on C.IDTransaccion=DC.IDTransaccion 
Join Transaccion T On C.IDTransaccion=T.ID

Union all

--Ticket de Bascula
Select 
C.IDTransaccion,
C.IDProducto,
C.IDTipoProducto,
C.IDLinea,
C.Producto,
'Referencia'=C.ReferenciaProducto,
'Fecha'=convert(date, C.Fecha),
'Fec'=convert (varchar(50),C.Fecha,5),
'Movimiento'='TICKET DE BASCULA',
'Operación'='TIBA: ' + convert (varchar(50), C.Numero),
'Tipo'= 'TICKET DE BASCULA',
'Cod.'=C.TipoComprobante,
'NroComprobante'=convert(varchar(50), C.NroComprobante),
C.IDDeposito,
C.IDSucursal,
'Cliente/Proveedor'=C.Proveedor,
'ComprobanteRelacionado'='',
'Entrada'=C.PesoBascula,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.Fecha, 5),
'Costo'=(C.TotalDiscriminado/C.PesoBascula),
'EstableceCosto'=1,
'Anulado'='False'

From VTicketBascula C 
Join Transaccion T On C.IDTransaccion=T.ID
Where Procesado = 'True'
and C.NroAcuerdo > 0

union all

--Nota de Crédito Cliente
Select
NC.IDTransaccion,
DNC.IDProducto,
DNC.IDTipoProducto,
DNC.IDLinea,
DNC.Producto,
'Referencia'=DNC.Referencia,
'Fecha'=convert(date, NC.Fecha),
'Fec'=convert (varchar(50),NC.Fecha,10),
'Movimiento'='NOTA DE CREDITO CLIENTE',
'Operación'='NOTA DE CREDITO CLIENTE',
'Tipo'= 'ENTRADA',
NC.[Cod.],
convert(varchar(50), NC.NroComprobante),
NC.IDDeposito,
NC.IDSucursal,
'Cliente/Proveedor'=NC.Cliente,
'ComprobanteRelacionado'=NC.Comprobante,
'Entrada'=DNC.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
DNC.CostoUnitario,
'EstableceCosto'=0,
--'Anulado'='False'
NC.Anulado
From VNotaCredito NC
join VDetalleNotaCredito DNC on NC.IDTransaccion=DNC.IDTransaccion
Join Transaccion T On NC.IDTransaccion=T.ID
Where NC.Procesado='True'

union all

--Movimiento Entrada
Select
M.IDTransaccion,
DM.IDProducto,
DM.IDTipoProducto,
DM.IDLinea,
DM.Producto,
'Referencia'=DM.Ref,
'Fecha'= convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
'Movimiento'=M.Motivo,
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+ ' ' + TipoComprobante + '-' +convert (Varchar (30),M.NroComprobante) ,
'Tipo'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoEntrada,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
DM.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='False'
M.Anulado
From VMovimiento M 
join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Where DM.Entrada='True'and DM.Salida='False'

union all

--Movimiento Salida Anulada
Select
M.IDTransaccion,
DM.IDProducto,
DM.IDTipoProducto,
DM.IDLinea,
DM.Producto,
'Referencia'=DM.Ref,
'Fecha'=convert(date, M.FechaAnulacion ),
'Fec'=convert (varchar(50),M.FechaAnulacion ,10),
'Movimiento'=M.Motivo,
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'Tipo'= M.Operacion +' '+ 'ANULADA',
M.[Cod.],
M.NroComprobante,
M.IDDepositoSalida,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
DM.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='True'
M.Anulado
From VMovimiento M 
join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Where DM.Entrada='False'And DM.Salida='True'
And M.Anulado = 'True'

Union All

--Transferencia Entrada
Select
M.IDTransaccion,
DM.IDProducto,
DM.IDTipoProducto,
DM.IDLinea,
DM.Producto,
'Referencia'=DM.Ref,
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
'Movimiento'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero),
'Operación'=M.Operacion +' - '+ M.NroComprobante,
'OP'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoEntrada ,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
DM.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='False'
M.Anulado

From VMovimiento M 
join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Where DM.Entrada='True'and DM.Salida='True'

Union All

--Transferencia Salida Anulada
Select
M.IDTransaccion,
DM.IDProducto,
DM.IDTipoProducto,
DM.IDLinea,
DM.Producto,
'Referencia'=DM.Ref,
'Fecha'=convert(date, M.FechaAnulacion),
'Fec'=convert (varchar(50),M.FechaAnulacion,10),
'Movimiento'=M.Motivo+' '+ 'ANULADA',
'Operación'=M.Operacion+' - '+M.NroComprobante+' - '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'Tipo'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoSalida ,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
DM.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='True'
M.Anulado
From VMovimiento M 
join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On DM.IDDepositoSalida=D.ID 
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Where DM.Entrada='True'And DM.Salida='True'
And M.Anulado = 'True'

Union all

--Ajuste Inventario Positivo
Select
V.IDTransaccion,
EDI.IDProducto,
EDI.IDTipoProducto,
EDI.IDLinea, 
EDI.Producto,
'Referencia'=EDI.Ref,
'Fecha'=convert(date, V.FechaAjuste),
'Fec'=convert (varchar(50),V.FechaAjuste,10),
'Movimiento'='AJUSTE POSITIVO',
'Operación'='AJUSTE INVENTARIO POSITIVO',
'Tipo'= 'ENTRADA',
'Cod.'='',
'NroComprobante'='',
EDI.IDDeposito,
EDI.IDSucursal,
'Cliente/Proveedor'=(Case When V.Observacion = Null Then '---' Else (Case When V.Observacion='' Then '---' Else V.Observacion End) End)  ,
'ComprobanteRelacionado'='--',
'Entrada'=EDI.DiferenciaSistema,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, V.FechaAjuste),
'FechaOperacion'=CONVERT(date, V.FechaAjuste),
--'Hora Ope.'=Convert(varchar(10), V.FechaAjuste, 8),
--'Fec. Ope.'=Convert(varchar(10), V.FechaAjuste, 5)
'Hora Ope.'=Convert(varchar(10), V.FechaTransaccion, 8),
'Fec. Ope.'=Convert(varchar(10), V.FechaTransaccion, 5),
EDI.Costo,
'EstableceCosto'=0,
--'Anulado'='False'
V.Anulado
From VControlInventario V
 join VExistenciaDepositoInventario EDI on V.IDTransaccion=EDI.IDTransaccion 
 Join Transaccion T On V.IDTransaccion=T.ID
Where EDI.DiferenciaSistema >0 And V.Procesado='True' And V.Anulado='False'

union all

--Ventas Anuladas
Select 
V.IDTransaccion,
DV.IDProducto,
DV.IDTipoProducto,
DV.IDLinea,
DV.Producto,
'Referencia'=DV.Referencia,
'Fecha'=Convert(date, V.FechaAnulacion),
'Fec'=convert (varchar(50),V.FechaAnulacion,10),
'Movimiento'='VENTA',
'Operación'='VENTA ANULADA',
'Tipo'= 'ENTRADA',
V.[Cod.],
'NroComprobante'=convert(varchar(50), V.NroComprobante),
V.IDDeposito,
V.IDSucursal,
'Cliente/Proveedor'=V.Cliente,
'ComprobanteRelacionado'='Venta:'+IsNull(convert(varchar(50), (V.Comprobante)), '---'),
'Entrada'=DV.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
DV.CostoUnitario,
'EstableceCosto'=0,
--'Anulado'='True'
V.Anulado
From VVenta V
join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion 
Join Transaccion T On V.IDTransaccion=T.ID
Join DocumentoAnulado DA On V.IDTransaccion=DA.IDTransaccion
Where V.Anulado='True'
And V.Procesado='True'

Union All

--Ajuste Inicial positivo
Select 
A.IDTransaccion,
D.IDProducto,
0,
0,
D.Producto,
'Referencia'=D.Referencia,
'Fecha'=convert(date, A.Fecha),
'Fec'=convert (varchar(50),A.Fecha,10),
'Movimiento'='AJUSTE INICIAL POSITIVO',
'Operación'='AJUSTE Nro: ' + CONVERT(varchar(50), A.Numero),
'Tipo'= 'ENTRADA',
'Cod.'='',
'NroComprobante'=convert(varchar(50), A.Numero),
D.IDDeposito,
D.IDSucursal,
'Cliente/Proveedor'='AJUSTE INICIAL POSITIVO',
'ComprobanteRelacionado'=A.Observacion,
'Entrada'=D.Entrada,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
--'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
--'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5)
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
0,
'EstableceCosto'=0,
'Anulado'='False'

From AjusteInicial A 
join VDetalleAjusteInicial D on A.IDTransaccion=D.IDTransaccion 
Join Transaccion T On A.IDTransaccion=T.ID
Where A.Procesado='True'
And D.Entrada>0 

--SALIDAS

Union All

--Venta
Select 
V.IDTransaccion,
DV.IDProducto,
DV.IDTipoProducto,
DV.IDLinea,
P.Descripcion,
'Referencia'=P.Referencia,
'Fecha'=convert(date, V.FechaEmision),
'Fec'=convert (varchar(50),V.FechaEmision,10),
'Movimiento'='VENTA',
'Operación'='VENTA'+ ' '+'Fact:'+CONVERT (varchar (50), V.NroComprobante) ,
'Tipo'= 'VENTA',
TC.Codigo,
'NroComprobante'=convert(varchar(50), V.NroComprobante),
V.IDDeposito,
V.IDSucursal,
'Cliente/Proveedor'=C.RazonSocial,
'ComprobanteRelacionado'='Pedido(s):'+IsNull(convert(varchar(50), (Select Top(1) PV.NumeroPedido From VPedidoVenta PV Where PV.IDTransaccionVenta=V.IDTransaccion)), '---'),
'Entrada'=0.00,
'Salida'=DV.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
DV.CostoUnitario,
'EstableceCosto'=0,
--'Anulado'='False'
V.Anulado
From Venta V
join VDetalleVenta DV on V.IDTransaccion=DV.IDTransaccion 
Join Producto P On DV.IDProducto=P.ID
Join Cliente C On V.IDCliente=C.ID
join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Join Transaccion T On V.IDTransaccion=T.ID
--Left Outer join VPedidoVenta PV on V.IDTransaccion=PV.IDTransaccionVenta 
Where V.Procesado='True'


union all

--Nota de Crédito Cliente ANULADO
Select
NC.IDTransaccion,
DNC.IDProducto,
DNC.IDTipoProducto,
DNC.IDLinea,
DNC.Producto,
'Referencia'=DNC.Referencia,
'Fecha'=convert(date, NC.FechaAnulacion),
'Fec'=convert (varchar(50),NC.FechaAnulacion,10),
'Movimiento'='NOTA DE CREDITO CLIENTE ANULADA',
'Operación'='NOTA DE CREDITO CLIENTE ANULADA',
'Tipo'= 'SALIDA',
NC.[Cod.],
convert(varchar(50), NC.NroComprobante),
NC.IDDeposito,
NC.IDSucursal,
'Cliente/Proveedor'=NC.Cliente,
'ComprobanteRelacionado'=NC.Comprobante,
'Entrada'=0.00,
'Salida'=DNC.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
DNC.CostoUnitario,
'EstableceCosto'=0,
--'Anulado'='True'
NC.ANulado
From VNotaCredito NC
join VDetalleNotaCredito DNC on NC.IDTransaccion=DNC.IDTransaccion
Join Transaccion T On NC.IDTransaccion=T.ID
Join DocumentoAnulado DA On NC.IDTransaccion=DA.IDTransaccion
WHERE DNC.Anulado = 1
And NC.Procesado='True'

union all

--Nota Crédito Proveedor
Select 
NCP.IDTransaccion,
DNP.IDProducto,
DNP.IDTipoProducto,
DNP.IDLinea,
DNP.Producto,
'Referencia'=NCP.Referencia,
'Fecha'=convert(date, NCP.Fecha),
'Fec'=convert (varchar(50),NCP.Fecha,10),
'Movimiento'='NOTA DE CREDITO PROVEEDOR',
'Operación'='NOTA CREDITO PROVEEDOR',
'TIPO'= 'SALIDA',
NCP.[Cod.],
'NroComprobante'=NCP.Comprobante,
NCP.IDDeposito,
NCP.IDSucursal,
'Cliente/Proveedor'=NCP.Proveedor,
'ComprobanteRelacionado'=C.Comprobante +':'+' ' + NCP.Observacion ,
'Entrada'=0.00,
'Salida'=DNP.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
DNP.CostoUnitario,
'EstableceCosto'=0,
--'Anulado'='False'
NCP.ANulado
From VNotaCreditoProveedor NCP
join VDetalleNotaCreditoProveedor DNP on NCP.IDTransaccion=DNP.IDTransaccion 
join NotaCreditoProveedorCompra NCPC on NCPC.IDTransaccionNotaCreditoProveedor=NCP.IDTransaccion And DNP.ID=NCPC.ID
join VCompra C on C.IDTransaccion=NCPC.IDTransaccionEgreso
Join Transaccion T On NCP.IDTransaccion=T.ID

union all

--Movimiento Salida
Select
M.IDTransaccion,
DM.IDProducto,
DM.IDTipoProducto,
DM.IDLinea,
DM.Producto,
'Referencia'=DM.Ref,
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
'Movimiento'=M.Motivo,
'Operación'=M.Operacion+': '+ M.CodigoSucursalOperacion +' '+ CONVERT ( varchar (30), M.Numero)+ '  ' + TipoComprobante + ' - ' +convert (Varchar (30),M.NroComprobante) ,
'Tipo'= M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoSalida,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
DM.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='False'
M.Anulado
From VMovimiento M 
join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Where DM.Entrada='False'and DM.Salida='True'

Union All 

--Movimiento Entrada Anulado

Select
M.IDTransaccion,
DM.IDProducto,
DM.IDTipoProducto,
DM.IDLinea,
DM.Producto,
'Referencia'=DM.Ref,
'Fecha'= convert(date, M.FechaAnulacion),
'Fec'=convert (varchar(50),M.FechaAnulacion ,10),
'Movimiento'=M.Motivo + ' ' + 'ANULADA',
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'Tipo'=M.Operacion +' '+ 'ANULADA',
M.[Cod.],
M.NroComprobante,
M.IDDepositoEntrada,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
DM.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='True'
M.Anulado
From VMovimiento M 
join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Where DM.Entrada='True'And DM.Salida='False'
And M.Anulado = 'True'

Union all

--Transferencia Salida
Select
M.IDTransaccion,
DM.IDProducto,
DM.IDTipoProducto,
DM.IDLinea,
DM.Producto,
'Referencia'=DM.Ref,
'Fecha'=convert(date, M.Fecha),
'Fec'=convert (varchar(50),M.Fecha,10),
'Movimiento'=M.Motivo,
'Operación'=M.Operacion +' '+M.NroComprobante+': '+ M.CodigoSucursalOperacion +' '+ CONVERT ( varchar (30), M.Numero) ,
'Tipo'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoSalida ,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
DM.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='False'
M.Anulado
From VMovimiento M 
join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Where DM.Entrada='True'and DM.Salida='True'

Union All

--Transferencia Entrada Anulada
Select
M.IDTransaccion,
DM.IDProducto,
DM.IDTipoProducto,
DM.IDLinea,
DM.Producto,
'Referencia'=DM.Ref,
'Fecha'=convert(date, M.FechaAnulacion ),
'Fec'=convert (varchar(50),M.FechaAnulacion ,10),
'Movimiento'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'Operación'=M.Operacion+' '+ CodigoSucursalOperacion +':'+ convert (Varchar (30),M.Numero)+' '+ 'ANULADA',
'OP'=M.Operacion,
M.[Cod.],
M.NroComprobante,
M.IDDepositoEntrada ,
D.IDSucursal,
'Cliente/Proveedor'=(Case When M.Observacion = Null Then '---' Else (Case When M.Observacion='' Then '---' Else M.Observacion End) End)  ,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
DM.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='True'
M.Anulado
From VMovimiento M 
join VDetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Where DM.Entrada='True'And DM.Salida='True'
And M.Anulado = 'True'

union all

--Ajuste Inventario Negativo
Select 
V.IDTransaccion,
EDI.IDProducto,
0,
0,
EDI.Producto,
'Referencia'=EDI.Ref,
'Fecha'=convert(date, V.FechaAjuste),
'Fec'=convert (varchar(50),V.FechaAjuste,10),
'Movimiento'='AJUSTE NEGATIVO',
'Operación'='AJUSTE INVENTARIO NEGATIVO ' + V.Suc + ': '  + convert(varchar(50), V.Num),
'Tipo'='SALIDA',
'Cod.'='',
'NroComprobante'='',
EDI.IDDeposito,
EDI.IDSucursal,
'Cliente/Proveedor'= (Case When V.Observacion = Null Then '---' Else (Case When V.Observacion='' Then '---' Else V.Observacion End) End)  ,
'ComprobanteRelacionado'='---',
'Entrada'=0.00,
'Salida'=EDI.DiferenciaSistema * -1,
'Saldo'=0.00,
'Existencia'=0.00,
--'Hora'=CONVERT(time, V.FechaAjuste),
'Hora'=CONVERT(time, V.FechaTransaccion),
'FechaOperacion'=CONVERT(date, V.FechaAjuste),
'Hora Ope.'=Convert(varchar(10), V.FechaAjuste, 8),
'Fec. Ope.'=Convert(varchar(10), V.FechaAjuste, 5),
EDI.Costo,
'EstableceCosto'=0,
--'Anulado'='False'
V.Anulado
From VControlInventario V
 join VExistenciaDepositoInventario EDI on V.IDTransaccion=EDI.IDTransaccion 
 Join Transaccion T On V.IDTransaccion=T.ID
Where EDI.DiferenciaSistema < 0 And V.Procesado='True' And V.Anulado = 'False'

Union All

--Ajuste Inicial Negativo
Select 
A.IDTransaccion,
D.IDProducto,
0,
0,
D.Producto,
'Referencia'=D.Referencia,
'Fecha'=convert(date, A.Fecha),
'Fec'=convert (varchar(50),A.Fecha,10),
'Movimiento'='AJUSTE INICIAL NEGATIVO',
'Operación'='AJUSTE Nro: ' + CONVERT(varchar(50), A.Numero),
'Tipo'= 'SALIDA',
'Cod.'='',
'NroComprobante'=convert(varchar(50), A.Numero),
D.IDDeposito,
D.IDSucursal,
'Cliente/Proveedor'='AJUSTE INICIAL NEGATIVO',
'ComprobanteRelacionado'=A.Observacion,
'Entrada'=0.00,
'Salida'=D.Salida,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, T.Fecha),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), T.Fecha, 5),
0,
'EstableceCosto'=0,
'Anulado'='False'

From AjusteInicial A 
join VDetalleAjusteInicial D on A.IDTransaccion=D.IDTransaccion 
Join Transaccion T On A.IDTransaccion=T.ID
Where A.Procesado='True'
And D.Salida>0

union all

--Ticket de Bascula
Select 
C.IDTransaccion,
C.IDProducto,
C.IDTipoProducto,
C.IDLinea,
C.Producto,
'Referencia'=C.ReferenciaProducto,
'Fecha'=convert(date, C.FechaAnulacion),
'Fec'=convert (varchar(50),C.FechaAnulacion,5),
'Movimiento'='TICKET DE BASCULA',
'Operación'='TIBA: ' + convert (varchar(50), C.Numero),
'Tipo'= 'TICKET DE BASCULA',
'Cod.'=C.TipoComprobante,
'NroComprobante'=convert(varchar(50), C.NroComprobante),
C.IDDeposito,
C.IDSucursal,
'Cliente/Proveedor'=C.Proveedor,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=C.PesoBascula,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.FechaAnulacion),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.Fecha, 5),
'Costo'=C.PrecioUnitario,
'EstableceCosto'=0,
--'Anulado'='True'
C.Anulado
From VTicketBascula C 
Join Transaccion T On C.IDTransaccion=T.ID
And C.Anulado = 'True' And C.Procesado = 'True'

union all
--Ajuste manual de Costo
Select 
DPC.IDTransaccion,
DPC.IDProducto,
DPC.IDTipoProducto,
DPC.IDLinea,
P.Descripcion,
'Referencia'=P.Referencia,
'Fecha'=convert(date, C.Fecha),
'Fec'=convert (varchar(50),C.Fecha,5),
'Movimiento'='AJUSTE DE COSTO',
'Operación'='AJUSTECOSTO: ' + convert (varchar(50), C.Numero),
'Tipo'= 'AJUSTE DE COSTO',
'Cod.'='AJUSTECOSTO',
'NroComprobante'=convert(varchar(50), C.Numero),
0,
0,
'Cliente/Proveedor'=C.Observacion,
'ComprobanteRelacionado'='',
'Entrada'=0.00,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'='',
'FechaOperacion'='',--CONVERT(date, C.Fecha),
'Hora Ope.'='',--Convert(varchar(10), C.Fecha, 8),
'Fec. Ope.'='',--Convert(varchar(10), C.Fecha, 5),
'Costo'=DPC.Costo,
'EstableceCosto'=1,
'Anulado'='False'

From vDetalleProductoCosto DPC 
join ProductoCosto C on DPC.IDTransaccion = C.IDTransaccion
join Producto P ON DPC.IDProducto = P.ID
--Join Transaccion T On C.IDTransaccion=T.ID
And C.Anulado = 'False' 
























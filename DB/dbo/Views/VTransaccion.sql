﻿Create View VTransaccion

As

Select 

T.ID,
'IDTransaccion'=T.ID,
T.Fecha,

--Usuario
T.IDUsuario,
'Usuario'=U.Usuario,

--Sucursal
T.IDSucursal,
'Sucursal'=S.Descripcion,
'CodigoSucursal'=S.Codigo,

--Deposito
T.IDDeposito,
'Deposito'=D.Descripcion,

--Terminal
T.IDTerminal,
'Terminal'=TE.Descripcion,

--Operacion
T.IDOperacion,
'Operacion'=O.Descripcion,
'CodigoOperacion'=O.Codigo,
'FormName'=O.FormName,
'Tabla'=O.Tabla


From Transaccion T
Join Usuario U On T.IDUsuario=U.ID
Join Sucursal S On T.IDSucursal=S.ID
Join Deposito D On T.IDDeposito=D.ID
Join Terminal TE On T.IDTerminal=TE.ID
Join Operacion O On T.IDOperacion=O.ID
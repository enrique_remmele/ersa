﻿

CREATE View [dbo].[VDeposito]

As

Select
D.ID,
'Deposito'=D.Descripcion,
D.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc-Dep'=S.Codigo + ' - ' + D.Descripcion,
'Estado'=Case When (D.Estado) = 'True' Then 'OK' Else '-' End,
'Activo'=D.Estado,
'IDCiudad'=S.IDCiudad,
'Ciudad'=C.Descripcion,
'CodigoCiudad'=C.Codigo,
'SucDeposito'=S.Codigo+' - ' + D.Descripcion,

--Cuenta Contable
'CuentaContableMercaderia'=IsNull(CC.Codigo, ''),
'CuentaContableMercaderiaDescripcion'=IsNull(CC.Descripcion, ''),
'CuentaContableCombustible'=Isnull(D.CuentaContableCombustible,''),
'CuentaContableCombustibleDescripcion'=isnull((select descripcion from vcuentacontable where codigo = D.CuentaContableCombustible), ''),
D.IDTipoDeposito,
'TipoDeposito'= TD.Descripcion,
'IDSucursalDestinoTransito'=isnull(D.IDSucursalDestinoTransito,0),
'SucursalDestinoTransito'=(Case when (isnull(D.IDSucursalDestinoTransito,0)) = 0 then '' else (select descripcion from Sucursal where id =D.IDSucursalDestinoTransito) END),
'Compra'=(Case When (D.Compra)= 'True' Then 'True' Else 'False' End),
'Venta'=(Case When (D.Venta)= 'True' Then 'True' Else 'False' End),
'DescargaStock'=(Case When (D.DescargaStock)= 'True' Then 'True' Else 'False' End),
'ConsumoCombustible'=(Case When (D.ConsumoCombustible)= 'True' Then 'True' Else 'False' End),
'MovimientoMateriaPrima'=(Case When (D.MovimientoMateriaPrima)= 'True' Then 'True' Else 'False' End),
'DescargaCompra'=(Case When (D.DescargaCompra)= 'True' Then 'True' Else 'False' End)

From Deposito D
Join Sucursal S On D.IDSucursal=S.ID
Left Outer Join Ciudad C On S.IDCiudad=C.ID
Left Outer Join VCuentaContable CC On D.CuentaContableMercaderia=CC.Codigo
Left Outer Join TipoDeposito TD on TD.ID = D.IDTipoDeposito









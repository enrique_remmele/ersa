﻿


CREATE view [dbo].[VRetencionIVA]
AS


Select

--Punto de Expedicion
RI.IDPuntoExpedicion,
'PE'=PE.Descripcion,
PE.Ciudad,
PE.IDCiudad,
PE.IDSucursal,
PE.Sucursal,
PE.ReferenciaSucursal,
PE.ReferenciaPunto,
PE.Timbrado,

--Proveedor
RI.IDProveedor,
P.IDUsuarioAlta,
'Proveedor'=P.RazonSocial,
P.Telefono, 
P.Referencia ,
P.RUC ,
P.Direccion,
P.Estado, 



RI.IDTransaccion,
RI.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
RI.NroComprobante,
'Comprobante'=PE.ReferenciaSucursal + '-' + PE.ReferenciaPunto + '-' + convert(varchar(50),RI.NroComprobante),
'Comprobante2'='001' + '-' + PE.ReferenciaPunto + '-' + convert(varchar(50),RI.NroComprobante),
RI.IDTransaccionOrdenPago,
DR.IDTransaccionEgreso,
OP.Numero,
RI.OP,


'Suc'=S.Codigo,

RI.Fecha,
'Fec'=CONVERT(varchar(50), RI.Fecha, 6),
'Dia'='ASUNCION,' +' '+ dbo.FFormatoDosDigitos(DatePart(dd, RI.Fecha))+' '+' DE  ',
'Mes'=' '+' '+dbo.FMes(DatePart(MM, RI.Fecha))+' '+' DE',
'Año'=DatePart(yyyy, RI.Fecha),
RI.IDMoneda,
'Moneda'=M.Referencia,
'DescripcionMoneda'=M.Descripcion,
RI.Cotizacion,

RI.Observacion,


--Totales
RI.TotalIVA,
RI.TotalRenta,
RI.Total,
RI.Anulado,
'EstadoAnulado'=(Case When RI.Anulado='True'Then'Anulado'Else'---'End),
RI.Procesado,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'Usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=RI.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=RI.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=RI.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=RI.IDTransaccion),
'DocumentoCompra'=Isnull((select distinct TipoyComprobante from DetalleRetencion where idtransaccion = RI.Idtransaccion),'--'),
'FechaCompra'=Isnull((select distinct CONVERT(varchar(50), Fecha, 3) from DetalleRetencion where idtransaccion = RI.Idtransaccion),'--')



From RetencionIVA  RI
Join Transaccion T On RI.IDTransaccion=T.ID
Left Outer Join VOrdenPago OP On OP.IDTransaccion=RI.IDTransaccionOrdenPago
Left Outer Join DetalleRetencion DR on RI.IDTransaccion = DR.IDTransaccion
Left Outer Join VPuntoExpedicion PE On RI.IDPuntoExpedicion=PE.ID
Left Outer Join TipoComprobante TC On RI.IDTipoComprobante=TC.ID
Left Outer Join Proveedor P On RI.IDProveedor=P.ID
Left Outer Join Sucursal S On RI.IDSucursal=S.ID
Left Outer Join Moneda M On RI.IDMoneda=M.ID


﻿CREATE VIEW [dbo].[VLibroInventario]
AS
SELECT CC.IDCuentaContable,CC.Codigo, CC.Descripcion, padre,CC.Tipo,
SUBSTRING(CC.Codigo, 1, 1) AS Orden,
ISNULL(SUM(DA.Importe),0)AS importe

FROM dbo.vCuentaContable CC
LEFT JOIN dbo.DetalleAsiento DA ON Da.IDCuentaContable = CC.IDCuentaContable
LEFT JOIN dbo.Asiento A ON A.IDTransaccion = DA.IDTransaccion
GROUP BY CC.IDCuentaContable, CC.Descripcion, padre, CC.Codigo, tipo,SUBSTRING(CC.Codigo, 1, 1)






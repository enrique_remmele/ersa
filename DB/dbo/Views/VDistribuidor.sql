﻿



create View [dbo].[VDistribuidor]
As
Select
D.ID,
D.Nombres,
D.NroDocumento,
D.Telefono,
D.Celular,
D.Direccion,
D.Email,
D.Estado
From Distribuidor D

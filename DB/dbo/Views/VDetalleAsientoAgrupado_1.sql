﻿
CREATE View [dbo].[VDetalleAsientoAgrupado]
As
Select 
DA.IDTransaccion,
DA.IDCuentaContable,
'Codigo'=(IsNull(CC.Codigo, DA.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
'Cuenta'=IsNull(CC.Codigo, DA.CuentaContable) + ' - ' + IsNull(CC.Descripcion, '---'),
'Alias'=IsNull(CC.Alias, '---'),
'Credito'=Sum(DA.Credito),
'Debito'=sum(DA.Debito),
A.Fecha

From DetalleAsiento DA
Join Asiento A On DA.IDTransaccion=A.IDTransaccion
Left Outer Join vCuentaContable CC On DA.CuentaContable=CC.Codigo 
Where CC.PlanCuentaTitular='True'
group by CC.PlanCuentaTitular, DA.IDTransaccion, DA.IDCuentaContable, 
A.IDTRansaccion, CC.Codigo, DA.CuentaContable, CC.Descripcion, CC.DescripcionAbuelo,
CC.Alias,A.Fecha

















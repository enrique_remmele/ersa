﻿CREATE View [dbo].[VFormaPagoDocumentoRetencion]
As
Select 

FPD.IDTransaccion,
FPD.ID,
FPD.IDTransaccionVenta,

'ComprobanteVenta'=V.Comprobante,
V.FechaEmision,
'Total'=IsNull(FPD.Total, V.Total),
'TotalDiscriminado'=IsNull(FPD.TotalDiscriminado, V.TotalDiscriminado),
'TotalImpuesto'=IsNull(FPD.TotalImpuesto, V.TotalImpuesto),

FPD.Importe
 
From FormaPagoDocumentoRetencion FPD
Join Venta V On FPD.IDTransaccionVenta=V.IDTransaccion




﻿





CREATE View [dbo].[VCFDescuentoCheque]
As
Select
V.*,

'CuentaBancaria'=IsNull(CFV.CuentaBancaria, 'False'),
'GastoBancario'=IsNull(CFV.GastoBancario, 'False'),
'Cheque'=IsNull(CFV.Cheque, 'False'),
'SucursalCheque'=(Case When(IsNull(CFV.Cheque, 'False')) = 'True' Then S.Descripcion Else '---' End),
'IDSucursalCheque'=CFV.IDSucursalCheque,
'Tipo'=(Case When (CFV.CuentaBancaria) = 'True' Then 'CUENTA BANCARIA' Else (Case When (CFV.GastoBancario) = 'True' Then 'GASTO BANCARIO' Else Case When (CFV.Cheque) = 'True' Then 'CHEQUE' Else '---' End  End) End)

From VCF V
Join CFDescuentoCheque CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Left Outer Join Sucursal S on CFV.IDSucursalCheque=S.ID










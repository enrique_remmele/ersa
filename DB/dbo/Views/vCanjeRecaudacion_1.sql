﻿
CREATE view [dbo].[vCanjeRecaudacion]
as
select 
C.IDTransaccion
,C.IDSucursal
,'Sucursal'=(Select Codigo from Sucursal where ID = C.IDSucursal)
,C.Numero
,C.Fecha
,C.Total 
,C.Anulado
,C.FechaAnulado
,C.IDUsuarioAnulado
,'UsuarioAnulado' = (select Usuario from Usuario where ID = C.IDUsuarioAnulado)
,C.Observacion
,C.IDMoneda
,'Moneda'=(Case when C.IDMoneda = 1 then 'GS' else 'US' end)
,C.Cotizacion
,'IDUsuarioTransaccion'=T.IDUsuario
,'UsuarioTransaccion'=(select Usuario from Usuario where ID = T.IDUsuario)
,'FechaTransaccion'=T.Fecha
from CanjeRecaudacion C
Join Transaccion T on C.IDTransaccion = T.ID

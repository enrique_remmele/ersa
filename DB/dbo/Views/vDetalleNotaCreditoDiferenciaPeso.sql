﻿CREATE view [dbo].[vDetalleNotaCreditoDiferenciaPeso]
as 
select 
D.*,
P.Referencia,
P.Descripcion,
P.IDImpuesto,
'IDProductoDescuento' = (Case when P.IDImpuesto = 1 then 218 else
						(Case When P.IDImpuesto = 2 then 219 else 
						230 end)end)
from 
DetalleNotaCreditoDiferenciaPeso D
left outer join Producto P on D.IDProducto = P.ID



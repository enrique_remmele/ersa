﻿



CREATE View [dbo].[VOrdenPago]

As

Select 
OP.IDTransaccion,
OP.Numero,
'Num'=OP.Numero,
OP.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
OP.NroComprobante,
'Comprobante'=OP.NroComprobante,
'Fecha'=OP.Fecha,
'Fec'=CONVERT(varchar(50), OP.Fecha, 6),
OP.Observacion,
OP.PagoProveedor,
OP.AnticipoProveedor,
OP.EgresoRendir,
--Sucursal
OP.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
S.IDCiudad,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
OP.Total,
'TotalImporteMoneda'=IsNull(C.ImporteMoneda, 0) + IsNull(OP.ImporteMonedaEfectivo, 0),
'AplicarRetencion'=IsNull(OP.AplicarRetencion, 'False'),
'DiferenciaCambio'=IsNull(OP.DiferenciaCambio, 0),

--Moneda
'IDMoneda'=isnull(CB.IDMoneda,ISNULL(OP.IdMonedaDocumento,1)),
'Moneda'=isnull(M.Referencia,isNULL(M2.Referencia,1)),
--'Moneda'=M.Referencia,

--Moneda de Comprobantes
'IDMonedaComprobante'=OP.IDMoneda,
'MonedaComprobante'=M2.Referencia,

--Proveedor
'IDProveedor'=IsNull(OP.IDProveedor, 0),
'Proveedor'=ISNull(P.RazonSocial, ''),
'RUC'=IsNull(P.RUC, ''),
'Referencia'=P.Referencia,
P.IDTipoProveedor,

--Cheque
'Cheque'=Case When (C.IDTransaccion) Is Null Then 'False' Else 'True' End,
C.IDCuentaBancaria,
'Cuenta Bancaria'=CB.Descripcion,
C.CuentaBancaria,
'IDBanco'=B.ID,
C.Banco,
'CodigoBanco'=B.Referencia,
C.Mon,
C.NroCheque,
'FechaCheque'=convert(varchar(50),C.Fecha,6),
'Fec Chq'=C.Fec,
C.FechaPago,
'FecPago'=C.[Fec Pago],
'Cotizacion'=isnull(C.Cotizacion,ISNULL(OP.CotizacionDocumento,1)),
C.ImporteMoneda,
'ImporteMonedaInforme'=Case When OP.Anulado = 'True' Then 0 Else C.ImporteMoneda End,
C.Importe,
'Diferido'=isnull(C.Diferido,0),
C.FechaVencimiento,
C.[Fec. Venc.],
C.ALaOrden,
C.Conciliado,
'ChequeAnulado'=C.Anulado,
'ChequeEstado'=C.Estado,

--Efectivo
'CotizacionEfectivo'=IsNull(OP.CotizacionEfectivo, 0),
'IDMonedaEfectivo'=IsNull(OP.IDMonedaEfectivo, 0),
'ImporteMonedaEfectivo'=IsNull(OP.ImporteMonedaEfectivo, 0),

--Documento
'IdMonedaDocumento' = ISNULL(OP.IdMonedaDocumento,0),
'ImporteDocumento' = ISNULL(OP.ImporteDocumento,0),
'ImporteMonedaDocumento' = ISNULL(OP.ImporteMonedaDocumento,0),
'CotizacionDocumento' = ISNULL(OP.CotizacionDocumento,0),

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'EstadoOP'=OP.Cancelado,
OP.Anulado,
'Estado'=Case When OP.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=OP.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=OP.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=OP.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=OP.IDTransaccion),
--Impreso
OP.Impreso,
--Tipo Orden de Pago
'Tipo'=Case When (OP.PagoProveedor) = 'True' Then 'Pago Proveedor' When (OP.AnticipoProveedor)='True' Then 'Anticipo Proveedor' When(OP.EgresoRendir)='True' Then 'Egreso a Rendir'  End,
'Retencion'='',

--Para reportes agrupados
'Grupo1'='',
'Grupo2'='',
'Grupo3'=''

From OrdenPago OP
Join Transaccion T On OP.IDTransaccion=T.ID
Join TipoComprobante TC On OP.IDTipoComprobante=TC.ID
Join Sucursal S On OP.IDSucursal=S.ID
Left Outer Join Proveedor P On OP.IDProveedor=P.ID
Left Outer Join VCheque C On OP.IDTransaccion=C.IDTransaccion
Left Outer Join VCuentaBancaria CB on C.IDCuentaBancaria =CB.ID
Left Outer Join Banco B on CB.IDBanco=B.ID
Left Outer Join Moneda M On CB.IDMoneda=M.ID
Left Outer Join Moneda M2 On OP.IDMoneda=M2.ID
Left outer Join FormaPago FP on FP.IDTransaccion = OP.IDTransaccion













﻿
CREATE View [dbo].[VMotivoMovimiento]
As
Select 
M.ID,
M.Descripcion,
'Operacion'=V.Operacion,
'Tipo'=V.Descripcion,
M.Activo,
'Estado'=Case When (M.Activo) = 'True' Then 'OK' Else '---' End,
M.CodigoCuentaContable,
'CuentaContable'=isnull((Select Descripcion from VCuentaContable where M.CodigoCuentaContable = M.CodigoCuentaContable),'')
From MotivoMovimiento M
Join VTipoOperacion V On M.IDTipoMovimiento=V.ID


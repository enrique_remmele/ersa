﻿
CREATE View [dbo].[VFondoFijoCuentaContable]
As
Select
FFCC.*, FF.Grupo, CC.Codigo, CC.Descripcion
from FondoFijoCuentaContable FFCC
join vFondoFijo FF on FF.IDGrupo = FFCC.IDFondoFijo
join vCuentaContable CC on CC.ID = FFCC.IdCuentaContable

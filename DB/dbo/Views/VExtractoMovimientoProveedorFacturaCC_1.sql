﻿CREATE view [dbo].[VExtractoMovimientoProveedorFacturaCC]

As

--Egreso
Select
C.IDTransaccion, 
'Operacion'=CodigoOperacion + ' ' + isnull(convert(varchar(50),C.Numero),convert(varchar(50),'--')),
'Codigo'=C.IDProveedor,
'RazonSocial'=P.RazonSocial ,
'Referencia'=P.Referencia ,
'Fecha'=C.Fecha,
'Documento'=C.TipoComprobante + ' ' + convert(varchar(50),C.NroComprobante), 
'Detalle/Concepto'=C.Observacion,
'Debito'=0,
'Credito'=Da.Credito,
'Saldo'=0.00,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=C.Moneda,
'Movimiento'='EG',
'ComprobanteAsociado'=C.IDTransaccion,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = C.IDTransaccion),
'FechaEgreso' = C.Fecha,
'CodigoCC' = DA.Codigo
From 
VCompraGasto C 
Join VProveedor P on C.IDProveedor=P.ID 
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Credito>0

union all

--Nota Débito Proveedor
Select
ND.IDTransaccion,
'Operación'='Cr ' + isnull(convert(varchar(50),Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'RazonSocial'=Proveedor,
'Referencia'=Referencia,
'Fecha'=ND.Fecha,
'Documento'=[Cod.] + ' ' + convert(varchar(50),ND.NroComprobante),
'Detalle/Concepto'='',
'Debito'=0,
'Credito'=Da.Credito,
'Saldo'=0,
'IDMoneda'=1,
'DescripcionMoneda'=DescripcionMoneda,
'Movimiento'='ND',
'ComprobanteAsociado'=NDC.IDTransaccionEgreso,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = NDC.IDTransaccionEgreso),
'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =   NDC.IDTransaccionEgreso),
'CodigoCC' = DA.Codigo
From
VNotaDebitoProveedor ND
Join NotaDebitoProveedorCompra NDC On  ND.IDTransaccion = NDC.IDTransaccionNotaDebitoProveedor
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = NDC.IDTransaccionEgreso and DA.Descripcion like '%Proveed%'  and Da.Credito>0
where ND.Anulado = 'False'

union all

--Nota de Crédito Proveedor
Select
NC.IDTransaccion,
'Operación'='Db ' + isnull(convert(varchar(50),NC.Numero),convert(varchar(50),'--')),
'Codigo'=IDProveedor,
'RazonSocial'=Proveedor,
NC.Referencia,
'Fecha'=NC.Fecha,
'Documento'=[Cod.] + ' ' + convert(varchar(50),NC.NroComprobante),
'Detalle/Concepto'='',
'Debito'=Da.Debito,
'Credito'=0,
'Saldo'=0,
NC.IDMoneda,
'DescripcionMoneda'=M.Descripcion,
'Movimiento'='NC',
'ComprobanteAsociado'=NCC.IDTransaccionEgreso,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = NCC.IDTransaccionEgreso),
'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  NCC.IDTransaccionEgreso),
'CodigoCC' = DA.Codigo
From
VNotaCreditoProveedor NC
Join VMoneda M on NC.IDMoneda=M.ID 
Join NotaCreditoProveedorCompra NCC On NC.IDTransaccion = NCC.IDTransaccionNotaCreditoProveedor
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = NCC.IDTransaccionEgreso and DA.Descripcion like '%Proveed%' and Da.Debito>0
where NC.Anulado = 'False'

union all

--Ordenes de Pago
Select
OPE.IDTransaccionOrdenPago	,
'Operación'='OPE ' + isnull(convert(varchar(50),OP.Numero),convert(varchar(50),'--')),
'Codigo'=OPE.IDProveedor,
'RazonSocial'=OPE.Proveedor,
'Referencia'=C.Referencia,
'Fecha'=OP.Fecha,
'Documento'=CHQ.Banco + ' ' + CHQ.CuentaBancaria + ' ' + CHQ.Mon + ' - ' + CHQ.NroCheque,
'Detalle/Concepto'=OP.Observacion,
'Debito' = Da.Debito,
'Credito'=0,
'Saldo'=0,
'IDMoneda'=M.ID,
'DescripcionMoneda'=M.Referencia,
'Movimiento'='OPE',
'ComprobanteAsociado'=OPE.IDTransaccion,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = OPE.IDTransaccion),
'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  OPE.IDTransaccion)
,'CodigoCC'=DA.Codigo
From
VOrdenPagoEgreso OPE
join OrdenPago OP on OPE.IDTransaccionOrdenPago=OP.IDTransaccion
Join Moneda M On OP.IDMoneda=M.ID
--Join VCheque CHQ On OP.IDTransaccion=CHQ.IDTransaccion
left Join VCheque CHQ On OP.IDTransaccion=CHQ.IDTransaccion
Join VCompraGasto C On OPE.IDTransaccion=C.IDTransaccion
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = OPE.IDTransaccion and DA.Descripcion like '%Proveed%'  and Da.Debito>0
Where OP.Anulado='False'

Union All

--Compras Importadas con cobros parciales en sistema anterior
Select
C.IDTransaccion, 
'Operación'='Migracion',
'Codigo'=C.IDProveedor,
'RazonSocial'=C.Proveedor,
'Referencia'=C.Referencia ,
'Fecha'=Convert(date, '20151231'),
'Documento'=C.TipoComprobante + ' ' + convert(varchar(50),C.NroComprobante), 
'Detalle/Concepto'='Pagos parciales en sistema anterior',
'Debito'=Da.Credito,
'Credito'=0,
'Saldo'=0,
'IDMoneda'=C.IDMoneda,
'DescripcionMoneda'=C.Moneda,
'Movimiento'='PAG. MIGRACION',
'ComprobanteAsociado'=C.IDTransaccion,
'ComprobanteGasto' =(select Concat(Operacion,' ',Numero,' - Comprobante ', TipoComprobante,' ',NroComprobante) from VCompraGasto where idtransaccion = C.IDTransaccion),
'FechaEgreso' = (Select Fecha from VCompraGasto where IDTransaccion =  C.IDTransaccion)
,'CodigoCC'=DA.Codigo
From 
VGasto C
Join GastoImportado GI On C.IDTransaccion=GI.IDTransaccion 
Left Join VDetalleAsientoAgrupado DA on DA.IDTransaccion = C.IDTransaccion and DA.Descripcion like '%Proveed%' and Da.Credito>0
Where IsNull(GI.Pagado,0)>0




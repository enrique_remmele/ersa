﻿

CREATE View [dbo].[VListadoChequeDescontadoVencido]
As

Select
--Cheque
DDB.IDTransaccion,
'BancoEmision'=DDB.Banco,
'FechaEmisionCheque'=(Select Fecha from ChequeCliente where idtransaccion = DDB.IDTransaccion),--DDB.Fecha,
DDB.CuentaBancaria,
DDB.NroCheque, 
DDB.NroOperacionCheque,
DDB.Importe ,
DDB.Cliente,
DDB.IDCliente,
--Descuento
'NroOperacionDescuento'=DB.Numero,
DDB.IDTransaccionDescuentoCheque,
'IDBancoDescuento'=(Select IDBanco from cuentabancaria where id= DB.IDCuentaBancaria),
'BancoDescuento'=(Select Nombre from cuentabancaria where id= DB.IDCuentaBancaria),
'CuentaBancariaDescuento' = (Select cuentabancaria from cuentabancaria where id= DB.IDCuentaBancaria),
'NroOperacionVencimiento'=VCD.Numero,
'Vencimiento'=(Select FechaVencimiento from ChequeCliente where idtransaccion = DDB.IDTransaccion)--VCD.Fecha
From VDetalleDescuentoCheque  DDB
Join DescuentoCheque  DB on DDB.IDTransaccionDescuentoCheque  = DB.IDTransaccion 
Join VencimientoChequeDescontado VCD on DDB.IDTransaccion = VCD.IDTransaccionCheque
--select * from vdetalledescuentocheque



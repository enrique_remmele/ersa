﻿
CREATE view [dbo].[VDetalleVentaDescuentoDetalle]
AS
select 
DV.IDTransaccion,
'Sucursal'=S.Descripcion,
DV.Comprobante,
DV.Fecha,
DV.ReferenciaCliente,
DV.Cliente,
DV.ListaPrecio,
DV.Vendedor,
DV.Referencia,
DV.Producto,
DV.TipoProducto,
DV.Cantidad,
DV.PRecioUnitario,
'DescuentoUnitarioFACT'=DV.DescuentoUnitario,
'DescuentoUnitarioNCDiferenciaPrecio'= (select dbo.FDescuentoUnitarioDiferenciaPrecio(DV.IDProducto,DV.IDTransaccion)),
'DescuentoUnitarioNCAcuerdo' = (select dbo.FDescuentoUnitarioAcuerdo(DV.IDProducto,DV.IDTransaccion)),
'DescuentoUnitarioNCDiferenciaPeso' = (select dbo.FDescuentoUnitarioDiferenciaPeso(DV.IDProducto,DV.IDTransaccion)),
'PrecioUnitarioNeto' = (select dbo.FPrecioUnitarioNeto(DV.IDProducto,DV.IDTransaccion))

from 
VDetalleVenta DV
join Sucursal S on DV.IDSucursal = S.ID


﻿CREATE view [dbo].[vPorcentajeProductoPrecio] 
as
Select 
P.IDUsuario,
'NombreUsuario'=U.Nombre,
P.IDPresentacion,
'Presentacion'=Pr.Descripcion,
P.Porcentaje,
P.Importe,
P.Estado
From PorcentajeProductoPrecio P
join Usuario U on P.IDUsuario = U.ID
join Presentacion Pr on P.IDPresentacion = Pr.ID

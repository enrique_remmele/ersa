﻿
CREATE view [dbo].[vdepartamentoempresa]
as 
select 
de.ID,
de.Departamento,
de.IDUsuarioJefe,
'Jefe'=u.Nombre,
'CorreoJefe'=U.Email,
de.IDSucursal,
'Sucursal' = S.Descripcion
from departamentoempresa de
join Usuario u on de.IDUsuarioJefe = u.ID
join Sucursal S on de.idsucursal = S.ID

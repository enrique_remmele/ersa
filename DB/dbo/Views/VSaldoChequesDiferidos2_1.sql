﻿CREATE view [dbo].[VSaldoChequesDiferidos2]
as

--Ingreso
Select
'Fecha'=convert(date,FechaCobranza),
'FechaCobranza'=convert(date,FechaCobranza),
'Dia'=dbo.FDiaSemana(DATEPART(DW,FechaCobranza)),
'Ingreso'=Importe,
'Deposito'=0,
'DescuentoCheque'=0,
'Saldo'=0,
IDSucursal ,
Condicion
From VChequeCliente 
Where FechaCobranza Is Not NULL And Diferido ='True'

Union All

--Depositado
Select
'Fecha'=(Select Fecha From VDetalleDepositoBancario DDB Where DDB.IDTransaccionDocumento=V.IDTransaccion),
'FechaCobranza'=convert(date,FechaCobranza),
'Dia'=dbo.FDiaSemana(DATEPART(DW,V.FechaCobranza)),
'Ingreso'=0,
'Deposito'=V.Importe,
'DescuentoCheque'=0,
'Saldo'=0,
IDsucursal ,
Condicion
From VChequeCliente V
Where V.Diferido = 'True' And V.Depositado='True' 
And V.IDTransaccion  not in(Select IDTransaccionChequeCliente From DetalleDescuentoCheque)
And V.FechaCobranza Is Not NULL

Union All

--Descuento Cheque
Select
'Fecha'=(Select Fecha From VDetalleDescuentoCheque DDC Where DDC.IDTransaccionDepositoBancario=V.IDTransaccion),
'FechaCobranza'=convert(date,V.FechaCobranza),
'Dia'=dbo.FDiaSemana(DATEPART(DW,V.FechaCobranza)),
'Ingreso'=0,
'Deposito'=V.Importe,
'DescuentoCheque'=0,
'Saldo'=0,
V.IDsucursal ,
V.Condicion
From VChequeCliente V
Where  V.Diferido = 'True' And V.Depositado='False' 
And V.IDTransaccion  not in(Select IDTransaccionChequeCliente From DetalleDescuentoCheque)
And V.FechaCobranza Is Not NULL

Union All

--Importados
Select
'Fecha'=convert(date, V.Fecha),
'FechaCobranza'=convert(date, V.Fecha),
'Dia'=dbo.FDiaSemana(DATEPART(DW, V.Fecha)),
'Ingreso'=V.Importe,
'Deposito'=0,
'DescuentoCheque'=0,
'Saldo'=V.Saldo,
V.IDsucursal ,
V.Condicion
From VChequeCliente V
Join ChequeClienteImportar C On V.IDTransaccion=C.IDTransaccion
Where  V.Diferido = 'True' And V.Depositado='False' 
















﻿

CREATE view [dbo].[VPedidoNotaCreditoPedidoVenta] 
as
select 
PNCPV.* ,
'Numero' = PV.Numero, -- Campo Requerido para pedido de notacredito
'TotalVenta'=PV.Total,
'IDTransaccion'=PV.IDTransaccion,  -- Campo Requerido para pedido de notacredito
'IDTransaccionVenta'= Isnull(PV.IDTransaccionVenta,0),
'IDTransaccionNotaCredito'= PNC.IDTransaccionNotaCredito,
'PorcentajeDescuento'=(PNCPV.Total/PV.Total)*100.000
from PedidoNotaCreditoPedidoVenta PNCPV
left outer join vPedidoNotaCredito PNC on PNCPV.IDTransaccionPedidoNotaCredito = PNC.IDTransaccion
left outer join vPedido PV on PNCPV.IDTransaccionPedidoVenta = PV.IDTransaccion
where PNC.anulado = 0


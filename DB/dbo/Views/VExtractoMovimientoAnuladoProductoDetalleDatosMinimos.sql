﻿CREATE view [dbo].[VExtractoMovimientoAnuladoProductoDetalleDatosMinimos]
as
--Entrada

--Movimiento Salida Anulada
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, DA.Fecha ),
'Fec'=convert (varchar(50),DA.Fecha ,10),
'IDDeposito'=M.IDDepositoSalida,
D.IDSucursal,
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=Da.Fecha

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoSalida=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=0 and Cast(Ti.Salida as bit)=1
And M.Anulado = 1

Union All

--Transferencia Salida Anulada
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, DA.Fecha),
'Fec'=convert (varchar(50),DA.Fecha,10),
M.IDDepositoSalida ,
D.IDSucursal,
'Entrada'=DM.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=Da.Fecha

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On DM.IDDepositoSalida=D.ID 
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=1
And M.Anulado = 1

Union all

--Ventas Anuladas
Select 
V.IDTransaccion,
DV.IDProducto,
'FechaEmision'=convert(date, V.FechaEmision),
'Fecha'=Case When DA.Fecha > V.FechaEmision then Convert(date, da.Fecha) else Convert(date, V.FechaEmision) end,
'Fec'=Case When DA.Fecha > V.FechaEmision then convert (varchar(50),da.Fecha,10) else Convert(varchar(50), V.FechaEmision,10) end,
V.IDDeposito,
V.IDSucursal,
'Entrada'=DV.Cantidad,
'Salida'=0.00,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, V.FechaEmision),
'FechaOperacion'=CONVERT(date, V.FechaEmision),
'Hora Ope.'=Convert(varchar(10), V.FechaEmision, 8),
'Fec. Ope.'=Convert(varchar(10), V.FechaEmision, 5),
'Anulado'= V.Anulado,
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion)

From Venta V
join DetalleVenta DV on V.IDTransaccion=DV.IDTransaccion 
Join Transaccion T On V.IDTransaccion=T.ID
Join DocumentoAnulado DA On V.IDTransaccion=DA.IDTransaccion
Where V.Anulado=1
And V.Procesado=1

Union All

--SALIDAS

--Nota de Crédito Cliente ANULADO
Select
NC.IDTransaccion,
DNC.IDProducto,
'FechaEmision'=convert(date, NC.Fecha),
'Fecha'=convert(date, DA.Fecha),
'Fec'=convert (varchar(50),DA.Fecha,10),
NC.IDDeposito,
NC.IDSucursal,
'Entrada'=0.00,
'Salida'=DNC.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= NC.Anulado,
'FechaAnulacion'=NC.FechaAnulado

From notaCredito NC
join DetalleNotaCredito DNC on NC.IDTransaccion=DNC.IDTransaccion
Join Transaccion T On NC.IDTransaccion=T.ID
Join DocumentoAnulado DA On NC.IDTransaccion=DA.IDTransaccion
WHERE NC.Anulado = 1
And NC.Procesado=1

union all

--Movimiento Entrada Anulado
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'= convert(date, DA.Fecha),
'Fec'=convert (varchar(50), DA.Fecha,10),
M.IDDepositoEntrada,
D.IDSucursal,
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=DA.Fecha

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=0
And M.Anulado = 1

Union all

--Transferencia Entrada Anulada
Select
M.IDTransaccion,
DM.IDProducto,
'FechaEmision'=convert(date, M.Fecha),
'Fecha'=convert(date, DA.Fecha),
'Fec'=convert (varchar(50),DA.Fecha,10),
M.IDDepositoEntrada ,
D.IDSucursal,
'Entrada'=0.00,
'Salida'=DM.Cantidad,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, DA.Fecha),
'FechaOperacion'=CONVERT(date, DA.Fecha),
'Hora Ope.'=Convert(varchar(10), DA.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), DA.Fecha, 5),
'Anulado'= M.Anulado,
'FechaAnulacion'=DA.Fecha

From Movimiento M 
join DetalleMovimiento DM on DM.IDTransaccion=M.IDTransaccion 
Join Deposito D On DM.IDDepositoEntrada=D.ID
Join Transaccion T On M.IDTransaccion=T.ID
Join DocumentoAnulado DA On M.IDTransaccion=DA.IDTransaccion
Join TipoOperacion Ti On M.IDTipoOperacion=Ti.ID
Where Cast(Ti.Entrada as bit)=1 and Cast(Ti.Salida as bit)=1
And M.Anulado = 1

union all

--Ticket de Bascula anulado
Select 
C.IDTransaccion,
C.IDProducto,
'FechaEmision'=convert(date, C.Fecha),
'Fecha'=convert(date, C.FechaAnulacion),
'Fec'=convert (varchar(50),C.FechaAnulacion,5),
C.IDDeposito,
C.IDSucursal,
'Entrada'=0.00,
'Salida'=C.PesoBascula,
'Saldo'=0.00,
'Existencia'=0.00,
'Hora'=CONVERT(time, T.Fecha),
'FechaOperacion'=CONVERT(date, C.FechaAnulacion),
'Hora Ope.'=Convert(varchar(10), T.Fecha, 8),
'Fec. Ope.'=Convert(varchar(10), C.Fecha, 5),
'Anulado'= C.Anulado,
'FechaAnulacion'=C.FechaAnulacion

From TicketBascula C 
Join Transaccion T On C.IDTransaccion=T.ID
And C.Anulado = 1 And C.Procesado = 1


﻿CREATE View [dbo].[VRuta] As 

Select R.*, 
S.Descripcion as Sucursal, 
A.Descripcion as Area,
ZV.Descripcion as ZonaVenta
From Ruta R
join Sucursal S on S.Id = R.IDSucursal
join Area A on A.Id = R.IDArea
join ZonaVenta ZV on ZV.ID = R.IDZonaVenta


﻿
CREATE View [dbo].[VKilosTransportadosResumido]
As

Select Fecha,
IDCamion, 
Camion,
IDChofer, 
Chofer, 
DLOTE.IDTipoProducto,
'TipoProducto'= (select Descripcion from TipoProducto where ID = IDTipoProducto),
IDSucursal, 
Sucursal,
sum(cantidad) as Cantidad,
Sum(Peso) as Peso
from VLoteDistribucion LOTE
Join vDetalleLoteDistribucion DLOTE on DLOTE.IDTransaccion = LOTE.IDTransaccion
Group by Fecha, IDCamion, Camion, IDChofer, Chofer, DLOTE.IDTipoProducto, IDSucursal, Sucursal





﻿CREATE View [dbo].[VDetalleDepositoBancarioCheque]
As

Select

C.IDTransaccion,
'ID'=0,

'Sel'='True',
C.CodigoTipo, 
'Comprobante'=C.NroCheque ,
C.Banco,
C.BancoLocal,

--Si la moneda no es GS (ID <> a 1) entonces mostrar importe moneda
'Importe'= Case When(C.IDMoneda) <> 1 Then C.ImporteMoneda Else C.Importe End ,

C.Cliente,
'Referencia'=C.CodigoCliente,
C.CuentaBancaria,
C.Estado,
C.Numero,
C.IDSucursal,
C.Ciudad,
C.NroCheque,

DDB.IDTransaccionCheque,
'IDTransaccionDepositoBancario' = DDB.IDTransaccionDepositoBancario ,
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
'Depositado'=C.Depositado,

'IDCuentaBancariaDeposito'=DB.IDCuentaBancaria,
DB.Fecha,
'NroOperacion'=DB.Numero,
DB.NroComprobante,
DB.Sucursal,

'NroCobranza'=(Select Top 1  CC.Numero From ChequeCliente  CHQ
Join FormaPago FP on CHQ.IDTransaccion=FP.IDTransaccionCheque
Join CobranzaCredito CC On CC.IDTransaccion=FP.IDTransaccion
Where CHQ.IDTransaccion=C.IDTransaccion),
C.Diferido,
M.Decimales,
C.Cotizacion


From DetalleDepositoBancario DDB
Join Transaccion T On DDB.IDTransaccionDepositoBancario =T.ID
Join VDepositoBancario DB on DDB.IDTransaccionDepositoBancario = DB.IDTransaccion 
Join VChequeCliente  C on DDB.IDTransaccionCheque  = C .IDTransaccion
Join Moneda M On C.IDMoneda=M.ID 


























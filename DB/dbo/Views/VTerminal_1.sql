﻿


CREATE View [dbo].[VTerminal]
As
Select 
T.ID,
T.Descripcion,
T.Impresora,
T.CodigoActivacion,

--Sucursal
T.IDSucursal,
'Sucursal'=S.Descripcion,
'ReferenciaSucursal'=S.Referencia, 
'CodigoSucursal'=S.Codigo, 

--Deposito
T.IDDeposito,
'Deposito'=D.Descripcion,

T.Activado

From Terminal T
Join Sucursal S On T.IDSucursal=S.ID
Join Deposito D On T.IDDeposito=D.ID



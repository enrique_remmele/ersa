﻿CREATE View [dbo].[VCobranzaCredito]
As

Select 

--Cabecera
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
CC.Numero,
CC.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
CC.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
CC.NroComprobante,
'Comprobante'=CC.NroComprobante,
CC.FechaEmision,
'Fec'=CONVERT(varchar(50), CC.FechaEmision, 6),
CC.IDCobrador,
'Cobrador'=CO.Nombres,
CC.Observacion,
CC.Anulado,
--Totales
CC.Total,
'TotalMonedaCobranza' = CC.Total,
CC.NroPlanilla,

--Cliente
'IDCliente'=ISNULL(CC.IDCliente,0),
'Cliente'=ISNULL(C.RazonSocial,''),
'IDTipoCliente'=ISNULL(TCLI.ID,''),
'TipoCliente'=ISNULL(TCLI.Descripcion,''),
'RUC'=ISNULL(C.RUC,''),
'Referencia'=ISNULL(C.Referencia,''),
'IDVendedor'=isnull(C.IDVendedor,0),

--Transaccion
CC.IDTransaccion,
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),


--Anulacion
'Estado'=Case When CC.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CC.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=CC.IDTransaccion),
CC.IDMoneda,
CC.Cotizacion,
'Moneda'= (select Referencia from Moneda where ID = CC.IDMoneda),
CC.DiferenciaCambio,
'AnticipoCliente' = (Case When AnticipoCliente  = 1 then 'True' else 'False' end),
CC.IDTipoAnticipo,
'TipoAnticipo' = ISnull((Select Descripcion from TipoAnticipo where ID = CC.IDTipoAnticipo),''),
'IDRecibo'= IsNull(IDRecibo,0),
CC.ReciboInterno,
'CANCELADO'=(Case when CC.AnticipoCliente = 0 then 'True' else 
	(Case when CC.Total = (Select Sum(Total) from AnticipoAplicacion where Anulado = 0 and Procesado = 1 and IDTransaccionCobranza = CC.IDTransaccion) then 'True' else 'False' end)end)
From CobranzaCredito CC
Join Transaccion T On CC.IDTransaccion=T.ID
Left Outer Join TipoComprobante TC On CC.IDTipoComprobante=TC.ID
Left Outer Join Cliente C On CC.IDCliente=C.ID
Left Outer Join TipoCliente TCLI On TCLI.ID=C.IDTipoCliente
Left Outer Join Sucursal S On CC.IDSucursal=S.ID
Left Outer Join Cobrador CO On CC.IDCobrador=CO.ID

Where CC.Procesado=1











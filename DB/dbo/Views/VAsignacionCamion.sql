﻿
CREATE View [dbo].[VAsignacionCamion]
As

--CARLOS 03-03-2014

Select 
M.IDTransaccion,
M.Numero,
'Num'=M.Numero,
M.Fecha,
'Fec'=CONVERT(varchar(50), M.Fecha, 6),
--'Operacion'=T.Descripcion,
M.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
M.NroComprobante,
'Comprobante'=M.NroComprobante,
M.Observacion,
M.Anulado,
'Estado'=Case When M.Anulado='True' Then 'Anulado' Else 'OK' End,

--Transaccion
'FechaTransaccion'=TR.Fecha,
TR.IDDeposito,
TR.IDSucursal,
TR.IDTerminal,
TR.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=M.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=M.IDTransaccion),

'IDCamion'=isnull(M.IDCamion,0),
'Camion'=Isnull((select Descripcion from Camion where ID = Isnull(M.IDCamion,0)),''),
'IDChofer'=isnull(M.IDChofer,0),
'Chofer'=Isnull((select Nombres from Chofer where ID = Isnull(M.IDChofer,0)),''),
'Sucursal' = (select codigo from sucursal where id = TR.IDSucursal),
'PedidoCliente'= (Case when PedidoCliente = 'True' then 'True' else 'False' end),
M.IDSucursalAbastecer,
'SucursalAbastecer' = (Select Descripcion from Sucursal where id = M.IDSucursalAbastecer)
From AsignacionCamion M
Join Transaccion TR On M.IDTransaccion=TR.ID
--Join TipoOperacion T On M.IDTipoOperacion=T.ID
Join TipoComprobante TC On M.IDTipoComprobante=TC.ID























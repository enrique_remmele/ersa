﻿
CREATE View [dbo].[VCompra]
As

Select

C.IDTransaccion,
C.Numero,
'Num'=C.Numero,
C.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
C.NroComprobante,
'Comprobante'=C.NroComprobante,
'Timbrado'= C.NroTimbrado,

--Proveedor
C.IDProveedor,
'Proveedor'=P.RazonSocial,
P.Telefono,
P.RUC,
P.Referencia, 
P.IDTipoProveedor,
'TipoProveedor'=P.TipoProveedor,
 
C.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
S.IDCiudad,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
C.IDDeposito,
'Deposito'=D.Descripcion,
C.Fecha,
'Fec'=CONVERT(varchar(50), C.Fecha, 6),
C.Credito,
'CreditoContado'=(Case When (C.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
C.FechaVencimiento,
'Fec. Venc.'= (Case When C.Credito='True' Then (CONVERT(varchar(50), C.FechaVencimiento, 6)) Else '---' End),
C.FechaEntrada,
'Fec. Ent.'= CONVERT(varchar(50), C.FechaEntrada, 6),
c.FechaVencimientoTimbrado,
'Fec. Vec. Timbrado'= CONVERT (varchar (50), C.FechaVencimientoTimbrado, 6),
C.IDMoneda,
'Moneda'=M.Referencia,
'DescripcionMoneda'=M.Descripcion,
C.Cotizacion,
C.Observacion,
'Directo'=IsNull(C.Directo, 'False'),

--Totales
C.Total,
C.TotalImpuesto,
C.TotalDiscriminado,
C.RetencionIVA,
C.RetencionRenta,

--Pago
'Pagar'=IsNull(C.Pagar, 'False'),
'ImporteAPagar'=(Case When IsNull(C.Pagar, 'False') = 'True' Then C.ImporteAPagar Else 0 End),
C.IDCuentaBancaria,
'CuentaBancaria'=CB.Descripcion,
C.ObservacionPago,

--Credito
C.Saldo,
C.Cancelado,


--Departamento
C.IDDepartamentoEmpresa,
'DepartamentoEmpresa'=DE.Departamento,


--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario)

From Compra C
Join Transaccion T On C.IDTransaccion=T.ID
Left Outer Join TipoComprobante TC On C.IDTipoComprobante=TC.ID
Left Outer Join vProveedor P On C.IDProveedor=P.ID
Left Outer Join Sucursal S On C.IDSucursal=S.ID
Left Outer Join Deposito D On C.IDDeposito=D.ID
Left Outer Join Moneda M On C.IDMoneda=M.ID
Left Outer Join VCuentaBancaria CB On C.IDCuentaBancaria=CB.ID
left outer join DepartamentoEmpresa DE on C.IDDepartamentoEmpresa =DE.ID


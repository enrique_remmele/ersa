﻿

CREATE view [dbo].[VVentaTotalAnualCliente]
as

--Total Enero
Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=Total,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Enero' 

--Total Febrero

union all
Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=Total,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Febrero'

--Total Marzo

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=Total,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Marzo'

--Total Abril

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=Total,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Abril'

--Total Mayo

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=Total,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Mayo'

--Total Junio

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=Total,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Junio'

--Total Julio

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=Total,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Julio'

--Total Agosto

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=Total,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Agosto'

--Total Setiembre

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=Total,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Setiembre'

--Total Octubre

Union all

Select
IDCliente, 
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=Total,
'Noviembre'=0,
'Diciembre'=0
from VVenta Where MesP='Octubre'

--Total Noviembre

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=Total,
'Diciembre'=0
from VVenta Where MesP='Noviembre'

--Total Diciembre

Union all

Select 
IDCliente,
Cliente,
Referencia,
Fecha,
IDMoneda,
IDTipoComprobante,
IDVendedor,
IDCiudad,
IDSucursal,
IDDeposito,
IDZonaVenta,
IDListaPrecio,
Credito,
IDCiudadCliente,
'Enero'=0,
'Febrero'=0,
'Marzo'=0,
'Abril'=0,
'Mayo'=0,
'Junio'=0,
'Julio'=0,
'Agosto'=0,
'Setiembre'=0,
'Octubre'=0,
'Noviembre'=0,
'Diciembre'=Total
from VVenta Where MesP='Diciembre'





﻿


CREATE View [dbo].[VDescuentoCheque]

As

Select 

---Cabecera
DB.IDTransaccion,
DB.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=S.CodigoCiudad,
DB.Numero,
'Num'=DB.Numero,

DB.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
DB.NroComprobante,
'Comprobante'=DB.NroComprobante,
DB.CuentaBancaria,

DB.Fecha,
'Fec'=CONVERT(varchar(50), DB.Fecha, 4),

--Cuenta Bancaria
'IDCuentaBancaria'=CB.ID,
'Cuenta' = CB.CuentaBancaria,
'Banco'=CB.Banco,
'Moneda'=CB.Mon,

DB.Cotizacion,
DB.Observacion,
DB.TotalDescontado,
DB.TotalAcreditado,
DB.GastoBancario,
DB.Conciliado,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario) 

 
From DescuentoCheque  DB
Join Transaccion T On DB.IDTransaccion=T.ID
Join VSucursal S On DB.IDSucursal=S.ID
Left Outer JOin VCuentaBancaria CB On DB.IDCuentaBancaria=CB.ID 
Join TipoComprobante TC On DB.IDTipoComprobante=TC.ID








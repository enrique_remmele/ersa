﻿

CREATE View [dbo].[VChequeCliente]
As

Select 
CHQ.IDTransaccion,
CHQ.Numero,
CHQ.IDSucursal,
'Ciudad'=S.CodigoCiudad,

--Sucursal
'Sucursal'=S.Descripcion,
'CodigoSucursal'=S.Codigo,

--Cliente
CHQ.IDCliente,
'Cliente'=C.RazonSocial,
'CodigoCliente'=C.Referencia,
C.RUC,
'Emitido Por'= Case When CHQ.Librador is Null Then  C.RazonSocial    Else chq.Librador End ,

--Banco
CHQ.IDBanco,
'Banco'=B.Descripcion,
'IDSucursalBanco'=B.IDSucursal,
'BancoLocal'=(Case When CHQ.IDSucursal=B.IDSucursal Then 'True' Else 'False'End),
'CodigoBanco'=B.Referencia,

--CuentaBancaria
CHQ.IDCuentaBancaria,
CCB.CuentaBancaria,

CHQ.Fecha,
'Fec'=CONVERT(varchar(50), CHQ.Fecha, 5),
CHQ.NroCheque,
CHQ.IDMoneda,
'Moneda'=M.Referencia,
CHQ.Cotizacion,
'ImporteMoneda'= (Case When S.ID=CHQ.IDSucursal Then CHQ.ImporteMoneda Else 0 End),
'Importe'=(Case When S.ID=CHQ.IDSucursal Then CHQ.Importe Else 0 End),
'ImporteOtroCheque'=(Case When S.ID<> CHQ.IDSucursal Then CHQ.Importe Else 0 End),
'Total'=CHQ.Importe,
CHQ.Diferido,
'Tipo'=Case When(CHQ.Diferido) = 'True' Then 'DIFERIDO' Else 'AL DIA' End,
'CodigoTipo'=Case When(CHQ.Diferido) = 'True' Then 'DIF' Else 'CHQ' End,
CHQ.FechaVencimiento,
'Vencimiento'=Case When(CHQ.Diferido) = 'True' Then Convert(varchar(50), CHQ.FechaVencimiento, 5) Else '---' End,
CHQ.ChequeTercero,
CHQ.Librador,
CHQ.Saldo,
CHQ.Cancelado,
CHQ.Cartera,
CHQ.Depositado,
CHQ.Rechazado,
CHQ.Conciliado,
CHQ.SaldoACuenta,
'Estado'=(Case When(CHQ.Cartera) = 'True' Then 'CARTERA' Else 
         (Case When(CHQ.Depositado) = 'True' and (CHQ.Descontado) = 'False' Then 'DEPOSITADO' Else
         (Case When(CHQ.Rechazado) = 'True' Then (Case When (CHQ.SAldoACuenta)=0 Then 'RECHAZADO CANCELADO' ELSE 'RECHAZADO' End ) Else 
         (Case When(CHQ.Descontado) = 'True' Then 'DESCONTADO' Else
		 --(Case When(CHQ.Rechazado) = 'True' Then 'RECHAZADO' Else
         (Case When(CHQ.Conciliado) = 'True' Then 'CONCILIADO' 
         Else '---' End) End) End) End)End),

--Rechazo
'Motivo'=(Case When (CHQ.IDMotivoRechazo) IS Not Null Then (Select MOT.Descripcion From MotivoRechazoCheque MOT Where MOT.ID=CHQ.IDMotivoRechazo) Else '---' End),

'IDOperacion'=O.ID,
'Operacion'=O.Descripcion,
'OP'=O.Codigo,

CHQ.Titular,
'FecCobranza'= CHQ.FechaCobranza,
'FechaCobranza'=convert(varchar(50),CHQ.FechaCobranza,5),

'Condicion'=IsNull((Select Top 1 Credito From ChequeCliente CC 

Join FormaPago FP on CC.IDTransaccion=FP.IDTransaccionCheque 
Join VentaCobranza VC on VC.IDTransaccionCobranza=FP.IDTransaccion 
Join Venta V on V.IDTransaccion=VC.IDTransaccionVenta
Where CC.IDTransaccion=CHQ.IDTransaccion), 'False'),
'FecRechazo'=isnull((select max(Fecha) from ChequeClienteRechazado where IDTransaccionCheque = CHQ.IDTransaccion and Anulado = 0),''),
'FechaRechazo'=Isnull((select cast(max(convert(varchar(50),Fecha,5)) as varchar(10)) from ChequeClienteRechazado where IDTransaccionCheque = CHQ.IDTransaccion and Anulado = 0),''),
'FechaCargaRechazo'=Isnull((select cast(max(convert(varchar(50),T.Fecha,5)) as varchar(10)) from ChequeClienteRechazado CCR Join Transaccion T on T.ID = CCR.IDTransaccion  where CCR.IDTransaccionCheque = CHQ.IDTransaccion and Anulado = 0),'')
, CHQ.Descontado,
'Cliente/Titular' = Case When CHQ.Titular = C.RazonSocial Then  C.RazonSocial  Else 
					(Case When CHQ.Titular is null then C.RazonSocial ELSE 
					(Case when CHQ.Titular = '' then C.RazonSocial else 
					C.RazonSocial +' / '+ CHQ.Titular end) end) end
--Cobrado, se saco porque en la carga de cobranza duplicaba
From ChequeCliente CHQ
Join VSucursal S On CHQ.IDSucursal=S.ID
Join Cliente C On CHQ.IDCliente=C.ID
Join Banco B On CHQ.IDBanco=B.ID
Left Outer Join ClienteCuentaBancaria CCB On CHQ.IDCuentaBancaria=CCB.ID
Join Moneda M On CHQ.IDMoneda=M.ID
left Join Transaccion T On CHQ.IDTransaccion=T.ID
left Join Operacion O On T.IDOperacion=O.ID

















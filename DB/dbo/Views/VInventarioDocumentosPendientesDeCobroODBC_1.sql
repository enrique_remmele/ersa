﻿
CREATE view [dbo].[VInventarioDocumentosPendientesDeCobroODBC]
 as 

--Sergio 06/03/2014, cambiar ULTIMO PAGO

Select v.Idtransaccion,
'Fecha'=CONVERT(date, Fecha),
'Documento' = v.Comprobante, v.IDCliente, v.ReferenciaCliente, v.IDTipoComprobante,
 v.IDVendedor, v.IDCobrador,   vlt.IDChofer,  vlt.IDCiudad, v.IDSucursal, v.sucursal,
 v.IDDeposito, v.IDZonaVenta, v.IDTipoCliente, V.IDEstado, v.IDMoneda,  v.Condicion,
 'Ultimo Pago' = (SELECT    Convert(date,MAX(C.FechaCobranza))
                  FROM          VVentaDetalleCobranza C
                  WHERE      C.IDTransaccion = V.IDTransaccion),
	 v.total,
  'Saldo' = v.Saldo, 
  'Dias Vencidos' = DATEDIFF(D, V.FechaEmision, GETDATE()) ,
  SUM((CASE
WHEN (DATEDIFF(D, V.FechaEmision, GETDATE()) > 0 and DATEDIFF(D, V.FechaEmision, GETDATE()) <=30) THEN
  v.Saldo
ELSE
  0
END)) AS TOTA30,SUM((CASE
WHEN  (DATEDIFF(D, V.FechaEmision, GETDATE()) > 30 and DATEDIFF(D, V.FechaEmision, GETDATE()) <=60) THEN
  v.Saldo
ELSE
  0
END)) AS TOTA60,
SUM((CASE
WHEN  (DATEDIFF(D, V.FechaEmision, GETDATE()) > 60 and DATEDIFF(D, V.FechaEmision, GETDATE()) <=90) THEN
  v.Saldo
ELSE  
  0
END)) AS TOTA90,
'Ciudad'=ci.Descripcion,
'Cobrador'=c.Nombres,
'Vendedor'=x.Nombres,
'Cliente'=cli.RazonSocial,
cli.RUC,
cli.Telefono,
v.Anulado, 
v.Cancelado
,'FechaVencimiento'= convert(varchar(50),isnull(v.FechaVencimiento,'')),
'LineaCredito' = cli.LimiteCredito,
'Plazo' = cli.PlazoCredito,
'Año' = year(Fecha)

from VVenta v
join Cliente cli on cli.ID = v.IDCliente
join Vendedor x on x.ID = v.IDVendedor
left join Cobrador c on c.ID	= v.IDCobrador
left join VVentaLoteDistribucion vlt on vlt.IDTransaccionVenta = v.IDTransaccion
left join Ciudad ci on ci.ID = v.IDCiudad
where  V.Cancelado = 'False'
group by v.Idtransaccion, v.Fecha, v.Comprobante, v.IDCliente, v.ReferenciaCliente,
v.IDTipoCliente, v.IDTipoComprobante, v.IDVendedor, v.IDCobrador, vlt.IDChofer, vlt.IDCiudad,
v.IDSucursal, v.Sucursal, v.IDDeposito, v.IDZonaVenta, v.IDEstado, v.IDMoneda, v.Condicion,v.Total, v.Saldo,
v.FechaEmision, ci.Descripcion, c.Nombres, x.Nombres,
cli.RazonSocial,
cli.RUC,
cli.Telefono, v.Anulado, v.Cancelado, v.FechaVencimiento, cli.LimiteCredito, cli.PlazoCredito

union all

Select nc.Idtransaccion,
'Fecha'=CONVERT(date, Fecha),
'Documento' = nc.Comprobante, nc.IDCliente, 'ReferenciaCliente'=c.Referencia,  nc.IDTipoComprobante,
 'IdVendedor' = Null, 'IdCobrador' =Null,'IdChofer' = Null,  nc.IDCiudad, nc.IDSucursal, nc.Sucursal,
 nc.IDDeposito, nc.IDZonaVenta, nc.IDTipoCliente, nc.IDEstado, nc.IDMoneda,  nc.Condicion,
 'Ultimo Pago' = (SELECT     Convert(date,MAX(C.FechaCobranza))
                  FROM          VVentaDetalleCobranza C
                  WHERE      C.IDTransaccion = nc.IDTransaccion),
 nc.Total,
  'Saldo' = nc.Saldo, 
  'Dias Vencidos' = DATEDIFF(D, nc.Fecha, GETDATE()),
  SUM((CASE
WHEN (DATEDIFF(D, nc.Fecha, GETDATE()) > 0 and DATEDIFF(D, nc.Fecha, GETDATE()) <=30) THEN
  nc.Saldo
ELSE
  0
END)) AS TOTA30,SUM((CASE
WHEN  (DATEDIFF(D, nc.Fecha, GETDATE()) > 30 and DATEDIFF(D, nc.Fecha, GETDATE()) <=60) THEN
 nc.Saldo
ELSE
  0
END)) AS TOTA60,
SUM((CASE
WHEN  (DATEDIFF(D, nc.Fecha, GETDATE()) > 60 and DATEDIFF(D, nc.Fecha, GETDATE()) <=90) THEN
  nc.Saldo
ELSE  
  0
END)) AS TOTA90,
'Ciudad'= ci.Descripcion,
'Cobrador'= '',
'Vendedor'='',
'Cliente'=c.RazonSocial,
c.RUC,
c.Telefono, nc.Anulado,
'Cancelado'= (Case When(NC.Aplicar) = 'True' Then (Case When(NC.Saldo) = 0 Then 'True' Else 'False' End) Else 'True' End)
,'FechaVencimiento'= convert(varchar(50),isnull(nc.Fec,'')),
'LineaCredito' = c.LimiteCredito,
'Plazo' = c.PlazoCredito,
'Año' = year(Fecha)

from vNotaCredito nc
join Cliente c on c.ID = nc.IDCliente
join Ciudad ci on ci.ID = nc.IDCiudad
WHERE     (CASE WHEN (NC.Aplicar) = 'True' THEN (CASE WHEN (NC.Saldo) = 0 THEN 'True' ELSE 'False' END) ELSE 'False' END) = 'True'
group by nc.IDTransaccion, nc.Fecha, nc.Comprobante, nc.IDCliente, c.Referencia, nc.IDTipoComprobante, nc.IDCiudad,
nc.IDSucursal, nc.Sucursal, nc.IDDeposito, nc.IDZonaVenta, nc.idtipocliente, nc.IDEstado, nc.IDMoneda, nc.Condicion,
nc.Total, nc.Saldo, nc.Fecha, ci.Descripcion,c.RazonSocial,c.RUC,c.Telefono, nc.Anulado, nc.Aplicar, nc.Fec, c.LimiteCredito, c.PlazoCredito

union all

Select nd.Idtransaccion,
'Fecha'=CONVERT(date, Fecha),
'Documento' = nd.Comprobante, nd.IDCliente, 'ReferenciaCliente'=c.Referencia,  nd.IDTipoComprobante,
 'IdVendedor' = Null, 'IdCobrador' =Null,'IdChofer' = Null,  nd.IDCiudad, nd.IDSucursal, nd.sucursal,
 nd.IDDeposito, nd.IDZonaVenta, nd.IDTipoCliente, nd.IDEstado, nd.IDMoneda,  nd.Condicion,
 'Ultimo Pago' = (SELECT     Convert(date,MAX(C.FechaCobranza))
                  FROM          VVentaDetalleCobranza C
                  WHERE      C.IDTransaccion = nd.IDTransaccion), 
 nd.total,
  'Saldo' = nd.Saldo, 
  'Dias Vencidos' = DATEDIFF(D, nd.Fecha, GETDATE()) ,
  SUM((CASE
WHEN (DATEDIFF(D, nd.Fecha, GETDATE()) > 0 and DATEDIFF(D, nd.Fecha, GETDATE()) <=30) THEN
  nd.Saldo
ELSE
  0
END)) AS TOTA30,SUM((CASE
WHEN  (DATEDIFF(D, nd.Fecha, GETDATE()) > 30 and DATEDIFF(D, nd.Fecha, GETDATE()) <=60) THEN
  nd.Saldo
ELSE
  0
END)) AS TOTA60,
SUM((CASE
WHEN  (DATEDIFF(D, nd.Fecha, GETDATE()) > 60 and DATEDIFF(D, nd.Fecha, GETDATE()) <=90) THEN
  nd.Saldo
ELSE  
  0
END)) AS TOTA90,
'Ciudad'=ci.Descripcion,
'Cobrador'='',
'Vendedor''',
'Cliente'=c.RazonSocial,
c.RUC,
c.Telefono, nd.Anulado,
'Cancelado'= (Case When(ND.Aplicar) = 'True' Then (Case When(ND.Saldo) = 0 Then 'True' Else 'False' End) Else 'True' End),
'FechaVencimiento'= convert(varchar(50),isnull(nd.Fec,'')),
'LineaCredito' = c.LimiteCredito,
'Plazo' = c.PlazoCredito,
'Año' = year(Fecha)
  
from VNotaDebito nd
join Cliente c on c.ID = nd.IDTransaccion
join Ciudad ci on ci.ID = nd.IDCiudad
WHERE     (CASE WHEN (ND.Aplicar) = 'True' THEN (CASE WHEN (ND.Saldo) = 0 THEN 'True' ELSE 'False' END) ELSE 'False' END) = 'True'
group by nd.IDTransaccion, nd.Fecha, nd.Comprobante, nd.IDCliente, c.Referencia, nd.IDTipoComprobante, nd.IDCiudad,
nd.IDSucursal, nd.Sucursal, nd.IDDeposito, nd.IDZonaVenta, nd.idtipocliente, nd.IDEstado, nd.IDMoneda, nd.Condicion,
nd.Total, nd.Saldo, nd.Fecha, ci.Descripcion,c.RazonSocial,
c.RUC,c.Telefono, nd.Anulado, nd.Aplicar, nd.Fec, c.LimiteCredito, c.PlazoCredito












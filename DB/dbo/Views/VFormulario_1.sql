﻿CREATE View [dbo].[VFormulario]
As
Select 
F.IDFormulario,
F.IDImpresion,
F.Descripcion,
F.Detalle,
F.CantidadDetalle,
F.Copias,
F.Fondo,
F.TamañoFondoX,
F.TamañoFondoY,

--Impresion
'Impresion'=I.Descripcion,
I.Vista,
I.VistaDetalle,
I.Observacion,

--Papel 
'TamañoPapel'=IsNull(F.TamañoPapel, 'A4'),
'TamañoPapelX'=IsNull(F.TamañoPapelX, 827),
'TamañoPapelY'=IsNull(F.TamañoPapelY, 1169),
'Horizontal'=IsNull(F.Horizontal, 'False'),
'MargenIzquierdo'=IsNull(F.MargenIzquierdo, 0),
'MargenDerecho'=IsNull(F.MargenDerecho, 0),
'MargenArriba'=IsNull(F.MargenArriba, 0),
'MargenAbajo'=IsNull(F.MargenAbajo, 0)

From Formulario F
Join Impresion I On F.IDImpresion=I.IDImpresion

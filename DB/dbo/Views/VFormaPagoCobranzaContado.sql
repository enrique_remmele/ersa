﻿CREATE View [dbo].[VFormaPagoCobranzaContado]

As

--Efectivo
Select
FP.IDTransaccion,
FP.ID,
'FormaPago'='Efectivo',
'Moneda'=M.Referencia,
'ImporteMoneda'=E.ImporteMoneda,
'Cambio'=ISnull(FP.Cotizacion,1),
'Banco'='---',
'Comprobante'=E.Comprobante,
'FechaEmision'=NULL,
'Vencimiento'=NULL,
'TipoComprobante'=TC.Descripcion,
'Tipo'='---',
'Importe'=FP.Importe,
'CancelarCheque'=FP.CancelarCheque

From FormaPago FP
Join Efectivo E On FP.IDTransaccion=E.IDTransaccion And FP.ID=E.ID
Join TipoComprobante TC On E.IDTipoComprobante=TC.ID
Join Moneda M On E.IDMoneda=M.ID

Union All

--Cheque Cliente
Select
FP.IDTransaccion,
FP.ID,
'FormaPago'='Cheque',
'Moneda'=V.Moneda,
'ImporteMoneda'=FP.ImporteCheque,
'Cambio'=ISnull(FP.Cotizacion,1),
'Banco'=V.Banco,
'Comprobante'=V.NroCheque,
'FechaEmision'=convert(varchar(50),V.Fecha,10),
'Vencimiento'=convert(varchar(50),V.FechaVencimiento,10),
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE' End),
'Tipo'=V.Tipo,
'Importe'=FP.Importe,
FP.CancelarCheque

From FormaPago FP
Join VChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion

Union All

--Documento
Select
FP.IDTransaccion,
FP.ID,
'FormaPago'='Documento',
'Moneda'=M.Referencia,
'ImporteMoneda'=FPD.ImporteMoneda,
'Cambio'=FPD.Cotizacion,
'Banco'=Isnull(B.referencia,'---'),
'Comprobante'=FPD.Comprobante,
'FechaEmision'=NULL,
'Vencimiento'=NULL,
'TipoComprobante'=TC.Descripcion,
'Tipo'='---',
'Importe'=FP.Importe,
FP.CancelarCheque

From FormaPago FP
Join FormaPagoDocumento FPD On FP.IDTransaccion=FPD.IDTransaccion And FP.ID=FPD.ID
Join TipoComprobante TC On FPD.IDTipoComprobante=TC.ID
Join Moneda M On FPD.IDMoneda=M.ID
Left join Banco B on B.ID = FPD.IDBanco








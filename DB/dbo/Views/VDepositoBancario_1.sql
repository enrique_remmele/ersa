﻿CREATE View [dbo].[VDepositoBancario]

As

Select 

---Cabecera
DB.IDTransaccion,
DB.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'Ciudad'=S.CodigoCiudad,
DB.Numero,
'Num'=DB.Numero,

DB.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
DB.NroComprobante,
'Comprobante'=DB.NroComprobante,

DB.Fecha,
'Fec'=CONVERT(varchar(50), DB.Fecha, 5),

--Cuenta Bancaria
'IDCuentaBancaria'=CB.ID,
'Cuenta' = CB.CuentaBancaria,
'TipoCuenta'=(Case When CB.IDTipoCuentaBancaria=1 Then 'False' Else 'True' End),
'Banco'=CB.Banco,
'ReferenciaBanco'=CB.Referencia,
CB.IDMoneda,
'Moneda'=CB.Mon,

DB.Cotizacion,
DB.Observacion,
DB.TotalEfectivo,
DB.TotalChequeLocal,
TotalChequeOtros,
DB.TotalDocumento,
DB.TotalTarjeta,
DB.Total,
DB.Conciliado,
DB.DiferenciaCambio,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario)

 
From DepositoBancario DB
Join Transaccion T On DB.IDTransaccion=T.ID
Join VSucursal S On DB.IDSucursal=S.ID
JOin VCuentaBancaria CB On DB.IDCuentaBancaria=CB.ID 
Join TipoComprobante TC On DB.IDTipoComprobante=TC.ID


















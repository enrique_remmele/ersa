﻿
CREATE View [dbo].[VTeleventaOtrosProductos]
As
select 
V.Idcliente,
DV.IDProducto,
P.Descripcion,
P.Referencia,
(Select Max(FechaEmision) from DetalleVenta 
Join Venta on  Venta.Idtransaccion = DetalleVenta.Idtransaccion
where IdProducto = DV.IdProducto
and IdCliente = V.IdCliente
and FechaEmision between '20140101' and '20140630') UltimaCompra,
'UltimoPrecio'=(select top(1) PrecioUnitario from Venta 
join DetalleVenta on Venta.IdTransaccion = DetalleVenta.Idtransaccion
Join Cliente Cliente on Venta.IdCliente = Cliente.Id
where Venta.idcliente = V.IdCliente
and DetalleVenta.idproducto = DV.IDProducto
order by DetalleVenta.idtransaccion desc),
'UltimaCantidad'=(select top(1) Cantidad from Venta 
join DetalleVenta on Venta.IdTransaccion = DetalleVenta.Idtransaccion
Join Cliente Cliente on Venta.IdCliente = Cliente.Id
where Venta.idcliente = V.IdCliente
and DetalleVenta.idproducto = DV.IDProducto
order by DetalleVenta.idtransaccion desc),
'Total'=(select sum(Cantidad) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = V.IdCliente),
'Maximo'=(select max(Cantidad) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = V.IdCliente),
'Promedio'=(select Round(sum(Cantidad)/Count(Venta.Idtransaccion),0) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = V.IdCliente),
'Veces'=(select count(Venta.Idtransaccion) 
		 from DetalleVenta join Venta on Venta.Idtransaccion = DetalleVenta.Idtransaccion
		 where DetalleVenta.IDProducto = DV.IDProducto and Venta.Idcliente = V.IdCliente)

from Venta V
join DetalleVenta DV on V.IdTransaccion = DV.Idtransaccion
Join Producto P on P.Id = DV.IdProducto
and V.Procesado = 1
and Anulado = 0
group by V.IdCliente,
DV.IdProducto,
P.Descripcion,
P.Referencia














﻿CREATE view [dbo].[VDetalleFondoFijoContabilidad]
as

--DetalleFondoFijo y Gasto
Select 

--Gasto
GFF.Numero,
'Tipo'='GASTO',
--'TipoComprobante'=G.[Cod.],
'TipoComprobante'=concat(G.[Cod.],(case when G.IncluirLibro = 1 then '' else ' - N' end)),
DFF.IDSucursal,
'Sucursal'=S.Descripcion,
--DFF.IDGrupo,
G.IDGrupo,
DFF.ID,
'Grupo'=GR.Descripcion,
'NroComprobante'=G.NroComprobante,
G.Observacion,
'RazonSocial'=P.RazonSocial,

--Detalle Fondo Fijo
'IDTransaccionVale'=IsNull(DFF.IDTransaccionVale,0),
'IDTransaccionGasto'=IsNull(DFF.IDTransaccionGasto,0),
DFF.Cancelado,
DFF.IDTransaccionRendicionFondoFijo,
'Fecha'=convert(varchar(50),G.Fecha,5),
G.Total,

--Cuenta Contable
'Codigo'=IsNull(VDA.Codigo, '---'),
'Descripcion'=IsNull(VDA.Descripcion, '---'),
'ObservacionAsiento'=IsNull(VDA.Observacion, '---'),
'Debito'=IsNull(VDA.Debito, 0),

--Timbrado
G.NroTimbrado,
G.FechaVencimientoTimbrado

From DetalleFondoFijo DFF
Join VGasto G on DFF.IDTransaccionGasto=G.IDTransaccion 
Join VGastoFondoFijo GFF On G.IDTransaccion=GFF.IDTransaccion
Join Sucursal S on S.ID=DFF.IDSucursal 
--Join Grupo GR on GR.ID=DFF.IDGrupo 
Join Grupo GR on GR.ID=G.IDGrupo 
Join Proveedor  P on G.IDProveedor=P.ID 
Left Outer Join VDetalleAsiento VDA On G.IDTransaccion = VDA.IDTransaccion
Where IsNull(VDA.Debito, 0)>0

Union all
 
--Vale
Select 
--Vale
V.Numero,
'Tipo'='VALE',
'TipoComprobante'='VALE',
DFF.IDSucursal,
'Sucursal'=S.Descripcion,
DFF.IDGrupo,
DFF.ID,
'Grupo'=GR.Descripcion,
'NroComprobante'=CONVERT(Varchar(50),V.NroComprobante),
'Observacion'=V.Motivo,
'RazonSocial'=V.Nombre,

--Detalle Fondo Fijo
'IDTransaccionVale'=IsNull(DFF.IDTransaccionVale,0),
'IDTransaccionGasto'=IsNull(DFF.IDTransaccionGasto,0),
DFF.Cancelado,
DFF.IDTransaccionRendicionFondoFijo,
'Fecha'=convert(varchar(50),V.Fecha,5),
V.Total, 

--Cuenta Contable
VDA.Codigo,
VDA.Descripcion,
'ObservacionAsiento'=VDA.Observacion,
VDA.Debito,

--Timbrado
'NroTimbrado'= '',
'FechaVencimientoTimbrado'=''

From VVale V 
Join DetalleFondoFijo DFF on DFF.IDTransaccionVale=V.IDTransaccion 
Join Sucursal S on S.ID=DFF.IDSucursal
Join Grupo GR on GR.ID=DFF.IDGrupo 
Join VDetalleAsiento VDA on VDA.IDTransaccion=V.IDTransaccion

Where V.ARendir = 'False' And  V.Anulado = 'False' And VDA.Debito>0



























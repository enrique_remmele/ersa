﻿




CREATE View [dbo].[VControlInventario]
As
Select 
CI.IDTransaccion,
CI.Numero,
'Num'=CI.Numero,
CI.Fecha,
'Fec'=CONVERT(varchar(50), CI.Fecha, 6),

'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
'IDSucursalOperacion'=CI.IDSucursal,
'IDDepositoOperacion'=CI.IDDeposito,
'Deposito'=D.Descripcion,

CI.Observacion,
CI.Total,
CI.Anulado,
'Estado'=Case When CI.Anulado='True' Then 'Anulado' Else (Case When CI.Procesado='True' Then 'Procesado' Else (Case When CI.ControlSistema='True' Then 'Sistema Controlado' Else (Case When CI.ControlEquipo='True' Then 'Equipo Controlado' Else 'Pendiente' End) End) End) End,
CI.ControlEquipo,
CI.ControlSistema,
CI.Procesado,

--Transaccion
'FechaTransaccion'=TR.Fecha,
TR.IDDeposito,
TR.IDSucursal,
TR.IDTerminal,
TR.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=CI.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CI.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CI.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=CI.IDTransaccion),

--Ajuste
'IDUsuarioAjuste'=(IsNull(CI.IDUsuarioAjuste, 0)),
'UsuarioAjuste'=(Select IsNull((Select U.Usuario From Usuario U  Where U.ID=CI.IDUsuarioAjuste), '---')),
'UsuarioAjusteIdentificador'=(Select IsNull((Select U.Identificador From Usuario U  Where U.ID=CI.IDUsuarioAjuste), '---')),
'FechaAjuste'=(IsNull(Convert(varchar(50), CI.FechaAjuste), '---'))

From ControlInventario CI
Join Transaccion TR On CI.IDTransaccion=TR.ID
Join Deposito D On CI.IDDeposito=D.ID
Join Sucursal S On CI.IDSucursal=S.ID







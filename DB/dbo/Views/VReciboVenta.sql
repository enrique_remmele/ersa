﻿
CREATE View [dbo].[VReciboVenta]
As

Select RV.IDTransaccionVenta,
RV.IDRecibo,
R.Referencia,
RV.NroRecibo,
'Recibo'=Concat(R.Referencia, '-',RV.NroRecibo),
RV.Fecha,
RV.Anulado,
RV.IDUsuarioAnulacion,
RV.FechaAnulacion,
V.Saldo,
V.Total,
V.ReferenciaCliente,
V.Cliente,
V.FechaEmision,
'Fec'=CONVERT(varchar(50), V.FechaEmision, 3),
V.Comprobante,
V.RUC,
V.IDSucursal,
V.Sucursal,
'Documento'= concat(R.Referencia,'-',RV.NroRecibo),
V.IDCliente,
V.IDMoneda,
V.Moneda,
V.Boleta,
'Estado'=(Case when Isnull(RV.Anulado,0) = 1 then 'ANULADO' else 
		 (Case when V.Saldo = 0 then (Case when(isnull((select top(1) [Comp. Cob.] from VVentaDetalleCobranza where  AnuladoCobranza = 0 and IDTransaccion = V.IDTransaccion),'') = Concat(R.Referencia, '-',RV.NroRecibo)) then 'COBRADO' else 'COBRADO CON COMPROBANTE DISTINTO' end) else 
		 'PENDIENTE DE RENDICION' end) end),
'ReciboCobranza'=isnull((select top(1) [Comp. Cob.] from VVentaDetalleCobranza where  AnuladoCobranza = 0 and IDTransaccion = V.IDTransaccion),'')
From ReciboVenta RV
Join VVenta V on V.IDTransaccion = RV.IDTransaccionVenta
Join Recibo R on R.ID = RV.IDRecibo
Where RV.IDTransaccionVenta in (Select IDTransaccion From Venta)

Union all 

Select RV.IDTransaccionVenta,
RV.IDRecibo,
R.Referencia,
RV.NroRecibo,
'Recibo'=Concat(R.Referencia, '-',RV.NroRecibo),
RV.Fecha,
RV.Anulado,
RV.IDUsuarioAnulacion,
RV.FechaAnulacion,
'Saldo'= 0,
'Total'= 0,
'ReferenciaCliente'='',
'Cliente'='LA VENTA ASOCIADA SE ELIMINO',
'FechaEmision'=RV.Fecha,
'Fec'=CONVERT(varchar(50), RV.Fecha, 3),
'Comprobante'='',
'RUC'='',
'IDSucursal'=0,
'Sucursal'='',
'Documento'= concat('020','-',NroRecibo),
'IDCliente'=0,
'IDMoneda'=0,
'Moneda'='GS',
'Boleta'= 'False',
'Estado'='LA VENTA ASOCIADA SE ELIMINO',
'ReciboCobranza'=''
From ReciboVenta RV
--Join VVenta V on V.IDTransaccion = RV.IDTransaccionVenta
Join Recibo R on R.ID = RV.IDRecibo
Where RV.IDTransaccionVenta not in (Select IDTransaccion From Venta)













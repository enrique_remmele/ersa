﻿



CREATE View [dbo].[VImpresionFacturaDetalle]
As
Select 
IDTransaccion,
DV.Referencia,
Cantidad,
'Descripcion'=DV.Descripcion,
CodigoBarra,
'PrecioUnitario'=dbo.FFormatoNumericoImpresion(PrecioUnitario - descuentoUnitario,M.Decimales),
'Exento'=dbo.FFormatoNumericoImpresion((Case when IDImpuesto = 3 then Total else 0 end ),M.Decimales),
'IVA10' = dbo.FFormatoNumericoImpresion((Case when IDImpuesto = 1 then Total else 0 end ),M.Decimales),
'IVA5'=dbo.FFormatoNumericoImpresion((Case when IDImpuesto = 2 then Total else 0 end ),M.Decimales)
 from vDetalleVenta DV
 Join Moneda M on M.ID = DV.IDMoneda






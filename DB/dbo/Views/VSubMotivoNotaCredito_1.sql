﻿
CREATE view [dbo].[VSubMotivoNotaCredito]
As 
Select 
*,
'Tipo' = (Case when Anulacion = 1 then 'Anulacion' else 
			(Case when Devolucion = 1 then 'Devolucion' else
			(Case when AcuerdoComercial = 1 then 'Acuerdo' else 
			(Case when DiferenciaPrecio = 1 then 'Diferencia de Precio' else
			(Case when DiferenciaPeso = 1 then 'Diferencia de PESO' else
			'Financiero' end)end)end) end) end)
from SubMotivoNotaCredito


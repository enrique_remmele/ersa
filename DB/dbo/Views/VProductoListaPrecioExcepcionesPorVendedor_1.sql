﻿CREATE View [dbo].[VProductoListaPrecioExcepcionesPorVendedor]
As
Select 
PLPE.IDProducto,
'Producto' = P.Descripcion,
P.IDTipoProducto,
PLPE.IDListaPrecio,
'ListaPrecio' = LP.Descripcion,
PLPE.IDCliente,
PLPE.IDSucursal,
PLPE.IDTipoDescuento,
PLPE.IDMoneda,
'PrecioLista' = (Select top(1) Precio from ProductoListaPrecio where IDListaPrecio = LP.ID and IDProducto = P.ID),
PLPE.Descuento,
'PrecioConDescuento' = ((Select top(1) Precio from ProductoListaPrecio where IDListaPrecio = LP.ID and IDProducto = P.ID) - PLPE.Descuento),
PLPE.Porcentaje,
'Desde' = Convert(date,PLPE.Desde),
'Hasta' = Convert(date,PLPE.Hasta),
'TipoDescuento'=TD.Descripcion,
'TipoDescuentoCodigo'=TD.Codigo,
'FechaDesde'=IsNull(CONVERT(varchar(50), PLPE.Desde, 5), '---'),
'FechaHasta'=IsNull(CONVERT(varchar(50), PLPE.Hasta, 5), '---'),
'Cliente'=C.RazonSocial,
'Moneda'=M.Referencia,
'Sucursal'=S.Descripcion,
'Referencia' = C.Referencia,
'IDVendedor' = C.IDVendedor,
'Vendedor' = C.Vendedor

From ProductoListaPrecioExcepciones PLPE
Join VProducto P On PLPE.IDProducto=P.ID
Join ListaPrecio LP On PLPE.IDListaPrecio=LP.ID
Join TipoDescuento TD On PLPE.IDTipoDescuento=TD.ID
Join VCliente C On PLPE.IDCliente = C.ID
Join VMoneda M On PLPE.IDMoneda = M.ID
Join Sucursal S On PLPE.IDSucursal=S.ID
Join ProductoListaPrecio PLP on P.id = PLP.IDProducto and LP.ID = PLP.IDListaPrecio and S.id = PLP.IDSucursal
where PLP.Precio >0


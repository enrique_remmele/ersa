﻿CREATE View [dbo].[VChequeClienteVenta]
As

Select
'Tipo'='COBRANZA CREDITO',
'Cheque Nro'=C.NroCheque, 
'Banco'= C.Banco ,
C.IdBanco,
'Cuenta Bancaria'=C.CuentaBancaria  ,
'Emitido Por'= Case When C.Librador is Null Then  CL.RazonSocial    Else c.Librador End ,
'Clientes'=C.Cliente ,
C.idcliente,
'Importes'= C.Importe ,
'Emision'=c.Fec,  
C.IDTransaccion, 
V.Comprobante,
V.Fec,
V.[Fec. Venc.],
C.FechaVencimiento ,
'Vto.Cheque'=IsNull(CONVERT(varchar(50), C.FechaVencimiento, 5), '---'),
V.Cliente ,
CL.PlazoCredito,
'PlazoAdicional'=  DATEDIFF(D,V.FechaVencimiento,V.FechaEmision), 
VC.Importe,
C.Diferido,
C.Cartera,
C.fecha,
C.librador,
Cl.RazonSocial,

--Compos para informe cheques diferidos
'Fecha de Cobranza'= convert (varchar(50), CC.FechaEmision,5), 
'FechaCobranza'= CC.FechaEmision,
'Cond.'=v.Condicion ,
'Fact.Nro'=V.NroComprobante ,
'Fecha Emis.Fact' = CONVERT (varchar(50), v.FechaEmision,5 ),
'Plazo Diaz'= DATEDIFF(D ,V.FechaEmision,C.FechaVencimiento ) ,
'Sucursal'=V.Sucursal, 
C.IDSucursal,
C.Descontado
  
From VChequeCliente C 
join FormaPago FP on C.IDTransaccion  = FP.IDTransaccionCheque
join CobranzaCredito CC on FP.IDTransaccion = CC.IDTransaccion  
join VentaCobranza VC on CC.IDTransaccion = VC.IDTransaccionCobranza 
join VVenta  V on VC.IDTransaccionVenta = v.IDTransaccion
join Cliente CL on C.IDCliente = CL.ID 

Union All

Select
'Tipo'='COBRANZA CONTADO',
'Cheque Nro'=C.NroCheque, 
'Banco'= C.Banco ,
C.IdBanco,
'Cuenta Bancaria'=C.CuentaBancaria  ,
'Emitido Por'= Case When C.Librador is Null Then  CL.RazonSocial    Else c.Librador End ,
'Clientes'=C.Cliente ,
C.idcliente,
'Importes'= C.Importe ,
'Emision'=c.Fec,  
C.IDTransaccion, 
V.Comprobante,
V.Fec,
V.[Fec. Venc.],
C.FechaVencimiento,
'Vto.Cheque'=IsNull(CONVERT(varchar(50), C.FechaVencimiento, 5), '---'),
V.Cliente ,
CL.PlazoCredito,
'PlazoAdicional'=  DATEDIFF(D,V.FechaVencimiento,V.FechaEmision), 
VC.Importe,
C.Diferido,
C.Cartera,
c.fecha,
C.librador,
Cl.RazonSocial,

--Compos para informe cheques diferidos
'Fecha de Cobranza'= convert (varchar(50), CC.Fecha,5), 
'FechaCobranza'= CC.Fecha,
'Cond.'=v.Condicion ,
'Fact.Nro'=V.NroComprobante ,
'Fecha Emis.Fact' = CONVERT (varchar(50), v.FechaEmision,5 ),
'Plazo Diaz'= DATEDIFF(D ,V.FechaEmision,C.FechaVencimiento ) ,
'Sucursal'=V.Sucursal, 
C.IDSucursal,
C.Descontado
  
From vChequeCliente C 
join FormaPago FP on C.IDTransaccion  = FP.IDTransaccionCheque
join CobranzaContado CC on FP.IDTransaccion = CC.IDTransaccion  
join VentaCobranza VC on CC.IDTransaccion = VC.IDTransaccionCobranza And VC.IDTransaccionCobranza=CC.IDTransaccion
join VVenta  V on VC.IDTransaccionVenta = v.IDTransaccion
join ChequeClienteCobranzaContado CCC on V.IDTransaccion  = CCC.IDTransaccionVenta   
join Cliente CL on C.IDCliente = CL.ID 





﻿CREATE view [dbo].[VLibroMayorDetalle]
as
Select
DA.IDTransaccion,
'Codigo'=convert(varchar(50),DA.Codigo),
DA.Descripcion,
a.Fecha, 
'Asiento'=A.Numero,
DA.NroComprobante,
'Comprobante'=concat(A.TipoComprobante,' ',DA.NroComprobante),
DA.IDSucursal,
'Suc'=S.Descripcion,
'CodSucursal'=S.Codigo,
A.IDTipoComprobante,
'Concepto'=Concat('',(Case When(DA.Observacion) = '' Then (Case When(A.Detalle) = '' Then O.Descripcion Else A.Detalle End) Else concat(dbo.FComprobanteLibroMayor(A.IDTransaccion,O.FormName),'-',DA.Observacion) End)),
DA.Debito,
DA.Credito,
'TipoCuenta'=dbo.FCuentaContableTipo(convert(varchar(50),DA.Codigo)),

'Producto' = (Select top(1) DM.Producto from vDetalleMovimiento DM where DM.IDTransaccion = DA.IDTransaccion),
'ProductoCantidad' = (Select top(1) DM.Cantidad from vDetalleMovimiento DM where DM.IDTransaccion = DA.IDTransaccion),
'CostoUnitario' = (Select top(1) DM.PrecioUnitario from vDetalleMovimiento DM where DM.IDTransaccion = DA.IDTransaccion),
'CostoTotal' = (Select top(1) DM.Total from vDetalleMovimiento DM where DM.IDTransaccion = DA.IDTransaccion),

--Operacion
T.IDOperacion,
'Operacion'=O.Descripcion,

--Centro de Costos
DA.IDCentroCosto,
'CentroCosto'=ISNull(CC.Descripcion, '---'),
'CodigoCentroCosto'=IsNull(CC.Codigo, ''),
--Unidad de negocio
DA.IDUnidadNegocio,
'UnidadNegocio'=ISNull(UN.Descripcion, '---'),
'CodigoUnidadNegocio'=IsNull(UN.Codigo, ''),
A.Usuario,
A.FechaTransaccion,
A.Cotizacion,
O.FormName,
A.IDMoneda,
A.Moneda

From VDetalleAsiento DA 
Join VAsiento A on A.IDTransaccion=DA.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Left Outer JOin Sucursal S On DA.IDSucursal=S.ID
Left Outer Join CentroCosto CC On DA.IDCentroCosto=CC.ID
Left Outer Join UnidadNegocio UN On DA.IDUnidadNegocio=UN.ID
Where O.ID in(59,57,62,63,61,15)

union all

Select
DA.IDTransaccion,
'Codigo'=convert(varchar(50),DA.Codigo),
DA.Descripcion,
a.Fecha, 
'Asiento'=A.Numero,
DA.NroComprobante,
'Comprobante'=concat(A.TipoComprobante,' ',DA.NroComprobante),
DA.IDSucursal,
'Suc'=S.Descripcion,
'CodSucursal'=S.Codigo,
A.IDTipoComprobante,
'Concepto'=Concat('',(Case When(DA.Observacion) = '' Then (Case When(A.Detalle) = '' Then O.Descripcion Else A.Detalle End) Else concat(dbo.FComprobanteLibroMayor(A.IDTransaccion,O.FormName),'-',DA.Observacion) End)),
DA.Debito,
DA.Credito,
'TipoCuenta'=dbo.FCuentaContableTipo(convert(varchar(50),DA.Codigo)),

'Producto' = (Select top(1) concat(DM.ReferenciaProducto,' - ',DM.Producto) from VTicketBascula DM where DM.IDTransaccion = DA.IDTransaccion),
'ProductoCantidad' = (Select top(1) DM.PesoBascula from VTicketBascula DM where DM.IDTransaccion = DA.IDTransaccion),
'CostoUnitario' = (Select top(1) DM.PrecioUnitarioDiscriminado from VTicketBascula DM where DM.IDTransaccion = DA.IDTransaccion),
'CostoTotal' = (Select top(1) DM.TotalDiscriminado from VTicketBascula DM where DM.IDTransaccion = DA.IDTransaccion),

--Operacion
T.IDOperacion,
'Operacion'=O.Descripcion,

--Centro de Costos
DA.IDCentroCosto,
'CentroCosto'=ISNull(CC.Descripcion, '---'),
'CodigoCentroCosto'=IsNull(CC.Codigo, ''),
--Unidad de negocio
DA.IDUnidadNegocio,
'UnidadNegocio'=ISNull(UN.Descripcion, '---'),
'CodigoUnidadNegocio'=IsNull(UN.Codigo, ''),
A.Usuario,
A.FechaTransaccion,
A.Cotizacion,
O.FormName,
A.IDMoneda,
A.Moneda

From VDetalleAsiento DA 
Join VAsiento A on A.IDTransaccion=DA.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Left Outer JOin Sucursal S On DA.IDSucursal=S.ID
Left Outer Join CentroCosto CC On DA.IDCentroCosto=CC.ID
Left Outer Join UnidadNegocio UN On DA.IDUnidadNegocio=UN.ID
where O.ID in(64)














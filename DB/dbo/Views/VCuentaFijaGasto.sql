﻿
CREATE View [dbo].[VCuentaFijaGasto]
AS
Select 
C.ID,

--Cuenta Contable
C.IDCuentaContable,
'Cuenta'=CC.Descripcion,
'Codigo'=CC.Codigo,
'CuentaContable'=CC.Codigo + ' - ' + CC.Descripcion,

C.Debe,
C.Haber,
'DebeHaber'=(Case When (C.Debe) = 'True' Then 'DEBE' Else (Case When (C.Haber) = 'True' Then 'HABER' Else '---' End) End),	

--Tipo de Comprobante
C.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,

--Moneda
C.IDMoneda,
'Moneda'=M.Descripcion,

--Tipo de Cuenta
C.IDTipoCuentaFija,
'TipoCuentaFija'=TCF.Descripcion,
'Tipo'=(Case When (TCF.Impuesto)='True' Then 'IMPUESTO' Else (Case When (TCF.Producto)='True' Then 'PRODUCTO' Else (Case When (TCF.Total)='True' Then 'TOTAL' Else (Case When (TCF.Descuento)='True' Then 'DESCUENTO' Else '---' End) End) End) End),
TCF.IDImpuesto,
TCF.Producto,
TCF.Total,
TCF.Descuento,
TCF.IncluirImpuesto,
TCF.IncluirDescuento,
TCF.Campo,

C.Orden,
C.Descripcion,
C.BuscarProveedor

From CuentaFijaGasto C
Join CuentaContable CC on C.IDCuentaContable=CC.ID
Join TipoComprobante TC On C.IDTipoComprobante=TC.ID
Join Moneda M On C.IDMoneda=M.ID
Join TipoCuentaFija TCF On C.IDTipoCuentaFija=TCF.ID

﻿CREATE View [dbo].[vInventarioClienteFecha] 
As 
select IDTransaccion, 
'IDTrasaccionOperacion'=IDTransaccion ,
1 as orden, 
OPER='Venta',
Comprobante, 
Timbrado,
IDCliente,
Cliente, 
RUC,
FechaEmision, 
Total as IMporte, 
Documento=Comprobante, 
Saldo, 
Total as Venta, 
0 as VentaImportada,
0 as NotaCreditoTotal,
0 as NotaCreditoSinAplicar,
0 as CobranzaCredito,
0 as CobranzaContado,
'DocumentoVenta'= concat(Cliente,'  -  ',Comprobante)
from vventa
where Anulado = 0 
and Procesado = 1
and CancelarAutomatico = 0

union all

select VI.IDTransaccion,
'IDTrasaccionOperacion'=VI.IDTransaccion,
2 as orden,
OPER='VentaImportada',
'Comprobante'=Comprobante,
'Timbrado'='',
'IDCliente'=(Select IDCliente from VVenta where IDTransaccion = VI.IDTransaccion),
'Cliente'=(Select Cliente from VVenta where IDTransaccion = VI.IDTransaccion),
'RUC'=(Select RUC from VVenta where IDTransaccion = VI.IDTransaccion),
(Select FechaEmision from VVenta where IDTransaccion = VI.IDTransaccion) as Fecha,
(Isnull(VI.Cobrado,0)* -1) as Importe,
'Documento'= VI.Comprobante,
VI.Saldo,
0 as Venta, 
(Isnull(VI.Cobrado,0)* -1) as VentaImportada,
0 as NotaCreditoTotal,
0 as NotaCreditoSinAplicar,
0 as CobranzaCredito,
0 as CobranzaContado,
'DocumentoVenta'= concat((Select Cliente from VVenta where IDTransaccion = VI.IDTransaccion),'  -  ',(Select Comprobante from VVenta where IDTransaccion = VI.IDTransaccion))
 from VentaImportada VI
 where VI.Cobrado is not null

 union all
--Nota de credito aplicado totalmente
select 'IDTransaccion'=NCV.IDTransaccionVenta,
'IDTrasaccionOperacion'= NC.IDTransaccion,
3 as orden,
OPER='NotaDeCreditoTotal',
'Comprobante'=NC.Comprobante,
'Timbrado'=0,
NC.IDCliente,
NC.Cliente,
NC.RUC,
'Fecha'=NC.Fecha,
(NCV.Importe)* -1 as Importe,
'Documento'= V.Comprobante,
NC.Saldo,
0 as Venta, 
0 as VentaImportada,
(NCV.Importe)* -1 as NotaCreditoTotal,
0 as NotaCreditoSinAplicar,
0 as CobranzaCredito,
0 as CobranzaContado,
'DocumentoVenta'= concat(Isnull(V.Cliente,''),'  -  ',IsNull(V.Comprobante,''))
 from vNotaCreditoVenta NCV
Join VNotaCredito NC on NC.IDTransaccion = NCV.IDTransaccion
Join vVenta V on V.IDTransaccion = NCV.IDTransaccionVenta
where NC.Anulado = 0 and NC.Procesado = 1
and NC.Saldo = 0


union  all
--Nota de credito sin aplicar
select 'IDTransaccion'=NC.IDTransaccion,
'IDTrasaccionOperacion'= NC.IDTransaccion,
4 as orden,
OPER='NC Sin Aplicar',
'Comprobante'=NC.Comprobante,
'Timbrado'=0,
NC.IDCliente,
NC.Cliente,
NC.RUC,
'Fecha'=NC.Fecha,
'Importe'=NC.TOtal,
'DocumentoVenta'= NC.Comprobante,
NC.Saldo,
0 as Venta, 
0 as VentaImportada,
0 as NotaCreditoTotal,
NC.TOtal as NotaCreditoSinAplicar,
0 as CobranzaCredito,
0 as CobranzaContado,
'DocumentoVenta'= ''
 from VNotaCredito NC
where NC.Anulado = 0
and NC.Procesado = 1
and NC.Saldo = NC.Total


 union all
 --Cobranza Credito
select 'IDTransaccion'=VC.IDTransaccionVenta,
'IDTrasaccionOperacion'=VC.IDTransaccionCobranza,
5 as orden,
OPER='CobranzaCredito',
'Comprobante'=concat(CC.Ciudad,'-',Numero),
'Timbrado'=0,
CC.IDCliente,
CC.Cliente,
CC.RUC,
'Fecha'=CC.FechaEmision,
(VC.Importe) * -1 as Importe,
'Documento'= V.Comprobante,
0 as saldo,
0 as Venta, 
0 as VentaImportada,
0 as NotaCreditoTotal,
0 as NotaCreditoSinAplicar,
(VC.Importe) * -1 as CobranzaCredito,
0 as CobranzaContado,
'DocumentoVenta'= concat((Select Cliente from VVenta where IDTransaccion = V.IDTransaccion),'  -  ',(Select Comprobante from VVenta where IDTransaccion = V.IDTransaccion))
 from VentaCobranza VC
Join vCobranzaCredito CC on CC.IDTransaccion = VC.IDTransaccionCobranza
Join vVenta V on V.IDTransaccion = VC.IDTransaccionVenta
where CC.Anulado = 0

union all

--Cobranza Contado
select 'IDTransaccion'=VC.IDTransaccionVenta,
'IDTrasaccionOperacion'=VC.IDTransaccionCobranza,
6 as orden,
OPER='CobranzaContado',
'Comprobante'=concat(CC.Ciudad,'-',Numero),
'Timbrado'=0,
'IDCliente'=(select IDCliente from vVenta where IDTransaccion = VC.IDTransaccionVenta),
'Cliente'=(select Cliente from vVenta where IDTransaccion = VC.IDTransaccionVenta),
'RUC'=(select RUC from vVenta where IDTransaccion = VC.IDTransaccionVenta),
'Fecha'=CC.Fecha,
(VC.Importe * -1) as Importe,
'Documento'= V.Comprobante,
0 as saldo,
0 as Venta, 
0 as VentaImportada,
0 as NotaCreditoTotal,
0 as NotaCreditoSinAplicar,
0 as CobranzaCredito,
(VC.Importe * -1) as CobranzaContado,
'DocumentoVenta'= concat((Select Cliente from VVenta where IDTransaccion = V.IDTransaccion),'  -  ',(Select NroComprobante from VVenta where IDTransaccion = V.IDTransaccion))
 from VentaCobranza VC
Join vCobranzaContado CC on CC.IDTransaccion = VC.IDTransaccionCobranza
Join VVenta V on V.IDTransaccion = VC.IDTransaccionVenta
where CC.Anulado = 0
 








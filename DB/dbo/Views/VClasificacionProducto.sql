﻿


CREATE View [dbo].[VClasificacionProducto]
As

Select 
CP.*,
--'Clasificacion'=Codigo + '-' + Descripcion
'Clasificacion'=CP.Descripcion,
'Grupo'=ISNULL((Select CPA.Descripcion From ClasificacionProductoAgrupador CPA Where CPA.ID=CP.IDAgrupador), '')

From ClasificacionProducto CP




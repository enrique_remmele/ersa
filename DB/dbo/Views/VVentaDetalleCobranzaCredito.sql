﻿CREATE View [dbo].[VVentaDetalleCobranzaCredito]
As
Select
V.IDTransaccion,
V.[Cod.],
V.IDSucursal,
V.Sucursal,
V.IDDeposito,
CC.IDCobrador,
CC.Cobrador,
V.IDVendedor,
V.Vendedor,
V.Comprobante,
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.Fec,
V.FechaVencimiento,
'Vencimiento'=V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,
VC.Importe,
'ImporteGs'=0,
V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
VC.IDTransaccionCobranza,

--Cobranza
CC.Numero,
'FechaCobranza'=CC.FechaEmision,
'Recibo'=CC.Comprobante,
CC.NroPlanilla,

--Para asiento
V.IDMoneda,
V.IDTipoComprobante,
'SaldoGs'= 0,
'CobranzaAnulada'=CC.Anulado ,
CC.IdSucursal as IdSucursalOperacion,
'FechaFactura'=V.FechaEmision,
'CotizacionHoy'=CC.Cotizacion,
'CotizacionCobranza'=CC.Cotizacion,
'IDTransaccionAnticipo' = 0

From VentaCobranza VC
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join VCobranzaCredito CC On VC.IDTransaccionCobranza = CC.IDTransaccion
and AnticipoCliente = 'False'

Union all

Select
V.IDTransaccion,
V.[Cod.],
--V.IDSucursal,
--V.Sucursal,
CC.IDSucursal,
CC.Sucursal,
V.IDDeposito,
CC.IDCobrador,
CC.Cobrador,
V.IDVendedor,
V.Vendedor,
V.Comprobante,
V.IDCliente,
V.Cliente,
V.Referencia,
V.Fecha,
V.Fec,
V.FechaVencimiento,
'Vencimiento'=V.[Fec. Venc.],
V.Moneda,
V.Cotizacion,
V.Total,
V.Cobrado,
V.Descontado,
V.Saldo,
VC.Importe,
'ImporteGs'=0,
V.Credito,
V.Condicion,
V.Cancelado,
VC.Cancelar,
V.Anulado,
'IDTransaccionCobranza'= CC.IDTransaccion,

--Cobranza
CC.Numero,
'FechaCobranza'= AP.Fecha,
'Recibo'=CC.Comprobante,
CC.NroPlanilla,

--Para asiento
V.IDMoneda,
V.IDTipoComprobante,
'SaldoGs'= 0,
'CobranzaAnulada'=CC.Anulado ,
CC.IdSucursal as IdSucursalOperacion,
'FechaFactura'=V.FechaEmision,
'CotizacionHoy'=CC.Cotizacion,
'CotizacionCobranza'=AP.Cotizacion,
VC.IDTransaccionAnticipo

From AnticipoVenta VC
Join VVenta V On VC.IDTransaccionVenta = V.IDTransaccion
Join VAnticipoAplicacion AP on AP.IDTransaccion = VC.IDTransaccionAnticipo
Join VCobranzaCredito CC On AP.IDTransaccionCobranza = CC.IDTransaccion
and AnticipoCliente = 'True'
and AP.Anulado = 'False'























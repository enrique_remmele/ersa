﻿CREATE View [dbo].[VDetallePedidoDescuentoDetalleAGuaraniesSINIVA]
As
Select
DP.IDTransaccion,

--Producto
DP.IDProducto,
DP.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DP.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DP.Observacion End),
'CodigoBarra'=P.CodigoBarra,
'UnidadMedida'=P.UnidadMedida,
P.Referencia,
DP.Observacion,
PE.Cotizacion,
PE.IDMoneda,

--Deposito
DP.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DP.Cantidad,
'PrecioUnitario'=(DP.PrecioUnitario * PE.Cotizacion)/I.FactorDiscriminado,
'Pre. Uni.'=(DP.PrecioUnitario * PE.Cotizacion)/I.FactorDiscriminado,
--DP.Total, ***** Se calcula el total sin descuento tactico, porque en esta vista, el descuento tactico esta declarado a parte
'Total'= (DP.PrecioUnitario * DP.Cantidad * PE.Cotizacion)/I.FactorDiscriminado,

--Pedido
PE.Fecha,
PE.IDCliente,
PE.FechaFacturar,
PE.IDVendedor,
'Vendedor'=(select Nombres from Vendedor where id = PE.IDVendedor),
'FechaFactura'=(Select Top(1) V.FechaEmision From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc),
PE.IDSucursal,
'Facturado'=(Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End),

'Estado'=(Case When (PE.Anulado) = 'True' Then 'Anulado' Else (Case When (Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End) = 'True' Then 'Facturado' Else 'Pendiente' End) End),
'Lista de Precio'=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=PE.IDListaPrecio), '---'),
'IDTipoCliente'=(Select idtipocliente from cliente where id = PE.IDCliente),
T.IDUsuario,

'CantidadAEntregar'= Isnull(DP.CantidadAEntregar,DP.Cantidad),
PE.Numero,
'Suc'= (Select codigo from Sucursal where Id = PE.IDSucursal),
'Peso'=cast(P.Peso as money),
P.IDTipoProducto,
PE.Anulado,
'EntregaCliente'=(Case When (PE.EntregaCliente) = 'True' Then 'True' Else 'False' End),
'CancelarAutomatico'=ISnull((Select FPF.CancelarAutomatico from FormaPagoFactura FPF where FPF.id = PE.IDFormaPagoFactura), 'False'),
'DescuentoUnitarioFACT'=(DV.DescuentoUnitario * PE.Cotizacion)/I.FactorDiscriminado,
'DescuentoUnitarioNCDiferenciaPrecio'= ((select dbo.FDescuentoUnitarioDiferenciaPrecio(DV.IDProducto,DV.IDTransaccion)) * PE.Cotizacion)/I.FactorDiscriminado,
'DescuentoUnitarioNCAcuerdo' = ((select dbo.FDescuentoUnitarioAcuerdo(DV.IDProducto,DV.IDTransaccion)) * PE.Cotizacion)/I.FactorDiscriminado,
'DescuentoUnitarioNCDiferenciaPeso' = ((select dbo.FDescuentoUnitarioDiferenciaPeso(DV.IDProducto,DV.IDTransaccion)) * PE.Cotizacion)/I.FactorDiscriminado,
'PrecioUnitarioNeto' = ((select dbo.FPrecioUnitarioNeto(DV.IDProducto,DV.IDTransaccion)) * PE.Cotizacion)/I.FactorDiscriminado,
'IDTransaccionVenta'=Isnull(DV.IDTransaccion,0)

From DetallePedido DP
join PedidoVenta PV on DP.IDTRansaccion = PV.IDTransaccionPedido
Join DetalleVenta DV on PV.IDTransaccionVenta = DV.IDTransaccion and DP.IDProducto = DV.IDProducto
Join Pedido PE On DP.IDTransaccion=PE.IDTransaccion
Join VProducto P On DP.IDProducto=P.ID
Join Deposito D On DP.IDDeposito=D.ID
Join Impuesto I On DP.IDImpuesto=I.ID
Join Transaccion T on T.ID = PE.IDTransaccion

Where PE.Procesado='True'

Union All

Select
0,

--Producto
DV.IDProducto,
DV.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DV.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DV.Observacion End),
'CodigoBarra'=P.CodigoBarra,
'UnidadMedida'=P.UnidadMedida,
P.Referencia,
V.Observacion,
V.Cotizacion,
V.IDMoneda,

--Deposito
DV.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DV.Cantidad,
'PrecioUnitario'=(DV.PrecioUnitario * V.Cotizacion)/I.FactorDiscriminado,
'Pre. Uni.'=(DV.PrecioUnitario * V.Cotizacion)/I.FactorDiscriminado,
--DP.Total, ***** Se calcula el total sin descuento tactico, porque en esta vista, el descuento tactico esta declarado a parte
'Total'= (DV.PrecioUnitario * DV.Cantidad * V.Cotizacion)/I.FactorDiscriminado,

--Pedido
V.FechaEmision,
V.IDCliente,
V.FechaEmision,
V.IDVendedor,
'Vendedor'=(select Nombres from Vendedor where id = V.IDVendedor),
'FechaFactura'=V.FechaEmision,
V.IDSucursal,
'Facturado'='True',

'Estado'='Facturado',
'Lista de Precio'=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=V.IDListaPrecio), '---'),
'IDTipoCliente'=(Select idtipocliente from cliente where id = V.IDCliente),
T.IDUsuario,

'CantidadAEntregar'= DV.Cantidad,
0,
'Suc'= (Select codigo from Sucursal where Id = V.IDSucursal),
'Peso'=cast(P.Peso as money),
P.IDTipoProducto,
0,
'EntregaCliente'='False',
'CancelarAutomatico'=ISnull((Select FPF.CancelarAutomatico from FormaPagoFactura FPF where FPF.id = V.IDFormaPagoFactura), 'False'),
'DescuentoUnitarioFACT'=(DV.DescuentoUnitario * V.Cotizacion)/I.FactorDiscriminado,
'DescuentoUnitarioNCDiferenciaPrecio'= ((select dbo.FDescuentoUnitarioDiferenciaPrecio(DV.IDProducto,DV.IDTransaccion)) * V.Cotizacion)/I.FactorDiscriminado,
'DescuentoUnitarioNCAcuerdo' = ((select dbo.FDescuentoUnitarioAcuerdo(DV.IDProducto,DV.IDTransaccion)) * V.Cotizacion)/I.FactorDiscriminado,
'DescuentoUnitarioNCDiferenciaPeso' = ((select dbo.FDescuentoUnitarioDiferenciaPeso(DV.IDProducto,DV.IDTransaccion)) * V.Cotizacion)/I.FactorDiscriminado,
'PrecioUnitarioNeto' = ((select dbo.FPrecioUnitarioNeto(DV.IDProducto,DV.IDTransaccion)) * V.Cotizacion)/I.FactorDiscriminado,
'IDTransaccionVenta'=V.IDTransaccion

From DetalleVenta DV
Join Venta V on DV.IDTRansaccion = V.IDTRansaccion
Join VProducto P On DV.IDProducto=P.ID
Join Deposito D On DV.IDDeposito=D.ID
Join Impuesto I On DV.IDImpuesto=I.ID
Join Transaccion T on T.ID = V.IDTransaccion
Where V.IDTransaccion not in (select IDTransaccionVenta from pedidoventa)
And V.Anulado = 0 
and V.Procesado = 1
































﻿





--USE [EXPRESS]
--GO

--/****** Object:  View [dbo].[VDetalleNotaCredito]    Script Date: 12/10/2012 13:53:32 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


CREATE View [dbo].[VDetalleNotaDebitoProveedor]
As
Select


DNCP.IDTransaccion,

--Producto
DNCP.IDProducto,
DNCP.ID,
'Producto'=P.Descripcion,
DNCP.Observacion ,
'Descripcion'=(Case When DNCP.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DNCP.Observacion End),
P.Referencia ,
'CodigoBarra'=P.CodigoBarra,


--Deposito
DNCP.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DNCP.Cantidad,
DNCP.PrecioUnitario,
'Pre. Uni.'=DNCP.PrecioUnitario,
DNCP.Total,

--Impuesto
DNCP.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
DNCP.TotalImpuesto,
DNCP.TotalDiscriminado,
'Exento'=I.Exento,

--Descuento
DNCP.TotalDescuento,
'Descuento'=DNCP.TotalDescuento,
DNCP.DescuentoUnitario,
'Desc Uni'=DNCP.DescuentoUnitario,
DNCP.PorcentajeDescuento,
'Desc %'=DNCP.PorcentajeDescuento,

--Costos
DNCP.CostoUnitario,
DNCP.TotalCosto,
DNCP.TotalCostoImpuesto,
DNCP.TotalCostoDiscriminado,

--Compra
DNCP.IDTransaccionCompra,

'Comprobante'= IsNull(C.Comprobante , '---'),
--Cantidad de Caja
'Caja'=IsNull(DNCP.Caja, 'False'),
DNCP.CantidadCaja,
'Cajas'=  ISNULL ((DNCP.Cantidad ),1) / ISNULL((Case When (P.UnidadPorCaja) = 0 Then 1 Else P.UnidadPorCaja End),1)

From DetalleNotaDebitoProveedor  DNCP
Join NotaDebitoProveedor NC On DNCP.IDTransaccion=NC.IDTransaccion
Left Outer Join  VCompra  C On DNCP.IDTransaccionCompra=C.IDTransaccion
Join Producto P On DNCP.IDProducto=P.ID
Join Deposito D On DNCP.IDDeposito=D.ID
Join Impuesto I On DNCP.IDImpuesto=I.ID



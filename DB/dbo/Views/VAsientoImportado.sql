﻿Create View [dbo].[VAsientoImportado]
As
Select 
AI.IDTransaccion,
AI.IDSucursal,
'Sucursal'=S.Descripcion,
'CodigoSucursal'=S.Codigo,
'Ciudad'=S.Ciudad,
AI.Numero,
AI.Fecha,
'Fec'=CONVERT(varchar(50), AI.Fecha, 6),
AI.Cotizacion,
AI.Observacion,
AI.Operacion,
AI.Total,
Estado=''


From AsientoImportado AI
Join VSucursal S on AI.IDSucursal=S.ID

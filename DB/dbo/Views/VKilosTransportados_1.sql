﻿

CREATE View [dbo].[VKilosTransportados]
As

Select Fecha,IDChofer, Chofer, IDCamion, 
Camion,IDProducto, Ref,Producto, 
IDSucursal, Sucursal,
sum(cantidad) as Cantidad,
Sum(Peso) as Peso,
'IDTipoProducto'= (select IDTipoProducto from Producto where ID = DLOTE.IDProducto),
'TipoProducto'= (select TipoProducto from vProducto where ID = DLOTE.IDProducto)
from VLoteDistribucion LOTE
Join vDetalleLoteDistribucion DLOTE on DLOTE.IDTransaccion = LOTE.IDTransaccion
Group by Fecha, IdProducto,Ref,Producto, IdChofer,Chofer, IdCamion, Camion, IdSucursal, Sucursal, Peso




﻿Create view vDestinoDevolucion
as 
select
DD.IDSucursal,
DD.IDDeposito,
DD.Destino,
'Deposito'=D.Descripcion 
from
DestinoDevolucion DD
join Deposito D on DD.IDDeposito = D.ID

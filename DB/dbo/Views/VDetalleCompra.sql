﻿

CREATE View [dbo].[VDetalleCompra]
As
Select
DT.IDTransaccion,
C.IDTipoComprobante,
C.IDProveedor,
C.Numero,
DT.IDProducto,
DT.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DT.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DT.Observacion End),
P.Referencia,
'CodigoBarra'=P.CodigoBarra,
P.Peso,
'PesoNumerico'= CONVERT (Decimal (18,8), P.Peso),
DT.IDDeposito,
'Deposito'=D.Descripcion,
'CuentaContableDeposito'=(Case when D.IDTipoDeposito = 5 then P.CodigoCuentaCompra else  (Case when P.IDTipoProducto = 27 then D.CuentaContableCombustible else D.CuentaContableMercaderia end) end),

--Departamento
C.IDDepartamentoEmpresa,
'DepartamentoEmpresa'=DE.Departamento,

DT.Observacion,
DT.IDImpuesto,
'Impuesto'=I.Descripcion,
DT.Cantidad,
DT.PrecioUnitario,
'Pre. Uni.'=DT.PrecioUnitario,
'Proveedor'= (Select Proveedor From	VCompra Where IDTransaccion= DT.IDTransaccion),
'ReferenciaProveedor'=(Select Referencia  From	VCompra Where IDTransaccion= DT.IDTransaccion),
'Comprobante'= (Select Comprobante From VCompra Where IDTransaccion= DT.IDTransaccion),
'IDSucursal'= (Select IDSucursal From VCompra Where IDTransaccion= DT.IDTransaccion),
'Sucursal'=(Select Sucursal From VCompra Where IDtransaccion= DT.IDTransaccion),
'Cod'=(Select [Cod.] From VCompra Where IDtransaccion= DT.IDTransaccion),
'Nro Comprobante'=(Select NroComprobante From VCompra Where IDtransaccion= DT.IDTransaccion),
'FechaFactura'=Convert(varchar(10),(Select Fecha From VCompra Where IDtransaccion= DT.IDTransaccion),103),
'FechaVto'=Convert (varchar (10),(Select FechaVencimiento From VCompra Where IDtransaccion= DT.IDTransaccion),103),
'Condicion'=(Select Condicion From VCompra Where IDtransaccion= DT.IDTransaccion),
'IDMoneda'=(Select IDMoneda From VCompra Where IDtransaccion= DT.IDTransaccion),
'Cotizacion'=isnull(C.Cotizacion,1),
--Total
DT.Total,
DT.TotalImpuesto,
DT.TotalDiscriminado,
DT.TotalDescuento,
'TotalSinDescuento'=DT.Total - DT.TotalDescuento,
'TotalSinImpuesto'=DT.Total - DT.TotalImpuesto,
'Caja'=IsNull(DT.Caja, 'False'),
DT.CantidadCaja,
'Cajas'=IsNull((DT.Cantidad / P.UnidadPorCaja), 0),
C.Fecha,
'Fec'= CONVERT (varchar (50),C.Fecha , 10),
C.FechaEntrada,
C.FechaVencimiento,

--DT.FactorCosto

--Clasificacion Producto
P.IDLinea,
'Linea'=ISNULL(L.Descripcion, ''),
P.IDSubLinea,
'SubLinea'=ISNULL(SL.Descripcion, ''),
P.IDSubLinea2,
'SubLinea2'=ISNULL(SL2.Descripcion, ''),
P.IDTipoProducto,
'TipoProducto'=ISNULL(TP.Descripcion, ''),
'CuentaContableProveedorTipoProducto'=ISnull(TP.CuentaContableProveedor,''),
'CuentaContableCompra'=ISnull(P.CodigoCuentaCompra,''),
P.IDMarca,
'Marca'=ISNULL(M.Descripcion, ''),
'IDTipoProveedor'=(Select IDTipoProveedor from VCompra where IDTransaccion = C.IDTransaccion),
'TipoProveedor'=(Select TipoProveedor from VCompra where IDTransaccion = C.IDTransaccion),
P.IDUnidadMedida,
P.UnidadMedida,
T.IDUsuario,
'FechaTransaccion'=T.Fecha,
'ControlarExistencia'=isnull(P.ControlarExistencia,'False')



From DetalleCompra DT
Join Compra C On DT.IDTransaccion=C.IDTransaccion
Join VProducto P On DT.IDProducto=P.ID
Join Deposito D On DT.IDDeposito=D.ID
Join Impuesto I On DT.IDImpuesto=I.ID
Left Outer Join Linea L on L.ID=P.IDLinea
Left Outer Join SubLinea SL on SL.ID=P.IDSubLinea
Left Outer Join SubLinea2 SL2 on SL2.ID=P.IDSubLinea2 
Left Outer Join TipoProducto TP on TP.ID=P.IDTipoProducto
Left Outer Join Marca M on M.ID=P.IDMarca
Left Outer Join Categoria K on P.IDCategoria=K.ID
Left Outer Join Transaccion T on DT.IDTransaccion = T.ID
left outer join DepartamentoEmpresa DE on C.IDDepartamentoEmpresa =DE.ID
















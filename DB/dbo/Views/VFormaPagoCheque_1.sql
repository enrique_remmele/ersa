﻿CREATE view [dbo].[VFormaPagoCheque]
as


--Cheque Cliente Cobranza Contado
Select
FP.IDTransaccion,
FP.ID,
--V.IDOperacion,
'FormaPago'='Cheque',
--'Moneda'=V.Moneda,
'ImporteMoneda'=FP.ImporteCheque,
'Cambio'=V.Cotizacion,
--'Banco'=V.Banco,
'Comprobante'=V.NroCheque,
'FechaEmision'=convert(varchar(50),CCO.Fecha,3),
'Vencimiento'=convert(varchar(50),V.FechaVencimiento,3),
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE' End),
--'Tipo'=V.Tipo,
'Importe'=FP.Importe,
FP.CancelarCheque,
'Restar'=0,
'Cobranza'='CO-'+''+convert(varchar(50),CCO.Numero)


From FormaPago FP
Join ChequeCliente V On FP.IDTransaccion=V.IDTransaccion
Join CobranzaContado CCO On FP.IDTransaccion=CCO.IDTransaccion 

Union All

--Cheque Cliente Cobranza Crédito
Select
V.IDTransaccion,
FP.ID,
--V.IDOperacion,
'FormaPago'='Cheque',
--'Moneda'=V.Moneda,
'ImporteMoneda'=FP.ImporteCheque,
'Cambio'=V.Cotizacion,
--'Banco'=V.Banco,
'Comprobante'=V.NroCheque,
'FechaEmision'=convert(varchar(50),CCR.FechaEmision,3),
'Vencimiento'=convert(varchar(50),V.FechaVencimiento,3),
'TipoComprobante'=(Case When (V.Diferido)='True' Then 'CHEQUE DIFERIDO' Else 'CHEQUE' End),
--'Tipo'=V.Tipo,
'Importe'=FP.Importe,
FP.CancelarCheque,
'Restar'=0,
'Cobranza'='CR-'+''+convert(varchar(50),CCR.Numero)


From FormaPago FP
Join ChequeCliente V On FP.IDTransaccionCheque=V.IDTransaccion
Join CobranzaCredito CCR On FP.IDTransaccion=CCR.IDTransaccion 






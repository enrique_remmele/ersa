﻿CREATE View [dbo].[VDetalleEfectivo]
As
Select 
DE.IDTransaccion,
DE.ID,
DE.IDTipoComprobante,
'TipoComprobante'=TC.Codigo,
DE.Comprobante,
DE.Fecha,
'Fec'=CONVERT(varchar(50), DE.Fecha, 6),

--Las descripciones en 'TIPO' estan relacionadas al programa
--Si se quiere cambiar estas descripciones (CREDITO-CONTADO-GASTOS) se deberian cambiar tambien
--en el software (programacion)
'Tipo'=(Case When (DE.VentasCredito) > 0 Then 'CREDITO' Else (Case When (DE.VentasContado) > 0 Then 'CONTADO' Else (Case When (DE.Gastos) > 0 Then 'GASTOS' Else '---' End) End) End),
'Importe'=(Case When (DE.VentasCredito) > 0 Then DE.VentasCredito Else (Case When (DE.VentasContado) > 0 Then DE.VentasContado Else (Case When (DE.Gastos) > 0 Then DE.Gastos Else 0 End) End) End),
DE.VentasCredito,
DE.VentasContado,
DE.Gastos,
DE.IDMoneda,
'Moneda'=M.Referencia,
DE.ImporteMoneda,
DE.Cotizacion,
DE.Observacion

From DetalleEfectivo DE
Join TipoComprobante TC On DE.IDTipoComprobante=TC.ID
Join Moneda M On DE.IDMoneda=M.ID

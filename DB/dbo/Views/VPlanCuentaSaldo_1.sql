﻿CREATE View [dbo].[VPlanCuentaSaldo]
as
Select 
C.IDPlanCuenta,
C.PlanCuenta,
PCS.Año,
PCS.Mes,
'M'=dbo.FMes(PCS.Mes),
'Codigo'=PCS.Cuenta,
'Denominacion'=IsNull((Select Descripcion From VCuentaContable CC Where CC.Codigo=PCS.Cuenta And IDPlancuenta = C.IDPlancuenta),'---'),

--Padre
'CodigoPadre'=(select CodigoPadre from vcuentacontable where IDPlancuenta = C.IDPlancuenta and Codigo=PCS.Cuenta),
'DenominacionPadre'=(Select Descripcion from CuentaContable where IDPlancuenta = C.IDPlancuenta and Codigo=(select CodigoPadre from vcuentacontable where IDPlancuenta = C.IDPlancuenta and Codigo=PCS.Cuenta)),

--Abuelo
'CodigoAbuelo'=(select CodigoPadre from vcuentacontable where IDPlancuenta = C.IDPlancuenta and Codigo=(select CodigoPadre from vcuentacontable where IDPlancuenta = C.IDPlancuenta and Codigo=PCS.Cuenta)),
'DenominacionAbuelo'=(Select Descripcion from CuentaContable where IDPlancuenta = C.IDPlancuenta and Codigo=(select CodigoPadre from vcuentacontable where IDPlancuenta = C.IDPlancuenta and Codigo=(select CodigoPadre from vcuentacontable where IDPlancuenta = C.IDPlancuenta and Codigo=PCS.Cuenta))),

--Totales
'Debito'=PCS.Debito,
'Credito'=PCS.Credito,
'Saldo'=PCS.Saldo,

--Centro de Costo
C.IDCentroCosto,
'CentroCosto'=IsNull(CC.Descripcion, '---'),
'CodigoCentroCosto'=IsNull(CC.Codigo, '---'),

--Unidad de Negocio
--'IDUnidadNegocio'=IsNull((Select IDUnidadNegocio From VCuentaContable CC Where CC.Codigo=PCS.Cuenta And CC.PlanCuentaTitular='True'),0)
IDUnidadNegocio,
UnidadNegocio
From PlanCuentaSaldo PCS
Left outer join vCuentaContable C on PCS.Cuenta = C.Codigo
Left outer Join CentroCosto CC On C.IDCentroCosto=CC.ID






﻿Create view vCompraLogistica 
as
select 
CL.IDTransaccion,
CL.IDCamion,
CL.IDProducto,
CL.Cantidad
from CompraLogistica CL
join Camion C on CL.IDCamion = C.ID
join Producto P on CL.IDProducto = P.ID
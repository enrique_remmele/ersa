﻿Create view vSerieDetalleGraficoContable
as 
select 
SDGC.IDGrafico,
SDGC.IDSerie,
SDGC.Codigo,
'CuentaContable'= CC.CuentaContable,
SGC.Año
From SerieDetalleGraficoContable SDGC
join SerieGraficoContable SGC on SDGC.IDSerie = SGC.ID
JOin VCuentaContable CC on SDGC.Codigo = CC.Codigo

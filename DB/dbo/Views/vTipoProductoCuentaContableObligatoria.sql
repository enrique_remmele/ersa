﻿Create View vTipoProductoCuentaContableObligatoria
as 
select 
TPC.IDTipoPRoducto,
'TipoProducto'=TP.Descripcion,
TPC.CuentaContableVenta,
TPC.CuentaContableCompra,
TPC.CuentaContableDeudor,
TPC.CuentaContableCosto,
TPC.IDUsuarioUltimaModificacion,
'UsuarioUltimaModificacion'= U.Usuario,
TPC.FechaUltimaModificacion


from TipoProductoCuentaContableObligatoria TPC
join TipoProducto TP on TPC.IDTipoProducto = TP.ID
join usuario U on TPC.IDUsuarioUltimaModificacion = U.ID

﻿CREATE View [dbo].[VMarcaPresentacion]
As
Select
MP.*,
'TipoProducto'=TP.Descripcion,
'Linea'=L.Descripcion,
'SubLinea'=SL.Descripcion,
'SubLinea2'=SL2.Descripcion,
'Marca'=M.Descripcion,
'Presentacion'=P.Descripcion

From MarcaPresentacion MP
Join TipoProducto TP On MP.IDTipoProducto=TP.ID
Join Linea L On MP.IDLinea=L.ID
JOin SubLinea SL On MP.IDSubLinea=SL.ID
JOin SubLinea2 SL2 On MP.IDSubLinea2=SL2.ID
JOin Marca M On MP.IDMarca=M.ID
Join Presentacion P On MP.IDPresentacion=P.ID

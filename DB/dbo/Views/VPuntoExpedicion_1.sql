﻿CREATE View [dbo].[VPuntoExpedicion]
As

Select 
PE.ID,
PE.IDTipoComprobante,
'TipoComprobante'=TC.Codigo,
PE.IDSucursal,
'Sucursal'=S.Codigo,
'Suc'=S.Descripcion,
S.IDCiudad,
'Ciudad'=S.CodigoCiudad,
'CiudadDesc.'= s.Descripcion, 
'ReferenciaSucursal'=S.Referencia,
'ReferenciaPunto'=PE.Referencia,
PE.Descripcion,
PE.Timbrado,
PE.Vencimiento,
PE.NumeracionDesde,
PE.NumeracionHasta,
PE.CantidadPorTalonario,
'Talonarios'= CEILING((PE.NumeracionHasta - PE.NumeracionDesde)  / (Case When(PE.CantidadPorTalonario) = 0 Then 1 Else PE.CantidadPorTalonario End)),
'UltimoNumeroExpedido'=IsNull(PE.UltimoNumero, 0),
'ProximoNumero'=dbo.FNroComprobante(PE.ID),
'ProximoComprobante'=S.Referencia + '-' + PE.Referencia + '-' + Convert(varchar(50), dbo.FNroComprobante(PE.ID)),
'TalonarioActual'=Floor(((dbo.FNroComprobante(PE.ID) - PE.NumeracionDesde)/PE.CantidadPorTalonario) + 1),
'Saldo'=(PE.NumeracionHasta - dbo.FNroComprobante(PE.ID)) + 1, 
PE.Estado,

--OPERACION
TC.IDOperacion,
'Operacion'=O.Descripcion,
'CodigoOperacion'=O.Codigo,

'VentaDirecta'=Isnull(PE.VentaDirecta,0),
PE.IDDepositoVentaDirecta,
'DepositoVentaDirecta'=isnull(D.Descripcion,'')

From PuntoExpedicion PE
Join TipoComprobante TC On PE.IDTipoComprobante=TC.ID
Join Operacion O On TC.IDOperacion=O.ID
Join VSucursal S On PE.IDSucursal=S.ID
left outer join Deposito D on PE.IDDepositoVentaDirecta = D.ID








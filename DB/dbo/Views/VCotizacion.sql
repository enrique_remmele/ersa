﻿

CREATE View [dbo].[VCotizacion]
As
Select 
CO.ID,
CO.Fecha,
CO.IDMoneda,
CO.Cotizacion,
'Moneda' = MO.Descripcion,
MO.Referencia
From Cotizacion CO
join Moneda MO on CO.IDMoneda = MO.ID


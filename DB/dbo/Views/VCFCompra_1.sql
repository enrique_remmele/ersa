﻿

CREATE View [dbo].[VCFCompra]
As
Select
V.*,
CFV.BuscarEnProveedor,
CFV.BuscarEnProducto,

--Tipo Cuenta Fija
CFV.IDTipoCuentaFija,
'TipoCuentaFija'=TC.Tipo,
'CuentaFija'=TC.Descripcion,
TC.IDImpuesto,

--Tipo de Cuenta
TC.Campo,
TC.IncluirDescuento,
TC.IncluirImpuesto

From VCF V
Join CFCompra CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join VTipoCuentaFija TC On CFV.IDTipoCuentaFija=TC.ID



﻿Create View [dbo].[VEquipoConteoUsuario]
As
Select 
ECU.IDEquipo,
ECU.IDUsuario,
'Equipo'=EC.Descripcion,
EC.IDZonaDeposito,
EC.ZonaDeposito,
'Usuario'=U.Usuario,
'User'=U.Identificador
 
From EquipoConteoUsuario ECU
Join VEquipoConteo EC On ECU.IDEquipo = EC.ID
Join Usuario U On ECU.IDUsuario = U.ID

﻿
CREATE View [dbo].[VMovimientoBancario]

As

--CARLOS 26-02-2014

--DEPOSITO

Select
IDCuentaBancaria,
'Fec'=CONVERT (varchar (50),fecha,6),
'Movimiento'='DEPOSITO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'=Suc,
'Debito'=Total,
'Credito'=0,
'Saldo'=Total,
'Observacion'=Observacion,
'A la Orden'='',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha 

From VDepositoBancario

Union All

--DEBITO

Select
IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='DEBITO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'= Suc ,
'Debito'=Total,
'Credito'=0,
'Saldo'=Total,
'Observacion'=Observacion,
'A la Orden'='',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha 
 
From VDebitoCreditoBancario
Where Debito = 'True'

Union All

--CREDITO
Select
IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='CREDITO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'=Suc ,
'Debito'=0,
'Credito'=Total,
'Saldo'=Total,
'Observacion'=Observacion,
'A la Orden'='',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha  

From VDebitoCreditoBancario
Where Credito = 'True'

Union All

--ORDEN DE PAGO
Select
IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='PAGO',
'Operacion'=Numero,
'Comprobante'=(Case When Diferido ='True'Then 'DIF'+'  '+CONVERT(varchar(50),NroCheque) When Diferido='False'Then 'CHQ'+'  '+CONVERT(varchar(50),NroCheque) Else '' End) ,
'Sucursal'= Suc ,
'Debito'=0,
'Credito'=ImporteMoneda,
'Saldo'=TotalImporteMoneda,
'Observacion'=Observacion,
'A la Orden'=ALaOrden,
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha 
 
From VOrdenPago
Where Cheque = 'True'And Anulado ='False'

Union All

--Descuento Cheque
Select
IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50),fecha,6),
'Movimiento'='DEPOSITO',
'Operacion'=Numero,
'Comprobante'=TipoComprobante + ' ' + Convert(varchar(50), Comprobante),
'Sucursal'= Suc ,
'Debito'=TotalAcreditado,
'Credito'=0,
'Saldo'=TotalAcreditado,
'Observacion'=Observacion,
'A la Orden'='',
'Conciliado'=Case When(Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= IDTransaccion,
'Fecha'=Fecha 
 
From VDescuentoCheque 

Union All

--Cheque Rechazado
Select
V.IDCuentaBancaria,
'Fecha'=CONVERT (varchar (50), V.fecha,6),
'Movimiento'='CHEQUE RECHAZADO',
'Operacion'=V.Numero,
--'Operacion'=CHQ.Numero,
'Comprobante'= CHQ.CodigoTipo + ' ' + Convert(varchar(50), CHQ.NroCheque),
'Sucursal'= Suc ,
'Debito'=0,
'Credito'=CHQ.Importe,
'Saldo'=CHQ.Importe,
'Observacion'=V.MotivoRechazo,
'A la Orden'=CHQ.Librador,
'Conciliado'=Case When(V.Conciliado) = 'True' Then 'Si' Else '---' End,
'IDTransaccion'= V.IDTransaccion,
'Fecha'=V.Fecha 
 
From VChequeClienteRechazado V
Join VChequeCliente CHQ On V.IDTransaccionCheque=CHQ.IDTransaccion

Where V.Anulado='False'



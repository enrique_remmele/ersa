﻿
CREATE View [dbo].[VCuentaBancaria]
As
Select 
CB.ID,
CB.IDBanco,
'Banco'=B.Descripcion,
'Referencia'=B.Referencia,
'IDMoneda'=M.ID,

'Moneda'=M.Descripcion,
'Mon'=M.Referencia,
CB.CuentaBancaria,
'Cuenta'=CB.CuentaBancaria,
CB.Nombre,
'Titulares'=IsNull(CB.Titulares, ''),
'Firma1'=IsNull(CB.Firma1, ''),
'Firma2'=IsNull(CB.Firma2, ''),
'Firma3'=IsNull(CB.Firma3, ''),

CB.CodigoCuentaContable,
'CuentaContable'=IsNull(CC.Descripcion, ''),
'Codigo CC'=IsNull(CC.Codigo, ''),
CC.Alias,

CB.IDTipoCuentaBancaria,
'TipoCuentaBancaria'=TC.Descripcion,
CB.Apertura,
'Fec Apertura'=CONVERT(varchar(50), CB.Apertura, 6),
CB.Estado,
CB.Observacion,
CB.FormatoAlDia,
CB.FormatoDiferido,
'Descripcion'=B.Referencia + ' ' + CB.CuentaBancaria + ' - ' + M.Referencia,
M.Decimales

From CuentaBancaria CB
Join Banco B On CB.IDBanco=B.ID
Join Moneda M On CB.IDMoneda=M.ID
Left Outer Join VCuentaContable CC On CB.CodigoCuentaContable=CC.Codigo
Join TipoCuentaBancaria TC On CB.IDTipoCuentaBancaria=TC.ID









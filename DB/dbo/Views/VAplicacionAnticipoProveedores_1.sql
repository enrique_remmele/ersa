﻿


CREATE View [dbo].[VAplicacionAnticipoProveedores]

As

Select 
AP.IdTransaccion,
AP.Fecha , 
AP.Numero ,
AP.Procesado ,
AP.Observacion,
AP.TotalEgreso,
AP.TotalOrdenPago,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
AP.NroComprobante,
'Comprobante'=AP.NroComprobante,
AP.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
S.IDCiudad,
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
'IDMoneda'= AP.IDMoneda,
'Moneda'=M2.Referencia,
'IDProveedor'=IsNull(AP.IDProveedor, 0),
'Proveedor'=ISNull(P.RazonSocial, ''),
'RUC'=IsNull(P.RUC, ''),
'Referencia'=P.Referencia,
P.IDTipoProveedor,


--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
AP.Anulado,
'Estado'=Case When AP.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=AP.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=AP.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=AP.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=AP.IDTransaccion)
From AplicacionAnticipoProveedores AP

Join Transaccion T On AP.IDTransaccion=T.ID
Join TipoComprobante TC On AP.IDTipoComprobante=TC.ID
Join Sucursal S On AP.IDSucursal=S.ID
Left Outer Join Proveedor P On AP.IDProveedor=P.ID
Left Outer Join Moneda M2 On AP.IDMoneda=M2.ID


















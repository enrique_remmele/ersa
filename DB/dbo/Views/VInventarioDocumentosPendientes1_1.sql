﻿CREATE view [dbo].[VInventarioDocumentosPendientes1]
 as 

-- Daniel 21/01/2015 se agrega Idtransaccion para realacion con vDetlleVenta y poder filtrar por IdTipoproducto
Select
V.IDCliente, 
C.RazonSocial as Cliente ,
V.Direccion,
V.Referencia ,
V.IDTipoCliente,
V.IDSucursalCliente,
V.SucursalCliente,
V.IDMoneda, 
v.IDZonaVenta, 
V.Telefono,
V.RUC , 
V.Condicion ,
V.IDTipoComprobante,
V.Comprobante ,
V.NroComprobante,
'Vendedor'=ISNULL(V.Vendedor,'-----') ,
'Cobrador'=ISNULL(V.Cobrador,'-----'),
V.IDSucursal,
V.Sucursal,
V.IDCiudad,
V.Ciudad ,
V.IDDeposito, 
V.Fecha,
V.IDEstado,
V.Estado,
V.IDVendedor,
V.IDCobrador, 
'Fecha Emision'=FechaEmision,
'Fecha Vencimiento'= ISNULL(V.[Fec. Venc.], V.Fec),
'Ven.'= CONVERT (varchar (50), FechaVencimiento ,6),
'Plazo'= DATEDIFF (D, FechaEmision, FechaVencimiento ),
'Ultimo Pago'=(select MAX(C.FechaCobranza) from VVentaDetalleCobranza C Where C.IDTransaccion = V.IDTransaccion),
'Dias Vencidos'= DATEDIFF (D,   V.FechaVencimiento, GETDATE() ),
V.Total ,
V.Saldo,
V.Cancelado,
V.Anulado,
'Documento'='Venta',
'IVA 5%'=IsNull((Select Total From DetalleImpuesto DI Where DI.IDTransaccion=V.IDTransaccion And DI.IDImpuesto=2),0),
VLD.Chofer,
VLD.IDChofer,
--V.FechaVencimiento,
'FechaVencimiento'= (Case When(V.Condicion) = 'CONT' Then V.FechaEmision Else V.FechaVencimiento End),
'ChequeDiferido'='False',
v.Idtransaccion,
'FormaPagoFactura'=(Select referencia from FormaPagoFactura where ID = V.IDFormaPagoFactura)

From VVenta V
Left Outer Join VVentaLoteDistribucion VLD on V.IDTransaccion=VLD.IDTransaccionVenta
join cliente C on C.Id = V.Idcliente
Where V.Cancelado = 'False' And V.CancelarAutomatico = 'False'
 
Union All
 
Select
NC.IDCliente,  
NC.Cliente,
NC.Direccion,
NC.Referencia,
NC.IDTipoCliente,
'IDSucursalCliente'=NULL,
'SucursalCliente'=NULL,
NC.IDMoneda, 
NC.IDZonaVenta,
NC.Telefono,
NC.RUC,
--NC.Condicion  ,
'Condicion'='NC',
NC.IDTipoComprobante ,
NC.Comprobante,
NC.NroComprobante,
'Vendedor'= '-----', 
'Cobrador'= '-----',
NC.IDSucursal,
NC.Sucursal,
NC.IDCiudad,
NC.Ciudad,
NC.IDDeposito, 
NC.Fecha,
NC.IDEstado,
NC.Estado,
'IDVendedor'= '',
'IDCobrador'='', 
'Fecha Emision'= NC.Fecha,
'Fecha Vencimiento'= NC.Fec ,
'Ven.'= NC.Fec,
'Plazo'= '',
'Ultimo Pago'=NULL,
'Dias Vencidos'= '',
NC.Total,
NC .Saldo * -1,  
'Cancelado'= (Case When(NC.Aplicar) = 'True' Then (Case When(NC.Saldo) = 0 Then 'True' Else 'False' End) Else 'True' End),
NC.Anulado,
'Documento'='Nota de Credito',
'IVA 5%'=IsNull((Select Total From DetalleImpuesto DI Where DI.IDTransaccion=NC.IDTransaccion And DI.IDImpuesto=2),0),
'',
0,
NC.Fecha,
'ChequeDiferido'='False',
NC.Idtransaccion,
'FormaPagoFactura'='--'

From VNotaCredito NC
Where (Case When(NC.Aplicar) = 'True' 
	   Then (Case When(NC.Saldo) > 0 Then 'True' Else 'False' End) 
	   Else 'False' End) = 'True' 
Union All

Select 
ND.IDCliente,
ND.Cliente,
ND.Direccion,
ND.Referencia,
ND.IDTipoCliente,
'IDSucursalCliente'=NULL,
'SucursalCliente'=NULL,
ND.IDMoneda, 
ND.IDZonaVenta,
ND.Telefono,
ND.RUC,
ND.Condicion  ,
ND.IDTipoComprobante ,
ND.Comprobante,
ND.NroComprobante,
'Vendedor'= '-----', 
'Cobrador'= '-----',
ND.IDSucursal,
ND.Sucursal,
ND.IDCiudad,
ND.Ciudad,
ND.IDDeposito, 
ND.Fecha,
ND.IDEstado,
ND.Estado,
'IDVendedor'= '',
'IDCobrador'='', 
'Fecha Emision'=ND.Fecha,
'FechaVencimiento'= ND.Fec,
'Ven.'=  ND.Fec,
'Plazo'= '',
'Ultimo Pago'=NULL,
'Dias Vencidos'= '',
ND.Total,
ND.Saldo,
'Cancelado'= (Case When(ND.Aplicar) = 'True' Then (Case When(ND.Saldo) = 0 Then 'True' Else 'False' End) Else 'True' End),
ND.Anulado,
'Documento'='Nota de Debito',
'IVA 5%'=IsNull((Select Total From DetalleImpuesto DI Where DI.IDTransaccion=ND.IDTransaccion And DI.IDImpuesto=2),0), 
'',
0,
ND.Fecha,
'ChequeDiferido'='False',
ND.Idtransaccion,
'FormaPagoFactura'='--'

From VNotaDebito ND 
Where (Case When(ND.Aplicar) = 'True' 
Then (Case When(ND.Saldo) = 0 Then 'True' Else 'False' End) Else 'False' End) = 'True'  

--Union All

----Cheque Diferidos pendientes
--Select
--V.IDCliente, 
--V.Cliente ,
--C.Direccion,
--C.Referencia ,
--C.IDTipoCliente,
--'IDSucursalCliente'=0,
--'SucursalCliente'='',
--V.IDMoneda, 
--C.IDZonaVenta, 
--C.Telefono,
--C.RUC , 
--'Condicion'='DIF - ' + V.CodigoBanco,
--'IDTipoComprobante'=0,
--'Comprobante'=V.NroCheque,
--'NroComprobante'=V.NroCheque,
--'Vendedor'='-----',
--'Cobrador'='-----',
--V.IDSucursal,
--V.Sucursal,
--S.IDCiudad,
--V.Ciudad ,
--'IDDeposito'=0, 
--V.Fecha,
--'IDEstado'=0,
--'Estado'='',
--'IDVendedor'=0,
--'IDCobrador'=0, 
--'Fecha Emision'=Fecha,
--'Fecha Vencimiento'= ISNULL(V.Vencimiento, V.Fec),
--'Ven.'= CONVERT (varchar (50), V.FechaVencimiento ,6),
--'Plazo'= DATEDIFF (D, V.Fecha, V.FechaVencimiento ),
--'Ultimo Pago'=NULL,
--'Dias Vencidos'= DATEDIFF (D,   V.FechaVencimiento, GETDATE() ),
--V.Total,
----El saldo del cheque va a ser siempre el total
----Queda sin saldo al depositar unicamente, el saldo de cobranza no quieren saber.
--'Saldo'=V.Total,
----V.Saldo,
--'Cancelado'='False',--V.Cancelado,
--'Anulado'='False',
--'Documento'='Cheque Diferido',
--'IVA 5%'=0,
--'Chofer'='',
--'IDChofer'=0,
--V.FechaVencimiento,
--'ChequeDiferido'='True',
--V.Idtransaccion,
--'FormaPagoFactura'='--'

--From VChequeCliente V
--Join VCliente C On V.IDCliente=C.ID
--Join VSucursal S On V.IDSucursal=S.ID
--Where V.Cartera='True' And V.Diferido='True'

UNion all

--Cheque Diferidos pendientes
Select
V.IDCliente, 
V.Cliente ,
C.Direccion,
C.Referencia ,
C.IDTipoCliente,
'IDSucursalCliente'=0,
'SucursalCliente'='',
V.IDMoneda, 
C.IDZonaVenta, 
C.Telefono,
C.RUC , 
'Condicion'='RECH - ' + V.CodigoBanco,
'IDTipoComprobante'=0,
'Comprobante'=V.NroCheque,
'NroComprobante'=V.NroCheque,
'Vendedor'='-----',
'Cobrador'='-----',
V.IDSucursal,
V.Sucursal,
S.IDCiudad,
V.Ciudad ,
'IDDeposito'=0, 
V.Fecha,
'IDEstado'=0,
'Estado'='',
'IDVendedor'=0,
'IDCobrador'=0, 
'Fecha Emision'=Fecha,
'Fecha Vencimiento'= ISNULL(V.Vencimiento, V.Fec),
'Ven.'= CONVERT (varchar (50), V.FechaVencimiento ,6),
'Plazo'= DATEDIFF (D, V.Fecha, V.FechaVencimiento ),
'Ultimo Pago'=NULL,
'Dias Vencidos'= DATEDIFF (D,   V.FechaVencimiento, GETDATE() ),
V.Total,
--El saldo del cheque va a ser siempre el total
--Queda sin saldo al depositar unicamente, el saldo de cobranza no quieren saber.
'Saldo'=V.Total,
--V.Saldo,
'Cancelado'='False',--V.Cancelado,
'Anulado'='False',
'Documento'='Cheque Rechazado',
'IVA 5%'=0,
'Chofer'='',
'IDChofer'=0,
V.FechaVencimiento,
'ChequeDiferido'='True',
V.Idtransaccion,
'FormaPagoFactura'='--'

From VChequeCliente V
Join VCliente C On V.IDCliente=C.ID
Join VSucursal S On V.IDSucursal=S.ID
Where V.Rechazado='True'
and V.IDTransaccion not in (select DPC.IDTransaccionChequeCliente from DetallePagoChequeCliente DPC
join PagoChequeCliente PCC on PCC.IDtransaccion = DPC.IdTransaccionPagoChequeCliente 
and PCC.Anulado = 0)







﻿
CREATE View [dbo].[VKardexInforme] 


As 

---Iniciales
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= '---',
'TipoComprobante'='---',
'Numero'= '---',

K.IDTransaccion,

--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,


--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
where IDTransaccion = 0

union all

--Movimientos
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Concat((Case When M.MovimientoCombustible=1 then 'CCO' else
				(Case When M.MovimientoStock=1 Then 'MOV' else
				(Case WHen M.DescargaStock=1 Then 'DST' else
				(Case WHen M.MovimientoMateriaPrima=1 Then 'MMP' else
				'DCOM'end)end)end)end),' ', M.Numero),

K.IDTransaccion,

--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,


--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Movimiento M on M.IDTransaccion = K.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
where CK.IDTipoComprobante NOT IN (select IDTipoComprobanteProduccion from configuraciones)
and CK.IDTipoComprobante not in (Select  id from TipoCOmprobante where comprobanteconsumo = 1)
and M.Anulado = 0

union all

--Ticket Bascula
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= cONCAT(Convert(varchar(50),C.Numero),'- Comp. ', Convert(varchar(50),C.NroComprobante)) ,

K.IDTransaccion,

--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=isnull(K.CantidadEntrada,0),
'CostoUnitarioCompraProduccion'=isnull(K.CostoUnitarioEntrada,0),
'TotalCompraProduccion'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Entrada
'CantidadEntrada'=0,
'CostoUnitarioEntrada'=0,
'TotalEntrada'=0,

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join TicketBascula C On K.IDTransaccion=C.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where C.Anulado = 0

union all


--Ticket Bascula
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= cONCAT(Convert(varchar(50),C.Numero),'- Comp. ', Convert(varchar(50),C.NroComprobante)) ,

K.IDTransaccion,

--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=isnull(K.CantidadEntrada,0),
'CostoUnitarioCompraProduccion'=isnull(K.CostoUnitarioEntrada,0),
'TotalCompraProduccion'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Entrada
'CantidadEntrada'=0,
'CostoUnitarioEntrada'=0,
'TotalEntrada'=0,

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join MacheoFacturaTicket C On K.IDTransaccion=C.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where C.Anulado = 0

union all



--Compra
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Convert(varchar(50),C.Numero),

K.IDTransaccion,

--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,
K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=isnull(K.CantidadEntrada,0),
'CostoUnitarioCompraProduccion'=isnull(K.CostoUnitarioEntrada,0),
'TotalCompraProduccion'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Entrada
'CantidadEntrada'=0,
'CostoUnitarioEntrada'=0,
'TotalEntrada'=0,

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Compra C On K.IDTransaccion=C.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID

union all

--Nota Credito Proveedor
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Convert(varchar(50),C.Numero),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=isnull(K.CantidadEntrada,0),
'CostoUnitarioCompraProduccion'=isnull(K.CostoUnitarioEntrada,0),
'TotalCompraProduccion'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Entrada
'CantidadEntrada'=0,
'CostoUnitarioEntrada'=0,
'TotalEntrada'=0,

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join NotaCreditoProveedor C On K.IDTransaccion=C.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
AND C.Anulado = 0
union all

--Produccion
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Concat((Case When M.MovimientoCombustible=1 then 'CCO' else
				(Case When M.MovimientoStock=1 Then 'MOV' else
				(Case WHen M.DescargaStock=1 Then 'DST' else
				(Case WHen M.MovimientoMateriaPrima=1 Then 'MMP' else
				'DCOM'end)end)end)end),' ', M.Numero),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioCompraProduccion'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalCompraProduccion'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Entrada
'CantidadEntrada'=0,
'CostoUnitarioEntrada'=0,
'TotalEntrada'=0,

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Movimiento M On K.IDTransaccion=M.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
where CK.IDTipoComprobante = (select Top(1) IDTipoComprobanteProduccion from configuraciones)
And M.Anulado = 0

union all

--Venta
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Convert(varchar(50),V.NroComprobante),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,

--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=0,
'CostoUnitarioSalida'=0,
'TotalSalida'=0,

--Venta/Consumo
'CantidadVentaConsumo'=isnull(K.CantidadSalida,0),
'CostoUnitarioVentaConsumo'=isnull(K.CostoUnitarioSalida,0),
'TotalVentaConsumo'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Venta V On K.IDTransaccion=V.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto and CK.ID=K.ID
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where V.Anulado = 0
union all

--Nota Credito
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= PE.ReferenciaSucursal + '-' + PE.ReferenciaPunto + '-' + Convert(varchar(50), V.NroComprobante),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,

--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=0,
'CostoUnitarioSalida'=0,
'TotalSalida'=0,

--Venta/Consumo
'CantidadVentaConsumo'=isnull(K.CantidadSalida,0),
'CostoUnitarioVentaConsumo'=isnull(K.CostoUnitarioSalida,0),
'TotalVentaConsumo'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join NotaCredito V On K.IDTransaccion=V.IDTransaccion
Left Outer Join VPuntoExpedicion PE On V.IDPuntoExpedicion=PE.ID
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where V.Anulado = 0
union all


--Consumo
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Concat((Case When M.MovimientoCombustible=1 then 'CCO' else
				(Case When M.MovimientoStock=1 Then 'MOV' else
				(Case WHen M.DescargaStock=1 Then 'DST' else
				(Case WHen M.MovimientoMateriaPrima=1 Then 'MMP' else
				'DCOM'end)end)end)end),' ', M.Numero),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,

--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=0,
'CostoUnitarioSalida'=0,
'TotalSalida'=0,

--Venta/Consumo
'CantidadVentaConsumo'=isnull(K.CantidadSalida,0),
'CostoUnitarioVentaConsumo'=isnull(K.CostoUnitarioSalida,0),
'TotalVentaConsumo'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Movimiento M On K.IDTransaccion=M.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where CK.IDTipoComprobante in (Select id from TipoComprobante where comprobanteconsumo = 1)
and M.Anulado = 0

union all
--------------------------------------------####################################----------------------------------
--ANULADOS 

--Movimientos
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Concat((Case When M.MovimientoCombustible=1 then 'CCO' else
				(Case When M.MovimientoStock=1 Then 'MOV' else
				(Case WHen M.DescargaStock=1 Then 'DST' else
				(Case WHen M.MovimientoMateriaPrima=1 Then 'MMP' else
				'DCOM'end)end)end)end),' ', M.Numero),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,


--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Movimiento M on M.IDTransaccion = K.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
where CK.IDTipoComprobante NOT IN (select IDTipoComprobanteProduccion from configuraciones)
and CK.IDTipoComprobante not in (Select  id from TipoCOmprobante where comprobanteconsumo = 1)
And M.Anulado = 1

union all

--Ticket Bascula
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Convert(varchar(50),C.Numero),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=isnull(K.CantidadEntrada,0),
'CostoUnitarioCompraProduccion'=isnull(K.CostoUnitarioEntrada,0),
'TotalCompraProduccion'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Entrada
'CantidadEntrada'=0,
'CostoUnitarioEntrada'=0,
'TotalEntrada'=0,

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join TicketBascula C On K.IDTransaccion=C.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where C.Anulado = 1
union all

--Nota Credito Proveedor
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Convert(varchar(50),C.Numero),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=isnull(K.CantidadEntrada,0),
'CostoUnitarioCompraProduccion'=isnull(K.CostoUnitarioEntrada,0),
'TotalCompraProduccion'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Entrada
'CantidadEntrada'=0,
'CostoUnitarioEntrada'=0,
'TotalEntrada'=0,

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join NotaCreditoProveedor C On K.IDTransaccion=C.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where C.Anulado = 1
union all

--Produccion
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Concat((Case When M.MovimientoCombustible=1 then 'CCO' else
				(Case When M.MovimientoStock=1 Then 'MOV' else
				(Case WHen M.DescargaStock=1 Then 'DST' else
				(Case WHen M.MovimientoMateriaPrima=1 Then 'MMP' else
				'DCOM'end)end)end)end),' ', M.Numero),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioCompraProduccion'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalCompraProduccion'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Entrada
'CantidadEntrada'=0,
'CostoUnitarioEntrada'=0,
'TotalEntrada'=0,

--Salida
'CantidadSalida'=isnull(K.CantidadSalida,0),
'CostoUnitarioSalida'=isnull(K.CostoUnitarioSalida,0),
'TotalSalida'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Venta/Consumo
'CantidadVentaConsumo'=0,
'CostoUnitarioVentaConsumo'=0,
'TotalVentaConsumo'=0,

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Movimiento M On K.IDTransaccion=M.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
where CK.IDTipoComprobante = (select Top(1) IDTipoComprobanteProduccion from configuraciones)
And M.Anulado = 1
union all



--Venta
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Convert(varchar(50),V.NroComprobante),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,

--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=0,
'CostoUnitarioSalida'=0,
'TotalSalida'=0,

--Venta/Consumo
'CantidadVentaConsumo'=isnull(K.CantidadSalida,0),
'CostoUnitarioVentaConsumo'=isnull(K.CostoUnitarioSalida,0),
'TotalVentaConsumo'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Venta V On K.IDTransaccion=V.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where V.Anulado = 1

union all

--Nota Credito
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= PE.ReferenciaSucursal + '-' + PE.ReferenciaPunto + '-' + Convert(varchar(50), V.NroComprobante),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,

--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=0,
'CostoUnitarioSalida'=0,
'TotalSalida'=0,

--Venta/Consumo
'CantidadVentaConsumo'=isnull(K.CantidadSalida,0),
'CostoUnitarioVentaConsumo'=isnull(K.CostoUnitarioSalida,0),
'TotalVentaConsumo'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join NotaCredito V On K.IDTransaccion=V.IDTransaccion
Left Outer Join VPuntoExpedicion PE On V.IDPuntoExpedicion=PE.ID
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where V.Anulado = 1


union all


--Consumo
Select

'IDKardex'=K.IDKardex,
'Indice'=IsNull(K.Indice,0),
K.Fecha,

--Transaccion
'Operacion'= O.Descripcion,
'TipoComprobante'=TC.Descripcion,
'Numero'= Concat((Case When M.MovimientoCombustible=1 then 'CCO' else
				(Case When M.MovimientoStock=1 Then 'MOV' else
				(Case WHen M.DescargaStock=1 Then 'DST' else
				(Case WHen M.MovimientoMateriaPrima=1 Then 'MMP' else
				'DCOM'end)end)end)end),' ', M.Numero),

K.IDTransaccion,
--Producto
K.IDProducto,
P.Referencia,
'Producto'=P.Descripcion,
'TipoProducto'=TP.Descripcion,
P.IDTipoProducto,
'Linea'=L.Descripcion,
P.IDLinea,
'SubLinea'=SL.Descripcion,
P.IDSubLinea,
'SubLinea2'=SL2.Descripcion,
P.IDSubLinea2,
'PesoUnitario'=P.Peso,

K.ID,

K.Orden,
K.IDSucursal,
K.Comprobante,
'Observacion'=IsNull(K.Observacion, ''),

--Saldo Inicial
'CantidadInicial'=0,
'CostoPromedioPonderadoInicial'=0,
'TotalValorInicial'=0,

--Compra/Produccion
'CantidadCompraProduccion'=0,
'CostoUnitarioCompraProduccion'=0,
'TotalCompraProduccion'=0,

--Entrada
'CantidadEntrada'=ISNULL(K.CantidadEntrada,0),
'CostoUnitarioEntrada'=ISNULL(K.CostoUnitarioEntrada,0),
'TotalEntrada'=isnull(K.TotalEntrada,ISNULL(K.CantidadEntrada,0) * ISNULL(K.CostoUnitarioEntrada,0)),

--Salida
'CantidadSalida'=0,
'CostoUnitarioSalida'=0,
'TotalSalida'=0,

--Venta/Consumo
'CantidadVentaConsumo'=isnull(K.CantidadSalida,0),
'CostoUnitarioVentaConsumo'=isnull(K.CostoUnitarioSalida,0),
'TotalVentaConsumo'=isnull(K.TotalSalida,isnull(K.CantidadSalida,0) * isnull(K.CostoUnitarioSalida,0)),

--Saldos
K.CantidadSaldo,
K.CostoPromedio,
K.TotalSaldo,
K.CostoPromedioOperacion,

K.IDKardexAnterior,
K.DiferenciaTotal

From Kardex K
Join Producto P On K.IDProducto=P.ID
Join TipoProducto TP On P.IDTipoProducto = TP.ID
Left Outer Join Linea L on P.IDLinea = L.ID
Left Outer Join SubLinea SL on P.IDSubLinea = SL.ID
Left Outer Join SubLinea2 SL2 on P.IDSubLinea2 = SL2.ID
Left Outer Join Transaccion T On K.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Join Movimiento M On K.IDTransaccion=M.IDTransaccion
Join VCargaKardexInforme CK on CK.IDTransaccion = K.IDTransaccion and CK.IDProducto = K.IDProducto
Left Outer Join TipoComprobante TC on CK.IDTipoComprobante = TC.ID
Where CK.IDTipoComprobante in (Select id from TipoComprobante where comprobanteconsumo = 1)
and M.Anulado = 1






﻿


CREATE View [dbo].[VComisionProducto]
As
Select 
'IDListaVendedor'=V.ID,
'ListaVendedor'=V.Nombres ,
'IDProducto'=P.ID,
'Producto'=P.Descripcion,
'PorcentajeBase'= CP.PorcentajeBase,
'PorcentajeEsteProducto'=CP.PorcentajeEsteProducto,
'PorcentajeAnterior'=IsNull (CP.PorcentajeAnterior,0),
'Ult.Cambio'= IsNull(convert(varchar(50), CP.FechaUltimoCambio, 3), '---'),
-- IDentificador Usuario
Cp.IdUsuario,
'Usuario'= U.Identificador 




--Sucursal
--LP.IDSucursal,
--'Sucursal'=S.Descripcion

From Vendedor V
Join ComisionProducto CP On V.ID  = CP.IDVendedor
Join Producto P On CP.IDProducto  = P.ID
Join VUsuario U On  CP.IDUsuario = U.ID   







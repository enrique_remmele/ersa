﻿

CREATE View [dbo].[VCFMovimiento]
As
Select
V.*,
CFV.IDTipoOperacion,
'BuscarEnDeposito'=Isnull(CFV.BuscarEnDeposito,0),
'BuscarEnProducto'=isnull(CFV.BuscarEnProducto,(Case when TC.Tipo = 'PRODUCTO' then 1 else 0 end)),

--Tipo Cuenta Fija
CFV.IDTipoCuentaFija,
'TipoCuentaFija'=TC.Tipo,
'CuentaFija'=TC.Descripcion,
TC.IDImpuesto,

--Tipo de Movimiento
'Tipo Operacion'=TP.Descripcion,

--Tipo de Cuenta
TC.Campo,
TC.IncluirDescuento,
TC.IncluirImpuesto

From VCF V
Join CFMovimiento CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join TipoOperacion TP On CFV.IDTipoOperacion=TP.ID
Join VTipoCuentaFija TC On CFV.IDTipoCuentaFija=TC.ID


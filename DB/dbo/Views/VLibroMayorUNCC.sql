﻿Create view [dbo].[VLibroMayorUNCC]

as

Select

DA.IDTransaccion,
'Codigo'=convert(varchar(50),DA.Codigo),
DA.Descripcion,
a.Fecha, 
'Asiento'=A.Numero,
DA.NroComprobante,
'Comprobante'=A.TipoComprobante + ' ' + DA.NroComprobante,
DA.IDSucursal,
'Suc'=S.Descripcion,
'CodSucursal'=S.Codigo,
A.IDTipoComprobante,
--'Concepto'=Concat(A.Moneda,'-',A.Cotizacion,'-',(Case When(DA.Observacion) = '' Then (Case When(A.Detalle) = '' Then O.Descripcion Else A.Detalle End) Else concat(dbo.FComprobanteLibroMayor(A.IDTransaccion,O.FormName),'-',DA.Observacion) End)),
'Concepto'=Concat('',(Case When(DA.Observacion) = '' Then (Case When(A.Detalle) = '' Then O.Descripcion Else A.Detalle End) Else concat(dbo.FComprobanteLibroMayor(A.IDTransaccion,O.FormName),'-',DA.Observacion) End)),
DA.Debito,
DA.Credito,
'TipoCuenta'=dbo.FCuentaContableTipo(convert(varchar(50),DA.Codigo)),

--Operacion
T.IDOperacion,
'Operacion'=O.Descripcion,

--Centro de Costos
DA.IDCentroCosto,
'CentroCosto'=ISNull(CC.Descripcion, '---'),
'CodigoCentroCosto'=IsNull(CC.Codigo, ''),
--Unidad de negocio
DA.IDUnidadNegocio,
'UnidadNegocio'=ISNull(UN.Descripcion, '---'),
'CodigoUnidadNegocio'=IsNull(UN.Codigo, ''),
A.Usuario,
A.FechaTransaccion,
A.Cotizacion,
O.FormName,
A.IDMoneda,
A.Moneda

From VDetalleAsiento DA 
Join VAsiento A on A.IDTransaccion=DA.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Left Outer Join Operacion O On T.IDOperacion=O.ID
Left Outer JOin Sucursal S On DA.IDSucursal=S.ID
Left Outer Join CentroCosto CC On DA.IDCentroCosto=CC.ID
Left Outer Join UnidadNegocio UN On DA.IDCentroCosto=UN.ID









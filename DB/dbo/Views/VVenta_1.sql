﻿

CREATE View [dbo].[VVenta]
As
Select
V.IDTransaccion,

--Punto de Expedicion
V.IDPuntoExpedicion,
'PE'=PE.Descripcion,
PE.Ciudad,
PE.[CiudadDesc.],
PE.IDCiudad,
PE.IDSucursal,
PE.Sucursal,
PE.ReferenciaSucursal,
PE.ReferenciaPunto,
PE.Timbrado,

V.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
V.NroComprobante,
'Comprobante'=V.Comprobante,

--Cliente
V.IDCliente,
--'Cliente'=C.RazonSocial,
'Cliente'=Isnull(V.RazonSocial,C.RazonSocial),
'ReferenciaCliente'=C.Referencia,
--C.RUC,
'RUC'=Isnull(V.RUC,C.RUC),
V.IDSucursalCliente,
'SucursalCliente'=(Case When (V.IDSucursalCliente) IS Null Then 'MATRIZ' Else (Case When (V.IDSucursalCliente) = 0 Then 'MATRIZ' Else (Select Top(1) CS.Sucursal From ClienteSucursal CS Where V.IDCliente=CS.IDCliente And CS.ID=V.IDSucursalCliente) End) End),
'IDCiudadCliente'=C.IDCiudad,
--V.Direccion,
'Direccion'=(Case When (V.IDSucursalCliente) IS Null Then C.Direccion Else (Case When (V.IDSucursalCliente) = 0 Then C.Direccion Else (Select Top(1) CS.Direccion From ClienteSucursal CS Where V.IDCliente=CS.IDCliente And CS.ID=V.IDSucursalCliente) End) End),
'Telefono'=(Case When (C.Telefono) = '' Then 'X' Else C.Telefono End),
C.IDZonaVenta,
'ZonaVenta'=Isnull((Select Top(1) Descripcion from ZonaVenta where ID = C.IDZonaVenta),''),
C.IDTipoCliente,
'TipoCliente'=IsNull(TP.Descripcion, '---'),
C.Referencia,
C.IDCobrador ,
C.Cobrador ,
C.IDEstado,
C.Estado ,

--Opciones
V.IDDeposito,
'Deposito'=D.Descripcion,
V.FechaEmision,
'Fec'=CONVERT(varchar(50), V.FechaEmision, 3),
'Fecha'=V.FechaEmision,
V.Credito,
'CreditoContado'=(Case When (V.Credito) = 'True' Then 'CREDITO' Else 'CONTADO' End),
'Condicion'=(Case When (V.Credito) = 'True' Then 'CRED' Else 'CONT' End),
'Operacion'=convert(varchar(50),TC.Codigo) +'   '+convert(Varchar(50),V.NroComprobante)+'  '+ (Case When (V.Credito) = 'True' Then 'CRED' Else 'CONT' End),
V.FechaVencimiento,
'Fec. Venc.'= (Case When V.Credito='True' Then (CONVERT(varchar(50), V.FechaVencimiento, 3)) Else '---' End),
V.Observacion,
'IDMoneda'=isnull(V.IDMoneda,1),
'Moneda'=isnull(M.Referencia,(select Top(1) Referencia from moneda where id = 1)),
'DescripcionMoneda'=isnull(M.Descripcion,(select Top(1) Descripcion from moneda where id = 1)),
V.Cotizacion,
V.Anulado,

--Otros
V.IDListaPrecio,
'Lista de Precio'=IsNull((Select Top(1) LP.Descripcion From ListaPrecio LP Where LP.ID=V.IDListaPrecio), '---'),
'Descuento'=ISNULL(V.Descuento, 0),
V.IDVendedor,
'Vendedor'=ISNULL((Select Top(1) VE.Nombres From Vendedor VE Where VE.ID=V.IDVendedor), '---'),
V.IDPromotor,
'Promotor'=ISNULL((Select Top(1) PRO.Nombres From Promotor PRO Where PRO.ID=V.IDPromotor), '---'),
V.NroComprobanteRemision,
'Remision'=V.NroComprobanteRemision,
V.EsVentaSucursal,

--Totales
V.Total,
V.TotalImpuesto,
V.TotalDiscriminado,
V.TotalDescuento,
'TotalBruto'=isnull(V.Total,0) + isnull(V.TotalDescuento,0),

--Credito
V.Saldo,
'SaldoAConfirmar' =(select Top(1) dbo.FSaldoConPedidoNC(V.IDTransaccion)),
V.Cobrado,
V.Descontado,
V.Cancelado,
V.Acreditado,

--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select Top(1) U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select Top(1) U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Estado
V.Procesado,
'EstadoVenta'=dbo.FVentaEstado(V.IDTransaccion),

--Anulacion
'IDUsuarioAnulacion'=(Select Top(1) IsNull((Select Top(1) DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select Top(1) IsNull((Select Top(1) U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=V.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select Top(1) IsNull((Select Top(1) U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=V.IDTransaccion), '---')),
'FechaAnulacion'=(Select Top(1) DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=V.IDTransaccion),

--Mes y Año
'DiaP'=dbo.FFormatoDosDigitos(DatePart(dd,V.FechaEmision )),
'MesP'=dbo.FMes(DatePart(MM,V.FechaEmision )),
'Año'= Convert (Varchar (50),YEAR(V.FechaEmision)),
'Fec.Vencimiento'= Convert (Varchar (50),YEAR (V.FechaVencimiento))+Convert (Varchar(50),dbo.FFormatoDosDigitos (DATEPART (MM,V.FechaVencimiento)))+Convert (Varchar (50), dbo.FFormatoDosDigitos (DATEPART (DD,V.FechaVencimiento ))),

V.Autoriza,
'IDMotivo'=MAV.ID,
'MotivoAnulacion'=ISNULL(MAV.Descripcion,'---'),

--Direccion Alternativa
'DireccionAlternativa'=(Case When V.EsVentaSucursal='False' Then C.RazonSocial Else C.RazonSocial + ' - ' + (Case When (V.IDSucursalCliente) IS Null Then 'MATRIZ' Else (Case When (V.IDSucursalCliente) = 0 Then 'MATRIZ' Else (Select Top(1) CS.Sucursal From ClienteSucursal CS Where V.IDCliente=CS.IDCliente And CS.ID=V.IDSucursalCliente) End) End) End),

--Condicion Contado
'Contado'=(Case When (V.Credito) = 'True' Then 'False' Else 'True' End),

--Forma de Pago Factura
'IDFormaPagoFactura'=IsNull(V.IdFormaPagoFactura, 0),
'Forma.Pago'=IsNull(FPF.Referencia, '---'),
'FormaPago'=IsNull(FPF.Referencia, '---'),
'CancelarAutomatico'=IsNull(FPF.CancelarAutomatico, 'False'),
'Decimales'=(select Top(1) Decimales from Moneda where id = V.IDMoneda),
'Impresion'=(Case When (V.IDMoneda) = 1 Then '' Else (select Top(1) Impresion from Moneda where id = V.IDMoneda) End),
'DiasCredito'= (Case when V.Credito = 'True' then DateDiff(day,V.FechaEmision,V.FechaVencimiento) else 0 end),
--'IDSucursalFiltro'= Isnull((Select IDSucursal from ClienteSucursal where id =  IDSucursalCliente and IDCliente = V.IDCliente),V.IDSucursal),
'IDSucursalFiltro'= IsNull(IsNull((Select Top(1) IDSucursal From ClienteSucursal where IDCliente = v.IDCliente and id = Isnull(IDSucursalCliente,0)),
(Select Top(1) IDSucursal From Cliente where ID = v.IDCliente)), v.IDSucursal),
C.Boleta,
'IDCategoriaCliente'=IsNULL(C.idCategoriaCliente,0),
'RestriccionHorariaEntrega'=(Select Top(1) RestriccionHorariaEntrega from Pedido P join PedidoVenta PV on p.IDTransaccion=pv.IDTransaccionPedido where pv.IDTransaccionVenta = v.IDTransaccion),

'HoraDesde'=(Case when isnull((Select Top(1) RestriccionHorariaEntrega from Pedido P join PedidoVenta PV on p.IDTransaccion=pv.IDTransaccionPedido where pv.IDTransaccionVenta = v.IDTransaccion),0) = 0 then cast('00:00:00' as time) else 
			isnull((select top(1) HoraDesde from Pedido where IDTransaccion= 
			(select top(1) isnull(idtransaccionpedido,0) from pedidoventa where idtransaccionVenta = V.IDTransaccion)),'00:00:00')End),



--'HoraDesde'=(select top(1) HoraDesde from Pedido where IDTransaccion= (select top(1) isnull(idtransaccionpedido,0) 
--			from pedidoventa where idtransaccionVenta = V.IDTransaccion)),
'HoraHasta'=(Case when isnull((Select Top(1) RestriccionHorariaEntrega from Pedido P join PedidoVenta PV on p.IDTransaccion=pv.IDTransaccionPedido where pv.IDTransaccionVenta = v.IDTransaccion),0) = 0 then cast('00:00:00' as time) else 
			isnull((select top(1) HoraHasta from Pedido where IDTransaccion= 
			(select top(1) isnull(idtransaccionpedido,0) from pedidoventa where idtransaccionVenta = V.IDTransaccion)),'00:00:00')End)

--'HoraHasta'=(select top(1) HoraHasta from Pedido where IDTransaccion= (select top(1) isnull(idtransaccionpedido,0) 
--			from pedidoventa where idtransaccionVenta = V.IDTransaccion))

From Venta V
Join Transaccion T On V.IDTransaccion=T.ID
Left Outer Join VPuntoExpedicion PE On V.IDPuntoExpedicion=PE.ID
Left Outer Join TipoComprobante TC On V.IDTipoComprobante=TC.ID
Left Outer Join VCliente C On V.IDCliente=C.ID
Left Outer Join TipoCliente TP on TP.ID=C.IDTipoCliente
Left Outer Join Sucursal S On V.IDSucursal=S.ID
Left Outer Join Deposito D On V.IDDeposito=D.ID
Left Outer Join Moneda M On V.IDMoneda=M.ID
Left Outer Join MotivoAnulacionVenta MAV On MAV.ID=V.IDMotivo
Left outer join FormaPagoFactura FPF on FPF.ID = V.IdFormaPagoFactura

Where V.Procesado='True'








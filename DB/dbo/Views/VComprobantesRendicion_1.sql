﻿CREATE view [dbo].[VComprobantesRendicion]
as

Select 

--Gasto
GFF.Numero,
'Tipo'='GASTO',
'TipoComprobante'=G.[Cod.],
DFF.IDSucursal,
'Sucursal'=S.Descripcion,
DFF.IDGrupo,
DFF.ID,
'Grupo'=GR.Descripcion,
'NroComprobante'=G.NroComprobante,
'RazonSocial'=P.RazonSocial,

--Detalle Fondo Fijo
DFF.IDTransaccionVale,
DFF.IDTransaccionGasto,
DFF.Cancelado,
DFF.IDTransaccionRendicionFondoFijo,
'Fecha'=convert(varchar(50),G.Fecha,5),
G.Total,
OPE.IDTransaccionOrdenPago,
'NroOP'=OPE.Numero,
'NroRendicion'=RFF.Numero  

From DetalleFondoFijo DFF
Join VGasto G on DFF.IDTransaccionGasto=G.IDTransaccion 
Join VGastoFondoFijo GFF On G.IDTransaccion=GFF.IDTransaccion
Join Sucursal S on S.ID=DFF.IDSucursal 
Join Grupo GR on GR.ID=DFF.IDGrupo 
Join Proveedor  P on G.IDProveedor=P.ID 
Join VOrdenPagoEgreso OPE on OPE.IDTransaccion=DFF.IDTransaccionGasto 
Left Outer Join VRendicionFondoFijo RFF on RFF.IDTransaccionOrdenPago=OPE.IDTransaccionOrdenPago



 Union all
 
--Vale
Select 
--Vale
V.Numero,
'Tipo'='VALE',
'TipoComprobante'='VALE',
DFF.IDSucursal,
'Sucursal'=S.Descripcion,
DFF.IDGrupo,
DFF.ID,
'Grupo'=GR.Descripcion,
'NroComprobante'=CONVERT(Varchar(50),V.NroComprobante),
'RazonSocial'=V.Nombre,

--Detalle Fondo Fijo
DFF.IDTransaccionVale,
DFF.IDTransaccionGasto,
DFF.Cancelado,
DFF.IDTransaccionRendicionFondoFijo,
'Fecha'=convert(varchar(50),V.Fecha,5),
V.Total,
OPE.IDTransaccionOrdenPago,
'NroOP'=OPE.Numero,
'NroRendicion'=RFF.Numero     

From VVale V 
Join DetalleFondoFijo DFF on DFF.IDTransaccionVale=V.IDTransaccion 
Join Sucursal S on S.ID=DFF.IDSucursal
Join Grupo GR on GR.ID=DFF.IDGrupo 
Join VOrdenPagoEgreso OPE on OPE.IDTransaccion=DFF.IDTransaccionVale 
Left Outer Join VRendicionFondoFijo RFF on RFF.IDTransaccionOrdenPago=OPE.IDTransaccionOrdenPago
Where V.ARendir = 'False' And  V.Anulado = 'False' 























﻿


CREATE view [dbo].[VGastoVale]
as

select 
GV.IDTransaccionGasto ,
V.IDTransaccion, 
V.Fec,
V.Motivo, 
V.Nombre,
V.Total,
V.Saldo,
GV.Importe    

from GastoVale GV
join VGasto G On GV.IDTransaccionGasto=G.IDTransaccion 
join VVale V On GV.IDTransaccionVale= V.IDTransaccion  


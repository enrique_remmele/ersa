﻿CREATE view [dbo].[VEfectivizacion]
as
Select 
E.IDTransaccion,
E.IDSucursal,
'Sucursal'=S.Descripcion, 
S.Ciudad,
E.IDTipoComprobante,
'TipoComprobante'=TC.Descripcion,
E.Numero,
E.NroComprobante,
E.Fecha,
'Fec'=CONVERT(varchar(50),E.Fecha,5),
E.IDTransaccionCheque,
CC.IDCliente,
CC.Cliente,
'Referencia'=CC.CodigoCliente,
CC.RUC,
CC.Titular,
CC.CuentaBancaria,
CC.NroCheque,
CC.IDBanco,
CC.Banco,
CC.Importe,


--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
'Anulado'=isnull(E.Anulado,'False'),
'Estado'=Case When E.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=E.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=E.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=E.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=E.IDTransaccion)
From Efectivizacion E
Join VSucursal S on E.IDSucursal=S.ID 
Join TipoComprobante TC on TC.ID=E.IDTipoComprobante 
Join VChequeCliente CC on CC.IDTransaccion=E.IDTransaccionCheque 
Join Transaccion T on T.ID=E.IDTransaccion













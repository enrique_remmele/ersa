﻿Create view VTipoProductoCuentaContableMotivoNC
as 
select
TPNC.IDTipoProducto,
'TipoProducto' = T.Descripcion,
TPNC.CuentaContableAcuerdo,
'IDCuentaContableAcuerdo' = (Select ID from CuentaContable where Codigo = TPNC.CuentaContableAcuerdo),
tpnc.CuentaContableDescuento,
'IDCuentaContableDescuento' = (Select ID from CuentaContable where Codigo = TPNC.CuentaContableDescuento),
TPNC.CuentaContableDiferenciaPrecio,
'IDCuentaContableDiferenciaPrecio' = (Select ID from CuentaContable where Codigo = TPNC.CuentaContableDiferenciaPrecio),
'IDUltimoUsuarioModificacion' = UltimoUsuarioModificacion,
'UltimoUsuarioModificacion' = (Select Usuario from Usuario where ID = TPNC.UltimoUsuarioModificacion),
TPNC.UltimaFechaModificacion
from 
TipoProductoCuentaContableMotivoNC TPNC
join TipoProducto T on TPNC.IDTipoProducto = T.ID

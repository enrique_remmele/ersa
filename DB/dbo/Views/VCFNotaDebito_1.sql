﻿

CREATE View [dbo].[VCFNotaDebito]
As
Select
V.*,
CFV.Credito,
CFV.Contado,
'CRE/CONT'=(Case When(CFV.Credito) = 'True' Then 'CREDITO' Else (Case When(CFV.Contado) = 'True' Then 'CONTADO' Else '---' End) End),
CFV.BuscarEnCliente,
CFV.BuscarEnProducto,

--Tipo Cuenta Fija
CFV.IDTipoCuentaFija,
'TipoCuentaFija'=TC.Tipo,
'CuentaFija'=TC.Descripcion,
TC.IDImpuesto,

--Descuento
CFV.IDTipoDescuento,
'TipoDescuento'=(ISNULL(TD.Descripcion, '')),

--Tipo de Cuenta
TC.Campo,
TC.IncluirDescuento,
TC.IncluirImpuesto

From VCF V
Join CFNotaDebito CFV On V.IDOperacion=CFV.IDOperacion And V.ID=CFV.ID
Join VTipoCuentaFija TC On CFV.IDTipoCuentaFija=TC.ID
Left Outer Join TipoDescuento TD On CFV.IDTipoDescuento=TD.ID






﻿CREATE View [dbo].[VDetalleCanjeRecaudacionTarjeta]
As

Select

D.ID,
'Sel'='True',
'CodigoComprobante' = D.CodigoComprobante, 
'Comprobante' = D.Comprobante,
'Banco'='',
'BancoLocal'='', 
'Importe'= DDB.Importe,
'Cliente'='',
'CuentaBancaria'='',
'Estado'='',
'Numero'=0,
'IDSucursal'=0,
'Ciudad'='',


DDB.IDTransaccion,
DDB.IDTransaccionTarjeta,
--Transaccion
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),
D.Deposito,
'IDCuentaBancariaDeposito'=0,
DB.Fecha,
'NroOperacion'=DB.Numero,
TC.Restar,
D.Cotizacion

From DetalleCanjeRecaudacion DDB
Join Transaccion T On DDB.IDTransaccion =T.ID
Join CanjeRecaudacion DB on DDB.IDTransaccion = DB.IDTransaccion 
Join VFormaPagoTarjeta D on DDB.IDTransaccionTarjeta = D.IDTransaccion and D.ID = DDB.IDDetalleDocumento
Join TipoComprobante TC On D.IDTipoComprobante=TC.ID











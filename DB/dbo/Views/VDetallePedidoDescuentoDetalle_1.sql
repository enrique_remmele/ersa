﻿

CREATE View [dbo].[VDetallePedidoDescuentoDetalle]
As
Select
DP.IDTransaccion,

--Producto
DP.IDProducto,
DP.ID,
'Producto'=P.Descripcion,
'Descripcion'=(Case When DP.Observacion='' Then P.Descripcion Else P.Descripcion + ' - ' + DP.Observacion End),
'CodigoBarra'=P.CodigoBarra,
'UnidadMedida'=P.UnidadMedida,
P.Referencia,
DP.Observacion,
PE.Cotizacion,
PE.IDMoneda,

--Deposito
DP.IDDeposito,
'Deposito'=D.Descripcion,

--Cantidad y Precio
DP.Cantidad,
DP.PrecioUnitario,
'Pre. Uni.'=DP.PrecioUnitario,
--DP.Total, ***** Se calcula el total sin descuento tactico, porque en esta vista, el descuento tactico esta declarado a parte
'Total'= DP.PrecioUnitario * DP.Cantidad,

--Impuesto
DP.IDImpuesto,
'Impuesto'=I.Descripcion,
'Ref Imp'=I.Referencia,
DP.TotalImpuesto,
DP.TotalDiscriminado,
'Exento'=I.Exento,
'IDProductoDescuento' = (Case when DP.IDImpuesto = 1 then 218 else
						(Case When DP.IDImpuesto = 2 then 219 else 
						230 end)end),

--Descuento
DP.TotalDescuento,
'Descuento'=DP.TotalDescuento,
DP.DescuentoUnitario,
'Desc Uni'=DP.DescuentoUnitario,
DP.DescuentoUnitarioDiscriminado,
DP.TotalDescuentoDiscriminado,
DP.PorcentajeDescuento,
'Desc %'=DP.PorcentajeDescuento,
'PrecioNeto'=IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0),
'TotalPrecioNeto'=(IsNull(DP.PrecioUnitario,0) - IsNull(DP.DescuentoUnitario,0)) * DP.Cantidad,

--Costos
DP.CostoUnitario,
DP.TotalCosto,
DP.TotalCostoImpuesto,
DP.TotalCostoDiscriminado,

----Cantidad de Caja
'Caja'=IsNull(DP.Caja, 'False'),
DP.CantidadCaja,
'Cajas'=convert (decimal (10,2) ,IsNull((DP.Cantidad / P.UnidadPorCaja), 0)),
'UnidadMedidas'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IDUnidadMedida  ),'UND'),
--Unidad de medida
'UnidadMedidaConvertir'= ISNULL((Select Referencia  from UnidadMedida Where ID= P.IdUnidadMedidaConvertir ),'UND'),
'UnidadConvertir'=Isnull (P.UnidadConvertir,1),

--Totales
'TotalBruto'= isnull(DP.TotalDescuento,0)+isnull(DP.Total,0),
'TotalSinDescuento'=DP.Total - DP.TotalDescuento,
'TotalSinImpuesto'=DP.Total - DP.TotalImpuesto,
'TotalNeto'=DP.Total - (DP.TotalImpuesto + DP.TotalDescuento),
'TotalNetoConDescuentoNeto'=DP.TotalDiscriminado + DP.TotalDescuentoDiscriminado,

--Pedido
PE.Fecha,
PE.IDCliente,
PE.FechaFacturar,
PE.IDVendedor,
'Vendedor'=(select Nombres from Vendedor where id = PE.IDVendedor),
'FechaFactura'=(Select Top(1) V.FechaEmision From VVenta V Join PedidoVenta PV On V.IDTransaccion=PV.IDTransaccionVenta Where PV.IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc),
PE.IDSucursal,

'Facturado'=(Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End),
'Estado'=(Case When (PE.Anulado) = 'True' Then 'Anulado' Else (Case When (Case When (Select Top(1) IDTransaccionPedido From PedidoVenta Where IDTransaccionPedido=PE.IDTransaccion Order By IDTransaccionPedido Desc) Is Null Then 'False' Else 'True' End) = 'True' Then 'Facturado' Else 'Pendiente' End) End),
'Lista de Precio'=IsNull((Select LP.Descripcion From ListaPrecio LP Where LP.ID=PE.IDListaPrecio), '---'),
'IDTipoCliente'=(Select idtipocliente from cliente where id = PE.IDCliente),
T.IDUsuario,

'CantidadAEntregar'= Isnull(DP.CantidadAEntregar,DP.Cantidad),
PE.Numero,
'Suc'= (Select codigo from Sucursal where Id = PE.IDSucursal),
'Peso'=cast(P.Peso as decimal(18,2)),
P.IDTipoProducto,
PE.Anulado,
'EntregaCliente'=(Case When (PE.EntregaCliente) = 'True' Then 'True' Else 'False' End),
'CancelarAutomatico'=ISnull((Select FPF.CancelarAutomatico from FormaPagoFactura FPF where FPF.id = PE.IDFormaPagoFactura), 'False'),
'DescuentoUnitarioFACT'=DV.DescuentoUnitario,
'DescuentoUnitarioNCDiferenciaPrecio'= (select dbo.FDescuentoUnitarioDiferenciaPrecio(DV.IDProducto,DV.IDTransaccion)),
'DescuentoUnitarioNCAcuerdo' = (select dbo.FDescuentoUnitarioAcuerdo(DV.IDProducto,DV.IDTransaccion)),
'DescuentoUnitarioNCDiferenciaPeso' = (select dbo.FDescuentoUnitarioDiferenciaPeso(DV.IDProducto,DV.IDTransaccion)),
'PrecioUnitarioNeto' = (select dbo.FPrecioUnitarioNeto(DV.IDProducto,DV.IDTransaccion))

From DetallePedido DP
join PedidoVenta PV on DP.IDTRansaccion = PV.IDTransaccionPedido
Join DetalleVenta DV on PV.IDTransaccionVenta = DV.IDTransaccion and DP.IDProducto = DV.IDProducto
Join Pedido PE On DP.IDTransaccion=PE.IDTransaccion
Join VProducto P On DP.IDProducto=P.ID
Join Deposito D On DP.IDDeposito=D.ID
Join Impuesto I On DP.IDImpuesto=I.ID
Join Transaccion T on T.ID = PE.IDTransaccion

Where PE.Procesado='True'






























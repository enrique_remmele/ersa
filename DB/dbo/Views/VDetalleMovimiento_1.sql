﻿CREATE View [dbo].[VDetalleMovimiento]
As

Select	

DM.IDTransaccion,
DM.IDProducto,
DM.ID,
'IDDepositoEntrada' = IsNull((Select D.ID From Deposito D Where D.ID=M.IDDepositoEntrada), 0) ,
'DepositoEntrada'=IsNull((Select D.Descripcion From Deposito D Where D.ID=M.IDDepositoEntrada), '---') ,
'IDDepositoSalida' = IsNull((Select D.ID From Deposito D Where D.ID=M.IDDepositoSalida), 0) ,
'DepositoSalida'=IsNull((Select D.Descripcion From Deposito D Where D.ID=M.IDDepositoSalida), '---'),
'CuentaContableEntrada'=IsNull((Select (Case when D.IDTipoDeposito = 5 then  P.CuentaContableCompra else (Case when P.IDTipoProducto = 27 then D.CuentaContableCombustible else D.CuentaContableMercaderia end) end) From Deposito D Where D.ID=M.IDDepositoEntrada), '---') ,
'CuentaContableSalida'=IsNull((Select (Case when D.IDTipoDeposito = 5 then  P.CuentaContableCompra else (Case when P.IDTipoProducto = 27 then D.CuentaContableCombustible else D.CuentaContableMercaderia end) end) From Deposito D Where D.ID=M.IDDepositoSalida), '---'),

DM.Observacion,
DM.Cantidad,

--Impuesto
DM.IDImpuesto,
'Impuesto'=I.Descripcion,

--Totales
DM.PrecioUnitario,
'Total'=round(DM.Total,0),
DM.TotalImpuesto,
DM.TotalDiscriminado,
DM.TotalDescuento,


DM.Caja,
DM.CantidadCaja,
M.Anulado,

--Operacion
M.Numero,
M.Fecha,
M.Fec,

--Producto
'Producto'=Concat(P.Referencia,' - ',P.Descripcion) + (Case When DM.Observacion!='' Then ' - ' + DM.Observacion Else '' End),
'Descripcion'=P.Descripcion,
'CodigoBarra'=P.CodigoBarra,
'Ref'=P.Referencia,
P.Peso,
p.IDTipoProducto,
P.IDLinea,

 M.IDMotivo,
 M.Motivo,
 M.[Cod.],
 M.IDTipoOperacion,
 M.Operacion,
 M.Suc,
 'NroComprobante' = M.Comprobante,
 M.Entrada,
 M.Salida,
 M.IDSucursal,
 'CantidadEntrada'=Case When (M.Entrada) = 'True' Then DM.Cantidad Else 0 End,
 'CantidadSalida'=Case When (M.Salida) = 'True' Then DM.Cantidad Else 0 End,
 'PesoEntrada'=Case When (M.Entrada) = 'True' Then DM.Cantidad*P.Peso Else 0 End,
 'PesoSalida'=Case When (M.Salida) = 'True' Then DM.Cantidad*P.Peso Else 0 End,
 M.Autorizacion,

 CASE T.Entrada
      WHEN 1 THEN Round(M.Total,0)
      ELSE 0
      END AS TotalEntrada
      , CASE T.Salida
      WHEN 1 THEN Round(M.Total,0)
      ELSE 0
      END AS TotalSalida,


 CASE T.Entrada
      WHEN 1 THEN Round(DM.Total,0)
      ELSE 0
      END AS TotalDetalleEntrada
      , CASE T.Salida
      WHEN 1 THEN Round(DM.Total,0)
      ELSE 0
      END AS TotalDetalleSalida,

M.IdTipoComprobante,
M.MovimientoStock,
M.DescargaStock,
M.DescargaCompra,
M.MovimientoCombustible,
M.MovimientoMateriaPrima,
M.Camion,
M.IDCamion,
M.Chofer,
M.IDChofer,
'CuentaContableMercaderia'=P.CuentaContableCompra,
M.NumeroSolicitudCliente
 
From DetalleMovimiento DM
Join VMovimiento M On DM.IDTransaccion=M.IDTransaccion
Join TipoOperacion T On M.IDTipoOperacion=T.ID
Join Producto P On DM.IDProducto=P.ID
Join Impuesto I On DM.IDImpuesto=I.ID






















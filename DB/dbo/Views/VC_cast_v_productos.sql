﻿CREATE View [dbo].[VC_cast_v_productos]
As
Select
'cod_producto'=P.Referencia,
'descripcion'=P.Descripcion,
'codigo_barras'=P.CodigoBarra,
'familia'=P.TipoProducto,
'Codigo_familia'=P.IDTipoProducto,
'iva'=P.Impuesto,
'estado'=P.Estado,
'unidad_medida'=P.UnidadMedida,
'precio base'=0

From VProducto P

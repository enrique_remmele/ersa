﻿



CREATE View [dbo].[VCargaMercaderia]
As
Select 

--Cabecera
'Ciudad'=(Select CIU.Codigo From Ciudad CIU Where CIU.ID=S.IDCiudad),
S.IDCiudad,
CM.Numero,
CM.IDSucursal,
'Sucursal'=S.Descripcion,
'Suc'=S.Codigo,
CM.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
'Comprobante'=CM.Comprobante,
'Lote'=LD.Comprobante,
CM.Fecha,
'Fec'=CONVERT(varchar(50), CM.Fecha, 6),
'Distribuidor'=LD.Distribuidor,
LD.IDDistribuidor,
'Chofer'=LD.Chofer, LD.IDChofer,
'Vehiculo'=LD.Camion,
'Zona'=LD.Zona, LD.IDZonaVenta,
CM.Observacion,

--Lote
LD.FechaReparto,
'Fec Reparto'=CONVERT(varchar(50), LD.FechaReparto, 6),

--Totales

--Transaccion
CM.IDTransaccion,
'FechaTransaccion'=T.Fecha,
'IDDepositoTransaccion'=T.IDDeposito,
'IDSucursalTransaccion'=T.IDSucursal,
'IDTerminalTransaccion'=T.IDTerminal,
T.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=T.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=T.IDUsuario),

--Anulacion
CM.Anulado,
'Estado'=Case When CM.Anulado='True' Then 'Anulado' Else '---' End,
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=CM.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CM.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=CM.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=CM.IDTransaccion)

From CargaMercaderia CM
Join Transaccion T On CM.IDTransaccion=T.ID
Join VLoteDistribucion LD On CM.IDTransaccionLote=LD.IDTransaccion
Left Outer Join Sucursal S On CM.IDSucursal=S.ID
Left Outer Join TipoComprobante TC On CM.IDTipoComprobante=TC.ID










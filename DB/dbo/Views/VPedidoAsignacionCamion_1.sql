﻿CREATE view [dbo].[VPedidoAsignacionCamion] as
select
Distinct DAC.IDTransaccionPedido, 
AC.Comprobante,
DAC.Tipo,
AC.IDCamion,
AC.Camion,
AC.IDChofer,
AC.Chofer,
AC.IDSucursal,
AC.Sucursal,
AC.PedidoCliente

 from vDetalleAsignacionCamion DAC
join vAsignacionCamion AC on DAC.IDTransaccion = AC.IDTransaccion


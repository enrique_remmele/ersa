﻿

CREATE View [dbo].[VChequera]
As
Select 
C.ID,
C.IDBanco,
C.NroComprobante,
'ReferenciaBanco'=B.Referencia,
'DescripcionBanco'=B.Descripcion,
C.IdCuentaBancaria,
CB.CuentaBancaria,
C.NroDesde,
C.NroHasta,
C.AUtilizar,
C.Observacion,
C.Estado
from Chequera C
JOin Banco B on B.ID = C.IdBanco
Join CuentaBancaria CB on CB.id = C.IDCuentaBancaria





﻿

CREATE View [dbo].[VVentaDocumentosSinAplicar]

As

Select
'IDTransaccionDocumento'=NC.IDTransaccion,
'Documento'= 'NOTACREDITO',
--Venta
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
V.Condicion,

--Nota de Credito
NC.Fecha,
'Comp.'=NC.[Cod.] + ' ' + NC.Comprobante,

'Usuario'=NC.Usuario,
'Tipo'=NC.Tipo,
'Estado'=NC.[Estado NC], 
NC.Total,
'Importe'=(Case When (NC.Anulado) = 'True' Then 0 Else Sum(NCV.Importe) End),

NC.Observacion,
'Aplicado'='True'
From NotaCreditoVenta  NCV
Join VNotaCredito NC On NCV.IDTransaccionNotaCredito=NC.IDTransaccion
join VVenta  V on NCV.IDTransaccionVentaGenerada  = V .IDTransaccion 
Where NC.Aplicar = 'True'
And NC.Anulado = 0
Group by NC.IDTransaccion,V.IDTransaccion, V.Comprobante,V.IDCliente,V.Saldo,V.FechaEmision,V.Condicion,NC.Fecha,NC.[Cod.],NC.Comprobante,NC.Usuario,NC.Tipo,NC.[Estado NC], NC.Total,NC.Observacion,NC.Anulado

Union All

Select
'IDTransaccionDocumento'=PNC.IDTransaccion,
'Documento'= 'PEDIDONOTACREDITO',
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
V.Condicion,

--Pedido Nota de Credito
'Fecha'= PNC.Fecha,
'Comp.'='Ped. NCR (' + Convert(varchar(50), PNC.Numero) + ') ',
'Usuario'=PNC.Usuario,
'Tipo'=PNC.MotivoNotaCredito,
'Estado'=(Case When PNC.Anulado='True' Then 'Anulado' Else 
			'---' 
			End),

PNC.Total,
'Importe'=(Case When (PNC.Anulado) = 'True' Then 0 Else PNCPV.Total End),

PNC.Observacion,
'Aplicado'='True'
From PedidoNotaCreditoPedidoVenta   PNCPV
Join PedidoVenta PV On PNCPV.IDTransaccionPedidoVenta= PV.IDTransaccionPedido
join VVenta  V on PV.IDTransaccionVenta= V .IDTransaccion 
Join VPedidoNotaCredito PNC On PNCPV.IDTransaccionPedidoNotaCredito = PNC.IDTransaccion
where PNC.ProcesadoNC = 'PENDIENTE'
and pnc.Anulado = 0 

Union All

Select
'IDTransaccionDocumento'=PNC.IDTransaccion,
'Documento'= 'PEDIDONOTACREDITO',
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
V.Condicion,

--Pedido Nota de Credito
'Fecha'= PNC.Fecha,
'Comp.'='Ped. NCR (' + Convert(varchar(50), PNC.Numero) + ') ',
'Usuario'=PNC.Usuario,
'Tipo'=PNC.MotivoNotaCredito,
'Estado'=(Case When PNC.Anulado='True' Then 'Anulado' Else 
			'---' 
			End),

PNC.Total,
'Importe'=(Case When (PNC.Anulado) = 'True' Then 0 Else PNCPV.Importe End),

PNC.Observacion,
'Aplicado'='True'

From PedidoNotaCreditoVenta   PNCPV
join VVenta  V on PNCPV.IDTransaccionVentaGenerada= V .IDTransaccion 
Join VPedidoNotaCredito PNC On PNCPV.IDTransaccionPedidoNotaCredito = PNC.IDTransaccion
where PNC.ProcesadoNC = 'PENDIENTE'
and pnc.Anulado = 0 

Union All

Select
'IDTransaccionDocumento'=PNC.IDTransaccion,
'Documento'= 'PEDIDONOTACREDITO',
V.IDTransaccion, 
V.Comprobante,
V.IDCliente,
V.Saldo,
V.FechaEmision,
V.Condicion,

--Pedido Nota de Credito
'Fecha'= PNC.Fecha,
'Comp.'='Ped. NCR (' + Convert(varchar(50), PNC.Numero) + ') ',
'Usuario'=PNC.Usuario,
'Tipo'=PNC.MotivoNotaCredito,
'Estado'=(Case When PNC.Anulado='True' Then 'Anulado' Else 
			'---' 
			End),

PNC.Total,
'Importe'=(Case When (PNC.Anulado) = 'True' Then 0 Else Sum(PNCPV.Total) End),

PNC.Observacion,
'Aplicado'='True'

From DetallePedidoNotaCredito   PNCPV
join VVenta  V on PNCPV.IDTransaccionVenta= V .IDTransaccion 
Join VPedidoNotaCredito PNC On PNCPV.IDTransaccion = PNC.IDTransaccion
where PNC.ProcesadoNC = 'PENDIENTE'
and pnc.Anulado = 0 
and PNC.Devolucion = 1
Group by PNC.IDTransaccion,V.IDTransaccion, V.Comprobante,V.IDCliente,V.Saldo,V.FechaEmision,V.Condicion,PNC.MotivoNotaCredito,PNC.Fecha,PNC.Numero,PNC.Usuario,PNC.Tipo, PNC.Total,PNC.Observacion,PNC.Anulado


﻿

CREATE View [dbo].[VDetalleAsientoContable2]

As

--Ventas
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Venta - ' + '  ' + V.Comprobante +'  ' + D.Observacion,
D.Debito,
D.Credito,
'Tipo'='VENTA DETALLADA',
'Observacion'=C.RazonSocial,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
V.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID 
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Venta V On D.IDTransaccion=V.IDTransaccion
Join Cliente C on V.IDCliente = C.ID

Union All

--Ventas Resumidas
Select
'NumeroAsiento'=0,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Suc,
A.Moneda,
'Operacion'= ' Ventas del ' + CONVERT(varchar(50),A.Fecha) + ' - ' + A.Sucursal,
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='VENTA RESUMIDA',
'Observacion'='Ventas del dia',
'Orden'=D.ID,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
'Usuario'='',
'IDUsuario'=0,
'FechaTransaccion'=''

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID 
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Venta V On D.IDTransaccion=V.IDTransaccion
Group By A.Fecha, D.ID,CC.Codigo,D.CuentaContable, CC.DescripcionAbuelo, A.Suc, A.IDSucursal, A.Sucursal,A.Moneda, D.ID, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto

Union All

--Nota de Débito Cliente
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Deb. Cli '+' '+TC.Codigo+ '  ' +CONVERT(varchar(50),ND.NroComprobante)+ ' ' + (Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
D.Debito,
D.Credito,
'Tipo'='NOTA DEBITO CLIENTE',
'Observacion'=ND.Observacion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
ND.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID 
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join NotaDebito ND On D.IDTransaccion=ND.IDTransaccion
Join TipoComprobante TC on ND.IDTipoComprobante =TC.ID
Join Cliente C on ND.IDCliente = C.ID

union all

--Nota de Crédito Cliente
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Cred. Cli '+' '+TC.Codigo + '  ' +CONVERT(varchar(50),NC.NroComprobante)+'  '+(Case When (C.Credito) = 'True' Then 'CRED' Else 'CONT' End),
D.Debito,
D.Credito,
'Tipo'='NOTA CREDITO CLIENTE',
'Observacion'=NC.Observacion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
NC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID 
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join NotaCredito NC On D.IDTransaccion=NC.IDTransaccion
Join TipoComprobante TC on NC.IDTipoComprobante =TC.ID
join Cliente C on NC.IDCLiente = C.ID

union all

--Cobranza Contado Detallado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. por Lote: '+'  '+ Convert(varchar(50), CCO.Numero),
D.Debito,
D.Credito,
'Tipo'='COBRANZA CONTADO DETALLADO',
'Observacion'='Nro de Lote: ' + LD.Comprobante,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
CCO.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID 
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join CobranzaContado CCO On D.IDTransaccion=CCO.IDTransaccion
Join LoteDistribucion LD On CCO.IDTransaccionLote=LD.IDTransaccion

union all

--Cobranza Crédito Detallado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Crédito: '+'  '+ Convert(varchar(50), CCR.Numero),
D.Debito,
D.Credito,
'Tipo'='COBRANZA CREDITO DETALLADO',
'Observacion'=TC.Codigo + '  ' +CONVERT(varchar(50),CCR.NroComprobante)+' - '+ISNULL(C.RazonSocial,''),
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
CCR.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID 
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join CobranzaCredito CCR On D.IDTransaccion=CCR.IDTransaccion
Join TipoComprobante TC on CCR.IDTipoComprobante = TC.ID
left outer join Cliente C on CCR.IDCliente = C.ID


union all

--Cobranza Contado Resumido
Select
'NumeroAsiento'=0,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Contado del ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='COBRANZA CONTADO RESUMIDO',
'Observacion'='Cobranzas del dia',
'Orden'=D.ID,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
'Usuario'='',
'IDUsuario'=0,
'FechaTransaccion'=''

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join CobranzaContado CCO On D.IDTransaccion=CCO.IDTransaccion
Group By A.Fecha, CC.Codigo, D.CuentaContable, CC.DescripcionAbuelo,A.IDSucursal,A.Sucursal,A.Moneda, D.ID, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto

union all

--Cobranza Crédito Resumido
Select
'NumeroAsiento'=0,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cob. Crédito del ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='COBRANZA CREDITO RESUMIDO',
'Observacion'='Cobranzas del dia',
'Orden'=D.ID,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
'Usuario'='',
'IDUsuario'=0,
'FechaTransaccion'=''

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join CobranzaCredito CCR On D.IDTransaccion=CCR.IDTransaccion
Group By A.Fecha, CC.Codigo, D.CuentaContable, CC.DescripcionAbuelo,A.IDSucursal,A.Sucursal,A.Moneda, D.ID, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto

union all

--Compra
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Compra: '+'  '+ Convert(varchar(50), C.Numero),
D.Debito,
D.Credito,
'Tipo'='COMPRA',
'Observacion'= TC.Descripcion + ' ' + CONVERT(varchar(50),C.NroComprobante)+' - '+IsNull(P.RazonSocial, ''),
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
C.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Compra C On D.IDTransaccion=C.IDTransaccion
Join TipoComprobante TC on C.IDTipoComprobante = TC.ID
Left Outer Join Proveedor P on C.IDProveedor = P.ID

union all

--Gasto Contado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Gasto Contado: ' + convert(varchar(50),G.Numero),
D.Debito,
D.Credito,
'Tipo'='GASTO CONTADO',
'Observacion'=TC.Codigo+' '+CONVERT(varchar(50),G.NroComprobante)+' : '+CONVERT(varchar(50),Isnull(P.RazonSocial,'')),
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
G.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join vGasto G On D.IDTransaccion=G.IDTransaccion
Join TipoComprobante TC on G.IDTipoComprobante = TC.ID
Left Outer Join Proveedor P on G.IDProveedor = P.ID
Where isnull(G.Credito,'False') = 'False'

Union All

--Gasto Credito
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Gasto Credito: ' + convert(varchar(50),G.Numero),
D.Debito,
D.Credito,
'Tipo'='GASTO CREDITO',
'Observacion'=TC.Codigo+' '+CONVERT(varchar(50),G.NroComprobante)+' : '+CONVERT(varchar(50),Isnull(P.RazonSocial,'')),
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
G.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join vGasto G On D.IDTransaccion=G.IDTransaccion
Join TipoComprobante TC on G.IDTipoComprobante = TC.ID
Left Outer Join Proveedor P on G.IDProveedor = P.ID
Where G.Credito = 'True'

union all

--Nota de Débito Proveedor

Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Deb. Prov: '+'  '+ convert(varchar(50), ND.Numero),
D.Debito,
D.Credito,
'Tipo'='NOTA DEBITO PROVEEDOR',
'Observacion'=TC.Codigo + '  ' +CONVERT(varchar(50),ND.NroComprobante)+' - '+Isnull(P.RazonSocial,''),
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
ND.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join NotaDebitoProveedor ND On D.IDTransaccion=ND.IDTransaccion
Join TipoComprobante TC on ND.IDTipoComprobante = TC.ID
Left Outer Join Proveedor P on ND.IDProveedor = P.ID

union all

--Nota de Crédito Proveedor

Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Nota Cred. Prov '+'  '+ convert(varchar(50),NC.Numero),
D.Debito,
D.Credito,
'Tipo'='NOTA CREDITO PROVEEDOR',
'Observacion'=TC.COdigo + '  ' +CONVERT(varchar(50),NC.NroComprobante)+' - '+ISnull(P.RazonSocial, ''),
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
NC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join NotaCreditoProveedor NC On D.IDTransaccion=NC.IDTransaccion
Join TipoComprobante TC on NC.IDTipoComprobante = TC.ID
Left Outer Join Proveedor P on NC.IDProveedor = P.ID

union all

--Depósito Bancario Detallado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Dep. Bancario '+'  ' + convert(varchar(50), DB.Numero),
D.Debito,
D.Credito,
'Tipo'='DEPOSITO DETALLADO',
'Observacion'= DB.TipoComprobante + ' ' + convert(varchar(50), DB.Comprobante) + ' - ' +  DB.Banco + ' - Nro Cuenta: ' + DB.Cuenta,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
DB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VDepositoBancario DB On D.IDTransaccion=DB.IDTransaccion

union all

--Depósito Bancario Resumido
Select
'NumeroAsiento'=0,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= ' Depósito del  ' + CONVERT(varchar(50),A.Fecha),
'Debito'=SUM(D.Debito),
'Credito'=SUM(D.Credito),
'Tipo'='DEPOSITO RESUMIDO',
'Observacion'='Depositos del Dia',
'Orden'=D.ID,
'Resumido'='True',
'IDTransaccion'=0,
0,
'NroComprobante'='',
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
'Usuario'='',
'IDUsuario'=0,
'FechaTransaccion'=''

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join DepositoBancario DB On D.IDTransaccion=DB.IDTransaccion
Group By A.Fecha, CC.Codigo, D.CuentaContable, CC.DescripcionAbuelo,A.IDSucursal,A.Sucursal,A.Moneda, D.ID, D.ID, D.Observacion, A.IDCentroCosto, A.CentroCosto

union all

--Descuento Cheque
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Desc. Cheque '+'  '+ convert(varchar(50), DC.Numero),
D.Debito,
D.Credito,
'Tipo'='DESCUENTO CHEQUE',
'Observacion'= DC.Observacion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
DC.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join DescuentoCheque DC On D.IDTransaccion=DC.IDTransaccion

Union All

--Débito Bancario
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Deb. Bancario '+'  '+ convert(varchar(50), DCB.Numero),
D.Debito,
D.Credito,
--EL TIPO ES DEBITO/CREDITO PORQUE EN EL FORM DE ASIENTOS CONTABLES NO DISCRIMINA
--SI SE CAMBIA, CAMBIAR EL FORM TAMBIEN
'Tipo'='DEBITO/CREDITO BANCARIO',
'Observacion'=DCB.Observacion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
DCB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join DebitoCreditoBancario DCB On D.IDTransaccion=DCB.IDTransaccion
Where DCB.Debito = 'True'

Union All

--Crédito Bancario
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cred. Bancario '+'  '+CONVERT(varchar(50),DCB.Numero),
D.Debito,
D.Credito,
--EL TIPO ES DEBITO/CREDITO PORQUE EN EL FORM DE ASIENTOS CONTABLES NO DISCRIMINA
--SI SE CAMBIA, CAMBIAR EL FORM TAMBIEN
'Tipo'='DEBITO/CREDITO BANCARIO',
'Observacion'=DCB.Observacion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
DCB.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join DebitoCreditoBancario DCB On D.IDTransaccion=DCB.IDTransaccion
Where DCB.Credito = 'True'

Union All

--Orden de Pago
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Orden Pago '+'  '+CONVERT(varchar(50),OP.Numero),
D.Debito,
D.Credito,
'Tipo'='ORDEN PAGO',
'Observacion'=OP.Observacion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
OP.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join OrdenPago OP On D.IDTransaccion=OP.IDTransaccion

union all
--Entrega de cheques
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Entrega de Cheque OP '+'  '+CONVERT(varchar(50),VO.Numero),
D.Debito,
D.Credito,
'Tipo'='ENTREGA DE CHEQUE',
'Observacion'=OP.RetiradoPor,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
'IDTipoComprobante'=(select IDTipoComprobante from OrdenPago where Idtransaccion = OP.IdTransaccionOP),
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join EntregaChequeOP OP On D.IDTransaccion=OP.IDTransaccion
Join OrdenPago VO On OP.IDTransaccionOP = VO.IDTransaccion

Union All

--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Mov. Stock '+'  '+CONVERT(varchar(50),T.Descripcion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='MOVIMIENTO',
'Observacion'=MM.Descripcion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Movimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
Join TipoOperacion T On M.IDTipoOperacion=T.ID
Join MotivoMovimiento MM On M.IDMotivo=MM.ID
where M.MovimientoStock = 'True'

Union All

--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Desc. Stock '+'  '+CONVERT(varchar(50),T.Descripcion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='DESCARGASTOCK',
'Observacion'=MM.Descripcion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Movimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
Join TipoOperacion T On M.IDTipoOperacion=T.ID
Join MotivoMovimiento MM On M.IDMotivo=MM.ID
where M.DescargaStock = 'True'

UNION ALL

--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Cons. Combustible '+'  '+CONVERT(varchar(50),T.Descripcion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='COMBUSTIBLE',
'Observacion'=MM.Descripcion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Movimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
Join TipoOperacion T On M.IDTipoOperacion=T.ID
Join MotivoMovimiento MM On M.IDMotivo=MM.ID
where M.MovimientoCombustible = 'True'

UNION ALL

--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Mov. Mat. Prima Balanceado '+'  '+CONVERT(varchar(50),T.Descripcion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='MATERIAPRIMA',
'Observacion'=MM.Descripcion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Movimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
Join TipoOperacion T On M.IDTipoOperacion=T.ID
Join MotivoMovimiento MM On M.IDMotivo=MM.ID
where M.MovimientoMateriaPrima = 'True'

union all

--Ajuste
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Ajuste ',
D.Debito,
D.Credito,
'Tipo'='AJUSTE',
'Observacion'=AI.Sucursal + ' - ' + AI.Deposito,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join VControlInventario AI On D.IDTransaccion=AI.IDTransaccion

union all
--Movimiento
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= 'Descarga de Compras '+'  '+CONVERT(varchar(50),T.Descripcion) + ' ' + convert(varchar(50),M.Numero),
D.Debito,
D.Credito,
'Tipo'='DESCARGACOMPRA',
'Observacion'=MM.Descripcion,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
M.IDTipoComprobante,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Movimiento M On D.IDTransaccion=M.IDTransaccion
Join TipoComprobante TC on TC.ID = M.IDTipoComprobante
Join Operacion O on O.ID = TC.IDOperacion
Join TipoOperacion T On M.IDTipoOperacion=T.ID
Join MotivoMovimiento MM On M.IDMotivo=MM.ID
where M.DescargaCompra = 'True'

Union All

--Asiento Importado
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= '* ' + V.Operacion,
D.Debito,
D.Credito,
'Tipo'='* ASIENTOS IMPORTADOS',
'Observacion'=Convert(varchar(50), V.Operacion) + ' - Nro:' + Convert(varchar(50), V.Numero),
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join AsientoImportado V On D.IDTransaccion=V.IDTransaccion

Union All

--Asiento con Operaciones sin Formularios de Carga
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= O.Descripcion + ' ' + A.Comprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=A.Detalle,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Where O.FormName Is Null

Union All

--Asientos de ajuste cargados manualmente por contabilidad
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= O.Descripcion + ' ' + A.Comprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',-- IMPORTANTE NO BORRAR Mantener el tipo Otros al igual que los asientos sin formulario de carga, sino cambiar en el codigo tambien
'Observacion'=A.Detalle,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Where O.ID =36 --IMPORTANTE NO BORRAR Asientos de ajuste cargados manualmente por contabilidad

Union All

--Asiento con Operaciones en Comprobantes para Libro IVA
Select
'NumeroAsiento'=A.Numero,
'Codigo'=(IsNull(CC.Codigo, D.CuentaContable)),
'Descripcion'=IsNull(CC.DescripcionAbuelo, '---'),
A.Fecha,
A.IDSucursal,
A.Sucursal,
A.Moneda,
'Operacion'= TC.Codigo + ' ' + C.NroComprobante,
D.Debito,
D.Credito,
'Tipo'='OTROS',
'Observacion'=C.Detalle,
'Orden'=D.ID,
'Resumido'='False',
A.IDTransaccion,
0,
'NroComprobante'=A.Comprobante,
'Primero'=(Case When (D.ID) = 0 Then 'True' Else 'False' End),
'Concepto'=D.Observacion,
A.IDCentroCosto,
A.CentroCosto,
A.Usuario,
A.IDUsuario,
A.FechaTransaccion

From DetalleAsiento D
Left Outer
Join VCuentaContable CC On D.IDCuentaContable=CC.ID
Join VAsiento A on D.IDTransaccion=A.IDTransaccion
Join ComprobanteLibroIVA C On A.IDTransaccion=C.IDTransaccion
Join Transaccion T On A.IDTransaccion=T.ID
Join Operacion O On T.IDOperacion=O.ID
Join TipoComprobante TC On C.IDTipoComprobante=TC.ID


















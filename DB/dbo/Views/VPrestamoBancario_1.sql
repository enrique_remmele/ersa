﻿


CREATE View [dbo].[VPrestamoBancario]

As


Select 
P.IDTransaccion,
P.Numero,
'Num'=P.Numero,
P.IDTipoComprobante,
'DescripcionTipoComprobante'=TC.Descripcion,
'TipoComprobante'=TC.Codigo,
'Cod.'=TC.Codigo,
P.NroComprobante,
'Comprobante'=P.NroComprobante,
TR.IDSucursal,
'Sucursal'=S.Descripcion,
P.Fecha,
'Fec'=CONVERT(varchar(50), P.Fecha, 6),

--CuentaBancaria
P.IDCuentaBancaria,
C.CuentaBancaria,
C.IDBanco,
'Banco'=C.Banco,
C.IDMoneda,
'Moneda'=C.Moneda,

P.Cotizacion,
'Observacion'=(Case When (P.Observacion) ='' Then '--' Else P.Observacion End),
P.PlazoDias,
P.PorcentajeInteres,
P.Capital,
P.Interes,
P.ImpuestoInteres,
P.Gastos,
P.Neto,
P.TipoGarantia,
P.Anulado,
'Estado'=Case When P.Anulado='True' Then 'Anulado' Else 'OK' End,

--Transaccion
'FechaTransaccion'=TR.Fecha,
TR.IDDeposito,

TR.IDTerminal,
TR.IDUsuario,
'usuario'=(Select U.Usuario From Usuario U Where U.ID=TR.IDUsuario),
'UsuarioIdentificador'=(Select U.Identificador From Usuario U Where U.ID=TR.IDUsuario),

--Anulacion
'IDUsuarioAnulacion'=(Select IsNull((Select DA.IDUsuario From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion), 0)),
'UsuarioAnulacion'=(Select IsNull((Select U.Usuario From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), '---')),
'UsuarioIdentificacionAnulacion'=(Select IsNull((Select U.Identificador From DocumentoAnulado DA Join Usuario U On DA.IDUsuario=U.ID  Where DA.IDTransaccion=P.IDTransaccion), '---')),
'FechaAnulacion'=(Select DA.Fecha From DocumentoAnulado DA Where DA.IDTransaccion=P.IDTransaccion),

--Debito Bancario
P.IDTransaccionDebitoCreditoBancario,
'DebitoBancario'=IsNull((Select DB.Suc + ' ' + Convert(varchar(20), DB.Numero) + ' - ' + DB.[Cod.] + ' ' + Convert(varchar(20), DB.Comprobante) From VDebitoCreditoBancario DB Where DB.IDTransaccion=P.IDTransaccionDebitoCreditoBancario), '')

From PrestamoBancario P
Join Transaccion TR On P.IDTransaccion=TR.ID
Join Sucursal S On P.IDSucursal=S.ID
Join VCuentaBancaria C On P.IDCuentaBancaria=C.ID
Join TipoComprobante TC On P.IDTipoComprobante=TC.ID



